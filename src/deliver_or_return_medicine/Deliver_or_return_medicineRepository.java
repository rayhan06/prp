package deliver_or_return_medicine;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Deliver_or_return_medicineRepository implements Repository {
	Deliver_or_return_medicineDAO deliver_or_return_medicineDAO = null;
	
	static Logger logger = Logger.getLogger(Deliver_or_return_medicineRepository.class);
	Map<Long, Deliver_or_return_medicineDTO>mapOfDeliver_or_return_medicineDTOToiD;
	Gson gson;

  
	private Deliver_or_return_medicineRepository(){
		deliver_or_return_medicineDAO = Deliver_or_return_medicineDAO.getInstance();
		mapOfDeliver_or_return_medicineDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Deliver_or_return_medicineRepository INSTANCE = new Deliver_or_return_medicineRepository();
    }

    public static Deliver_or_return_medicineRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Deliver_or_return_medicineDTO> deliver_or_return_medicineDTOs = deliver_or_return_medicineDAO.getAllDTOs(reloadAll);
			for(Deliver_or_return_medicineDTO deliver_or_return_medicineDTO : deliver_or_return_medicineDTOs) {
				Deliver_or_return_medicineDTO oldDeliver_or_return_medicineDTO = getDeliver_or_return_medicineDTOByiD(deliver_or_return_medicineDTO.iD);
				if( oldDeliver_or_return_medicineDTO != null ) {
					mapOfDeliver_or_return_medicineDTOToiD.remove(oldDeliver_or_return_medicineDTO.iD);
				
					
				}
				if(deliver_or_return_medicineDTO.isDeleted == 0) 
				{
					
					mapOfDeliver_or_return_medicineDTOToiD.put(deliver_or_return_medicineDTO.iD, deliver_or_return_medicineDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Deliver_or_return_medicineDTO clone(Deliver_or_return_medicineDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Deliver_or_return_medicineDTO.class);
	}
	
	
	public List<Deliver_or_return_medicineDTO> getDeliver_or_return_medicineList() {
		List <Deliver_or_return_medicineDTO> deliver_or_return_medicines = new ArrayList<Deliver_or_return_medicineDTO>(this.mapOfDeliver_or_return_medicineDTOToiD.values());
		return deliver_or_return_medicines;
	}
	
	public List<Deliver_or_return_medicineDTO> copyDeliver_or_return_medicineList() {
		List <Deliver_or_return_medicineDTO> deliver_or_return_medicines = getDeliver_or_return_medicineList();
		return deliver_or_return_medicines
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Deliver_or_return_medicineDTO getDeliver_or_return_medicineDTOByiD( long iD){
		return mapOfDeliver_or_return_medicineDTOToiD.get(iD);
	}
	
	public Deliver_or_return_medicineDTO copyDeliver_or_return_medicineDTOByiD( long iD){
		return clone(mapOfDeliver_or_return_medicineDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return deliver_or_return_medicineDAO.getTableName();
	}
}


