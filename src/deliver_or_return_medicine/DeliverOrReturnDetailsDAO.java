package deliver_or_return_medicine;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class DeliverOrReturnDetailsDAO  implements CommonDAOService<DeliverOrReturnDetailsDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private DeliverOrReturnDetailsDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"deliver_or_return_medicine_id",
			"is_delivery",
			"doctor_id",
			"doctor_user_name",
			"stock_lot_number",
			"patient_id",
			"patient_user_name",
			"drug_information_type",
			"medicine_generic_name_id",
			"quantity",
			"transaction_date",
			"search_column",
			"unit_price",
			"medical_item_type",
			"other_office_id",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("doctor_user_name"," and (doctor_user_name like ?)");
		searchMap.put("stock_lot_number"," and (stock_lot_number like ?)");
		searchMap.put("patient_user_name"," and (patient_user_name like ?)");
		searchMap.put("drug_information_type"," and (drug_information_type = ?)");
		searchMap.put("transaction_date_start"," and (transaction_date >= ?)");
		searchMap.put("transaction_date_end"," and (transaction_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final DeliverOrReturnDetailsDAO INSTANCE = new DeliverOrReturnDetailsDAO();
	}

	public static DeliverOrReturnDetailsDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(DeliverOrReturnDetailsDTO deliverorreturndetailsDTO)
	{
		deliverorreturndetailsDTO.searchColumn = "";
		deliverorreturndetailsDTO.searchColumn += deliverorreturndetailsDTO.doctorUserName + " ";
		deliverorreturndetailsDTO.searchColumn += deliverorreturndetailsDTO.stockLotNumber + " ";
		deliverorreturndetailsDTO.searchColumn += deliverorreturndetailsDTO.patientUserName + " ";
		deliverorreturndetailsDTO.searchColumn += CommonDAO.getName("English", "drug_information", deliverorreturndetailsDTO.drugInformationType) + " " + CommonDAO.getName("Bangla", "drug_information", deliverorreturndetailsDTO.drugInformationType) + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, DeliverOrReturnDetailsDTO deliverorreturndetailsDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(deliverorreturndetailsDTO);
		if(isInsert)
		{
			ps.setObject(++index,deliverorreturndetailsDTO.iD);
		}
		ps.setObject(++index,deliverorreturndetailsDTO.deliverOrReturnMedicineId);
		ps.setObject(++index,deliverorreturndetailsDTO.isDelivery);
		ps.setObject(++index,deliverorreturndetailsDTO.doctorId);
		ps.setObject(++index,deliverorreturndetailsDTO.doctorUserName);
		ps.setObject(++index,deliverorreturndetailsDTO.stockLotNumber);
		ps.setObject(++index,deliverorreturndetailsDTO.patientId);
		ps.setObject(++index,deliverorreturndetailsDTO.patientUserName);
		ps.setObject(++index,deliverorreturndetailsDTO.drugInformationType);
		ps.setObject(++index,deliverorreturndetailsDTO.medicineGenericNameId);
		ps.setObject(++index,deliverorreturndetailsDTO.quantity);
		ps.setObject(++index,deliverorreturndetailsDTO.transactionDate);
		ps.setObject(++index,deliverorreturndetailsDTO.searchColumn);
		ps.setObject(++index,deliverorreturndetailsDTO.unitPrice);
		ps.setObject(++index,deliverorreturndetailsDTO.medicalItemType);
		ps.setObject(++index,deliverorreturndetailsDTO.otherOfficeId);
		if(isInsert)
		{
			ps.setObject(++index,deliverorreturndetailsDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,deliverorreturndetailsDTO.iD);
		}
	}
	
	@Override
	public DeliverOrReturnDetailsDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			DeliverOrReturnDetailsDTO deliverorreturndetailsDTO = new DeliverOrReturnDetailsDTO();
			int i = 0;
			deliverorreturndetailsDTO.iD = rs.getLong(columnNames[i++]);
			deliverorreturndetailsDTO.deliverOrReturnMedicineId = rs.getLong(columnNames[i++]);
			deliverorreturndetailsDTO.isDelivery = rs.getBoolean(columnNames[i++]);
			deliverorreturndetailsDTO.doctorId = rs.getLong(columnNames[i++]);
			deliverorreturndetailsDTO.doctorUserName = rs.getString(columnNames[i++]);
			deliverorreturndetailsDTO.stockLotNumber = rs.getString(columnNames[i++]);
			deliverorreturndetailsDTO.patientId = rs.getLong(columnNames[i++]);
			deliverorreturndetailsDTO.patientUserName = rs.getString(columnNames[i++]);
			deliverorreturndetailsDTO.drugInformationType = rs.getLong(columnNames[i++]);
			deliverorreturndetailsDTO.medicineGenericNameId = rs.getLong(columnNames[i++]);
			deliverorreturndetailsDTO.quantity = rs.getInt(columnNames[i++]);
			deliverorreturndetailsDTO.transactionDate = rs.getLong(columnNames[i++]);
			deliverorreturndetailsDTO.searchColumn = rs.getString(columnNames[i++]);
			deliverorreturndetailsDTO.unitPrice = rs.getDouble(columnNames[i++]);
			deliverorreturndetailsDTO.medicalItemType = rs.getInt(columnNames[i++]);
			deliverorreturndetailsDTO.otherOfficeId = rs.getLong(columnNames[i++]);
			deliverorreturndetailsDTO.isDeleted = rs.getInt(columnNames[i++]);
			deliverorreturndetailsDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return deliverorreturndetailsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public DeliverOrReturnDetailsDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "deliver_or_return_details";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((DeliverOrReturnDetailsDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((DeliverOrReturnDetailsDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	