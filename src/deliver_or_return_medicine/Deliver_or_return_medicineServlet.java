package deliver_or_return_medicine;


import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;


import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDAO;
import user.UserDTO;
import user.UserRepository;
import util.*;
import javax.servlet.http.*;
import java.util.*;

import language.LC;
import language.LM;
import medical_equipment_name.Medical_equipment_nameDAO;
import medical_equipment_name.Medical_equipment_nameDTO;
import medical_inventory_lot.MedicalInventoryInDAO;
import medical_inventory_lot.MedicalInventoryInDTO;
import medical_inventory_out.Medical_inventory_outDAO;
import medical_inventory_out.Medical_inventory_outDTO;
import common.ApiResponse;
import common.BaseServlet;
import drug_information.Drug_informationDAO;
import drug_information.Drug_informationDTO;

import com.google.gson.Gson;

import borrow_medicine.BorrowMedicineDetailsDTO;
import borrow_medicine.PersonalStockDAO;
import borrow_medicine.PersonalStockDTO;
import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Deliver_or_return_medicineServlet
 */
@WebServlet("/Deliver_or_return_medicineServlet")
@MultipartConfig
public class Deliver_or_return_medicineServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Deliver_or_return_medicineServlet.class);
    Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
    MedicalInventoryInDAO medicalInventoryInDAO = new MedicalInventoryInDAO();
    Medical_inventory_outDAO medical_inventory_outDAO = new Medical_inventory_outDAO();
    Medical_equipment_nameDAO medical_equipment_nameDAO = new Medical_equipment_nameDAO();

    @Override
    public String getTableName() {
        return Deliver_or_return_medicineDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Deliver_or_return_medicineServlet";
    }

    @Override
    public Deliver_or_return_medicineDAO getCommonDAOService() {
        return Deliver_or_return_medicineDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.DELIVER_OR_RETURN_MEDICINE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.DELIVER_OR_RETURN_MEDICINE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.DELIVER_OR_RETURN_MEDICINE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Deliver_or_return_medicineServlet.class;
    }
	DeliverOrReturnDetailsDAO deliverOrReturnDetailsDAO = DeliverOrReturnDetailsDAO.getInstance();
    private final Gson gson = new Gson();
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        init(request);
        String actionType = request.getParameter("actionType");
        logger.debug("medicineServletServlet " + actionType);
		if ("ajax_add".equals(actionType)) {
			if (Utils.checkPermission(commonLoginData.userDTO, getAddPageMenuConstants()) && getAddPermission(request)) {
                try {
                    CommonDTO commonDTO = addT(request, true, commonLoginData.userDTO);
                    finalize(request);
                    long medicalItemType = Long.parseLong(request.getParameter("medicalItemType")) ;
                    if(commonDTO != null)
                    {
                    	 if(medicalItemType == SessionConstants.MEDICAL_ITEM_DRUG)
                         {
                         	ApiResponse.sendSuccessResponse(response, "Transaction_report_Servlet?actionType=reportPage");
                         }
                         else
                         {
                         	ApiResponse.sendSuccessResponse(response, "Equipment_transaction_report_Servlet?actionType=reportPage");
                         }
                    }
                    else
                    {
                    	ApiResponse.sendErrorResponse(response, "Error");
                    }
                   
                    
                } catch (Exception ex) {
                    ex.printStackTrace();
                    logger.error(ex);
                    ApiResponse.sendErrorResponse(response, ex.getMessage());
                }
            } else {
                ApiResponse.sendErrorResponse(response, commonLoginData.isLangEng ? "You do not have permission to do this task" : "আপনার এই ক্রিয়া সম্পাদনের অনুমতি নেই");
            }
		}
	
		else
		{
			super.doPost(request,response);
		}
    }
 	

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		// TODO Auto-generated method stub

		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addDeliver_or_return_medicine");
		String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
		Deliver_or_return_medicineDTO deliver_or_return_medicineDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
		if(addFlag == true)
		{
			deliver_or_return_medicineDTO = new Deliver_or_return_medicineDTO();
		}
		else
		{
			deliver_or_return_medicineDTO = Deliver_or_return_medicineDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		Value = request.getParameter("doctorUserName");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("doctorUserName = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			deliver_or_return_medicineDTO.doctorUserName = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		UserDTO employeeUserDTO  = UserRepository.getUserDTOByUserName(deliver_or_return_medicineDTO.doctorUserName);
	    if(employeeUserDTO != null)
	    {
	    	deliver_or_return_medicineDTO.doctorId = employeeUserDTO.organogramID;
	    }
	    else
	    {
	    	UserDAO userDAO = new UserDAO();
	    	employeeUserDTO = userDAO.getUserDTOByUsernameFromOnlyUsers(deliver_or_return_medicineDTO.doctorUserName);
	    }

		

		Value = request.getParameter("stockLotNumber");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("stockLotNumber = " + Value);
		if(Value != null)
		{
			deliver_or_return_medicineDTO.stockLotNumber = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("isDelivery");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("isDelivery = " + Value);
        deliver_or_return_medicineDTO.isDelivery = Value != null && !Value.equalsIgnoreCase("");

		Value = request.getParameter("patientId");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("patientId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			deliver_or_return_medicineDTO.patientId = Long.parseLong(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		Value = request.getParameter("otherOfficeId");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("otherOfficeId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			deliver_or_return_medicineDTO.otherOfficeId = Long.parseLong(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		if(!deliver_or_return_medicineDTO.isDelivery)
		{
			UserDTO patientDTO = UserRepository.getUserDTOByOrganogramID(deliver_or_return_medicineDTO.patientId);
			if(patientDTO == null)
			{
				throw new Exception(ErrorMessage.getGenericInvalidMessage(language));
			}
			deliver_or_return_medicineDTO.patientUserName = patientDTO.userName;
		}

		Value = request.getParameter("remarks");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("remarks = " + Value);
		if(Value != null)
		{
			deliver_or_return_medicineDTO.remarks = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		Value = request.getParameter("medicalItemType");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("medicalItemType = " + Value);
		if(Value != null)
		{
			deliver_or_return_medicineDTO.medicalItemType = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		if(addFlag)
		{
			deliver_or_return_medicineDTO.insertedByUserId = userDTO.ID;
		}

		if(addFlag)
		{
			deliver_or_return_medicineDTO.insertedByOrganogramId = userDTO.organogramID;
		}


		if(addFlag)
		{				
			deliver_or_return_medicineDTO.insertionDate = TimeConverter.getToday();
		}			


		Value = request.getParameter("name");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("name = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			deliver_or_return_medicineDTO.returnedByName = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		Value = request.getParameter("phone");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("phone = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			deliver_or_return_medicineDTO.returnedByPhone = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		Value = request.getParameter("transactionDate");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("transactionDate = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			try 
			{
				Date d = f.parse(Value);
				deliver_or_return_medicineDTO.transactionDate = d.getTime();
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		Value = request.getParameter("returnedByUserName");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("returnedByUserName = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			deliver_or_return_medicineDTO.returnedByUserName = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		UserDTO borrowedByDTO = UserRepository.getUserDTOByUserName(deliver_or_return_medicineDTO.returnedByUserName);
		if(borrowedByDTO != null)
		{
			deliver_or_return_medicineDTO.returnedByEmployeeId = borrowedByDTO.employee_record_id;
		}
		
		System.out.println("Done adding  addDeliver_or_return_medicine dto = " + deliver_or_return_medicineDTO);
		long returnedID = -1;
		
		List<DeliverOrReturnDetailsDTO> deliverOrReturnDetailsDTOList = createDeliverOrReturnDetailsDTOListByRequest(request, language, deliver_or_return_medicineDTO);
		
		if(addFlag == true)
		{
			returnedID = Deliver_or_return_medicineDAO.getInstance().add(deliver_or_return_medicineDTO);
		}
		else
		{				
			returnedID = Deliver_or_return_medicineDAO.getInstance().update(deliver_or_return_medicineDTO);										
		}
		
	
		if(addFlag == true) //add or validate
		{
			if(deliverOrReturnDetailsDTOList != null)
			{				
				for(DeliverOrReturnDetailsDTO deliverOrReturnDetailsDTO: deliverOrReturnDetailsDTOList)
				{
					deliverOrReturnDetailsDTO.deliverOrReturnMedicineId = deliver_or_return_medicineDTO.iD; 
					deliverOrReturnDetailsDAO.add(deliverOrReturnDetailsDTO);
					
					logger.debug("added deliverOrReturnDetailsDTO, going to add medicalInventoryInDTO " + deliver_or_return_medicineDTO.insertionDate 
							+ " " + deliver_or_return_medicineDTO.transactionDate);
					MedicalInventoryInDTO medicalInventoryInDTO = new MedicalInventoryInDTO(deliverOrReturnDetailsDTO,
							userDTO, employeeUserDTO, deliverOrReturnDetailsDTO.medicalItemType, deliver_or_return_medicineDTO.transactionDate);
					medicalInventoryInDAO.add(medicalInventoryInDTO);
					if(deliverOrReturnDetailsDTO.isDelivery)
					{
						Drug_informationDTO drug_informationDTO = drug_informationDAO.getDTOByID(deliverOrReturnDetailsDTO.drugInformationType);
						UserDTO patientDTO = UserRepository.getUserDTOByOrganogramID(deliver_or_return_medicineDTO.patientId);
						Medical_inventory_outDTO medical_inventory_outDTO = new Medical_inventory_outDTO(drug_informationDTO,
								deliverOrReturnDetailsDTO.quantity, userDTO, MedicalInventoryInDTO.TR_RECEIVE_MEDICINE,
								patientDTO, deliverOrReturnDetailsDTO.medicalItemType,
								patientDTO.fullName, patientDTO.phoneNo, deliver_or_return_medicineDTO.transactionDate, -1, -1);				
						medical_inventory_outDAO.add(medical_inventory_outDTO);
						
						PersonalStockDTO personalStockDTO = PersonalStockDAO.getInstance().getDTOByUserNameAndDrugId(patientDTO.userName,
								deliverOrReturnDetailsDTO.drugInformationType, SessionConstants.MEDICAL_ITEM_DRUG);
						if(personalStockDTO == null)
						{
							
							personalStockDTO = new PersonalStockDTO(deliverOrReturnDetailsDTO, drug_informationDTO);
							PersonalStockDAO.getInstance().add(personalStockDTO);
						}
						else
						{
							personalStockDTO.quantity += deliverOrReturnDetailsDTO.quantity;
							PersonalStockDAO.getInstance().update(personalStockDTO);
						}

					}
					PersonalStockDTO personalStockDTO = PersonalStockDAO.getInstance().getDTOByUserNameAndDrugId(deliverOrReturnDetailsDTO.doctorUserName,
							deliverOrReturnDetailsDTO.drugInformationType, deliverOrReturnDetailsDTO.medicalItemType);
					if(personalStockDTO != null)						
					{
						personalStockDTO.quantity -= deliverOrReturnDetailsDTO.quantity;
						PersonalStockDAO.getInstance().update(personalStockDTO);
					}
				}
			}
		
		}			
			
		return deliver_or_return_medicineDTO;
	}
	private List<DeliverOrReturnDetailsDTO> createDeliverOrReturnDetailsDTOListByRequest(HttpServletRequest request, String language, Deliver_or_return_medicineDTO deliver_or_return_medicineDTO) throws Exception{ 
		List<DeliverOrReturnDetailsDTO> deliverOrReturnDetailsDTOList = new ArrayList<DeliverOrReturnDetailsDTO>();
		if(request.getParameterValues("deliverOrReturnDetails.iD") != null) 
		{
			int deliverOrReturnDetailsItemNo = request.getParameterValues("deliverOrReturnDetails.iD").length;
			
			
			for(int index=0;index<deliverOrReturnDetailsItemNo;index++)
			{
				DeliverOrReturnDetailsDTO deliverOrReturnDetailsDTO = createDeliverOrReturnDetailsDTOByRequestAndIndex(request,true,index, language, deliver_or_return_medicineDTO);
				if(deliverOrReturnDetailsDTO != null)
				{
					for(DeliverOrReturnDetailsDTO existingDTO: deliverOrReturnDetailsDTOList)
					{
						if(existingDTO.drugInformationType == deliverOrReturnDetailsDTO.drugInformationType)
						{
							throw new Exception(LM.getText(LC.BORROW_MEDICINE_ADD_BORROW_MEDICINE_DETAILS_DRUGINFORMATIONTYPE, language) + " " + ErrorMessage.getInvalidMessage(language));
						}
					}				
					deliverOrReturnDetailsDTOList.add(deliverOrReturnDetailsDTO);
				}
			}
			
			return deliverOrReturnDetailsDTOList;
		}
		return null;
	}
	
	private DeliverOrReturnDetailsDTO createDeliverOrReturnDetailsDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, 
			String language, Deliver_or_return_medicineDTO deliver_or_return_medicineDTO) throws Exception{
	
		DeliverOrReturnDetailsDTO deliverOrReturnDetailsDTO = new DeliverOrReturnDetailsDTO(deliver_or_return_medicineDTO);
		
		
		String Value = "";
		
		Value = request.getParameterValues("deliverOrReturnDetails.quantity")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		else
		{
			throw new Exception(LM.getText(LC.DELIVER_OR_RETURN_MEDICINE_ADD_DELIVER_OR_RETURN_DETAILS_QUANTITY, language) + " " + ErrorMessage.getInvalidMessage(language));
		}

		deliverOrReturnDetailsDTO.quantity = Integer.parseInt(Value);
		
		
		if(deliverOrReturnDetailsDTO.quantity <= 0)
		{
			return null;
		}

				
		
		Value = request.getParameterValues("deliverOrReturnDetails.drugInformationType")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		else
		{
			throw new Exception(LM.getText(LC.DELIVER_OR_RETURN_MEDICINE_ADD_DELIVER_OR_RETURN_DETAILS_DRUGINFORMATIONTYPE, language) + " " + ErrorMessage.getInvalidMessage(language));
		}

		deliverOrReturnDetailsDTO.drugInformationType = Long.parseLong(Value);
		deliverOrReturnDetailsDTO.medicalItemType = deliver_or_return_medicineDTO.medicalItemType;
		if(deliverOrReturnDetailsDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_DRUG)
		{
			Drug_informationDTO drug_informationDTO = drug_informationDAO.getDTOByID(deliverOrReturnDetailsDTO.drugInformationType);
			if(drug_informationDTO == null)
			{
				throw new Exception(LM.getText(LC.BORROW_MEDICINE_ADD_BORROW_MEDICINE_DETAILS_DRUGINFORMATIONTYPE, language) + " " + ErrorMessage.getInvalidMessage(language));
	
			}
			deliverOrReturnDetailsDTO.medicineGenericNameId = drug_informationDTO.medicineGenericNameType;
		}
		else if(deliverOrReturnDetailsDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_EQUIPMENT)
		{
			Medical_equipment_nameDTO medical_equipment_nameDTO = medical_equipment_nameDAO.getDTOByID(deliverOrReturnDetailsDTO.drugInformationType);
			if(medical_equipment_nameDTO == null)
			{
				if(medical_equipment_nameDTO == null)
				{
					throw new Exception(ErrorMessage.getGenericInvalidMessage(language));

				}
			}
		}


		PersonalStockDTO personalStockDTO = PersonalStockDAO.getInstance().getDTOByUserNameAndDrugId(deliverOrReturnDetailsDTO.doctorUserName, deliverOrReturnDetailsDTO.drugInformationType, deliverOrReturnDetailsDTO.medicalItemType);
		if(deliverOrReturnDetailsDTO.quantity > personalStockDTO.quantity)
		{
			throw new Exception(ErrorMessage.getGenericInvalidMessage(language));

		}
		return deliverOrReturnDetailsDTO;
	
	}	
}

