package deliver_or_return_medicine;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Deliver_or_return_medicineDAO  implements CommonDAOService<Deliver_or_return_medicineDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Deliver_or_return_medicineDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"doctor_id",
			"doctor_user_name",
			"stock_lot_number",
			"is_delivery",
			"patient_id",
			"patient_user_name",
			"remarks",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"transaction_date",
			"search_column",
			"medical_item_type",
			"returned_by_employee_id",
			"returned_by_user_name",
			"returned_by_name",
			"returned_by_phone",
			"other_office_id",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("doctor_user_name"," and (doctor_user_name like ?)");
		searchMap.put("stock_lot_number"," and (stock_lot_number like ?)");
		searchMap.put("patient_user_name"," and (patient_user_name like ?)");
		searchMap.put("remarks"," and (remarks like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Deliver_or_return_medicineDAO INSTANCE = new Deliver_or_return_medicineDAO();
	}

	public static Deliver_or_return_medicineDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Deliver_or_return_medicineDTO deliver_or_return_medicineDTO)
	{
		deliver_or_return_medicineDTO.searchColumn = "";
		deliver_or_return_medicineDTO.searchColumn += deliver_or_return_medicineDTO.doctorUserName + " ";
		deliver_or_return_medicineDTO.searchColumn += deliver_or_return_medicineDTO.stockLotNumber + " ";
		deliver_or_return_medicineDTO.searchColumn += deliver_or_return_medicineDTO.patientUserName + " ";
		deliver_or_return_medicineDTO.searchColumn += deliver_or_return_medicineDTO.remarks + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Deliver_or_return_medicineDTO deliver_or_return_medicineDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(deliver_or_return_medicineDTO);
		if(isInsert)
		{
			ps.setObject(++index,deliver_or_return_medicineDTO.iD);
		}
		ps.setObject(++index,deliver_or_return_medicineDTO.doctorId);
		ps.setObject(++index,deliver_or_return_medicineDTO.doctorUserName);
		ps.setObject(++index,deliver_or_return_medicineDTO.stockLotNumber);
		ps.setObject(++index,deliver_or_return_medicineDTO.isDelivery);
		ps.setObject(++index,deliver_or_return_medicineDTO.patientId);
		ps.setObject(++index,deliver_or_return_medicineDTO.patientUserName);
		ps.setObject(++index,deliver_or_return_medicineDTO.remarks);
		ps.setObject(++index,deliver_or_return_medicineDTO.insertedByUserId);
		ps.setObject(++index,deliver_or_return_medicineDTO.insertedByOrganogramId);
		ps.setObject(++index,deliver_or_return_medicineDTO.insertionDate);
		ps.setObject(++index,deliver_or_return_medicineDTO.transactionDate);
		ps.setObject(++index,deliver_or_return_medicineDTO.searchColumn);
		ps.setObject(++index,deliver_or_return_medicineDTO.medicalItemType);
		ps.setObject(++index,deliver_or_return_medicineDTO.returnedByEmployeeId);
		ps.setObject(++index,deliver_or_return_medicineDTO.returnedByUserName);
		ps.setObject(++index,deliver_or_return_medicineDTO.returnedByName);
		ps.setObject(++index,deliver_or_return_medicineDTO.returnedByPhone);
		ps.setObject(++index,deliver_or_return_medicineDTO.otherOfficeId);
		if(isInsert)
		{
			ps.setObject(++index,deliver_or_return_medicineDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,deliver_or_return_medicineDTO.iD);
		}
	}
	
	@Override
	public Deliver_or_return_medicineDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Deliver_or_return_medicineDTO deliver_or_return_medicineDTO = new Deliver_or_return_medicineDTO();
			int i = 0;
			deliver_or_return_medicineDTO.iD = rs.getLong(columnNames[i++]);
			deliver_or_return_medicineDTO.doctorId = rs.getLong(columnNames[i++]);
			deliver_or_return_medicineDTO.doctorUserName = rs.getString(columnNames[i++]);
			deliver_or_return_medicineDTO.stockLotNumber = rs.getString(columnNames[i++]);
			deliver_or_return_medicineDTO.isDelivery = rs.getBoolean(columnNames[i++]);
			deliver_or_return_medicineDTO.patientId = rs.getLong(columnNames[i++]);
			deliver_or_return_medicineDTO.patientUserName = rs.getString(columnNames[i++]);
			deliver_or_return_medicineDTO.remarks = rs.getString(columnNames[i++]);
			deliver_or_return_medicineDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			deliver_or_return_medicineDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			deliver_or_return_medicineDTO.insertionDate = rs.getLong(columnNames[i++]);
			deliver_or_return_medicineDTO.transactionDate = rs.getLong(columnNames[i++]);
			deliver_or_return_medicineDTO.searchColumn = rs.getString(columnNames[i++]);
			deliver_or_return_medicineDTO.medicalItemType = rs.getInt(columnNames[i++]);			
			
			deliver_or_return_medicineDTO.returnedByEmployeeId = rs.getLong(columnNames[i++]);
			deliver_or_return_medicineDTO.returnedByUserName = rs.getString(columnNames[i++]);
			deliver_or_return_medicineDTO.returnedByName = rs.getString(columnNames[i++]);
			deliver_or_return_medicineDTO.returnedByPhone = rs.getString(columnNames[i++]);
			
			deliver_or_return_medicineDTO.otherOfficeId = rs.getLong(columnNames[i++]);
			
			deliver_or_return_medicineDTO.isDeleted = rs.getInt(columnNames[i++]);
			deliver_or_return_medicineDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return deliver_or_return_medicineDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Deliver_or_return_medicineDTO getDTOByID (long id)
	{
		Deliver_or_return_medicineDTO deliver_or_return_medicineDTO = null;
		try 
		{
			deliver_or_return_medicineDTO = getDTOFromID(id);
			if(deliver_or_return_medicineDTO != null)
			{
				DeliverOrReturnDetailsDAO deliverOrReturnDetailsDAO = DeliverOrReturnDetailsDAO.getInstance();				
				List<DeliverOrReturnDetailsDTO> deliverOrReturnDetailsDTOList = (List<DeliverOrReturnDetailsDTO>)deliverOrReturnDetailsDAO.getDTOsByParent("deliver_or_return_medicine_id", deliver_or_return_medicineDTO.iD);
				deliver_or_return_medicineDTO.deliverOrReturnDetailsDTOList = deliverOrReturnDetailsDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return deliver_or_return_medicineDTO;
	}

	@Override
	public String getTableName() {
		return "deliver_or_return_medicine";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Deliver_or_return_medicineDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Deliver_or_return_medicineDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	