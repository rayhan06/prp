package deliver_or_return_medicine;
import java.util.*;

import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*; 


public class Deliver_or_return_medicineDTO extends CommonDTO
{

	public long doctorId = -1;
    public String doctorUserName = "";
    public String stockLotNumber = "";
	public boolean isDelivery = false;
	public long patientId = -1;
    public String patientUserName = "";

	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	public long transactionDate = TimeConverter.getToday();
	
	public String returnedByUserName = "";
	public long returnedByEmployeeId = -1;
	public String returnedByName = "";
	public String returnedByPhone = "";
	
	public long otherOfficeId = -1;
		
	
	public int medicalItemType = SessionConstants.MEDICAL_ITEM_DRUG;
	
	public List<DeliverOrReturnDetailsDTO> deliverOrReturnDetailsDTOList = new ArrayList<>();
	
	public Deliver_or_return_medicineDTO()
	{
		
	}
	
	public Deliver_or_return_medicineDTO(UserDTO drDTO, UserDTO patientDTO)
	{
		doctorId = drDTO.organogramID;
		doctorUserName= drDTO.userName;
		isDelivery= true;
		patientId = patientDTO.organogramID;
		patientUserName= patientDTO.userName;
		
		insertedByUserId= drDTO.ID;
		insertedByOrganogramId= drDTO.organogramID;
		insertionDate = TimeConverter.getToday();
	}
	
    @Override
	public String toString() {
            return "$Deliver_or_return_medicineDTO[" +
            " iD = " + iD +
            " doctorId = " + doctorId +
            " doctorUserName = " + doctorUserName +
            " stockLotNumber = " + stockLotNumber +
            " isDelivery = " + isDelivery +
            " patientId = " + patientId +
            " patientUserName = " + patientUserName +
            " remarks = " + remarks +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}