package deliver_or_return_medicine;
import java.util.*; 
import util.*;


public class DeliverOrReturnDetailsMAPS extends CommonMaps
{	
	public DeliverOrReturnDetailsMAPS(String tableName)
	{
		


		java_SQL_map.put("deliver_or_return_medicine_id".toLowerCase(), "deliverOrReturnMedicineId".toLowerCase());
		java_SQL_map.put("is_delivery".toLowerCase(), "isDelivery".toLowerCase());
		java_SQL_map.put("doctor_id".toLowerCase(), "doctorId".toLowerCase());
		java_SQL_map.put("doctor_user_name".toLowerCase(), "doctorUserName".toLowerCase());
		java_SQL_map.put("stock_lot_number".toLowerCase(), "stockLotNumber".toLowerCase());
		java_SQL_map.put("patient_id".toLowerCase(), "patientId".toLowerCase());
		java_SQL_map.put("patient_user_name".toLowerCase(), "patientUserName".toLowerCase());
		java_SQL_map.put("drug_information_type".toLowerCase(), "drugInformationType".toLowerCase());
		java_SQL_map.put("medicine_generic_name_id".toLowerCase(), "medicineGenericNameId".toLowerCase());
		java_SQL_map.put("quantity".toLowerCase(), "quantity".toLowerCase());
		java_SQL_map.put("transaction_date".toLowerCase(), "transactionDate".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Deliver Or Return Medicine Id".toLowerCase(), "deliverOrReturnMedicineId".toLowerCase());
		java_Text_map.put("Is Delivery".toLowerCase(), "isDelivery".toLowerCase());
		java_Text_map.put("Doctor Id".toLowerCase(), "doctorId".toLowerCase());
		java_Text_map.put("Doctor User Name".toLowerCase(), "doctorUserName".toLowerCase());
		java_Text_map.put("Stock Lot Number".toLowerCase(), "stockLotNumber".toLowerCase());
		java_Text_map.put("Patient Id".toLowerCase(), "patientId".toLowerCase());
		java_Text_map.put("Patient User Name".toLowerCase(), "patientUserName".toLowerCase());
		java_Text_map.put("Drug Information".toLowerCase(), "drugInformationType".toLowerCase());
		java_Text_map.put("Medicine Generic Name Id".toLowerCase(), "medicineGenericNameId".toLowerCase());
		java_Text_map.put("Quantity".toLowerCase(), "quantity".toLowerCase());
		java_Text_map.put("Transaction Date".toLowerCase(), "transactionDate".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}