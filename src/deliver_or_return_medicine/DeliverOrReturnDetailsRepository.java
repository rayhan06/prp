package deliver_or_return_medicine;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class DeliverOrReturnDetailsRepository implements Repository {
	DeliverOrReturnDetailsDAO deliverorreturndetailsDAO = null;
	
	static Logger logger = Logger.getLogger(DeliverOrReturnDetailsRepository.class);
	Map<Long, DeliverOrReturnDetailsDTO>mapOfDeliverOrReturnDetailsDTOToiD;
	Map<Long, Set<DeliverOrReturnDetailsDTO> >mapOfDeliverOrReturnDetailsDTOTodeliverOrReturnMedicineId;
	Gson gson;

  
	private DeliverOrReturnDetailsRepository(){
		deliverorreturndetailsDAO = DeliverOrReturnDetailsDAO.getInstance();
		mapOfDeliverOrReturnDetailsDTOToiD = new ConcurrentHashMap<>();
		mapOfDeliverOrReturnDetailsDTOTodeliverOrReturnMedicineId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static DeliverOrReturnDetailsRepository INSTANCE = new DeliverOrReturnDetailsRepository();
    }

    public static DeliverOrReturnDetailsRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<DeliverOrReturnDetailsDTO> deliverorreturndetailsDTOs = deliverorreturndetailsDAO.getAllDTOs(reloadAll);
			for(DeliverOrReturnDetailsDTO deliverorreturndetailsDTO : deliverorreturndetailsDTOs) {
				DeliverOrReturnDetailsDTO oldDeliverOrReturnDetailsDTO = getDeliverOrReturnDetailsDTOByiD(deliverorreturndetailsDTO.iD);
				if( oldDeliverOrReturnDetailsDTO != null ) {
					mapOfDeliverOrReturnDetailsDTOToiD.remove(oldDeliverOrReturnDetailsDTO.iD);
				
					if(mapOfDeliverOrReturnDetailsDTOTodeliverOrReturnMedicineId.containsKey(oldDeliverOrReturnDetailsDTO.deliverOrReturnMedicineId)) {
						mapOfDeliverOrReturnDetailsDTOTodeliverOrReturnMedicineId.get(oldDeliverOrReturnDetailsDTO.deliverOrReturnMedicineId).remove(oldDeliverOrReturnDetailsDTO);
					}
					if(mapOfDeliverOrReturnDetailsDTOTodeliverOrReturnMedicineId.get(oldDeliverOrReturnDetailsDTO.deliverOrReturnMedicineId).isEmpty()) {
						mapOfDeliverOrReturnDetailsDTOTodeliverOrReturnMedicineId.remove(oldDeliverOrReturnDetailsDTO.deliverOrReturnMedicineId);
					}
					
					
				}
				if(deliverorreturndetailsDTO.isDeleted == 0) 
				{
					
					mapOfDeliverOrReturnDetailsDTOToiD.put(deliverorreturndetailsDTO.iD, deliverorreturndetailsDTO);
				
					if( ! mapOfDeliverOrReturnDetailsDTOTodeliverOrReturnMedicineId.containsKey(deliverorreturndetailsDTO.deliverOrReturnMedicineId)) {
						mapOfDeliverOrReturnDetailsDTOTodeliverOrReturnMedicineId.put(deliverorreturndetailsDTO.deliverOrReturnMedicineId, new HashSet<>());
					}
					mapOfDeliverOrReturnDetailsDTOTodeliverOrReturnMedicineId.get(deliverorreturndetailsDTO.deliverOrReturnMedicineId).add(deliverorreturndetailsDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public DeliverOrReturnDetailsDTO clone(DeliverOrReturnDetailsDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, DeliverOrReturnDetailsDTO.class);
	}
	
	
	public List<DeliverOrReturnDetailsDTO> getDeliverOrReturnDetailsList() {
		List <DeliverOrReturnDetailsDTO> deliverorreturndetailss = new ArrayList<DeliverOrReturnDetailsDTO>(this.mapOfDeliverOrReturnDetailsDTOToiD.values());
		return deliverorreturndetailss;
	}
	
	public List<DeliverOrReturnDetailsDTO> copyDeliverOrReturnDetailsList() {
		List <DeliverOrReturnDetailsDTO> deliverorreturndetailss = getDeliverOrReturnDetailsList();
		return deliverorreturndetailss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public DeliverOrReturnDetailsDTO getDeliverOrReturnDetailsDTOByiD( long iD){
		return mapOfDeliverOrReturnDetailsDTOToiD.get(iD);
	}
	
	public DeliverOrReturnDetailsDTO copyDeliverOrReturnDetailsDTOByiD( long iD){
		return clone(mapOfDeliverOrReturnDetailsDTOToiD.get(iD));
	}
	
	
	public List<DeliverOrReturnDetailsDTO> getDeliverOrReturnDetailsDTOBydeliverOrReturnMedicineId(long deliverOrReturnMedicineId) {
		return new ArrayList<>( mapOfDeliverOrReturnDetailsDTOTodeliverOrReturnMedicineId.getOrDefault(deliverOrReturnMedicineId,new HashSet<>()));
	}
	
	public List<DeliverOrReturnDetailsDTO> copyDeliverOrReturnDetailsDTOBydeliverOrReturnMedicineId(long deliverOrReturnMedicineId)
	{
		List <DeliverOrReturnDetailsDTO> deliverorreturndetailss = getDeliverOrReturnDetailsDTOBydeliverOrReturnMedicineId(deliverOrReturnMedicineId);
		return deliverorreturndetailss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return deliverorreturndetailsDAO.getTableName();
	}
}


