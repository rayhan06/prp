package deliver_or_return_medicine;

import drug_information.Drug_informationDTO;
import sessionmanager.SessionConstants;
import util.*; 


public class DeliverOrReturnDetailsDTO extends CommonDTO
{

	public long deliverOrReturnMedicineId = -1;
	public boolean isDelivery = false;
	public long doctorId = -1;
    public String doctorUserName = "";
    public String stockLotNumber = "";
	public long patientId = -1;
    public String patientUserName = "";
	public long drugInformationType = -1;
	public long medicineGenericNameId = -1;
	public int quantity = -1;
	public long transactionDate = TimeConverter.getToday();
	public double unitPrice = 0;
	public int medicalItemType = SessionConstants.MEDICAL_ITEM_DRUG;
	public long otherOfficeId = -1;
	
	public DeliverOrReturnDetailsDTO() {}
	
	public DeliverOrReturnDetailsDTO (Deliver_or_return_medicineDTO deliver_or_return_medicineDTO)
	{
		deliverOrReturnMedicineId = deliver_or_return_medicineDTO.iD;
		isDelivery = deliver_or_return_medicineDTO.isDelivery;
		doctorUserName = deliver_or_return_medicineDTO.doctorUserName;
		doctorId = deliver_or_return_medicineDTO.doctorId;
		patientId = deliver_or_return_medicineDTO.patientId;
		patientUserName = deliver_or_return_medicineDTO.patientUserName;
		transactionDate= deliver_or_return_medicineDTO.transactionDate;
		otherOfficeId = deliver_or_return_medicineDTO.otherOfficeId;
	}
	
	public DeliverOrReturnDetailsDTO (Deliver_or_return_medicineDTO deliver_or_return_medicineDTO, Drug_informationDTO drug_informationDTO, int quantity)
	{
		this(deliver_or_return_medicineDTO);
		this.quantity = quantity;
		this.drugInformationType = drug_informationDTO.iD;
		this.medicineGenericNameId = drug_informationDTO.medicineGenericNameType;
		this.unitPrice = drug_informationDTO.unitPrice;
		
	}
	
    @Override
	public String toString() {
            return "$DeliverOrReturnDetailsDTO[" +
            " iD = " + iD +
            " deliverOrReturnMedicineId = " + deliverOrReturnMedicineId +
            " isDelivery = " + isDelivery +
            " doctorId = " + doctorId +
            " doctorUserName = " + doctorUserName +
            " stockLotNumber = " + stockLotNumber +
            " patientId = " + patientId +
            " patientUserName = " + patientUserName +
            " drugInformationType = " + drugInformationType +
            " medicineGenericNameId = " + medicineGenericNameId +
            " quantity = " + quantity +
            " transactionDate = " + transactionDate +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}