package util;

import approval_execution_table.Approval_execution_tableDAO;
import approval_execution_table.Approval_execution_tableDTO;
import common.ApiResponse;
import files.FilesDAO;
import files.FilesDTO;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import pb.PBNameRepository;
import pb.Utils;
import pbReport.ReportTemplate;
import pb_notifications.Pb_notificationsDAO;
import sessionmanager.SessionConstants;
import user.UserDTO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;


public class CommonRequestHandler {
    private static final Logger logger = Logger.getLogger(CommonRequestHandler.class);
    NavigationService4 navigationService4;


    public CommonRequestHandler(NavigationService4 navigationService4) {
        this.navigationService4 = navigationService4;
    }
    
    public CommonRequestHandler() {
        this.navigationService4 = null;
    }
    
    public Hashtable fillHashTable(Hashtable hashTable, HttpServletRequest request) {
        Enumeration paramList = request.getParameterNames();
        hashTable = new Hashtable();
        do {
            if (!paramList.hasMoreElements()) {
                break;
            }
            String paramName = (String) paramList.nextElement();
            if (!paramName.equalsIgnoreCase("RECORDS_PER_PAGE") && !paramName.equalsIgnoreCase("search") && !paramName.equalsIgnoreCase(SessionConstants.HTML_SEARCH_CHECK_FIELD)) {
                String paramValue = request.getParameter(paramName);

                if(Utils.isValidSearchString(paramValue))
                {
                    hashTable.put(paramName, paramValue);
                }
            }
        } while (true);
        return hashTable;
    }
    
    public XSSFCellStyle getHeaderStyle(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.LEFT);
        cellStyle.setBorderTop(BorderStyle.MEDIUM);
        cellStyle.setBorderBottom(BorderStyle.MEDIUM);
        cellStyle.setBorderLeft(BorderStyle.MEDIUM);
        cellStyle.setBorderRight(BorderStyle.MEDIUM);
        //cellStyle.setFillForegroundColor(IndexedColors.CORNFLOWER_BLUE.getIndex());
        //cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        XSSFFont font = wb.createFont();
        font.setFontHeightInPoints((short) 12);
        font.setBold(true);
        cellStyle.setFont(font);
        return cellStyle;
    }
    
    public XSSFCellStyle getTitleStyle(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        //cellStyle.setFillForegroundColor(IndexedColors.CORNFLOWER_BLUE.getIndex());
        //cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        XSSFFont font = wb.createFont();
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);
        cellStyle.setFont(font);
        return cellStyle;
    }
    
    public void autoSizeColumns(XSSFWorkbook workbook) {
        int numberOfSheets = workbook.getNumberOfSheets();
        for (int i = 0; i < numberOfSheets; i++) {
            Sheet sheet = workbook.getSheetAt(i);
            if (sheet.getPhysicalNumberOfRows() > 0) {
                Row row = sheet.getRow(sheet.getFirstRowNum());
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    int columnIndex = cell.getColumnIndex();
                    sheet.autoSizeColumn(columnIndex);
                }
            }
        }
    }


    public void approve(HttpServletRequest request, HttpServletResponse response, boolean b, UserDTO userDTO, boolean approveOrReject) {
        try {
            long id = Long.parseLong(request.getParameter("idToApprove"));
            int rejectTo = Integer.parseInt(request.getParameter("rejectTo"));
            CommonDTO commonDTO = navigationService4.getDTOByID(id);
            commonDTO.remarks = request.getParameter("remarks");
            commonDTO.fileID = Long.parseLong(request.getParameter("fileID"));
            commonDTO.rejectTo = rejectTo;
            if (approveOrReject) {
                navigationService4.manageWriteOperations(commonDTO, SessionConstants.APPROVE, id, userDTO);
            } else {
                navigationService4.manageWriteOperations(commonDTO, SessionConstants.REJECT, id, userDTO);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
        delete(request, response, userDTO, false);
    }

    public void delete(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, boolean reloadNameRepo) {
        try {
            String[] IDsToDelete = request.getParameterValues("ID");
            for (int i = 0; i < IDsToDelete.length; i++) {
                long id = Long.parseLong(IDsToDelete[i]);
                System.out.println("------ DELETING " + IDsToDelete[i]);
                CommonDTO commonDTO = navigationService4.getDTOByID(id);
                if (commonDTO.jobCat == -1) {
                    navigationService4.manageWriteOperations(commonDTO, SessionConstants.DELETE, id, userDTO);
                }
            }
            if (reloadNameRepo) {
                PBNameRepository.getInstance().reload(false);
            }
            response.sendRedirect(navigationService4.servletName + "?actionType=search");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void terminate(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
        try {
            long id = Long.parseLong(request.getParameter("idToApprove"));
            CommonDTO commonDTO = navigationService4.getDTOByID(id);
            commonDTO.remarks = request.getParameter("remarks");
            commonDTO.fileID = Long.parseLong(request.getParameter("fileID"));

            System.out.println("terminate " + navigationService4.tableName);

            navigationService4.manageWriteOperations(commonDTO, SessionConstants.TERMINATE, id, userDTO);


            boolean redirect = Boolean.parseBoolean(request.getParameter("redirect"));
            System.out.println("redirect = " + redirect);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void skipStep(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
        try {
            long id = Long.parseLong(request.getParameter("id"));
            CommonDTO commonDTO = navigationService4.getDTOByID(id);
            commonDTO.remarks = request.getParameter("remarks");

            navigationService4.manageWriteOperations(commonDTO, SessionConstants.SKIP_STEP, id, userDTO);


            String redirect = "Approval_execution_tableServlet?actionType=search&navigationService4.tableName=" + navigationService4.tableName + "&previousRowId=" + id;
            System.out.println("redirect = " + redirect);

            response.sendRedirect(redirect);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void validate(CommonDTO commonDTO, HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
        try {

            commonDTO.remarks = request.getParameter("remarks");
            commonDTO.fileID = Long.parseLong(request.getParameter("approval_attached_fileDropzone"));


            long updatedRowID = Long.parseLong(request.getParameter("iD"));

            System.out.println("validate " + navigationService4.tableName + " updatedRowID = " + updatedRowID);

            navigationService4.manageWriteOperations(commonDTO, SessionConstants.VALIDATE, updatedRowID, userDTO);


            String redirect = navigationService4.servletName + "?actionType=getApprovalPage";
            System.out.println("redirect = " + redirect);

            ApiResponse.sendSuccessResponse(response, redirect);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void UploadFilesFromDropZone(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
        long ColumnID = Long.parseLong(request.getParameter("ColumnID"));
        String pageType = request.getParameter("pageType");

        System.out.println("In " + pageType);
        Utils.UploadFilesFromDropZone(request, response, userDTO.ID, ColumnID, navigationService4.servletName);
    }

    public void downloadDropzoneFile(HttpServletRequest request, HttpServletResponse response, FilesDAO filesDAO) {
        long id = Long.parseLong(request.getParameter("id"));

        FilesDTO filesDTO;
        try {
            filesDTO = (FilesDTO) filesDAO.getDTOByID(id);
            Utils.ProcessFile(request, response, filesDTO.fileTitle, filesDTO.inputStream);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void deleteFileFromDropZone(HttpServletRequest request, HttpServletResponse response, FilesDAO filesDAO) {
        long id = Long.parseLong(request.getParameter("id"));
        System.out.println("In delete file");
        try {
            filesDAO.hardDeleteByID(id);
            response.getWriter().write("Deleted");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void viewApprovalNotification(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long id = Long.parseLong(request.getParameter("updatedRowID"));
        Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
        Approval_execution_tableDTO approval_execution_tableDTO = (Approval_execution_tableDTO) approval_execution_tableDAO.getMostRecentDTOByUpdatedRowsPreviousRowId(navigationService4.tableName, id);
        long recentID = approval_execution_tableDTO.updatedRowId;

        System.out.println("recent id = " + recentID);

        long notificationToDelete = Long.parseLong(request.getParameter("notificationToDelete"));
        if (notificationToDelete != -1) {
            Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();
            try {
                pb_notificationsDAO.deletePb_notificationsByID(notificationToDelete);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        RequestDispatcher rd = request.getRequestDispatcher(navigationService4.tableName + "/" + navigationService4.tableName + "ApprovalView.jsp?ID=" + recentID);

        rd.forward(request, response);

    }

    public void view(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String ID = request.getParameter("ID");
        String modal = request.getParameter("modal");
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        RequestDispatcher rd;
        System.out.println("View requested, tableName = " + navigationService4.tableName);
        if (isPermanentTable) {
            if (modal != null && modal.equalsIgnoreCase("1")) {
                rd = request.getRequestDispatcher(navigationService4.tableName + "/" + navigationService4.tableName + "ViewModal.jsp?ID=" + ID);
            } else {
                rd = request.getRequestDispatcher(navigationService4.tableName + "/" + navigationService4.tableName + "View.jsp?ID=" + ID);
            }
        } else {
            if (modal != null && modal.equalsIgnoreCase("1")) {
                rd = request.getRequestDispatcher(navigationService4.tableName + "/" + navigationService4.tableName + "ApprovalModal.jsp?ID=" + ID);
            } else {
                rd = request.getRequestDispatcher(navigationService4.tableName + "/" + navigationService4.tableName + "ApprovalView.jsp?ID=" + ID);
            }
        }
        rd.forward(request, response);

    }

    public void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String empId = request.getParameter("empId");
        if (empId == null) {
            UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
            empId = String.valueOf(userDTO.employee_record_id);
        }
        request.setAttribute("empId", empId);
        request.setAttribute("ID", -1L);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher(navigationService4.tableName + "/" + navigationService4.tableName + "Edit.jsp");
        requestDispatcher.forward(request, response);
    }

    public String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        System.out.println("Part Header = {0}" + partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    public void sendToApprovalPath(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
        sendToApprovalPath(request, response, userDTO, "");
    }

    public void sendToApprovalPath(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, String subject) {
        long id = Long.parseLong(request.getParameter("idToApprove"));
        int jobCat = Integer.parseInt(request.getParameter("jobCat"));
        System.out.println("sending to approval path");
        try {
            CommonDTO commonDTO = navigationService4.getDTOByID(id);
            if (request.getParameter("remarks") != null) {
                logger.debug(request.getParameter("remarks"));
                logger.debug(commonDTO);
                commonDTO.remarks = request.getParameter("remarks");
            }
            if (request.getParameter("fileID") != null) {
                commonDTO.fileID = Long.parseLong(request.getParameter("fileID"));
            }
            commonDTO.subject = subject;
            commonDTO.jobCat = jobCat;
            navigationService4.update(commonDTO);
            navigationService4.manageWriteOperations(commonDTO, SessionConstants.INITIATE, commonDTO.iD, userDTO);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void geUploadPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("Getting upload page");
        request.setAttribute("ID", -1L);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher(navigationService4.tableName + "/" + navigationService4.tableName + "Upload.jsp");
        requestDispatcher.forward(request, response);
    }

    public void ajaxDelete(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
        try {
            String[] IDsToDelete = request.getParameterValues("ID");
            for (int i = 0; i < IDsToDelete.length; i++) {
                long id = Long.parseLong(IDsToDelete[i]);
                System.out.println("------ DELETING " + IDsToDelete[i]);


                CommonDTO commonDTO = navigationService4.getDTOByID(id);
                navigationService4.manageWriteOperations(commonDTO, SessionConstants.DELETE, id, userDTO);

            }
            //response.sendRedirect(navigationService4.servletName + "?actionType=search");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public ReportTemplate createReportTemplate(HttpServletRequest request) {

        String[] orderByColumns = request.getParameterValues("orderByColumns");
        List<String> displayColumns = new ArrayList<>();
        List<String> criteriaColumns = new ArrayList<>();

        for (String parameterName : request.getParameterMap().keySet()) {
            if (parameterName.startsWith("display")) {
                displayColumns.add(parameterName + "=" + request.getParameter(parameterName));
            }
            if (parameterName.startsWith("criteria")) {
                criteriaColumns.add(parameterName);
            }
        }
        String orderByColumnString = "";
        String displayColumnString = "";
        String criteriaColumnString = "";
        if (orderByColumns != null) {
            for (int i = 0; i < orderByColumns.length; i++) {
                if (i != 0) {
                    orderByColumnString += ",";
                }
                String orderByColumn = orderByColumns[i];
                orderByColumnString += orderByColumn;
            }
        }
        for (int i = 0; i < displayColumns.size(); i++) {
            if (i != 0) {
                displayColumnString += ",";
            }
            String displayColumn = displayColumns.get(i);
            displayColumnString += displayColumn;
        }
        for (int i = 0; i < criteriaColumns.size(); i++) {
            if (i != 0) {
                criteriaColumnString += ",";
            }
            String criteraiColumn = criteriaColumns.get(i);
            criteriaColumnString += criteraiColumn;
        }

        ReportTemplate reportTemplate = new ReportTemplate();

        reportTemplate.reportDisplay = displayColumnString;
        reportTemplate.reportCriteria = criteriaColumnString;
        reportTemplate.reportOrder = orderByColumnString;

        return reportTemplate;

    }


}
