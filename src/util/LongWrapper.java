package util;

@SuppressWarnings("unused")
public class LongWrapper {
    private long value;
    private final long initValue;

    public LongWrapper(long value) {
        this.value = value;
        this.initValue = value;
    }

    public LongWrapper() {
        this.value = 0;
        this.initValue = 0;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public boolean isValueModified(){
        return this.value != this.initValue;
    }

    public long incrementAndGet(){
        value++;
        return value;
    }
}
