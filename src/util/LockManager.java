package util;

/*
 * @author Md. Erfan Hossain
 * @created 16/06/2021 - 9:08 PM
 * @project parliament
 */

import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

public class LockManager {
    private static final Logger logger = Logger.getLogger(LockManager.class);
    private static final Map<String, Object> locks = new ConcurrentHashMap<>();
    private static final Map<String, Long> timeOfLock = new ConcurrentHashMap<>();
    private static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private static final ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
    private static final ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();

    static {
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor(r -> {
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            return thread;
        });
        executorService.scheduleAtFixedRate(() -> {
            writeLock.lock();
            logger.debug("<<<<Scheduler is triggered>>>>");
            long currentTime = System.currentTimeMillis();
            List<String> timeoutKeys = timeOfLock.keySet().parallelStream()
                    .filter(aLong -> currentTime - timeOfLock.get(aLong) > 900000) //15 minutes
                    .collect(Collectors.toList());
            logger.debug("timeoutKeys size : " + timeoutKeys.size());
            if (timeoutKeys.size() > 0) {
                timeoutKeys.parallelStream()
                        .forEach(k -> {
                            timeOfLock.remove(k);
                            locks.remove(k);
                        });
            }
            logger.debug("<<<<End of scheduler is triggered>>>>");
            writeLock.unlock();
        }, 15, 30, TimeUnit.MINUTES);
    }

    public static Object getLock(String key) {
        readLock.lock();
        locks.putIfAbsent(key, new Object());
        timeOfLock.put(key, System.currentTimeMillis());
        readLock.unlock();
        return locks.get(key);
    }

    public static void updateTime(String key) {
        timeOfLock.put(key, System.currentTimeMillis());
    }
}