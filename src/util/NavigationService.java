package util;

import common.VbSequencerService;
import login.LoginDTO;

import java.util.Collection;
import java.util.Hashtable;

public interface NavigationService extends VbSequencerService
{
	Collection getIDs(LoginDTO loginDTO) throws Exception;
	Collection getIDsWithSearchCriteria(Hashtable searchCriteria, LoginDTO loginDTO) throws Exception;
	Collection getDTOs(Collection recordIDs, LoginDTO loginDTO) throws Exception;
}