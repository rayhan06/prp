package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


import annotation.Map;
import approval_module_map.Approval_module_mapDTO;

import common.ConnectionAndStatementUtil;
import common.VbSequencerService;
import dbm.*;
import pb.*;
import sessionmanager.SessionConstants;
import user.UserDTO;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;


public abstract class NavigationService3 implements VbSequencerService {

    public static final int GETIDS = 1;
    public static final int GETDTOS = 2;
    public static final int GETCOUNT = 3;

    public String tableName = "";
    public String tempTableName = "";
    Approval_module_mapDTO approval_module_mapDTO;

    public NavigationService3(String tableName, String tempTableName, Approval_module_mapDTO approval_module_mapDTO) {
        this.tableName = tableName;
        this.tempTableName = tempTableName;
        this.approval_module_mapDTO = approval_module_mapDTO;
    }


    public void printSql(String sql) {
        System.out.println("sql: " + sql);
    }

    public void hardDeleteByID(long ID) throws Exception {
        Connection connection = null;
        Statement stmt = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            String sql = "delete from " + tableName;

            sql += " WHERE ID = " + ID;

            printSql(sql);

            connection = DBMW.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.execute(sql);


            recordUpdateTime(connection, lastModificationTime, tableName);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
    }

    private static final String deleteByIdListSQL = "UPDATE %s SET isDeleted=1,lastModificationTime = %d WHERE ID in (%s)";

    public void deleteByIdList(List<Long> ls, String tableName, long lastModificationTime, Connection connection) throws Exception {
        if (ls == null || ls.size() == 0) {
            return;
        }
        ls = ls.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        if (ls.size() == 0) {
            return;
        }

        String ids = ls.stream()
                .map(String::valueOf)
                .collect(Collectors.joining(","));

        String sql = String.format(deleteByIdListSQL, tableName, lastModificationTime, ids);

        Utils.wrapWithAtomicReference(ar ->
                ConnectionAndStatementUtil.getWriteStatement(st -> {
                    try {
                        st.executeUpdate(sql);
                    } catch (SQLException e) {
                        e.printStackTrace();
                        ar.set(e);
                    }
                }, connection)
        );
    }

    public void deleteByID(long ID, String tableName) throws Exception {
        Connection connection = null;
        Statement stmt = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            String sql = "UPDATE " + tableName;

            sql += " SET isDeleted=1,lastModificationTime=" + lastModificationTime + " WHERE ID = " + ID;

            printSql(sql);

            connection = DBMW.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.execute(sql);


            recordUpdateTime(connection, lastModificationTime, tableName);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
    }


    public void recordUpdateTime(Connection connection, long lastModificationTime, String tableName) throws SQLException {
        recordUpdateTime(connection, tableName, lastModificationTime);
    }

    public int getCount(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        int count = 0;

        try {

            String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETCOUNT, tableName, isPermanentTable, userApprovalPathType);

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                count = rs.getInt("countID");

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return count;

    }

    public long getLastModificationTime(String tableName) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        long lastModificationTime = 0;

        try {

            String sql = "select table_LastModificationTime from vbSequencer where table_name = '" + tableName + "'";

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                lastModificationTime = rs.getLong("table_LastModificationTime");

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return lastModificationTime;

    }

    public void incrementApprovalOrder(String tableName, long id) {
        Connection connection = null;
        PreparedStatement ps = null;
        long lastModificationTime = System.currentTimeMillis();
        try {

            String sql = "update " + tableName + " set approval_order = approval_order + 1 where id = " + id;

            connection = DBMW.getInstance().getConnection();
            ps = connection.prepareStatement(sql);

            ps.executeUpdate();

            recordUpdateTime(connection, lastModificationTime, tableName);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
    }

    public TempTableDTO getTempTableDTOFromTableById(String tableName, long id) {
        TempTableDTO tempTableDTO = new TempTableDTO();
        Connection connection = null;
        PreparedStatement ps = null;

        try {

            String sql = "select permanent_table_id, operation_type, approval_path_type, approval_order from " + tableName + "  where id = " + id;

            connection = DBMR.getInstance().getConnection();
            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                tempTableDTO.permanent_table_id = rs.getLong("permanent_table_id");
                tempTableDTO.operation_type = rs.getInt("operation_type");
                tempTableDTO.approval_path_type = rs.getLong("approval_path_type");
                tempTableDTO.approval_order = rs.getInt("approval_order");
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return tempTableDTO;
    }

    public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, String tableName, boolean isPermanentTable, long userApprovalPathType) throws Exception {
        System.out.println("table: " + p_searchCriteria);
        List<Long> idList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement ps = null;

        try {

            String sql = getSqlWithSearchCriteria(p_searchCriteria, -1, -1, GETIDS, tableName, isPermanentTable, userApprovalPathType);

            connection = DBMR.getInstance().getConnection();
            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                idList.add(rs.getLong("ID"));
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return idList;
    }

    public void manageWriteOperations(CommonDTO commonDTO, int operationType, long id, UserDTO userDTO) {
        TempTableDTO tempTableDTO = new TempTableDTO();
        tempTableDTO.approval_path_type = userDTO.approvalPathID;
        tempTableDTO.permanent_table_id = commonDTO.iD;
        tempTableDTO.approval_order = userDTO.approvalOrder;
        if (userDTO.approvalOrder == -1) {
            System.out.println("setting approval order = 0");
            tempTableDTO.approval_order = 0;
        }
        try {
            if (operationType == SessionConstants.INSERT) {
                if (approval_module_mapDTO.hasAddApproval && userDTO.approvalOrder < userDTO.maxApprovalOrder) {
                    System.out.println("Inserting into temp table for ading");
                    tempTableDTO.operation_type = SessionConstants.INSERT;
                    add(commonDTO, tempTableName, tempTableDTO);
                } else {
                    System.out.println("Inserting into real table");
                    add(commonDTO);
                }
            } else if (operationType == SessionConstants.UPDATE) {
                if (approval_module_mapDTO.hasEditApproval && userDTO.approvalOrder < userDTO.maxApprovalOrder) {
                    System.out.println("Inserting into temp table for updating");
                    tempTableDTO.operation_type = SessionConstants.UPDATE;
                    add(commonDTO, tempTableName, tempTableDTO);
                } else {
                    System.out.println("Updating into real table");
                    update(commonDTO);
                }
            } else if (operationType == SessionConstants.DELETE) {
                if (approval_module_mapDTO.hasDeleteApproval && userDTO.approvalOrder < userDTO.maxApprovalOrder) {
                    System.out.println("Inserting into temp table for updating");
                    tempTableDTO.operation_type = SessionConstants.DELETE;
                    add(commonDTO, tempTableName, tempTableDTO);
                } else {
                    System.out.println("Updating into real table");
                    delete(id);
                }

            } else if (operationType == SessionConstants.VALIDATE) {
                if (userDTO.approvalOrder == tempTableDTO.approval_order && userDTO.approvalRole == SessionConstants.VALIDATOR) //or else that user cannot validate
                {
                    System.out.println("Inserting into temp table for updating");
                    update(commonDTO, tempTableName);
                }
            } else if (operationType == SessionConstants.REJECT) {
                if (userDTO.approvalOrder == tempTableDTO.approval_order) //or else that user cannot reject
                {
                    deleteByID(id, tempTableName);
                }

            } else if (operationType == SessionConstants.APPROVE) {
                tempTableDTO = getTempTableDTOFromTableById(tempTableName, id);
                if (userDTO.approvalOrder == tempTableDTO.approval_order) //or else that user cannot approve
                {
                    if (userDTO.approvalOrder < userDTO.maxApprovalOrder) {
                        incrementApprovalOrder(tempTableName, id);
                    } else {
                        long permanent_table_id = tempTableDTO.permanent_table_id;
                        if (tempTableDTO.operation_type == SessionConstants.INSERT) {
                            add(commonDTO);

                        } else if (tempTableDTO.operation_type == SessionConstants.UPDATE) {
                            commonDTO.iD = permanent_table_id;
                            update(commonDTO);
                        } else if (tempTableDTO.operation_type == SessionConstants.DELETE) {
                            delete(permanent_table_id);
                        }

                        deleteByID(id, tempTableName);

                    }

                }
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public Collection getIDs(String tableName) {
        Collection data = new ArrayList();
        Connection connection = null;
        Statement stmt = null;
        ResultSet resultSet = null;

        String sql = getSqlWithSearchCriteria(null, -1, -1, GETIDS, tableName, true, 0);

        printSql(sql);

        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();

            for (resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("ID"))) ;

            resultSet.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null && !resultSet.isClosed()) {
                    resultSet.close();
                }
            } catch (Exception ex) {

            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return data;
    }

    public abstract Collection getDTOs(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType);

    public abstract String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType);

    public long add(CommonDTO commonDTO) throws Exception {
        return add(commonDTO, tableName, null);
    }

    public abstract long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception;

    public long update(CommonDTO commonDTO) throws Exception {
        return update(commonDTO, tableName);
    }

    public abstract long update(CommonDTO commonDTO, String tableName) throws Exception;

    public void delete(long ID) throws Exception {
        deleteByID(ID, tableName);
    }

    public void delete(List<Long> idList,long lasModificationTime,Connection connection) throws Exception {
        deleteByIdList(idList,tableName,lasModificationTime,connection);
    }
    public CommonDTO getDTOByID(long ID) throws Exception {
        return getDTOByID(ID, tableName);
    }

    public abstract CommonDTO getDTOByID(long ID, String tableName) throws Exception;

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category) {
        return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, tableName, true, 0);
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType,
                                           CommonMaps maps, String joinSQL) {
        String sql = "SELECT ";
        if (category == GETIDS) {
            sql += " " + tableName + ".ID as ID ";
        } else if (category == GETCOUNT) {
            sql += " count( " + tableName + ".ID) as countID ";
        } else if (category == GETDTOS) {
            sql += " distinct " + tableName + ".* ";
        }
        sql += "FROM " + tableName + " ";
        sql += joinSQL;

        String AnyfieldSql = "";
        String AllFieldSql = "";

        if (p_searchCriteria != null) {


            Enumeration names = p_searchCriteria.keys();
            String str, value;

            AnyfieldSql = "(";

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                int i = 0;
                Iterator it = (maps).java_anyfield_search_map.entrySet().iterator();
                while (it.hasNext()) {
                    if (i > 0) {
                        AnyfieldSql += " OR  ";
                    }
                    Entry pair = (Entry) it.next();
                    AnyfieldSql += pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
                    i++;
                }
            }
            AnyfieldSql += ")";
            System.out.println("AnyfieldSql = " + AnyfieldSql);

            AllFieldSql = "(";
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                System.out.println(str + ": " + value);
                if ((maps).java_allfield_type_map.get(str.toLowerCase()) != null && !(maps).java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
                        && !str.equalsIgnoreCase("AnyField")
                        && value != null && !value.equalsIgnoreCase("")) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql += " AND  ";
                    }
                    if ((maps).java_allfield_type_map.get(str.toLowerCase()).equals("String")) {
                        AllFieldSql += "" + tableName + "." + str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
                    } else {
                        AllFieldSql += "" + tableName + "." + str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
                    }
                    i++;
                }
            }

            AllFieldSql += ")";
            System.out.println("AllFieldSql = " + AllFieldSql);


        }

        if (!AnyfieldSql.equals("()") && !AnyfieldSql.equals("")) {

        }
        sql += " WHERE ";
        sql += " " + tableName + ".isDeleted = 0 ";

        if (!AnyfieldSql.equals("()") && !AnyfieldSql.equals("")) {
            sql += " AND " + AnyfieldSql;

        }
        if (!AllFieldSql.equals("()") && !AllFieldSql.equals("")) {
            sql += " AND " + AllFieldSql;
        }

        if (!isPermanentTable) {
            sql += " AND approval_path_type = " + userApprovalPathType;
        }


        sql += " order by " + tableName + ".lastModificationTime desc ";

        printSql(sql);

        if (limit >= 0) {
            sql += " limit " + limit;
        }
        if (offset >= 0) {
            sql += " offset " + offset;
        }

        //System.out.println("-------------- sql = " + sql);

        return sql;
    }


    public List<Long> getChilIds(String parentName, String childName, long parentID) throws Exception {

        List<Long> chilIds = new ArrayList<Long>();
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {

            String sql = "select id from " + childName + " WHERE " + parentName + "_id=" + parentID + " and isDeleted = 0";

            System.out.println("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                chilIds.add(rs.getLong("id"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return chilIds;
    }

    public void deleteChildrenNotInList(String parentName, String childName, long parentID, List<Long> chilIds) throws Exception {


        Connection connection = null;
        Statement stmt = null;
        try {

            //String sql = "UPDATE project_tracker_files SET isDeleted=0 WHERE project_tracker_id="+projectTrackerID;
            String sql = "delete from " + childName + " WHERE " + parentName + "_id=" + parentID;
            if (chilIds != null && chilIds.size() > 0) {
                sql += " and id not in (";
                sql += chilIds.get(0);
                for (int i = 1; i < chilIds.size(); i++) {
                    sql += " ," + chilIds.get(i);
                }
                sql += ")";
            }
            System.out.println("sql " + sql);
            connection = DBMW.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.execute(sql);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
    }

    public List<Long> getChildIdsFromRequest(HttpServletRequest request, String childDTOName) throws Exception {
        ArrayList<Long> childIdList = new ArrayList<Long>();
        if (request.getParameterValues(childDTOName + ".iD") != null) {

            for (int index = 0; index < request.getParameterValues(childDTOName + ".iD").length; index++) {
                childIdList.add(Long.parseLong(request.getParameterValues(childDTOName + ".iD")[index]));
            }

            return childIdList;
        }
        return null;
    }
}	
	
	
