package util;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class HttpUtil {

    private static final Logger logger = Logger.getLogger( HttpUtil.class );

    public static String sendGet( String url, Map<String,String> headers ) throws IOException {

        URL obj = new URL( url );
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");

        if( headers != null && headers.get( "User-Agent" ) != null )
            con.setRequestProperty("User-Agent", headers.get( "User-Agent" ) );

        int responseCode = con.getResponseCode();

        logger.debug( "Url " + url + ", GET Response Code :: " + responseCode );

        if ( responseCode == HttpURLConnection.HTTP_OK ) { // success

            BufferedReader in = new BufferedReader( new InputStreamReader( con.getInputStream() ) );
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null ) {
                response.append(inputLine);
            }
            in.close();

            logger.debug( "response from " + url + ", " + response);

            return response.toString();
        } else {

            logger.debug( "GET request to " + url + " not worked" );
            return null;
        }
    }
}
