package util;

import login.LoginDTO;
import org.apache.log4j.Logger;
import pb.Utils;
import sessionmanager.SessionConstants;
import user.UserDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;

public class RecordNavigationManager4 {
    String m_navigatorName;
    String m_tableName;
    String m_dtoCollectionName;
    HttpServletRequest m_request;
    NavigationService4 m_service;
    String[][] m_searchFieldInfo;
    int m_countIDs = 0;
    int m_totalPage = 0;
    int m_recordsPerPage = 0;
    Hashtable m_Hashtable = null;
    HttpSession m_Session = null;
    long m_timeToSet = 0;
    boolean m_isPermanentTable = true;
    UserDTO m_userDTO;
    String m_filter = "";
    boolean m_tableHasJobCat = false;
    private final Logger logger = Logger.getLogger(RecordNavigationManager4.class);

    public RecordNavigationManager4(String p_navigatorName, HttpServletRequest p_request, NavigationService4 p_service,
                                    String p_dtoCollectionName, String[][] p_searchFieldInfo, String p_tableName, boolean isPermanentTable, UserDTO userDTO) {
        m_navigatorName = p_navigatorName;
        m_request = p_request;
        m_service = p_service;
        m_dtoCollectionName = p_dtoCollectionName;
        m_searchFieldInfo = p_searchFieldInfo;
        m_Hashtable = null;
        m_tableName = p_tableName;
        m_isPermanentTable = isPermanentTable;
        m_userDTO = userDTO;
    }

    public RecordNavigationManager4(String p_navigatorName, HttpServletRequest p_request, NavigationService4 p_service,
                                    String p_dtoCollectionName, String[][] p_searchFieldInfo, String p_tableName, boolean isPermanentTable,
                                    UserDTO userDTO, String filter, boolean tableHasJobCat) {
        this(p_navigatorName, p_request, p_service, p_dtoCollectionName, p_searchFieldInfo, p_tableName, isPermanentTable, userDTO);
        m_filter = filter;
        m_tableHasJobCat = tableHasJobCat;
    }


    private void setRN(RecordNavigator rn, int tempPageNumber) {
        rn.setSearchFieldInfo(m_searchFieldInfo);
        m_totalPage = m_countIDs / rn.getPageSize();
        if (m_countIDs % rn.getPageSize() != 0) {
            m_totalPage++;
        }
        if (m_countIDs > 0) {
            rn.setCurrentPageNo(1);
        } else {
            rn.setCurrentPageNo(0);
        }
        rn.setTotalRecords(m_countIDs);
        rn.setTotalPages(m_totalPage);

        rn.m_isPermanentTable = m_isPermanentTable;
        rn.m_tableName = m_tableName;

        logger.debug("total pages = " + rn.getTotalPages() + " page size = " + rn.getPageSize());


        if (tempPageNumber > rn.getTotalPages()) {
            tempPageNumber = rn.getTotalPages();
        } else if (tempPageNumber <= 0) {
            tempPageNumber = 1;
        }

        rn.setCurrentPageNo(tempPageNumber);
        rn.setSearchTime(m_timeToSet);

        

    }


    private void fillHashTable() {
        Enumeration paramList = m_request.getParameterNames();
        m_Hashtable = new Hashtable();
        do {
            if (!paramList.hasMoreElements()) {
                break;
            }
            String paramName = (String) paramList.nextElement();
            if (!paramName.equalsIgnoreCase("RECORDS_PER_PAGE") && !paramName.equalsIgnoreCase("search") && !paramName.equalsIgnoreCase(SessionConstants.HTML_SEARCH_CHECK_FIELD)) {
                String paramValue = m_request.getParameter(paramName);

                if(Utils.isValidSearchString(paramValue))
                {
                	m_Session.setAttribute(paramName, paramValue);
                    m_Hashtable.put(paramName, paramValue);
                }
            }
        } while (true);

    }
    
    public void doJob(LoginDTO loginDTO) throws Exception
    {
    	doJob(loginDTO, m_request.getParameter("RECORDS_PER_PAGE"));
    }
    
    
   
    

    public void doJob(LoginDTO loginDTO, String pageSizeGiven) throws Exception {
    	
    	logger.debug("doJob, filter = " + m_filter);

        Collection records = null;
        m_Session = m_request.getSession();

        RecordNavigator rn = (RecordNavigator) m_Session.getAttribute(m_navigatorName);
        if (rn == null) {
            rn = new RecordNavigator();
        }
        String goCheck = m_request.getParameter("go");
        String searchCheck = m_request.getParameter("search");
        String link = m_request.getParameter("link");

        long currentTime = System.currentTimeMillis();
        long lastSearchTime = 0;
        if (m_request.getParameter("lastSearchTime") != null && !m_request.getParameter("lastSearchTime").equalsIgnoreCase("")) {
            lastSearchTime = Long.parseLong(m_request.getParameter("lastSearchTime"));
        }
        m_timeToSet = lastSearchTime;


        int pageno = 1;
        
        int pageSizeInt = -1;
        String pageSize = pageSizeGiven;
        logger.debug("Page Size Given : " + pageSize);


        if (pageSize != null) {
            try {
                pageSizeInt = Integer.parseInt(pageSize);
                logger.debug("pageSizeInt = " + pageSizeInt);
                if (pageSizeInt > 0)
                    rn.setPageSize(pageSizeInt);
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
                //logger.fatal("Next page Size is not number ");
            }
        }

        if (searchCheck != null) //Normal or ajax search
        {
            logger.debug("###########Search pressed");

            fillHashTable();

            if (goCheck != null) //Go Button Pressed
            {
                logger.debug("###########Go pressed");
                pageno = Integer.parseInt(m_request.getParameter("pageno"));
                logger.debug("pagenumber = " + pageno);
                m_countIDs = Integer.parseInt(m_request.getParameter("TotalRecords"));

                if (lastSearchTime < m_service.getLastModificationTime()) {
                    m_countIDs = m_service.getCount(m_Hashtable, -1, -1, m_isPermanentTable, m_userDTO, m_filter, m_tableHasJobCat);
                    m_timeToSet = currentTime;
                }
            } else {
                m_countIDs = m_service.getCount(m_Hashtable, -1, -1, m_isPermanentTable, m_userDTO, m_filter, m_tableHasJobCat);
                m_timeToSet = currentTime;
            }
            setRN(rn, pageno);
        } else //1st time in the search page
        {
            logger.debug("###########From Menu");
            m_Session.setAttribute(m_navigatorName, null);

            fillHashTable();

            m_countIDs = m_service.getCount(m_Hashtable, -1, -1, m_isPermanentTable, m_userDTO, m_filter, m_tableHasJobCat);
            m_timeToSet = currentTime;

            setRN(rn, -1);

        }


        int nextCollectionSize;
        if (rn.getTotalRecords() == 0) {
            nextCollectionSize = 0;
        } else if (rn.getTotalRecords() > 0 && rn.getCurrentPageNo() == rn.getTotalPages() && rn.getTotalRecords() % rn.getPageSize() != 0) {
            nextCollectionSize = rn.getTotalRecords() % rn.getPageSize();
        } else {
            nextCollectionSize = rn.getPageSize();
        }
        int initial = nextCollectionSize != 0 ? (rn.getCurrentPageNo() - 1) * rn.getPageSize() : 0;

        logger.debug("############################ limit = " + nextCollectionSize + " offset = " + initial);

        if (nextCollectionSize > 0) {
            if (m_filter.equalsIgnoreCase("") && !m_tableHasJobCat) {
                records = m_service.getDTOs(m_Hashtable, nextCollectionSize, initial, m_isPermanentTable, m_userDTO);
            } else {
                records = m_service.getDTOs(m_Hashtable, nextCollectionSize, initial, m_isPermanentTable, m_userDTO,
                        m_filter, m_tableHasJobCat);
            }

        }
        m_Session.setAttribute(m_navigatorName, rn);
        m_Session.setAttribute(m_dtoCollectionName, records);
    }
}