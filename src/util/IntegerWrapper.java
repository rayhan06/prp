package util;

@SuppressWarnings("unused")
public class IntegerWrapper {
    private int value;
    private final int initValue;

    public IntegerWrapper(int value) {
        this.value = value;
        this.initValue = value;
    }

    public IntegerWrapper() {
        this.value = 0;
        this.initValue = 0;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isValueModified(){
        return this.value != this.initValue;
    }

    public int incrementAndGet(){
        value++;
        return value;
    }
}
