package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import common.ConnectionAndStatementUtil;
import dbm.*;
import org.apache.log4j.Logger;

/**
 * @author Kayesh Parvez
 */
public class ConnectionUtil {
    private static final Logger logger = Logger.getLogger(ConnectionUtil.class);

    private static final String updateVbSequencer = "update vbSequencer set table_LastModificationTime = %d where table_name = '%s'";

    public static void updateVbSequencer(String tableName, long currentTime, Connection connection) throws SQLException {
        ConnectionAndStatementUtil.getWriteStatement(st->{
            String sql = String.format(updateVbSequencer,currentTime,tableName);
            try {
                st.executeUpdate(sql);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        },connection);
    }

    public static void updateVbSequencer(String tableName, long currentTime, Statement statement) throws SQLException {
        //String sql = "update vbSequencer set table_LastModificationTime = " + currentTime + " where table_name = '" + tableName + "'";
        statement.executeUpdate(String.format(updateVbSequencer,currentTime,tableName));
    }
}
