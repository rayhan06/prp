package util;

/*
 * @author Md. Erfan Hossain
 * @created 22/07/2022 - 4:04 PM
 * @project parliament
 */

public class BooleanWrapper {
    private boolean value;

    private BooleanWrapper(boolean value) {
        this.value = value;
    }

    public static BooleanWrapper of() {
        return of(false);
    }

    public static BooleanWrapper of(boolean value) {
        return new BooleanWrapper(value);
    }

    public boolean getValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }
}
