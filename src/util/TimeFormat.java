package util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;
import pb.Utils;

import java.time.Period;
import java.time.ZoneId;
import java.time.Instant;
import java.time.LocalDate;


public class TimeFormat {
    public static Logger logger = Logger.getLogger(TimeFormat.class);


    public TimeFormat() {
    }

    public static String[] weekDayInBangla = {"রবিবার", "সোমবার", "মঙ্গলবার", "বুধবার", "বৃহস্পতিবার", "শুক্রবার", "শনিবার"};
    
    public static String getAge(long dateOfBirth, String language)
    {
    	LocalDate today = LocalDate.now();
    	LocalDate birthday = Instant.ofEpochMilli(dateOfBirth).atZone(ZoneId.systemDefault()).toLocalDate();


    	Period p = Period.between(birthday, today);
    	
    	String age = "";
    	
    	if(language.equalsIgnoreCase("english"))
    	{
    		age = p.getYears() + " years ";
    	}
    	else
    	{
    		age = p.getYears() + " বছর ";
        }
    	
    	
    	return Utils.getDigits(age, language);
    }
    
    public static long getMillisFromHourMiunteAMPM(String value)
    {
    	try
    	{
    		String [] timeAndAmPm = value.split(" ");
    		if(timeAndAmPm == null || timeAndAmPm.length < 2)
    		{
    			return 0;
    		}
    		
    		String amPm = timeAndAmPm[1];
    		String[] hourMinute = timeAndAmPm[0].split(":");
    		if(hourMinute == null || hourMinute.length < 2)
    		{
    			return 0;
    		}
    		
    		if(hourMinute[0].equalsIgnoreCase("Nan") || hourMinute[1].equalsIgnoreCase("Nan"))
    		{
    			return 0;
    		}
	    		    	
			int hour = Integer.parseInt(hourMinute[0]);
			int minute = Integer.parseInt(hourMinute[1]);
			
			long timeInMillis = (hour * 60 + minute) * 60000;
			if(amPm.equalsIgnoreCase("pm"))
			{
				timeInMillis += (12 * 60 + minute) * 60000;
			}
			System.out.println(" timeInMillis = " + timeInMillis);
			return timeInMillis;
    	}
    	catch(Exception e)
    	{
    		System.out.println("Returning 0");
    		return 0;
    	}    	
    }
    
    public static long getMillisFromHourMiunte(String value)
    {
    	try
    	{
	    	String hourStr = value.split(":")[0];
			int hour = Integer.parseInt(hourStr);
			String minuteStr = value.split(":")[1];
			int minute = Integer.parseInt(minuteStr);
			
			long timeInMillis = (hour * 60 + minute) * 60000;
			return timeInMillis;
    	}
    	catch(Exception e)
    	{
    		System.out.println("Returning 0");
    		return 0;
    	}    	
    }
    
    public static String getInAmPmFormat (long value)
    {
    	return getInAmPmFormat(value + "");
    }
    
    public static String getInAmPmFormat (String value)
    {
    	try
    	{
	    	String hourStr = value.split(":")[0];
			int hour = Integer.parseInt(hourStr);
			DecimalFormat formatter = new DecimalFormat("00");
			String minuteStr = value.split(":")[1];
			int minute = Integer.parseInt(minuteStr);
			minuteStr = formatter.format(minute);
			String ampm;
			if(hour < 12)
			{
				ampm = "AM";
			}
			else
			{
				ampm = "PM";
				hour -= 12;
			}
			if(hour == 0)
			{
				hour = 12;
			}
			//System.out.println("hour : " + hourStr + " --> " + hour + " " + ampm);
			
			return formatter.format(hour) + ":" + minuteStr + " " + ampm;
			
    	}
    	catch(Exception e)
    	{
    		//logger.error("",e);
    		System.out.println("Returning empty for " + value);
    		return "";
    	} 
    }
    
    public static String getIn24HourFormat (String value)
    {
    	if(value == null || value.equalsIgnoreCase(""))
    	{
    		return "";
    	}
    	String hourMinStr = value.split(" ")[0];
    	String amPm = value.split(" ")[1];
    	String hourStr = hourMinStr.split(":")[0];
    	int hour = Integer.parseInt(hourStr);
    	String minuteStr = hourMinStr.split(":")[1].split(" ")[0];
    	int minute = Integer.parseInt(minuteStr);
    	
    	if(amPm.equalsIgnoreCase("pm") && hour != 12)
    	{
    		hour += 12;
    	}
    	else if(amPm.equalsIgnoreCase("am") && hour == 12)
    	{
    		hour = 0;
    	}
    	if(hour == 24)
    	{
    		hour = 0;
    	}
    	
    	DecimalFormat formatter = new DecimalFormat("00");
    	String ret = formatter.format(hour) + ":" + formatter.format(minute);
    	//System.out.println(value + " => " + ret);
    	return ret;
    }

    public static String getHourDifference(String startTime, String endTime, long startDate, long endDate){
        try{
            startTime = getIn24HourFormat(startTime);
            endTime = getIn24HourFormat(endTime);

            int dayDifference = (int) ((endDate - startDate) / 86400);
            int startTimeHour = Integer.parseInt(startTime.split(":")[0]);
            int startTimeMinute = Integer.parseInt(startTime.split(":")[1]);
            int endTimeHour = 24 * dayDifference + Integer.parseInt(endTime.split(":")[0]);
            int endTimeMinute = Integer.parseInt(endTime.split(":")[1]);

            if(endTimeMinute < startTimeMinute){
                endTimeMinute += 60;
                endTimeHour++;
            }

            StringBuilder timeDiff = new StringBuilder();
            timeDiff.append(""+ (endTimeHour - startTimeHour));
            timeDiff.append(":");
            timeDiff.append("" + (endTimeMinute - startTimeMinute));
            return timeDiff.toString();
        }catch (Exception e){
            logger.debug(e);
        }
        return "";
    }
    
    public static int getNthWeek(long lDate)
    {
    	Calendar cal = Calendar.getInstance();
    	cal.setTimeInMillis(lDate);
    	int day = cal.get(Calendar.DAY_OF_MONTH);
    	System.out.println("Found day = " + day);
    	cal.set(Calendar.DAY_OF_MONTH, 1);
    	int i1stDayOfMonth = cal.get(Calendar.DAY_OF_WEEK);
    	int modded1stDay = i1stDayOfMonth % 7;
    	
    	int weekCount = (day -1 + modded1stDay) / 7 + 1;
    	System.out.println("Found i1stDayOfMonth = " + i1stDayOfMonth);
    	
    	return weekCount;
    }
    
    public static int getNthWeek()
    {
    	Calendar cal = Calendar.getInstance();
    	int day = cal.get(Calendar.DAY_OF_MONTH);
    	System.out.println("Found day = " + day);
    	cal.set(Calendar.DAY_OF_MONTH, 1);
    	int i1stDayOfMonth = cal.get(Calendar.DAY_OF_WEEK);
    	int modded1stDay = i1stDayOfMonth % 7;
    	
    	int weekCount = (day -1 + modded1stDay) / 7 + 1;
    	System.out.println("Found i1stDayOfMonth = " + i1stDayOfMonth);
    	
    	return weekCount;
    }
    public static String getInAmPmFormatUpdated (String value)
    {
        try
        {
            String hourStr = value.split(":")[0];
            int hour = Integer.parseInt(hourStr);
            DecimalFormat formatter = new DecimalFormat("00");
            String minuteStr = value.split(":")[1].split(" ")[0];
            int minute = Integer.parseInt(minuteStr);
            minuteStr = formatter.format(minute);
            String ampm = value.split(":")[1].split(" ")[1];

            System.out.println("hour : " + hourStr + " --> " + hour);

            return hour + ":" + minuteStr + " " + ampm;

        }
        catch(Exception e)
        {
            System.out.println("Returning 0");
            return "";
        }
    }
    
    public static int getHour (String value)
    {
    	String hourStr = value.split(":")[0];
		int hour = Integer.parseInt(hourStr);
		if(hour >= 12)
		{				
			hour -= 12;
		}
		if(hour == 0)
		{
			hour = 12;
		}
		return hour;
    }
    
    public static int getMinute (String value)
    {
		String minuteStr = value.split(":")[1];
		return Integer.parseInt(minuteStr);
    }
    
    public static String getAMPM (String value)
    {
    	String hourStr = value.split(":")[0];
		int hour = Integer.parseInt(hourStr);
		String ampm;
		if(hour < 12)
		{
			ampm = "AM";
		}
		else
		{
			ampm = "PM";			
		}
		
		return ampm;
    }
    
    public static Long MILLIS_IN_A_DAY = 86400000L;

    public static String formatDuration(long timeInSecond) {
        String duration = null;
        long minute = timeInSecond / 60L;
        long second = timeInSecond % 60L;
        String min = Long.toString(minute);
        String sec = Long.toString(second);
        if (second < 10L) {
            sec = '0' + sec;
        }
        duration = min + ":" + sec;
        return duration;
    }

    public static long getDateFromSystemTime() {
    	DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    	return TimeFormat.getSpecificDateInMilis(df.format(System.currentTimeMillis()));
    			
    }
    
    public static String getBillFileDate(){       
        String date="";
        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        Calendar calendar = Calendar.getInstance();        
        calendar.setTimeInMillis(System.currentTimeMillis());
        date = formatter.format(calendar.getTime());        
        return date;
    }
    
    public static String getBillFileDate(long time){       
        String date="";
        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        Calendar calendar = Calendar.getInstance();        
        calendar.setTimeInMillis(System.currentTimeMillis());
        date = formatter.format(time);        
        return date;
    }

    public static String getCurrentMonthStart() {
        long time = System.currentTimeMillis();
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time);

        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);

        month++;
        String date = "01-" + ((month + "").length() == 1 ? ("0" + month) : month) + "-" + year;

        return date;
    }

    public static boolean isLeapYear(int year) {
        return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
    }

    public static int lastDayOfMonth(int month, int year) {
        int endOfMonth = -1;
        // alert("M="+month);

        switch (month) {
            case 0://January
            case 2://March
            case 4://May
            case 6://July
            case 7://August
            case 9://October
            case 11://December
                endOfMonth = 31;
                break;

            case 1://February
                endOfMonth = isLeapYear(year) ? 29 : 28;
                break;

            case 3://April
            case 5://June
            case 8://September
            case 10://November
                endOfMonth = 30;
                break;
        }
        return endOfMonth;
    }

    public static String getCurrentMonthFinish() {
        long time = System.currentTimeMillis();
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time);

        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);

        int day = lastDayOfMonth(month, year);

        month++;
        String date = ""+day+"-" + ((month + "").length() == 1 ? ("0" + month) : month) + "-" + year;

        return date;
    }

    public static String getValidityDate(int day) {
        long time = System.currentTimeMillis();
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time);

        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);

        String date;
        month += 2;
        if (month <= 12) {
            date = ((day + "").length() == 1 ? ("0" + day) : day) + "-" + ((month + "").length() == 1 ? ("0" + month) : month) + "-" + year;
        } else {
            year++;
            date = ((day + "").length() == 1 ? ("0" + day) : day) + "-01-" + year;
        }

        return date;
    }
    
    public static String getLastDateofPayment(int day)
    {
    	long time = TimeFormat.getStartTimeOfDayInMilis(System.currentTimeMillis());
    	time += day*TimeFormat.MILLIS_IN_A_DAY;
    	SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
    	return format.format(new Date(time));
    }

    public static long getCurrentMonthStartInMilis() {
        long time = 0;
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        try {
            time = df.parse(getCurrentMonthStart()).getTime();
        } catch (ParseException e) {
            logger.error("",e);
        }

        return time;
    }

    public static long getSpecificDateInMilis(String date) {
        long time = 0;
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        try {
            time = df.parse(date).getTime();
        } catch (ParseException e) {
            logger.error("",e);
        }

        return time;
    }
    
    public static String getSpecificDate(long time) {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        return df.format(time);
    }
    
    
    public static long getStartTimeOfDayInMilis(long time_in_milis) {
        long time=0;
        DateFormat formatter = new SimpleDateFormat("d-M-yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time_in_milis);
        String date = formatter.format(calendar.getTime());
        
        try {
            time = formatter.parse(date).getTime();
        } catch (Exception e) {
            logger.error("",e);
        }

        return time;
    }
    
    public static long getLastTimeOfMonthInMilis(long time_in_milisec) {
        long lastDayMili = 0;
        DateFormat formatter = new SimpleDateFormat("d-M-yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time_in_milisec);
        String date = formatter.format(calendar.getTime());
        String[] parse = date.split("-");
        ////System.out.println("Day:"+parse[0]+":month:"+parse[1]+":year:"+parse[2]);
        int days_in_month = getDaysInMonth(parse[2], parse[1]);
        ////System.out.println("Days in month:"+days_in_month);
        date = days_in_month + "-" + parse[1] + "-" + parse[2];
        ////System.out.println("Modified Date:"+date);
        lastDayMili = getStartTimeOfDayInMilis(date);
        return lastDayMili + MILLIS_IN_A_DAY - 1000;
    }
    public static int getDaysInMonth(String year, String month) {
        
        GregorianCalendar calendar = new GregorianCalendar();
        int yearInt = Integer.parseInt(year);
        int monthInt = Integer.parseInt(month);
        monthInt = monthInt - 1;
        calendar.set(yearInt, monthInt, 1);
        int dayInt = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);

        return dayInt;
    }
    
    public static long getStartTimeOfDayInMilis(String date) {
        long time = 0;
        DateFormat df = new SimpleDateFormat("d-M-yyyy");
        try {
            time = df.parse(date).getTime();
        } catch (Exception e) {
            logger.error("",e);
        }

        return time;
    }
    
    public static long getStartTimeOfDayInMilisFromBillFileDate(String date){
        long time = 0;
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        try {
            time = df.parse(date).getTime();
        } catch (Exception e) {
            logger.error("",e);
        }

        return time;
        
    }
    
    public static long getEndTimeOfDayInMilis(long time_in_milis) {
        long time=0;
        DateFormat formatter = new SimpleDateFormat("d-M-yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time_in_milis);
        String date = formatter.format(calendar.getTime());
        
        try {
            time = formatter.parse(date).getTime();
        } catch (Exception e) {
            logger.error("",e);
        }

        return time+UtilityConstants.MILI_SECONDS_IN_A_DAY-1000;
    }
    
    

    

    public static String getCurrentDateWithFormat(long time, String dateFormat) {
        DateFormat df = new SimpleDateFormat(dateFormat);
        return df.format(time);
    }
    public static String getCurrentDateWithFormat( String dateFormat) {
        DateFormat df = new SimpleDateFormat(dateFormat);
        return df.format(System.currentTimeMillis());
    }

    public static String getAgeFromSpecificDate(long dateOfBirth, long specificDate, String language)
    {
        LocalDate today = Instant.ofEpochMilli(specificDate).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate birthday = Instant.ofEpochMilli(dateOfBirth).atZone(ZoneId.systemDefault()).toLocalDate();


        Period p = Period.between(birthday, today);


        String age = "";

        if(language.equalsIgnoreCase("english"))
        {
            age = p.getYears() + " years " + p.getMonths() +
                    " months " + p.getDays() + " days";
        }
        else
        {
            age = p.getYears() + " বছর " + p.getMonths() +
                    " মাস " + p.getDays() + " দিন";
            age = Utils.getDigits(age, "bangla");
        }


        return age;
    }

    public static String getAgeFromSpecificDays(long duration, String language)
    {
        long years = duration / 365;
        long res = duration % 365;
        long month = res / 30;
        long days = res % 30;


        String age = "";


        if(language.equalsIgnoreCase("english"))
        {
            age = years + " years " + month +
                    " months " + days + " days";
        }
        else
        {
            age = years + " বছর " + month +
                    " মাস " + days + " দিন";
            age = Utils.getDigits(age, "bangla");
        }



        return age;
    }

    public static String getMorningOrNoonInBangla(String value){
        String returnValue;
        if(value.contains("AM")){
            returnValue = "সকাল";
        } else {
            returnValue = "বিকাল";
        }
        return returnValue;
    }

    public static String replaceAmPM(String value){
        value = value.replace(" AM", "");
        value = value.replace(" PM", "");
        return value;
    }
}