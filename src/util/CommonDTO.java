package util;

import java.util.Arrays;

public class CommonDTO {
	public long iD = -1;
	public int isDeleted = 0;
	public long lastModificationTime = 0;
	public int jobCat = -1;
	public String subject = "";
	public String remarks = "";
	public long fileID = -1;
	public int rejectTo = -1;
	public String searchColumn = "";
	
	public boolean isSeen = false; //for inbox
	public long inboxMovementId = -1; //for inbox
	public long inboxOriginatorId = -1; //for inbox
	
	public String[] columnNames;
	
	
	public static final int ACTIVE = 0;
	public static final int DELETED = 1;
	public static final int WAITING_FOR_APPROVAL = 2;
	public static final int OUTDATED = 3;

	@Override
	public String toString() {
		return "CommonDTO{" +
				"iD=" + iD +
				", isDeleted=" + isDeleted +
				", lastModificationTime=" + lastModificationTime +
				", jobCat=" + jobCat +
				", subject='" + subject + '\'' +
				", remarks='" + remarks + '\'' +
				", fileID=" + fileID +
				", rejectTo=" + rejectTo +
				", searchColumn='" + searchColumn + '\'' +
				", isSeen=" + isSeen +
				", inboxMovementId=" + inboxMovementId +
				", inboxOriginatorId=" + inboxOriginatorId +
				", columnNames=" + Arrays.toString(columnNames) +
				'}';
	}
}
