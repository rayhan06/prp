package util;

import login.LoginDTO;
import org.apache.log4j.Logger;
import pb.Utils;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

@SuppressWarnings("unused")
public class HttpRequestUtils {
    private static final Logger logger = Logger.getLogger(HttpRequestUtils.class);
    public static ThreadLocal<Boolean> IGNORE_FILTER_THREAD_LOCAL = new ThreadLocal<>();
    public static ThreadLocal<Boolean> BACK_END_AJAX_CALL_THREAD_LOCAL = new ThreadLocal<>();
    public static ThreadLocal<CommonLoginData> COMMON_LOGIN_DATA_THREAD_LOCAL = new ThreadLocal<>();
    public static final String[] loginIgnoreExtList = {".css", ".js", ".jpg", ".png", ".ico", ".ttf", ".woff", ".woff2", ".gif",".css.map",".js.map"};

    static public boolean isAjaxRequest(ServletRequest request) {
        return "XMLHttpRequest".equals(((HttpServletRequest) request).getHeader("X-Requested-With"));
    }

    static public LoginDTO getLoginDTO(ServletRequest request) {
        return (LoginDTO) ((HttpServletRequest) request).getSession(true).getAttribute("user_login");
    }

    static public boolean isGetRequest(ServletRequest request) {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        return httpServletRequest.getMethod().equalsIgnoreCase("GET");
    }

    public static boolean ignoreHit(HttpServletRequest httpServletRequest) {
        String requestedURI = httpServletRequest.getRequestURI();
        boolean ignore = Stream.of(loginIgnoreExtList)
                .filter(requestedURI::endsWith)
                .findAny()
                .orElse(null) != null;
        return ignore || requestedURI.contains("/assets/");
    }

    public static boolean isBackEndAjaxCall(HttpServletRequest httpServletRequest) {
        return getURLWithoutContext(httpServletRequest).contains("actionType=ajax_");
    }

    public static boolean isBackEndAjaxAddOrEditCall(HttpServletRequest httpServletRequest) {
        return getURLWithoutContext(httpServletRequest).contains("actionType=ajax_add")
                || getURLWithoutContext(httpServletRequest).contains("actionType=ajax_edit");
    }

    public static String getURLWithoutContext(HttpServletRequest httpServletRequest) {
        String URL = httpServletRequest.getRequestURL().toString();
        String context = httpServletRequest.getContextPath();
        String URLWithoutContext = URL.replaceFirst(".*" + context + "/", "");
        String queryString = httpServletRequest.getQueryString();
        if (queryString != null && queryString.trim().length() != 0) {
            URLWithoutContext += "?" + queryString;
        }
        return URLWithoutContext;
    }

    public static Map<String,String> buildRequestParams(HttpServletRequest request){
        Enumeration<String> paramList = request.getParameterNames();
        Map<String,String> params = new HashMap<>();
        while (paramList.hasMoreElements()){
            String paramName = paramList.nextElement();
            String paramValue = request.getParameter(paramName);
            if(Utils.isValidSearchString(paramValue)){
                params.put(paramName,paramValue);
            }
        }
        return params;
    }
}