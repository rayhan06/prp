package util;

import com.google.gson.Gson;
import common.ApiResponse;
import common.RequestFailureException;
import language.LC;
import language.LM;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import pb.Utils;
import pbReport.*;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.tuple.*;
import org.apache.log4j.Logger;

@SuppressWarnings({"Duplicates"})
public class ReportRequestHandler {
	private static final Logger logger = Logger.getLogger(ReportRequestHandler.class);

    String[][] Criteria;

    String[][] Display;

    String GroupBy = "";
    String OrderBY = "";
    
    public static final int RIGHT_ALIGN_INT = 1;
    public static final int RIGHT_ALIGN_FLOAT = 2;
    
    public ArrayList<Pair<Integer, Integer>> clientSideDataFormatting = null;

    String sql;

    ReportService reportService;

    ReportTemplate reportTemplate;

    NavigationService4 navigationService4;


    public ReportRequestHandler(NavigationService4 navigationService4,
                                String[][] Criteria, String[][] Display, String GroupBy, String OrderBY, String sql,
                                ReportService reportService) {
        this.navigationService4 = navigationService4;
        this.Criteria = Criteria;
        this.Display = Display;
        this.GroupBy = GroupBy;
        this.OrderBY = OrderBY;
        this.sql = sql;

        this.reportService = reportService;
        reportTemplate = new ReportTemplate();
    }


    public int getTotalCount(HttpServletRequest request, HttpServletResponse response, String language) throws Exception {
        int count = reportService.getTotalCount(sql, request, Criteria, Display, GroupBy, OrderBY, language);
        return count;
    }
    
    public static final int HTML = 1;
    public static final int XL = 2;
    public static final int BOTH = 3;
    
    public List<List<Object>> getReport(HttpServletRequest request, HttpServletResponse response, String language, boolean hasLimit) throws Exception {

       return getReport(request, response, language, hasLimit, BOTH);

    }

    public List<List<Object>> getReport(HttpServletRequest request, HttpServletResponse response, String language, boolean hasLimit, int XLState) throws Exception {

        if (hasLimit) {
            Integer recordPerPage = Integer.parseInt(request.getParameter("RECORDS_PER_PAGE"));
            Integer pageNo = Integer.parseInt(request.getParameter("pageno"));
            System.out.println("recordPerPage = " + recordPerPage + " pageNo = " + pageNo);
            int offset = (pageNo - 1) * recordPerPage;
            return reportService.getResultSet(sql, request, recordPerPage, offset, Criteria, Display, GroupBy, OrderBY, language, XLState);
        } else {
            return reportService.getResultSet(sql, request, -1, -1, Criteria, Display, GroupBy, OrderBY, language, XLState);
        }

    }

    public String getConvetertedData(String paramInCamelCase, String data, String language, boolean add1WithEnd, String paramFromReq) {
        System.out.println("paramInCamelCase = " + paramInCamelCase + " paramFromReq = " + paramFromReq);
        if (data == null || data.equalsIgnoreCase("")) {
            return "";
        }
        ColumnConvertor convertor = new DefaultConvertor();
        String tableName = "";
        if (paramInCamelCase.endsWith("_type")) {
            convertor = new TypeConverter();
            tableName = paramInCamelCase.substring(0, paramInCamelCase.length() - "_type".length());
        } else if (paramInCamelCase.endsWith("_cat")) {
            convertor = new CatConverter();
            tableName = paramInCamelCase.substring(0, paramInCamelCase.length() - "_cat".length());
        } else if (paramInCamelCase.endsWith("_date")) {
            convertor = new DateConvertor();
        } else if (paramInCamelCase.endsWith("_organogram_id")) {
            convertor = new OrganogramConverter();
        }
        if(add1WithEnd && paramFromReq.equalsIgnoreCase("endDate"))
        {
        	System.out.println("Minusing 1 from enddate");
        	long lData = Long.parseLong(data);
        	lData -= SessionConstants.MS_IN_A_DAY;
        	return convertor.convert(lData, language, tableName).toString();
        }
        return convertor.convert(data, language, tableName).toString();
    }

    public XSSFCellStyle getBold(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        XSSFFont font = wb.createFont();
        font.setBold(true);
        cellStyle.setFont(font);
        return cellStyle;
    }

    public XSSFCellStyle getHeaderStyle(XSSFWorkbook wb, int fontSize) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setFillForegroundColor(IndexedColors.CORNFLOWER_BLUE.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        XSSFFont font = wb.createFont();
        font.setFontHeightInPoints((short) fontSize);
        font.setBold(true);
        cellStyle.setFont(font);
        return cellStyle;
    }

    public XSSFCellStyle getParamStyle(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        XSSFFont font = wb.createFont();
        font.setBold(true);
        cellStyle.setFont(font);
        return cellStyle;
    }

    public XSSFCellStyle getParamValueStyle(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        XSSFFont font = wb.createFont();
        font.setBold(true);
        cellStyle.setFont(font);
        return cellStyle;
    }

    public XSSFCellStyle get1stRowStyle(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        XSSFFont font = wb.createFont();
        font.setBold(true);
        cellStyle.setFont(font);
        return cellStyle;
    }

    public void autoSizeColumns(XSSFWorkbook workbook) {
        int numberOfSheets = workbook.getNumberOfSheets();
        for (int i = 0; i < numberOfSheets; i++) {
            Sheet sheet = workbook.getSheetAt(i);
            if (sheet.getPhysicalNumberOfRows() > 0) {
                Row row = sheet.getRow(sheet.getFirstRowNum());
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    int columnIndex = cell.getColumnIndex();
                    sheet.autoSizeColumn(columnIndex);
                }
            }
        }
    }

    public static final int DISPLAY_INDEX = 9;
    public static final int DB_COLUMN_NAME = 2;
    public static final int LANGUAGE_COLUMN = 10;

    public Object[] getSum(List<List<Object>> data) {
        Object[] sum = new Object[data.get(0).size()];

        if (data.size() > 1) {
            List<Object> row1stData = data.get(1);
            if (row1stData != null) {
                int j = 0;
                for (Object cellData : row1stData) {
                    if (j == 0) {
                        sum[j] = "";
                    } else if (cellData instanceof Integer) {
                        sum[j] = 0;
                    } else if (cellData instanceof Long) {
                        sum[j] = 0L;
                    } else if (cellData instanceof Double) {
                        sum[j] = 0.0;
                    } else if (Display[j][3].equalsIgnoreCase("int")) {
                        sum[j] = 0;
                    } else if (Display[j][3].equalsIgnoreCase("double")) {
                        sum[j] = 0.0;
                    } else {
                        sum[j] = "";
                    }
                    j++;
                }
            }
        }
        return sum;
    }

    public String getFormattedParam(String[] criteriaRow, UserDTO userDTO) {
        if (criteriaRow.length > 10) {
            long constant = Long.parseLong(criteriaRow[LANGUAGE_COLUMN]);
            return LM.getText(constant, userDTO) + ":";
        } else {
            return Utils.camelToSnake(criteriaRow[DISPLAY_INDEX]).replaceAll("_", " ").toUpperCase() + ":";
        }
    }
    
    public double setImageInCell(XSSFWorkbook wb, Sheet sheet, int rowIdx, int colIdx,
   		 byte[] bytes)
   {
		int pictureIdx = wb.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
		
		//Creates the top-level drawing patriarch.
		XSSFDrawing drawing = (XSSFDrawing)sheet.createDrawingPatriarch();
		//Create an anchor that is attached to the worksheet

		XSSFClientAnchor anchor = new XSSFClientAnchor();
		
		//create an anchor with upper left cell _and_ bottom right cell
		anchor.setCol1(colIdx); //Column B
		anchor.setRow1(rowIdx); //Row 3
		anchor.setCol2(colIdx + 1); //Column C
		anchor.setRow2(rowIdx + 1); //Row 4
		
		
		
		//Creates a picture
		Picture pict = drawing.createPicture(anchor, pictureIdx);
			
		//Reset the image to the original size
		//pict.resize(); //don't do that. Let the anchor resize the image!
		//pict.resize(0.2); //don't do that. Let the anchor resize the image!
		double height = pict.getImageDimension().getHeight();
		return height;
   }


    public XSSFWorkbook createXl(List<List<Object>> data, String headerStr, HttpServletRequest request, String language, UserDTO userDTO) {
        XSSFWorkbook wb = new XSSFWorkbook();
        Sheet sheet = wb.createSheet();

        int i = 2;

        Row row;

        Cell cell;

        System.out.println("Data size = " + data.size());
        Header header = sheet.getHeader();
        header.setCenter(headerStr);
        System.out.println("Header added");
        boolean isLangBng = "Bangla".equalsIgnoreCase(language);

        //Add Parameters
        for (String[] criteriaRow : Criteria) {
            String paramData = getConvetertedData(criteriaRow[DB_COLUMN_NAME], request.getParameter(criteriaRow[DISPLAY_INDEX]), language,
            		Boolean.parseBoolean(request.getParameter("add1WithEnd")), criteriaRow[DISPLAY_INDEX]);
            if (!paramData.equalsIgnoreCase("")) {
                row = sheet.createRow(i);

                cell = row.createCell(0);
                cell.setCellValue(getFormattedParam(criteriaRow, userDTO));
                cell.setCellStyle(getParamStyle(wb));

                System.out.println("param = " + criteriaRow[DISPLAY_INDEX] + " " + request.getParameter(criteriaRow[DISPLAY_INDEX]));
                cell = row.createCell(1);
                cell.setCellValue(paramData);
                cell.setCellStyle(getParamValueStyle(wb));

                for (int j = 0; j < data.get(0).size() - 2; j++) {
                    cell = row.createCell(j + 2);
                    cell.setCellStyle(getParamStyle(wb));
                }
                i++;
            }
        }


        Object[] sum = getSum(data);
        System.out.println("sum size = " + sum.length);

        int tableRowCount = 0;
        for (List<Object> rowData : data) {
            System.out.println("rowData size = " + rowData.size());
            row = sheet.createRow(i);
            int j = 0;
            for (Object cellData : rowData) {
            	if(Display[j][3].equalsIgnoreCase("invisible"))
            	{
            		j++;
            		continue;
            	}
                cell = row.createCell(j);
                if (tableRowCount == 0) {//Make the Tile Bold
                    cell.setCellStyle(get1stRowStyle(wb));
                    cell.setCellValue(cellData.toString());
                } else {
                    if ((Display[j][3].equalsIgnoreCase("double") || Display[j][3].equalsIgnoreCase("int")) && cellData instanceof String) {
                        cell.setCellValue(Utils.getDigits(cellData.toString(), language));
                    } else if(!(cellData instanceof byte[])){
                        cell.setCellValue(cellData.toString().replaceAll("<br>", " "));
                    }
                    if (j > 0) {
                    	if(cellData instanceof byte[])
                    	{
                    		
                    		short height = (short)setImageInCell(wb, sheet, i, j, (byte[])cellData);
                    		//short height = (short)getImageHeight((byte[])cellData);
                    		System.out.println("height = " + height);
                    		row.setHeight((short)(height * 3));
            				
                    	}
                    	else if (cellData instanceof Integer) {
                            String val = cellData.toString();
                            if (isLangBng) {
                                val = StringUtils.convertToEngNumber(val);
                            }
                            sum[j] = (int) sum[j] + Integer.parseInt(val);
                        } else if (cellData instanceof Long) {
                            String val = cellData.toString();
                            if (isLangBng) {
                                val = StringUtils.convertToEngNumber(val);
                            }
                            sum[j] = (long) sum[j] + Long.parseLong(val);
                        } else if (cellData instanceof Double) {
                            String val = cellData.toString();
                            if (isLangBng) {
                                val = StringUtils.convertToEngNumber(val);
                            }
                            sum[j] = (double) sum[j] + Double.parseDouble(val);
                        } else if (Display[j][3].equalsIgnoreCase("double") && cellData instanceof String) {
                            String val = cellData.toString().replaceAll(",", "");
                            if(val.equalsIgnoreCase(""))
                            {
                            	val = "0";
                            }
                            if (isLangBng) {
                                val = StringUtils.convertToEngNumber(val);
                            }
                            double dCellData = Double.parseDouble(val);
                            sum[j] = (double) sum[j] + dCellData;
                        } else if (Display[j][3].equalsIgnoreCase("int") && cellData instanceof String) {
                            String val = cellData.toString().replaceAll(",", "");
                            if(val.equalsIgnoreCase(""))
                            {
                            	val = "0";
                            }
                            if (isLangBng) {
                                val = StringUtils.convertToEngNumber(val);
                            }
                            int iCellData = Integer.parseInt(val);
                            sum[j] = (int) sum[j] + iCellData;
                        }
                    }
                }
                j++;
            }
            i++;
            tableRowCount++;
        }

        //System.out.println("sum size now = " + sum.length);

        row = sheet.createRow(i);

        int j = 0;
        int invisibleCount = 0;
        if (data.size() > 1) {
            List<Object> row1stData = data.get(1);
            cell = row.createCell(j);
            cell.setCellValue(Utils.getDigits(LM.getText(LC.HM_SUM, userDTO), language));
            cell.setCellStyle(get1stRowStyle(wb));
            if (row1stData != null) {
                for (Object cellData : sum) {
                	if(Display[j][3].equalsIgnoreCase("invisible"))
                	{
                		j++;
                		invisibleCount ++;
                		continue;
                	}
                    cell = row.createCell(j);
                    cell.setCellValue(Utils.getDigits(cellData.toString(), language));
                    cell.setCellStyle(get1stRowStyle(wb));
                    j++;
                }
            }
        }

        autoSizeColumns(wb);
        
        
        int mergeUpto = data.get(0).size() - 1 - invisibleCount;
        
        row = sheet.createRow(0);
        cell = row.createCell(0);
        cell.setCellValue(LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, userDTO));
        cell.setCellStyle(getHeaderStyle(wb, 20));

        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, mergeUpto));
        
		/*
		 * row = sheet.createRow(1); cell = row.createCell(0);
		 * cell.setCellValue(LM.getText(LC.HM_PARLIAMENT_ADDRESS, userDTO));
		 * cell.setCellStyle(getHeaderStyle(wb, 16));
		 * 
		 * sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, mergeUpto));
		 * 
		 * row = sheet.createRow(2); cell = row.createCell(0);
		 * cell.setCellValue(LM.getText(LC.HM_PARLIAMENT_PHONE, userDTO));
		 * cell.setCellStyle(getHeaderStyle(wb, 14));
		 * 
		 * sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, mergeUpto));
		 * 
		 * row = sheet.createRow(3); cell = row.createCell(0);
		 * cell.setCellValue(headerStr); cell.setCellStyle(getHeaderStyle(wb, 13));
		 * 
		 * sheet.addMergedRegion(new CellRangeAddress(3, 3, 0, mergeUpto));
		 */
        return wb;
    }


    public void handleReportGet(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, String tableName,
                                int menuConstant, String language, String reportName, String fileName) {
    	
    	Enumeration<String> parameterNames = request.getParameterNames();
    	 
        while (parameterNames.hasMoreElements()) {
 
            String paramName = parameterNames.nextElement();
          
            String[] paramValues = request.getParameterValues(paramName);
            for (int i = 0; i < paramValues.length; i++) {
                String paramValue = paramValues[i];
                
                if(!Utils.isValidReportSearchString(paramName, paramValue))
                {
                	System.out.println("Invalid arguments: paramName = " + paramName + " paramValue = "+ paramValue);
                	paramValues[i] = "";
                	
                }
              
            }
        }

        String actionType = request.getParameter("actionType");
        ApiResponse apiResponse = null;

        String jspName = "pbreport/report.jsp";
        String bodyName = tableName + "/" + tableName + "_Body.jsp";

        int orderByCol = -1;
        boolean isAscending = true;
        boolean isLanguageBangla = "Bangla".equalsIgnoreCase(language);
        if (request.getParameter("orderByCol") != null && !request.getParameter("orderByCol").equalsIgnoreCase("")) {
            orderByCol = Integer.parseInt(request.getParameter("orderByCol"));
        }
        if (request.getParameter("isAscending") != null && !request.getParameter("isAscending").equalsIgnoreCase("")) {
            isAscending = Boolean.parseBoolean(request.getParameter("isAscending"));
        }
        
        if (orderByCol > 0 && orderByCol <= Display.length) {
            int actualColIndex = orderByCol - 1;
            if (Display[actualColIndex][1].length() > 0) {
            	OrderBY = " " + Display[actualColIndex][1] + ".";
            } else {
            	OrderBY = " ";
            }
            OrderBY += Display[actualColIndex][2] + " " + (isAscending ? "asc" : "desc") + " ";
        }
     

        System.out.println("OrderBY = " + OrderBY);

        if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, menuConstant)) {

            if (ActionTypeConstant.REPORT_PAGE.equals(actionType)) {

                try {
                    request.setAttribute("tableName", tableName);
                    request.setAttribute("reportName", reportName);
                    request.setAttribute("bodyName", bodyName);
                    request.setAttribute("clientSideDataFormatting", clientSideDataFormatting);
                    request.getRequestDispatcher(jspName).forward(request, response);
                } catch (ServletException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                return;

            } else if (ActionTypeConstant.REPORT_COUNT.equals(actionType)) {
                try {
                    int count = getTotalCount(request, response, language);
                    apiResponse = ApiResponse.makeSuccessResponse(count, "Success");
                } catch (Exception ex) {
                    apiResponse = ApiResponse.makeErrorResponse(ex.getMessage());
                    if (ex instanceof RequestFailureException) {
                        throw (RequestFailureException) ex;
                    }
                    throw new RuntimeException(ex);
                }
            } else if (ActionTypeConstant.REPORT_RESULT.equals(actionType)) {
                try {
                    List<List<Object>> list = getReport(request, response, language, true, HTML);
                    Integer recordPerPage = Integer.parseInt(request.getParameter("RECORDS_PER_PAGE"));
                    Integer pageNo = Integer.parseInt(request.getParameter("pageno"));
                    System.out.println("recordPerPage = " + recordPerPage + " pageNo = " + pageNo);
                    int offset = (pageNo - 1) * recordPerPage;
                    addSerialNoColumn(isLanguageBangla, list, userDTO, offset);
                    List<Object> combinedList = new ArrayList<>();
                    combinedList.add(list);
                    int count = list.size() - 1;
                    logger.debug("##reportcount list size found = " + count);
                    if(count >= recordPerPage)
                    {
                    	count = getTotalCount(request, response, language);
                    	logger.debug("##reportcount after 2nd query the count is = " + count);
                    }
                    
                    combinedList.add(count);
                    apiResponse = ApiResponse.makeSuccessResponse(combinedList, "Success");
                } catch (Exception ex) {
                    apiResponse = ApiResponse.makeErrorResponse(ex.getMessage());
                    if (ex instanceof RequestFailureException) {
                        throw (RequestFailureException) ex;
                    }
                    throw new RuntimeException(ex);
                }
            } else if ("xl".equals(actionType)) {
                try {

                    String file_name = fileName + ".xlsx";
                    response.setContentType("application/vnd.ms-excel");
                    response.setHeader("Content-Disposition", "attachment; filename=" + file_name);

                    List<List<Object>> list = getReport(request, response, language, false, XL);
                    addSerialNoColumn(isLanguageBangla, list, userDTO);
                    String columns = request.getParameter("columns");
                    Set<Integer> columnSet = Stream.of(columns.split(","))
                            .map(String::trim)
                            .map(Utils::convertToInteger)
                            .filter(Objects::nonNull)
                            .collect(Collectors.toSet());
                    List<Integer> sortedList = columnSet.stream()
                            .sorted()
                            .collect(Collectors.toList());
                    String[][] tempDisplay = new String[sortedList.size()][Display[0].length];
                    for (int i = 1; i < sortedList.size(); i++) {
                        System.arraycopy(Display[sortedList.get(i) - 1], 0, tempDisplay[i], 0, Display[0].length);
                    }
                    tempDisplay[0][0] = "display";
                    tempDisplay[0][1] = "";
                    tempDisplay[0][2] = "SL";
                    tempDisplay[0][3] = "text";
                    tempDisplay[0][4] = "";
                    Display = tempDisplay;
                    list = list.stream()
                            .map(ls -> sortedList.stream().map(ls::get).collect(Collectors.toList()))
                            .collect(Collectors.toList());
                    XSSFWorkbook wb = createXl(list, reportName, request, language, userDTO);
                    wb.write(response.getOutputStream()); // Write workbook to response.
                    wb.close();

                    apiResponse = ApiResponse.makeSuccessResponse(list, "Success");
                } catch (Exception ex) {
                    apiResponse = ApiResponse.makeErrorResponse(ex.getMessage());

                    ex.printStackTrace();
                }
            } else if (ActionTypeConstant.REPORT_TEMPLATE.equals(actionType)) {

                apiResponse = ApiResponse.makeSuccessResponse(reportTemplate, "Success");

            }
            else
			{
				try {
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				} catch (ServletException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}


            if (!"xl".equals(actionType)) {
                PrintWriter writer;
                try {
                    writer = response.getWriter();
                    writer.write(new Gson().toJson(apiResponse));
                    writer.flush();
                    writer.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }


        }
        else
		{
			try {
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

    }
    
    private void addSerialNoColumn(boolean isLanguageBangla, List<List<Object>> list, UserDTO userDTO) {
    	addSerialNoColumn(isLanguageBangla, list, userDTO, 0);
    }

    private void addSerialNoColumn(boolean isLanguageBangla, List<List<Object>> list, UserDTO userDTO, int offset) {
    	if(list.get(0) != null)
    	{
    		list.get(0).add(0, LM.getText(LC.HM_SL, userDTO));
    	}
        
        for (int i = 1; i < list.size(); i++) {
            list.get(i).add(0, isLanguageBangla ? StringUtils.convertToBanNumber(String.valueOf(i + offset)) : (i + offset));
        }
    }

}
