package util;

import java.util.ArrayList;
import java.util.List;

public class SqlPair {
	public String sql;
	public List<Object> objectList;

	public SqlPair() {
	}

	public SqlPair(String sql) {
		this.sql = sql;
		this.objectList = new ArrayList<>();
	}

	@Override
	public String toString() {
		return "SqlPair{" +
			   "sql='" + sql + '\'' +
			   ", objectList=" + objectList +
			   '}';
	}
}
