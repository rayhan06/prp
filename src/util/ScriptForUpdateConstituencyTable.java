package util;

import common.ConnectionAndStatementUtil;
import election_constituency.Election_constituencyDTO;
import election_constituency.Election_constituencyRepository;
import geolocation.GeoDistrictDTO;
import geolocation.GeoDistrictRepository;
import geolocation.GeoDivisionDTO;
import geolocation.GeoDivisionRepository;
import ministry_office_mapping.Ministry_office_mappingDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class ScriptForUpdateConstituencyTable {


    public static void main(String[] args) {
        List<Election_constituencyDTO> election_constituencyDTOS = Election_constituencyRepository.getInstance().getElection_constituencyList();
        List<GeoDistrictDTO> geoDistrictDTOS = GeoDistrictRepository.getInstance().getAllDistrict();
        String districtStr;
        String singleUpdateString = "update election_constituency set geo_districts_type = ? where ID = ?";

        for (Election_constituencyDTO ecDTO: election_constituencyDTOS) {
            districtStr = ecDTO.constituencyNameEn.split("-")[0];
            for (GeoDistrictDTO gdDTO: geoDistrictDTOS) {
                if (districtStr.equalsIgnoreCase(gdDTO.nameEng)) {
                    ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
                        PreparedStatement ps = (PreparedStatement) model.getStatement();
                        try {
                            ps.setLong(1, gdDTO.id);
                            ps.setLong(2, ecDTO.iD);
                            ps.executeUpdate();
                        } catch (SQLException ex) {
                            System.out.println(ex);
                        }
                    }, singleUpdateString);
                }
            }
        }
    }
}
