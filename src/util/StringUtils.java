package util;

import org.apache.commons.codec.binary.Base64;
import sessionmanager.SessionConstants;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringUtils {
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    private static final char[] isEngDigit = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    private static final char[] isBngDigit = {'০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯'};

    public static Boolean isValidString(String str) {

        return str != null && str.trim().length() > 0;
    }


    public static boolean containsIgnoringCase(String source, String pattern) {
        return toUpperCase(source).contains(toUpperCase(pattern));
    }

    public static boolean isEqualIgnoringCase(String str1, String str2) {
        str1 = toUpperCase(str1);
        str2 = toUpperCase(str2);
        return str1.equals(str2);
    }

    public static boolean isBlank(String str) {
        str = trim(str);
        return str.isEmpty();
    }

    public static boolean isNotBlank(String str) {
        return !isBlank(str);
    }

    public static String toUpperCase(String str) {
        str = trim(str);
        return str.toUpperCase();
    }

    public static boolean isEqual(String str1, String str2) {
        if (str1 == null || str2 == null) {
            return false;
        }
        return str1.equals(str2);
    }

    public static String trim(String str) {
        if (str == null) {
            return "";
        }
        return str.trim();
    }

    public static String getCommaSeparatedString(Collection<? extends Object> collection) {
        StringBuilder stringBuilder = new StringBuilder("(");
        int index = 0;
        for (Iterator<? extends Object> iterator = collection.iterator(); iterator.hasNext(); index++) {
            if (index > 0) {
                stringBuilder.append(",");
            }
            stringBuilder.append(iterator.next());
        }
        stringBuilder.append(")");
        return stringBuilder.toString();
    }

    public static <T> String getCommaSeparatedString(T[] list) {
        StringBuilder stringBuilder = new StringBuilder("(");
        int index = 0;
        for (int i = 0; i < list.length; i++) {
            if (index > 0) {
                stringBuilder.append(",");
            }
            stringBuilder.append(list[i]);
        }
        stringBuilder.append(")");
        return stringBuilder.toString();
    }

    public static String getCommaSeparatedString(long[] list) {
        StringBuilder stringBuilder = new StringBuilder("(");
        int index = 0;
        for (int i = 0; i < list.length; i++) {
            if (index > 0) {
                stringBuilder.append(",");
            }
            stringBuilder.append(list[i]);
        }
        stringBuilder.append(")");
        return stringBuilder.toString();
    }

    public static String getCommaSeparatedString(List<String> list) {
        if (list == null || list.size() == 0) {
            return "";
        }
        return String.join(",", list);
    }

    public static <T> Optional<String> getCommaSeparatedStringFromLongList(List<T> list) {
        if (list == null || list.size() == 0) {
            return Optional.empty();
        }
        list = list.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        if (list.size() == 0) {
            return Optional.empty();
        }
        return Optional.of(
                list.stream()
                        .filter(Objects::nonNull)
                        .map(String::valueOf)
                        .map(e -> "'" + e + "'")
                        .collect(Collectors.joining(", "))
        );
    }

    public static String getCommaSeparatedQuotedString(List<Long> list) {
        if (list == null || list.size() == 0) {
            return "";
        }
        return list.stream()
                .filter(Objects::nonNull)
                .map(String::valueOf)
                .map(str -> "'" + str + "'")
                .collect(Collectors.joining(","));
    }

    public static Integer[] toIntArray(String[] param) {
        return toArray(param, Integer.class);
    }

    public static Double[] toDoubleArray(String[] parameterValues) {
        return toArray(parameterValues, Double.class);
    }

    @SuppressWarnings("unchecked")
    private static <T extends Number> T[] toArray(String[] params, Class<T> clazz) {

        T[] resultList = (T[]) Array.newInstance(clazz, params.length);


        for (int i = 0; i < params.length; i++) {
            try {
                resultList[i] = clazz.getConstructor(String.class).newInstance(params[i]);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        return resultList;
    }

    /**
     * @author dhrubo
     */
    public static List<Long> getLongListFromCommaSeperatedString(String additionalIPs) {
        if (additionalIPs == null) {
            return new ArrayList<>();
        }
        return Arrays.stream(additionalIPs.split(","))
                .map(Long::parseLong)
                .collect(Collectors.toList());
    }


    /**
     * @author Dhrubo
     */
    public static String getBaseUrl(HttpServletRequest request) {
        String scheme = request.getScheme() + "://";
        String serverName = request.getServerName();
        String serverPort = (request.getServerPort() == 80) ? "" : ":" + request.getServerPort();
        String contextPath = request.getContextPath();
        return scheme + serverName + serverPort + contextPath;
    }

    public static String getFormattedDate(String language, long date) {
        return getFormattedDate("English".equalsIgnoreCase(language), date);
    }

    public static String getFormattedDate(boolean isLangEng, long date) {
        String formattedDate = "";
        if (date > SessionConstants.MIN_DATE) {
            formattedDate = simpleDateFormat.format(new Date(date));
            if (!isLangEng) {
                formattedDate = convertToBanNumber(formattedDate);
            }
        }
        return formattedDate;
    }

    public static String getFormattedTime(boolean isLangEng, long timeStamp) {
        String formattedDate = "";
        if (timeStamp > SessionConstants.MIN_DATE) {
            formattedDate = dateTimeFormat.format(new Date(timeStamp));
            if (!isLangEng) {
                formattedDate = convertToBanNumber(formattedDate);
            }
        }
        return formattedDate;
    }

    public static String convertToDateAndTime(String language, long ms) {
        return convertToDateAndTime("English".equalsIgnoreCase(language), ms);
    }

    public static String convertToDateAndTime(boolean isEnglishLanguage, long ms) {
        LocalDateTime ldt = new Date(ms).toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
        int hour = ldt.getHour() > 12 ? ldt.getHour() - 12 : (ldt.getHour() == 0 ? 12 : ldt.getHour());
        String am_pm = ldt.getHour() >= 12 && ldt.getSecond() > 0 ? "PM" : "AM";
        String date = String.format("%02d-%02d-%04d %02d:%02d:%02d %s",
                ldt.getDayOfMonth(), ldt.getMonth().getValue(), ldt.getYear(), hour, ldt.getMinute(), ldt.getSecond(), am_pm);
        return isEnglishLanguage ? date : convertToBanNumber(date);
    }

    public static String getYesNo(String language, boolean flag) {
        if ("English".equalsIgnoreCase(language)) {
            return flag ? "Yes" : "No";
        } else {
            return flag ? "হ্যাঁ" : "না";
        }
    }

    public static String getYesNo(boolean isLang, boolean flag) {
        if (isLang) {
            return flag ? "Yes" : "No";
        } else {
            return flag ? "হ্যাঁ" : "না";
        }
    }

    public static String convertIntToString(String language, int value) {
        return value == 0 ? "" : convertBanglaIfLanguageIsBangla(language, String.valueOf(value));
    }

    public static String convertLongToString(String language, long value) {
        return value == 0 ? "" : convertBanglaIfLanguageIsBangla(language, String.valueOf(value));
    }

    public static String convertDoubleToString(String language, double value) {
        return value == 0 ? "" : convertBanglaIfLanguageIsBangla(language, String.valueOf(value));
    }

    public static String convertBanglaIfLanguageIsBangla(String language, String value) {
        return value == null ? "" : ("English".equalsIgnoreCase(language) ? value : convertToBanNumber(value));
    }

    public static String convertToBanNumber(String str) {
        if (str == null) {
            return "";
        }
        str = str.replaceAll("0", "০");
        str = str.replaceAll("1", "১");
        str = str.replaceAll("2", "২");
        str = str.replaceAll("3", "৩");
        str = str.replaceAll("4", "৪");
        str = str.replaceAll("5", "৫");
        str = str.replaceAll("6", "৬");
        str = str.replaceAll("7", "৭");
        str = str.replaceAll("8", "৮");
        str = str.replaceAll("9", "৯");
        return str;
    }

    public static String convertToEngNumber(String str) {
        if (str == null) {
            return null;
        }
        return str.chars()
                .mapToObj(c -> String.valueOf(convertToEngNum((char) c)))
                .collect(Collectors.joining());
    }

    public static char convertToEngNum(char ch) {
        switch (ch) {
            case '০':
                return '0';
            case '১':
                return '1';
            case '২':
                return '2';
            case '৩':
                return '3';
            case '৪':
                return '4';
            case '৫':
                return '5';
            case '৬':
                return '6';
            case '৭':
                return '7';
            case '৮':
                return '8';
            case '৯':
                return '9';
        }
        return ch;
    }

    public static String convertToBanglaMonth(String month) {
        if (month == null) return null;
        switch (month.trim().toUpperCase()) {
            case "JANUARY":
                return "জানুয়ারি";
            case "FEBRUARY":
                return "ফেব্রুয়ারি";
            case "MARCH":
                return "মার্চ";
            case "APRIL":
                return "এপ্রিল";
            case "MAY":
                return "মে";
            case "JUNE":
                return "জুন";
            case "JULY":
                return "জুলাই";
            case "AUGUST":
                return "আগস্ট";
            case "SEPTEMBER":
                return "সেপ্টম্বর";
            case "OCTOBER":
                return "অক্টোবর";
            case "NOVEMBER":
                return "নভেম্বর";
            case "DECEMBER":
                return "ডিসেম্বর";
            default:
                return month;
        }
    }

    public static String getPlaceholderForInQuery(String value) {
        int numberOfValues = value.split(",").length;
        return Stream.generate(() -> "?")
                .limit(numberOfValues)
                .collect(Collectors.joining(",", "(", ")"));
    }

    public static boolean isEngNumber(String str) {
    	
    	if (str == null) {
            return false;
        }
        try {
            long l = Long.parseLong(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;      
    }

    public static Integer parseNullableInteger(String s) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    public static Long parseNullableLong(String s) {
        if (s == null) return null;
        s = s.trim();
        if (s.isEmpty()) return null;
        try {
            return Long.parseLong(s);
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    public static String getBase64EncodedImageStr(byte[] photoBytes) {
        return "data:image/jpg;base64,"
                .concat(photoBytes != null ? new String(Base64.encodeBase64(photoBytes)) : "");
    }
}
