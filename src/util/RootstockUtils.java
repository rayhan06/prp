package util;
import java.io.*;

//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;
import project_tracker.FilesDAO;
import project_tracker.FilesDTO;

public class RootstockUtils {
	private static final Logger logger = Logger.getLogger(RootstockUtils.class);
	
//	 public static byte[] readFile(String file) 
//	 {
//	        ByteArrayOutputStream bos = null;
//	        try {
//	            File f = new File(file);
//	            FileInputStream fis = new FileInputStream(f);
//	            byte[] buffer = new byte[1024];
//	            bos = new ByteArrayOutputStream();
//	            for (int len; (len = fis.read(buffer)) != -1;) {
//	                bos.write(buffer, 0, len);
//	            }
//	        } catch (FileNotFoundException e) {
//	            System.err.println(e.getMessage());
//	        } catch (IOException e2) {
//	            System.err.println(e2.getMessage());
//	        }
//	        return bos != null ? bos.toByteArray() : null;
//	 }
	
	
	public static void ProcessFile(String fileId,HttpServletRequest request, HttpServletResponse response)
	{
		
		
		long tempId = Long.parseLong(fileId);
		FilesDAO tempDao = new FilesDAO();
		
		FilesDTO tempObj = tempDao.getFileByID(tempId);
				
		
				String mimeType = "application/octet-stream";              
                 
                // set content properties and header attributes for the response
                response.setContentType(mimeType);
                response.setContentLength(tempObj.fileData.length);
                String headerKey = "Content-Disposition";
                String headerValue = String.format("attachment; filename=\"%s\"", tempObj.fileTitle);
                response.setHeader(headerKey, headerValue);
 
                try
                {
                // writes the file to the client
                OutputStream outStream = response.getOutputStream();
                
                outStream.write(tempObj.fileData);                 

                outStream.close();    
                
                }
                catch(Exception ex)
                {
                	
                }
			
	}

	 
	  public static String generateRandomHexToken(int byteLength) {
	        SecureRandom secureRandom = new SecureRandom();
	        byte[] token = new byte[byteLength];
	        secureRandom.nextBytes(token);
	        return new BigInteger(1, token).toString(16); //hex encoding
	    }
	  
	  public static String getFileExtension(String file) {
	        String fileName = file;
	        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
	        return fileName.substring(fileName.lastIndexOf(".")+1);
	        else return "";
	    }
	  
	  
	  public static Part uploadFileByIndex2(String fieldName, String path, int index, HttpServletRequest request)
		{
			int j = 0;
			String uploadedFileName = "";
			try 
			{
				for (Part part : request.getParts()) 
				{
					String fileName = part.getName();//part.getContentType()
					String name = part.getName();
					if(name.equalsIgnoreCase(fieldName))
					{
						if(j == index)
						{
//							if(fileName!= null && !fileName.equalsIgnoreCase(""))
//							{
//								fileName = Jsoup.clean(fileName,Whitelist.simpleText());
//								uploadedFileName = name + "_" + index + "_" + fileName;
//								System.out.println("... UPLOADING " + fileName + ", j = " + j + " name = " + name);
//								Utils.uploadFile(part, uploadedFileName, path);
//							}
							
							
							
							return part;
						}
						
						j ++;
					}

				}
			} catch (IOException | ServletException e) {
				// TODO Auto-generated catch block
				logger.error("",e);
			}
			return null;
		}
	  
	  public static byte[] uploadFileToByteAray(Part filePart)
		{

		    OutputStream out = null;
		    InputStream filecontent = null;

		    try 
			{		        
		        filecontent = filePart.getInputStream();
		        return inputStreamToByteArray(filecontent);

		    }
			catch (IOException fne) 
			{
		    	System.out.println("You either did not specify a file to upload or are "
		                + "trying to upload a file to a protected or nonexistent "
		                + "location.");
		    	System.out.println("ERROR: " + fne.getMessage());
		    }
			finally 
			{
		        if (out != null) 
				{
		            try 
					{
						out.close();
					}
					catch (IOException e) 
					{
						logger.error("",e);
					}
		        }
		        if (filecontent != null) 
				{
		            try 
					{
						filecontent.close();
					}
					catch (IOException e) 
					{
						logger.error("",e);
					}
		        }	       
		    }
		    return null;
		}
	  
	  public static byte[] inputStreamToByteArray(InputStream is)
		 {
			 if(is == null)
			 {
				 return null;
			 }
		    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			int nRead;
			byte[] data = new byte[1024];
			try {
				while ((nRead = is.read(data, 0, data.length)) != -1) {
				    buffer.write(data, 0, nRead);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
					logger.error("",e);
				}
			 
			    try {
					buffer.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
				logger.error("",e);
			}
			byte[] byteArray = buffer.toByteArray();
			System.out.println("We got a byte array of the size:" + byteArray.length);
			return byteArray;

		 }
		 

}
