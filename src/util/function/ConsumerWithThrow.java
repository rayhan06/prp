package util.function;

/*
 * @author Md. Erfan Hossain
 * @created 08/06/2022 - 8:00 PM
 * @project parliament
 */

@FunctionalInterface
public interface ConsumerWithThrow<T> {
    void accept(T t) throws Exception;
}
