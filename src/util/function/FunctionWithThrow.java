package util.function;

/*
 * @author Md. Erfan Hossain
 * @created 08/06/2022 - 8:01 PM
 * @project parliament
 */

@FunctionalInterface
public interface FunctionWithThrow<T,R,E extends Exception> {
    R apply(T t) throws E;
}
