package util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Kayesh Parvez
 *
 */
public class CommonConstant {
	public static final int Language_ID_English = 1;
	public static final int Language_ID_Bangla = 2;
	
	public static final int SERVICE_TYPE_PUBLIC_QUARRY = 3;
	public static final int SERVICE_TYPE_PRIVATE_QUARRY = 4;
	public static final int SERVICE_TYPE_EXPLORATION = 1;
	public static final int SERVICE_TYPE_MINE = 2;
	
	public static final int COMMITTEE_TYPE_DCM = 1;
	public static final int COMMITTEE_TYPE_TENDER_OPENING = 4;
	public static final int COMMITTEE_TYPE_VALUE_FIXATION = 3;
	public static final int COMMITTEE_TYPE_DEMARCATION = 2;

	public static final int DISTRIBUTION_STATE_ID = 35;
	public static final int DISTRIBUTION_FINISH_STATE_ID = 36;

	public static final int EDMS_DOCTYPE_RFI = 2100;


    public static final long EDMS_DOC_DEADLINE_OFFSET = 21*24*60*60*1000;
    public static final String EDMS_DEADLINE_DATE_FORMAT = "dd/MM/yyyy";

    public static final String EDMS_CATEGORY_DOMAIN_DOC_PROCESS = "document_process";
	public static final String EDMS_CATEGORY_DOMAIN_DOC_TYPE = "document_type";

	public static final String NCN_ISSUED_TO_OFC_TYPE = "issued_to_ofc_type";
	public static final String NCN_ISSUED_TO_PROCESS = "issued_to_process";

	public static final Integer DOC_INCOMING = 0;
	public static final Integer DOC_OUTGOING = 1;

	public static final String INCOMING_DOC_TYPE_OPTIONS = "<option value=\"-1\" selected=\"selected\"> Choose Dcoument Type </option><option value=\"1\">Letter</option><option value=\"2\">RFI</option>";
	public static final String OUTGOING_DOC_TYPE_OPTIONS =
			"\t<option value=\"-1\" selected=\"selected\"> Choose Dcoument Type </option>\t\t\t\t\t\n" +
			"\t<option value=\"1\">Letter</option>\n" +
			"\t<option value=\"3\">QSDR</option>\n" +
			"\t<option value=\"4\">NCN</option>\n" +
			"\t<option value=\"5\">HSE</option>";

	public static final Integer DOC_SOURCE_CREC = 100;
	public static final Integer DOC_SOURCE_BR = 200;
	public static final Integer DOC_SOURCE_BUET = 201;
	public static final Integer DOC_SOURCE_OTHER = 202;

	public static final Integer DOC_TYPE_DRAWING = 0;
	public static final Integer DOC_TYPE_LETTER = 1;
	public static final Integer DOC_TYPE_RFI = 2;
	public static final Integer DOC_TYPE_QSDR = 3;
	public static final Integer DOC_TYPE_NCN = 4;
	public static final Integer DOC_TYPE_HSE = 5;
	public static final Integer DOC_TYPE_NCR = 6;

	public static final String FILE_TAG = "##FileTag##";
	public static final String NAME_INITIAL = "##NameInitial##";
	public static final String INPUT_ID_SECTION = "##input:section##";

	public static final Integer STATUS_OPEN = 6;
	public static final Integer STATUS_CLOSE = 7;

	public static final Integer DEVELOPMENT_PARTNER_ROLE_ID = 9801;

	//SMS
    public static final int SMS_JOB_ID_UPDATE_BATCH_SIZE = 2;
	public static final long SMS_QUEUE_POPULATOR_DELAY_SECOND = 30;
    public static final long JOB_ID_UPDATE_THRESHOLD_TIME_MILLISECOND = 20;
    public static final long DEFAULT_OFFICE_ID = 2294;
    public static final long DEFAULT_OFFICE_ORIGIN_UNIT_ORGANOGRAM_ID = 1;
    public static final long DEFAULT_OFFICE_ORIGIN_ID = 223;
    public static final long DEFAULT_ROLE_TYPE = 8005;

    public static Map<Long, Integer> officeUnitPbrlpContractorMap = new HashMap<>();

	static{

		officeUnitPbrlpContractorMap.put( 57250L, 100 );
		officeUnitPbrlpContractorMap.put( 57251L, 200 );
	}
}
