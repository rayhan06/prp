package util;

/*
 * @author Md. Erfan Hossain
 * @created 12/12/2021 - 2:47 PM
 * @project parliament
 */

public class StringWrapper {
    private String value;

    public StringWrapper(String value) {
        this.value = value;
    }

    public StringWrapper() {
        this.value = null;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
