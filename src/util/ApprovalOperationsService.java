package util;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import approval_execution_table.Approval_execution_tableDTO;
import approval_path.ApprovalNotificationRecipientsDAO;
import approval_path.ApprovalNotificationRecipientsDTO;
import approval_path.ApprovalPathDetailsDAO;
import approval_path.ApprovalPathDetailsDTO;
import approval_summary.Approval_summaryDAO;
import approval_summary.Approval_summaryDTO;
import common.ConnectionAndStatementUtil;
import edms_documents.Edms_documentsDAO;
import edms_documents.Edms_documentsDTO;
import holidays.CalenderUtil;
import inbox.InboxDAO;
import inbox.InboxDTO;
import inbox_movements.Inbox_movementsDAO;
import inbox_movements.Inbox_movementsDTO;
import pb.Utils;
import sessionmanager.SessionConstants;
import user.UserDTO;
import workflow.WorkflowController;

public class ApprovalOperationsService {
	
	public String tableName = "";
	public NavigationService4 ns4;
	public Logger logger;
	public ApprovalOperationsService(NavigationService4 ns4)
	{
		this.ns4 = ns4;
		this.tableName = ns4.tableName;
		this.logger = NavigationService4.logger;
	}
		
	
    public void addToApprovalInbox(long approval_summary_ID, UserDTO userDTO, String details, long toOrganogramID,
            long approval_execution_id) 
    {

		long lastModificationTime = System.currentTimeMillis();
		
		InboxDAO inboxDao = new InboxDAO();
		InboxDTO inboxDTO = new InboxDTO();
		
		inboxDTO.organogramId = toOrganogramID;
		inboxDTO.userId = userDTO.ID;
		inboxDTO.submissionDate = lastModificationTime;
		inboxDTO.messageType = SessionConstants.INBOX_TYPE_APPROVAL_PATH;
		inboxDTO.subject = tableName;
		inboxDTO.trackingNumber = "";
		inboxDTO.details = details;
		inboxDTO.inboxOriginatorId = approval_summary_ID;
		inboxDTO.taskDecision = "Add Edms Doc";
		inboxDTO.taskComments = "Add Edms Doc";
		inboxDTO.createdAt = lastModificationTime;
		inboxDTO.createdBy = approval_execution_id;
		inboxDTO.currentStatus = CommonDTO.WAITING_FOR_APPROVAL;
		
		long inboxID = -1;
		try {
		
			inboxID = inboxDao.add(inboxDTO);
		} catch (Exception e1) {
		
			e1.printStackTrace();
		}
	
		// add to inbox movements
	
		addToApprovalInboxMovements(approval_summary_ID, userDTO, toOrganogramID, approval_execution_id, inboxID);
	}
    

    public void addToApprovalInboxMovements(long approval_summary_ID, UserDTO userDTO, long toOrganogramID,
                                            long approval_execution_id, long inboxID) {
        long lastModificationTime = System.currentTimeMillis();

        Inbox_movementsDAO inboxMovementDao = new Inbox_movementsDAO();
        Inbox_movementsDTO inboxMovementDTO = new Inbox_movementsDTO();

        inboxMovementDTO.inboxId = inboxID;
        inboxMovementDTO.fromOrganogramId = userDTO.organogramID;
        inboxMovementDTO.toOrganogramId = toOrganogramID;
        inboxMovementDTO.inbox_type = SessionConstants.INBOX_TYPE_APPROVAL_PATH;
        inboxMovementDTO.fromEmployeeUserId = userDTO.ID;
        inboxMovementDTO.createdAt = lastModificationTime;
        inboxMovementDTO.createdBy = approval_execution_id;
        inboxMovementDTO.currentStatus = CommonDTO.WAITING_FOR_APPROVAL;
        inboxMovementDTO.isSeen = false;

        try {

            inboxMovementDao.add(inboxMovementDTO);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }
    
	public long firstTimeAddToApprovalPath(CommonDTO commonDTO, int operationOrAction, long id, UserDTO userDTO,
			String ip) {
		long idToReturn = -1;
		Approval_execution_tableDTO approval_execution_tableDTO = new Approval_execution_tableDTO();
		approval_execution_tableDTO.tableName = tableName;

		approval_execution_tableDTO.remarks = commonDTO.remarks;
		approval_execution_tableDTO.fileDropzone = commonDTO.fileID;

		approval_execution_tableDTO.operation = operationOrAction;
		approval_execution_tableDTO.action = operationOrAction;
		approval_execution_tableDTO.userId = userDTO.ID;
		approval_execution_tableDTO.organogramID = userDTO.organogramID;
		approval_execution_tableDTO.userIp = ip;

		approval_execution_tableDTO.approvalStatusCat = Approval_execution_tableDTO.PENDING;

		commonDTO.isDeleted = CommonDTO.WAITING_FOR_APPROVAL;
		if (operationOrAction == SessionConstants.UPDATE || operationOrAction == SessionConstants.DELETE) {
			approval_execution_tableDTO.previousRowId = commonDTO.iD;
		}

		try {
			if (operationOrAction != SessionConstants.INITIATE) {
				approval_execution_tableDTO.updatedRowId = ns4.add(commonDTO);
			} else {
				approval_execution_tableDTO.updatedRowId = id;
				addInitiator(id, userDTO.ID);
			}
			if (operationOrAction == SessionConstants.INSERT || operationOrAction == SessionConstants.INITIATE) {
				approval_execution_tableDTO.previousRowId = approval_execution_tableDTO.updatedRowId;
			}
			approval_execution_tableDTO.approvalPathOrder = 0;
			approval_execution_tableDTO.approvalPathId = WorkflowController
					.getApprovalPathIDFromJobCatandOfficeID(commonDTO.jobCat, userDTO);
			ns4.setIsDeleted(approval_execution_tableDTO.updatedRowId, CommonDTO.WAITING_FOR_APPROVAL); // may not be
			// necessary, still,
			// safety check
			idToReturn = approval_execution_tableDTO.previousRowId;

			long approval_execution_tableDTOID = ns4.approval_execution_tableDAO.add(approval_execution_tableDTO);

			updateAssignedToAndAddNotification((Approval_execution_tableDTO) ns4.approval_execution_tableDAO
					.getDTOByID(approval_execution_tableDTOID));
			updateStatusInSummaryTable(approval_execution_tableDTO.updatedRowId,
					approval_execution_tableDTO.approvalStatusCat);

			Approval_summaryDAO approval_summaryDAO = new Approval_summaryDAO();
			Approval_summaryDTO approval_summaryDTO = approval_summaryDAO.getDTOByTableNameAndTableID(tableName,
					approval_execution_tableDTO.previousRowId);

			addToApprovalInbox(approval_summaryDTO.iD, userDTO, commonDTO.subject, userDTO.organogramID,
					approval_execution_tableDTOID); // Initiator
			addToApprovalInbox(approval_summaryDTO.iD, userDTO, commonDTO.subject, approval_summaryDTO.assignedTo,
					approval_execution_tableDTOID); // 1st Approver
		} catch (Exception e) {

			e.printStackTrace();
		}

		return idToReturn;
	}
    
    

    public void addToApprovalInboxMovements(long approvalSummaryID, UserDTO userDTO, long approval_execution_id) {
        Approval_summaryDAO approval_summaryDAO = new Approval_summaryDAO();
        Approval_summaryDTO approval_summaryDTO = (Approval_summaryDTO) approval_summaryDAO
                .getDTOByID(approvalSummaryID);

        InboxDAO inboxDao = new InboxDAO();
        InboxDTO inboxDTO = inboxDao.getDTOBySubjectAndOriginator(tableName, approval_summaryDTO.iD);

        logger.debug(" Inbox id = " + inboxDTO.iD);

        long lastModificationTime = System.currentTimeMillis();

        Inbox_movementsDAO inboxMovementDao = new Inbox_movementsDAO();
        Inbox_movementsDTO inboxMovementDTO = new Inbox_movementsDTO();

        inboxMovementDTO.inboxId = inboxDTO.iD;
        inboxMovementDTO.fromOrganogramId = userDTO.organogramID;
        inboxMovementDTO.toOrganogramId = approval_summaryDTO.assignedTo;
        inboxMovementDTO.inbox_type = SessionConstants.INBOX_TYPE_APPROVAL_PATH;
        inboxMovementDTO.fromEmployeeUserId = userDTO.ID;
        inboxMovementDTO.createdAt = lastModificationTime;
        inboxMovementDTO.createdBy = approval_execution_id;
        inboxMovementDTO.currentStatus = approval_summaryDTO.approvalStatusCat;
        inboxMovementDTO.isSeen = false;

        try {

            inboxMovementDao.add(inboxMovementDTO);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public void updateApprovalInboxMovements(long approvalSummaryID, UserDTO userDTO, long approval_execution_id) {
        Approval_summaryDAO approval_summaryDAO = new Approval_summaryDAO();
        Approval_summaryDTO approval_summaryDTO = (Approval_summaryDTO) approval_summaryDAO
                .getDTOByID(approvalSummaryID);

        InboxDAO inboxDao = new InboxDAO();
        InboxDTO inboxDTO = inboxDao.getDTOBySubjectAndOriginator(tableName, approval_summaryDTO.iD);

        Inbox_movementsDAO inbox_movementsDAO = new Inbox_movementsDAO();
        inbox_movementsDAO.updateInboxMovementsSetCreatedBy(inboxDTO.iD, approval_execution_id);

    }
	
	public long approve(int operationOrAction, long id, UserDTO userDTO, String ip,
                        int userApprovalOrder, int maxApprovalOrder, Approval_execution_tableDTO approval_execution_tableDTO,
                        Approval_summaryDTO approval_summaryDTO) {
        long idToReturn = -1;

        if (userApprovalOrder != approval_execution_tableDTO.approvalPathOrder) {
            logger.debug("Not permitted");
            return -1;
        }
        if (userApprovalOrder < maxApprovalOrder) {
            approval_execution_tableDTO.approvalPathOrder++;

            long approval_execution_tableDTOID = ns4.approval_execution_tableDAO.add(approval_execution_tableDTO);
            try {
                updateAssignedToAndAddNotification((Approval_execution_tableDTO) ns4.approval_execution_tableDAO
                        .getDTOByID(approval_execution_tableDTOID));
                addToApprovalInboxMovements(approval_summaryDTO.iD, userDTO, approval_execution_tableDTOID);
            } catch (Exception e) {

                e.printStackTrace();
            }
        } else {
            try {
                if (approval_execution_tableDTO.operation == SessionConstants.INSERT
                        || approval_execution_tableDTO.operation == SessionConstants.INITIATE) {
                    logger.debug("setIsDeleted 0");
                    ns4.setIsDeleted(approval_execution_tableDTO.updatedRowId, CommonDTO.ACTIVE);
                } else if (approval_execution_tableDTO.operation == SessionConstants.DELETE) {
                    ns4.deleteByPreviousRowId(approval_execution_tableDTO.previousRowId);
                    ns4.delete(approval_execution_tableDTO.previousRowId);
                } else if (approval_execution_tableDTO.operation == SessionConstants.UPDATE) {
                    CommonDTO commonDTO = ns4.getDTOByID(approval_execution_tableDTO.updatedRowId);
                    commonDTO.iD = approval_execution_tableDTO.previousRowId;
                    ns4.update(commonDTO);
                    ns4.deleteByPreviousRowId(approval_execution_tableDTO.previousRowId);
                }

                approval_execution_tableDTO.approvalPathOrder++;
                approval_execution_tableDTO.approvalStatusCat = Approval_execution_tableDTO.APPROVED;
                long approval_execution_tableDTOID = ns4.approval_execution_tableDAO.add(approval_execution_tableDTO);
                updateStatusInSummaryTable(approval_execution_tableDTO.previousRowId,
                        approval_execution_tableDTO.approvalStatusCat);
                updateAssignedToAndAddFinalNotification(approval_execution_tableDTO, "Approved");
                addToApprovalInboxMovements(approval_summaryDTO.iD, userDTO, approval_execution_tableDTOID);

            } catch (Exception e) {

                e.printStackTrace();
            }
        }

        return idToReturn;
    }

    public long reject(CommonDTO commonDTO, int operationOrAction, long id, UserDTO userDTO, String ip,
                       int userApprovalOrder, int maxApprovalOrder, Approval_execution_tableDTO approval_execution_tableDTO,
                       Approval_summaryDTO approval_summaryDTO) {
        long idToReturn = -1;

        if (userApprovalOrder != approval_execution_tableDTO.approvalPathOrder) {
            logger.debug("Not permitted");
            return -1;
        }
        approval_execution_tableDTO.approvalPathOrder = commonDTO.rejectTo;
        logger.debug("Reject2 = " + commonDTO.rejectTo);
        if (approval_execution_tableDTO.approvalPathOrder >= 0) {

            long approval_execution_tableDTOID = ns4.approval_execution_tableDAO.add(approval_execution_tableDTO);
            try {
                updateAssignedToAndAddNotification((Approval_execution_tableDTO) ns4.approval_execution_tableDAO
                        .getDTOByID(approval_execution_tableDTOID));
                addToApprovalInboxMovements(approval_summaryDTO.iD, userDTO, approval_execution_tableDTOID);
            } catch (Exception e) {

                e.printStackTrace();
            }
        } else {
            try {
                if (approval_execution_tableDTO.operation != SessionConstants.INITIATE) {
                    ns4.deleteByPreviousRowId(approval_execution_tableDTO.previousRowId);
                }

                approval_execution_tableDTO.approvalStatusCat = Approval_execution_tableDTO.REJECTED;
                if (approval_execution_tableDTO.operation == SessionConstants.INITIATE) {
                    updateStatusInSummaryTable(approval_execution_tableDTO.previousRowId,
                            approval_execution_tableDTO.approvalStatusCat);
                    updateAssignedToAndAddFinalNotification(approval_execution_tableDTO, "Rejected");
                }
                long approval_execution_tableDTOID = ns4.approval_execution_tableDAO.add(approval_execution_tableDTO);
                addToApprovalInboxMovements(approval_summaryDTO.iD, userDTO, approval_execution_tableDTOID);
            } catch (Exception e) {

                e.printStackTrace();
            }
        }

        return idToReturn;
    }

    public long validate(CommonDTO commonDTO, int operationOrAction, long id, UserDTO userDTO, String ip,
                         int userApprovalOrder, int maxApprovalOrder, Approval_execution_tableDTO approval_execution_tableDTO,
                         Approval_summaryDTO approval_summaryDTO) {
        long idToReturn = -1;

        if (userApprovalOrder != approval_execution_tableDTO.approvalPathOrder) {
            logger.debug("Not permitted");
            return -1;
        }
        try {

            approval_execution_tableDTO.updatedRowId = commonDTO.iD;

            long approval_execution_tableDTOID = ns4.approval_execution_tableDAO.add(approval_execution_tableDTO);
            idToReturn = approval_execution_tableDTO.updatedRowId;
            updateApprovalInboxMovements(approval_summaryDTO.iD, userDTO, approval_execution_tableDTOID);
        } catch (Exception e) {

            e.printStackTrace();
        }

        return idToReturn;
    }

    public long terminate(CommonDTO commonDTO, int operationOrAction, long id, UserDTO userDTO, String ip,
                          int userApprovalOrder, int maxApprovalOrder, Approval_execution_tableDTO approval_execution_tableDTO,
                          Approval_summaryDTO approval_summaryDTO) {
        long idToReturn = -1;

        logger.debug("terminate");

        if (!WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId,
                userDTO.organogramID)) {
            logger.debug("Not permitted");
            return -1;
        }
        try {
            logger.debug("terminate2");
            ns4.setIsDeleted(approval_execution_tableDTO.updatedRowId, CommonDTO.OUTDATED);
            removeFromApprovalPath(approval_execution_tableDTO.previousRowId);

            approval_execution_tableDTO.approvalPathOrder = -1;
            approval_execution_tableDTO.approvalStatusCat = Approval_execution_tableDTO.TERMINATED;

            long approval_execution_tableDTOID = ns4.approval_execution_tableDAO.add(approval_execution_tableDTO);
            idToReturn = approval_execution_tableDTO.updatedRowId;
            updateStatusInSummaryTable(approval_execution_tableDTO.previousRowId,
                    approval_execution_tableDTO.approvalStatusCat);
            updateAssignedTo(approval_execution_tableDTO.previousRowId);
            addToApprovalInboxMovements(approval_summaryDTO.iD, userDTO, approval_execution_tableDTOID);
        } catch (Exception e) {

            e.printStackTrace();
        }

        return idToReturn;
    }

    public long skip_step(CommonDTO commonDTO, int operationOrAction, long id, UserDTO userDTO, String ip,
                          int userApprovalOrder, int maxApprovalOrder, Approval_execution_tableDTO approval_execution_tableDTO,
                          Approval_summaryDTO approval_summaryDTO) {
        long idToReturn = -1;

        if (!WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId,
                userDTO.organogramID)) {
            logger.debug("Not permitted");
            return -1;
        }
        if (approval_execution_tableDTO.approvalPathOrder < maxApprovalOrder) {
            approval_execution_tableDTO.approvalPathOrder++;
            long approval_execution_tableDTOID = ns4.approval_execution_tableDAO.add(approval_execution_tableDTO);
            try {
                updateAssignedToAndAddNotification((Approval_execution_tableDTO) ns4.approval_execution_tableDAO
                        .getDTOByID(approval_execution_tableDTOID));
            } catch (Exception e) {

                e.printStackTrace();
            }
        } else {
            idToReturn = terminate(commonDTO, operationOrAction, id, userDTO, ip, userApprovalOrder, maxApprovalOrder,
                    approval_execution_tableDTO, approval_summaryDTO);

        }

        return idToReturn;
    }
    
    public long addInitiator(long id, long initiator) {
        Approval_summaryDAO approval_summaryDAO = new Approval_summaryDAO();
        Approval_summaryDTO approval_summaryDTO = new Approval_summaryDTO();
        approval_summaryDTO.tableName = tableName;
        approval_summaryDTO.tableId = id;
        approval_summaryDTO.initiator = initiator;
        approval_summaryDTO.dateOfInitiation = System.currentTimeMillis();
        try {
            return approval_summaryDAO.add(approval_summaryDTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public long updateAssignedTo(long id, long organogramID) {
        long lastModificationTime = System.currentTimeMillis();
        String sql = "UPDATE approval_summary " + " SET assigned_to = " + organogramID + ", " + "lastModificationTime=" + lastModificationTime
                + " WHERE table_id = " + id + " and table_name = '" + tableName + "'";
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            Connection connection = model.getConnection();
            Statement stmt = model.getStatement();
            try {
                logger.debug(sql);
                stmt.execute(sql);
                ns4.recordUpdateTime(connection, lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
        return organogramID;
    }

    public long addApprovalNotifications(long id, long organogramID, final String message, Date date,
                                         long notificationToDelete, int sendSMS, long approvalPathId) {
        StringBuilder URLprefix = new StringBuilder(NavigationService4.context);
        String tableNameUp = tableName.substring(0, 1).toUpperCase() + tableName.substring(1);
        String URL = tableNameUp + "Servlet" + "?actionType=viewApprovalNotification&isPermanentTable=false&updatedRowID=" + id
                + "&notificationToDelete=" + notificationToDelete;

        String[] URLprefixes = URLprefix.toString().split("/");
        URLprefix = new StringBuilder();
        int i;
        for (i = 0; i < URLprefixes.length; i++) {
            if (i < URLprefixes.length - 1) {
                URLprefix.append(URLprefixes[i]).append("/");
            }
        }

        logger.debug("URLprefix = " + URLprefix);
        
        String tableNameUpper = Utils.capitalizeFirstLettersOfWords(tableName.replaceAll("_", " "));

        String smsContentNormal = "You have a/an " + tableNameUpper
                + " approval pending at EDMS system. Please login to take necessary action.\r\n"
                + "URL: https://edms.pbrlp.gov.bd/\r\n" + "Thank you.";

        String smsContentApproved = "A/an " + tableNameUpper
                + " document is Approved. Please login to EDMS to view it.\r\n" + "URL: https://edms.pbrlp.gov.bd/";

        String emailContentNormal = "Dear Concern,\r\n" + "One "
                + tableNameUpper
                + " document is pending for your kind Approval, Please login to EDMS to approve or click on the below URL:\r\n"
                + URLprefix + URL + "\r\n" + "\r\n" + "This is an auto-generated e-mail, please do not reply to this.";

        String emailApproved = "Dear Concern,\r\n" + "One " + tableNameUpper
                + " document is approved, Please login to EDMS to approve or click on the below URL:\r\n" + URLprefix
                + URL + "\r\n" + "\r\n" + "This is an auto-generated e-mail, please do not reply to this.";

        if (message.equalsIgnoreCase("approved") || message.equalsIgnoreCase("rejected")) {
            smsContentNormal = "Your " + tableNameUpper
                    + " document is finally " + message + ". Please login to EDMS to view it.\r\n"
                    + "URL: https://edms.pbrlp.gov.bd/";

            emailContentNormal = "Your " + tableNameUpper
                    + " document is finally " + message + ". Please login to EDMS to view it.\r\n" + URLprefix + URL
                    + "\r\n" + "\r\n" + "This is an auto-generated e-mail, please do not reply to this.";
        }

        String notiMessage = message + " for " + tableNameUpper;

        if (tableName.equalsIgnoreCase("edms_documents")) {
            Edms_documentsDAO edms_documentsDAO = new Edms_documentsDAO();
            try {
                Edms_documentsDTO edms_documentsDTO = (Edms_documentsDTO) edms_documentsDAO.getDTOByID(id);
                notiMessage += " , document number " + edms_documentsDTO.documentNo;
            } catch (Exception e) {

                e.printStackTrace();
            }
        }

        if (sendSMS == 1 || sendSMS == 2) {
            ns4.pb_notificationsDAO.sendSms(organogramID, smsContentNormal);
            ns4.pb_notificationsDAO.sendMail(organogramID, message, emailContentNormal, -1, null);
        }
        if (sendSMS == 2 && message.equalsIgnoreCase("approved")) // Send recipients also
        {
            byte[] template = null;
            long filesDropzone = -1;
            logger.debug("approved tableName = " + tableName + " id = " + id);

            ApprovalNotificationRecipientsDAO approval_notification_recipientsDAO = new ApprovalNotificationRecipientsDAO();
            List<ApprovalNotificationRecipientsDTO> approval_notification_recipientsDTOList;
			try {
				approval_notification_recipientsDTOList = approval_notification_recipientsDAO.getApprovalNotificationRecipientsDTOListByApprovalPathID(approvalPathId);
				 long[] organogramIds = new long[approval_notification_recipientsDTOList.size()];
		            i = 0;
		            for (ApprovalNotificationRecipientsDTO approval_notification_recipientsDTO : approval_notification_recipientsDTOList) {
		                ns4.pb_notificationsDAO.sendSms(approval_notification_recipientsDTO.organogramId, smsContentApproved);
		                organogramIds[i] = approval_notification_recipientsDTO.organogramId;
		                i++;
		            }
		            ns4.pb_notificationsDAO.sendMail(organogramIds, message, emailApproved, filesDropzone, template);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
                    

           
        }

        /*if (sendSMS == 2 && tableName.equalsIgnoreCase("meeting_request_details")) // Send recipients also
        {

        }*/

        return ns4.pb_notificationsDAO.addPb_notifications(organogramID, date.getTime(), URL, notiMessage);
    }

    public void updateAssignedToAndAddNotification(Approval_execution_tableDTO approval_execution_tableDTO) {
        ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
        ApprovalPathDetailsDTO approvalPathDetailsDTO = approvalPathDetailsDAO
                .getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(
                        approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
        logger.debug("updating assigned to, apdto = " + approvalPathDetailsDTO);
        updateAssignedTo(approval_execution_tableDTO.previousRowId, approvalPathDetailsDTO.organogramId);

        Date fromDate = new Date(approval_execution_tableDTO.lastModificationTime);
        Date dueDate = CalenderUtil.getDateAfter(fromDate, approvalPathDetailsDTO.daysRequired);

        long overdueID = addApprovalNotifications(approval_execution_tableDTO.updatedRowId,
                approvalPathDetailsDTO.organogramId, "Deadline Over", dueDate, -1, 0, -1); // Overdue Notification

        addApprovalNotifications(approval_execution_tableDTO.updatedRowId, approvalPathDetailsDTO.organogramId,
                "Pending Approval", fromDate, overdueID, 1, -1); // Normal Notification

    }

    public void updateAssignedToAndAddFinalNotification(Approval_execution_tableDTO approval_execution_tableDTO,
                                                        String message) {
        long organogamId = updateAssignedTo(approval_execution_tableDTO.previousRowId);

        Date fromDate = new Date(approval_execution_tableDTO.lastModificationTime);

        addApprovalNotifications(approval_execution_tableDTO.updatedRowId, organogamId, message, fromDate, -1, 2,
                approval_execution_tableDTO.approvalPathId); // Normal Notification

    }

    public long updateAssignedTo(long id) {
        Approval_summaryDAO approval_summaryDAO = new Approval_summaryDAO();

        Approval_summaryDTO approval_summaryDTO = null;
        try {
            approval_summaryDTO = approval_summaryDAO.getDTOByTableNameAndTableID(tableName, id);
        } catch (Exception e2) {
            e2.printStackTrace();
        }

        long organogramID = -2;
        try {
            if (approval_summaryDTO != null) {
                organogramID = WorkflowController.getOrganogramIDFromUserID(approval_summaryDTO.initiator);
            }
        } catch (Exception e1) {

            e1.printStackTrace();
        }

        long lastModificationTime = System.currentTimeMillis();
        String sql = "UPDATE approval_summary" + " SET assigned_to = " + organogramID + " ," + "lastModificationTime=" + lastModificationTime
                + " WHERE table_id = " + id + " and table_name = '" + tableName + "'";
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            Connection connection = model.getConnection();
            Statement stmt = model.getStatement();
            try {
                logger.debug(sql);
                stmt.execute(sql);
                ns4.recordUpdateTime(connection, lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
        return organogramID;
    }
    
    public void removeFromApprovalPath(long ID) throws Exception {
        long lastModificationTime = System.currentTimeMillis();
        String sql = "UPDATE " + tableName + " SET isDeleted =  0, lastModificationTime = " + lastModificationTime + "," + " job_cat = "
                + SessionConstants.DEFAULT_JOB_CAT + " WHERE ID = " + ID;
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            Connection connection = model.getConnection();
            Statement stmt = model.getStatement();
            try {
                logger.debug(sql);
                stmt.execute(sql);
                ns4.recordUpdateTime(connection, lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
    }
    
    public void updateStatusInSummaryTable(long id, int approvalStatus) {
        long lastModificationTime = System.currentTimeMillis();
        String sql = "UPDATE approval_summary " + " SET approval_status_cat = " + approvalStatus + ",lastModificationTime=" + lastModificationTime
                + " WHERE table_id = " + id + " and table_name = '" + tableName + "'";
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            Connection connection = model.getConnection();
            Statement stmt = model.getStatement();
            try {
                logger.debug(sql);
                stmt.execute(sql);
                ns4.recordUpdateTime(connection, lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
    }


}
