package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;

import common.ConnectionAndStatementUtil;
import common.VbSequencerService;
import dbm.*;
import org.apache.log4j.Logger;


public abstract class NavigationService2 implements VbSequencerService
{
	
	public static final int GETIDS = 1;
	public static final int GETDTOS = 2;
	public static final int GETCOUNT = 3;

	private static final Logger logger = Logger.getLogger(NavigationService2.class);
	
	public void printSql(String sql)
	{
		 logger.debug("sql: " + sql);
	}
	
	public void deleteByID(long ID, String tableName) throws Exception{
		Connection connection = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		
		long lastModificationTime = System.currentTimeMillis();	
		try{
			String sql = "UPDATE " + tableName;
			
			sql += " SET isDeleted=1,lastModificationTime="+ lastModificationTime +" WHERE ID = " + ID;
			
			printSql(sql);

			connection = DBMW.getInstance().getConnection();
			stmt  = connection.createStatement();
			stmt.execute(sql);
			

			
			recordUpdateTime(connection, lastModificationTime);

			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMW.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
	}

	public void recordUpdateTime(Connection connection, long lastModificationTime) throws SQLException
	{
		recordUpdateTime(connection,"office_unit_organograms",lastModificationTime);
	}
	public int getCount(Hashtable p_searchCriteria, int limit, int offset)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		int count = 0;

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETCOUNT);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				count = rs.getInt("countID");
				
			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 

						DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return count;
	
	}
	
	public long getLastModificationTime(String tableName)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		long lastModificationTime = 0;

		try{
			
			String sql = "select table_LastModificationTime from vbSequencer where table_name = '" + tableName +  "'";
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				lastModificationTime = rs.getLong("table_LastModificationTime");
				
			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return lastModificationTime;
	
	}
	
	public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria) throws Exception
    {
		logger.debug("table: " + p_searchCriteria);
		List<Long> idList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement ps = null;
		
		try{

			String sql = getSqlWithSearchCriteria(p_searchCriteria, -1, -1, GETIDS);
								
			connection = DBMR.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				idList.add(rs.getLong("ID"));
			}
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){

					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return idList;
	}
	
	
	public Collection getIDs() 
    {
        Collection data = new ArrayList();
        Connection connection=null;
    	Statement stmt=null;
    	ResultSet resultSet = null;
    	
        String sql = getSqlWithSearchCriteria(null, -1, -1, GETIDS);
		
		printSql(sql);
		
        try
        {
	        connection = DBMR.getInstance().getConnection();
	        stmt = connection.createStatement();
	        
	        for(resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("ID")));
	
	        resultSet.close();
        }
        catch (Exception e)
        {
	    	e.printStackTrace();
        }
	    finally
        {
	    	try
            {
          	  if(resultSet!= null && !resultSet.isClosed())
          	  {
          		  resultSet.close();
          	  }
            }
            catch(Exception ex)
            {
          	  
            }
          try{if (stmt != null){stmt.close();}}catch (Exception e){}
          try{if (connection != null){DBMR.getInstance().freeConnection(connection);}}
          catch (Exception e){e.printStackTrace();}
        }
        return data;
    }

	public abstract Collection getDTOs(Hashtable p_searchCriteria, int limit, int offset);
	public abstract String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category);
	
}