package util;

import common.CustomException;
import marquee_text.Marquee_textServlet;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import sessionmanager.SessionConstants;

import javax.rmi.CORBA.Util;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

final public class DateTimeUtil {
    public static Logger logger = Logger.getLogger(DateTimeUtil.class);

    private DateTimeUtil() {
    }

    public static SimpleDateFormat dateTimeIsoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
    public static SimpleDateFormat dateTimeViewFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a");

    public static Long getTimestamp(String dateTimeIsoString, String exceptionMessage, Long defaultValue) {
        try {
            return dateTimeIsoFormat.parse(dateTimeIsoString).getTime();
        } catch (Exception e) {
            logger.error(e);
            if (exceptionMessage != null) {
                throw new CustomException(exceptionMessage);
            }
        }
        return defaultValue;
    }

    public static String getDateTimeIsoString(Long timeInMillis) {
        if (timeInMillis == null || Objects.equals(timeInMillis, SessionConstants.MIN_DATE)) {
            return "";
        }
        return dateTimeIsoFormat.format(new Date(timeInMillis));
    }

    public static String getViewString(Long timeInMillis, boolean isLangEn) {
        if (timeInMillis == null || Objects.equals(timeInMillis, SessionConstants.MIN_DATE)) {
            return "";
        }
        String viewString = dateTimeViewFormat.format(new Date(timeInMillis));
        if (!isLangEn) {
            viewString = StringUtils.convertToBanNumber(viewString)
                                    .replaceAll("AM", "(সকাল)")
                                    .replaceAll("PM", "(বিকাল)");
        }
        return viewString;
    }

    public static int getHourValue(int hour, boolean is24HourFormat) {
        if (is24HourFormat) {
            return (hour % 24 + 24) % 24;
        }
        hour = (hour % 12 + 12) % 12;
        return hour == 0 ? 12 : hour;
    }

    public static OptionDTO getHourOption(int hour, boolean is24HourFormat) {
        int hourValue = getHourValue(hour, is24HourFormat);
        String hourValueStr = String.valueOf(hourValue);
        String hourStr = String.valueOf(hour);
        return new OptionDTO(
                hourValueStr,
                StringUtils.convertToBanNumber(hourValueStr),
                hourStr,
                "", hourStr
        );
    }

    public static String buildTimeDropDown(int from, int to, int selected, String language,
                                           OptionDTO nothingSelectedOption, boolean is24HourFormat) {
        List<OptionDTO> optionDTOList = new ArrayList<>();
        if (nothingSelectedOption != null) {
            optionDTOList.add(nothingSelectedOption);
        }
        List<OptionDTO> hourOptions =
                IntStream.rangeClosed(from, to)
                         .mapToObj(hour -> getHourOption(hour, is24HourFormat))
                         .collect(Collectors.toList());
        optionDTOList.addAll(hourOptions);
        return Utils.buildOptionsWithoutSelectWithSelectId(optionDTOList, language, String.valueOf(selected));
    }
}
