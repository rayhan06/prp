package util;

import java.text.SimpleDateFormat;

public class TimeUtils {
	
	public final static long  MILLIS_IN_A_DAY = 86400000L;
	public final static SimpleDateFormat DATE_FORMAT_TIME_AM_PM = new SimpleDateFormat("hh:mm a");
}
