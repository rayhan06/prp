package util;

import java.util.Collection;
import java.util.List;

public class RecordNavigator
{
	private int m_currentPageNo;
    private int m_totalRecords;
    private int m_pageSize;
    private int m_totalPages;
    private Collection m_ids;
    private String[][] m_searchFieldInfo;
    private long m_searchTime;
    public boolean m_isPermanentTable;
    public String m_tableName;
    public List list;
    public RecordNavigator()
    {
        m_currentPageNo = 0;
        m_totalRecords = 0;
        m_pageSize = 10;
        m_totalPages = 0;
        m_ids = null;
        m_searchFieldInfo = null;
        m_searchTime = 0;
        m_isPermanentTable = true;
        m_tableName= "";
        
    }

    public String[][] getSearchFieldInfo()
    {
        return m_searchFieldInfo;
    }

    public void setSearchFieldInfo(String[][] p_searchFieldInfo)
    {
        m_searchFieldInfo = p_searchFieldInfo;
    }

    public Collection getIDs()
    {
        return m_ids;
    }

    public void setIDs(Collection p_ids)
    {
        m_ids = p_ids;
    }
    
    public long getSearchTime()
    {
        return m_searchTime;
    }

    public void setSearchTime(long p_searchTime)
    {
    	m_searchTime = p_searchTime;
    }

    public int getCurrentPageNo()
    {
        return m_currentPageNo;
    }

    public void setCurrentPageNo(int p_currentPageNo)
    {
        m_currentPageNo = p_currentPageNo;
    }

    public int getTotalRecords()
    {
        return m_totalRecords;
    }

    public void setTotalRecords(int p_totalRecords)
    {
        m_totalRecords = p_totalRecords;
    }

    public int getPageSize()
    {
        return m_pageSize;
    }

	public void setPageSize(int p_pageSize)
    {
        m_pageSize = p_pageSize;
    }

    public int getTotalPages()
    {
        return m_totalPages;
    }

    public void setTotalPages(int p_totalPages)
    {
        m_totalPages = p_totalPages;
    }

    public String toString()
    {
        String s = "";
        s = s + "\nm_pageSize = " + m_pageSize;
        s = s + "\nm_totalRecords = " + m_totalRecords;
        s = s + "\nm_currentPageNo = " + m_currentPageNo;
        s = s + "\nm_totalPages = " + m_totalPages;
        return s;
    }
}