package util;

import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import fiscal_year.Fiscal_yearDTO;
import fiscal_year.Fiscal_yearRepository;
import job_applicant_application.Job_applicant_applicationDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class UtilCharacter {

    public static String convertNumberEnToBn(String txt) {
        if (txt == null) return null;
        String replacedVersion = txt
                .replaceAll("0", "০").replaceAll("1", "১")
                .replaceAll("2", "২").replaceAll("3", "৩")
                .replaceAll("4", "৪").replaceAll("5", "৫")
                .replaceAll("6", "৬").replaceAll("7", "৭")
                .replaceAll("8", "৮").replaceAll("9", "৯");
        return replacedVersion;
    }

    public static String convertNumberBnToEn(String txt) {
        if (txt == null) return null;
        String replacedVersion = txt
                .replaceAll("০", "0").replaceAll("১", "1")
                .replaceAll("২", "2").replaceAll("৩", "3")
                .replaceAll("৪", "4").replaceAll("৫", "5")
                .replaceAll("৬", "6").replaceAll("৭", "7")
                .replaceAll("৮", "8").replaceAll("৯", "9");
        return replacedVersion;
    }

    public static String convertDataByLanguage(String language, String txt) {
        if (language.equalsIgnoreCase("English")) {
            return convertNumberBnToEn(txt);
        } else {
            return convertNumberEnToBn(txt);
        }
    }

    public boolean noData(String txt) {
        //return txt == null || txt.trim().equals("");
        boolean flag = false;
        if (null == txt) {
            flag = true;
        } else if (txt.trim().isEmpty()) {
            flag = true;
        }
        return flag;
    }

    public boolean noData(Object obj) {
        return obj == null;
    }

    public static String getDataByLanguage(String language, String banglaValue, String englishValue) {
        if (language.equalsIgnoreCase("English")) {
            return englishValue;
        } else {
            return banglaValue;
        }
    }

    /*Excel start*/

    public static XSSFWorkbook newWorkBook() {
        XSSFWorkbook workbook = new XSSFWorkbook();
        return workbook;
    }

    public static Sheet newSheet(XSSFWorkbook workbook, String sheetName) {
        Sheet sheet = workbook.createSheet(sheetName);
        return sheet;
    }

    public static Cell newRow(XSSFWorkbook workbook, Sheet sheet, String cellVal, int rowStartIndex, int rowLastIndex, int columnStartIndex, int columnLastIndex, boolean addMerged) {
        Row row = sheet.createRow(rowStartIndex);
        CellStyle style = workbook.createCellStyle();
        Cell cell = row.createCell(columnStartIndex);

        if (addMerged) {
            sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowLastIndex, columnStartIndex, columnLastIndex));
        }
        Font font = workbook.createFont();
        font.setColor(IndexedColors.BLACK.index);
        font.setBold(true);
        style.setFont(font);


        cell.setCellValue(cellVal);
        style.setWrapText(true);
        cell.setCellStyle(style);
        return cell;

    }

    public static Cell alreadyCreatedRow(XSSFWorkbook workbook, Sheet sheet, String cellVal, int rowStartIndex, int rowLastIndex, int columnStartIndex, int columnLastIndex, boolean addMerged, Row row, IndexedColors indexedColors, boolean bold) {
        CellStyle style = workbook.createCellStyle();
        Cell cell = row.createCell(columnStartIndex);

        if (addMerged) {
            sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowLastIndex, columnStartIndex, columnLastIndex));
        }

        Font font = workbook.createFont();
        font.setColor(indexedColors.index);
        if (bold) {
            font.setBold(true);
        }
        style.setFont(font);

        cell.setCellValue(cellVal);
        style.setWrapText(true);
        cell.setCellStyle(style);
        return cell;

    }

    public static int alreadyCreatedRowForColumnSelect(XSSFWorkbook workbook, Sheet sheet,
                                                       String cellVal, int rowStartIndex,
                                                       int rowLastIndex, int columnStartIndex, int columnLastIndex,
                                                       boolean addMerged, Row row, IndexedColors indexedColors, boolean bold, HttpServletRequest request,int reqColIter) {


        String isSelectedFromReq = "c" + reqColIter;
        if (request.getParameter("isColumnSelect").trim().equals("1") ||
                (request.getParameter(isSelectedFromReq) != null && request.getParameter(isSelectedFromReq).trim().equalsIgnoreCase("on"))) {
            System.out.println("columnStartIndex2: "+columnStartIndex);
            System.out.println("columnLastIndex2: "+columnLastIndex);
            CellStyle style = workbook.createCellStyle();
            Cell cell = row.createCell(columnStartIndex);

            if (addMerged) {
                sheet.addMergedRegion(new CellRangeAddress(rowStartIndex, rowLastIndex, columnStartIndex, columnLastIndex));
            }

            Font font = workbook.createFont();
            font.setColor(indexedColors.index);
            if (bold) {
                font.setBold(true);
            }
            style.setFont(font);

            cell.setCellValue(cellVal);
            style.setWrapText(true);
            cell.setCellStyle(style);
            return columnLastIndex+1;
        }

        return columnStartIndex;

    }

    /*Excel end*/
    public static Job_applicant_applicationDTO getJobApplicantApplicationDTO(List<Job_applicant_applicationDTO> job_applicant_applicationDTOS, String roll) {

//        Job_applicant_applicationDTO dto = new Job_applicant_applicationDTO();
//         for(Job_applicant_applicationDTO job_applicant_applicationDTO:job_applicant_applicationDTOS){
//             if(job_applicant_applicationDTO.rollNumber.equals(roll)){
//                 dto = job_applicant_applicationDTO;
//                 break;
//             }
//
//         }

        Job_applicant_applicationDTO dto = job_applicant_applicationDTOS
                .stream()
                .filter(d -> d.rollNumber.equals(roll))
                .findFirst()
                .orElse(null);
        return dto;
    }


    public static String getBooleanReadable(boolean value, String language) {
        String returnValue = "";
        if (language.equalsIgnoreCase("English")) {
            if (value) {
                returnValue = "Yes";
            } else {
                returnValue = "No";
            }
        } else {
            if (value) {
                returnValue = "হ্যাঁ";
            } else {
                returnValue = "না";
            }
        }

        return returnValue;

    }

    public static void throwException(String bn, String en) throws Exception {
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        throw new Exception(UtilCharacter.getDataByLanguage(Language, bn, en));
    }

    public static String getNameByOrg(long orgId, String language) {
        StringBuilder text = new StringBuilder();
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().
                getByOfficeUnitOrganogramId(orgId);
        if (employeeOfficeDTO != null) {
            Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().
                    getById(employeeOfficeDTO.employeeRecordId);
            if (employeeRecordsDTO != null) {
                text.append(getDataByLanguage(language, employeeRecordsDTO.nameBng, employeeRecordsDTO.nameEng));
            }

        }

        OfficeUnitOrganograms organograms = OfficeUnitOrganogramsRepository.getInstance().getById(orgId);
        if (organograms != null) {
            text.append(", ").append(getDataByLanguage(language, organograms.designation_bng, organograms.designation_eng));
            Office_unitsDTO unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(organograms.office_unit_id);
            if (unitsDTO != null) {
                text.append(", ").append(getDataByLanguage(language, unitsDTO.unitNameBng,
                        unitsDTO.unitNameEng));
            }
        }

        return text.toString();
    }

    public static String getNameOfEmployee(String nameEn, String nameBn, String unitName, String unitNameBn,
                                           String orgName, String orgNameBn, String Language) {
        if (nameEn == null) nameEn = "";
        if (nameBn == null) nameBn = "";
        if (unitName == null) unitName = "";
        if (unitNameBn == null) unitNameBn = "";
        if (orgName == null) orgName = "";
        if (orgNameBn == null) orgNameBn = "";
        StringBuilder text = new StringBuilder();
        text.append(UtilCharacter.getDataByLanguage(Language, nameBn, nameEn))
                .append(", ")
                .append(UtilCharacter.getDataByLanguage(Language, orgNameBn, orgName))
                .append(", ")
                .append(UtilCharacter.getDataByLanguage(Language, unitNameBn, unitName));

        return text.toString();
    }

    public static String getOfficeUnitDataById(long officeUnitId, String language) {
        String officeUnit = "";
        Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitId);
        if (office_unitsDTO != null) {
            officeUnit = getDataByLanguage(language, office_unitsDTO.unitNameBng, office_unitsDTO.unitNameEng);
        }
        return officeUnit;
    }

    public static String getFiscalYearDataById(long fiscalYearId, String language) {
        String fiscalYear = "";
        Fiscal_yearDTO fiscalYearDTO = Fiscal_yearRepository.getInstance().getFiscal_yearDTOByid(fiscalYearId);
        if (fiscalYearDTO != null) {
            fiscalYear = getDataByLanguage(language, fiscalYearDTO.nameBn, fiscalYearDTO.nameEn);
        }
        return fiscalYear;
    }

    public static String monthEnToBn(String month) {
        String convertedMonth = "";
        switch (month) {
            case "January":
                convertedMonth = "জানুয়ারী";
                break;
            case "February":
                convertedMonth = "ফেব্রুয়ারী";
                break;
            case "March":
                convertedMonth = "মার্চ";
                break;
            case "April":
                convertedMonth = "এপ্রিল";
                break;
            case "May":
                convertedMonth = "মে";
                break;
            case "June":
                convertedMonth = "জুন";
                break;
            case "July":
                convertedMonth = "জুলাই";
                break;
            case "August":
                convertedMonth = "আগস্ট";
                break;
            case "September":
                convertedMonth = "সেপ্টেম্বর";
                break;
            case "October":
                convertedMonth = "অক্টোবর";
                break;
            case "November":
                convertedMonth = "নভেম্বর";
                break;
            case "December":
                convertedMonth = "ডিসেম্বর";
                break;
            default:
                convertedMonth = "";
        }
        return convertedMonth;
    }

    public static String newDateFormat(String date) {
        String[] arr = date.split(" ");
        String convertedMonth = monthEnToBn(arr[1].trim());
        String convertedDate = arr[0] + " " + convertedMonth + " " + arr[2];
        return convertedDate;

    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }


}