package util;

/*
 * @author Md. Erfan Hossain
 * @created 28/10/2021 - 12:40 AM
 * @project parliament
 */

import common.ConnectionAndStatementUtil;
import office_unit_organogram.Office_unit_organogramDAO;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ScriptForUpdatingOfficeUnitOrganograms {
    public static void main(String[] args) {
        ConnectionAndStatementUtil.getReadStatement(st -> {
            String sql = "SELECT office_unit_organogram_id, employee_record_id " +
                    " FROM employee_offices " +
                    " WHERE isDeleted = 0 " +
                    " AND status = 1";
            try {
                ResultSet rs = st.executeQuery(sql);
                while (rs.next()) {
                    long id = Long.parseLong(rs.getString(1));
                    long recordId = Long.parseLong(rs.getString(2));
                    Office_unit_organogramDAO.getInstance().updateVacancy(id, recordId, false, -1, System.currentTimeMillis());
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        });
    }
}
