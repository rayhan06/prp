package util;

import java.io.UnsupportedEncodingException;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import login.LoginDTO;
//import org.apache.log4j.Logger;
import pb.Utils;
import sessionmanager.SessionConstants;

public class RecordNavigationManager2
{
	String m_navigatorName;
	String m_tableName;
	String m_dtoCollectionName;
	HttpServletRequest m_request;
	NavigationService2 m_service;
	String[][] m_searchFieldInfo;
	int m_countIDs = 0;
	int m_totalPage = 0;
	int m_recordsPerPage = 0;
	Hashtable m_Hashtable = null;
	HttpSession m_Session = null;
	long m_timeToSet = 0;

	//static Logger logger = Logger.getLogger(RecordNavigationManager.class.getName());
	
    public RecordNavigationManager2(String p_navigatorName, HttpServletRequest p_request, NavigationService2 p_service,
                                    String p_dtoCollectionName, String[][] p_searchFieldInfo, String p_tableName)
    {
        m_navigatorName = p_navigatorName;
        m_request = p_request;
        m_service = p_service;
        m_dtoCollectionName = p_dtoCollectionName;
        m_searchFieldInfo = p_searchFieldInfo;
        m_Hashtable = null;
        m_tableName = p_tableName;
    }
    

    private void setRN(RecordNavigator rn, int tempPageNumber)
    {
    	rn.setSearchFieldInfo(m_searchFieldInfo);
    	m_totalPage = m_countIDs / rn.getPageSize();
	    if(m_countIDs % rn.getPageSize() != 0)
	    {
	    	m_totalPage++;
	    }
	    if(m_countIDs > 0)
	    {
	        rn.setCurrentPageNo(1);
	    }
	    else
	    {
	        rn.setCurrentPageNo(0);
	    }
	    rn.setTotalRecords(m_countIDs);
	    rn.setTotalPages(m_totalPage);
	    
	    
	    if(tempPageNumber > rn.getTotalPages())
        {
	    	tempPageNumber = rn.getTotalPages();
        }
        else  if(tempPageNumber <= 0)
        {
        	tempPageNumber = 1;
        }
	   
        rn.setCurrentPageNo(tempPageNumber);
        rn.setSearchTime(m_timeToSet);
        
        System.out.println("after setting, rn = " + rn);
        
    }
    
    
    private void fillHashTable()
    {
    	 Enumeration paramList = m_request.getParameterNames();
    	 m_Hashtable = new Hashtable();
		 do
		 {
		     if(!paramList.hasMoreElements())
		     {
		         break;
		     }
		     String paramName = (String)paramList.nextElement();
		     if(!paramName.equalsIgnoreCase("RECORDS_PER_PAGE") && !paramName.equalsIgnoreCase("search") && !paramName.equalsIgnoreCase(SessionConstants.HTML_SEARCH_CHECK_FIELD))
		     {
				String paramValue = m_request.getParameter(paramName);
				/*byte ptext[];
				try 
				{
					ptext = paramValue.getBytes("ISO-8859-1");
					paramValue = new String(ptext, "UTF-8");
				} 
				catch (UnsupportedEncodingException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
				 
				if(Utils.isValidSearchString(paramValue))
                {
					 m_Session.setAttribute(paramName, paramValue);
					 m_Hashtable.put(paramName, paramValue);
                }
		     }
		 } while(true);
         
    }

    public void doJob(LoginDTO loginDTO) throws Exception
    {
    	
        Collection records = null;

        System.out.println("Doing Job");
        
        m_Session = m_request.getSession();
        
        RecordNavigator rn = (RecordNavigator)m_Session.getAttribute(m_navigatorName);
        String goCheck = m_request.getParameter("go");
        String searchCheck = m_request.getParameter("search");
        String link = m_request.getParameter("link");
        
        long currentTime = System.currentTimeMillis();
        long lastSearchTime = 0;
        if(m_request.getParameter("lastSearchTime") != null && !m_request.getParameter("lastSearchTime").equalsIgnoreCase("") )
        {
        	lastSearchTime = Long.parseLong(m_request.getParameter("lastSearchTime"));
        }
        m_timeToSet = lastSearchTime;


        int pageno = 1;
      
        if(searchCheck != null) //Normal or ajax search
        {
        	System.out.println("###########Search pressed");

            fillHashTable();


            int pageSizeInt = -1;
            String pageSize = m_request.getParameter("RECORDS_PER_PAGE");
            System.out.println("Page Size Given : " + pageSize);
            
            
            if(pageSize != null)
            {
            	try
                {
                    pageSizeInt = Integer.parseInt(pageSize);
                    if(pageSizeInt > 0)
                        rn.setPageSize(pageSizeInt);
                }
                catch(NumberFormatException ex)
                {
                    //logger.fatal("Next page Size is not number ");
                }
            }
            
            if(goCheck != null) //Go Button Pressed
            {
                System.out.println("###########Go pressed");
                pageno = Integer.parseInt(m_request.getParameter("pageno"));                                
                System.out.println("pagenumber = " + pageno);
                m_countIDs = Integer.parseInt(m_request.getParameter("TotalRecords"));
                
                if(lastSearchTime < m_service.getLastModificationTime(m_tableName))
                {
                	m_countIDs = m_service.getCount(m_Hashtable, -1, -1);
                	m_timeToSet = currentTime;
                }
            }
            else
            {
            	m_countIDs = m_service.getCount(m_Hashtable, -1, -1);
            	m_timeToSet = currentTime;
            }
            setRN(rn, pageno);
        } 
        else //1st time in the search page
        {
        	System.out.println("###########From Menu");
		    m_Session.setAttribute(m_navigatorName, null);
		    
		    rn = new RecordNavigator();
		    		   
		    m_countIDs = m_service.getCount(null, -1, -1);
		    m_timeToSet = currentTime;
		
		    setRN(rn, -1);
		    
        }
        

        int nextCollectionSize;
        if(rn.getTotalRecords() == 0)
        {
            nextCollectionSize = 0;
        }
        else if(rn.getTotalRecords() > 0 && rn.getCurrentPageNo() == rn.getTotalPages() && rn.getTotalRecords() % rn.getPageSize() != 0)
        {
            nextCollectionSize = rn.getTotalRecords() % rn.getPageSize();
        }
        else
        {
            nextCollectionSize = rn.getPageSize();
        }
        int initial = nextCollectionSize != 0 ? (rn.getCurrentPageNo() - 1) * rn.getPageSize()  : 0;
        
        System.out.println("############################ limit = " + nextCollectionSize + " offset = " + initial);
        
        if(nextCollectionSize > 0)
        {
        	
        	records = m_service.getDTOs(m_Hashtable, nextCollectionSize, initial);
        	
        }
        //logger.debug("rn.toString is : " + rn.toString());
        m_Session.setAttribute(m_navigatorName, rn);
        m_Session.setAttribute(m_dtoCollectionName, records);
    }    
}