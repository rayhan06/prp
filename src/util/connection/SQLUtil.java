package util.connection;

/*
 * @author Md. Erfan Hossain
 * @created 08/06/2022 - 8:03 PM
 * @project parliament
 */

import common.ConnectionAndStatementModel;
import common.ConnectionAndStatementUtil;
import common.ConnectionType;
import dbm.DBML;
import dbm.DBMR;
import dbm.DBMW;
import org.apache.log4j.Logger;
import pb.Utils;
import util.function.ConsumerWithThrow;
import util.function.FunctionWithThrow;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates","unused","unchecked"})
public class SQLUtil {
    private static final Logger logger = Logger.getLogger(SQLUtil.class);
    
    private SQLUtil(){}

    //READ
    public static Object getReadStatement(FunctionWithThrow<Statement, Object,Exception> func) throws Exception{
        return getStatement(func,ConnectionType.READ);
    }

    public static void getReadStatement(ConsumerWithThrow<Statement> consumerWithThrow) throws Exception{
        getStatement(consumerWithThrow,ConnectionType.READ);
    }

    public static Object getReadStatement(FunctionWithThrow<Statement, Object,Exception> func, Connection con) throws Exception{
        return getStatement(func,con);
    }

    public static void getReadStatement(ConsumerWithThrow<Statement> consumerWithThrow, Connection con) throws Exception{
        getStatement(consumerWithThrow,con);
    }

    public static Object getReadConnectionAnStatement(FunctionWithThrow<ConnectionAndStatementModel, Object,Exception> func) throws Exception{
        return getConnectionAndStatement(func,ConnectionType.READ);
    }

    public static void getReadConnectionAndStatement(ConsumerWithThrow<ConnectionAndStatementModel> consumerWithThrow) throws Exception{
        getConnectionAndStatement(consumerWithThrow,ConnectionType.READ);
    }

    public static Object getReadPrepareStatement(FunctionWithThrow<PreparedStatement, Object,Exception> func, String sqlQuery) throws Exception{
        return getPrepareStatement(func,sqlQuery,ConnectionType.READ);
    }

    public static void getReadPrepareStatement(ConsumerWithThrow<PreparedStatement> consumerWithThrow, String sqlQuery) throws Exception{
        getPrepareStatement(consumerWithThrow,sqlQuery,ConnectionType.READ);
    }

    public static Object getReadPrepareStatement(FunctionWithThrow<PreparedStatement, Object,Exception> func, Connection con, String sqlQuery) throws Exception{
        return getPrepareStatement(func,con,sqlQuery);
    }

    public static void getReadPrepareStatement(ConsumerWithThrow<PreparedStatement> consumerWithThrow, Connection con, String sqlQuery) throws Exception{
        getPrepareStatement(consumerWithThrow,con,sqlQuery);
    }

    public static Object getReadConnectionAndPrepareStatement(FunctionWithThrow<ConnectionAndStatementModel, Object,Exception> func, String sqlQuery) throws Exception{
        return getConnectionAndPrepareStatement(func,sqlQuery,ConnectionType.READ);
    }

    public static void getReadConnectionAndPrepareStatement(ConsumerWithThrow<ConnectionAndStatementModel> consumerWithThrow, String sqlQuery) throws Exception{
        getConnectionAndPrepareStatement(consumerWithThrow,sqlQuery,ConnectionType.READ);
    }


    //WRITE
    public static Object getWriteStatement(FunctionWithThrow<Statement, Object,Exception> func) throws Exception{
        return getStatement(func,ConnectionType.WRITE);
    }

    public static void getWriteStatement(ConsumerWithThrow<Statement> consumerWithThrow) throws Exception{
        getStatement(consumerWithThrow,ConnectionType.WRITE);
    }

    public static Object getWriteStatement(FunctionWithThrow<Statement, Object,Exception> func, Connection con) throws Exception{
        return getStatement(func,con);
    }

    public static void getWriteStatement(ConsumerWithThrow<Statement> consumerWithThrow, Connection con) throws Exception{
        getStatement(consumerWithThrow,con);
    }

    public static Object getWriteConnectionAndStatement(FunctionWithThrow<ConnectionAndStatementModel, Object,Exception> func) throws Exception{
        return getConnectionAndStatement(func,ConnectionType.WRITE);
    }

    public static void getWriteConnectionAndStatement(ConsumerWithThrow<ConnectionAndStatementModel> consumerWithThrow)throws Exception{
        getConnectionAndStatement(consumerWithThrow,ConnectionType.WRITE);
    }

    public static Object getWritePrepareStatement(FunctionWithThrow<PreparedStatement, Object,Exception> func, String sqlQuery) throws Exception{
        return getPrepareStatement(func, sqlQuery, ConnectionType.WRITE);
    }

    public static void getWritePrepareStatement(ConsumerWithThrow<PreparedStatement> consumerWithThrow, String sqlQuery) throws Exception{
        getPrepareStatement(consumerWithThrow, sqlQuery, ConnectionType.WRITE);
    }

    public static Object getWritePrepareStatement(FunctionWithThrow<PreparedStatement, Object,Exception> func, Connection con, String sqlQuery) throws Exception {
        return getPrepareStatement(func,con,sqlQuery);
    }

    public static void getWritePrepareStatement(ConsumerWithThrow<PreparedStatement> consumerWithThrow, Connection con, String sqlQuery) throws Exception{
        getPrepareStatement(consumerWithThrow,con,sqlQuery);
    }

    public static Object getWriteConnectionAndPrepareStatement(FunctionWithThrow<ConnectionAndStatementModel, Object,Exception> func, String sqlQuery) throws Exception{
        return getConnectionAndPrepareStatement(func,sqlQuery,ConnectionType.WRITE);
    }

    public static void getWriteConnectionAndPrepareStatement(ConsumerWithThrow<ConnectionAndStatementModel> consumerWithThrow, String sqlQuery) throws Exception{
        getConnectionAndPrepareStatement(consumerWithThrow,sqlQuery,ConnectionType.WRITE);
    }


    //LOG
    public static Object getLogWriteStatement(FunctionWithThrow<Statement, Object,Exception> func) throws Exception{
        return getStatement(func,ConnectionType.LOG);
    }

    public static void getLogWriteStatement(ConsumerWithThrow<Statement> consumerWithThrow) throws Exception{
        getStatement(consumerWithThrow,ConnectionType.LOG);
    }

    public static Object getLogWriteStatement(FunctionWithThrow<Statement, Object,Exception> func, Connection con) throws Exception{
        return getStatement(func,con);
    }

    public static void getLogWriteStatement(ConsumerWithThrow<Statement> consumerWithThrow, Connection con) throws Exception{
        getStatement(consumerWithThrow,con);
    }

    public static Object getLogWriteConnectionAndStatement(FunctionWithThrow<ConnectionAndStatementModel, Object,Exception> func) throws Exception{
        return getConnectionAndStatement(func,ConnectionType.LOG);
    }

    public static void getLogWriteConnectionAndStatement(ConsumerWithThrow<ConnectionAndStatementModel> consumerWithThrow) throws Exception{
        getConnectionAndStatement(consumerWithThrow,ConnectionType.LOG);
    }

    public static Object getLogWritePrepareStatement(FunctionWithThrow<PreparedStatement, Object,Exception> func, String sqlQuery)throws Exception{
        return getPrepareStatement(func, sqlQuery, ConnectionType.LOG);
    }

    public static void getLogWritePrepareStatement(ConsumerWithThrow<PreparedStatement> consumerWithThrow, String sqlQuery) throws Exception{
        getPrepareStatement(consumerWithThrow, sqlQuery, ConnectionType.LOG);
    }

    public static Object getLogWritePrepareStatement(FunctionWithThrow<PreparedStatement, Object,Exception> func, Connection con, String sqlQuery) throws Exception{
        return getPrepareStatement(func,con,sqlQuery);
    }

    public static void getLogWritePrepareStatement(ConsumerWithThrow<PreparedStatement> consumerWithThrow, Connection con, String sqlQuery) throws Exception{
        getPrepareStatement(consumerWithThrow,con,sqlQuery);
    }

    public static Object getLogWriteConnectionAndPrepareStatement(FunctionWithThrow<ConnectionAndStatementModel, Object,Exception> func, String sqlQuery) throws Exception{
        return getConnectionAndPrepareStatement(func,sqlQuery,ConnectionType.LOG);
    }

    public static void getLogWriteConnectionAndPrepareStatement(ConsumerWithThrow<ConnectionAndStatementModel> consumerWithThrow, String sqlQuery) throws Exception{
        getConnectionAndPrepareStatement(consumerWithThrow,sqlQuery,ConnectionType.LOG);
    }

    private static Object getStatement(FunctionWithThrow<Statement, Object,Exception> func, Connection con) throws Exception{
        Statement st = null;
        try {
            st = con.createStatement();
            //logger.debug("Successfully Open Statement");
            return func.apply(st);
        }catch (Exception ex) {
            logger.error("",ex);
            throw ex;
        } finally {
            if (st != null) {
                try {
                    st.close();
                    //logger.debug("Successfully Closed Statement");
                } catch (SQLException ex) {
                    logger.error("",ex);
                    logger.error("Failed to Closed Statement "+ex);
                    logger.error("",ex);
                }
            }
        }
    }

    private static Object getStatement(FunctionWithThrow<Statement, Object,Exception> func,
                                       Connection con, ConnectionType connectionType) throws Exception{
        Statement st = null;
        try {
            st = con.createStatement();
            //logger.debug("Successfully Open Statement for "+connectionType);
            return func.apply(st);
        }catch (Exception ex) {
            logger.error("",ex);
            if(connectionType == ConnectionType.WRITE){
                throw ex;
            }
        } finally {
            if (st != null) {
                try {
                    st.close();
                    //logger.debug("Successfully Closed Statement for "+connectionType);
                } catch (SQLException ex) {
                    logger.error("Failed to Closed Statement "+ex);
                    logger.error("",ex);
                }
            }
        }
        logger.error("Failed to create Statement");
        return null;
    }

    private static void getStatement(ConsumerWithThrow<Statement> consumerWithThrow, Connection con) throws Exception{
        Statement st = null;
        try {
            st = con.createStatement();
            //logger.debug("Successfully Open Statement");
            consumerWithThrow.accept(st);
        }catch (Exception ex) {
            logger.error("",ex);
            throw ex;
        } finally {
            if (st != null) {
                try {
                    st.close();
                    //logger.debug("Successfully Closed Statement");
                } catch (SQLException ex) {
                    logger.error("Failed to Closed Statement "+ex);
                    logger.error("",ex);
                }
            }
        }
    }

    private static void getStatement(ConsumerWithThrow<Statement> consumerWithThrow, Connection con, ConnectionType connectionType) throws Exception{
        Statement st = null;
        try {
            st = con.createStatement();
            //logger.debug("Successfully Open Statement for "+connectionType);
            consumerWithThrow.accept(st);
        }catch (Exception ex) {
            logger.error("",ex);
            if(connectionType == ConnectionType.WRITE){
                throw ex;
            }
        } finally {
            if (st != null) {
                try {
                    st.close();
                    //logger.debug("Successfully Closed Statement for "+connectionType);
                } catch (SQLException ex) {
                    logger.error("Failed to Closed Statement "+ex);
                    logger.error("",ex);
                }
            }
        }
    }

    private static Object getPrepareStatement(FunctionWithThrow<PreparedStatement, Object,Exception> func,
                                              Connection con, String sqlQuery) throws Exception{
        logger.debug(sqlQuery);
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(sqlQuery);
            //logger.debug("Successfully Open PrepareStatement");
            return func.apply(st);
        }catch (Exception ex) {
            logger.error("",ex);
            throw ex;
        }finally {
            if (st != null) {
                try {
                    st.close();
                    //logger.debug("Successfully Closed PrepareStatement");
                } catch (SQLException ex) {
                    logger.error("Failed to Closed PrepareStatement "+ex);
                    logger.error("",ex);
                }
            }
        }
    }

    private static Object getPrepareStatement(FunctionWithThrow<PreparedStatement, Object,Exception> func,
                                              Connection con, String sqlQuery, ConnectionType connectionType) throws Exception{
        logger.debug(sqlQuery);
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(sqlQuery);
            //logger.debug("Successfully Open PrepareStatement for "+connectionType);
            return func.apply(st);
        }catch (Exception ex) {
            logger.error("",ex);
            if(connectionType == ConnectionType.WRITE){
                throw ex;
            }
        }finally {
            if (st != null) {
                try {
                    st.close();
                    //logger.debug("Successfully Closed PrepareStatement for "+connectionType);
                } catch (SQLException ex) {
                    logger.error("Failed to Closed PrepareStatement "+ex);
                    logger.error("",ex);
                }
            }
        }
        logger.error("Failed to create PrepareStatement");
        return null;
    }

    private static void getPrepareStatement(ConsumerWithThrow<PreparedStatement> consumerWithThrow, Connection con, String sqlQuery) throws Exception{
        logger.debug(sqlQuery);
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(sqlQuery);
            //logger.debug("Successfully Open PrepareStatement");
            consumerWithThrow.accept(st);
        }catch (Exception ex) {
            logger.error("",ex);
            throw ex;
        }finally {
            if (st != null) {
                try {
                    st.close();
                    //logger.debug("Successfully Closed PrepareStatement");
                } catch (SQLException ex) {
                    logger.error("Failed to Closed PrepareStatement : "+ex);
                    logger.error("",ex);
                }
            }
        }
    }

    private static void getPrepareStatement(ConsumerWithThrow<PreparedStatement> consumerWithThrow, Connection con,
                                            String sqlQuery, ConnectionType connectionType) throws Exception{
        logger.debug(sqlQuery);
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(sqlQuery);
            //logger.debug("Successfully Open PrepareStatement "+connectionType);
            consumerWithThrow.accept(st);
        }catch (Exception ex) {
            logger.error("",ex);
            if(connectionType == ConnectionType.WRITE){
                throw ex;
            }
        }finally {
            if (st != null) {
                try {
                    st.close();
                    //logger.debug("Successfully Closed PrepareStatement "+connectionType);
                } catch (SQLException ex) {
                    logger.error("Failed to Closed PrepareStatement : "+ex);
                    logger.error("",ex);
                }
            }
        }
    }

    private static Object getConnectionAndPrepareStatement(FunctionWithThrow<ConnectionAndStatementModel,
                Object,Exception> func, String sqlQuery, ConnectionType connectionType) throws Exception{
        return getConnection(con->{
            return getPrepareStatement(ps->{
                return func.apply(new ConnectionAndStatementModel(con,ps));
            },con, sqlQuery);
        }, connectionType);
    }

    private static void getConnectionAndPrepareStatement(ConsumerWithThrow<ConnectionAndStatementModel> consumerWithThrow,
                                                         String sqlQuery, ConnectionType connectionType)throws Exception {
        getConnection(con->{
            getPrepareStatement(ps->{
                consumerWithThrow.accept(new ConnectionAndStatementModel(con,ps));
            },con, sqlQuery);
        }, connectionType);
    }

    private static Object getConnectionAndStatement(FunctionWithThrow<ConnectionAndStatementModel, Object,Exception> func,
                                                    ConnectionType connectionType) throws Exception{
        return getConnection(con->{
            return getStatement(ps->{
                return func.apply(new ConnectionAndStatementModel(con,ps));
            },con);
        }, connectionType);
    }

    private static void getConnectionAndStatement(ConsumerWithThrow<ConnectionAndStatementModel> consumerWithThrow,
                                                  ConnectionType connectionType) throws Exception{
        getConnection(con->{
            getStatement(ps->{
                consumerWithThrow.accept(new ConnectionAndStatementModel(con,ps));
            },con);
        }, connectionType);
    }

    private static Object getStatement(FunctionWithThrow<Statement, Object,Exception> func, ConnectionType connectionType) throws Exception{
        return getConnection(con -> {
            return getStatement(func,con);
        },connectionType);
    }

    private static void getStatement(ConsumerWithThrow<Statement> consumerWithThrow, ConnectionType connectionType) throws Exception{
        getConnection(con -> {
            getStatement(consumerWithThrow,con);
        },connectionType);
    }

    private static Object getPrepareStatement(FunctionWithThrow<PreparedStatement, Object,Exception> func,
                                              String sqlQuery, ConnectionType connectionType) throws Exception{
        return getConnection(con -> {
            return getPrepareStatement(func,con,sqlQuery);
        },connectionType);
    }

    private static void getPrepareStatement(ConsumerWithThrow<PreparedStatement> consumerWithThrow, String sqlQuery,
                                            ConnectionType connectionType) throws Exception{
        getConnection(con -> {
            getPrepareStatement(consumerWithThrow,con,sqlQuery);
        },connectionType);
    }

    private static Object getConnection(FunctionWithThrow<Connection, Object,Exception> func, ConnectionType connectionType) throws Exception{
        Connection connection = null;
        try{
            do {
                connection = getConnection(connectionType);
            }while (connection == null);
            return func.apply(connection);
        }catch(Exception ex) {
            logger.error("",ex);
            throw ex;
        }finally {
            closeConnection(connection, connectionType);
        }
    }

    private static void getConnection(ConsumerWithThrow<Connection> consumerWithThrow, ConnectionType connectionType) throws Exception{
        Connection connection = null;
        try{
            do {
                connection = getConnection(connectionType);
            }while (connection == null);
            consumerWithThrow.accept(connection);
        }catch(Exception ex) {
            logger.error("",ex);
            throw ex;
        }finally {
            closeConnection(connection, connectionType);
        }
    }

    public static Connection getConnection(ConnectionType connectionType) throws Exception{
        Connection connection ;
        switch(connectionType) {
            case READ:
                connection = DBMR.getInstance().getConnection();
                break;
            case WRITE:
                connection = DBMW.getInstance().getConnection();
                break;
            case LOG:
                connection = DBML.getInstance().getConnection();
                break;
            default:
                throw new Exception("Invalid connection type : "+connectionType);
        }
        //logger.debug("Open Connection For "+connectionType.name());
        return connection;
    }

    public static void closeConnection(Connection connection,ConnectionType connectionType) {
        if(connection!=null){
            try {
                switch(connectionType) {
                    case READ:
                        DBMR.getInstance().freeConnection(connection);
                        break;
                    case WRITE:
                        DBMW.getInstance().freeConnection(connection);
                        break;
                    case LOG:
                        DBML.getInstance().freeConnection(connection);
                        break;
                }
                //logger.debug("Closed Connection For "+connectionType.name());
            } catch (Exception e) {
                logger.error("Exception is occurred during to do free connection "+e);
                logger.error(e);
            }
        }
    }

    public static <T> T getT(String sql, ConsumerWithThrow<PreparedStatement> psConsumerWithThrow,
                             FunctionWithThrow<ResultSet,T,Exception> transformToT){
        List<T> list = getListOfT(sql, psConsumerWithThrow,transformToT);
        return list.size() == 0 ? null : list.get(0);
    }

    public static <T> List<T> getListOfT(String sql, ConsumerWithThrow<PreparedStatement> psConsumerWithThrow,
                                         FunctionWithThrow<ResultSet,T,Exception> transformToT){
        try {
            return (List<T>) getReadPrepareStatement(ps->{
                List<T> list = new ArrayList<>();
                psConsumerWithThrow.accept(ps);
                try {
                    logger.debug(ps);
                    ResultSet rs = ps.executeQuery();
                    while (rs.next()){
                        T t = transformToT.apply(rs);
                        if(t!=null){
                            list.add(t);
                        }
                    }
                } catch (SQLException ex) {
                    logger.error("",ex);
                }
                return list;
            },sql);
        } catch (Exception e) {
            logger.error(e);
            return new ArrayList<>();
        }
    }

    public static <T> T getT(String sql, FunctionWithThrow<ResultSet,T,Exception> transformToT){
        return getT(sql,transformToT,null);
    }

    public static <T> T getT(String sql, FunctionWithThrow<ResultSet,T,Exception> transformToT, T defaultValue){
        List<T> list = getListOfT(sql,transformToT);
        return list.size() == 0 ? defaultValue : list.get(0);
    }

    public static <T> T getT(String sql,List<Object>objectList, FunctionWithThrow<ResultSet,T,Exception> transformToT){
        return getT(sql, objectList, transformToT,null);
    }

    public static <T> T getT(String sql,List<Object>objectList, FunctionWithThrow<ResultSet,T,Exception> transformToT,
                             T defaultValue){
        List<T> list = getListOfT(sql,objectList,transformToT);
        return list.size() == 0 ? defaultValue : list.get(0);
    }

    public static <T> List<T> getListOfT(String sql, FunctionWithThrow<ResultSet,T,Exception> transformToT){
        try {
            return (List<T>)getReadStatement(st->{
                List<T> list = new ArrayList<>();
                try {
                    logger.debug("sql : "+sql);
                    ResultSet rs = st.executeQuery(sql);
                    while (rs.next()){
                        T t = transformToT.apply(rs);
                        if(t!=null){
                            list.add(t);
                        }
                    }
                } catch (SQLException ex) {
                    logger.error("",ex);
                }
                return list;
            });
        } catch (Exception e) {
            logger.error(e);
            return new ArrayList<>();
        }
    }

    public static <T> List<T> getListOfT(String sql,List<Object>objectList,
                                         FunctionWithThrow<ResultSet,T,Exception> transformToT){
        try {
            return (List<T>)getReadPrepareStatement(ps->{
                List<T> list = new ArrayList<>();
                try {
                    if(objectList!=null && objectList.size()>0){
                        for(int i = 1;i<=objectList.size();i++){
                            ps.setObject(i,objectList.get(i-1));
                        }
                    }
                    logger.debug(ps);
                    ResultSet rs = ps.executeQuery();
                    while (rs.next()){
                        T t = transformToT.apply(rs);
                        if(t!=null){
                            list.add(t);
                        }
                    }
                } catch (SQLException ex) {
                    logger.error("",ex);
                }
                return list;
            },sql);
        } catch (Exception e) {
            logger.error(e);
            return new ArrayList<>();
        }
    }

    public static <T> List<T> getDTOListByNumbers(String sqlQuery,List<? extends Number>list,
                                                  FunctionWithThrow<ResultSet,T,Exception> transformToT){
        return Utils.getSubList(list,500)
                .stream()
                .map(ls->ls.stream().map(String::valueOf).collect(Collectors.joining(",")))
                .map(ids->String.format(sqlQuery, ids))
                .map(sql->getListOfT(sql, transformToT))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public static <T,U> List<T> getDTOList(String sqlQuery,List<U>list, FunctionWithThrow<ResultSet,T,Exception> transformToT) {
        return Utils.getSubList(list,500)
                .stream()
                .map(ConnectionAndStatementUtil::joinByComma)
                .map(ids->String.format(sqlQuery, ids))
                .map(sql->getListOfT(sql, transformToT))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public static <U> String joinByComma(List<U>list){
        return list.stream()
                .map(String::valueOf)
                .map(e->"'".concat(e).concat("'"))
                .collect(Collectors.joining(","));
    }

    public static void executeByNumberList(String sqlQuery,List<? extends Number>list) throws Exception{
        Utils.getSubList(list,500)
                .stream()
                .map(ls->ls.stream().map(String::valueOf).collect(Collectors.joining(",")))
                .map(ids->String.format(sqlQuery, ids))
                .forEach(sql-> {
                    try {
                        getWriteStatement(st->{
                            st.executeUpdate(sql);
                        });
                    } catch (Exception e) {
                        logger.error(e);
                    }
                });
    }

    public static <U>void executeByList(String sqlQuery,List<U>list) throws Exception{
        Utils.getSubList(list,500)
                .stream()
                .map(SQLUtil::joinByComma)
                .map(ids->String.format(sqlQuery, ids))
                .forEach(sql-> {
                    try {
                        getWriteStatement(st->{
                            st.executeUpdate(sql);
                        });
                    } catch (Exception e) {
                        logger.error(e);
                    }
                });
    }
}
