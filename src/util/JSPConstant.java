package util;
/**
 * @author Kayesh Parvez
 *
 */
public class JSPConstant {
	
	
	public static final String ALL_EXPLORATION_DETAILS = "inbox_details/AllExplorationDetails.jsp";
	public static final String AllMinesDetails = "inbox_details/AllMinesDetails.jsp";
	public static final String AllPrivateQuarryDetails = "inbox_details/AllPrivateQuarryDetails.jsp";
	public static final String AllPublicQuarryDetails = "inbox_details/AllPublicQuarryDetails.jsp";
	public static final String DcmCommitteeDetails = "inbox_details/DcmCommitteeDetails.jsp";
	public static final String DcmRecommendationDetails = "inbox_details/DcmRecommendationDetails.jsp";
	public static final String DEmarcationCommitteeDetails = "inbox_details/DEmarcationCommitteeDetails.jsp";
	public static final String DemarcationCommitteeReportDetails = "inbox_details/DemarcationCommitteeReportDetails.jsp";
	public static final String ExplorationApplicantDetails = "inbox_details/ExplorationApplicantDetails.jsp";
	public static final String LeaseeHistoryDetails = "inbox_details/LeaseeHistoryDetails.jsp";
	public static final String LicenseeHistoryDetails = "inbox_details/LicenseeHistoryDetails.jsp";
	public static final String MineApplicantDetails = "inbox_details/MineApplicantDetails.jsp";
	public static final String PaymentDetails = "inbox_details/PaymentDetails.jsp";
	public static final String PaymentDueDetails = "inbox_details/PaymentDueDetails.jsp";
	public static final String PaymentHistoryDetails = "inbox_details/PaymentHistoryDetails.jsp";
	public static final String PaymentDueListDetails = "inbox_details/PaymentDueListDetails.jsp";
	public static final String PrivateQuarryApplicantDetails = "inbox_details/PrivateQuarryApplicantDetails.jsp";
	public static final String PublicQuarryApplicantDetails = "inbox_details/PublicQuarryApplicantDetails.jsp";
	public static final String SingleExplorationLicensingDetails = "inbox_details/SingleExplorationLicensingDetails.jsp";
	public static final String SingleMineLeasingDetails = "inbox_details/SingleMineLeasingDetails.jsp";
	public static final String SinglePrivateQuarryLeasingDetails = "inbox_details/SinglePrivateQuarryLeasingDetails.jsp";
	public static final String SinglePublicQuarryLeasingDetails = "inbox_details/SinglePublicQuarryLeasingDetails.jsp";
	public static final String TenderOpeningCommitteeDetails = "inbox_details/TenderOpeningCommitteeDetails.jsp";
	public static final String TenderOpeningDetails = "inbox_details/TenderOpeningDetails.jsp";
	public static final String TenderPublicationDetails = "inbox_details/TenderPublicationDetails.jsp";
	public static final String TenderSummeryDetails = "inbox_details/TenderSummeryDetails.jsp";
	public static final String ValueFixationCommitteeDetails = "inbox_details/ValueFixationCommitteeDetails.jsp";
	public static final String ValueFixationCommitteeReportDetails = "inbox_details/ValueFixationCommitteeReportDetails.jsp";

	public static final String PUBLIC_SERVLET = "PublicServlet";
	public static final String PUBLIC_SEARCH_SERVLET = PUBLIC_SERVLET + "?" + ActionTypeConstant.ACTION_TYPE + "=" + ActionTypeConstant.PUBLIC_SEARCH;


	public static final String MAP = "map/map.jsp";
	public static final String USER_SERVLET = "UserServlet";
	public static final String ERROR_GLOBAL = "/common/error_page.jsp";
	public static final String USER_EDIT = "users/userEdit.jsp";
	public static final String USER_SEARCH_SERVLET = USER_SERVLET + "?" + ActionTypeConstant.ACTION_TYPE + "=" + ActionTypeConstant.USER_SEARCH;
	public static final String USER_SEARCH = "users/userSearch.jsp";
	public static final String USER_CHANGE_PASSWORD = "users/userChangePassword.jsp";
	
	public static final String ROLE_SERVLET = "RoleServlet";
	public static final String ROLE_EDIT = "roles/roleEdit.jsp";
	public static final String ROLE_GET_EDIT_PAGE_SERVLET = ROLE_SERVLET + "?" + ActionTypeConstant.ACTION_TYPE + "=" + ActionTypeConstant.ROLE_GET_EDIT_PAGE;
	public static final String ROLE_SEARCH_SERVLET = ROLE_SERVLET + "?" + ActionTypeConstant.ACTION_TYPE + "=" + ActionTypeConstant.ROLE_SEARCH;
	public static final String ROLE_SEARCH = "roles/roleSearch.jsp";
	
	public static final String LANGUAGE_SERVLET = "LanguageServlet";
	public static final String LANGUAGE_SEARCH = "language/languageSearch.jsp";
	public static final String LANGUAGE_SEARCH_SERVLET = LANGUAGE_SERVLET + "?" + ActionTypeConstant.ACTION_TYPE + "=" + ActionTypeConstant.LANGUAGE_SEARCH;
	
	public static final String IMAGE_SETTING_PAGE = "image/imageSetting.jsp";
	public static final String PERFORMANCE_LOG_REPORT = "/report/performance_log/newPerformanceLogReport.jsp";
	
	public static final String REPORT = "/report/report.jsp";
	
	public static final String PERFORMANCE_LOG_SUMMARY_REPORT = "/report/performance_log/performanceLogSummary.jsp";
}
