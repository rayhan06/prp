package util;

import approval_execution_table.Approval_execution_tableDAO;
import common.ConnectionType;
import common.*;
import dbm.DBMW;
import org.apache.log4j.Logger;
import pb.Utils;
import pb_notifications.Pb_notificationsDAO;
import repository.RepositoryManager;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.function.ConsumerWithThrow;

import javax.servlet.http.HttpServletRequest;
import java.sql.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings({"rawtypes", "unused", "Duplicates"})
public abstract class NavigationService4 implements VbSequencerService {

    public static final int GETIDS = 1;
    public static final int GETDTOS = 2;
    public static final int GETCOUNT = 3;
    public static final ThreadLocal<Connection> CONNECTION_THREAD_LOCAL = new ThreadLocal<>();

    public static final ThreadLocal<CacheUpdateModel> CACHE_UPDATE_MODEL_THREAD_LOCAL = new ThreadLocal<>();

    public String tableName;
    public String joinSQL = "";
    public String servletName;
    public CommonMaps commonMaps = null;
    public UserDTO userDTO = null;
    public static final Logger logger = Logger.getLogger(NavigationService4.class);
    public static String context = "";
    public String[] columnNames;

    public boolean useSafeSearch = false;

    Approval_execution_tableDAO approval_execution_tableDAO;
    Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();
    public CommonMaps approvalMaps;
    public ApprovalOperationsService approvalOperationsService;

    public NavigationService4(String tableName) {
        this.tableName = tableName;
        servletName = tableName.substring(0, 1).toUpperCase() + tableName.substring(1) + "Servlet";
        if (!tableName.equals("approval_execution_table")) {
            approval_execution_tableDAO = new Approval_execution_tableDAO("approval_execution_table");
        } else {
            approval_execution_tableDAO = (Approval_execution_tableDAO) this;
        }
        approvalOperationsService = new ApprovalOperationsService(this);
    }

    public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException {

    }


    public long update(CommonDTO commonDTO) throws Exception {
        if (CONNECTION_THREAD_LOCAL.get() == null) {
            Connection connection = null;
            try {
                connection = ConnectionAndStatementUtil.getConnection(ConnectionType.WRITE);
                connection.setAutoCommit(false);
                long id = update(commonDTO, connection);
                connection.commit();
                return id;
            } catch (Exception ex) {
                if (connection != null) {
                    connection.rollback();
                }
                throw ex;
            } finally {
                if (connection != null) {
                    connection.setAutoCommit(true);
                    ConnectionAndStatementUtil.closeConnection(connection, ConnectionType.WRITE);
                }
            }
        } else {
            return update(commonDTO, CONNECTION_THREAD_LOCAL.get());
        }
    }

    private long update(CommonDTO commonDTO, Connection connection) throws Exception {
        AtomicReference<Exception> atomicReference = new AtomicReference<>();
        long lastModificationTime = System.currentTimeMillis();
        StringBuilder sqlBuilder = new StringBuilder("UPDATE ").append(tableName).append(" SET ");
        for (String columnName : columnNames) {
            if (!columnName.equals("ID") && !columnName.equals("lastModificationTime") && !columnName.equals("isDeleted")) {
                sqlBuilder.append(columnName).append(" = ?, ");
            }
        }
        sqlBuilder.append("lastModificationTime = ").append(lastModificationTime);
        sqlBuilder.append(" WHERE ID = ").append(commonDTO.iD);
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                doSet(ps, commonDTO, false);
                logger.debug(ps);
                ps.executeUpdate();
                recordUpdateTime(connection, lastModificationTime);
            } catch (Exception ex) {
                ex.printStackTrace();
                atomicReference.set(ex);
            }
        }, connection, sqlBuilder.toString());
        if (atomicReference.get() != null) {
            throw atomicReference.get();
        }
        return commonDTO.iD;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        if (CONNECTION_THREAD_LOCAL.get() == null) {
            Connection connection = null;
            try {
                connection = ConnectionAndStatementUtil.getConnection(ConnectionType.WRITE);
                connection.setAutoCommit(false);
                long id = add(commonDTO, connection);
                connection.commit();
                return id;
            } catch (Exception ex) {
                if (connection != null) {
                    connection.rollback();
                }
                throw ex;
            } finally {
                if (connection != null) {
                    connection.setAutoCommit(true);
                    ConnectionAndStatementUtil.closeConnection(connection, ConnectionType.WRITE);
                }
            }
        } else {
            return add(commonDTO, CONNECTION_THREAD_LOCAL.get());
        }
    }

    public long add(CommonDTO commonDTO, Connection connection) throws Exception {
        commonDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);
        AtomicReference<Exception> atomicReference = new AtomicReference<>();
        StringBuilder sqlBuilder = new StringBuilder("INSERT INTO ")
                .append(tableName)
                .append(" (")
                .append(String.join(",", columnNames))
                .append(") VALUES(");
        String[] questionMarksArr = new String[columnNames.length];
        Arrays.fill(questionMarksArr, "?");
        sqlBuilder.append(String.join(",", questionMarksArr)).append(")");
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                doSet(ps, commonDTO, true);
                logger.debug(ps);
                ps.execute();
                if (commonDTO.lastModificationTime == 0) {
                    recordUpdateTime(connection, System.currentTimeMillis());
                } else {
                    recordUpdateTime(connection, commonDTO.lastModificationTime);
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
                atomicReference.set(ex);
            }
        }, connection, sqlBuilder.toString());
        if (atomicReference.get() != null) {
            throw atomicReference.get();
        }
        return commonDTO.iD;
    }

    public long directlyAdd(CommonDTO commonDTO, int operationOrAction, long id) {
        commonDTO.isDeleted = 0;
        long idToReturn = -1;
        try {
            if (operationOrAction == SessionConstants.INSERT) {

                logger.debug("Inserting into real table");
                idToReturn = add(commonDTO);
            } else if (operationOrAction == SessionConstants.UPDATE) {
                if (commonDTO.jobCat != -1) {
                    return -1;
                }
                logger.debug("Updating into real table");
                idToReturn = update(commonDTO);
            } else if (operationOrAction == SessionConstants.DELETE) {
                if (commonDTO.jobCat != -1) {
                    return -1;
                }
                idToReturn = delete(id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return idToReturn;
    }


    public long manageWriteOperations(CommonDTO commonDTO, int operationOrAction, long id, UserDTO userDTO) {
        return manageWriteOperations(commonDTO, operationOrAction, id, userDTO, "");
    }

    public long manageWriteOperations(CommonDTO commonDTO, int operationOrAction, long id, UserDTO userDTO, String ip) {

        return directlyAdd(commonDTO, operationOrAction, id); // only contains valid id for the primary operations
    }

    public void printSql(String sql) {
        logger.debug("sql: " + sql);
    }

    public void deleteByPreviousRowId(long previousRowId) {
        long lastModificationTime = System.currentTimeMillis();
        StringBuilder sqlBuilder = new StringBuilder("UPDATE ")
                .append(tableName)
                .append(" SET isDeleted=1,lastModificationTime=")
                .append(lastModificationTime)
                .append(" WHERE ID in ( ")
                .append("select updated_row_id from approval_execution_table where isDeleted = 0 and previous_row_id = ")
                .append(previousRowId)
                .append(")");
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            String sql = sqlBuilder.toString();
            Connection connection = model.getConnection();
            Statement stmt = model.getStatement();
            try {
                logger.debug(sql);
                stmt.execute(sql);
                recordUpdateTime(connection, lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
    }

    public long delete(long ID) throws Exception {
        long lastModificationTime = System.currentTimeMillis();
        StringBuilder sqlBuilder = new StringBuilder("UPDATE ")
                .append(tableName)
                .append(" SET isDeleted=1,lastModificationTime=")
                .append(lastModificationTime)
                .append(" WHERE ID = ")
                .append(ID);
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            String sql = sqlBuilder.toString();
            Connection connection = model.getConnection();
            Statement stmt = model.getStatement();
            try {
                logger.debug(sql);
                stmt.execute(sql);
                recordUpdateTime(connection, lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
        return ID;
    }

    public long deleteChildrenByParent(long parentId, String parentColName, long lastModificationTime) throws Exception {
        String sqlBuilder = "UPDATE " +
                tableName +
                " SET isDeleted=1,lastModificationTime=" +
                lastModificationTime + " WHERE " +
                parentColName + " = " +
                parentId;
        executeQuery(sqlBuilder, lastModificationTime);
        return parentId;
    }

    private void executeQuery(String sql, long lastModificationTime) throws Exception {
        if (CommonDAOService.CONNECTION_THREAD_LOCAL.get() != null) {
            executeSQLAndLastModification.accept(new
                    SQLAndLastModification(sql, lastModificationTime));
        } else {
            Utils.handleTransaction(() -> executeSQLAndLastModification.accept(new
                    SQLAndLastModification(sql, lastModificationTime)));
        }
    }

    private static class SQLAndLastModification {
        String sql;
        long lastModification;

        public SQLAndLastModification(String sql, long lastModification) {
            this.sql = sql;
            this.lastModification = lastModification;
        }
    }

    private final ConsumerWithThrow<SQLAndLastModification> executeSQLAndLastModification = (obj) -> {
        AtomicReference<Exception> ar = new AtomicReference<>();
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            try {
                st.executeUpdate(obj.sql);
                recordUpdateTime(CommonDAOService.CONNECTION_THREAD_LOCAL.get(), obj.lastModification);
            } catch (SQLException e) {
                e.printStackTrace();
                ar.set(e);
            }
        }, CommonDAOService.CONNECTION_THREAD_LOCAL.get());

        if (ar.get() != null) {
            throw ar.get();
        }
    };

    public long deleteChildrenByParent(long parentId, String parentColName) throws Exception {
        return deleteChildrenByParent(parentId, parentColName, System.currentTimeMillis());
    }

    public long hardDeleteChildrenByParent(long parentId, String parentColName) throws Exception {
        long lastModificationTime = System.currentTimeMillis();
        StringBuilder sqlBuilder = new StringBuilder("delete from ")
                .append(tableName)
                .append(" WHERE ")
                .append(parentColName).append(" = ")
                .append(parentId);
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            String sql = sqlBuilder.toString();
            Connection connection = model.getConnection();
            Statement stmt = model.getStatement();
            try {
                logger.debug(sql);
                stmt.execute(sql);
                recordUpdateTime(connection, lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
        return parentId;
    }

    public void deleteNChildrenByParent(long parentId, String parentColName, int n) throws Exception {
        long lastModificationTime = System.currentTimeMillis();
        StringBuilder sqlBuilder = new StringBuilder("UPDATE ")
                .append(tableName)
                .append(" SET isDeleted=1,lastModificationTime=")
                .append(lastModificationTime).append(" WHERE ").append(parentColName).append(" = ")
                .append(parentId);
        if (n > 0) {
            sqlBuilder.append(" order by id desc limit ").append(n);
        }
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            String sql = sqlBuilder.toString();
            Connection connection = model.getConnection();
            Statement stmt = model.getStatement();
            try {
                logger.debug(sql);
                stmt.execute(sql);
                recordUpdateTime(connection, lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
    }

    public long hardDelete(long ID) throws Exception {
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            String sql = "delete from " + tableName + " WHERE ID = " + ID;
            logger.debug(sql);
            try {
                st.execute(sql);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
        return ID;
    }


    public void setIsDeleted(long ID, int isDeleted) throws Exception {
        long lastModificationTime = System.currentTimeMillis();
        String sql = "UPDATE " + tableName + " SET isDeleted = " + isDeleted + ",lastModificationTime = " + lastModificationTime + " WHERE ID = " + ID;
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            Connection connection = model.getConnection();
            Statement stmt = model.getStatement();
            try {
                logger.debug(sql);
                stmt.execute(sql);
                recordUpdateTime(connection, lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
    }

    public void setJobCat(long ID, int jobCat) throws Exception {
        long lastModificationTime = System.currentTimeMillis();
        String sql = "UPDATE " + tableName
                + " SET job_cat = " + jobCat + ",lastModificationTime = " + lastModificationTime + " WHERE ID = " + ID;
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            Connection connection = model.getConnection();
            Statement stmt = model.getStatement();
            try {
                logger.debug(sql);
                stmt.execute(sql);
                recordUpdateTime(connection, lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
    }

    public void recordUpdateTime(Connection connection, long lastModificationTime) throws SQLException {
        recordUpdateTime(connection, tableName, lastModificationTime);
    }

    public int getCount(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
                        String filter, boolean tableHasJobCat) {
        if (!useSafeSearch) {
            String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETCOUNT, isPermanentTable, userDTO, filter, tableHasJobCat);
            Integer count = ConnectionAndStatementUtil.getT(sql, rs -> {
                try {
                    return rs.getInt("countID");
                } catch (SQLException ex) {
                    logger.error(ex);
                    return null;
                }
            });
            return count == null ? 0 : count;
        } else {
            List<Object> objectList = new ArrayList<>();
            String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETCOUNT, isPermanentTable, userDTO, filter, tableHasJobCat, objectList);
            Integer count = ConnectionAndStatementUtil.getT(sql, objectList, rs -> {
                try {
                    return rs.getInt("countID");
                } catch (SQLException ex) {
                    logger.error(ex);
                    return null;
                }
            });
            return count == null ? 0 : count;
        }

    }

    public long getLastModificationTime() {
        String sql = "select table_LastModificationTime from vbSequencer where table_name = '" + tableName + "'";
        Long lastModificationTime = ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getLong("table_LastModificationTime");
            } catch (SQLException ex) {
                logger.error(ex);
                return null;
            }
        });
        return lastModificationTime == null ? 0 : lastModificationTime;
    }

    public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, boolean isPermanentTable, UserDTO userDTO) throws Exception {
        if (!useSafeSearch) {
            String sql = getSqlWithSearchCriteria(p_searchCriteria, -1, -1, GETIDS, isPermanentTable, userDTO);
            return ConnectionAndStatementUtil.getListOfT(sql, rs -> {
                try {
                    return rs.getLong("ID");
                } catch (SQLException ex) {
                    logger.error(ex);
                    return null;
                }
            });
        } else {
            List<Object> objectList = new ArrayList<>();
            String sql = getSqlWithSearchCriteria(p_searchCriteria, -1, -1, GETIDS, isPermanentTable, userDTO, "", false, objectList);
            return ConnectionAndStatementUtil.getListOfT(sql, rs -> {
                try {
                    return rs.getLong("ID");
                } catch (SQLException ex) {
                    logger.error(ex);
                    return null;
                }
            });

        }
    }

    public Collection getIDs(String tableName, boolean isPermanentTable, UserDTO userDTO) {
        if (!useSafeSearch) {
            String sql = getSqlWithSearchCriteria(null, -1, -1, GETIDS, isPermanentTable, userDTO);
            return ConnectionAndStatementUtil.getListOfT(sql, rs -> {
                try {
                    return rs.getString("ID");
                } catch (SQLException ex) {
                    logger.error(ex);
                    return null;
                }
            });
        } else {
            List<Object> objectList = new ArrayList<>();
            String sql = getSqlWithSearchCriteria(null, -1, -1, GETIDS, isPermanentTable, userDTO, "", false, objectList);
            return ConnectionAndStatementUtil.getListOfT(sql, rs -> {
                try {
                    return rs.getLong("ID");
                } catch (SQLException ex) {
                    logger.error(ex);
                    return null;
                }
            });
        }
    }

    public CommonDTO getDTOByID(long id) throws Exception {
        String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
        return (CommonDTO) ConnectionAndStatementUtil.getT(sql, this::build);
    }

    public Collection getDTOs(Collection recordIDs) {
        if (recordIDs.isEmpty()) return new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM " + tableName + " WHERE ID IN ( ");
        for (int i = 0; i < recordIDs.size(); i++) {
            if (i != 0) {
                sql.append(",");
            }
            sql.append(((ArrayList) recordIDs).get(i));
        }
        sql.append(")  order by lastModificationTime desc");
        printSql(sql.toString());
        return ConnectionAndStatementUtil.getListOfT(sql.toString(), this::build);
    }

    public Collection getAll(boolean isFirstReload) {
        String sql = "SELECT * FROM " + tableName + " WHERE ";
        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        sql += " order by " + tableName + ".id asc";
        return ConnectionAndStatementUtil.getListOfT(sql, this::build);
    }


    public Collection getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat) {
        List<Object> objectList = new ArrayList<>();
        String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat, objectList);
        return ConnectionAndStatementUtil.getListOfT(sql, objectList, this::build);
    }

    public Collection getDTOsByParent(String parentColumnName, long parentId) {
        String sql = "SELECT * FROM " + tableName + " where isDeleted=0 and " + parentColumnName + " = " + parentId + " order by " + tableName + ".id asc";
        return ConnectionAndStatementUtil.getListOfT(sql, this::build);
    }

    public Collection getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }

    public Object build(ResultSet rs) //Users must override it
    {
        return null;
    }


    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
                                           boolean isPermanentTable, UserDTO userDTO) {
        return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, isPermanentTable, userDTO, "",
                false);

    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
                                           boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList) //Approval Path won't work yet
    {
        return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat, objectList);
    }


    protected String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset,
                                                             int category, UserDTO userDTO2, String filter, boolean tableHasJobCat, List<Object> objectList) //must be overridden
    {
        return "";
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
                                           boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat) {
        boolean viewAll = false;

        if (p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null) {
            logger.debug("ViewAll = " + p_searchCriteria.get("ViewAll"));
            viewAll = true;
        }

        String sql = "SELECT ";
        if (category == GETIDS) {
            sql += " " + tableName + ".ID as ID ";
        } else if (category == GETCOUNT) {
            sql += " count( " + tableName + ".ID) as countID ";
        } else if (category == GETDTOS) {
            sql += " " + tableName + ".* ";
        }
        sql += "FROM " + tableName + " ";
        if (!isPermanentTable) {

            sql += " join approval_execution_table on (" + tableName + ".id = approval_execution_table.previous_row_id "
                    + ")";

            sql += " join approval_summary on (" + tableName
                    + ".id = approval_summary.table_id and  approval_summary.table_name = '" + tableName + "'" + ")";
            if (!viewAll) {

                sql += " join users on (" + "users.id = approval_execution_table.user_id " + ")";

                sql += " join employee_offices on (" + "users.employee_record_id = employee_offices.employee_record_id "
                        + ")";

                sql += " left join approval_path_details on ("
                        + "approval_execution_table.approval_path_id = approval_path_details.approval_path_id "
                        + "and approval_execution_table.approval_path_order = approval_path_details.approval_order "
                        + ")";
            }

        }

        StringBuilder AnyfieldSql = new StringBuilder();
        StringBuilder AllFieldSql = new StringBuilder();

        if (p_searchCriteria != null) {

            Enumeration names = p_searchCriteria.keys();
            String str, value;

            AnyfieldSql = new StringBuilder("(");

            if (p_searchCriteria.get("AnyField") != null
                    && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                sql += joinSQL;
                int i = 0;
                for (Entry<String, String> stringStringEntry : (commonMaps).java_anyfield_search_map.entrySet()) {
                    if (i > 0) {
                        AnyfieldSql.append(" OR  ");
                    }
                    AnyfieldSql.append(((Entry) stringStringEntry).getKey()).append(" like '%").append(p_searchCriteria.get("AnyField").toString()).append("%'");
                    i++;
                }
            }
            AnyfieldSql.append(")");
            logger.debug("AnyfieldSql = " + AnyfieldSql);

            AllFieldSql = new StringBuilder("(");
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                logger.debug(str + ": " + value);
                if ((commonMaps).java_allfield_type_map.get(str.toLowerCase()) != null
                        && !(commonMaps).java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
                        && !str.equalsIgnoreCase("AnyField") && value != null && !value.equalsIgnoreCase("")) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql.append(" AND  ");
                    }
                    if ((commonMaps).java_allfield_type_map.get(str.toLowerCase()).equals("String")) {
                        AllFieldSql.append(tableName).append(".").append(str.toLowerCase()).append(" like '%").append(p_searchCriteria.get(str)).append("%'");
                    } else {
                        AllFieldSql.append(tableName).append(".").append(str.toLowerCase()).append(" = '").append(p_searchCriteria.get(str)).append("'");
                    }
                    i++;
                }
            }

            AllFieldSql.append(")");
            logger.debug("AllFieldSql = " + AllFieldSql);
        }

        sql += " WHERE ";
        if (isPermanentTable) {
            sql += " (" + tableName + ".isDeleted = 0 ";
            sql += ")";
        } else {
            if (viewAll) {
                sql += " (" + tableName + ".isDeleted != 1" + ")";
            } else {
                sql += " (" + tableName + ".isDeleted != 1" + " and (approval_path_details.organogram_id = "
                        + userDTO.organogramID + " or employee_offices.office_unit_organogram_id = "
                        + userDTO.organogramID + ")" + ")";
            }
        }

        if (!filter.equalsIgnoreCase("")) {
            sql += " and " + filter + " ";
        }

        if (!AnyfieldSql.toString().equals("()") && !AnyfieldSql.toString().equals("")) {
            sql += " AND " + AnyfieldSql;

        }
        if (!AllFieldSql.toString().equals("()") && !AllFieldSql.toString().equals("")) {
            sql += " AND " + AllFieldSql;
        }

        sql += " order by " + tableName + ".lastModificationTime desc ";

        printSql(sql);

        if (limit >= 0) {
            sql += " limit " + limit;
        }
        if (offset >= 0) {
            sql += " offset " + offset;
        }
        return sql;
    }

    public List<Long> getChilIds(String parentName, String childName, long parentID) throws Exception {
        String sql = "select id from " + childName + " WHERE " + parentName + "_id=" + parentID + " and isDeleted = 0";
        return ConnectionAndStatementUtil.getListOfT(sql, rs -> {
            try {
                return rs.getLong("id");
            } catch (SQLException ex) {
                logger.error(ex);
                return null;
            }
        });
    }

    public int getIsDeleted(String tableName, long id) throws Exception {
        String sql = "select isDeleted from " + tableName + " WHERE id = " + id;
        Integer isDeleted = ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt("isDeleted");
            } catch (SQLException ex) {
                logger.error(ex);
                return null;
            }
        });
        return isDeleted == null ? 0 : isDeleted;
    }

    public void deleteChildrenNotInList(String parentName, String childName, long parentID, List<Long> chilIds) throws Exception {
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            String sql = "update " + childName + " set isDeleted = 1, lastModificationTime = " + System.currentTimeMillis()
                    + " WHERE " + parentName + "_id=" + parentID;
            if (chilIds != null && chilIds.size() > 0) {
                String ids = chilIds.stream()
                        .map(String::valueOf)
                        .collect(Collectors.joining(","));
                sql += " and id not in (" + ids + ")";
            }
            logger.debug("sql " + sql);
            try {
                st.execute(sql);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
    }

    public void hardDeleteChildrenNotInList(String parentName, String childName, long parentID, List<Long> chilIds) throws Exception {
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            String sql = "delete from " + childName
                    + " WHERE " + parentName + "_id=" + parentID;
            if (chilIds != null && chilIds.size() > 0) {
                String ids = chilIds.stream()
                        .map(String::valueOf)
                        .collect(Collectors.joining(","));
                sql += " and id not in (" + ids + ")";
            }
            logger.debug("sql " + sql);
            try {
                st.execute(sql);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
    }

    public List<Long> getChildIdsFromRequest(HttpServletRequest request, String childDTOName) throws Exception {
        String[] values = request.getParameterValues(childDTOName + ".iD");
        if (values == null || values.length == 0) {
            return null;
        }
        return Stream.of(values)
                .map(Long::parseLong)
                .collect(Collectors.toList());
    }

    public void updateParentTTableID(String childName, long oldParentID, long newParentID) throws Exception {
        String sql = "update " + childName + " set " + tableName + "_id = " + newParentID + " where " + tableName + "_id = " + oldParentID;
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            try {
                logger.debug("sql " + sql);
                st.executeUpdate(sql);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
    }

    public long getCount(String filter, UserDTO userDTO) {
        if (!useSafeSearch) {
            String sql = getSqlWithSearchCriteria(null, -1, -1, GETCOUNT, true, userDTO, filter, true);
            Long count = ConnectionAndStatementUtil.getT(sql, rs -> {
                try {
                    return rs.getLong("countID");
                } catch (SQLException ex) {
                    logger.error(ex);
                    return null;
                }
            });
            return count == null ? 0 : count;
        } else {
            List<Object> objectList = new ArrayList<>();
            String sql = getSqlWithSearchCriteria(null, -1, -1, GETCOUNT, true, userDTO, "", false, objectList);
            Long count = ConnectionAndStatementUtil.getT(sql, rs -> {
                try {
                    return rs.getLong("countID");
                } catch (SQLException ex) {
                    logger.error(ex);
                    return null;
                }
            });
            return count == null ? 0 : count;
        }
    }


    public String getSqlWithSearchCriteriaForApprovalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                            UserDTO userDTO, String filter, boolean tableHasJobCat) {
        return getSqlWithSearchCriteriaForApprovalSearch(p_searchCriteria, limit, offset, category,
                userDTO, filter, tableHasJobCat, null);
    }


    public String getSqlWithSearchCriteriaForApprovalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                            UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList) {

        boolean viewAll = false;

        if (p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null) {
            logger.debug("ViewAll = " + p_searchCriteria.get("ViewAll"));
            viewAll = true;
        } else {
            logger.debug("ViewAll is null ");
        }

        String sql = "SELECT ";
        if (category == GETIDS) {
            sql += " " + tableName + ".ID as ID ";
        } else if (category == GETCOUNT) {
            sql += " count( " + tableName + ".ID) as countID ";
        } else if (category == GETDTOS) {
            sql += "  " + tableName + ".* ";
        }
        sql += "FROM " + tableName + " ";


        sql += " join approval_execution_table as aet on ("
                + tableName + ".id = aet.updated_row_id "
                + ")";

        sql += " join approval_summary on ("
                + "aet.previous_row_id = approval_summary.table_id and  approval_summary.table_name = '" + tableName + "'"
                + ")";

        if (!viewAll) {

            sql += " left join approval_path_details on ("
                    + "aet.approval_path_id = approval_path_details.approval_path_id "
                    + "and aet.approval_path_order = approval_path_details.approval_order "
                    + ")";
        }


        StringBuilder AllFieldSql = new StringBuilder();


        if (p_searchCriteria != null) {


            Enumeration names = p_searchCriteria.keys();
            String str, value;


            AllFieldSql = new StringBuilder("(");
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                if (value == null) {
                    continue;
                }
                logger.debug(str + ": " + value);
                if (approvalMaps.java_allfield_type_map.get(str.toLowerCase()) != null && !approvalMaps.java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("") && !str.equalsIgnoreCase("AnyField") && (!value.equalsIgnoreCase("") || commonMaps.rangeMap.get(str.toLowerCase()) != null)) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql.append(" AND  ");
                    }

                    String dataType = approvalMaps.java_allfield_type_map.get(str.toLowerCase());

                    String fromTable = tableName;
                    if (approvalMaps.java_table_map.get(str) != null) {
                        fromTable = approvalMaps.java_table_map.get(str);
                    }

                    if (str.equalsIgnoreCase("starting_date") || str.equalsIgnoreCase("ending_date")) {
                        if (str.equalsIgnoreCase("starting_date")) {
                            AllFieldSql.append("approval_summary.date_of_initiation >= ?");
                        } else {
                            AllFieldSql.append("approval_summary.date_of_initiation <= ?");
                        }
                        objectList.add(Long.parseLong(p_searchCriteria.get(str).toString()));
                    } else {
                        if (dataType.equals("String")) {
                            AllFieldSql.append(fromTable).append(".").append(str.toLowerCase()).append(" like %");
                            objectList.add("%" + p_searchCriteria.get(str) + "%");
                        } else if (dataType.equals("Integer") || dataType.equals("Long")) {
                            AllFieldSql.append(fromTable).append(".").append(str.toLowerCase()).append(" = ?");
                            objectList.add(Long.parseLong(p_searchCriteria.get(str).toString()));
                        }
                    }


                    i++;
                }
            }

            AllFieldSql.append(")");
            logger.debug("AllFieldSql = " + AllFieldSql);


        }
        sql += " WHERE ";

        if (viewAll) {
            sql += " (" + tableName + ".isDeleted != " + CommonDTO.DELETED + " and " + tableName + ".isDeleted != " + CommonDTO.OUTDATED
                    + ")";
        } else {
            sql += " (" + tableName + ".isDeleted != " + CommonDTO.DELETED + " and " + tableName + ".isDeleted != " + CommonDTO.OUTDATED
                    + " and (approval_path_details.organogram_id = " + userDTO.organogramID
                    + " or " + userDTO.organogramID + " in ("
                    + " select organogram_id from approval_execution_table where previous_row_id = aet.previous_row_id"
                    + "))"
                    + ")";
        }


        if (!filter.equalsIgnoreCase("")) {
            sql += " and " + filter + " ";
        }

        if (!AllFieldSql.toString().equals("()") && !AllFieldSql.toString().equals("")) {
            sql += " AND " + AllFieldSql;
        }


        sql += "  order by " + tableName + ".lastModificationTime desc ";

        printSql(sql);

        if (limit >= 0) {
            sql += " limit " + limit;
        }
        if (offset >= 0) {
            sql += " offset " + offset;
        }

        logger.debug("-------------- sql = " + sql);

        return sql;
    }
}
