package util;

/*
 * @author Md. Erfan Hossain
 * @created 13/07/2021 - 6:02 AM
 * @project parliament
 */

import login.LoginDTO;
import user.UserDTO;

public class CommonLoginData {
    public LoginDTO loginDTO;
    public UserDTO userDTO;
    public String language;
    public boolean isLangEng;
    public String ipAddress;
}
