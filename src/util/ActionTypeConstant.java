package util;
/**
 * @author Kayesh Parvez
 *
 */
public class ActionTypeConstant {
	public static final String ACTION_TYPE = "actionType";
	
	public static final String ALL_EXPLORATION_DETAILS = "AllExplorationDetails";
	public static final String AllMinesDetails = "AllMinesDetails";
	public static final String AllPrivateQuarryDetails = "AllPrivateQuarryDetails";
	public static final String AllPublicQuarryDetails = "AllPublicQuarryDetails";
	public static final String DcmCommitteeDetails = "DcmCommitteeDetails";
	public static final String DcmRecommendationDetails = "DcmRecommendationDetails";
	public static final String DEmarcationCommitteeDetails = "DEmarcationCommitteeDetails";
	public static final String DemarcationCommitteeReportDetails = "DemarcationCommitteeReportDetails";
	public static final String ExplorationApplicantDetails = "ExplorationApplicantDetails";
	public static final String LeaseeHistoryDetails = "LeaseeHistoryDetails";
	public static final String LicenseeHistoryDetails = "LicenseeHistoryDetails";
	public static final String MineApplicantDetails = "MineApplicantDetails";
	public static final String PaymentDetails = "PaymentDetails";
	public static final String PaymentHistoryDetails = "PaymentHistoryDetails";
	public static final String PaymentDueDetails = "PaymentDueDetails";
	public static final String PrivateQuarryApplicantDetails = "PrivateQuarryApplicantDetails";
	public static final String PublicQuarryApplicantDetails = "PublicQuarryApplicantDetails";
	public static final String SingleExplorationLicensingDetails = "SingleExplorationLicensingDetails";
	public static final String SingleMineLeasingDetails = "SingleMineLeasingDetails";
	public static final String SinglePrivateQuarryLeasingDetails = "SinglePrivateQuarryLeasingDetails";
	public static final String SinglePublicQuarryLeasingDetails = "SinglePublicQuarryLeasingDetails";
	public static final String TenderOpeningCommitteeDetails = "TenderOpeningCommitteeDetails";
	public static final String TenderOpeningDetails = "TenderOpeningDetails";
	public static final String TenderPublicationDetails = "TenderPublicationDetails";
	public static final String TenderSummeryDetails = "TenderSummeryDetails";
	public static final String ValueFixationCommitteeDetails = "ValueFixationCommitteeDetails";
	public static final String ValueFixationCommitteeReportDetails = "ValueFixationCommitteeReportDetails";
	public static final String PaymentDueListDetails = "PaymentDueListDetails";
	
	
	
	
	public static final String MAP = "map";

	public static final String PUBLIC_SEARCH = "public_search";

	public static final String USER_GET_ADD_PAGE = "getAddPage";
	public static final String USER_GET_EDIT_PAGE = "getEditPage";
	public static final String USER_ADD = "add";
	public static final String USER_EDIT = "edit";
	public static final String USER_SEARCH = "search";
	public static final String USER_DELETE = "delete";
	public static final String USER_GET_CHANGE_PASSWORD = "getChangePassword";
	public static final String USER_CHANGE_PASSWORD = "changePassword";
	
	public static final String ROLE_GET_ADD_PAGE = "getAddPage";
	public static final String ROLE_GET_EDIT_PAGE = "getEditPage";
	public static final String ROLE_ADD = "add";
	public static final String ROLE_EDIT = "edit";
	public static final String ROLE_SEARCH = "search";
	public static final String ROLE_DELETE = "delete";
	
	public static final String LANGUAGE_ADD = "add";
	public static final String LANGUAGE_EDIT = "edit";
	public static final String LANGUAGE_SEARCH = "search";
	public static final String LANGUAGE_DELETE = "delete";
	
	public static final String USER_SEARCH_FOR_OTP = "search_user";
	
	public static final String REPORT_PAGE = "reportPage";
	public static final String REPORT_PAGE_SUMMARY = "reportPageSummary";
	public static final String REPORT_COUNT = "reportCount";
	public static final String REPORT_PAGINATION = "reportPagination";
	public static final String REPORT_RESULT = "reportResult";
	public static final String REPORT_TEMPLATE = "reportTemplate";
	public static final String DTO_FIELD_LIST = "dtoFieldList";
	public static final String DEFAULT_FIELD_LIST = "defaultFieldList";
	public static final String EXCEL_REPORT = "xl";
}
