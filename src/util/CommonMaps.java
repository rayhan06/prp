package util;

import java.util.HashMap;
import java.util.List;

public class CommonMaps {
	
	public HashMap<String, String> java_allfield_type_map = new HashMap<>();
	public HashMap<String, String> java_anyfield_search_map = new HashMap<>();
	public HashMap<String, String> java_DTO_map = new HashMap<>();
	public HashMap<String, String> java_SQL_map = new HashMap<>();
	public HashMap<String, String> java_Text_map = new HashMap<>();
	public HashMap<String, String> java_table_map = new HashMap<>();

	public HashMap<String, List<Range>> rangeMap = new HashMap<>();
	public HashMap<String, String> dateFormat = new HashMap<>();

	public enum Range {

		FROM,TO
    }

}
