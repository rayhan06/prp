package recruitment_job_summary;

import com.google.gson.Gson;
import config.GlobalConfigConstants;
import config.GlobalConfigurationRepository;
import employee_pay_scale.Employee_pay_scaleDTO;
import employee_pay_scale.Employee_pay_scaleRepository;
import job_applicant_application.CandidateCountDTO;
import job_applicant_application.Job_applicant_applicationDAO;
import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import pb.CatRepository;
import pb.Utils;
import recruitment_job_description.Recruitment_job_descriptionDAO;
import recruitment_job_description.Recruitment_job_descriptionDTO;
import recruitment_job_description.Recruitment_job_descriptionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.RecordNavigationManager4;
import util.UtilCharacter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Servlet implementation class Recruitment_job_descriptionServlet
 */
@WebServlet("/RecruitmentJobSummaryServlet")
@MultipartConfig
public class RecruitmentJobSummaryServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(RecruitmentJobSummaryServlet.class);
	private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RecruitmentJobSummaryServlet()
	{
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		try
		{

			String actionType = request.getParameter("actionType");
			if(actionType.equals("job_summary"))
			{
				// permission check missing

				request.getRequestDispatcher("recruitment_job_summary/recruitment_job_summary.jsp").forward(request, response);

			}

			else if(actionType.equals("getData")){
				long jobId = Long.parseLong(request.getParameter("jobId"));
				String status = request.getParameter("status");

				String filter = " isDeleted = 0 ";
				if(jobId != -1){
					filter += " and  ID = " + jobId + " ";
				}

				if(!"ALL".equals(status)){
					Recruitment_job_descriptionDAO jobDescriptionDAO = new Recruitment_job_descriptionDAO();
					if(!jobDescriptionDAO.statusList.contains(status)){
						status = "";
					}
					filter += " and status like  '" + status + "' ";
				}

				searchRecruitment_job_description(request, response, true, filter);
			}

			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	private void searchRecruitment_job_description(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchRecruitment_job_description 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

		Recruitment_job_descriptionDAO recruitment_job_descriptionDAO =
				new Recruitment_job_descriptionDAO();
		RecordNavigationManager4 rnManager = new RecordNavigationManager4(
				SessionConstants.NAV_RECRUITMENT_JOB_DESCRIPTION,
				request,
				recruitment_job_descriptionDAO,
				SessionConstants.VIEW_RECRUITMENT_JOB_DESCRIPTION,
				SessionConstants.SEARCH_RECRUITMENT_JOB_DESCRIPTION,
				"recruitment_job_description",
				isPermanent,
				userDTO,
				filter,
				true);
		try
		{
			System.out.println("trying to dojob");
			rnManager.doJob(loginDTO);
		}
		catch(Exception e)
		{
			System.out.println("failed to dojob" + e);
		}

		request.setAttribute("recruitment_job_descriptionDAO",recruitment_job_descriptionDAO);
		RequestDispatcher rd;
		System.out.println("Going to recruitment_job_summary/recruitment_job_descriptionSearchForm.jsp");
		rd = request.getRequestDispatcher("recruitment_job_summary/recruitment_job_descriptionSearchForm.jsp");
		rd.forward(request, response);
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);



		try {
			String actionType = request.getParameter("actionType");

				if (actionType.equals("DownloadTemplate")) {
					long jobId = Long.parseLong(request.getParameter("job"));
					String status = request.getParameter("status");
					int languageID = LM.getLanguageIDByUserDTO(userDTO);
					String Language = languageID == 1?"english":"bangla";

					Recruitment_job_descriptionDAO jobDescriptionDAO = new Recruitment_job_descriptionDAO();

					List<Recruitment_job_descriptionDTO> jobDescriptionDTOS =
							Recruitment_job_descriptionRepository.getInstance().getRecruitment_job_descriptionList();

					String filter = " isDeleted = 0 ";
					if(jobId != -1){
//						filter += " and  ID = " + jobId + " ";
						jobDescriptionDTOS = jobDescriptionDTOS.stream()
								.filter(i -> i.iD == jobId).collect(Collectors.toList());

					}

					if(!"ALL".equals(status)){
//						filter += " and status like  '" + status + "' ";
						if(!jobDescriptionDAO.statusList.contains(status)){
							status = "";
						}
						String currentStatus = status;
						jobDescriptionDTOS = jobDescriptionDTOS.stream().filter(i -> i.status.equals(currentStatus))
								.collect(Collectors.toList());
					}

//					List<Recruitment_job_descriptionDTO> jobDescriptionDTOS =
//							jobDescriptionDAO.getByFilter(filter);


//					System.out.println("###");
//					System.out.println(jobId);
//					System.out.println(status);
//					System.out.println(jobDescriptionDTOS);
//					System.out.println("###");


					String file_name = "recruitment_summary.xlsx";
					response.setContentType("application/vnd.ms-excel");
					response.setHeader("Content-Disposition", "attachment; filename="+file_name);

					XSSFWorkbook workbook    = UtilCharacter.newWorkBook();
					Sheet sheet = UtilCharacter.newSheet(workbook, LM.getText(LC.CANDIDATE_LIST_POST_SUMMARY,loginDTO));
					int row_index=0;
					int cell_col_index=0;
					boolean addMerged=false;
					Row newRow = sheet.createRow(row_index);
					IndexedColors indexedColor = IndexedColors.ROYAL_BLUE;
					boolean isBold=true;


					UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.CANDIDATE_LIST_SERIAL,loginDTO),row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,true);
					UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.CANDIDATE_LIST_POST_NAME_SUMMARY,loginDTO),row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,true);
					UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.CANDIDATE_LIST_SALARY_SCALE,loginDTO),row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,true);

					UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBPURPOSE,loginDTO),row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,true);
					UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_NUMBEROFVACANCY,loginDTO),row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,true);
					UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.CANDIDATE_LIST_APPLIED,loginDTO),row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,true);
					UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.CANDIDATE_LIST_ACCEPTED,loginDTO),row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,true);
					UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.CANDIDATE_LIST_DISCARDED,loginDTO),row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,true);

					indexedColor = IndexedColors.BLACK;
					isBold=false;

					Recruitment_job_descriptionDTO dto;

					for(int i = 0; i <  jobDescriptionDTOS.size(); i++) {
						dto  = jobDescriptionDTOS.get(i);
						row_index++;
						cell_col_index = 0;
						newRow = sheet.createRow(row_index);

						CandidateCountDTO candidateCountDTO = new Job_applicant_applicationDAO().getApplicantCount(dto.iD);



						UtilCharacter.alreadyCreatedRow(workbook,sheet, Utils.getDigits((i + 1) + "", Language),row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,isBold);
						UtilCharacter.alreadyCreatedRow(workbook,sheet, UtilCharacter.getDataByLanguage(Language,
								dto.jobTitleBn, dto.jobTitleEn),row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,isBold);

						String grade = CatRepository.getName(Language, "job_grade", dto.jobGradeCat);
						String payScale = "";
						Employee_pay_scaleDTO employee_pay_scaleDTO = Employee_pay_scaleRepository.getInstance().getEmployee_pay_scaleDTOByID(dto.employeePayScaleType);
						if(employee_pay_scaleDTO != null){
							payScale = CatRepository.getName(Language, "national_pay_scale_type", employee_pay_scaleDTO.nationalPayScaleCat);

						}

						String gradeAndPayScale = grade + " (" + payScale + " )";
						String jobPurpose = dto.jobPurpose;
						String numberVac = Utils.getDigits(dto.numberOfVacancy + "", Language);

						long internShipId = Long.parseLong(GlobalConfigurationRepository.
								getGlobalConfigDTOByID(GlobalConfigConstants.INTERNSHIP_ID).value);

						if(dto.iD == internShipId){
							gradeAndPayScale = "";
							jobPurpose = "";
							numberVac = "";
						}

						UtilCharacter.alreadyCreatedRow(workbook,sheet, gradeAndPayScale,row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,isBold);

						UtilCharacter.alreadyCreatedRow(workbook,sheet, jobPurpose ,row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,isBold);

						UtilCharacter.alreadyCreatedRow(workbook,sheet, numberVac,row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,isBold);

						UtilCharacter.alreadyCreatedRow(workbook,sheet, Utils.getDigits(candidateCountDTO.applied + "", Language),row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,isBold);

						UtilCharacter.alreadyCreatedRow(workbook,sheet, Utils.getDigits(candidateCountDTO.accepted + "", Language),row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,isBold);

						UtilCharacter.alreadyCreatedRow(workbook,sheet, Utils.getDigits(candidateCountDTO.declined + "", Language),row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,isBold);


					}







					workbook.write(response.getOutputStream()); // Write workbook to response.
					workbook.close();

				}

		    }
			catch(Exception ex)
			{
				ex.printStackTrace();
				logger.debug(ex);
			}

	}


}

