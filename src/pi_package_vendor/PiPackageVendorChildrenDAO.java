package pi_package_vendor;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import user.UserDTO;
import util.CommonDTO;
import vm_requisition.CommonApprovalStatus;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PiPackageVendorChildrenDAO implements CommonDAOService<PiPackageVendorChildrenDTO> {
    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private String[] columnNames = null;

    private static final String deletePiPackageVendorChildren =
            "UPDATE pi_package_vendor_children SET isDeleted = %d,modified_by = %d,lastModificationTime = %d WHERE pi_package_vendor_id = %d ";

    private static final String getVendorByPackageAndFiscalYearIdAndOfficeUnitIdAndWinner =
            "SELECT * FROM pi_package_vendor_children WHERE  fiscal_year_id = %d AND" +
                    " package_id = %d AND is_chosen = 1 AND vendor_status = %d AND office_unit_id = %d  AND isDeleted = 0 ORDER BY lastModificationTime DESC";

    private static final String getVendorByPackageAndFiscalYearIdAndAndWinner =
            "SELECT * FROM pi_package_vendor_children WHERE  fiscal_year_id = %d AND" +
                    " package_id = %d AND is_chosen = 1 AND vendor_status = %d AND isDeleted = 0 ORDER BY lastModificationTime DESC";

    private static final String getVendorByPackageAndFiscalYearIdAndLotIdAndWinner =
            "SELECT * FROM pi_package_vendor_children WHERE  fiscal_year_id = %d AND" +
                    " package_id = %d And lot_id = %d AND is_chosen = 1 AND vendor_status = %d AND isDeleted = 0 ORDER BY lastModificationTime DESC";

    private static final String getVendorByFiscalYearIdAndOfficeUnitIdAndWinner =
            "SELECT * FROM pi_package_vendor_children WHERE  fiscal_year_id = %d AND " +
                    "  is_chosen = 1 AND vendor_status = %d AND office_unit_id = %d  AND isDeleted = 0 ORDER BY lastModificationTime DESC";

    private PiPackageVendorChildrenDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "pi_package_vendor_id",
                        "name",
                        "address",
                        "mobile",
                        "is_chosen",
                        "insertion_date",
                        "inserted_by",
                        "modified_by",
                        "search_column",
                        "fiscal_year_id",
                        "package_id",
                        "lot_id",
                        "vendor_status",
                        "office_unit_id",
                        "actual_vendor_id",
                        "isDeleted",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("insertion_date_start", " and (insertion_date >= ?)");
        searchMap.put("insertion_date_end", " and (insertion_date <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final PiPackageVendorChildrenDAO INSTANCE = new PiPackageVendorChildrenDAO();
    }

    public static PiPackageVendorChildrenDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(PiPackageVendorChildrenDTO pipackagevendorchildrenDTO) {
        pipackagevendorchildrenDTO.searchColumn = "";
        pipackagevendorchildrenDTO.searchColumn += pipackagevendorchildrenDTO.name + " ";
        pipackagevendorchildrenDTO.searchColumn += pipackagevendorchildrenDTO.mobile + " ";
    }

    @Override
    public void set(PreparedStatement ps, PiPackageVendorChildrenDTO pipackagevendorchildrenDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(pipackagevendorchildrenDTO);
        if (isInsert) {
            ps.setObject(++index, pipackagevendorchildrenDTO.iD);
        }
        ps.setObject(++index, pipackagevendorchildrenDTO.piPackageVendorId);
        ps.setObject(++index, pipackagevendorchildrenDTO.name);
        ps.setObject(++index, pipackagevendorchildrenDTO.address);
        ps.setObject(++index, pipackagevendorchildrenDTO.mobile);
        ps.setObject(++index, pipackagevendorchildrenDTO.isChosen);
        ps.setObject(++index, pipackagevendorchildrenDTO.insertionDate);
        ps.setObject(++index, pipackagevendorchildrenDTO.insertedBy);
        ps.setObject(++index, pipackagevendorchildrenDTO.modifiedBy);
        ps.setObject(++index, pipackagevendorchildrenDTO.searchColumn);
        ps.setObject(++index, pipackagevendorchildrenDTO.fiscalYearId);
        ps.setObject(++index, pipackagevendorchildrenDTO.packageId);
        ps.setObject(++index, pipackagevendorchildrenDTO.lotId);
        ps.setObject(++index, pipackagevendorchildrenDTO.vendorStatus);
        ps.setObject(++index, pipackagevendorchildrenDTO.officeUnitId);
        ps.setObject(++index, pipackagevendorchildrenDTO.actualVendorId);
        if (isInsert) {
            ps.setObject(++index, pipackagevendorchildrenDTO.isDeleted);
        }
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, pipackagevendorchildrenDTO.iD);
        }
    }

    @Override
    public PiPackageVendorChildrenDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            PiPackageVendorChildrenDTO pipackagevendorchildrenDTO = new PiPackageVendorChildrenDTO();
            int i = 0;
            pipackagevendorchildrenDTO.iD = rs.getLong(columnNames[i++]);
            pipackagevendorchildrenDTO.piPackageVendorId = rs.getLong(columnNames[i++]);
            pipackagevendorchildrenDTO.name = rs.getString(columnNames[i++]);
            pipackagevendorchildrenDTO.address = rs.getString(columnNames[i++]);
            pipackagevendorchildrenDTO.mobile = rs.getString(columnNames[i++]);
            pipackagevendorchildrenDTO.isChosen = rs.getBoolean(columnNames[i++]);
            pipackagevendorchildrenDTO.insertionDate = rs.getLong(columnNames[i++]);
            pipackagevendorchildrenDTO.insertedBy = rs.getString(columnNames[i++]);
            pipackagevendorchildrenDTO.modifiedBy = rs.getString(columnNames[i++]);
            pipackagevendorchildrenDTO.searchColumn = rs.getString(columnNames[i++]);
            pipackagevendorchildrenDTO.fiscalYearId = rs.getLong(columnNames[i++]);
            pipackagevendorchildrenDTO.packageId = rs.getLong(columnNames[i++]);
            pipackagevendorchildrenDTO.lotId = rs.getLong(columnNames[i++]);
            pipackagevendorchildrenDTO.vendorStatus = rs.getInt(columnNames[i++]);
            pipackagevendorchildrenDTO.officeUnitId = rs.getLong(columnNames[i++]);
            pipackagevendorchildrenDTO.actualVendorId = rs.getLong(columnNames[i++]);
            pipackagevendorchildrenDTO.isDeleted = rs.getInt(columnNames[i++]);
            pipackagevendorchildrenDTO.lastModificationTime = rs.getLong(columnNames[i++]);

            return pipackagevendorchildrenDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public PiPackageVendorChildrenDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "pi_package_vendor_children";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((PiPackageVendorChildrenDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((PiPackageVendorChildrenDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

    public void deletePiPackageVendorChildren(long pi_package_vendor_id, UserDTO userDTO) {

        String sql = String.format(
                deletePiPackageVendorChildren, 1,
                userDTO.employee_record_id, System.currentTimeMillis(), pi_package_vendor_id
        );


        boolean res = (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
            try {
                st.executeUpdate(sql);
                return true;
            } catch (SQLException ex) {
                logger.error(ex);
                return false;
            }
        });
    }

    public List<PiPackageVendorChildrenDTO> getVendorByPackageAndFiscalYearIdAndOfficeUnitIdAndWinner(long fiscalYearId, long packageId, long officeUnitId) {
        String sql = String.format(getVendorByPackageAndFiscalYearIdAndOfficeUnitIdAndWinner, fiscalYearId, packageId, CommonApprovalStatus.ACTIVE.getValue(), officeUnitId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public List<PiPackageVendorChildrenDTO> getVendorByPackageAndFiscalYearIdAndWinner(long fiscalYearId, long packageId) {
        String sql = String.format(getVendorByPackageAndFiscalYearIdAndAndWinner, fiscalYearId, packageId, CommonApprovalStatus.ACTIVE.getValue());
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public List<PiPackageVendorChildrenDTO> getVendorByPackageAndFiscalYearIdAndLotIdAndWinner(long fiscalYearId, long packageId, long lotId) {
        String sql = String.format(getVendorByPackageAndFiscalYearIdAndLotIdAndWinner, fiscalYearId, packageId, lotId, CommonApprovalStatus.ACTIVE.getValue());
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public List<PiPackageVendorChildrenDTO> getVendorByFiscalYearIdAndOfficeUnitIdAndWinner(long fiscalYearId, long officeUnitId) {
        String sql = String.format(getVendorByFiscalYearIdAndOfficeUnitIdAndWinner, fiscalYearId, CommonApprovalStatus.ACTIVE.getValue(), officeUnitId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }
}
	