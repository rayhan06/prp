package pi_package_vendor;
import java.util.*; 
import util.*;


public class PiPackageVendorChildrenMAPS extends CommonMaps
{	
	public PiPackageVendorChildrenMAPS(String tableName)
	{
		



		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Pi Package Vendor Id".toLowerCase(), "piPackageVendorId".toLowerCase());
		java_Text_map.put("Vendor Name".toLowerCase(), "name".toLowerCase());
		java_Text_map.put("Address".toLowerCase(), "address".toLowerCase());
		java_Text_map.put("Mobile Number".toLowerCase(), "mobile".toLowerCase());
		java_Text_map.put("Tender Winner".toLowerCase(), "isChosen".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
			
	}

}