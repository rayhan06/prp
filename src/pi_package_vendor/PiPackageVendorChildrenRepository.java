package pi_package_vendor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class PiPackageVendorChildrenRepository implements Repository {
	PiPackageVendorChildrenDAO pipackagevendorchildrenDAO = null;
	
	static Logger logger = Logger.getLogger(PiPackageVendorChildrenRepository.class);
	Map<Long, PiPackageVendorChildrenDTO>mapOfPiPackageVendorChildrenDTOToiD;
	Map<Long, Set<PiPackageVendorChildrenDTO> >mapOfPiPackageVendorChildrenDTOTopiPackageVendorId;
	Gson gson;

  
	private PiPackageVendorChildrenRepository(){
		pipackagevendorchildrenDAO = PiPackageVendorChildrenDAO.getInstance();
		mapOfPiPackageVendorChildrenDTOToiD = new ConcurrentHashMap<>();
		mapOfPiPackageVendorChildrenDTOTopiPackageVendorId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static PiPackageVendorChildrenRepository INSTANCE = new PiPackageVendorChildrenRepository();
    }

    public static PiPackageVendorChildrenRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<PiPackageVendorChildrenDTO> pipackagevendorchildrenDTOs = pipackagevendorchildrenDAO.getAllDTOs(reloadAll);
			for(PiPackageVendorChildrenDTO pipackagevendorchildrenDTO : pipackagevendorchildrenDTOs) {
				PiPackageVendorChildrenDTO oldPiPackageVendorChildrenDTO = getPiPackageVendorChildrenDTOByiD(pipackagevendorchildrenDTO.iD);
				if( oldPiPackageVendorChildrenDTO != null ) {
					mapOfPiPackageVendorChildrenDTOToiD.remove(oldPiPackageVendorChildrenDTO.iD);
				
					if(mapOfPiPackageVendorChildrenDTOTopiPackageVendorId.containsKey(oldPiPackageVendorChildrenDTO.piPackageVendorId)) {
						mapOfPiPackageVendorChildrenDTOTopiPackageVendorId.get(oldPiPackageVendorChildrenDTO.piPackageVendorId).remove(oldPiPackageVendorChildrenDTO);
					}
					if(mapOfPiPackageVendorChildrenDTOTopiPackageVendorId.get(oldPiPackageVendorChildrenDTO.piPackageVendorId).isEmpty()) {
						mapOfPiPackageVendorChildrenDTOTopiPackageVendorId.remove(oldPiPackageVendorChildrenDTO.piPackageVendorId);
					}
					
					
				}
				if(pipackagevendorchildrenDTO.isDeleted == 0) 
				{
					
					mapOfPiPackageVendorChildrenDTOToiD.put(pipackagevendorchildrenDTO.iD, pipackagevendorchildrenDTO);
				
					if( ! mapOfPiPackageVendorChildrenDTOTopiPackageVendorId.containsKey(pipackagevendorchildrenDTO.piPackageVendorId)) {
						mapOfPiPackageVendorChildrenDTOTopiPackageVendorId.put(pipackagevendorchildrenDTO.piPackageVendorId, new HashSet<>());
					}
					mapOfPiPackageVendorChildrenDTOTopiPackageVendorId.get(pipackagevendorchildrenDTO.piPackageVendorId).add(pipackagevendorchildrenDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public PiPackageVendorChildrenDTO clone(PiPackageVendorChildrenDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, PiPackageVendorChildrenDTO.class);
	}
	
	
	public List<PiPackageVendorChildrenDTO> getPiPackageVendorChildrenList() {
		List <PiPackageVendorChildrenDTO> pipackagevendorchildrens = new ArrayList<PiPackageVendorChildrenDTO>(this.mapOfPiPackageVendorChildrenDTOToiD.values());
		return pipackagevendorchildrens;
	}
	
	public List<PiPackageVendorChildrenDTO> copyPiPackageVendorChildrenList() {
		List <PiPackageVendorChildrenDTO> pipackagevendorchildrens = getPiPackageVendorChildrenList();
		return pipackagevendorchildrens
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public PiPackageVendorChildrenDTO getPiPackageVendorChildrenDTOByiD( long iD){
		return mapOfPiPackageVendorChildrenDTOToiD.get(iD);
	}
	
	public PiPackageVendorChildrenDTO copyPiPackageVendorChildrenDTOByiD( long iD){
		return clone(mapOfPiPackageVendorChildrenDTOToiD.get(iD));
	}
	
	
	public List<PiPackageVendorChildrenDTO> getPiPackageVendorChildrenDTOBypiPackageVendorId(long piPackageVendorId) {
		return new ArrayList<>( mapOfPiPackageVendorChildrenDTOTopiPackageVendorId.getOrDefault(piPackageVendorId,new HashSet<>()));
	}
	
	public List<PiPackageVendorChildrenDTO> copyPiPackageVendorChildrenDTOBypiPackageVendorId(long piPackageVendorId)
	{
		List <PiPackageVendorChildrenDTO> pipackagevendorchildrens = getPiPackageVendorChildrenDTOBypiPackageVendorId(piPackageVendorId);
		return pipackagevendorchildrens
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return pipackagevendorchildrenDAO.getTableName();
	}
}


