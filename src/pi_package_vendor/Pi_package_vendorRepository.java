package pi_package_vendor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Pi_package_vendorRepository implements Repository {
	Pi_package_vendorDAO pi_package_vendorDAO = null;
	
	static Logger logger = Logger.getLogger(Pi_package_vendorRepository.class);
	Map<Long, Pi_package_vendorDTO>mapOfPi_package_vendorDTOToiD;
	Map<Long, Set<Pi_package_vendorDTO> >mapOfPi_package_vendorDTOTofiscalYearId;
	Map<Long, Set<Pi_package_vendorDTO> >mapOfPi_package_vendorDTOTopackageId;
	Gson gson;

  
	private Pi_package_vendorRepository(){
		pi_package_vendorDAO = Pi_package_vendorDAO.getInstance();
		mapOfPi_package_vendorDTOToiD = new ConcurrentHashMap<>();
		mapOfPi_package_vendorDTOTofiscalYearId = new ConcurrentHashMap<>();
		mapOfPi_package_vendorDTOTopackageId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Pi_package_vendorRepository INSTANCE = new Pi_package_vendorRepository();
    }

    public static Pi_package_vendorRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Pi_package_vendorDTO> pi_package_vendorDTOs = pi_package_vendorDAO.getAllDTOs(reloadAll);
			for(Pi_package_vendorDTO pi_package_vendorDTO : pi_package_vendorDTOs) {
				Pi_package_vendorDTO oldPi_package_vendorDTO = getPi_package_vendorDTOByiD(pi_package_vendorDTO.iD);
				if( oldPi_package_vendorDTO != null ) {
					mapOfPi_package_vendorDTOToiD.remove(oldPi_package_vendorDTO.iD);
				
					if(mapOfPi_package_vendorDTOTofiscalYearId.containsKey(oldPi_package_vendorDTO.fiscalYearId)) {
						mapOfPi_package_vendorDTOTofiscalYearId.get(oldPi_package_vendorDTO.fiscalYearId).remove(oldPi_package_vendorDTO);
					}
					if(mapOfPi_package_vendorDTOTofiscalYearId.get(oldPi_package_vendorDTO.fiscalYearId).isEmpty()) {
						mapOfPi_package_vendorDTOTofiscalYearId.remove(oldPi_package_vendorDTO.fiscalYearId);
					}
					
					if(mapOfPi_package_vendorDTOTopackageId.containsKey(oldPi_package_vendorDTO.packageId)) {
						mapOfPi_package_vendorDTOTopackageId.get(oldPi_package_vendorDTO.packageId).remove(oldPi_package_vendorDTO);
					}
					if(mapOfPi_package_vendorDTOTopackageId.get(oldPi_package_vendorDTO.packageId).isEmpty()) {
						mapOfPi_package_vendorDTOTopackageId.remove(oldPi_package_vendorDTO.packageId);
					}
					
					
				}
				if(pi_package_vendorDTO.isDeleted == 0) 
				{
					
					mapOfPi_package_vendorDTOToiD.put(pi_package_vendorDTO.iD, pi_package_vendorDTO);
				
					if( ! mapOfPi_package_vendorDTOTofiscalYearId.containsKey(pi_package_vendorDTO.fiscalYearId)) {
						mapOfPi_package_vendorDTOTofiscalYearId.put(pi_package_vendorDTO.fiscalYearId, new HashSet<>());
					}
					mapOfPi_package_vendorDTOTofiscalYearId.get(pi_package_vendorDTO.fiscalYearId).add(pi_package_vendorDTO);
					
					if( ! mapOfPi_package_vendorDTOTopackageId.containsKey(pi_package_vendorDTO.packageId)) {
						mapOfPi_package_vendorDTOTopackageId.put(pi_package_vendorDTO.packageId, new HashSet<>());
					}
					mapOfPi_package_vendorDTOTopackageId.get(pi_package_vendorDTO.packageId).add(pi_package_vendorDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Pi_package_vendorDTO clone(Pi_package_vendorDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Pi_package_vendorDTO.class);
	}
	
	
	public List<Pi_package_vendorDTO> getPi_package_vendorList() {
		List <Pi_package_vendorDTO> pi_package_vendors = new ArrayList<Pi_package_vendorDTO>(this.mapOfPi_package_vendorDTOToiD.values());
		return pi_package_vendors;
	}
	
	public List<Pi_package_vendorDTO> copyPi_package_vendorList() {
		List <Pi_package_vendorDTO> pi_package_vendors = getPi_package_vendorList();
		return pi_package_vendors
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Pi_package_vendorDTO getPi_package_vendorDTOByiD( long iD){
		return mapOfPi_package_vendorDTOToiD.get(iD);
	}
	
	public Pi_package_vendorDTO copyPi_package_vendorDTOByiD( long iD){
		return clone(mapOfPi_package_vendorDTOToiD.get(iD));
	}
	
	
	public List<Pi_package_vendorDTO> getPi_package_vendorDTOByfiscalYearId(long fiscalYearId) {
		return new ArrayList<>( mapOfPi_package_vendorDTOTofiscalYearId.getOrDefault(fiscalYearId,new HashSet<>()));
	}
	
	public List<Pi_package_vendorDTO> copyPi_package_vendorDTOByfiscalYearId(long fiscalYearId)
	{
		List <Pi_package_vendorDTO> pi_package_vendors = getPi_package_vendorDTOByfiscalYearId(fiscalYearId);
		return pi_package_vendors
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<Pi_package_vendorDTO> getPi_package_vendorDTOBypackageId(long packageId) {
		return new ArrayList<>( mapOfPi_package_vendorDTOTopackageId.getOrDefault(packageId,new HashSet<>()));
	}
	
	public List<Pi_package_vendorDTO> copyPi_package_vendorDTOBypackageId(long packageId)
	{
		List <Pi_package_vendorDTO> pi_package_vendors = getPi_package_vendorDTOBypackageId(packageId);
		return pi_package_vendors
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return pi_package_vendorDAO.getTableName();
	}
}


