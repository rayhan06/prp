package pi_package_vendor;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import fiscal_year.Fiscal_yearDTO;
import fiscal_year.Fiscal_yearRepository;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pi_package_final.Pi_package_finalDAO;
import pi_package_final.Pi_package_finalDTO;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Pi_package_vendorDAO implements CommonDAOService<Pi_package_vendorDTO> {
    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private String[] columnNames = null;

    private static final String getVendorDtoByFiscalYearAndOfficeUnit =
            "SELECT * FROM pi_package_vendor WHERE  fiscal_year_id = %d AND " +
                    " office_unit_id = %d  AND isDeleted = 0 ORDER BY lastModificationTime DESC";

    private static final String getVendorDtoByFiscalYear =
            "SELECT * FROM pi_package_vendor WHERE  fiscal_year_id = %d AND " +
                    "  isDeleted = 0 ORDER BY lastModificationTime DESC";

    private static final String getVendorByFiscalYearIdAndPackageID =
            "SELECT * FROM pi_package_vendor WHERE  fiscal_year_id = %d AND " +
                    "  package_id = %d AND  isDeleted = 0 ORDER BY lastModificationTime DESC";

    private final String getDTOByStartAndEnd = "SELECT * FROM pi_package_vendor WHERE isDeleted = 0 AND " +
            " office_unit_id= ? AND agreement_date >= ? AND agreement_date < ? ";


    private Pi_package_vendorDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "fiscal_year_id",
                        "package_id",
                        "lot_id",
                        "moment_code_time",
                        "tender_advertise_cat",
                        "temder_advertise_date",
                        "tender_opening_date",
                        "noa_date",
                        "agreement_date",
                        "agreement_ending_date",
                        "tender_advertise_time",
                        "tender_opening_time",
                        "noa_time",
                        "agreement_time",
                        "agreement_ending_time",
                        "files_dropzone",
                        "vendor_status",
                        "insertion_date",
                        "inserted_by",
                        "modified_by",
                        "search_column",
                        "office_unit_id",
                        "winner_vendor_id",
                        "winner_vendor_name",
                        "winner_vendor_address",
                        "winner_vendor_mobile",
                        "actual_winner_vendor_id",
                        "isDeleted",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("tender_advertise_cat", " and (tender_advertise_cat = ?)");
        searchMap.put("temder_advertise_date_start", " and (temder_advertise_date >= ?)");
        searchMap.put("temder_advertise_date_end", " and (temder_advertise_date <= ?)");
        searchMap.put("tender_opening_date_start", " and (tender_opening_date >= ?)");
        searchMap.put("tender_opening_date_end", " and (tender_opening_date <= ?)");
        searchMap.put("noa_date_start", " and (noa_date >= ?)");
        searchMap.put("noa_date_end", " and (noa_date <= ?)");
        searchMap.put("agreement_date_start", " and (agreement_date >= ?)");
        searchMap.put("agreement_date_end", " and (agreement_date <= ?)");
        searchMap.put("agreement_ending_date_start", " and (agreement_ending_date >= ?)");
        searchMap.put("agreement_ending_date_end", " and (agreement_ending_date <= ?)");
        searchMap.put("insertion_date_start", " and (insertion_date >= ?)");
        searchMap.put("insertion_date_end", " and (insertion_date <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");

        searchMap.put("officeUnitId", " AND (office_unit_id = ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Pi_package_vendorDAO INSTANCE = new Pi_package_vendorDAO();
    }

    public static Pi_package_vendorDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Pi_package_vendorDTO pi_package_vendorDTO) {
        Fiscal_yearDTO fiscal_yearDTO = Fiscal_yearRepository.getInstance().getFiscal_yearDTOByid(pi_package_vendorDTO.fiscalYearId);
        //Procurement_packageDTO procurement_packageDTO = Procurement_packageRepository.getInstance().getProcurement_packageDTOByID(pi_package_vendorDTO.packageId);
        Pi_package_finalDTO packageDTO = Pi_package_finalDAO.getInstance().getDTOByID(pi_package_vendorDTO.packageId);

        pi_package_vendorDTO.searchColumn = "";
        pi_package_vendorDTO.searchColumn += fiscal_yearDTO.nameEn + " " + fiscal_yearDTO.nameBn + " ";
        pi_package_vendorDTO.searchColumn += packageDTO.packageNameEn + " " + packageDTO.packageNameBn + " ";
        pi_package_vendorDTO.searchColumn += pi_package_vendorDTO.winnerVendorName + " ";
        pi_package_vendorDTO.searchColumn += pi_package_vendorDTO.winnerVendorAddress + " ";
        pi_package_vendorDTO.searchColumn += pi_package_vendorDTO.winnerVendorMobile + " ";
        pi_package_vendorDTO.searchColumn += CatRepository.getInstance()
                .getText("English", "pi_vendor_status", Long.valueOf(pi_package_vendorDTO.vendorStatus)) + " "
                + CatRepository.getInstance()
                .getText("Bangla", "pi_vendor_status", Long.valueOf(pi_package_vendorDTO.vendorStatus)) + " ";
    }

    @Override
    public void set(PreparedStatement ps, Pi_package_vendorDTO pi_package_vendorDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(pi_package_vendorDTO);
        if (isInsert) {
            ps.setObject(++index, pi_package_vendorDTO.iD);
        }
        ps.setObject(++index, pi_package_vendorDTO.fiscalYearId);
        ps.setObject(++index, pi_package_vendorDTO.packageId);
        ps.setObject(++index, pi_package_vendorDTO.lotId);
        ps.setObject(++index, pi_package_vendorDTO.momentCodeTime);
        ps.setObject(++index, pi_package_vendorDTO.tenderAdvertiseCat);
        ps.setObject(++index, pi_package_vendorDTO.temderAdvertiseDate);
        ps.setObject(++index, pi_package_vendorDTO.tenderOpeningDate);
        ps.setObject(++index, pi_package_vendorDTO.noaDate);
        ps.setObject(++index, pi_package_vendorDTO.agreementDate);
        ps.setObject(++index, pi_package_vendorDTO.agreementEndingDate);
        ps.setObject(++index, pi_package_vendorDTO.tenderAdvertiseTime);
        ps.setObject(++index, pi_package_vendorDTO.tenderOpeningTime);
        ps.setObject(++index, pi_package_vendorDTO.noaTime);
        ps.setObject(++index, pi_package_vendorDTO.agreementTime);
        ps.setObject(++index, pi_package_vendorDTO.agreementEndingTime);
        ps.setObject(++index, pi_package_vendorDTO.filesDropzone);
        ps.setObject(++index, pi_package_vendorDTO.vendorStatus);
        ps.setObject(++index, pi_package_vendorDTO.insertionDate);
        ps.setObject(++index, pi_package_vendorDTO.insertedBy);
        ps.setObject(++index, pi_package_vendorDTO.modifiedBy);
        ps.setObject(++index, pi_package_vendorDTO.searchColumn);
        ps.setObject(++index, pi_package_vendorDTO.officeUnitId);
        ps.setObject(++index, pi_package_vendorDTO.winnerVendorId);
        ps.setObject(++index, pi_package_vendorDTO.winnerVendorName);
        ps.setObject(++index, pi_package_vendorDTO.winnerVendorAddress);
        ps.setObject(++index, pi_package_vendorDTO.winnerVendorMobile);
        ps.setObject(++index, pi_package_vendorDTO.actualWinnerVendorId);
        if (isInsert) {
            ps.setObject(++index, pi_package_vendorDTO.isDeleted);
        }
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, pi_package_vendorDTO.iD);
        }
    }

    @Override
    public Pi_package_vendorDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Pi_package_vendorDTO pi_package_vendorDTO = new Pi_package_vendorDTO();
            int i = 0;
            pi_package_vendorDTO.iD = rs.getLong(columnNames[i++]);
            pi_package_vendorDTO.fiscalYearId = rs.getLong(columnNames[i++]);
            pi_package_vendorDTO.packageId = rs.getLong(columnNames[i++]);
            pi_package_vendorDTO.lotId = rs.getLong(columnNames[i++]);
            pi_package_vendorDTO.momentCodeTime = rs.getString(columnNames[i++]);
            pi_package_vendorDTO.tenderAdvertiseCat = rs.getString(columnNames[i++]);
            pi_package_vendorDTO.temderAdvertiseDate = rs.getLong(columnNames[i++]);
            pi_package_vendorDTO.tenderOpeningDate = rs.getLong(columnNames[i++]);
            pi_package_vendorDTO.noaDate = rs.getLong(columnNames[i++]);
            pi_package_vendorDTO.agreementDate = rs.getLong(columnNames[i++]);
            pi_package_vendorDTO.agreementEndingDate = rs.getLong(columnNames[i++]);
            pi_package_vendorDTO.tenderAdvertiseTime = rs.getString(columnNames[i++]);
            pi_package_vendorDTO.tenderOpeningTime = rs.getString(columnNames[i++]);
            pi_package_vendorDTO.noaTime = rs.getString(columnNames[i++]);
            pi_package_vendorDTO.agreementTime = rs.getString(columnNames[i++]);
            pi_package_vendorDTO.agreementEndingTime = rs.getString(columnNames[i++]);
            pi_package_vendorDTO.filesDropzone = rs.getLong(columnNames[i++]);
            pi_package_vendorDTO.vendorStatus = rs.getInt(columnNames[i++]);
            pi_package_vendorDTO.insertionDate = rs.getLong(columnNames[i++]);
            pi_package_vendorDTO.insertedBy = rs.getString(columnNames[i++]);
            pi_package_vendorDTO.modifiedBy = rs.getString(columnNames[i++]);
            pi_package_vendorDTO.searchColumn = rs.getString(columnNames[i++]);
            pi_package_vendorDTO.officeUnitId = rs.getLong(columnNames[i++]);
            pi_package_vendorDTO.winnerVendorId = rs.getLong(columnNames[i++]);
            pi_package_vendorDTO.winnerVendorName = rs.getString(columnNames[i++]);
            pi_package_vendorDTO.winnerVendorAddress = rs.getString(columnNames[i++]);
            pi_package_vendorDTO.winnerVendorMobile = rs.getString(columnNames[i++]);
            pi_package_vendorDTO.actualWinnerVendorId = rs.getLong(columnNames[i++]);
            pi_package_vendorDTO.isDeleted = rs.getInt(columnNames[i++]);
            pi_package_vendorDTO.lastModificationTime = rs.getLong(columnNames[i++]);

            return pi_package_vendorDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Pi_package_vendorDTO getDTOByID(long id) {
        Pi_package_vendorDTO pi_package_vendorDTO = null;
        try {
            pi_package_vendorDTO = getDTOFromID(id);
            if (pi_package_vendorDTO != null) {
                PiPackageVendorChildrenDAO piPackageVendorChildrenDAO = PiPackageVendorChildrenDAO.getInstance();
                List<PiPackageVendorChildrenDTO> piPackageVendorChildrenDTOList = (List<PiPackageVendorChildrenDTO>) piPackageVendorChildrenDAO.getDTOsByParent("pi_package_vendor_id", pi_package_vendorDTO.iD);
                pi_package_vendorDTO.piPackageVendorChildrenDTOList = piPackageVendorChildrenDTOList;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return pi_package_vendorDTO;
    }

    @Override
    public String getTableName() {
        return "pi_package_vendor";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_package_vendorDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_package_vendorDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

    public List<Pi_package_vendorDTO> getVendorByFiscalYearIdAndOfficeUnitId(long fiscalYearId, long officeUnitId) {
        String sql = String.format(getVendorDtoByFiscalYearAndOfficeUnit, fiscalYearId, officeUnitId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public List<Pi_package_vendorDTO> getVendorByFiscalYearId(long fiscalYearId) {
        String sql = String.format(getVendorDtoByFiscalYear, fiscalYearId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public List<Pi_package_vendorDTO> getVendorByFiscalYearIdAndPackageID(long fiscalYearId, Long packageId) {
        String sql = String.format(getVendorByFiscalYearIdAndPackageID, fiscalYearId,packageId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public int getCountByWinnerVendor(long vendorId) {
        String countQuery = " SELECT count(*) as countID FROM "
                + " pi_purchase where isDeleted = 0 and vendor_id = ?  ";
        return ConnectionAndStatementUtil.getT(countQuery, Arrays.asList(vendorId), rs -> {
            try {
                return rs.getInt("countID");
            } catch (SQLException ex) {
                ex.printStackTrace();
                return 0;
            }
        }, 0);
    }

    public boolean isNotUsed(long iD) {

        Pi_package_vendorDTO pi_package_vendorDTO = Pi_package_vendorRepository.getInstance().getPi_package_vendorDTOByiD(iD);

        return getCountByWinnerVendor(pi_package_vendorDTO.actualWinnerVendorId) <= 0;
    }

    public List<Pi_package_vendorDTO> getByStartAndEndTime(long officeUnitId, long startTime, long endTime) {
        return getDTOs(getDTOByStartAndEnd, Arrays.asList(officeUnitId, startTime, endTime));
    }
}
	