package pi_package_vendor;

import java.util.*;

import util.*;


public class Pi_package_vendorDTO extends CommonDTO {
    public long fiscalYearId = -1;
    public long packageId = -1;
    public long lotId = -1;
    public String momentCodeTime = "12:00 PM";
    public String tenderAdvertiseCat = "";
    public long temderAdvertiseDate = System.currentTimeMillis();
    public long tenderOpeningDate = System.currentTimeMillis();
    public long noaDate = System.currentTimeMillis();
    public long agreementDate = System.currentTimeMillis();
    public long agreementEndingDate = System.currentTimeMillis();
    public long filesDropzone = -1;
    public int vendorStatus = -1;
    public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";

    public String tenderAdvertiseTime = "12:00 PM";
    public String tenderOpeningTime = "12:00 PM";
    public String noaTime = "12:00 PM";
    public String agreementTime = "12:00 PM";
    public String agreementEndingTime = "12:00 PM";

    public long officeUnitId = -1;

    public long winnerVendorId = -1;
    public String winnerVendorName = "";
    public String winnerVendorAddress = "";
    public String winnerVendorMobile = "";

    public long actualWinnerVendorId = -1;

    public List<PiPackageVendorChildrenDTO> piPackageVendorChildrenDTOList = new ArrayList<>();

    @Override
    public String toString() {
        return "$Pi_package_vendorDTO[" +
                " iD = " + iD +
                " fiscalYearId = " + fiscalYearId +
                " packageId = " + packageId +
                " momentCodeTime = " + momentCodeTime +
                " tenderAdvertiseCat = " + tenderAdvertiseCat +
                " temderAdvertiseDate = " + temderAdvertiseDate +
                " tenderOpeningDate = " + tenderOpeningDate +
                " noaDate = " + noaDate +
                " agreementDate = " + agreementDate +
                " agreementEndingDate = " + agreementEndingDate +
                " filesDropzone = " + filesDropzone +
                " vendorStatus = " + vendorStatus +
                " insertionDate = " + insertionDate +
                " insertedBy = " + insertedBy +
                " modifiedBy = " + modifiedBy +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                " searchColumn = " + searchColumn +
                " tenderAdvertiseTime = " + tenderAdvertiseTime +
                " tenderOpeningTime = " + tenderOpeningTime +
                " noaTime = " + noaTime +
                " agreementTime = " + agreementTime +
                " agreementEndingTime = " + agreementEndingTime +
                " officeUnitId = " + officeUnitId +
                " winnerVendorId = " + winnerVendorId +
                " winnerVendorName = " + winnerVendorName +
                " winnerVendorAddress = " + winnerVendorAddress +
                " winnerVendorMobile = " + winnerVendorMobile +
                " actualWinnerVendorId = " + actualWinnerVendorId +

                "]";
    }
}