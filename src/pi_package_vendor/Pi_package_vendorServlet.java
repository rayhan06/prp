package pi_package_vendor;

import com.google.gson.Gson;
import common.ApiResponse;
import common.BaseServlet;
import common.CommonDAOService;
import files.FilesDAO;
import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.intellij.lang.annotations.Language;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.ErrorMessage;
import pb.OptionDTO;
import pb.Utils;
import permission.MenuConstants;
import pi_app_request.Pi_app_requestDAO;
import pi_app_request.Pi_app_requestDTO;
import pi_app_request_details.Pi_app_request_detailsDAO;
import pi_app_request_details.Pi_app_request_detailsDTO;
import pi_app_request_package_lot_item_list.Pi_app_request_package_lot_item_listDAO;
import pi_app_request_package_lot_item_list.Pi_app_request_package_lot_item_listDTO;
import pi_package_final.PiPackageLotFinalDTO;
import pi_package_final.PiPackageLotFinalRepository;
import pi_package_final.Pi_package_finalDTO;
import pi_package_final.Pi_package_finalRepository;
import pi_package_new.Pi_package_newDTO;
import pi_package_new.Pi_package_newRepository;
import pi_package_vendor_items.OptionsWithCountDTO;
import pi_package_vendor_items.Pi_package_vendor_itemsDAO;
import pi_package_vendor_items.Pi_package_vendor_itemsDTO;
import pi_purchase.Pi_purchaseDAO;
import pi_purchase.Pi_purchaseDTO;
import pi_purchase_parent.Pi_purchase_parentDAO;
import pi_unique_item.PiActionTypeEnum;
import pi_unique_item.PiStageEnum;
import pi_unique_item.PiUniqueItemAssignmentDAO;
import pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsDTO;
import pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsRepository;
import procurement_goods.Procurement_goodsDAO;
import procurement_goods.Procurement_goodsDTO;
import procurement_goods.Procurement_goodsRepository;
import procurement_package.ProcurementGoodsTypeDTO;
import procurement_package.ProcurementGoodsTypeRepository;
import role.PermissionRepository;
import role.RoleDTO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.HttpRequestUtils;
import vm_requisition.CommonApprovalStatus;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Servlet implementation class Pi_package_vendorServlet
 */
@WebServlet("/Pi_package_vendorServlet")
@MultipartConfig
public class Pi_package_vendorServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_package_vendorServlet.class);

    @Override
    public String getTableName() {
        return Pi_package_vendorDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_package_vendorServlet";
    }

    @Override
    public Pi_package_vendorDAO getCommonDAOService() {
        return Pi_package_vendorDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PI_PACKAGE_VENDOR_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PI_PACKAGE_VENDOR_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PI_PACKAGE_VENDOR_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_package_vendorServlet.class;
    }

    FilesDAO filesDAO = new FilesDAO();
    PiPackageVendorChildrenDAO piPackageVendorChildrenDAO = PiPackageVendorChildrenDAO.getInstance();
    Pi_package_vendor_itemsDAO pi_package_vendor_itemsDAO = Pi_package_vendor_itemsDAO.getInstance();
    Pi_app_requestDAO pi_app_requestDAO = Pi_app_requestDAO.getInstance();
    private final Gson gson = new Gson();


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        // TODO Auto-generated method stub

        request.setAttribute("failureMessage", "");
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        Pi_package_vendorDTO pi_package_vendorDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLanEng = Language.equalsIgnoreCase("English");

        if (addFlag) {
            pi_package_vendorDTO = new Pi_package_vendorDTO();

            pi_package_vendorDTO.insertionDate = pi_package_vendorDTO.lastModificationTime = System.currentTimeMillis();
            pi_package_vendorDTO.insertedBy = pi_package_vendorDTO.modifiedBy = String.valueOf(userDTO.ID);
        } else {
            pi_package_vendorDTO = Pi_package_vendorDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));

            if (pi_package_vendorDTO == null) {
                throw new Exception("Vendor information is not found");
            }
            pi_package_vendorDTO.lastModificationTime = System.currentTimeMillis();
            pi_package_vendorDTO.modifiedBy = String.valueOf(userDTO.ID);


        }

        String Value = "";

        if (addFlag) {
            Value = request.getParameter("fiscalYearId");
            if (Value == null || Value.equals("") || Value.equals("-1")) {
                throw new Exception(isLanEng ? "Please provide fiscal year" : "অনুগ্রহপূর্বক অর্থবছর প্রদান করুন");
            }
            pi_package_vendorDTO.fiscalYearId = Long.parseLong(Value);
        }

        if (addFlag) {
            Value = request.getParameter("piPackageFinalId");
            if (Value == null || Value.equals("") || Value.equals("-1")) {
                throw new Exception(isLanEng ? "Please provide package" : "অনুগ্রহপূর্বক প্যাকেজ প্রদান করুন");
            }

            pi_package_vendorDTO.packageId = Long.parseLong(Value);
            pi_package_vendorDTO.officeUnitId = userDTO.unitID;

            Value = request.getParameter("piLotFinalId");
            if (isValueValid(Value)) {
                pi_package_vendorDTO.lotId = Long.parseLong(Value);
            }
        }

        boolean isAlreadyPurchased = Pi_purchase_parentDAO.getInstance()
                .isAlreadyPurchasedFiscalYearAndOfficeUnitAndPackageId(pi_package_vendorDTO.fiscalYearId,
                        pi_package_vendorDTO.officeUnitId, pi_package_vendorDTO.packageId);

        if (isAlreadyPurchased) {
            throw new Exception(isLanEng ? "Item already purchased of this package" : "এই প্যাকেজের পণ্য ইতিমধ্যে ক্রয় করা হয়েছে");
        }


        Value = request.getParameter("momentCodeTime");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide time code" : "অনুগ্রহপূর্বক মোমেন্ট কোড টাইম প্রদান করুন");
        }
        pi_package_vendorDTO.momentCodeTime = (Value);


        Value = request.getParameter("tenderAdvertiseDate");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide tender advertise date" : "অনুগ্রহপূর্বক টেন্ডার বিজ্ঞাপনের তারিখ প্রদান করুন");
        }
        Date d = f.parse(Value);
        pi_package_vendorDTO.temderAdvertiseDate = d.getTime();

        Value = request.getParameter("tenderAdvertiseTime");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide tender advertise time" : "অনুগ্রহপূর্বক টেন্ডার বিজ্ঞাপনের সময় প্রদান করুন");
        }
        pi_package_vendorDTO.tenderAdvertiseTime = (Value);

        Value = request.getParameter("tenderOpeningDate");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide tender opening date" : "অনুগ্রহপূর্বক টেন্ডার ওপেনিং ডেট প্রদান করুন");
        }
        d = f.parse(Value);
        pi_package_vendorDTO.tenderOpeningDate = d.getTime();

        Value = request.getParameter("tenderOpeningTime");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide tender opening time" : "অনুগ্রহপূর্বক টেন্ডার ওপেনিং সময় প্রদান করুন");
        }
        pi_package_vendorDTO.tenderOpeningTime = (Value);

        Value = request.getParameter("noaDate");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide notification of award date" : "অনুগ্রহপূর্বক নোটিফিকেশন অফ এওয়ার্ড ডেট প্রদান করুন");
        }
        d = f.parse(Value);
        pi_package_vendorDTO.noaDate = d.getTime();

        Value = request.getParameter("noaTime");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide notification of award time" : "অনুগ্রহপূর্বক নোটিফিকেশন অফ এওয়ার্ড সময় প্রদান করুন");
        }
        pi_package_vendorDTO.noaTime = (Value);

        Value = request.getParameter("agreementDate");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide agreement date" : "অনুগ্রহপূর্বক চুক্তি স্বাক্ষরের তারিখ প্রদান করুন");
        }
        d = f.parse(Value);
        pi_package_vendorDTO.agreementDate = d.getTime();

        Value = request.getParameter("agreementTime");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide agreement time" : "অনুগ্রহপূর্বক চুক্তি স্বাক্ষরের সময় প্রদান করুন");
        }
        pi_package_vendorDTO.agreementTime = (Value);

        Value = request.getParameter("agreementEndingDate");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide agreement ending date" : "অনুগ্রহপূর্বক চুক্তি শেষের তারিখ প্রদান করুন");
        }
        d = f.parse(Value);
        pi_package_vendorDTO.agreementEndingDate = d.getTime();

        Value = request.getParameter("agreementEndingTime");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide agreement ending time" : "অনুগ্রহপূর্বক চুক্তি শেষের সময় প্রদান করুন");
        }
        pi_package_vendorDTO.agreementEndingTime = (Value);

        Value = request.getParameter("filesDropzone");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        if (Value != null && !Value.equalsIgnoreCase("")) {
            pi_package_vendorDTO.filesDropzone = Long.parseLong(Value);

            if (!addFlag) {
                String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        }

        Value = request.getParameter("vendorStatus");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide vendor status" : "অনুগ্রহপূর্বক ভেন্ডরের অবস্থা প্রদান করুন");
        }
        pi_package_vendorDTO.vendorStatus = Integer.parseInt(Value);

        Value = request.getParameter("tenderAdvertiseCat");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide tender advertise medium" : "অনুগ্রহপূর্বক বিজ্ঞাপনের মাধ্যম প্রদান করুন");
        }
        pi_package_vendorDTO.tenderAdvertiseCat = Value;

        List<PiPackageVendorChildrenDTO> piPackageVendorChildrenDTOList = createPiPackageVendorChildrenDTOListByRequest(request, language);
        int submittedPriceLen = request.getParameterValues("hiddenSubmittedVendorPriceCheckbox").length;
        int submittedPriceIndex = 0;
        boolean atLeastOneGiven = false;
        for (int i = 0; i < submittedPriceLen; i++) {
            String hiddenCheckBoxValue = request.getParameterValues("hiddenSubmittedVendorPriceCheckbox")[i];
            String val = "";
            for (int j = 0; j < piPackageVendorChildrenDTOList.size(); j++) {
                val = request.getParameterValues("submittedVendorPrice")[submittedPriceIndex++];
                if (hiddenCheckBoxValue.equals("1")) {
                    atLeastOneGiven = true;
                    if (val == null || val.equals("") || val.equals("-1") || val.contains("-")) {
                        throw new Exception(isLanEng ? "Please provide product's price which are include in tender" : "অনুগ্রহপূর্বক টেন্ডারে অর্ন্তভূক্ত এমন পণ্যের দর প্রদান করুন");
                    }
                }
            }
        }
        if (!atLeastOneGiven) {
            throw new Exception(isLanEng ? "Please check at least one product from selected package" : "অনুগ্রহপূর্বক নির্বাচিত প্যাকেজ থেকে কমপক্ষে একটি পণ্য বাছাই করুন");
        }

        logger.debug("Done adding  addPi_package_vendor dto = " + pi_package_vendorDTO);

        if (addFlag) {
            Pi_package_vendorDAO.getInstance().add(pi_package_vendorDTO);
        } else {
            Pi_package_vendorDAO.getInstance().update(pi_package_vendorDTO);
            PiPackageVendorChildrenDAO.getInstance().deletePiPackageVendorChildren(pi_package_vendorDTO.iD, userDTO);
            Pi_package_vendor_itemsDAO.getInstance().deletePiPackageVendorChildrenItems(pi_package_vendorDTO, userDTO);
        }

        if (piPackageVendorChildrenDTOList != null) {
            int itemIndex = 0;
            for (PiPackageVendorChildrenDTO piPackageVendorChildrenDTO : piPackageVendorChildrenDTOList) {
                piPackageVendorChildrenDTO.piPackageVendorId = pi_package_vendorDTO.iD;
                piPackageVendorChildrenDTO.fiscalYearId = pi_package_vendorDTO.fiscalYearId;
                piPackageVendorChildrenDTO.packageId = pi_package_vendorDTO.packageId;
                piPackageVendorChildrenDTO.lotId = pi_package_vendorDTO.lotId;
                piPackageVendorChildrenDTO.officeUnitId = pi_package_vendorDTO.officeUnitId;

                long childrenId = piPackageVendorChildrenDAO.add(piPackageVendorChildrenDTO);

                if (piPackageVendorChildrenDTO.isChosen) {
                    pi_package_vendorDTO.winnerVendorId = childrenId;
                    pi_package_vendorDTO.winnerVendorName = piPackageVendorChildrenDTO.name;
                    pi_package_vendorDTO.winnerVendorAddress = piPackageVendorChildrenDTO.address;
                    pi_package_vendorDTO.winnerVendorMobile = piPackageVendorChildrenDTO.mobile;
                    pi_package_vendorDTO.actualWinnerVendorId = piPackageVendorChildrenDTO.actualVendorId;
                    Pi_package_vendorDAO.getInstance().update(pi_package_vendorDTO);

                }
            }
            addProductVendorAndPrice(request, piPackageVendorChildrenDTOList, pi_package_vendorDTO, itemIndex++, userDTO.unitID);
        }

        return pi_package_vendorDTO;


    }


    private List<PiPackageVendorChildrenDTO> createPiPackageVendorChildrenDTOListByRequest(HttpServletRequest request, String language) throws Exception {
        List<PiPackageVendorChildrenDTO> piPackageVendorChildrenDTOList = new ArrayList<PiPackageVendorChildrenDTO>();
        if (request.getParameterValues("piPackageVendorChildren.iD") != null) {
            int piPackageVendorChildrenItemNo = request.getParameterValues("piPackageVendorChildren.iD").length;

            for (int index = 0; index < piPackageVendorChildrenItemNo; index++) {
                PiPackageVendorChildrenDTO piPackageVendorChildrenDTO = createPiPackageVendorChildrenDTOByRequestAndIndex(request, true, index, language);
                piPackageVendorChildrenDTOList.add(piPackageVendorChildrenDTO);
            }

            return piPackageVendorChildrenDTOList;
        }
        return null;
    }

    private PiPackageVendorChildrenDTO createPiPackageVendorChildrenDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, String language) throws Exception {

        PiPackageVendorChildrenDTO piPackageVendorChildrenDTO;
        piPackageVendorChildrenDTO = new PiPackageVendorChildrenDTO();

        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");


        String Value = "";
        Value = request.getParameterValues("piPackageVendorChildren.name")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN_NAME, language) + " " + ErrorMessage.getInvalidMessage(language));
        }

        Pi_vendor_auctioneer_detailsDTO pi_vendor_auctioneer_detailsDTO = Pi_vendor_auctioneer_detailsRepository
                .getInstance()
                .getPi_vendor_auctioneer_detailsDTOByiD(Long.parseLong(Value));

        if (pi_vendor_auctioneer_detailsDTO == null) {
            throw new Exception(LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN_NAME, language) + " " + ErrorMessage.getInvalidMessage(language));
        }
        piPackageVendorChildrenDTO.actualVendorId = Long.parseLong(Value);
        piPackageVendorChildrenDTO.name = pi_vendor_auctioneer_detailsDTO.nameBn;
        piPackageVendorChildrenDTO.address = pi_vendor_auctioneer_detailsDTO.address;
        piPackageVendorChildrenDTO.mobile = pi_vendor_auctioneer_detailsDTO.mobile;

        piPackageVendorChildrenDTO.isChosen = false;
        piPackageVendorChildrenDTO.vendorStatus = CommonApprovalStatus.INACTIVE.getValue();

        String checkBoxValue = request.getParameterValues("piPackageVendorChildren.isChosenVal")[index];
        if (checkBoxValue.equals("1")) {
            piPackageVendorChildrenDTO.isChosen = true;
            String vendorStatus = request.getParameter("vendorStatus");
            piPackageVendorChildrenDTO.vendorStatus = Integer.parseInt(vendorStatus);
        }

        //Value = request.getParameterValues("piPackageVendorChildren.isChosen")[index];


        if (addFlag) {
            piPackageVendorChildrenDTO.insertedBy = piPackageVendorChildrenDTO.modifiedBy = String.valueOf(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.ID);
            piPackageVendorChildrenDTO.insertionDate = piPackageVendorChildrenDTO.lastModificationTime = System.currentTimeMillis();
        } else {
            piPackageVendorChildrenDTO.modifiedBy = String.valueOf(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.ID);
            piPackageVendorChildrenDTO.lastModificationTime = System.currentTimeMillis();
        }

        return piPackageVendorChildrenDTO;

    }

    private void addProductVendorAndPrice(HttpServletRequest request, List<PiPackageVendorChildrenDTO> piPackageVendorChildrenDTOS,
                                          Pi_package_vendorDTO pi_package_vendorDTO, int index, long officeUnitId) throws Exception {

        List<Pi_package_vendor_itemsDTO> pi_package_vendor_itemsNotDTOS = Pi_package_vendor_itemsDAO.getInstance()
                .getProductIdByPackageAndLotAndFiscalYearAndNotVendorId(pi_package_vendorDTO.fiscalYearId,
                        pi_package_vendorDTO.packageId, pi_package_vendorDTO.lotId, pi_package_vendorDTO.iD);

        String Value = "";

        int submittedPriceLen = request.getParameterValues("hiddenSubmittedVendorPriceCheckbox").length;
        int submittedPriceIndex = 0;
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLanEng = Language.equalsIgnoreCase("English");

        for (int i = 0; i < submittedPriceLen; i++) {

            String hiddenCheckBoxValue = request.getParameterValues("hiddenSubmittedVendorPriceCheckbox")[i];

            String submittedProductId = request.getParameterValues("submittedProductId")[i];
            if (submittedProductId == null || submittedProductId.equals("") || submittedProductId.equals("-1")) {
                throw new Exception(isLanEng ? "Please provide product" : "অনুগ্রহপূর্বক পণ্য প্রদান করুন");
            }

            for (PiPackageVendorChildrenDTO piPackageVendorChildrenDTO : piPackageVendorChildrenDTOS) {
                Pi_package_vendor_itemsDTO pi_package_vendor_itemsDTO = new Pi_package_vendor_itemsDTO();

                pi_package_vendor_itemsDTO.piPackageVendorId = pi_package_vendorDTO.iD;
                pi_package_vendor_itemsDTO.piPackageVendorChildrenId = piPackageVendorChildrenDTO.iD;
                pi_package_vendor_itemsDTO.packageId = pi_package_vendorDTO.packageId;
                pi_package_vendor_itemsDTO.lotId = pi_package_vendorDTO.lotId;
                pi_package_vendor_itemsDTO.fiscalYearId = pi_package_vendorDTO.fiscalYearId;
                pi_package_vendor_itemsDTO.officeUnitId = piPackageVendorChildrenDTO.officeUnitId;

                pi_package_vendor_itemsDTO.insertedBy = pi_package_vendor_itemsDTO.modifiedBy = piPackageVendorChildrenDTO.insertedBy;
                pi_package_vendor_itemsDTO.insertionDate = pi_package_vendor_itemsDTO.lastModificationTime = piPackageVendorChildrenDTO.insertionDate;


                pi_package_vendor_itemsDTO.productId = Long.parseLong(submittedProductId);

                pi_package_vendor_itemsDTO.actualVendorId = piPackageVendorChildrenDTO.actualVendorId;
                pi_package_vendor_itemsDTO.price = 0;
                pi_package_vendor_itemsDTO.isWinner = piPackageVendorChildrenDTO.isChosen;


                Value = request.getParameterValues("submittedVendorPrice")[submittedPriceIndex++];
                if (Value == null || Value.equals("") || Value.equals("-1")) {
                    //throw new Exception("Please provide price");
                } else {
                    pi_package_vendor_itemsDTO.price = Double.parseDouble((Jsoup.clean(
                            Value, Whitelist.simpleText()
                    )));
                }

                pi_package_vendor_itemsDTO.appApprovedQuantity = Pi_app_requestDAO.getInstance().
                        getApprovedAmountOfItemByFiscalYearIdAndOfficeUnitId(pi_package_vendor_itemsDTO.productId,
                                pi_package_vendor_itemsDTO.fiscalYearId, pi_package_vendor_itemsDTO.officeUnitId);
                pi_package_vendor_itemsDTO.totalBill = pi_package_vendor_itemsDTO.price * pi_package_vendor_itemsDTO.appApprovedQuantity;

                //to check if this product is already exist in another pi_package_vendor id
//                boolean isProductExist = Pi_package_vendor_itemsDAO.getInstance()
//                        .productExistInThisVendor(pi_package_vendor_itemsNotDTOS, pi_package_vendor_itemsDTO.productId);
//                if (hiddenCheckBoxValue.equals("1") && !isProductExist) {
//                    pi_package_vendor_itemsDAO.add(pi_package_vendor_itemsDTO);
//                }

                if (hiddenCheckBoxValue.equals("1")) {
                    pi_package_vendor_itemsDAO.add(pi_package_vendor_itemsDTO);
                }
            }
        }
    }

    @Override
    protected void deleteT(HttpServletRequest request, UserDTO userDTO) {
        String[] IDsToDelete = request.getParameterValues("ID");
        if (IDsToDelete.length > 0) {
            List<Long> ids = Stream.of(IDsToDelete)
                    .map(Long::parseLong)
                    .filter(i -> Pi_package_vendorDAO.getInstance().isNotUsed(i))
                    .collect(Collectors.toList());

            if (!ids.isEmpty()) {
                getCommonDAOService().deletePb(userDTO.employee_record_id, ids);
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");

            List<OptionDTO> optionDTOList = new ArrayList<>();
            String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
            String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

            long fiscalYearId = -1;
            long officeUnitId = -1;
            long procurementMethod = -1;
            long packageId = -1;
            long vendorId = -1;
            long piFinalLotId = -1;
            String responseText = "";


            String idString = request.getParameter("id");
            long id = idString == null ? -1 : Long.parseLong(idString);

            switch (actionType) {

                case "getEditPage":
                    request.getRequestDispatcher("pi_package_vendor/pi_package_vendorForEdit.jsp").forward(request, response);
                    return;

                case "getPackageByFiscalYearIdAndOfficeUnitId":


                    fiscalYearId = Long.parseLong(request.getParameter("fiscalYearId"));
                    officeUnitId = Long.parseLong(request.getParameter("officeUnitId"));

                    List<Pi_package_vendorDTO> requestDTOS = Pi_package_vendorDAO.getInstance().
                            getVendorByFiscalYearIdAndOfficeUnitId(fiscalYearId, officeUnitId);

                    if (!requestDTOS.isEmpty()) {

                        List<Long> packageIds = requestDTOS.stream().map(i -> i.packageId).distinct().
                                collect(Collectors.toList());
//                        List<Procurement_packageDTO> packageDTOS = Procurement_packageRepository.getInstance().
//                                getDTOsByIds(packageIds);

                        List<Pi_package_newDTO> packageDTOS = Pi_package_newRepository.getInstance().
                                getDTOsByIds(packageIds);
                        optionDTOList = packageDTOS
                                .stream()
                                .map(e -> new OptionDTO(e.packageNameEn, e.packageNameBn, String.valueOf(e.iD)))
                                .collect(Collectors.toList());
                    }


                    responseText = Utils.buildOptions(optionDTOList, language, String.valueOf(id));
                    ApiResponse.sendSuccessResponse(response, responseText);

                    return;

                case "getPackageByFiscalYearIdAndProcurementMethod":


                    fiscalYearId = Long.parseLong(request.getParameter("fiscalYearId"));
                    procurementMethod = Long.parseLong(request.getParameter("procurementMethod"));

                    if (procurementMethod == 1 || procurementMethod == 3) { // for OTM & RFQ
                        List<Pi_package_vendorDTO> requestDTOS1 = Pi_package_vendorDAO.getInstance().
                                getVendorByFiscalYearId(fiscalYearId);

                        if (!requestDTOS1.isEmpty()) {

                            List<Long> packageIds = requestDTOS1.stream().map(i -> i.packageId).distinct().
                                    collect(Collectors.toList());

                            List<Pi_package_finalDTO> packageDTOS = Pi_package_finalRepository.getInstance().
                                    getDTOsByIds(packageIds);
                            optionDTOList = packageDTOS
                                    .stream()
                                    .map(e -> new OptionDTO(e.packageNumberEn, e.packageNumberBn, String.valueOf(e.iD)))
                                    .collect(Collectors.toList());
                        }
                    } else if (procurementMethod == 2) {  // for DPM

                        List<Pi_app_requestDTO> pi_app_requestDTOS = pi_app_requestDAO.getByFiscalYearId(fiscalYearId);
                        pi_app_requestDTOS = pi_app_requestDTOS.stream().filter(e -> e.status == CommonApprovalStatus.SATISFIED.getValue()).collect(Collectors.toList());
                        if (pi_app_requestDTOS.size() == 1) {

                            Pi_app_requestDTO pi_app_requestDTO = pi_app_requestDTOS.get(0);
                            List<Pi_app_request_detailsDTO> pi_app_request_detailsDTOS = Pi_app_request_detailsDAO.getInstance().getByAppId(pi_app_requestDTO.iD);

                            if (!pi_app_request_detailsDTOS.isEmpty()) {

                                List<Long> packageIds = pi_app_request_detailsDTOS.stream().map(i -> i.packageFinalId).distinct().
                                        collect(Collectors.toList());

                                List<Pi_package_finalDTO> packageDTOS = Pi_package_finalRepository.getInstance().
                                        getDTOsByIds(packageIds);
                                optionDTOList = packageDTOS
                                        .stream()
                                        .map(e -> new OptionDTO(e.packageNumberEn, e.packageNumberBn, String.valueOf(e.iD)))
                                        .collect(Collectors.toList());
                            }
                        }

                    }

                    responseText = Utils.buildOptions(optionDTOList, language, String.valueOf(id));
                    ApiResponse.sendSuccessResponse(response, responseText);

                    return;

                case "getLotByFiscalYearIdAndProcurementMethodAndPackageId":


                    fiscalYearId = Long.parseLong(request.getParameter("fiscalYearId"));
                    procurementMethod = Long.parseLong(request.getParameter("procurementMethod"));
                    packageId = Long.parseLong(request.getParameter("packageId"));

                    if (procurementMethod == 1 || procurementMethod == 3) { // for OTM & RFQ
                        List<Pi_package_vendorDTO> requestDTOS1 = Pi_package_vendorDAO.getInstance().
                                getVendorByFiscalYearIdAndPackageID(fiscalYearId, packageId);

                        if (!requestDTOS1.isEmpty()) {

                            List<Long> lotIds = requestDTOS1.stream().map(i -> i.lotId).distinct().
                                    collect(Collectors.toList());

                            List<PiPackageLotFinalDTO> lotDTOS = PiPackageLotFinalRepository.getInstance().
                                    getDTOsByIds(lotIds);
                            optionDTOList = lotDTOS
                                    .stream()
                                    .map(e -> new OptionDTO(e.lotNumberEn, e.lotNumberBn, String.valueOf(e.iD)))
                                    .collect(Collectors.toList());
                        }
                    } else if (procurementMethod == 2) {  // for DPM

                        List<Pi_app_requestDTO> pi_app_requestDTOS = pi_app_requestDAO.getByFiscalYearId(fiscalYearId);
                        pi_app_requestDTOS = pi_app_requestDTOS.stream().filter(e -> e.status == CommonApprovalStatus.SATISFIED.getValue()).collect(Collectors.toList());
                        if (pi_app_requestDTOS.size() == 1) {

                            Pi_app_requestDTO pi_app_requestDTO = pi_app_requestDTOS.get(0);
                            List<Pi_app_request_detailsDTO> pi_app_request_detailsDTOS = Pi_app_request_detailsDAO.getInstance().getByAppId(pi_app_requestDTO.iD);

                            if (!pi_app_request_detailsDTOS.isEmpty()) {

                                long finalPackageId = packageId;
                                List<Long> lotIds = pi_app_request_detailsDTOS.stream().
                                        filter(e -> e.packageFinalId == finalPackageId).
                                        map(i -> i.lotFinalId).distinct().
                                        collect(Collectors.toList());

                                List<PiPackageLotFinalDTO> lotDTOS = PiPackageLotFinalRepository.getInstance().
                                        getDTOsByIds(lotIds);
                                optionDTOList = lotDTOS
                                        .stream()
                                        .map(e -> new OptionDTO(e.lotNumberEn, e.lotNumberBn, String.valueOf(e.iD)))
                                        .collect(Collectors.toList());
                            }
                        }

                    }

                    responseText = Utils.buildOptions(optionDTOList, language, String.valueOf(id));
                    ApiResponse.sendSuccessResponse(response, responseText);

                    return;

                case "getVendorByFiscalYearIdAndOfficeUnitIdAndPackageId":

                    optionDTOList = new ArrayList<>();
                    fiscalYearId = Long.parseLong(request.getParameter("fiscalYearId"));
                    packageId = Long.parseLong(request.getParameter("packageId"));

                    List<PiPackageVendorChildrenDTO> childrenDTOS = PiPackageVendorChildrenDAO.getInstance().
                            getVendorByPackageAndFiscalYearIdAndWinner(fiscalYearId, packageId);

                    if (!childrenDTOS.isEmpty()) {


                        optionDTOList = childrenDTOS
                                .stream()
                                .map(dto -> Pi_vendor_auctioneer_detailsRepository.getInstance().getPi_vendor_auctioneer_detailsDTOByiD(dto.actualVendorId))
                                .map(e -> new OptionDTO(e.nameEn, e.nameBn, String.valueOf(e.iD)))
                                .collect(Collectors.toList());
                    }

                    responseText = Utils.buildOptions(optionDTOList, language, String.valueOf(id));
                    ApiResponse.sendSuccessResponse(response, responseText);

                    return;

                case "getVendorByFiscalYearIdAndPackageIdAndLotIdAndProcurementMethod":

                    optionDTOList = new ArrayList<>();
                    fiscalYearId = Long.parseLong(request.getParameter("fiscalYearId"));
                    packageId = Long.parseLong(request.getParameter("packageId"));
                    if (!request.getParameter("piFinalLotId").trim().isEmpty())
                        piFinalLotId = Long.parseLong(request.getParameter("piFinalLotId"));
                    procurementMethod = Long.parseLong(request.getParameter("procurementMethod"));


                    if (procurementMethod == 1 || procurementMethod == 3) { // for OTM & RFQ

                        List<PiPackageVendorChildrenDTO> childrenDTOS1 = PiPackageVendorChildrenDAO.getInstance().
                                getVendorByPackageAndFiscalYearIdAndLotIdAndWinner(fiscalYearId, packageId, piFinalLotId);
                        if (!childrenDTOS1.isEmpty()) {
                            optionDTOList = childrenDTOS1
                                    .stream()
                                    .map(dto -> Pi_vendor_auctioneer_detailsRepository.getInstance().
                                            getPi_vendor_auctioneer_detailsDTOByiD(dto.actualVendorId))
                                    .map(e -> new OptionDTO(e.nameEn, e.nameBn, String.valueOf(e.iD)))
                                    .collect(Collectors.toList());
                        }
                    } else if (procurementMethod == 2) {  // for DPM
                        List<Pi_vendor_auctioneer_detailsDTO> pi_vendor_auctioneer_detailsDTOS = Pi_vendor_auctioneer_detailsRepository.
                                getInstance().getAllPi_vendor_auctioneer_detailsDTOs("1"); // 1 for vendor not auctioneer

                        optionDTOList = pi_vendor_auctioneer_detailsDTOS
                                .stream()
                                .map(e -> new OptionDTO(e.nameEn, e.nameBn, String.valueOf(e.iD)))
                                .collect(Collectors.toList());
                    }


                    responseText = Utils.buildOptions(optionDTOList, language, String.valueOf(id));
                    ApiResponse.sendSuccessResponse(response, responseText);

                    return;

                case "getPriceByFiscalYearIdAndOfficeUnitIdAndPackageIdAndVendorId":

                    optionDTOList = new ArrayList<>();
                    fiscalYearId = Long.parseLong(request.getParameter("fiscalYearId"));
                    officeUnitId = Long.parseLong(request.getParameter("officeUnitId"));
                    packageId = Long.parseLong(request.getParameter("packageId"));
                    vendorId = Long.parseLong(request.getParameter("vendorId"));


                    List<Pi_package_vendor_itemsDTO> items = Pi_package_vendor_itemsDAO.getInstance().
                            getPriceByFiscalYearIdAndOfficeUnitIdAndPackageIdAndVendorId(fiscalYearId, officeUnitId, packageId, vendorId);

                    OptionsWithCountDTO optionsWithCountDTO = getStringResponseByFiscalYearIdAndOfficeUnitIdAndPackageIdAndVendorId(new ArrayList<>(),
                            items, Language, officeUnitId, fiscalYearId, id);
                    ApiResponse.sendSuccessResponse(response, gson.toJson(optionsWithCountDTO));

                    return;

                case "getPriceByFiscalYearIdAndVendorIdAndPackageIdAndLotId":

                    optionDTOList = new ArrayList<>();
                    fiscalYearId = Long.parseLong(request.getParameter("fiscalYearId"));
                    officeUnitId = Long.parseLong(request.getParameter("officeUnitId"));
                    packageId = Long.parseLong(request.getParameter("packageId"));
                    vendorId = Long.parseLong(request.getParameter("vendorId"));


                    if (!request.getParameter("piFinalLotId").trim().isEmpty())
                        piFinalLotId = Long.parseLong(request.getParameter("piFinalLotId"));


                    procurementMethod = Long.parseLong(request.getParameter("procurementMethod"));

                    List<Pi_app_requestDTO> pi_app_requestDTOS = pi_app_requestDAO.getByFiscalYearId(fiscalYearId);
                    pi_app_requestDTOS = pi_app_requestDTOS.stream().filter(e -> e.status == CommonApprovalStatus.SATISFIED.getValue()).collect(Collectors.toList());
                    List<Pi_app_request_package_lot_item_listDTO> pi_app_request_package_lot_item_listDTOS = new ArrayList<>();

                    if (pi_app_requestDTOS.size() == 1) {

                        Pi_app_requestDTO pi_app_requestDTO = pi_app_requestDTOS.get(0);
                        pi_app_request_package_lot_item_listDTOS = Pi_app_request_package_lot_item_listDAO.getInstance().getByAppId(pi_app_requestDTO.iD);
                    }

                    if (procurementMethod == 1 || procurementMethod == 3) { // for OTM & RFQ

                        List<Pi_package_vendor_itemsDTO> items1 = Pi_package_vendor_itemsDAO.getInstance().
                                getPriceByFiscalYearIdAndVendorIdAndPackageIdAndLotId(fiscalYearId, packageId, piFinalLotId, vendorId);


                        OptionsWithCountDTO optionsWithCountDTO2 = getStringResponseByFiscalYearIdAndOfficeUnitIdAndPackageIdAndVendorId(
                                pi_app_request_package_lot_item_listDTOS,items1, Language, officeUnitId, fiscalYearId, id);
                        ApiResponse.sendSuccessResponse(response, gson.toJson(optionsWithCountDTO2));

                    } else if (procurementMethod == 2) {  // for DPM


                        if (!pi_app_request_package_lot_item_listDTOS.isEmpty()) {

                            long finalPackageId = packageId;
                            long finalPiFinalLotId = piFinalLotId;
                            pi_app_request_package_lot_item_listDTOS = pi_app_request_package_lot_item_listDTOS.stream().
                                    filter(e -> e.packageFinalId == finalPackageId && e.lotFinalId == finalPiFinalLotId).
                                    collect(Collectors.toList());

                            OptionsWithCountDTO optionsWithCountDTO2 = getStringResponseFromAppRequestDetails(
                                    pi_app_request_package_lot_item_listDTOS, Language, officeUnitId, fiscalYearId, id, vendorId);
                            ApiResponse.sendSuccessResponse(response, gson.toJson(optionsWithCountDTO2));

                        }

                    }


                    return;

                case "getVendorByFiscalYearIdAndOfficeUnitIdAndDPM":

                    optionDTOList = new ArrayList<>();
                    fiscalYearId = Long.parseLong(request.getParameter("fiscalYearId"));
                    officeUnitId = Long.parseLong(request.getParameter("officeUnitId"));

                    List<PiPackageVendorChildrenDTO> piPackageVendorChildrenDTOS2 = PiPackageVendorChildrenDAO.getInstance().
                            getVendorByFiscalYearIdAndOfficeUnitIdAndWinner(fiscalYearId, officeUnitId);

                    if (!piPackageVendorChildrenDTOS2.isEmpty()) {


                        optionDTOList = piPackageVendorChildrenDTOS2
                                .stream()
                                .map(dto -> Pi_vendor_auctioneer_detailsRepository.getInstance().getPi_vendor_auctioneer_detailsDTOByiD(dto.actualVendorId))
                                .map(e -> new OptionDTO(e.nameEn, e.nameBn, String.valueOf(e.iD)))
                                .collect(Collectors.toList());
                    }

                    responseText = Utils.buildOptions(optionDTOList, language, String.valueOf(id));
                    ApiResponse.sendSuccessResponse(response, responseText);

                    return;

                case "getPriceByFiscalYearIdAndOfficeUnitIdAndVendorIdAndDPM":

                    optionDTOList = new ArrayList<>();
                    fiscalYearId = Long.parseLong(request.getParameter("fiscalYearId"));
                    officeUnitId = Long.parseLong(request.getParameter("officeUnitId"));
                    vendorId = Long.parseLong(request.getParameter("vendorId"));

                    List<Pi_package_vendor_itemsDTO> pi_package_vendor_itemsDTOS = Pi_package_vendor_itemsDAO.getInstance().
                            getPriceByFiscalYearIdAndOfficeUnitIdAndVendorId(fiscalYearId, officeUnitId, vendorId);

                    OptionsWithCountDTO optionsWithCountDTO1 = getStringResponseByFiscalYearIdAndOfficeUnitIdAndPackageIdAndVendorId( new ArrayList<>(),
                            pi_package_vendor_itemsDTOS, Language, officeUnitId, fiscalYearId, id);
                    ApiResponse.sendSuccessResponse(response, gson.toJson(optionsWithCountDTO1));

                    return;

                case "search":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_AUCTION_SEARCH)) {

                        RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
                        boolean isAdmin = role.ID == SessionConstants.INVENTORY_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;
                        Map<String, String> extraCriteriaMap = new HashMap<>();
                        if (isAdmin) {
                            request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                            search(request, response);
                            return;
                        } else {
                            extraCriteriaMap.put("officeUnitId", String.valueOf(userDTO.unitID));
                            request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                            search(request, response);
                            return;
                        }
                    }
                    break;

                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }


    public void getPriceByFiscalYearIdAndOfficeUnitIdAndPackageIdAndVendorId(List<Pi_package_vendor_itemsDTO> vendorItems, HttpServletRequest request, HttpServletResponse response) throws Exception {

        List<Pi_purchaseDTO> items = new ArrayList<>();
        for (Pi_package_vendor_itemsDTO dto : vendorItems) {
            Pi_purchaseDTO purchaseDTO = new Pi_purchaseDTO();
            purchaseDTO.fiscalYearId = dto.fiscalYearId;
            purchaseDTO.itemId = dto.productId;
            purchaseDTO.itemTypeId = Procurement_goodsRepository
                    .getInstance()
                    .getProcurement_goodsDTOByID(purchaseDTO.itemId).procurementGoodsTypeId;
            purchaseDTO.packageId = dto.packageId;
            purchaseDTO.officeUnitId = dto.officeUnitId;
            purchaseDTO.vendorId = dto.piPackageVendorId;

            items.add(purchaseDTO);
        }
        String url = "pi_purchase/pi_purchaseEditBodyItems.jsp";
        request.setAttribute("items", items);
        request.getRequestDispatcher(url).forward(request, response);
    }

    public OptionsWithCountDTO getStringResponseByFiscalYearIdAndOfficeUnitIdAndPackageIdAndVendorId(List<Pi_app_request_package_lot_item_listDTO> pi_app_request_package_lot_item_listDTOS,
                                                                                                     List<Pi_package_vendor_itemsDTO> items,
                                                                                                     String Language, long officeUnitId, long fiscalYearId, long parentId) {

        String tempString = "";

        int childTableStartingID = 0;

        HashMap<Long, Pi_purchaseDTO> itemIdToPurchaseMap = new HashMap<>();

        if (parentId != -1) {
            List<Pi_purchaseDTO> purchaseDTOS = (List<Pi_purchaseDTO>) Pi_purchaseDAO.getInstance().getDTOsByParent("pi_purchase_parent_id", parentId);
            purchaseDTOS
                    .forEach(purchaseDTO -> {
                        itemIdToPurchaseMap.put(purchaseDTO.itemId, purchaseDTO);
                    });
        }

        for (Pi_package_vendor_itemsDTO dto : items) {
            childTableStartingID++;
            Pi_purchaseDTO purchaseDTO = itemIdToPurchaseMap.getOrDefault(dto.productId, new Pi_purchaseDTO());
            String checked = itemIdToPurchaseMap.containsKey(dto.productId) ? "checked" : "";
            checked += parentId != -1 ? " disabled" : "";
            purchaseDTO.fiscalYearId = dto.fiscalYearId;
            purchaseDTO.itemId = dto.productId;
            purchaseDTO.itemTypeId = Procurement_goodsRepository
                    .getInstance()
                    .getProcurement_goodsDTOByID(purchaseDTO.itemId).procurementGoodsTypeId;
            purchaseDTO.packageId = dto.packageId;
            purchaseDTO.lotId = dto.lotId;
            purchaseDTO.officeUnitId = officeUnitId;
            purchaseDTO.vendorId = dto.actualVendorId;

            Procurement_goodsDTO itemDTO = Procurement_goodsDAO.getInstance().getDTOByID(dto.productId);
            ProcurementGoodsTypeDTO goodsTypeDTO = ProcurementGoodsTypeRepository.getInstance().getProcurementGoodsTypeDTOByID(itemDTO.procurementGoodsTypeId);
            //Procurement_packageDTO packageDTO = Procurement_packageRepository.getInstance().getProcurement_packageDTOByID(goodsTypeDTO.procurementPackageId);
            Pi_package_finalDTO pi_package_finalDTO = Pi_package_finalRepository.getInstance().getPi_package_finalDTOByiD(dto.packageId);
            String packageName = Language.equals("English") ?
                    pi_package_finalDTO.packageNumberEn : pi_package_finalDTO.packageNumberBn;
            String typeName = Language.equals("English") ?
                    goodsTypeDTO.nameEn : goodsTypeDTO.nameBn;
            String itemName = Language.equals("English") ?
                    itemDTO.nameEn : itemDTO.nameBn;


            //  current stock calculation starts

            int initialStockInt = PiUniqueItemAssignmentDAO.getInstance().getTotalItemCountWithStageAllOver(officeUnitId, purchaseDTO.itemId,
                    PiStageEnum.IN_STOCK.getValue());
            String remainingStockAmount = Utils.getDigits(initialStockInt, "English");

            //  current stock calculation ends


            //  APP calculation starts


            List<Pi_app_request_package_lot_item_listDTO> lotItemListDTOSForSpecificItem = pi_app_request_package_lot_item_listDTOS.stream().
                    filter(e -> e.itemId== itemDTO.iD).collect(Collectors.toList());

            for(Pi_app_request_package_lot_item_listDTO lotItemListDTO : lotItemListDTOSForSpecificItem){

                int currentYearStockInt = PiUniqueItemAssignmentDAO.getInstance().getItemsCountWithoutStore( fiscalYearId, purchaseDTO.itemId,
                        PiActionTypeEnum.PURCHASE.getValue());
                double appAllocated = lotItemListDTO.approverThreeQuantity != -1? lotItemListDTO.approverThreeQuantity : 0;
                double appAllocatedRemaining = appAllocated - (currentYearStockInt * 1.0);

                String appAllocatedString = Utils.getDigits(appAllocated, "English");
                String appAllocatedRemainingString = Utils.getDigits(appAllocatedRemaining, "English");

                // price calculation starts

                double unitPrice = dto.price;
                String unitPriceString = Utils.getDigits(unitPrice, "English");

                // price calculation ends

                String backgroundColor = itemIdToPurchaseMap.containsKey(dto.productId) ? "" : "style=\"background-color: #B2BEB5\"";
                String readonly = itemIdToPurchaseMap.containsKey(dto.productId) ? "" : "readonly";

                tempString += getTempStringResponse(childTableStartingID, backgroundColor, purchaseDTO, packageName,
                        typeName, itemName, remainingStockAmount, appAllocatedString,
                        appAllocatedRemainingString, readonly, unitPriceString, checked);
            }

            //  APP calculation ends





        }

        OptionsWithCountDTO optionsWithCountDTO = new OptionsWithCountDTO();
        optionsWithCountDTO.itemsHtml = tempString;
        optionsWithCountDTO.itemsCount = childTableStartingID;
        return optionsWithCountDTO;
    }

    private boolean isValueValid(String value) {
        return value != null && !value.equals("");
    }

    public OptionsWithCountDTO getStringResponseFromAppRequestDetails(List<Pi_app_request_package_lot_item_listDTO> items,
                                                                      String Language,
                                                                      long officeUnitId, long fiscalYearId, long parentId,
                                                                      long actualVendorId) {

        String tempString = "";

        int childTableStartingID = 0;

        HashMap<Long, Pi_purchaseDTO> itemIdToPurchaseMap = new HashMap<>();

        if (parentId != -1) {
            List<Pi_purchaseDTO> purchaseDTOS = (List<Pi_purchaseDTO>) Pi_purchaseDAO.getInstance().getDTOsByParent("pi_purchase_parent_id", parentId);
            purchaseDTOS
                    .forEach(purchaseDTO -> {
                        itemIdToPurchaseMap.put(purchaseDTO.itemId, purchaseDTO);
                    });
        }

        for (Pi_app_request_package_lot_item_listDTO dto : items) {
            childTableStartingID++;
            Pi_purchaseDTO purchaseDTO = itemIdToPurchaseMap.getOrDefault(dto.itemId, new Pi_purchaseDTO());
            String checked = itemIdToPurchaseMap.containsKey(dto.itemId) ? "checked" : "";
            checked += parentId != -1 ? " disabled" : "";
            purchaseDTO.fiscalYearId = dto.fiscalYearId;
            purchaseDTO.itemId = dto.itemId;
            purchaseDTO.itemTypeId = Procurement_goodsRepository
                    .getInstance()
                    .getProcurement_goodsDTOByID(dto.itemId).procurementGoodsTypeId;
            purchaseDTO.packageId = dto.packageFinalId;
            purchaseDTO.lotId = dto.lotFinalId;
            purchaseDTO.officeUnitId = officeUnitId;
            purchaseDTO.vendorId = actualVendorId;

            Procurement_goodsDTO itemDTO = Procurement_goodsDAO.getInstance().getDTOByID(dto.itemId);
            ProcurementGoodsTypeDTO goodsTypeDTO = ProcurementGoodsTypeRepository.getInstance().getProcurementGoodsTypeDTOByID(itemDTO.procurementGoodsTypeId);
            //Procurement_packageDTO packageDTO = Procurement_packageRepository.getInstance().getProcurement_packageDTOByID(goodsTypeDTO.procurementPackageId);
            Pi_package_finalDTO packageDTO = Pi_package_finalRepository.getInstance().getPi_package_finalDTOByiD(dto.packageFinalId);
            String packageName = Language.equals("English") ?
                    packageDTO.packageNumberEn : packageDTO.packageNumberBn;
            String typeName = Language.equals("English") ?
                    goodsTypeDTO.nameEn : goodsTypeDTO.nameBn;
            String itemName = Language.equals("English") ?
                    itemDTO.nameEn : itemDTO.nameBn;


            //  current stock calculation starts

            int initialStockInt = PiUniqueItemAssignmentDAO.getInstance().getTotalItemCountWithStageAllOver(officeUnitId, purchaseDTO.itemId,
                    PiStageEnum.IN_STOCK.getValue());
            String remainingStockAmount = Utils.getDigits(initialStockInt, "English");

            //  current stock calculation ends

            //  APP calculation starts

            int currentYearStockInt = PiUniqueItemAssignmentDAO.getInstance().getItemsCountWithoutStore(fiscalYearId, purchaseDTO.itemId,
                    PiActionTypeEnum.PURCHASE.getValue());

            double appAllocated = dto.approverThreeQuantity != -1 ? dto.approverThreeQuantity : 0;
            double appAllocatedRemaining = appAllocated - (currentYearStockInt * 1.0);

            String appAllocatedString = Utils.getDigits(appAllocated, "English");
            String appAllocatedRemainingString = Utils.getDigits(appAllocatedRemaining, "English");


            //  APP calculation ends

            // price calculation starts

            double unitPrice = dto.approverThreeUnitPrice;
            String unitPriceString = Utils.getDigits(unitPrice, "English");

            // price calculation ends


            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String value = "expiryDate_js_" + childTableStartingID;

            String backgroundColor = itemIdToPurchaseMap.containsKey(dto.itemId) ? "" : "style=\"background-color: #B2BEB5\"";
            String readonly = itemIdToPurchaseMap.containsKey(dto.itemId) ? "" : "readonly";

            tempString += getTempStringResponse(childTableStartingID, backgroundColor, purchaseDTO, packageName,
                    typeName, itemName, remainingStockAmount, appAllocatedString,
                    appAllocatedRemainingString, readonly, unitPriceString, checked);


        }

        OptionsWithCountDTO optionsWithCountDTO = new OptionsWithCountDTO();
        optionsWithCountDTO.itemsHtml = tempString;
        optionsWithCountDTO.itemsCount = childTableStartingID;
        return optionsWithCountDTO;
    }

    public String getTempStringResponse(int childTableStartingID, String backgroundColor, Pi_purchaseDTO purchaseDTO, String packageName,
                                        String typeName, String itemName, String remainingStockAmount, String appAllocatedString,
                                        String appAllocatedRemainingString, String readonly, String unitPriceString, String checked) {
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String tempString = "<tr id = \"Purchase_" + childTableStartingID + "\" " + backgroundColor + ">\n" +
                "                                    <td style=\"display: none;\">\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "                                        <input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_" + childTableStartingID + "' value='" + purchaseDTO.iD + "' tag='pb_html'/>\n" +
                "                                        <input type='hidden' class='form-control'  name='itemId' id = 'itemId_hidden_" + childTableStartingID + "' value='" + purchaseDTO.itemId + "' tag='pb_html'/>\n" +
                "                                        <input type='hidden' class='form-control'  name='selected' id = 'selected_hidden_" + childTableStartingID + "' tag='pb_html'/>\n" +
                "                                        <input type='hidden' class='form-control'  name='itemTypeId' id = 'itemTypeId_hidden_" + childTableStartingID + "' value='" + purchaseDTO.itemTypeId + "' tag='pb_html'/>\n" +
                "                                        <input type='hidden' class='form-control'  name='allocatedApp' id = 'allocatedApp_hidden_" + childTableStartingID + "' value='" + appAllocatedString + "' tag='pb_html'/>\n" +
                "\n" +
                "                                    </td>\n" +
                "                                    <td>\n" +
                "\n" +
                "\n" +
                packageName +
                "\n" +
                "                                    </td>\n" +
                "                                    <td>\n" +
                "\n" +
                "\n" +
                typeName +
                "\n" +
                "                                    </td>\n" +
                "                                    <td>\n" +
                "\n" +
                "\n" +
                itemName +
                "\n" +
                "                                    </td>\n" +
                "                                    <td>\n" +
                "\n" +
                remainingStockAmount +
                "\n" +
                "                                    </td>\n" +
                "                                    <td>\n" +
                "\n" +
                "                                        <input type='number' min = '0' " + readonly + " class='form-control w-auto' onInput=\"loadTotalValue(" + childTableStartingID + ",'" + Language + "')\" name='amount' id = 'amount_number_" + childTableStartingID + "' value='" + purchaseDTO.amount + "' tag='pb_html'>\n" +
                "\n" +
                "                                    </td>\n" +
                "                                    <td>\n" +
                "\n" +
                appAllocatedString +
                "\n" +
                "                                    </td>\n" +
                "                                    <td>\n" +
                "\n" +
                appAllocatedRemainingString +
                "\n" +
                "                                    </td>\n" +
                "                                    <td>\n" +
                "\n" +
                "                                        <input type='number' min = '0' readonly class='form-control w-auto'  name='unitPrice' id = 'unitPrice_number_" + childTableStartingID + "' value='" + unitPriceString + "' tag='pb_html'>\n" +
                "\n" +
                "                                    </td>\n" +
                "                                    <td>\n" +
                "\n" +
                "                                        <input type='text' readonly class='form-control w-auto'  name='totalBill' id = 'totalBill_number_" + childTableStartingID + "' value='" + purchaseDTO.totalBill + "' tag='pb_html'>\n" +
                "\n" +
                "                                    </td>\n" +
                "                                    <td>\n" +
                "\n" +
                "                                        <input type='hidden' name='expiryDate' id = 'expiryDate_date_" + childTableStartingID + "' value= '" + dateFormat.format(new Date(purchaseDTO.expiryDate)) + "' >\n" +
                "\n" +
                "                                    </td>\n" +
                "                                    <td>\n" +
                "\n" +
                "                                        <input type='text' class='form-control w-auto'  name='purchaseItemRemarks' id = 'purchaseItemRemarks_" + childTableStartingID + "' value='" + purchaseDTO.purchaseItemRemarks + "' tag='pb_html'>\n" +
                "\n" +
                "                                    </td>\n" +
                "									 <td class=\"text-right\">\n" +
                "<input type='checkbox' " + checked + " name='isSelected' onClick='toggleRowEnabled(" + childTableStartingID + ");' id='isSelected_checkbox_" + childTableStartingID + "' tag='pb_html'/>" +
                "                                    </td>" +
                "                                    <td style=\"display: none;\">\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "                                        <input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_" + childTableStartingID + "' value='" + purchaseDTO.searchColumn + "' tag='pb_html'/>\n" +
                "                                    </td>\n" +
                "                                    <td style=\"display: none;\">\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "                                        <input type='hidden' class='form-control'  name='insertedBy' id = 'insertedByOrganogramId_hidden_" + childTableStartingID + "' value='" + purchaseDTO.insertedBy + "' tag='pb_html'/>\n" +
                "                                    </td>\n" +
                "                                    <td style=\"display: none;\">\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "                                        <input type='hidden' class='form-control'  name='insertionTime' id = 'insertionDate_hidden_" + childTableStartingID + "' value='" + purchaseDTO.insertionTime + "' tag='pb_html'/>\n" +
                "                                    </td>\n" +
                "                                    <td style=\"display: none;\">\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "                                        <input type='hidden' class='form-control'  name='modifiedBy' id = 'lastModifierUser_hidden_" + childTableStartingID + "' value='" + purchaseDTO.modifiedBy + "' tag='pb_html'/>\n" +
                "                                    </td>\n" +
                "                                    <td style=\"display: none;\">\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "                                        <input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_" + childTableStartingID + "' value= '" + purchaseDTO.isDeleted + "' tag='pb_html'/>\n" +
                "\n" +
                "                                    </td>\n" +
                "                                    <td style=\"display: none;\">\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "                                        <input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_" + childTableStartingID + "' value='" + purchaseDTO.lastModificationTime + "' tag='pb_html'/>\n" +
                "                                    </td>\n" +
                "                                </tr>";

        return tempString;
    }
}

