package pi_package_vendor;

import java.util.*;

import util.*;

public class PiPackageVendorChildrenDTO extends CommonDTO {
    public long piPackageVendorId = -1;
    public String name = "";
    public String address = "";
    public String mobile = "";
    public boolean isChosen = false;
    public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
    public long fiscalYearId = -1;
    public long packageId = -1;
    public long lotId = -1;
    public int vendorStatus = -1;
    public long officeUnitId = -1;
    public long actualVendorId = -1;

    public List<PiPackageVendorChildrenDTO> piPackageVendorChildrenDTOList = new ArrayList<>();

    @Override
    public String toString() {
        return "$PiPackageVendorChildrenDTO[" +
                " iD = " + iD +
                " piPackageVendorId = " + piPackageVendorId +
                " name = " + name +
                " address = " + address +
                " mobile = " + mobile +
                " isChosen = " + isChosen +
                " insertionDate = " + insertionDate +
                " insertedBy = " + insertedBy +
                " modifiedBy = " + modifiedBy +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                " searchColumn = " + searchColumn +
                " fiscalYearId = " + fiscalYearId +
                " packageId = " + packageId +
                " auctioneerStatus = " + vendorStatus +
                " officeUnitId = " + officeUnitId +
                " actualVendorId = " + actualVendorId +
                "]";
    }
}