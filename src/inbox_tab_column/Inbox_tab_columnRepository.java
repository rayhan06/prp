package inbox_tab_column;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Inbox_tab_columnRepository implements Repository {
	Inbox_tab_columnDAO inbox_tab_columnDAO = null;
	
	public void setDAO(Inbox_tab_columnDAO inbox_tab_columnDAO)
	{
		this.inbox_tab_columnDAO = inbox_tab_columnDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Inbox_tab_columnRepository.class);
	Map<Long, Inbox_tab_columnDTO>mapOfInbox_tab_columnDTOToid;
	Map<Integer, Set<Inbox_tab_columnDTO> >mapOfInbox_tab_columnDTOToinboxTabId;
	Map<Integer, Set<Inbox_tab_columnDTO> >mapOfInbox_tab_columnDTOToisSorted;
	Map<Integer, Set<Inbox_tab_columnDTO> >mapOfInbox_tab_columnDTOTovisibility;
	Map<Integer, Set<Inbox_tab_columnDTO> >mapOfInbox_tab_columnDTOTohasHyperlink;
	Map<Long, Set<Inbox_tab_columnDTO> >mapOfInbox_tab_columnDTOTolastModificationTime;


	static Inbox_tab_columnRepository instance = null;  
	private Inbox_tab_columnRepository(){
		mapOfInbox_tab_columnDTOToid = new ConcurrentHashMap<>();
		mapOfInbox_tab_columnDTOToinboxTabId = new ConcurrentHashMap<>();
		mapOfInbox_tab_columnDTOToisSorted = new ConcurrentHashMap<>();
		mapOfInbox_tab_columnDTOTovisibility = new ConcurrentHashMap<>();
		mapOfInbox_tab_columnDTOTohasHyperlink = new ConcurrentHashMap<>();
		mapOfInbox_tab_columnDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Inbox_tab_columnRepository getInstance(){
		if (instance == null){
			instance = new Inbox_tab_columnRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(inbox_tab_columnDAO == null)
		{
			return;
		}
		try {
			List<Inbox_tab_columnDTO> inbox_tab_columnDTOs = inbox_tab_columnDAO.getAllInbox_tab_column(reloadAll);
			for(Inbox_tab_columnDTO inbox_tab_columnDTO : inbox_tab_columnDTOs) {
				Inbox_tab_columnDTO oldInbox_tab_columnDTO = getInbox_tab_columnDTOByid(inbox_tab_columnDTO.id);
				if( oldInbox_tab_columnDTO != null ) {
					mapOfInbox_tab_columnDTOToid.remove(oldInbox_tab_columnDTO.id);
				
					if(mapOfInbox_tab_columnDTOToinboxTabId.containsKey(oldInbox_tab_columnDTO.inboxTabId)) {
						mapOfInbox_tab_columnDTOToinboxTabId.get(oldInbox_tab_columnDTO.inboxTabId).remove(oldInbox_tab_columnDTO);
					}
					if(mapOfInbox_tab_columnDTOToinboxTabId.get(oldInbox_tab_columnDTO.inboxTabId).isEmpty()) {
						mapOfInbox_tab_columnDTOToinboxTabId.remove(oldInbox_tab_columnDTO.inboxTabId);
					}
					
					if(mapOfInbox_tab_columnDTOToisSorted.containsKey(oldInbox_tab_columnDTO.isSorted)) {
						mapOfInbox_tab_columnDTOToisSorted.get(oldInbox_tab_columnDTO.isSorted).remove(oldInbox_tab_columnDTO);
					}
					if(mapOfInbox_tab_columnDTOToisSorted.get(oldInbox_tab_columnDTO.isSorted).isEmpty()) {
						mapOfInbox_tab_columnDTOToisSorted.remove(oldInbox_tab_columnDTO.isSorted);
					}
					
					if(mapOfInbox_tab_columnDTOTovisibility.containsKey(oldInbox_tab_columnDTO.visibility)) {
						mapOfInbox_tab_columnDTOTovisibility.get(oldInbox_tab_columnDTO.visibility).remove(oldInbox_tab_columnDTO);
					}
					if(mapOfInbox_tab_columnDTOTovisibility.get(oldInbox_tab_columnDTO.visibility).isEmpty()) {
						mapOfInbox_tab_columnDTOTovisibility.remove(oldInbox_tab_columnDTO.visibility);
					}
					
					if(mapOfInbox_tab_columnDTOTohasHyperlink.containsKey(oldInbox_tab_columnDTO.hasHyperlink)) {
						mapOfInbox_tab_columnDTOTohasHyperlink.get(oldInbox_tab_columnDTO.hasHyperlink).remove(oldInbox_tab_columnDTO);
					}
					if(mapOfInbox_tab_columnDTOTohasHyperlink.get(oldInbox_tab_columnDTO.hasHyperlink).isEmpty()) {
						mapOfInbox_tab_columnDTOTohasHyperlink.remove(oldInbox_tab_columnDTO.hasHyperlink);
					}
					
					if(mapOfInbox_tab_columnDTOTolastModificationTime.containsKey(oldInbox_tab_columnDTO.lastModificationTime)) {
						mapOfInbox_tab_columnDTOTolastModificationTime.get(oldInbox_tab_columnDTO.lastModificationTime).remove(oldInbox_tab_columnDTO);
					}
					if(mapOfInbox_tab_columnDTOTolastModificationTime.get(oldInbox_tab_columnDTO.lastModificationTime).isEmpty()) {
						mapOfInbox_tab_columnDTOTolastModificationTime.remove(oldInbox_tab_columnDTO.lastModificationTime);
					}
					
					
				}
				if(inbox_tab_columnDTO.isDeleted == 0) 
				{
					
					mapOfInbox_tab_columnDTOToid.put(inbox_tab_columnDTO.id, inbox_tab_columnDTO);
				
					if( ! mapOfInbox_tab_columnDTOToinboxTabId.containsKey(inbox_tab_columnDTO.inboxTabId)) {
						mapOfInbox_tab_columnDTOToinboxTabId.put(inbox_tab_columnDTO.inboxTabId, new HashSet<>());
					}
					mapOfInbox_tab_columnDTOToinboxTabId.get(inbox_tab_columnDTO.inboxTabId).add(inbox_tab_columnDTO);
					
					if( ! mapOfInbox_tab_columnDTOToisSorted.containsKey(inbox_tab_columnDTO.isSorted)) {
						mapOfInbox_tab_columnDTOToisSorted.put(inbox_tab_columnDTO.isSorted, new HashSet<>());
					}
					mapOfInbox_tab_columnDTOToisSorted.get(inbox_tab_columnDTO.isSorted).add(inbox_tab_columnDTO);
					
					if( ! mapOfInbox_tab_columnDTOTovisibility.containsKey(inbox_tab_columnDTO.visibility)) {
						mapOfInbox_tab_columnDTOTovisibility.put(inbox_tab_columnDTO.visibility, new HashSet<>());
					}
					mapOfInbox_tab_columnDTOTovisibility.get(inbox_tab_columnDTO.visibility).add(inbox_tab_columnDTO);
					
					if( ! mapOfInbox_tab_columnDTOTohasHyperlink.containsKey(inbox_tab_columnDTO.hasHyperlink)) {
						mapOfInbox_tab_columnDTOTohasHyperlink.put(inbox_tab_columnDTO.hasHyperlink, new HashSet<>());
					}
					mapOfInbox_tab_columnDTOTohasHyperlink.get(inbox_tab_columnDTO.hasHyperlink).add(inbox_tab_columnDTO);
					
					if( ! mapOfInbox_tab_columnDTOTolastModificationTime.containsKey(inbox_tab_columnDTO.lastModificationTime)) {
						mapOfInbox_tab_columnDTOTolastModificationTime.put(inbox_tab_columnDTO.lastModificationTime, new HashSet<>());
					}
					mapOfInbox_tab_columnDTOTolastModificationTime.get(inbox_tab_columnDTO.lastModificationTime).add(inbox_tab_columnDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Inbox_tab_columnDTO> getInbox_tab_columnList() {
		List <Inbox_tab_columnDTO> inbox_tab_columns = new ArrayList<Inbox_tab_columnDTO>(this.mapOfInbox_tab_columnDTOToid.values());
		return inbox_tab_columns;
	}
	
	
	public Inbox_tab_columnDTO getInbox_tab_columnDTOByid( long id){
		return mapOfInbox_tab_columnDTOToid.get(id);
	}
	
	
	public List<Inbox_tab_columnDTO> getInbox_tab_columnDTOByinbox_tab_id(int inbox_tab_id) {
		return new ArrayList<>( mapOfInbox_tab_columnDTOToinboxTabId.getOrDefault(inbox_tab_id,new HashSet<>()));
	}
	
	
	public List<Inbox_tab_columnDTO> getInbox_tab_columnDTOByisSorted(int isSorted) {
		return new ArrayList<>( mapOfInbox_tab_columnDTOToisSorted.getOrDefault(isSorted,new HashSet<>()));
	}
	
	
	public List<Inbox_tab_columnDTO> getInbox_tab_columnDTOByvisibility(int visibility) {
		return new ArrayList<>( mapOfInbox_tab_columnDTOTovisibility.getOrDefault(visibility,new HashSet<>()));
	}
	
	
	public List<Inbox_tab_columnDTO> getInbox_tab_columnDTOByhasHyperlink(int hasHyperlink) {
		return new ArrayList<>( mapOfInbox_tab_columnDTOTohasHyperlink.getOrDefault(hasHyperlink,new HashSet<>()));
	}
	
	
	public List<Inbox_tab_columnDTO> getInbox_tab_columnDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfInbox_tab_columnDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "inbox_tab_column";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


