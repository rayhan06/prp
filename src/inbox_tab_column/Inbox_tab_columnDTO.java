package inbox_tab_column;
import java.util.*; 
import util.*; 


public class Inbox_tab_columnDTO extends CommonDTO
{

	public long id = 0;
	public int inboxTabId = 0;
	public int isSorted = 0;
	public int visibility = 0;
	public int hasHyperlink = 0;
	
	
    @Override
	public String toString() {
            return "$Inbox_tab_columnDTO[" +
            " id = " + id +
            " inboxTabId = " + inboxTabId +
            " isSorted = " + isSorted +
            " visibility = " + visibility +
            " hasHyperlink = " + hasHyperlink +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}