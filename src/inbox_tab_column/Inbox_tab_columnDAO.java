package inbox_tab_column;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import approval_module_map.*;

public class Inbox_tab_columnDAO  extends NavigationService3
{
	
	Logger logger = Logger.getLogger(getClass());

	
	public Inbox_tab_columnDAO(String tableName, String tempTableName, Approval_module_mapDTO approval_module_mapDTO)
	{
		super(tableName, tempTableName, approval_module_mapDTO);		
	}
	
	
	
	public long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception
	{
		
		Inbox_tab_columnDTO inbox_tab_columnDTO = (Inbox_tab_columnDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			inbox_tab_columnDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "id";
			sql += ", ";
			sql += "inbox_tab_id";
			sql += ", ";
			sql += "isSorted";
			sql += ", ";
			sql += "visibility";
			sql += ", ";
			sql += "hasHyperlink";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
			if(tempTableDTO!=null)
			{
				sql += ", permanent_table_id, operation_type, approval_path_type, approval_order";
			}
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			if(tempTableDTO!=null)
			{
				sql += ", " + tempTableDTO.permanent_table_id + ", " + tempTableDTO.operation_type + ", " + tempTableDTO.approval_path_type + ", " + tempTableDTO.approval_order;
			}
			sql += ")";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,inbox_tab_columnDTO.id);
			ps.setObject(index++,inbox_tab_columnDTO.inboxTabId);
			ps.setObject(index++,inbox_tab_columnDTO.isSorted);
			ps.setObject(index++,inbox_tab_columnDTO.visibility);
			ps.setObject(index++,inbox_tab_columnDTO.hasHyperlink);
			ps.setObject(index++,inbox_tab_columnDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime, tableName);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return inbox_tab_columnDTO.id;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID, String tableName) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_tab_columnDTO inbox_tab_columnDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE id=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				inbox_tab_columnDTO = new Inbox_tab_columnDTO();

				inbox_tab_columnDTO.id = rs.getLong("id");
				inbox_tab_columnDTO.inboxTabId = rs.getInt("inbox_tab_id");
				inbox_tab_columnDTO.isSorted = rs.getInt("isSorted");
				inbox_tab_columnDTO.visibility = rs.getInt("visibility");
				inbox_tab_columnDTO.hasHyperlink = rs.getInt("hasHyperlink");
				inbox_tab_columnDTO.isDeleted = rs.getInt("isDeleted");
				inbox_tab_columnDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_tab_columnDTO;
	}
	
	public long update(CommonDTO commonDTO, String tableName) throws Exception
	{		
		Inbox_tab_columnDTO inbox_tab_columnDTO = (Inbox_tab_columnDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "inbox_tab_id=?";
			sql += ", ";
			sql += "isSorted=?";
			sql += ", ";
			sql += "visibility=?";
			sql += ", ";
			sql += "hasHyperlink=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE id = " + inbox_tab_columnDTO.id;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,inbox_tab_columnDTO.inboxTabId);
			ps.setObject(index++,inbox_tab_columnDTO.isSorted);
			ps.setObject(index++,inbox_tab_columnDTO.visibility);
			ps.setObject(index++,inbox_tab_columnDTO.hasHyperlink);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection,  lastModificationTime, tableName);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return inbox_tab_columnDTO.id;
	}
	
	public List<Inbox_tab_columnDTO> getDTOs(Collection recordIDs)
	{
		return getDTOs(recordIDs, tableName);
	}
	
	public List<Inbox_tab_columnDTO> getDTOs(Collection recordIDs, String tableName){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_tab_columnDTO inbox_tab_columnDTO = null;
		List<Inbox_tab_columnDTO> inbox_tab_columnDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return inbox_tab_columnDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE id IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				inbox_tab_columnDTO = new Inbox_tab_columnDTO();
				inbox_tab_columnDTO.id = rs.getLong("id");
				inbox_tab_columnDTO.inboxTabId = rs.getInt("inbox_tab_id");
				inbox_tab_columnDTO.isSorted = rs.getInt("isSorted");
				inbox_tab_columnDTO.visibility = rs.getInt("visibility");
				inbox_tab_columnDTO.hasHyperlink = rs.getInt("hasHyperlink");
				inbox_tab_columnDTO.isDeleted = rs.getInt("isDeleted");
				inbox_tab_columnDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + inbox_tab_columnDTO);
				
				inbox_tab_columnDTOList.add(inbox_tab_columnDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_tab_columnDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Inbox_tab_columnDTO> getAllInbox_tab_column (boolean isFirstReload)
    {
		List<Inbox_tab_columnDTO> inbox_tab_columnDTOList = new ArrayList<>();

		String sql = "SELECT * FROM inbox_tab_column";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by inbox_tab_column.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Inbox_tab_columnDTO inbox_tab_columnDTO = new Inbox_tab_columnDTO();
				inbox_tab_columnDTO.id = rs.getLong("id");
				inbox_tab_columnDTO.inboxTabId = rs.getInt("inbox_tab_id");
				inbox_tab_columnDTO.isSorted = rs.getInt("isSorted");
				inbox_tab_columnDTO.visibility = rs.getInt("visibility");
				inbox_tab_columnDTO.hasHyperlink = rs.getInt("hasHyperlink");
				inbox_tab_columnDTO.isDeleted = rs.getInt("isDeleted");
				inbox_tab_columnDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				inbox_tab_columnDTOList.add(inbox_tab_columnDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return inbox_tab_columnDTOList;
    }
	
	public List<Inbox_tab_columnDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset)
	{
		return getDTOs(p_searchCriteria, limit, offset, tableName, true, 0);
	}
	
	public List<Inbox_tab_columnDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Inbox_tab_columnDTO> inbox_tab_columnDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, tableName, isPermanentTable, userApprovalPathType);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Inbox_tab_columnDTO inbox_tab_columnDTO = new Inbox_tab_columnDTO();
				inbox_tab_columnDTO.id = rs.getLong("id");
				inbox_tab_columnDTO.inboxTabId = rs.getInt("inbox_tab_id");
				inbox_tab_columnDTO.isSorted = rs.getInt("isSorted");
				inbox_tab_columnDTO.visibility = rs.getInt("visibility");
				inbox_tab_columnDTO.hasHyperlink = rs.getInt("hasHyperlink");
				inbox_tab_columnDTO.isDeleted = rs.getInt("isDeleted");
				inbox_tab_columnDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				inbox_tab_columnDTOList.add(inbox_tab_columnDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_tab_columnDTOList;
	
	}

		
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType)
    {
		Inbox_tab_columnMAPS maps = new Inbox_tab_columnMAPS(tableName);
		String joinSQL = "";
		return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, tableName, isPermanentTable, userApprovalPathType, maps, joinSQL);
    }			
}
	