package inbox_tab_column;
import java.util.*; 
import util.*;


public class Inbox_tab_columnMAPS extends CommonMaps
{	
	public Inbox_tab_columnMAPS(String tableName)
	{
		
		java_allfield_type_map.put("inbox_tab_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("isSorted".toLowerCase(), "Integer");
		java_allfield_type_map.put("visibility".toLowerCase(), "Integer");
		java_allfield_type_map.put("hasHyperlink".toLowerCase(), "Integer");

		java_anyfield_search_map.put(tableName + ".inbox_tab_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".isSorted".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".visibility".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".hasHyperlink".toLowerCase(), "Integer");

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("inboxTabId".toLowerCase(), "inboxTabId".toLowerCase());
		java_DTO_map.put("isSorted".toLowerCase(), "isSorted".toLowerCase());
		java_DTO_map.put("visibility".toLowerCase(), "visibility".toLowerCase());
		java_DTO_map.put("hasHyperlink".toLowerCase(), "hasHyperlink".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("inbox_tab_id".toLowerCase(), "inboxTabId".toLowerCase());
		java_SQL_map.put("isSorted".toLowerCase(), "isSorted".toLowerCase());
		java_SQL_map.put("visibility".toLowerCase(), "visibility".toLowerCase());
		java_SQL_map.put("hasHyperlink".toLowerCase(), "hasHyperlink".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Inbox Tab Id".toLowerCase(), "inboxTabId".toLowerCase());
		java_Text_map.put("IsSorted".toLowerCase(), "isSorted".toLowerCase());
		java_Text_map.put("Visibility".toLowerCase(), "visibility".toLowerCase());
		java_Text_map.put("HasHyperlink".toLowerCase(), "hasHyperlink".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}