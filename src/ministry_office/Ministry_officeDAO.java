package ministry_office;

import common.NameDao;

public class Ministry_officeDAO extends NameDao {

    private Ministry_officeDAO() {
        super("ministry_office");
    }

    private static class LazyLoader{
        static final Ministry_officeDAO INSTANCE = new Ministry_officeDAO();
    }

    public static Ministry_officeDAO getInstance(){
        return LazyLoader.INSTANCE;
    }
}