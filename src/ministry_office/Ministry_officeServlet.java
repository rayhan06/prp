package ministry_office;

import common.BaseServlet;
import common.NameDao;
import common.NameInterface;
import common.NameRepository;
import language.LC;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

@WebServlet("/Ministry_officeServlet")
@MultipartConfig
public class Ministry_officeServlet extends BaseServlet implements NameInterface {
    private final Ministry_officeDAO ministry_officeDAO = Ministry_officeDAO.getInstance();

    public String commonPartOfDispatchURL(){
        return  "common/name";
    }

    @Override
    public String getTableName() {
        return ministry_officeDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Ministry_officeServlet";
    }

    @Override
    public NameDao getCommonDAOService() {
        return ministry_officeDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return addT(request,addFlag,userDTO,ministry_officeDAO);
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.MINISTRY_OFFICE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.MINISTRY_OFFICE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.MINISTRY_OFFICE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Ministry_officeServlet.class;
    }

    @Override
    public int getSearchTitleValue() {
        return LC.MINISTRY_OFFICE_SEARCH_MINISTRY_OFFICE_SEARCH_FORMNAME;
    }

    @Override
    public String get_p_navigatorName() {
        return SessionConstants.NAV_MINISTRY_OFFICE;
    }

    @Override
    public String get_p_dtoCollectionName() {
        return SessionConstants.VIEW_MINISTRY_OFFICE;
    }

    @Override
    public NameRepository getNameRepository() {
        return Ministry_officeRepository.getInstance();
    }

    @Override
    public int getAddTitleValue() {
        return LC.MINISTRY_OFFICE_ADD_MINISTRY_OFFICE_ADD_FORMNAME;
    }

    @Override
    public int getEditTitleValue() {
        return LC.MINISTRY_OFFICE_EDIT_MINISTRY_OFFICE_EDIT_FORMNAME;
    }

    @Override
    public void init(HttpServletRequest request) throws ServletException {
        setCommonAttributes(request,getServletName(),getCommonDAOService());
    }
}