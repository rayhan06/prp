package ministry_office;

import common.NameRepository;


public class Ministry_officeRepository extends NameRepository {

    private Ministry_officeRepository(){
        super(Ministry_officeDAO.getInstance(), Ministry_officeRepository.class);
    }

    private static class Ministry_officeRepositoryLoader{
        static Ministry_officeRepository INSTANCE = new Ministry_officeRepository();
    }

    public synchronized static Ministry_officeRepository getInstance(){
        return Ministry_officeRepository.Ministry_officeRepositoryLoader.INSTANCE;
    }
}