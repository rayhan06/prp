package gate_pass_type;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import parliament_gate.Parliament_gateDAO;
import parliament_gate.Parliament_gateDTO;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;


public class Gate_pass_typeRepository implements Repository {
	Gate_pass_typeDAO gate_pass_typeDAO = new Gate_pass_typeDAO();
	
	public void setDAO(Gate_pass_typeDAO gate_pass_typeDAO)
	{
		this.gate_pass_typeDAO = gate_pass_typeDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Gate_pass_typeRepository.class);
	Map<Long, Gate_pass_typeDTO>mapOfGate_pass_typeDTOToid;
	Map<String, Set<Gate_pass_typeDTO> >mapOfGate_pass_typeDTOTonameEng;
	Map<String, Set<Gate_pass_typeDTO> >mapOfGate_pass_typeDTOTonameBng;
	Map<String, Set<Gate_pass_typeDTO> >mapOfGate_pass_typeDTOTodescriptionEng;
	Map<String, Set<Gate_pass_typeDTO> >mapOfGate_pass_typeDTOTodescriptionBng;
	Map<Long, Set<Gate_pass_typeDTO> >mapOfGate_pass_typeDTOToinsertionDate;
	Map<String, Set<Gate_pass_typeDTO> >mapOfGate_pass_typeDTOToinsertedBy;
	Map<String, Set<Gate_pass_typeDTO> >mapOfGate_pass_typeDTOTomodifiedBy;
	Map<Long, Set<Gate_pass_typeDTO> >mapOfGate_pass_typeDTOTolastModificationTime;


	static Gate_pass_typeRepository instance = null;  
	private Gate_pass_typeRepository(){
		mapOfGate_pass_typeDTOToid = new ConcurrentHashMap<>();
		mapOfGate_pass_typeDTOTonameEng = new ConcurrentHashMap<>();
		mapOfGate_pass_typeDTOTonameBng = new ConcurrentHashMap<>();
		mapOfGate_pass_typeDTOTodescriptionEng = new ConcurrentHashMap<>();
		mapOfGate_pass_typeDTOTodescriptionBng = new ConcurrentHashMap<>();
		mapOfGate_pass_typeDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfGate_pass_typeDTOToinsertedBy = new ConcurrentHashMap<>();
		mapOfGate_pass_typeDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfGate_pass_typeDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Gate_pass_typeRepository getInstance(){
		if (instance == null){
			instance = new Gate_pass_typeRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(gate_pass_typeDAO == null)
		{
			return;
		}
		try {
			List<Gate_pass_typeDTO> gate_pass_typeDTOs = gate_pass_typeDAO.getAllGate_pass_type(reloadAll);
			for(Gate_pass_typeDTO gate_pass_typeDTO : gate_pass_typeDTOs) {
				Gate_pass_typeDTO oldGate_pass_typeDTO = getGate_pass_typeDTOByid(gate_pass_typeDTO.iD);
				if( oldGate_pass_typeDTO != null ) {
					mapOfGate_pass_typeDTOToid.remove(oldGate_pass_typeDTO.iD);
				
					if(mapOfGate_pass_typeDTOTonameEng.containsKey(oldGate_pass_typeDTO.nameEng)) {
						mapOfGate_pass_typeDTOTonameEng.get(oldGate_pass_typeDTO.nameEng).remove(oldGate_pass_typeDTO);
					}
					if(mapOfGate_pass_typeDTOTonameEng.get(oldGate_pass_typeDTO.nameEng).isEmpty()) {
						mapOfGate_pass_typeDTOTonameEng.remove(oldGate_pass_typeDTO.nameEng);
					}
					
					if(mapOfGate_pass_typeDTOTonameBng.containsKey(oldGate_pass_typeDTO.nameBng)) {
						mapOfGate_pass_typeDTOTonameBng.get(oldGate_pass_typeDTO.nameBng).remove(oldGate_pass_typeDTO);
					}
					if(mapOfGate_pass_typeDTOTonameBng.get(oldGate_pass_typeDTO.nameBng).isEmpty()) {
						mapOfGate_pass_typeDTOTonameBng.remove(oldGate_pass_typeDTO.nameBng);
					}
					
					if(mapOfGate_pass_typeDTOTodescriptionEng.containsKey(oldGate_pass_typeDTO.descriptionEng)) {
						mapOfGate_pass_typeDTOTodescriptionEng.get(oldGate_pass_typeDTO.descriptionEng).remove(oldGate_pass_typeDTO);
					}
					if(mapOfGate_pass_typeDTOTodescriptionEng.get(oldGate_pass_typeDTO.descriptionEng).isEmpty()) {
						mapOfGate_pass_typeDTOTodescriptionEng.remove(oldGate_pass_typeDTO.descriptionEng);
					}
					
					if(mapOfGate_pass_typeDTOTodescriptionBng.containsKey(oldGate_pass_typeDTO.descriptionBng)) {
						mapOfGate_pass_typeDTOTodescriptionBng.get(oldGate_pass_typeDTO.descriptionBng).remove(oldGate_pass_typeDTO);
					}
					if(mapOfGate_pass_typeDTOTodescriptionBng.get(oldGate_pass_typeDTO.descriptionBng).isEmpty()) {
						mapOfGate_pass_typeDTOTodescriptionBng.remove(oldGate_pass_typeDTO.descriptionBng);
					}
					
					if(mapOfGate_pass_typeDTOToinsertionDate.containsKey(oldGate_pass_typeDTO.insertionDate)) {
						mapOfGate_pass_typeDTOToinsertionDate.get(oldGate_pass_typeDTO.insertionDate).remove(oldGate_pass_typeDTO);
					}
					if(mapOfGate_pass_typeDTOToinsertionDate.get(oldGate_pass_typeDTO.insertionDate).isEmpty()) {
						mapOfGate_pass_typeDTOToinsertionDate.remove(oldGate_pass_typeDTO.insertionDate);
					}
					
					if(mapOfGate_pass_typeDTOToinsertedBy.containsKey(oldGate_pass_typeDTO.insertedBy)) {
						mapOfGate_pass_typeDTOToinsertedBy.get(oldGate_pass_typeDTO.insertedBy).remove(oldGate_pass_typeDTO);
					}
					if(mapOfGate_pass_typeDTOToinsertedBy.get(oldGate_pass_typeDTO.insertedBy).isEmpty()) {
						mapOfGate_pass_typeDTOToinsertedBy.remove(oldGate_pass_typeDTO.insertedBy);
					}
					
					if(mapOfGate_pass_typeDTOTomodifiedBy.containsKey(oldGate_pass_typeDTO.modifiedBy)) {
						mapOfGate_pass_typeDTOTomodifiedBy.get(oldGate_pass_typeDTO.modifiedBy).remove(oldGate_pass_typeDTO);
					}
					if(mapOfGate_pass_typeDTOTomodifiedBy.get(oldGate_pass_typeDTO.modifiedBy).isEmpty()) {
						mapOfGate_pass_typeDTOTomodifiedBy.remove(oldGate_pass_typeDTO.modifiedBy);
					}
					
					if(mapOfGate_pass_typeDTOTolastModificationTime.containsKey(oldGate_pass_typeDTO.lastModificationTime)) {
						mapOfGate_pass_typeDTOTolastModificationTime.get(oldGate_pass_typeDTO.lastModificationTime).remove(oldGate_pass_typeDTO);
					}
					if(mapOfGate_pass_typeDTOTolastModificationTime.get(oldGate_pass_typeDTO.lastModificationTime).isEmpty()) {
						mapOfGate_pass_typeDTOTolastModificationTime.remove(oldGate_pass_typeDTO.lastModificationTime);
					}
					
					
				}
				if(gate_pass_typeDTO.isDeleted == 0) 
				{
					
					mapOfGate_pass_typeDTOToid.put(gate_pass_typeDTO.iD, gate_pass_typeDTO);
				
					if( ! mapOfGate_pass_typeDTOTonameEng.containsKey(gate_pass_typeDTO.nameEng)) {
						mapOfGate_pass_typeDTOTonameEng.put(gate_pass_typeDTO.nameEng, new HashSet<>());
					}
					mapOfGate_pass_typeDTOTonameEng.get(gate_pass_typeDTO.nameEng).add(gate_pass_typeDTO);
					
					if( ! mapOfGate_pass_typeDTOTonameBng.containsKey(gate_pass_typeDTO.nameBng)) {
						mapOfGate_pass_typeDTOTonameBng.put(gate_pass_typeDTO.nameBng, new HashSet<>());
					}
					mapOfGate_pass_typeDTOTonameBng.get(gate_pass_typeDTO.nameBng).add(gate_pass_typeDTO);
					
					if( ! mapOfGate_pass_typeDTOTodescriptionEng.containsKey(gate_pass_typeDTO.descriptionEng)) {
						mapOfGate_pass_typeDTOTodescriptionEng.put(gate_pass_typeDTO.descriptionEng, new HashSet<>());
					}
					mapOfGate_pass_typeDTOTodescriptionEng.get(gate_pass_typeDTO.descriptionEng).add(gate_pass_typeDTO);
					
					if( ! mapOfGate_pass_typeDTOTodescriptionBng.containsKey(gate_pass_typeDTO.descriptionBng)) {
						mapOfGate_pass_typeDTOTodescriptionBng.put(gate_pass_typeDTO.descriptionBng, new HashSet<>());
					}
					mapOfGate_pass_typeDTOTodescriptionBng.get(gate_pass_typeDTO.descriptionBng).add(gate_pass_typeDTO);
					
					if( ! mapOfGate_pass_typeDTOToinsertionDate.containsKey(gate_pass_typeDTO.insertionDate)) {
						mapOfGate_pass_typeDTOToinsertionDate.put(gate_pass_typeDTO.insertionDate, new HashSet<>());
					}
					mapOfGate_pass_typeDTOToinsertionDate.get(gate_pass_typeDTO.insertionDate).add(gate_pass_typeDTO);
					
					if( ! mapOfGate_pass_typeDTOToinsertedBy.containsKey(gate_pass_typeDTO.insertedBy)) {
						mapOfGate_pass_typeDTOToinsertedBy.put(gate_pass_typeDTO.insertedBy, new HashSet<>());
					}
					mapOfGate_pass_typeDTOToinsertedBy.get(gate_pass_typeDTO.insertedBy).add(gate_pass_typeDTO);
					
					if( ! mapOfGate_pass_typeDTOTomodifiedBy.containsKey(gate_pass_typeDTO.modifiedBy)) {
						mapOfGate_pass_typeDTOTomodifiedBy.put(gate_pass_typeDTO.modifiedBy, new HashSet<>());
					}
					mapOfGate_pass_typeDTOTomodifiedBy.get(gate_pass_typeDTO.modifiedBy).add(gate_pass_typeDTO);
					
					if( ! mapOfGate_pass_typeDTOTolastModificationTime.containsKey(gate_pass_typeDTO.lastModificationTime)) {
						mapOfGate_pass_typeDTOTolastModificationTime.put(gate_pass_typeDTO.lastModificationTime, new HashSet<>());
					}
					mapOfGate_pass_typeDTOTolastModificationTime.get(gate_pass_typeDTO.lastModificationTime).add(gate_pass_typeDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Gate_pass_typeDTO> getGate_pass_typeList() {
		List <Gate_pass_typeDTO> gate_pass_types = new ArrayList<Gate_pass_typeDTO>(this.mapOfGate_pass_typeDTOToid.values());
		return gate_pass_types;
	}
	
	
	public Gate_pass_typeDTO getGate_pass_typeDTOByid( long id){
		return mapOfGate_pass_typeDTOToid.get(id);
	}
	
	
	public List<Gate_pass_typeDTO> getGate_pass_typeDTOByname_eng(String name_eng) {
		return new ArrayList<>( mapOfGate_pass_typeDTOTonameEng.getOrDefault(name_eng,new HashSet<>()));
	}
	
	
	public List<Gate_pass_typeDTO> getGate_pass_typeDTOByname_bng(String name_bng) {
		return new ArrayList<>( mapOfGate_pass_typeDTOTonameBng.getOrDefault(name_bng,new HashSet<>()));
	}
	
	
	public List<Gate_pass_typeDTO> getGate_pass_typeDTOBydescription_eng(String description_eng) {
		return new ArrayList<>( mapOfGate_pass_typeDTOTodescriptionEng.getOrDefault(description_eng,new HashSet<>()));
	}
	
	
	public List<Gate_pass_typeDTO> getGate_pass_typeDTOBydescription_bng(String description_bng) {
		return new ArrayList<>( mapOfGate_pass_typeDTOTodescriptionBng.getOrDefault(description_bng,new HashSet<>()));
	}
	
	
	public List<Gate_pass_typeDTO> getGate_pass_typeDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfGate_pass_typeDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Gate_pass_typeDTO> getGate_pass_typeDTOByinserted_by(String inserted_by) {
		return new ArrayList<>( mapOfGate_pass_typeDTOToinsertedBy.getOrDefault(inserted_by,new HashSet<>()));
	}
	
	
	public List<Gate_pass_typeDTO> getGate_pass_typeDTOBymodified_by(String modified_by) {
		return new ArrayList<>( mapOfGate_pass_typeDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Gate_pass_typeDTO> getGate_pass_typeDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfGate_pass_typeDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	public String buildOptions(String language, long selectedId) {
		List<OptionDTO> optionDTOList = null;
		List<Gate_pass_typeDTO> gate_pass_typeDTOS = new Gate_pass_typeDAO().getAllGate_pass_type(true);
		if (gate_pass_typeDTOS != null && gate_pass_typeDTOS.size() > 0) {
			optionDTOList = gate_pass_typeDTOS.stream()
					.map(dto -> new OptionDTO(dto.nameEng, dto.nameBng, String.valueOf(dto.iD)))
					.collect(Collectors.toList());
		}
		return Utils.buildOptions(optionDTOList, language, String.valueOf(selectedId));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "gate_pass_type";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


