package gate_pass_type;
import java.util.*; 
import util.*; 


public class Gate_pass_typeDTO extends CommonDTO
{
    public long id = -1;
    public String nameEng = "";
    public String nameBng = "";
    public String descriptionEng = "";
    public String descriptionBng = "";
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
    
    public static final int AREA_PASS =1 ;
    public static final int BUILDING_VISITOR_PASS =10;
    public static final int GALLERY_PASS =11 ;
    public static final int MEETING_PASS =110 ;
    
	
	
    @Override
	public String toString() {
            return "$Gate_pass_typeDTO[" +
            " id = " + iD +
            " nameEng = " + nameEng +
            " nameBng = " + nameBng +
            " descriptionEng = " + descriptionEng +
            " descriptionBng = " + descriptionBng +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}