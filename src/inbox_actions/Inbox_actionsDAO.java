package inbox_actions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import approval_module_map.*;

public class Inbox_actionsDAO  extends NavigationService3
{
	
	Logger logger = Logger.getLogger(getClass());

	
	public Inbox_actionsDAO(String tableName, String tempTableName, Approval_module_mapDTO approval_module_mapDTO)
	{
		super(tableName, tempTableName, approval_module_mapDTO);		
	}
	
	
	
	public long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception
	{
		
		Inbox_actionsDTO inbox_actionsDTO = (Inbox_actionsDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			inbox_actionsDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "id";
			sql += ", ";
			sql += "action_language_id";
			sql += ", ";
			sql += "link";
			sql += ", ";
			sql += "icon_link";
			sql += ", ";
			sql += "status";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
			if(tempTableDTO!=null)
			{
				sql += ", permanent_table_id, operation_type, approval_path_type, approval_order";
			}
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			if(tempTableDTO!=null)
			{
				sql += ", " + tempTableDTO.permanent_table_id + ", " + tempTableDTO.operation_type + ", " + tempTableDTO.approval_path_type + ", " + tempTableDTO.approval_order;
			}
			sql += ")";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,inbox_actionsDTO.id);
			ps.setObject(index++,inbox_actionsDTO.actionLanguageId);
			ps.setObject(index++,inbox_actionsDTO.link);
			ps.setObject(index++,inbox_actionsDTO.iconLink);
			ps.setObject(index++,inbox_actionsDTO.status);
			ps.setObject(index++,inbox_actionsDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection,  lastModificationTime, tableName);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return inbox_actionsDTO.id;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID, String tableName) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_actionsDTO inbox_actionsDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE id=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				inbox_actionsDTO = new Inbox_actionsDTO();

				inbox_actionsDTO.id = rs.getInt("id");
				inbox_actionsDTO.actionLanguageId = rs.getLong("action_language_id");
				inbox_actionsDTO.link = rs.getString("link");
				inbox_actionsDTO.iconLink = rs.getString("icon_link");
				inbox_actionsDTO.status = rs.getBoolean("status");
				inbox_actionsDTO.isDeleted = rs.getInt("isDeleted");
				inbox_actionsDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_actionsDTO;
	}
	
	public long update(CommonDTO commonDTO, String tableName) throws Exception
	{		
		Inbox_actionsDTO inbox_actionsDTO = (Inbox_actionsDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "action_language_id=?";
			sql += ", ";
			sql += "link=?";
			sql += ", ";
			sql += "icon_link=?";
			sql += ", ";
			sql += "status=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE id = " + inbox_actionsDTO.id;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,inbox_actionsDTO.actionLanguageId);
			ps.setObject(index++,inbox_actionsDTO.link);
			ps.setObject(index++,inbox_actionsDTO.iconLink);
			ps.setObject(index++,inbox_actionsDTO.status);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, lastModificationTime, tableName);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return inbox_actionsDTO.id;
	}
	
	public List<Inbox_actionsDTO> getDTOs(Collection recordIDs)
	{
		return getDTOs(recordIDs, tableName);
	}
	
	public List<Inbox_actionsDTO> getDTOs(Collection recordIDs, String tableName){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_actionsDTO inbox_actionsDTO = null;
		List<Inbox_actionsDTO> inbox_actionsDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return inbox_actionsDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE id IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				inbox_actionsDTO = new Inbox_actionsDTO();
				inbox_actionsDTO.id = rs.getInt("id");
				inbox_actionsDTO.actionLanguageId = rs.getLong("action_language_id");
				inbox_actionsDTO.link = rs.getString("link");
				inbox_actionsDTO.iconLink = rs.getString("icon_link");
				inbox_actionsDTO.status = rs.getBoolean("status");
				inbox_actionsDTO.isDeleted = rs.getInt("isDeleted");
				inbox_actionsDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + inbox_actionsDTO);
				
				inbox_actionsDTOList.add(inbox_actionsDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_actionsDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Inbox_actionsDTO> getAllInbox_actions (boolean isFirstReload)
    {
		List<Inbox_actionsDTO> inbox_actionsDTOList = new ArrayList<>();

		String sql = "SELECT * FROM inbox_actions";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by inbox_actions.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Inbox_actionsDTO inbox_actionsDTO = new Inbox_actionsDTO();
				inbox_actionsDTO.id = rs.getInt("id");
				inbox_actionsDTO.actionLanguageId = rs.getLong("action_language_id");
				inbox_actionsDTO.link = rs.getString("link");
				inbox_actionsDTO.iconLink = rs.getString("icon_link");
				inbox_actionsDTO.status = rs.getBoolean("status");
				inbox_actionsDTO.isDeleted = rs.getInt("isDeleted");
				inbox_actionsDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				inbox_actionsDTOList.add(inbox_actionsDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return inbox_actionsDTOList;
    }
	
	public List<Inbox_actionsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset)
	{
		return getDTOs(p_searchCriteria, limit, offset, tableName, true, 0);
	}
	
	public List<Inbox_actionsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Inbox_actionsDTO> inbox_actionsDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, tableName, isPermanentTable, userApprovalPathType);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Inbox_actionsDTO inbox_actionsDTO = new Inbox_actionsDTO();
				inbox_actionsDTO.id = rs.getInt("id");
				inbox_actionsDTO.actionLanguageId = rs.getLong("action_language_id");
				inbox_actionsDTO.link = rs.getString("link");
				inbox_actionsDTO.iconLink = rs.getString("icon_link");
				inbox_actionsDTO.status = rs.getBoolean("status");
				inbox_actionsDTO.isDeleted = rs.getInt("isDeleted");
				inbox_actionsDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				inbox_actionsDTOList.add(inbox_actionsDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_actionsDTOList;
	
	}

		
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType)
    {
		Inbox_actionsMAPS maps = new Inbox_actionsMAPS(tableName);
		String joinSQL = "";
		return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, tableName, isPermanentTable, userApprovalPathType, maps, joinSQL);
    }			
}
	