package inbox_actions;
import java.util.*; 
import util.*; 


public class Inbox_actionsDTO extends CommonDTO
{

	public int id = 0;
	public long actionLanguageId = 0;
    public String link = "";
    public String iconLink = "";
	public boolean status = false;
	
	
    @Override
	public String toString() {
            return "$Inbox_actionsDTO[" +
            " id = " + id +
            " actionLanguageId = " + actionLanguageId +
            " link = " + link +
            " iconLink = " + iconLink +
            " status = " + status +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}