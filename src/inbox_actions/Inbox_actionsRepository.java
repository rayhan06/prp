package inbox_actions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Inbox_actionsRepository implements Repository {
	Inbox_actionsDAO inbox_actionsDAO = null;
	
	public void setDAO(Inbox_actionsDAO inbox_actionsDAO)
	{
		this.inbox_actionsDAO = inbox_actionsDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Inbox_actionsRepository.class);
	Map<Integer, Inbox_actionsDTO>mapOfInbox_actionsDTOToid;
	Map<Long, Set<Inbox_actionsDTO> >mapOfInbox_actionsDTOToactionLanguageId;
	Map<String, Set<Inbox_actionsDTO> >mapOfInbox_actionsDTOTolink;
	Map<String, Set<Inbox_actionsDTO> >mapOfInbox_actionsDTOToiconLink;
	Map<Boolean, Set<Inbox_actionsDTO> >mapOfInbox_actionsDTOTostatus;
	Map<Long, Set<Inbox_actionsDTO> >mapOfInbox_actionsDTOTolastModificationTime;


	static Inbox_actionsRepository instance = null;  
	private Inbox_actionsRepository(){
		mapOfInbox_actionsDTOToid = new ConcurrentHashMap<>();
		mapOfInbox_actionsDTOToactionLanguageId = new ConcurrentHashMap<>();
		mapOfInbox_actionsDTOTolink = new ConcurrentHashMap<>();
		mapOfInbox_actionsDTOToiconLink = new ConcurrentHashMap<>();
		mapOfInbox_actionsDTOTostatus = new ConcurrentHashMap<>();
		mapOfInbox_actionsDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Inbox_actionsRepository getInstance(){
		if (instance == null){
			instance = new Inbox_actionsRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(inbox_actionsDAO == null)
		{
			return;
		}
		try {
			List<Inbox_actionsDTO> inbox_actionsDTOs = inbox_actionsDAO.getAllInbox_actions(reloadAll);
			for(Inbox_actionsDTO inbox_actionsDTO : inbox_actionsDTOs) {
				Inbox_actionsDTO oldInbox_actionsDTO = getInbox_actionsDTOByid(inbox_actionsDTO.id);
				if( oldInbox_actionsDTO != null ) {
					mapOfInbox_actionsDTOToid.remove(oldInbox_actionsDTO.id);
				
					if(mapOfInbox_actionsDTOToactionLanguageId.containsKey(oldInbox_actionsDTO.actionLanguageId)) {
						mapOfInbox_actionsDTOToactionLanguageId.get(oldInbox_actionsDTO.actionLanguageId).remove(oldInbox_actionsDTO);
					}
					if(mapOfInbox_actionsDTOToactionLanguageId.get(oldInbox_actionsDTO.actionLanguageId).isEmpty()) {
						mapOfInbox_actionsDTOToactionLanguageId.remove(oldInbox_actionsDTO.actionLanguageId);
					}
					
					if(mapOfInbox_actionsDTOTolink.containsKey(oldInbox_actionsDTO.link)) {
						mapOfInbox_actionsDTOTolink.get(oldInbox_actionsDTO.link).remove(oldInbox_actionsDTO);
					}
					if(mapOfInbox_actionsDTOTolink.get(oldInbox_actionsDTO.link).isEmpty()) {
						mapOfInbox_actionsDTOTolink.remove(oldInbox_actionsDTO.link);
					}
					
					if(mapOfInbox_actionsDTOToiconLink.containsKey(oldInbox_actionsDTO.iconLink)) {
						mapOfInbox_actionsDTOToiconLink.get(oldInbox_actionsDTO.iconLink).remove(oldInbox_actionsDTO);
					}
					if(mapOfInbox_actionsDTOToiconLink.get(oldInbox_actionsDTO.iconLink).isEmpty()) {
						mapOfInbox_actionsDTOToiconLink.remove(oldInbox_actionsDTO.iconLink);
					}
					
					if(mapOfInbox_actionsDTOTostatus.containsKey(oldInbox_actionsDTO.status)) {
						mapOfInbox_actionsDTOTostatus.get(oldInbox_actionsDTO.status).remove(oldInbox_actionsDTO);
					}
					if(mapOfInbox_actionsDTOTostatus.get(oldInbox_actionsDTO.status).isEmpty()) {
						mapOfInbox_actionsDTOTostatus.remove(oldInbox_actionsDTO.status);
					}
					
					if(mapOfInbox_actionsDTOTolastModificationTime.containsKey(oldInbox_actionsDTO.lastModificationTime)) {
						mapOfInbox_actionsDTOTolastModificationTime.get(oldInbox_actionsDTO.lastModificationTime).remove(oldInbox_actionsDTO);
					}
					if(mapOfInbox_actionsDTOTolastModificationTime.get(oldInbox_actionsDTO.lastModificationTime).isEmpty()) {
						mapOfInbox_actionsDTOTolastModificationTime.remove(oldInbox_actionsDTO.lastModificationTime);
					}
					
					
				}
				if(inbox_actionsDTO.isDeleted == 0) 
				{
					
					mapOfInbox_actionsDTOToid.put(inbox_actionsDTO.id, inbox_actionsDTO);
				
					if( ! mapOfInbox_actionsDTOToactionLanguageId.containsKey(inbox_actionsDTO.actionLanguageId)) {
						mapOfInbox_actionsDTOToactionLanguageId.put(inbox_actionsDTO.actionLanguageId, new HashSet<>());
					}
					mapOfInbox_actionsDTOToactionLanguageId.get(inbox_actionsDTO.actionLanguageId).add(inbox_actionsDTO);
					
					if( ! mapOfInbox_actionsDTOTolink.containsKey(inbox_actionsDTO.link)) {
						mapOfInbox_actionsDTOTolink.put(inbox_actionsDTO.link, new HashSet<>());
					}
					mapOfInbox_actionsDTOTolink.get(inbox_actionsDTO.link).add(inbox_actionsDTO);
					
					if( ! mapOfInbox_actionsDTOToiconLink.containsKey(inbox_actionsDTO.iconLink)) {
						mapOfInbox_actionsDTOToiconLink.put(inbox_actionsDTO.iconLink, new HashSet<>());
					}
					mapOfInbox_actionsDTOToiconLink.get(inbox_actionsDTO.iconLink).add(inbox_actionsDTO);
					
					if( ! mapOfInbox_actionsDTOTostatus.containsKey(inbox_actionsDTO.status)) {
						mapOfInbox_actionsDTOTostatus.put(inbox_actionsDTO.status, new HashSet<>());
					}
					mapOfInbox_actionsDTOTostatus.get(inbox_actionsDTO.status).add(inbox_actionsDTO);
					
					if( ! mapOfInbox_actionsDTOTolastModificationTime.containsKey(inbox_actionsDTO.lastModificationTime)) {
						mapOfInbox_actionsDTOTolastModificationTime.put(inbox_actionsDTO.lastModificationTime, new HashSet<>());
					}
					mapOfInbox_actionsDTOTolastModificationTime.get(inbox_actionsDTO.lastModificationTime).add(inbox_actionsDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Inbox_actionsDTO> getInbox_actionsList() {
		List <Inbox_actionsDTO> inbox_actionss = new ArrayList<Inbox_actionsDTO>(this.mapOfInbox_actionsDTOToid.values());
		return inbox_actionss;
	}
	
	
	public Inbox_actionsDTO getInbox_actionsDTOByid( int id){
		return mapOfInbox_actionsDTOToid.get(id);
	}
	
	
	public List<Inbox_actionsDTO> getInbox_actionsDTOByaction_language_id(long action_language_id) {
		return new ArrayList<>( mapOfInbox_actionsDTOToactionLanguageId.getOrDefault(action_language_id,new HashSet<>()));
	}
	
	
	public List<Inbox_actionsDTO> getInbox_actionsDTOBylink(String link) {
		return new ArrayList<>( mapOfInbox_actionsDTOTolink.getOrDefault(link,new HashSet<>()));
	}
	
	
	public List<Inbox_actionsDTO> getInbox_actionsDTOByicon_link(String icon_link) {
		return new ArrayList<>( mapOfInbox_actionsDTOToiconLink.getOrDefault(icon_link,new HashSet<>()));
	}
	
	
	public List<Inbox_actionsDTO> getInbox_actionsDTOBystatus(boolean status) {
		return new ArrayList<>( mapOfInbox_actionsDTOTostatus.getOrDefault(status,new HashSet<>()));
	}
	
	
	public List<Inbox_actionsDTO> getInbox_actionsDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfInbox_actionsDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "inbox_actions";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


