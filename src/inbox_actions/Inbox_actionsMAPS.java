package inbox_actions;
import java.util.*; 
import util.*;


public class Inbox_actionsMAPS extends CommonMaps
{	
	public Inbox_actionsMAPS(String tableName)
	{
		
		java_allfield_type_map.put("action_language_id".toLowerCase(), "Long");
		java_allfield_type_map.put("link".toLowerCase(), "String");
		java_allfield_type_map.put("icon_link".toLowerCase(), "String");
		java_allfield_type_map.put("status".toLowerCase(), "Boolean");

		java_anyfield_search_map.put(tableName + ".action_language_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".link".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".icon_link".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".status".toLowerCase(), "Boolean");

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("actionLanguageId".toLowerCase(), "actionLanguageId".toLowerCase());
		java_DTO_map.put("link".toLowerCase(), "link".toLowerCase());
		java_DTO_map.put("iconLink".toLowerCase(), "iconLink".toLowerCase());
		java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("action_language_id".toLowerCase(), "actionLanguageId".toLowerCase());
		java_SQL_map.put("link".toLowerCase(), "link".toLowerCase());
		java_SQL_map.put("icon_link".toLowerCase(), "iconLink".toLowerCase());
		java_SQL_map.put("status".toLowerCase(), "status".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Action Language Id".toLowerCase(), "actionLanguageId".toLowerCase());
		java_Text_map.put("Link".toLowerCase(), "link".toLowerCase());
		java_Text_map.put("Icon Link".toLowerCase(), "iconLink".toLowerCase());
		java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}