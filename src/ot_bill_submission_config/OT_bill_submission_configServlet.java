package ot_bill_submission_config;

import common.BaseServlet;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import login.LoginDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import org.apache.log4j.Logger;
import ot_employee_type.OT_employee_typeRepository;
import overtime_bill.BillStatusResponse;
import overtime_bill.Overtime_billDAO;
import pb.Utils;
import pbReport.DateUtils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
@WebServlet("/OT_bill_submission_configServlet")
@MultipartConfig
public class OT_bill_submission_configServlet extends BaseServlet {
    private final Logger logger = Logger.getLogger(OT_bill_submission_configServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        String actionType = request.getParameter("actionType");
        try {
            switch (actionType) {
                case "getAddPage":
                case "getEditPage":
                case "search":
                    request.setAttribute("servletName", "OT_bill_submission_configServlet");
                    super.doGet(request, response);
                    return;
                case "ajax_getActiveBillStatusForEmployeeDashboard":
                    List<BillStatusResponse> billStatusResponses = Collections.emptyList();
                    try {
                        billStatusResponses = getActiveBillStatusForEmployeeDashboard(userDTO);
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    request.setAttribute("billStatusResponses", billStatusResponses);
                    request.getRequestDispatcher("ot_bill_submission_config/ot_bill_submission_configStatusForEmployeeDashboard.jsp")
                           .forward(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        String actionType = request.getParameter("actionType");
        try {
            switch (actionType) {
                case "ajax_add":
                case "ajax_edit":
                case "delete":
                    super.doPost(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private List<BillStatusResponse> getActiveBillStatusForEmployeeDashboard(UserDTO userDTO) {
        OfficeUnitOrganograms organogram = OfficeUnitOrganogramsRepository.getInstance().getById(userDTO.organogramID);
        if (organogram == null) {
            return Collections.emptyList();
        }
        Employee_recordsDTO employeeRecord = Employee_recordsRepository.getInstance().getById(userDTO.employee_record_id);
        if (employeeRecord == null) {
            return Collections.emptyList();
        }
        long employeeRecordId = employeeRecord.iD, officeUnitsId = organogram.office_unit_id;
        int employmentCat = employeeRecord.employmentType, officerTypeCat = employeeRecord.officerTypeCat;
        Long otEmployeeTypeId = OT_employee_typeRepository.getInstance().getIdByEmployeeInfo(officeUnitsId, employmentCat, officerTypeCat);
        if (otEmployeeTypeId == null) {
            return Collections.emptyList();
        }
        boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        List<OT_bill_submission_configDTO> activeDTOs =
                OT_bill_submission_configDAO.getInstance()
                                            .findActiveDTOsByTime(System.currentTimeMillis());
        activeDTOs.sort(OT_bill_submission_configDTO.compareByBillDates);

        Overtime_billDAO overtimeBillDAO = Overtime_billDAO.getInstance();
        List<BillStatusResponse> response = new ArrayList<>();
        for (OT_bill_submission_configDTO activeDTO : activeDTOs) {
            BillStatusResponse billStatus = overtimeBillDAO.getStatusForEmployeeDashboard(
                    activeDTO.iD, officeUnitsId, otEmployeeTypeId, employeeRecordId, isLangEn
            );
            billStatus.billDescription = activeDTO.getText(isLangEn);
            response.add(billStatus);
        }
        return response;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        long currentTime = System.currentTimeMillis();

        OT_bill_submission_configDTO billSubmissionConfigDTO;
        if (addFlag) {
            billSubmissionConfigDTO = new OT_bill_submission_configDTO();
            billSubmissionConfigDTO.insertionTime = currentTime;
            billSubmissionConfigDTO.insertedBy = userDTO.ID;
        } else {
            Long id = Utils.parseMandatoryLong(
                    request.getParameter("iD"),
                    isLangEn ? "No record with found to edit" : "এডিট করার মত তথ্য পাওয়া যায়নি"
            );
            billSubmissionConfigDTO = OT_bill_submission_configDAO.getInstance().getDTOFromID(id);
        }
        billSubmissionConfigDTO.lastModificationTime = currentTime;
        billSubmissionConfigDTO.modifiedBy = userDTO.ID;

        billSubmissionConfigDTO.billStartDate = Utils.parseMandatoryDate(
                request.getParameter("billStartDate"),
                isLangEn ? "Select Bill Date" : "বিলের তারিখ বাছাই করুন"
        );
        billSubmissionConfigDTO.billEndDate = Utils.parseMandatoryDate(
                request.getParameter("billEndDate"),
                isLangEn ? "Select Bill Date" : "বিলের তারিখ বাছাই করুন"
        );
        billSubmissionConfigDTO.billEndDate += (DateUtils.ONE_DAY_IN_MILLIS - 1);
        if (billSubmissionConfigDTO.billStartDate > billSubmissionConfigDTO.billEndDate) {
            throw new IllegalArgumentException(
                    isLangEn ? "Bill To Date Must be before bill from date" : "বিল শেষের, বিল শুরুর তারিখের পরে হতে হবে"
            );
        }

        billSubmissionConfigDTO.overtimeBillTypeCat = Utils.parseOptionalInt(
                request.getParameter("overtimeBillTypeCat"),
                1,
                null
        );
        billSubmissionConfigDTO.kaDailyRate = Utils.parseOptionalLong(
                request.getParameter("kaDailyRate"),
                0L,
                null
        );
        billSubmissionConfigDTO.khaDailyRate = Utils.parseOptionalLong(
                request.getParameter("khaDailyRate"),
                0L,
                null
        );
        billSubmissionConfigDTO.revenueStampDeduction = Utils.parseOptionalInt(
                request.getParameter("revenueStampDeduction"),
                0,
                null
        );
        billSubmissionConfigDTO.ordinanceText = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("ordinanceText"));

        billSubmissionConfigDTO.startDate = Utils.parseMandatoryDate(
                request.getParameter("startDate"),
                isLangEn ? "Select submission start date" : "জমা শুরুর তারিখ বাছাই করুন"
        );
        billSubmissionConfigDTO.endDate = Utils.parseMandatoryDate(
                request.getParameter("endDate"),
                isLangEn ? "Select submission end date" : "জমা শেষের তারিখ বাছাই করুন"
        );
        billSubmissionConfigDTO.endDate += (DateUtils.ONE_DAY_IN_MILLIS - 1);
        if (billSubmissionConfigDTO.startDate > billSubmissionConfigDTO.endDate) {
            throw new IllegalArgumentException(
                    isLangEn
                    ? "Submission Start Date Must be before submission end date"
                    : "জমা শুরুর তারিখ জমা শেষের তারিখের আগে হতে হবে"
            );
        }
        billSubmissionConfigDTO.lastActiveTime = DateUtils.get1stDayOfNextMonth(billSubmissionConfigDTO.endDate) - 1;

        billSubmissionConfigDTO.allowedOfficeIds = null;
        boolean allOfficesNotChecked = request.getParameter("allOfficesCheckbox") == null;
        if (allOfficesNotChecked) {
            billSubmissionConfigDTO.allowedOfficeIds = new HashSet<>();
            String[] allowedOfficeIds = request.getParameterValues("allowedOfficeIds");
            if (allowedOfficeIds != null) {
                billSubmissionConfigDTO.allowedOfficeIds =
                        Arrays.stream(allowedOfficeIds)
                              .map(Long::parseLong)
                              .collect(Collectors.toSet());
            }
        }

        if (addFlag) {
            OT_bill_submission_configDAO.getInstance().add(billSubmissionConfigDTO);
        } else {
            OT_bill_submission_configDAO.getInstance().update(billSubmissionConfigDTO);
        }
        return billSubmissionConfigDTO;
    }

    @Override
    public String getServletName() {
        return "OT_bill_submission_configServlet";
    }

    @Override
    public OT_bill_submission_configDAO getCommonDAOService() {
        return OT_bill_submission_configDAO.getInstance();
    }

    @Override
    public String getTableName() {
        return getCommonDAOService().getTableName();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.OT_BILL_SUBMISSION_CONFIG_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.OT_BILL_SUBMISSION_CONFIG_ADD};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.OVERTIME_BILL_ADMIN_TASK};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return OT_bill_submission_configServlet.class;
    }
}
