package ot_bill_submission_config;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class OT_bill_submission_configDAO implements CommonDAOService<OT_bill_submission_configDTO> {
    private static final Logger logger = Logger.getLogger(OT_bill_submission_configDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (bill_start_date,bill_end_date,overtime_bill_type_cat,ka_daily_rate,kha_daily_rate,"
                    .concat("revenue_stamp_deduction,ordinance_text,start_date,end_date,last_active_time,allowed_office_ids,")
                    .concat("modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET bill_start_date=?,bill_end_date=?,overtime_bill_type_cat=?,ka_daily_rate=?,kha_daily_rate=?,"
                    .concat("revenue_stamp_deduction=?,ordinance_text=?,start_date=?,end_date=?,last_active_time=?,allowed_office_ids=?,")
                    .concat("modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private OT_bill_submission_configDAO() {
        searchMap.put("monthYearFrom", " AND (bill_end_date >= ?) ");
        searchMap.put("monthYearTo", " AND (bill_start_date <= ?) ");
        searchMap.put("overtimeBillTypeCat", " AND (overtime_bill_type_cat = ?) ");
    }

    private static class LazyLoader {
        static final OT_bill_submission_configDAO INSTANCE = new OT_bill_submission_configDAO();
    }

    public static OT_bill_submission_configDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public String getTableName() {
        return "ot_bill_submission_config";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public void set(PreparedStatement ps, OT_bill_submission_configDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.billStartDate);
        ps.setLong(++index, dto.billEndDate);
        ps.setInt(++index, dto.overtimeBillTypeCat);
        ps.setLong(++index, dto.kaDailyRate);
        ps.setLong(++index, dto.khaDailyRate);
        ps.setInt(++index, dto.revenueStampDeduction);
        ps.setString(++index, dto.ordinanceText);
        ps.setLong(++index, dto.startDate);
        ps.setLong(++index, dto.endDate);
        ps.setLong(++index, dto.lastActiveTime);
        ps.setString(++index, dto.getAllowedOfficeIdsStr());
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public OT_bill_submission_configDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            OT_bill_submission_configDTO dto = new OT_bill_submission_configDTO();
            dto.iD = rs.getLong("ID");
            dto.billStartDate = rs.getLong("bill_start_date");
            dto.billEndDate = rs.getLong("bill_end_date");
            dto.overtimeBillTypeCat = rs.getInt("overtime_bill_type_cat");
            dto.kaDailyRate = rs.getLong("ka_daily_rate");
            dto.khaDailyRate = rs.getLong("kha_daily_rate");
            dto.revenueStampDeduction = rs.getInt("revenue_stamp_deduction");
            dto.ordinanceText = rs.getString("ordinance_text");
            dto.startDate = rs.getLong("start_date");
            dto.endDate = rs.getLong("end_date");
            dto.lastActiveTime = rs.getLong("last_active_time");
            dto.setAllowedOfficeIds(rs.getString("allowed_office_ids"));
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((OT_bill_submission_configDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((OT_bill_submission_configDTO) commonDTO, updateSqlQuery, false);
    }

    private static final String findSubmittableDTOsByTimeSql =
            "SELECT * FROM ot_bill_submission_config WHERE isDeleted = 0 AND (start_date <= %d AND %d <= end_date)";

    public List<OT_bill_submission_configDTO> findSubmittableDTOsByTime(long timeStamp) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(findSubmittableDTOsByTimeSql, timeStamp, timeStamp),
                this::buildObjectFromResultSet
        );
    }

    private static final String findActiveDTOsByTimeSql =
            "SELECT * FROM ot_bill_submission_config WHERE isDeleted = 0 AND (bill_start_date <= %d AND %d <= last_active_time)";

    public List<OT_bill_submission_configDTO> findActiveDTOsByTime(long timeStamp) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(findActiveDTOsByTimeSql, timeStamp, timeStamp),
                this::buildObjectFromResultSet
        );
    }

    public String buildOptionsOfSubmittableDTOs(Long officeUnitId, long timeStamp, String language) {
        List<OptionDTO> optionDTOList =
                findSubmittableDTOsByTime(timeStamp)
                        .stream()
                        .filter(dto -> dto.isOfficeUnitIdAllowed(officeUnitId))
                        .sorted(OT_bill_submission_configDTO.compareByBillDates.reversed())
                        .map(OT_bill_submission_configDTO::getOptionDTO)
                        .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, null);
    }

    private static final String findAllSql = "select * from ot_bill_submission_config WHERE isDeleted = 0";

    public String buildOptions(String language, Long selectedId) {
        List<OptionDTO> optionDTOList =
                getDTOs(findAllSql).stream()
                                   .sorted(OT_bill_submission_configDTO.compareByBillDates.reversed())
                                   .map(OT_bill_submission_configDTO::getOptionDTO)
                                   .collect(Collectors.toList());
        return Utils.buildOptionsMultipleSelection(optionDTOList, language, selectedId == null ? null : String.format("%d", selectedId));
    }
}
