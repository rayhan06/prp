package ot_bill_submission_config;

import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.OptionDTO;
import pbReport.DateUtils;
import sessionmanager.SessionConstants;
import util.CommonDTO;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class OT_bill_submission_configDTO extends CommonDTO {

    private static final Logger logger = Logger.getLogger(OT_bill_submission_configDTO.class);

    public static final Comparator<OT_bill_submission_configDTO> compareByBillDates
            = Comparator.comparingLong((OT_bill_submission_configDTO submissionConfigDTO) -> submissionConfigDTO.billStartDate)
                        .thenComparingLong(submissionConfigDTO -> submissionConfigDTO.billEndDate);

    public long billStartDate = SessionConstants.MIN_DATE;
    public long billEndDate = SessionConstants.MIN_DATE;
    public int overtimeBillTypeCat = 1;

    public long kaDailyRate = 0;
    public long khaDailyRate = 0;
    public int revenueStampDeduction = 0;
    public String ordinanceText = "";
    public Set<Long> allowedOfficeIds;

    public long startDate = SessionConstants.MIN_DATE;
    public long endDate = SessionConstants.MIN_DATE;
    public long lastActiveTime = SessionConstants.MIN_DATE;
    // overtime bill active from [startDate, lastActiveTime]
    // this is used to get which bill is active at a timeframe

    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;

    public String getText(boolean isLangEn) {
        String dateRangeString = DateUtils.getDateRangeString(
                billStartDate,
                billEndDate,
                isLangEn
        );
        String billTypeText = CatRepository.getInstance().getText(
                isLangEn,
                SessionConstants.OT_BILL_TYPE_CAT_DOMAIN_NAME,
                overtimeBillTypeCat
        );
        return String.format("%s (%s)", dateRangeString, billTypeText);
    }

    public OptionDTO getOptionDTO() {
        return new OptionDTO(getText(true), getText(false), String.format("%d", iD));
    }

    public boolean isAllowedToSubmitByTime(long time) {
        boolean isStartEndDateInvalid = (startDate == SessionConstants.MIN_DATE)
                                        || (endDate == SessionConstants.MIN_DATE)
                                        || (startDate > endDate);
        if (isStartEndDateInvalid) {
            logger.error("INVALID startDate or endDate for " + this);
            return false;
        }
        return startDate <= time && time <= endDate;
    }

    public String getAllowedOfficeIdsStr() {
        if (allowedOfficeIds == null) {
            return null;
        }
        return allowedOfficeIds.stream()
                               .map(Objects::toString)
                               .collect(Collectors.joining(","));
    }

    public void setAllowedOfficeIds(String allowedOfficeIdsStr) {
        if (allowedOfficeIdsStr == null) {
            allowedOfficeIds = null;
            return;
        }
        allowedOfficeIds = Arrays.stream(allowedOfficeIdsStr.split(","))
                                 .filter(s -> !s.isEmpty())
                                 .map(Long::parseLong)
                                 .collect(Collectors.toSet());
    }

    public boolean isOfficeUnitIdAllowed(Long officeUnitId) {
        return officeUnitId == null || allowedOfficeIds == null || allowedOfficeIds.contains(officeUnitId);
    }

    @Override
    public String toString() {
        return "OT_bill_submission_configDTO{" +
               "iD=" + iD +
               ", billStartDate=" + billStartDate +
               ", billEndDate=" + billEndDate +
               ", overtimeBillTypeCat=" + overtimeBillTypeCat +
               ", kaDailyRate=" + kaDailyRate +
               ", khaDailyRate=" + khaDailyRate +
               ", ordinanceText='" + ordinanceText + "'" +
               ", startDate=" + startDate +
               ", endDate=" + endDate +
               '}';
    }
}
