package employee_acr_report;

import com.google.gson.Gson;
import common.RoleEnum;
import common.StringUtils;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import language.LC;
import language.LM;
import login.LoginDTO;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import pbReport.CriteriaColumn;
import pbReport.PBReportUtils;
import pbReport.ReportCommonService;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@WebServlet("/Employee_acr_report_Servlet")
public class Employee_acr_report_Servlet extends HttpServlet implements ReportCommonService {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Employee_acr_report_Servlet.class);
    private static final Set<String> searchParam = new HashSet<>(Arrays.asList(
            "officeUnitIds", "submissionStartDate", "submissionEndDate", "isPartial",
            "status", "year", "acrFromStart", "acrFromEnd", "nameEng", "nameBng"
    ));
    private final Map<String, String[]> stringMap = new HashMap<>();
    private String sql;
    private int reportNameLC = LC.EMPLOYEE_ACR_REPORT_OTHER_EMPLOYEE_ACR_REPORT;
    private static final String submittedAcrSql = " employee_acr ea INNER JOIN employee_offices eo ON ea.employee_records_id = eo.employee_record_id  left join employee_records er on eo.employee_record_id = er.id";

    public Employee_acr_report_Servlet() {
        stringMap.put("officeUnitIds", new String[]{"criteria", "eo", "office_unit_id", "IN", "AND", "String", "", "", "any", "officeUnitIdList",
                "officeUnitIds", String.valueOf(LC.EMPLOYEE_ACR_REPORT_WHERE_OFFICEUNITID), "office_units", null, "true", "1"});

        stringMap.put("year", new String[]{"criteria", "ea", "year", "=", "AND", "int", "", "", "any", "year",
                "year", String.valueOf(LC.EMPLOYEE_ACR_REPORT_WHERE_YEAR), "text", null, "true", "2"});

        stringMap.put("isPartial", new String[]{"criteria", "ea", "is_partial", "=", "AND", "int", "", "", "any", "isPartial",
                "isPartial", String.valueOf(LC.EMPLOYEE_ACR_REPORT_WHERE_ISPARTIAL), "category", "is_partial", "true", "3"});

        stringMap.put("acrFromStart", new String[]{"criteria", "ea", "acr_from", ">=", "AND", "long", "", "", "any", "acrFromStart",
                "acrFromStart", String.valueOf(LC.EMPLOYEE_ACR_REPORT_WHERE_ACRFROM), "date", null, "true", "4"});

        stringMap.put("acrFromEnd", new String[]{"criteria", "ea", "acr_from", "<=", "AND", "long", "", "", "any", "acrFromEnd",
                "acrFromEnd", String.valueOf(LC.EMPLOYEE_ACR_REPORT_WHERE_ACRFROM_8), "date", null, "true", "5"});

        stringMap.put("submissionStartDate", new String[]{"criteria", "ea", "insertion_date", ">=", "AND", "long", "", "", "any", "submissionStartDate",
                "submissionStartDate", String.valueOf(LC.EMPLOYEE_ACR_REPORT_WHERE_INSERTIONDATE), "date", null, "true", "6"});

        stringMap.put("submissionEndDate", new String[]{"criteria", "ea", "insertion_date", "<=", "AND", "long", "", "", "any", "submissionEndDate",
                "submissionEndDate", String.valueOf(LC.EMPLOYEE_ACR_REPORT_WHERE_INSERTIONDATE_3), "date", null, "true", "7"});

        stringMap.put("status", new String[]{"criteria", "ea", "status", "=", "AND", "int", "", "", "any", "status",
                "status", String.valueOf(LC.EMPLOYEE_ACR_REPORT_WHERE_STATUS), "category", "status", "true", "8"});
        stringMap.put("nameEng", new String[]{"criteria", "er", "name_eng", "Like", "AND", "String", "", "", "any", "nameEng",
                "nameEng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEENG), "text", null, "true", "9"});
        stringMap.put("nameBng", new String[]{"criteria", "er", "name_bng", "Like", "AND", "String", "", "", "any", "nameBng",
                "nameBng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG), "text", null, "true", "10"});
        stringMap.put("office_id", new String[]{"criteria", "eo", "office_id", "=", "AND", "long", "", "", "2294", "office_id", null, null, null, null, null, null});
        stringMap.put("isDeleted", new String[]{"criteria", "ea", "isDeleted", "=", "AND", "int", "", "", "0", "isDeleted", null, null, null, null, null, null});
        stringMap.put("isDeleted_eo", new String[]{"criteria", "eo", "isDeleted", "=", "AND", "int", "", "", "0", "isDeleted_eo", null, null, null, null, null, null});
        stringMap.put("status_eo", new String[]{"criteria", "eo", "status", "=", "AND", "long", "", "", "1", "status_eo", null, null, null, null, null, null});
    }

    private String[][] Criteria;
    private String[][] XLCriteria;

    private final String[][] Display =
            {
                    {"display", "eo", "employee_record_id", "employee_records_id", ""},
                    {"display", "eo", "employee_record_id", "user_name", ""},
                    {"display", "eo", "employee_record_id", "mobile_number", ""},
                    {"display", "eo", "employee_record_id", "email", ""},
                    {"display", "eo", "employee_record_id", "id_to_home_district", ""},
                    {"display", "eo", "office_unit_id", "office_unit", ""},
                    {"display", "eo", "id", "designation", ""},
                    {"display", "ea", "year", "text", ""},
                    {"display", "ea", "is_partial", "categorywithoutCat", ""},
                    {"display", "ea", "acr_from", "date", ""},
                    {"display", "ea", "acr_to", "date", ""},
                    {"display", "ea", "status", "categorywithoutCat", ""},
                    {"display", "ea", "insertion_date", "date", ""},
                    {"display", "ea", "remarks", "text", ""},
                    {"display", "eo", "employee_record_id", "id_to_present_address", ""},
                    {"display", "eo", "employee_record_id", "id_to_permanent_address", ""}
            };

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        Display[0][4] = LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_EMPLOYEERECORDSID, loginDTO);
        Display[1][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_USER_ID, loginDTO);
        Display[2][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_MOBILE_NUMBER, loginDTO);
        Display[3][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_EMAIL, loginDTO);
        Display[4][4] = LM.getText(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT, loginDTO);
        Display[5][4] = LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_OFFICEUNITID, loginDTO);
        Display[6][4] = LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO);
        Display[7][4] = LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_YEAR, loginDTO);
        Display[8][4] = LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_ISPARTIAL, loginDTO);
        Display[9][4] = LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_ACRFROM, loginDTO);
        Display[10][4] = LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_ACRTO, loginDTO);
        Display[11][4] = LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_STATUS, loginDTO);
        Display[12][4] = LM.getText(LC.EMPLOYEE_ACR_REPORT_WHERE_ACR_SUBMISSION_DATE, loginDTO);
        Display[13][4] = LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_REMARKS, loginDTO);
        Display[14][4] = LM.getText(LC.EMPLOYEE_RECORDS_ADD_PRESENTADDRESS, loginDTO);
        Display[15][4] = LM.getText(LC.EMPLOYEE_RECORDS_ADD_PERMANENTADDRESS, loginDTO);

        boolean isParliamentEmployee = userDTO.roleID == RoleEnum.EMPLOYEE_OF_PARLIAMENT.getRoleId();

        boolean onlySelectedOffice = Boolean.parseBoolean(request.getParameter("onlySelectedOffice"));

        String getNotSubmittedAcrReportStr = request.getParameter("getNotSubmittedAcrReport");
        boolean isSubmittedAcrReport = !Boolean.parseBoolean(getNotSubmittedAcrReportStr);

        Set<String> inputs = PBReportUtils.prepareInputSets(searchParam, request);
        inputs.add("office_id");
        if (isSubmittedAcrReport) inputs.add("isDeleted");
        inputs.add("isDeleted_eo");
        inputs.add("status_eo");
        if (isParliamentEmployee) inputs.add("officeUnitIds");

        Criteria = PBReportUtils.prepareCriteria(inputs, stringMap);
        XLCriteria = PBReportUtils.prepareCriteria(inputs, stringMap); // clone of Criteria

        if (isParliamentEmployee) {
            EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(userDTO.employee_record_id);
            long officeUnitId = employeeOfficeDTO == null ? -1 : employeeOfficeDTO.officeUnitId;

            Set<Long> descentsOfficeUnitIds = new HashSet<>(Office_unitsRepository.getInstance().getDescentsOfficeUnitId(officeUnitId));

            String officeUnitIdsStr = request.getParameter("officeUnitIds");
            if (officeUnitIdsStr != null) {
                long[] inputOfficeUnitIds = new Gson().fromJson(officeUnitIdsStr, long[].class);
                Set<Long> filteredInput = Arrays.stream(inputOfficeUnitIds)
                        .filter(descentsOfficeUnitIds::contains)
                        .boxed()
                        .collect(Collectors.toSet());
                if (!onlySelectedOffice) {
                    descentsOfficeUnitIds = filteredInput.stream()
                            .flatMap(filteredId -> Office_unitsRepository.getInstance()
                                    .getAllChildOfficeDTOWithParent(filteredId)
                                    .stream())
                            .map(e -> e.iD)
                            .collect(Collectors.toSet());
                }
            }
            setOfficeUnitIds(descentsOfficeUnitIds, Criteria);
        } else if (inputs.contains("officeUnitIds")) {
            Set<Long> officeUnitIds = Arrays.stream(new Gson().fromJson(request.getParameter("officeUnitIds"), long[].class))
                    .boxed()
                    .collect(Collectors.toSet());
            if (!onlySelectedOffice) {
                officeUnitIds = officeUnitIds.stream()
                        .flatMap(inputId -> Office_unitsRepository.getInstance()
                                .getAllChildOfficeDTOWithParent(inputId)
                                .stream())
                        .map(e -> e.iD)
                        .collect(Collectors.toSet());
            }
            setOfficeUnitIds(officeUnitIds, Criteria);
        }

        if (isSubmittedAcrReport) {
            sql = submittedAcrSql;
            reportNameLC = LC.EMPLOYEE_ACR_REPORT_OTHER_EMPLOYEE_ACR_REPORT;
        } else {
            sql = getAcrNotSubmittedQuery(request);
            Criteria = new String[][]{};
            reportNameLC = LC.EMPLOYEE_ACR_REPORT_EMPLOYEE_ACR_REPORT_NOT_SUBMITTED;
        }

        if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ACR_REPORT_DETAILS)) {
            reportGenerate(request, response);
        }
    }

    private void setOfficeUnitIds(Set<Long> officeUnitIds, String[][] Criteria) {
        if (officeUnitIds.isEmpty()) officeUnitIds.add(-1L);
        for (String[] arr : Criteria) {
            if (arr[9].equals("officeUnitIdList")) {
                arr[8] = officeUnitIds.stream()
                        .map(String::valueOf)
                        .collect(Collectors.joining(","));
                break;
            }
        }
    }

    private String getAcrNotSubmittedQuery(HttpServletRequest request) {
        Map<String, String> mapByTable = Arrays.stream(Criteria)
                .map(criterion -> new CriteriaColumn(criterion, request.getParameter(criterion[9])))
                .collect(Collectors.groupingBy(criteriaColumn -> criteriaColumn.tableName,
                        Collectors.collectingAndThen(
                                Collectors.toList(),
                                list -> list.stream()
                                        .map(CriteriaColumn::getCriterionSql)
                                        .collect(Collectors.joining())
                                        .trim()
                        )));

        StringBuilder notSubmittedAcrSql = new StringBuilder(" employee_offices eo LEFT JOIN (SELECT * FROM employee_acr WHERE isDeleted = 0) ea ")
                .append(" ON eo.employee_record_id = ea.employee_records_id ");

        String sqlOfOfficeTable = mapByTable.get("eo");
        if (StringUtils.isNotBlank(sqlOfOfficeTable))
            notSubmittedAcrSql.append("WHERE ").append(
                    sqlOfOfficeTable.startsWith("AND") ? sqlOfOfficeTable.substring(3) : sqlOfOfficeTable
            );

        String sqlOfAcrTable = mapByTable.get("ea");
        if (StringUtils.isNotBlank(sqlOfAcrTable))
            notSubmittedAcrSql.append(" AND (ea.ID is NULL OR NOT( ").append(
                    sqlOfAcrTable.startsWith("AND") ? sqlOfAcrTable.substring(3) : sqlOfAcrTable
            ).append("))");

        return notSubmittedAcrSql.toString();
    }

    @Override
    public String[][] getCriteria() {
        return Criteria;
    }

    @Override
    public String[][] getXLCriteria() {
        return XLCriteria;
    }

    @Override
    public String[][] getDisplay() {
        return Display;
    }

    @Override
    public String getSQL() {
        return sql;
    }

    @Override
    public int getLCForReportName() {
        return reportNameLC;
    }

    @Override
    public String getFileName() {
        return "employee_acr_report";
    }

    @Override
    public String getTableName() {
        return "employee_acr_report";
    }
}
