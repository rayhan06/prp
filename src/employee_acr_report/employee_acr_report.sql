-- employee_acr_report
SELECT ea.employee_records_id,
       eo.office_unit_id,
       eo.office_unit_organogram_id,
       ea.year,
       ea.is_partial,
       ea.acr_from,
       ea.acr_to,
       ea.status,
       ea.remarks
FROM employee_acr ea
         INNER JOIN employee_offices eo on ea.employee_records_id = eo.employee_record_id
WHERE ea.isDeleted = 0
  AND eo.office_unit_id = 'any'
  AND ea.insertion_date >= 'any'
  AND ea.insertion_date <= 'any'
  AND ea.is_partial = 'any'
  AND ea.status = 'any'
  AND ea.year = 'any'
  AND ea.acr_from >= 'any'
  AND ea.acr_from <= 'any'