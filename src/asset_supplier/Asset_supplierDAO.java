package asset_supplier;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.*;
import user.UserDTO;

public class Asset_supplierDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Asset_supplierDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Asset_supplierMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"company_website",
			"website",
			"contact_name",
			"contact_1",
			"contact_2",
			"email",
			"company_address",
			"image_dropzone",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Asset_supplierDAO()
	{
		this("asset_supplier");		
	}
	
	public void setSearchColumn(Asset_supplierDTO asset_supplierDTO)
	{
		asset_supplierDTO.searchColumn = "";
		asset_supplierDTO.searchColumn += asset_supplierDTO.nameEn + " ";
		asset_supplierDTO.searchColumn += asset_supplierDTO.nameBn + " ";
		asset_supplierDTO.searchColumn += asset_supplierDTO.companyWebsite + " ";
		asset_supplierDTO.searchColumn += asset_supplierDTO.website + " ";
		asset_supplierDTO.searchColumn += asset_supplierDTO.contactName + " ";
		asset_supplierDTO.searchColumn += asset_supplierDTO.contact1 + " ";
		asset_supplierDTO.searchColumn += asset_supplierDTO.contact2 + " ";
		asset_supplierDTO.searchColumn += asset_supplierDTO.email + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Asset_supplierDTO asset_supplierDTO = (Asset_supplierDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(asset_supplierDTO);
		if(isInsert)
		{
			ps.setObject(index++,asset_supplierDTO.iD);
		}
		ps.setObject(index++,asset_supplierDTO.nameEn);
		ps.setObject(index++,asset_supplierDTO.nameBn);
		ps.setObject(index++,asset_supplierDTO.companyWebsite);
		ps.setObject(index++,asset_supplierDTO.website);
		ps.setObject(index++,asset_supplierDTO.contactName);
		ps.setObject(index++,asset_supplierDTO.contact1);
		ps.setObject(index++,asset_supplierDTO.contact2);
		ps.setObject(index++,asset_supplierDTO.email);
		ps.setObject(index++,asset_supplierDTO.companyAddress);
		ps.setObject(index++,asset_supplierDTO.imageDropzone);
		ps.setObject(index++,asset_supplierDTO.searchColumn);
		ps.setObject(index++,asset_supplierDTO.insertedByUserId);
		ps.setObject(index++,asset_supplierDTO.insertedByOrganogramId);
		ps.setObject(index++,asset_supplierDTO.insertionDate);
		ps.setObject(index++,asset_supplierDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			
		}
		ps.setObject(index++, lastModificationTime);
	}
	
    private long update(CommonDTO commonDTO,Connection connection) throws Exception{
        AtomicReference<Exception> atomicReference = new AtomicReference<>();
        long lastModificationTime = System.currentTimeMillis();
        StringBuilder sqlBuilder = new StringBuilder("UPDATE ").append(tableName).append(" SET ");
        for (String columnName : columnNames) {
            if (!columnName.equals("ID") && !columnName.equals("isDeleted")) {
                sqlBuilder.append(columnName).append(" = ?, ");
            }
        }
        sqlBuilder.append("lastModificationTime = ").append(lastModificationTime);
        sqlBuilder.append(" WHERE ID = ").append(commonDTO.iD);
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                doSet(ps, commonDTO, false);
                logger.debug(ps);
                ps.executeUpdate();
                recordUpdateTime(connection, lastModificationTime);
            } catch (Exception ex) {
                ex.printStackTrace();
                atomicReference.set(ex);
            }
        }, sqlBuilder.toString());
        if(atomicReference.get()!=null){
            throw atomicReference.get();
        }
        return commonDTO.iD;
    }
	
	public Asset_supplierDTO build(ResultSet rs)
	{
		try
		{
			Asset_supplierDTO asset_supplierDTO = new Asset_supplierDTO();
			asset_supplierDTO.iD = rs.getLong("ID");
			asset_supplierDTO.nameEn = rs.getString("name_en");
			asset_supplierDTO.nameBn = rs.getString("name_bn");
			asset_supplierDTO.companyWebsite = rs.getString("company_website");
			asset_supplierDTO.website = rs.getString("website");
			asset_supplierDTO.contactName = rs.getString("contact_name");
			asset_supplierDTO.contact1 = rs.getString("contact_1");
			asset_supplierDTO.contact2 = rs.getString("contact_2");
			asset_supplierDTO.email = rs.getString("email");
			asset_supplierDTO.companyAddress = rs.getString("company_address");
			asset_supplierDTO.imageDropzone = rs.getLong("image_dropzone");
			asset_supplierDTO.searchColumn = rs.getString("search_column");
			asset_supplierDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			asset_supplierDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			asset_supplierDTO.insertionDate = rs.getLong("insertion_date");
			asset_supplierDTO.lastModifierUser = rs.getString("last_modifier_user");
			asset_supplierDTO.isDeleted = rs.getInt("isDeleted");
			asset_supplierDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return asset_supplierDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

	public Asset_supplierDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Asset_supplierDTO asset_supplierDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return asset_supplierDTO;
	}

	
	public List<Asset_supplierDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<Asset_supplierDTO> getAllAsset_supplier (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Asset_supplierDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Asset_supplierDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name_en")
						|| str.equals("name_bn")
						|| str.equals("company_website")
						|| str.equals("website")
						|| str.equals("contact_name")
						|| str.equals("contact_1")
						|| str.equals("contact_2")
						|| str.equals("email")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".name_en like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("name_bn"))
					{
						AllFieldSql += "" + tableName + ".name_bn like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("company_website"))
					{
						AllFieldSql += "" + tableName + ".company_website like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("website"))
					{
						AllFieldSql += "" + tableName + ".website like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("contact_name"))
					{
						AllFieldSql += "" + tableName + ".contact_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("contact_1"))
					{
						AllFieldSql += "" + tableName + ".contact_1 like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("contact_2"))
					{
						AllFieldSql += "" + tableName + ".contact_2 like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("email"))
					{
						AllFieldSql += "" + tableName + ".email like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	