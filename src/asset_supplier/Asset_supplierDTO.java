package asset_supplier;
import java.util.*; 
import util.*; 


public class Asset_supplierDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
    public String companyWebsite = "";
    public String website = "";
    public String contactName = "";
    public String contact1 = "";
    public String contact2 = "";
    public String email = "";
    public String companyAddress = "";
	public long imageDropzone = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	
	
    @Override
	public String toString() {
            return "$Asset_supplierDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " companyWebsite = " + companyWebsite +
            " website = " + website +
            " contactName = " + contactName +
            " contact1 = " + contact1 +
            " contact2 = " + contact2 +
            " email = " + email +
            " companyAddress = " + companyAddress +
            " imageDropzone = " + imageDropzone +
            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}