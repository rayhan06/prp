package asset_supplier;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import files.*;



/**
 * Servlet implementation class Asset_supplierServlet
 */
@WebServlet("/Asset_supplierServlet")
@MultipartConfig
public class Asset_supplierServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Asset_supplierServlet.class);

    String tableName = "asset_supplier";

	Asset_supplierDAO asset_supplierDAO;
	CommonRequestHandler commonRequestHandler;
	FilesDAO filesDAO = new FilesDAO();
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Asset_supplierServlet() 
	{
        super();
    	try
    	{
			asset_supplierDAO = new Asset_supplierDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(asset_supplierDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_SUPPLIER_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_SUPPLIER_UPDATE))
				{
					getAsset_supplier(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("downloadDropzoneFile"))
			{
				commonRequestHandler.downloadDropzoneFile(request, response, filesDAO);					
			}
			else if(actionType.equals("DeleteFileFromDropZone"))
			{
				commonRequestHandler.deleteFileFromDropZone(request, response, filesDAO);
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_SUPPLIER_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchAsset_supplier(request, response, isPermanentTable, filter);
						}
						else
						{
							searchAsset_supplier(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchAsset_supplier(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_SUPPLIER_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_SUPPLIER_ADD))
				{
					System.out.println("going to  addAsset_supplier ");
					addAsset_supplier(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addAsset_supplier ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("UploadFilesFromDropZone"))
			{
				commonRequestHandler.UploadFilesFromDropZone(request, response, userDTO);
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_SUPPLIER_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addAsset_supplier ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_SUPPLIER_UPDATE))
				{					
					addAsset_supplier(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_SUPPLIER_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO, true);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_SUPPLIER_SEARCH))
				{
					searchAsset_supplier(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Asset_supplierDTO asset_supplierDTO = asset_supplierDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(asset_supplierDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addAsset_supplier(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addAsset_supplier");
			String path = getServletContext().getRealPath("/img2/");
			Asset_supplierDTO asset_supplierDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				asset_supplierDTO = new Asset_supplierDTO();
			}
			else
			{
				asset_supplierDTO = asset_supplierDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("nameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEn = " + Value);
			if(Value != null)
			{
				asset_supplierDTO.nameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameBn = " + Value);
			if(Value != null)
			{
				asset_supplierDTO.nameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("companyWebsite");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("companyWebsite = " + Value);
			if(Value != null)
			{
				asset_supplierDTO.companyWebsite = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("website");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("website = " + Value);
			if(Value != null)
			{
				asset_supplierDTO.website = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("contactName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("contactName = " + Value);
			if(Value != null)
			{
				asset_supplierDTO.contactName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("contact1");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("contact1 = " + Value);
			if(Value != null)
			{
				asset_supplierDTO.contact1 = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("contact2");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("contact2 = " + Value);
			if(Value != null)
			{
				asset_supplierDTO.contact2 = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("email");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("email = " + Value);
			if(Value != null)
			{
				asset_supplierDTO.email = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("companyAddress");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("companyAddress = " + Value);
			if(Value != null)
			{
				asset_supplierDTO.companyAddress = GeoLocationDAO2.getAddressToSave(Value);				
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("imageDropzone");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("imageDropzone = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				System.out.println("imageDropzone = " + Value);
				
				asset_supplierDTO.imageDropzone = Long.parseLong(Value);
				
				
				if(addFlag == false)
				{
					String imageDropzoneFilesToDelete = request.getParameter("imageDropzoneFilesToDelete");
					String[] deleteArray = imageDropzoneFilesToDelete.split(",");
					for(int i = 0; i < deleteArray.length; i ++)
					{
						System.out.println("going to delete " + deleteArray[i]);
						if(i>0)
						{
							filesDAO.delete(Long.parseLong(deleteArray[i]));
						}
					}
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				asset_supplierDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				asset_supplierDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				asset_supplierDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				asset_supplierDTO.insertionDate = c.getTimeInMillis();
			}			


			asset_supplierDTO.lastModifierUser = userDTO.userName;

			
			System.out.println("Done adding  addAsset_supplier dto = " + asset_supplierDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				asset_supplierDAO.setIsDeleted(asset_supplierDTO.iD, CommonDTO.OUTDATED);
				returnedID = asset_supplierDAO.add(asset_supplierDTO);
				asset_supplierDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = asset_supplierDAO.manageWriteOperations(asset_supplierDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = asset_supplierDAO.manageWriteOperations(asset_supplierDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			Asset_supplierRepository.getInstance().reload(false);
			PBNameRepository.getInstance().reload(false);
			
			
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getAsset_supplier(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Asset_supplierServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(asset_supplierDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void getAsset_supplier(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getAsset_supplier");
		Asset_supplierDTO asset_supplierDTO = null;
		try 
		{
			asset_supplierDTO = asset_supplierDAO.getDTOByID(id);
			request.setAttribute("ID", asset_supplierDTO.iD);
			request.setAttribute("asset_supplierDTO",asset_supplierDTO);
			request.setAttribute("asset_supplierDAO",asset_supplierDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "asset_supplier/asset_supplierInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "asset_supplier/asset_supplierSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "asset_supplier/asset_supplierEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "asset_supplier/asset_supplierEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getAsset_supplier(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getAsset_supplier(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchAsset_supplier(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchAsset_supplier 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_ASSET_SUPPLIER,
			request,
			asset_supplierDAO,
			SessionConstants.VIEW_ASSET_SUPPLIER,
			SessionConstants.SEARCH_ASSET_SUPPLIER,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("asset_supplierDAO",asset_supplierDAO);
        RequestDispatcher rd;
     
        if(hasAjax == false)
        {
        	System.out.println("Going to asset_supplier/asset_supplierSearch.jsp");
        	rd = request.getRequestDispatcher("asset_supplier/asset_supplierSearch.jsp");
        }
        else
        {
        	System.out.println("Going to asset_supplier/asset_supplierSearchForm.jsp");
        	rd = request.getRequestDispatcher("asset_supplier/asset_supplierSearchForm.jsp");
        }
        
		rd.forward(request, response);
	}
	
}

