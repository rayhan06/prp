package asset_supplier;
import java.util.*; 
import util.*;


public class Asset_supplierMAPS extends CommonMaps
{	
	public Asset_supplierMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("nameEn".toLowerCase(), "nameEn".toLowerCase());
		java_DTO_map.put("nameBn".toLowerCase(), "nameBn".toLowerCase());
		java_DTO_map.put("companyWebsite".toLowerCase(), "companyWebsite".toLowerCase());
		java_DTO_map.put("website".toLowerCase(), "website".toLowerCase());
		java_DTO_map.put("contactName".toLowerCase(), "contactName".toLowerCase());
		java_DTO_map.put("contact1".toLowerCase(), "contact1".toLowerCase());
		java_DTO_map.put("contact2".toLowerCase(), "contact2".toLowerCase());
		java_DTO_map.put("email".toLowerCase(), "email".toLowerCase());
		java_DTO_map.put("companyAddress".toLowerCase(), "companyAddress".toLowerCase());
		java_DTO_map.put("imageDropzone".toLowerCase(), "imageDropzone".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("lastModifierUser".toLowerCase(), "lastModifierUser".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("name_en".toLowerCase(), "nameEn".toLowerCase());
		java_SQL_map.put("name_bn".toLowerCase(), "nameBn".toLowerCase());
		java_SQL_map.put("company_website".toLowerCase(), "companyWebsite".toLowerCase());
		java_SQL_map.put("website".toLowerCase(), "website".toLowerCase());
		java_SQL_map.put("contact_name".toLowerCase(), "contactName".toLowerCase());
		java_SQL_map.put("contact_1".toLowerCase(), "contact1".toLowerCase());
		java_SQL_map.put("contact_2".toLowerCase(), "contact2".toLowerCase());
		java_SQL_map.put("email".toLowerCase(), "email".toLowerCase());
		java_SQL_map.put("company_address".toLowerCase(), "companyAddress".toLowerCase());
		java_SQL_map.put("image_dropzone".toLowerCase(), "imageDropzone".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("English Name".toLowerCase(), "nameEn".toLowerCase());
		java_Text_map.put("Bangla Name".toLowerCase(), "nameBn".toLowerCase());
		java_Text_map.put("Company Website".toLowerCase(), "companyWebsite".toLowerCase());
		java_Text_map.put("Website".toLowerCase(), "website".toLowerCase());
		java_Text_map.put("Contact Name".toLowerCase(), "contactName".toLowerCase());
		java_Text_map.put("Contact 1".toLowerCase(), "contact1".toLowerCase());
		java_Text_map.put("Contact 2".toLowerCase(), "contact2".toLowerCase());
		java_Text_map.put("Email".toLowerCase(), "email".toLowerCase());
		java_Text_map.put("Company Address".toLowerCase(), "companyAddress".toLowerCase());
		java_Text_map.put("Image Dropzone".toLowerCase(), "imageDropzone".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Last Modifier User".toLowerCase(), "lastModifierUser".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}