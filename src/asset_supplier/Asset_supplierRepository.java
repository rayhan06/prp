package asset_supplier;

import java.util.ArrayList;

import java.util.List;
import java.util.Map;

import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;



public class Asset_supplierRepository implements Repository {
	Asset_supplierDAO asset_supplierDAO = null;
	
	public void setDAO(Asset_supplierDAO asset_supplierDAO)
	{
		this.asset_supplierDAO = asset_supplierDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Asset_supplierRepository.class);
	Map<Long, Asset_supplierDTO>mapOfAsset_supplierDTOToiD;
//	Map<String, Set<Asset_supplierDTO> >mapOfAsset_supplierDTOTonameEn;
//	Map<String, Set<Asset_supplierDTO> >mapOfAsset_supplierDTOTonameBn;
//	Map<String, Set<Asset_supplierDTO> >mapOfAsset_supplierDTOTocompanyWebsite;
//	Map<String, Set<Asset_supplierDTO> >mapOfAsset_supplierDTOTowebsite;
//	Map<String, Set<Asset_supplierDTO> >mapOfAsset_supplierDTOTocontactName;
//	Map<String, Set<Asset_supplierDTO> >mapOfAsset_supplierDTOTocontact1;
//	Map<String, Set<Asset_supplierDTO> >mapOfAsset_supplierDTOTocontact2;
//	Map<String, Set<Asset_supplierDTO> >mapOfAsset_supplierDTOToemail;
//	Map<String, Set<Asset_supplierDTO> >mapOfAsset_supplierDTOTocompanyAddress;
//	Map<Long, Set<Asset_supplierDTO> >mapOfAsset_supplierDTOToimageDropzone;
//	Map<String, Set<Asset_supplierDTO> >mapOfAsset_supplierDTOTosearchColumn;
//	Map<Long, Set<Asset_supplierDTO> >mapOfAsset_supplierDTOToinsertedByUserId;
//	Map<Long, Set<Asset_supplierDTO> >mapOfAsset_supplierDTOToinsertedByOrganogramId;
//	Map<Long, Set<Asset_supplierDTO> >mapOfAsset_supplierDTOToinsertionDate;
//	Map<String, Set<Asset_supplierDTO> >mapOfAsset_supplierDTOTolastModifierUser;
//	Map<Long, Set<Asset_supplierDTO> >mapOfAsset_supplierDTOTolastModificationTime;


	static Asset_supplierRepository instance = null;  
	private Asset_supplierRepository(){
		mapOfAsset_supplierDTOToiD = new ConcurrentHashMap<>();
		setDAO(new Asset_supplierDAO());
//		mapOfAsset_supplierDTOTonameEn = new ConcurrentHashMap<>();
//		mapOfAsset_supplierDTOTonameBn = new ConcurrentHashMap<>();
//		mapOfAsset_supplierDTOTocompanyWebsite = new ConcurrentHashMap<>();
//		mapOfAsset_supplierDTOTowebsite = new ConcurrentHashMap<>();
//		mapOfAsset_supplierDTOTocontactName = new ConcurrentHashMap<>();
//		mapOfAsset_supplierDTOTocontact1 = new ConcurrentHashMap<>();
//		mapOfAsset_supplierDTOTocontact2 = new ConcurrentHashMap<>();
//		mapOfAsset_supplierDTOToemail = new ConcurrentHashMap<>();
//		mapOfAsset_supplierDTOTocompanyAddress = new ConcurrentHashMap<>();
//		mapOfAsset_supplierDTOToimageDropzone = new ConcurrentHashMap<>();
//		mapOfAsset_supplierDTOTosearchColumn = new ConcurrentHashMap<>();
//		mapOfAsset_supplierDTOToinsertedByUserId = new ConcurrentHashMap<>();
//		mapOfAsset_supplierDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
//		mapOfAsset_supplierDTOToinsertionDate = new ConcurrentHashMap<>();
//		mapOfAsset_supplierDTOTolastModifierUser = new ConcurrentHashMap<>();
//		mapOfAsset_supplierDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Asset_supplierRepository getInstance(){
		if (instance == null){
			instance = new Asset_supplierRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(asset_supplierDAO == null)
		{
			return;
		}
		try {
			List<Asset_supplierDTO> asset_supplierDTOs = asset_supplierDAO.getAllAsset_supplier(reloadAll);
			for(Asset_supplierDTO asset_supplierDTO : asset_supplierDTOs) {
				Asset_supplierDTO oldAsset_supplierDTO = getAsset_supplierDTOByID(asset_supplierDTO.iD);
				if( oldAsset_supplierDTO != null ) {
					mapOfAsset_supplierDTOToiD.remove(oldAsset_supplierDTO.iD);
				
//					if(mapOfAsset_supplierDTOTonameEn.containsKey(oldAsset_supplierDTO.nameEn)) {
//						mapOfAsset_supplierDTOTonameEn.get(oldAsset_supplierDTO.nameEn).remove(oldAsset_supplierDTO);
//					}
//					if(mapOfAsset_supplierDTOTonameEn.get(oldAsset_supplierDTO.nameEn).isEmpty()) {
//						mapOfAsset_supplierDTOTonameEn.remove(oldAsset_supplierDTO.nameEn);
//					}
//
//					if(mapOfAsset_supplierDTOTonameBn.containsKey(oldAsset_supplierDTO.nameBn)) {
//						mapOfAsset_supplierDTOTonameBn.get(oldAsset_supplierDTO.nameBn).remove(oldAsset_supplierDTO);
//					}
//					if(mapOfAsset_supplierDTOTonameBn.get(oldAsset_supplierDTO.nameBn).isEmpty()) {
//						mapOfAsset_supplierDTOTonameBn.remove(oldAsset_supplierDTO.nameBn);
//					}
//
//					if(mapOfAsset_supplierDTOTocompanyWebsite.containsKey(oldAsset_supplierDTO.companyWebsite)) {
//						mapOfAsset_supplierDTOTocompanyWebsite.get(oldAsset_supplierDTO.companyWebsite).remove(oldAsset_supplierDTO);
//					}
//					if(mapOfAsset_supplierDTOTocompanyWebsite.get(oldAsset_supplierDTO.companyWebsite).isEmpty()) {
//						mapOfAsset_supplierDTOTocompanyWebsite.remove(oldAsset_supplierDTO.companyWebsite);
//					}
//
//					if(mapOfAsset_supplierDTOTowebsite.containsKey(oldAsset_supplierDTO.website)) {
//						mapOfAsset_supplierDTOTowebsite.get(oldAsset_supplierDTO.website).remove(oldAsset_supplierDTO);
//					}
//					if(mapOfAsset_supplierDTOTowebsite.get(oldAsset_supplierDTO.website).isEmpty()) {
//						mapOfAsset_supplierDTOTowebsite.remove(oldAsset_supplierDTO.website);
//					}
//
//					if(mapOfAsset_supplierDTOTocontactName.containsKey(oldAsset_supplierDTO.contactName)) {
//						mapOfAsset_supplierDTOTocontactName.get(oldAsset_supplierDTO.contactName).remove(oldAsset_supplierDTO);
//					}
//					if(mapOfAsset_supplierDTOTocontactName.get(oldAsset_supplierDTO.contactName).isEmpty()) {
//						mapOfAsset_supplierDTOTocontactName.remove(oldAsset_supplierDTO.contactName);
//					}
//
//					if(mapOfAsset_supplierDTOTocontact1.containsKey(oldAsset_supplierDTO.contact1)) {
//						mapOfAsset_supplierDTOTocontact1.get(oldAsset_supplierDTO.contact1).remove(oldAsset_supplierDTO);
//					}
//					if(mapOfAsset_supplierDTOTocontact1.get(oldAsset_supplierDTO.contact1).isEmpty()) {
//						mapOfAsset_supplierDTOTocontact1.remove(oldAsset_supplierDTO.contact1);
//					}
//
//					if(mapOfAsset_supplierDTOTocontact2.containsKey(oldAsset_supplierDTO.contact2)) {
//						mapOfAsset_supplierDTOTocontact2.get(oldAsset_supplierDTO.contact2).remove(oldAsset_supplierDTO);
//					}
//					if(mapOfAsset_supplierDTOTocontact2.get(oldAsset_supplierDTO.contact2).isEmpty()) {
//						mapOfAsset_supplierDTOTocontact2.remove(oldAsset_supplierDTO.contact2);
//					}
//
//					if(mapOfAsset_supplierDTOToemail.containsKey(oldAsset_supplierDTO.email)) {
//						mapOfAsset_supplierDTOToemail.get(oldAsset_supplierDTO.email).remove(oldAsset_supplierDTO);
//					}
//					if(mapOfAsset_supplierDTOToemail.get(oldAsset_supplierDTO.email).isEmpty()) {
//						mapOfAsset_supplierDTOToemail.remove(oldAsset_supplierDTO.email);
//					}
//
//					if(mapOfAsset_supplierDTOTocompanyAddress.containsKey(oldAsset_supplierDTO.companyAddress)) {
//						mapOfAsset_supplierDTOTocompanyAddress.get(oldAsset_supplierDTO.companyAddress).remove(oldAsset_supplierDTO);
//					}
//					if(mapOfAsset_supplierDTOTocompanyAddress.get(oldAsset_supplierDTO.companyAddress).isEmpty()) {
//						mapOfAsset_supplierDTOTocompanyAddress.remove(oldAsset_supplierDTO.companyAddress);
//					}
//
//					if(mapOfAsset_supplierDTOToimageDropzone.containsKey(oldAsset_supplierDTO.imageDropzone)) {
//						mapOfAsset_supplierDTOToimageDropzone.get(oldAsset_supplierDTO.imageDropzone).remove(oldAsset_supplierDTO);
//					}
//					if(mapOfAsset_supplierDTOToimageDropzone.get(oldAsset_supplierDTO.imageDropzone).isEmpty()) {
//						mapOfAsset_supplierDTOToimageDropzone.remove(oldAsset_supplierDTO.imageDropzone);
//					}
//
//					if(mapOfAsset_supplierDTOTosearchColumn.containsKey(oldAsset_supplierDTO.searchColumn)) {
//						mapOfAsset_supplierDTOTosearchColumn.get(oldAsset_supplierDTO.searchColumn).remove(oldAsset_supplierDTO);
//					}
//					if(mapOfAsset_supplierDTOTosearchColumn.get(oldAsset_supplierDTO.searchColumn).isEmpty()) {
//						mapOfAsset_supplierDTOTosearchColumn.remove(oldAsset_supplierDTO.searchColumn);
//					}
//
//					if(mapOfAsset_supplierDTOToinsertedByUserId.containsKey(oldAsset_supplierDTO.insertedByUserId)) {
//						mapOfAsset_supplierDTOToinsertedByUserId.get(oldAsset_supplierDTO.insertedByUserId).remove(oldAsset_supplierDTO);
//					}
//					if(mapOfAsset_supplierDTOToinsertedByUserId.get(oldAsset_supplierDTO.insertedByUserId).isEmpty()) {
//						mapOfAsset_supplierDTOToinsertedByUserId.remove(oldAsset_supplierDTO.insertedByUserId);
//					}
//
//					if(mapOfAsset_supplierDTOToinsertedByOrganogramId.containsKey(oldAsset_supplierDTO.insertedByOrganogramId)) {
//						mapOfAsset_supplierDTOToinsertedByOrganogramId.get(oldAsset_supplierDTO.insertedByOrganogramId).remove(oldAsset_supplierDTO);
//					}
//					if(mapOfAsset_supplierDTOToinsertedByOrganogramId.get(oldAsset_supplierDTO.insertedByOrganogramId).isEmpty()) {
//						mapOfAsset_supplierDTOToinsertedByOrganogramId.remove(oldAsset_supplierDTO.insertedByOrganogramId);
//					}
//
//					if(mapOfAsset_supplierDTOToinsertionDate.containsKey(oldAsset_supplierDTO.insertionDate)) {
//						mapOfAsset_supplierDTOToinsertionDate.get(oldAsset_supplierDTO.insertionDate).remove(oldAsset_supplierDTO);
//					}
//					if(mapOfAsset_supplierDTOToinsertionDate.get(oldAsset_supplierDTO.insertionDate).isEmpty()) {
//						mapOfAsset_supplierDTOToinsertionDate.remove(oldAsset_supplierDTO.insertionDate);
//					}
//
//					if(mapOfAsset_supplierDTOTolastModifierUser.containsKey(oldAsset_supplierDTO.lastModifierUser)) {
//						mapOfAsset_supplierDTOTolastModifierUser.get(oldAsset_supplierDTO.lastModifierUser).remove(oldAsset_supplierDTO);
//					}
//					if(mapOfAsset_supplierDTOTolastModifierUser.get(oldAsset_supplierDTO.lastModifierUser).isEmpty()) {
//						mapOfAsset_supplierDTOTolastModifierUser.remove(oldAsset_supplierDTO.lastModifierUser);
//					}
//
//					if(mapOfAsset_supplierDTOTolastModificationTime.containsKey(oldAsset_supplierDTO.lastModificationTime)) {
//						mapOfAsset_supplierDTOTolastModificationTime.get(oldAsset_supplierDTO.lastModificationTime).remove(oldAsset_supplierDTO);
//					}
//					if(mapOfAsset_supplierDTOTolastModificationTime.get(oldAsset_supplierDTO.lastModificationTime).isEmpty()) {
//						mapOfAsset_supplierDTOTolastModificationTime.remove(oldAsset_supplierDTO.lastModificationTime);
//					}
					
					
				}
				if(asset_supplierDTO.isDeleted == 0) 
				{
					
					mapOfAsset_supplierDTOToiD.put(asset_supplierDTO.iD, asset_supplierDTO);
				
//					if( ! mapOfAsset_supplierDTOTonameEn.containsKey(asset_supplierDTO.nameEn)) {
//						mapOfAsset_supplierDTOTonameEn.put(asset_supplierDTO.nameEn, new HashSet<>());
//					}
//					mapOfAsset_supplierDTOTonameEn.get(asset_supplierDTO.nameEn).add(asset_supplierDTO);
//
//					if( ! mapOfAsset_supplierDTOTonameBn.containsKey(asset_supplierDTO.nameBn)) {
//						mapOfAsset_supplierDTOTonameBn.put(asset_supplierDTO.nameBn, new HashSet<>());
//					}
//					mapOfAsset_supplierDTOTonameBn.get(asset_supplierDTO.nameBn).add(asset_supplierDTO);
//
//					if( ! mapOfAsset_supplierDTOTocompanyWebsite.containsKey(asset_supplierDTO.companyWebsite)) {
//						mapOfAsset_supplierDTOTocompanyWebsite.put(asset_supplierDTO.companyWebsite, new HashSet<>());
//					}
//					mapOfAsset_supplierDTOTocompanyWebsite.get(asset_supplierDTO.companyWebsite).add(asset_supplierDTO);
//
//					if( ! mapOfAsset_supplierDTOTowebsite.containsKey(asset_supplierDTO.website)) {
//						mapOfAsset_supplierDTOTowebsite.put(asset_supplierDTO.website, new HashSet<>());
//					}
//					mapOfAsset_supplierDTOTowebsite.get(asset_supplierDTO.website).add(asset_supplierDTO);
//
//					if( ! mapOfAsset_supplierDTOTocontactName.containsKey(asset_supplierDTO.contactName)) {
//						mapOfAsset_supplierDTOTocontactName.put(asset_supplierDTO.contactName, new HashSet<>());
//					}
//					mapOfAsset_supplierDTOTocontactName.get(asset_supplierDTO.contactName).add(asset_supplierDTO);
//
//					if( ! mapOfAsset_supplierDTOTocontact1.containsKey(asset_supplierDTO.contact1)) {
//						mapOfAsset_supplierDTOTocontact1.put(asset_supplierDTO.contact1, new HashSet<>());
//					}
//					mapOfAsset_supplierDTOTocontact1.get(asset_supplierDTO.contact1).add(asset_supplierDTO);
//
//					if( ! mapOfAsset_supplierDTOTocontact2.containsKey(asset_supplierDTO.contact2)) {
//						mapOfAsset_supplierDTOTocontact2.put(asset_supplierDTO.contact2, new HashSet<>());
//					}
//					mapOfAsset_supplierDTOTocontact2.get(asset_supplierDTO.contact2).add(asset_supplierDTO);
//
//					if( ! mapOfAsset_supplierDTOToemail.containsKey(asset_supplierDTO.email)) {
//						mapOfAsset_supplierDTOToemail.put(asset_supplierDTO.email, new HashSet<>());
//					}
//					mapOfAsset_supplierDTOToemail.get(asset_supplierDTO.email).add(asset_supplierDTO);
//
//					if( ! mapOfAsset_supplierDTOTocompanyAddress.containsKey(asset_supplierDTO.companyAddress)) {
//						mapOfAsset_supplierDTOTocompanyAddress.put(asset_supplierDTO.companyAddress, new HashSet<>());
//					}
//					mapOfAsset_supplierDTOTocompanyAddress.get(asset_supplierDTO.companyAddress).add(asset_supplierDTO);
//
//					if( ! mapOfAsset_supplierDTOToimageDropzone.containsKey(asset_supplierDTO.imageDropzone)) {
//						mapOfAsset_supplierDTOToimageDropzone.put(asset_supplierDTO.imageDropzone, new HashSet<>());
//					}
//					mapOfAsset_supplierDTOToimageDropzone.get(asset_supplierDTO.imageDropzone).add(asset_supplierDTO);
//
//					if( ! mapOfAsset_supplierDTOTosearchColumn.containsKey(asset_supplierDTO.searchColumn)) {
//						mapOfAsset_supplierDTOTosearchColumn.put(asset_supplierDTO.searchColumn, new HashSet<>());
//					}
//					mapOfAsset_supplierDTOTosearchColumn.get(asset_supplierDTO.searchColumn).add(asset_supplierDTO);
//
//					if( ! mapOfAsset_supplierDTOToinsertedByUserId.containsKey(asset_supplierDTO.insertedByUserId)) {
//						mapOfAsset_supplierDTOToinsertedByUserId.put(asset_supplierDTO.insertedByUserId, new HashSet<>());
//					}
//					mapOfAsset_supplierDTOToinsertedByUserId.get(asset_supplierDTO.insertedByUserId).add(asset_supplierDTO);
//
//					if( ! mapOfAsset_supplierDTOToinsertedByOrganogramId.containsKey(asset_supplierDTO.insertedByOrganogramId)) {
//						mapOfAsset_supplierDTOToinsertedByOrganogramId.put(asset_supplierDTO.insertedByOrganogramId, new HashSet<>());
//					}
//					mapOfAsset_supplierDTOToinsertedByOrganogramId.get(asset_supplierDTO.insertedByOrganogramId).add(asset_supplierDTO);
//
//					if( ! mapOfAsset_supplierDTOToinsertionDate.containsKey(asset_supplierDTO.insertionDate)) {
//						mapOfAsset_supplierDTOToinsertionDate.put(asset_supplierDTO.insertionDate, new HashSet<>());
//					}
//					mapOfAsset_supplierDTOToinsertionDate.get(asset_supplierDTO.insertionDate).add(asset_supplierDTO);
//
//					if( ! mapOfAsset_supplierDTOTolastModifierUser.containsKey(asset_supplierDTO.lastModifierUser)) {
//						mapOfAsset_supplierDTOTolastModifierUser.put(asset_supplierDTO.lastModifierUser, new HashSet<>());
//					}
//					mapOfAsset_supplierDTOTolastModifierUser.get(asset_supplierDTO.lastModifierUser).add(asset_supplierDTO);
//
//					if( ! mapOfAsset_supplierDTOTolastModificationTime.containsKey(asset_supplierDTO.lastModificationTime)) {
//						mapOfAsset_supplierDTOTolastModificationTime.put(asset_supplierDTO.lastModificationTime, new HashSet<>());
//					}
//					mapOfAsset_supplierDTOTolastModificationTime.get(asset_supplierDTO.lastModificationTime).add(asset_supplierDTO);
//
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Asset_supplierDTO> getAsset_supplierList() {
		List <Asset_supplierDTO> asset_suppliers = new ArrayList<Asset_supplierDTO>(this.mapOfAsset_supplierDTOToiD.values());
		return asset_suppliers;
	}
	
	
	public Asset_supplierDTO getAsset_supplierDTOByID( long ID){
		return mapOfAsset_supplierDTOToiD.get(ID);
	}
	
	
//	public List<Asset_supplierDTO> getAsset_supplierDTOByname_en(String name_en) {
//		return new ArrayList<>( mapOfAsset_supplierDTOTonameEn.getOrDefault(name_en,new HashSet<>()));
//	}
//
//
//	public List<Asset_supplierDTO> getAsset_supplierDTOByname_bn(String name_bn) {
//		return new ArrayList<>( mapOfAsset_supplierDTOTonameBn.getOrDefault(name_bn,new HashSet<>()));
//	}
//
//
//	public List<Asset_supplierDTO> getAsset_supplierDTOBycompany_website(String company_website) {
//		return new ArrayList<>( mapOfAsset_supplierDTOTocompanyWebsite.getOrDefault(company_website,new HashSet<>()));
//	}
//
//
//	public List<Asset_supplierDTO> getAsset_supplierDTOBywebsite(String website) {
//		return new ArrayList<>( mapOfAsset_supplierDTOTowebsite.getOrDefault(website,new HashSet<>()));
//	}
//
//
//	public List<Asset_supplierDTO> getAsset_supplierDTOBycontact_name(String contact_name) {
//		return new ArrayList<>( mapOfAsset_supplierDTOTocontactName.getOrDefault(contact_name,new HashSet<>()));
//	}
//
//
//	public List<Asset_supplierDTO> getAsset_supplierDTOBycontact_1(String contact_1) {
//		return new ArrayList<>( mapOfAsset_supplierDTOTocontact1.getOrDefault(contact_1,new HashSet<>()));
//	}
//
//
//	public List<Asset_supplierDTO> getAsset_supplierDTOBycontact_2(String contact_2) {
//		return new ArrayList<>( mapOfAsset_supplierDTOTocontact2.getOrDefault(contact_2,new HashSet<>()));
//	}
//
//
//	public List<Asset_supplierDTO> getAsset_supplierDTOByemail(String email) {
//		return new ArrayList<>( mapOfAsset_supplierDTOToemail.getOrDefault(email,new HashSet<>()));
//	}
//
//
//	public List<Asset_supplierDTO> getAsset_supplierDTOBycompany_address(String company_address) {
//		return new ArrayList<>( mapOfAsset_supplierDTOTocompanyAddress.getOrDefault(company_address,new HashSet<>()));
//	}
//
//
//	public List<Asset_supplierDTO> getAsset_supplierDTOByimage_dropzone(long image_dropzone) {
//		return new ArrayList<>( mapOfAsset_supplierDTOToimageDropzone.getOrDefault(image_dropzone,new HashSet<>()));
//	}
//
//
//	public List<Asset_supplierDTO> getAsset_supplierDTOBysearch_column(String search_column) {
//		return new ArrayList<>( mapOfAsset_supplierDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
//	}
//
//
//	public List<Asset_supplierDTO> getAsset_supplierDTOByinserted_by_user_id(long inserted_by_user_id) {
//		return new ArrayList<>( mapOfAsset_supplierDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
//	}
//
//
//	public List<Asset_supplierDTO> getAsset_supplierDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
//		return new ArrayList<>( mapOfAsset_supplierDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
//	}
//
//
//	public List<Asset_supplierDTO> getAsset_supplierDTOByinsertion_date(long insertion_date) {
//		return new ArrayList<>( mapOfAsset_supplierDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
//	}
//
//
//	public List<Asset_supplierDTO> getAsset_supplierDTOBylast_modifier_user(String last_modifier_user) {
//		return new ArrayList<>( mapOfAsset_supplierDTOTolastModifierUser.getOrDefault(last_modifier_user,new HashSet<>()));
//	}
//
//
//	public List<Asset_supplierDTO> getAsset_supplierDTOBylastModificationTime(long lastModificationTime) {
//		return new ArrayList<>( mapOfAsset_supplierDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
//	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "asset_supplier";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}

	public String getBuildOptions(String Language, String selectedId ){
		List<OptionDTO> optionDTOList = getAsset_supplierList()
				.stream()
				.map(e->new OptionDTO(e.nameEn,e.nameBn,String.valueOf(e.iD)))
				.collect(Collectors.toList());

		return Utils.buildOptionsWithoutSelectWithSelectId(optionDTOList, Language, selectedId );

	}

	public String getText(String language, long id) {
		Asset_supplierDTO dto = this.mapOfAsset_supplierDTOToiD.get(id);
		return dto == null ? ""
				: (language.equalsIgnoreCase("English")) ? (dto.nameEn == null ? "" : dto.nameEn)
				: (dto.nameBn == null ? "" : dto.nameBn);
	}
}


