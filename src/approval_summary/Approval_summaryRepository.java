package approval_summary;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Approval_summaryRepository implements Repository {
	Approval_summaryDAO approval_summaryDAO = null;
	
	public void setDAO(Approval_summaryDAO approval_summaryDAO)
	{
		this.approval_summaryDAO = approval_summaryDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Approval_summaryRepository.class);
	Map<Long, Approval_summaryDTO>mapOfApproval_summaryDTOToiD;
	Map<String, Set<Approval_summaryDTO> >mapOfApproval_summaryDTOTotableName;
	Map<Long, Set<Approval_summaryDTO> >mapOfApproval_summaryDTOTotableId;
	Map<Long, Set<Approval_summaryDTO> >mapOfApproval_summaryDTOToinitiator;
	Map<Integer, Set<Approval_summaryDTO> >mapOfApproval_summaryDTOToapprovalStatusCat;
	Map<Long, Set<Approval_summaryDTO> >mapOfApproval_summaryDTOTodateOfInitiation;
	Map<Long, Set<Approval_summaryDTO> >mapOfApproval_summaryDTOTodateOfResolution;
	Map<Long, Set<Approval_summaryDTO> >mapOfApproval_summaryDTOToresolver;
	Map<Long, Set<Approval_summaryDTO> >mapOfApproval_summaryDTOToassignedTo;
	Map<Long, Set<Approval_summaryDTO> >mapOfApproval_summaryDTOTolastModificationTime;


	static Approval_summaryRepository instance = null;  
	private Approval_summaryRepository(){
		mapOfApproval_summaryDTOToiD = new ConcurrentHashMap<>();
		mapOfApproval_summaryDTOTotableName = new ConcurrentHashMap<>();
		mapOfApproval_summaryDTOTotableId = new ConcurrentHashMap<>();
		mapOfApproval_summaryDTOToinitiator = new ConcurrentHashMap<>();
		mapOfApproval_summaryDTOToapprovalStatusCat = new ConcurrentHashMap<>();
		mapOfApproval_summaryDTOTodateOfInitiation = new ConcurrentHashMap<>();
		mapOfApproval_summaryDTOTodateOfResolution = new ConcurrentHashMap<>();
		mapOfApproval_summaryDTOToresolver = new ConcurrentHashMap<>();
		mapOfApproval_summaryDTOToassignedTo = new ConcurrentHashMap<>();
		mapOfApproval_summaryDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Approval_summaryRepository getInstance(){
		if (instance == null){
			instance = new Approval_summaryRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(approval_summaryDAO == null)
		{
			return;
		}
		try {
			List<Approval_summaryDTO> approval_summaryDTOs = approval_summaryDAO.getAllApproval_summary(reloadAll);
			for(Approval_summaryDTO approval_summaryDTO : approval_summaryDTOs) {
				Approval_summaryDTO oldApproval_summaryDTO = getApproval_summaryDTOByID(approval_summaryDTO.iD);
				if( oldApproval_summaryDTO != null ) {
					mapOfApproval_summaryDTOToiD.remove(oldApproval_summaryDTO.iD);
				
					if(mapOfApproval_summaryDTOTotableName.containsKey(oldApproval_summaryDTO.tableName)) {
						mapOfApproval_summaryDTOTotableName.get(oldApproval_summaryDTO.tableName).remove(oldApproval_summaryDTO);
					}
					if(mapOfApproval_summaryDTOTotableName.get(oldApproval_summaryDTO.tableName).isEmpty()) {
						mapOfApproval_summaryDTOTotableName.remove(oldApproval_summaryDTO.tableName);
					}
					
					if(mapOfApproval_summaryDTOTotableId.containsKey(oldApproval_summaryDTO.tableId)) {
						mapOfApproval_summaryDTOTotableId.get(oldApproval_summaryDTO.tableId).remove(oldApproval_summaryDTO);
					}
					if(mapOfApproval_summaryDTOTotableId.get(oldApproval_summaryDTO.tableId).isEmpty()) {
						mapOfApproval_summaryDTOTotableId.remove(oldApproval_summaryDTO.tableId);
					}
					
					if(mapOfApproval_summaryDTOToinitiator.containsKey(oldApproval_summaryDTO.initiator)) {
						mapOfApproval_summaryDTOToinitiator.get(oldApproval_summaryDTO.initiator).remove(oldApproval_summaryDTO);
					}
					if(mapOfApproval_summaryDTOToinitiator.get(oldApproval_summaryDTO.initiator).isEmpty()) {
						mapOfApproval_summaryDTOToinitiator.remove(oldApproval_summaryDTO.initiator);
					}
					
					if(mapOfApproval_summaryDTOToapprovalStatusCat.containsKey(oldApproval_summaryDTO.approvalStatusCat)) {
						mapOfApproval_summaryDTOToapprovalStatusCat.get(oldApproval_summaryDTO.approvalStatusCat).remove(oldApproval_summaryDTO);
					}
					if(mapOfApproval_summaryDTOToapprovalStatusCat.get(oldApproval_summaryDTO.approvalStatusCat).isEmpty()) {
						mapOfApproval_summaryDTOToapprovalStatusCat.remove(oldApproval_summaryDTO.approvalStatusCat);
					}
					
					if(mapOfApproval_summaryDTOTodateOfInitiation.containsKey(oldApproval_summaryDTO.dateOfInitiation)) {
						mapOfApproval_summaryDTOTodateOfInitiation.get(oldApproval_summaryDTO.dateOfInitiation).remove(oldApproval_summaryDTO);
					}
					if(mapOfApproval_summaryDTOTodateOfInitiation.get(oldApproval_summaryDTO.dateOfInitiation).isEmpty()) {
						mapOfApproval_summaryDTOTodateOfInitiation.remove(oldApproval_summaryDTO.dateOfInitiation);
					}
					
					if(mapOfApproval_summaryDTOTodateOfResolution.containsKey(oldApproval_summaryDTO.dateOfResolution)) {
						mapOfApproval_summaryDTOTodateOfResolution.get(oldApproval_summaryDTO.dateOfResolution).remove(oldApproval_summaryDTO);
					}
					if(mapOfApproval_summaryDTOTodateOfResolution.get(oldApproval_summaryDTO.dateOfResolution).isEmpty()) {
						mapOfApproval_summaryDTOTodateOfResolution.remove(oldApproval_summaryDTO.dateOfResolution);
					}
					
					if(mapOfApproval_summaryDTOToresolver.containsKey(oldApproval_summaryDTO.resolver)) {
						mapOfApproval_summaryDTOToresolver.get(oldApproval_summaryDTO.resolver).remove(oldApproval_summaryDTO);
					}
					if(mapOfApproval_summaryDTOToresolver.get(oldApproval_summaryDTO.resolver).isEmpty()) {
						mapOfApproval_summaryDTOToresolver.remove(oldApproval_summaryDTO.resolver);
					}
					
					if(mapOfApproval_summaryDTOToassignedTo.containsKey(oldApproval_summaryDTO.assignedTo)) {
						mapOfApproval_summaryDTOToassignedTo.get(oldApproval_summaryDTO.assignedTo).remove(oldApproval_summaryDTO);
					}
					if(mapOfApproval_summaryDTOToassignedTo.get(oldApproval_summaryDTO.assignedTo).isEmpty()) {
						mapOfApproval_summaryDTOToassignedTo.remove(oldApproval_summaryDTO.assignedTo);
					}
					
					if(mapOfApproval_summaryDTOTolastModificationTime.containsKey(oldApproval_summaryDTO.lastModificationTime)) {
						mapOfApproval_summaryDTOTolastModificationTime.get(oldApproval_summaryDTO.lastModificationTime).remove(oldApproval_summaryDTO);
					}
					if(mapOfApproval_summaryDTOTolastModificationTime.get(oldApproval_summaryDTO.lastModificationTime).isEmpty()) {
						mapOfApproval_summaryDTOTolastModificationTime.remove(oldApproval_summaryDTO.lastModificationTime);
					}
					
					
				}
				if(approval_summaryDTO.isDeleted == 0) 
				{
					
					mapOfApproval_summaryDTOToiD.put(approval_summaryDTO.iD, approval_summaryDTO);
				
					if( ! mapOfApproval_summaryDTOTotableName.containsKey(approval_summaryDTO.tableName)) {
						mapOfApproval_summaryDTOTotableName.put(approval_summaryDTO.tableName, new HashSet<>());
					}
					mapOfApproval_summaryDTOTotableName.get(approval_summaryDTO.tableName).add(approval_summaryDTO);
					
					if( ! mapOfApproval_summaryDTOTotableId.containsKey(approval_summaryDTO.tableId)) {
						mapOfApproval_summaryDTOTotableId.put(approval_summaryDTO.tableId, new HashSet<>());
					}
					mapOfApproval_summaryDTOTotableId.get(approval_summaryDTO.tableId).add(approval_summaryDTO);
					
					if( ! mapOfApproval_summaryDTOToinitiator.containsKey(approval_summaryDTO.initiator)) {
						mapOfApproval_summaryDTOToinitiator.put(approval_summaryDTO.initiator, new HashSet<>());
					}
					mapOfApproval_summaryDTOToinitiator.get(approval_summaryDTO.initiator).add(approval_summaryDTO);
					
					if( ! mapOfApproval_summaryDTOToapprovalStatusCat.containsKey(approval_summaryDTO.approvalStatusCat)) {
						mapOfApproval_summaryDTOToapprovalStatusCat.put(approval_summaryDTO.approvalStatusCat, new HashSet<>());
					}
					mapOfApproval_summaryDTOToapprovalStatusCat.get(approval_summaryDTO.approvalStatusCat).add(approval_summaryDTO);
					
					if( ! mapOfApproval_summaryDTOTodateOfInitiation.containsKey(approval_summaryDTO.dateOfInitiation)) {
						mapOfApproval_summaryDTOTodateOfInitiation.put(approval_summaryDTO.dateOfInitiation, new HashSet<>());
					}
					mapOfApproval_summaryDTOTodateOfInitiation.get(approval_summaryDTO.dateOfInitiation).add(approval_summaryDTO);
					
					if( ! mapOfApproval_summaryDTOTodateOfResolution.containsKey(approval_summaryDTO.dateOfResolution)) {
						mapOfApproval_summaryDTOTodateOfResolution.put(approval_summaryDTO.dateOfResolution, new HashSet<>());
					}
					mapOfApproval_summaryDTOTodateOfResolution.get(approval_summaryDTO.dateOfResolution).add(approval_summaryDTO);
					
					if( ! mapOfApproval_summaryDTOToresolver.containsKey(approval_summaryDTO.resolver)) {
						mapOfApproval_summaryDTOToresolver.put(approval_summaryDTO.resolver, new HashSet<>());
					}
					mapOfApproval_summaryDTOToresolver.get(approval_summaryDTO.resolver).add(approval_summaryDTO);
					
					if( ! mapOfApproval_summaryDTOToassignedTo.containsKey(approval_summaryDTO.assignedTo)) {
						mapOfApproval_summaryDTOToassignedTo.put(approval_summaryDTO.assignedTo, new HashSet<>());
					}
					mapOfApproval_summaryDTOToassignedTo.get(approval_summaryDTO.assignedTo).add(approval_summaryDTO);
					
					if( ! mapOfApproval_summaryDTOTolastModificationTime.containsKey(approval_summaryDTO.lastModificationTime)) {
						mapOfApproval_summaryDTOTolastModificationTime.put(approval_summaryDTO.lastModificationTime, new HashSet<>());
					}
					mapOfApproval_summaryDTOTolastModificationTime.get(approval_summaryDTO.lastModificationTime).add(approval_summaryDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Approval_summaryDTO> getApproval_summaryList() {
		List <Approval_summaryDTO> approval_summarys = new ArrayList<Approval_summaryDTO>(this.mapOfApproval_summaryDTOToiD.values());
		return approval_summarys;
	}
	
	
	public Approval_summaryDTO getApproval_summaryDTOByID( long ID){
		return mapOfApproval_summaryDTOToiD.get(ID);
	}
	
	
	public List<Approval_summaryDTO> getApproval_summaryDTOBytable_name(String table_name) {
		return new ArrayList<>( mapOfApproval_summaryDTOTotableName.getOrDefault(table_name,new HashSet<>()));
	}
	
	
	public List<Approval_summaryDTO> getApproval_summaryDTOBytable_id(long table_id) {
		return new ArrayList<>( mapOfApproval_summaryDTOTotableId.getOrDefault(table_id,new HashSet<>()));
	}
	
	
	public List<Approval_summaryDTO> getApproval_summaryDTOByinitiator(long initiator) {
		return new ArrayList<>( mapOfApproval_summaryDTOToinitiator.getOrDefault(initiator,new HashSet<>()));
	}
	
	
	public List<Approval_summaryDTO> getApproval_summaryDTOByapproval_status_cat(int approval_status_cat) {
		return new ArrayList<>( mapOfApproval_summaryDTOToapprovalStatusCat.getOrDefault(approval_status_cat,new HashSet<>()));
	}
	
	
	public List<Approval_summaryDTO> getApproval_summaryDTOBydate_of_initiation(long date_of_initiation) {
		return new ArrayList<>( mapOfApproval_summaryDTOTodateOfInitiation.getOrDefault(date_of_initiation,new HashSet<>()));
	}
	
	
	public List<Approval_summaryDTO> getApproval_summaryDTOBydate_of_resolution(long date_of_resolution) {
		return new ArrayList<>( mapOfApproval_summaryDTOTodateOfResolution.getOrDefault(date_of_resolution,new HashSet<>()));
	}
	
	
	public List<Approval_summaryDTO> getApproval_summaryDTOByresolver(long resolver) {
		return new ArrayList<>( mapOfApproval_summaryDTOToresolver.getOrDefault(resolver,new HashSet<>()));
	}
	
	
	public List<Approval_summaryDTO> getApproval_summaryDTOByassigned_to(long assigned_to) {
		return new ArrayList<>( mapOfApproval_summaryDTOToassignedTo.getOrDefault(assigned_to,new HashSet<>()));
	}
	
	
	public List<Approval_summaryDTO> getApproval_summaryDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfApproval_summaryDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "approval_summary";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


