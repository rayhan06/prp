package approval_summary;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Approval_summaryDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	
	public Approval_summaryDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";

		joinSQL += " join category on (";
		joinSQL += " (" + tableName + ".approval_status_cat = category.value  and category.domain_name = 'approval_status')";
		joinSQL += " )";
		joinSQL += " join language_text on category.language_id = language_text.id";		
		commonMaps = new Approval_summaryMAPS(tableName);
	}
	
	public Approval_summaryDAO()
	{
		this("approval_summary");		
	}
	
	
	
	public long add(CommonDTO commonDTO) throws Exception
	{
		
		Approval_summaryDTO approval_summaryDTO = (Approval_summaryDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			approval_summaryDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "table_name";
			sql += ", ";
			sql += "table_id";
			sql += ", ";
			sql += "initiator";
			sql += ", ";
			sql += "approval_status_cat";
			sql += ", ";
			sql += "date_of_initiation";
			sql += ", ";
			sql += "date_of_resolution";
			sql += ", ";
			sql += "resolver";
			sql += ", ";
			sql += "assigned_to";
			sql += ", ";
			sql += "originally_assigned_to";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
		
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
			sql += ")";
				

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,approval_summaryDTO.iD);
			ps.setObject(index++,approval_summaryDTO.tableName);
			ps.setObject(index++,approval_summaryDTO.tableId);
			ps.setObject(index++,approval_summaryDTO.initiator);
			ps.setObject(index++,approval_summaryDTO.approvalStatusCat);
			ps.setObject(index++,approval_summaryDTO.dateOfInitiation);
			ps.setObject(index++,approval_summaryDTO.dateOfResolution);
			ps.setObject(index++,approval_summaryDTO.resolver);
			ps.setObject(index++,approval_summaryDTO.assignedTo);
			ps.setObject(index++,approval_summaryDTO.assignedTo); //in originally_assigned_to
			ps.setObject(index++,approval_summaryDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection,lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return approval_summaryDTO.iD;		
	}
	
	public void replaceForLeaveStarting (long organogramToReplace, long replaceWith)
	{
		Connection connection = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		
		long lastModificationTime = System.currentTimeMillis();	
		try{
			String sql =  "update " + tableName + " set assigned_to = " + replaceWith + " where assigned_to = " + organogramToReplace ;
			
			
			
			printSql(sql);

			connection = DBMW.getInstance().getConnection();
			stmt  = connection.createStatement();
			stmt.execute(sql);
			

			
			recordUpdateTime(connection, lastModificationTime);

			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMW.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
	}
	
	public void replaceForLeaveEnding (long permanentOrganogramId)
	{
		Connection connection = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		
		long lastModificationTime = System.currentTimeMillis();	
		try{
			String sql =  "update " + tableName + " set assigned_to = " + permanentOrganogramId + " where originally_assigned_to = " + permanentOrganogramId ;
			
			
			
			printSql(sql);

			connection = DBMW.getInstance().getConnection();
			stmt  = connection.createStatement();
			stmt.execute(sql);
			

			
			recordUpdateTime(connection,lastModificationTime);

			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMW.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Approval_summaryDTO approval_summaryDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				approval_summaryDTO = new Approval_summaryDTO();

				approval_summaryDTO.iD = rs.getLong("ID");
				approval_summaryDTO.tableName = rs.getString("table_name");
				approval_summaryDTO.tableId = rs.getLong("table_id");
				approval_summaryDTO.initiator = rs.getLong("initiator");
				approval_summaryDTO.approvalStatusCat = rs.getInt("approval_status_cat");
				approval_summaryDTO.dateOfInitiation = rs.getLong("date_of_initiation");
				approval_summaryDTO.dateOfResolution = rs.getLong("date_of_resolution");
				approval_summaryDTO.resolver = rs.getLong("resolver");
				approval_summaryDTO.assignedTo = rs.getLong("assigned_to");
				approval_summaryDTO.isDeleted = rs.getInt("isDeleted");
				approval_summaryDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return approval_summaryDTO;
	}
	
	public Approval_summaryDTO getDTOByTableNameAndTableID (String tableNameToget, long tableID)
	{
		if(!tableNameToget.matches("^[a-zA-Z0-9_]*$"))
		{
			return null;
		}
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Approval_summaryDTO approval_summaryDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE table_id =" + tableID + " and table_name = '" + tableNameToget + "'";
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				approval_summaryDTO = new Approval_summaryDTO();

				approval_summaryDTO.iD = rs.getLong("ID");
				approval_summaryDTO.tableName = rs.getString("table_name");
				approval_summaryDTO.tableId = rs.getLong("table_id");
				approval_summaryDTO.initiator = rs.getLong("initiator");
				approval_summaryDTO.approvalStatusCat = rs.getInt("approval_status_cat");
				approval_summaryDTO.dateOfInitiation = rs.getLong("date_of_initiation");
				approval_summaryDTO.dateOfResolution = rs.getLong("date_of_resolution");
				approval_summaryDTO.resolver = rs.getLong("resolver");
				approval_summaryDTO.assignedTo = rs.getLong("assigned_to");
				approval_summaryDTO.isDeleted = rs.getInt("isDeleted");
				approval_summaryDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return approval_summaryDTO;
	}
	
	public long update(CommonDTO commonDTO) throws Exception
	{		
		Approval_summaryDTO approval_summaryDTO = (Approval_summaryDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "table_name=?";
			sql += ", ";
			sql += "table_id=?";
			sql += ", ";
			sql += "initiator=?";
			sql += ", ";
			sql += "approval_status_cat=?";
			sql += ", ";
			sql += "date_of_initiation=?";
			sql += ", ";
			sql += "date_of_resolution=?";
			sql += ", ";
			sql += "resolver=?";
			sql += ", ";
			sql += "assigned_to=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + approval_summaryDTO.iD;
				

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,approval_summaryDTO.tableName);
			ps.setObject(index++,approval_summaryDTO.tableId);
			ps.setObject(index++,approval_summaryDTO.initiator);
			ps.setObject(index++,approval_summaryDTO.approvalStatusCat);
			ps.setObject(index++,approval_summaryDTO.dateOfInitiation);
			ps.setObject(index++,approval_summaryDTO.dateOfResolution);
			ps.setObject(index++,approval_summaryDTO.resolver);
			ps.setObject(index++,approval_summaryDTO.assignedTo);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection,lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return approval_summaryDTO.iD;
	}
	
	
	public List<Approval_summaryDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Approval_summaryDTO approval_summaryDTO = null;
		List<Approval_summaryDTO> approval_summaryDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return approval_summaryDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				approval_summaryDTO = new Approval_summaryDTO();
				approval_summaryDTO.iD = rs.getLong("ID");
				approval_summaryDTO.tableName = rs.getString("table_name");
				approval_summaryDTO.tableId = rs.getLong("table_id");
				approval_summaryDTO.initiator = rs.getLong("initiator");
				approval_summaryDTO.approvalStatusCat = rs.getInt("approval_status_cat");
				approval_summaryDTO.dateOfInitiation = rs.getLong("date_of_initiation");
				approval_summaryDTO.dateOfResolution = rs.getLong("date_of_resolution");
				approval_summaryDTO.resolver = rs.getLong("resolver");
				approval_summaryDTO.assignedTo = rs.getLong("assigned_to");
				approval_summaryDTO.isDeleted = rs.getInt("isDeleted");
				approval_summaryDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + approval_summaryDTO);
				
				approval_summaryDTOList.add(approval_summaryDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return approval_summaryDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Approval_summaryDTO> getAllApproval_summary (boolean isFirstReload)
    {
		List<Approval_summaryDTO> approval_summaryDTOList = new ArrayList<>();

		String sql = "SELECT * FROM approval_summary";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by approval_summary.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Approval_summaryDTO approval_summaryDTO = new Approval_summaryDTO();
				approval_summaryDTO.iD = rs.getLong("ID");
				approval_summaryDTO.tableName = rs.getString("table_name");
				approval_summaryDTO.tableId = rs.getLong("table_id");
				approval_summaryDTO.initiator = rs.getLong("initiator");
				approval_summaryDTO.approvalStatusCat = rs.getInt("approval_status_cat");
				approval_summaryDTO.dateOfInitiation = rs.getLong("date_of_initiation");
				approval_summaryDTO.dateOfResolution = rs.getLong("date_of_resolution");
				approval_summaryDTO.resolver = rs.getLong("resolver");
				approval_summaryDTO.assignedTo = rs.getLong("assigned_to");
				approval_summaryDTO.isDeleted = rs.getInt("isDeleted");
				approval_summaryDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				approval_summaryDTOList.add(approval_summaryDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return approval_summaryDTOList;
    }

	
	public List<Approval_summaryDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Approval_summaryDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Approval_summaryDTO> approval_summaryDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Approval_summaryDTO approval_summaryDTO = new Approval_summaryDTO();
				approval_summaryDTO.iD = rs.getLong("ID");
				approval_summaryDTO.tableName = rs.getString("table_name");
				approval_summaryDTO.tableId = rs.getLong("table_id");
				approval_summaryDTO.initiator = rs.getLong("initiator");
				approval_summaryDTO.approvalStatusCat = rs.getInt("approval_status_cat");
				approval_summaryDTO.dateOfInitiation = rs.getLong("date_of_initiation");
				approval_summaryDTO.dateOfResolution = rs.getLong("date_of_resolution");
				approval_summaryDTO.resolver = rs.getLong("resolver");
				approval_summaryDTO.assignedTo = rs.getLong("assigned_to");
				approval_summaryDTO.isDeleted = rs.getInt("isDeleted");
				approval_summaryDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				approval_summaryDTOList.add(approval_summaryDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return approval_summaryDTOList;
	
	}
				
}
	