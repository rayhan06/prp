package approval_summary;

import java.io.IOException;
import java.io.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;

import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;

import approval_summary.Constants;




import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;


/**
 * Servlet implementation class Approval_summaryServlet
 */
@WebServlet("/Approval_summaryServlet")
@MultipartConfig
public class Approval_summaryServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Approval_summaryServlet.class);

    String tableName = "approval_summary";

	Approval_summaryDAO approval_summaryDAO;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Approval_summaryServlet() 
	{
        super();
    	try
    	{
			approval_summaryDAO = new Approval_summaryDAO(tableName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_SUMMARY_ADD))
				{
					getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_SUMMARY_UPDATE))
				{
					getApproval_summary(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_SUMMARY_SEARCH))
				{
					
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							if(filter.equalsIgnoreCase("getPending"))
							{
								filter = "approval_status_cat=2 and table_name!='approval_test'";
							}
							else if(filter.equalsIgnoreCase("getCompleted"))
							{
								filter = "approval_status_cat!=2 and table_name!='approval_test'";
							}
							else if(filter.equalsIgnoreCase("getAll"))
							{
								filter = "table_name!='approval_test'";
							}							
							else
							{
								filter = "";
							}
							searchApproval_summary(request, response, isPermanentTable, filter);
						}
						else
						{
							searchApproval_summary(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchApproval_summary(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		request.setAttribute("ID", -1L);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("approval_summary/approval_summaryEdit.jsp");
		requestDispatcher.forward(request, response);
	}
	private String getFileName(final Part part) 
	{
	    final String partHeader = part.getHeader("content-disposition");
	    System.out.println("Part Header = {0}" +  partHeader);
	    for (String content : part.getHeader("content-disposition").split(";")) {
	        if (content.trim().startsWith("filename")) {
	            return content.substring(
	                    content.indexOf('=') + 1).trim().replace("\"", "");
	        }
	    }
	    return null;
	}

	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_SUMMARY_ADD))
				{
					System.out.println("going to  addApproval_summary ");
					addApproval_summary(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addApproval_summary ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_SUMMARY_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addApproval_summary ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_SUMMARY_UPDATE))
				{					
					addApproval_summary(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{								
				deleteApproval_summary(request, response, userDTO);				
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_SUMMARY_SEARCH))
				{
					searchApproval_summary(request, response, true,"");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Approval_summaryDTO approval_summaryDTO = (Approval_summaryDTO)approval_summaryDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(approval_summaryDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	private void approveApproval_summary(HttpServletRequest request, HttpServletResponse response, boolean b, UserDTO userDTO, boolean approveOrReject) 
	{
		try
		{
			long id = Long.parseLong(request.getParameter("idToApprove"));
			Approval_summaryDTO approval_summaryDTO = (Approval_summaryDTO)approval_summaryDAO.getDTOByID(id);
			approval_summaryDTO.remarks = request.getParameter("remarks");
			approval_summaryDTO.fileID = Long.parseLong(request.getParameter("fileID"));
			if(approveOrReject)
			{
				approval_summaryDAO.manageWriteOperations(approval_summaryDTO, SessionConstants.APPROVE, id, userDTO);
			}
			else
			{
				approval_summaryDAO.manageWriteOperations(approval_summaryDTO, SessionConstants.REJECT, id, userDTO);
			}			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
	}
	private void addApproval_summary(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addApproval_summary");
			String path = getServletContext().getRealPath("/img2/");
			Approval_summaryDTO approval_summaryDTO;
			String FileNamePrefix;			
			if(addFlag == true)
			{
				approval_summaryDTO = new Approval_summaryDTO();
				FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
			}
			else
			{
				approval_summaryDTO = (Approval_summaryDTO)approval_summaryDAO.getDTOByID(Long.parseLong(request.getParameter("identity")));
				FileNamePrefix = request.getParameter("identity");
			}
			
			String Value = "";

			Value = request.getParameter("tableName");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("tableName = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				approval_summaryDTO.tableName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("tableId");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("tableId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				approval_summaryDTO.tableId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("initiator");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("initiator = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				approval_summaryDTO.initiator = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approvalStatusCat");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approvalStatusCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				approval_summaryDTO.approvalStatusCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("dateOfInitiation");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("dateOfInitiation = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				approval_summaryDTO.dateOfInitiation = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("dateOfResolution");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("dateOfResolution = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				approval_summaryDTO.dateOfResolution = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("resolver");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("resolver = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				approval_summaryDTO.resolver = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("assignedTo");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("assignedTo = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				approval_summaryDTO.assignedTo = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addApproval_summary dto = " + approval_summaryDTO);
			long returnedID = -1;
			
			if(addFlag == true)
			{
				returnedID = approval_summaryDAO.manageWriteOperations(approval_summaryDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				if(isPermanentTable)
				{
					returnedID = approval_summaryDAO.manageWriteOperations(approval_summaryDTO, SessionConstants.UPDATE, -1, userDTO);
				}
				else
				{
					returnedID = approval_summaryDAO.manageWriteOperations(approval_summaryDTO, SessionConstants.VALIDATE, -1, userDTO);
				}				
				
			}
			
			
			
			
			
			
			
			
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			
			if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				getApproval_summary(request, response, returnedID);
			}
			else
			{
				response.sendRedirect("Approval_summaryServlet?actionType=search");
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void deleteApproval_summary(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				
				Approval_summaryDTO approval_summaryDTO = (Approval_summaryDTO)approval_summaryDAO.getDTOByID(id);
				approval_summaryDAO.manageWriteOperations(approval_summaryDTO, SessionConstants.DELETE, id, userDTO);
				response.sendRedirect("Approval_summaryServlet?actionType=search");
				
			}			
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		
	}

	private void getApproval_summary(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getApproval_summary");
		Approval_summaryDTO approval_summaryDTO = null;
		try 
		{
			approval_summaryDTO = (Approval_summaryDTO)approval_summaryDAO.getDTOByID(id);
			request.setAttribute("ID", approval_summaryDTO.iD);
			request.setAttribute("approval_summaryDTO",approval_summaryDTO);
			request.setAttribute("approval_summaryDAO",approval_summaryDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "approval_summary/approval_summaryInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "approval_summary/approval_summarySearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "approval_summary/approval_summaryEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "approval_summary/approval_summaryEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getApproval_summary(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getApproval_summary(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchApproval_summary(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchApproval_summary 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_APPROVAL_SUMMARY,
			request,
			approval_summaryDAO,
			SessionConstants.VIEW_APPROVAL_SUMMARY,
			SessionConstants.SEARCH_APPROVAL_SUMMARY,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("approval_summaryDAO",approval_summaryDAO);
        RequestDispatcher rd;
        if(hasAjax == false)
        {
        	System.out.println("Going to approval_summary/approval_summarySearch.jsp");
        	rd = request.getRequestDispatcher("approval_summary/approval_summarySearch.jsp");
        }
        else
        {
        	System.out.println("Going to approval_summary/approval_summarySearchForm.jsp");
        	rd = request.getRequestDispatcher("approval_summary/approval_summarySearchForm.jsp");
        }
		rd.forward(request, response);
	}
	
}

