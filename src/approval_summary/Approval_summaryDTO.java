package approval_summary;
import java.util.*; 
import util.*; 


public class Approval_summaryDTO extends CommonDTO
{

    public String tableName = "";
	public long tableId = 0;
	public long initiator = 0;
	public int approvalStatusCat = 0;
	public long dateOfInitiation = 0;
	public long dateOfResolution = 0;
	public long resolver = 0;
	public long assignedTo = 0;
	public long originallyAssignedTo = 0;
	
	
    @Override
	public String toString() {
            return "$Approval_summaryDTO[" +
            " iD = " + iD +
            " tableName = " + tableName +
            " tableId = " + tableId +
            " initiator = " + initiator +
            " approvalStatusCat = " + approvalStatusCat +
            " dateOfInitiation = " + dateOfInitiation +
            " dateOfResolution = " + dateOfResolution +
            " resolver = " + resolver +
            " assignedTo = " + assignedTo +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}