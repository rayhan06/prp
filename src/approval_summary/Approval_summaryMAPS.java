package approval_summary;
import java.util.*; 
import util.*;


public class Approval_summaryMAPS extends CommonMaps
{	
	public Approval_summaryMAPS(String tableName)
	{
		
		java_allfield_type_map.put("table_name".toLowerCase(), "String");
		java_allfield_type_map.put("table_id".toLowerCase(), "Long");
		java_allfield_type_map.put("initiator".toLowerCase(), "Long");
		java_allfield_type_map.put("approval_status_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("date_of_initiation".toLowerCase(), "Long");
		java_allfield_type_map.put("date_of_resolution".toLowerCase(), "Long");
		java_allfield_type_map.put("resolver".toLowerCase(), "Long");
		java_allfield_type_map.put("assigned_to".toLowerCase(), "Long");

		java_anyfield_search_map.put("language_text.languageTextEnglish", "String");
		java_anyfield_search_map.put("language_text.languageTextBangla", "String");

		java_anyfield_search_map.put(tableName + ".table_name".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".table_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".initiator".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".date_of_initiation".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".date_of_resolution".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".resolver".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".assigned_to".toLowerCase(), "Long");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("tableName".toLowerCase(), "tableName".toLowerCase());
		java_DTO_map.put("tableId".toLowerCase(), "tableId".toLowerCase());
		java_DTO_map.put("initiator".toLowerCase(), "initiator".toLowerCase());
		java_DTO_map.put("approvalStatusCat".toLowerCase(), "approvalStatusCat".toLowerCase());
		java_DTO_map.put("dateOfInitiation".toLowerCase(), "dateOfInitiation".toLowerCase());
		java_DTO_map.put("dateOfResolution".toLowerCase(), "dateOfResolution".toLowerCase());
		java_DTO_map.put("resolver".toLowerCase(), "resolver".toLowerCase());
		java_DTO_map.put("assignedTo".toLowerCase(), "assignedTo".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("table_name".toLowerCase(), "tableName".toLowerCase());
		java_SQL_map.put("table_id".toLowerCase(), "tableId".toLowerCase());
		java_SQL_map.put("initiator".toLowerCase(), "initiator".toLowerCase());
		java_SQL_map.put("approval_status_cat".toLowerCase(), "approvalStatusCat".toLowerCase());
		java_SQL_map.put("date_of_initiation".toLowerCase(), "dateOfInitiation".toLowerCase());
		java_SQL_map.put("date_of_resolution".toLowerCase(), "dateOfResolution".toLowerCase());
		java_SQL_map.put("resolver".toLowerCase(), "resolver".toLowerCase());
		java_SQL_map.put("assigned_to".toLowerCase(), "assignedTo".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Table Name".toLowerCase(), "tableName".toLowerCase());
		java_Text_map.put("Table Id".toLowerCase(), "tableId".toLowerCase());
		java_Text_map.put("Initiator".toLowerCase(), "initiator".toLowerCase());
		java_Text_map.put("Approval Status Cat".toLowerCase(), "approvalStatusCat".toLowerCase());
		java_Text_map.put("Date Of Initiation".toLowerCase(), "dateOfInitiation".toLowerCase());
		java_Text_map.put("Date Of Resolution".toLowerCase(), "dateOfResolution".toLowerCase());
		java_Text_map.put("Resolver".toLowerCase(), "resolver".toLowerCase());
		java_Text_map.put("Assigned To".toLowerCase(), "assignedTo".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}