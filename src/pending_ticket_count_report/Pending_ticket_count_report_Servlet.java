package pending_ticket_count_report;


import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import support_ticket.Support_ticketDTO;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Pending_ticket_count_report_Servlet")
public class Pending_ticket_count_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","support_ticket","ticket_issues_type","=","","int","","","any","ticketIssuesType", LC.PENDING_TICKET_COUNT_REPORT_WHERE_TICKETISSUESTYPE + ""},		
		{"criteria","","current_assigned_organogram_id","=","AND","int","","","any","currentAssignedOrganogramId", LC.PENDING_TICKET_COUNT_REPORT_WHERE_CURRENTASSIGNEDORGANOGRAMID + ""},		
			
		{"criteria","","ticket_status_cat","=","AND","int","","",Support_ticketDTO.OPEN + "","status","ticketStatusCat", LC.PENDING_TICKET_COUNT_REPORT_WHERE_TICKETSTATUSCAT + ""}		
	};
	
	String[][] Display =
	{
		{"display","","current_assigned_organogram_id","organogram",""},		
		{"display","","ticket_issues_type","type",""},		
		{"display","","COUNT(support_ticket.id)","text",""}		
	};
	
	String GroupBy = "current_assigned_organogram_id, ticket_issues_type";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Pending_ticket_count_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "support_ticket";

		Display[0][4] = language.equalsIgnoreCase("english")?"Assigned Engineer":"দায়িত্বপ্রাপ্ত ইঞ্জিনিয়ার";

		Display[1][4] = LM.getText(LC.HM_TYPE, loginDTO);
		Display[2][4] = LM.getText(LC.HM_COUNT, loginDTO);

		
		String reportName = language.equalsIgnoreCase("english")?"Pending TIcket Count Report":"অমীমাংসিত টিকেটের সংখ্যার রিপোর্ট";
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(2, ReportRequestHandler.RIGHT_ALIGN_INT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "pending_ticket_count_report",
				MenuConstants.PENDING_TICKET_COUNT_REPORT_DETAILS, language, reportName, "pending_ticket_count_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
