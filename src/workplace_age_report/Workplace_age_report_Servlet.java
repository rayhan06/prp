package workplace_age_report;


import employee_education_report.Employee_education_report_Servlet;
import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pbReport.PBReportUtils;
import pbReport.ReportCommonService;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"unused", "Duplicates"})
@WebServlet("/Workplace_age_report_Servlet")
public class Workplace_age_report_Servlet extends HttpServlet implements ReportCommonService {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Employee_education_report_Servlet.class);
    private static final Set<String> searchParam = new HashSet<>(Arrays.asList("officeUnitIds", "officeUnitOrganogramId", "employmentCat", "startAge", "endAge", "home_district", "nameEng", "nameBng"));
    private final Map<String, String[]> stringMap = new HashMap<>();
    private static final String sql = " employee_offices eo INNER JOIN employee_records er ON eo.employee_record_id = er.id ";
    private String[][] Criteria;

    private final String[][] Display =
            {
                    {"display", "eo", "employee_record_id", "employee_records_id", ""},
                    {"display", "eo", "employee_record_id", "user_name", ""},
                    {"display", "er", "personal_mobile", "number", ""},
                    {"display", "er", "personal_email", "plain", ""},
                    {"display", "er", "home_district", "location", ""},
                    {"display", "eo", "office_unit_id", "office_unit", ""},
                    {"display", "eo", "id", "designation", ""},
                    {"display", "er", "employment_cat", "category", ""},
                    {"display", "er", "date_of_birth", "complete_age", ""},
                    {"display", "eo", "employee_record_id", "id_to_present_address", ""},
                    {"display", "eo", "employee_record_id", "id_to_permanent_address", ""}
            };

    public Workplace_age_report_Servlet() {
        stringMap.put("officeUnitIds", new String[]{"criteria", "eo", "office_unit_id", "IN", "AND", "String", "", "", "", "officeUnitIdList"
                , "officeUnitIds", String.valueOf(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITID), "office_units", null, "true", "1"});
        stringMap.put("officeUnitOrganogramId", new String[]{"criteria", "eo", "office_unit_organogram_id", "=", "AND", "long", "", "", ""
                , "officeUnitOrganogramId", "officeUnitOrganogramId", String.valueOf(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITORGANOGRAMID), "office_unit_organogram", null, "true", "2"});
        stringMap.put("employmentCat", new String[]{"criteria", "er", "employment_cat", "=", "AND", "int", "", "", "any", "employmentCat"
                , "employmentCat", String.valueOf(LC.WORKPLACE_AGE_REPORT_WHERE_EMPLOYMENTCAT), "category", "employment", "true", "3"});
        stringMap.put("startAge", new String[]{"criteria", "er", "date_of_birth", "<=", "AND", "long", "", "", "", "age_start"
                , "age_start", String.valueOf(LC.HR_REPORT_AGE_RANGE_START), "number", null, "true", "4"});
        stringMap.put("endAge", new String[]{"criteria", "er", "date_of_birth", ">=", "AND", "long", "", "", "", "age_end"
                , "age_end", String.valueOf(LC.HR_REPORT_AGE_RANGE_TO), "number", null, "true", "5"});
        stringMap.put("home_district", new String[]{"criteria", "er", "home_district", "=", "AND", "long", "", "", "", "home_district"
                , "home_district", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT), "location", null, "true", "6"});
        stringMap.put("nameEng", new String[]{"criteria", "er", "name_eng", "Like", "AND", "String", "", "", "any", "nameEng",
                "nameEng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEENG), "text", null, "true", "7"});
        stringMap.put("nameBng", new String[]{"criteria", "er", "name_bng", "Like", "AND", "String", "", "", "any", "nameBng",
                "nameBng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG), "text", null, "true", "8"});
        stringMap.put("office_id", new String[]{"criteria", "eo", "office_id", "=", "AND", "long", "", "", "2294", "office_id", null, null, null, null, null, null});
        stringMap.put("isDeleted", new String[]{"criteria", "eo", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted", null, null, null, null, null, null});
        stringMap.put("status", new String[]{"criteria", "eo", "status", "=", "AND", "long", "", "", "1", "status", null, null, null, null, null, null});
        stringMap.put("isDeleted_1", new String[]{"criteria", "er", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted_1", null, null, null, null, null, null});

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        Display[0][4] = LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_EMPLOYEERECORDID, loginDTO);
        Display[1][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_USER_ID, loginDTO);
        Display[2][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_MOBILE_NUMBER, loginDTO);
        Display[3][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_EMAIL, loginDTO);
        Display[4][4] = LM.getText(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT, loginDTO);
        Display[5][4] = LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_OFFICEUNITID, loginDTO);
        Display[6][4] = LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO);
        Display[7][4] = LM.getText(LC.WORKPLACE_AGE_REPORT_SELECT_EMPLOYMENTCAT, loginDTO);
        Display[8][4] = LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_AGE, loginDTO);
        Display[9][4] = LM.getText(LC.EMPLOYEE_RECORDS_ADD_PRESENTADDRESS, loginDTO);
        Display[10][4] = LM.getText(LC.EMPLOYEE_RECORDS_ADD_PERMANENTADDRESS, loginDTO);

        Set<String> inputs = PBReportUtils.prepareInputSets(searchParam, request);
        inputs.add("office_id");
        inputs.add("isDeleted");
        inputs.add("status");
        inputs.add("isDeleted_1");
        Criteria = PBReportUtils.prepareCriteria(inputs, stringMap);
        for (String[] arr : Criteria) {
            switch (arr[9]) {
                case "officeUnitIdList":
                    arr[8] = getOfficeIdsFromOfficeUnitIds(request).stream()
                            .map(String::valueOf)
                            .collect(Collectors.joining(","));
                    break;
                case "age_start":
                    long ageStart = Long.parseLong(request.getParameter("startAge"));
                    arr[8] = String.valueOf(calculateLongDateValue(ageStart));
                    break;
                case "age_end":
                    long ageEnd = Long.parseLong(request.getParameter("endAge"));
                    arr[8] = String.valueOf(calculateLongDateValue(ageEnd));
                    break;
            }
        }
        request.setAttribute("dontUseOutDatedModal", true);
        if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.WORKPLACE_AGE_REPORT_DETAILS)) {
            reportGenerate(request, response);
        }
    }

    private long calculateLongDateValue(long years) {
        LocalDate ld = LocalDate.now();
        ld = ld.minusYears(years);
        return Date.from(ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()).getTime();
    }

    @Override
    public String[][] getCriteria() {
        return Criteria;
    }

    @Override
    public String[][] getDisplay() {
        return Display;
    }

    @Override
    public String getGroupBy() {
        return " office_unit_id ";
    }

    @Override
    public String getSQL() {
        return sql;
    }

    @Override
    public int getLCForReportName() {
        return LC.WORKPLACE_AGE_REPORT_OTHER_WORKPLACE_AGE_REPORT;
    }

    @Override
    public String getFileName() {
        return "workplace_age_report";
    }

    @Override
    public String getTableName() {
        return "workplace_age_report";
    }
}