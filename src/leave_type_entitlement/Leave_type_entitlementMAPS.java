package leave_type_entitlement;

import util.CommonMaps;


public class Leave_type_entitlementMAPS extends CommonMaps
{	
	public Leave_type_entitlementMAPS(String tableName)
	{
		
		java_allfield_type_map.put("leave_type_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("number_of_days".toLowerCase(), "Integer");
		java_allfield_type_map.put("frequency".toLowerCase(), "Integer");
		java_allfield_type_map.put("minimum_job_duration".toLowerCase(), "Integer");
		java_allfield_type_map.put("start_date".toLowerCase(), "Long");
		java_allfield_type_map.put("end_date".toLowerCase(), "Long");

		java_anyfield_search_map.put("language_text.languageTextEnglish", "String");
		java_anyfield_search_map.put("language_text.languageTextBangla", "String");

		java_anyfield_search_map.put(tableName + ".number_of_days".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".frequency".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".minimum_job_duration".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".start_date".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".end_date".toLowerCase(), "Long");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("leaveTypeCat".toLowerCase(), "leaveTypeCat".toLowerCase());
		java_DTO_map.put("numberOfDays".toLowerCase(), "numberOfDays".toLowerCase());
		java_DTO_map.put("frequency".toLowerCase(), "frequency".toLowerCase());
		java_DTO_map.put("minimumJobDuration".toLowerCase(), "minimumJobDuration".toLowerCase());
		java_DTO_map.put("otherCondition1".toLowerCase(), "otherCondition1".toLowerCase());
		java_DTO_map.put("otherCondition2".toLowerCase(), "otherCondition2".toLowerCase());
		java_DTO_map.put("otherCondition3".toLowerCase(), "otherCondition3".toLowerCase());
		java_DTO_map.put("atATimeMaxEntitlementDays".toLowerCase(), "atATimeMaxEntitlementDays".toLowerCase());
		java_DTO_map.put("hasCarryForward".toLowerCase(), "hasCarryForward".toLowerCase());
		java_DTO_map.put("maxCarryForwardInYears".toLowerCase(), "maxCarryForwardInYears".toLowerCase());
		java_DTO_map.put("hasLeaveEncashment".toLowerCase(), "hasLeaveEncashment".toLowerCase());
		java_DTO_map.put("leaveEncashmentPercentage".toLowerCase(), "leaveEncashmentPercentage".toLowerCase());
		java_DTO_map.put("startDate".toLowerCase(), "startDate".toLowerCase());
		java_DTO_map.put("endDate".toLowerCase(), "endDate".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("leave_type_cat".toLowerCase(), "leaveTypeCat".toLowerCase());
		java_SQL_map.put("number_of_days".toLowerCase(), "numberOfDays".toLowerCase());
		java_SQL_map.put("frequency".toLowerCase(), "frequency".toLowerCase());
		java_SQL_map.put("minimum_job_duration".toLowerCase(), "minimumJobDuration".toLowerCase());
		java_SQL_map.put("other_condition_1".toLowerCase(), "otherCondition1".toLowerCase());
		java_SQL_map.put("other_condition_2".toLowerCase(), "otherCondition2".toLowerCase());
		java_SQL_map.put("other_condition_3".toLowerCase(), "otherCondition3".toLowerCase());
		java_SQL_map.put("at_a_time_max_entitlement_days".toLowerCase(), "atATimeMaxEntitlementDays".toLowerCase());
		java_SQL_map.put("has_carry_forward".toLowerCase(), "hasCarryForward".toLowerCase());
		java_SQL_map.put("max_carry_forward_in_years".toLowerCase(), "maxCarryForwardInYears".toLowerCase());
		java_SQL_map.put("has_leave_encashment".toLowerCase(), "hasLeaveEncashment".toLowerCase());
		java_SQL_map.put("leave_encashment_percentage".toLowerCase(), "leaveEncashmentPercentage".toLowerCase());
		java_SQL_map.put("start_date".toLowerCase(), "startDate".toLowerCase());
		java_SQL_map.put("end_date".toLowerCase(), "endDate".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Leave Type Cat".toLowerCase(), "leaveTypeCat".toLowerCase());
		java_Text_map.put("Number Of Days".toLowerCase(), "numberOfDays".toLowerCase());
		java_Text_map.put("Frequency".toLowerCase(), "frequency".toLowerCase());
		java_Text_map.put("Minimum Job Duration".toLowerCase(), "minimumJobDuration".toLowerCase());
		java_Text_map.put("Other Condition 1".toLowerCase(), "otherCondition1".toLowerCase());
		java_Text_map.put("Other Condition 2".toLowerCase(), "otherCondition2".toLowerCase());
		java_Text_map.put("Other Condition 3".toLowerCase(), "otherCondition3".toLowerCase());
		java_Text_map.put("At A Time Max Entitlement Days".toLowerCase(), "atATimeMaxEntitlementDays".toLowerCase());
		java_Text_map.put("Has Carry Forward".toLowerCase(), "hasCarryForward".toLowerCase());
		java_Text_map.put("Max Carry Forward In Years".toLowerCase(), "maxCarryForwardInYears".toLowerCase());
		java_Text_map.put("Has Leave Encashment".toLowerCase(), "hasLeaveEncashment".toLowerCase());
		java_Text_map.put("Leave Encashment Percentage".toLowerCase(), "leaveEncashmentPercentage".toLowerCase());
		java_Text_map.put("Start Date".toLowerCase(), "startDate".toLowerCase());
		java_Text_map.put("End Date".toLowerCase(), "endDate".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}