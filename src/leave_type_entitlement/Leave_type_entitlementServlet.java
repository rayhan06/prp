package leave_type_entitlement;

import com.google.gson.Gson;
import common.ApiResponse;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/Leave_type_entitlementServlet")
@MultipartConfig
public class Leave_type_entitlementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Leave_type_entitlementServlet.class);

    String tableName = "leave_type_entitlement";

    Leave_type_entitlementDAO leave_type_entitlementDAO;
    CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    public Leave_type_entitlementServlet() {
        super();

        leave_type_entitlementDAO = new Leave_type_entitlementDAO(tableName);
        commonRequestHandler = new CommonRequestHandler(leave_type_entitlementDAO);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getAddPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LEAVE_TYPE_ENTITLEMENT_ADD)) {
                        commonRequestHandler.getAddPage(request, response);
                        return;
                    }
                    break;
                case "getEditPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LEAVE_TYPE_ENTITLEMENT_UPDATE)) {
                        String URL = getLeave_type_entitlement(request, response);
                        request.getRequestDispatcher(URL).forward(request, response);
                        return;
                    }
                    break;
                case "getURL":
                    String URL = request.getParameter("URL");
                    response.sendRedirect(URL);
                    return;
                case "search":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LEAVE_TYPE_ENTITLEMENT_SEARCH)) {
                        searchLeave_type_entitlement(request, response);
                        return;
                    }
                    break;
                case "view":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LEAVE_TYPE_ENTITLEMENT_SEARCH)) {
                        commonRequestHandler.view(request, response);
                        return;
                    }

                    break;

            }
        } catch (Exception ex) {
            logger.debug(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);


    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "ajax_add":

                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LEAVE_TYPE_ENTITLEMENT_ADD)) {


                        try {
                            String URL = addLeave_type_entitlement(request, response, true, userDTO);
                            ApiResponse.sendSuccessResponse(response, URL);
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    }

                    break;
                case "getDTO":

                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LEAVE_TYPE_ENTITLEMENT_ADD)) {
                        getDTO(request, response);
                        return;
                    }

                    break;
                case "ajax_edit":

                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LEAVE_TYPE_ENTITLEMENT_UPDATE)) {
                        try {
                            String URL = addLeave_type_entitlement(request, response, false, userDTO);
                            ApiResponse.sendSuccessResponse(response, URL);
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    }
                    break;
                case "delete":
                    deleteLeave_type_entitlement(request, response, userDTO);
                    return;
                case "search":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LEAVE_TYPE_ENTITLEMENT_SEARCH)) {
                        searchLeave_type_entitlement(request, response);
                        return;
                    }
                    break;
                case "getGeo":
                    request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
                    return;
            }

        } catch (Exception ex) {
            logger.debug(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);

    }

    private void getDTO(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Leave_type_entitlementDTO leave_type_entitlementDTO = (Leave_type_entitlementDTO) leave_type_entitlementDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String encoded = this.gson.toJson(leave_type_entitlementDTO);
        out.print(encoded);
        out.flush();


    }

    private String addLeave_type_entitlement(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO) throws Exception {
        long currentTime = System.currentTimeMillis();
        request.setAttribute("failureMessage", "");

        Leave_type_entitlementDTO leave_type_entitlementDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

        if (addFlag) {
            leave_type_entitlementDTO = new Leave_type_entitlementDTO();
            leave_type_entitlementDTO.insertionDate = currentTime;
            leave_type_entitlementDTO.insertedBy = String.valueOf(userDTO.ID);
        } else {
            leave_type_entitlementDTO = (Leave_type_entitlementDTO) leave_type_entitlementDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
        }
        leave_type_entitlementDTO.modifiedBy = String.valueOf(userDTO.ID);
        leave_type_entitlementDTO.lastModificationTime = currentTime;

        String Value;

        leave_type_entitlementDTO.leaveTypeCat = Integer.parseInt(Jsoup.clean(request.getParameter("leaveTypeCat"), Whitelist.simpleText()));


        leave_type_entitlementDTO.numberOfDays = Integer.parseInt(Jsoup.clean(request.getParameter("numberOfDays"), Whitelist.simpleText()));

        leave_type_entitlementDTO.frequency = Integer.parseInt(Jsoup.clean(request.getParameter("frequency"), Whitelist.simpleText()));

        leave_type_entitlementDTO.minimumJobDuration = Integer.parseInt(Jsoup.clean(request.getParameter("minimumJobDuration"), Whitelist.simpleText()));


        leave_type_entitlementDTO.otherCondition1 = (Jsoup.clean(request.getParameter("otherCondition1"), Whitelist.simpleText()));


        leave_type_entitlementDTO.otherCondition2 = (Jsoup.clean(request.getParameter("otherCondition2"), Whitelist.simpleText()));

        leave_type_entitlementDTO.otherCondition3 = (Jsoup.clean(request.getParameter("otherCondition3"), Whitelist.simpleText()));

        leave_type_entitlementDTO.atATimeMaxEntitlementDays = Integer.parseInt(Jsoup.clean(request.getParameter("atATimeMaxEntitlementDays"), Whitelist.simpleText()));

        if (request.getParameter("hasCarryForward") != null) {
            Value = Jsoup.clean(request.getParameter("hasCarryForward"), Whitelist.simpleText());

            leave_type_entitlementDTO.hasCarryForward = Value != null && !Value.equalsIgnoreCase("");

        }


        leave_type_entitlementDTO.maxCarryForwardInYears = Integer.parseInt(Jsoup.clean(request.getParameter("maxCarryForwardInYears"), Whitelist.simpleText()));
        if (request.getParameter("hasLeaveEncashment") != null) {
            Value = Jsoup.clean(request.getParameter("hasLeaveEncashment"), Whitelist.simpleText());
            leave_type_entitlementDTO.hasLeaveEncashment = Value != null && !Value.equalsIgnoreCase("");
        }


        leave_type_entitlementDTO.leaveEncashmentPercentage = Double.parseDouble(Jsoup.clean(request.getParameter("leaveEncashmentPercentage"), Whitelist.simpleText()));
        Date d;
        Value = Jsoup.clean(request.getParameter("startDate"), Whitelist.simpleText());

        if (Value != null && !Value.equalsIgnoreCase("")) {
            d = f.parse(Value);
            leave_type_entitlementDTO.startDate = d.getTime();
        }
        Value = Jsoup.clean(request.getParameter("endDate"), Whitelist.simpleText());
        if (Value != null && !Value.equalsIgnoreCase("")) {
            d = f.parse(Value);
            leave_type_entitlementDTO.endDate = d.getTime();
        }

        System.out.println("Done adding  addLeave_type_entitlement dto = " + leave_type_entitlementDTO);
        long returnedID;

        if (addFlag) {
            returnedID = leave_type_entitlementDAO.manageWriteOperations(leave_type_entitlementDTO, SessionConstants.INSERT, -1, userDTO);
        } else {
            returnedID = leave_type_entitlementDAO.manageWriteOperations(leave_type_entitlementDTO, SessionConstants.UPDATE, -1, userDTO);
        }


        String inPlaceSubmit = request.getParameter("inplacesubmit");

        if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
            return getLeave_type_entitlement(request, response, returnedID);
        } else {
            return "Leave_type_entitlementServlet?actionType=search";
        }


    }


    private void deleteLeave_type_entitlement(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception {

        String[] IDsToDelete = request.getParameterValues("ID");
        for (String s : IDsToDelete) {
            long id = Long.parseLong(s);
            System.out.println("------ DELETING " + s);
            Leave_type_entitlementDTO leave_type_entitlementDTO = (Leave_type_entitlementDTO) leave_type_entitlementDAO.getDTOByID(id);
            leave_type_entitlementDAO.manageWriteOperations(leave_type_entitlementDTO, SessionConstants.DELETE, id, userDTO);
            response.sendRedirect("Leave_type_entitlementServlet?actionType=search");

        }


    }

    private String getLeave_type_entitlement(HttpServletRequest request, HttpServletResponse response, long id) throws Exception {
        Leave_type_entitlementDTO leave_type_entitlementDTO;
        leave_type_entitlementDTO = (Leave_type_entitlementDTO) leave_type_entitlementDAO.getDTOByID(id);
        request.setAttribute("ID", leave_type_entitlementDTO.iD);
        request.setAttribute("leave_type_entitlementDTO", leave_type_entitlementDTO);
        request.setAttribute("leave_type_entitlementDAO", leave_type_entitlementDAO);

        String URL;

        String inPlaceEdit = request.getParameter("inplaceedit");
        String inPlaceSubmit = request.getParameter("inplacesubmit");
        String getBodyOnly = request.getParameter("getBodyOnly");

        if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
            URL = "leave_type_entitlement/leave_type_entitlementInPlaceEdit.jsp";
            request.setAttribute("inplaceedit", "");
        } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
            URL = "leave_type_entitlement/leave_type_entitlementSearchRow.jsp";
            request.setAttribute("inplacesubmit", "");
        } else {
            if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                URL = "leave_type_entitlement/leave_type_entitlementEditBody.jsp?actionType=edit";
            } else {
                URL = "leave_type_entitlement/leave_type_entitlementEdit.jsp?actionType=edit";
            }
        }

        return URL;

    }


    private String getLeave_type_entitlement(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return getLeave_type_entitlement(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void searchLeave_type_entitlement(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_LEAVE_TYPE_ENTITLEMENT,
                request,
                leave_type_entitlementDAO,
                SessionConstants.VIEW_LEAVE_TYPE_ENTITLEMENT,
                SessionConstants.SEARCH_LEAVE_TYPE_ENTITLEMENT,
                tableName,
                true,
                userDTO,
                "",
                true);

        rnManager.doJob(loginDTO);


        request.setAttribute("leave_type_entitlementDAO", leave_type_entitlementDAO);

        if (!hasAjax) {
            System.out.println("Going to leave_type_entitlement/leave_type_entitlementSearch.jsp");
            request.getRequestDispatcher("leave_type_entitlement/leave_type_entitlementSearch.jsp").forward(request, response);
        } else {
            System.out.println("Going to leave_type_entitlement/leave_type_entitlementSearchForm.jsp");
            request.getRequestDispatcher("leave_type_entitlement/leave_type_entitlementSearchForm.jsp").forward(request, response);
        }

    }

}

