package leave_type_entitlement;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import common.CommonDTOService;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

@SuppressWarnings({"unused","rawtypes","Duplicates"})
public class Leave_type_entitlementDAO extends NavigationService4 implements CommonDTOService<Leave_type_entitlementDTO>{

	private static final Logger logger = Logger.getLogger(Leave_type_entitlementDAO.class);
	
	private static final String addSqlQuery = "INSERT INTO {tableName} (leave_type_cat,number_of_days,frequency,minimum_job_duration,"
											.concat("other_condition_1,other_condition_2,other_condition_3,at_a_time_max_entitlement_days,")
											.concat("has_carry_forward,max_carry_forward_in_years,has_leave_encashment,leave_encashment_percentage,start_date,end_date,")
											.concat("insertion_date,inserted_by,modified_by,lastModificationTime,isDeleted,ID)")
											.concat("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
	private static final String updateSqlQuery = "UPDATE {tableName} SET leave_type_cat=?, number_of_days=?, frequency=?, minimum_job_duration=?,"
											.concat("other_condition_1=?,other_condition_2=?,other_condition_3=?,at_a_time_max_entitlement_days=?,")
											.concat("has_carry_forward=?,max_carry_forward_in_years=?,has_leave_encashment=?,leave_encashment_percentage=?, ")
											.concat("start_date=?, end_date=?, insertion_date=?,inserted_by=?, modified_by=?,lastModificationTime = ? WHERE ID = ?");

	public Leave_type_entitlementDAO(String tableName) {
		super(tableName);
		joinSQL = "";

		joinSQL += " join category on (";
		joinSQL += " (" + tableName + ".leave_type_cat = category.value  and category.domain_name = 'leave_type')";
		joinSQL += " )";
		joinSQL += " join language_text on category.language_id = language_text.id";
		commonMaps = new Leave_type_entitlementMAPS(tableName);
	}

	public Leave_type_entitlementDAO() {
		this("leave_type_entitlement");
	}

	public void set(PreparedStatement ps, Leave_type_entitlementDTO leave_type_entitlementDTO, boolean isInsert) throws SQLException {
		int index = 0;
		ps.setObject(++index, leave_type_entitlementDTO.leaveTypeCat);
		ps.setObject(++index, leave_type_entitlementDTO.numberOfDays);
		ps.setObject(++index, leave_type_entitlementDTO.frequency);
		ps.setObject(++index, leave_type_entitlementDTO.minimumJobDuration);
		ps.setObject(++index, leave_type_entitlementDTO.otherCondition1);
		ps.setObject(++index, leave_type_entitlementDTO.otherCondition2);
		ps.setObject(++index, leave_type_entitlementDTO.otherCondition3);
		ps.setObject(++index, leave_type_entitlementDTO.atATimeMaxEntitlementDays);
		ps.setObject(++index, leave_type_entitlementDTO.hasCarryForward);
		ps.setObject(++index, leave_type_entitlementDTO.maxCarryForwardInYears);
		ps.setObject(++index, leave_type_entitlementDTO.hasLeaveEncashment);
		ps.setObject(++index, leave_type_entitlementDTO.leaveEncashmentPercentage);
		ps.setObject(++index, leave_type_entitlementDTO.startDate);
		ps.setObject(++index, leave_type_entitlementDTO.endDate);
		ps.setObject(++index, leave_type_entitlementDTO.insertionDate);
		ps.setObject(++index, leave_type_entitlementDTO.insertedBy);
		ps.setObject(++index, leave_type_entitlementDTO.modifiedBy);
		ps.setObject(++index, System.currentTimeMillis());
		if (isInsert) {
			ps.setObject(++index, 0);
		}
		ps.setObject(++index, leave_type_entitlementDTO.iD);
	}

	@Override
	public Leave_type_entitlementDTO buildObjectFromResultSet(ResultSet rs){
		try{
			Leave_type_entitlementDTO leave_type_entitlementDTO = new Leave_type_entitlementDTO();
			leave_type_entitlementDTO.iD = rs.getLong("ID");
			leave_type_entitlementDTO.leaveTypeCat = rs.getInt("leave_type_cat");
			leave_type_entitlementDTO.numberOfDays = rs.getInt("number_of_days");
			leave_type_entitlementDTO.frequency = rs.getInt("frequency");
			leave_type_entitlementDTO.minimumJobDuration = rs.getInt("minimum_job_duration");
			leave_type_entitlementDTO.otherCondition1 = rs.getString("other_condition_1");
			leave_type_entitlementDTO.otherCondition2 = rs.getString("other_condition_2");
			leave_type_entitlementDTO.otherCondition3 = rs.getString("other_condition_3");
			leave_type_entitlementDTO.atATimeMaxEntitlementDays = rs.getInt("at_a_time_max_entitlement_days");
			leave_type_entitlementDTO.hasCarryForward = rs.getBoolean("has_carry_forward");
			leave_type_entitlementDTO.maxCarryForwardInYears = rs.getInt("max_carry_forward_in_years");
			leave_type_entitlementDTO.hasLeaveEncashment = rs.getBoolean("has_leave_encashment");
			leave_type_entitlementDTO.leaveEncashmentPercentage = rs.getDouble("leave_encashment_percentage");
			leave_type_entitlementDTO.startDate = rs.getLong("start_date");
			leave_type_entitlementDTO.endDate = rs.getLong("end_date");
			leave_type_entitlementDTO.insertionDate = rs.getLong("insertion_date");
			leave_type_entitlementDTO.insertedBy = rs.getString("inserted_by");
			leave_type_entitlementDTO.modifiedBy = rs.getString("modified_by");
			leave_type_entitlementDTO.isDeleted = rs.getInt("isDeleted");
			leave_type_entitlementDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return leave_type_entitlementDTO;
		}catch (SQLException ex){
			logger.error(ex);
			return null;
		}
	}

	@Override
	public String getTableName() {
		return "leave_type_entitlement";
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Leave_type_entitlementDTO) commonDTO,addSqlQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Leave_type_entitlementDTO) commonDTO,updateSqlQuery,false);
	}
	
	public CommonDTO getDTOByID(long ID) throws Exception {
		return getDTOFromID(ID);
	}

	public List<Leave_type_entitlementDTO> getAllLeave_type_entitlement(boolean isFirstReload) {
		return getAllDTOs(isFirstReload);
	}

	public List<Leave_type_entitlementDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset,
			boolean isPermanentTable, UserDTO userDTO) {
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	public List<Leave_type_entitlementDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat) {
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO,
				filter, tableHasJobCat);
		return getDTOs(sql);
	}

}
