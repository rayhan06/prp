package leave_type_entitlement;

import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Leave_type_entitlementRepository implements Repository {
    Leave_type_entitlementDAO leave_type_entitlementDAO = null;

    public void setDAO(Leave_type_entitlementDAO leave_type_entitlementDAO) {
        this.leave_type_entitlementDAO = leave_type_entitlementDAO;
    }

    static Logger logger = Logger.getLogger(Leave_type_entitlementRepository.class);
    private final Map<Long, Leave_type_entitlementDTO> mapOfLeave_type_entitlementDTOToiD;


    private Leave_type_entitlementRepository() {
        mapOfLeave_type_entitlementDTOToiD = new ConcurrentHashMap<>();


        RepositoryManager.getInstance().addRepository(this);
    }

    private static class Leave_type_entitlementRepositoryLoader {
        static Leave_type_entitlementRepository INSTANCE = new Leave_type_entitlementRepository();
    }

    public static Leave_type_entitlementRepository getInstance() {
        return Leave_type_entitlementRepositoryLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        if (leave_type_entitlementDAO == null) {
            return;
        }
        try {
            List<Leave_type_entitlementDTO> leave_type_entitlementDTOs = leave_type_entitlementDAO
                    .getAllLeave_type_entitlement(reloadAll);
            for (Leave_type_entitlementDTO leave_type_entitlementDTO : leave_type_entitlementDTOs) {
                Leave_type_entitlementDTO oldLeave_type_entitlementDTO = getLeave_type_entitlementDTOByID(
                        leave_type_entitlementDTO.iD);
                if (oldLeave_type_entitlementDTO != null) {
                    mapOfLeave_type_entitlementDTOToiD.remove(oldLeave_type_entitlementDTO.iD);

                }
                if (leave_type_entitlementDTO.isDeleted == 0) {

                    mapOfLeave_type_entitlementDTOToiD.put(leave_type_entitlementDTO.iD, leave_type_entitlementDTO);
                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public List<Leave_type_entitlementDTO> getLeave_type_entitlementList() {
        return new ArrayList<>(
                this.mapOfLeave_type_entitlementDTOToiD.values());
    }

    public Leave_type_entitlementDTO getLeave_type_entitlementDTOByID(long ID) {
        if (mapOfLeave_type_entitlementDTOToiD.get(ID) == null) {
            synchronized (LockManager.getLock(ID + "LRMR")) {
                if (mapOfLeave_type_entitlementDTOToiD.get(ID) == null) {
                    try {
                        Leave_type_entitlementDTO dto = (Leave_type_entitlementDTO) leave_type_entitlementDAO.getDTOByID(ID);
                        if (dto != null) {
                            mapOfLeave_type_entitlementDTOToiD.put(dto.iD, dto);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return mapOfLeave_type_entitlementDTOToiD.get(ID);
    }


    @Override
    public String getTableName() {
        String tableName = "";
        try {
            tableName = "leave_type_entitlement";
        } catch (Exception ex) {
            logger.debug("FATAL", ex);
        }
        return tableName;
    }
}
