package leave_type_entitlement;

import sessionmanager.SessionConstants;
import util.CommonDTO;


public class Leave_type_entitlementDTO extends CommonDTO
{

	public int leaveTypeCat = 0;
	public int numberOfDays = 0;
	public int frequency = 0;
	public int minimumJobDuration = 0;
    public String otherCondition1 = "";
    public String otherCondition2 = "";
    public String otherCondition3 = "";
	public int atATimeMaxEntitlementDays = 0;
	public boolean hasCarryForward = false;
	public int maxCarryForwardInYears = 0;
	public boolean hasLeaveEncashment = false;
	public double leaveEncashmentPercentage = 0;
	public long startDate = SessionConstants.MIN_DATE;
    public long endDate = SessionConstants.MIN_DATE;
    public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Leave_type_entitlementDTO[" +
            " iD = " + iD +
            " leaveTypeCat = " + leaveTypeCat +
            " numberOfDays = " + numberOfDays +
            " frequency = " + frequency +
            " minimumJobDuration = " + minimumJobDuration +
            " otherCondition1 = " + otherCondition1 +
            " otherCondition2 = " + otherCondition2 +
            " otherCondition3 = " + otherCondition3 +
            " atATimeMaxEntitlementDays = " + atATimeMaxEntitlementDays +
            " hasCarryForward = " + hasCarryForward +
            " maxCarryForwardInYears = " + maxCarryForwardInYears +
            " hasLeaveEncashment = " + hasLeaveEncashment +
            " leaveEncashmentPercentage = " + leaveEncashmentPercentage +
            " startDate = " + startDate +
            " endDate = " + endDate +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}