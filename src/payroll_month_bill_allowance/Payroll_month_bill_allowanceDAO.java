package payroll_month_bill_allowance;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

@SuppressWarnings({"Duplicates"})
public class Payroll_month_bill_allowanceDAO implements CommonDAOService<Payroll_month_bill_allowanceDTO> {
    private static final Logger logger = Logger.getLogger(Payroll_month_bill_allowanceDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (payroll_month_bill_id,payroll_allowance_configuration_id,amount,modified_by,lastModificationTime,"
                    .concat("inserted_by,insertion_time,isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery =
            "UPDATE {tableName} SET payroll_month_bill_id=?,payroll_allowance_configuration_id=?,amount=?,"
                    .concat("modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final String getByMonthBillIdQuery =
            "SELECT * From payroll_month_bill_allowance where payroll_month_bill_id = %d AND isDeleted=0;";

    private static final String getByPayrollIds =
            "SELECT * FROM payroll_month_bill_allowance WHERE payroll_month_bill_id IN (%s) AND isDeleted=0";

    private static final Map<String, String> searchMap = new HashMap<>();

    private Payroll_month_bill_allowanceDAO() {
    }

    private static class LazyLoader {
        static final Payroll_month_bill_allowanceDAO INSTANCE = new Payroll_month_bill_allowanceDAO();
    }

    public static Payroll_month_bill_allowanceDAO getInstance() {
        return Payroll_month_bill_allowanceDAO.LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Payroll_month_bill_allowanceDTO payroll_month_bill_allowanceDTO, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, payroll_month_bill_allowanceDTO.payrollMonthBillId);
        ps.setLong(++index, payroll_month_bill_allowanceDTO.payrollAllowanceConfigurationId);
        ps.setInt(++index, payroll_month_bill_allowanceDTO.amount);
        ps.setLong(++index, payroll_month_bill_allowanceDTO.modifiedBy);
        ps.setLong(++index, payroll_month_bill_allowanceDTO.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, payroll_month_bill_allowanceDTO.insertedBy);
            ps.setLong(++index, payroll_month_bill_allowanceDTO.insertionTime);
            ps.setInt(++index, 0);
        }
        ps.setLong(++index, payroll_month_bill_allowanceDTO.iD);
    }

    @Override
    public Payroll_month_bill_allowanceDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Payroll_month_bill_allowanceDTO dto = new Payroll_month_bill_allowanceDTO();
            dto.iD = rs.getLong("ID");
            dto.payrollMonthBillId = rs.getInt("payroll_month_bill_id");
            dto.payrollAllowanceConfigurationId = rs.getInt("payroll_allowance_configuration_id");
            dto.amount = rs.getInt("amount");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "payroll_month_bill_allowance";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Payroll_month_bill_allowanceDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Payroll_month_bill_allowanceDTO) commonDTO, updateQuery, false);
    }

    public List<Payroll_month_bill_allowanceDTO> getByByMonthBillId(long monthBillId) {
        return getDTOs(String.format(getByMonthBillIdQuery, monthBillId));
    }

    public List<Payroll_month_bill_allowanceDTO> getBypayrollMonthBillIds(List<Long> payrollMonthBillIds) {
        String monthBillIds = payrollMonthBillIds.stream()
                                                 .map(String::valueOf)
                                                 .collect(Collectors.joining(","));
        return getDTOs(String.format(getByPayrollIds, monthBillIds));
    }

    public Map<Long, Integer> getSumOfAllowance(List<Payroll_month_bill_allowanceDTO> dtos) {
        return dtos.stream()
                   .collect(groupingBy(dto -> dto.payrollAllowanceConfigurationId, summingInt(dto -> dto.amount)));
    }
}