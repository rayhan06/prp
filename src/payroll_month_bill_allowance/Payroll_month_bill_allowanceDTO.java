package payroll_month_bill_allowance;

import sessionmanager.SessionConstants;
import util.CommonDTO;

import java.util.Objects;

public class Payroll_month_bill_allowanceDTO extends CommonDTO {
    public long payrollMonthBillId = 0;
    public long payrollAllowanceConfigurationId = 0;
    public int amount = 0;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;
    public long modifiedBy = -1;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Payroll_month_bill_allowanceDTO)) return false;
        Payroll_month_bill_allowanceDTO that = (Payroll_month_bill_allowanceDTO) o;
        return payrollAllowanceConfigurationId == that.payrollAllowanceConfigurationId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(payrollAllowanceConfigurationId);
    }

    @Override
    public String toString() {
        return "Payroll_month_billDTO{" +
                "payrollMonthBillId=" + payrollMonthBillId +
                ", payrollAllowanceConfigurationId=" + payrollAllowanceConfigurationId +
                ", amount=" + amount +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                '}';
    }
}
