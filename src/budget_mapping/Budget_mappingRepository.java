package budget_mapping;

import budget.BudgetDAO;
import budget_institutional_group.Budget_institutional_groupDTO;
import budget_institutional_group.Budget_institutional_groupRepository;
import budget_office.Budget_officeDTO;
import budget_office.Budget_officeRepository;
import budget_operation.BudgetOperationModel;
import budget_operation.Budget_operationDTO;
import budget_operation.Budget_operationRepository;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.OptionDTO;
import pb.Utils;
import util.LockManager;
import util.StringUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Budget_mappingRepository {
    private final Logger logger = Logger.getLogger(Budget_mappingRepository.class);
    private final Budget_mappingDAO budget_mappingDAO = Budget_mappingDAO.getInstance();
    private Map<Long, Budget_mappingDTO> mapById;
    private List<Budget_mappingDTO> allBudgetMappingDTOList;


    private Budget_mappingRepository() {
        reload();
    }

    private static class Budget_mappingRepositoryLazyLoader {
        static final Budget_mappingRepository INSTANCE = new Budget_mappingRepository();
    }

    public static Budget_mappingRepository getInstance() {
        return Budget_mappingRepository.Budget_mappingRepositoryLazyLoader.INSTANCE;
    }

    private void reload() {
        logger.debug("Budget_mappingRepository reload start");
        List<Budget_mappingDTO> list = budget_mappingDAO.getAllDTOs(true);
        if (list != null && list.size() > 0) {
            allBudgetMappingDTOList = list.stream()
                                          .filter(e -> e.isDeleted == 0)
                                          .collect(Collectors.toList());
            mapById = allBudgetMappingDTOList.stream()
                                             .collect(Collectors.toMap(e -> e.iD, e -> e, (e1, e2) -> e1));
        } else {
            mapById = new ConcurrentHashMap<>();
            allBudgetMappingDTOList = new ArrayList<>();
        }
        logger.debug("Budget_mappingRepository reload end");
    }

    public Budget_mappingDTO getById(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "BMAP")) {
                if (mapById.get(id) == null) {
                    Budget_mappingDTO dto = budget_mappingDAO.getDTOFromIdDeletedOrNot(id);
                    if (dto != null) {
                        mapById.put(dto.iD, dto);
                        if (dto.isDeleted == 0) {
                            allBudgetMappingDTOList.add(dto);
                        }
                    }
                }
            }
        }
        return mapById.get(id);
    }

    public Budget_mappingDTO getDTO(long budgetOfficeId, int budgetCat) {
        return allBudgetMappingDTOList.stream()
                                      .filter(dto -> dto.budgetOfficeId == budgetOfficeId)
                                      .filter(dto -> dto.budgetCat == budgetCat)
                                      .findFirst()
                                      .orElse(null);
    }

    public String buildBudgetOperationCat(String language, long budgetOfficeId, Long selectedId) {
        List<OptionDTO> budgetOptionDTOs = mapById.values().stream()
                                                  .filter(dto -> dto.budgetOfficeId == budgetOfficeId)
                                                  .map(dto -> Budget_operationRepository.getInstance().getById(dto.budgetOperationId))
                                                  .map(dto -> dto.getOptionDTO(true))
                                                  .collect(Collectors.toList());

        return Utils.buildOptions(budgetOptionDTOs, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String buildBudgetCat(String language, Predicate<Budget_mappingDTO> filterFunction, Long selectedId) {
        Set<Integer> budgetCatIds = mapById.values().stream()
                                           .filter(filterFunction)
                                           .map(dto -> dto.budgetCat)
                                           .collect(Collectors.toSet());

        List<OptionDTO> budgetOptionDTOs = budgetCatIds.stream()
                                                       .map(id -> CatRepository.getInstance().getCategoryLanguageModel("budget", id))
                                                       .map(dto -> new OptionDTO(dto.englishText, dto.banglaText, String.valueOf(dto.categoryValue)))
                                                       .collect(Collectors.toList());

        return Utils.buildOptions(budgetOptionDTOs, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String buildBudgetCatByInstitutionalGroup(String language, long budgetInstitutionalGroupId, Long selectedId) {
        return buildBudgetCat(
                language,
                dto -> dto.budgetInstitutionalGroupId == budgetInstitutionalGroupId,
                selectedId
        );
    }

    public String buildBudgetCatByOfficeId(String language, long budgetOfficeId, Long selectedId) {
        return buildBudgetCat(
                language,
                dto -> dto.budgetOfficeId == budgetOfficeId,
                selectedId
        );
    }

    public String buildBudgetOffice(String language, long budgetInstitutionalGroupId, int budgetCat, Long selectedId, boolean withCode) {
        Set<Long> budgetOfficeIds =
                mapById.values().stream()
                       .filter(dto -> dto.budgetInstitutionalGroupId == budgetInstitutionalGroupId)
                       .filter(dto -> budgetCat == -1 || dto.budgetCat == budgetCat)
                       .map(dto -> dto.budgetOfficeId)
                       .collect(Collectors.toSet());

        List<OptionDTO> budgetOptionDTOs =
                budgetOfficeIds.stream()
                               .map(id -> Budget_officeRepository.getInstance().getById(id))
                               .map(dto -> dto.getOptionDTO(withCode))
                               .collect(Collectors.toList());

        return Utils.buildOptions(budgetOptionDTOs, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String buildOperationDropDown(String language, Long selectedId, boolean withCode, Predicate<Budget_mappingDTO> filterFunction) {

        List<OptionDTO> budgetMappingOptions =
                mapById.values()
                       .stream()
                       .filter(filterFunction)
                       .map(dto -> Budget_operationRepository.getInstance().getById(dto.budgetOperationId))
                       .map(dto -> dto.getOptionDTO(withCode))
                       .collect(Collectors.toList());

        return Utils.buildOptions(budgetMappingOptions, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String buildOperationDropDown(String language, int budgetCat, long budgetOfficeId, Long selectedId, boolean withCode) {
        return buildOperationDropDown(
                language,
                selectedId,
                withCode,
                dto -> dto.budgetCat == budgetCat && dto.budgetOfficeId == budgetOfficeId
        );
    }

    public String buildDropDown(String language, Long selectedId, boolean withCode, Predicate<Budget_mappingDTO> filterFunction) {
        List<OptionDTO> budgetMappingOptions =
                mapById.values()
                       .stream()
                       .filter(filterFunction)
                       .map(dto -> dto.getOptionDTO(withCode))
                       .sorted(OptionDTO.defaultOrderingComparator)
                       .collect(Collectors.toList());
        return Utils.buildOptions(budgetMappingOptions, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String buildOperationDropDown(String language, long budgetInstitutionalGroupId, int budgetCat, long budgetOfficeId, Long selectedId, boolean withCode) {
        return buildOperationDropDown(
                language,
                selectedId,
                withCode,
                dto -> dto.budgetInstitutionalGroupId == budgetInstitutionalGroupId
                       && (budgetCat == -1 || dto.budgetCat == budgetCat)
                       && dto.budgetOfficeId == budgetOfficeId
        );
    }

    public BudgetOperationModel getBudgetOperationModelWithoutSubCode(long budgetMappingId, String economicYear, String language) {
        Budget_mappingDTO dto = getById(budgetMappingId);
        Budget_operationDTO operationDTO = Budget_operationRepository.getInstance().getById(dto.budgetOperationId);
        BudgetOperationModel model = new BudgetOperationModel();
        model.budgetMappingId = String.valueOf(dto.iD);

        boolean isLanguageEnglish = "English".equalsIgnoreCase(language);
        model.description = isLanguageEnglish ? operationDTO.descriptionEn : operationDTO.descriptionBn;
        model.operationCode = isLanguageEnglish ? operationDTO.code : StringUtils.convertToBanNumber(operationDTO.code);

        Budget_institutional_groupDTO institutionalGroupDTO = Budget_institutional_groupRepository.getInstance().getById(dto.budgetInstitutionalGroupId);
        if (institutionalGroupDTO != null) {
            model.institutionalName = isLanguageEnglish ? institutionalGroupDTO.nameEn : institutionalGroupDTO.nameBn;
            model.institutionalCode = isLanguageEnglish ? institutionalGroupDTO.code : StringUtils.convertToBanNumber(institutionalGroupDTO.code);
        }

        Budget_officeDTO officeDTO = Budget_officeRepository.getInstance().getById(dto.budgetOfficeId);
        if (officeDTO != null) {
            model.officeName = isLanguageEnglish ? officeDTO.nameEn : officeDTO.nameBn;
            model.officeCode = isLanguageEnglish ? officeDTO.code : StringUtils.convertToBanNumber(officeDTO.code);
        }

        model.budgetCatName = CatRepository.getInstance().getText(language, "budget", dto.budgetCat)
                              + (isLanguageEnglish ? " Budget" : " বাজেট");

        model.economicYear = (isLanguageEnglish ? "Economic Year - " : "আর্থিক বছর - ") + economicYear;
        model.economicYear = StringUtils.convertBanglaIfLanguageIsBangla(language, model.economicYear);
        model.economicSubCodeModels = new ArrayList<>();
        return model;
    }

    public BudgetOperationModel getBudgetOperationModelById(long budgetMappingId, String economicYear, String language) {
        BudgetOperationModel model = getBudgetOperationModelWithoutSubCode(budgetMappingId, economicYear, language);

        // TODO: read from repo when repo done
        model.economicSubCodeModels = BudgetDAO.getInstance().getEconomicSubCodeModels(language, economicYear, budgetMappingId);
        return model;
    }

    public List<Budget_mappingDTO> getBudgetMappingList() {
        return allBudgetMappingDTOList;
    }

    public long getOfficeId(long id) {
        Budget_mappingDTO mappingDTO = getById(id);
        return mappingDTO == null ? -1L : mappingDTO.budgetOfficeId;
    }

    public String getOperationText(String language, long id) {
        Budget_mappingDTO mappingDTO = getById(id);
        if (mappingDTO == null) return "";
        return Budget_operationRepository.getInstance().getTextById(language, mappingDTO.budgetOperationId);
    }

    public String getOperationTextWithoutCode(String language, long id) {
        Budget_mappingDTO mappingDTO = getById(id);
        if (mappingDTO == null) return "";
        return Budget_operationRepository.getInstance().getTextWithoutCodeById(language, mappingDTO.budgetOperationId);
    }

    public String getCode(String language, long id) {
        Budget_mappingDTO mappingDTO = getById(id);
        if (mappingDTO == null) return "";
        return Budget_operationRepository.getInstance().getCode(language, mappingDTO.budgetOperationId);
    }
}
