package budget_mapping;

import budget_operation.Budget_operationDTO;
import budget_operation.Budget_operationRepository;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import util.CommonDTO;

public class Budget_mappingDTO extends CommonDTO {
    private static final Logger logger = Logger.getLogger(Budget_mappingDTO.class);

    public long budgetInstitutionalGroupId = 0;
    public int budgetCat = 0;
    public long budgetOfficeId = 0;
    public long budgetOperationId = 0;


    public long insertedBy = 0;
    public long insertionTime = 0;
    public long modifiedBy = 0;

    public OptionDTO getOptionDTO(boolean withCode) {
        Budget_operationDTO operationDTO = Budget_operationRepository.getInstance().getById(budgetOperationId);
        if (operationDTO == null) {
            logger.error(String.format(
                    "Budget_mappingDTO with id=%d has no Budget_operationDTO with budgetOperationId=%d",
                    iD, budgetOperationId
            ));
            return null;
        }
        OptionDTO operationOptionDTO = operationDTO.getOptionDTO(withCode);
        operationOptionDTO.value = String.format("%d", iD);
        return operationOptionDTO;
    }

    @Override
    public String toString() {
        return "Budget_mappingDTO{" +
               "budgetInstitutionalGroupId=" + budgetInstitutionalGroupId +
               ", budgetCat=" + budgetCat +
               ", budgetOfficeId=" + budgetOfficeId +
               ", budgetOperationId=" + budgetOperationId +
               ", insertedBy=" + insertedBy +
               ", insertionTime=" + insertionTime +
               ", modifiedBy=" + modifiedBy +
               '}';
    }
}
