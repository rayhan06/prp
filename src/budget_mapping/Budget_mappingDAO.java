package budget_mapping;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Budget_mappingDAO implements CommonDAOService<Budget_mappingDTO> {
    private static final Logger logger = Logger.getLogger(Budget_mappingDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (budget_institutional_group_id, budget_cat, budget_office_id,budget_operation_id, "
            .concat("modified_by, lastModificationTime, inserted_by, insertion_time, isDeleted, ID) ")
            .concat("VALUES ( ?, ?, ?, ?, ?, ?,?, ?, ?, ?)");

    private static final String updateQuery = "UPDATE {tableName} SET budget_institutional_group_id = ?, budget_cat = ?, budget_office_id = ?,budget_operation_id=?, "
            .concat(" modified_by = ?, lastModificationTime = ? WHERE ID = ?");


    private static final Map<String, String> searchMap = new HashMap<>();


    private Budget_mappingDAO() {

    }

    private static class LazyLoader {
        static final Budget_mappingDAO INSTANCE = new Budget_mappingDAO();
    }

    public static Budget_mappingDAO getInstance() {
        return Budget_mappingDAO.LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Budget_mappingDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.budgetInstitutionalGroupId);
        ps.setInt(++index, dto.budgetCat);
        ps.setLong(++index, dto.budgetOfficeId);
        ps.setLong(++index, dto.budgetOperationId);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);

        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* dto.isDeleted */);
        }

        ps.setLong(++index, dto.iD);
    }

    @Override
    public Budget_mappingDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Budget_mappingDTO dto = new Budget_mappingDTO();
            dto.iD = rs.getLong("ID");
            dto.budgetInstitutionalGroupId = rs.getLong("budget_institutional_group_id");
            dto.budgetCat = rs.getInt("budget_cat");
            dto.budgetOfficeId = rs.getLong("budget_office_id");
            dto.budgetOperationId = rs.getInt("budget_operation_id");

            dto.isDeleted = rs.getInt("isDeleted");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");

            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "budget_mapping";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Budget_mappingDTO) commonDTO, updateQuery, false);
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Budget_mappingDTO) commonDTO, addQuery, true);
    }


}
