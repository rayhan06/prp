CREATE TABLE budget_mapping
(
    ID                   BIGINT(20) UNSIGNED PRIMARY KEY,
    budget_institutional_group_id          BIGINT(20),
    budget_cat          INT(11),
    budget_office_id          BIGINT(20),
    budget_operation_id          BIGINT(20),
    isDeleted            INT(11)      DEFAULT 0,
    inserted_by          BIGINT(20)   DEFAULT 0,
    insertion_time       BIGINT(20)   DEFAULT -62135791200000,
    modified_by          BIGINT(20)   DEFAULT 0,
    lastModificationTime BIGINT(20)   DEFAULT -62135791200000
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;