package budget_mapping;

import budget_operation.BudgetOperationModel;
import com.google.gson.Gson;
import common.BaseServlet;
import common.CommonDAOService;
import economic_code.Economic_codeRepository;
import economic_operation_mapping.Economic_operation_mappingRepository;
import economic_sub_code.Economic_sub_codeRepository;
import login.LoginDTO;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Budget_mappingServlet")
@MultipartConfig
public class Budget_mappingServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    public static Logger logger = Logger.getLogger(Budget_mappingServlet.class);

    @Override
    public String getTableName() {
        return "budget";
    }

    @Override
    public String getServletName() {
        return "Budget_mappingServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return null;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Budget_mappingServlet.class;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        String language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";

        try {
            String actionType = request.getParameter("actionType");
            Long selectedValue;
            long budgetInstituitionalGroupId;
            int budgetCat;
            String budgetCatStr;
            String options;
            boolean withCode;

            switch (actionType) {
                case "buildEconomicGroup":
                    selectedValue = request.getParameter("selectedValue") != null ? Long.parseLong(request.getParameter("selectedValue")) : null;
                    long budgetOperationId = Long.parseLong(request.getParameter("budget_operation_id"));
                    options = Economic_operation_mappingRepository.getInstance().buildEconomicGroupOption(language, budgetOperationId, selectedValue);
                    logger.debug(options);
                    response.setContentType("text/html; charset=UTF-8");
                    response.getWriter().println(options);
                    return;
                case "getBudgetCatList":
                    selectedValue = request.getParameter("selectedValue") != null ? Long.parseLong(request.getParameter("selectedValue")) : null;
                    budgetInstituitionalGroupId = Long.parseLong(request.getParameter("budget_instituitional_group_id"));
                    options = Budget_mappingRepository.getInstance().buildBudgetCatByInstitutionalGroup(language, budgetInstituitionalGroupId, selectedValue);
                    logger.debug(options);
                    response.setContentType("text/html; charset=UTF-8");
                    response.getWriter().println(options);
                    return;
                case "getBudgetOfficeList":
                    selectedValue = request.getParameter("selectedValue") != null ? Long.parseLong(request.getParameter("selectedValue")) : null;
                    budgetInstituitionalGroupId = Long.parseLong(request.getParameter("budget_instituitional_group_id"));
                    budgetCatStr = request.getParameter("budget_cat");
                    budgetCat = budgetCatStr == null ? -1 : Integer.parseInt(budgetCatStr);
                    withCode = Boolean.parseBoolean(request.getParameter("withCode"));
                    options = Budget_mappingRepository.getInstance().buildBudgetOffice(language, budgetInstituitionalGroupId, budgetCat, selectedValue, withCode);
                    response.setContentType("text/html; charset=UTF-8");
                    response.getWriter().println(options);
                    return;
                case "getOperationCodeList":
                    selectedValue = request.getParameter("selectedValue") != null ? Long.parseLong(request.getParameter("selectedValue")) : null;
                    budgetInstituitionalGroupId = Long.parseLong(request.getParameter("budget_instituitional_group_id"));
                    budgetCatStr = request.getParameter("budget_cat");
                    budgetCat = budgetCatStr == null ? -1 : Integer.parseInt(budgetCatStr);
                    long budgetOfficeId = Long.parseLong(request.getParameter("budget_office_id"));
                    withCode = Boolean.parseBoolean(request.getParameter("withCode"));
                    options = Budget_mappingRepository.getInstance().buildOperationDropDown(
                            language, budgetInstituitionalGroupId,
                            budgetCat, budgetOfficeId, selectedValue, withCode
                    );
                    logger.debug(options);
                    response.setContentType("text/html; charset=UTF-8");
                    response.getWriter().println(options);
                    return;
                case "getBudgetOperationModel":
                    long id = Long.parseLong(request.getParameter("id"));
                    String economicYear = request.getParameter("economicYear");
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    BudgetOperationModel model = Budget_mappingRepository.getInstance().getBudgetOperationModelById(id, economicYear, language);
                    response.getWriter().println(new Gson().toJson(model));
                    return;
                case "getEconomicSubCode":
                    long economicCode = Long.parseLong(request.getParameter("economicCode"));
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().println(Economic_sub_codeRepository.getInstance().getModelsJSON(economicCode, language));
                    return;

                case "getEconomicSubCodeOption": {
                    economicCode = Long.parseLong(request.getParameter("economicCode"));
                    selectedValue = request.getParameter("selectedId") != null ? Long.parseLong(request.getParameter("selectedId")) : null;
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().println(Economic_sub_codeRepository.getInstance().buildOption(language, economicCode, selectedValue));
                    response.getWriter().flush();
                    return;
                }
                case "buildEconomicCode":
                    selectedValue = request.getParameter("selectedValue") != null ? Long.parseLong(request.getParameter("selectedValue")) : null;
                    long economicGroupType = Long.parseLong(request.getParameter("economic_group_type"));
                    options = Economic_codeRepository.getInstance().buildEconomicCodeOptionFromEconomicGroup(language, economicGroupType, selectedValue);
                    logger.debug(options);
                    response.setContentType("text/html; charset=UTF-8");
                    response.getWriter().println(options);
                    return;
            }
        } catch (Exception e) {
            logger.error(e);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }
}
