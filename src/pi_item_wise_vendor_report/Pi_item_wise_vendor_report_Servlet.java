package pi_item_wise_vendor_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;


@WebServlet("/Pi_item_wise_vendor_report_Servlet")
public class Pi_item_wise_vendor_report_Servlet extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    String[][] Criteria =
            {
                    {"criteria", "tr", "actual_vendor_id", "=", "", "String", "", "", "any", "actualVendorId", LC.PI_ITEM_WISE_VENDOR_REPORT_WHERE_ACTUALVENDORID + ""},
                    {"criteria", "tr", "fiscal_year_id", "=", "AND", "String", "", "", "any", "fiscalYearId", LC.PI_ITEM_WISE_VENDOR_REPORT_WHERE_FISCALYEARID + ""},
                    {"criteria", "tr", "is_winner", "=", "AND", "int", "", "", "1", "isWinner", LC.PI_ITEM_WISE_VENDOR_REPORT_WHERE_ISWINNER + ""},
                    {"criteria", "tr", "isDeleted", "=", "AND", "String", "", "", "0", "isDeleted", LC.PI_ITEM_WISE_VENDOR_REPORT_WHERE_ISDELETED + ""}
            };

    String[][] Display =
            {
                    {"display", "tr", "actual_vendor_id", "actual_vendor_id", ""},
                    {"display", "tr", "fiscal_year_id", "fiscal_year_id_check", ""},
                    {"display", "tr", "office_unit_id", "office_unit", ""},
                    {"display", "tr", "package_id", "procurement_goods_type_id_check", ""},
                    {"display", "tr", "product_id", "productIdConverter", ""},
                    {"display", "tr", "price", "text", ""}
            };

    String GroupBy = "";
    String OrderBY = "";

    ReportRequestHandler reportRequestHandler;

    public Pi_item_wise_vendor_report_Servlet() {

    }

    private final ReportService reportService = new ReportService();

    private String sql;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String language = "english";
        if (userDTO != null && userDTO.languageID == SessionConstants.BANGLA) {
            language = "bangla";
        }

        String actionType = request.getParameter("actionType");

        System.out.println("In ssservlet doget, actiontype = " + actionType);

        sql = "pi_package_vendor_items tr";

        Display[0][4] = LM.getText(LC.PI_ITEM_WISE_VENDOR_REPORT_SELECT_ACTUALVENDORID, loginDTO);
        Display[1][4] = LM.getText(LC.PI_ITEM_WISE_VENDOR_REPORT_SELECT_FISCALYEARID, loginDTO);
        Display[2][4] = LM.getText(LC.PI_ITEM_WISE_VENDOR_REPORT_SELECT_OFFICEUNITID, loginDTO);
        Display[3][4] = LM.getText(LC.PI_ITEM_WISE_VENDOR_REPORT_SELECT_PACKAGEID, loginDTO);
        Display[4][4] = LM.getText(LC.PI_ITEM_WISE_VENDOR_REPORT_SELECT_PRODUCTID, loginDTO);
        Display[5][4] = LM.getText(LC.PI_PACKAGE_AUCTIONEER_ITEMS_ADD_PRICE, loginDTO);


        String reportName = LM.getText(LC.PI_ITEM_WISE_VENDOR_REPORT_OTHER_PI_ITEM_WISE_VENDOR_REPORT, loginDTO);

        reportRequestHandler = new ReportRequestHandler(null,
                Criteria, Display, GroupBy, OrderBY, sql,
                reportService);


        reportRequestHandler.handleReportGet(request, response, userDTO, "pi_item_wise_vendor_report",
                MenuConstants.PI_ITEM_WISE_VENDOR_REPORT_DETAILS, language, reportName, "pi_item_wise_vendor_report");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
