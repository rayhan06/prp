package officewise_asset;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;

import login.LoginDTO;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Officewise_asset_Servlet")
public class Officewise_asset_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{	
		{"criteria","","responsible_organogram_id","=","","String","","","any","none", LC.HM_OFFICE + ""}		
	};
	
	String[][] Display =
	{
		{"display","employee_offices","office_unit_id","office_unit",""},		
		{"display","","SUM(CASE WHEN asset_category_type = 1404 THEN 1 ELSE 0 END)","int",""},		
		{"display","","SUM(CASE WHEN asset_category_type = 1401 THEN 1 ELSE 0 END)","int",""},		
		{"display","","SUM(CASE WHEN asset_category_type = 1400 THEN 1 ELSE 0 END)","int",""},		
		{"display","","SUM(CASE WHEN asset_category_type = 1403 THEN 1 ELSE 0 END)","int",""},		
		{"display","","SUM(CASE WHEN asset_category_type = 1405 THEN 1 ELSE 0 END)","int",""},		
		{"display","","SUM(CASE WHEN asset_category_type = 1402 THEN 1 ELSE 0 END)","int",""}		
	};
	
	String GroupBy = "employee_offices.office_unit_id";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Officewise_asset_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
		boolean isLangEng = language.equalsIgnoreCase("english");		
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "asset_assignee JOIN employee_offices ON (employee_offices.office_unit_organogram_id = asset_assignee.responsible_organogram_id  AND employee_offices.isDeleted = 0 and employee_offices.is_default_role = 1)";

		Display[0][4] = LM.getText(LC.HM_OFFICE, loginDTO);
		Display[1][4] = isLangEng ? "CPU Count" : "সি পি ইউর সংখ্যা";
		Display[2][4] = isLangEng ? "Laptop Count" : "ল্যাপটপের সংখ্যা";
		
		Display[3][4] = isLangEng ? "Monitor Count" : "মনিটরের সংখ্যা";
		Display[4][4] = isLangEng ? "Printer Count" : "প্রিন্টারের সংখ্যা";
		Display[5][4] = isLangEng ? "Scanner Count" : "স্ক্যানারের সংখ্যা";
		Display[6][4] = isLangEng ? "UPS Count" : "ইউ পি এসের সংখ্যা";

		
		String officeUnitId = request.getParameter("officeUnitId");
		if(officeUnitId == null || officeUnitId.equalsIgnoreCase(""))
		{
			Criteria[0][SessionConstants.REPORT_OPERATOR_POS] = "=";
			Criteria[0][SessionConstants.REPORT_VALUE_POS] = "any";
		}
		else
		{
			long lOfficeUnitId = Long.parseLong(officeUnitId);
			Set<Long> organograms = OfficeUnitOrganogramsRepository.getInstance().getAllDescentsOrganogramIdsInclusive (lOfficeUnitId);
			
			String valStr = "" ;
			int i = 0;
			for(long org: organograms)
			{
				if(i > 0)
				{
					valStr += ", ";
				}
				valStr +=  org + "";
				i ++;
			}
			
			
			Criteria[0][SessionConstants.REPORT_OPERATOR_POS] = "in";
			Criteria[0][SessionConstants.REPORT_VALUE_POS] = valStr;
			
		}
		
		String reportName = LM.getText(LC.OFFICEWISE_ASSET_OTHER_OFFICEWISE_ASSET, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(1, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(2, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(3, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(4, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(5, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(6, ReportRequestHandler.RIGHT_ALIGN_INT));

		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "officewise_asset",
				MenuConstants.OFFICEWISE_ASSET_DETAILS, language, reportName, "officewise_asset");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
