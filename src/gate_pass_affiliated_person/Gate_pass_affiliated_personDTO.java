package gate_pass_affiliated_person;
import java.util.*; 
import util.*; 


public class Gate_pass_affiliated_personDTO extends CommonDTO
{

//	public long id = 0;
	public long gatePassId = 0;
	public int credentialType = 0;
    public String credentialNo = "";
    public String mobileNumber = "";
    public String name = "";
	public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
    public String officeName = "";
    public String designation = "";
	
	
    @Override
	public String toString() {
            return "$Gate_pass_affiliated_personDTO[" +
            " id = " + iD +
            " gatePassId = " + gatePassId +
            " credentialType = " + credentialType +
            " credentialNo = " + credentialNo +
            " mobileNumber = " + mobileNumber +
            " name = " + name +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}