package gate_pass_affiliated_person;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.*;

import gate_pass.GatePassPersonModel;
import gate_pass.Gate_passDAO;
import gate_pass.Gate_passDTO;
import gate_pass_item.Gate_pass_itemDAO;
import gate_pass_visitor.Gate_pass_visitorDAO;
import gate_pass_visitor.Gate_pass_visitorDTO;

import java.sql.SQLException;
import java.util.stream.Collectors;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Gate_pass_affiliated_personDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Gate_pass_affiliated_personDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Gate_pass_affiliated_personMAPS(tableName);
		columnNames = new String[] 
		{
			"id",
			"gate_pass_id",
			"credential_type",
			"credential_no",
			"mobile_number",
			"name",
			"insertion_date",
			"inserted_by",
			"office_name",
			"designation",
			"modified_by",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Gate_pass_affiliated_personDAO()
	{
		this("gate_pass_affiliated_person");		
	}
	
	public void setSearchColumn(Gate_pass_affiliated_personDTO gate_pass_affiliated_personDTO)
	{
		gate_pass_affiliated_personDTO.searchColumn = "";
		gate_pass_affiliated_personDTO.searchColumn += gate_pass_affiliated_personDTO.credentialNo + " ";
		gate_pass_affiliated_personDTO.searchColumn += gate_pass_affiliated_personDTO.mobileNumber + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Gate_pass_affiliated_personDTO gate_pass_affiliated_personDTO = (Gate_pass_affiliated_personDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(gate_pass_affiliated_personDTO);
		if(isInsert)
		{
			ps.setObject(index++,gate_pass_affiliated_personDTO.iD);
		}
		ps.setObject(index++,gate_pass_affiliated_personDTO.gatePassId);
		ps.setObject(index++,gate_pass_affiliated_personDTO.credentialType);
		ps.setObject(index++,gate_pass_affiliated_personDTO.credentialNo);
		ps.setObject(index++,"88"+gate_pass_affiliated_personDTO.mobileNumber);
		ps.setObject(index++,gate_pass_affiliated_personDTO.name);
		ps.setObject(index++,gate_pass_affiliated_personDTO.insertionDate);
		ps.setObject(index++,gate_pass_affiliated_personDTO.insertedBy);
		ps.setObject(index++,gate_pass_affiliated_personDTO.officeName);
		ps.setObject(index++,gate_pass_affiliated_personDTO.designation);
		ps.setObject(index++,gate_pass_affiliated_personDTO.modifiedBy);
		
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Gate_pass_affiliated_personDTO gate_pass_affiliated_personDTO, ResultSet rs) throws SQLException
	{
		gate_pass_affiliated_personDTO.iD = rs.getLong("id");
		gate_pass_affiliated_personDTO.gatePassId = rs.getLong("gate_pass_id");
		gate_pass_affiliated_personDTO.credentialType = rs.getInt("credential_type");
		gate_pass_affiliated_personDTO.credentialNo = rs.getString("credential_no");
		gate_pass_affiliated_personDTO.mobileNumber = rs.getString("mobile_number");
		gate_pass_affiliated_personDTO.name = rs.getString("name");
		gate_pass_affiliated_personDTO.insertionDate = rs.getLong("insertion_date");
		gate_pass_affiliated_personDTO.insertedBy = rs.getString("inserted_by");
		gate_pass_affiliated_personDTO.officeName = rs.getString("office_name");
		gate_pass_affiliated_personDTO.designation = rs.getString("designation");
		gate_pass_affiliated_personDTO.modifiedBy = rs.getString("modified_by");
		
		gate_pass_affiliated_personDTO.isDeleted = rs.getInt("isDeleted");
		gate_pass_affiliated_personDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}


	public void buildGatePassPersonModel(GatePassPersonModel gatePassPersonModel, ResultSet rs) throws SQLException
	{
		String data = rs.getString("mobile_number");
		gatePassPersonModel.mobileNoEng = data.substring(2);
		gatePassPersonModel.mobileNoBng = StringUtils.convertToBanNumber(gatePassPersonModel.mobileNoEng);

		int categoryType = rs.getInt("credential_type");
		gatePassPersonModel.credentialTypeEng = CatRepository.getInstance().getText("English", "credential", categoryType);
		gatePassPersonModel.credentialTypeBng = CatRepository.getInstance().getText("Bangla", "credential", categoryType);

		gatePassPersonModel.credentialNo = rs.getString("credential_no");

		gatePassPersonModel.name = rs.getString("name");
		gatePassPersonModel.officeName = rs.getString("office_name");
		gatePassPersonModel.designation = rs.getString("designation");

		gatePassPersonModel.id = rs.getLong("id");
	}




	public void buildGatePassPersonModelForCardNumber(GatePassPersonModel gatePassPersonModel, ResultSet rs) throws SQLException
	{
		Gate_pass_itemDAO gate_pass_itemDAO = new Gate_pass_itemDAO();
		String data = rs.getString("mobile_number");
		gatePassPersonModel.mobileNoEng = data.substring(2);
		gatePassPersonModel.mobileNoBng = StringUtils.convertToBanNumber(gatePassPersonModel.mobileNoEng);

		int categoryType = rs.getInt("credential_type");
		gatePassPersonModel.credentialTypeEng = CatRepository.getInstance().getText("English", "credential", categoryType);
		gatePassPersonModel.credentialTypeBng = CatRepository.getInstance().getText("Bangla", "credential", categoryType);

		gatePassPersonModel.credentialNo = rs.getString("credential_no");

		gatePassPersonModel.name = rs.getString("name");

		gatePassPersonModel.id = rs.getLong("id");

		gatePassPersonModel.cardNumber = gate_pass_itemDAO.getCardNumberByVisitorName(gatePassPersonModel.name);
	}
	
		
	

	//need another getter for repository
	public Gate_pass_affiliated_personDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Gate_pass_affiliated_personDTO gate_pass_affiliated_personDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE id=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				gate_pass_affiliated_personDTO = new Gate_pass_affiliated_personDTO();

				get(gate_pass_affiliated_personDTO, rs);

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return gate_pass_affiliated_personDTO;
	}

	public Gate_pass_affiliated_personDTO getDTOByMobileNumberAndCredential(String mobileNumber, String credentialNo)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Gate_pass_affiliated_personDTO gate_pass_affiliated_personDTO = null;
		try{

			String sql = "SELECT * ";

			sql += " FROM " + tableName;

			sql += " WHERE mobile_number=" + mobileNumber;

			sql += " AND credential_no=" + credentialNo;

			sql += " ORDER BY lastModificationTime DESC LIMIT 1";

			printSql(sql);

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				gate_pass_affiliated_personDTO = new Gate_pass_affiliated_personDTO();

				get(gate_pass_affiliated_personDTO, rs);

			}

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return gate_pass_affiliated_personDTO;
	}
	
	
	public List<Gate_pass_affiliated_personDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Gate_pass_affiliated_personDTO gate_pass_affiliated_personDTO = null;
		List<Gate_pass_affiliated_personDTO> gate_pass_affiliated_personDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return gate_pass_affiliated_personDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;

            sql += " WHERE id IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";

			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				gate_pass_affiliated_personDTO = new Gate_pass_affiliated_personDTO();
				get(gate_pass_affiliated_personDTO, rs);
				System.out.println("got this DTO: " + gate_pass_affiliated_personDTO);
				
				gate_pass_affiliated_personDTOList.add(gate_pass_affiliated_personDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return gate_pass_affiliated_personDTOList;
	
	}


	
	
	
	//add repository
	public List<Gate_pass_affiliated_personDTO> getAllGate_pass_affiliated_person (boolean isFirstReload)
    {
		List<Gate_pass_affiliated_personDTO> gate_pass_affiliated_personDTOList = new ArrayList<>();

		String sql = "SELECT * FROM gate_pass_affiliated_person";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by gate_pass_affiliated_person.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Gate_pass_affiliated_personDTO gate_pass_affiliated_personDTO = new Gate_pass_affiliated_personDTO();
				get(gate_pass_affiliated_personDTO, rs);
				
				gate_pass_affiliated_personDTOList.add(gate_pass_affiliated_personDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return gate_pass_affiliated_personDTOList;
    }

	
	public List<Gate_pass_affiliated_personDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Gate_pass_affiliated_personDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Gate_pass_affiliated_personDTO> gate_pass_affiliated_personDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Gate_pass_affiliated_personDTO gate_pass_affiliated_personDTO = new Gate_pass_affiliated_personDTO();
				get(gate_pass_affiliated_personDTO, rs);
				
				gate_pass_affiliated_personDTOList.add(gate_pass_affiliated_personDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return gate_pass_affiliated_personDTOList;
	
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("credential_type")
						|| str.equals("credential_no")
						|| str.equals("mobile_number")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("credential_type"))
					{
						AllFieldSql += "" + tableName + ".credential_type = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("credential_no"))
					{
						AllFieldSql += "" + tableName + ".credential_no like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("mobile_number"))
					{
						AllFieldSql += "" + tableName + ".mobile_number like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }


    public List<GatePassPersonModel> getGatePassPersonModelsByGatePassId(long gatePassId) {
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		GatePassPersonModel gatePassPersonModel = null;
		List<GatePassPersonModel> gatePassPersonModels = new ArrayList<>();

		try{

			String sql = "select * from gate_pass_affiliated_person where isDeleted=0 and gate_pass_id=%d";
			sql = String.format(sql, gatePassId);
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();

			rs = stmt.executeQuery(sql);

			while(rs.next()){
				gatePassPersonModel = new GatePassPersonModel();
				buildGatePassPersonModel(gatePassPersonModel, rs);

				gatePassPersonModels.add(gatePassPersonModel);
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return gatePassPersonModels;
	}

	public List<GatePassPersonModel> getGatePassPersonModelsByGatePassIdForCardNumber(long gatePassId) {
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		GatePassPersonModel gatePassPersonModel = null;
		List<GatePassPersonModel> gatePassPersonModels = new ArrayList<>();

		try{

			String sql = "select * from gate_pass_affiliated_person where isDeleted=0 and gate_pass_id=%d";
			sql = String.format(sql, gatePassId);
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();

			rs = stmt.executeQuery(sql);

			while(rs.next()){
				gatePassPersonModel = new GatePassPersonModel();
				buildGatePassPersonModelForCardNumber(gatePassPersonModel, rs);

				gatePassPersonModels.add(gatePassPersonModel);
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return gatePassPersonModels;
	}


	public String buildVisitorOptions(String language, long gatePassId) {

		Gate_pass_visitorDAO gatePassVisitorDAO = new Gate_pass_visitorDAO();
		Gate_passDAO gatePassDAO = new Gate_passDAO();
		Gate_passDTO gatePassDTO = gatePassDAO.getDTOByID(gatePassId);
		Gate_pass_visitorDTO gatePassVisitorDTO = gatePassVisitorDAO.getDTOByID(gatePassDTO.gatePassVisitorId);

		List<OptionDTO> optionDTOList = new ArrayList<>();
		optionDTOList.add(new OptionDTO(gatePassVisitorDTO.name, gatePassVisitorDTO.name, gatePassVisitorDTO.name));

		List<GatePassPersonModel> personList = getGatePassPersonModelsByGatePassId(gatePassId);
		if (personList != null && !personList.isEmpty()) {
			optionDTOList.addAll(personList.stream()
					.map(dto -> new OptionDTO(dto.name, dto.name, dto.name))
					.collect(Collectors.toList()));
		}
		return Utils.buildOptions(optionDTOList, language, gatePassVisitorDTO.name);
	}

	public String buildEligibleVisitorOptions(String language, long gatePassId) {
		Gate_pass_itemDAO gatePassItemDAO = new Gate_pass_itemDAO();
		Gate_pass_visitorDAO gatePassVisitorDAO = new Gate_pass_visitorDAO();
		Gate_passDAO gatePassDAO = new Gate_passDAO();
		Gate_passDTO gatePassDTO = gatePassDAO.getDTOByID(gatePassId);
		Gate_pass_visitorDTO gatePassVisitorDTO = gatePassVisitorDAO.getDTOByID(gatePassDTO.gatePassVisitorId);
		List<String> visitorNames = gatePassItemDAO.getAdditionalItems(gatePassId).stream()
				.map(dto -> dto.visitorName).collect(Collectors.toList());
		
		/*for(String visitorName: visitorNames)
		{
			logger.debug("##visitorName = " + visitorName);
		}*/

		List<OptionDTO> optionDTOList = new ArrayList<>();

		logger.debug("##gatePassVisitorDTO.name = " + gatePassVisitorDTO.name);
		if (!visitorNames.contains(gatePassVisitorDTO.name)) {
			optionDTOList.add(new OptionDTO(gatePassVisitorDTO.name, gatePassVisitorDTO.name, gatePassVisitorDTO.name));
		}

		List<GatePassPersonModel> personList = getGatePassPersonModelsByGatePassId(gatePassId);
		if (personList != null && !personList.isEmpty()) {
			optionDTOList.addAll(personList.stream().filter(dto -> !visitorNames.contains(dto.name))
					.map(dto -> new OptionDTO(dto.name, dto.name, dto.name))
					.collect(Collectors.toList()));
		}
		
		/*for(OptionDTO optionDTO: optionDTOList)
		{
			logger.debug("##optionDTO = " + optionDTO.englishText);
		}*/

		return Utils.buildOptions(optionDTOList, language, null);
	}
				
}
	