package gate_pass_affiliated_person;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Gate_pass_affiliated_personRepository implements Repository {
	Gate_pass_affiliated_personDAO gate_pass_affiliated_personDAO = null;
	
	public void setDAO(Gate_pass_affiliated_personDAO gate_pass_affiliated_personDAO)
	{
		this.gate_pass_affiliated_personDAO = gate_pass_affiliated_personDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Gate_pass_affiliated_personRepository.class);
	Map<Long, Gate_pass_affiliated_personDTO>mapOfGate_pass_affiliated_personDTOToid;
	Map<Long, Set<Gate_pass_affiliated_personDTO> >mapOfGate_pass_affiliated_personDTOTogatePassId;
	Map<Integer, Set<Gate_pass_affiliated_personDTO> >mapOfGate_pass_affiliated_personDTOTocredentialType;
	Map<String, Set<Gate_pass_affiliated_personDTO> >mapOfGate_pass_affiliated_personDTOTocredentialNo;
	Map<String, Set<Gate_pass_affiliated_personDTO> >mapOfGate_pass_affiliated_personDTOTomobileNumber;
	Map<Long, Set<Gate_pass_affiliated_personDTO> >mapOfGate_pass_affiliated_personDTOToinsertionDate;
	Map<String, Set<Gate_pass_affiliated_personDTO> >mapOfGate_pass_affiliated_personDTOToinsertedBy;
	Map<String, Set<Gate_pass_affiliated_personDTO> >mapOfGate_pass_affiliated_personDTOTomodifiedBy;
	Map<Long, Set<Gate_pass_affiliated_personDTO> >mapOfGate_pass_affiliated_personDTOTolastModificationTime;


	static Gate_pass_affiliated_personRepository instance = null;  
	private Gate_pass_affiliated_personRepository(){
		mapOfGate_pass_affiliated_personDTOToid = new ConcurrentHashMap<>();
		mapOfGate_pass_affiliated_personDTOTogatePassId = new ConcurrentHashMap<>();
		mapOfGate_pass_affiliated_personDTOTocredentialType = new ConcurrentHashMap<>();
		mapOfGate_pass_affiliated_personDTOTocredentialNo = new ConcurrentHashMap<>();
		mapOfGate_pass_affiliated_personDTOTomobileNumber = new ConcurrentHashMap<>();
		mapOfGate_pass_affiliated_personDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfGate_pass_affiliated_personDTOToinsertedBy = new ConcurrentHashMap<>();
		mapOfGate_pass_affiliated_personDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfGate_pass_affiliated_personDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Gate_pass_affiliated_personRepository getInstance(){
		if (instance == null){
			instance = new Gate_pass_affiliated_personRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(gate_pass_affiliated_personDAO == null)
		{
			return;
		}
		try {
			List<Gate_pass_affiliated_personDTO> gate_pass_affiliated_personDTOs = gate_pass_affiliated_personDAO.getAllGate_pass_affiliated_person(reloadAll);
			for(Gate_pass_affiliated_personDTO gate_pass_affiliated_personDTO : gate_pass_affiliated_personDTOs) {
				Gate_pass_affiliated_personDTO oldGate_pass_affiliated_personDTO = getGate_pass_affiliated_personDTOByid(gate_pass_affiliated_personDTO.iD);
				if( oldGate_pass_affiliated_personDTO != null ) {
					mapOfGate_pass_affiliated_personDTOToid.remove(oldGate_pass_affiliated_personDTO.iD);
				
					if(mapOfGate_pass_affiliated_personDTOTogatePassId.containsKey(oldGate_pass_affiliated_personDTO.gatePassId)) {
						mapOfGate_pass_affiliated_personDTOTogatePassId.get(oldGate_pass_affiliated_personDTO.gatePassId).remove(oldGate_pass_affiliated_personDTO);
					}
					if(mapOfGate_pass_affiliated_personDTOTogatePassId.get(oldGate_pass_affiliated_personDTO.gatePassId).isEmpty()) {
						mapOfGate_pass_affiliated_personDTOTogatePassId.remove(oldGate_pass_affiliated_personDTO.gatePassId);
					}
					
					if(mapOfGate_pass_affiliated_personDTOTocredentialType.containsKey(oldGate_pass_affiliated_personDTO.credentialType)) {
						mapOfGate_pass_affiliated_personDTOTocredentialType.get(oldGate_pass_affiliated_personDTO.credentialType).remove(oldGate_pass_affiliated_personDTO);
					}
					if(mapOfGate_pass_affiliated_personDTOTocredentialType.get(oldGate_pass_affiliated_personDTO.credentialType).isEmpty()) {
						mapOfGate_pass_affiliated_personDTOTocredentialType.remove(oldGate_pass_affiliated_personDTO.credentialType);
					}
					
					if(mapOfGate_pass_affiliated_personDTOTocredentialNo.containsKey(oldGate_pass_affiliated_personDTO.credentialNo)) {
						mapOfGate_pass_affiliated_personDTOTocredentialNo.get(oldGate_pass_affiliated_personDTO.credentialNo).remove(oldGate_pass_affiliated_personDTO);
					}
					if(mapOfGate_pass_affiliated_personDTOTocredentialNo.get(oldGate_pass_affiliated_personDTO.credentialNo).isEmpty()) {
						mapOfGate_pass_affiliated_personDTOTocredentialNo.remove(oldGate_pass_affiliated_personDTO.credentialNo);
					}
					
					if(mapOfGate_pass_affiliated_personDTOTomobileNumber.containsKey(oldGate_pass_affiliated_personDTO.mobileNumber)) {
						mapOfGate_pass_affiliated_personDTOTomobileNumber.get(oldGate_pass_affiliated_personDTO.mobileNumber).remove(oldGate_pass_affiliated_personDTO);
					}
					if(mapOfGate_pass_affiliated_personDTOTomobileNumber.get(oldGate_pass_affiliated_personDTO.mobileNumber).isEmpty()) {
						mapOfGate_pass_affiliated_personDTOTomobileNumber.remove(oldGate_pass_affiliated_personDTO.mobileNumber);
					}
					
					if(mapOfGate_pass_affiliated_personDTOToinsertionDate.containsKey(oldGate_pass_affiliated_personDTO.insertionDate)) {
						mapOfGate_pass_affiliated_personDTOToinsertionDate.get(oldGate_pass_affiliated_personDTO.insertionDate).remove(oldGate_pass_affiliated_personDTO);
					}
					if(mapOfGate_pass_affiliated_personDTOToinsertionDate.get(oldGate_pass_affiliated_personDTO.insertionDate).isEmpty()) {
						mapOfGate_pass_affiliated_personDTOToinsertionDate.remove(oldGate_pass_affiliated_personDTO.insertionDate);
					}
					
					if(mapOfGate_pass_affiliated_personDTOToinsertedBy.containsKey(oldGate_pass_affiliated_personDTO.insertedBy)) {
						mapOfGate_pass_affiliated_personDTOToinsertedBy.get(oldGate_pass_affiliated_personDTO.insertedBy).remove(oldGate_pass_affiliated_personDTO);
					}
					if(mapOfGate_pass_affiliated_personDTOToinsertedBy.get(oldGate_pass_affiliated_personDTO.insertedBy).isEmpty()) {
						mapOfGate_pass_affiliated_personDTOToinsertedBy.remove(oldGate_pass_affiliated_personDTO.insertedBy);
					}
					
					if(mapOfGate_pass_affiliated_personDTOTomodifiedBy.containsKey(oldGate_pass_affiliated_personDTO.modifiedBy)) {
						mapOfGate_pass_affiliated_personDTOTomodifiedBy.get(oldGate_pass_affiliated_personDTO.modifiedBy).remove(oldGate_pass_affiliated_personDTO);
					}
					if(mapOfGate_pass_affiliated_personDTOTomodifiedBy.get(oldGate_pass_affiliated_personDTO.modifiedBy).isEmpty()) {
						mapOfGate_pass_affiliated_personDTOTomodifiedBy.remove(oldGate_pass_affiliated_personDTO.modifiedBy);
					}
					
					if(mapOfGate_pass_affiliated_personDTOTolastModificationTime.containsKey(oldGate_pass_affiliated_personDTO.lastModificationTime)) {
						mapOfGate_pass_affiliated_personDTOTolastModificationTime.get(oldGate_pass_affiliated_personDTO.lastModificationTime).remove(oldGate_pass_affiliated_personDTO);
					}
					if(mapOfGate_pass_affiliated_personDTOTolastModificationTime.get(oldGate_pass_affiliated_personDTO.lastModificationTime).isEmpty()) {
						mapOfGate_pass_affiliated_personDTOTolastModificationTime.remove(oldGate_pass_affiliated_personDTO.lastModificationTime);
					}
					
					
				}
				if(gate_pass_affiliated_personDTO.isDeleted == 0) 
				{
					
					mapOfGate_pass_affiliated_personDTOToid.put(gate_pass_affiliated_personDTO.iD, gate_pass_affiliated_personDTO);
				
					if( ! mapOfGate_pass_affiliated_personDTOTogatePassId.containsKey(gate_pass_affiliated_personDTO.gatePassId)) {
						mapOfGate_pass_affiliated_personDTOTogatePassId.put(gate_pass_affiliated_personDTO.gatePassId, new HashSet<>());
					}
					mapOfGate_pass_affiliated_personDTOTogatePassId.get(gate_pass_affiliated_personDTO.gatePassId).add(gate_pass_affiliated_personDTO);
					
					if( ! mapOfGate_pass_affiliated_personDTOTocredentialType.containsKey(gate_pass_affiliated_personDTO.credentialType)) {
						mapOfGate_pass_affiliated_personDTOTocredentialType.put(gate_pass_affiliated_personDTO.credentialType, new HashSet<>());
					}
					mapOfGate_pass_affiliated_personDTOTocredentialType.get(gate_pass_affiliated_personDTO.credentialType).add(gate_pass_affiliated_personDTO);
					
					if( ! mapOfGate_pass_affiliated_personDTOTocredentialNo.containsKey(gate_pass_affiliated_personDTO.credentialNo)) {
						mapOfGate_pass_affiliated_personDTOTocredentialNo.put(gate_pass_affiliated_personDTO.credentialNo, new HashSet<>());
					}
					mapOfGate_pass_affiliated_personDTOTocredentialNo.get(gate_pass_affiliated_personDTO.credentialNo).add(gate_pass_affiliated_personDTO);
					
					if( ! mapOfGate_pass_affiliated_personDTOTomobileNumber.containsKey(gate_pass_affiliated_personDTO.mobileNumber)) {
						mapOfGate_pass_affiliated_personDTOTomobileNumber.put(gate_pass_affiliated_personDTO.mobileNumber, new HashSet<>());
					}
					mapOfGate_pass_affiliated_personDTOTomobileNumber.get(gate_pass_affiliated_personDTO.mobileNumber).add(gate_pass_affiliated_personDTO);
					
					if( ! mapOfGate_pass_affiliated_personDTOToinsertionDate.containsKey(gate_pass_affiliated_personDTO.insertionDate)) {
						mapOfGate_pass_affiliated_personDTOToinsertionDate.put(gate_pass_affiliated_personDTO.insertionDate, new HashSet<>());
					}
					mapOfGate_pass_affiliated_personDTOToinsertionDate.get(gate_pass_affiliated_personDTO.insertionDate).add(gate_pass_affiliated_personDTO);
					
					if( ! mapOfGate_pass_affiliated_personDTOToinsertedBy.containsKey(gate_pass_affiliated_personDTO.insertedBy)) {
						mapOfGate_pass_affiliated_personDTOToinsertedBy.put(gate_pass_affiliated_personDTO.insertedBy, new HashSet<>());
					}
					mapOfGate_pass_affiliated_personDTOToinsertedBy.get(gate_pass_affiliated_personDTO.insertedBy).add(gate_pass_affiliated_personDTO);
					
					if( ! mapOfGate_pass_affiliated_personDTOTomodifiedBy.containsKey(gate_pass_affiliated_personDTO.modifiedBy)) {
						mapOfGate_pass_affiliated_personDTOTomodifiedBy.put(gate_pass_affiliated_personDTO.modifiedBy, new HashSet<>());
					}
					mapOfGate_pass_affiliated_personDTOTomodifiedBy.get(gate_pass_affiliated_personDTO.modifiedBy).add(gate_pass_affiliated_personDTO);
					
					if( ! mapOfGate_pass_affiliated_personDTOTolastModificationTime.containsKey(gate_pass_affiliated_personDTO.lastModificationTime)) {
						mapOfGate_pass_affiliated_personDTOTolastModificationTime.put(gate_pass_affiliated_personDTO.lastModificationTime, new HashSet<>());
					}
					mapOfGate_pass_affiliated_personDTOTolastModificationTime.get(gate_pass_affiliated_personDTO.lastModificationTime).add(gate_pass_affiliated_personDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Gate_pass_affiliated_personDTO> getGate_pass_affiliated_personList() {
		List <Gate_pass_affiliated_personDTO> gate_pass_affiliated_persons = new ArrayList<Gate_pass_affiliated_personDTO>(this.mapOfGate_pass_affiliated_personDTOToid.values());
		return gate_pass_affiliated_persons;
	}
	
	
	public Gate_pass_affiliated_personDTO getGate_pass_affiliated_personDTOByid( long id){
		return mapOfGate_pass_affiliated_personDTOToid.get(id);
	}
	
	
	public List<Gate_pass_affiliated_personDTO> getGate_pass_affiliated_personDTOBygate_pass_id(long gate_pass_id) {
		return new ArrayList<>( mapOfGate_pass_affiliated_personDTOTogatePassId.getOrDefault(gate_pass_id,new HashSet<>()));
	}
	
	
	public List<Gate_pass_affiliated_personDTO> getGate_pass_affiliated_personDTOBycredential_type(int credential_type) {
		return new ArrayList<>( mapOfGate_pass_affiliated_personDTOTocredentialType.getOrDefault(credential_type,new HashSet<>()));
	}
	
	
	public List<Gate_pass_affiliated_personDTO> getGate_pass_affiliated_personDTOBycredential_no(String credential_no) {
		return new ArrayList<>( mapOfGate_pass_affiliated_personDTOTocredentialNo.getOrDefault(credential_no,new HashSet<>()));
	}
	
	
	public List<Gate_pass_affiliated_personDTO> getGate_pass_affiliated_personDTOBymobile_number(String mobile_number) {
		return new ArrayList<>( mapOfGate_pass_affiliated_personDTOTomobileNumber.getOrDefault(mobile_number,new HashSet<>()));
	}
	
	
	public List<Gate_pass_affiliated_personDTO> getGate_pass_affiliated_personDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfGate_pass_affiliated_personDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Gate_pass_affiliated_personDTO> getGate_pass_affiliated_personDTOByinserted_by(String inserted_by) {
		return new ArrayList<>( mapOfGate_pass_affiliated_personDTOToinsertedBy.getOrDefault(inserted_by,new HashSet<>()));
	}
	
	
	public List<Gate_pass_affiliated_personDTO> getGate_pass_affiliated_personDTOBymodified_by(String modified_by) {
		return new ArrayList<>( mapOfGate_pass_affiliated_personDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Gate_pass_affiliated_personDTO> getGate_pass_affiliated_personDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfGate_pass_affiliated_personDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "gate_pass_affiliated_person";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


