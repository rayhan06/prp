package training_calender;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Training_calenderRepository  {
    private static final Logger logger = Logger.getLogger(Training_calenderRepository.class);
    private static final String updateToOnGoingSQL = "UPDATE training_calender SET status = 2,lastModificationTime=%d WHERE start_date<= %d AND end_date> %d AND status = 1";
    private static final String updateToCompletedSQL = "UPDATE training_calender SET status = 3,lastModificationTime= %d WHERE end_date<= %d AND status IN (1,2)";
    static {
        logger.debug("Training_calenderRepository's static method is loaded");
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor(r -> {
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            return thread;
        });

        executorService.scheduleAtFixedRate(() -> ConnectionAndStatementUtil.getWriteStatement(st->{
            try {
                long currentTime = System.currentTimeMillis();
                String sql = String.format(updateToOnGoingSQL,currentTime,currentTime,currentTime);
                logger.debug(sql);
                st.executeUpdate(sql);
                sql = String.format(updateToCompletedSQL,currentTime,currentTime);
                logger.debug(sql);
                st.executeUpdate(sql);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }), 0, 30, TimeUnit.MINUTES);
    }
}