package training_calender;

/*
 * @author Md. Erfan Hossain
 * @created 30/06/2021 - 1:17 PM
 * @project parliament
 */

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum TrainingCalendarStatusEnum {
    UPCOMING(1,"gray"),
    ONGOING(2,"forestgreen"),
    COMPLETED(3,"crimson");

    private final int value;
    private final String color;

    TrainingCalendarStatusEnum(int value,String color){
        this.value = value;
        this.color = color;
    }

    private static Map<Integer,TrainingCalendarStatusEnum> mapByValue = null;

    private static synchronized void reload(){
        if(mapByValue == null){
            mapByValue = Stream.of(TrainingCalendarStatusEnum.values())
                               .collect(Collectors.toMap(e->e.value,e->e));
        }
    }

    public static TrainingCalendarStatusEnum getByValue(int value){
        if(mapByValue == null){
            reload();
        }
        return mapByValue.get(value);
    }

    public int getValue(){
        return value;
    }

    public String getColor() {
        return color;
    }
}