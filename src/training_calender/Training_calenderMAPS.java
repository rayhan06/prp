package training_calender;

import java.util.*;

import util.*;


public class Training_calenderMAPS extends CommonMaps {
    public Training_calenderMAPS(String tableName) {

        java_allfield_type_map.put("name_en".toLowerCase(), "String");
        java_allfield_type_map.put("name_bn".toLowerCase(), "String");
        java_allfield_type_map.put("training_type_cat".toLowerCase(), "Integer");
        java_allfield_type_map.put("country".toLowerCase(), "Long");
        java_allfield_type_map.put("institute_name".toLowerCase(), "String");
        java_allfield_type_map.put("start_date".toLowerCase(), "Long");
        java_allfield_type_map.put("end_date".toLowerCase(), "Long");
        java_allfield_type_map.put("start_time".toLowerCase(), "String");
        java_allfield_type_map.put("end_time".toLowerCase(), "String");
        java_allfield_type_map.put("training_arrange_by_cat".toLowerCase(), "String");
        java_allfield_type_map.put("training_mode_cat".toLowerCase(), "Integer");
        java_allfield_type_map.put("venue".toLowerCase(), "String");
        java_allfield_type_map.put("max_number_of_participants".toLowerCase(), "Integer");
        java_allfield_type_map.put("is_enrollment_applicale".toLowerCase(), "Boolean");
        java_allfield_type_map.put("last_enrollment_date".toLowerCase(), "Long");
        java_allfield_type_map.put("target_group_type".toLowerCase(), "String");
        java_allfield_type_map.put("training_objective".toLowerCase(), "String");
        java_allfield_type_map.put("training_prerequisite".toLowerCase(), "String");
        java_allfield_type_map.put("is_bond_applicable".toLowerCase(), "Boolean");
        java_allfield_type_map.put("year_of_bond".toLowerCase(), "String");
        java_allfield_type_map.put("insertion_date".toLowerCase(), "Long");
        java_allfield_type_map.put("inserted_by_user_id".toLowerCase(), "Long");
        java_allfield_type_map.put("modified_by_id".toLowerCase(), "String");

        java_anyfield_search_map.put("language_text.languageTextEnglish", "String");
        java_anyfield_search_map.put("language_text.languageTextBangla", "String");

        java_anyfield_search_map.put(tableName + ".name_en".toLowerCase(), "String");
        java_anyfield_search_map.put(tableName + ".name_bn".toLowerCase(), "String");
        java_anyfield_search_map.put(tableName + ".institute_name".toLowerCase(), "String");
        java_anyfield_search_map.put(tableName + ".start_date".toLowerCase(), "Long");
        java_anyfield_search_map.put(tableName + ".end_date".toLowerCase(), "Long");
        java_anyfield_search_map.put(tableName + ".start_time".toLowerCase(), "String");
        java_anyfield_search_map.put(tableName + ".end_time".toLowerCase(), "String");
        java_anyfield_search_map.put(tableName + ".training_arrange_by_cat".toLowerCase(), "String");
        java_anyfield_search_map.put(tableName + ".is_foreign_training".toLowerCase(), "Boolean");
        java_anyfield_search_map.put(tableName + ".venue".toLowerCase(), "String");
        java_anyfield_search_map.put(tableName + ".max_number_of_participants".toLowerCase(), "Integer");
        java_anyfield_search_map.put(tableName + ".is_enrollment_applicale".toLowerCase(), "Boolean");
        java_anyfield_search_map.put(tableName + ".last_enrollment_date".toLowerCase(), "Long");
        java_anyfield_search_map.put("office_unit_organograms.name_en".toLowerCase(), "String");
        java_anyfield_search_map.put("office_unit_organograms.name_bn".toLowerCase(), "String");
        java_anyfield_search_map.put(tableName + ".training_objective".toLowerCase(), "String");
        java_anyfield_search_map.put(tableName + ".training_prerequisite".toLowerCase(), "String");
        java_anyfield_search_map.put(tableName + ".is_bond_applicable".toLowerCase(), "Boolean");
        java_anyfield_search_map.put(tableName + ".year_of_bond".toLowerCase(), "String");
        java_anyfield_search_map.put(tableName + ".insertion_date".toLowerCase(), "Long");
        java_anyfield_search_map.put(tableName + ".inserted_by_user_id".toLowerCase(), "Long");
        java_anyfield_search_map.put(tableName + ".modified_by_id".toLowerCase(), "String");

        java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
        java_DTO_map.put("nameEn".toLowerCase(), "nameEn".toLowerCase());
        java_DTO_map.put("nameBn".toLowerCase(), "nameBn".toLowerCase());
        java_DTO_map.put("trainingTypeCat".toLowerCase(), "trainingTypeCat".toLowerCase());
        java_DTO_map.put("instituteName".toLowerCase(), "instituteName".toLowerCase());
        java_DTO_map.put("startDate".toLowerCase(), "startDate".toLowerCase());
        java_DTO_map.put("endDate".toLowerCase(), "endDate".toLowerCase());
        java_DTO_map.put("startTime".toLowerCase(), "startTime".toLowerCase());
        java_DTO_map.put("endTime".toLowerCase(), "endTime".toLowerCase());
        java_DTO_map.put("arrangedBy".toLowerCase(), "arrangedBy".toLowerCase());
        java_DTO_map.put("trainingModeCat".toLowerCase(), "trainingModeCat".toLowerCase());
        java_DTO_map.put("isForeignTraining".toLowerCase(), "isForeignTraining".toLowerCase());
        java_DTO_map.put("venue".toLowerCase(), "venue".toLowerCase());
        java_DTO_map.put("maxNumberOfParticipants".toLowerCase(), "maxNumberOfParticipants".toLowerCase());
        java_DTO_map.put("isEnrollmentApplicale".toLowerCase(), "isEnrollmentApplicale".toLowerCase());
        java_DTO_map.put("lastEnrollmentDate".toLowerCase(), "lastEnrollmentDate".toLowerCase());
        java_DTO_map.put("targetGroupType".toLowerCase(), "targetGroupType".toLowerCase());
        java_DTO_map.put("trainingObjective".toLowerCase(), "trainingObjective".toLowerCase());
        java_DTO_map.put("trainingPrerequisite".toLowerCase(), "trainingPrerequisite".toLowerCase());
        java_DTO_map.put("trainingMaterialsDropzone".toLowerCase(), "trainingMaterialsDropzone".toLowerCase());
        java_DTO_map.put("isBondApplicable".toLowerCase(), "isBondApplicable".toLowerCase());
        java_DTO_map.put("yearOfBond".toLowerCase(), "yearOfBond".toLowerCase());
        java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
        java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
        java_DTO_map.put("modifiedById".toLowerCase(), "modifiedById".toLowerCase());
        java_DTO_map.put("jobCat".toLowerCase(), "jobCat".toLowerCase());
        java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

        java_SQL_map.put("name_en".toLowerCase(), "nameEn".toLowerCase());
        java_SQL_map.put("name_bn".toLowerCase(), "nameBn".toLowerCase());
        java_SQL_map.put("training_type_cat".toLowerCase(), "trainingTypeCat".toLowerCase());
        java_SQL_map.put("institute_name".toLowerCase(), "instituteName".toLowerCase());
        java_SQL_map.put("start_date".toLowerCase(), "startDate".toLowerCase());
        java_SQL_map.put("end_date".toLowerCase(), "endDate".toLowerCase());
        java_SQL_map.put("start_time".toLowerCase(), "startTime".toLowerCase());
        java_SQL_map.put("end_time".toLowerCase(), "endTime".toLowerCase());
        java_SQL_map.put("training_arrange_by_cat".toLowerCase(), "trainingArrangedByCat".toLowerCase());
        java_SQL_map.put("training_mode_cat".toLowerCase(), "trainingModeCat".toLowerCase());
        java_SQL_map.put("is_foreign_training".toLowerCase(), "isForeignTraining".toLowerCase());
        java_SQL_map.put("venue".toLowerCase(), "venue".toLowerCase());
        java_SQL_map.put("max_number_of_participants".toLowerCase(), "maxNumberOfParticipants".toLowerCase());
        java_SQL_map.put("is_enrollment_applicale".toLowerCase(), "isEnrollmentApplicale".toLowerCase());
        java_SQL_map.put("last_enrollment_date".toLowerCase(), "lastEnrollmentDate".toLowerCase());
        java_SQL_map.put("target_group_type".toLowerCase(), "targetGroupType".toLowerCase());
        java_SQL_map.put("training_objective".toLowerCase(), "trainingObjective".toLowerCase());
        java_SQL_map.put("training_prerequisite".toLowerCase(), "trainingPrerequisite".toLowerCase());
        java_SQL_map.put("training_materials_dropzone".toLowerCase(), "trainingMaterialsDropzone".toLowerCase());
        java_SQL_map.put("is_bond_applicable".toLowerCase(), "isBondApplicable".toLowerCase());
        java_SQL_map.put("year_of_bond".toLowerCase(), "yearOfBond".toLowerCase());
        java_SQL_map.put("insertion_date".toLowerCase(), "insertionDate".toLowerCase());
        java_SQL_map.put("inserted_by_user_id".toLowerCase(), "insertedByUserId".toLowerCase());
        java_SQL_map.put("modified_by_id".toLowerCase(), "modifiedById".toLowerCase());

        java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
        java_Text_map.put("English Name".toLowerCase(), "nameEn".toLowerCase());
        java_Text_map.put("Bangla Name".toLowerCase(), "nameBn".toLowerCase());
        java_Text_map.put("Training Type Cat".toLowerCase(), "trainingTypeCat".toLowerCase());
        java_Text_map.put("Institute Name".toLowerCase(), "instituteName".toLowerCase());
        java_Text_map.put("Start Date".toLowerCase(), "startDate".toLowerCase());
        java_Text_map.put("End Date".toLowerCase(), "endDate".toLowerCase());
        java_Text_map.put("Start Time".toLowerCase(), "startTime".toLowerCase());
        java_Text_map.put("End Time".toLowerCase(), "endTime".toLowerCase());
        java_Text_map.put("Arranged By".toLowerCase(), "arrangedBy".toLowerCase());
        java_Text_map.put("Training Mode Cat".toLowerCase(), "trainingModeCat".toLowerCase());
        java_Text_map.put("Is Foreign Training".toLowerCase(), "isForeignTraining".toLowerCase());
        java_Text_map.put("Venue".toLowerCase(), "venue".toLowerCase());
        java_Text_map.put("Max Number Of Participants".toLowerCase(), "maxNumberOfParticipants".toLowerCase());
        java_Text_map.put("Is Enrollment Applicale".toLowerCase(), "isEnrollmentApplicale".toLowerCase());
        java_Text_map.put("Last Enrollment Date".toLowerCase(), "lastEnrollmentDate".toLowerCase());
        java_Text_map.put("Target Group".toLowerCase(), "targetGroupType".toLowerCase());
        java_Text_map.put("Training Objective".toLowerCase(), "trainingObjective".toLowerCase());
        java_Text_map.put("Training Prerequisite".toLowerCase(), "trainingPrerequisite".toLowerCase());
        java_Text_map.put("Training Materials Dropzone".toLowerCase(), "trainingMaterialsDropzone".toLowerCase());
        java_Text_map.put("Is Bond Applicable".toLowerCase(), "isBondApplicable".toLowerCase());
        java_Text_map.put("Year Of Bond".toLowerCase(), "yearOfBond".toLowerCase());
        java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
        java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
        java_Text_map.put("Modified By Id".toLowerCase(), "modifiedById".toLowerCase());
        java_Text_map.put("Job Cat".toLowerCase(), "jobCat".toLowerCase());
        java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

    }

}