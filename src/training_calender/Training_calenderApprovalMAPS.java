package training_calender;

import util.*;

public class Training_calenderApprovalMAPS extends CommonMaps {
    public Training_calenderApprovalMAPS(String tableName) {

        java_allfield_type_map.put("name_en".toLowerCase(), "String");
        java_allfield_type_map.put("name_bn".toLowerCase(), "String");
        java_allfield_type_map.put("training_type_cat".toLowerCase(), "Integer");
        java_allfield_type_map.put("institute_name".toLowerCase(), "String");
        java_allfield_type_map.put("start_date".toLowerCase(), "Long");
        java_allfield_type_map.put("end_date".toLowerCase(), "Long");
        java_allfield_type_map.put("start_time".toLowerCase(), "String");
        java_allfield_type_map.put("end_time".toLowerCase(), "String");
        java_allfield_type_map.put("training_arrange_by_cat".toLowerCase(), "String");
        java_allfield_type_map.put("training_mode_cat".toLowerCase(), "Integer");
        java_allfield_type_map.put("venue".toLowerCase(), "String");
        java_allfield_type_map.put("max_number_of_participants".toLowerCase(), "Integer");
        java_allfield_type_map.put("is_enrollment_applicale".toLowerCase(), "Boolean");
        java_allfield_type_map.put("last_enrollment_date".toLowerCase(), "Long");
        java_allfield_type_map.put("target_group_type".toLowerCase(), "String");
        java_allfield_type_map.put("training_objective".toLowerCase(), "String");
        java_allfield_type_map.put("training_prerequisite".toLowerCase(), "String");
        java_allfield_type_map.put("is_bond_applicable".toLowerCase(), "Boolean");
        java_allfield_type_map.put("year_of_bond".toLowerCase(), "String");
        java_allfield_type_map.put("insertion_date".toLowerCase(), "Long");
        java_allfield_type_map.put("inserted_by_user_id".toLowerCase(), "Long");
        java_allfield_type_map.put("modified_by_id".toLowerCase(), "String");

        java_allfield_type_map.put("job_cat", "Integer");
        java_allfield_type_map.put("approval_status_cat", "Integer");
        java_allfield_type_map.put("initiator", "Long");
        java_allfield_type_map.put("assigned_to", "Long");
        java_allfield_type_map.put("starting_date", "Long");
        java_allfield_type_map.put("ending_date", "Long");

        java_table_map.put("approval_status_cat", "approval_summary");
        java_table_map.put("initiator", "approval_summary");
        java_table_map.put("assigned_to", "approval_summary");
    }

}