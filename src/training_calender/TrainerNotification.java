package training_calender;

import pb_notifications.Pb_notificationsDAO;
import util.StringUtils;

import java.util.List;

public class TrainerNotification {
    private final Pb_notificationsDAO pb_notificationsDAO;

    private TrainerNotification() {
        pb_notificationsDAO = Pb_notificationsDAO.getInstance();
    }

    private static class TrainerNotificationLazyLoader {
        static final TrainerNotification INSTANCE = new TrainerNotification();
    }

    public static TrainerNotification getInstance() {
        return TrainerNotification.TrainerNotificationLazyLoader.INSTANCE;
    }

    public void sendTrainerSelectionNotification(List<Long> organogramIds, Training_calenderDTO training_calenderDTO) {
        String notificationEngText = "You Have been Selected as Trainer in Training: " + training_calenderDTO.nameEn + " Starting from " +
                StringUtils.getFormattedDate("ENGLISH", training_calenderDTO.startDate);
        String notificationBngText = "আপনি নির্বাচিত হয়েছেন প্রশিক্ষক হিসেবে প্রশিক্ষণঃ " + training_calenderDTO.nameBn + " এ যা শুরু হবে " +
                StringUtils.getFormattedDate("BANGLA", training_calenderDTO.startDate) + " তারিখে";
        String notificationMessage = notificationEngText + "$" + notificationBngText;
        String url = "Training_calenderServlet?actionType=view&ID=" + training_calenderDTO.iD;
        sendNotification(organogramIds, notificationMessage, url);
    }

    public void sendTrainerEjectNotification(List<Long> organogramIds, Training_calenderDTO training_calenderDTO) {
        String notificationEngText = "You Have Been Removed as Trainer in Training: " + training_calenderDTO.nameEn + " Starting from " +
                StringUtils.getFormattedDate("ENGLISH", training_calenderDTO.startDate);
        String notificationBngText = "আপনি বাদ পড়েছেন প্রশিক্ষক হিসেবে প্রশিক্ষণঃ " + training_calenderDTO.nameBn + " হতে যা শুরু হবে " +
                StringUtils.getFormattedDate("BANGLA", training_calenderDTO.startDate) + " তারিখে";
        String notificationMessage = notificationEngText + "$" + notificationBngText;
        String url = "Training_calenderServlet?actionType=view&ID=" + training_calenderDTO.iD;
        sendNotification(organogramIds, notificationMessage, url);
    }

    private void sendNotification(List<Long> organogramIds, String notificationMessage, String url) {
        if (organogramIds == null || organogramIds.size() == 0) {
            return;
        }
        Thread thread = new Thread(() -> {
            long currentTime = System.currentTimeMillis();
            organogramIds.forEach(id -> pb_notificationsDAO.addPb_notifications(id, currentTime, url, notificationMessage, false));
        });
        thread.setDaemon(true);
        thread.start();
    }
}
