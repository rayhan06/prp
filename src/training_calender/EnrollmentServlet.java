package training_calender;

import com.google.gson.Gson;
import common.BaseServlet;
import common.CommonDAOService;
import login.LoginDTO;
import org.apache.log4j.Logger;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.RecordNavigator;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/EnrollmentServlet")
@MultipartConfig
public class EnrollmentServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(EnrollmentServlet.class);

    private final Training_calenderDAO training_calenderDAO = Training_calenderDAO.getInstance();


    @Override
    public String getTableName() {
        return "training_calender";
    }

    @Override
    public String getServletName() {
        return "EnrollmentServlet";
    }

    @Override
    public Training_calenderDAO getCommonDAOService() {
        return training_calenderDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return EnrollmentServlet.class;
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "searchEnrolmentHistory":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TRAINING_CALENDER_ENROLMENT_HISTORY)) {
                        Map<String, String> extraCriteriaMap;
                        if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) == null) {
                            extraCriteriaMap = new HashMap<>();
                        } else {
                            extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
                        }

                        extraCriteriaMap.put("trainee", String.valueOf(userDTO.employee_record_id));
                        request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                        searchEnrolmentHistory(request, response, userDTO);
                        return;
                    }
                case "searchEnrolment":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TRAINING_CALENDER_ENROLMENT)) {
                        Map<String, String> extraCriteriaMap;
                        if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) == null) {
                            extraCriteriaMap = new HashMap<>();
                        } else {
                            extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
                        }
                        String curTime = String.valueOf(Calendar.getInstance().getTimeInMillis());


                        extraCriteriaMap.put("is_enrollment_applicale", String.valueOf(1));
                        extraCriteriaMap.put("last_enrollment_date", curTime);
                        extraCriteriaMap.put("is_private ", String.valueOf(0));
                        request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                        searchEnrolment(request, response, userDTO);
                        return;

                    }
                case "getChart":
                    long employeeRecordId = Long.parseLong(request.getParameter("employeeRecordId"));
                    Map<String, Integer> result = Training_calenderDAO.getInstance().getEnrolmentHistoryChartData(language, employeeRecordId);
                    String data = new Gson().toJson(result);
                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();
                    out.print(data);
                    out.flush();
                    out.close();
                    return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }


    private void searchEnrolmentHistory(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception {
        request.setAttribute("formName", "../training_calender/training_calenderEnrolmentHistoryForm.jsp");
        request.setAttribute("navName", "../training_calender/training_calenderEnrolmentHistoryNav.jsp");
        request.setAttribute("servletName", "EnrollmentServlet");

        Map<String, String> params = HttpRequestUtils.buildRequestParams(request);
        if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) != null) {
            Map<String, String> extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
            extraCriteriaMap.forEach(params::put);
        }
        String joinClause = "  join training_calendar_details on training_calendar_details.training_calendar_id = training_calender.ID ";
        String whereClause = " AND training_calendar_details.isDeleted = 0 ";

        RecordNavigator recordNavigator = getCommonDAOService().getRecordNavigator(params, joinClause, whereClause, "");
        request.setAttribute(RECORD_NAVIGATOR, recordNavigator);
        String ajax = request.getParameter("ajax");
        String url;
        if (ajax != null && ajax.equalsIgnoreCase("true")) {
            url = "training_calender/training_calenderEnrolmentHistoryForm.jsp";
        } else {
            url = defaultNonAjaxSearchPageURL();
        }
        logger.debug("url : " + url);
        request.getRequestDispatcher(url).forward(request, response);
    }

    void searchEnrolment(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception {
        request.setAttribute("formName", "../training_calender/training_calenderSearchEnrolmentForm.jsp");
        request.setAttribute("navName", "../training_calender/training_calenderSearchEnrolmentNav.jsp");
        request.setAttribute("servletName", "EnrollmentServlet");

        Map<String, String> params = HttpRequestUtils.buildRequestParams(request);
        if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) != null) {
            Map<String, String> extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
            extraCriteriaMap.forEach(params::put);
        }

        String whereClause = " AND training_calender.ID NOT IN(SELECT training_calendar_details.training_calendar_id" +
                " from training_calendar_details" +
                " where training_calendar_details.employee_records_id =" + userDTO.employee_record_id +
                " and training_calendar_details.isDeleted = 0) ";


        RecordNavigator recordNavigator = getCommonDAOService().getRecordNavigator(params, getTableJoinClause(request), whereClause, "");
        request.setAttribute(RECORD_NAVIGATOR, recordNavigator);
        String ajax = request.getParameter("ajax");
        String url;
        if (ajax != null && ajax.equalsIgnoreCase("true")) {
            url = "training_calender/training_calenderSearchEnrolmentForm.jsp";
        } else {
            url = defaultNonAjaxSearchPageURL();
        }
        logger.debug("url : " + url);
        request.getRequestDispatcher(url).forward(request, response);
    }
}
