package training_calender;

public class TrainingCalendarModel {
    public Training_calenderDTO trainingCalenderDTO;
    public String trainingTypeEng;
    public String trainingTypeBan;
    public String trainingModeEng;
    public String trainingModeBan;
    public String startDateEng;
    public String startDateBan;
    public String endDateEng;
    public String endDateBan;

    @Override
    public String toString() {
        return "TrainingCalendarModel{" +
                "trainingCalenderDTO= " + trainingCalenderDTO.toString() +
                ", trainingTypeEng='" + trainingTypeEng + '\'' +
                ", trainingTypeBan='" + trainingTypeBan + '\'' +
                ", trainingModeEng='" + trainingModeEng + '\'' +
                ", trainingModeBan='" + trainingModeBan + '\'' +
                ", startDateEng='" + startDateEng + '\'' +
                ", startDateBan='" + startDateBan + '\'' +
                ", endDateEng='" + endDateEng + '\'' +
                ", endDateBan='" + endDateBan + '\'' +
                '}';
    }
}
