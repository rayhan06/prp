package training_calender;

import com.google.gson.Gson;
import common.ApiResponse;
import common.BaseServlet;
import common.CommonDAOService;
import common.RoleEnum;
import election_wise_mp.Election_wise_mpDAO;
import election_wise_mp.Election_wise_mpTrainingReportModel;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.EmpOfficeModel;
import files.FilesDAO;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import training_calendar_details.TrainerDAO;
import training_calendar_details.TrainerDTO;
import training_calendar_details.Training_calendar_detailsDAO;
import training_calendar_details.Training_calendar_detailsDTO;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

@WebServlet("/Training_calenderServlet")
@MultipartConfig
public class Training_calenderServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Training_calenderServlet.class);

    private final Training_calenderDAO training_calenderDAO = Training_calenderDAO.getInstance();
    private final FilesDAO filesDAO = new FilesDAO();
    private final Gson gson = new Gson();


    private boolean isValid(HttpServletRequest request) {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        return userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId() || userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.TRAINING.getRoleId()
                || (request.getParameter("empId") != null && Long.parseLong(request.getParameter("empId")) == userDTO.employee_record_id);
    }

    
    @Override
    public String getTableName() {
        return "training_calender";
    }

    @Override
    public String getServletName() {
        return "Training_calenderServlet";
    }

    @Override
    public Training_calenderDAO getCommonDAOService() {
        return training_calenderDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = userDTO.languageID == CommonConstant.Language_ID_English;
        boolean isAdmin = userDTO.roleID == RoleEnum.ADMIN.getRoleId() 
        		|| userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId()
        		|| userDTO.roleID == RoleEnum.TRAINING.getRoleId();
        long empId = Long.parseLong(Utils.doJsoupCleanOrReturnEmpty(request.getParameter("empId")));

        if (!isAdmin && userDTO.employee_record_id != empId) {
            throw new Exception(isLangEng ? "You are only allowed to change your own records!"
                    : 	"আপনি শুধুমাত্র আপনার নিজের তথ্য পরিবর্তন করতে পারবেন!");
        }

        Training_calenderDTO training_calenderDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        final long requestTime = Calendar.getInstance().getTimeInMillis();

        if (addFlag) {
            training_calenderDTO = new Training_calenderDTO();
            training_calenderDTO.insertionDate = requestTime;
            training_calenderDTO.insertedByUserId = userDTO.ID;
        } else {
            training_calenderDTO = training_calenderDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));

            if (!isAdmin && userDTO.employee_record_id != training_calenderDTO.privateEmpId) {
                throw new Exception(isLangEng ? "You are only allowed to change your own records!"
                        : 	"আপনি শুধুমাত্র আপনার নিজের তথ্য পরিবর্তন করতে পারবেন!");
            }

            //KONA requested to OPEN this logic
            /*if (!training_calenderDTO.isPrivate && training_calenderDTO.status == TrainingCalendarStatusEnum.COMPLETED.getValue()) {
                throw new Exception(isLangEng ? "Can't change training calendar because training was completed"
                        : "প্রশিক্ষন সম্পন্ন হয়ে গেছে, তাই প্রশিক্ষন ক্যালেন্ডার পরিবর্তন করা যাবে না");
            }*/
        }
        training_calenderDTO.lastModificationTime = requestTime;
        training_calenderDTO.modifiedById = String.valueOf(userDTO.ID);
        training_calenderDTO.privateEmpId = empId;

        String Value;
        training_calenderDTO.nameEn = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("nameEn"));
        training_calenderDTO.nameBn = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("nameBn"));
        training_calenderDTO.trainingCategoryCat = Integer.parseInt(request.getParameter("trainingCategoryCat"));
        training_calenderDTO.trainingTypeCat = Integer.parseInt(request.getParameter("trainingTypeCat"));
        training_calenderDTO.trainingModeCat = Integer.parseInt(request.getParameter("trainingModeCat"));
        training_calenderDTO.isPrivate = empId != 0;
        if (request.getParameter("instituteName") != null) {
            training_calenderDTO.instituteName = Jsoup.clean(request.getParameter("instituteName"), Whitelist.simpleText());
        } else {
            training_calenderDTO.instituteName = "";
        }
        training_calenderDTO.venue = request.getParameter("venue");

        String startDate = request.getParameter("startDate");
        String startTime = request.getParameter("startTime");
        if (!startDate.equalsIgnoreCase("")) {

            if (startTime == null || startTime.trim().length() == 0) {
                startTime = "00:00:00";
            }
            startDate += " " + startTime;
            training_calenderDTO.startDate = f.parse(startDate).getTime();
        } else if (!training_calenderDTO.isPrivate) {
            training_calenderDTO.startDate = SessionConstants.MIN_DATE;
            throw new Exception(
                    isLangEng ? "Start date not found"
                            : 	"শুরুর তারিখ পাওয়া যায় নি"
            );
        } else {
            training_calenderDTO.startDate = SessionConstants.MIN_DATE;
        }


        String endDate = request.getParameter("endDate");
        String endTime = request.getParameter("endTime");
        if (!endDate.equalsIgnoreCase("")) {
            if (endTime == null || endTime.trim().length() == 0) {
                endTime = "23:55:59";
            }
            endDate += " " + endTime;
            training_calenderDTO.endDate = f.parse(endDate).getTime();
        } else if (!training_calenderDTO.isPrivate) {
            training_calenderDTO.endDate = SessionConstants.MIN_DATE;
            throw new Exception(
                    isLangEng ? "End date not found"
                            : 	"শেষের তারিখ পাওয়া যায় নি"
            );
        } else {
            training_calenderDTO.endDate = SessionConstants.MIN_DATE;
        }
        training_calenderDTO.startTime = startTime;
        training_calenderDTO.endTime = endTime;


        if (training_calenderDTO.startDate >= System.currentTimeMillis()) {
            training_calenderDTO.status = TrainingCalendarStatusEnum.UPCOMING.getValue();
        } else if (training_calenderDTO.endDate <= System.currentTimeMillis()) {
            training_calenderDTO.status = TrainingCalendarStatusEnum.COMPLETED.getValue();
        } else {
            training_calenderDTO.status = TrainingCalendarStatusEnum.ONGOING.getValue();
        }
        if (!training_calenderDTO.isPrivate) {
            if (addFlag || training_calenderDTO.status == TrainingCalendarStatusEnum.UPCOMING.getValue()) {

                //New requirement : To create training calendar on older date
                /*long currentTimeInMilliSecond = new Date().getTime();
                if (currentTimeInMilliSecond > training_calenderDTO.startDate) {
                    throw new Exception(
                            isLangEng ? "Training start time can not be greater than current time"
                                    : "প্রশিক্ষন শুরু হওয়ার সময়, বর্তমান সময় থেকে বড় হতে পারে না"
                    );
                }*/
                training_calenderDTO.status = TrainingCalendarStatusEnum.UPCOMING.getValue();
            } else {
                long newStartTime = f.parse(startDate).getTime();
                if (!training_calenderDTO.isPrivate && newStartTime != training_calenderDTO.startDate) {
                    throw new Exception(
                            isLangEng ? "Can't change training start time during training running period"
                                    : 	"প্রশিক্ষন চলাকালীন, প্রশিক্ষন শুরুর সময় পরিবর্তন করা যাবে না"
                    );
                }
            }
            if (training_calenderDTO.startDate >= training_calenderDTO.endDate)
                throw new Exception(
                        isLangEng ? "End date can not be greater than to start date"
                                : 	"শেষ হওয়ার তারিখ, শুরু হওয়ার তারিখ থেকে বড় হতে পারে না"
                );
            if (training_calenderDTO.endDate <= System.currentTimeMillis()) {
                training_calenderDTO.status = TrainingCalendarStatusEnum.COMPLETED.getValue();
            }
            // training_calenderDTO.trainerCat = Integer.parseInt(request.getParameter("trainerCat"));
            training_calenderDTO.trainerCat = 3; // Internal & External
            training_calenderDTO.externalTrainerInfo = Utils.cleanAndGetOptionalString(request.getParameter("extTrainerInfo"));

            if(request.getParameter("arrangedBy")!=null && request.getParameter("arrangedBy").trim().length()>0){
                training_calenderDTO.arrangedBy = Integer.parseInt(request.getParameter("arrangedBy"));
            }
            training_calenderDTO.onlineTrainingDetails = request.getParameter("onlineTrainingDetails");
            training_calenderDTO.trainingCost = request.getParameter("trainingCost");

            training_calenderDTO.targetGroupType = request.getParameter("targetGroupType");

            Value = request.getParameter("maxNumberOfParticipants");

            if (Value != null) {
                training_calenderDTO.maxNumberOfParticipants = Integer.parseInt(Value.trim());
            } else {
                training_calenderDTO.maxNumberOfParticipants = 0;
            }

            Value = request.getParameter("isEnrollmentApplicale");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            training_calenderDTO.isEnrollmentApplicale = Value != null && !Value.equalsIgnoreCase("");


            Value = request.getParameter("lastEnrollmentDate");
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    training_calenderDTO.lastEnrollmentDate = new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("lastEnrollmentDate")).getTime();
                } catch (Exception e) {
                    training_calenderDTO.lastEnrollmentDate = SessionConstants.MIN_DATE;
                }
            } else {
                training_calenderDTO.lastEnrollmentDate = SessionConstants.MIN_DATE;
            }
            training_calenderDTO.lastEnrollmentDate = applyTimeChage(training_calenderDTO.lastEnrollmentDate,
                    training_calenderDTO.startDate, training_calenderDTO.startTime);

            /* 20/12/2021: Client Told To Remove: trainingPrerequisite, trainingObjective
            training_calenderDTO.trainingPrerequisite = request.getParameter("trainingPrerequisite");
            Value = request.getParameter("trainingObjective");
            if (Value != null) {
                Value = Value
                        .replaceAll("<strong>", "<b>")
                        .replaceAll("</strong>", "</b>")
                        .replaceAll("<em>", "<i>")
                        .replaceAll("</em>", "</i>")
                        .replaceAll("<s>", "<strike>")
                        .replaceAll("</s>", "</strike>");
                training_calenderDTO.trainingObjective = Value;
            } else {
                training_calenderDTO.trainingObjective = "";
            }
            */

            Value = request.getParameter("isBondApplicable");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            training_calenderDTO.isBondApplicable = Value != null && !Value.equalsIgnoreCase("");

            Value = request.getParameter("yearOfBond");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                training_calenderDTO.yearOfBond = Value;
            } else {
                training_calenderDTO.yearOfBond = "";
            }
        }

        if (training_calenderDTO.trainingTypeCat == 2) {
            training_calenderDTO.country = Long.parseLong(request.getParameter("country"));
        } else {
            training_calenderDTO.country = 0;
        }

        Value = request.getParameter("trainingMaterialsDropzone");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {
            if (!Value.equalsIgnoreCase("")) {
                training_calenderDTO.trainingMaterialsDropzone = Long.parseLong(Value);
            } else {
                training_calenderDTO.trainingMaterialsDropzone = 0;
            }
            if (!addFlag) {
                String trainingMaterialsDropzoneFilesToDelete = request.getParameter("trainingMaterialsDropzoneFilesToDelete");
                String[] deleteArray = trainingMaterialsDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    logger.debug("going to delete " + deleteArray[i]);
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        }


        Value = request.getParameter("jobCat");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            training_calenderDTO.jobCat = Integer.parseInt(Value);
        } else {
            training_calenderDTO.jobCat = -1;
        }

        if (addFlag) {
            Utils.handleTransaction(() -> {
                training_calenderDAO.add(training_calenderDTO);
                addTrainer(request, training_calenderDTO.iD, userDTO, training_calenderDTO.trainerCat, training_calenderDTO);
                addTrainingCalendarDetails(training_calenderDTO, addFlag, empId, userDTO);
            });

        } else {

            Utils.handleTransaction(() -> {
                training_calenderDAO.update(training_calenderDTO);
                addTrainer(request, training_calenderDTO.iD, userDTO, training_calenderDTO.trainerCat, training_calenderDTO);
                addTrainingCalendarDetails(training_calenderDTO, addFlag, empId, userDTO);
            });

        }


        return training_calenderDTO;
    }

    void addTrainingCalendarDetails(Training_calenderDTO training_calenderDTO, boolean addFlag, long empId, UserDTO userDTO) throws Exception {
        if (training_calenderDTO.isPrivate && addFlag) {
            EmpOfficeModel empOfficeModel = new EmpOfficeModel();
            empOfficeModel.rowId = 0;
            empOfficeModel.employeeRecordId = empId;
            EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getFromCacheByEmployeeRecordIdIsDefault(empId);
            if (employeeOfficeDTO != null) {
                empOfficeModel.officeUnitId = employeeOfficeDTO.officeId;
                empOfficeModel.officeUnitOrganogramId = employeeOfficeDTO.officeUnitOrganogramId;
                empOfficeModel.organogramId = employeeOfficeDTO.officeUnitOrganogramId;
            }


            new Training_calendar_detailsDAO().add(Collections.singletonList(empOfficeModel), training_calenderDTO.iD, userDTO.userName);
        }
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Training_calenderServlet.class;
    }


    public String getTableJoinClause(HttpServletRequest request) {
        String joinClause = "";
        if (request.getParameter("trainee") != null && request.getParameter("trainee").trim().length() > 0) {
            joinClause += " JOIN training_calendar_details on training_calendar_details.training_calendar_id = training_calender.id ";
        }
        if (request.getParameter("trainer") != null && request.getParameter("trainer").trim().length() > 0) {
            joinClause += " JOIN training_calendar_trainer on training_calendar_trainer.training_calendar_id = training_calender.id ";
        }

        return joinClause;
    }

    public String getWhereClause(HttpServletRequest request) {
        String whereClause = "";
        if (request.getParameter("trainee") != null && request.getParameter("trainee").trim().length() > 0) {
            whereClause += " and (training_calendar_details.isDeleted = 0) ";
        }
        if (request.getParameter("trainer") != null && request.getParameter("trainer").trim().length() > 0) {
            whereClause += " and (training_calendar_trainer.isDeleted = 0) ";
        }

        return whereClause;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {

                case "addEnrolment":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TRAINING_CALENDER_ENROLMENT)) {
                        addEnrolment(request, userDTO);
                        response.sendRedirect("EnrollmentServlet?actionType=searchEnrolmentHistory");
                        return;
                    }
                    break;
                case "deleteEnrolment":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TRAINING_CALENDER_ENROLMENT)) {
                        deleteEnrolment(request, userDTO);
                        return;
                    }
                    break;
                case "getTrainingReportPage":
                    if (Utils.checkPermission(userDTO, MenuConstants.ETR)) {
                        request.getRequestDispatcher("employee_training_report/employee_training_report.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "ajax_getTrainingReport":
                    if (Utils.checkPermission(userDTO, MenuConstants.ETR)) {
                    	getTrainingReport(request, response);
                        return;
                    }
                    break;
                case "search":
                    Map<String, String> extraCriteriaMap;
                    if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) == null) {
                        extraCriteriaMap = new HashMap<>();
                    } else {
                        extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
                    }
                    extraCriteriaMap.put("is_private", String.valueOf(0));
                    request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                    super.doGet(request, response);
                    return;
                default:
                    super.doGet(request, response);
                    return;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }


    private void getTrainingReport(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	List<Long> employeeIds = new ArrayList<>();
        String employeeIdsStr = request.getParameter("employeeIds");
        if (employeeIdsStr != null) {
        	employeeIds =
                    Arrays.stream(employeeIdsStr.split(","))
                          .map(idStr -> Utils.parseOptionalLong(idStr, null, null))
                          .filter(Objects::nonNull)
                          .collect(Collectors.toList());
        }
        List<Election_wise_mpTrainingReportModel> employeeTrainingReportModels =
                Election_wise_mpDAO.getInstance().getForeignTrainingReportModels(employeeIds);
        request.setAttribute("employeeIds", employeeIds);
        request.setAttribute("employeeTrainingReportModels", employeeTrainingReportModels);
        request.getRequestDispatcher("employee_training_report/employee_trainingReportTable.jsp").forward(request, response);
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        boolean isLangEng = userDTO.languageID == SessionConstants.ENGLISH;
        boolean isAdmin = userDTO.roleID == RoleEnum.ADMIN.getRoleId() 
        		|| userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId()
        		|| userDTO.roleID == RoleEnum.TRAINING.getRoleId();


        try {
            long empId = Utils.parseOptionalInt(request.getParameter("empId"), 0, isLangEng ? "No employee id is found" : "কোনো কর্মকর্তার আইডি পাওয়া যায় নি");

            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "ajax_add":
                    if (isValid(request)) {
                    	logger.debug("valid role");
                        try {
                            addT(request, true, userDTO);
                            finalize(request);
                            logger.debug("sending admin response: " + isAdmin);
                            ApiResponse.sendSuccessResponse(response, getRedirectUrl(request, isAdmin, empId));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                    } else {
                        ApiResponse.sendErrorResponse(response, isLangEng ? "You have no permission to do this task" : "আপনার এই ক্রিয়া সম্পাদনের অনুমতি নেই");
                    }
                    return;

                case "ajax_edit":
                    if (isValid(request)) {
                        try {
                            addT(request, false, userDTO);
                            finalize(request);
                            ApiResponse.sendSuccessResponse(response, getRedirectUrl(request, isAdmin, empId));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                    } else {
                        ApiResponse.sendErrorResponse(response, isLangEng ? "You have no permission to do this task" : "আপনার এই ক্রিয়া সম্পাদনের অনুমতি নেই");
                    }
                    return;
                default:
                    super.doPost(request, response);
                    return;
            }

        } catch (Exception ex) {
            logger.debug(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);

    }


    private void addTrainer(HttpServletRequest request, long trainingCalendarId, UserDTO userDTO, int trainerCat, Training_calenderDTO training_calenderDTO) throws Exception {
        TrainerDAO trainerDAO = new TrainerDAO();
        List<TrainerDTO> oldDTOList = trainerDAO.getByForeignKey(String.valueOf(trainingCalendarId));
        List<EmpOfficeModel> employeeRecords = new ArrayList<>();
        String employeeIds = request.getParameter("addedEmployees");
        if (employeeIds != null) {
            employeeIds = Jsoup.clean(employeeIds, Whitelist.simpleText());
            employeeRecords = Arrays.asList(gson.fromJson(employeeIds, EmpOfficeModel[].class));
        }

        if ((employeeRecords.size() == 0) && (oldDTOList == null || oldDTOList.size() == 0)) {
            return;
        }

        if (oldDTOList == null || oldDTOList.size() == 0) {
            trainerDAO.add(employeeRecords, trainingCalendarId, userDTO.userName);
            List<Long> addedOrganograms = employeeRecords.stream()
                    .map(dto -> dto.organogramId)
                    .collect(Collectors.toList());
            TrainerNotification.getInstance().sendTrainerSelectionNotification(addedOrganograms, training_calenderDTO);
        } else {
            trainerDAO.update(employeeRecords, trainingCalendarId, userDTO.userName, training_calenderDTO);
        }
    }

    public String getDeleteRedirectURL(HttpServletRequest request) {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        boolean isLangEng = userDTO.languageID == SessionConstants.ENGLISH;
        boolean isAdmin = userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId();
        long empId = 0;

        try {
            empId = Utils.parseOptionalInt(request.getParameter("empId"), 0, isLangEng ? "No employee id is found" : "কোনো কর্মকর্তার আইডি পাওয়া যায় নি");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getRedirectUrl(request, isAdmin, empId);
    }

    private String getRedirectUrl(HttpServletRequest request, boolean isAdmin, long empId) {
        String redirectUrl;
        if (request.getParameter("tab") != null) {
            if (isAdmin) {
                redirectUrl = "Employee_recordsServlet?actionType=viewMultiForm&data=trainingCalender" + "&tab=4" + "&ID=" + empId;
            } else {
                redirectUrl = "Employee_recordsServlet?actionType=viewMyProfile&data=trainingCalender" + "&tab=4" + "&ID=" + empId;
            }
        } else {
            redirectUrl = "Training_calenderServlet?actionType=search";
        }
        return redirectUrl;
    }


    private void addEnrolment(HttpServletRequest request, UserDTO user) throws Exception {
        long trainingCalenderId = Long.parseLong(request.getParameter("ID"));
        long employeeRecordId = user.employee_record_id;
        Training_calendar_detailsDAO trainingCalendarDetailsDAO = new Training_calendar_detailsDAO();
        Training_calendar_detailsDTO dto = trainingCalendarDetailsDAO.getDTOByCalendarIdAndEmployeeRecordId(trainingCalenderId, employeeRecordId);
        if (dto != null) {
            logger.error("Training Calendar :" + trainingCalenderId + " is already enrolled to " + employeeRecordId);
            return;
        }
        EmployeeOfficeDTO mainResponsibilityDTO = getOfficeUnit(employeeRecordId);
        if (mainResponsibilityDTO == null) {
            logger.error("Office unitId not valid");
            return;
        }
        long curTime = Calendar.getInstance().getTimeInMillis();
        long officeUnitId = mainResponsibilityDTO.officeUnitId;
        long organogramId = mainResponsibilityDTO.officeUnitOrganogramId;
        Training_calendar_detailsDTO calendar_detailsDTO = new Training_calendar_detailsDTO();
        calendar_detailsDTO.trainingCalendarId = trainingCalenderId;
        calendar_detailsDTO.employeeRecordsId = employeeRecordId;
        calendar_detailsDTO.officeUnitId = officeUnitId;
        calendar_detailsDTO.organogramId = organogramId;
        calendar_detailsDTO.insertBy = user.userName;
        calendar_detailsDTO.modified_by = user.userName;
        calendar_detailsDTO.lastModificationTime = curTime;
        calendar_detailsDTO.insertionDate = curTime;

        trainingCalendarDetailsDAO.addEmployeeFlatInfoDTO(calendar_detailsDTO);

    }

    private void deleteEnrolment(HttpServletRequest request, UserDTO user) {
        new Training_calendar_detailsDAO().deletebyCalendarAndEmployee(Long.parseLong(request.getParameter("ID")), user.employee_record_id);
    }

    private EmployeeOfficeDTO getOfficeUnit(long employeeRecordId) {
        List<EmployeeOfficeDTO> officeDTOS = EmployeeOfficeRepository.getInstance().getByEmployeeRecordId(employeeRecordId);
        long curTime = Calendar.getInstance().getTimeInMillis();
        for (EmployeeOfficeDTO dto : officeDTOS) {
            if (dto.isDefault == 1 && dto.isDeleted == 0 &&
                    (dto.lastOfficeDate == SessionConstants.MIN_DATE || dto.lastOfficeDate > curTime)) {
                return dto;
            }
        }
        return null;
    }

    private long applyTimeChage(long lastEnrolmentDate, long startDate, String startTime) {
        long returnVal = lastEnrolmentDate;
        if (lastEnrolmentDate != SessionConstants.MIN_DATE && lastEnrolmentDate == startDate) {
            if (startTime != null && startTime.length() > 0) {
                long addtime = LocalTime.parse(startTime).toSecondOfDay() * 1000L;
                returnVal = returnVal + addtime;

            }
        }
        if (returnVal == lastEnrolmentDate && returnVal != SessionConstants.MIN_DATE) {
            returnVal = returnVal + (24 * 60 * 60 * 1000) - (60 * 1000); //end time will be 23:59
        }
        return returnVal;
    }
}