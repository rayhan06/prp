package training_calender;

/*
 * @author Md. Erfan Hossain
 * @created 18/03/2021 - 2:00 PM
 * @project parliament
 */

import java.util.Arrays;

public class TrainingCalendarDTODetails extends Training_calenderDTO{
    public long employeeRecordId;
    public String trainerNameEn;
    public String trainerNameBn;
    public String trainerOfficeEn;
    public String trainerOfficeBn;
    public String trainerDesignationEn;
    public String trainerDesignationBn;

    public String traineeNameEn;
    public String traineeNameBn;
    public String traineeOfficeEn;
    public String traineeOfficeBn;
    public String traineeDesignationEn;
    public String traineeDesignationBn;

    @Override
    public String toString() {
        return "TrainingCalendarDTODetails{" +
                "employeeRecordId=" + employeeRecordId +
                ", trainerNameEn='" + trainerNameEn + '\'' +
                ", trainerNameBn='" + trainerNameBn + '\'' +
                ", trainerOfficeEn='" + trainerOfficeEn + '\'' +
                ", trainerOfficeBn='" + trainerOfficeBn + '\'' +
                ", trainerDesignationEn='" + trainerDesignationEn + '\'' +
                ", trainerDesignationBn='" + trainerDesignationBn + '\'' +
                ", traineeNameEn='" + traineeNameEn + '\'' +
                ", traineeNameBn='" + traineeNameBn + '\'' +
                ", traineeOfficeEn='" + traineeOfficeEn + '\'' +
                ", traineeOfficeBn='" + traineeOfficeBn + '\'' +
                ", traineeDesignationEn='" + traineeDesignationEn + '\'' +
                ", traineeDesignationBn='" + traineeDesignationBn + '\'' +
                ", nameEn='" + nameEn + '\'' +
                ", nameBn='" + nameBn + '\'' +
                ", trainingTypeCat=" + trainingTypeCat +
                ", country=" + country +
                ", instituteName='" + instituteName + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", arrangedBy='" + arrangedBy + '\'' +
                ", trainingModeCat=" + trainingModeCat +
                ", venue='" + venue + '\'' +
                ", maxNumberOfParticipants=" + maxNumberOfParticipants +
                ", isEnrollmentApplicale=" + isEnrollmentApplicale +
                ", lastEnrollmentDate=" + lastEnrollmentDate +
                ", targetGroupType='" + targetGroupType + '\'' +
                ", trainingObjective='" + trainingObjective + '\'' +
                ", trainingPrerequisite='" + trainingPrerequisite + '\'' +
                ", trainingMaterialsDropzone=" + trainingMaterialsDropzone +
                ", isBondApplicable=" + isBondApplicable +
                ", yearOfBond='" + yearOfBond + '\'' +
                ", insertionDate=" + insertionDate +
                ", insertedByUserId=" + insertedByUserId +
                ", modifiedById='" + modifiedById + '\'' +
                ", searchColumn='" + searchColumn + '\'' +
                ", onlineTrainingDetails='" + onlineTrainingDetails + '\'' +
                ", trainingCost='" + trainingCost + '\'' +
                ", trainerCat=" + trainerCat +
                ", externalTrainerInfo='" + externalTrainerInfo + '\'' +
                ", iD=" + iD +
                ", isDeleted=" + isDeleted +
                ", lastModificationTime=" + lastModificationTime +
                ", jobCat=" + jobCat +
                ", subject='" + subject + '\'' +
                ", remarks='" + remarks + '\'' +
                ", fileID=" + fileID +
                ", rejectTo=" + rejectTo +
                ", searchColumn='" + searchColumn + '\'' +
                ", isSeen=" + isSeen +
                ", inboxMovementId=" + inboxMovementId +
                ", inboxOriginatorId=" + inboxOriginatorId +
                ", columnNames=" + Arrays.toString(columnNames) +
                '}';
    }
}
