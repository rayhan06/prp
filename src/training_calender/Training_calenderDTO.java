package training_calender;

import sessionmanager.SessionConstants;
import util.CommonDTO;


public class Training_calenderDTO extends CommonDTO {
    public String nameEn = "";
    public String nameBn = "";
    public int trainingCategoryCat = 0;
    public int trainingTypeCat = 0;
    public long country = 0;
    public String instituteName = "";
    public long startDate = SessionConstants.MIN_DATE;
    public long endDate = SessionConstants.MIN_DATE;
    public String startTime = "";
    public String endTime = "";
    public int arrangedBy = 0;
    public int trainingModeCat = 0;
    public String venue = "";
    public int maxNumberOfParticipants = 0;
    public boolean isEnrollmentApplicale = false;
    public long lastEnrollmentDate = SessionConstants.MIN_DATE;
    public String targetGroupType = "";
    public String trainingObjective = "";
    public String trainingPrerequisite = "";
    public long trainingMaterialsDropzone = 0;
    public boolean isPrivate = false;
    public long privateEmpId = -1;
    public boolean isBondApplicable = false;
    public String yearOfBond = "";
    public long insertionDate = 0;
    public long insertedByUserId = 0;
    public String modifiedById = "";
    public String onlineTrainingDetails = "";
    public String trainingCost = "";
    public int trainerCat = 0;
    public String externalTrainerInfo = "";
    public int status = 0;

    @Override
    public String toString() {
        return "Training_calenderDTO{" +
                "nameEn='" + nameEn + '\'' +
                ", nameBn='" + nameBn + '\'' +
                ", trainingTypeCat=" + trainingTypeCat +
                ", country=" + country +
                ", instituteName='" + instituteName + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", arrangedBy='" + arrangedBy + '\'' +
                ", trainingModeCat=" + trainingModeCat +
                ", venue='" + venue + '\'' +
                ", maxNumberOfParticipants=" + maxNumberOfParticipants +
                ", isEnrollmentApplicale=" + isEnrollmentApplicale +
                ", lastEnrollmentDate=" + lastEnrollmentDate +
                ", targetGroupType='" + targetGroupType + '\'' +
                ", trainingObjective='" + trainingObjective + '\'' +
                ", trainingPrerequisite='" + trainingPrerequisite + '\'' +
                ", trainingMaterialsDropzone=" + trainingMaterialsDropzone +
                ", isBondApplicable=" + isBondApplicable +
                ", yearOfBond='" + yearOfBond + '\'' +
                ", insertionDate=" + insertionDate +
                ", insertedByUserId=" + insertedByUserId +
                ", modifiedById='" + modifiedById + '\'' +
                ", searchColumn='" + searchColumn + '\'' +
                ", onlineTrainingDetails='" + onlineTrainingDetails + '\'' +
                ", trainingCost='" + trainingCost + '\'' +
                ", trainerCat=" + trainerCat +
                ", externalTrainerInfo='" + externalTrainerInfo + '\'' +
                ", status=" + status +
                '}';
    }
}