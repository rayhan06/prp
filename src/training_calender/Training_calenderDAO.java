package training_calender;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import training_calendar_details.Training_calendar_detailsDAO;
import util.CommonDTO;
import util.CommonMaps;
import util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"unused", "Duplicates"})
public class Training_calenderDAO implements CommonDAOService<Training_calenderDTO> {

    private static final Logger logger = Logger.getLogger(Training_calenderDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (name_en,name_bn,training_category_cat,training_type_cat,country,institute_name,start_date,end_date,start_time,"
            .concat(" end_time,training_arrange_by_cat,training_mode_cat,venue,max_number_of_participants,is_enrollment_applicale,last_enrollment_date,")
            .concat(" target_group_type,training_objective,training_prerequisite,training_materials_dropzone,")
            .concat(" is_private,private_emp_id,is_bond_applicable,year_of_bond,online_training_details,training_cost,trainer_cat,ext_trainer_info,status,")
            .concat(" modified_by_id,search_column,job_cat,lastModificationTime,isDeleted,inserted_by_user_id,insertion_date,ID) ")
            .concat(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");


    private static final String updateQuery = "UPDATE {tableName} SET name_en=?,name_bn=?,training_category_cat=?,training_type_cat=?,country=?,institute_name=?,start_date=?,end_date=?,start_time=?,"
            .concat(" end_time=?,training_arrange_by_cat=?,training_mode_cat=?,venue=?,max_number_of_participants=?,is_enrollment_applicale=?,")
            .concat(" last_enrollment_date=?,target_group_type=?,training_objective=?,training_prerequisite=?,training_materials_dropzone=?,")
            .concat(" is_private=?,private_emp_id=?,is_bond_applicable=?,year_of_bond=?,online_training_details=?,training_cost=?,trainer_cat=?,ext_trainer_info=?,status=?,")
            .concat(" modified_by_id=?,search_column=?,job_cat=?,lastModificationTime =? WHERE ID = ?");

    private static final String chartQuery = "SELECT  * FROM  training_calender INNER JOIN training_calendar_details"
            .concat("    on training_calendar_details.training_calendar_id = training_calender.ID WHERE (training_calender.isDeleted = 0) and (training_calendar_details.isDeleted = 0 )")
            .concat("    and (training_calendar_details.employee_records_id=%d )  and training_calender.end_date >%d  order by training_calender.lastModificationTime desc");


    public CommonMaps approvalMaps = new Training_calenderApprovalMAPS("training_calender");

    private String additionalFilter = "";
    private boolean hasTrainee = false;

    private static final Map<String, String> searchMap = new HashMap<>();

    private Training_calenderDAO() {
        searchMap.put("training_category_cat", " and (training_calender.training_category_cat = ?)");
        searchMap.put("training_type_cat", " and (training_calender.training_type_cat = ?)");
        searchMap.put("start_date", " and (training_calender.start_date >= ?)");
        searchMap.put("end_date", " and (training_calender.end_date <= ?)");
        searchMap.put("training_mode_cat", " and (training_calender.training_mode_cat = ?)");
        searchMap.put("is_private", " and (training_calender.is_private = ?)");
        searchMap.put("status", " and (training_calender.status = ?)");
        searchMap.put("AnyField", " and (training_calender.search_column like ?)");
        searchMap.put("trainer", "and (training_calendar_trainer.employee_records_id = ?)");
        searchMap.put("trainee", "and (training_calendar_details.employee_records_id in ?)");
        searchMap.put("is_enrollment_applicale", "and (training_calender.is_enrollment_applicale = ?)");
        searchMap.put("last_enrollment_date", "and (training_calender.last_enrollment_date >= ?)");
    }

    private static class LazyLoader {
        static final Training_calenderDAO INSTANCE = new Training_calenderDAO();
    }

    public static Training_calenderDAO getInstance() {
        return Training_calenderDAO.LazyLoader.INSTANCE;
    }


    public void setSearchColumn(Training_calenderDTO training_calenderDTO) {
        StringBuilder column = new StringBuilder();
        if (training_calenderDTO != null) {
            if (training_calenderDTO.nameBn != null && training_calenderDTO.nameBn.trim().length() > 0) {
                column.append(training_calenderDTO.nameBn.trim()).append(" ");
            }
            if (training_calenderDTO.nameEn != null && training_calenderDTO.nameEn.trim().length() > 0) {
                column.append(training_calenderDTO.nameEn.trim()).append(" ");
            }
            if (training_calenderDTO.instituteName != null && training_calenderDTO.instituteName.trim().length() > 0) {
                column.append(training_calenderDTO.instituteName.trim()).append(" ");
            }
            column.append(training_calenderDTO.arrangedBy).append(" ");
            if (training_calenderDTO.venue != null && training_calenderDTO.venue.trim().length() > 0) {
                column.append(training_calenderDTO.venue.trim()).append(" ");
            }
            training_calenderDTO.searchColumn = column.toString();
        }
    }

    public void set(PreparedStatement ps, Training_calenderDTO training_calenderDTO, boolean isInsert) throws SQLException {
        setSearchColumn(training_calenderDTO);
        int index = 0;
        ps.setObject(++index, training_calenderDTO.nameEn);
        ps.setObject(++index, training_calenderDTO.nameBn);
        ps.setObject(++index, training_calenderDTO.trainingCategoryCat);
        ps.setObject(++index, training_calenderDTO.trainingTypeCat);
        ps.setObject(++index, training_calenderDTO.country);
        ps.setObject(++index, training_calenderDTO.instituteName);
        ps.setObject(++index, training_calenderDTO.startDate);
        ps.setObject(++index, training_calenderDTO.endDate);
        ps.setObject(++index, training_calenderDTO.startTime);
        ps.setObject(++index, training_calenderDTO.endTime);
        ps.setObject(++index, training_calenderDTO.arrangedBy);
        ps.setObject(++index, training_calenderDTO.trainingModeCat);
        ps.setObject(++index, training_calenderDTO.venue);
        ps.setObject(++index, training_calenderDTO.maxNumberOfParticipants);
        ps.setObject(++index, training_calenderDTO.isEnrollmentApplicale);
        ps.setObject(++index, training_calenderDTO.lastEnrollmentDate);
        ps.setObject(++index, training_calenderDTO.targetGroupType);
        ps.setObject(++index, training_calenderDTO.trainingObjective);
        ps.setObject(++index, training_calenderDTO.trainingPrerequisite);
        ps.setObject(++index, training_calenderDTO.trainingMaterialsDropzone);
        ps.setObject(++index, training_calenderDTO.isPrivate);
        ps.setObject(++index, training_calenderDTO.privateEmpId);
        ps.setObject(++index, training_calenderDTO.isBondApplicable);
        ps.setObject(++index, training_calenderDTO.yearOfBond);
        ps.setObject(++index, training_calenderDTO.onlineTrainingDetails);
        ps.setObject(++index, training_calenderDTO.trainingCost);
        ps.setObject(++index, training_calenderDTO.trainerCat);
        ps.setObject(++index, training_calenderDTO.externalTrainerInfo);
        ps.setObject(++index, training_calenderDTO.status);
        ps.setObject(++index, training_calenderDTO.modifiedById);
        ps.setObject(++index, training_calenderDTO.searchColumn);
        ps.setObject(++index, training_calenderDTO.jobCat);
        ps.setObject(++index, System.currentTimeMillis());
        if (isInsert) {
            ps.setObject(++index, 0);
            ps.setObject(++index, training_calenderDTO.insertedByUserId);
            ps.setObject(++index, training_calenderDTO.insertionDate);
        }
        ps.setObject(++index, training_calenderDTO.iD);
    }

    public Training_calenderDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Training_calenderDTO dto = new Training_calenderDTO();
            setTrainingCalenderDTOFromResultSet(rs, dto);
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "training_calender";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public void setTrainingCalenderDTOFromResultSet(ResultSet rs, Training_calenderDTO training_calenderDTO) throws SQLException {
        training_calenderDTO.iD = rs.getLong("training_calender.ID");
        training_calenderDTO.nameEn = rs.getString("training_calender.name_en");
        training_calenderDTO.nameBn = rs.getString("training_calender.name_bn");
        training_calenderDTO.trainingCategoryCat = rs.getInt("training_calender.training_category_cat");
        training_calenderDTO.trainingTypeCat = rs.getInt("training_calender.training_type_cat");
        training_calenderDTO.country = rs.getLong("training_calender.country");
        training_calenderDTO.instituteName = rs.getString("training_calender.institute_name");
        training_calenderDTO.startDate = rs.getLong("training_calender.start_date");
        training_calenderDTO.endDate = rs.getLong("training_calender.end_date");
        training_calenderDTO.startTime = rs.getString("training_calender.start_time");
        training_calenderDTO.endTime = rs.getString("training_calender.end_time");
        training_calenderDTO.arrangedBy = rs.getInt("training_calender.training_arrange_by_cat");
        training_calenderDTO.trainingModeCat = rs.getInt("training_calender.training_mode_cat");
        training_calenderDTO.venue = rs.getString("training_calender.venue");
        training_calenderDTO.maxNumberOfParticipants = rs.getInt("training_calender.max_number_of_participants");
        training_calenderDTO.isEnrollmentApplicale = rs.getBoolean("training_calender.is_enrollment_applicale");
        training_calenderDTO.lastEnrollmentDate = rs.getLong("training_calender.last_enrollment_date");
        training_calenderDTO.targetGroupType = rs.getString("training_calender.target_group_type");
        training_calenderDTO.trainingObjective = rs.getString("training_calender.training_objective");
        training_calenderDTO.trainingPrerequisite = rs.getString("training_calender.training_prerequisite");
        training_calenderDTO.trainingMaterialsDropzone = rs.getLong("training_calender.training_materials_dropzone");
        training_calenderDTO.isPrivate = rs.getBoolean("training_calender.is_private");
        training_calenderDTO.privateEmpId = rs.getLong("training_calender.private_emp_id");
        training_calenderDTO.isBondApplicable = rs.getBoolean("training_calender.is_bond_applicable");
        training_calenderDTO.yearOfBond = rs.getString("training_calender.year_of_bond");
        training_calenderDTO.insertionDate = rs.getLong("training_calender.insertion_date");
        training_calenderDTO.insertedByUserId = rs.getLong("training_calender.inserted_by_user_id");
        training_calenderDTO.modifiedById = rs.getString("training_calender.modified_by_id");
        training_calenderDTO.jobCat = rs.getInt("training_calender.job_cat");
        training_calenderDTO.trainerCat = rs.getInt("training_calender.trainer_cat");
        training_calenderDTO.externalTrainerInfo = rs.getString("training_calender.ext_trainer_info");
        training_calenderDTO.status = rs.getInt("training_calender.status");
        training_calenderDTO.onlineTrainingDetails = rs.getString("training_calender.online_training_details");
        training_calenderDTO.trainingCost = rs.getString("training_calender.training_cost");
        training_calenderDTO.isDeleted = rs.getInt("training_calender.isDeleted");
        training_calenderDTO.lastModificationTime = rs.getLong("training_calender.lastModificationTime");
    }

    public TrainingCalendarDTODetails buildObjectFromResultSetForTrainer(ResultSet rs) {
        return setTrainingCalendarDTODetailsFromResultSetForTrainer(rs, new TrainingCalendarDTODetails());
    }

    public String getLastModifierUser() {
        return "modified_by_id";
    }

    public TrainingCalendarDTODetails setTrainingCalendarDTODetailsFromResultSetForTrainer(ResultSet rs, TrainingCalendarDTODetails dtoDetails) {
        try {
            setTrainingCalenderDTOFromResultSet(rs, dtoDetails);
            dtoDetails.employeeRecordId = rs.getLong("training_calendar_trainer.employee_records_id");
            dtoDetails.trainerNameEn = rs.getString("training_calendar_trainer.employee_name_en");
            dtoDetails.trainerNameBn = rs.getString("training_calendar_trainer.employee_name_bn");
            dtoDetails.trainerOfficeEn = rs.getString("training_calendar_trainer.office_name_en");
            dtoDetails.trainerOfficeBn = rs.getString("training_calendar_trainer.office_name_bn");
            dtoDetails.trainerDesignationEn = rs.getString("training_calendar_trainer.organogram_name_en");
            dtoDetails.trainerDesignationBn = rs.getString("training_calendar_trainer.organogram_name_bn");
            return dtoDetails;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public TrainingCalendarDTODetails buildObjectFromResultSetForTrainee(ResultSet rs) {
        try {
            TrainingCalendarDTODetails dtoDetails = new TrainingCalendarDTODetails();
            setTrainingCalenderDTOFromResultSet(rs, dtoDetails);
            setTrainingCalendarDTODetailsFromResultSetForTrainee(rs, dtoDetails);
            return dtoDetails;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public void setTrainingCalendarDTODetailsFromResultSetForTrainee(ResultSet rs, TrainingCalendarDTODetails dtoDetails) throws SQLException {
        dtoDetails.employeeRecordId = rs.getLong("training_calendar_details.employee_records_id");
        dtoDetails.traineeNameEn = rs.getString("training_calendar_details.employee_name_en");
        dtoDetails.traineeNameBn = rs.getString("training_calendar_details.employee_name_bn");
        dtoDetails.traineeOfficeEn = rs.getString("training_calendar_details.office_name_en");
        dtoDetails.traineeOfficeBn = rs.getString("training_calendar_details.office_name_bn");
        dtoDetails.traineeDesignationEn = rs.getString("training_calendar_details.organogram_name_en");
        dtoDetails.traineeDesignationBn = rs.getString("training_calendar_details.organogram_name_bn");
    }

    public TrainingCalendarDTODetails buildObjectFromResultSetForTrainerAndTrainee(ResultSet rs) {
        TrainingCalendarDTODetails dtoDetails = buildObjectFromResultSetForTrainer(rs);
        if (dtoDetails != null) {
            try {
                setTrainingCalendarDTODetailsFromResultSetForTrainee(rs, dtoDetails);
            } catch (SQLException ex) {
                logger.error(ex);
                return null;
            }
        }
        return dtoDetails;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Training_calenderDTO) commonDTO, addQuery, true);
    }


    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Training_calenderDTO) commonDTO, updateQuery, false);
    }

    public List<Training_calenderDTO> getAllTraining_calender(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }

    public TrainingCalendarModel getTrainingCalendarModelById(long id) {
        Training_calenderDTO dto = getDTOFromID(id);
        TrainingCalendarModel model = new TrainingCalendarModel();
        model.trainingCalenderDTO = dto;
        if (dto != null) {
            CategoryLanguageModel languageModel = CatRepository.getInstance().getCategoryLanguageModel("training_type", dto.trainingTypeCat);
            if (languageModel != null) {
                model.trainingTypeEng = languageModel.englishText;
                model.trainingTypeBan = languageModel.banglaText;
            }
            languageModel = CatRepository.getInstance().getCategoryLanguageModel("training_mode", dto.trainingModeCat);
            if (languageModel != null) {
                model.trainingModeEng = languageModel.englishText;
                model.trainingModeBan = languageModel.banglaText;
            }

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            model.startDateEng = sdf.format(new Date(dto.startDate));
            model.startDateBan = StringUtils.convertToBanNumber(model.startDateEng);
            model.endDateEng = sdf.format(new Date(dto.endDate));
            model.endDateBan = StringUtils.convertToBanNumber(model.endDateEng);
        }
        return model;
    }

    public void setHasTrainee() {
        hasTrainee = true;
    }

    public void addAdditionalFilter(Map<String, String> columnValMap) {
        columnValMap.forEach((key, val) -> {
            logger.debug("key :" + key + " val: " + val);
            additionalFilter = additionalFilter.concat(" and (" + key);
            additionalFilter = additionalFilter.concat(val + " ) ");
        });
        logger.debug("Additional Filter: " + additionalFilter);
    }

    public Map<String, Integer> getEnrolmentHistoryChartData(String language, long employeeRecordId) {
        Map<String, Integer> result;
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -6);
        long timeBeforeSixMonths = c.getTimeInMillis();

        List<Long> trainingCalendarIds = new Training_calendar_detailsDAO().getTrainingCalendarIdsList(employeeRecordId, language);
        List<Training_calenderDTO> training_calenderDTOList = getDTOs(trainingCalendarIds);

        result = training_calenderDTOList
                .stream()
                .filter(dto -> (dto != null) && (dto.status == 3) && (dto.endDate > timeBeforeSixMonths))
                .collect(Collectors.groupingBy(dto -> dto.trainingTypeCat, Collectors.collectingAndThen(Collectors.counting(), Long::intValue)))
                .entrySet()
                .stream()
                .collect(Collectors.toMap(e -> CatRepository.getInstance().getText(language, "training_type", e.getKey()), Map.Entry::getValue));

        List<CategoryLanguageModel> modelList = CatRepository.getInstance().getCategoryLanguageModelList("training_type");

        for (CategoryLanguageModel model : modelList) {
            if (language.equalsIgnoreCase("ENGLISH")) {
                if (!result.containsKey(model.englishText))
                    result.put(model.englishText, 0);
            } else {
                if (!result.containsKey(model.banglaText))
                    result.put(model.banglaText, 0);
            }
        }
        return result;
    }
}
