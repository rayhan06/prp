package asset_transaction;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import java.sql.Statement;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;

import asset_model.AssetAssigneeDTO;
import util.*;
import workflow.WorkflowController;
import pb.*;
import user.UserDTO;

public class Asset_transactionDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Asset_transactionDAO(String tableName)
	{
		super(tableName);
		useSafeSearch = true;
		joinSQL = "";
		columnNames = new String[] 
		{
			"ID",
			"table_name",
			"brand_type",
			"asset_model_type",
			"asset_assignee_type",
			"asset_category_type",
			"model",
			"sl",
			"transaction_cat",
			"lot",
			"maintenance_vendor_id",
			"last_active_status",
			"cost",
			
			"comment",


			"assigned_organogram_id",
			"employee_record_id",
			"to_org_id",
		
			"transaction_date",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"quantity",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Asset_transactionDAO()
	{
		this("asset_transaction");		
	}
	
	public void setSearchColumn(Asset_transactionDTO asset_transactionDTO)
	{
		asset_transactionDTO.searchColumn = "";
		asset_transactionDTO.searchColumn += asset_transactionDTO.tableName + " ";
		asset_transactionDTO.searchColumn += CommonDAO.getName("English", "brand", asset_transactionDTO.brandType) + " " + CommonDAO.getName("Bangla", "brand", asset_transactionDTO.brandType) + " ";
		asset_transactionDTO.searchColumn += CommonDAO.getName("English", "asset_model", asset_transactionDTO.assetModelId) + " " + CommonDAO.getName("Bangla", "asset_model", asset_transactionDTO.assetModelId) + " ";
		asset_transactionDTO.searchColumn += CommonDAO.getName("English", "asset_category", asset_transactionDTO.assetCategoryId) + " " + CommonDAO.getName("Bangla", "asset_category", asset_transactionDTO.assetCategoryId) + " ";
		asset_transactionDTO.searchColumn += asset_transactionDTO.model + " ";
		asset_transactionDTO.searchColumn += WorkflowController.getWingNameFromOrganogramId(asset_transactionDTO.assignedOrganogramId, "English") + " ";
		asset_transactionDTO.searchColumn += WorkflowController.getWingNameFromOrganogramId(asset_transactionDTO.assignedOrganogramId, "Bangla") + " ";
		asset_transactionDTO.searchColumn += asset_transactionDTO.sl + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Asset_transactionDTO asset_transactionDTO = (Asset_transactionDTO)commonDTO;
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(asset_transactionDTO);
		if(isInsert)
		{
			ps.setObject(++index,asset_transactionDTO.iD);
		}
		ps.setObject(++index,asset_transactionDTO.tableName);
		ps.setObject(++index,asset_transactionDTO.brandType);
		ps.setObject(++index,asset_transactionDTO.assetModelId);
		ps.setObject(++index,asset_transactionDTO.assetAssigneeId);
		ps.setObject(++index,asset_transactionDTO.assetCategoryId);
		ps.setObject(++index,asset_transactionDTO.model);
		ps.setObject(++index,asset_transactionDTO.sl);
		ps.setObject(++index,asset_transactionDTO.transactionCat);
		ps.setObject(++index,asset_transactionDTO.lot);
		ps.setObject(++index,asset_transactionDTO.maintenance_vendor_id);
		ps.setObject(++index,asset_transactionDTO.lastActiveStatus);
		ps.setObject(++index,asset_transactionDTO.cost);
		
		ps.setObject(++index,asset_transactionDTO.comment);

		ps.setObject(++index,asset_transactionDTO.assignedOrganogramId);
		ps.setObject(++index,asset_transactionDTO.employeeRecordId);
		ps.setObject(++index,asset_transactionDTO.toOrgId);

		ps.setObject(++index,asset_transactionDTO.transactionDate);
		ps.setObject(++index,asset_transactionDTO.searchColumn);
		ps.setObject(++index,asset_transactionDTO.insertedByUserId);
		ps.setObject(++index,asset_transactionDTO.insertedByOrganogramId);
		ps.setObject(++index,asset_transactionDTO.insertionDate);
		ps.setObject(++index,asset_transactionDTO.lastModifierUser);
		ps.setObject(++index,asset_transactionDTO.quantity);
		if(isInsert)
		{
			ps.setObject(++index, 0);
			ps.setObject(++index, lastModificationTime);
		}
	}
	
	public Asset_transactionDTO build(ResultSet rs)
	{
		try
		{
			Asset_transactionDTO asset_transactionDTO = new Asset_transactionDTO();
			int i = 0;
			asset_transactionDTO.iD = rs.getLong(columnNames[i++]);
			asset_transactionDTO.tableName = rs.getString(columnNames[i++]);
			asset_transactionDTO.brandType = rs.getInt(columnNames[i++]);
			asset_transactionDTO.assetModelId = rs.getLong(columnNames[i++]);
			asset_transactionDTO.assetAssigneeId = rs.getLong(columnNames[i++]);
			asset_transactionDTO.assetCategoryId = rs.getLong(columnNames[i++]);
			asset_transactionDTO.model = rs.getString(columnNames[i++]);
			asset_transactionDTO.sl = rs.getString(columnNames[i++]);
			asset_transactionDTO.transactionCat = rs.getInt(columnNames[i++]);
			asset_transactionDTO.lot = rs.getString(columnNames[i++]);
			asset_transactionDTO.maintenance_vendor_id = rs.getLong(columnNames[i++]);
			asset_transactionDTO.lastActiveStatus = rs.getInt(columnNames[i++]);
			asset_transactionDTO.cost = rs.getDouble(columnNames[i++]);
			
			asset_transactionDTO.comment = rs.getString(columnNames[i++]);

			asset_transactionDTO.assignedOrganogramId = rs.getLong(columnNames[i++]);
			asset_transactionDTO.employeeRecordId = rs.getLong(columnNames[i++]);
			asset_transactionDTO.toOrgId = rs.getLong(columnNames[i++]);

			asset_transactionDTO.transactionDate = rs.getLong(columnNames[i++]);
			asset_transactionDTO.searchColumn = rs.getString(columnNames[i++]);
			asset_transactionDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			asset_transactionDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			asset_transactionDTO.insertionDate = rs.getLong(columnNames[i++]);
			asset_transactionDTO.lastModifierUser = rs.getString(columnNames[i++]);
			asset_transactionDTO.quantity = rs.getInt(columnNames[i++]);
			asset_transactionDTO.isDeleted = rs.getInt(columnNames[i++]);
			asset_transactionDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return asset_transactionDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	 public long updateSL(AssetAssigneeDTO assetAssigneeDTO) throws Exception {
	        long lastModificationTime = System.currentTimeMillis();
	        String sql = "Update asset_transaction set sl = ? , lastModificationTime = " + lastModificationTime 
	        		+ " where asset_assignee_type = ?" ;
	        return (Long) ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
	            Connection connection = model.getConnection();
	            PreparedStatement ps = (PreparedStatement) model.getStatement();
	            try {
	            	ps.setObject(1,assetAssigneeDTO.sl);
	            	ps.setObject(2,assetAssigneeDTO.iD);
	                logger.debug(ps);
	                ps.executeUpdate();
	                recordUpdateTime(connection, lastModificationTime);
	                return 1L;
	            } catch (SQLException ex) {
	                logger.error(ex);
	                return -1L;
	            }
	        }, sql);
	    }

	public List<Asset_transactionDTO> getUnassigned(long parentID) throws Exception {
        String sql = "select * from asset_transaction WHERE asset_model_type =" + parentID + " and isDeleted = 0";
        return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }
	
	 public long deleteByAssetModelId(long assetModelId) throws Exception {
	        long lastModificationTime = System.currentTimeMillis();
	        StringBuilder sqlBuilder = new StringBuilder("UPDATE ")
	                .append(tableName)
	                .append(" SET isDeleted=1,lastModificationTime=")
	                .append(lastModificationTime)
	                .append(" WHERE asset_model_type = ")
	                .append(assetModelId);
	        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
	            String sql = sqlBuilder.toString();
	            Connection connection = model.getConnection();
	            Statement stmt = model.getStatement();
	            try {
	                logger.debug(sql);
	                stmt.execute(sql);
	                recordUpdateTime(connection, lastModificationTime);
	            } catch (SQLException ex) {
	                logger.error(ex);
	            }
	        });
	        return assetModelId;
	    }


	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {

		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ?";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("table_name")
						|| str.equals("brand_type")
						|| str.equals("asset_model_type")
						|| str.equals("asset_category_type")
						|| str.equals("model")
						|| str.equals("sl")
						|| str.equals("transaction_cat")
						|| str.equals("lot")
						|| str.equals("office_unit_type")
						|| str.equals("wing_type")
						|| str.equals("receiver_name_en")
						|| str.equals("receiver_name_bn")
						|| str.equals("transaction_date_start")
						|| str.equals("transaction_date_end")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("table_name"))
					{
						AllFieldSql += "" + tableName + ".table_name like ?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("brand_type"))
					{
						AllFieldSql += "" + tableName + ".brand_type = ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("asset_model_type"))
					{
						AllFieldSql += "" + tableName + ".asset_model_type = ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("asset_category_type"))
					{
						AllFieldSql += "" + tableName + ".asset_category_type = ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("model"))
					{
						AllFieldSql += "" + tableName + ".model like ?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("sl"))
					{
						AllFieldSql += "" + tableName + ".sl like ?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("transaction_cat"))
					{
						AllFieldSql += "" + tableName + ".transaction_cat = ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("lot"))
					{
						AllFieldSql += "" + tableName + ".lot like ?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("office_unit_type"))
					{
						AllFieldSql += "" + tableName + ".office_unit_type = ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("wing_type"))
					{
						AllFieldSql += "" + tableName + ".wing_type = ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("receiver_name_en"))
					{
						AllFieldSql += "" + tableName + ".receiver_name_en like ?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("receiver_name_bn"))
					{
						AllFieldSql += "" + tableName + ".receiver_name_bn like ?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("transaction_date_start"))
					{
						AllFieldSql += "" + tableName + ".transaction_date >= ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("transaction_date_end"))
					{
						AllFieldSql += "" + tableName + ".transaction_date <= ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		
		if(category == GETDTOS)
		{
			sql += " order by " + tableName + ".transaction_date desc ";
		}

		//printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}

		
		return sql;
    }


	public Asset_transactionDTO getLastByAssetAssigneeId(long assetAssigneeId) throws Exception {

		String sql = "select * from asset_transaction WHERE asset_assignee_id =" + assetAssigneeId
				+ " and isDeleted = 0 order by id desc limit 1,1";
		return ConnectionAndStatementUtil.getT(sql,this::build);
	}
				
}
	