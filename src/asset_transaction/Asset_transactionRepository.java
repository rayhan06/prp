package asset_transaction;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Asset_transactionRepository implements Repository {
	Asset_transactionDAO asset_transactionDAO = null;
	
	public void setDAO(Asset_transactionDAO asset_transactionDAO)
	{
		this.asset_transactionDAO = asset_transactionDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Asset_transactionRepository.class);
	Map<Long, Asset_transactionDTO>mapOfAsset_transactionDTOToiD;

  
	private Asset_transactionRepository(){
		asset_transactionDAO = new Asset_transactionDAO();
		mapOfAsset_transactionDTOToiD = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Asset_transactionRepository INSTANCE = new Asset_transactionRepository();
    }

    public static Asset_transactionRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Asset_transactionDTO> asset_transactionDTOs = (List<Asset_transactionDTO>)asset_transactionDAO.getAll(reloadAll);
			for(Asset_transactionDTO asset_transactionDTO : asset_transactionDTOs) {
				Asset_transactionDTO oldAsset_transactionDTO = getAsset_transactionDTOByID(asset_transactionDTO.iD);
				if( oldAsset_transactionDTO != null ) {
					mapOfAsset_transactionDTOToiD.remove(oldAsset_transactionDTO.iD);
				
					
				}
				if(asset_transactionDTO.isDeleted == 0) 
				{
					
					mapOfAsset_transactionDTOToiD.put(asset_transactionDTO.iD, asset_transactionDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Asset_transactionDTO> getAsset_transactionList() {
		List <Asset_transactionDTO> asset_transactions = new ArrayList<Asset_transactionDTO>(this.mapOfAsset_transactionDTOToiD.values());
		return asset_transactions;
	}
	
	
	public Asset_transactionDTO getAsset_transactionDTOByID( long ID){
		return mapOfAsset_transactionDTOToiD.get(ID);
	}

	
	@Override
	public String getTableName() {
		return "asset_transaction";
	}
}


