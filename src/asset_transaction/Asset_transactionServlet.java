package asset_transaction;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.*;
import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;


import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;
import common.ApiResponse;
import com.google.gson.Gson;
import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Asset_transactionServlet
 */
@WebServlet("/Asset_transactionServlet")
@MultipartConfig
public class Asset_transactionServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Asset_transactionServlet.class);

    String tableName = "asset_transaction";

	Asset_transactionDAO asset_transactionDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Asset_transactionServlet() 
	{
        super();
    	try
    	{
			asset_transactionDAO = new Asset_transactionDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(asset_transactionDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_TRANSACTION_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_TRANSACTION_UPDATE))
				{
					getAsset_transaction(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_TRANSACTION_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchAsset_transaction(request, response, isPermanentTable, filter);
						}
						else
						{
							searchAsset_transaction(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchAsset_transaction(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_TRANSACTION_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_TRANSACTION_ADD))
				{
					System.out.println("going to  addAsset_transaction ");
					addAsset_transaction(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addAsset_transaction ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_TRANSACTION_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addAsset_transaction ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_TRANSACTION_UPDATE))
				{					
					addAsset_transaction(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_TRANSACTION_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_TRANSACTION_SEARCH))
				{
					searchAsset_transaction(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Asset_transactionDTO asset_transactionDTO = (Asset_transactionDTO)asset_transactionDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(asset_transactionDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addAsset_transaction(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		/*
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addAsset_transaction");
			String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
			Asset_transactionDTO asset_transactionDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				asset_transactionDTO = new Asset_transactionDTO();
			}
			else
			{
				asset_transactionDTO = (Asset_transactionDTO)asset_transactionDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("tableName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("tableName = " + Value);
			if(Value != null)
			{
				asset_transactionDTO.tableName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("brandType");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("brandType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_transactionDTO.brandType = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("assetModelType");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("assetModelType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_transactionDTO.assetModelId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("assetCategoryType");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("assetCategoryType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_transactionDTO.assetCategoryId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("model");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("model = " + Value);
			if(Value != null)
			{
				asset_transactionDTO.model = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("sl");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("sl = " + Value);
			if(Value != null)
			{
				asset_transactionDTO.sl = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("transactionCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("transactionCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_transactionDTO.transactionCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("lot");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("lot = " + Value);
			if(Value != null)
			{
				asset_transactionDTO.lot = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("officeUnitType");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("officeUnitType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_transactionDTO.officeUnitType = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("wingType");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("wingType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_transactionDTO.wingType = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("assignedUserId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("assignedUserId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_transactionDTO.assignedUserId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("assignedOrganogramId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("assignedOrganogramId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_transactionDTO.assignedOrganogramId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("receiverNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("receiverNameEn = " + Value);
			if(Value != null)
			{
				asset_transactionDTO.receiverNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("receiverNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("receiverNameBn = " + Value);
			if(Value != null)
			{
				asset_transactionDTO.receiverNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("transactionDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("transactionDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try 
				{
					Date d = f.parse(Value);
					asset_transactionDTO.transactionDate = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
					throw new Exception(LM.getText(LC.ASSET_TRANSACTION_ADD_TRANSACTIONDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				asset_transactionDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				asset_transactionDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				asset_transactionDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{				
				asset_transactionDTO.insertionDate = TimeConverter.getToday();
			}			


			asset_transactionDTO.lastModifierUser = userDTO.userName;

			
			System.out.println("Done adding  addAsset_transaction dto = " + asset_transactionDTO);
			long returnedID = -1;
			
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				asset_transactionDAO.setIsDeleted(asset_transactionDTO.iD, CommonDTO.OUTDATED);
				returnedID = asset_transactionDAO.add(asset_transactionDTO);
				asset_transactionDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = asset_transactionDAO.manageWriteOperations(asset_transactionDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = asset_transactionDAO.manageWriteOperations(asset_transactionDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
		
			if(!isPermanentTable)
			{
				commonRequestHandler.validate(asset_transactionDAO.getDTOByID(returnedID), request, response, userDTO);
			}
			else
			{
				ApiResponse.sendSuccessResponse(response, "Asset_transactionServlet?actionType=search");
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			ApiResponse.sendErrorResponse(response, e.getMessage());
		}*/
	}
	
	



	
	
	

	private void getAsset_transaction(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getAsset_transaction");
		Asset_transactionDTO asset_transactionDTO = null;
		try 
		{
			asset_transactionDTO = (Asset_transactionDTO)asset_transactionDAO.getDTOByID(id);
			request.setAttribute("ID", asset_transactionDTO.iD);
			request.setAttribute("asset_transactionDTO",asset_transactionDTO);
			request.setAttribute("asset_transactionDAO",asset_transactionDAO);
			
			String URL= "";
			String getBodyOnly = request.getParameter("getBodyOnly");
						
			if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
			{
				URL = "asset_transaction/asset_transactionEditBody.jsp?actionType=edit";
			}
			else
			{
				URL = "asset_transaction/asset_transactionEdit.jsp?actionType=edit";
			}				
						
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getAsset_transaction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getAsset_transaction(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchAsset_transaction(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchAsset_transaction 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		request.setAttribute("navigator", "navASSET_TRANSACTION");
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			"navASSET_TRANSACTION",
			request,
			asset_transactionDAO,
			"viewASSET_TRANSACTION",
			null,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("asset_transactionDAO",asset_transactionDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to approval_path/commonApproval.jsp");
	        	rd = request.getRequestDispatcher("approval_path/commonApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to asset_transaction/asset_transactionApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("asset_transaction/asset_transactionApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to pb/search.jsp");
	        	rd = request.getRequestDispatcher("pb/search.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to asset_transaction/asset_transactionSearchForm.jsp");
	        	rd = request.getRequestDispatcher("asset_transaction/asset_transactionSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

