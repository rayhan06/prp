package asset_transaction;
import java.util.*;

import asset_model.AssetAssigneeDTO;
import asset_model.Asset_modelDTO;
import util.*;
import workflow.WorkflowController; 


public class Asset_transactionDTO extends CommonDTO
{

    public String tableName = "";
	public int brandType = -1;
	public long assetModelId = -1;
	public long assetAssigneeId = -1;
	public long assetCategoryId = -1;
    public String model = "";
    public String sl = "";
	public int transactionCat = -1;
    public String lot = "";
    public long maintenance_vendor_id = -1;
    public double cost = 0;

	public long assignedOrganogramId = -1;
	public long employeeRecordId = -1;
  
	public long transactionDate = System.currentTimeMillis();
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
    
    public String comment = "";
    
    public int quantity = 1;

    public int lastActiveStatus = -1;
    public long toOrgId = -1;
    
    public static final int STOCK_IN = AssetAssigneeDTO.STOCK_IN;
    public static final int ASSIGN = AssetAssigneeDTO.ASSIGNED;
    public static final int UNASSIGN = AssetAssigneeDTO.NOT_ASSIGNED;
    public static final int ACTIVATE = AssetAssigneeDTO.ACTIVATE;
    public static final int DEACTIVATE = AssetAssigneeDTO.DEACTIVATE;
	public static final int WARRANTY = AssetAssigneeDTO.WARRANTY;
	public static final int SEND_TO_REPAIR = AssetAssigneeDTO.SEND_TO_REPAIR;
	public static final int CONDEMN = AssetAssigneeDTO.CONDEMNED;
	public static final int RELEASE = AssetAssigneeDTO.RELEASED;
	public static final int EMPLOYEE_LEFT_POST = AssetAssigneeDTO.EMPLOYEE_LEFT_POST;
	public static final int EMPLOYEE_JOINED_POST = AssetAssigneeDTO.EMPLOYEE_JOINED_POST;
	public static final int ASSET_TRANFER = AssetAssigneeDTO.ASSET_TRANFER;
    
    
    public Asset_transactionDTO()
    {
    	
    }
	
    public Asset_transactionDTO(Asset_modelDTO asset_modelDTO, AssetAssigneeDTO assetAssigneeDTO, int transactionCat)
    {
    	assetModelId = asset_modelDTO.iD;
    	this.transactionCat = transactionCat;
    	assetAssigneeId = assetAssigneeDTO.iD;
    	model = asset_modelDTO.nameEn;
    	lot = asset_modelDTO.lot;
    	brandType = asset_modelDTO.brandType;
    	assetCategoryId = asset_modelDTO.assetCategoryType;

    	assignedOrganogramId = assetAssigneeDTO.assignedOrganogramId;
    	employeeRecordId = assetAssigneeDTO.employeeRecordId;

    	sl = assetAssigneeDTO.sl;
    	
    	insertedByUserId = assetAssigneeDTO.purchasingUserId;
    	insertedByOrganogramId = assetAssigneeDTO.purchasingOrganogramId;
    	insertionDate = assetAssigneeDTO.deliveryDate;
    }
    
    public Asset_transactionDTO(AssetAssigneeDTO assetAssigneeDTO, long fromOrgId, long toOrgId)
    {
    	assetModelId = assetAssigneeDTO.assetModelId;
    	this.transactionCat = ASSET_TRANFER;
    	assetAssigneeId = assetAssigneeDTO.iD;
    	model = assetAssigneeDTO.model;

    	assetCategoryId = assetAssigneeDTO.assetCategoryType;

    	assignedOrganogramId = fromOrgId;
    	this.toOrgId = toOrgId;
    	employeeRecordId = assetAssigneeDTO.employeeRecordId;

    	sl = assetAssigneeDTO.sl;
    	
    	insertedByUserId = assetAssigneeDTO.purchasingUserId;
    	insertedByOrganogramId = assetAssigneeDTO.purchasingOrganogramId;
    	insertionDate = assetAssigneeDTO.deliveryDate;
    }

	public Asset_transactionDTO(Asset_modelDTO asset_modelDTO, AssetAssigneeDTO assetAssigneeDTO, int transactionCat, String comment) {
		this(asset_modelDTO, assetAssigneeDTO, transactionCat);
		this.comment = comment;
	}

	public Asset_transactionDTO(Asset_modelDTO asset_modelDTO, AssetAssigneeDTO assetAssigneeDTO, int transactionCat, long vendor_id)
	{
		this(asset_modelDTO, assetAssigneeDTO, transactionCat);

		maintenance_vendor_id = vendor_id;

	}
	public Asset_transactionDTO(Asset_modelDTO asset_modelDTO, AssetAssigneeDTO assetAssigneeDTO, int transactionCat, long vendor_id, int assignmentStatus, String comment)
	{
		this(asset_modelDTO, assetAssigneeDTO, transactionCat);

		maintenance_vendor_id = vendor_id;
		lastActiveStatus = assignmentStatus;
		this.comment = comment;
	}
    
    public Asset_transactionDTO(Asset_modelDTO asset_modelDTO,  int transactionCat)
    {
    	assetModelId = asset_modelDTO.iD;
    	this.transactionCat = transactionCat;
    	model = asset_modelDTO.nameEn;
    	lot = asset_modelDTO.lot;
    	brandType = asset_modelDTO.brandType;
    	assetCategoryId = asset_modelDTO.assetCategoryType;
    	
    	insertedByUserId = asset_modelDTO.insertedByUserId;
    	insertedByOrganogramId = asset_modelDTO.insertedByOrganogramId;
    	insertionDate = asset_modelDTO.insertionDate;
    }
	
    @Override
	public String toString() {
            return "$Asset_transactionDTO[" +
            " iD = " + iD +
            " tableName = " + tableName +
            " brandType = " + brandType +
            " assetModelType = " + assetModelId +
            " assetCategoryType = " + assetCategoryId +
            " model = " + model +
            " sl = " + sl +
            " transactionCat = " + transactionCat +
            " lot = " + lot +


            " assignedOrganogramId = " + assignedOrganogramId +

            " transactionDate = " + transactionDate +
            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}