package feedback_question;

import employee_records.Employee_recordsDTO;

public class FeedbackSummaryDTO {
    public Employee_recordsDTO employee_recordsDTO;
    public int totalPoint = 0;
    public int totalQuestion = 0;
    public String metadata1 = "";
    public String metadata2 = "";

    @Override
    public String toString() {
        return "FeedbackSummaryDTO{" +
                "employee_recordsDTO=" + employee_recordsDTO +
                ", totalPoint=" + totalPoint +
                ", totalQuestion=" + totalQuestion +
                ", metadata1='" + metadata1 + '\'' +
                ", metadata2='" + metadata2 + '\'' +
                '}';
    }


}
