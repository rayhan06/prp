package feedback_question;

import common.BaseServlet;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;


@WebServlet("/Feedback_questionServlet")
@MultipartConfig
public class  Feedback_questionServlet extends BaseServlet {
    public static final Logger logger = Logger.getLogger(Feedback_questionServlet.class);

    @Override
    public String getTableName() {
        return "feedback_question";
    }

    @Override
    public String getServletName() {
        return "Feedback_questionServlet";
    }

    @Override
    public Feedback_questionDAO getCommonDAOService() {
        return Feedback_questionDAO.getInstance();
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = userDTO.languageID == CommonConstant.Language_ID_English;
        long requestTime = Calendar.getInstance().getTimeInMillis();

        Feedback_questionDTO feedback_questionDTO = new Feedback_questionDTO();

        feedback_questionDTO.insertBy = userDTO.userName;
        feedback_questionDTO.insertionDate = requestTime;

        feedback_questionDTO.modifiedBy = userDTO.userName;
        feedback_questionDTO.lastModificationTime = requestTime;

        feedback_questionDTO.questionEng = Jsoup.clean(request.getParameter("question_eng"), Whitelist.simpleText());
        if (feedback_questionDTO.questionEng.trim().isEmpty())
            throw new Exception(
                    isLangEng ? "No English description provided" : "কোন ইংরেজি বিবিরণ দেওয়া হয়নি"
            );

        feedback_questionDTO.questionBng = Jsoup.clean(request.getParameter("question_bng"), Whitelist.simpleText());
        if (feedback_questionDTO.questionBng.trim().isEmpty())
            throw new Exception(
                    isLangEng ? "No Bangla description provided" : "কোন বাংলা বিবিরণ দেওয়া হয়নি"
            );

        getCommonDAOService().add(feedback_questionDTO);
        return feedback_questionDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.FEEDBACK_QUESTION_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.FEEDBACK_QUESTION_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.FEEDBACK_QUESTION_SEARCH};
    }


    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Feedback_questionServlet.class;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            if ("getFeedbacks".equals(actionType)) {
                String ID = request.getParameter("ID");
                long trainingCalendarId = Long.parseLong(ID);
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FEEDBACK_QUESTION_ADD)) {
                    request.setAttribute("Feedback", Feedback_questionDAO.getInstance().getFeedbacksForTraining(trainingCalendarId));
                    request.getRequestDispatcher("feedback_question_answer/feedbackView.jsp").forward(request, response);
                    return;
                }
            } else {
                super.doGet(request, response);
                return;
            }
        } catch (Exception ex) {
            logger.debug(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);

    }


}

