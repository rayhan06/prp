package feedback_question;

import util.CommonMaps;


public class Feedback_questionMAPS extends CommonMaps
{	
	public Feedback_questionMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("qUESTIONEN".toLowerCase(), "qUESTIONEN".toLowerCase());
		java_DTO_map.put("qUESTIONBN".toLowerCase(), "qUESTIONBN".toLowerCase());
		java_DTO_map.put("iNSERTIONDATE".toLowerCase(), "iNSERTIONDATE".toLowerCase());
		java_DTO_map.put("lASTMODIFICATIONTIME".toLowerCase(), "lASTMODIFICATIONTIME".toLowerCase());
		java_DTO_map.put("iNSERTBY".toLowerCase(), "iNSERTBY".toLowerCase());
		java_DTO_map.put("mODIFIEDBY".toLowerCase(), "mODIFIEDBY".toLowerCase());
		java_DTO_map.put("iSDELETED".toLowerCase(), "iSDELETED".toLowerCase());

		java_SQL_map.put("QUESTION_EN".toLowerCase(), "qUESTIONEN".toLowerCase());
		java_SQL_map.put("QUESTION_BN".toLowerCase(), "qUESTIONBN".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("QUESTION EN".toLowerCase(), "qUESTIONEN".toLowerCase());
		java_Text_map.put("QUESTION BN".toLowerCase(), "qUESTIONBN".toLowerCase());
		java_Text_map.put("INSERTION DATE".toLowerCase(), "iNSERTIONDATE".toLowerCase());
		java_Text_map.put("LASTMODIFICATIONTIME".toLowerCase(), "lASTMODIFICATIONTIME".toLowerCase());
		java_Text_map.put("INSERT BY".toLowerCase(), "iNSERTBY".toLowerCase());
		java_Text_map.put("MODIFIED BY".toLowerCase(), "mODIFIEDBY".toLowerCase());
		java_Text_map.put("IS DELETED".toLowerCase(), "iSDELETED".toLowerCase());
			
	}

}