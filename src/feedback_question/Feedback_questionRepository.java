package feedback_question;

import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class Feedback_questionRepository implements Repository {

    private final Feedback_questionDAO feedback_questionDAO;

    private final Map<Long, Feedback_questionDTO> mapById;
    private List<Feedback_questionDTO> feedbackQuestionDTOList;

    private Feedback_questionRepository() {
        mapById = new ConcurrentHashMap<>();
        feedback_questionDAO = Feedback_questionDAO.getInstance();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        static final Feedback_questionRepository INSTANCE = new Feedback_questionRepository();
    }

    public static Feedback_questionRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        List<Feedback_questionDTO> dtoList = feedback_questionDAO.getAllFeedback_question(reloadAll);
        dtoList.stream()
                .peek(this::removeIfPresent)
                .forEach(dto -> mapById.put(dto.iD, dto));
        feedbackQuestionDTOList = new ArrayList<>(mapById.values());
        feedbackQuestionDTOList.sort(Comparator.comparingLong(o -> o.iD));
    }

    private void removeIfPresent(Feedback_questionDTO dto) {
        if (mapById.get(dto.iD) != null) {
            mapById.remove(dto.iD);
        }
    }

    public List<Feedback_questionDTO> getFeedback_questionList() {
        return feedbackQuestionDTOList;
    }

    public Feedback_questionDTO getById(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "FQR")) {
                if (mapById.get(id) == null) {
                    Feedback_questionDTO dto = feedback_questionDAO.getDTOByID(id);
                    if (dto != null) {
                        mapById.put(dto.iD, dto);
                    }
                }
            }
        }
        return mapById.get(id);
    }

    @Override
    public String getTableName() {
        return "feedback_question";
    }
}


