package feedback_question;

import common.CommonDAOService;
import dbm.DBMR;
import employee_records.Employee_recordsDAO;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused"})
public class Feedback_questionDAO implements CommonDAOService<Feedback_questionDTO> {

    private static final Logger logger = Logger.getLogger(Feedback_questionDAO.class);

    private static final String addSqlQuery = "INSERT INTO feedback_question (QUESTION_EN,QUESTION_BN,LASTMODIFICATIONTIME," +
            "MODIFIED_BY,INSERT_BY,INSERTION_DATE,isDeleted,ID) values (?,?,?,?,?,?,?,?)";

    private static final String updateSqlQuery = "UPDATE feedback_question SET QUESTION_EN = ?,QUESTION_BN = ?," +
            "LASTMODIFICATIONTIME=?,MODIFIED_BY=? WHERE ID =?";
    private static final String getFeedbacksQuery = "select training_calendar_details.employee_records_id, feedback_question_answer.feedback_ques_ans_point_cat,training_calendar_details.metadata_1,training_calendar_details.metadata_2\n" +
            "from training_calender\n" +
            "join tran_cal_fb_ques_mapping on tran_cal_fb_ques_mapping.training_calendar_id = training_calender.id\n" +
            "join feedback_question_answer on feedback_question_answer.tran_cal_fb_ques_mapping_id = tran_cal_fb_ques_mapping.id\n" +
            "join training_calendar_details  on feedback_question_answer.training_calendar_details_id = training_calendar_details.id\n" +
            "where training_calender.id=%d;";
    private static final Map<String, String> searchMap = new HashMap<>();


    private Feedback_questionDAO() {
        searchMap.put("QUESTION_EN", " and QUESTION_EN like ?");
        searchMap.put("QUESTION_BN", " and QUESTION_BN like ?");
    }

    private static class LazyLoader {
        static final Feedback_questionDAO INSTANCE = new Feedback_questionDAO();
    }

    public static Feedback_questionDAO getInstance() {
        return Feedback_questionDAO.LazyLoader.INSTANCE;
    }

    @Override
    public Feedback_questionDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Feedback_questionDTO dto = new Feedback_questionDTO();
            dto.iD = rs.getLong("ID");
            dto.questionEng = rs.getString("QUESTION_EN");
            dto.questionBng = rs.getString("QUESTION_BN");
            dto.insertionDate = rs.getLong("INSERTION_DATE");
            dto.lastModificationTime = rs.getLong("LASTMODIFICATIONTIME");
            dto.insertBy = rs.getString("INSERT_BY");
            dto.modifiedBy = rs.getString("MODIFIED_BY");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "feedback_question";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public void set(PreparedStatement ps, Feedback_questionDTO feedback_questionDTO, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setObject(++index, feedback_questionDTO.questionEng);
        ps.setObject(++index, feedback_questionDTO.questionBng);
        ps.setObject(++index, feedback_questionDTO.lastModificationTime);
        ps.setObject(++index, feedback_questionDTO.modifiedBy);
        if (isInsert) {
            ps.setObject(++index, feedback_questionDTO.insertBy);
            ps.setObject(++index, feedback_questionDTO.insertionDate);
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, feedback_questionDTO.iD);
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Feedback_questionDTO) commonDTO, addSqlQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Feedback_questionDTO) commonDTO, updateSqlQuery, false);
    }

    public Feedback_questionDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }

    public List<Feedback_questionDTO> getAllFeedback_question(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }


    public List<FeedbackSummaryDTO> getFeedbacksForTraining(long trainingCalendarId) {
        Connection connection = null;
        ResultSet rs;
        Statement stmt = null;
        long empId;
        int point;
        int totalQuestion;

        List<FeedbackSummaryDTO> feedbackSummaryDTOs = new ArrayList<>();

        try {


            String query = String.format(getFeedbacksQuery, trainingCalendarId);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            logger.debug(query);

            rs = stmt.executeQuery(query);
            HashMap<Long, FeedbackSummaryDTO> feedbacks = new HashMap<>();
            while (rs.next()) {
                empId = rs.getInt(1);
                if (feedbacks.get(empId) == null) {
                    feedbacks.put(empId, new FeedbackSummaryDTO());
                }
                feedbacks.get(empId).totalPoint += rs.getInt(2);
                feedbacks.get(empId).totalQuestion += 1;
                feedbacks.get(empId).metadata1 = rs.getString(3);
                feedbacks.get(empId).metadata2 = rs.getString(4);
            }
            for (Long keyEmpId : feedbacks.keySet()) {
                FeedbackSummaryDTO feedbackSummaryDTO = feedbacks.get(keyEmpId);
                feedbackSummaryDTO.employee_recordsDTO = new Employee_recordsDAO().getEmployee_recordsDTOByID(keyEmpId);
                feedbackSummaryDTOs.add(feedbackSummaryDTO);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.error(e);
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
                logger.error(ex2);
            }
        }

        return feedbackSummaryDTOs;
    }

}
	