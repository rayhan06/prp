package feedback_question;

import util.CommonDTO;


public class Feedback_questionDTO extends CommonDTO {
    public String questionEng = "";
    public String questionBng = "";
    public long insertionDate = 0;
    public long lastModificationTime = 0;
    public String insertBy = "";
    public String modifiedBy = "";
    public int isDeleted = 0;

    @Override
    public String toString() {
        return "Feedback_questionDTO{" +
                "questionEng='" + questionEng + '\'' +
                ", questionBng='" + questionBng + '\'' +
                ", insertionDate=" + insertionDate +
                ", lastModificationTime=" + lastModificationTime +
                ", insertBy='" + insertBy + '\'' +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", isDeleted=" + isDeleted +
                '}';
    }
}