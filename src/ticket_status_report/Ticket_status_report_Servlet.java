package ticket_status_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Ticket_status_report_Servlet")
public class Ticket_status_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","office_unit_type","=","","int","","","any","officeUnitType", LC.HM_OFFICE_UNIT + ""},		
		{"criteria","","organogram_type","=","AND","int","","","any","organogramType", LC.HM_NAME + ""},		
		{"criteria","","ticket_status_cat","=","AND","int","","","any","ticketStatusCat", LC.HM_STATUS + ""},		
		{"criteria","","min_date",">=","AND","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","max_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""}		
	};
	
	String[][] Display =
	{
		{"display","","office_unit_type","office_unit",""},		
		{"display","","organogram_type","organogram",""},		
		{"display","","ticket_status_cat","cat",""},		
		{"display","","ticket_count","int",""}		
	};
	
	String GroupBy = "";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Ticket_status_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "(SELECT ticket_status_cat, employee_offices.office_unit_id AS office_unit_type, office_unit_organogram_id AS organogram_type, COUNT(support_ticket.id) AS ticket_count, MIN(insertion_date) AS min_date, MAX(insertion_date) AS max_date FROM support_ticket JOIN employee_offices ON employee_offices.office_unit_organogram_id = support_ticket.issue_raiser_organogram_id GROUP BY ticket_status_cat, office_unit_organogram_id, office_unit_type) AS innerq";

		Display[0][4] = LM.getText(LC.TICKET_STATUS_REPORT_SELECT_OFFICEUNITTYPE, loginDTO);
		Display[1][4] = LM.getText(LC.HM_NAME, loginDTO);
		Display[2][4] = LM.getText(LC.SUPPORT_TICKET_ADD_TICKETSTATUSCAT, loginDTO);
		Display[3][4] = LM.getText(LC.TICKET_STATUS_REPORT_SELECT_TICKETCOUNT, loginDTO);

		
		String reportName = LM.getText(LC.TICKET_STATUS_REPORT_OTHER_TICKET_STATUS_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "ticket_status_report",
				MenuConstants.TICKET_STATUS_REPORT_DETAILS, language, reportName, "ticket_status_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
