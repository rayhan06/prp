package pi_package_auctioneer_items;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Pi_package_auctioneer_itemsRepository implements Repository {
	Pi_package_auctioneer_itemsDAO pi_package_auctioneer_itemsDAO = null;
	
	static Logger logger = Logger.getLogger(Pi_package_auctioneer_itemsRepository.class);
	Map<Long, Pi_package_auctioneer_itemsDTO>mapOfPi_package_auctioneer_itemsDTOToiD;
	Gson gson;

  
	private Pi_package_auctioneer_itemsRepository(){
		pi_package_auctioneer_itemsDAO = Pi_package_auctioneer_itemsDAO.getInstance();
		mapOfPi_package_auctioneer_itemsDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Pi_package_auctioneer_itemsRepository INSTANCE = new Pi_package_auctioneer_itemsRepository();
    }

    public static Pi_package_auctioneer_itemsRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Pi_package_auctioneer_itemsDTO> pi_package_auctioneer_itemsDTOs = pi_package_auctioneer_itemsDAO.getAllDTOs(reloadAll);
			for(Pi_package_auctioneer_itemsDTO pi_package_auctioneer_itemsDTO : pi_package_auctioneer_itemsDTOs) {
				Pi_package_auctioneer_itemsDTO oldPi_package_auctioneer_itemsDTO = getPi_package_auctioneer_itemsDTOByiD(pi_package_auctioneer_itemsDTO.iD);
				if( oldPi_package_auctioneer_itemsDTO != null ) {
					mapOfPi_package_auctioneer_itemsDTOToiD.remove(oldPi_package_auctioneer_itemsDTO.iD);
				
					
				}
				if(pi_package_auctioneer_itemsDTO.isDeleted == 0) 
				{
					
					mapOfPi_package_auctioneer_itemsDTOToiD.put(pi_package_auctioneer_itemsDTO.iD, pi_package_auctioneer_itemsDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Pi_package_auctioneer_itemsDTO clone(Pi_package_auctioneer_itemsDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Pi_package_auctioneer_itemsDTO.class);
	}
	
	
	public List<Pi_package_auctioneer_itemsDTO> getPi_package_auctioneer_itemsList() {
		List <Pi_package_auctioneer_itemsDTO> pi_package_auctioneer_itemss = new ArrayList<Pi_package_auctioneer_itemsDTO>(this.mapOfPi_package_auctioneer_itemsDTOToiD.values());
		return pi_package_auctioneer_itemss;
	}
	
	public List<Pi_package_auctioneer_itemsDTO> copyPi_package_auctioneer_itemsList() {
		List <Pi_package_auctioneer_itemsDTO> pi_package_auctioneer_itemss = getPi_package_auctioneer_itemsList();
		return pi_package_auctioneer_itemss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Pi_package_auctioneer_itemsDTO getPi_package_auctioneer_itemsDTOByiD( long iD){
		return mapOfPi_package_auctioneer_itemsDTOToiD.get(iD);
	}
	
	public Pi_package_auctioneer_itemsDTO copyPi_package_auctioneer_itemsDTOByiD( long iD){
		return clone(mapOfPi_package_auctioneer_itemsDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return pi_package_auctioneer_itemsDAO.getTableName();
	}
}


