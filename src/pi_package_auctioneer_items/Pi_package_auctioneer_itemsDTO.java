package pi_package_auctioneer_items;
import java.util.*; 
import util.*; 


public class Pi_package_auctioneer_itemsDTO extends CommonDTO
{

	public long piPackageAuctioneerId = -1;
	public long piPackageAuctioneerChildrenId = -1;
	public long piAuctionId = -1;
	public long fiscalYearId = -1;
	public long productId = -1;
	public double price = -1;
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";

	public long actualAuctioneerId = -1;
	
	
    @Override
	public String toString() {
            return "$Pi_package_auctioneer_itemsDTO[" +
            " iD = " + iD +
            " piPackageAuctioneerId = " + piPackageAuctioneerId +
            " piPackageAuctioneerChildrenId = " + piPackageAuctioneerChildrenId +
            " piAuctionId = " + piAuctionId +
            " fiscalYearId = " + fiscalYearId +
            " productId = " + productId +
            " price = " + price +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
			" actualAuctioneerId = " + actualAuctioneerId +
            "]";
    }

}