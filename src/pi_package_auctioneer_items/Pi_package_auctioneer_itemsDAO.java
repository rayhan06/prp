package pi_package_auctioneer_items;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pi_package_auctioneer.Pi_package_auctioneerDTO;
import pi_package_vendor.Pi_package_vendorDTO;
import pi_package_vendor_items.Pi_package_vendor_itemsDTO;
import user.UserDTO;
import util.*;
import pb.*;

public class Pi_package_auctioneer_itemsDAO  implements CommonDAOService<Pi_package_auctioneer_itemsDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private static final String getByAuctioneerAndAuctionPackageId =
			"SELECT * FROM pi_package_auctioneer_items WHERE pi_package_auctioneer_id = %d AND pi_auction_id = %d " +
					"  AND isDeleted = 0 ORDER BY lastModificationTime DESC";

	private static final String deletePiPackageAuctioneerChildrenItems =
			"UPDATE pi_package_auctioneer_items SET isDeleted = %d,modified_by = %d,lastModificationTime = %d " +
					" WHERE pi_package_auctioneer_id = %d AND pi_auction_id = %d  ";

	private Pi_package_auctioneer_itemsDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"pi_package_auctioneer_id",
			"pi_package_auctioneer_children_id",
			"pi_auction_id",
			"fiscal_year_id",
			"product_id",
			"price",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"search_column",
			"actual_auctioneer_id",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Pi_package_auctioneer_itemsDAO INSTANCE = new Pi_package_auctioneer_itemsDAO();
	}

	public static Pi_package_auctioneer_itemsDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Pi_package_auctioneer_itemsDTO pi_package_auctioneer_itemsDTO)
	{
		pi_package_auctioneer_itemsDTO.searchColumn = "";
	}
	
	@Override
	public void set(PreparedStatement ps, Pi_package_auctioneer_itemsDTO pi_package_auctioneer_itemsDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(pi_package_auctioneer_itemsDTO);
		if(isInsert)
		{
			ps.setObject(++index,pi_package_auctioneer_itemsDTO.iD);
		}
		ps.setObject(++index,pi_package_auctioneer_itemsDTO.piPackageAuctioneerId);
		ps.setObject(++index,pi_package_auctioneer_itemsDTO.piPackageAuctioneerChildrenId);
		ps.setObject(++index,pi_package_auctioneer_itemsDTO.piAuctionId);
		ps.setObject(++index,pi_package_auctioneer_itemsDTO.fiscalYearId);
		ps.setObject(++index,pi_package_auctioneer_itemsDTO.productId);
		ps.setObject(++index,pi_package_auctioneer_itemsDTO.price);
		ps.setObject(++index,pi_package_auctioneer_itemsDTO.insertionDate);
		ps.setObject(++index,pi_package_auctioneer_itemsDTO.insertedBy);
		ps.setObject(++index,pi_package_auctioneer_itemsDTO.modifiedBy);
		ps.setObject(++index,pi_package_auctioneer_itemsDTO.searchColumn);
		ps.setObject(++index,pi_package_auctioneer_itemsDTO.actualAuctioneerId);
		if(isInsert)
		{
			ps.setObject(++index,pi_package_auctioneer_itemsDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,pi_package_auctioneer_itemsDTO.iD);
		}
	}
	
	@Override
	public Pi_package_auctioneer_itemsDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Pi_package_auctioneer_itemsDTO pi_package_auctioneer_itemsDTO = new Pi_package_auctioneer_itemsDTO();
			int i = 0;
			pi_package_auctioneer_itemsDTO.iD = rs.getLong(columnNames[i++]);
			pi_package_auctioneer_itemsDTO.piPackageAuctioneerId = rs.getLong(columnNames[i++]);
			pi_package_auctioneer_itemsDTO.piPackageAuctioneerChildrenId = rs.getLong(columnNames[i++]);
			pi_package_auctioneer_itemsDTO.piAuctionId = rs.getLong(columnNames[i++]);
			pi_package_auctioneer_itemsDTO.fiscalYearId = rs.getLong(columnNames[i++]);
			pi_package_auctioneer_itemsDTO.productId = rs.getLong(columnNames[i++]);
			pi_package_auctioneer_itemsDTO.price = rs.getDouble(columnNames[i++]);
			pi_package_auctioneer_itemsDTO.insertionDate = rs.getLong(columnNames[i++]);
			pi_package_auctioneer_itemsDTO.insertedBy = rs.getString(columnNames[i++]);
			pi_package_auctioneer_itemsDTO.modifiedBy = rs.getString(columnNames[i++]);
			pi_package_auctioneer_itemsDTO.searchColumn = rs.getString(columnNames[i++]);
			pi_package_auctioneer_itemsDTO.actualAuctioneerId = rs.getLong(columnNames[i++]);
			pi_package_auctioneer_itemsDTO.isDeleted = rs.getInt(columnNames[i++]);
			pi_package_auctioneer_itemsDTO.lastModificationTime = rs.getLong(columnNames[i++]);

			return pi_package_auctioneer_itemsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Pi_package_auctioneer_itemsDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "pi_package_auctioneer_items";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Pi_package_auctioneer_itemsDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Pi_package_auctioneer_itemsDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }

	public List<Pi_package_auctioneer_itemsDTO> getAllDTOByAuctioneerAndAuctionPackageId(long piPackageAuctioneerId, long auctionId) {
		String sql = String.format(getByAuctioneerAndAuctionPackageId, piPackageAuctioneerId,auctionId);
		return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
	}

	public Pi_package_auctioneer_itemsDTO getPiPackageItemsDtoByChildrenId(List<Pi_package_auctioneer_itemsDTO> pi_package_auctioneer_itemsDTOS,
																				   long piPackageChildrenId){

		return  pi_package_auctioneer_itemsDTOS.stream()
				.filter(dto ->  dto.piPackageAuctioneerChildrenId == piPackageChildrenId)
				.findFirst()
				.orElse(null);

	}

	public void deletePiPackageAuctioneerChildrenItems(Pi_package_auctioneerDTO pi_package_auctioneerDTO, UserDTO userDTO) {

		String sql = String.format(
				deletePiPackageAuctioneerChildrenItems, 1,
				userDTO.employee_record_id, System.currentTimeMillis(), pi_package_auctioneerDTO.iD,pi_package_auctioneerDTO.piAuctionId
		);

		boolean res = (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
			try {
				st.executeUpdate(sql);
				return true;
			} catch (SQLException ex) {
				logger.error(ex);
				return false;
			}
		});
	}


				
}
	