package pi_app_request_package_lot_item_list;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import org.intellij.lang.annotations.Language;
import pb.CatDTO;
import pb.CatRepository;
import pi_app_request_details.Pi_app_request_detailsDTO;
import procurement_goods.Procurement_goodsDTO;
import procurement_goods.Procurement_goodsRepository;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.UtilCharacter;
import vm_requisition.CommonApprovalStatus;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Pi_app_request_package_lot_item_listDAO implements CommonDAOService<Pi_app_request_package_lot_item_listDTO> {
    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private String[] columnNames = null;

    private Pi_app_request_package_lot_item_listDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "app_id",
                        "app_details_id",
                        "pi_package_item_map_id",
                        "serial_number",
                        "fiscal_year_id",
                        "package_id",
                        "lot_id",
                        "package_final_id",
                        "lot_final_id",
                        "item_group_id",
                        "item_type_id",
                        "item_id",
                        "office_unit_id",

                        "requested_quantity",
                        "requested_unit_price",
                        "requested_estimated_total_cost",

                        "approver_one_quantity",
                        "approver_one_unit_price",
                        "approver_one_estimated_total_cost",

                        "approver_two_quantity",
                        "approver_two_unit_price",
                        "approver_two_estimated_total_cost",

                        "approver_three_quantity",
                        "approver_three_unit_price",
                        "approver_three_estimated_total_cost",

                        "search_column",
                        "inserted_by",
                        "modified_by",
                        "insertion_date",
                        "isDeleted",
                        "lastModificationTime"
                };

        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("app_id", " and (fiscal_year_id = ?)");
        searchMap.put("app_details_id", " and (fiscal_year_id = ?)");
        searchMap.put("fiscal_year_id", " and (fiscal_year_id = ?)");
        searchMap.put("pi_package_final_id", " and (pi_package_final_id = ?)");
        searchMap.put("pi_lot_final_id", " and (pi_lot_final_id = ?)");
        searchMap.put("insertion_date_start", " and (insertion_date >= ?)");
        searchMap.put("insertion_date_end", " and (insertion_date <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Pi_app_request_package_lot_item_listDAO INSTANCE = new Pi_app_request_package_lot_item_listDAO();
    }

    public static Pi_app_request_package_lot_item_listDAO getInstance() {
        return Pi_app_request_package_lot_item_listDAO.LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Pi_app_request_package_lot_item_listDTO pi_app_request_package_lot_item_listDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();

        if (isInsert) {
            ps.setObject(++index, pi_app_request_package_lot_item_listDTO.iD);
        }
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.appId);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.appDetailsId);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.piPackageItemMapId);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.serialNumber);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.fiscalYearId);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.packageId);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.lotId);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.packageFinalId);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.lotFinalId);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.itemGroupId);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.itemTypeId);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.itemId);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.officeUnitId);

        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.requestedQuantity);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.requestedUnitPrice);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.requestedEstimatedTotalPrice);

        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.approverOneQuantity);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.approverOneUnitPrice);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.approverOneEstimatedTotalPrice);

        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.approverTwoQuantity);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.approverTwoUnitPrice);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.approverTwoEstimatedTotalPrice);

        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.approverThreeQuantity);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.approverThreeUnitPrice);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.approverThreeEstimatedTotalPrice);

        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.searchColumn);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.insertedBy);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.modifiedBy);
        ps.setObject(++index, pi_app_request_package_lot_item_listDTO.insertionDate);
        if (isInsert) {
            ps.setObject(++index, pi_app_request_package_lot_item_listDTO.isDeleted);
        }
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, pi_app_request_package_lot_item_listDTO.iD);
        }
    }

    @Override
    public Pi_app_request_package_lot_item_listDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Pi_app_request_package_lot_item_listDTO package_lot_item_listDTO = new Pi_app_request_package_lot_item_listDTO();
            int i = 0;
            package_lot_item_listDTO.iD = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.appId = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.appDetailsId = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.piPackageItemMapId = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.serialNumber = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.fiscalYearId = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.packageId = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.lotId = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.packageFinalId = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.lotFinalId = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.itemGroupId = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.itemTypeId = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.itemId = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.officeUnitId = rs.getLong(columnNames[i++]);

            package_lot_item_listDTO.requestedQuantity = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.requestedUnitPrice = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.requestedEstimatedTotalPrice = rs.getLong(columnNames[i++]);

            package_lot_item_listDTO.approverOneQuantity = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.approverOneUnitPrice = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.approverOneEstimatedTotalPrice = rs.getLong(columnNames[i++]);

            package_lot_item_listDTO.approverTwoQuantity = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.approverTwoUnitPrice = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.approverTwoEstimatedTotalPrice = rs.getLong(columnNames[i++]);

            package_lot_item_listDTO.approverThreeQuantity = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.approverThreeUnitPrice = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.approverThreeEstimatedTotalPrice = rs.getLong(columnNames[i++]);

            package_lot_item_listDTO.insertedBy = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.modifiedBy = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.insertionDate = rs.getLong(columnNames[i++]);
            package_lot_item_listDTO.searchColumn = rs.getString(columnNames[i++]);
            package_lot_item_listDTO.isDeleted = rs.getInt(columnNames[i++]);
            package_lot_item_listDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            return package_lot_item_listDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "pi_app_request_package_lot_item_list";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_app_request_package_lot_item_listDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_app_request_package_lot_item_listDTO) commonDTO, updateQuery, false);
    }

    public List<Pi_app_request_package_lot_item_listDTO> getDTOsByAppDetailsId(long appDetailsId){
        List<Pi_app_request_package_lot_item_listDTO> models;
        models = (List<Pi_app_request_package_lot_item_listDTO>) Pi_app_request_package_lot_item_listDAO.getInstance()
                .getDTOsByParent("app_details_id", appDetailsId);
        return models;
    }

    public List<Procurement_goodsDTO> getApprovedItemsByFiscalYearIdPackageIdLotId(long fiscalYearId, long packageId, long lotId){
        List<Procurement_goodsDTO> goods;
        List<Long> itemIds = Pi_app_request_package_lot_item_listDAO.getInstance().getAllDTOs()
                .stream().filter(dto -> dto.fiscalYearId == fiscalYearId)
                .filter(dto -> dto.packageFinalId == packageId)
                .filter(dto -> dto.lotFinalId == lotId)
                .map(dto -> dto.itemId).collect(Collectors.toList());

        goods = Procurement_goodsRepository.getInstance().getProcurement_goodsList()
                .stream().filter(dto -> itemIds.contains(dto.iD)).collect(Collectors.toList());

        return goods;
    }

    public List<Pi_app_request_package_lot_item_listDTO> getByAppId(Long appId) {
        String sql = String.format("SELECT * FROM pi_app_request_package_lot_item_list WHERE app_id = %d AND isDeleted = 0", appId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public long getTotalPriceByAppStatus(Pi_app_request_package_lot_item_listDTO estCostModel, long appStatus){
        if(CommonApprovalStatus.PENDING.getValue() == appStatus){
            return estCostModel.requestedUnitPrice * estCostModel.requestedQuantity;
        } else if(CommonApprovalStatus.REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue() == appStatus ||
                CommonApprovalStatus.SECOND_APPROVER_PENDING.getValue() == appStatus){
            return estCostModel.approverOneUnitPrice * estCostModel.approverOneQuantity;
        } else if(CommonApprovalStatus.REQUISITION_APPROVED_BY_SECOND_APPROVER.getValue() == appStatus ||
                CommonApprovalStatus.THIRD_APPROVER_PENDING.getValue() == appStatus){
            return estCostModel.approverTwoUnitPrice * estCostModel.approverTwoQuantity;
        } else if(CommonApprovalStatus.SATISFIED.getValue() == appStatus){
            return estCostModel.approverThreeUnitPrice * estCostModel.approverThreeQuantity;
        }
        return 0;
    }

    private String getMethodName(long methodValue){
        String domainName = "procurement_method";
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        return CatRepository.getInstance().getText(Language, domainName, methodValue);
    }

    public String getProcurementMethodName(Pi_app_request_detailsDTO model, long appStatus){
        if(model.procurementMethodId.equals("")){
            return "";
        }
        if(CommonApprovalStatus.PENDING.getValue() == appStatus){
            return Arrays.stream(model.procurementMethodId.split(","))
                    .map(e -> getMethodName(Long.parseLong(e.trim())))
                    .collect(Collectors.joining("/"));
        } else if(CommonApprovalStatus.REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue() == appStatus ||
                CommonApprovalStatus.SECOND_APPROVER_PENDING.getValue() == appStatus){
            return Arrays.stream(model.procurementMethodId.split(","))
                    .map(e -> getMethodName(Long.parseLong(e.trim())))
                    .collect(Collectors.joining("/"));
        } else if(CommonApprovalStatus.REQUISITION_APPROVED_BY_SECOND_APPROVER.getValue() == appStatus ||
                CommonApprovalStatus.THIRD_APPROVER_PENDING.getValue() == appStatus){
            return Arrays.stream(model.procurementMethodId.split(","))
                    .map(e -> getMethodName(Long.parseLong(e.trim())))
                    .collect(Collectors.joining("/"));
        } else if(CommonApprovalStatus.SATISFIED.getValue() == appStatus){
            return Arrays.stream(model.procurementMethodId.split(","))
                    .map(e -> getMethodName(Long.parseLong(e.trim())))
                    .collect(Collectors.joining("/"));
        }
        return "";
    }

    public String getApprovalAuthorityName(Pi_app_request_detailsDTO model, long appStatus){
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        String domainName = "pi_contract_approving_authority";
        if(CommonApprovalStatus.PENDING.getValue() == appStatus){
            return CatRepository.getInstance().getText(Language, domainName, model.contractAppAuthId);
        } else if(CommonApprovalStatus.REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue() == appStatus ||
                CommonApprovalStatus.SECOND_APPROVER_PENDING.getValue() == appStatus){
            return CatRepository.getInstance().getText(Language, domainName, model.approveOneContractAppAuthId);
        } else if(CommonApprovalStatus.REQUISITION_APPROVED_BY_SECOND_APPROVER.getValue() == appStatus ||
                CommonApprovalStatus.THIRD_APPROVER_PENDING.getValue() == appStatus){
            return CatRepository.getInstance().getText(Language, domainName, model.approveTwoContractAppAuthId);
        } else if(CommonApprovalStatus.SATISFIED.getValue() == appStatus){
            return CatRepository.getInstance().getText(Language, domainName, model.approveThreeContractAppAuthId);
        }
        return "";
    }
}
