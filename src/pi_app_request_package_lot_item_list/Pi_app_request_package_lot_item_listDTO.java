package pi_app_request_package_lot_item_list;

import util.CommonDTO;

public class Pi_app_request_package_lot_item_listDTO extends CommonDTO {
    public long appId = -1;
    public long appDetailsId = -1;
    public long piPackageItemMapId = -1;
    public long serialNumber = -1;
    public long fiscalYearId = -1;
    public long packageId = -1;
    public long lotId = -1;
    public long packageFinalId = -1;
    public long lotFinalId = -1;
    public long itemGroupId = -1;
    public long itemTypeId = -1;
    public long itemId = -1;
    public long officeUnitId = -1;

    public long requestedQuantity = -1;
    public long requestedUnitPrice = -1;
    public long requestedEstimatedTotalPrice = -1;

    public long approverOneQuantity = -1;
    public long approverOneUnitPrice = -1;
    public long approverOneEstimatedTotalPrice = -1;

    public long approverTwoQuantity = -1;
    public long approverTwoUnitPrice = -1;
    public long approverTwoEstimatedTotalPrice = -1;

    public long approverThreeQuantity = -1;
    public long approverThreeUnitPrice = -1;
    public long approverThreeEstimatedTotalPrice = -1;

    public long insertedBy = -1;
    public long modifiedBy = -1;
    public long insertionDate = -1;
}
