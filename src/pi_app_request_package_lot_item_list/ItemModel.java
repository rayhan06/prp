package pi_app_request_package_lot_item_list;

public class ItemModel {
    public long iD = -1;
    public long appId = -1;
    public long appDetailsId = -1;
    public long piPackageItemMapId = -1;
    public long serialNumber = -1;
    public long fiscalYearId = -1;
    public String fiscalYearText = "";
    public long packageId = -1;
    public String packageIdText = "";
    public long lotId = -1;
    public String lotIdText = "";
    public long packageFinalId = -1;
    public String packageFinalText = "";
    public long lotFinalId = -1;
    public String lotFinalIdText = "";
    public long itemGroupId = -1;
    public String itemGroupIdText = "";
    public long itemTypeId = -1;
    public String itemTypeIdText = "";
    public long itemId = -1;
    public String itemIdText = "";
    public long unitId = -1;
    public String unitIdText = "";
    public long officeUnitId = -1;
    public String officeUnitIdText = "";

    public long requestedQuantity = -1;
    public long requestedUnitPrice = -1;
    public long requestedEstimatedTotalPrice = -1;

    public long approverOneQuantity = -1;
    public long approverOneUnitPrice = -1;
    public long approverOneEstimatedTotalPrice = -1;

    public long approverTwoQuantity = -1;
    public long approverTwoUnitPrice = -1;
    public long approverTwoEstimatedTotalPrice = -1;

    public long approverThreeQuantity = -1;
    public long approverThreeUnitPrice = -1;
    public long approverThreeEstimatedTotalPrice = -1;
}
