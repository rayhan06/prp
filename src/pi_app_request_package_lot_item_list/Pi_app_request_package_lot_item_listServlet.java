package pi_app_request_package_lot_item_list;

import com.google.gson.Gson;
import common.ApiResponse;
import common.BaseServlet;
import common.CommonDAOService;
import fiscal_year.Fiscal_yearRepository;
import office_units.Office_unitsRepository;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pi_app_request_details.Pi_app_request_detailsDAO;
import pi_app_request_details.Pi_app_request_detailsDTO;
import pi_package_final.PiPackageLotFinalRepository;
import pi_package_final.Pi_package_finalRepository;
import procurement_goods.Procurement_goodsRepository;
import user.UserDTO;
import util.CommonDTO;
import util.CommonLoginData;
import util.HttpRequestUtils;
import util.UtilCharacter;
import vm_requisition.CommonApprovalStatus;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/Pi_app_request_package_lot_item_listServlet")
public class Pi_app_request_package_lot_item_listServlet extends BaseServlet {
    @Override
    public String getTableName() {
        return Pi_app_request_package_lot_item_listDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_app_request_package_lot_item_listServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return Pi_app_request_package_lot_item_listDAO.getInstance();
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_app_request_package_lot_item_listServlet.class;
    }

    private final Gson gson = new Gson();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        init(request);
        try {
            switch (request.getParameter("actionType")) {
                case "getApproverItemList":
                    boolean hasPermission = true;
                    if (hasPermission) {
                        String appDetails = request.getParameter("appDetailsId");
                        List<Pi_app_request_package_lot_item_listDTO> item_listDTOS = Pi_app_request_package_lot_item_listDAO
                                .getInstance().getDTOsByAppDetailsId(Long.parseLong(appDetails));
                        List<ItemModel> models = new ArrayList<>();
                        for (Pi_app_request_package_lot_item_listDTO item : item_listDTOS) {
                            ItemModel itemModel = new ItemModel();
                            itemModel.iD = item.iD;
                            itemModel.appId = item.appId;
                            itemModel.appDetailsId = item.appDetailsId;
                            itemModel.piPackageItemMapId = item.piPackageItemMapId;
                            itemModel.serialNumber = item.serialNumber;
                            itemModel.fiscalYearId = item.fiscalYearId;
                            itemModel.fiscalYearText = Fiscal_yearRepository.getInstance().getText(item.fiscalYearId, Language);
                            itemModel.packageId = item.packageId;
                            itemModel.lotId = item.lotId;
                            itemModel.packageFinalId = item.packageFinalId;
                            itemModel.packageFinalText = Pi_package_finalRepository.getInstance().getPackageNumberTextById(Language, item.packageFinalId);
                            itemModel.lotFinalId = item.lotFinalId;
                            itemModel.lotFinalIdText = PiPackageLotFinalRepository.getInstance().getLotNumberTextById(Language, item.lotFinalId);
                            itemModel.itemGroupId = item.itemGroupId;
                            itemModel.itemTypeId = item.itemTypeId;
                            itemModel.itemId = item.itemId;
                            itemModel.itemIdText = Procurement_goodsRepository.getInstance().geText(Language, item.itemId);
                            itemModel.unitId = Procurement_goodsRepository.getInstance().getItemUnitId(Language, item.itemId);
                            itemModel.unitIdText = Procurement_goodsRepository.getInstance().getItemUnitIdText(Language, item.itemId);
                            itemModel.officeUnitId = item.officeUnitId;
                            itemModel.officeUnitIdText = Office_unitsRepository.getInstance().geText(Language, item.officeUnitId);

                            itemModel.requestedQuantity = item.requestedQuantity;
                            itemModel.requestedUnitPrice = item.requestedUnitPrice;
                            itemModel.requestedEstimatedTotalPrice = item.requestedEstimatedTotalPrice;

                            itemModel.approverOneQuantity = item.approverOneQuantity;
                            itemModel.approverOneUnitPrice = item.approverOneUnitPrice;
                            itemModel.approverOneEstimatedTotalPrice = item.approverOneEstimatedTotalPrice;

                            itemModel.approverTwoQuantity = item.approverTwoQuantity;
                            itemModel.approverTwoUnitPrice = item.approverTwoUnitPrice;
                            itemModel.approverTwoEstimatedTotalPrice = item.approverTwoEstimatedTotalPrice;

                            itemModel.approverThreeQuantity = item.approverThreeQuantity;
                            itemModel.approverThreeUnitPrice = item.approverThreeUnitPrice;
                            itemModel.approverThreeEstimatedTotalPrice = item.approverThreeEstimatedTotalPrice;

                            models.add(itemModel);
                        }
                        ApiResponse.sendSuccessResponse(response, gson.toJson(models));
                        return;
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        init(request);
        try {
            switch (request.getParameter("actionType")) {
                case "saveApproverData":
                    boolean hasPermission = true;
                    if (hasPermission) {
                        Pi_app_request_package_lot_item_listDTO model;
                        int itemCount = request.getParameterValues("itemPackageLot.iD").length;
                        for (int i = 0; i < itemCount; i++) {
                            String id = request.getParameterValues("itemPackageLot.iD")[i];
                            model = Pi_app_request_package_lot_item_listDAO.getInstance().getDTOFromID(Long.parseLong(id));
                            if (model == null) {
                                ApiResponse.sendErrorResponse(response, UtilCharacter.getDataByLanguage(Language,
                                        "কিছু একটা ভুল হয়েছে!", "Something went wrong!"));
                            }

                            validateItemPackageLot(model.appDetailsId);

                            String value = "";
                            long approveLevel = 0;
                            value = request.getParameter("approveLevel");
                            if (isValueValid(value)) {
                                value = Jsoup.clean(value, Whitelist.simpleText());
                                approveLevel = Long.parseLong(value);
                            }

                            value = request.getParameterValues("itemPackageLot.quantity")[i];
                            if (isValueValid(value)) {
                                value = Jsoup.clean(value, Whitelist.simpleText());
                                if (approveLevel == 1) model.approverOneQuantity = Long.parseLong(value);
                                else if (approveLevel == 2) model.approverTwoQuantity = Long.parseLong(value);
                                else if (approveLevel == 3) model.approverThreeQuantity = Long.parseLong(value);
                            }

                            value = request.getParameterValues("itemPackageLot.unitPrice")[i];
                            if (isValueValid(value)) {
                                value = Jsoup.clean(value, Whitelist.simpleText());
                                if (approveLevel == 1) model.approverOneUnitPrice = Long.parseLong(value);
                                else if (approveLevel == 2) model.approverTwoUnitPrice = Long.parseLong(value);
                                else if (approveLevel == 3) model.approverThreeUnitPrice = Long.parseLong(value);
                            }

                            value = request.getParameterValues("itemPackageLot.totalPrice")[i];
                            if (isValueValid(value)) {
                                value = Jsoup.clean(value, Whitelist.simpleText());
                                if (approveLevel == 1) model.approverOneEstimatedTotalPrice = Long.parseLong(value);
                                else if (approveLevel == 2)
                                    model.approverTwoEstimatedTotalPrice = Long.parseLong(value);
                                else if (approveLevel == 3)
                                    model.approverThreeEstimatedTotalPrice = Long.parseLong(value);
                            }

                            Pi_app_request_package_lot_item_listDAO.getInstance().update(model);
                        }
                        ApiResponse.sendSuccessResponse(response, UtilCharacter.getDataByLanguage(Language,
                                "সফল হয়েছে", "Successful"));
                        return;
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void validateItemPackageLot(long appDetailsId) throws Exception {
        Pi_app_request_detailsDTO dto = Pi_app_request_detailsDAO.getInstance().getDTOByID(appDetailsId);
        // IF ANY PACKAGE IS ALREADY APPROVED, ITS ITEM DATA CAN NOT BE CHANGED
        if(dto != null && dto.status == CommonApprovalStatus.SATISFIED.getValue()){
            UtilCharacter.throwException("আইটেম তথ্য পরিবর্তন করা যাবে না!", "Item data can not be changed!");
        }
    }

    private boolean isValueValid(String value) {
        return value != null && !value.equals("");
    }
}
