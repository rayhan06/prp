package travel_allowance;

import common.BaseServlet;
import common.CommonDAOService;
import common.RoleEnum;
import employee_records.EmployeeFlatInfoDTO;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings({"Duplicates"})
@WebServlet("/Travel_allowanceServlet")
@MultipartConfig
public class Travel_allowanceServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Travel_allowanceServlet.class);

    @Override
    public String getTableName() {
        return Travel_allowanceDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Travel_allowanceServlet";
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.TRAVEL_ALLOWANCE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.TRAVEL_ALLOWANCE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.TRAVEL_ALLOWANCE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Travel_allowanceServlet.class;
    }

    @Override
    public Travel_allowanceDAO getCommonDAOService() {
        return Travel_allowanceDAO.getInstance();
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Travel_allowanceDTO travel_allowanceDTO;

        if (addFlag) {
            travel_allowanceDTO = new Travel_allowanceDTO();
            travel_allowanceDTO.insertedBy = userDTO.employee_record_id;
            travel_allowanceDTO.insertionDate = new Date().getTime();
        } else {
            travel_allowanceDTO = Travel_allowanceDAO.getInstance().getDTOByID(Long.parseLong(request.getParameter("iD")));
        }
        travel_allowanceDTO.modifiedBy = userDTO.employee_record_id;
        travel_allowanceDTO.lastModificationTime = new Date().getTime();
        travel_allowanceDTO.purpose = (Jsoup.clean(request.getParameter("purpose"), Whitelist.simpleText()));
        travel_allowanceDTO.startDate = Long.parseLong(Jsoup.clean(request.getParameter("startDate"), Whitelist.simpleText()));
        travel_allowanceDTO.startTimeCat = Integer.parseInt(Jsoup.clean(request.getParameter("startTimeCat"), Whitelist.simpleText()));
        travel_allowanceDTO.endDate = Long.parseLong(Jsoup.clean(request.getParameter("endDate"), Whitelist.simpleText()));
        travel_allowanceDTO.endTimeCat = Integer.parseInt(Jsoup.clean(request.getParameter("endTimeCat"), Whitelist.simpleText()));
        travel_allowanceDTO.startAddress = Jsoup.clean(request.getParameter("startAddress"), Whitelist.simpleText());
        travel_allowanceDTO.employeeRecordsId = Long.parseLong(Jsoup.clean(request.getParameter("employeeRecordsId"), Whitelist.simpleText()));
        travel_allowanceDTO.setEmployeeInfo(EmployeeFlatInfoDTO.getFlatInfoOfDefaultOffice(
                travel_allowanceDTO.employeeRecordsId, LM.getLanguage(userDTO)
        ));
        System.out.println("Done adding  addTravel_allowance dto = " + travel_allowanceDTO);

        List<TravelDestinationDTO> travelDestinationDTOList = createTravelDestinationDTOListByRequest(request, userDTO);
        if (addFlag) {
            Travel_allowanceDAO.getInstance().add(travel_allowanceDTO);
        } else {

            List<TravelDestinationDTO> oldTravelDestinationList = TravelDestinationDAO.getInstance().getByTravelAllowanceId(travel_allowanceDTO.iD);

            if (request.getParameterValues("travelDestination.iD") != null) {
                Set<Long> ids = Stream.of(request.getParameterValues("travelDestination.iD"))
                        .map(Long::parseLong)
                        .filter(e -> e > 0)
                        .collect(Collectors.toSet());
                List<TravelDestinationDTO> deletedList = oldTravelDestinationList.stream()
                        .filter(e -> !ids.contains(e.iD))
                        .peek(dto -> dto.isDeleted = 1)
                        .collect(Collectors.toList());
                if (deletedList.size() > 0) {
                    if (travelDestinationDTOList == null) {
                        travelDestinationDTOList = deletedList;
                    } else {
                        travelDestinationDTOList.addAll(deletedList);
                    }
                }
            }

            Travel_allowanceDAO.getInstance().update(travel_allowanceDTO);
        }
        if (travelDestinationDTOList != null) {
            for (TravelDestinationDTO travelDestinationDTO : travelDestinationDTOList) {
                travelDestinationDTO.travelAllowanceID = travel_allowanceDTO.iD;
                if (travelDestinationDTO.iD == -1) {
                    TravelDestinationDAO.getInstance().add(travelDestinationDTO);
                } else {
                    TravelDestinationDAO.getInstance().update(travelDestinationDTO);
                }
            }
        }
        return travel_allowanceDTO;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "search":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.TRAVEL_ALLOWANCE_SEARCH)) {
                        if (userDTO.roleID == RoleEnum.EMPLOYEE_OF_PARLIAMENT.getRoleId()) {
                            Map<String, String> extraCriteriaMap = new HashMap<>();
                            extraCriteriaMap.put("employeeRecordIdInternal", String.valueOf(userDTO.employee_record_id));
                            request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                        }
                        search(request, response);
                        return;
                    }
                    break;
                case "form":
                    logger.debug("form requested");
                    if (Utils.checkPermission(userDTO, getViewMenuConstants()) && getViewPagePermission(request)) {
                        Travel_allowanceDTO travel_allowanceDTO = Travel_allowanceDAO.getInstance().getDTOByID(getId(request));
                        request.setAttribute(BaseServlet.DTO_FOR_JSP, travel_allowanceDTO);
                        finalize(request);
                        request.getRequestDispatcher(commonPartOfDispatchURL() + "Form.jsp?ID=" + getId(request)).forward(request, response);
                        return;
                    }
                    break;
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private List<TravelDestinationDTO> createTravelDestinationDTOListByRequest(HttpServletRequest request, UserDTO userDTO) {
        List<TravelDestinationDTO> travelDestinationDTOList = new ArrayList<>();
        if (request.getParameterValues("travelDestination.iD") != null) {
            int travelDestinationItemNo = request.getParameterValues("travelDestination.iD").length;


            for (int index = 0; index < travelDestinationItemNo; index++) {
                TravelDestinationDTO travelDestinationDTO = createTravelDestinationDTOByRequestAndIndex(request, index, userDTO);
                travelDestinationDTOList.add(travelDestinationDTO);
            }

            return travelDestinationDTOList;
        }
        return null;
    }


    private TravelDestinationDTO createTravelDestinationDTOByRequestAndIndex(HttpServletRequest request, int index, UserDTO userDTO) {
        TravelDestinationDTO travelDestinationDTO;
        long id = Long.parseLong(request.getParameterValues("travelDestination.iD")[index]);
        if (id == -1) {
            travelDestinationDTO = new TravelDestinationDTO();
            travelDestinationDTO.insertedBy = userDTO.employee_record_id;
            travelDestinationDTO.insertionTime = new Date().getTime();
        } else {
            travelDestinationDTO = TravelDestinationDAO.getInstance().getDTOByID(id);
        }
        travelDestinationDTO.destinationAddress = Jsoup.clean(request.getParameterValues("travelDestination.destinationAddress")[index], Whitelist.simpleText());
        travelDestinationDTO.timeCat = Integer.parseInt(Jsoup.clean(request.getParameterValues("travelDestination.timeCat")[index], Whitelist.simpleText()));
        travelDestinationDTO.travelMediumCat = Integer.parseInt(Jsoup.clean(request.getParameterValues("travelDestination.travelMediumCat")[index], Whitelist.simpleText()));
        travelDestinationDTO.travelTypeCat = Integer.parseInt(Jsoup.clean(request.getParameterValues("travelDestination.travelTypeCat")[index], Whitelist.simpleText()));
        travelDestinationDTO.distanceOrSubCost = Double.parseDouble(Jsoup.clean(request.getParameterValues("travelDestination.distanceOrSubCost")[index], Whitelist.simpleText()));

        travelDestinationDTO.cost = travelDestinationDTO.distanceOrSubCost * 2.0;

//        travelDestinationDTO.cost = Double.parseDouble(Jsoup.clean(request.getParameterValues("travelDestination.cost")[index], Whitelist.simpleText()));
        travelDestinationDTO.reachDate = Long.parseLong(Jsoup.clean(request.getParameterValues("travelDestination.reachDate")[index], Whitelist.simpleText()));
        travelDestinationDTO.modifiedBy = userDTO.employee_record_id;
        travelDestinationDTO.lastModificationTime = new Date().getTime();
        return travelDestinationDTO;
    }


}

