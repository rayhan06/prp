package travel_allowance;

import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class Travel_allowanceRepository implements Repository {


    static Logger logger = Logger.getLogger(Travel_allowanceRepository.class);
    Map<Long, Travel_allowanceDTO> mapOfTravel_allowanceDTOToiD;


    private Travel_allowanceRepository() {
        mapOfTravel_allowanceDTOToiD = new ConcurrentHashMap<>();

        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Travel_allowanceRepository INSTANCE = new Travel_allowanceRepository();
    }

    public static Travel_allowanceRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        try {
            List<Travel_allowanceDTO> travel_allowanceDTOs = Travel_allowanceDAO.getInstance().getAllTravelAllowanceDTO_details(reloadAll);
            for (Travel_allowanceDTO travel_allowanceDTO : travel_allowanceDTOs) {
                Travel_allowanceDTO oldTravel_allowanceDTO = getTravel_allowanceDTOByID(travel_allowanceDTO.iD);
                if (oldTravel_allowanceDTO != null) {
                    mapOfTravel_allowanceDTOToiD.remove(oldTravel_allowanceDTO.iD);


                }
                if (travel_allowanceDTO.isDeleted == 0) {

                    mapOfTravel_allowanceDTOToiD.put(travel_allowanceDTO.iD, travel_allowanceDTO);

                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public List<Travel_allowanceDTO> getTravel_allowanceList() {
        return new ArrayList<>(this.mapOfTravel_allowanceDTOToiD.values());
    }


    public Travel_allowanceDTO getTravel_allowanceDTOByID(long ID) {
        return mapOfTravel_allowanceDTOToiD.get(ID);
    }


    @Override
    public String getTableName() {
        return "travel_allowance";
    }
}


