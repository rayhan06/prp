package travel_allowance;

import common.EmployeeCommonDAOService;
import org.apache.log4j.Logger;
import pb.CatDAO;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused", "Duplicates"})
public class Travel_allowanceDAO implements EmployeeCommonDAOService<Travel_allowanceDTO> {

    private static final Logger logger = Logger.getLogger(Travel_allowanceDAO.class);


    private static final String addSqlQuery = "INSERT INTO {tableName} (employee_records_id,purpose,start_date,start_time_cat,end_date,end_time_cat,"
            .concat("modified_by,start_address,employee_name_en, employee_name_bn,employee_user_name, office_unit_id, office_name_en, office_name_bn, organogram_id, organogram_name_en, organogram_name_bn,")
            .concat("search_column,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    private static final String updateSqlQuery = "UPDATE {tableName} SET employee_records_id=?,purpose=?,start_date=?,start_time_cat=?,end_date=?,end_time_cat=?,"
            .concat("modified_by=?,start_address=?,employee_name_en=?, employee_name_bn=?,employee_user_name=?, office_unit_id=?, office_name_en=?, office_name_bn=?, organogram_id=?, organogram_name_en=?, organogram_name_bn=?,search_column=?,lastModificationTime = ? WHERE ID = ?");
    private static final Map<String, String> searchMap = new HashMap<>();

    private Travel_allowanceDAO() {
        searchMap.put("start_time_cat", " AND (start_time_cat = ?)");
        searchMap.put("end_time_cat", " AND (end_time_cat = ?)");
        searchMap.put("employeeRecordId", " AND (employee_records_id = ?)");
        searchMap.put("officeUnitId", " AND (office_unit_id = ?)");
        searchMap.put("employeeRecordIdInternal", " AND (employee_records_id = ?)");
        searchMap.put("start_date_start", " and (start_date >= ?)");
        searchMap.put("start_date_end", " and (start_date <= ?)");
        searchMap.put("end_date_start", " and (end_date >= ?)");
        searchMap.put("end_date_end", " and (end_date <= ?)");
        searchMap.put("AnyField", " AND (search_column LIKE ?)");
    }

    private static class LazyLoader {
        static final Travel_allowanceDAO INSTANCE = new Travel_allowanceDAO();
    }

    public static Travel_allowanceDAO getInstance() {
        return Travel_allowanceDAO.LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Travel_allowanceDTO travel_allowanceDTO) {
        travel_allowanceDTO.searchColumn = "";
        travel_allowanceDTO.searchColumn += CatDAO.getName("English", "travel_time", travel_allowanceDTO.startTimeCat) + " " + CatDAO.getName("Bangla", "travel_time", travel_allowanceDTO.startTimeCat) + " ";
        travel_allowanceDTO.searchColumn += CatDAO.getName("English", "travel_time", travel_allowanceDTO.endTimeCat) + " " + CatDAO.getName("Bangla", "travel_time", travel_allowanceDTO.endTimeCat) + " ";
        travel_allowanceDTO.searchColumn += travel_allowanceDTO.employeeNameEn + " " + travel_allowanceDTO.employeeNameBn;
    }


    @Override
    public void set(PreparedStatement ps, Travel_allowanceDTO travel_allowanceDTO, boolean isInsert) throws SQLException {
        int index = 0;

        ps.setObject(++index, travel_allowanceDTO.employeeRecordsId);
        ps.setObject(++index, travel_allowanceDTO.purpose);
        ps.setObject(++index, travel_allowanceDTO.startDate);
        ps.setObject(++index, travel_allowanceDTO.startTimeCat);
        ps.setObject(++index, travel_allowanceDTO.endDate);
        ps.setObject(++index, travel_allowanceDTO.endTimeCat);
        ps.setObject(++index, travel_allowanceDTO.modifiedBy);

        ps.setObject(++index, travel_allowanceDTO.startAddress);
        ps.setObject(++index, travel_allowanceDTO.employeeNameEn);
        ps.setObject(++index, travel_allowanceDTO.employeeNameBn);
        ps.setObject(++index, travel_allowanceDTO.employeeUserName);
        ps.setObject(++index, travel_allowanceDTO.officeUnitId);
        ps.setObject(++index, travel_allowanceDTO.officeNameEn);
        ps.setObject(++index, travel_allowanceDTO.officeNameBn);
        ps.setObject(++index, travel_allowanceDTO.organogramId);
        ps.setObject(++index, travel_allowanceDTO.organogramNameEn);
        ps.setObject(++index, travel_allowanceDTO.organogramNameBn);
        setSearchColumn(travel_allowanceDTO);
        ps.setObject(++index, travel_allowanceDTO.searchColumn);
        ps.setObject(++index, travel_allowanceDTO.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, travel_allowanceDTO.insertedBy);
            ps.setObject(++index, travel_allowanceDTO.insertionDate);
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, travel_allowanceDTO.iD);
    }

    @Override
    public Travel_allowanceDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Travel_allowanceDTO travel_allowanceDTO = new Travel_allowanceDTO();
            travel_allowanceDTO.iD = rs.getLong("ID");
            travel_allowanceDTO.employeeRecordsId = rs.getLong("employee_records_id");
            travel_allowanceDTO.purpose = rs.getString("purpose");
            travel_allowanceDTO.startDate = rs.getLong("start_date");
            travel_allowanceDTO.startTimeCat = rs.getInt("start_time_cat");
            travel_allowanceDTO.endDate = rs.getLong("end_date");
            travel_allowanceDTO.endTimeCat = rs.getInt("end_time_cat");
            travel_allowanceDTO.startAddress = rs.getString("start_address");

            travel_allowanceDTO.employeeNameEn = rs.getString("employee_name_en");
            travel_allowanceDTO.employeeNameBn = rs.getString("employee_name_bn");
            travel_allowanceDTO.employeeUserName = rs.getString("employee_user_name");

            travel_allowanceDTO.officeUnitId = rs.getLong("office_unit_id");
            travel_allowanceDTO.officeNameEn = rs.getString("office_name_en");
            travel_allowanceDTO.officeNameBn = rs.getString("office_name_bn");

            travel_allowanceDTO.organogramId = rs.getLong("organogram_id");
            travel_allowanceDTO.organogramNameEn = rs.getString("organogram_name_en");
            travel_allowanceDTO.organogramNameBn = rs.getString("organogram_name_bn");

            travel_allowanceDTO.insertionDate = rs.getLong("insertion_time");
            travel_allowanceDTO.insertedBy = rs.getLong("inserted_by");
            travel_allowanceDTO.modifiedBy = rs.getLong("modified_by");
            travel_allowanceDTO.searchColumn = rs.getString("search_column");
            travel_allowanceDTO.isDeleted = rs.getInt("isDeleted");
            travel_allowanceDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return travel_allowanceDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "travel_allowance";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Travel_allowanceDTO) commonDTO, addSqlQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Travel_allowanceDTO) commonDTO, updateSqlQuery, false);
    }


    public Travel_allowanceDTO getDTOByID(long ID) {
        Travel_allowanceDTO travel_allowanceDTO = getDTOFromID(ID);
        travel_allowanceDTO.travelDestinationDTOList = TravelDestinationDAO.getInstance().getByTravelAllowanceId(ID);
        return travel_allowanceDTO;
    }

    public List<Travel_allowanceDTO> getAllTravelAllowanceDTO_details(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }

}
	