package travel_allowance;

import sessionmanager.SessionConstants;
import util.CommonDTO;

import java.util.ArrayList;
import java.util.List;


public class TravelDestinationDTO extends CommonDTO {

    public long travelAllowanceID = 0;
    public String destinationAddress = "";
    public int timeCat = 0;
    public int travelMediumCat = 0;
    public int travelTypeCat = 0;
    public double distanceOrSubCost = 0.0;
    public double cost = 0.0;
    public long reachDate = 0;
    public long insertedBy = 0;
    public long insertionTime = SessionConstants.MIN_DATE;
    public long modifiedBy = 0;

    public List<TravelDestinationDTO> travelDestinationDTOList = new ArrayList<>();

    @Override
    public String toString() {
        return "TravelDestinationDTO{" +
                "travelAllowanceID=" + travelAllowanceID +
                ", destinationAddress='" + destinationAddress + '\'' +
                ", timeCat=" + timeCat +
                ", travelMediumCat=" + travelMediumCat +
                ", travelTypeCat=" + travelTypeCat +
                ", distanceOrSubCost=" + distanceOrSubCost +
                ", cost=" + cost +
                ", reachDate=" + reachDate +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                ", travelDestinationDTOList=" + travelDestinationDTOList +
                '}';
    }

}