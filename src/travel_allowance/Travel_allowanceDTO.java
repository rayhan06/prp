package travel_allowance;

import employee_records.EmployeeFlatInfoDTO;
import sessionmanager.SessionConstants;

import java.util.ArrayList;
import java.util.List;


public class Travel_allowanceDTO extends EmployeeFlatInfoDTO {

    public String purpose = "";
    public long startDate = SessionConstants.MIN_DATE;
    public int startTimeCat = 0;
    public long endDate = SessionConstants.MIN_DATE;
    public int endTimeCat = 0;
    public String startAddress = "";
    public long insertedBy = 0;
    public long modifiedBy = 0;

    public List<TravelDestinationDTO> travelDestinationDTOList = new ArrayList<>();

    @Override
    public String toString() {
        return "$Travel_allowanceDTO[" +
                " iD = " + iD +
                " purpose = " + purpose +
                " startDate = " + startDate +
                " startTimeCat = " + startTimeCat +
                " endDate = " + endDate +
                " endTimeCat = " + endTimeCat +
                " searchColumn = " + searchColumn +
                " startAddress = " + startAddress +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionDate +
                " isDeleted = " + isDeleted +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }
    public void setEmployeeInfo(EmployeeFlatInfoDTO employeeFlatInfoDTO){
        employeeRecordsId = employeeFlatInfoDTO.employeeRecordsId;
        employeeNameEn = employeeFlatInfoDTO.employeeNameEn;
        employeeNameBn = employeeFlatInfoDTO.employeeNameBn;
        employeeUserName = employeeFlatInfoDTO.employeeUserName;

        officeUnitId = employeeFlatInfoDTO.officeUnitId;
        officeNameEn = employeeFlatInfoDTO.officeNameEn;
        officeNameBn = employeeFlatInfoDTO.officeNameBn;

        organogramId = employeeFlatInfoDTO.organogramId;
        organogramNameEn = employeeFlatInfoDTO.organogramNameEn;
        organogramNameBn = employeeFlatInfoDTO.organogramNameBn;
    }
}