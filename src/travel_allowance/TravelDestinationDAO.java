package travel_allowance;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TravelDestinationDAO implements CommonDAOService<TravelDestinationDTO> {

    private static final Logger logger = Logger.getLogger(TravelDestinationDAO.class);


    private static final String addSqlQuery = "INSERT INTO {tableName} (travel_allowance_ID,time_cat,destination_address,travel_medium_cat,travel_type_cat,distance_or_sub_cost,cost,reach_date,"
            .concat("modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID)")
            .concat(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    private static final String updateSqlQuery = "UPDATE {tableName} SET travel_allowance_ID=?,time_cat=?,destination_address=?,travel_medium_cat=?,travel_type_cat=?,distance_or_sub_cost=?,cost=?,reach_date=?,"
            .concat("modified_by=?,lastModificationTime = ?, isDeleted=? WHERE ID = ?");

    private static final String getByTravelAllowanceIdSqlQuery = "SELECT * FROM %s WHERE travel_allowance_ID = %d AND isDeleted = 0 ORDER BY ID";

    private static final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final TravelDestinationDAO INSTANCE = new TravelDestinationDAO();
    }

    public static TravelDestinationDAO getInstance() {
        return TravelDestinationDAO.LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, TravelDestinationDTO travelDestinationDTO, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setObject(++index, travelDestinationDTO.travelAllowanceID);
        ps.setObject(++index, travelDestinationDTO.timeCat);
        ps.setObject(++index, travelDestinationDTO.destinationAddress);
        ps.setObject(++index, travelDestinationDTO.travelMediumCat);
        ps.setObject(++index, travelDestinationDTO.travelTypeCat);
        ps.setObject(++index, travelDestinationDTO.distanceOrSubCost);
        ps.setObject(++index, travelDestinationDTO.cost);
        ps.setObject(++index, travelDestinationDTO.reachDate);
        ps.setObject(++index, travelDestinationDTO.modifiedBy);
        ps.setObject(++index, travelDestinationDTO.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, travelDestinationDTO.insertedBy);
            ps.setObject(++index, travelDestinationDTO.insertionTime);

        }
        ps.setObject(++index, travelDestinationDTO.isDeleted);
        ps.setObject(++index, travelDestinationDTO.iD);
    }

    @Override
    public TravelDestinationDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            TravelDestinationDTO travelDestinationDTO = new TravelDestinationDTO();
            travelDestinationDTO.iD = rs.getLong("ID");
            travelDestinationDTO.travelAllowanceID = rs.getLong("travel_allowance_ID");
            travelDestinationDTO.destinationAddress = rs.getString("destination_address");
            travelDestinationDTO.timeCat = rs.getInt("time_cat");
            travelDestinationDTO.travelMediumCat = rs.getInt("travel_medium_cat");
            travelDestinationDTO.travelTypeCat = rs.getInt("travel_type_cat");
            travelDestinationDTO.distanceOrSubCost = rs.getDouble("distance_or_sub_cost");
            travelDestinationDTO.cost = rs.getDouble("cost");
            travelDestinationDTO.reachDate = rs.getLong("reach_date");
            travelDestinationDTO.insertionTime = rs.getLong("insertion_time");
            travelDestinationDTO.insertedBy = rs.getLong("inserted_by");
            travelDestinationDTO.modifiedBy = rs.getLong("modified_by");
            travelDestinationDTO.isDeleted = rs.getInt("isDeleted");
            travelDestinationDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return travelDestinationDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "travel_destination";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((TravelDestinationDTO) commonDTO, addSqlQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((TravelDestinationDTO) commonDTO, updateSqlQuery, false);
    }


    public TravelDestinationDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }

    public List<TravelDestinationDTO> getAllTravelDestinationDTO_details(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }

    List<TravelDestinationDTO> getByTravelAllowanceId(long travelAllowanceId) {
        return getDTOs(String.format(getByTravelAllowanceIdSqlQuery, getTableName(), travelAllowanceId));
    }
}
	