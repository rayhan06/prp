package travel_allowance;

import util.CommonMaps;


public class TravelDestinationMAPS extends CommonMaps
{	
	public TravelDestinationMAPS(String tableName)
	{
		


		java_SQL_map.put("travel_allowance_ID".toLowerCase(), "travelAllowanceID".toLowerCase());
		java_SQL_map.put("destination_address".toLowerCase(), "destinationAddress".toLowerCase());
		java_SQL_map.put("travel_medium_cat".toLowerCase(), "travelMediumCat".toLowerCase());
		java_SQL_map.put("travel_type_cat".toLowerCase(), "travelTypeCat".toLowerCase());
		java_SQL_map.put("cost".toLowerCase(), "cost".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Travel Allowance ID".toLowerCase(), "travelAllowanceID".toLowerCase());
		java_Text_map.put("Destination Address".toLowerCase(), "destinationAddress".toLowerCase());
		java_Text_map.put("Travel Medium".toLowerCase(), "travelMediumCat".toLowerCase());
		java_Text_map.put("Travel Type".toLowerCase(), "travelTypeCat".toLowerCase());
		java_Text_map.put("Cost".toLowerCase(), "cost".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Insertion Time".toLowerCase(), "insertionTime".toLowerCase());
		java_Text_map.put("Is Deleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Last Modification Time".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}