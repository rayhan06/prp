package physiotherapy_plan_report;
import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;

@WebServlet("/Physiotherapy_plan_report_Servlet")
public class Physiotherapy_plan_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(getClass());

	String[][] Criteria =
	{
		{"criteria","appointment","dr_employee_record_id","=","","String","","","any","dr_user_name", LC.PRESCRIPTION_REPORT_WHERE_DOCTORID + "", "userNameToEmployeeRecordId"},		
		{"criteria","appointment","employee_record_id","=","AND","int","","","any","employee_user_name", LC.PRESCRIPTION_REPORT_WHERE_EMPLOYEEID + "", "userNameToEmployeeRecordId"},		
		{"criteria","appointment","patient_name","like","AND","String","","","%","name", LC.PRESCRIPTION_REPORT_WHERE_NAME + ""},		
		{"criteria","appointment","phone_number","like","AND","String","","","%","phone", LC.PRESCRIPTION_REPORT_WHERE_PHONE + ""},		
		{"criteria","appointment","visit_date",">=","AND","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","appointment","visit_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""}		
	};
	
	String[][] Display =
	{
		{"display","","appointment.visit_date","date",""},		
		{"display","","dr_employee_record_id","erIdToName",""},		
		{"display","","appointment.employee_record_id","erIdToName",""},		
		{"display","","patient_name","text",""},		
		{"display","","phone_number","text",""},		
		{"display","","physiotherapy_plan.id","invisible",""}		
	};
	
	String GroupBy = "";
	String OrderBY = "visit_date desc";
	
	ReportRequestHandler reportRequestHandler;
	
	public Physiotherapy_plan_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
		boolean isLangEng = language.equalsIgnoreCase("english");
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget println, employee_user_name inplemented, actiontype = " + actionType);
		logger.debug("In ssservlet doget logger, employee_user_name inplemented, actiontype = " + actionType);
		
		sql = "physiotherapy_plan join appointment on appointment.id = physiotherapy_plan.appointment_id";

		Display[0][4] = LM.getText(LC.HM_DATE, loginDTO);
		Display[1][4] = isLangEng?"Physiotherapist":"ফিজিওথেরাপিস্ট";
		Display[2][4] = LM.getText(LC.HM_EMPLOYEE_ID, loginDTO);
		Display[3][4] = LM.getText(LC.HM_NAME, loginDTO);
		Display[4][4] = LM.getText(LC.HM_PHONE, loginDTO);
		Display[5][4] = "";

		
		String reportName = isLangEng?"Physiotherapist Plan Report":"ফিজিওথেরাপিস্ট প্ল্যান রিপোর্ট";
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "physiotherapy_plan_report",
				MenuConstants.PHYSIOTHERAPY_PLAN_REPORT_DETAILS, language, reportName, "physiotherapy_plan_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
