package ot_employee_type;

import login.LoginDTO;
import org.apache.log4j.Logger;
import pb.Utils;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/OT_employee_typeServlet")
@MultipartConfig
@SuppressWarnings({"Duplicates"})
public class OT_employee_typeServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(OT_employee_typeServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        String actionType = request.getParameter("actionType");
        try {
            if ("ajax_getOptionsWithOfficeUnitId".equals(actionType)) {
                response.setContentType("text/html; charset=UTF-8");
                String options = getOptionsWithOfficeId(request);
                response.getWriter().println(options);
                return;
            } else if ("ajax_getOptionsWithBudgetOfficeId".equals(actionType)) {
                response.setContentType("text/html; charset=UTF-8");
                String options = getOptionsWithBudgetOfficeId(request);
                response.getWriter().println(options);
                return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private String getOptionsWithOfficeId(HttpServletRequest request) {
        try {
            String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
            Long officeUnitId = Utils.parseOptionalLong(
                    request.getParameter("officeUnitId"), null, null
            );
            Long selectedId = Utils.parseOptionalLong(
                    request.getParameter("selectedId"), null, null
            );
            return OT_employee_typeRepository.getInstance().buildOptionWithOfficeUnitId(language, officeUnitId, selectedId);
        } catch (Exception ex) {
            logger.error(ex);
        }
        return "";
    }

    private String getOptionsWithBudgetOfficeId(HttpServletRequest request) {
        try {
            String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
            Long budgetOfficeId = Utils.parseOptionalLong(
                    request.getParameter("budgetOfficeId"), null, null
            );
            Long selectedId = Utils.parseOptionalLong(
                    request.getParameter("selectedId"), null, null
            );
            return OT_employee_typeRepository.getInstance().buildOptionWithBudgetOfficeId(language, budgetOfficeId, selectedId);
        } catch (Exception ex) {
            logger.error(ex);
        }
        return "";
    }
}
