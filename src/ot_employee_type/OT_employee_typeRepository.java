package ot_employee_type;

import budget_office.Budget_officeRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import ot_bill_type_config.OT_bill_type_configRepository;
import overtime_bill.Overtime_billServlet;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OT_employee_typeRepository implements Repository {
    private static final Logger logger = Logger.getLogger(OT_employee_typeRepository.class);

    private final Map<Long, OT_employee_typeDTO> mapById;

    private OT_employee_typeRepository() {
        mapById = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static OT_employee_typeRepository INSTANCE = new OT_employee_typeRepository();
    }

    public static OT_employee_typeRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public String getTableName() {
        return "ot_employee_type";
    }

    public synchronized void reload(boolean reloadAll) {
        logger.debug("OT_employee_typeRepository loading start for reloadAll: " + reloadAll);
        List<OT_employee_typeDTO> dtoList = OT_employee_typeDAO.getInstance().getAllDTOs(reloadAll);
        if (dtoList != null && dtoList.size() > 0) {
            dtoList.stream()
                   .peek(this::removeIfPresent)
                   .filter(dto -> dto.isDeleted == 0)
                   .forEach(dto -> mapById.put(dto.iD, dto));
        }
        logger.debug("OT_employee_typeRepository loading end for reloadAll: " + reloadAll);
    }

    private void removeIfPresent(OT_employee_typeDTO dto) {
        if (mapById.get(dto.iD) != null) {
            mapById.remove(dto.iD);
        }
    }

    public OT_employee_typeDTO getById(Long id) {
        if (id == null) {
            return null;
        }
        return mapById.getOrDefault(id, null);
    }

    public String getTextById(long id, boolean isLangEn) {
        OT_employee_typeDTO dto = getById(id);
        if (dto == null) {
            return "";
        }
        return isLangEn ? dto.nameEn : dto.nameBn;
    }

    public String getDesignationPrefixId(Long id) {
        if (id == null) {
            return "";
        }
        OT_employee_typeDTO dto = getById(id);
        return dto == null ? "" : dto.designationPrefix;
    }

    public String buildOptions(String language, Long selectedId) {
        return buildOptions(new ArrayList<>(mapById.values()), language, selectedId);
    }

    public String buildOptions(List<OT_employee_typeDTO> employeeTypeDTOs, String language, Long selectedId) {
        List<OptionDTO> optionDTOList =
                employeeTypeDTOs
                        .stream()
                        .map(OT_employee_typeDTO::getOptionDTO)
                        .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.format("%d", selectedId));
    }

    public String buildOptionWithBudgetOfficeId(String language, Long budgetOfficeId, Long selectedId) {
        if (budgetOfficeId == null) {
            return "";
        }
        return buildOptions(
                getOtEmployeeTypeStreamForBudgetOfficeId(budgetOfficeId).collect(Collectors.toList()),
                language,
                selectedId
        );
    }

    private Stream<OT_employee_typeDTO> getOtEmployeeTypeStreamForBudgetOfficeId(Long budgetOfficeId) {
        return OT_bill_type_configRepository
                .getInstance()
                .findByBudgetOffice(budgetOfficeId)
                .stream()
                .map(dto -> dto.otEmployeeTypeId)
                .map(id -> mapById.getOrDefault(id, null))
                .filter(Objects::nonNull);
    }

    public String buildOptionWithOfficeUnitId(String language, Long officeUnitId, Long selectedId) {
        if (officeUnitId == null) {
            return "";
        }
        long budgetOfficeId = Budget_officeRepository.getInstance().getBudgetOfficeIdByOfficeUnitId(officeUnitId);
        List<OT_employee_typeDTO> employeeTypeDTOs =
                getOtEmployeeTypeStreamForBudgetOfficeId(budgetOfficeId)
                        .filter(otEmployeeType -> otEmployeeType.isOfficeUnitIdAllowed(officeUnitId))
                        .collect(Collectors.toList());
        return buildOptions(employeeTypeDTOs, language, selectedId);
    }

    public List<Office_unitsDTO> getAllPossibleOfficeUnits(long budgetOfficeId, long otEmployeeTypeId) {
        OT_employee_typeDTO otEmployeeType = getById(otEmployeeTypeId);
        if (otEmployeeType == null) {
            return Collections.emptyList();
        }
        return Budget_officeRepository
                .getInstance()
                .getOfficeUnitIdSet(budgetOfficeId)
                .stream()
                .map(officeUnitId -> Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitId))
                .filter(Objects::nonNull)
                .filter(officeUnitDTO -> Overtime_billServlet.isValidOfficeType(officeUnitDTO.officeOrder))
                .filter(officeUnitDTO -> otEmployeeType.isOfficeUnitIdAllowed(officeUnitDTO.iD))
                .collect(Collectors.toList());
    }

    public Long getIdByEmployeeInfo(long officeUnitId, int employmentCat, int officerTypeCat) {
        long budgetOfficeId = Budget_officeRepository.getInstance().getBudgetOfficeIdByOfficeUnitId(officeUnitId);
        return getOtEmployeeTypeStreamForBudgetOfficeId(budgetOfficeId)
                .filter(dto -> dto.isOfficeUnitIdAllowed(officeUnitId))
                .filter(dto -> dto.isEmploymentCatAllowed(employmentCat))
                .filter(dto -> dto.isOfficerTypeCatAllowed(officerTypeCat))
                .findFirst()
                .map(dto -> dto.iD)
                .orElse(null);
    }
}