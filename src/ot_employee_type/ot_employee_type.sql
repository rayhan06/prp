CREATE TABLE ot_employee_type
(
    ID                   BIGINT PRIMARY KEY,
    employment_cat_arr   VARCHAR(4096),
    officer_type_cat_arr VARCHAR(4096),
    office_unit_id_arr   VARCHAR(4096),
    name_en              VARCHAR(512),
    name_bn              VARCHAR(512),
    designation_prefix   VARCHAR(512),
    modified_by          BIGINT,
    lastModificationTime BIGINT DEFAULT -62135791200000,
    inserted_by          BIGINT,
    insertion_time       BIGINT DEFAULT -62135791200000,
    isDeleted            INT    DEFAULT 0
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

INSERT INTO vb_sequencer (table_name, next_id, table_LastModificationTime)
VALUES ('ot_employee_type', 1, 0);

INSERT INTO ot_employee_type (ID, employment_cat_arr, officer_type_cat_arr, office_unit_id_arr, name_en, name_bn,
                              designation_prefix, modified_by, lastModificationTime, inserted_by, insertion_time, isDeleted)
VALUES (1, '[1,6]', '[]', '[]', 'Regular & Contractual', 'নিয়মিত ও চুক্তিভিত্তিক', '', -1, 0, -1, -62135791200000, 0),
       (2, '[2]', '[]', '[]', 'Deputation', 'ডেপুটেশন', '', -1, 0, -1, -62135791200000, 0),
       (3, '[3]', '[]', '[]', 'Privileged', 'প্রিভিলেজ', '', -1, 0, -1, -62135791200000, 0),
       (4, '[4]', '[]', '[]', 'Master Role', 'দৈনিক ভিত্তিক সাংবাৎসরিক', 'সাং-', -1, 0, -1, -62135791200000, 0),
       (5, '[]', '[]', '[1103]', 'Audit Unit', 'অডিট ইউনিট', '', -1, 0, -1, -62135791200000, 0),
       -- Regular,Contractual,Deputation for Secretariat
       (6, '[1,2,6]', '[]', '[]', 'Regular & Contractual', 'নিয়মিত ও চুক্তিভিত্তিক', '', -1, 0, -1, -62135791200000, 0),
       (7, '[1]', '[]', '[]', 'Regular', 'নিয়মিত', '', -1, 0, -1, -62135791200000, 0);