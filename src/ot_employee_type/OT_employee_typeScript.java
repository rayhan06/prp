package ot_employee_type;

import employee_records.EmployeeOfficerType;
import employee_records.EmploymentEnum;
import org.apache.log4j.Logger;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.Arrays.asList;


public class OT_employee_typeScript {
    private static final Logger logger = Logger.getLogger(OT_employee_typeScript.class);

    private static final int TOTAL = 5;

    public static final List<Set<Integer>> employmentCatSetList = asList(
            new HashSet<>(asList(EmploymentEnum.REGULAR.getValue(), EmploymentEnum.CONTACT_BASIS.getValue())),
            new HashSet<>(asList(EmploymentEnum.MASTER_ROLL.getValue())),
            new HashSet<>(asList(EmploymentEnum.OTHERS.getValue())),
            new HashSet<>(),
            new HashSet<>()
    );
    public static final List<Set<Integer>> officerTypeCatSetList = asList(
            new HashSet<>(),
            new HashSet<>(),
            new HashSet<>(),
            new HashSet<>(asList(EmployeeOfficerType.DEPUTATION.getValue())),
            new HashSet<>()
    );
    public static final List<Set<Long>> officeUnitIdSetList = asList(
            new HashSet<>(),
            new HashSet<>(),
            new HashSet<>(),
            new HashSet<>(),
            new HashSet<>(asList(1103L))
    );
    public static final String[][] names = new String[][]{
            new String[]{"Regular & Contractual", "নিয়মিত ও চুক্তিভিত্তিক"},
            new String[]{"Master Role", "দৈনিক ভিত্তিক সাংবাৎসরিক"},
            new String[]{"Others", "অন্যান্য"},
            new String[]{"Deputation", "ডেপুটেশন"},
            new String[]{"Audit Unit", "অডিট ইউনিট"},
    };

    private static List<OT_employee_typeDTO> getStaticOtEmployeeTypeDTOList() {
        return IntStream.range(0, TOTAL)
                        .mapToObj(i -> {
                            OT_employee_typeDTO dto = new OT_employee_typeDTO();
                            dto.iD = i + 1;
                            dto.employmentCatSet = employmentCatSetList.get(i);
                            dto.officerTypeCatSet = officerTypeCatSetList.get(i);
                            dto.officeUnitIdSet = officeUnitIdSetList.get(i);
                            dto.nameEn = names[i][0];
                            dto.nameBn = names[i][1];
                            return dto;
                        })
                        .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        OT_employee_typeDAO dao = OT_employee_typeDAO.getInstance();
        for (OT_employee_typeDTO dto : getStaticOtEmployeeTypeDTOList()) {
            try {
                dao.addWithSetId(dto);
            } catch (Exception exception) {
                logger.error(exception);
            }
        }
        List<OT_employee_typeDTO> dtoList = dao.getAllDTOs(true);
        for (OT_employee_typeDTO dto : dtoList) {
            System.out.println(dto);
        }
        OT_employee_typeRepository repository = OT_employee_typeRepository.getInstance();
        System.out.println(repository);
    }
}
