package ot_employee_type;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class OT_employee_typeDAO implements CommonDAOService<OT_employee_typeDTO> {
    private static final Logger logger = Logger.getLogger(OT_employee_typeDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (employment_cat_arr,officer_type_cat_arr,office_unit_id_arr,name_en,name_bn,designation_prefix,"
                    .concat("modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET employment_cat_arr=?,officer_type_cat_arr=?,office_unit_id_arr=?,name_en=?,name_bn=?,designation_prefix=?,"
                    .concat("modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private OT_employee_typeDAO() {

    }

    private static class LazyLoader {
        static final OT_employee_typeDAO INSTANCE = new OT_employee_typeDAO();
    }

    public static OT_employee_typeDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public String getTableName() {
        return "ot_employee_type";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public void set(PreparedStatement ps, OT_employee_typeDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setString(++index, OT_employee_typeDTO.getStringFromSet(dto.employmentCatSet));
        ps.setString(++index, OT_employee_typeDTO.getStringFromSet(dto.officerTypeCatSet));
        ps.setString(++index, OT_employee_typeDTO.getStringFromSet(dto.officeUnitIdSet));
        ps.setString(++index, dto.nameEn);
        ps.setString(++index, dto.nameBn);
        ps.setString(++index, dto.designationPrefix);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public OT_employee_typeDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            OT_employee_typeDTO dto = new OT_employee_typeDTO();
            dto.iD = rs.getLong("ID");
            dto.employmentCatSet = OT_employee_typeDTO.getIntegerSetFromString(rs.getString("employment_cat_arr"));
            dto.officerTypeCatSet = OT_employee_typeDTO.getIntegerSetFromString(rs.getString("officer_type_cat_arr"));
            dto.officeUnitIdSet = OT_employee_typeDTO.getLongSetFromString(rs.getString("office_unit_id_arr"));
            dto.nameEn = rs.getString("name_en");
            dto.nameBn = rs.getString("name_bn");
            dto.designationPrefix = rs.getString("designation_prefix");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((OT_employee_typeDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((OT_employee_typeDTO) commonDTO, updateSqlQuery, false);
    }

    @SuppressWarnings("UnusedReturnValue")
    public long addWithSetId(CommonDTO commonDTO) throws Exception {
        String sql = addSqlQuery.replace("{tableName}", getTableName());
        return (long) ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(obj -> {
            PreparedStatement ps = (PreparedStatement) obj.getStatement();
            long returnId = -1L;
            try {
                set(ps, (OT_employee_typeDTO) commonDTO, true);
                logger.debug(ps);
                ps.execute();
                returnId = commonDTO.iD;
                recordUpdateTime(obj.getConnection(), getTableName());
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return returnId;
        }, sql);
    }
}
