package ot_employee_type;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import pb.OptionDTO;
import sessionmanager.SessionConstants;
import util.CommonDTO;

import java.util.HashSet;
import java.util.Set;

public class OT_employee_typeDTO extends CommonDTO {
    private final static Gson gson = new Gson();

    public Set<Integer> employmentCatSet = new HashSet<>();
    public Set<Integer> officerTypeCatSet = new HashSet<>();
    public Set<Long> officeUnitIdSet = new HashSet<>();
    public String nameEn;
    public String nameBn;
    public String designationPrefix;

    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;

    public static <T> String getStringFromSet(Set<T> set) {
        return gson.toJson(set);
    }

    public static Set<Long> getLongSetFromString(String arrayAsStr) {
        return gson.fromJson(
                arrayAsStr,
                new TypeToken<Set<Long>>() {
                }.getType()
        );
    }

    public static Set<Integer> getIntegerSetFromString(String arrayAsStr) {
        return gson.fromJson(
                arrayAsStr,
                new TypeToken<Set<Integer>>() {
                }.getType()
        );
    }

    public OptionDTO getOptionDTO() {
        return new OptionDTO(nameEn, nameBn, String.format("%d", iD));
    }

    public boolean isEmploymentCatFilterPresent() {
        return !(employmentCatSet == null || employmentCatSet.isEmpty());
    }

    public boolean isEmploymentCatAllowed(int employmentCat) {
        if (!isEmploymentCatFilterPresent()) {
            return true;
        }
        return employmentCatSet.contains(employmentCat);
    }

    public boolean isOfficerTypeCatFilterPresent() {
        return !(officerTypeCatSet == null || officerTypeCatSet.isEmpty());
    }

    public boolean isOfficerTypeCatAllowed(int officerTypeCat) {
        if (!isOfficerTypeCatFilterPresent()) {
            return true;
        }
        return officerTypeCatSet.contains(officerTypeCat);
    }

    public boolean isOfficeUnitIdFilterPresent() {
        return !(officeUnitIdSet == null || officeUnitIdSet.isEmpty());
    }

    public boolean isOfficeUnitIdAllowed(long officeUnitId) {
        if (!isOfficeUnitIdFilterPresent()) {
            return true;
        }
        return officeUnitIdSet.contains(officeUnitId);
    }
}
