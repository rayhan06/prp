package employee_pay_scale;

import com.google.gson.Gson;
import common.ApiResponse;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.CommonDAO;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;


@WebServlet("/Employee_pay_scaleServlet")
@MultipartConfig
public class Employee_pay_scaleServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Employee_pay_scaleServlet.class);
    private static final String tableName = "employee_pay_scale";
    private final Employee_pay_scaleDAO employee_pay_scaleDAO;
    private final CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    public Employee_pay_scaleServlet() {
        super();
        employee_pay_scaleDAO = new Employee_pay_scaleDAO(tableName);
        commonRequestHandler = new CommonRequestHandler(employee_pay_scaleDAO);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getAddPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_PAY_SCALE_ADD)) {
                        commonRequestHandler.getAddPage(request, response);
                        return;
                    }
                    break;
                case "getEditPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_PAY_SCALE_UPDATE)) {
                        getEmployee_pay_scale(request, response);
                        return;
                    }
                    break;
                case "getURL":
                    String URL = request.getParameter("URL");
                    logger.debug("URL = " + URL);
                    response.sendRedirect(URL);
                    return;
                case "search":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_PAY_SCALE_SEARCH)) {
                        searchEmployee_pay_scale(request, response);
                        return;
                    }
                    break;
                case "view":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_PAY_SCALE_SEARCH)) {
                        commonRequestHandler.view(request, response);
                        return;
                    }
                    break;
                case "getOptionByJobGradeCat":
                    long defaultValue = Long.parseLong(request.getParameter("defaultValue"));
                    String language = request.getParameter("language");
                    Integer jobGradeCat = Integer.parseInt(request.getParameter("jobGradeCat"));
                    String options = CommonDAO.getEmpPayScaleByjobGradeCat(defaultValue, language, jobGradeCat);
                    PrintWriter out = response.getWriter();
                    out.println(options);
                    out.close();
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");

            switch (actionType) {
                case "ajax_add":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_PAY_SCALE_ADD)) {
                        try {
                            addEmployee_pay_scale(request,true, userDTO);
                            ApiResponse.sendSuccessResponse(response, "Employee_pay_scaleServlet?actionType=search");
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    }
                    break;
                case "getDTO":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_PAY_SCALE_ADD)) {
                        getDTO(request, response);
                        return;
                    }
                    break;
                case "ajax_edit":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_PAY_SCALE_UPDATE)) {
                        try {
                            addEmployee_pay_scale(request,false, userDTO);
                            ApiResponse.sendSuccessResponse(response, "Employee_pay_scaleServlet?actionType=search");
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    }
                    break;
                case "delete":
                    deleteEmployee_pay_scale(request, response, userDTO);
                    return;
                case "search":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_PAY_SCALE_SEARCH)) {
                        searchEmployee_pay_scale(request, response);
                        return;
                    }
                    break;
                case "getGeo":
                    request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void getDTO(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Employee_pay_scaleDTO employee_pay_scaleDTO = employee_pay_scaleDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        String encoded = this.gson.toJson(employee_pay_scaleDTO);
        out.print(encoded);
        out.flush();
    }

    private void addEmployee_pay_scale(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) {
        Employee_pay_scaleDTO employee_pay_scaleDTO;

        if (addFlag) {
            employee_pay_scaleDTO = new Employee_pay_scaleDTO();
            employee_pay_scaleDTO.insertedByUserId = userDTO.ID;
            employee_pay_scaleDTO.insertionDate = new Date().getTime();
        } else {
            employee_pay_scaleDTO = employee_pay_scaleDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
        }
        employee_pay_scaleDTO.modifiedBy = String.valueOf(userDTO.ID);
        employee_pay_scaleDTO.lastModificationTime = new Date().getTime();

        employee_pay_scaleDTO.jobGradeCat = Integer.parseInt(request.getParameter("jobGradeCat"));
        employee_pay_scaleDTO.payScaleShort = Jsoup.clean(request.getParameter("payScaleShort"), Whitelist.simpleText());
        employee_pay_scaleDTO.payScaleBrief = Jsoup.clean(request.getParameter("payScaleBrief"), Whitelist.simpleText());
        employee_pay_scaleDTO.nationalPayScaleCat = Integer.parseInt(request.getParameter("nationalPayScaleCat"));
        employee_pay_scaleDTO.employeeClassCat = Integer.parseInt(request.getParameter("employeeClass"));

        String Value = request.getParameter("isActive");
        if (Value != null && Value.equalsIgnoreCase("true")) {
            employee_pay_scaleDTO.isActive = 1;
        } else {
            employee_pay_scaleDTO.isActive = 0;
        }

        employee_pay_scaleDAO.manageWriteOperations(
                employee_pay_scaleDTO,
                addFlag ? SessionConstants.INSERT : SessionConstants.UPDATE,
                -1, userDTO
        );
    }

    private void deleteEmployee_pay_scale(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException {
        String[] IDsToDelete = request.getParameterValues("ID");
        for (String s : IDsToDelete) {
            long id = Long.parseLong(s);
            logger.debug("------ DELETING " + s);
            Employee_pay_scaleDTO employee_pay_scaleDTO = employee_pay_scaleDAO.getDTOByID(id);
            employee_pay_scaleDAO.manageWriteOperations(employee_pay_scaleDTO, SessionConstants.DELETE, id, userDTO);
        }
        response.sendRedirect("Employee_pay_scaleServlet?actionType=search");
    }

    private void getEmployee_pay_scale(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {
        Employee_pay_scaleDTO employee_pay_scaleDTO;

        employee_pay_scaleDTO = employee_pay_scaleDAO.getDTOByID(id);
        request.setAttribute("ID", employee_pay_scaleDTO.iD);
        request.setAttribute("employee_pay_scaleDTO", employee_pay_scaleDTO);
        request.setAttribute("employee_pay_scaleDAO", employee_pay_scaleDAO);

        String getBodyOnly = request.getParameter("getBodyOnly");

        String URL;
        if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
            URL = "employee_pay_scale/employee_pay_scaleEditBody.jsp?actionType=edit";
        } else {
            URL = "employee_pay_scale/employee_pay_scaleEdit.jsp?actionType=edit";
        }
        request.getRequestDispatcher(URL).forward(request, response);
    }


    private void getEmployee_pay_scale(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getEmployee_pay_scale(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void searchEmployee_pay_scale(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");

        logger.debug("ajax = " + ajax + " hasAjax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_EMPLOYEE_PAY_SCALE,
                request,
                employee_pay_scaleDAO,
                SessionConstants.VIEW_EMPLOYEE_PAY_SCALE,
                SessionConstants.SEARCH_EMPLOYEE_PAY_SCALE,
                tableName,
                true,
                userDTO,
                "",
                true
        );

        rnManager.doJob(loginDTO);

        request.setAttribute("employee_pay_scaleDAO", employee_pay_scaleDAO);

        if (!hasAjax) {
            logger.debug("Going to employee_pay_scale/employee_pay_scaleSearch.jsp");
            request.getRequestDispatcher("employee_pay_scale/employee_pay_scaleSearch.jsp").forward(request, response);
        } else {
            logger.debug("Going to employee_pay_scale/employee_pay_scaleSearchForm.jsp");
            request.getRequestDispatcher("employee_pay_scale/employee_pay_scaleSearchForm.jsp").forward(request, response);
        }
    }
}

