package employee_pay_scale;

import util.CommonDTO;

public class Employee_pay_scaleDTO extends CommonDTO {

    public int jobGradeCat = 0;
    public int employeeClassCat = 0;
    public String payScaleShort = "";
    public String payScaleBrief = "";
    public int nationalPayScaleCat = 0;
    public int isActive = 0;
    public long insertedByUserId = 0;
    public long insertionDate = 0;
    public String modifiedBy = "";

    @Override
    public String toString() {
        return "Employee_pay_scaleDTO{" +
                "jobGradeCat=" + jobGradeCat +
                ", employeeClassCat=" + employeeClassCat +
                ", payScaleShort='" + payScaleShort + '\'' +
                ", payScaleBrief='" + payScaleBrief + '\'' +
                ", nationalPayScaleCat=" + nationalPayScaleCat +
                ", isActive=" + isActive +
                ", insertedByUserId=" + insertedByUserId +
                ", insertionDate=" + insertionDate +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", iD=" + iD +
                ", isDeleted=" + isDeleted +
                ", lastModificationTime=" + lastModificationTime +
                ", jobCat=" + jobCat +
                ", subject='" + subject + '\'' +
                ", remarks='" + remarks + '\'' +
                ", fileID=" + fileID +
                ", rejectTo=" + rejectTo +
                ", searchColumn='" + searchColumn + '\'' +
                '}';
    }
}