package employee_pay_scale;

import util.CommonMaps;


public class Employee_pay_scaleMAPS extends CommonMaps
{	
	public Employee_pay_scaleMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("jobGradeCat".toLowerCase(), "jobGradeCat".toLowerCase());
		java_DTO_map.put("payScaleShort".toLowerCase(), "payScaleShort".toLowerCase());
		java_DTO_map.put("payScaleBrief".toLowerCase(), "payScaleBrief".toLowerCase());
		java_DTO_map.put("nationalPayScaleCat".toLowerCase(), "nationalPayScaleCat".toLowerCase());
		java_DTO_map.put("isActive".toLowerCase(), "isActive".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("job_grade_cat".toLowerCase(), "jobGradeCat".toLowerCase());
		java_SQL_map.put("pay_scale_short".toLowerCase(), "payScaleShort".toLowerCase());
		java_SQL_map.put("pay_scale_brief".toLowerCase(), "payScaleBrief".toLowerCase());
		java_SQL_map.put("national_pay_scale_cat".toLowerCase(), "nationalPayScaleCat".toLowerCase());
		java_SQL_map.put("is_active".toLowerCase(), "isActive".toLowerCase());
		java_SQL_map.put("inserted_by_user_id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_SQL_map.put("insertion_date".toLowerCase(), "insertionDate".toLowerCase());
		java_SQL_map.put("modified_by".toLowerCase(), "modifiedBy".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Job Grade".toLowerCase(), "jobGradeCat".toLowerCase());
		java_Text_map.put("Pay Scale Short".toLowerCase(), "payScaleShort".toLowerCase());
		java_Text_map.put("Pay Scale Brief".toLowerCase(), "payScaleBrief".toLowerCase());
		java_Text_map.put("National Pay Scale".toLowerCase(), "nationalPayScaleCat".toLowerCase());
		java_Text_map.put("Is Active".toLowerCase(), "isActive".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}