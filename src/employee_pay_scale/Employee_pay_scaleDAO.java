package employee_pay_scale;

import employee_records.EmployeeCommonDTOService;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@SuppressWarnings({"unused","rawtypes"})
public class Employee_pay_scaleDAO extends NavigationService4 implements EmployeeCommonDTOService<Employee_pay_scaleDTO> {

    private static final Logger logger = Logger.getLogger(Employee_pay_scaleDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (job_grade_cat, pay_scale_short," +
            " pay_scale_brief, national_pay_scale_cat, is_active, inserted_by_user_id, insertion_date, modified_by," +
            " lastModificationTime, employee_class_cat, search_column, isDeleted, ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private static final String updateQuery = "UPDATE {tableName} SET job_grade_cat=?, pay_scale_short=?," +
            " pay_scale_brief=?, national_pay_scale_cat=?, is_active=?, inserted_by_user_id=?, insertion_date=?, modified_by=?," +
            " lastModificationTime=?,employee_class_cat=?, search_column=? WHERE ID = ?";

    private static final Map<String,String > searchMap = new HashMap<>();

    static {
        searchMap.put("job_grade_cat"," and (job_grade_cat = ?)");
        searchMap.put("employee_class_cat"," and (employee_class_cat = ?)");
        searchMap.put("national_pay_scale_cat"," and (national_pay_scale_cat = ?)");
        searchMap.put("is_active_cat"," and (is_active = ?)");
        searchMap.put("AnyField"," and (search_column like ?)");
    }

    public Employee_pay_scaleDAO(String tableName) {
        super(tableName);
        joinSQL = "";
        useSafeSearch = true;
        commonMaps = new Employee_pay_scaleMAPS(tableName);
    }

    public Employee_pay_scaleDAO() {
        this("employee_pay_scale");
    }

    public void setSearchColumn(Employee_pay_scaleDTO employee_pay_scaleDTO) {
        List<String> list = new ArrayList<>();
        CategoryLanguageModel languageModel;

        languageModel = CatRepository.getInstance().getCategoryLanguageModel("job_grade_type",employee_pay_scaleDTO.jobGradeCat);
        if(languageModel!=null){
            list.add(languageModel.englishText);
            list.add(languageModel.banglaText);
        }

        languageModel = CatRepository.getInstance().getCategoryLanguageModel("national_pay_scale_type",employee_pay_scaleDTO.nationalPayScaleCat);
        if(languageModel!=null){
            list.add(languageModel.englishText);
            list.add(languageModel.banglaText);
        }

        languageModel = CatRepository.getInstance().getCategoryLanguageModel("employee_class",employee_pay_scaleDTO.employeeClassCat);
        if(languageModel!=null){
            list.add(languageModel.englishText);
            list.add(languageModel.banglaText);
        }
        if(employee_pay_scaleDTO.payScaleShort!=null && employee_pay_scaleDTO.payScaleShort.trim().length()>0){
            list.add(employee_pay_scaleDTO.payScaleShort.trim());
        }
        if(employee_pay_scaleDTO.payScaleBrief!=null && employee_pay_scaleDTO.payScaleBrief.trim().length()>0){
            list.add(employee_pay_scaleDTO.payScaleBrief.trim());
        }

        languageModel = CatRepository.getInstance().getCategoryLanguageModel("yes_no",employee_pay_scaleDTO.isActive);
        if(languageModel!=null){
            list.add(languageModel.englishText);
            list.add(languageModel.banglaText);
        }

        employee_pay_scaleDTO.searchColumn = String.join(" ", list);
    }

    public void set(PreparedStatement ps, Employee_pay_scaleDTO employee_pay_scaleDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(employee_pay_scaleDTO);
        ps.setObject(++index, employee_pay_scaleDTO.jobGradeCat);
        ps.setObject(++index, employee_pay_scaleDTO.payScaleShort);
        ps.setObject(++index, employee_pay_scaleDTO.payScaleBrief);
        ps.setObject(++index, employee_pay_scaleDTO.nationalPayScaleCat);
        ps.setObject(++index, employee_pay_scaleDTO.isActive);
        ps.setObject(++index, employee_pay_scaleDTO.insertedByUserId);
        ps.setObject(++index, employee_pay_scaleDTO.insertionDate);
        ps.setObject(++index, employee_pay_scaleDTO.modifiedBy);
        ps.setObject(++index, System.currentTimeMillis());
        ps.setObject(++index, employee_pay_scaleDTO.employeeClassCat);
        ps.setObject(++index, employee_pay_scaleDTO.searchColumn);
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, employee_pay_scaleDTO.iD);
    }

    @Override
    public Employee_pay_scaleDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Employee_pay_scaleDTO employee_pay_scaleDTO = new Employee_pay_scaleDTO();
            employee_pay_scaleDTO.iD = rs.getLong("ID");
            employee_pay_scaleDTO.jobGradeCat = rs.getInt("job_grade_cat");
            employee_pay_scaleDTO.payScaleShort = rs.getString("pay_scale_short");
            employee_pay_scaleDTO.payScaleBrief = rs.getString("pay_scale_brief");
            employee_pay_scaleDTO.nationalPayScaleCat = rs.getInt("national_pay_scale_cat");
            employee_pay_scaleDTO.isActive = rs.getInt("is_active");
            employee_pay_scaleDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
            employee_pay_scaleDTO.insertionDate = rs.getLong("insertion_date");
            employee_pay_scaleDTO.modifiedBy = rs.getString("modified_by");
            employee_pay_scaleDTO.isDeleted = rs.getInt("isDeleted");
            employee_pay_scaleDTO.lastModificationTime = rs.getLong("lastModificationTime");
            employee_pay_scaleDTO.employeeClassCat = rs.getInt("employee_class_cat");
            employee_pay_scaleDTO.searchColumn = rs.getString("search_column");
            return employee_pay_scaleDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "employee_pay_scale";
    }


    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_pay_scaleDTO) commonDTO, addQuery, true);
    }


    public Employee_pay_scaleDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_pay_scaleDTO) commonDTO, updateQuery, false);
    }


    public List<Employee_pay_scaleDTO> getAllEmployee_pay_scale(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }


    public List<Employee_pay_scaleDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }


    public List<Employee_pay_scaleDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
                                               String filter, boolean tableHasJobCat) {
        List<Object> objectList = new ArrayList<>();
        String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat,objectList);
        return getDTOs(sql,objectList);
    }

    public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                          UserDTO userDTO, String filter, boolean tableHasJobCat,List<Object>objectList) {
        return getSearchQuery(tableName,p_searchCriteria,limit,offset,category,searchMap,objectList);
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
                                           boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat,List<Object>objectList) {
        return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat,objectList);
    }
}