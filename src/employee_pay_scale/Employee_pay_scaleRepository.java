package employee_pay_scale;

import election_wise_mp.Election_wise_mpDTO;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class Employee_pay_scaleRepository implements Repository {
    private final Employee_pay_scaleDAO employee_pay_scaleDAO;
    private final Map<Long, Employee_pay_scaleDTO> mapOfEmployee_pay_scaleDTOToiD;
    private List<Employee_pay_scaleDTO> employeePayScaleDTOList;

    private Employee_pay_scaleRepository() {
        employee_pay_scaleDAO = new Employee_pay_scaleDAO();
        mapOfEmployee_pay_scaleDTOToiD = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        static Employee_pay_scaleRepository INSTANCE = new Employee_pay_scaleRepository();
    }

    public synchronized static Employee_pay_scaleRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        List<Employee_pay_scaleDTO> employee_pay_scaleDTOs = employee_pay_scaleDAO.getAllEmployee_pay_scale(reloadAll);
        if (employee_pay_scaleDTOs != null && employee_pay_scaleDTOs.size() > 0) {
            employee_pay_scaleDTOs.stream()
                    .peek(this::removedIfHave)
                    .filter(dto -> dto.isDeleted == 0)
                    .forEach(dto -> mapOfEmployee_pay_scaleDTOToiD.put(dto.iD, dto));
            employeePayScaleDTOList = new ArrayList<>(mapOfEmployee_pay_scaleDTOToiD.values());
        }
    }

    private void removedIfHave(Employee_pay_scaleDTO dto) {
        Employee_pay_scaleDTO oldEmployee_pay_scaleDTO = getEmployee_pay_scaleDTOByID(dto.iD);
        if (oldEmployee_pay_scaleDTO != null) {
            mapOfEmployee_pay_scaleDTOToiD.remove(oldEmployee_pay_scaleDTO.iD);
        }
    }

    public List<Employee_pay_scaleDTO> getEmployee_pay_scaleList() {
        return employeePayScaleDTOList == null ? new ArrayList<>() : employeePayScaleDTOList;
    }

    public Employee_pay_scaleDTO getEmployee_pay_scaleDTOByID(long ID) {
        if (mapOfEmployee_pay_scaleDTOToiD.get(ID) == null) {
            synchronized (LockManager.getLock(ID + "EPSR")) {
                Employee_pay_scaleDTO dto = employee_pay_scaleDAO.getDTOFromID(ID);
                if (dto != null) {
                    mapOfEmployee_pay_scaleDTOToiD.put(dto.iD, dto);
                }
            }
        }
        return mapOfEmployee_pay_scaleDTOToiD.get(ID);
    }

    public Employee_pay_scaleDTO getByJobGradeCat(int jobGradeCat) {
        return employeePayScaleDTOList.stream()
                .filter(dto -> dto.jobGradeCat == jobGradeCat)
                .findAny()
                .orElse(null);
    }

    public String buildOptions(String language, Long selectedId) {
        List<OptionDTO> optionDTOList = null;
        if (employeePayScaleDTOList != null && employeePayScaleDTOList.size() > 0) {
            optionDTOList = employeePayScaleDTOList.stream()
                    .map(this::buildOptionDTO)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            optionDTOList.sort((o1, o2) -> Integer.compare(Integer.parseInt(o2.value), Integer.parseInt(o1.value)));
        }
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    private OptionDTO buildOptionDTO(Employee_pay_scaleDTO dto) {
        CategoryLanguageModel model = CatRepository.getInstance().getCategoryLanguageModel("job_grade_type", dto.jobGradeCat);
        if (model == null) {
            return null;
        }
        return new OptionDTO(model.englishText, model.banglaText, String.valueOf(dto.iD));
    }

    public String getText(long id, String Language) {
        Employee_pay_scaleDTO dto = getEmployee_pay_scaleDTOByID(id);
        if (dto == null) {
            return "";
        }
        return CatRepository.getInstance().getText(Language, "job_grade_type", dto.jobGradeCat);
    }

    @Override
    public String getTableName() {
        return "employee_pay_scale";
    }
}