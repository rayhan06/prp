package ticket_type_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Ticket_type_report_Servlet")
public class Ticket_type_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","ticket_issues_type","=","","int","","","any","ticketIssuesType", LC.TICKET_TYPE_REPORT_SELECT_TICKETISSUESTYPE + ""},		
		{"criteria","","ticket_issues_subtype_type","=","AND","int","","","any","ticketIssuesSubtypeType", LC.TICKET_TYPE_REPORT_SELECT_TICKETISSUESSUBTYPETYPE + ""},		
		{"criteria","","insertion_date",">=","AND","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","insertion_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""}		
	};
	
	String[][] Display =
	{
		{"display","","ticket_issues_type","type",""},		
		{"display","","ticket_issues_subtype_type","type",""},		
		{"display","","count(id)","int",""}		
	};
	
	String GroupBy = " ticket_issues_subtype_type, ticket_issues_type ";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Ticket_type_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = " support_ticket ";

		Display[0][4] = LM.getText(LC.TICKET_TYPE_REPORT_SELECT_TICKETISSUESTYPE, loginDTO);
		Display[1][4] = LM.getText(LC.TICKET_TYPE_REPORT_SELECT_TICKETISSUESSUBTYPETYPE, loginDTO);
		Display[2][4] = LM.getText(LC.TICKET_TYPE_REPORT_SELECT_TICKETCOUNT, loginDTO);

		
		String reportName = LM.getText(LC.TICKET_TYPE_REPORT_OTHER_TICKET_TYPE_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "ticket_type_report",
				MenuConstants.TICKET_TYPE_REPORT_DETAILS, language, reportName, "ticket_type_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
