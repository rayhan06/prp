package extra_labor_allowance;

import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import common.EmployeeCommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static util.StringUtils.convertToBanNumber;

@SuppressWarnings({"Duplicates"})
public class Extra_labor_allowanceDAO implements EmployeeCommonDAOService<Extra_labor_allowanceDTO> {
    private static final Logger logger = Logger.getLogger(Extra_labor_allowanceDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (budget_office_id,month_year,allowance_employee_info_id,employee_records_id,office_unit_id,organogram_id,"
                    .concat("daily_rate_ka,daily_rate_kha,day_ka,day_kha,revenue_stamp_deduction,search_column,modified_by,lastModificationTime,")
                    .concat("inserted_by,insertion_date,isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET budget_office_id=?,month_year=?,allowance_employee_info_id=?,employee_records_id=?,office_unit_id=?,"
                    .concat("organogram_id=?,daily_rate_ka=?,daily_rate_kha=?,day_ka=?,day_kha=?,revenue_stamp_deduction=?,search_column=?,modified_by=?,")
                    .concat("lastModificationTime=? WHERE ID=?");

    private static final String getAlreadyAddedRecords =
            "SELECT * FROM extra_labor_allowance WHERE month_year=? AND budget_office_id=? AND isDeleted=0";

    private static final Map<String, String> searchMap = new HashMap<>();

    private Extra_labor_allowanceDAO() {
        searchMap.put("AnyField", " AND (search_column LIKE ?) ");
        searchMap.put("budgetOfficeId", " AND (budget_office_id = ?) ");
        searchMap.put("monthYear", " AND (month_year = ?) ");
    }

    private static class LazyLoader {
        static final Extra_labor_allowanceDAO INSTANCE = new Extra_labor_allowanceDAO();
    }

    public static Extra_labor_allowanceDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Extra_labor_allowanceDTO dto) {
        AllowanceEmployeeInfoDTO allowanceEmployeeInfoDTO =
                AllowanceEmployeeInfoRepository.getInstance().getById(dto.allowanceEmployeeInfoId);

        dto.searchColumn =
                allowanceEmployeeInfoDTO.nameEn + " " + allowanceEmployeeInfoDTO.nameBn + " "
                        + allowanceEmployeeInfoDTO.officeNameEn + " "
                        + allowanceEmployeeInfoDTO.officeNameBn + " "
                        + allowanceEmployeeInfoDTO.organogramNameEn + " "
                        + allowanceEmployeeInfoDTO.organogramNameBn + " "
                        + allowanceEmployeeInfoDTO.mobileNumber + " "
                        + convertToBanNumber(allowanceEmployeeInfoDTO.mobileNumber) + " "
                        + allowanceEmployeeInfoDTO.savingAccountNumber + " "
                        + convertToBanNumber(allowanceEmployeeInfoDTO.savingAccountNumber);
    }

    @Override
    public void set(PreparedStatement ps, Extra_labor_allowanceDTO dto, boolean isInsert) throws SQLException {
        setSearchColumn(dto);
        int index = 0;
        ps.setLong(++index, dto.budgetOfficeId);
        ps.setLong(++index, dto.monthYear);
        ps.setLong(++index, dto.allowanceEmployeeInfoId);
        ps.setLong(++index, dto.employeeRecordsId);
        ps.setLong(++index, dto.officeUnitId);
        ps.setLong(++index, dto.organogramId);
        ps.setInt(++index, dto.dailyRateKa);
        ps.setInt(++index, dto.dailyRateKha);
        ps.setInt(++index, dto.dayKa);
        ps.setInt(++index, dto.dayKha);
        ps.setInt(++index, dto.revenueStampDeduction);
        ps.setString(++index, dto.searchColumn);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionDate);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Extra_labor_allowanceDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Extra_labor_allowanceDTO dto = new Extra_labor_allowanceDTO();
            dto.iD = rs.getLong("ID");
            dto.budgetOfficeId = rs.getLong("budget_office_id");
            dto.monthYear = rs.getLong("month_year");
            dto.allowanceEmployeeInfoId = rs.getLong("allowance_employee_info_id");
            dto.employeeRecordsId = rs.getLong("employee_records_id");
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.organogramId = rs.getLong("organogram_id");
            dto.dailyRateKa = rs.getInt("daily_rate_ka");
            dto.dailyRateKha = rs.getInt("daily_rate_kha");
            dto.dayKa = rs.getInt("day_ka");
            dto.dayKha = rs.getInt("day_kha");
            dto.totalAmount = dto.dailyRateKa * dto.dayKa + dto.dailyRateKha * dto.dayKha;
            dto.revenueStampDeduction = rs.getInt("revenue_stamp_deduction");
            dto.netAmount = dto.totalAmount - dto.revenueStampDeduction;
            dto.searchColumn = rs.getString("search_column");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionDate = rs.getLong("insertion_date");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "extra_labor_allowance";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Extra_labor_allowanceDTO) commonDTO, addSqlQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Extra_labor_allowanceDTO) commonDTO, updateSqlQuery, false);
    }

    public List<Extra_labor_allowanceDTO> getDTOs(long budgetOfficeId, long monthYear) {
        return getDTOs(
                getAlreadyAddedRecords,
                Arrays.asList(monthYear, budgetOfficeId)
        );
    }
}