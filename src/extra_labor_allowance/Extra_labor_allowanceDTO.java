package extra_labor_allowance;

import java.util.*;

import util.*;


public class Extra_labor_allowanceDTO extends CommonDTO {
    public long budgetOfficeId = -1;
    public long monthYear = -1;
    public long allowanceEmployeeInfoId = -1;
    public long employeeRecordsId = -1;
    public long officeUnitId = -1;
    public long organogramId = -1;
    public int dailyRateKa = -1;
    public int dailyRateKha = -1;
    public int dayKa = -1;
    public int dayKha = -1;
    public int totalAmount = -1;
    public int revenueStampDeduction = -1;
    public int netAmount = -1;
    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionDate = -1;

    @Override
    public String toString() {
        return "Extra_labor_allowanceDTO{" +
                "budgetOfficeId=" + budgetOfficeId +
                ", monthYear=" + monthYear +
                ", allowanceEmployeeInfoId=" + allowanceEmployeeInfoId +
                ", employeeRecordsId=" + employeeRecordsId +
                ", officeUnitId=" + officeUnitId +
                ", organogramId=" + organogramId +
                ", dailyRateKa=" + dailyRateKa +
                ", dailyRateKha=" + dailyRateKha +
                ", dayKa=" + dayKa +
                ", dayKha=" + dayKha +
                ", revenueStampDeduction=" + revenueStampDeduction +
                ", searchColumn='" + searchColumn + '\'' +
                ", modifiedBy=" + modifiedBy +
                ", insertedBy=" + insertedBy +
                ", insertionDate=" + insertionDate +
                '}';
    }
}