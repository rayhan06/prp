package extra_labor_allowance;

import allowance_configure.AllowanceCatEnum;
import allowance_configure.Allowance_configureDTO;
import allowance_configure.Allowance_configureRepository;
import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import budget_office.Budget_officeRepository;
import com.google.gson.Gson;
import common.BaseServlet;
import login.LoginDTO;
import org.apache.log4j.Logger;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


@WebServlet("/Extra_labor_allowanceServlet")
@MultipartConfig
@SuppressWarnings({"Duplicates"})
public class Extra_labor_allowanceServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    private final Logger logger = Logger.getLogger(Extra_labor_allowanceServlet.class);

    @Override
    public String getTableName() {
        return Extra_labor_allowanceDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Extra_labor_allowanceServlet";
    }

    @Override
    public Extra_labor_allowanceDAO getCommonDAOService() {
        return Extra_labor_allowanceDAO.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "ajax_getExtraLaborAllowanceData": {
                    long monthYear = new SimpleDateFormat("dd/MM/yyyy").parse(
                            request.getParameter("monthYear")
                    ).getTime();
                    long budgetOfficeId = Long.parseLong(request.getParameter("budgetOfficeId"));
                    String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";

                    Map<String, Object> res = getExtraLaborAllowanceData(budgetOfficeId, monthYear, language);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().println(new Gson().toJson(res));
                    return;
                }
                case "view": {
                    long monthYear = Long.parseLong(request.getParameter("monthYear"));
                    long budgetOfficeId = Long.parseLong(request.getParameter("budgetOfficeId"));

                    List<Extra_labor_allowanceDTO> allowanceDTOs = Extra_labor_allowanceDAO.getInstance().getDTOs(budgetOfficeId, monthYear);
                    if (allowanceDTOs == null || allowanceDTOs.isEmpty())
                        throw new FileNotFoundException("no DTOs found");

                    request.setAttribute("allowanceDTOs", allowanceDTOs);
                    request.setAttribute("monthYear", monthYear);
                    request.setAttribute("budgetOfficeId", budgetOfficeId);

                    request.getRequestDispatcher("extra_labor_allowance/extra_labor_allowanceView.jsp").forward(request, response);
                    return;
                }
                case "individualBill": {
                    long id = Long.parseLong(request.getParameter("iD"));

                    Extra_labor_allowanceDTO allowanceDTO = Extra_labor_allowanceDAO.getInstance().getDTOFromID(id);
                    if (allowanceDTO == null) throw new FileNotFoundException("no DTO found with iD = " + id);

                    request.setAttribute("allowanceDTO", allowanceDTO);
                    request.getRequestDispatcher("extra_labor_allowance/extra_labor_allowanceIndividualBill.jsp").forward(request, response);
                    return;
                }
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private Map<String, Object> getExtraLaborAllowanceData(long budgetOfficeId, long monthYear, String language) {
        List<Extra_labor_allowanceDTO> dtoList = Extra_labor_allowanceDAO.getInstance().getDTOs(budgetOfficeId, monthYear);
        List<Extra_labor_allowanceModel> models;

        boolean isAlreadyAdded = dtoList != null && !dtoList.isEmpty();
        if (isAlreadyAdded) {
            models = dtoList.stream()
                    .map(allowanceDTO -> new Extra_labor_allowanceModel(allowanceDTO, language))
                    .collect(Collectors.toList());
        } else {
            Set<Long> budgetOfficeIds = Budget_officeRepository.getInstance()
                    .getOfficeUnitIdSet(budgetOfficeId);
            List<AllowanceEmployeeInfoDTO> employeeInfoDTOs =
                    AllowanceEmployeeInfoRepository.getInstance().buildDTOsFromCache(budgetOfficeIds);

            models = employeeInfoDTOs.stream()
                    .map(employeeInfoDTO -> new Extra_labor_allowanceModel(employeeInfoDTO, monthYear, language))
                    .filter(Extra_labor_allowanceModel::isValidModel)
                    .collect(Collectors.toList());
        }

        Map<String, Object> res = new HashMap<>();
        res.put("isAlreadyAdded", isAlreadyAdded);
        res.put("extraLaborAllowanceModels", models);
        return res;
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        Extra_labor_allowanceDTO allowanceDTO = (Extra_labor_allowanceDTO) commonDTO;

        return getServletName() + "?actionType=view&monthYear=" + allowanceDTO.monthYear
                + "&budgetOfficeId=" + allowanceDTO.budgetOfficeId;
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAjaxAddRedirectURL(request, commonDTO);
    }

    public void addExtraLaborAllowance(UserInput userInput) throws Exception {
        Extra_labor_allowanceDTO allowanceDTO = new Extra_labor_allowanceDTO();
        allowanceDTO.insertedBy = allowanceDTO.modifiedBy = userInput.modifiedBy;
        allowanceDTO.insertionDate = allowanceDTO.lastModificationTime = System.currentTimeMillis();

        allowanceDTO.budgetOfficeId = userInput.budgetOfficeId;
        allowanceDTO.monthYear = userInput.monthYear;

        AllowanceEmployeeInfoDTO employeeInfoDTO =
                AllowanceEmployeeInfoRepository.getInstance()
                        .getByEmployeeRecordId(userInput.employeeRecordsId);
        if (employeeInfoDTO == null) return;

        allowanceDTO.allowanceEmployeeInfoId = employeeInfoDTO.iD;
        allowanceDTO.officeUnitId = employeeInfoDTO.officeUnitId;
        allowanceDTO.organogramId = employeeInfoDTO.organogramId;
        allowanceDTO.employeeRecordsId = employeeInfoDTO.employeeRecordId;

        allowanceDTO.dailyRateKa = getDailyRate(AllowanceCatEnum.OVERTIME_A_ALLOWANCE.getValue());
        allowanceDTO.dailyRateKha = getDailyRate(AllowanceCatEnum.OVERTIME_B_ALLOWANCE.getValue());
        allowanceDTO.dayKa = getExtraLaborDay(
                userInput.employeeRecordsId,
                userInput.monthYear,
                AllowanceCatEnum.OVERTIME_A_ALLOWANCE.getValue()
        );
        allowanceDTO.dayKha = getExtraLaborDay(
                userInput.employeeRecordsId,
                userInput.monthYear,
                AllowanceCatEnum.OVERTIME_B_ALLOWANCE.getValue()
        );
        allowanceDTO.revenueStampDeduction = getTaxDeduction(AllowanceCatEnum.OVERTIME_A_ALLOWANCE.getValue());

        if (allowanceDTO.dayKa == 0 && allowanceDTO.dayKha == 0) {
            logger.error("Attempt to Insert Invalid Value + " + allowanceDTO);
            return;
        }
        Extra_labor_allowanceDAO.getInstance().add(allowanceDTO);
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        boolean isLanguageEnglish = language.equalsIgnoreCase("English");
        UserInput userInput = new UserInput();
        String budgetOfficeStr = request.getParameter("budgetOfficeId");
        if (budgetOfficeStr == null) {
            throw new Exception(isLanguageEnglish ? "Budget office mandatory" : "বাজেট অফিস প্রদান আবশ্যক");
        }
        userInput.budgetOfficeId = Long.parseLong(budgetOfficeStr);

        String monthYearStr = request.getParameter("monthYear");
        if (monthYearStr == null) {
            throw new Exception(isLanguageEnglish ? "Allowance month and year mandatory" : "ভাতার জন্য মাস ও বছর প্রদান আবশ্যক");
        }
        userInput.monthYear = new SimpleDateFormat("dd/MM/yyyy").parse(monthYearStr).getTime();
        if (addFlag) {
            List<Extra_labor_allowanceDTO> dtoList = Extra_labor_allowanceDAO.getInstance().getDTOs(userInput.budgetOfficeId, userInput.monthYear);
            boolean isAlreadyAdded = dtoList != null && !dtoList.isEmpty();
            if (!isAlreadyAdded) {
                userInput.modifiedBy = userDTO.ID;
                String[] employeeRecordsIds = request.getParameterValues("employeeRecordsId");
                String[] extraLaborAllowanceIds = request.getParameterValues("extraLaborAllowanceId");
                for (int i = 0; i < employeeRecordsIds.length; i++) {
                    userInput.employeeRecordsId = Long.parseLong(employeeRecordsIds[i]);
                    if (userInput.employeeRecordsId < 0) continue;
                    userInput.extraLaborAllowanceId = Long.parseLong(extraLaborAllowanceIds[i]);
                    addExtraLaborAllowance(userInput);
                }
            }
        }
        // used monthYear & budgetOfficeId in getAjaxAddRedirectURL to redirect to preview page
        Extra_labor_allowanceDTO allowanceDTO = new Extra_labor_allowanceDTO();
        allowanceDTO.monthYear = userInput.monthYear;
        allowanceDTO.budgetOfficeId = userInput.budgetOfficeId;
        return allowanceDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.EXTRA_LABOR_ALLOWANCE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.EXTRA_LABOR_ALLOWANCE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.EXTRA_LABOR_ALLOWANCE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Extra_labor_allowanceServlet.class;
    }

    public static int getDailyRate(int extraLaborCat) {
        Allowance_configureDTO configureDTO =
                Allowance_configureRepository.getInstance()
                        .getByAllowanceCat(extraLaborCat);
        return configureDTO == null ? 0 : configureDTO.allowanceAmount;
    }

    public static int getTaxDeduction(int extraLaborCat) {
        Allowance_configureDTO configureDTO =
                Allowance_configureRepository.getInstance()
                        .getByAllowanceCat(extraLaborCat);
        return configureDTO == null ? 0 : configureDTO.taxDeduction;
    }

    @SuppressWarnings({"unused"})
    public static int getExtraLaborDay(long employeeRecordId, long monthYear, int extraLaborCat) {
        // TODO: 27-Jul-21  this data will come from Attendance Module
        return 10;
    }

    static class UserInput {
        long budgetOfficeId;
        long monthYear;
        long extraLaborAllowanceId;
        long employeeRecordsId;
        long modifiedBy;
    }
}