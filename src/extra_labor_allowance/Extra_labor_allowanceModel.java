package extra_labor_allowance;

import allowance_configure.AllowanceCatEnum;
import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;

import static util.StringUtils.convertBanglaIfLanguageIsBangla;
import static util.UtilCharacter.getDataByLanguage;

@SuppressWarnings({"Duplicates"})
public class Extra_labor_allowanceModel {
    public String employeeRecordsId = "-1";
    public String extraLaborAllowanceId = "-1";
    public String name = "";
    public String officeName = "";
    public String organogramName = "";
    public String mobileNumber = "";
    public String savingAccountNumber = "";
    public String dailyRateKa = "";
    public String dailyRateKha = "";
    public String dayKa = "";
    public String dayKha = "";
    public String totalAmount = "";
    public String revenueStampDeduction = "";
    public String netAmount = "";

    public Extra_labor_allowanceModel(AllowanceEmployeeInfoDTO employeeInfoDTO, long monthYear, String language) {
        setEmployeeInfo(this, employeeInfoDTO, language);

        int rateKa = Extra_labor_allowanceServlet.getDailyRate(AllowanceCatEnum.OVERTIME_A_ALLOWANCE.getValue());
        int rateKha = Extra_labor_allowanceServlet.getDailyRate(AllowanceCatEnum.OVERTIME_B_ALLOWANCE.getValue());
        int daysKa = Extra_labor_allowanceServlet.getExtraLaborDay(
                employeeInfoDTO.employeeRecordId, monthYear, AllowanceCatEnum.OVERTIME_A_ALLOWANCE.getValue()
        );
        int daysKha = Extra_labor_allowanceServlet.getExtraLaborDay(
                employeeInfoDTO.employeeRecordId, monthYear, AllowanceCatEnum.OVERTIME_A_ALLOWANCE.getValue()
        );
        int deduction = Extra_labor_allowanceServlet.getTaxDeduction(AllowanceCatEnum.OVERTIME_A_ALLOWANCE.getValue());

        dailyRateKa = convertBanglaIfLanguageIsBangla(language, String.valueOf(rateKa));
        dailyRateKha = convertBanglaIfLanguageIsBangla(language, String.valueOf(rateKha));
        dayKa = convertBanglaIfLanguageIsBangla(language, String.valueOf(daysKa));
        dayKha = convertBanglaIfLanguageIsBangla(language, String.valueOf(daysKha));

        int total = rateKa  * daysKa + rateKha  * daysKha;
        totalAmount = convertBanglaIfLanguageIsBangla(language, String.valueOf(total));
        revenueStampDeduction = convertBanglaIfLanguageIsBangla(language, String.valueOf(deduction));
        netAmount = convertBanglaIfLanguageIsBangla(language, String.valueOf(total - deduction));
    }

    public Extra_labor_allowanceModel(Extra_labor_allowanceDTO allowanceDTO, String language) {
        AllowanceEmployeeInfoDTO allowanceEmployeeInfoDTO = AllowanceEmployeeInfoRepository.getInstance().getById(allowanceDTO.allowanceEmployeeInfoId);
        setEmployeeInfo(this, allowanceEmployeeInfoDTO, language);
        setAllowanceInfo(this, allowanceDTO, language);
    }

    private static void setEmployeeInfo(Extra_labor_allowanceModel model, AllowanceEmployeeInfoDTO employeeInfoDTO, String language) {
        model.employeeRecordsId = String.valueOf(employeeInfoDTO.employeeRecordId);
        model.name = getDataByLanguage(language, employeeInfoDTO.nameBn, employeeInfoDTO.nameEn);
        model.officeName = getDataByLanguage(language, employeeInfoDTO.officeNameBn, employeeInfoDTO.officeNameEn);
        model.organogramName = getDataByLanguage(language, employeeInfoDTO.organogramNameBn, employeeInfoDTO.organogramNameEn);
        model.mobileNumber = convertBanglaIfLanguageIsBangla(language, employeeInfoDTO.mobileNumber);
        model.savingAccountNumber = convertBanglaIfLanguageIsBangla(language, employeeInfoDTO.savingAccountNumber);
    }

    private static void setAllowanceInfo(Extra_labor_allowanceModel model, Extra_labor_allowanceDTO allowanceDTO, String language) {
        model.extraLaborAllowanceId = String.valueOf(allowanceDTO.iD);
        model.dailyRateKa = convertBanglaIfLanguageIsBangla(language, String.valueOf(allowanceDTO.dailyRateKa));
        model.dailyRateKha = convertBanglaIfLanguageIsBangla(language, String.valueOf(allowanceDTO.dailyRateKha));
        model.dayKa = convertBanglaIfLanguageIsBangla(language, String.valueOf(allowanceDTO.dayKa));
        model.dayKha = convertBanglaIfLanguageIsBangla(language, String.valueOf(allowanceDTO.dayKha));
        model.totalAmount = convertBanglaIfLanguageIsBangla(language, String.valueOf(allowanceDTO.totalAmount));
        model.revenueStampDeduction = convertBanglaIfLanguageIsBangla(language, String.valueOf(allowanceDTO.revenueStampDeduction));
        model.netAmount = convertBanglaIfLanguageIsBangla(language, String.valueOf(allowanceDTO.netAmount));
    }

    public boolean isValidModel() {
        return Integer.parseInt(netAmount) > 0;
    }
}
