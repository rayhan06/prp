package religion_allowance;

import allowance_configure.AllowanceTypeEnum;
import allowance_configure.Allowance_configureDTO;
import allowance_configure.Allowance_configureRepository;
import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import pb.Utils;
import user.UserDTO;

import java.util.Objects;

public class Religion_allowanceModel {
    public long iD = 0;
    public long allowanceEmployeeInfoId;
    public long employeeRecordsId;
    public int allowanceCatId;
    public String employeeName = "";
    public String designation = "";
    public String mobileNumber = "";
    public String savingAccountNumber = "";
    public int mainSalary = 0;
    public int bonusAmount = 0;
    public int totalAmount = 0;
    public int taxDeduction = 0;
    public long netAmount = 0;
    public int year;
    public long budgetOfficeUnitId;

    @Override
    public String toString() {
        return "Religion_allowanceModel{" +
               "iD=" + iD +
               ", allowanceEmployeeInfoId=" + allowanceEmployeeInfoId +
               ", allowanceCatId=" + allowanceCatId +
               ", employeeName='" + employeeName + '\'' +
               ", designation='" + designation + '\'' +
               ", mobileNumber='" + mobileNumber + '\'' +
               ", savingAccountNumber='" + savingAccountNumber + '\'' +
               ", mainSalary=" + mainSalary +
               ", bonusAmount=" + bonusAmount +
               ", totalAmount=" + totalAmount +
               ", taxDeduction=" + taxDeduction +
               ", netAmount=" + netAmount +
               '}';
    }

    public Religion_allowanceModel(AllowanceEmployeeInfoDTO infoDTO, int allowanceCat, int year, long budgetOfficeUnitId, String language) {
        allowanceEmployeeInfoId = infoDTO.iD;
        this.employeeRecordsId = infoDTO.employeeRecordId;
        this.allowanceCatId = allowanceCat;
        this.year = year;
        this.budgetOfficeUnitId = budgetOfficeUnitId;
        setEmployeeInfo(infoDTO, language);
        setAllowanceInfo();
    }

    public Religion_allowanceModel(Religion_allowanceDTO religion_allowanceDTO, int allowanceCat, int year,
                                   long budgetOfficeUnitId, String language) {
        this.iD = religion_allowanceDTO.iD;
        this.allowanceEmployeeInfoId = religion_allowanceDTO.allowanceEmployeeInfoId;
        AllowanceEmployeeInfoDTO infoDTO = AllowanceEmployeeInfoRepository.getInstance().getById(allowanceEmployeeInfoId);
        this.employeeRecordsId = infoDTO.employeeRecordId;
        this.allowanceCatId = allowanceCat;
        this.year = year;
        setEmployeeInfo(infoDTO, language);
        this.mainSalary = infoDTO.mainSalary;
        this.bonusAmount = religion_allowanceDTO.bonusAmount;
        this.taxDeduction = religion_allowanceDTO.taxDeduction;
        this.netAmount = this.bonusAmount - this.taxDeduction;
        this.budgetOfficeUnitId = budgetOfficeUnitId;
    }

    private void setEmployeeInfo(AllowanceEmployeeInfoDTO infoDTO, String language) {
        boolean isLanguageEnglish = "English".equalsIgnoreCase(language);
        this.employeeName = isLanguageEnglish ? infoDTO.nameEn : infoDTO.nameBn;
        this.designation = isLanguageEnglish ? infoDTO.organogramNameEn : infoDTO.organogramNameBn;
        this.mobileNumber = Utils.getDigits(infoDTO.mobileNumber, language);
        this.savingAccountNumber = Utils.getDigits(infoDTO.savingAccountNumber, language);
        this.mainSalary = infoDTO.mainSalary;
    }

    private void setAllowanceInfo() {
        Allowance_configureDTO configureDTO = Allowance_configureRepository.getInstance().getByAllowanceCat(allowanceCatId);
        bonusAmount = configureDTO.amountType == AllowanceTypeEnum.FIXED.getValue() ? configureDTO.allowanceAmount :
                           mainSalary * configureDTO.allowanceAmount / 100;
        totalAmount = mainSalary + bonusAmount;
        taxDeduction = configureDTO.getTaxDeductionAmount(bonusAmount);
        netAmount = bonusAmount - taxDeduction;
    }

    public Religion_allowanceDTO getReligionAllowance(long currentTime, UserDTO userDTO) {
        Religion_allowanceDTO religion_allowanceDTO = new Religion_allowanceDTO();
        religion_allowanceDTO.iD = this.iD;
        religion_allowanceDTO.year = this.year;
        religion_allowanceDTO.allowanceCat = this.allowanceCatId;
        religion_allowanceDTO.budgetOfficeUnitId = this.budgetOfficeUnitId;
        religion_allowanceDTO.allowanceEmployeeInfoId = AllowanceEmployeeInfoRepository.getInstance().getByEmployeeRecordId(employeeRecordsId).iD;
        religion_allowanceDTO.bonusAmount = this.bonusAmount;
        religion_allowanceDTO.taxDeduction = this.taxDeduction;
        if (religion_allowanceDTO.iD == 0) {
            religion_allowanceDTO.insertionTime = currentTime;
            religion_allowanceDTO.insertedBy = userDTO.ID;
        }
        religion_allowanceDTO.lastModificationTime = currentTime;
        religion_allowanceDTO.modifiedBy = userDTO.ID;
        return religion_allowanceDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Religion_allowanceModel that = (Religion_allowanceModel) o;
        return employeeRecordsId == that.employeeRecordsId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(employeeRecordsId);
    }
}
