package religion_allowance;

import allowance_configure.AllowanceCatEnum;
import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import budget_office.Budget_officeRepository;
import com.google.gson.Gson;
import common.BaseServlet;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


@WebServlet("/Religion_allowanceServlet")
@MultipartConfig
public class Religion_allowanceServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Religion_allowanceServlet.class);
    private final Religion_allowanceDAO religion_allowanceDAO = Religion_allowanceDAO.getInstance();

    @Override
    public String getTableName() {
        return "religion_allowance";
    }

    @Override
    public String getServletName() {
        return "Religion_allowanceServlet";
    }

    @Override
    public Religion_allowanceDAO getCommonDAOService() {
        return religion_allowanceDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        long curTime = System.currentTimeMillis();
        String Value = request.getParameter("religionAllowanceModels");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        Religion_allowanceModel[] allowanceModels = gson.fromJson(Value, Religion_allowanceModel[].class);
        List<Religion_allowanceDTO> allowanceDTOS = Arrays.stream(allowanceModels)
                                                          .distinct()
                                                          .map(model -> model.getReligionAllowance(curTime, userDTO))
                                                          .collect(Collectors.toList());
        long currentTimeInMilliseconds = new Date().getTime();
        String voucherNumber = Religion_allowanceDAO.getInstance().getNextVoucherNumber();
        for (Religion_allowanceDTO dto : allowanceDTOS) {
            dto.modifiedBy = userDTO.employee_record_id;
            dto.lastModificationTime = currentTimeInMilliseconds;
            if (dto.iD == 0) {
                dto.voucherNumber = voucherNumber;
                dto.insertedBy = userDTO.employee_record_id;
                dto.insertionTime = currentTimeInMilliseconds;
                religion_allowanceDAO.add(dto);
            } else {
                religion_allowanceDAO.update(dto);
            }
        }
        return allowanceDTOS.size() > 0 ? allowanceDTOS.get(0) : new Religion_allowanceDTO();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.RELIGION_ALLOWANCE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.RELIGION_ALLOWANCE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.RELIGION_ALLOWANCE_SEARCH};
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        Religion_allowanceDTO religion_allowanceDTO = (Religion_allowanceDTO) commonDTO;

        return getServletName() + "?actionType=generateBill&year=" + religion_allowanceDTO.year
               + "&budgetOfficeId=" + religion_allowanceDTO.budgetOfficeUnitId;
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Religion_allowanceServlet.class;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "ajax_getEmployeeList":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RELIGION_ALLOWANCE_ADD)) {
                        int year = Integer.parseInt(request.getParameter("year"));
                        int allowanceCat = Integer.parseInt(request.getParameter("allowanceCat"));
                        long budgetOfficeId = Long.parseLong(request.getParameter("budgetOfficeId"));
                        Map<String, Object> responseMap = getReligionAllowanceModels(allowanceCat, year, budgetOfficeId, language);
                        response.setContentType("application/json");
                        response.setCharacterEncoding("UTF-8");
                        response.getWriter().println(new Gson().toJson(responseMap));
                        response.getWriter().flush();
                        return;
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "generateBill":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RELIGION_ALLOWANCE_ADD)) {
                        long budgetOfficeId = Long.parseLong(request.getParameter("budgetOfficeId"));
                        int year = Integer.parseInt(request.getParameter("year"));
                        List<Religion_allowanceDTO> religion_allowanceDTOList = religion_allowanceDAO.getByYearAndBudgetOffice(year, budgetOfficeId);
                        request.setAttribute("religionAllowanceDTOList", religion_allowanceDTOList);
                        request.setAttribute("budgetOfficeId", budgetOfficeId);
                        request.setAttribute("year", year);
                        request.getRequestDispatcher(commonPartOfDispatchURL() + "Bill.jsp").forward(request, response);
                        return;
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                default:
                    super.doGet(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private Map<String, Object> getReligionAllowanceModels(int allowanceCat, int year, long budgetOfficeId, String language) {
        List<Religion_allowanceDTO> religion_allowanceDTOList = religion_allowanceDAO.getByYearAndAllowanceCat(year, allowanceCat, budgetOfficeId);
        List<Religion_allowanceModel> allowanceModels;
        Map<String, Object> responseMap = new HashMap<>();
        boolean isAlreadyAdded = religion_allowanceDTOList != null && religion_allowanceDTOList.size() > 0;
        responseMap.put("isAlreadyAdded", isAlreadyAdded);
        if (isAlreadyAdded) {
            allowanceModels = religion_allowanceDTOList.stream()
                                                       .map(dto -> new Religion_allowanceModel(dto, allowanceCat, year, budgetOfficeId, language))
                                                       .collect(Collectors.toList());
        } else {
            Set<Long> budgetOfficeIds = Budget_officeRepository.getInstance()
                                                               .getOfficeUnitIdSet(budgetOfficeId);
            List<AllowanceEmployeeInfoDTO> employeeInfoDTOs =
                    AllowanceEmployeeInfoRepository.getInstance().buildDTOsFromCache(budgetOfficeIds).stream()
                                                   .filter(dto -> allowanceCat == AllowanceCatEnum.BANGLA_NEW_YEAR_ALLOWANCE.getValue() || dto.religion == AllowanceCatEnum.getReligion(allowanceCat))
                                                   .collect(Collectors.toList());
            allowanceModels = employeeInfoDTOs.stream()
                                              .map(e -> new Religion_allowanceModel(e, allowanceCat, year, budgetOfficeId, language))
                                              .distinct()
                                              .collect(Collectors.toList());
        }
        responseMap.put("allowanceModels", allowanceModels);
        return responseMap;
    }
}

