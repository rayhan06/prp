package religion_allowance;

import allowance_configure.AllowanceCatEnum;
import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import common.CommonDAOService;
import dbm.DBMW;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import pb.OptionDTO;
import pb.Utils;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

import static util.StringUtils.convertToBanNumber;

@SuppressWarnings({"Duplicates"})
public class Religion_allowanceDAO implements CommonDAOService<Religion_allowanceDTO> {
    private static final int RELIGIOUS_CAT_LIMIT = 5;

    private static final Logger logger = Logger.getLogger(Religion_allowanceDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (bonus_amount,tax_deduction,allowance_employee_info_id,allowance_cat,year,budget_office_id,voucher_number,modified_by,lastModificationTime,search_column,"
                    .concat("inserted_by,insertion_time,isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery =
            "UPDATE {tableName} SET bonus_amount=?,tax_deduction=?,allowance_employee_info_id=?,allowance_cat=?,year=?,budget_office_id=?,voucher_number=?,"
                    .concat("modified_by=?,lastModificationTime=?,search_column=? WHERE ID=?");

    private static final String getByYearAndAllowanceCatAndBudgetOffice = "SELECT * FROM religion_allowance WHERE year=%d AND allowance_cat=%d AND budget_office_id=%d AND isDeleted=0";

    private static final String getByYearAndBudgetOffice = "SELECT * FROM religion_allowance WHERE year=%d AND budget_office_id=%d AND isDeleted=0";
    private static final Map<String, String> searchMap = new HashMap<>();

    private Religion_allowanceDAO() {
        searchMap.put("AnyField", " AND (search_column LIKE ?) ");
        searchMap.put("allowance_cat", " AND (allowance_cat = ?) ");
        searchMap.put("year", " AND (year = ?) ");
    }

    private static class LazyLoader {
        static final Religion_allowanceDAO INSTANCE = new Religion_allowanceDAO();
    }

    public static Religion_allowanceDAO getInstance() {
        return Religion_allowanceDAO.LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Religion_allowanceDTO religion_allowanceDTO) {
        AllowanceEmployeeInfoDTO allowanceEmployeeInfoDTO =
                AllowanceEmployeeInfoRepository.getInstance().getById(religion_allowanceDTO.allowanceEmployeeInfoId);

        religion_allowanceDTO.searchColumn =
                allowanceEmployeeInfoDTO.nameEn + " " + allowanceEmployeeInfoDTO.nameBn + " "
                + allowanceEmployeeInfoDTO.officeNameEn + " "
                + allowanceEmployeeInfoDTO.officeNameBn + " "
                + allowanceEmployeeInfoDTO.organogramNameEn + " "
                + allowanceEmployeeInfoDTO.organogramNameBn + " "
                + allowanceEmployeeInfoDTO.mobileNumber + " "
                + convertToBanNumber(allowanceEmployeeInfoDTO.mobileNumber) + " "
                + allowanceEmployeeInfoDTO.savingAccountNumber + " "
                + convertToBanNumber(allowanceEmployeeInfoDTO.savingAccountNumber);
    }

    @Override
    public void set(PreparedStatement ps, Religion_allowanceDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(dto);
        ps.setInt(++index, dto.bonusAmount);
        ps.setInt(++index, dto.taxDeduction);
        ps.setLong(++index, dto.allowanceEmployeeInfoId);
        ps.setLong(++index, dto.allowanceCat);
        ps.setInt(++index, dto.year);
        ps.setLong(++index, dto.budgetOfficeUnitId);
        ps.setString(++index, dto.voucherNumber);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        ps.setString(++index, dto.searchColumn);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Religion_allowanceDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Religion_allowanceDTO dto = new Religion_allowanceDTO();
            dto.iD = rs.getLong("ID");
            dto.bonusAmount = rs.getInt("bonus_amount");
            dto.taxDeduction = rs.getInt("tax_deduction");
            dto.allowanceEmployeeInfoId = rs.getLong("allowance_employee_info_id");
            dto.allowanceCat = rs.getLong("allowance_cat");
            dto.year = rs.getInt("year");
            dto.budgetOfficeUnitId = rs.getLong("budget_office_id");
            dto.voucherNumber = rs.getString("voucher_number");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.searchColumn = rs.getString("search_column");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "religion_allowance";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Religion_allowanceDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Religion_allowanceDTO) commonDTO, updateQuery, false);
    }

    public List<Religion_allowanceDTO> getByYearAndAllowanceCat(int year, int allowanceCat, long budgetOfficeUnitId) {
        return getDTOs(String.format(getByYearAndAllowanceCatAndBudgetOffice, year, allowanceCat, budgetOfficeUnitId));
    }

    public List<Religion_allowanceDTO> getByYearAndBudgetOffice(int year, long budgetOfficeUnitId) {
        return getDTOs(String.format(getByYearAndBudgetOffice, year, budgetOfficeUnitId));
    }

    public String buildReligionAllowanceCat(String language, Long selectedId) {
        List<CategoryLanguageModel> allowanceModelList = CatRepository.getInstance().getCategoryLanguageModelList("allowance");
        List<OptionDTO> optionDTOList = allowanceModelList.stream()
                                                          .filter(dto -> dto.categoryValue <= RELIGIOUS_CAT_LIMIT || dto.categoryValue == AllowanceCatEnum.BANGLA_NEW_YEAR_ALLOWANCE.getValue())
                                                          .map(dto -> new OptionDTO(dto.englishText, dto.banglaText, String.valueOf(dto.categoryValue)))
                                                          .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String buildYears(String language, Long selectedId) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        List<Integer> yearList = new ArrayList<>();
        yearList.add(currentYear - 1);
        yearList.add(currentYear);
        yearList.add(currentYear + 1);
        List<OptionDTO> optionDTOList = yearList.stream()
                                                .map(this::getOptionDTO)
                                                .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? String.valueOf(currentYear) : String.valueOf(selectedId));
    }

    private OptionDTO getOptionDTO(int year) {
        String englishText = Utils.getDigits(year, "ENGLISH");
        String banglaText = Utils.getDigits(year, "BANGLA");
        return new OptionDTO(englishText, banglaText, String.valueOf(year));
    }

    public int getCurrentYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    public int getSelectedFestivalSum(List<Religion_allowanceDTO> allowanceDTOs, int allowanceCat) {
        return allowanceDTOs.stream()
                            .filter(dto -> dto.allowanceCat == allowanceCat)
                            .map(Religion_allowanceDTO::getBillAmount)
                            .mapToInt(Integer::intValue).sum();
    }

    public String getNextVoucherNumber() throws Exception {
        long nextId = DBMW.getInstance().getNextSequenceId(getTableName());
        return "FT-" + nextId;
    }
}
	