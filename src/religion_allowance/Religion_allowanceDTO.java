package religion_allowance;

import sessionmanager.SessionConstants;
import util.CommonDTO;


public class Religion_allowanceDTO extends CommonDTO {
    public int bonusAmount = 0;
    public int taxDeduction = 0;
    public long allowanceEmployeeInfoId = -1;
    public long allowanceCat = 0;
    public long budgetOfficeUnitId = 0;
    public int year = -1;
    public String voucherNumber = "";
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;
    public long modifiedBy = -1;

    public int getBillAmount() {
        return bonusAmount - taxDeduction;
    }

    @Override
    public String toString() {
        return "Religion_allowanceDTO{" +
                "bonusAmount=" + bonusAmount +
                ", taxDeduction=" + taxDeduction +
                ", allowanceEmployeeInfoId=" + allowanceEmployeeInfoId +
                ", allowanceCat=" + allowanceCat +
                ", budgetOfficeUnitId=" + budgetOfficeUnitId +
                ", year=" + year +
                ", voucherNumber='" + voucherNumber + '\'' +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                '}';
    }
}