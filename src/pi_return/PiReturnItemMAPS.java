package pi_return;
import java.util.*; 
import util.*;


public class PiReturnItemMAPS extends CommonMaps
{	
	public PiReturnItemMAPS(String tableName)
	{
		


		java_SQL_map.put("requester_org_id".toLowerCase(), "requesterOrgId".toLowerCase());
		java_SQL_map.put("requester_office_id".toLowerCase(), "requesterOfficeId".toLowerCase());
		java_SQL_map.put("requester_office_unit_id".toLowerCase(), "requesterOfficeUnitId".toLowerCase());
		java_SQL_map.put("requester_emp_id".toLowerCase(), "requesterEmpId".toLowerCase());
		java_SQL_map.put("requester_phone_num".toLowerCase(), "requesterPhoneNum".toLowerCase());
		java_SQL_map.put("requester_name_en".toLowerCase(), "requesterNameEn".toLowerCase());
		java_SQL_map.put("requester_name_bn".toLowerCase(), "requesterNameBn".toLowerCase());
		java_SQL_map.put("requester_office_name_en".toLowerCase(), "requesterOfficeNameEn".toLowerCase());
		java_SQL_map.put("requester_office_name_bn".toLowerCase(), "requesterOfficeNameBn".toLowerCase());
		java_SQL_map.put("requester_office_unit_name_en".toLowerCase(), "requesterOfficeUnitNameEn".toLowerCase());
		java_SQL_map.put("requester_office_unit_name_bn".toLowerCase(), "requesterOfficeUnitNameBn".toLowerCase());
		java_SQL_map.put("requester_office_unit_org_name_en".toLowerCase(), "requesterOfficeUnitOrgNameEn".toLowerCase());
		java_SQL_map.put("requester_office_unit_org_name_bn".toLowerCase(), "requesterOfficeUnitOrgNameBn".toLowerCase());
		java_SQL_map.put("purchase_date".toLowerCase(), "purchaseDate".toLowerCase());
		java_SQL_map.put("package_id".toLowerCase(), "packageId".toLowerCase());
		java_SQL_map.put("goods_type_id".toLowerCase(), "goodsTypeId".toLowerCase());
		java_SQL_map.put("item_id".toLowerCase(), "itemId".toLowerCase());
		java_SQL_map.put("return_amount".toLowerCase(), "returnAmount".toLowerCase());
		java_SQL_map.put("requisition_amount".toLowerCase(), "requisitionAmount".toLowerCase());
		java_SQL_map.put("remaining_amount".toLowerCase(), "remainingAmount".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Requester Org Id".toLowerCase(), "requesterOrgId".toLowerCase());
		java_Text_map.put("Requester Office Id".toLowerCase(), "requesterOfficeId".toLowerCase());
		java_Text_map.put("Requester Office Unit Id".toLowerCase(), "requesterOfficeUnitId".toLowerCase());
		java_Text_map.put("Requester Emp Id".toLowerCase(), "requesterEmpId".toLowerCase());
		java_Text_map.put("Requester Phone Num".toLowerCase(), "requesterPhoneNum".toLowerCase());
		java_Text_map.put("Requester Name En".toLowerCase(), "requesterNameEn".toLowerCase());
		java_Text_map.put("Requester Name Bn".toLowerCase(), "requesterNameBn".toLowerCase());
		java_Text_map.put("Requester Office Name En".toLowerCase(), "requesterOfficeNameEn".toLowerCase());
		java_Text_map.put("Requester Office Name Bn".toLowerCase(), "requesterOfficeNameBn".toLowerCase());
		java_Text_map.put("Requester Office Unit Name En".toLowerCase(), "requesterOfficeUnitNameEn".toLowerCase());
		java_Text_map.put("Requester Office Unit Name Bn".toLowerCase(), "requesterOfficeUnitNameBn".toLowerCase());
		java_Text_map.put("Requester Office Unit Org Name En".toLowerCase(), "requesterOfficeUnitOrgNameEn".toLowerCase());
		java_Text_map.put("Requester Office Unit Org Name Bn".toLowerCase(), "requesterOfficeUnitOrgNameBn".toLowerCase());
		java_Text_map.put("Purchase Date".toLowerCase(), "purchaseDate".toLowerCase());
		java_Text_map.put("Package Id".toLowerCase(), "packageId".toLowerCase());
		java_Text_map.put("Goods Type Id".toLowerCase(), "goodsTypeId".toLowerCase());
		java_Text_map.put("Item Id".toLowerCase(), "itemId".toLowerCase());
		java_Text_map.put("Return Amount".toLowerCase(), "returnAmount".toLowerCase());
		java_Text_map.put("Requisition Amount".toLowerCase(), "requisitionAmount".toLowerCase());
		java_Text_map.put("Remaining Amount".toLowerCase(), "remainingAmount".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Last Modifier User".toLowerCase(), "lastModifierUser".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}