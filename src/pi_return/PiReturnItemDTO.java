package pi_return;
import java.util.*; 
import util.*; 


public class PiReturnItemDTO extends CommonDTO
{

	public long requesterOrgId = -1;
	public long requesterOfficeId = -1;
	public long requesterOfficeUnitId = -1;
	public long requesterEmpId = -1;
    public String requesterPhoneNum = "";
    public String requesterNameEn = "";
    public String requesterNameBn = "";
    public String requesterOfficeNameEn = "";
    public String requesterOfficeNameBn = "";
    public String requesterOfficeUnitNameEn = "";
    public String requesterOfficeUnitNameBn = "";
    public String requesterOfficeUnitOrgNameEn = "";
    public String requesterOfficeUnitOrgNameBn = "";
	public long purchaseDate = System.currentTimeMillis();
	public long returnDate = System.currentTimeMillis();
	public long packageId = -1;
	public long goodsTypeId = -1;
	public long piReturnId = -1;
	public long itemId = -1;
	public double returnAmount = -1;
	public double requisitionAmount = -1;
	public double remainingAmount = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	
	public List<PiReturnItemDTO> piReturnItemDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$PiReturnItemDTO[" +
            " iD = " + iD +
            " requesterOrgId = " + requesterOrgId +
            " requesterOfficeId = " + requesterOfficeId +
            " requesterOfficeUnitId = " + requesterOfficeUnitId +
            " requesterEmpId = " + requesterEmpId +
            " requesterPhoneNum = " + requesterPhoneNum +
            " requesterNameEn = " + requesterNameEn +
            " requesterNameBn = " + requesterNameBn +
            " requesterOfficeNameEn = " + requesterOfficeNameEn +
            " requesterOfficeNameBn = " + requesterOfficeNameBn +
            " requesterOfficeUnitNameEn = " + requesterOfficeUnitNameEn +
            " requesterOfficeUnitNameBn = " + requesterOfficeUnitNameBn +
            " requesterOfficeUnitOrgNameEn = " + requesterOfficeUnitOrgNameEn +
            " requesterOfficeUnitOrgNameBn = " + requesterOfficeUnitOrgNameBn +
            " purchaseDate = " + purchaseDate +
			" returnDate = " + returnDate +
            " packageId = " + packageId +
            " goodsTypeId = " + goodsTypeId +
			" piReturnId = " + piReturnId +
            " itemId = " + itemId +
            " returnAmount = " + returnAmount +
            " requisitionAmount = " + requisitionAmount +
            " remainingAmount = " + remainingAmount +
            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}