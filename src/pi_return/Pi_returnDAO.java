package pi_return;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Pi_returnDAO  implements CommonDAOService<Pi_returnDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Pi_returnDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"requester_org_id",
			"requester_office_id",
			"requester_office_unit_id",
			"requester_emp_id",
			"requester_phone_num",
			"requester_name_en",
			"requester_name_bn",
			"requester_office_name_en",
			"requester_office_name_bn",
			"requester_office_unit_name_en",
			"requester_office_unit_name_bn",
			"requester_office_unit_org_name_en",
			"requester_office_unit_org_name_bn",
			"purchase_date",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("requester_phone_num"," and (requester_phone_num like ?)");
		searchMap.put("requester_name_en"," and (requester_name_en like ?)");
		searchMap.put("requester_name_bn"," and (requester_name_bn like ?)");
		searchMap.put("requester_office_name_en"," and (requester_office_name_en like ?)");
		searchMap.put("requester_office_name_bn"," and (requester_office_name_bn like ?)");
		searchMap.put("requester_office_unit_name_en"," and (requester_office_unit_name_en like ?)");
		searchMap.put("requester_office_unit_name_bn"," and (requester_office_unit_name_bn like ?)");
		searchMap.put("requester_office_unit_org_name_en"," and (requester_office_unit_org_name_en like ?)");
		searchMap.put("requester_office_unit_org_name_bn"," and (requester_office_unit_org_name_bn like ?)");
		searchMap.put("purchase_date_start"," and (purchase_date >= ?)");
		searchMap.put("purchase_date_end"," and (purchase_date <= ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Pi_returnDAO INSTANCE = new Pi_returnDAO();
	}

	public static Pi_returnDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Pi_returnDTO pi_returnDTO)
	{
		pi_returnDTO.searchColumn = "";
		pi_returnDTO.searchColumn += pi_returnDTO.requesterPhoneNum + " ";
		pi_returnDTO.searchColumn += pi_returnDTO.requesterNameEn + " ";
		pi_returnDTO.searchColumn += pi_returnDTO.requesterNameBn + " ";
		pi_returnDTO.searchColumn += pi_returnDTO.requesterOfficeNameEn + " ";
		pi_returnDTO.searchColumn += pi_returnDTO.requesterOfficeNameBn + " ";
		pi_returnDTO.searchColumn += pi_returnDTO.requesterOfficeUnitNameEn + " ";
		pi_returnDTO.searchColumn += pi_returnDTO.requesterOfficeUnitNameBn + " ";
		pi_returnDTO.searchColumn += pi_returnDTO.requesterOfficeUnitOrgNameEn + " ";
		pi_returnDTO.searchColumn += pi_returnDTO.requesterOfficeUnitOrgNameBn + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Pi_returnDTO pi_returnDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(pi_returnDTO);
		if(isInsert)
		{
			ps.setObject(++index,pi_returnDTO.iD);
		}
		ps.setObject(++index,pi_returnDTO.requesterOrgId);
		ps.setObject(++index,pi_returnDTO.requesterOfficeId);
		ps.setObject(++index,pi_returnDTO.requesterOfficeUnitId);
		ps.setObject(++index,pi_returnDTO.requesterEmpId);
		ps.setObject(++index,pi_returnDTO.requesterPhoneNum);
		ps.setObject(++index,pi_returnDTO.requesterNameEn);
		ps.setObject(++index,pi_returnDTO.requesterNameBn);
		ps.setObject(++index,pi_returnDTO.requesterOfficeNameEn);
		ps.setObject(++index,pi_returnDTO.requesterOfficeNameBn);
		ps.setObject(++index,pi_returnDTO.requesterOfficeUnitNameEn);
		ps.setObject(++index,pi_returnDTO.requesterOfficeUnitNameBn);
		ps.setObject(++index,pi_returnDTO.requesterOfficeUnitOrgNameEn);
		ps.setObject(++index,pi_returnDTO.requesterOfficeUnitOrgNameBn);
		ps.setObject(++index,pi_returnDTO.purchaseDate);
		ps.setObject(++index,pi_returnDTO.searchColumn);
		ps.setObject(++index,pi_returnDTO.insertedByUserId);
		ps.setObject(++index,pi_returnDTO.insertedByOrganogramId);
		ps.setObject(++index,pi_returnDTO.insertionDate);
		ps.setObject(++index,pi_returnDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(++index,pi_returnDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,pi_returnDTO.iD);
		}
	}
	
	@Override
	public Pi_returnDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Pi_returnDTO pi_returnDTO = new Pi_returnDTO();
			int i = 0;
			pi_returnDTO.iD = rs.getLong(columnNames[i++]);
			pi_returnDTO.requesterOrgId = rs.getLong(columnNames[i++]);
			pi_returnDTO.requesterOfficeId = rs.getLong(columnNames[i++]);
			pi_returnDTO.requesterOfficeUnitId = rs.getLong(columnNames[i++]);
			pi_returnDTO.requesterEmpId = rs.getLong(columnNames[i++]);
			pi_returnDTO.requesterPhoneNum = rs.getString(columnNames[i++]);
			pi_returnDTO.requesterNameEn = rs.getString(columnNames[i++]);
			pi_returnDTO.requesterNameBn = rs.getString(columnNames[i++]);
			pi_returnDTO.requesterOfficeNameEn = rs.getString(columnNames[i++]);
			pi_returnDTO.requesterOfficeNameBn = rs.getString(columnNames[i++]);
			pi_returnDTO.requesterOfficeUnitNameEn = rs.getString(columnNames[i++]);
			pi_returnDTO.requesterOfficeUnitNameBn = rs.getString(columnNames[i++]);
			pi_returnDTO.requesterOfficeUnitOrgNameEn = rs.getString(columnNames[i++]);
			pi_returnDTO.requesterOfficeUnitOrgNameBn = rs.getString(columnNames[i++]);
			pi_returnDTO.purchaseDate = rs.getLong(columnNames[i++]);
			pi_returnDTO.searchColumn = rs.getString(columnNames[i++]);
			pi_returnDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			pi_returnDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			pi_returnDTO.insertionDate = rs.getLong(columnNames[i++]);
			pi_returnDTO.lastModifierUser = rs.getString(columnNames[i++]);
			pi_returnDTO.isDeleted = rs.getInt(columnNames[i++]);
			pi_returnDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return pi_returnDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Pi_returnDTO getDTOByID (long id)
	{
		Pi_returnDTO pi_returnDTO = null;
		try 
		{
			pi_returnDTO = getDTOFromID(id);
			if(pi_returnDTO != null)
			{
				PiReturnItemDAO piReturnItemDAO = PiReturnItemDAO.getInstance();				
				List<PiReturnItemDTO> piReturnItemDTOList = (List<PiReturnItemDTO>)piReturnItemDAO.getDTOsByParent("pi_return_id", pi_returnDTO.iD);
				pi_returnDTO.piReturnItemDTOList = piReturnItemDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return pi_returnDTO;
	}

	@Override
	public String getTableName() {
		return "pi_return";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Pi_returnDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Pi_returnDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
				
}
	