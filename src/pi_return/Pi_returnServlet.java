package pi_return;

import com.google.gson.Gson;
import common.ApiResponse;
import common.BaseServlet;
import employee_assign.EmployeeSearchModalUtil;
import employee_assign.EmployeeSearchModel;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_offices.EmployeeOfficesDAO;
import employee_records.Employee_recordsRepository;
import login.LoginDTO;
import offices.OfficesDTO;
import offices.OfficesRepository;
import org.apache.log4j.Logger;
import pb.Utils;
import permission.MenuConstants;
import pi_unique_item.*;
import procurement_goods.Procurement_goodsDTO;
import procurement_goods.Procurement_goodsRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonLoginData;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Servlet implementation class Pi_returnServlet
 */
@WebServlet("/Pi_returnServlet")
@MultipartConfig
public class Pi_returnServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_returnServlet.class);
    private final EmployeeOfficesDAO employeeOfficesDAO = new EmployeeOfficesDAO();

    @Override
    public String getTableName() {
        return Pi_returnDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_returnServlet";
    }

    @Override
    public Pi_returnDAO getCommonDAOService() {
        return Pi_returnDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PI_RETURN_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PI_RETURN_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PI_RETURN_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_returnServlet.class;
    }

    PiReturnItemDAO piReturnItemDAO = PiReturnItemDAO.getInstance();
    private final Gson gson = new Gson();


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        // TODO Auto-generated method stub


        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        Pi_returnDTO pi_returnDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLanEng = Language.equalsIgnoreCase("English");

        if (!addFlag) {
            throw new Exception(isLanEng ? "This page is not editable" : "এই পেজটি সম্পাদনাযোগ্য নয়");
        }

        if (addFlag) {
            pi_returnDTO = new Pi_returnDTO();

            pi_returnDTO.insertionDate = pi_returnDTO.lastModificationTime = System.currentTimeMillis();
            pi_returnDTO.insertedByUserId = userDTO.ID;
            pi_returnDTO.lastModifierUser = userDTO.userName;
        } else {
            pi_returnDTO = Pi_returnDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
            if (pi_returnDTO == null) {
                throw new Exception(isLanEng ? "Product return request information is not found" : "পণ্য ফেরতের অনুরোধ খুঁজে পাওয়া যায়নি");
            }
            pi_returnDTO.lastModificationTime = System.currentTimeMillis();
            pi_returnDTO.lastModifierUser = userDTO.userName;
        }

        String Value = "";

        Value = request.getParameter("employeeRecordId");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide employee" : "অনুগ্রহপূর্বক এমপ্লয়ি প্রদান করুন");
        }


        EmployeeOfficeDTO model = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(Long.parseLong(Value));
        pi_returnDTO.requesterEmpId = model.employeeRecordId;
        pi_returnDTO.requesterOfficeUnitId = model.officeUnitId;
        pi_returnDTO.requesterOrgId = model.officeUnitOrganogramId;

        EmployeeSearchModel model1 = (gson.fromJson
                (EmployeeSearchModalUtil.getEmployeeSearchModelJson
                                (model.employeeRecordId,
                                        model.officeUnitId,
                                        model.officeUnitOrganogramId),
                        EmployeeSearchModel.class)
        );

        pi_returnDTO.requesterNameEn = model1.employeeNameEn;
        pi_returnDTO.requesterNameBn = model1.employeeNameBn;
        pi_returnDTO.requesterOfficeUnitNameEn = model1.officeUnitNameEn;
        pi_returnDTO.requesterOfficeUnitNameBn = model1.officeUnitNameBn;
        pi_returnDTO.requesterOfficeUnitOrgNameEn = model1.organogramNameEn;
        pi_returnDTO.requesterOfficeUnitOrgNameBn = model1.organogramNameBn;
        pi_returnDTO.requesterPhoneNum = model1.phoneNumber;

        OfficesDTO requesterOffice = OfficesRepository.getInstance().getOfficesDTOByID(model.officeId);

        if (requesterOffice != null) {
            pi_returnDTO.requesterOfficeId = requesterOffice.iD;
            pi_returnDTO.requesterOfficeNameEn = requesterOffice.officeNameEng;
            pi_returnDTO.requesterOfficeNameBn = requesterOffice.officeNameBng;
        }

//            Value = request.getParameter("purchaseDate");
//            if (Value == null || Value.equals("") || Value.equals("-1")) {
//                throw new Exception(isLanEng ? "Please provide purchase date" : "অনুগ্রহপূর্বক বিতরণের তারিখ প্রদান করুন");
//            }
//            pi_returnDTO.purchaseDate = f.parse(request.getParameter("assignmentDate")).getTime();
        Value = request.getParameter("returnDate");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide return date" : "অনুগ্রহপূর্বক পণ্য ফেরতের তারিখ প্রদান করুন");
        }
        long returnDate = f.parse(request.getParameter("returnDate")).getTime();


        int submittedPriceLen = request.getParameterValues("selected_items").length;
        if (submittedPriceLen < 1) {
            throw new Exception(isLanEng ? "Please provide item" : "অনুগ্রহপূর্বক পণ্য বাছাই করুন");
        }
        if (submittedPriceLen > 1000) {
            throw new Exception(isLanEng ? "Please provide items less than 1000" : "অনুগ্রহপূর্বক ফেরতযোগ্য পণ্য সংখ্যা ১০০০ এর চাইতে কম প্রদান করুন");
        }
        int totalLen = request.getParameterValues("hiddenCheckbox").length;
        HashMap<String, Long> map = new HashMap<String, Long>();

        Set<Long> itemSet = new HashSet<Long>();
        for (int i = 0; i < totalLen; i++) {
            String hiddenCheckBoxValue = request.getParameterValues("hiddenCheckbox")[i];
            long itemId = Long.parseLong(request.getParameterValues("itemId")[i]);
            if (hiddenCheckBoxValue.equals("1")) {
                itemSet.add(itemId);
            }

        }
        String mapKey = "";
        for (int i = 0; i < totalLen; i++) {
            String hiddenCheckBoxValue = request.getParameterValues("hiddenCheckbox")[i];
            long tableId = Long.parseLong(request.getParameterValues("tableId")[i]);
            long uniqueItemId = Long.parseLong(request.getParameterValues("uniqueItemId")[i]);
            long itemId = Long.parseLong(request.getParameterValues("itemId")[i]);
            //long purchaseId = Long.parseLong(request.getParameterValues("purchaseId")[i]);
            mapKey = uniqueItemId + "-unChecked";
            long requisitionId = PiUniqueItemAssignmentDAO.getInstance().getLatestRequisitionIdForItem(uniqueItemId);

            if (hiddenCheckBoxValue.equals("1")) {
                Utils.handleTransaction(()->{
                    PiUniqueItemAssignmentDAO.getInstance().updateReturnedItem(PiStageEnum.IN_STOCK.getValue(),
                            returnDate, pi_returnDTO.lastModificationTime, userDTO.ID, uniqueItemId,
                            PiActionTypeEnum.PURCHASE.getValue(), tableId, 1,pi_returnDTO.requesterOrgId);
                    recordItemTransaction(pi_returnDTO, itemId, requisitionId, userDTO);
                });
                mapKey = uniqueItemId + "-checked";
            }
            if (itemSet.contains(itemId)) {
                map.put(mapKey, itemId);
            }
        }

        logger.debug("Done adding  addPi_return dto = " + pi_returnDTO);


        List<PiReturnItemDTO> piReturnItemDTOList = createPiReturnItemDTOListByRequest(request, language, itemSet, map, pi_returnDTO);

        Utils.handleTransaction(()->{
            if (addFlag) {
                Pi_returnDAO.getInstance().add(pi_returnDTO);
            } else {
                Pi_returnDAO.getInstance().update(pi_returnDTO);
            }


            if (addFlag) {
                if (piReturnItemDTOList != null) {
                    for (PiReturnItemDTO piReturnItemDTO : piReturnItemDTOList) {
                        piReturnItemDTO.piReturnId = pi_returnDTO.iD;
                        piReturnItemDAO.add(piReturnItemDTO);
                    }
                }
            }
        });

        return pi_returnDTO;


    }


    public void updateRemovalProduct(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {

        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLanEng = Language.equalsIgnoreCase("English");

        if (addFlag) {
            throw new Exception(isLanEng ? "This page is not editable" : "এই পেজটি সম্পাদনাযোগ্য নয়");
        }

        String Value = "";

        EmployeeOfficeDTO model = null;

        Value = request.getParameter("employeeOfficeId");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            String ValueEmployeeRecordId = request.getParameter("employeeRecordId");
            String ValueOrganogramId = request.getParameter("organogramId");
            if (ValueEmployeeRecordId == null || ValueEmployeeRecordId.equals("") || ValueEmployeeRecordId.equals("-1") ||
                    ValueOrganogramId == null || ValueOrganogramId.equals("") || ValueOrganogramId.equals("-1")) {
                throw new Exception(isLanEng ? "Please provide employee" : "অনুগ্রহপূর্বক এমপ্লয়ি প্রদান করুন");
            }
            else {
                model = employeeOfficesDAO.getLastByEmployeeRecordIdAndOrganogramId(Long.parseLong(ValueEmployeeRecordId), Long.parseLong(ValueOrganogramId));
            }
        }
        else {
            model = employeeOfficesDAO.getById(Long.parseLong(Value));
        }

//        Value = request.getParameter("employeeRecordId");
//        if (Value == null || Value.equals("") || Value.equals("-1")) {
//            throw new Exception(isLanEng ? "Please provide employee" : "অনুগ্রহপূর্বক এমপ্লয়ি প্রদান করুন");
//        }

//        EmployeeOfficeDTO model = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(Long.parseLong(Value));
//        EmployeeOfficeDTO model = EmployeeOfficeRepository.getInstance().getById(Long.parseLong(Value));

//        Value = request.getParameter("returnDate");
//        if (Value == null || Value.equals("") || Value.equals("-1")) {
//            throw new Exception(isLanEng ? "Please provide product moving date" : "অনুগ্রহপূর্বক পণ্য স্থানান্তরের তারিখ প্রদান করুন");
//        }
//        long returnDate = f.parse(request.getParameter("returnDate")).getTime();

        if (request.getParameterValues("selected_items") != null && request.getParameterValues("selected_items").length > 1000) {
            throw new Exception(isLanEng ? "Please provide items less than 1000" : "অনুগ্রহপূর্বক ফেরতযোগ্য পণ্য সংখ্যা ১০০০ এর চাইতে কম প্রদান করুন");
        }
        int totalLen = request.getParameterValues("hiddenCheckbox").length;
        EmployeeOfficeDTO finalModel = model;
        Utils.handleTransaction(()->{
            for (int i = 0; i < totalLen; i++) {
                String hiddenCheckBoxValue = request.getParameterValues("hiddenCheckbox")[i];
                long tableId = Long.parseLong(request.getParameterValues("tableId")[i]);

                int taken = hiddenCheckBoxValue.equals("1")?1:0;

                if (finalModel.lastOfficeDate != SessionConstants.MIN_DATE) {
                    PiUniqueItemAssignmentDAO.getInstance().moveTakenItems(taken, finalModel.lastOfficeDate + (24*60*60*1000 - 1),System.currentTimeMillis(), userDTO.ID, tableId, finalModel.officeUnitOrganogramId);
                }
                else {
                    PiUniqueItemAssignmentDAO.getInstance().moveTakenItemsWithoutToDate(taken, System.currentTimeMillis(), userDTO.ID, tableId, finalModel.officeUnitOrganogramId);
                }

            }
            PiUniqueItemAssignmentDAO.getInstance().handleInventoryAfterEmployeeSuspend(finalModel.employeeRecordId);
        });

    }

    private List<PiReturnItemDTO> createPiReturnItemDTOListByRequest(HttpServletRequest request, String language,
                                                                     Set<Long> itemSet, HashMap<String, Long> map, Pi_returnDTO pi_returnDTO) throws Exception {
        List<PiReturnItemDTO> piReturnItemDTOList = new ArrayList<PiReturnItemDTO>();
        Iterator<Long> i = itemSet.iterator();
        while (i.hasNext()) {
            PiReturnItemDTO piReturnItemDTO = createPiReturnItemDTOByRequestAndIndex(request, true, 0, language, map, pi_returnDTO, i.next());
            piReturnItemDTOList.add(piReturnItemDTO);
        }
        return piReturnItemDTOList;
    }

    private PiReturnItemDTO createPiReturnItemDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index,
                                                                   String language, HashMap<String, Long> map, Pi_returnDTO pi_returnDTO, Long itemId) throws Exception {

        PiReturnItemDTO piReturnItemDTO;
        if (addFlag) {
            piReturnItemDTO = new PiReturnItemDTO();
        } else {
            piReturnItemDTO = piReturnItemDAO.getDTOByID(Long.parseLong(request.getParameterValues("piReturnItem.iD")[index]));
        }

        piReturnItemDTO.requesterOrgId = pi_returnDTO.requesterOrgId;
        piReturnItemDTO.requesterOfficeId = pi_returnDTO.requesterOfficeId;
        piReturnItemDTO.requesterOfficeUnitId = pi_returnDTO.requesterOfficeUnitId;
        piReturnItemDTO.requesterEmpId = pi_returnDTO.requesterEmpId;
        piReturnItemDTO.requesterPhoneNum = pi_returnDTO.requesterPhoneNum;
        piReturnItemDTO.requesterNameEn = pi_returnDTO.requesterNameEn;
        piReturnItemDTO.requesterNameBn = pi_returnDTO.requesterNameBn;
        piReturnItemDTO.requesterOfficeNameEn = pi_returnDTO.requesterOfficeNameEn;
        piReturnItemDTO.requesterOfficeNameBn = pi_returnDTO.requesterOfficeNameBn;
        piReturnItemDTO.requesterOfficeUnitNameEn = pi_returnDTO.requesterOfficeUnitNameEn;
        piReturnItemDTO.requesterOfficeUnitNameBn = pi_returnDTO.requesterOfficeUnitNameBn;
        piReturnItemDTO.requesterOfficeUnitOrgNameEn = pi_returnDTO.requesterOfficeUnitOrgNameEn;
        piReturnItemDTO.requesterOfficeUnitOrgNameBn = pi_returnDTO.requesterOfficeUnitOrgNameBn;
        piReturnItemDTO.purchaseDate = pi_returnDTO.purchaseDate;
        piReturnItemDTO.returnDate = pi_returnDTO.purchaseDate;

        piReturnItemDTO.itemId = itemId;
        Procurement_goodsDTO procurementGoodsDTO = Procurement_goodsRepository.getInstance().getProcurement_goodsDTOByID(piReturnItemDTO.itemId);
        piReturnItemDTO.goodsTypeId = procurementGoodsDTO.procurementGoodsTypeId;
        piReturnItemDTO.packageId = procurementGoodsDTO.procurementPackageId;

        long requisitionAmount = 0, returnAmount = 0, remainingAmount = 0;

        for (Map.Entry<String, Long> entry : map.entrySet()) {
            if (entry.getValue().equals(itemId)) {
                requisitionAmount++;
                if (entry.getValue().equals(itemId) && entry.getKey().contains("checked")) {
                    returnAmount++;
                }
            }

        }

        piReturnItemDTO.returnAmount = returnAmount;
        piReturnItemDTO.requisitionAmount = requisitionAmount;
        piReturnItemDTO.remainingAmount = requisitionAmount - returnAmount;

        piReturnItemDTO.insertionDate = piReturnItemDTO.lastModificationTime = pi_returnDTO.insertionDate;
        piReturnItemDTO.insertedByUserId = pi_returnDTO.insertedByUserId;
        piReturnItemDTO.lastModifierUser = pi_returnDTO.lastModifierUser;

        return piReturnItemDTO;

    }

    private void recordItemTransaction(Pi_returnDTO pi_returnDTO, long itemId, long requisitionId, UserDTO userDTO) throws Exception {
        Pi_unique_item_transactionDTO transactionDTO = new Pi_unique_item_transactionDTO();
        transactionDTO.transactionDate = System.currentTimeMillis();
        transactionDTO.transactionType = Pi_item_transaction_typeEnum.PRODUCT_RETURNED_TO_STORE_FROM_EMPLOYEE.getValue();
        transactionDTO.itemId = itemId;
        transactionDTO.piRequisitionId = requisitionId;
        Procurement_goodsDTO item = Procurement_goodsRepository.getInstance()
                .getProcurement_goodsDTOByID(transactionDTO.itemId);
        if (item != null) {
            transactionDTO.itemNameEn = item.nameEn;
            transactionDTO.itemNameBn = item.nameBn;
            transactionDTO.isItemReturnable = item.returnable;
        }

        transactionDTO.quantity = 1;
        transactionDTO.employeeRecordId = userDTO.employee_record_id;
        transactionDTO.employeeNameEn = Employee_recordsRepository.getInstance()
                .getEmployeeName(transactionDTO.employeeRecordId, "English");
        transactionDTO.employeeNameBn = Employee_recordsRepository.getInstance()
                .getEmployeeName(transactionDTO.employeeRecordId, "Bangla");
        transactionDTO.itemStockCountAfterTransaction = PiUniqueItemAssignmentDAO.getInstance()
                .getStockCountForAllStoreByItemId(transactionDTO.itemId);
                //+ transactionDTO.quantity; // no need. Because stock (pi_uniq_item_assignment table) updates before executing this function

        transactionDTO.insertedBy = transactionDTO.modifiedBy = userDTO.employee_record_id;
        transactionDTO.insertionTime = transactionDTO.lastModificationTime = System.currentTimeMillis();

        Pi_unique_item_transactionDAO.getInstance().add(transactionDTO);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {

                case "search":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_RETURN_SEARCH)) {

                        request.getRequestDispatcher("pi_return/pi_returnNewSearch.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "movableProduct":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.OFFICE_AND_EMPLOYEE_MANAGEMENT)) {

                        request.getRequestDispatcher("pi_return/pi_returnRemovalProductEdit.jsp").forward(request, response);
                        return;
                    }
                    break;

                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getServletName() + "?actionType=search";
    }

    public String getAfterMovableURL(HttpServletRequest request) {
        return getServletName() + "?actionType=movableProduct";
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        init(request);
        try {
            switch (request.getParameter("actionType")) {

                case "ajax_add":
                    if (Utils.checkPermission(commonLoginData.userDTO, getAddPageMenuConstants()) && getAddPermission(request)) {
                        try {
                            CommonDTO commonDTO = addT(request, true, commonLoginData.userDTO);
                            finalize(request);
                            ApiResponse.sendSuccessResponse(response, getAjaxAddRedirectURL(request, commonDTO));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    } else {
                        ApiResponse.sendErrorResponse(response, commonLoginData.isLangEng ? "You have no permission to do this task" : "আপনার এই ক্রিয়া সম্পাদনের অনুমতি নেই");
                        return;
                    }

                case "movableProductSubmit":
                    if (Utils.checkPermission(String.valueOf(commonLoginData.userDTO.employee_record_id), commonLoginData.userDTO, MenuConstants.OFFICE_AND_EMPLOYEE_MANAGEMENT)) {
                        try {
                            updateRemovalProduct(request, false, commonLoginData.userDTO);
                            finalize(request);
                            ApiResponse.sendSuccessResponse(response, getAfterMovableURL(request));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    } else {
                        ApiResponse.sendErrorResponse(response, commonLoginData.isLangEng ? "You have no permission to do this task" : "আপনার এই ক্রিয়া সম্পাদনের অনুমতি নেই");
                        return;
                    }

                default:
                    super.doPost(request, response);
                    return;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }
}

