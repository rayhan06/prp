package pi_return;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class PiReturnItemDAO  implements CommonDAOService<PiReturnItemDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private PiReturnItemDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"requester_org_id",
			"requester_office_id",
			"requester_office_unit_id",
			"requester_emp_id",
			"requester_phone_num",
			"requester_name_en",
			"requester_name_bn",
			"requester_office_name_en",
			"requester_office_name_bn",
			"requester_office_unit_name_en",
			"requester_office_unit_name_bn",
			"requester_office_unit_org_name_en",
			"requester_office_unit_org_name_bn",
			"purchase_date",
			"return_date",
			"package_id",
			"goods_type_id",
			"pi_return_id",
			"item_id",
			"return_amount",
			"requisition_amount",
			"remaining_amount",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("requester_phone_num"," and (requester_phone_num like ?)");
		searchMap.put("requester_name_en"," and (requester_name_en like ?)");
		searchMap.put("requester_name_bn"," and (requester_name_bn like ?)");
		searchMap.put("requester_office_name_en"," and (requester_office_name_en like ?)");
		searchMap.put("requester_office_name_bn"," and (requester_office_name_bn like ?)");
		searchMap.put("requester_office_unit_name_en"," and (requester_office_unit_name_en like ?)");
		searchMap.put("requester_office_unit_name_bn"," and (requester_office_unit_name_bn like ?)");
		searchMap.put("requester_office_unit_org_name_en"," and (requester_office_unit_org_name_en like ?)");
		searchMap.put("requester_office_unit_org_name_bn"," and (requester_office_unit_org_name_bn like ?)");
		searchMap.put("purchase_date_start"," and (purchase_date >= ?)");
		searchMap.put("purchase_date_end"," and (purchase_date <= ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final PiReturnItemDAO INSTANCE = new PiReturnItemDAO();
	}

	public static PiReturnItemDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(PiReturnItemDTO pireturnitemDTO)
	{
		pireturnitemDTO.searchColumn = "";
		pireturnitemDTO.searchColumn += pireturnitemDTO.requesterPhoneNum + " ";
		pireturnitemDTO.searchColumn += pireturnitemDTO.requesterNameEn + " ";
		pireturnitemDTO.searchColumn += pireturnitemDTO.requesterNameBn + " ";
		pireturnitemDTO.searchColumn += pireturnitemDTO.requesterOfficeNameEn + " ";
		pireturnitemDTO.searchColumn += pireturnitemDTO.requesterOfficeNameBn + " ";
		pireturnitemDTO.searchColumn += pireturnitemDTO.requesterOfficeUnitNameEn + " ";
		pireturnitemDTO.searchColumn += pireturnitemDTO.requesterOfficeUnitNameBn + " ";
		pireturnitemDTO.searchColumn += pireturnitemDTO.requesterOfficeUnitOrgNameEn + " ";
		pireturnitemDTO.searchColumn += pireturnitemDTO.requesterOfficeUnitOrgNameBn + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, PiReturnItemDTO pireturnitemDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(pireturnitemDTO);
		if(isInsert)
		{
			ps.setObject(++index,pireturnitemDTO.iD);
		}
		ps.setObject(++index,pireturnitemDTO.requesterOrgId);
		ps.setObject(++index,pireturnitemDTO.requesterOfficeId);
		ps.setObject(++index,pireturnitemDTO.requesterOfficeUnitId);
		ps.setObject(++index,pireturnitemDTO.requesterEmpId);
		ps.setObject(++index,pireturnitemDTO.requesterPhoneNum);
		ps.setObject(++index,pireturnitemDTO.requesterNameEn);
		ps.setObject(++index,pireturnitemDTO.requesterNameBn);
		ps.setObject(++index,pireturnitemDTO.requesterOfficeNameEn);
		ps.setObject(++index,pireturnitemDTO.requesterOfficeNameBn);
		ps.setObject(++index,pireturnitemDTO.requesterOfficeUnitNameEn);
		ps.setObject(++index,pireturnitemDTO.requesterOfficeUnitNameBn);
		ps.setObject(++index,pireturnitemDTO.requesterOfficeUnitOrgNameEn);
		ps.setObject(++index,pireturnitemDTO.requesterOfficeUnitOrgNameBn);
		ps.setObject(++index,pireturnitemDTO.purchaseDate);
		ps.setObject(++index,pireturnitemDTO.returnDate);
		ps.setObject(++index,pireturnitemDTO.packageId);
		ps.setObject(++index,pireturnitemDTO.goodsTypeId);
		ps.setObject(++index,pireturnitemDTO.piReturnId);
		ps.setObject(++index,pireturnitemDTO.itemId);
		ps.setObject(++index,pireturnitemDTO.returnAmount);
		ps.setObject(++index,pireturnitemDTO.requisitionAmount);
		ps.setObject(++index,pireturnitemDTO.remainingAmount);
		ps.setObject(++index,pireturnitemDTO.searchColumn);
		ps.setObject(++index,pireturnitemDTO.insertedByUserId);
		ps.setObject(++index,pireturnitemDTO.insertedByOrganogramId);
		ps.setObject(++index,pireturnitemDTO.insertionDate);
		ps.setObject(++index,pireturnitemDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(++index,pireturnitemDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,pireturnitemDTO.iD);
		}
	}
	
	@Override
	public PiReturnItemDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			PiReturnItemDTO pireturnitemDTO = new PiReturnItemDTO();
			int i = 0;
			pireturnitemDTO.iD = rs.getLong(columnNames[i++]);
			pireturnitemDTO.requesterOrgId = rs.getLong(columnNames[i++]);
			pireturnitemDTO.requesterOfficeId = rs.getLong(columnNames[i++]);
			pireturnitemDTO.requesterOfficeUnitId = rs.getLong(columnNames[i++]);
			pireturnitemDTO.requesterEmpId = rs.getLong(columnNames[i++]);
			pireturnitemDTO.requesterPhoneNum = rs.getString(columnNames[i++]);
			pireturnitemDTO.requesterNameEn = rs.getString(columnNames[i++]);
			pireturnitemDTO.requesterNameBn = rs.getString(columnNames[i++]);
			pireturnitemDTO.requesterOfficeNameEn = rs.getString(columnNames[i++]);
			pireturnitemDTO.requesterOfficeNameBn = rs.getString(columnNames[i++]);
			pireturnitemDTO.requesterOfficeUnitNameEn = rs.getString(columnNames[i++]);
			pireturnitemDTO.requesterOfficeUnitNameBn = rs.getString(columnNames[i++]);
			pireturnitemDTO.requesterOfficeUnitOrgNameEn = rs.getString(columnNames[i++]);
			pireturnitemDTO.requesterOfficeUnitOrgNameBn = rs.getString(columnNames[i++]);
			pireturnitemDTO.purchaseDate = rs.getLong(columnNames[i++]);
			pireturnitemDTO.returnDate = rs.getLong(columnNames[i++]);
			pireturnitemDTO.packageId = rs.getLong(columnNames[i++]);
			pireturnitemDTO.goodsTypeId = rs.getLong(columnNames[i++]);
			pireturnitemDTO.piReturnId = rs.getLong(columnNames[i++]);
			pireturnitemDTO.itemId = rs.getLong(columnNames[i++]);
			pireturnitemDTO.returnAmount = rs.getDouble(columnNames[i++]);
			pireturnitemDTO.requisitionAmount = rs.getDouble(columnNames[i++]);
			pireturnitemDTO.remainingAmount = rs.getDouble(columnNames[i++]);
			pireturnitemDTO.searchColumn = rs.getString(columnNames[i++]);
			pireturnitemDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			pireturnitemDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			pireturnitemDTO.insertionDate = rs.getLong(columnNames[i++]);
			pireturnitemDTO.lastModifierUser = rs.getString(columnNames[i++]);
			pireturnitemDTO.isDeleted = rs.getInt(columnNames[i++]);
			pireturnitemDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return pireturnitemDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public PiReturnItemDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "pi_return_item";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((PiReturnItemDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((PiReturnItemDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
				
}
	