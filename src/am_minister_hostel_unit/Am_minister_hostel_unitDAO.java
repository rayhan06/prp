package am_minister_hostel_unit;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Am_minister_hostel_unitDAO  implements CommonDAOService<Am_minister_hostel_unitDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Am_minister_hostel_unitDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"am_minister_hostel_block_id",
			"am_minister_hostel_side_id",
			"unit_number",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("am_minister_hostel_block_id"," and (am_minister_hostel_block_id = ?)");
		searchMap.put("unit_number"," and (unit_number like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Am_minister_hostel_unitDAO INSTANCE = new Am_minister_hostel_unitDAO();
	}

	public static Am_minister_hostel_unitDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Am_minister_hostel_unitDTO am_minister_hostel_unitDTO)
	{
		am_minister_hostel_unitDTO.searchColumn = "";
		am_minister_hostel_unitDTO.searchColumn += CommonDAO.getName(am_minister_hostel_unitDTO.amMinisterHostelBlockId, "am_minister_hostel_block", "block_no", "id") ;
		am_minister_hostel_unitDTO.searchColumn += am_minister_hostel_unitDTO.unitNumber + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Am_minister_hostel_unitDTO am_minister_hostel_unitDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(am_minister_hostel_unitDTO);
		if(isInsert)
		{
			ps.setObject(++index,am_minister_hostel_unitDTO.iD);
		}
		ps.setObject(++index,am_minister_hostel_unitDTO.amMinisterHostelBlockId);
		ps.setObject(++index,am_minister_hostel_unitDTO.amMinisterHostelSideId);
		ps.setObject(++index,am_minister_hostel_unitDTO.unitNumber);
		ps.setObject(++index,am_minister_hostel_unitDTO.searchColumn);
		ps.setObject(++index,am_minister_hostel_unitDTO.insertedByUserId);
		ps.setObject(++index,am_minister_hostel_unitDTO.insertedByOrganogramId);
		ps.setObject(++index,am_minister_hostel_unitDTO.insertionDate);
		ps.setObject(++index,am_minister_hostel_unitDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(++index,am_minister_hostel_unitDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,am_minister_hostel_unitDTO.iD);
		}
	}
	
	@Override
	public Am_minister_hostel_unitDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Am_minister_hostel_unitDTO am_minister_hostel_unitDTO = new Am_minister_hostel_unitDTO();
			int i = 0;
			am_minister_hostel_unitDTO.iD = rs.getLong(columnNames[i++]);
			am_minister_hostel_unitDTO.amMinisterHostelBlockId = rs.getLong(columnNames[i++]);
			am_minister_hostel_unitDTO.amMinisterHostelSideId = rs.getLong(columnNames[i++]);
			am_minister_hostel_unitDTO.unitNumber = rs.getString(columnNames[i++]);
			am_minister_hostel_unitDTO.searchColumn = rs.getString(columnNames[i++]);
			am_minister_hostel_unitDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			am_minister_hostel_unitDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			am_minister_hostel_unitDTO.insertionDate = rs.getLong(columnNames[i++]);
			am_minister_hostel_unitDTO.lastModifierUser = rs.getString(columnNames[i++]);
			am_minister_hostel_unitDTO.isDeleted = rs.getInt(columnNames[i++]);
			am_minister_hostel_unitDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return am_minister_hostel_unitDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Am_minister_hostel_unitDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "am_minister_hostel_unit";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_minister_hostel_unitDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_minister_hostel_unitDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
				
}
	