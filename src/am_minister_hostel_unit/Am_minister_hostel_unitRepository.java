package am_minister_hostel_unit;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import am_minister_hostel_unit.Am_minister_hostel_unitDTO;
import com.google.gson.Gson;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Am_minister_hostel_unitRepository implements Repository {
	Am_minister_hostel_unitDAO am_minister_hostel_unitDAO = null;
	Gson gson = null;

	static Logger logger = Logger.getLogger(Am_minister_hostel_unitRepository.class);
	Map<Long, Am_minister_hostel_unitDTO>mapOfAm_minister_hostel_unitDTOToiD;
	Map<Long, List<Am_minister_hostel_unitDTO>>mapOfAm_minister_hostel_unitDTOToSideId;


	private Am_minister_hostel_unitRepository(){
		gson = new Gson();
		mapOfAm_minister_hostel_unitDTOToiD = new ConcurrentHashMap<>();
		mapOfAm_minister_hostel_unitDTOToSideId = new ConcurrentHashMap<>();
		am_minister_hostel_unitDAO = Am_minister_hostel_unitDAO.getInstance();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Am_minister_hostel_unitRepository INSTANCE = new Am_minister_hostel_unitRepository();
    }

    public static Am_minister_hostel_unitRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Am_minister_hostel_unitDTO> am_minister_hostel_unitDTOs = am_minister_hostel_unitDAO.getAllDTOs(reloadAll);
			for(Am_minister_hostel_unitDTO am_minister_hostel_unitDTO : am_minister_hostel_unitDTOs) {
				Am_minister_hostel_unitDTO oldAm_minister_hostel_unitDTO = getAm_minister_hostel_unitDTOByIDWithoutClone(am_minister_hostel_unitDTO.iD);
				if( oldAm_minister_hostel_unitDTO != null ) {
					mapOfAm_minister_hostel_unitDTOToiD.remove(oldAm_minister_hostel_unitDTO.iD);

					if (mapOfAm_minister_hostel_unitDTOToSideId.get(oldAm_minister_hostel_unitDTO.amMinisterHostelSideId) != null) {
						mapOfAm_minister_hostel_unitDTOToSideId.get(oldAm_minister_hostel_unitDTO.amMinisterHostelSideId).remove(oldAm_minister_hostel_unitDTO);
					}

				}
				if(am_minister_hostel_unitDTO.isDeleted == 0) 
				{
					
					mapOfAm_minister_hostel_unitDTOToiD.put(am_minister_hostel_unitDTO.iD, am_minister_hostel_unitDTO);

					List<Am_minister_hostel_unitDTO> minister_hostel_blockDTOS = mapOfAm_minister_hostel_unitDTOToSideId.getOrDefault(am_minister_hostel_unitDTO.amMinisterHostelSideId, new ArrayList<>());
					minister_hostel_blockDTOS.add(am_minister_hostel_unitDTO);
					mapOfAm_minister_hostel_unitDTOToSideId.put(am_minister_hostel_unitDTO.amMinisterHostelSideId, minister_hostel_blockDTOS);

				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Am_minister_hostel_unitDTO> getAm_minister_hostel_unitList() {
		List <Am_minister_hostel_unitDTO> am_minister_hostel_units = new ArrayList<Am_minister_hostel_unitDTO>(this.mapOfAm_minister_hostel_unitDTOToiD.values());
		return am_minister_hostel_units;
	}

	public Am_minister_hostel_unitDTO clone(Am_minister_hostel_unitDTO dto) {
		if (dto == null)return null;
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Am_minister_hostel_unitDTO.class);
	}

	public List<Am_minister_hostel_unitDTO> clone(List<Am_minister_hostel_unitDTO> dtoList) {
		if (dtoList == null)return new ArrayList<>();
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public Am_minister_hostel_unitDTO getAm_minister_hostel_unitDTOByIDWithoutClone( long ID){
		return mapOfAm_minister_hostel_unitDTOToiD.get(ID);
	}

	public Am_minister_hostel_unitDTO getAm_minister_hostel_unitDTOByID( long ID){
		return clone(mapOfAm_minister_hostel_unitDTOToiD.get(ID));
	}

	public List<Am_minister_hostel_unitDTO> getAm_minister_hostel_unitDTOBySideID( long ID){
		return clone(mapOfAm_minister_hostel_unitDTOToSideId.get(ID));
	}

	@Override
	public String getDtoJsonById(long ID) {
		return gson.toJson(getAm_minister_hostel_unitDTOByID(ID));
	}
	
	@Override
	public String getTableName() {
		return "am_minister_hostel_unit";
	}
}


