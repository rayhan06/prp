package am_minister_hostel_unit;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import am_minister_hostel_block.AmMinisterHostelSideDAO;
import files.FilesDTO;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;

import role.RoleDTO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;
import javax.servlet.http.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


import language.LC;
import language.LM;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;


/**
 * Servlet implementation class Am_minister_hostel_unitServlet
 */
@WebServlet("/Am_minister_hostel_unitServlet")
@MultipartConfig
public class Am_minister_hostel_unitServlet extends BaseServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Am_minister_hostel_unitServlet.class);

    @Override
    public String getTableName() {
        return Am_minister_hostel_unitDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Am_minister_hostel_unitServlet";
    }

    @Override
    public Am_minister_hostel_unitDAO getCommonDAOService() {
        return Am_minister_hostel_unitDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.AM_MINISTER_HOSTEL_UNIT_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.AM_MINISTER_HOSTEL_UNIT_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.AM_MINISTER_HOSTEL_UNIT_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Am_minister_hostel_unitServlet.class;
    }
    private final Gson gson = new Gson();


    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
		init(request);
		try {
			switch (request.getParameter("actionType")) {
				case "getAddPage":
					if (Utils.checkPermission(commonLoginData.userDTO,getAddPageMenuConstants()) && getAddPagePermission(request)) {
						finalize(request);
						request.getRequestDispatcher(commonPartOfDispatchURL()+"Edit.jsp").forward(request,response);
						return;
					}
					break;
				case "getEditPage":
					if (Utils.checkPermission(commonLoginData.userDTO, getEditPageMenuConstants()) && getEditPagePermission(request)) {
						setDTOForJsp(request);
						finalize(request);
						request.getRequestDispatcher(commonPartOfDispatchURL()+"Edit.jsp?actionType=edit").forward(request,response);
						return;
					}
					break;
				case "getUnitOptionsBySideId":
					getUnitOptionsBySideId(request, response);
					return;
				case "downloadDropzoneFile": {
					long id = Long.parseLong(request.getParameter("id"));
					FilesDTO filesDTO = (FilesDTO) filesDAO.getDTOByID(id);
					Utils.ProcessFile(request, response, filesDTO.fileTitle, filesDTO.inputStream);
					return;
				}
				case "DeleteFileFromDropZone": {
					long id = Long.parseLong(request.getParameter("id"));
					logger.debug("In delete file");
					filesDAO.hardDeleteByID(id);
					response.getWriter().write("Deleted");
					return;
				}
				case "getURL":
					String URL = request.getParameter("URL");
					response.sendRedirect(URL);
					return;
				case "search":
					if (Utils.checkPermission(commonLoginData.userDTO, getSearchMenuConstants()) && getSearchPagePermission(request)) {
						UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
						RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
//						boolean isAdmin = role.ID == SessionConstants.VEHICLE_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;
						boolean isAdmin = role.ID == SessionConstants.ADMIN_ROLE || role.ID == SessionConstants.ASSET_ADMIN_ROLE;
						if (!isAdmin) {
							return;
						}
						search(request, response);
						return;
					}
					break;
				case "view":
					logger.debug("view requested");
					if (Utils.checkPermission(commonLoginData.userDTO, getViewMenuConstants()) && getViewPagePermission(request)) {
						setDTOForJsp(request);
						finalize(request);
						request.getRequestDispatcher(commonPartOfDispatchURL() + "View.jsp?ID=" + getId(request)).forward(request,response);
						return;
					}
					break;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
		}
		request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
	}


	@Override
	public boolean getEditPermission(HttpServletRequest request) {
		return checkEditPermission(request);
	}

	@Override
	public boolean getEditPagePermission(HttpServletRequest request) {
		return checkEditPagePermission(request);
	}

	@Override
	public boolean getAddPermission(HttpServletRequest request) {
		return checkEditPermission(request);
	}

	@Override
	public boolean getAddPagePermission(HttpServletRequest request) {
		return checkEditPermission(request);
	}

	@Override
	public boolean getViewPagePermission(HttpServletRequest request) {
		return checkEditPagePermission(request);
	}

	private boolean checkEditPagePermission(HttpServletRequest request) {
		UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
		RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
		boolean isAdmin = role.ID == SessionConstants.ADMIN_ROLE || role.ID == SessionConstants.ASSET_ADMIN_ROLE;
		return isAdmin;
//		if (isAdmin) {
//			return true;
//		}
//		Am_minister_hostel_unitDTO am_minister_hostel_unitDTO = getCommonDAOService().getDTOFromID(Long.parseLong(request.getParameter("ID")));
//		if (am_minister_hostel_unitDTO == null) {
//			return false;
//		}
//		return am_minister_hostel_unitDTO.insertedByOrganogramId == userDTO.organogramID;
	}

	private boolean checkEditPermission(HttpServletRequest request) {
		UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
		RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
		boolean isAdmin = role.ID == SessionConstants.ADMIN_ROLE || role.ID == SessionConstants.ASSET_ADMIN_ROLE;
		return isAdmin;
//		if (isAdmin) {
//			return true;
//		}
//		Am_minister_hostel_unitDTO am_minister_hostel_unitDTO = getCommonDAOService().getDTOFromID(Long.parseLong(request.getParameter("iD")));
//		if (am_minister_hostel_unitDTO == null) {
//			return false;
//		}
//		return am_minister_hostel_unitDTO.insertedByOrganogramId == userDTO.organogramID;
	}
	

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception
	{
		// TODO Auto-generated method stub

			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addAm_minister_hostel_unit");
			String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
			Am_minister_hostel_unitDTO am_minister_hostel_unitDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				am_minister_hostel_unitDTO = new Am_minister_hostel_unitDTO();
			}
			else
			{
				am_minister_hostel_unitDTO = Am_minister_hostel_unitDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));

				List<Long> childExists = null;
				childExists = AmMinisterHostelSideDAO.getInstance().getDistinctColumnIds("am_minister_hostel_unit","am_minister_hostel_level", "am_minister_hostel_unit_id", Arrays.asList(am_minister_hostel_unitDTO.iD));
				if (!childExists.isEmpty()) {
					String errMsg = language.equals("English") ?
							"This cannot be edited, because it has at least one child" :
							"দুঃখিত, এটি পরিবর্তন করা যাবে না, কারণ এর অন্তত একটি চাইল্ড বিদ্যমান";
					throw new Exception(errMsg);
				}
			}

			String Value = "";

			Value = request.getParameter("amMinisterHostelBlockId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("amMinisterHostelBlockId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_minister_hostel_unitDTO.amMinisterHostelBlockId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("amMinisterHostelSideId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("amMinisterHostelSideId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_minister_hostel_unitDTO.amMinisterHostelSideId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("unitNumber");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("unitNumber = " + Value);
			if(Value != null)
			{
				am_minister_hostel_unitDTO.unitNumber = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				am_minister_hostel_unitDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				am_minister_hostel_unitDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				am_minister_hostel_unitDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				am_minister_hostel_unitDTO.insertionDate = TimeConverter.getToday();
			}


			am_minister_hostel_unitDTO.lastModifierUser = userDTO.userName;


			System.out.println("Done adding  addAm_minister_hostel_unit dto = " + am_minister_hostel_unitDTO);


		int existingCount = Am_minister_hostel_unitDAO.getInstance().getCount(null, "",
				" and am_minister_hostel_side_id = " + am_minister_hostel_unitDTO.amMinisterHostelSideId +
						" and unit_number = '" + am_minister_hostel_unitDTO.unitNumber + "' " +
						" and iD != " + am_minister_hostel_unitDTO.iD);
		if (existingCount > 0) {
			String errMSg = language.equals("English") ?
					"Sorry, another unit was created with this name under same side" :
					"দুঃখিত, এই নামে একই পাশে আরেকটি ইউনিট বিদ্যমান";
			throw new Exception(errMSg);
		}


		Utils.handleTransaction(()->{
			if(addFlag == true)
			{
				Am_minister_hostel_unitDAO.getInstance().add(am_minister_hostel_unitDTO);
			}
			else
			{
				Am_minister_hostel_unitDAO.getInstance().update(am_minister_hostel_unitDTO);
			}
		});


			return am_minister_hostel_unitDTO;

	}


	@Override
	public void deleteT(HttpServletRequest request, UserDTO userDTO) {
		String[] IDsToDelete = request.getParameterValues("ID");
		if(IDsToDelete.length>0){
			List<Long> ids = Stream.of(IDsToDelete)
					.map(Long::parseLong)
					.collect(Collectors.toList());

			List<Long> childExists = null;
			try {
				childExists = AmMinisterHostelSideDAO.getInstance().getDistinctColumnIds("am_minister_hostel_unit","am_minister_hostel_level", "am_minister_hostel_unit_id", ids);
			} catch (Exception e) {
				e.printStackTrace();
			}
			childExists
					.forEach(childExistsId -> ids.remove(childExistsId));

			if (!ids.isEmpty()) {
				try {
					Utils.handleTransaction(()->{
						getCommonDAOService().deletePb(userDTO.employee_record_id, ids);
					});
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}



	private void getUnitOptionsBySideId(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getUnitOptionsBySideId");
			LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
			String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);
			List<Am_minister_hostel_unitDTO> amMinisterHostelUnitDTOS = Am_minister_hostel_unitRepository.getInstance().getAm_minister_hostel_unitDTOBySideID(Long.parseLong(request.getParameter("sideId")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			long id = -1;
			String blockIdString = request.getParameter("ID");
			if (StringUtils.isValidString(blockIdString)) id = Long.parseLong(blockIdString);

			String tempString = "";
			String selected = "";
			tempString += Utils.buildSelectOption(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language.equalsIgnoreCase("english"));

			for (Am_minister_hostel_unitDTO am_minister_hostel_unitDTO : amMinisterHostelUnitDTOS) {
				selected = "";
				if (am_minister_hostel_unitDTO.iD == id) selected = "selected";
				tempString += "<option " + selected + " value='" + am_minister_hostel_unitDTO.iD + "'>" + (am_minister_hostel_unitDTO.unitNumber) + "</option>";
			}

			String encoded = this.gson.toJson(tempString);
//			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

