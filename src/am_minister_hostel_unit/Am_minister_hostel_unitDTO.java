package am_minister_hostel_unit;
import java.util.*; 
import util.*; 


public class Am_minister_hostel_unitDTO extends CommonDTO
{

	public long amMinisterHostelBlockId = -1;
	public long amMinisterHostelSideId = -1;
    public String unitNumber = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	
	
    @Override
	public String toString() {
            return "$Am_minister_hostel_unitDTO[" +
            " iD = " + iD +
            " amMinisterHostelBlockId = " + amMinisterHostelBlockId +
            " amMinisterHostelSideId = " + amMinisterHostelSideId +
            " unitNumber = " + unitNumber +
            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}