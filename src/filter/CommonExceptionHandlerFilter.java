package filter;

import com.google.gson.Gson;
import common.ApiResponse;
import common.ExceptionDTO;
import common.ExceptionLogDAO;
import common.RequestFailureException;
import org.apache.log4j.Logger;
import util.CurrentTimeFactory;
import util.HttpRequestUtils;
import util.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.Enumeration;

@SuppressWarnings("unused")
public class CommonExceptionHandlerFilter implements Filter {

    private static final Logger logger = Logger.getLogger(CommonExceptionHandlerFilter.class);

    private static final String errorPageURI = "/error.html";

    private final ExceptionLogDAO exceptionLogDAO = new ExceptionLogDAO();

    @Override
    public void init(FilterConfig paramFilterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        if(HttpRequestUtils.IGNORE_FILTER_THREAD_LOCAL.get() || HttpRequestUtils.BACK_END_AJAX_CALL_THREAD_LOCAL.get()){
            chain.doFilter(request, response);
        }else{
            CurrentTimeFactory.initializeCurrentTimeFactory();
            try {
                if (isGetRequest(request)) {
                    String requestedURLWithQueryParameter = getRequestedURLWithQueryParameter(request);
                    request.setAttribute("requestedURL", URLEncoder.encode(requestedURLWithQueryParameter, "UTF-8"));
                }
                chain.doFilter(request, response);
            } catch (Exception ex) {

                ex.printStackTrace();

                if (ex instanceof RequestFailureException) {
                    RequestFailureException requestFailureException = (RequestFailureException) ex;
                    if (requestFailureException.getException() != null) {
                        exceptionLogDAO.insertException(new ExceptionDTO(requestFailureException.getException()));
                    }
                } else {
                    exceptionLogDAO.insertException(new ExceptionDTO(ex));
                }

                String referer = ((HttpServletRequest) request).getHeader("referer");

                while (ex.getCause() != null && ex.getCause() instanceof Exception
                        && !(ex instanceof RequestFailureException)) {
                    ex = (Exception) ex.getCause();
                }


                if (isAjaxRequest((HttpServletRequest) request)) {
                    ApiResponse apiResponse = ApiResponse.makeErrorResponse(ex.getMessage());
                    PrintWriter writer = response.getWriter();
                    writer.write(new Gson().toJson(apiResponse));
                    writer.flush();
                    writer.close();
                } else {
                    String failureMsg = (ex instanceof RequestFailureException) ? ex.getMessage() : "Server Error";
                    String forwardingLink = (!StringUtils.isBlank(referer)) ? referer : errorPageURI;
                    handleExceptionForwardToErrorPage(request, response, failureMsg, forwardingLink);
                }
            }
            CurrentTimeFactory.destroyCurrentTimeFactory();
        }
    }


    private void handleExceptionForwardToSamePage(ServletRequest request, ServletResponse response, String failureMsg)
            throws ServletException, IOException {
        String requestedURI = request.getParameter("requestedURL");
        logger.debug("requestedURI " + requestedURI);
        request.setAttribute("failureMsg", failureMsg);
        String applicationContex = ((HttpServletRequest) request).getContextPath();
        requestedURI = requestedURI.replace(applicationContex, "");
        request.getRequestDispatcher(requestedURI).forward(request, response);

    }

    private void handleExceptionForwardToErrorPage(ServletRequest request, ServletResponse response, String failureMsg,
                                                   String errorPageURI) throws IOException {
        ((HttpServletRequest) request).getSession().setAttribute("failureMsg", failureMsg);
        ((HttpServletResponse) response).sendRedirect(errorPageURI);
    }

    private String getRequestedURLWithQueryParameter(ServletRequest request) {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        StringBuilder requestedURLWithQueryParameter = new StringBuilder(httpServletRequest.getRequestURI());

        boolean isParameterAdded = false;
        Enumeration<String> names = request.getParameterNames();
        while (names.hasMoreElements()) {
            if (isParameterAdded) {
                requestedURLWithQueryParameter.append("&");
            } else {
                requestedURLWithQueryParameter.append("?");
            }
            String name = names.nextElement();
            String value = request.getParameter(name);
            if (!StringUtils.isBlank(value)) {
                requestedURLWithQueryParameter.append(name).append("=").append(value);
            }
            isParameterAdded = true;
        }
        return requestedURLWithQueryParameter.toString();

    }

    @Override
    public void destroy() {
    }

    private void parseQueryAndInsertIntoRequest(String url, ServletRequest request) {
        if (url.contains("?")) {
            String queryString = url.substring(url.indexOf('?') + 1);
            String[] requestParams = queryString.split("&");
            for (String requestParam : requestParams) {
                String[] keyValue = requestParam.split("=");
                request.setAttribute(keyValue[0], keyValue[1]);
            }
        }
    }

    private boolean isGetRequest(ServletRequest request) {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        return httpServletRequest.getMethod().equalsIgnoreCase("GET");
    }

    private boolean isAjaxRequest(HttpServletRequest request) {
        return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
    }
}