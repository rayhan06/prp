package filter;


import common.ApiResponse;
import config.GlobalConfigConstants;
import config.GlobalConfigurationRepository;
import forgetPassword.VerificationConstants;
import language.LM;
import login.IgnoreLoginDAO;
import login.LoginDTO;
import login.RememberMeOptionDAO;
import oisf.OisfDAO;
import org.apache.log4j.Logger;
import permission.MenuDTO;
import permission.MenuRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static util.HttpRequestUtils.loginIgnoreExtList;

@SuppressWarnings({"unused"})
public class LoginFilter implements Filter {
    private static final Logger logger = Logger.getLogger(LoginFilter.class);
    private static final int REDIRECT_TO_ERROR_PAGE_URL = 5;
    private static final int REDIRECT_TO_BASE_URL = 4;
    private static final int REDIRECT_TO_HOME = 3;
    private static final int REDIRECT_TO_LOGIN = 2;
    private static final int CONTINUE_CHAIN = 1;
    String contextPath = "";
    String loginURI = "";

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        httpServletResponse.addHeader("X-Frame-Options", "SAMEORIGIN");
        if(HttpRequestUtils.IGNORE_FILTER_THREAD_LOCAL.get()){
            chain.doFilter(request,response);
        }else if(HttpRequestUtils.BACK_END_AJAX_CALL_THREAD_LOCAL.get() && isLoggedIn(httpServletRequest)){
            setCommonLoginData((HttpServletRequest) request);
            chain.doFilter(request,response);
        } else{
            String url = httpServletRequest.getScheme()
                    + "://"
                    + httpServletRequest.getServerName();
            if (httpServletRequest.getServerName().equalsIgnoreCase("localhost")) {
                url += ":" + httpServletRequest.getServerPort();
            }

            url += httpServletRequest.getRequestURI();


            NavigationService4.context = url;
            contextPath = httpServletRequest.getSession().getServletContext().getContextPath();
            loginURI = contextPath + "/";

            int result = 0;
            String URI = httpServletRequest.getRequestURI();
            if (URI.matches(".*\\.jsp.*") && !URI.endsWith("/home/otpVerifier.jsp") && !URI.endsWith("/forgotPassword/forgotPassword.jsp") && !URI.endsWith("/user_details/user_detailsEdit.jsp")) {
                httpServletResponse.sendRedirect(contextPath);
                return;
            }
            if (skipLoginCheck(httpServletRequest)) {
                result = CONTINUE_CHAIN;
            }else if (ignoreHit(httpServletRequest)) {
                result = CONTINUE_CHAIN;
            }else if (isLoggedIn(httpServletRequest)) {
                logger.debug("result log " + result);
                result = handleLoggedIn(httpServletRequest, httpServletResponse, chain);
                if(result == CONTINUE_CHAIN){
                    if(!setCommonLoginData(httpServletRequest)){
                        result = REDIRECT_TO_ERROR_PAGE_URL;
                    }
                }
            } else {
                logger.debug("result not log " + result);
                result = handleNotLoggedIn(httpServletRequest, httpServletResponse);
            }
            logger.debug("result " + result);
            processResult(result, chain, httpServletRequest, httpServletResponse);
            if(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get()!=null){
                HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.remove();
            }
        }
    }

    private boolean setCommonLoginData(HttpServletRequest httpServletRequest){
        LoginDTO loginDTO = (LoginDTO) httpServletRequest.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if(userDTO!=null){
            CommonLoginData commonLoginData = new CommonLoginData();
            commonLoginData.loginDTO = loginDTO;
            commonLoginData.userDTO = userDTO;
            commonLoginData.language = LM.getLanguage(userDTO);
            commonLoginData.isLangEng = "English".equalsIgnoreCase(commonLoginData.language);
            commonLoginData.ipAddress = httpServletRequest.getRemoteAddr();
            HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.set(commonLoginData);
            return true;
        }
        return false;
    }

    private void processResult(int result, FilterChain chain, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, ServletException {

        switch (result) {
            case REDIRECT_TO_BASE_URL: {
                httpServletResponse.sendRedirect("");
                break;
            }
            case CONTINUE_CHAIN: {
                chain.doFilter(httpServletRequest, httpServletResponse);
                break;
            }
            case REDIRECT_TO_HOME: {

                LoginDTO loginDTO = (LoginDTO) httpServletRequest.getSession().getAttribute(SessionConstants.USER_LOGIN);

                if (isGetRequest(httpServletRequest)) {
                    if (Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.ENABLE_DEFAULT_LOGIN).value) == 1
                            &&
                            loginDTO != null
                            &&
                            Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.DEFAULT_USER_ID).value) == loginDTO.userID
                    ) {


                        int menuID = Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.DEFAULT_LANDING_MENU).value);
                        MenuDTO menuDTO = MenuRepository.getInstance().getMenuDTOByMenuID(menuID);
                        if (menuDTO == null) {
                            throw new RuntimeException("No default landing menu found with menuID " + menuID);
                        }
                        httpServletResponse.sendRedirect(contextPath + "/" + menuDTO.hyperLink);

                    } else {
                    	System.out.println("#############################Getting index from loginfilter##############################");
                        httpServletRequest.getRequestDispatcher("/home/index.jsp").forward(httpServletRequest, httpServletResponse);
                    }
                } else {
                    if(HttpRequestUtils.BACK_END_AJAX_CALL_THREAD_LOCAL.get()) {
                        ApiResponse.sendSuccessResponse(httpServletResponse, "");
                    } else {
                        httpServletResponse.sendRedirect("");
                    }
                }
                break;
            }
            case REDIRECT_TO_LOGIN: {
                if (isGetRequest(httpServletRequest)) {

                    if (Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_LOGIN_FILTER).value) == 1) {
                        String loginIP = httpServletRequest.getRemoteAddr();
                        httpServletRequest.getSession(true).setAttribute(VerificationConstants.LOGIN_IP, loginIP);
                        //4 for login filter
                        OisfDAO tempDao = new OisfDAO();

                        String tempRedirect = tempDao.processLoginPage(httpServletRequest, "");
                        httpServletResponse.sendRedirect(tempRedirect);
                        return;

                    } else {
                        setNonce(httpServletRequest);
                        if(httpServletRequest.getServerName().equalsIgnoreCase("localhost")){
                            httpServletResponse.sendRedirect(httpServletRequest.getContextPath());
                        }else{
                            String url = httpServletRequest.getServerName()+"/"+httpServletRequest.getContextPath();
                            url = url.replace("/{2,}","/");
                            httpServletResponse.sendRedirect("https://"+url);
                        }
                    }

                } else {
                    setNonce(httpServletRequest);
                    if(HttpRequestUtils.BACK_END_AJAX_CALL_THREAD_LOCAL.get()) {
                        ApiResponse.sendRedirectResponse(httpServletResponse, "");
                    }else {
                        httpServletResponse.sendRedirect("");
                    }
                }
                break;
            }

            case REDIRECT_TO_ERROR_PAGE_URL:
                httpServletRequest.getRequestDispatcher("common/error_page.jsp").forward(httpServletRequest, httpServletResponse);
                break;
        }
    }

    private void setNonce(HttpServletRequest httpServletRequest) {
        String tempNonce = RootstockUtils.generateRandomHexToken(10);

        httpServletRequest.setAttribute("nonce", tempNonce);

        httpServletRequest.getSession().setAttribute("nonce", tempNonce);
    }

    private boolean skipLoginCheck(HttpServletRequest httpServletRequest) {
        String requestedURI = httpServletRequest.getRequestURI();
        return IgnoreLoginDAO.getInstance()
                             .getIgnoreLoginList()
                             .stream()
                             .filter(requestedURI::matches)
                             .findAny()
                             .orElse(null)!=null;

    }

    private boolean redirectToHome(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {


        String requestedURI = httpServletRequest.getRequestURI();
        boolean redirectToHome = false;
        System.out.println("requestedURI = " + requestedURI + " loginURI = " + loginURI);
        

        if (Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.ENABLE_DEFAULT_LOGIN).value) == 1
                &&
                requestedURI.endsWith("LoginServlet")
        ) {
            return false;
        }


        if (requestedURI.equals(loginURI) || requestedURI.endsWith("LoginServlet") || requestedURI.endsWith("login.jsp")) {
            redirectToHome = true;
        } else if (!(requestedURI.endsWith("Servlet") || requestedURI.endsWith("jsp"))) {
            redirectToHome = true;
        }
        System.out.println("returning " + redirectToHome);
        return redirectToHome;
    }

    private int handleNotLoggedIn(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        setLastRequestedURI(httpServletRequest);
        String requestedURI = httpServletRequest.getRequestURI();
        if (requestedURI.equals(loginURI) || requestedURI.endsWith("LoginServlet") || requestedURI.endsWith("login.jsp")
                || requestedURI.endsWith("PublicServlet") || requestedURI.endsWith("UserServlet")
                || requestedURI.endsWith("Entry_pageServlet")) {
            return CONTINUE_CHAIN;
        }
        return REDIRECT_TO_LOGIN;
    }

    private boolean isGetRequest(HttpServletRequest httpServletRequest) {
        String methodName = httpServletRequest.getMethod();
        return methodName.equalsIgnoreCase("get");
    }

    private boolean ignoreHit(HttpServletRequest httpServletRequest) {
        String requestedURI = httpServletRequest.getRequestURI();
        boolean ignore = false;
        for (String loginIgnoreExtension : loginIgnoreExtList) {
            if (requestedURI.endsWith(loginIgnoreExtension)) {
                ignore = true;
                break;
            }
        }
        return ignore;
    }

    private int handleLoggedIn(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain chain) {
        if (!isFromOriginalIP(httpServletRequest)) {
            logger.debug("!isFromOriginalIP(httpServletRequest)");
            return REDIRECT_TO_LOGIN;
        }
        if (!isGetRequest(httpServletRequest)) {
            logger.debug("!isGetRequest(httpServletRequest)");
            return CONTINUE_CHAIN;
        }
        if (redirectToHome(httpServletRequest, httpServletResponse)) {
            logger.debug("redirectToHome(httpServletRequest, httpServletResponse)");
            return REDIRECT_TO_HOME;
        } else {
            logger.debug("CONTINUE_CHAIN");
            return CONTINUE_CHAIN;
        }
    }

    private boolean isFromOriginalIP(HttpServletRequest httpServletRequest) {
        LoginDTO loginDTO = (login.LoginDTO) httpServletRequest.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

        if (loginDTO != null) {
            if (loginDTO.isOisf == 1) {
                return true;
            } else {
                logger.debug("loginDTO " + loginDTO + " httpServletRequest.getRemoteAddr() " + httpServletRequest.getRemoteAddr());

                return httpServletRequest.getRemoteAddr().equals(loginDTO.loginSourceIP);
            }
        } else {
            return false;
        }
    }

    private String getRememberMeOptionCookie(HttpServletRequest httpServletRequest) {

        Cookie[] cookies = httpServletRequest.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(ServletConstant.REMEMBER_ME_COOKIE_NAME)) {
                    return cookie.getValue();
                }
            }
        }

        return null;
    }

    private boolean isLoggedIn(HttpServletRequest httpServletRequest) {
        LoginDTO loginDTO = (login.LoginDTO) httpServletRequest.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

        LoginDTO tempLoginDTO = null;
        if (loginDTO != null) {
            if (loginDTO.userID == 0) {
                return false;
            } else {
                logger.debug("result log user id  " + loginDTO.userID);
                return true;
            }
        }
        String cookieValue = getRememberMeOptionCookie(httpServletRequest);

        boolean isDefaultLoginEnabled = (Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.ENABLE_DEFAULT_LOGIN).value) == 1);

        Long userID = null;

        if (isDefaultLoginEnabled) {
            if (cookieValue != null) {
                tempLoginDTO = RememberMeOptionDAO.getInstance().getUserIDByCookieValue(cookieValue);
                if (tempLoginDTO == null) {
                    userID = Long.parseLong(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.DEFAULT_USER_ID).value);
                }
            } else {
                userID = Long.parseLong(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.DEFAULT_USER_ID).value);
            }
        } else {
            if (cookieValue == null) {
                return false;
            }
            tempLoginDTO = RememberMeOptionDAO.getInstance().getUserIDByCookieValue(cookieValue);
            if (tempLoginDTO == null) {
                return false;
            }
        }

        loginDTO = new LoginDTO();
        loginDTO.userID = tempLoginDTO.userID;
        loginDTO.isOisf = tempLoginDTO.isOisf;

        loginDTO.loginSourceIP = httpServletRequest.getRemoteAddr();
        HttpSession session = httpServletRequest.getSession(true);
        session.setAttribute(SessionConstants.USER_LOGIN, loginDTO);
        return true;
    }

    private void setLastRequestedURI(HttpServletRequest httpServletRequest) {
        String requestedURI = httpServletRequest.getRequestURI();
        boolean setLastRequestedURI = true;

        if (!isGetRequest(httpServletRequest)) {
            setLastRequestedURI = false;
        } else if (requestedURI.equals(loginURI) || requestedURI.endsWith("LoginServlet") || requestedURI.endsWith("login.jsp")) {
            setLastRequestedURI = false;
        } else if (!(requestedURI.endsWith("Servlet") || requestedURI.endsWith("jsp"))) {
            setLastRequestedURI = false;
        }
        if (setLastRequestedURI) {
            String lastRequestedURL = requestedURI;
            if (httpServletRequest.getQueryString() != null) {
                lastRequestedURL += "?" + httpServletRequest.getQueryString();
            }
            httpServletRequest.getSession(true).setAttribute("lastRequestedURL", lastRequestedURL);
        }
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
    }
}
