package filter;

import login.LoginDTO;
import org.apache.log4j.Logger;
import permission.MenuDTO;
import permission.MenuRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.HttpRequestUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings({"unused"})
public class MenuPermissionFilter implements Filter {

    private static final Logger logger = Logger.getLogger(MenuPermissionFilter.class);

    public void destroy() {
    }

    private List<MenuDTO> getMenuPathByHyperlinkAndRequestType(String hyperlink, int requestMethodType) {
        MenuDTO menuDTO = MenuRepository.getInstance().getMenuDTOByHyperlinkAndRequestType(hyperlink, requestMethodType);
        if (menuDTO == null) {
            return Collections.emptyList();
        }
        if (!menuDTO.isVisible) {
            menuDTO = MenuRepository.getInstance().getMenuDTOByMenuID(menuDTO.selectedMenuID);
        }
        if (menuDTO == null) {
            return Collections.emptyList();
        }
        return getMenuPath(menuDTO);
    }

    private List<MenuDTO> getMenuPath(MenuDTO menuDTO) {
        List<MenuDTO> menuPath = new ArrayList<>();
        while (menuDTO != null && menuDTO.parentMenuID != 1) {
            menuPath.add(menuDTO);
            menuDTO = MenuRepository.getInstance().getMenuDTOByMenuID(menuDTO.parentMenuID);
        }
        if (menuDTO != null) menuPath.add(menuDTO);
        Collections.reverse(menuPath);
        return menuPath;
    }

    private boolean checkPermission(HttpServletRequest request, HttpServletResponse response, String URLWithoutContext, int requestMethodType) throws Exception {

        MenuDTO menuDTO = MenuRepository.getInstance().getMenuDTOByHyperlinkAndRequestType(URLWithoutContext, requestMethodType);
        if (menuDTO == null) {
            return true;
        }
        LoginDTO loginDTO = (login.LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        if (loginDTO == null) {
            return false;
        }
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        if (userDTO == null) {
            throw new Exception("No user found with userID " + loginDTO.userID);
        }
        return true;
    }


    private void setLeftSideMenuSelected(HttpServletRequest request, HttpServletResponse response,
                                         String URLWithoutContext, int requestMethodType, FilterChain chain) {
        logger.debug("setLeftSideMenuSelected called " + URLWithoutContext);
        List<MenuDTO> menuPath = getMenuPathByHyperlinkAndRequestType(URLWithoutContext, requestMethodType);

        /*
        checks for menuId in param and search by it
        Commented out for future reference.
        ** Remove it, if decision is final that this will not be used

        List<MenuDTO> menuPath;
        Integer menuId = StringUtils.parseNullableInteger(request.getParameter(MenuDAO.MENU_ID_PARAM_NAME));
        if (menuId != null) {
            menuPath = getMenuPath(
                    MenuRepository.getInstance().getMenuDTOByMenuID(menuId)
            );
        } else {
            menuPath = getMenuPathByHyperlinkAndRequestType(URLWithoutContext, requestMethodType);
        }*/

        List<Integer> menuPathIds = menuPath.stream()
                                            .map(MenuDTO::getMenuID)
                                            .collect(Collectors.toList());
        request.setAttribute("menuIDPath", menuPathIds);

        for (int i = 0; i < menuPath.size(); i++) {
            MenuDTO menuDTO = menuPath.get(menuPath.size() - 1 - i);
            if (i == 0) {
                request.setAttribute("menu", "" + menuDTO.getMenuID());
            } else {
                request.setAttribute("subMenu" + i, "" + menuDTO.getMenuID());
            }
        }
    }


    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;

        if (HttpRequestUtils.IGNORE_FILTER_THREAD_LOCAL.get() || HttpRequestUtils.BACK_END_AJAX_CALL_THREAD_LOCAL.get()) {
            chain.doFilter(request, response);
        } else {
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
            String URLWithoutContext = HttpRequestUtils.getURLWithoutContext(httpServletRequest);

            String requestMethodName = httpServletRequest.getMethod();

            int requestMethodType = -1;

            if ("GET".equalsIgnoreCase(requestMethodName)) {
                requestMethodType = MenuDTO.GET;
            } else if ("POST".equalsIgnoreCase(requestMethodName)) {
                requestMethodType = MenuDTO.POST;
            }

            try {
                setLeftSideMenuSelected(httpServletRequest, httpServletResponse, URLWithoutContext, requestMethodType, chain);
                if (checkPermission(httpServletRequest, httpServletResponse, URLWithoutContext, requestMethodType)) {
                    chain.doFilter(request, response);
                } else {
                    request.getRequestDispatcher("/common/error_page.jsp").forward(httpServletRequest, httpServletResponse);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void init(FilterConfig fConfig) throws ServletException {
    }
}