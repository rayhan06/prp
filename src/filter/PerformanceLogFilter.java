package filter;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.PerformanceLog;
import common.PerformanceLogDAO;
import login.LoginDTO;
import sessionmanager.SessionConstants;
import util.CurrentTimeFactory;
import util.HttpRequestUtils;


public class PerformanceLogFilter implements Filter {
    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        boolean isIgnore = HttpRequestUtils.ignoreHit(httpServletRequest);
        HttpRequestUtils.IGNORE_FILTER_THREAD_LOCAL.set(isIgnore);
        HttpRequestUtils.BACK_END_AJAX_CALL_THREAD_LOCAL.set(HttpRequestUtils.isBackEndAjaxCall((HttpServletRequest) request));
        if(isIgnore){
            filterChain.doFilter(request, response);
        }else{
            LoginDTO loginDTO = (LoginDTO) httpServletRequest.getSession().getAttribute(SessionConstants.USER_LOGIN);
            long requestTime = System.currentTimeMillis();
            CurrentTimeFactory.initializeCurrentTimeFactory();
            ((HttpServletResponse)response).addHeader("X-Frame-Options", "SAMEORIGIN");
            filterChain.doFilter(request, response);
            CurrentTimeFactory.destroyCurrentTimeFactory();
            insertToPerformanceLogDao(requestTime,System.currentTimeMillis(),((HttpServletRequest) request).getRequestURI(),
                    loginDTO != null ? loginDTO.userID : 0,httpServletRequest.getRemoteAddr(),httpServletRequest.getRemotePort());
        }
        HttpRequestUtils.IGNORE_FILTER_THREAD_LOCAL.remove();
        HttpRequestUtils.BACK_END_AJAX_CALL_THREAD_LOCAL.remove();
    }

    private static void insertToPerformanceLogDao(long requestTime,long responseTime, String uri,long userID,String ipAddress,int portNumber){
        PerformanceLog activityLog = new PerformanceLog();
        activityLog.requestTime = requestTime;
        activityLog.totalServiceTime = (int) (responseTime - requestTime);
        activityLog.URI = uri;
        activityLog.userID = userID;
        activityLog.ipAddress = ipAddress;
        activityLog.portNumber = portNumber;
        PerformanceLogDAO.insert(activityLog);
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
    }

}
