package gate_pass_item;
import java.util.*; 
import util.*; 


public class Gate_pass_itemDTO extends CommonDTO
{

	public long id = 0;
	public long gatePassId = 0;
	public long parliamentItemId = 0;
	public int amount = 0;
	public int approvedAmount = 0;
	public int holdAmount = 0;
    public String description = "";
	public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Gate_pass_itemDTO[" +
            " id = " + id +
            " gatePassId = " + gatePassId +
            " parliamentItemId = " + parliamentItemId +
            " amount = " + amount +
            " approvedAmount = " + approvedAmount +
            " description = " + description +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}