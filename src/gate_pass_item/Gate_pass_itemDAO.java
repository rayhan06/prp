package gate_pass_item;

import dbm.DBMR;
import dbm.DBMW;
import gate_pass.GatePassCardIssueModalItem;
import gate_pass.GatePassItemModel;
import gate_pass.Gate_passDAO;
import org.apache.log4j.Logger;
import parliament_item.Parliament_itemDTO;
import parliament_item.Parliament_itemRepository;
import pb.OptionDTO;
import pb.Utils;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.NavigationService4;
import util.StringUtils;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class Gate_pass_itemDAO extends NavigationService4 {

    Logger logger = Logger.getLogger(getClass());


    public Gate_pass_itemDAO(String tableName) {
        super(tableName);
        joinSQL = "";
        commonMaps = new Gate_pass_itemMAPS(tableName);
        columnNames = new String[]
                {
                        "id",
                        "gate_pass_id",
                        "parliament_item_id",
                        "amount",
                        "approved_amount",
                        "hold_amount",
                        "description",
                        "insertion_date",
                        "inserted_by",
                        "modified_by",
                        "isDeleted",
                        "lastModificationTime"
                };
    }

    public Gate_pass_itemDAO() {
        this("gate_pass_item");
    }

    public void setSearchColumn(Gate_pass_itemDTO gate_pass_itemDTO) {
        gate_pass_itemDTO.searchColumn = "";

    }

    public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException {
        Gate_pass_itemDTO gate_pass_itemDTO = (Gate_pass_itemDTO) commonDTO;
        int index = 1;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(gate_pass_itemDTO);
        if (isInsert) {
            ps.setObject(index++, gate_pass_itemDTO.iD);
        }
        ps.setObject(index++, gate_pass_itemDTO.gatePassId);
        ps.setObject(index++, gate_pass_itemDTO.parliamentItemId);
        ps.setObject(index++, gate_pass_itemDTO.amount);
        ps.setObject(index++, gate_pass_itemDTO.approvedAmount);
        ps.setObject(index++, gate_pass_itemDTO.holdAmount);
        ps.setObject(index++, gate_pass_itemDTO.description);
        ps.setObject(index++, gate_pass_itemDTO.insertionDate);
        ps.setObject(index++, gate_pass_itemDTO.insertedBy);
        ps.setObject(index++, gate_pass_itemDTO.modifiedBy);
        if (isInsert) {
            ps.setObject(index++, 0);
            ps.setObject(index++, lastModificationTime);
        }
    }

    public void get(Gate_pass_itemDTO gate_pass_itemDTO, ResultSet rs) throws SQLException {
        gate_pass_itemDTO.id = rs.getLong("id");
        gate_pass_itemDTO.gatePassId = rs.getLong("gate_pass_id");
        gate_pass_itemDTO.parliamentItemId = rs.getLong("parliament_item_id");
        gate_pass_itemDTO.amount = rs.getInt("amount");
        gate_pass_itemDTO.approvedAmount = rs.getInt("approved_amount");
        gate_pass_itemDTO.holdAmount = rs.getInt("hold_amount");
        gate_pass_itemDTO.description = rs.getString("description");
        gate_pass_itemDTO.insertionDate = rs.getLong("insertion_date");
        gate_pass_itemDTO.insertedBy = rs.getString("inserted_by");
        gate_pass_itemDTO.modifiedBy = rs.getString("modified_by");
        gate_pass_itemDTO.isDeleted = rs.getInt("isDeleted");
        gate_pass_itemDTO.lastModificationTime = rs.getLong("lastModificationTime");
    }

    public void buildGatePassItemModel(GatePassItemModel gatePassItemModel, ResultSet rs) throws SQLException {
        Parliament_itemDTO dto = Parliament_itemRepository.getInstance().getParliament_itemDTOByid(rs.getLong("parliament_item_id"));
        gatePassItemModel.id = rs.getLong("id");
        gatePassItemModel.nameEng = dto.nameEng;
        gatePassItemModel.nameBng = dto.nameBng;
        gatePassItemModel.amount = rs.getInt("amount");
        gatePassItemModel.approvedAmount = rs.getInt("approved_amount");
        gatePassItemModel.description = rs.getString("description");
    }

    //need another getter for repository
    public Gate_pass_itemDTO getDTOByID(long ID) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Gate_pass_itemDTO gate_pass_itemDTO = null;
        try {

            String sql = "SELECT * ";

            sql += " FROM " + tableName;

            sql += " WHERE id=" + ID;

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                gate_pass_itemDTO = new Gate_pass_itemDTO();

                get(gate_pass_itemDTO, rs);

            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return gate_pass_itemDTO;
    }

    public List<Gate_pass_itemDTO> getDTOs(Collection recordIDs) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Gate_pass_itemDTO gate_pass_itemDTO = null;
        List<Gate_pass_itemDTO> gate_pass_itemDTOList = new ArrayList<>();
        if (recordIDs.isEmpty()) {
            return gate_pass_itemDTOList;
        }
        try {

            String sql = "SELECT * ";

            sql += " FROM " + tableName;

            sql += " WHERE id IN ( ";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql += ",";
                }
                sql += ((ArrayList) recordIDs).get(i);
            }
            sql += ")  order by lastModificationTime desc";

            printSql(sql);

            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                gate_pass_itemDTO = new Gate_pass_itemDTO();
                get(gate_pass_itemDTO, rs);
                System.out.println("got this DTO: " + gate_pass_itemDTO);

                gate_pass_itemDTOList.add(gate_pass_itemDTO);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return gate_pass_itemDTOList;

    }

    //add repository
    public List<Gate_pass_itemDTO> getAllGate_pass_item(boolean isFirstReload) {
        List<Gate_pass_itemDTO> gate_pass_itemDTOList = new ArrayList<>();

        String sql = "SELECT * FROM gate_pass_item";
        sql += " WHERE ";


        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        sql += " order by gate_pass_item.lastModificationTime desc";
        printSql(sql);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;


        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);


            while (rs.next()) {
                Gate_pass_itemDTO gate_pass_itemDTO = new Gate_pass_itemDTO();
                get(gate_pass_itemDTO, rs);

                gate_pass_itemDTOList.add(gate_pass_itemDTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return gate_pass_itemDTOList;
    }


    public List<Gate_pass_itemDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }


    public List<Gate_pass_itemDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
                                           String filter, boolean tableHasJobCat) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        List<Gate_pass_itemDTO> gate_pass_itemDTOList = new ArrayList<>();

        try {

            String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Gate_pass_itemDTO gate_pass_itemDTO = new Gate_pass_itemDTO();
                get(gate_pass_itemDTO, rs);

                gate_pass_itemDTOList.add(gate_pass_itemDTO);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return gate_pass_itemDTOList;

    }

    public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                          UserDTO userDTO, String filter, boolean tableHasJobCat) {
        boolean viewAll = false;

        if (p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null) {
            System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
            viewAll = true;
        } else {
            System.out.println("ViewAll is null ");
        }

        String sql = "SELECT ";
        if (category == GETIDS) {
            sql += " " + tableName + ".ID as ID ";
        } else if (category == GETCOUNT) {
            sql += " count( " + tableName + ".ID) as countID ";
        } else if (category == GETDTOS) {
            sql += " distinct " + tableName + ".* ";
        }
        sql += "FROM " + tableName + " ";
        sql += joinSQL;


        String AnyfieldSql = "";
        String AllFieldSql = "";

        if (p_searchCriteria != null) {


            Enumeration names = p_searchCriteria.keys();
            String str, value;

            AnyfieldSql = "(";

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                AnyfieldSql += tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
            }
            AnyfieldSql += ")";
            System.out.println("AnyfieldSql = " + AnyfieldSql);

            AllFieldSql = "(";
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                System.out.println(str + ": " + value);
                if (value != null && !value.equalsIgnoreCase("") && (
                        str.equals("insertion_date_start")
                                || str.equals("insertion_date_end")
                )

                ) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql += " AND  ";
                    }

                    if (str.equals("insertion_date_start")) {
                        AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
                        i++;
                    } else if (str.equals("insertion_date_end")) {
                        AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
                        i++;
                    }


                }
            }

            AllFieldSql += ")";
            System.out.println("AllFieldSql = " + AllFieldSql);


        }


        sql += " WHERE ";

        sql += " (" + tableName + ".isDeleted = 0 ";
        sql += ")";


        if (!filter.equalsIgnoreCase("")) {
            sql += " and " + filter + " ";
        }

        if (!AnyfieldSql.equals("()") && !AnyfieldSql.equals("")) {
            sql += " AND " + AnyfieldSql;

        }
        if (!AllFieldSql.equals("()") && !AllFieldSql.equals("")) {
            sql += " AND " + AllFieldSql;
        }


        sql += " order by " + tableName + ".lastModificationTime desc ";

        printSql(sql);

        if (limit >= 0) {
            sql += " limit " + limit;
        }
        if (offset >= 0) {
            sql += " offset " + offset;
        }

        System.out.println("-------------- sql = " + sql);

        return sql;
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
                                           boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat) {
        return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
    }

    public List<GatePassItemModel> getGatePassItemModelByGatePassId(long gatePassId) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        GatePassItemModel gatePassItemModel = null;
        List<GatePassItemModel> gatePassItemModels = new ArrayList<>();

        try {

            String sql = "select * from gate_pass_item where isDeleted=0 and gate_pass_id=%d and amount!=0";
            sql = String.format(sql, gatePassId);
            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                gatePassItemModel = new GatePassItemModel();
                buildGatePassItemModel(gatePassItemModel, rs);

                gatePassItemModels.add(gatePassItemModel);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return gatePassItemModels;
    }

    public void updateItemAmountById(long ID, int amount) {
        Connection connection = null;
        Statement stmt = null;
        Gate_pass_itemDTO gate_pass_itemDTO = null;
        try {

            String sql = "update gate_pass_item set approved_amount=%d where id=%d";
            sql = String.format(sql, amount, ID);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.executeUpdate(sql);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
    }

    private Gate_pass_additional_itemDTO buildGatePassAdditionalItemDTO(ResultSet rs) throws SQLException {
        Gate_pass_additional_itemDTO gate_pass_additional_itemDTO = new Gate_pass_additional_itemDTO();
        Parliament_itemDTO dto = Parliament_itemRepository.getInstance().getParliament_itemDTOByid(rs.getLong("parliament_item_id"));

        gate_pass_additional_itemDTO.id = rs.getLong("ID");
        gate_pass_additional_itemDTO.parliamentItemId = rs.getLong("parliament_item_id");
        gate_pass_additional_itemDTO.gatePassId = rs.getLong("gate_pass_id");
        gate_pass_additional_itemDTO.amount = rs.getInt("amount");
        gate_pass_additional_itemDTO.description = rs.getString("description");
        gate_pass_additional_itemDTO.cardNumber = rs.getInt("card_number");
        gate_pass_additional_itemDTO.galleryCat = rs.getInt("gallery_cat");
        gate_pass_additional_itemDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
        gate_pass_additional_itemDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
        gate_pass_additional_itemDTO.insertionDate = rs.getLong("insertion_date");
        gate_pass_additional_itemDTO.lastModifierUser = rs.getString("last_modifier_user");
        gate_pass_additional_itemDTO.isDeleted = rs.getInt("isDeleted");
        gate_pass_additional_itemDTO.lastModificationTime = rs.getLong("lastModificationTime");
        gate_pass_additional_itemDTO.parliamentItemIdList = rs.getString("parliament_item_id_list");
        gate_pass_additional_itemDTO.visitorName = rs.getString("visitor_name");

        return gate_pass_additional_itemDTO;
    }

    public void addAdditionItemDTO(Gate_pass_additional_itemDTO dto) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        try {

            String sql = "insert into gate_pass_additional_item (gate_pass_id, parliament_item_id, parliament_item_id_list,"
            		+ " visitor_name, amount, description,"
            		+ " card_number, gallery_cat, inserted_by_user_id, " +
                    "inserted_by_organogram_id, insertion_date, last_modifier_user,"
                    + " isDeleted, lastModificationTime) "
                    + "values (%d,%d,'%s',"
                    + "'%s',%d,'%s',"
                    + "%d,%d,%d,"
                    + "%d,%d,'%s',"
                    + "%d,%d)";

            sql = String.format(sql, dto.gatePassId, dto.parliamentItemId, dto.parliamentItemIdList,
            		dto.visitorName, dto.amount, dto.description,
            		dto.cardNumber, dto.galleryCat, dto.insertedByUserId,
                    dto.insertedByOrganogramId, dto.insertionDate, dto.lastModifierUser,
                    dto.isDeleted, dto.lastModificationTime);


            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();

            stmt.executeUpdate(sql);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
    }

    public Gate_pass_additional_itemDTO getAdditionalItemDTOByCardNumber(long cardNumber) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Gate_pass_additional_itemDTO dto = null;
        try {

            String sql = "select * from gate_pass_additional_item where card_number=%d";
            sql = String.format(sql, cardNumber);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                dto = buildGatePassAdditionalItemDTO(rs);
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return dto;
    }

    public void deleteAdditionItemDTO(long cardNumber) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        long lastModificationTime = System.currentTimeMillis();

        try {

            String sql = "UPDATE gate_pass_additional_item SET isDeleted=1,lastModificationTime=" + lastModificationTime + " WHERE card_number = " + cardNumber + " AND isDeleted=0";

            connection = DBMW.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.executeUpdate(sql);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {

            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {

            }
        }
    }
    
    public void returnAdditionItemDTO(long id) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        long lastModificationTime = System.currentTimeMillis();

        try {

            String sql = "UPDATE gate_pass_additional_item SET return_time= " + lastModificationTime 
            		+ ", lastModificationTime=" + lastModificationTime 
            		+ " WHERE id = " + id + " AND return_time < 0";

            connection = DBMW.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.executeUpdate(sql);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {

            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {

            }
        }
    }

    public ReceivedItemModel buildReceivedItemModel(ResultSet rs) throws SQLException {

        int tempData;
        long gatePassId;
        ReceivedItemModel model = new ReceivedItemModel();
        Parliament_itemDTO parliament_itemDTO;
        String parliamentItemNameEn = "";
        String parliamentItemNameBn = "";

        String commanSeparatedItemIdList = rs.getString("parliament_item_id_list");
        long[] itemIdList = Arrays.asList(commanSeparatedItemIdList.split(",")).stream()
                .map(String::trim)
                .mapToLong(Long::parseLong).toArray();

        for (int i = 0; i < itemIdList.length; ++i) {
            parliament_itemDTO = Parliament_itemRepository.getInstance().getParliament_itemDTOByid(itemIdList[i]);

            parliamentItemNameEn += parliament_itemDTO.nameEng;
            parliamentItemNameBn += parliament_itemDTO.nameBng;

            if (i != itemIdList.length - 1) {
                parliamentItemNameEn += ", ";
                parliamentItemNameBn += ", ";
            }
        }


        model.itemNameEng = parliamentItemNameEn;
        model.itemNameBng = parliamentItemNameBn;


        tempData = rs.getInt("amount");
        model.amountEng = String.valueOf(tempData);
        model.amountBng = StringUtils.convertToBanNumber(model.amountEng);
        tempData = rs.getInt("card_number");
        model.cardNumberEng = String.valueOf(tempData);
        model.cardNumberBng = StringUtils.convertToBanNumber(model.cardNumberEng);

        model.description = rs.getString("description");

        gatePassId = rs.getLong("gate_pass_id");
        Gate_passDAO gate_passDAO = new Gate_passDAO();
        model.visitorName = rs.getString("visitor_name");

        return model;
    }

    public List<ReceivedItemModel> getReceivedItemModelByCardNumber(int cardNumber) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        List<ReceivedItemModel> modelList = new ArrayList<>();
        try {
            String sql = "select * from gate_pass_additional_item where card_number=%d AND isDeleted=0";
            sql = String.format(sql, cardNumber);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                modelList.add(buildReceivedItemModel(rs));
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return modelList;
    }

    public String getCardNumberByVisitorName(String name) {
        Connection connection = null;
        ResultSet rs;
        Statement stmt = null;
        try {
            String sql = "select * from gate_pass_additional_item where visitor_name='%s' AND isDeleted=0";
            sql = String.format(sql, name);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                return rs.getString("card_number");
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return "";
    }

    public List<GatePassCardIssueModalItem> getAdditionalItems(long gatePassId) {
        List<GatePassCardIssueModalItem> gatePassCardIssueModalItems = new ArrayList<>();
        Connection connection = null;
        ResultSet rs;
        Statement stmt = null;
        try {

            String sql = "select * from gate_pass_additional_item where gate_pass_id=%d";
            sql = String.format(sql, gatePassId);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                gatePassCardIssueModalItems.add(buildGatePassCardIssueModalItem(rs));
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }


        return gatePassCardIssueModalItems;
    }

    public GatePassCardIssueModalItem buildGatePassCardIssueModalItem(ResultSet rs) throws SQLException {
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        GatePassCardIssueModalItem gatePassCardIssueModalItem = new GatePassCardIssueModalItem();


        gatePassCardIssueModalItem.id = rs.getLong("ID");
        gatePassCardIssueModalItem.visitorName = rs.getString("visitor_name");
        gatePassCardIssueModalItem.amount = rs.getInt("amount");
        gatePassCardIssueModalItem.cardNumber = rs.getInt("card_number");
        gatePassCardIssueModalItem.galleryCat = rs.getInt("gallery_cat");
        gatePassCardIssueModalItem.description = rs.getString("description");
        gatePassCardIssueModalItem.itemIdList = rs.getString("parliament_item_id_list");
        gatePassCardIssueModalItem.returnTime = rs.getLong("return_time");

        return gatePassCardIssueModalItem;
    }

    public String buildHoldingItemsDropdown(String language, long gatePassId) {

        List<OptionDTO> optionDTOList = getAvailableItemList(gatePassId).stream()
                .map(dto -> new OptionDTO(dto.nameEng, dto.nameBng, String.valueOf(dto.id))).collect(Collectors.toList());

        return Utils.buildOptions(optionDTOList, language, null);
    }

    public List<GatePassItemModel> getAvailableItemList(long gatePassId) {
        List<GatePassItemModel> gatePassItemModels = new ArrayList<>();
        GatePassItemModel gatePassItemModel;
        Connection connection = null;
        ResultSet rs;
        Statement stmt = null;
        try {

            String sql = "select * from gate_pass_item where gate_pass_id=%d and hold_amount < approved_amount ";
            sql = String.format(sql, gatePassId);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                gatePassItemModel = new GatePassItemModel();
                buildGatePassItemModel(gatePassItemModel, rs);

                gatePassItemModels.add(gatePassItemModel);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return gatePassItemModels;
    }

    public int getMaxHoldingAmountByGatePassItemId(Long gatePassItemId) {
        Gate_pass_itemDTO dto = getDTOByID(gatePassItemId);
        return dto.approvedAmount - dto.holdAmount;
    }

    public List<GatePassItemModel> getHoldingItemsFromEncodedString(String holdingItemIdAmountString) {
        List<GatePassItemModel> gatePassItemModelList = new ArrayList<>();

        if (holdingItemIdAmountString.isEmpty()) {
            return gatePassItemModelList;
        }

        Arrays.stream(holdingItemIdAmountString.split(";")).forEach(idAmountString -> {
            String[] idAmountStringArr = idAmountString.split(",");
            Parliament_itemDTO parliamentItemDTO = Parliament_itemRepository.getInstance().getParliament_itemDTOByid(Long.parseLong(idAmountStringArr[0]));
            GatePassItemModel gatePassItemModel = new GatePassItemModel();
            gatePassItemModel.nameEng = parliamentItemDTO.nameEng;
            gatePassItemModel.nameBng = parliamentItemDTO.nameBng;
            gatePassItemModel.approvedAmount = Integer.parseInt(idAmountStringArr[1]);
            gatePassItemModelList.add(gatePassItemModel);
        });

        return gatePassItemModelList;
    }

    public void updateHoldAmount(Gate_pass_itemDTO gatePassItemDTO) {
        Connection connection = null;
        Statement stmt = null;
        try {

            String sql = "update gate_pass_item set hold_amount=%d where id=%d";
            sql = String.format(sql, gatePassItemDTO.holdAmount, gatePassItemDTO.id);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.executeUpdate(sql);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
    }
}
	