package gate_pass_item;

public class ReceivedItemModel {
    public String visitorName = "";
    public String itemNameEng = "";
    public String itemNameBng = "";
    public String amountEng = "";
    public String amountBng = "";
    public String cardNumberEng = "";
    public String cardNumberBng = "";
    public String description = "";


    @Override
    public String toString() {
        return "$ReceivedItemModel[" +
                " visitorName = " + visitorName +
                " amountEng = " + amountEng +
                " itemNameEng = " + itemNameEng +
                "]";
    }
}
