package gate_pass_item;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import gate_pass.GatePassPersonModel;
import gate_pass.Gate_passDAO;
import gate_pass.Gate_passDTO;
import gate_pass.Gate_passRepository;
import gate_pass_affiliated_person.Gate_pass_affiliated_personDAO;
import gate_pass_type.Gate_pass_typeDTO;
import gate_pass_visitor.Gate_pass_visitorDAO;
import gate_pass_visitor.Gate_pass_visitorDTO;
import org.apache.log4j.Logger;

import login.LoginDTO;
import parliament_item.Parliament_itemDAO;
import parliament_item.Parliament_itemDTO;

import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;

import java.util.stream.Collectors;

import com.google.gson.Gson;

import pb.*;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import workflow.WorkflowController;


/**
 * Servlet implementation class Gate_pass_itemServlet
 */
@WebServlet("/Gate_pass_itemServlet")
@MultipartConfig
public class Gate_pass_itemServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Gate_pass_itemServlet.class);

    String tableName = "gate_pass_item";

	Gate_pass_itemDAO gate_pass_itemDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Gate_pass_itemServlet()
	{
        super();
    	try
    	{
			gate_pass_itemDAO = new Gate_pass_itemDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(gate_pass_itemDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				commonRequestHandler.getAddPage(request, response);
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_ITEM_ADD))
//				{
//					commonRequestHandler.getAddPage(request, response);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_ITEM_UPDATE))
				{
					getGate_pass_item(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_ITEM_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchGate_pass_item(request, response, isPermanentTable, filter);
						}
						else
						{
							searchGate_pass_item(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchGate_pass_item(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_ITEM_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("getItemRowIssueCard"))
			{
				request.getRequestDispatcher("parliament_building_security/add_item_row.jsp").forward(request, response);
			}
			else if(actionType.equals("getHoldingItemRowIssueCard"))
			{
				request.getRequestDispatcher("parliament_building_security/add_holding_item_row.jsp").forward(request, response);
			}
			else if (actionType.equals("getAllItemList"))
			{
				List<OptionDTO> optionDTOList = null;
				List<Parliament_itemDTO> parliament_itemDTOS = new Parliament_itemDAO().getAllParliament_item(true);
				if (parliament_itemDTOS != null && !parliament_itemDTOS.isEmpty()) {
					optionDTOList = parliament_itemDTOS.stream()
							.map(dto -> new OptionDTO(dto.nameEng, dto.nameBng, String.valueOf(dto.iD)))
							.collect(Collectors.toList());
				}

				response.getWriter().write(gson.toJson(optionDTOList));
				response.getWriter().flush();
				response.getWriter().close();

				return;
			}
			else if(actionType.equals("return"))
			{
				long id = Long.parseLong(request.getParameter("id"));
				Gate_pass_itemDAO dao = new Gate_pass_itemDAO();
				dao.returnAdditionItemDTO(id);
				response.sendRedirect("Gate_passServlet?actionType=search");
			}
			else if (actionType.equals("maxNumberOfHoldingItem"))
			{
				Long gatePassItemId = Long.valueOf(request.getParameter("gatePassItemId"));

				response.getWriter().write(gson.toJson(gate_pass_itemDAO.getMaxHoldingAmountByGatePassItemId(gatePassItemId)));
				response.getWriter().flush();
				response.getWriter().close();

				return;
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_ITEM_ADD))
				{
					System.out.println("going to  addGate_pass_item ");
					addGate_pass_item(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addGate_pass_item ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_ITEM_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addGate_pass_item ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_ITEM_UPDATE))
				{
					addGate_pass_item(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_ITEM_SEARCH))
				{
					searchGate_pass_item(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
			else if(actionType.equals("issueCard"))
			{
				Gate_pass_itemDAO dao = new Gate_pass_itemDAO();
				Gate_passDAO gate_passDAO = new Gate_passDAO();
				int itemCount = Integer.parseInt(request.getParameter("totalItemCount"));
				int holdingItemCount = Integer.parseInt(request.getParameter("totalItemCountHold"));
				long gatePassId = Long.parseLong(request.getParameter("gatePassId"));
				List<String> cardIssuerNameList = new ArrayList<>();
				StringBuilder holdingItemIdAmountString = new StringBuilder();
				Gate_passDTO gate_passDTO = gate_passDAO.getDTOByID(gatePassId);
				boolean isGalleryPass = gate_passDTO.gatePassTypeId == Gate_pass_typeDTO.GALLERY_PASS;
				
				logger.debug("itemCount = " + itemCount + " holdingItemCount = " + holdingItemCount);

				for (int i = 0; i < holdingItemCount ; ++i) {
					try {
						long gatePassItemId = Long.parseLong(request.getParameter("parliamentItemId_hold_" + (i + 1)));
						int holdAmount = Integer.parseInt(request.getParameter("amount_hold_" + (i + 1)));
						Gate_pass_itemDTO gatePassItemDTO = gate_pass_itemDAO.getDTOByID(gatePassItemId);
						gatePassItemDTO.holdAmount += holdAmount;
						gate_pass_itemDAO.updateHoldAmount(gatePassItemDTO);
						holdingItemIdAmountString.append(gatePassItemDTO.parliamentItemId).append(",").append(holdAmount).append(";");
					} catch (Exception ex) {
						logger.debug("No new item is holding!");
					}
				}

				for(int i = 0; i < itemCount; i++){
					Gate_pass_additional_itemDTO dto = new Gate_pass_additional_itemDTO();

					try {
						dto.id = Long.parseLong(request.getParameter("additionItemId_" + (i + 1)));
					} catch (Exception e) {
						logger.debug("exception in addition item ID!");
					}

					try {
						dto.visitorName = request.getParameter("parliamentNameId_" + (i + 1));
					} catch (Exception e) {
						logger.debug("parliamentNameId_ not found");
						continue;
					}

					try {
						dto.parliamentItemIdList = request.getParameter("parliamentItemId_" + (i + 1));
					} catch (Exception e) {
						logger.debug("parliamentItemId_ not found");
						continue;
					}

					dto.gatePassId = gatePassId;

					try {
						dto.amount = Integer.parseInt(request.getParameter("amount_" + (i + 1)));
					} catch (Exception e) {
						logger.debug("amount_ not found");
						continue;
					}
					
					if(isGalleryPass)
					{
						try {
							dto.galleryCat = Integer.parseInt(request.getParameter("gallery_" + (i + 1)));
						} catch (Exception e) {
							logger.debug("gallery_ not found");
							continue;
						}
					}

					try {
						dto.cardNumber = Integer.parseInt(request.getParameter("card_number_" + (i + 1)));
					} catch (Exception e) {
						logger.debug("card_number_ not found");
						continue;
					}

					dto.description = request.getParameter("description_" + (i + 1));
					logger.debug("dto.amount = " + dto.amount + " dto.cardNumber = " + dto.cardNumber);
					System.out.println(dto);
					if(dto.amount > 0 && dto.cardNumber != 0) {
						dto.lastModifierUser = String.valueOf(userDTO.ID);
						dto.insertedByUserId = userDTO.ID;
						dto.insertedByOrganogramId = WorkflowController.getOrganogramIDFromUserID(userDTO.ID);
						dto.lastModificationTime = System.currentTimeMillis();
						dto.insertionDate =  Calendar.getInstance().getTimeInMillis();

						if (dto.id==0) {
							dao.addAdditionItemDTO(dto);
						}

						cardIssuerNameList.add(dto.visitorName);
						logger.debug("Added visitor = " + dto.visitorName);
					}
				}

				
				
				gate_passDTO.isIssued = setCardIssueStatusInBuildingSecurity(cardIssuerNameList, gatePassId, gate_passDTO.gatePassVisitorId);
				gate_passDTO.partiallyIssued = true;
				assert userDTO != null;
				gate_passDTO.issuedBy = userDTO.userName;
				gate_passDTO.issueDate = Calendar.getInstance().getTimeInMillis();
				gate_passDAO.update(gate_passDTO);

				response.sendRedirect(getServletContext().getContextPath()+"/Gate_passServlet?actionType=printPreview&ID="
						+ gatePassId + "&holdingItemIdAmountString=" + holdingItemIdAmountString);
			}
			else if(actionType.equals("checkCardAvailability"))
			{
				logger.debug("Do Nothing in checkCardAvailability actionType");
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private int setCardIssueStatusInBuildingSecurity(List<String> cardIssuerNameList, long gatePassId, long gatePassVisitorId) {
		List<String> allVisitorNameList = getAllVisitorNameListByGatePassId(gatePassId, gatePassVisitorId);

		for (String s : allVisitorNameList) {
			if (!cardIssuerNameList.contains(s)) {
				return 0;
			}
		}

		return 1;
	}

	private List<String> getAllVisitorNameListByGatePassId(long gatePassId, long gatePassVisitorId) {
		Gate_pass_visitorDAO gatePassVisitorDAO = new Gate_pass_visitorDAO();
		Gate_pass_affiliated_personDAO gatePassAffiliatedPersonDAO = new Gate_pass_affiliated_personDAO();
		List<String> resultString = new ArrayList<>();

		Gate_pass_visitorDTO gatePassVisitorDTO = gatePassVisitorDAO.getDTOByID(gatePassVisitorId);
		resultString.add(gatePassVisitorDTO.name);

		List<GatePassPersonModel> affiliatedPersonDTOS = gatePassAffiliatedPersonDAO.getGatePassPersonModelsByGatePassId(gatePassId);
		affiliatedPersonDTOS.forEach(gatePassPersonModel -> {
			resultString.add(gatePassPersonModel.name);
		});

		return resultString;
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Gate_pass_itemDTO gate_pass_itemDTO = gate_pass_itemDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(gate_pass_itemDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addGate_pass_item(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addGate_pass_item");
			String path = getServletContext().getRealPath("/img2/");
			Gate_pass_itemDTO gate_pass_itemDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				gate_pass_itemDTO = new Gate_pass_itemDTO();
			}
			else
			{
				gate_pass_itemDTO = gate_pass_itemDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("gatePassId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("gatePassId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				gate_pass_itemDTO.gatePassId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("parliamentItemId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("parliamentItemId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				gate_pass_itemDTO.parliamentItemId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("amount");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("amount = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				gate_pass_itemDTO.amount = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("description");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("description = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				gate_pass_itemDTO.description = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				gate_pass_itemDTO.insertionDate = c.getTimeInMillis();
			}


			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				gate_pass_itemDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				gate_pass_itemDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addGate_pass_item dto = " + gate_pass_itemDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				gate_pass_itemDAO.setIsDeleted(gate_pass_itemDTO.iD, CommonDTO.OUTDATED);
				returnedID = gate_pass_itemDAO.add(gate_pass_itemDTO);
				gate_pass_itemDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = gate_pass_itemDAO.manageWriteOperations(gate_pass_itemDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = gate_pass_itemDAO.manageWriteOperations(gate_pass_itemDTO, SessionConstants.UPDATE, -1, userDTO);
			}









			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getGate_pass_item(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Gate_pass_itemServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(gate_pass_itemDAO.getDTOByID(returnedID), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}









	private void getGate_pass_item(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getGate_pass_item");
		Gate_pass_itemDTO gate_pass_itemDTO = null;
		try
		{
			gate_pass_itemDTO = gate_pass_itemDAO.getDTOByID(id);
			request.setAttribute("ID", gate_pass_itemDTO.iD);
			request.setAttribute("gate_pass_itemDTO",gate_pass_itemDTO);
			request.setAttribute("gate_pass_itemDAO",gate_pass_itemDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "gate_pass_item/gate_pass_itemInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "gate_pass_item/gate_pass_itemSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "gate_pass_item/gate_pass_itemEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "gate_pass_item/gate_pass_itemEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getGate_pass_item(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getGate_pass_item(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchGate_pass_item(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchGate_pass_item 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_GATE_PASS_ITEM,
			request,
			gate_pass_itemDAO,
			SessionConstants.VIEW_GATE_PASS_ITEM,
			SessionConstants.SEARCH_GATE_PASS_ITEM,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("gate_pass_itemDAO",gate_pass_itemDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to gate_pass_item/gate_pass_itemApproval.jsp");
	        	rd = request.getRequestDispatcher("gate_pass_item/gate_pass_itemApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to gate_pass_item/gate_pass_itemApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("gate_pass_item/gate_pass_itemApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to gate_pass_item/gate_pass_itemSearch.jsp");
	        	rd = request.getRequestDispatcher("gate_pass_item/gate_pass_itemSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to gate_pass_item/gate_pass_itemSearchForm.jsp");
	        	rd = request.getRequestDispatcher("gate_pass_item/gate_pass_itemSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

