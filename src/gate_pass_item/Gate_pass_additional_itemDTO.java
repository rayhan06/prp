package gate_pass_item;


public class Gate_pass_additional_itemDTO
{

    public long id = 0;
    public long gatePassId = 0;
    public long parliamentItemId = 0;
    public int amount = 0;
    public String description = "";
    public int cardNumber = 0;
    public long insertionDate = 0;
    public long insertedByUserId = 0;
    public long insertedByOrganogramId = 0;
    public String lastModifierUser = "";
    public int isDeleted = 0;
    public long lastModificationTime = 0;
    public String parliamentItemIdList = "";
    public String visitorName = "";
    public long returnTime = -1;
    public int galleryCat = -1;


    @Override
    public String toString() {
        return "$Gate_pass_additional_itemDTO[" +
                " id = " + id +
                " gatePassId = " + gatePassId +
                " parliamentItemId = " + parliamentItemId +
                " parliamentItemIdList = " + parliamentItemIdList +
                " visitorName = " + visitorName +
                " amount = " + amount +
                " description = " + description +
                " insertionDate = " + insertionDate +
                " cardNumber = " + cardNumber +
                " insertedByUserId = " + insertedByUserId +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}