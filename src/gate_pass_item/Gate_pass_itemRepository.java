package gate_pass_item;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Gate_pass_itemRepository implements Repository {
	Gate_pass_itemDAO gate_pass_itemDAO = null;
	
	public void setDAO(Gate_pass_itemDAO gate_pass_itemDAO)
	{
		this.gate_pass_itemDAO = gate_pass_itemDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Gate_pass_itemRepository.class);
	Map<Long, Gate_pass_itemDTO>mapOfGate_pass_itemDTOToid;
	Map<Long, Set<Gate_pass_itemDTO> >mapOfGate_pass_itemDTOTogatePassId;
	Map<Long, Set<Gate_pass_itemDTO> >mapOfGate_pass_itemDTOToparliamentItemId;
	Map<Integer, Set<Gate_pass_itemDTO> >mapOfGate_pass_itemDTOToamount;
	Map<String, Set<Gate_pass_itemDTO> >mapOfGate_pass_itemDTOTodescription;
	Map<Long, Set<Gate_pass_itemDTO> >mapOfGate_pass_itemDTOToinsertionDate;
	Map<String, Set<Gate_pass_itemDTO> >mapOfGate_pass_itemDTOToinsertedBy;
	Map<String, Set<Gate_pass_itemDTO> >mapOfGate_pass_itemDTOTomodifiedBy;
	Map<Long, Set<Gate_pass_itemDTO> >mapOfGate_pass_itemDTOTolastModificationTime;


	static Gate_pass_itemRepository instance = null;  
	private Gate_pass_itemRepository(){
		mapOfGate_pass_itemDTOToid = new ConcurrentHashMap<>();
		mapOfGate_pass_itemDTOTogatePassId = new ConcurrentHashMap<>();
		mapOfGate_pass_itemDTOToparliamentItemId = new ConcurrentHashMap<>();
		mapOfGate_pass_itemDTOToamount = new ConcurrentHashMap<>();
		mapOfGate_pass_itemDTOTodescription = new ConcurrentHashMap<>();
		mapOfGate_pass_itemDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfGate_pass_itemDTOToinsertedBy = new ConcurrentHashMap<>();
		mapOfGate_pass_itemDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfGate_pass_itemDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Gate_pass_itemRepository getInstance(){
		if (instance == null){
			instance = new Gate_pass_itemRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(gate_pass_itemDAO == null)
		{
			return;
		}
		try {
			List<Gate_pass_itemDTO> gate_pass_itemDTOs = gate_pass_itemDAO.getAllGate_pass_item(reloadAll);
			for(Gate_pass_itemDTO gate_pass_itemDTO : gate_pass_itemDTOs) {
				Gate_pass_itemDTO oldGate_pass_itemDTO = getGate_pass_itemDTOByid(gate_pass_itemDTO.id);
				if( oldGate_pass_itemDTO != null ) {
					mapOfGate_pass_itemDTOToid.remove(oldGate_pass_itemDTO.id);
				
					if(mapOfGate_pass_itemDTOTogatePassId.containsKey(oldGate_pass_itemDTO.gatePassId)) {
						mapOfGate_pass_itemDTOTogatePassId.get(oldGate_pass_itemDTO.gatePassId).remove(oldGate_pass_itemDTO);
					}
					if(mapOfGate_pass_itemDTOTogatePassId.get(oldGate_pass_itemDTO.gatePassId).isEmpty()) {
						mapOfGate_pass_itemDTOTogatePassId.remove(oldGate_pass_itemDTO.gatePassId);
					}
					
					if(mapOfGate_pass_itemDTOToparliamentItemId.containsKey(oldGate_pass_itemDTO.parliamentItemId)) {
						mapOfGate_pass_itemDTOToparliamentItemId.get(oldGate_pass_itemDTO.parliamentItemId).remove(oldGate_pass_itemDTO);
					}
					if(mapOfGate_pass_itemDTOToparliamentItemId.get(oldGate_pass_itemDTO.parliamentItemId).isEmpty()) {
						mapOfGate_pass_itemDTOToparliamentItemId.remove(oldGate_pass_itemDTO.parliamentItemId);
					}
					
					if(mapOfGate_pass_itemDTOToamount.containsKey(oldGate_pass_itemDTO.amount)) {
						mapOfGate_pass_itemDTOToamount.get(oldGate_pass_itemDTO.amount).remove(oldGate_pass_itemDTO);
					}
					if(mapOfGate_pass_itemDTOToamount.get(oldGate_pass_itemDTO.amount).isEmpty()) {
						mapOfGate_pass_itemDTOToamount.remove(oldGate_pass_itemDTO.amount);
					}
					
					if(mapOfGate_pass_itemDTOTodescription.containsKey(oldGate_pass_itemDTO.description)) {
						mapOfGate_pass_itemDTOTodescription.get(oldGate_pass_itemDTO.description).remove(oldGate_pass_itemDTO);
					}
					if(mapOfGate_pass_itemDTOTodescription.get(oldGate_pass_itemDTO.description).isEmpty()) {
						mapOfGate_pass_itemDTOTodescription.remove(oldGate_pass_itemDTO.description);
					}
					
					if(mapOfGate_pass_itemDTOToinsertionDate.containsKey(oldGate_pass_itemDTO.insertionDate)) {
						mapOfGate_pass_itemDTOToinsertionDate.get(oldGate_pass_itemDTO.insertionDate).remove(oldGate_pass_itemDTO);
					}
					if(mapOfGate_pass_itemDTOToinsertionDate.get(oldGate_pass_itemDTO.insertionDate).isEmpty()) {
						mapOfGate_pass_itemDTOToinsertionDate.remove(oldGate_pass_itemDTO.insertionDate);
					}
					
					if(mapOfGate_pass_itemDTOToinsertedBy.containsKey(oldGate_pass_itemDTO.insertedBy)) {
						mapOfGate_pass_itemDTOToinsertedBy.get(oldGate_pass_itemDTO.insertedBy).remove(oldGate_pass_itemDTO);
					}
					if(mapOfGate_pass_itemDTOToinsertedBy.get(oldGate_pass_itemDTO.insertedBy).isEmpty()) {
						mapOfGate_pass_itemDTOToinsertedBy.remove(oldGate_pass_itemDTO.insertedBy);
					}
					
					if(mapOfGate_pass_itemDTOTomodifiedBy.containsKey(oldGate_pass_itemDTO.modifiedBy)) {
						mapOfGate_pass_itemDTOTomodifiedBy.get(oldGate_pass_itemDTO.modifiedBy).remove(oldGate_pass_itemDTO);
					}
					if(mapOfGate_pass_itemDTOTomodifiedBy.get(oldGate_pass_itemDTO.modifiedBy).isEmpty()) {
						mapOfGate_pass_itemDTOTomodifiedBy.remove(oldGate_pass_itemDTO.modifiedBy);
					}
					
					if(mapOfGate_pass_itemDTOTolastModificationTime.containsKey(oldGate_pass_itemDTO.lastModificationTime)) {
						mapOfGate_pass_itemDTOTolastModificationTime.get(oldGate_pass_itemDTO.lastModificationTime).remove(oldGate_pass_itemDTO);
					}
					if(mapOfGate_pass_itemDTOTolastModificationTime.get(oldGate_pass_itemDTO.lastModificationTime).isEmpty()) {
						mapOfGate_pass_itemDTOTolastModificationTime.remove(oldGate_pass_itemDTO.lastModificationTime);
					}
					
					
				}
				if(gate_pass_itemDTO.isDeleted == 0) 
				{
					
					mapOfGate_pass_itemDTOToid.put(gate_pass_itemDTO.id, gate_pass_itemDTO);
				
					if( ! mapOfGate_pass_itemDTOTogatePassId.containsKey(gate_pass_itemDTO.gatePassId)) {
						mapOfGate_pass_itemDTOTogatePassId.put(gate_pass_itemDTO.gatePassId, new HashSet<>());
					}
					mapOfGate_pass_itemDTOTogatePassId.get(gate_pass_itemDTO.gatePassId).add(gate_pass_itemDTO);
					
					if( ! mapOfGate_pass_itemDTOToparliamentItemId.containsKey(gate_pass_itemDTO.parliamentItemId)) {
						mapOfGate_pass_itemDTOToparliamentItemId.put(gate_pass_itemDTO.parliamentItemId, new HashSet<>());
					}
					mapOfGate_pass_itemDTOToparliamentItemId.get(gate_pass_itemDTO.parliamentItemId).add(gate_pass_itemDTO);
					
					if( ! mapOfGate_pass_itemDTOToamount.containsKey(gate_pass_itemDTO.amount)) {
						mapOfGate_pass_itemDTOToamount.put(gate_pass_itemDTO.amount, new HashSet<>());
					}
					mapOfGate_pass_itemDTOToamount.get(gate_pass_itemDTO.amount).add(gate_pass_itemDTO);
					
					if( ! mapOfGate_pass_itemDTOTodescription.containsKey(gate_pass_itemDTO.description)) {
						mapOfGate_pass_itemDTOTodescription.put(gate_pass_itemDTO.description, new HashSet<>());
					}
					mapOfGate_pass_itemDTOTodescription.get(gate_pass_itemDTO.description).add(gate_pass_itemDTO);
					
					if( ! mapOfGate_pass_itemDTOToinsertionDate.containsKey(gate_pass_itemDTO.insertionDate)) {
						mapOfGate_pass_itemDTOToinsertionDate.put(gate_pass_itemDTO.insertionDate, new HashSet<>());
					}
					mapOfGate_pass_itemDTOToinsertionDate.get(gate_pass_itemDTO.insertionDate).add(gate_pass_itemDTO);
					
					if( ! mapOfGate_pass_itemDTOToinsertedBy.containsKey(gate_pass_itemDTO.insertedBy)) {
						mapOfGate_pass_itemDTOToinsertedBy.put(gate_pass_itemDTO.insertedBy, new HashSet<>());
					}
					mapOfGate_pass_itemDTOToinsertedBy.get(gate_pass_itemDTO.insertedBy).add(gate_pass_itemDTO);
					
					if( ! mapOfGate_pass_itemDTOTomodifiedBy.containsKey(gate_pass_itemDTO.modifiedBy)) {
						mapOfGate_pass_itemDTOTomodifiedBy.put(gate_pass_itemDTO.modifiedBy, new HashSet<>());
					}
					mapOfGate_pass_itemDTOTomodifiedBy.get(gate_pass_itemDTO.modifiedBy).add(gate_pass_itemDTO);
					
					if( ! mapOfGate_pass_itemDTOTolastModificationTime.containsKey(gate_pass_itemDTO.lastModificationTime)) {
						mapOfGate_pass_itemDTOTolastModificationTime.put(gate_pass_itemDTO.lastModificationTime, new HashSet<>());
					}
					mapOfGate_pass_itemDTOTolastModificationTime.get(gate_pass_itemDTO.lastModificationTime).add(gate_pass_itemDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Gate_pass_itemDTO> getGate_pass_itemList() {
		List <Gate_pass_itemDTO> gate_pass_items = new ArrayList<Gate_pass_itemDTO>(this.mapOfGate_pass_itemDTOToid.values());
		return gate_pass_items;
	}
	
	
	public Gate_pass_itemDTO getGate_pass_itemDTOByid( long id){
		return mapOfGate_pass_itemDTOToid.get(id);
	}
	
	
	public List<Gate_pass_itemDTO> getGate_pass_itemDTOBygate_pass_id(long gate_pass_id) {
		return new ArrayList<>( mapOfGate_pass_itemDTOTogatePassId.getOrDefault(gate_pass_id,new HashSet<>()));
	}
	
	
	public List<Gate_pass_itemDTO> getGate_pass_itemDTOByparliament_item_id(long parliament_item_id) {
		return new ArrayList<>( mapOfGate_pass_itemDTOToparliamentItemId.getOrDefault(parliament_item_id,new HashSet<>()));
	}
	
	
	public List<Gate_pass_itemDTO> getGate_pass_itemDTOByamount(int amount) {
		return new ArrayList<>( mapOfGate_pass_itemDTOToamount.getOrDefault(amount,new HashSet<>()));
	}
	
	
	public List<Gate_pass_itemDTO> getGate_pass_itemDTOBydescription(String description) {
		return new ArrayList<>( mapOfGate_pass_itemDTOTodescription.getOrDefault(description,new HashSet<>()));
	}
	
	
	public List<Gate_pass_itemDTO> getGate_pass_itemDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfGate_pass_itemDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Gate_pass_itemDTO> getGate_pass_itemDTOByinserted_by(String inserted_by) {
		return new ArrayList<>( mapOfGate_pass_itemDTOToinsertedBy.getOrDefault(inserted_by,new HashSet<>()));
	}
	
	
	public List<Gate_pass_itemDTO> getGate_pass_itemDTOBymodified_by(String modified_by) {
		return new ArrayList<>( mapOfGate_pass_itemDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Gate_pass_itemDTO> getGate_pass_itemDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfGate_pass_itemDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "gate_pass_item";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


