package gate_pass_item;
import java.util.*; 
import util.*;


public class Gate_pass_itemMAPS extends CommonMaps
{	
	public Gate_pass_itemMAPS(String tableName)
	{
		

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("gatePassId".toLowerCase(), "gatePassId".toLowerCase());
		java_DTO_map.put("parliamentItemId".toLowerCase(), "parliamentItemId".toLowerCase());
		java_DTO_map.put("amount".toLowerCase(), "amount".toLowerCase());
		java_DTO_map.put("description".toLowerCase(), "description".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("gate_pass_id".toLowerCase(), "gatePassId".toLowerCase());
		java_SQL_map.put("parliament_item_id".toLowerCase(), "parliamentItemId".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Gate Pass Id".toLowerCase(), "gatePassId".toLowerCase());
		java_Text_map.put("Parliament Item Id".toLowerCase(), "parliamentItemId".toLowerCase());
		java_Text_map.put("Amount".toLowerCase(), "amount".toLowerCase());
		java_Text_map.put("Description".toLowerCase(), "description".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}