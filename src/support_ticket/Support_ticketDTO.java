package support_ticket;
import java.text.SimpleDateFormat;
import java.util.*;

import asset_clearance_letter.Asset_clearance_letterDTO;
import asset_model.AssetAssigneeDTO;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import ticket_issues.Ticket_issuesDAO;
import ticket_issues.Ticket_issuesDTO;
import user.UserDTO;
import user.UserRepository;
import util.*;
import workflow.WingModel;
import workflow.WorkflowController; 


public class Support_ticketDTO extends CommonDTO
{
	public static final int OPEN = 1;
	public static final int CLOSED = 20;
	public static final int SOLVED = AssetAssigneeDTO.SOLVE_TICKET;
	
	public static final int HIGH = 3;
	public static final int MEDIUM = 2;
	public static final int LOW = 1;
	

	public long departmentDatetimeId = 0;
	public long ticketIssuesType = -1;
	public long ticketIssuesSubtypeType = -1;
    public String description = "";
    public String issueText = "";
    public String configuration = "";
	public int ticketStatusCat = OPEN;
	public long dueDate = 0;
	public long dueTime = 0;
	public long filesDropzone = -1;
	public long currentAssignedUserId = 0;
	public long currentAssignedOrganoramId = -1;
	public int priorityCat = 0;
	public int roomNoCat = -1;
	public long insertedByUserId = 0;
	public long insertedByOrganogramId = 0;
	public long insertionDate = 0;
    public String modifiedBy = "";
    public String closingComments = "";
	public long closingDate = 0;
	public long closedById = 0;
	public long issueRaiserUserId = 0;
	public long issueRaiserOrganoramId = -1;
	public long issueRaiserDept = -1;
	public long parentUnit = -1;
	public String iruEn = "";
	public String iruBn = "";
	public String puEn = "";
	public String puBn = "";
	public long lastDTOId = -1;
	public long wingId = -1;
	public String wingEn = "";
	public String wingBn = "";
	public String lastDTOText = "";
	
	public String nocClearanceRemarks = "";
	public String nocApprovalRemarks = "";
	
	public String supportOrgs = "";
	public String origSupportOrgs = "";
	public String supportEids = "";
	public String supportJobs = "";
	public String supportDones = "";
	
	public List<Long> supportOrgsArray = null;
	public List<Long> origSupportOrgsArray = null;
	public List<Long> supportEidsArray = null;
	public List<Long> supportJobsArray =  null;
	public List<Long> supportDonesArray = null;
	
	public int forwardActionCat = -1; //not in dao
	public long assetOwnerOrgId = -1;
	
	public long officeBuildingId = -1;
	
	public String assetAssigneeId = "-1";
	public int nocStatusCat = Asset_clearance_letterDTO.PENDING;
	
	public Support_ticketDTO()
	{
		
	}
	
	public Support_ticketDTO(Asset_clearance_letterDTO asset_clearance_letterDTO, UserDTO userDTO)
	{
		ticketIssuesType = Ticket_issuesDTO.NOC_ISSUE;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		Ticket_issuesDAO ticket_issuesDAO = Ticket_issuesDAO.getInstance();
		Support_ticketDAO support_ticketDAO = new Support_ticketDAO();
        Ticket_issuesDTO ticket_issuesDTO = ticket_issuesDAO.getDTOFromID(this.ticketIssuesType);

        this.currentAssignedOrganoramId = ticket_issuesDTO.adminOrganogramId;
        this.currentAssignedUserId = UserRepository.getUserDTOByOrganogramID(this.currentAssignedOrganoramId).ID;
        this.issueRaiserOrganoramId = asset_clearance_letterDTO.organogramId;
        this.priorityCat = HIGH;
        this.nocStatusCat = asset_clearance_letterDTO.clearanceStatusCat;
        
        
        Support_ticketDTO lastDTO = support_ticketDAO.getLastDTO(this.issueRaiserOrganoramId);
        UserDTO issueRaiserDTO = UserRepository.getUserDTOByOrganogramID(this.issueRaiserOrganoramId);
        if (issueRaiserDTO != null) {
            this.issueRaiserUserId = issueRaiserDTO.ID;
            this.issueRaiserDept = issueRaiserDTO.unitID;

            Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(this.issueRaiserDept);
            if (office_unitsDTO != null) {
                this.parentUnit = office_unitsDTO.parentUnitId;
                this.iruEn = office_unitsDTO.unitNameEng;
                this.iruBn = office_unitsDTO.unitNameBng;

                Office_unitsDTO parentUnitDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(this.parentUnit);

                if (parentUnitDTO != null) {
                    this.puEn = parentUnitDTO.unitNameEng;
                    this.puBn = parentUnitDTO.unitNameBng;
                }
            }
            WingModel wingModel = WorkflowController.getWingModelFromOrganogramId(this.issueRaiserOrganoramId);
            this.wingId = wingModel.id;
            this.wingEn = wingModel.nameEn;
            this.wingBn = wingModel.nameBn;
        }
        if (lastDTO != null) {
            this.lastDTOId = lastDTO.closedById;
            this.lastDTOText = f.format(new Date(lastDTO.insertionDate));
        }
        
        this.description = "Check if the user assets are handed over properly.";
        
        this.insertedByUserId = userDTO.ID;
        this.insertedByOrganogramId = userDTO.organogramID;
        this.insertionDate = System.currentTimeMillis();
	}
	
	
	
	
	
    @Override
	public String toString() {
            return "$Support_ticketDTO[" +
            " iD = " + iD +
            " departmentDatetimeId = " + departmentDatetimeId +
            " ticketIssuesType = " + ticketIssuesType +
            " ticketIssuesSubtypeType = " + ticketIssuesSubtypeType +
            " description = " + description +
            " configuration = " + configuration +
            " ticketStatusCat = " + ticketStatusCat +
            " dueDate = " + dueDate +
            " filesDropzone = " + filesDropzone +
            " currentAssignedUserId = " + currentAssignedUserId +
            " priorityCat = " + priorityCat +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " modifiedBy = " + modifiedBy +
            " searchColumn = " + searchColumn +
            " closingComments = " + closingComments +
            " closingDate = " + closingDate +
            " closedById = " + closedById +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}