package support_ticket;
import java.util.*; 
import util.*; 


public class Forward_ticketDTO extends CommonDTO
{

	public int ticketStatusCat = -1;
    public String remarks = "";
	public long dueDate = System.currentTimeMillis();
    public String configuration = "";
	public long filesDropzone = -1;
	public long mainReceiver = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String modifiedBy = "";
    public long ticketIssuesType = 0;
	public long ticketIssuesSubtypeType = 0;
	
	
    @Override
	public String toString() {
            return "$Forward_ticketDTO[" +
            " iD = " + iD +
            " ticketStatusCat = " + ticketStatusCat +
            " remarks = " + remarks +
            " dueDate = " + dueDate +
            " configuration = " + configuration +
            " filesDropzone = " + filesDropzone +
            " mainReceiver = " + mainReceiver +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " modifiedBy = " + modifiedBy +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}