package support_ticket;

import appointment.KeyCountDTO;
import appointment.YearMonthCount;
import common.ConnectionAndStatementUtil;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import org.apache.log4j.Logger;
import pb.CommonDAO;
import pb.Utils;
import pb_notifications.Pb_notificationsDAO;
import repository.RepositoryManager;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.NavigationService4;
import util.TimeConverter;
import workflow.WorkflowController;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

@SuppressWarnings({"rawtypes","Duplicates"})
public class Support_ticketDAO extends NavigationService4 {

    private final Logger logger = Logger.getLogger(Support_ticketDAO.class);
    public static final String DEADLINE_TICKET_START = "Deadline over for ticket";

    public Support_ticketDAO(String tableName) {
        super(tableName);
        joinSQL = "";
        commonMaps = new Support_ticketMAPS(tableName);
        columnNames = new String[]
                {
                        "ID",
                        "department_datetime_id",
                        "ticket_issues_type",
                        "ticket_issues_subtype_type",
                        "description",
                        "configuration",
                        "ticket_status_cat",
                        "due_date",
                        "due_time",
                        "files_dropzone",
                        "current_assigned_user_id",
                        "priority_cat",
                        "inserted_by_user_id",
                        "inserted_by_organogram_id",
                        "insertion_date",
                        "modified_by",
                        "search_column",
                        "closing_comments",
                        "closing_date",
                        "closed_by_id",
                        "current_assigned_organogram_id",
                        "issue_raiser_user_id",
                        "issue_raiser_organogram_id",
                        "issue_text",
                        "last_dto_id",
                        "last_dto_text",
                        "issue_raiser_dept",
                        "issue_raiser_unit_name_bn",
                        "issue_raiser_unit_name_en",
                        "parent_unit",
                        "parent_unit_name_en",
                        "parent_unit_name_bn",
                        "asset_assignee_id",
                        "room_no_cat",
                        "wing_id",
                        "wing_en",
                        "wing_bn",
                        "noc_status_cat",
                        "noc_clearance_remarks",
                        "noc_approval_remarks",
                        "asset_owner_org_id",
                        
                        "support_orgs",
                        "support_eids",
                        "support_jobs",
                        "support_dones",
                        "orig_support_orgs",
                        
                        "office_building_id",
                        
                        "isDeleted",
                        "lastModificationTime"
                };
    }

    public Support_ticketDAO() {
        this("support_ticket");
    }

    public void setSearchColumn(Support_ticketDTO support_ticketDTO) {
        support_ticketDTO.searchColumn = support_ticketDTO.iD + " " + Utils.getDigits(support_ticketDTO.iD, "bangla");
        support_ticketDTO.searchColumn += CommonDAO.getName("English", "ticket_issues", support_ticketDTO.ticketIssuesType) + " " + CommonDAO.getName("Bangla", "ticket_issues", support_ticketDTO.ticketIssuesType) + " ";
        support_ticketDTO.searchColumn += CommonDAO.getName("English", "ticket_issues_subtype", support_ticketDTO.ticketIssuesSubtypeType) + " " + CommonDAO.getName("Bangla", "ticket_issues_subtype", support_ticketDTO.ticketIssuesSubtypeType) + " ";

    }

    public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException {
        Support_ticketDTO support_ticketDTO = (Support_ticketDTO) commonDTO;
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(support_ticketDTO);
        if (isInsert) {
            ps.setObject(++index, support_ticketDTO.iD);
        }
        ps.setObject(++index, support_ticketDTO.departmentDatetimeId);
        ps.setObject(++index, support_ticketDTO.ticketIssuesType);
        ps.setObject(++index, support_ticketDTO.ticketIssuesSubtypeType);
        ps.setObject(++index, support_ticketDTO.description);
        ps.setObject(++index, support_ticketDTO.configuration);
        ps.setObject(++index, support_ticketDTO.ticketStatusCat);
        ps.setObject(++index, support_ticketDTO.dueDate);
        ps.setObject(++index, support_ticketDTO.dueTime);
        ps.setObject(++index, support_ticketDTO.filesDropzone);
        ps.setObject(++index, support_ticketDTO.currentAssignedUserId);
        ps.setObject(++index, support_ticketDTO.priorityCat);
        ps.setObject(++index, support_ticketDTO.insertedByUserId);
        ps.setObject(++index, support_ticketDTO.insertedByOrganogramId);
        ps.setObject(++index, support_ticketDTO.insertionDate);
        ps.setObject(++index, support_ticketDTO.modifiedBy);
        ps.setObject(++index, support_ticketDTO.searchColumn);
        ps.setObject(++index, support_ticketDTO.closingComments);
        ps.setObject(++index, support_ticketDTO.closingDate);
        ps.setObject(++index, support_ticketDTO.closedById);
        ps.setObject(++index, support_ticketDTO.currentAssignedOrganoramId);
        ps.setObject(++index, support_ticketDTO.issueRaiserUserId);
        ps.setObject(++index, support_ticketDTO.issueRaiserOrganoramId);
        ps.setObject(++index, support_ticketDTO.issueText);
        ps.setObject(++index, support_ticketDTO.lastDTOId);
        ps.setObject(++index, support_ticketDTO.lastDTOText);
        ps.setObject(++index, support_ticketDTO.issueRaiserDept);
        ps.setObject(++index, support_ticketDTO.iruBn);
        ps.setObject(++index, support_ticketDTO.iruEn);
        ps.setObject(++index, support_ticketDTO.parentUnit);
        ps.setObject(++index, support_ticketDTO.puEn);
        ps.setObject(++index, support_ticketDTO.puBn);
        ps.setObject(++index, support_ticketDTO.assetAssigneeId);
        ps.setObject(++index, support_ticketDTO.roomNoCat);
        ps.setObject(++index, support_ticketDTO.wingId);
        ps.setObject(++index, support_ticketDTO.wingEn);
        ps.setObject(++index, support_ticketDTO.wingBn);
        ps.setObject(++index, support_ticketDTO.nocStatusCat);
        ps.setObject(++index, support_ticketDTO.nocClearanceRemarks);
        ps.setObject(++index, support_ticketDTO.nocApprovalRemarks);
        ps.setObject(++index, support_ticketDTO.assetOwnerOrgId);
        
        ps.setObject(++index, Utils.ArraylistToString(support_ticketDTO.supportOrgsArray));
        ps.setObject(++index, Utils.ArraylistToString(support_ticketDTO.supportEidsArray));
        ps.setObject(++index, Utils.ArraylistToString(support_ticketDTO.supportJobsArray));
        ps.setObject(++index, Utils.ArraylistToString(support_ticketDTO.supportDonesArray));
        ps.setObject(++index, Utils.ArraylistToString(support_ticketDTO.origSupportOrgsArray));
        
        ps.setObject(++index, support_ticketDTO.officeBuildingId);
        if (isInsert) {
            ps.setObject(++index, 0);
            ps.setObject(++index, lastModificationTime);
        }
    }

    public Support_ticketDTO build(ResultSet rs) {
        try {
            Support_ticketDTO support_ticketDTO = new Support_ticketDTO();
            support_ticketDTO.iD = rs.getLong("ID");
            support_ticketDTO.departmentDatetimeId = rs.getLong("department_datetime_id");
            support_ticketDTO.ticketIssuesType = rs.getLong("ticket_issues_type");
            support_ticketDTO.ticketIssuesSubtypeType = rs.getLong("ticket_issues_subtype_type");
            support_ticketDTO.description = rs.getString("description");
            support_ticketDTO.configuration = rs.getString("configuration");
            support_ticketDTO.ticketStatusCat = rs.getInt("ticket_status_cat");
            support_ticketDTO.dueDate = rs.getLong("due_date");
            support_ticketDTO.dueTime = rs.getLong("due_time");
            support_ticketDTO.filesDropzone = rs.getLong("files_dropzone");
            support_ticketDTO.currentAssignedUserId = rs.getLong("current_assigned_user_id");
            support_ticketDTO.priorityCat = rs.getInt("priority_cat");
            support_ticketDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
            support_ticketDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
            support_ticketDTO.insertionDate = rs.getLong("insertion_date");
            support_ticketDTO.modifiedBy = rs.getString("modified_by");
            support_ticketDTO.searchColumn = rs.getString("search_column");
            support_ticketDTO.closingComments = rs.getString("closing_comments");
            support_ticketDTO.closingDate = rs.getLong("closing_date");
            support_ticketDTO.closedById = rs.getLong("closed_by_id");
            support_ticketDTO.issueRaiserOrganoramId = rs.getLong("current_assigned_organogram_id");
            support_ticketDTO.currentAssignedOrganoramId = rs.getLong("current_assigned_organogram_id");
            support_ticketDTO.issueRaiserUserId = rs.getLong("issue_raiser_user_id");
            support_ticketDTO.issueRaiserOrganoramId = rs.getLong("issue_raiser_organogram_id");
            support_ticketDTO.issueText = rs.getString("issue_text");
            support_ticketDTO.lastDTOId = rs.getLong("last_dto_id");
            support_ticketDTO.lastDTOText = rs.getString("last_dto_text");
            support_ticketDTO.issueRaiserDept = rs.getLong("issue_raiser_dept");
            support_ticketDTO.iruBn = rs.getString("issue_raiser_unit_name_bn");
            support_ticketDTO.iruEn = rs.getString("issue_raiser_unit_name_en");
            support_ticketDTO.parentUnit = rs.getLong("parent_unit");
            support_ticketDTO.puEn = rs.getString("parent_unit_name_en");
            support_ticketDTO.puBn = rs.getString("parent_unit_name_bn");
            support_ticketDTO.assetAssigneeId = rs.getString("asset_assignee_id");
            support_ticketDTO.roomNoCat = rs.getInt("room_no_cat");
            support_ticketDTO.wingId = rs.getLong("wing_id");
            support_ticketDTO.wingEn = rs.getString("wing_en");
            support_ticketDTO.wingBn = rs.getString("wing_bn");
            support_ticketDTO.nocStatusCat = rs.getInt("noc_status_cat");
            
            support_ticketDTO.nocClearanceRemarks = rs.getString("noc_clearance_remarks");
            support_ticketDTO.nocApprovalRemarks = rs.getString("noc_approval_remarks");
            
            support_ticketDTO.assetOwnerOrgId = rs.getLong("asset_owner_org_id");
            
            support_ticketDTO.supportOrgs = rs.getString("support_orgs");
            support_ticketDTO.supportEids = rs.getString("support_eids");
            support_ticketDTO.supportJobs = rs.getString("support_jobs");
            support_ticketDTO.supportDones = rs.getString("support_dones");
            support_ticketDTO.origSupportOrgs = rs.getString("orig_support_orgs");
            
            support_ticketDTO.supportOrgsArray = Utils.StringToArrayList(support_ticketDTO.supportOrgs);
            support_ticketDTO.supportEidsArray = Utils.StringToArrayList(support_ticketDTO.supportEids);
            support_ticketDTO.supportJobsArray = Utils.StringToArrayList(support_ticketDTO.supportJobs);
            support_ticketDTO.supportDonesArray = Utils.StringToArrayList(support_ticketDTO.supportDones);
            support_ticketDTO.origSupportOrgsArray = Utils.StringToArrayList(support_ticketDTO.origSupportOrgs);
            
            support_ticketDTO.officeBuildingId = rs.getLong("office_building_id");
            
            support_ticketDTO.isDeleted = rs.getInt("isDeleted");
            support_ticketDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return support_ticketDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }
    
    public void supportDone(Support_ticketDTO support_ticketDTO, long orgId)
    {
    	support_ticketDTO.supportOrgsArray.remove(orgId);
    }
    
    public void addTicketNoti(Support_ticketDTO support_ticketDTO)
    {
    	addTicketNoti(support_ticketDTO, false);
    }
    
    public void addTicketNoti(Support_ticketDTO support_ticketDTO, boolean isSupport)
    {
    	Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();
    	String URL = "Support_ticketServlet?actionType=view&ID=" + support_ticketDTO.iD;
		String englishText = "", banglaText= "";
		if(support_ticketDTO.ticketStatusCat == Support_ticketDTO.CLOSED)
		{
	          englishText = "Your ticket #" + support_ticketDTO.iD + " is solved.";
	          banglaText = "আপনার টিকেট  #" + Utils.getDigits(support_ticketDTO.iD, "bangla") + " নিষ্পত্তি হয়েছে।";
		}
		else
		{
			  englishText = "A ticket #" + support_ticketDTO.iD + " is waiting for your action.";
	          banglaText = "একটি টিকেট #" + Utils.getDigits(support_ticketDTO.iD, "bangla") + " আপনার পদক্ষেপের অপেক্ষায় আছে।";

		}
		if(isSupport)
		{
			String deadEng = DEADLINE_TICKET_START + " #" + support_ticketDTO.iD;
	        String deadBng = "টিকেট #" + Utils.getDigits(support_ticketDTO.iD, "bangla") + " এর ডেডলাইন শেষ।";

			for(long org: support_ticketDTO.supportOrgsArray)
			{
				pb_notificationsDAO.addPb_notificationsAndSendMailSMS(org, System.currentTimeMillis(), URL,
		        		englishText, banglaText, "Ticket Action", "");
				pb_notificationsDAO.addPb_notificationsAndSendMailSMS(org, getCrossTime(support_ticketDTO), URL,
						deadEng, deadBng, "Ticket Deadline Over", "");
			}
			
		}
		else
		{
			pb_notificationsDAO.addPb_notificationsAndSendMailSMS(support_ticketDTO.currentAssignedOrganoramId, System.currentTimeMillis(), URL,
	        		englishText, banglaText, "Ticket Action", "");
		}
    }

    public boolean canRaiseIssueForThisUser(UserDTO userDTO, Support_ticketDTO support_ticketDTO) {
        
        if (userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE
                || userDTO.roleID == SessionConstants.ADMIN_ROLE) {
            return true;
        } 
        else 
        {
        	UserDTO issueRaiserDTO = UserRepository.getUserDTOByOrganogramID(support_ticketDTO.issueRaiserOrganoramId);
            if (issueRaiserDTO == null || userDTO == null) {
                return false;
            }
            if (userDTO.organogramID == support_ticketDTO.issueRaiserOrganoramId) {
                return true;
            }

            return userDTO.unitID == support_ticketDTO.issueRaiserDept
                    && OfficeUnitOrganogramsRepository.getInstance().getImmediateSuperior(support_ticketDTO.issueRaiserOrganoramId).id == userDTO.organogramID;
        }
    }

    public KeyCountDTO getTicketStatuswiseCount(ResultSet rs) {
        try {
            KeyCountDTO keyCountDTO = new KeyCountDTO();
            keyCountDTO.key = rs.getLong("ticket_status_cat");
            keyCountDTO.count = rs.getInt("count(distinct support_ticket.id)");
            return keyCountDTO;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<KeyCountDTO> getTicketStatusCount(long issueRaiserOrganogramId) {
        String sql = "SELECT ticket_status_cat, count(distinct support_ticket.id) FROM support_ticket where isdeleted = 0";
        if (issueRaiserOrganogramId != -1) {
            sql += " and issue_raiser_organogram_id = " + issueRaiserOrganogramId;
        }
        sql += " group by ticket_status_cat";
        return ConnectionAndStatementUtil.getListOfT(sql, this::getTicketStatuswiseCount);
    }
    
    public String getTicketJoinSQLForAdmins(UserDTO userDTO)
    {
    	String sql = "";
		if (userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE || userDTO.roleID == SessionConstants.SUPPORT_ENGINEER_ROLE) {
	         sql += " join ticket_issues on ticket_issues.id = support_ticket.ticket_issues_type ";
	         sql += " join employee_offices on (ticket_issues.admin_organogram_id = employee_offices.office_unit_organogram_id  "
	         		+ " and employee_offices.status = 1)";
	     }
		 return sql;
    }
    
    public String getTicketFilterSQLForAdmins(UserDTO userDTO)
    {
    	String sql = "";
    	if (userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE || userDTO.roleID == SessionConstants.SUPPORT_ENGINEER_ROLE)
        {
        	long userOffice = WorkflowController.getOfficeIdFromOrganogramId(userDTO.organogramID);
        	if(userOffice == SessionConstants.MAINTENANCE1_OFFICE || userOffice == SessionConstants.MAINTENANCE2_OFFICE)
        	{
        		sql += " and ( office_unit_id = " + SessionConstants.MAINTENANCE1_OFFICE  + " or  office_unit_id = " +  SessionConstants.MAINTENANCE2_OFFICE + ")";
        	}
        	else
        	{
        		sql += " and ( office_unit_id = " + userOffice  + ")";
        	}
        }
		 return sql;
    }
    
    public List<KeyCountDTO> getTicketStatusCount(UserDTO userDTO) {
        String sql = "SELECT ticket_status_cat, count(distinct support_ticket.id) FROM support_ticket ";
        sql += getTicketJoinSQLForAdmins(userDTO);
        sql += " where support_ticket.isdeleted = 0";
        sql += getTicketFilterSQLForAdmins(userDTO);
        sql += " group by ticket_status_cat";
        return ConnectionAndStatementUtil.getListOfT(sql, this::getTicketStatuswiseCount);
    }

  

    public List<KeyCountDTO> getLast7DayOpenCount(UserDTO userDTO) {
        String sql = "SELECT ((support_ticket.insertion_date DIV 86400000) * 86400000 - 21600000) AS tdate, count(distinct support_ticket.id) FROM support_ticket";
        sql += getTicketJoinSQLForAdmins(userDTO);
        sql += " where support_ticket.isDeleted = 0 and support_ticket.insertion_date >=" + TimeConverter.getNthDay(-7) ;
        sql += getTicketFilterSQLForAdmins(userDTO);
        sql += " group by tdate order by insertion_date asc";

        return ConnectionAndStatementUtil.getListOfT(sql, this::getOpenCloseCount);
    }

    public KeyCountDTO getOpenCloseCount(ResultSet rs) {
        try {
            KeyCountDTO keyCountDTO = new KeyCountDTO();
            keyCountDTO.key = rs.getLong("tdate");
            keyCountDTO.count = rs.getInt("count(distinct support_ticket.id)");
            return keyCountDTO;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<KeyCountDTO> getLast7DayCloseCount(UserDTO userDTO) {
        String sql = "SELECT ((closing_date DIV 86400000) * 86400000 - 21600000) AS tdate, count(distinct support_ticket.id) FROM support_ticket";
        sql += getTicketJoinSQLForAdmins(userDTO);
        sql += " where closing_date >=" + TimeConverter.getNthDay(-7)
                + " and support_ticket.isDeleted = 0 and ticket_status_cat = " + Support_ticketDTO.CLOSED;
        sql += getTicketFilterSQLForAdmins(userDTO);
        sql += " group by tdate order by closing_date asc";

        return ConnectionAndStatementUtil.getListOfT(sql, this::getOpenCloseCount);
    }

    public KeyCountDTO getTypewiseCount(ResultSet rs) {
        try {
            KeyCountDTO keyCountDTO = new KeyCountDTO();
            keyCountDTO.key = rs.getLong("ticket_issues_type");
            keyCountDTO.count = rs.getInt("count(id)");

            return keyCountDTO;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<KeyCountDTO> getTypewiseCount() {
        String sql = "SELECT ticket_issues_type, count(id) FROM support_ticket where isDeleted = 0";
        sql += " group by ticket_issues_type order by ticket_issues_type asc";

        return ConnectionAndStatementUtil.getListOfT(sql, this::getTypewiseCount);
    }

    public KeyCountDTO getWingwiseCount(ResultSet rs) {
        try {
            KeyCountDTO keyCountDTO = new KeyCountDTO();
            keyCountDTO.key = rs.getLong("wing_id");
            keyCountDTO.count = rs.getInt("count(distinct support_ticket.id)");
            keyCountDTO.nameEn = rs.getString("wing_en");
            keyCountDTO.nameBn = rs.getString("wing_bn");

            return keyCountDTO;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<KeyCountDTO> getWingwiseCount(UserDTO userDTO) {
        String sql = "SELECT wing_id, wing_en, wing_bn, count(distinct support_ticket.id) FROM support_ticket";
        sql += getTicketJoinSQLForAdmins(userDTO);
        sql += " where support_ticket.isDeleted = 0 and wing_id != -1 ";
        sql += getTicketFilterSQLForAdmins(userDTO);
        sql += " group by wing_id order by wing_id asc";

        return ConnectionAndStatementUtil.getListOfT(sql, this::getWingwiseCount);
    }

    public YearMonthCount getYmCount(ResultSet rs) {
        try {
            YearMonthCount ymCount = new YearMonthCount();
            String ym = rs.getString("ym");
            ymCount.year = Integer.parseInt(ym.split("-")[0]);
            ymCount.month = Integer.parseInt(ym.split("-")[1]);
            ymCount.count = rs.getInt("count(distinct support_ticket.id)");
            return ymCount;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public List<YearMonthCount> getLast6MonthOpenCount(UserDTO userDTO) {
        String sql = "SELECT \r\n" +
                "    DATE_FORMAT(FROM_UNIXTIME(`insertion_date` / 1000),\r\n" +
                "            '%Y-%m') AS ym,\r\n" +
                "    COUNT(distinct support_ticket.id)\r\n" +
                "FROM\r\n" +
                "    support_ticket\r\n";
        sql += getTicketJoinSQLForAdmins(userDTO);
        sql +=   " WHERE\r\n" +
                "    insertion_date >= " + TimeConverter.get1stDayOfNthtMonth(-6) + " AND support_ticket.isDeleted = 0 \r\n";
        sql += getTicketFilterSQLForAdmins(userDTO);
        sql += " GROUP BY ym\r\n";
        return ConnectionAndStatementUtil.getListOfT(sql, this::getYmCount);
    }

    public List<YearMonthCount> getLast6MonthOpenCount(long issueRaiserId) {
        String sql = "SELECT \r\n" +
                "    DATE_FORMAT(FROM_UNIXTIME(`insertion_date` / 1000),\r\n" +
                "            '%Y-%m') AS ym,\r\n" +
                "    COUNT(distinct support_ticket.id)\r\n" +
                "FROM\r\n" +
                "    support_ticket\r\n" +
                "WHERE\r\n" +
                "    insertion_date >= " + TimeConverter.get1stDayOfNthtMonth(-6) + " AND support_ticket.isDeleted = 0 \r\n";
        if (issueRaiserId != -1) {
            sql += " and issue_raiser_organogram_id = " + issueRaiserId;
        }
        sql += " GROUP BY ym\r\n";
        return ConnectionAndStatementUtil.getListOfT(sql, this::getYmCount);
    }
    
    public List<YearMonthCount> getLast6MonthCloseCount(UserDTO userDTO) {
        String sql = "SELECT \r\n" +
                "    DATE_FORMAT(FROM_UNIXTIME(`closing_date` / 1000),\r\n" +
                "            '%Y-%m') AS ym,\r\n" +
                "    COUNT(distinct support_ticket.id)\r\n" +
                "FROM\r\n" +
                "    support_ticket\r\n";
        sql += getTicketJoinSQLForAdmins(userDTO);
        sql += " WHERE\r\n" +
                "    closing_date >= " + TimeConverter.get1stDayOfNthtMonth(-6) + " AND support_ticket.isDeleted = 0 and ticket_status_cat = " + Support_ticketDTO.CLOSED + "\r\n";
        sql += getTicketFilterSQLForAdmins(userDTO);
        sql += " GROUP BY ym\r\n";
        return ConnectionAndStatementUtil.getListOfT(sql, this::getYmCount);
    }

    public List<YearMonthCount> getLast6MonthCloseCount(long issueRaiserId) {
        String sql = "SELECT \r\n" +
                "    DATE_FORMAT(FROM_UNIXTIME(`closing_date` / 1000),\r\n" +
                "            '%Y-%m') AS ym,\r\n" +
                "    COUNT(distinct support_ticket.id)\r\n" +
                "FROM\r\n" +
                "    support_ticket\r\n" +
                "WHERE\r\n" +
                "    closing_date >= " + TimeConverter.get1stDayOfNthtMonth(-6) + " AND support_ticket.isDeleted = 0 and ticket_status_cat = " + Support_ticketDTO.CLOSED + "\r\n";
        if (issueRaiserId != -1) {
            sql += " and issue_raiser_organogram_id = " + issueRaiserId;
        }
        sql += " GROUP BY ym\r\n";
        return ConnectionAndStatementUtil.getListOfT(sql, this::getYmCount);
    }

    public Support_ticketDTO getDTOByID(long id) {
        String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
        return ConnectionAndStatementUtil.getT(sql, this::build);
    }

    public Support_ticketDTO getSupportTicketDTOByAssigneeId(long assetAssigneeId) {
        String sql = "SELECT * FROM " + tableName + " WHERE isDeleted = 0 and ticket_status_cat = " + Support_ticketDTO.OPEN
                + " and asset_assignee_id = " + assetAssigneeId;
        return ConnectionAndStatementUtil.getT(sql, this::build);
    }

    public Support_ticketDTO getLastDTO(long issueRaiser) {
        String sql = "SELECT * FROM support_ticket where isDeleted = 0 and inserted_by_organogram_id = "
                + issueRaiser + " order by insertion_date desc limit 1";
        return ConnectionAndStatementUtil.getT(sql, this::build);
    }

    public List<Support_ticketDTO> getDTOs(List<Long> recordIDs) {
        StringBuilder sql = new StringBuilder("SELECT * FROM " + tableName + " WHERE ID IN ( ");
        for (int i = 0; i < recordIDs.size(); i++) {
            if (i != 0) {
                sql.append(",");
            }
            sql.append((recordIDs).get(i));
        }
        sql.append(")  order by lastModificationTime desc");
        printSql(sql.toString());
        return ConnectionAndStatementUtil.getListOfT(sql.toString(), this::build);
    }

    public int getCount(ResultSet rs) {
        try {
            return rs.getInt("count(distinct issue_raiser_dept)");
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }

    }

    public int getUnitCount(String filter) {
        String sql = "select count(distinct issue_raiser_dept) from support_ticket where isDeleted = 0 ";
        if (filter != null && !filter.equalsIgnoreCase("")) {
            sql += " and " + filter;
        }
        return ConnectionAndStatementUtil.getT(sql, this::getCount, 0);
    }

    public long getUnitId(ResultSet rs) {
        try {
            return rs.getLong("issue_raiser_dept");
        } catch (SQLException e) {
            e.printStackTrace();
            return 0L;
        }

    }

    public long getAssignedTo(ResultSet rs) {
        try {
            return rs.getLong("current_assigned_organogram_id");
        } catch (SQLException e) {
            e.printStackTrace();
            return 0L;
        }

    }

    public List<Long> getAssignedTos() {
        String sql = "select distinct current_assigned_organogram_id from support_ticket where isDeleted = 0 and current_assigned_organogram_id != -1";
        return ConnectionAndStatementUtil.getListOfT(sql, this::getAssignedTo);
    }

    public List<Long> getUnits(String filter) {
        String sql = "select distinct issue_raiser_dept from support_ticket where isDeleted = 0 ";
        if (filter != null && !filter.equalsIgnoreCase("")) {
            sql += " and " + filter;
        }
        return ConnectionAndStatementUtil.getListOfT(sql, this::getUnitId);
    }

    public List<Support_ticketDTO> getAllSupport_ticket(boolean isFirstReload) {
        String sql = "SELECT * FROM " + tableName + " WHERE ";
        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        sql += " order by " + tableName + ".lastModificationTime desc";
        return ConnectionAndStatementUtil.getListOfT(sql, this::build);
    }


    public List<Support_ticketDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }


    public List<Support_ticketDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
                                           String filter, boolean tableHasJobCat) {
        String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
        return ConnectionAndStatementUtil.getListOfT(sql, this::build);
    }
    
    public long getCrossTime(Support_ticketDTO support_ticketDTO)
    {
    	return support_ticketDTO.dueDate + support_ticketDTO.dueTime + Utils.HOUR6_IN_MILLIS;
    }

    public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                          UserDTO userDTO, String filter) {
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");

        if (p_searchCriteria != null && p_searchCriteria.get("viewAll") != null) {
            System.out.println("ViewAll = " + p_searchCriteria.get("viewAll"));
        } else {
            System.out.println("ViewAll is null ");
        }

        String sql = "SELECT ";
        if (category == GETIDS) {
            sql += " " + tableName + ".ID as ID ";
        } else if (category == GETCOUNT) {
            sql += " count( distinct " + tableName + ".ID) as countID ";
        } else if (category == GETDTOS) {
            sql += " distinct " + tableName + ".* ";
        }
        sql += "FROM " + tableName + " ";
        /*if (userDTO.roleID != SessionConstants.ADMIN_ROLE && userDTO.roleID != SessionConstants.TICKET_ADMIN_ROLE) {
            sql += " join ticket_forward_history on ticket_forward_history.support_ticket_id = support_ticket.id ";
        }*/
        
        sql += getTicketJoinSQLForAdmins(userDTO);


        String AnyfieldSql = "";
        StringBuilder AllFieldSql = new StringBuilder();

        if (p_searchCriteria != null) {


            Enumeration names = p_searchCriteria.keys();
            String str, value;

            AnyfieldSql = "(";

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                AnyfieldSql += tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
            }
            AnyfieldSql += ")";
            System.out.println("AnyfieldSql = " + AnyfieldSql);

            AllFieldSql = new StringBuilder("(");
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                System.out.println(str + ": " + value);
                if (value != null && !value.equalsIgnoreCase("") && (
                        str.equals("ticket_issues_type")
                                || str.equals("ticket_issues_subtype_type")
                                || str.equals("ticket_status_cat")
                                || str.equals("closing_date_start")
                                || str.equals("closing_date_end")
                                || str.equals("issue_raiser_organogram_id")
                                || str.equals("current_assigned_organogram_id")
                                || str.equals("insertion_date_start")
                                || str.equals("insertion_date_end")
                                || str.equals("ticket_id")
                                || str.equals("room_no_cat")
                                || str.equals("deadline")

                )

                ) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql.append(" AND  ");
                    }

                    switch (str) {
                        case "ticket_issues_type":
                            AllFieldSql.append(tableName).append(".ticket_issues_type = ").append(Long.parseLong((String) p_searchCriteria.get(str)));
                            i++;
                            break;
                        case "ticket_issues_subtype_type":
                            AllFieldSql.append(tableName).append(".ticket_issues_subtype_type = ").append(Long.parseLong((String) p_searchCriteria.get(str)));
                            i++;
                            break;
                        case "ticket_status_cat":
                            AllFieldSql.append(tableName).append(".ticket_status_cat = ").append(Integer.parseInt((String) p_searchCriteria.get(str)));
                            i++;
                            break;
                        case "room_no_cat":
                            AllFieldSql.append(tableName).append(".room_no_cat = ").append(Integer.parseInt((String) p_searchCriteria.get(str)));
                            i++;
                            break;
                        case "deadline":
                        	int deadlineStatus = Integer.parseInt((String) p_searchCriteria.get(str));
                        	long now = System.currentTimeMillis();
                            if(deadlineStatus == 1)
                            {
                            	AllFieldSql.append(" (support_ticket.due_date != 0 and support_ticket.due_date < " + now + ") and ticket_status_cat = " + Support_ticketDTO.OPEN);
                            }
                            else if(deadlineStatus == 0)
                            {
                            	AllFieldSql.append(" (support_ticket.due_date = 0 or support_ticket.due_date >= " + now + " or ticket_status_cat != " +  Support_ticketDTO.OPEN + ")");
                            }
                            
                            i++;
                            break;
                        case "ticket_id":
                            AllFieldSql.append(tableName).append(".id = ").append(Long.parseLong((String) p_searchCriteria.get(str)));
                            i++;
                            break;
                        case "closing_date_start":
                        	System.out.println("closing_date_start = " + simpleDateFormat.format(Long.parseLong((String) p_searchCriteria.get(str))));
                            AllFieldSql.append(tableName).append(".closing_date >= ").append(Long.parseLong((String) p_searchCriteria.get(str)));
                            i++;
                            break;
                        case "closing_date_end":
                        	System.out.println("closing_date_end = " + simpleDateFormat.format(Long.parseLong((String) p_searchCriteria.get(str))));
                            AllFieldSql.append(tableName).append(".closing_date <= ").append(Long.parseLong((String) p_searchCriteria.get(str)));
                            i++;
                            break;
                        case "issue_raiser_organogram_id":
                            AllFieldSql.append(tableName).append(".issue_raiser_organogram_id = ").append(Long.parseLong((String) p_searchCriteria.get(str)));
                            i++;
                            break;
                        case "current_assigned_organogram_id":
                            AllFieldSql.append("( " + tableName).append(".current_assigned_organogram_id = ").append(Long.parseLong((String) p_searchCriteria.get(str)));
                            AllFieldSql.append(" or " + tableName).append(".orig_support_orgs like '%").append(Long.parseLong((String) p_searchCriteria.get(str))).append("%' ")
                            .append(")");
                            
                            i++;
                            break;
                        case "insertion_date_start":
                        	System.out.println("insertion_date_start = " + simpleDateFormat.format(Long.parseLong((String) p_searchCriteria.get(str))));
                            AllFieldSql.append(tableName).append(".insertion_date >= ").append(Long.parseLong((String) p_searchCriteria.get(str)));
                            i++;
                            break;
                        case "insertion_date_end":
                        	System.out.println("insertion_date_end = " + simpleDateFormat.format(Long.parseLong((String) p_searchCriteria.get(str))));
                            AllFieldSql.append(tableName).append(".insertion_date <= ").append(Long.parseLong((String) p_searchCriteria.get(str)));
                            i++;
                            break;
                    }


                }
            }

            AllFieldSql.append(")");
            System.out.println("AllFieldSql = " + AllFieldSql);

        }


        sql += " WHERE ";

        sql += " (" + tableName + ".isDeleted = 0 ";
        sql += ")";


        if (!filter.equalsIgnoreCase("")) {
            sql += " and " + filter + " ";
        }

        if (!AnyfieldSql.equals("()") && !AnyfieldSql.equals("")) {
            sql += " AND " + AnyfieldSql;

        }
        if (!AllFieldSql.toString().equals("()") && !AllFieldSql.toString().equals("")) {
            sql += " AND " + AllFieldSql;
        }

        if (userDTO.roleID != SessionConstants.ADMIN_ROLE && userDTO.roleID != SessionConstants.TICKET_ADMIN_ROLE && userDTO.roleID != SessionConstants.SUPPORT_ENGINEER_ROLE) {
            sql += " AND ( support_ticket.inserted_by_organogram_id = " + userDTO.organogramID
                    + " or support_ticket.issue_raiser_organogram_id = " + userDTO.organogramID
                    + ")";
        }
        
        sql += getTicketFilterSQLForAdmins(userDTO);


        sql += " order by support_ticket.ticket_status_cat asc, support_ticket.priority_cat desc, support_ticket.id desc ";
        //sql += " order by support_ticket.id desc ";

        printSql(sql);

        if (limit >= 0) {
            sql += " limit " + limit;
        }
        if (offset >= 0) {
            sql += " offset " + offset;
        }

        System.out.println("-------------- sql = " + sql);

        return sql;
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
                                           boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat) {
        return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter);
    }
}