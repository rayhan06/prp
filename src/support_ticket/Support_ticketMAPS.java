package support_ticket;
import java.util.*; 
import util.*;


public class Support_ticketMAPS extends CommonMaps
{	
	public Support_ticketMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("departmentDatetimeId".toLowerCase(), "departmentDatetimeId".toLowerCase());
		java_DTO_map.put("ticketIssuesType".toLowerCase(), "ticketIssuesType".toLowerCase());
		java_DTO_map.put("ticketIssuesSubtypeType".toLowerCase(), "ticketIssuesSubtypeType".toLowerCase());
		java_DTO_map.put("description".toLowerCase(), "description".toLowerCase());
		java_DTO_map.put("configuration".toLowerCase(), "configuration".toLowerCase());
		java_DTO_map.put("ticketStatusCat".toLowerCase(), "ticketStatusCat".toLowerCase());
		java_DTO_map.put("dueDate".toLowerCase(), "dueDate".toLowerCase());
		java_DTO_map.put("filesDropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_DTO_map.put("currentAssignedUserId".toLowerCase(), "currentAssignedUserId".toLowerCase());
		java_DTO_map.put("priorityCat".toLowerCase(), "priorityCat".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("closingComments".toLowerCase(), "closingComments".toLowerCase());
		java_DTO_map.put("closingDate".toLowerCase(), "closingDate".toLowerCase());
		java_DTO_map.put("closedById".toLowerCase(), "closedById".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("department_datetime_id".toLowerCase(), "departmentDatetimeId".toLowerCase());
		java_SQL_map.put("ticket_issues_type".toLowerCase(), "ticketIssuesType".toLowerCase());
		java_SQL_map.put("ticket_issues_subtype_type".toLowerCase(), "ticketIssuesSubtypeType".toLowerCase());
		java_SQL_map.put("description".toLowerCase(), "description".toLowerCase());
		java_SQL_map.put("configuration".toLowerCase(), "configuration".toLowerCase());
		java_SQL_map.put("ticket_status_cat".toLowerCase(), "ticketStatusCat".toLowerCase());
		java_SQL_map.put("due_date".toLowerCase(), "dueDate".toLowerCase());
		java_SQL_map.put("files_dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_SQL_map.put("current_assigned_user_id".toLowerCase(), "currentAssignedUserId".toLowerCase());
		java_SQL_map.put("priority_cat".toLowerCase(), "priorityCat".toLowerCase());
		java_SQL_map.put("inserted_by_user_id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_SQL_map.put("inserted_by_organogram_id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_SQL_map.put("insertion_date".toLowerCase(), "insertionDate".toLowerCase());
		java_SQL_map.put("modified_by".toLowerCase(), "modifiedBy".toLowerCase());
		java_SQL_map.put("closing_comments".toLowerCase(), "closingComments".toLowerCase());
		java_SQL_map.put("closing_date".toLowerCase(), "closingDate".toLowerCase());
		java_SQL_map.put("closed_by_id".toLowerCase(), "closedById".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Department Datetime Id".toLowerCase(), "departmentDatetimeId".toLowerCase());
		java_Text_map.put("Ticket Issues".toLowerCase(), "ticketIssuesType".toLowerCase());
		java_Text_map.put("Ticket Issues Subtype".toLowerCase(), "ticketIssuesSubtypeType".toLowerCase());
		java_Text_map.put("Description".toLowerCase(), "description".toLowerCase());
		java_Text_map.put("Configuration".toLowerCase(), "configuration".toLowerCase());
		java_Text_map.put("Ticket Status".toLowerCase(), "ticketStatusCat".toLowerCase());
		java_Text_map.put("Due Date".toLowerCase(), "dueDate".toLowerCase());
		java_Text_map.put("Files Dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_Text_map.put("Current Assigned User Id".toLowerCase(), "currentAssignedUserId".toLowerCase());
		java_Text_map.put("Priority".toLowerCase(), "priorityCat".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Closing Comments".toLowerCase(), "closingComments".toLowerCase());
		java_Text_map.put("Closing Date".toLowerCase(), "closingDate".toLowerCase());
		java_Text_map.put("Closed By Id".toLowerCase(), "closedById".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}