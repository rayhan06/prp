package support_ticket;


import asset_model.AssetAssigneeDAO;
import asset_model.AssetAssigneeDTO;

import com.google.gson.Gson;

import asset_clearance_letter.Asset_clearance_letterDAO;
import asset_clearance_letter.Asset_clearance_letterDTO;
import asset_clearance_status.Asset_clearance_statusDAO;
import asset_clearance_status.Asset_clearance_statusDTO;
import files.FilesDAO;
import language.LC;
import language.LM;
import login.LoginDTO;
import workflow.OrganogramDetails;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.VerticalAlignment;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import pb.CatRepository;
import pb.PBNameRepository;
import pb.Utils;
import pb_notifications.Pb_notificationsDAO;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import support_engineers_diary.Support_engineers_diaryDAO;
import support_engineers_diary.Support_engineers_diaryDTO;
import ticket_forward_history.Ticket_forward_historyDAO;
import ticket_forward_history.Ticket_forward_historyDTO;
import ticket_issues.TicketIssueSubTypeRepository;
import ticket_issues.Ticket_issuesDAO;
import ticket_issues.Ticket_issuesDTO;
import ticket_issues.Ticket_issuesRepository;
import user.UserDTO;
import user.UserRepository;
import user_expertise_map.User_expertise_mapDAO;
import user_expertise_map.User_expertise_mapDTO;
import util.CommonRequestHandler;
import util.HttpRequestUtils;
import util.RecordNavigationManager4;

import workflow.WingModel;
import workflow.WorkflowController;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import java.util.Hashtable;

import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates", "unchecked"})
@WebServlet("/Support_ticketServlet")
@MultipartConfig
public class Support_ticketServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Support_ticketServlet.class);
    private final Support_ticketDAO support_ticketDAO = new Support_ticketDAO();
    private final CommonRequestHandler commonRequestHandler = new CommonRequestHandler(support_ticketDAO);
    private final Ticket_forward_historyDAO ticket_forward_historyDAO = new Ticket_forward_historyDAO();
    private final FilesDAO filesDAO = new FilesDAO();
    private final Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();
    Asset_clearance_letterDAO asset_clearance_letterDAO = new Asset_clearance_letterDAO();
    private final Gson gson = new Gson();


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getAddPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_ADD)) {
                    commonRequestHandler.getAddPage(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
                return;
            }
            switch (actionType) {
                case "getForwardPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_ADD)) 
                    {
                    	long supportTicketId = Long.parseLong(request.getParameter("supportTicketId"));
                    	Support_ticketDTO support_ticketDTO = support_ticketDAO.getDTOByID(supportTicketId);
                    	if(support_ticketDTO.ticketIssuesType == Ticket_issuesDTO.NOC_ISSUE)
                    	{
                    		RequestDispatcher requestDispatcher = request.getRequestDispatcher("support_ticket/forward_nocEdit.jsp");
                    		requestDispatcher.forward(request, response);
                    	}
                    	else
                    	{
                    		RequestDispatcher requestDispatcher = request.getRequestDispatcher("support_ticket/forward_ticketEdit.jsp");
                    		requestDispatcher.forward(request, response);
                    	}
                        
                        
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    return;
                case "downloadDropzoneFile":
                    commonRequestHandler.downloadDropzoneFile(request, response, filesDAO);
                    break;
                case "search":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH)) {
                        String filter = request.getParameter("filter");
                        logger.debug("filter = " + filter);
                        if (filter != null && filter.equalsIgnoreCase("getHistory")) {
                            long issuer = Long.parseLong(request.getParameter("issuer"));
                            filter = " issue_raiser_organogram_id = " + issuer;
                            searchSupport_ticket(request, response, filter);
                        }
                        if (filter != null && filter.equalsIgnoreCase("typeWise")) {
                            long type = Long.parseLong(request.getParameter("type"));
                            filter = " ticket_issues_type = " + type;
                            searchSupport_ticket(request, response, filter);
                        }
                        else if (filter != null) 
                        {
                            filter = ""; //shouldn't be directly used, rather manipulate it.
                            searchSupport_ticket(request, response, filter);
                        }
                        else 
                        {
                            searchSupport_ticket(request, response, "");
                        }

                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    return;
                case "OPEN_TICKET_COUNT_FILTER":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH)) {
                        String filter = " support_ticket.ticket_status_cat = " + Support_ticketDTO.OPEN;                        
                        searchSupport_ticket(request, response, filter);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    return;
                case "CLOSED_TICKET_COUNT_FILTER":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH)) {
                        String filter = " support_ticket.ticket_status_cat = " + Support_ticketDTO.CLOSED;                        
                        searchSupport_ticket(request, response, filter);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    return;
                case "view":
                    logger.debug("view requested");
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH)) {
                        commonRequestHandler.view(request, response);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    return;
                case "xl":
                	
    				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH))
    				{
    					getXl(request, response, userDTO);
    				}
    				else
    				{
    					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    				}
        			
                case "SOLVED_TICKET_COUNT_FILTER":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH)) {
                        String filter = " support_ticket.ticket_status_cat = " + Support_ticketDTO.SOLVED;                        
                        searchSupport_ticket(request, response, filter);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    return;
                case "ticketCloseCancel":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_MAIN)) {
                        response.sendRedirect("Support_ticketServlet?actionType=search");
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    return;
                default:
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }
    
	
    
    public void getXl(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
		Hashtable hashTable = null;
		hashTable  = commonRequestHandler.fillHashTable(hashTable, request);
		
		List <Support_ticketDTO> support_tiketDTOs = support_ticketDAO.getDTOs(hashTable, -1, -1, true, userDTO);
		
		String Language = LM.getLanguage(userDTO);
		
		getXl(support_tiketDTOs, response, Language);
		
	}
    
    public void getXl(List <Support_ticketDTO> support_tiketDTOs, HttpServletResponse response, String Language)
    {
    	XSSFWorkbook wb = new XSSFWorkbook();
    	boolean isLangEng = Language.equalsIgnoreCase("english");

        List<String> headers = new ArrayList<String>();
        if(isLangEng)
        {
        	headers.add("Ticket#");
            headers.add("Client/User");
            headers.add("Problem Details");
            headers.add("Problem Type");
            headers.add("Branch/Wing");
            headers.add("Priority");
            headers.add("Status");
            headers.add("Assigned To");
        }
        else
        {
        	headers.add("টিকেট#");
            headers.add("=ক্লায়েন্ট/ইউজার");
            headers.add("সমস্যার বিস্তারিত");
            headers.add("সমস্যার ধরণ");
            headers.add("শাখা");
            headers.add("গুরুত্ব");
            headers.add("অবস্থা");
            headers.add("দায়িত্বপ্রাপ্ত ব্যক্তি");
        }
       
        Sheet sheet = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");

        String tabName = "Support Tickets";
        
        sheet = wb.createSheet(tabName);
		Row headerRow = sheet.createRow(0);
		try {
			int cellIndex = 0;
			for(String header: headers)
			{
				sheet.setColumnWidth(cellIndex, 25 * 256);
				Cell cell = headerRow.createCell(cellIndex);
				cell.setCellValue(header);
				cell.setCellStyle(commonRequestHandler.getHeaderStyle(wb));
				
				cellIndex++;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int i = 1;
		CellStyle wrapStyle = wb.createCellStyle();
		wrapStyle.setWrapText(true);
		wrapStyle.setVerticalAlignment(VerticalAlignment.TOP);
		for(Support_ticketDTO support_ticketDTO: support_tiketDTOs)
		{
			Row row = sheet.createRow(i);
			
			int cellIndex = 0;
			
			Cell cell = row.createCell(cellIndex);
			cell.setCellStyle(wrapStyle);
			String value = Utils.getDigits(support_ticketDTO.iD, isLangEng);
			value += "\r\n";
			value += Utils.getDigits(simpleDateFormat.format(new Date(support_ticketDTO.insertionDate)), Language);
			cell.setCellValue(value);
			cellIndex ++;
			

			value = "";
			cell = row.createCell(cellIndex);
			cell.setCellStyle(wrapStyle);
			if (support_ticketDTO.roomNoCat == -1) 
			{
	            OrganogramDetails organogramDetails = WorkflowController.getOrganogramDetailsByOrganogramId(support_ticketDTO.issueRaiserOrganoramId, isLangEng);
	            value += organogramDetails.empName;
	            if(!organogramDetails.organogramName.equalsIgnoreCase(""))
	            {
	            	value += "\n" + organogramDetails.organogramName + ", " + organogramDetails.officeName;	            
	            }
	            value += "\n" + organogramDetails.empAddress;
	            value += "\n" +  organogramDetails.empMobileNo;
	            value += "\n" + organogramDetails.username;
			}
			else
			{
				value += isLangEng ? "Room" : "ঘর";
				value += CatRepository.getInstance().getText(isLangEng, "room_no", support_ticketDTO.roomNoCat);
			}
			cell.setCellValue(value);
			cellIndex ++;
			
			
			value = "";
			cell = row.createCell(cellIndex);
			cell.setCellStyle(wrapStyle);
			value = support_ticketDTO.description;
			cell.setCellValue(value);
			cellIndex ++;
			
			value = "";
			cell = row.createCell(cellIndex);
			cell.setCellStyle(wrapStyle);
			value = Ticket_issuesRepository.getInstance().getText(support_ticketDTO.ticketIssuesType,isLangEng);
			value+= "\n" + TicketIssueSubTypeRepository.getInstance().getText(support_ticketDTO.ticketIssuesSubtypeType,isLangEng);
			cell.setCellValue(value);
			cellIndex ++;
			
			value = "";
			cell = row.createCell(cellIndex);
			cell.setCellStyle(wrapStyle);
			value = WorkflowController.getWingNameFromOrganogramId(support_ticketDTO.issueRaiserOrganoramId, isLangEng);		
			cell.setCellValue(value);
			cellIndex ++;
			
			value = "";
			cell = row.createCell(cellIndex);
			cell.setCellStyle(wrapStyle);
			value = CatRepository.getInstance().getText(isLangEng, "priority", support_ticketDTO.priorityCat);		
			cell.setCellValue(value);
			cellIndex ++;
			
			value = "";
			cell = row.createCell(cellIndex);
			cell.setCellStyle(wrapStyle);
			value = CatRepository.getInstance().getText(isLangEng, "ticket_status", support_ticketDTO.ticketStatusCat);		
			cell.setCellValue(value);
			cellIndex ++;
			
			value = "";
			cell = row.createCell(cellIndex);
			cell.setCellStyle(wrapStyle);
			if (support_ticketDTO.currentAssignedOrganoramId != -1) 
			{
	            OrganogramDetails organogramDetails = WorkflowController.getOrganogramDetailsByOrganogramId(support_ticketDTO.currentAssignedOrganoramId,isLangEng);
	            value = organogramDetails.empName + "\n" + organogramDetails.username;
	            if (organogramDetails.roleID == SessionConstants.SUPPORT_ENGINEER_ROLE) 
	            {
	                value += "\n" + organogramDetails.empMobileNo;
	                
	            }
			}
			cell.setCellValue(value);
			cellIndex ++;


			i ++;
		}
		
		//autoSizeColumns(wb);
		
		try {
        	String fileName =  "SupportTickets.xlsx";
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            System.out.println("Writing xl");
			wb.write(response.getOutputStream());
			wb.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // Write workbook to
        
        
    }
    
 


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "add":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_ADD)) {
                        addSupport_ticket(request, response, true, userDTO);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "UploadFilesFromDropZone":
                    commonRequestHandler.UploadFilesFromDropZone(request, response, userDTO);
                    break;
                case "getUserAssets": {
                    long orgId = Long.parseLong(request.getParameter("orgId"));
                    long assetAssigneeId = -1;
                    if (!request.getParameter("assetAssigneeId").equalsIgnoreCase("")) {
                        assetAssigneeId = Long.parseLong(request.getParameter("assetAssigneeId"));
                    }
                    AssetAssigneeDAO assetAssigneeDAO = new AssetAssigneeDAO();
                    List<AssetAssigneeDTO> assetAssigneeDTOs = new ArrayList<>();
                    if (orgId != -1) {
                        assetAssigneeDTOs = (List<AssetAssigneeDTO>) assetAssigneeDAO.getDTOsByParent("assigned_organogram_id", orgId);
                    }

                    StringBuilder options = new StringBuilder("<option value = '-1'>" + LM.getText(LC.HM_SELECT, userDTO) + "</option>");
                    for (AssetAssigneeDTO assetAssigneeDTO : assetAssigneeDTOs) {
                        options.append("<option value = '").append(assetAssigneeDTO.iD).append("' ");
                        if (assetAssigneeDTO.iD == assetAssigneeId) {
                            options.append("selected");
                        }
                        options.append(">").append(assetAssigneeDAO.getName(assetAssigneeDTO)).append("</option>");
                    }
                    response.getWriter().write(options.toString());
                    break;
                }
                case "getSubType": {
                    long type = Long.parseLong(request.getParameter("type"));
                    String language = request.getParameter("language");
                    response.getWriter().write(PBNameRepository.getInstance().getOptionsWithExtraColumnVal(language, "ticket_issues_subtype", type));
                    break;
                }
                case "getDTO":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_ADD)) {
                        getDTO(request, response);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }

                    break;
                case "edit":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_UPDATE)) {
                        addSupport_ticket(request, response, false, userDTO);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "delete":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_UPDATE)) {
                        commonRequestHandler.delete(request, response, userDTO);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "search":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH)) {
                        searchSupport_ticket(request, response, "");
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "getGeo":
                    request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
                    break;
               
                case "forwardTicket":
                    logger.debug("ticketForward requested");
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_UPDATE)) {
                        forwardTicket(request, response, userDTO);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "addComment":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_UPDATE)) {
                        addComment(request, response, userDTO);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }


    private void updateExpertise(HttpServletRequest request, Support_ticketDTO support_ticketDTO) throws Exception {
        String issueRaiserName = WorkflowController.getUserNameFromOrganogramId(support_ticketDTO.issueRaiserOrganoramId);
        long issueRaiserUserId = WorkflowController.getUserIDFromOrganogramId(support_ticketDTO.issueRaiserOrganoramId);

        int expertise = Integer.parseInt(request.getParameter("expertise"));
        String expertiseRemarks = Jsoup.clean(request.getParameter("expertiseRemarks"), Whitelist.simpleText());

        User_expertise_mapDAO user_expertise_mapDAO = new User_expertise_mapDAO();
        User_expertise_mapDTO user_expertise_mapDTO = user_expertise_mapDAO.getDTOByUserId(issueRaiserUserId);

        boolean dtoFound = true;
        if (user_expertise_mapDTO == null) {
            dtoFound = false;
            user_expertise_mapDTO = new User_expertise_mapDTO();
            user_expertise_mapDTO.userName = issueRaiserName;
            user_expertise_mapDTO.userId = issueRaiserUserId;
        }

        user_expertise_mapDTO.remarks = expertiseRemarks;
        user_expertise_mapDTO.userExpertiseCat = expertise;

        if (dtoFound) {
            user_expertise_mapDAO.update(user_expertise_mapDTO);
        } else {
            user_expertise_mapDAO.add(user_expertise_mapDTO);
        }
    }

    private void addComment(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
        long id = Long.parseLong(request.getParameter("id"));
        String remarks = Jsoup.clean(request.getParameter("remarks"), Whitelist.simpleText());
        Support_ticketDTO support_ticketDTO = support_ticketDAO.getDTOByID(id);
        try {
            addToTicketHistory(support_ticketDTO, userDTO, null, remarks, -1, userDTO, Ticket_forward_historyDTO.ADD_COMMENTS);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        String URL = "Support_ticketServlet?actionType=view&ID=" + id;
		try {
			String englishText = WorkflowController.getNameFromEmployeeRecordID(userDTO.employee_record_id, true) + " commented on ticket #" + id;
			String banglaText = WorkflowController.getNameFromEmployeeRecordID(userDTO.employee_record_id, false) + " #" 
			+ Utils.getDigits(id, "bangla") + " টিকেটটিতে মন্তব্য করেছেন";

			englishText += ": " + remarks;
			banglaText += ": " + remarks;
		    pb_notificationsDAO.addPb_notificationsAndSendMailSMS(support_ticketDTO.currentAssignedOrganoramId,
		                System.currentTimeMillis(), URL, englishText, banglaText, "Ticket Action");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
       
        try {
            response.sendRedirect("Support_ticketServlet?actionType=view&ID=" + id);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private void getDueDate(HttpServletRequest request, Support_ticketDTO support_ticketDTO)
    {
    	 String dueDate = request.getParameter("dueDate");
         SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");


         if (dueDate != null) {
             dueDate = Jsoup.clean(dueDate, Whitelist.simpleText());
         }
         logger.debug("dueDate = " + dueDate);
         if (dueDate != null && !dueDate.equalsIgnoreCase("")) {

             try {
                 Date d = f.parse(dueDate);
                 support_ticketDTO.dueDate = d.getTime();

             } catch (Exception e) {
                 e.printStackTrace();
             }
         } else {
             logger.debug("FieldName has a null Value, not updating" + " = " + dueDate);
         }

    }
    
    private void getDueTime(HttpServletRequest request, Support_ticketDTO support_ticketDTO)
    {
    	 String dueTime = request.getParameter("dueTime");
         SimpleDateFormat f = new SimpleDateFormat("hh:mm aa");


         if (dueTime != null) {
        	 dueTime = Jsoup.clean(dueTime, Whitelist.simpleText());
         }
         logger.debug("dueTime = " + dueTime);
         if (dueTime != null && !dueTime.equalsIgnoreCase("")) {

             try {
                 Date d = f.parse(dueTime);
                 support_ticketDTO.dueTime = d.getTime();

             } catch (Exception e) {
                 e.printStackTrace();
             }
         } else {
             logger.debug("FieldName has a null Value, not updating" + " = " + dueTime);
         }

    }
    
    private void forwardNormalTicket(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, Support_ticketDTO support_ticketDTO) throws Exception
    {
    	support_ticketDTO.ticketStatusCat = Integer.parseInt(request.getParameter("ticketStatusCat"));
        String remarks = Jsoup.clean(request.getParameter("remarks"), Whitelist.simpleText());
        long filesDropzone = Long.parseLong(request.getParameter("filesDropzone"));
        
        
        if(userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE || userDTO.roleID == SessionConstants.ADMIN_ROLE)
        {
        	int forwardActionCat = Integer.parseInt(request.getParameter("forwardActionCat"));
        	if (support_ticketDTO.ticketStatusCat == Support_ticketDTO.CLOSED) 
        	{
                support_ticketDTO.closedById = userDTO.organogramID;
                support_ticketDTO.closingDate = System.currentTimeMillis();
                support_ticketDTO.closingComments = remarks;
                support_ticketDTO.currentAssignedOrganoramId = support_ticketDTO.issueRaiserOrganoramId;
                support_ticketDTO.currentAssignedUserId = support_ticketDTO.issueRaiserUserId;
                support_ticketDAO.update(support_ticketDTO);
            	support_ticketDAO.addTicketNoti(support_ticketDTO);
            	UserDTO toUserDTO = UserRepository.getUserDTOByOrganogramID(support_ticketDTO.currentAssignedOrganoramId);
            	addToTicketHistory(support_ticketDTO, userDTO, toUserDTO, remarks, filesDropzone, userDTO, forwardActionCat);
                try {
                    updateExpertise(request, support_ticketDTO);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        	else // ticket is open
        	{
        		support_ticketDTO.priorityCat = Integer.parseInt(request.getParameter("priorityCat"));
        		
        		String Value = request.getParameter("ticketIssuesType");

                if (Value != null && !Value.equalsIgnoreCase("")) {
                    Value = Jsoup.clean(Value, Whitelist.simpleText());
                }
                logger.debug("ticketIssuesType = " + Value);
                if (Value != null && !Value.equalsIgnoreCase("")) {
                    support_ticketDTO.ticketIssuesType = Long.parseLong(Value);

                } else {
                    logger.debug("FieldName has a null Value, not updating" + " = " + Value);
                }

                Value = request.getParameter("ticketIssuesSubtypeType");

                if (Value != null && !Value.equalsIgnoreCase("")) {
                    Value = Jsoup.clean(Value, Whitelist.simpleText());
                }
                logger.debug("ticketIssuesSubtypeType = " + Value);
                if (Value != null && !Value.equalsIgnoreCase("")) {
                    support_ticketDTO.ticketIssuesSubtypeType = Long.parseLong(Value);
                } else {
                    logger.debug("FieldName has a null Value, not updating" + " = " + Value);
                }
                
                getDueDate(request, support_ticketDTO);
                getDueTime(request, support_ticketDTO);
                
                if(forwardActionCat == Ticket_forward_historyDTO.FORWARD)
                {
                	support_ticketDTO.currentAssignedOrganoramId = Long.parseLong(request.getParameter("orgId"));
                	support_ticketDAO.update(support_ticketDTO);
                	support_ticketDAO.addTicketNoti(support_ticketDTO);
                	UserDTO toUserDTO = UserRepository.getUserDTOByOrganogramID(support_ticketDTO.currentAssignedOrganoramId);
                	addToTicketHistory(support_ticketDTO, userDTO, toUserDTO, remarks, filesDropzone, userDTO, forwardActionCat);
                }
                else //Ticket Assign
                {
                	String [] supportIds = request.getParameterValues("orgId");
                	if(supportIds != null)
                	{
                		support_ticketDTO.supportOrgsArray = new ArrayList<Long>();
                		support_ticketDTO.supportEidsArray = new ArrayList<Long>();                		
                		support_ticketDTO.origSupportOrgsArray = new ArrayList<Long>();
                		List<UserDTO> toUserDTOs = new ArrayList<UserDTO>();
                		for(String supportId: supportIds)
                		{
                			long org = Long.parseLong(supportId);
                			if(org != -1)
                			{
	                			support_ticketDTO.supportOrgsArray.add(org);
	                			support_ticketDTO.origSupportOrgsArray.add(org);
	                			support_ticketDTO.supportEidsArray.add(WorkflowController.getEmployeeRecordIDFromOrganogramID(org));
	                			toUserDTOs.add(UserRepository.getUserDTOByOrganogramID(org));
                			}
                		}
                		support_ticketDAO.update(support_ticketDTO);
                    	support_ticketDAO.addTicketNoti(support_ticketDTO, true);
                    	addToTicketHistoryMultiple(support_ticketDTO, userDTO, toUserDTOs, remarks, filesDropzone, userDTO, forwardActionCat, -1);
                	}
                	
                }
        	}
        	
        }
        else if (userDTO.roleID == SessionConstants.SUPPORT_ENGINEER_ROLE)
        {
        	
        	int forwardActionCat = Ticket_forward_historyDTO.SOLVE_TICKET;
        	int solvedJob = -1;
        	if(request.getParameter("solvedJob") != null)
        	{
        		solvedJob = Integer.parseInt(request.getParameter("solvedJob"));
        	}
        	support_ticketDTO.supportOrgsArray.remove(userDTO.organogramID);
        	if(support_ticketDTO.supportOrgsArray.isEmpty())
        	{
        		support_ticketDTO.ticketStatusCat = Support_ticketDTO.SOLVED;
        	}
        	support_ticketDAO.update(support_ticketDTO);
        	if(support_ticketDTO.supportOrgsArray.isEmpty())
        	{
        		support_ticketDAO.addTicketNoti(support_ticketDTO, false);
        	}
        	addToTicketHistoryMultiple(support_ticketDTO, userDTO, null, remarks, filesDropzone, userDTO, forwardActionCat, solvedJob);
        }
    }
    
    /*private void forwardNormalTicket2(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, Support_ticketDTO support_ticketDTO) //legacy code, may be removed later
    {
    	long id = Long.parseLong(request.getParameter("id"));


        int ticketStatusCat = Integer.parseInt(request.getParameter("ticketStatusCat"));
        String remarks = Jsoup.clean(request.getParameter("remarks"), Whitelist.simpleText());
        long filesDropzone = Long.parseLong(request.getParameter("filesDropzone"));


        
        support_ticketDTO.ticketStatusCat = ticketStatusCat;

        if (request.getParameter("priorityCat") != null && userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE) {
            support_ticketDTO.priorityCat = Integer.parseInt(request.getParameter("priorityCat"));
        }

        if (support_ticketDTO.ticketStatusCat == Support_ticketDTO.CLOSED) {
            support_ticketDTO.closedById = userDTO.organogramID;
            support_ticketDTO.closingDate = System.currentTimeMillis();
            support_ticketDTO.closingComments = remarks;
            support_ticketDTO.currentAssignedOrganoramId = -1;
            support_ticketDTO.currentAssignedUserId = -1;
            String URL = "Support_ticketServlet?actionType=view&ID=" + id;
            String englishText = "Your ticket #" + id + " is solved.";
            String banglaText = "আপনার টিকেট  #" + Utils.getDigits(id, "bangla") + " নিষ্পত্তি হয়েছে।";

            pb_notificationsDAO.addPb_notificationsAndSendMailSMS(support_ticketDTO.issueRaiserOrganoramId, System.currentTimeMillis(), URL, englishText, banglaText, "TicketResolved");

            try {
                updateExpertise(request, support_ticketDTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (support_ticketDTO.ticketStatusCat == Support_ticketDTO.SOLVED) {
                Ticket_forward_historyDTO lastHistory = ticket_forward_historyDAO.getLastAssignedDTOBySupportTicketId(support_ticketDTO.iD);
                if (lastHistory != null) {
                    support_ticketDTO.currentAssignedOrganoramId = lastHistory.forwardFromOrganogramId;
                    support_ticketDTO.currentAssignedUserId = lastHistory.forwardFromUserId;
                }
            } else {
                support_ticketDTO.currentAssignedOrganoramId = Long.parseLong(request.getParameter("orgId"));
            }
            String URL = "Support_ticketServlet?actionType=view&ID=" + id;
            String englishText = "A ticket #" + id + " is waiting for your action.";
            String banglaText = "একটি টিকেট #" + Utils.getDigits(id, "bangla") + " আপনার পদক্ষেপের অপেক্ষায় আছে।";

            
            pb_notificationsDAO.addPb_notificationsAndSendMailSMS(support_ticketDTO.currentAssignedOrganoramId, System.currentTimeMillis(), URL,
            		englishText, banglaText, "Ticket Action", "");
         
            
        }

        if (userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE
                || userDTO.roleID == SessionConstants.ADMIN_ROLE) {

            String Value = request.getParameter("ticketIssuesType");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("ticketIssuesType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                support_ticketDTO.ticketIssuesType = Long.parseLong(Value);

            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("ticketIssuesSubtypeType");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("ticketIssuesSubtypeType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                support_ticketDTO.ticketIssuesSubtypeType = Long.parseLong(Value);
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }
        }

        getDueDate(request, support_ticketDTO);

        try {
            support_ticketDAO.update(support_ticketDTO);
        } catch (Exception e2) {
            e2.printStackTrace();
        }


        try {
            UserDTO toUserDTO = UserRepository.getUserDTOByOrganogramID(support_ticketDTO.currentAssignedOrganoramId);
            int forwardActionCat = Ticket_forward_historyDTO.TICKET_ASSIGN;
            if (request.getParameter("forwardActionCat") != null) {
                forwardActionCat = Integer.parseInt(request.getParameter("forwardActionCat"));
            }
            if (support_ticketDTO.ticketStatusCat == Support_ticketDTO.CLOSED) {
                toUserDTO = null;
                forwardActionCat = Ticket_forward_historyDTO.CLOSE;
            }
            addToTicketHistory(support_ticketDTO, userDTO, toUserDTO, remarks, filesDropzone, userDTO, forwardActionCat);


        } catch (Exception e1) {
            e1.printStackTrace();
        }
        
    }*/
    
    private void forwardNOC(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, Support_ticketDTO support_ticketDTO) throws Exception
    {


    	if(support_ticketDTO.nocStatusCat == Asset_clearance_letterDTO.PENDING)
    	{
    		if (request.getParameter("priorityCat") != null && userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE) {
                support_ticketDTO.priorityCat = Integer.parseInt(request.getParameter("priorityCat"));
            }   		
    		getDueDate(request, support_ticketDTO);            
            support_ticketDTO.remarks = Jsoup.clean(request.getParameter("remarks"), Whitelist.simpleText());
            support_ticketDTO.nocStatusCat = Asset_clearance_letterDTO.NOC_VERIFICATION_STARTED;
            
            support_ticketDTO.supportOrgsArray = new ArrayList<Long>();
    		support_ticketDTO.supportEidsArray = new ArrayList<Long>();
    		support_ticketDTO.origSupportOrgsArray = new ArrayList<Long>();
    		long orgId = Long.parseLong(request.getParameter("orgId"));
    		support_ticketDTO.supportOrgsArray.add(orgId);
    		support_ticketDTO.origSupportOrgsArray.add(orgId);
    		support_ticketDTO.supportEidsArray.add(WorkflowController.getEmployeeRecordIDFromOrganogramID(orgId));
    		
            support_ticketDTO.forwardActionCat = Ticket_forward_historyDTO.TICKET_ASSIGN;
    	}
    	else if(support_ticketDTO.nocStatusCat == Asset_clearance_letterDTO.NOC_VERIFICATION_STARTED)
    	{
    		support_ticketDTO.remarks = Jsoup.clean(request.getParameter("remarks"), Whitelist.simpleText());
    		support_ticketDTO.nocClearanceRemarks = support_ticketDTO.remarks;
    		
    		support_ticketDTO.supportOrgsArray.remove(userDTO.organogramID);
    		support_ticketDTO.forwardActionCat = Ticket_forward_historyDTO.SOLVE_TICKET;
    		support_ticketDTO.nocStatusCat = Asset_clearance_letterDTO.NOC_VERIFIED;
    		support_ticketDTO.ticketStatusCat = Support_ticketDTO.SOLVED;
    		
    		List<Asset_clearance_statusDTO> asset_clearance_statusDTOs = Asset_clearance_statusDAO.getInstance().getByTicketId(support_ticketDTO.iD);
        	if(asset_clearance_statusDTOs != null)
        	{
        		int i = 0;
        		for(Asset_clearance_statusDTO asset_clearance_statusDTO: asset_clearance_statusDTOs)
        		{
        			asset_clearance_statusDTO.assetClearanceStatusCat = Integer.parseInt(request.getParameterValues("assetClearanceStatusCat")[i]);
        			asset_clearance_statusDTO.clearingRemarks = (request.getParameterValues("clearingRemarks")[i]);
        			try {
						Asset_clearance_statusDAO.getInstance().update(asset_clearance_statusDTO);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
        			i++;
        		}
        	}
    	}   	
    	else if(support_ticketDTO.nocStatusCat == Asset_clearance_letterDTO.NOC_VERIFIED)
    	{
    		support_ticketDTO.remarks = Jsoup.clean(request.getParameter("remarks"), Whitelist.simpleText());
			if (request.getParameter("forwardActionCat") != null) 
			{
				support_ticketDTO.forwardActionCat = Integer.parseInt(request.getParameter("forwardActionCat"));
			}
			if(support_ticketDTO.forwardActionCat != Ticket_forward_historyDTO.FORWARD && 
					support_ticketDTO.forwardActionCat != Ticket_forward_historyDTO.NOC_APPROVAL &&
					support_ticketDTO.forwardActionCat != Ticket_forward_historyDTO.NOC_REJECTION)
			{
				return;
			}
			
			if(support_ticketDTO.forwardActionCat == Ticket_forward_historyDTO.FORWARD)
			{
				support_ticketDTO.currentAssignedOrganoramId = Long.parseLong(request.getParameter("orgId"));
	            support_ticketDTO.currentAssignedUserId = WorkflowController.getUserIdFromOrganogramId(support_ticketDTO.currentAssignedOrganoramId);
	            support_ticketDTO.forwardActionCat = Ticket_forward_historyDTO.TICKET_ASSIGN;
			}
			else
			{
				support_ticketDTO.currentAssignedOrganoramId = support_ticketDTO.issueRaiserOrganoramId;
				support_ticketDTO.currentAssignedUserId = support_ticketDTO.issueRaiserUserId;
				support_ticketDTO.ticketStatusCat = Support_ticketDTO.CLOSED;
				if(support_ticketDTO.forwardActionCat == Ticket_forward_historyDTO.NOC_APPROVAL)
				{
					support_ticketDTO.nocStatusCat = Asset_clearance_letterDTO.NOC_APPROVED;
				}
				else
				{
					support_ticketDTO.nocStatusCat = Asset_clearance_letterDTO.NOC_REJECTED;
				}
				support_ticketDTO.nocApprovalRemarks = support_ticketDTO.remarks;
			}
    		
    	}
    	
    	try 
    	{
    		Utils.handleTransaction(() -> {
				support_ticketDAO.update(support_ticketDTO);
				UserDTO toUserDTO = null;
				if(support_ticketDTO.forwardActionCat == Ticket_forward_historyDTO.TICKET_ASSIGN)
				{
					toUserDTO = UserRepository.getUserDTOByOrganogramID(support_ticketDTO.supportOrgsArray.get(0));
					support_ticketDAO.addTicketNoti(support_ticketDTO, true);
				}
				else
				{
					toUserDTO = UserRepository.getUserDTOByOrganogramID(support_ticketDTO.currentAssignedOrganoramId);
					support_ticketDAO.addTicketNoti(support_ticketDTO);
				}
				
				addToTicketHistory(support_ticketDTO, userDTO, toUserDTO, support_ticketDTO.remarks, -1, userDTO, 
						support_ticketDTO.forwardActionCat);
				
				Asset_clearance_letterDTO asset_clearance_letterDTO = asset_clearance_letterDAO.getBySupportTicketId(support_ticketDTO.iD);
				asset_clearance_letterDTO.clearanceStatusCat = support_ticketDTO.nocStatusCat;
				
				if(support_ticketDTO.forwardActionCat == Ticket_forward_historyDTO.NOC_APPROVAL || support_ticketDTO.forwardActionCat == Ticket_forward_historyDTO.NOC_REJECTION)
				{
					asset_clearance_letterDTO.signingOrganogramId = userDTO.organogramID;
					asset_clearance_letterDTO.responseDate = System.currentTimeMillis();
				}
				asset_clearance_letterDAO.update(asset_clearance_letterDTO);
    		});
    		
    		
			
		}
    	catch (Exception e) 
    	{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    private void forwardTicket(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception {
        long id = Long.parseLong(request.getParameter("id"));
        Support_ticketDTO support_ticketDTO = support_ticketDAO.getDTOByID(id);
        
        String sForwardAction = request.getParameter("forwardActionCat");
    	if(sForwardAction != null)
    	{
    		support_ticketDTO.forwardActionCat = Integer.parseInt(request.getParameter("forwardActionCat"));
    		if(support_ticketDTO.forwardActionCat == Ticket_forward_historyDTO.DELETE)
    		{
    			Asset_clearance_letterDTO asset_clearance_letterDTO = asset_clearance_letterDAO.getBySupportTicketId(support_ticketDTO.iD);
    			if(asset_clearance_letterDTO != null)
    			{
    				asset_clearance_letterDAO.delete(asset_clearance_letterDTO.iD);
    			}
    			support_ticketDAO.delete(support_ticketDTO.iD);
    			try 
    	        {
    	            response.sendRedirect("Support_ticketServlet?actionType=search");
    	        }
    	        catch (IOException e) 
    	        {
    	            e.printStackTrace();
    	        }
    			return;
    			
    		}
    	}
        
        if(support_ticketDTO.ticketIssuesType == Ticket_issuesDTO.NOC_ISSUE)
        {
        	forwardNOC(request, response, userDTO, support_ticketDTO);
        }
        else
        {
        	forwardNormalTicket(request, response, userDTO, support_ticketDTO);
        }

        try 
        {
            response.sendRedirect("Support_ticketServlet?actionType=view&ID=" + support_ticketDTO.iD);
        }
        catch (IOException e) 
        {
            e.printStackTrace();
        }

    }

    private void addToTicketHistory(Support_ticketDTO support_ticketDTO, UserDTO fromUserDTO, UserDTO toUserDTO, String forwardComments,
                                    long filesDropzone, UserDTO userDTO, int forwardActionCat) throws IOException {
        try {

            Ticket_forward_historyDTO ticket_forward_historyDTO = new Ticket_forward_historyDTO ( support_ticketDTO,  fromUserDTO,  toUserDTO,  forwardComments,
                     filesDropzone,  userDTO,  forwardActionCat, -1);

            ticket_forward_historyDAO.add(ticket_forward_historyDTO);
            
            if(forwardActionCat == Ticket_forward_historyDTO.TICKET_ASSIGN 
            		&& support_ticketDTO.origSupportOrgsArray != null && !support_ticketDTO.origSupportOrgsArray.isEmpty())//NOC assign, no job
            {
            	Support_engineers_diaryDAO.getInstance().deleteChildrenByParent(support_ticketDTO.iD, "support_ticket_id");
            	Support_engineers_diaryDTO support_engineers_diaryDTO = new Support_engineers_diaryDTO(support_ticketDTO, support_ticketDTO.origSupportOrgsArray.get(0), ticket_forward_historyDTO, -1);
            	Support_engineers_diaryDAO.getInstance().add(support_engineers_diaryDTO);
            }
            

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
	private void addToTicketHistoryMultiple(Support_ticketDTO support_ticketDTO, UserDTO fromUserDTO, List<UserDTO> toUserDTOs,
			String forwardComments, long filesDropzone, UserDTO userDTO, int forwardActionCat, int solvedJob) throws IOException {
		try {

			Ticket_forward_historyDTO ticket_forward_historyDTO = new Ticket_forward_historyDTO(support_ticketDTO,
					fromUserDTO, toUserDTOs, forwardComments, filesDropzone, userDTO, forwardActionCat, solvedJob);

			ticket_forward_historyDAO.add(ticket_forward_historyDTO);
			
			if(forwardActionCat == Ticket_forward_historyDTO.TICKET_ASSIGN)
            {
				int i = 0;
				Support_engineers_diaryDAO.getInstance().deleteChildrenByParent(support_ticketDTO.iD, "support_ticket_id");
				for(long supportOrg: support_ticketDTO.origSupportOrgsArray)
				{
					int job = -1;
					if(support_ticketDTO.supportJobsArray != null && support_ticketDTO.supportJobsArray.size() > i)
					{
						job = support_ticketDTO.supportJobsArray.get(i).intValue();
					}
					Support_engineers_diaryDTO support_engineers_diaryDTO = new Support_engineers_diaryDTO(support_ticketDTO, supportOrg, ticket_forward_historyDTO, job);
	            	Support_engineers_diaryDAO.getInstance().add(support_engineers_diaryDTO);
	            	i++;
				}            	
            }
			else if(forwardActionCat == Ticket_forward_historyDTO.SOLVE_TICKET)
            {
				Support_engineers_diaryDTO support_engineers_diaryDTO = Support_engineers_diaryDAO.getInstance().getBySupportManJobEid(support_ticketDTO.iD, fromUserDTO.employee_record_id, solvedJob);
				if(support_engineers_diaryDTO != null)
				{
					support_engineers_diaryDTO.doneTime = System.currentTimeMillis();
					Support_engineers_diaryDAO.getInstance().update(support_engineers_diaryDTO);
				}
            }

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


    private void getDTO(HttpServletRequest request, HttpServletResponse response) {
        try {
            Support_ticketDTO support_ticketDTO = support_ticketDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            String encoded = this.gson.toJson(support_ticketDTO);
            out.print(encoded);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addSupport_ticket(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO) {
        try {
            Support_ticketDTO support_ticketDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
            if (addFlag) {
                support_ticketDTO = new Support_ticketDTO();
            } else {
                support_ticketDTO = support_ticketDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
            }

            String Value;

            Value = request.getParameter("departmentDatetimeId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("departmentDatetimeId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                support_ticketDTO.departmentDatetimeId = Long.parseLong(Value);
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("ticketIssuesType");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("ticketIssuesType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                support_ticketDTO.ticketIssuesType = Long.parseLong(Value);
                Ticket_issuesDAO ticket_issuesDAO = Ticket_issuesDAO.getInstance();
                Ticket_issuesDTO ticket_issuesDTO = ticket_issuesDAO.getDTOFromID(support_ticketDTO.ticketIssuesType);

                support_ticketDTO.currentAssignedOrganoramId = ticket_issuesDTO.adminOrganogramId;
                support_ticketDTO.currentAssignedUserId = UserRepository.getUserDTOByOrganogramID(support_ticketDTO.currentAssignedOrganoramId).ID;

            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("ticketIssuesSubtypeType");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("ticketIssuesSubtypeType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                support_ticketDTO.ticketIssuesSubtypeType = Long.parseLong(Value);
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("description");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("description = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                support_ticketDTO.description = (Value);
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("issueText");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("issueText = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                support_ticketDTO.issueText = (Value);
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }
            
            support_ticketDTO.supportJobsArray = new ArrayList<Long> ();
            String[] eses = request.getParameterValues("es");
            if(eses != null)
            {
            	for(String sEs: eses)
            	{
            		support_ticketDTO.supportJobsArray.add(Long.parseLong(sEs));
            	}
            }


            Value = request.getParameter("issueRaiserOrganoramId");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("issueRaiserOrganoramId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                support_ticketDTO.issueRaiserOrganoramId = Long.parseLong(Value);
                if (addFlag) {
                    Support_ticketDTO lastDTO = support_ticketDAO.getLastDTO(support_ticketDTO.issueRaiserOrganoramId);
                    UserDTO issueRaiserDTO = UserRepository.getUserDTOByOrganogramID(support_ticketDTO.issueRaiserOrganoramId);
                    if (issueRaiserDTO != null) {
                        support_ticketDTO.issueRaiserUserId = issueRaiserDTO.ID;
                        support_ticketDTO.issueRaiserDept = issueRaiserDTO.unitID;

                        Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(support_ticketDTO.issueRaiserDept);
                        if (office_unitsDTO != null) {
                            support_ticketDTO.parentUnit = office_unitsDTO.parentUnitId;
                            support_ticketDTO.iruEn = office_unitsDTO.unitNameEng;
                            support_ticketDTO.iruBn = office_unitsDTO.unitNameBng;

                            Office_unitsDTO parentUnitDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(support_ticketDTO.parentUnit);

                            if (parentUnitDTO != null) {
                                support_ticketDTO.puEn = parentUnitDTO.unitNameEng;
                                support_ticketDTO.puBn = parentUnitDTO.unitNameBng;
                            }
                        }
                        WingModel wingModel = WorkflowController.getWingModelFromOrganogramId(support_ticketDTO.issueRaiserOrganoramId);
                        support_ticketDTO.wingId = wingModel.id;
                        support_ticketDTO.wingEn = wingModel.nameEn;
                        support_ticketDTO.wingBn = wingModel.nameBn;
                    }
                    if (lastDTO != null) {
                        support_ticketDTO.lastDTOId = lastDTO.closedById;
                        support_ticketDTO.lastDTOText = f.format(new Date(lastDTO.insertionDate));
                    }
                }
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

            if (!support_ticketDAO.canRaiseIssueForThisUser(userDTO, support_ticketDTO)) {
                response.sendRedirect("Support_ticketServlet?actionType=search");
                return;
            }


            Value = request.getParameter("ticketStatusCat");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("ticketStatusCat = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                support_ticketDTO.ticketStatusCat = Integer.parseInt(Value);
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("filesDropzone");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("filesDropzone = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                logger.debug("filesDropzone = " + Value);

                support_ticketDTO.filesDropzone = Long.parseLong(Value);


                if (!addFlag) {
                    String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                    String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                    for (int i = 0; i < deleteArray.length; i++) {
                        if (i > 0) {
                            filesDAO.delete(Long.parseLong(deleteArray[i]));
                        }
                    }
                }
            }

            String[] assetAssignees = request.getParameterValues("assetAssigneeId");
            if (assetAssignees != null && assetAssignees.length > 0) {
                support_ticketDTO.assetAssigneeId = Arrays.stream(assetAssignees)
                        .map(assetAssignee -> Jsoup.clean(assetAssignee, Whitelist.simpleText()))
                        .collect(Collectors.joining(","));
            }


            if (addFlag) {
                support_ticketDTO.insertedByUserId = userDTO.ID;
            }


            if (addFlag) {
                support_ticketDTO.insertedByOrganogramId = userDTO.organogramID;
            }


            if (addFlag) {

                support_ticketDTO.insertionDate = System.currentTimeMillis();
            }


            Value = request.getParameter("modifiedBy");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("modifiedBy = " + Value);
            if (Value != null) {
                support_ticketDTO.modifiedBy = (Value);
            } else {
                logger.debug("FieldName has a null Value, not updating");
            }

            Value = request.getParameter("roomNoCat");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("roomNoCat = " + Value);
            if (Value != null && Value.trim().length() >0) {
                support_ticketDTO.roomNoCat = Integer.parseInt(Value);
                if(support_ticketDTO.roomNoCat != -1)
                {
                	support_ticketDTO.issueRaiserOrganoramId = SessionConstants.ROOM_OFFSET + support_ticketDTO.roomNoCat;
                }
            } else {
                logger.debug("FieldName has a null Value, not updating");
            }

            Value = request.getParameter("officeBuildingId");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("officeBuildingId = " + Value);
            if (Value != null) {
                support_ticketDTO.officeBuildingId = Long.parseLong(Value);
            } else {
                logger.debug("FieldName has a null Value, not updating");
            }
            

            long returnedID;
            if (addFlag) {
                returnedID = support_ticketDAO.add(support_ticketDTO);
            } else {
                returnedID = support_ticketDAO.update(support_ticketDTO);
            }


            String URL = "Support_ticketServlet?actionType=view&ID=" + returnedID;
            String englishText = "A ticket #" + returnedID + " is waiting for your action.";
            String banglaText = "একটি টিকেট #" + Utils.getDigits(returnedID, "bangla") + " আপনার পদক্ষেপের অপেক্ষায় আছে।";
           
            pb_notificationsDAO.addPb_notificationsAndSendMailSMS(support_ticketDTO.currentAssignedOrganoramId,
                    System.currentTimeMillis(), URL, englishText, banglaText, "Ticket Action", SessionConstants.TICKET_ADMIN_ROLE);

            if(support_ticketDTO.roomNoCat == -1)
            {
            	addToTicketHistory(support_ticketDTO, UserRepository.getUserDTOByOrganogramID(support_ticketDTO.issueRaiserOrganoramId),
                        UserRepository.getUserDTOByOrganogramID(support_ticketDTO.currentAssignedOrganoramId),
                        support_ticketDTO.description, support_ticketDTO.filesDropzone, userDTO, Ticket_forward_historyDTO.TICKET_ASSIGN);
            }
            else
            {
            	addToTicketHistory(support_ticketDTO, userDTO,
                        UserRepository.getUserDTOByOrganogramID(support_ticketDTO.currentAssignedOrganoramId),
                        support_ticketDTO.description, support_ticketDTO.filesDropzone, userDTO, Ticket_forward_historyDTO.TICKET_ASSIGN);
            }
            
            response.sendRedirect("Support_ticketServlet?actionType=view&ID=" + support_ticketDTO.iD);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void searchSupport_ticket(HttpServletRequest request, HttpServletResponse response, String filter) throws ServletException, IOException {
        LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_SUPPORT_TICKET,
                request,
                support_ticketDAO,
                SessionConstants.VIEW_SUPPORT_TICKET,
                SessionConstants.SEARCH_SUPPORT_TICKET,
                support_ticketDAO.tableName,
                true,
                userDTO,
                filter,
                true);
        try {
            rnManager.doJob(loginDTO, "20");
        } catch (Exception e) {
            e.printStackTrace();
        }

        request.setAttribute("support_ticketDAO", support_ticketDAO);
        RequestDispatcher rd;
        if (!hasAjax) {
            logger.debug("Going to support_ticket/support_ticketSearch.jsp");
            rd = request.getRequestDispatcher("support_ticket/support_ticketSearch.jsp");
        } else {
            logger.debug("Going to support_ticket/support_ticketSearchForm.jsp");
            rd = request.getRequestDispatcher("support_ticket/support_ticketSearchForm.jsp");
        }
        rd.forward(request, response);
    }
}
