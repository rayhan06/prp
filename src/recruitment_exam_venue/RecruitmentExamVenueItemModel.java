package recruitment_exam_venue;

import recruitment_seat_plan.RecruitmentSeatPlanChildDTO;

public class RecruitmentExamVenueItemModel {
    public String iD = "";
    public String recruitmentExamVenueItemId = "";
    public String building = "";
    public String floor = "";
    public String roomNo = "";
    public String capacity = "";
    public String startRoll = "";
    public String endRoll = "";
    public String total = "";
    public boolean isRange = false;



    public RecruitmentExamVenueItemModel(){ }
    public RecruitmentExamVenueItemModel(RecruitmentExamVenueItemDTO dto, String language){
        boolean isLanguageEnglish = "English".equalsIgnoreCase(language);

        //iD = String.valueOf(dto.iD);
        recruitmentExamVenueItemId = String.valueOf(dto.iD);
        building = dto.building;
        floor = dto.floor;
        roomNo = dto.roomNo;
        capacity = dto.capacity;

    }

    public RecruitmentExamVenueItemModel(RecruitmentSeatPlanChildDTO dto,
                                         String language){
        boolean isLanguageEnglish = "English".equalsIgnoreCase(language);

        iD = String.valueOf(dto.iD);
        recruitmentExamVenueItemId = String.valueOf(dto.recruitmentExamVenueItemId);
        building = dto.building;
        floor = dto.floor;
        roomNo = dto.roomNo;
        capacity = dto.capacity;
        startRoll = dto.startRoll;
        endRoll = dto.endRoll;
        total = dto.total;
        isRange = dto.isRangeAutoCount;

    }

    @Override
    public String toString() {
        return "PiProductModel{" +
                "iD='" + iD + '\'' +
                "recruitmentExamVenueItemId='" + recruitmentExamVenueItemId + '\'' +
                ", building='" + building + '\'' +
                ", floor='" + floor + '\'' +
                ", roomNo='" + roomNo + '\'' +
                ", capacity='" + capacity + '\'' +
                ", startRoll='" + startRoll + '\'' +
                ", endRoll='" + endRoll + '\'' +
                ", total='" + total + '\'' +
                ", isRange='" + isRange + '\'' +
                '}';
    }
}
