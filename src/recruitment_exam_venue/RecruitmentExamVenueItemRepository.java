package recruitment_exam_venue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import pb.OptionDTO;
import pb.Utils;
import recruitment_job_description.Recruitment_job_descriptionDTO;
import repository.Repository;
import repository.RepositoryManager;


public class RecruitmentExamVenueItemRepository implements Repository {
	RecruitmentExamVenueItemDAO recruitmentexamvenueitemDAO = null;
	
	static Logger logger = Logger.getLogger(RecruitmentExamVenueItemRepository.class);
	Map<Long, RecruitmentExamVenueItemDTO>mapOfRecruitmentExamVenueItemDTOToiD;
	Map<Long, Set<RecruitmentExamVenueItemDTO> >mapOfRecruitmentExamVenueItemDTOTorecruitmentExamVenueId;
	Gson gson;

  
	private RecruitmentExamVenueItemRepository(){
		recruitmentexamvenueitemDAO = RecruitmentExamVenueItemDAO.getInstance();
		mapOfRecruitmentExamVenueItemDTOToiD = new ConcurrentHashMap<>();
		mapOfRecruitmentExamVenueItemDTOTorecruitmentExamVenueId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static RecruitmentExamVenueItemRepository INSTANCE = new RecruitmentExamVenueItemRepository();
    }

    public static RecruitmentExamVenueItemRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<RecruitmentExamVenueItemDTO> recruitmentexamvenueitemDTOs = recruitmentexamvenueitemDAO.getAllDTOs(reloadAll);
			for(RecruitmentExamVenueItemDTO recruitmentexamvenueitemDTO : recruitmentexamvenueitemDTOs) {
				RecruitmentExamVenueItemDTO oldRecruitmentExamVenueItemDTO = getRecruitmentExamVenueItemDTOByiD(recruitmentexamvenueitemDTO.iD);
				if( oldRecruitmentExamVenueItemDTO != null ) {
					mapOfRecruitmentExamVenueItemDTOToiD.remove(oldRecruitmentExamVenueItemDTO.iD);
				
					if(mapOfRecruitmentExamVenueItemDTOTorecruitmentExamVenueId.containsKey(oldRecruitmentExamVenueItemDTO.recruitmentExamVenueId)) {
						mapOfRecruitmentExamVenueItemDTOTorecruitmentExamVenueId.get(oldRecruitmentExamVenueItemDTO.recruitmentExamVenueId).remove(oldRecruitmentExamVenueItemDTO);
					}
					if(mapOfRecruitmentExamVenueItemDTOTorecruitmentExamVenueId.get(oldRecruitmentExamVenueItemDTO.recruitmentExamVenueId).isEmpty()) {
						mapOfRecruitmentExamVenueItemDTOTorecruitmentExamVenueId.remove(oldRecruitmentExamVenueItemDTO.recruitmentExamVenueId);
					}
					
					
				}
				if(recruitmentexamvenueitemDTO.isDeleted == 0) 
				{
					
					mapOfRecruitmentExamVenueItemDTOToiD.put(recruitmentexamvenueitemDTO.iD, recruitmentexamvenueitemDTO);
				
					if( ! mapOfRecruitmentExamVenueItemDTOTorecruitmentExamVenueId.containsKey(recruitmentexamvenueitemDTO.recruitmentExamVenueId)) {
						mapOfRecruitmentExamVenueItemDTOTorecruitmentExamVenueId.put(recruitmentexamvenueitemDTO.recruitmentExamVenueId, new HashSet<>());
					}
					mapOfRecruitmentExamVenueItemDTOTorecruitmentExamVenueId.get(recruitmentexamvenueitemDTO.recruitmentExamVenueId).add(recruitmentexamvenueitemDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public RecruitmentExamVenueItemDTO clone(RecruitmentExamVenueItemDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, RecruitmentExamVenueItemDTO.class);
	}
	
	
	public List<RecruitmentExamVenueItemDTO> getRecruitmentExamVenueItemList() {
		List <RecruitmentExamVenueItemDTO> recruitmentexamvenueitems = new ArrayList<RecruitmentExamVenueItemDTO>(this.mapOfRecruitmentExamVenueItemDTOToiD.values());
		return recruitmentexamvenueitems;
	}
	
	public List<RecruitmentExamVenueItemDTO> copyRecruitmentExamVenueItemList() {
		List <RecruitmentExamVenueItemDTO> recruitmentexamvenueitems = getRecruitmentExamVenueItemList();
		return recruitmentexamvenueitems
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public RecruitmentExamVenueItemDTO getRecruitmentExamVenueItemDTOByiD( long iD){
		return mapOfRecruitmentExamVenueItemDTOToiD.get(iD);
	}
	
	public RecruitmentExamVenueItemDTO copyRecruitmentExamVenueItemDTOByiD( long iD){
		return clone(mapOfRecruitmentExamVenueItemDTOToiD.get(iD));
	}
	
	
	public List<RecruitmentExamVenueItemDTO> getRecruitmentExamVenueItemDTOByrecruitmentExamVenueId(long recruitmentExamVenueId) {
		return new ArrayList<>( mapOfRecruitmentExamVenueItemDTOTorecruitmentExamVenueId.getOrDefault(recruitmentExamVenueId,new HashSet<>()));
	}
	
	public List<RecruitmentExamVenueItemDTO> copyRecruitmentExamVenueItemDTOByrecruitmentExamVenueId(long recruitmentExamVenueId)
	{
		List <RecruitmentExamVenueItemDTO> recruitmentexamvenueitems = getRecruitmentExamVenueItemDTOByrecruitmentExamVenueId(recruitmentExamVenueId);
		return recruitmentexamvenueitems
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public List<RecruitmentExamVenueItemDTO> getExamVenueBuildingFloorRoomByVenueId(long recruitmentExamVenueId){

		List<RecruitmentExamVenueItemDTO> recruitmentJobDescriptionDTOS = getRecruitmentExamVenueItemDTOByrecruitmentExamVenueId(recruitmentExamVenueId);

		return recruitmentJobDescriptionDTOS;
	}

	@Override
	public String getTableName() {
		return recruitmentexamvenueitemDAO.getTableName();
	}
}


