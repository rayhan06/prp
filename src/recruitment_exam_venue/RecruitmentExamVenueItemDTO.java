package recruitment_exam_venue;
import java.util.*; 
import util.*; 


public class RecruitmentExamVenueItemDTO extends CommonDTO
{

	public long recruitmentExamVenueId = -1;
    public String building = "";
    public String floor = "";
    public String roomNo = "";
    public String capacity = "";
    public String insertedBy = "";
    public String modifiedBy = "";
	public long insertionDate = -1;
	
	public List<RecruitmentExamVenueItemDTO> recruitmentExamVenueItemDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$RecruitmentExamVenueItemDTO[" +
            " iD = " + iD +
            " recruitmentExamVenueId = " + recruitmentExamVenueId +
            " building = " + building +
            " floor = " + floor +
            " roomNo = " + roomNo +
            " capacity = " + capacity +
            " searchColumn = " + searchColumn +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}