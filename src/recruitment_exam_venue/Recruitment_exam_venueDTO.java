package recruitment_exam_venue;
import java.util.*;

import pb.OptionDTO;
import util.*;


public class Recruitment_exam_venueDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
    public String address = "";
    public String phone = "";
    public String email = "";
    public String insertedBy = "";
    public String modifiedBy = "";
	public long insertionDate = -1;
	
	public List<RecruitmentExamVenueItemDTO> recruitmentExamVenueItemDTOList = new ArrayList<>();

    public OptionDTO getOptionDTO() {

        return new OptionDTO(
                nameEn,
                nameBn,
                String.format("%d", iD)
        );
    }
	
    @Override
	public String toString() {
            return "$Recruitment_exam_venueDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " address = " + address +
            " phone = " + phone +
            " email = " + email +
            " searchColumn = " + searchColumn +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}