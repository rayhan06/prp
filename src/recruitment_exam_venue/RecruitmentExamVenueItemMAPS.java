package recruitment_exam_venue;
import java.util.*; 
import util.*;


public class RecruitmentExamVenueItemMAPS extends CommonMaps
{	
	public RecruitmentExamVenueItemMAPS(String tableName)
	{
		


		java_SQL_map.put("recruitment_exam_venue_id".toLowerCase(), "recruitmentExamVenueId".toLowerCase());
		java_SQL_map.put("building".toLowerCase(), "building".toLowerCase());
		java_SQL_map.put("floor".toLowerCase(), "floor".toLowerCase());
		java_SQL_map.put("room_no".toLowerCase(), "roomNo".toLowerCase());
		java_SQL_map.put("capacity".toLowerCase(), "capacity".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Recruitment Exam Venue Id".toLowerCase(), "recruitmentExamVenueId".toLowerCase());
		java_Text_map.put("Building".toLowerCase(), "building".toLowerCase());
		java_Text_map.put("Floor".toLowerCase(), "floor".toLowerCase());
		java_Text_map.put("Room No".toLowerCase(), "roomNo".toLowerCase());
		java_Text_map.put("Capacity".toLowerCase(), "capacity".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}