package recruitment_exam_venue;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Recruitment_exam_venueDAO  implements CommonDAOService<Recruitment_exam_venueDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Recruitment_exam_venueDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"address",
			"phone",
			"email",
			"search_column",
			"inserted_by",
			"modified_by",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("name_en"," and (name_en like ?)");
		searchMap.put("name_bn"," and (name_bn like ?)");
		searchMap.put("phone"," and (phone like ?)");
		searchMap.put("email"," and (email like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Recruitment_exam_venueDAO INSTANCE = new Recruitment_exam_venueDAO();
	}

	public static Recruitment_exam_venueDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Recruitment_exam_venueDTO recruitment_exam_venueDTO)
	{
		recruitment_exam_venueDTO.searchColumn = "";
		recruitment_exam_venueDTO.searchColumn += recruitment_exam_venueDTO.nameEn + " ";
		recruitment_exam_venueDTO.searchColumn += recruitment_exam_venueDTO.nameBn + " ";
		recruitment_exam_venueDTO.searchColumn += recruitment_exam_venueDTO.phone + " ";
		recruitment_exam_venueDTO.searchColumn += recruitment_exam_venueDTO.email + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Recruitment_exam_venueDTO recruitment_exam_venueDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(recruitment_exam_venueDTO);
		if(isInsert)
		{
			ps.setObject(++index,recruitment_exam_venueDTO.iD);
		}
		ps.setObject(++index,recruitment_exam_venueDTO.nameEn);
		ps.setObject(++index,recruitment_exam_venueDTO.nameBn);
		ps.setObject(++index,recruitment_exam_venueDTO.address);
		ps.setObject(++index,recruitment_exam_venueDTO.phone);
		ps.setObject(++index,recruitment_exam_venueDTO.email);
		ps.setObject(++index,recruitment_exam_venueDTO.searchColumn);
		ps.setObject(++index,recruitment_exam_venueDTO.insertedBy);
		ps.setObject(++index,recruitment_exam_venueDTO.modifiedBy);
		ps.setObject(++index,recruitment_exam_venueDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(++index,recruitment_exam_venueDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,recruitment_exam_venueDTO.iD);
		}
	}
	
	@Override
	public Recruitment_exam_venueDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Recruitment_exam_venueDTO recruitment_exam_venueDTO = new Recruitment_exam_venueDTO();
			int i = 0;
			recruitment_exam_venueDTO.iD = rs.getLong(columnNames[i++]);
			recruitment_exam_venueDTO.nameEn = rs.getString(columnNames[i++]);
			recruitment_exam_venueDTO.nameBn = rs.getString(columnNames[i++]);
			recruitment_exam_venueDTO.address = rs.getString(columnNames[i++]);
			recruitment_exam_venueDTO.phone = rs.getString(columnNames[i++]);
			recruitment_exam_venueDTO.email = rs.getString(columnNames[i++]);
			recruitment_exam_venueDTO.searchColumn = rs.getString(columnNames[i++]);
			recruitment_exam_venueDTO.insertedBy = rs.getString(columnNames[i++]);
			recruitment_exam_venueDTO.modifiedBy = rs.getString(columnNames[i++]);
			recruitment_exam_venueDTO.insertionDate = rs.getLong(columnNames[i++]);
			recruitment_exam_venueDTO.isDeleted = rs.getInt(columnNames[i++]);
			recruitment_exam_venueDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return recruitment_exam_venueDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Recruitment_exam_venueDTO getDTOByID (long id)
	{
		Recruitment_exam_venueDTO recruitment_exam_venueDTO = null;
		try 
		{
			recruitment_exam_venueDTO = getDTOFromID(id);
			if(recruitment_exam_venueDTO != null)
			{
				RecruitmentExamVenueItemDAO recruitmentExamVenueItemDAO = RecruitmentExamVenueItemDAO.getInstance();				
				List<RecruitmentExamVenueItemDTO> recruitmentExamVenueItemDTOList = (List<RecruitmentExamVenueItemDTO>)recruitmentExamVenueItemDAO.getDTOsByParent("recruitment_exam_venue_id", recruitment_exam_venueDTO.iD);
				recruitment_exam_venueDTO.recruitmentExamVenueItemDTOList = recruitmentExamVenueItemDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return recruitment_exam_venueDTO;
	}

	@Override
	public String getTableName() {
		return "recruitment_exam_venue";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Recruitment_exam_venueDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Recruitment_exam_venueDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }
				
}
	