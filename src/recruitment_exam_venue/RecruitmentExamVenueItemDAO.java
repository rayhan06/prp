package recruitment_exam_venue;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class RecruitmentExamVenueItemDAO  implements CommonDAOService<RecruitmentExamVenueItemDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private RecruitmentExamVenueItemDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"recruitment_exam_venue_id",
			"building",
			"floor",
			"room_no",
			"capacity",
			"search_column",
			"inserted_by",
			"modified_by",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("building"," and (building like ?)");
		searchMap.put("floor"," and (floor like ?)");
		searchMap.put("room_no"," and (room_no like ?)");
		searchMap.put("capacity"," and (capacity like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final RecruitmentExamVenueItemDAO INSTANCE = new RecruitmentExamVenueItemDAO();
	}

	public static RecruitmentExamVenueItemDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(RecruitmentExamVenueItemDTO recruitmentexamvenueitemDTO)
	{
		recruitmentexamvenueitemDTO.searchColumn = "";
		recruitmentexamvenueitemDTO.searchColumn += recruitmentexamvenueitemDTO.building + " ";
		recruitmentexamvenueitemDTO.searchColumn += recruitmentexamvenueitemDTO.floor + " ";
		recruitmentexamvenueitemDTO.searchColumn += recruitmentexamvenueitemDTO.roomNo + " ";
		recruitmentexamvenueitemDTO.searchColumn += recruitmentexamvenueitemDTO.capacity + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, RecruitmentExamVenueItemDTO recruitmentexamvenueitemDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(recruitmentexamvenueitemDTO);
		if(isInsert)
		{
			ps.setObject(++index,recruitmentexamvenueitemDTO.iD);
		}
		ps.setObject(++index,recruitmentexamvenueitemDTO.recruitmentExamVenueId);
		ps.setObject(++index,recruitmentexamvenueitemDTO.building);
		ps.setObject(++index,recruitmentexamvenueitemDTO.floor);
		ps.setObject(++index,recruitmentexamvenueitemDTO.roomNo);
		ps.setObject(++index,recruitmentexamvenueitemDTO.capacity);
		ps.setObject(++index,recruitmentexamvenueitemDTO.searchColumn);
		ps.setObject(++index,recruitmentexamvenueitemDTO.insertedBy);
		ps.setObject(++index,recruitmentexamvenueitemDTO.modifiedBy);
		ps.setObject(++index,recruitmentexamvenueitemDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(++index,recruitmentexamvenueitemDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,recruitmentexamvenueitemDTO.iD);
		}
	}
	
	@Override
	public RecruitmentExamVenueItemDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			RecruitmentExamVenueItemDTO recruitmentexamvenueitemDTO = new RecruitmentExamVenueItemDTO();
			int i = 0;
			recruitmentexamvenueitemDTO.iD = rs.getLong(columnNames[i++]);
			recruitmentexamvenueitemDTO.recruitmentExamVenueId = rs.getLong(columnNames[i++]);
			recruitmentexamvenueitemDTO.building = rs.getString(columnNames[i++]);
			recruitmentexamvenueitemDTO.floor = rs.getString(columnNames[i++]);
			recruitmentexamvenueitemDTO.roomNo = rs.getString(columnNames[i++]);
			recruitmentexamvenueitemDTO.capacity = rs.getString(columnNames[i++]);
			recruitmentexamvenueitemDTO.searchColumn = rs.getString(columnNames[i++]);
			recruitmentexamvenueitemDTO.insertedBy = rs.getString(columnNames[i++]);
			recruitmentexamvenueitemDTO.modifiedBy = rs.getString(columnNames[i++]);
			recruitmentexamvenueitemDTO.insertionDate = rs.getLong(columnNames[i++]);
			recruitmentexamvenueitemDTO.isDeleted = rs.getInt(columnNames[i++]);
			recruitmentexamvenueitemDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return recruitmentexamvenueitemDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public RecruitmentExamVenueItemDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "recruitment_exam_venue_item";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((RecruitmentExamVenueItemDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((RecruitmentExamVenueItemDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }
				
}
	