package recruitment_exam_venue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import pb.OptionDTO;
import pb.Utils;
import recruitment_test_name.Recruitment_test_nameDTO;
import repository.Repository;
import repository.RepositoryManager;


public class Recruitment_exam_venueRepository implements Repository {
	Recruitment_exam_venueDAO recruitment_exam_venueDAO = null;
	private List<Recruitment_exam_venueDTO> recruitment_exam_venueDTOS;
	
	static Logger logger = Logger.getLogger(Recruitment_exam_venueRepository.class);
	Map<Long, Recruitment_exam_venueDTO>mapOfRecruitment_exam_venueDTOToiD;
	Gson gson;

  
	private Recruitment_exam_venueRepository(){
		recruitment_exam_venueDAO = Recruitment_exam_venueDAO.getInstance();
		mapOfRecruitment_exam_venueDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Recruitment_exam_venueRepository INSTANCE = new Recruitment_exam_venueRepository();
    }

    public static Recruitment_exam_venueRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Recruitment_exam_venueDTO> recruitment_exam_venueDTOs = recruitment_exam_venueDAO.getAllDTOs(reloadAll);
			for(Recruitment_exam_venueDTO recruitment_exam_venueDTO : recruitment_exam_venueDTOs) {
				Recruitment_exam_venueDTO oldRecruitment_exam_venueDTO = getRecruitment_exam_venueDTOByiD(recruitment_exam_venueDTO.iD);
				if( oldRecruitment_exam_venueDTO != null ) {
					mapOfRecruitment_exam_venueDTOToiD.remove(oldRecruitment_exam_venueDTO.iD);
				
					
				}
				if(recruitment_exam_venueDTO.isDeleted == 0) 
				{
					
					mapOfRecruitment_exam_venueDTOToiD.put(recruitment_exam_venueDTO.iD, recruitment_exam_venueDTO);
				
				}
			}
			recruitment_exam_venueDTOS = new ArrayList<>(mapOfRecruitment_exam_venueDTOToiD.values());
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Recruitment_exam_venueDTO clone(Recruitment_exam_venueDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Recruitment_exam_venueDTO.class);
	}
	
	
	public List<Recruitment_exam_venueDTO> getRecruitment_exam_venueList() {
		List <Recruitment_exam_venueDTO> recruitment_exam_venues = new ArrayList<Recruitment_exam_venueDTO>(this.mapOfRecruitment_exam_venueDTOToiD.values());
		return recruitment_exam_venues;
	}
	
	public List<Recruitment_exam_venueDTO> copyRecruitment_exam_venueList() {
		List <Recruitment_exam_venueDTO> recruitment_exam_venues = getRecruitment_exam_venueList();
		return recruitment_exam_venues
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Recruitment_exam_venueDTO getRecruitment_exam_venueDTOByiD( long iD){
		recruitment_exam_venueDTOS = new ArrayList<>(mapOfRecruitment_exam_venueDTOToiD.values());
		return mapOfRecruitment_exam_venueDTOToiD.get(iD);
	}
	
	public Recruitment_exam_venueDTO copyRecruitment_exam_venueDTOByiD( long iD){
		return clone(mapOfRecruitment_exam_venueDTOToiD.get(iD));
	}

	public String buildOptions(String language, Long selectedId) {
		List<OptionDTO> optionDTOList = null;
		if (recruitment_exam_venueDTOS != null && recruitment_exam_venueDTOS.size() > 0) {
			optionDTOList = recruitment_exam_venueDTOS.stream()
					.map(Recruitment_exam_venueDTO::getOptionDTO)
					.collect(Collectors.toList());
		}
		return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
	}

	
	@Override
	public String getTableName() {
		return recruitment_exam_venueDAO.getTableName();
	}
}


