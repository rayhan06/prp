package recruitment_exam_venue;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import job_applicant_application.JobApplicantApplicationModel;
import job_applicant_application.Job_applicant_applicationDTO;
import job_applicant_application.Job_applicant_applicationRepository;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import pi_auction_items.PiProductModel;
import pi_purchase.Pi_purchaseDAO;
import pi_purchase.Pi_purchaseDTO;
import recruitment_seat_plan.RecruitmentSeatPlanChildDTO;
import recruitment_seat_plan.RecruitmentSeatPlanChildRepository;
import role.PermissionRepository;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;

import javax.servlet.http.*;
import java.util.*;
import java.util.stream.Collectors;


import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;
import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;


/**
 * Servlet implementation class Recruitment_exam_venueServlet
 */
@WebServlet("/Recruitment_exam_venueServlet")
@MultipartConfig
public class Recruitment_exam_venueServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Recruitment_exam_venueServlet.class);

    @Override
    public String getTableName() {
        return Recruitment_exam_venueDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Recruitment_exam_venueServlet";
    }

    @Override
    public Recruitment_exam_venueDAO getCommonDAOService() {
        return Recruitment_exam_venueDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.RECRUITMENT_EXAM_VENUE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.RECRUITMENT_EXAM_VENUE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.RECRUITMENT_EXAM_VENUE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Recruitment_exam_venueServlet.class;
    }

    RecruitmentExamVenueItemDAO recruitmentExamVenueItemDAO = RecruitmentExamVenueItemDAO.getInstance();
    private final Gson gson = new Gson();


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {

        request.setAttribute("failureMessage", "");
        System.out.println("%%%% addRecruitment_exam_venue");
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        Recruitment_exam_venueDTO recruitment_exam_venueDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLanEng = Language.equalsIgnoreCase("English");

        if (addFlag) {
            recruitment_exam_venueDTO = new Recruitment_exam_venueDTO();
            recruitment_exam_venueDTO.insertionDate = recruitment_exam_venueDTO.lastModificationTime = System.currentTimeMillis();
            recruitment_exam_venueDTO.insertedBy = recruitment_exam_venueDTO.modifiedBy = String.valueOf(userDTO.ID);
        } else {
            recruitment_exam_venueDTO = Recruitment_exam_venueDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
            if (recruitment_exam_venueDTO == null) {
                throw new Exception(isLanEng ? "Recruitment exam venue information is not found" : "এক্সাম ভেন্যু তথ্য খুঁজে পাওয়া যায়নি");
            }
            recruitment_exam_venueDTO.lastModificationTime = System.currentTimeMillis();
            recruitment_exam_venueDTO.modifiedBy = String.valueOf(userDTO.ID);
        }

        String Value = "";

        Value = request.getParameter("nameEn");

        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide recruitment exam venue english name" : "অনুগ্রহপূর্বক এক্সাম ভেন্যুর ইংরেজি নাম দিন");
        }
        boolean hasNonEnglish =
                Value.chars()
                        .anyMatch(ch -> !(ch >= ' ' && ch <= '~'));
        if (!hasNonEnglish) {
            recruitment_exam_venueDTO.nameEn = (Value);
        } else {
            throw new Exception(isLanEng ? "Please provide recruitment exam venue english name" : "অনুগ্রহপূর্বক এক্সাম ভেন্যুর ইংরেজি নাম দিন");
        }

        Value = request.getParameter("nameBn");

        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide recruitment exam venue bangla name" : "অনুগ্রহপূর্বক এক্সাম ভেন্যুর বাংলা নাম দিন");
        }

        boolean hasAllBangla = Value.chars().allMatch(Utils::isValidBanglaCharacter);
        if (!hasAllBangla) {
            throw new Exception(isLanEng ? "Please provide recruitment exam venue bangla name" : "অনুগ্রহপূর্বক এক্সাম ভেন্যুর বাংলা নাম দিন");
        }

        recruitment_exam_venueDTO.nameBn = (Value);

        Value = request.getParameter("address");
        if (Value == null ||Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng?"Please provide exam venue address":"অনুগ্রহপূর্বক এক্সাম ভেন্যুর ঠিকানা দিন");
        }
        recruitment_exam_venueDTO.address = (Value);

        Value = request.getParameter("phone");
        if (Value == null ||Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng?"Please provide phone number":"অনুগ্রহপূর্বক ফোন নম্বর দিন");
        }
        recruitment_exam_venueDTO.phone = (Value);

        Value = request.getParameter("email");
        recruitment_exam_venueDTO.email = (Value);

        Value = request.getParameter("searchColumn");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        System.out.println("searchColumn = " + Value);
        if (Value != null) {
            recruitment_exam_venueDTO.searchColumn = (Value);
        } else {
        }

        List<RecruitmentExamVenueItemDTO> recruitmentExamVenueItemDTOList = createRecruitmentExamVenueItemDTOListByRequest(request, language);

        if (addFlag) {
            Recruitment_exam_venueDAO.getInstance().add(recruitment_exam_venueDTO);
        } else {
            Recruitment_exam_venueDAO.getInstance().update(recruitment_exam_venueDTO);
        }


        if (addFlag) //add or validate
        {
            if (recruitmentExamVenueItemDTOList != null) {
                for (RecruitmentExamVenueItemDTO recruitmentExamVenueItemDTO : recruitmentExamVenueItemDTOList) {
                    recruitmentExamVenueItemDTO.recruitmentExamVenueId = recruitment_exam_venueDTO.iD;
                    recruitmentExamVenueItemDAO.add(recruitmentExamVenueItemDTO);
                }
            }

        } else {
            List<Long> childIdsFromRequest = recruitmentExamVenueItemDAO.getChildIdsFromRequest(request, "recruitmentExamVenueItem");
            //delete the removed children
            recruitmentExamVenueItemDAO.deleteChildrenNotInList("recruitment_exam_venue", "recruitment_exam_venue_item", recruitment_exam_venueDTO.iD, childIdsFromRequest);
            List<Long> childIDsInDatabase = recruitmentExamVenueItemDAO.getChilIds("recruitment_exam_venue", "recruitment_exam_venue_item", recruitment_exam_venueDTO.iD);


            if (childIdsFromRequest != null) {
                for (int i = 0; i < childIdsFromRequest.size(); i++) {
                    Long childIDFromRequest = childIdsFromRequest.get(i);
                    if (childIDsInDatabase.contains(childIDFromRequest)) {
                        RecruitmentExamVenueItemDTO recruitmentExamVenueItemDTO = createRecruitmentExamVenueItemDTOByRequestAndIndex(request, false, i, language);
                        recruitmentExamVenueItemDTO.recruitmentExamVenueId = recruitment_exam_venueDTO.iD;
                        recruitmentExamVenueItemDAO.update(recruitmentExamVenueItemDTO);
                    } else {
                        RecruitmentExamVenueItemDTO recruitmentExamVenueItemDTO = createRecruitmentExamVenueItemDTOByRequestAndIndex(request, true, i, language);
                        recruitmentExamVenueItemDTO.recruitmentExamVenueId = recruitment_exam_venueDTO.iD;
                        recruitmentExamVenueItemDAO.add(recruitmentExamVenueItemDTO);
                    }
                }
            } else {
                recruitmentExamVenueItemDAO.deleteChildrenByParent(recruitment_exam_venueDTO.iD, "recruitment_exam_venue_id");
            }

        }
        return recruitment_exam_venueDTO;


    }

    private List<RecruitmentExamVenueItemDTO> createRecruitmentExamVenueItemDTOListByRequest(HttpServletRequest request, String language) throws Exception {
        List<RecruitmentExamVenueItemDTO> recruitmentExamVenueItemDTOList = new ArrayList<RecruitmentExamVenueItemDTO>();
        if (request.getParameterValues("recruitmentExamVenueItem.iD") != null) {
            int recruitmentExamVenueItemItemNo = request.getParameterValues("recruitmentExamVenueItem.iD").length;


            for (int index = 0; index < recruitmentExamVenueItemItemNo; index++) {
                RecruitmentExamVenueItemDTO recruitmentExamVenueItemDTO = createRecruitmentExamVenueItemDTOByRequestAndIndex(request, true, index, language);
                recruitmentExamVenueItemDTOList.add(recruitmentExamVenueItemDTO);
            }

            return recruitmentExamVenueItemDTOList;
        }
        return null;
    }

    private RecruitmentExamVenueItemDTO createRecruitmentExamVenueItemDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, String language) throws Exception {

        RecruitmentExamVenueItemDTO recruitmentExamVenueItemDTO;
        if (addFlag == true) {
            recruitmentExamVenueItemDTO = new RecruitmentExamVenueItemDTO();
        } else {
            recruitmentExamVenueItemDTO = recruitmentExamVenueItemDAO.getDTOByID(Long.parseLong(request.getParameterValues("recruitmentExamVenueItem.iD")[index]));
        }
        String path = getServletContext().getRealPath("/img2/");
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");


        String Value = "";
        Value = request.getParameterValues("recruitmentExamVenueItem.recruitmentExamVenueId")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_ITEM_RECRUITMENTEXAMVENUEID, language) + " " + ErrorMessage.getInvalidMessage(language));
        }

        recruitmentExamVenueItemDTO.recruitmentExamVenueId = Long.parseLong(Value);
        Value = request.getParameterValues("recruitmentExamVenueItem.building")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_ITEM_BUILDING, language) + " " + ErrorMessage.getInvalidMessage(language));
        }

        recruitmentExamVenueItemDTO.building = (Value);
        Value = request.getParameterValues("recruitmentExamVenueItem.floor")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_ITEM_FLOOR, language) + " " + ErrorMessage.getInvalidMessage(language));
        }

        recruitmentExamVenueItemDTO.floor = (Value);
        Value = request.getParameterValues("recruitmentExamVenueItem.roomNo")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_ITEM_ROOMNO, language) + " " + ErrorMessage.getInvalidMessage(language));
        }

        recruitmentExamVenueItemDTO.roomNo = (Value);
        Value = request.getParameterValues("recruitmentExamVenueItem.capacity")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_ITEM_CAPACITY, language) + " " + ErrorMessage.getInvalidMessage(language));
        }

        recruitmentExamVenueItemDTO.capacity = (Value);
       
        return recruitmentExamVenueItemDTO;

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            long recruitmentExamVenueId ;
            List<RecruitmentExamVenueItemDTO> recruitmentExamVenueItemDTOS;
            List<RecruitmentExamVenueItemModel> modelList = null;
            switch (actionType) {
                case "getExamVenueBuildingFloorRoomByVenueId":
                    recruitmentExamVenueId = Long.parseLong(request.getParameter("recruitmentExamVenueId"));
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    recruitmentExamVenueItemDTOS = RecruitmentExamVenueItemRepository.getInstance().
                            getExamVenueBuildingFloorRoomByVenueId(recruitmentExamVenueId);

                    if(recruitmentExamVenueItemDTOS!=null &&  recruitmentExamVenueItemDTOS.size()>0){
                        modelList = recruitmentExamVenueItemDTOS
                                .stream()
                                .map(dto -> new RecruitmentExamVenueItemModel(dto, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language))
                                .collect(Collectors.toList());
                    }

                    response.getWriter().println(gson.toJson(modelList));
                    return;

                case "getExamVenueByPreviousConfiguration":
                    long recruitmentTestName = Long.parseLong(request.getParameter("recruitmentTestName"));
                    long recruitmentJobDescription = Long.parseLong(request.getParameter("recruitmentJobDescription"));
                    long recruitmentJobSpecificExamType = Long.parseLong(request.getParameter("recruitmentJobSpecificExamType"));
                    long recruitmentSeatPlanId = Long.parseLong(request.getParameter("recruitmentSeatPlanId"));
                    recruitmentExamVenueId = Long.parseLong(request.getParameter("recruitmentExamVenueId"));
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    List<RecruitmentSeatPlanChildDTO> recruitmentSeatPlanChildDTOS = RecruitmentSeatPlanChildRepository.getInstance().
                            getRecruitmentSeatPlanChildDTOByrecruitmentSeatPlanId(recruitmentSeatPlanId);

                    if(recruitmentSeatPlanChildDTOS!=null &&  recruitmentSeatPlanChildDTOS.size()>0){
                        modelList = recruitmentSeatPlanChildDTOS
                                .stream()
                                .filter(e -> e.recruitmentTestNameId == recruitmentTestName &&
                                        e.recruitmentJobDescriptionId == recruitmentJobDescription &&
                                        e.recruitmentJobSpecificExamTypeId == recruitmentJobSpecificExamType &&
                                        e.recruitmentExamVenueId == recruitmentExamVenueId)
                                .map(dto -> new RecruitmentExamVenueItemModel(dto, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language))
                                .collect(Collectors.toList());
                    }

                    response.getWriter().println(gson.toJson(modelList));
                    return;

                case "getSpecificExamStartAndEndRoll" :
                    recruitmentTestName = Long.parseLong(request.getParameter("recruitmentTestName"));
                    recruitmentJobDescription = Long.parseLong(request.getParameter("recruitmentJobDescription"));
                    recruitmentJobSpecificExamType = Long.parseLong(request.getParameter("recruitmentJobSpecificExamType"));
                    JobApplicantApplicationModel model = null;
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    List<Job_applicant_applicationDTO> job_applicant_applicationDTOS = Job_applicant_applicationRepository.getInstance().
                            getJob_applicant_applicationDTOByjob_id(recruitmentJobDescription);

                    if(job_applicant_applicationDTOS!=null &&  job_applicant_applicationDTOS.size()>0){
                        job_applicant_applicationDTOS = job_applicant_applicationDTOS
                                .stream()
                                .filter(e -> e.level == recruitmentJobSpecificExamType &&
                                        !e.isRejected)
                                .collect(Collectors.toList());

                        Long startRoll = job_applicant_applicationDTOS.stream()
                                .mapToLong(e -> Long.parseLong(e.rollNumber)).min().orElse(-1);

                        Long endRoll = job_applicant_applicationDTOS.stream()
                                .mapToLong(e -> Long.parseLong(e.rollNumber)).max().orElse(-1);

                        model = new JobApplicantApplicationModel(String.valueOf(startRoll),String.valueOf(endRoll),HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language);
                    }

                    response.getWriter().println(gson.toJson(model));
                     return;


                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);


    }
}

