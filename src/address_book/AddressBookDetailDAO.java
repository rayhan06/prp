package address_book;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class AddressBookDetailDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public AddressBookDetailDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";

		commonMaps = new AddressBookDetailMAPS(tableName);
	}
	
	public AddressBookDetailDAO()
	{
		this("address_book_detail");		
	}
	
	
	
	public long add(CommonDTO commonDTO) throws Exception
	{
		
		AddressBookDetailDTO addressbookdetailDTO = (AddressBookDetailDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			addressbookdetailDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "address_book_id";
			sql += ", ";
			sql += "employee_organogram_id";
			sql += ", ";
			sql += "employee_designation";
			sql += ", ";
			sql += "employee_office_unit";
			sql += ", ";
			sql += "employee_email";
			sql += ", ";
			sql += "name";
			sql += ", ";
			sql += "phone_number";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
		
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
			sql += ")";
				

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,addressbookdetailDTO.iD);
			ps.setObject(index++,addressbookdetailDTO.addressBookId);
			ps.setObject(index++,addressbookdetailDTO.employeeOrganogramId);
			ps.setObject(index++,addressbookdetailDTO.employeeDesignation);
			ps.setObject(index++,addressbookdetailDTO.employeeOfficeUnit);
			ps.setObject(index++,addressbookdetailDTO.employeeEmail);
			ps.setObject(index++,addressbookdetailDTO.name);
			ps.setObject(index++,addressbookdetailDTO.phoneNumber);
			ps.setObject(index++,addressbookdetailDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection,lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return addressbookdetailDTO.iD;		
	}
		
	
	public void deleteAddressBookDetailByAddressBookID(long addressBookID) throws Exception{
		
		
		Connection connection = null;
		Statement stmt = null;
		try{
			
			//String sql = "UPDATE address_book_detail SET isDeleted=0 WHERE address_book_id="+addressBookID;
			String sql = "delete from address_book_detail WHERE address_book_id=" + addressBookID;
			logger.debug("sql " + sql);
			connection = DBMW.getInstance().getConnection();
			stmt = connection.createStatement();
			stmt.execute(sql);
		}catch(Exception ex){
			logger.debug("fatal",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMW.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
	}		
   
	public List<AddressBookDetailDTO> getAddressBookDetailDTOListByAddressBookID(long addressBookID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		AddressBookDetailDTO addressbookdetailDTO = null;
		List<AddressBookDetailDTO> addressbookdetailDTOList = new ArrayList<>();
		
		try{
			
			String sql = "SELECT * FROM address_book_detail where isDeleted=0 and address_book_id="+addressBookID+" order by address_book_detail.lastModificationTime";
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);

			while(rs.next()){
				addressbookdetailDTO = new AddressBookDetailDTO();
				addressbookdetailDTO.iD = rs.getLong("ID");
				addressbookdetailDTO.addressBookId = rs.getLong("address_book_id");
				addressbookdetailDTO.employeeOrganogramId = rs.getLong("employee_organogram_id");
				addressbookdetailDTO.employeeDesignation = rs.getString("employee_designation");
				addressbookdetailDTO.employeeOfficeUnit = rs.getString("employee_office_unit");
				addressbookdetailDTO.employeeEmail = rs.getString("employee_email");
				addressbookdetailDTO.name = rs.getString("name");
				addressbookdetailDTO.phoneNumber = rs.getString("phone_number");
				addressbookdetailDTO.isDeleted = rs.getInt("isDeleted");
				addressbookdetailDTO.lastModificationTime = rs.getLong("lastModificationTime");
				addressbookdetailDTOList.add(addressbookdetailDTO);

			}			
			
		}catch(Exception ex){
			logger.debug("fatal",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return addressbookdetailDTOList;
	}

	//need another getter for repository
	public CommonDTO getDTOByID (long ID) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		AddressBookDetailDTO addressbookdetailDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				addressbookdetailDTO = new AddressBookDetailDTO();

				addressbookdetailDTO.iD = rs.getLong("ID");
				addressbookdetailDTO.addressBookId = rs.getLong("address_book_id");
				addressbookdetailDTO.employeeOrganogramId = rs.getLong("employee_organogram_id");
				addressbookdetailDTO.employeeDesignation = rs.getString("employee_designation");
				addressbookdetailDTO.employeeOfficeUnit = rs.getString("employee_office_unit");
				addressbookdetailDTO.employeeEmail = rs.getString("employee_email");
				addressbookdetailDTO.name = rs.getString("name");
				addressbookdetailDTO.phoneNumber = rs.getString("phone_number");
				addressbookdetailDTO.isDeleted = rs.getInt("isDeleted");
				addressbookdetailDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
			
			AddressBookDetailDAO addressBookDetailDAO = new AddressBookDetailDAO("address_book_detail");			
			List<AddressBookDetailDTO> addressBookDetailDTOList = addressBookDetailDAO.getAddressBookDetailDTOListByAddressBookID(addressbookdetailDTO.iD);
			addressbookdetailDTO.addressBookDetailDTOList = addressBookDetailDTOList;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return addressbookdetailDTO;
	}
	
	public long update(CommonDTO commonDTO) throws Exception
	{		
		AddressBookDetailDTO addressbookdetailDTO = (AddressBookDetailDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "address_book_id=?";
			sql += ", ";
			sql += "employee_organogram_id=?";
			sql += ", ";
			sql += "employee_designation=?";
			sql += ", ";
			sql += "employee_office_unit=?";
			sql += ", ";
			sql += "employee_email=?";
			sql += ", ";
			sql += "name=?";
			sql += ", ";
			sql += "phone_number=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + addressbookdetailDTO.iD;
				

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,addressbookdetailDTO.addressBookId);
			ps.setObject(index++,addressbookdetailDTO.employeeOrganogramId);
			ps.setObject(index++,addressbookdetailDTO.employeeDesignation);
			ps.setObject(index++,addressbookdetailDTO.employeeOfficeUnit);
			ps.setObject(index++,addressbookdetailDTO.employeeEmail);
			ps.setObject(index++,addressbookdetailDTO.name);
			ps.setObject(index++,addressbookdetailDTO.phoneNumber);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection,lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return addressbookdetailDTO.iD;
	}
	
	
	public List<AddressBookDetailDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		AddressBookDetailDTO addressbookdetailDTO = null;
		List<AddressBookDetailDTO> addressbookdetailDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return addressbookdetailDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				addressbookdetailDTO = new AddressBookDetailDTO();
				addressbookdetailDTO.iD = rs.getLong("ID");
				addressbookdetailDTO.addressBookId = rs.getLong("address_book_id");
				addressbookdetailDTO.employeeOrganogramId = rs.getLong("employee_organogram_id");
				addressbookdetailDTO.employeeDesignation = rs.getString("employee_designation");
				addressbookdetailDTO.employeeOfficeUnit = rs.getString("employee_office_unit");
				addressbookdetailDTO.employeeEmail = rs.getString("employee_email");
				addressbookdetailDTO.name = rs.getString("name");
				addressbookdetailDTO.phoneNumber = rs.getString("phone_number");
				addressbookdetailDTO.isDeleted = rs.getInt("isDeleted");
				addressbookdetailDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + addressbookdetailDTO);
				
				addressbookdetailDTOList.add(addressbookdetailDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return addressbookdetailDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<AddressBookDetailDTO> getAllAddressBookDetail (boolean isFirstReload)
    {
		List<AddressBookDetailDTO> addressbookdetailDTOList = new ArrayList<>();

		String sql = "SELECT * FROM address_book_detail";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by addressbookdetail.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				AddressBookDetailDTO addressbookdetailDTO = new AddressBookDetailDTO();
				addressbookdetailDTO.iD = rs.getLong("ID");
				addressbookdetailDTO.addressBookId = rs.getLong("address_book_id");
				addressbookdetailDTO.employeeOrganogramId = rs.getLong("employee_organogram_id");
				addressbookdetailDTO.employeeDesignation = rs.getString("employee_designation");
				addressbookdetailDTO.employeeOfficeUnit = rs.getString("employee_office_unit");
				addressbookdetailDTO.employeeEmail = rs.getString("employee_email");
				addressbookdetailDTO.name = rs.getString("name");
				addressbookdetailDTO.phoneNumber = rs.getString("phone_number");
				addressbookdetailDTO.isDeleted = rs.getInt("isDeleted");
				addressbookdetailDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				addressbookdetailDTOList.add(addressbookdetailDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return addressbookdetailDTOList;
    }

	
	public List<AddressBookDetailDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<AddressBookDetailDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<AddressBookDetailDTO> addressbookdetailDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				AddressBookDetailDTO addressbookdetailDTO = new AddressBookDetailDTO();
				addressbookdetailDTO.iD = rs.getLong("ID");
				addressbookdetailDTO.addressBookId = rs.getLong("address_book_id");
				addressbookdetailDTO.employeeOrganogramId = rs.getLong("employee_organogram_id");
				addressbookdetailDTO.employeeDesignation = rs.getString("employee_designation");
				addressbookdetailDTO.employeeOfficeUnit = rs.getString("employee_office_unit");
				addressbookdetailDTO.employeeEmail = rs.getString("employee_email");
				addressbookdetailDTO.name = rs.getString("name");
				addressbookdetailDTO.phoneNumber = rs.getString("phone_number");
				addressbookdetailDTO.isDeleted = rs.getInt("isDeleted");
				addressbookdetailDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				addressbookdetailDTOList.add(addressbookdetailDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return addressbookdetailDTOList;
	
	}
				
}
	