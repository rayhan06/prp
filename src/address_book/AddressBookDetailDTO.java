package address_book;
import java.util.*; 
import util.*; 


public class AddressBookDetailDTO extends CommonDTO
{

	public long addressBookId = 0;
	public long employeeOrganogramId = 0;
    public String employeeDesignation = "";
    public String employeeOfficeUnit = "";
    public String employeeEmail = "";
    public String name = "";
    public String phoneNumber = "";
	
	public List<AddressBookDetailDTO> addressBookDetailDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$AddressBookDetailDTO[" +
            " iD = " + iD +
            " addressBookId = " + addressBookId +
            " employeeOrganogramId = " + employeeOrganogramId +
            " employeeDesignation = " + employeeDesignation +
            " employeeOfficeUnit = " + employeeOfficeUnit +
            " employeeEmail = " + employeeEmail +
            " name = " + name +
            " phoneNumber = " + phoneNumber +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}