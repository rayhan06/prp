package address_book;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Address_bookDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public Address_bookDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";

		commonMaps = new Address_bookMAPS(tableName);
	}
	
	public Address_bookDAO()
	{
		this("address_book");		
	}
	
	
	
	public long add(CommonDTO commonDTO) throws Exception
	{
		
		Address_bookDTO address_bookDTO = (Address_bookDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			address_bookDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "name_en";
			sql += ", ";
			sql += "name_bn";
			sql += ", ";
			sql += "created_by";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
		
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
			sql += ")";
				

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,address_bookDTO.iD);
			ps.setObject(index++,address_bookDTO.nameEn);
			ps.setObject(index++,address_bookDTO.nameBn);
			ps.setObject(index++,address_bookDTO.createdBy);
			ps.setObject(index++,address_bookDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return address_bookDTO.iD;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Address_bookDTO address_bookDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				address_bookDTO = new Address_bookDTO();

				address_bookDTO.iD = rs.getLong("ID");
				address_bookDTO.nameEn = rs.getString("name_en");
				address_bookDTO.nameBn = rs.getString("name_bn");
				address_bookDTO.createdBy = rs.getString("created_by");
				address_bookDTO.isDeleted = rs.getInt("isDeleted");
				address_bookDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
			
			AddressBookDetailDAO addressBookDetailDAO = new AddressBookDetailDAO("address_book_detail");			
			List<AddressBookDetailDTO> addressBookDetailDTOList = addressBookDetailDAO.getAddressBookDetailDTOListByAddressBookID(address_bookDTO.iD);
			address_bookDTO.addressBookDetailDTOList = addressBookDetailDTOList;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return address_bookDTO;
	}
	
	public long update(CommonDTO commonDTO) throws Exception
	{		
		Address_bookDTO address_bookDTO = (Address_bookDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "name_en=?";
			sql += ", ";
			sql += "name_bn=?";
			sql += ", ";
			sql += "created_by=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + address_bookDTO.iD;
				

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,address_bookDTO.nameEn);
			ps.setObject(index++,address_bookDTO.nameBn);
			ps.setObject(index++,address_bookDTO.createdBy);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return address_bookDTO.iD;
	}
	
	
	public List<Address_bookDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Address_bookDTO address_bookDTO = null;
		List<Address_bookDTO> address_bookDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return address_bookDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				address_bookDTO = new Address_bookDTO();
				address_bookDTO.iD = rs.getLong("ID");
				address_bookDTO.nameEn = rs.getString("name_en");
				address_bookDTO.nameBn = rs.getString("name_bn");
				address_bookDTO.createdBy = rs.getString("created_by");
				address_bookDTO.isDeleted = rs.getInt("isDeleted");
				address_bookDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + address_bookDTO);
				
				address_bookDTOList.add(address_bookDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return address_bookDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Address_bookDTO> getAllAddress_book (boolean isFirstReload)
    {
		List<Address_bookDTO> address_bookDTOList = new ArrayList<>();

		String sql = "SELECT * FROM address_book";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by address_book.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Address_bookDTO address_bookDTO = new Address_bookDTO();
				address_bookDTO.iD = rs.getLong("ID");
				address_bookDTO.nameEn = rs.getString("name_en");
				address_bookDTO.nameBn = rs.getString("name_bn");
				address_bookDTO.createdBy = rs.getString("created_by");
				address_bookDTO.isDeleted = rs.getInt("isDeleted");
				address_bookDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				address_bookDTOList.add(address_bookDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return address_bookDTOList;
    }

	
	public List<Address_bookDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Address_bookDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Address_bookDTO> address_bookDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Address_bookDTO address_bookDTO = new Address_bookDTO();
				address_bookDTO.iD = rs.getLong("ID");
				address_bookDTO.nameEn = rs.getString("name_en");
				address_bookDTO.nameBn = rs.getString("name_bn");
				address_bookDTO.createdBy = rs.getString("created_by");
				address_bookDTO.isDeleted = rs.getInt("isDeleted");
				address_bookDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				address_bookDTOList.add(address_bookDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return address_bookDTOList;
	
	}
				
}
	