package address_book;
import java.util.*; 
import util.*;


public class AddressBookDetailMAPS extends CommonMaps
{	
	public AddressBookDetailMAPS(String tableName)
	{
		
		java_allfield_type_map.put("address_book_id".toLowerCase(), "Long");
		java_allfield_type_map.put("employee_organogram_id".toLowerCase(), "Long");
		java_allfield_type_map.put("employee_designation".toLowerCase(), "String");
		java_allfield_type_map.put("employee_office_unit".toLowerCase(), "String");
		java_allfield_type_map.put("employee_email".toLowerCase(), "String");
		java_allfield_type_map.put("name".toLowerCase(), "String");
		java_allfield_type_map.put("phone_number".toLowerCase(), "String");


		java_anyfield_search_map.put(tableName + ".address_book_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".employee_organogram_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".employee_designation".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".employee_office_unit".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".employee_email".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".name".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".phone_number".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("addressBookId".toLowerCase(), "addressBookId".toLowerCase());
		java_DTO_map.put("employeeOrganogramId".toLowerCase(), "employeeOrganogramId".toLowerCase());
		java_DTO_map.put("employeeDesignation".toLowerCase(), "employeeDesignation".toLowerCase());
		java_DTO_map.put("employeeOfficeUnit".toLowerCase(), "employeeOfficeUnit".toLowerCase());
		java_DTO_map.put("employeeEmail".toLowerCase(), "employeeEmail".toLowerCase());
		java_DTO_map.put("name".toLowerCase(), "name".toLowerCase());
		java_DTO_map.put("phoneNumber".toLowerCase(), "phoneNumber".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("address_book_id".toLowerCase(), "addressBookId".toLowerCase());
		java_SQL_map.put("employee_organogram_id".toLowerCase(), "employeeOrganogramId".toLowerCase());
		java_SQL_map.put("employee_designation".toLowerCase(), "employeeDesignation".toLowerCase());
		java_SQL_map.put("employee_office_unit".toLowerCase(), "employeeOfficeUnit".toLowerCase());
		java_SQL_map.put("employee_email".toLowerCase(), "employeeEmail".toLowerCase());
		java_SQL_map.put("name".toLowerCase(), "name".toLowerCase());
		java_SQL_map.put("phone_number".toLowerCase(), "phoneNumber".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Address Book Id".toLowerCase(), "addressBookId".toLowerCase());
		java_Text_map.put("Employee Organogram Id".toLowerCase(), "employeeOrganogramId".toLowerCase());
		java_Text_map.put("Employee Designation".toLowerCase(), "employeeDesignation".toLowerCase());
		java_Text_map.put("Employee Office Unit".toLowerCase(), "employeeOfficeUnit".toLowerCase());
		java_Text_map.put("Employee Email".toLowerCase(), "employeeEmail".toLowerCase());
		java_Text_map.put("Name".toLowerCase(), "name".toLowerCase());
		java_Text_map.put("Phone Number".toLowerCase(), "phoneNumber".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}