package address_book;

import java.awt.*;
import java.io.IOException;
import java.io.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.List;
import java.util.UUID;

import address_book.Constants;




import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Address_bookServlet
 */
@WebServlet("/Address_bookServlet")
@MultipartConfig
public class Address_bookServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Address_bookServlet.class);

    String tableName = "address_book";

	Address_bookDAO address_bookDAO;
	CommonRequestHandler commonRequestHandler;
	AddressBookDetailDAO addressBookDetailDAO;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Address_bookServlet() 
	{
        super();
    	try
    	{
			address_bookDAO = new Address_bookDAO(tableName);
			addressBookDetailDAO = new AddressBookDetailDAO("address_book_detail");
			commonRequestHandler = new CommonRequestHandler(address_bookDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ADDRESS_BOOK_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				/*else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}*/
			}
			else if(actionType.equals("getEditPage"))
			{
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ADDRESS_BOOK_UPDATE))
				{
					getAddress_book(request, response);					
				}
				/*else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}*/						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ADDRESS_BOOK_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchAddress_book(request, response, isPermanentTable, filter);
						}
						else
						{
							searchAddress_book(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchAddress_book(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ADDRESS_BOOK_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				/*else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}*/
				
			}
			else if( actionType.equals( "getDetailsByGroupId" ) )
			{
				///if( PermissionRepository.checkPermissionByRoleIDAndMenuID( userDTO.roleID, MenuConstants.ADDRESS_BOOK_ADD ) )
				{
					getDetailsByGroupId( request, response );
				}
				/*else 
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}*/
			}
			else if( actionType.equals( "getDetailsByGroupId" ) ){

				//if( PermissionRepository.checkPermissionByRoleIDAndMenuID( userDTO.roleID, MenuConstants.ADDRESS_BOOK_ADD ) )
				{

					getDetailsByGroupId( request, response );
				}
				/*else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}*/
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDetailsByGroupId(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String groupIdStr = request.getParameter( "id" );
		Long groupId = Long.parseLong( groupIdStr );

		List<AddressBookDetailDTO> addressBookDetailDTOList = addressBookDetailDAO.getAddressBookDetailDTOListByAddressBookID( groupId );
		String res = gson.toJson( addressBookDetailDTOList );
		response.setContentType( "application/json" );
		response.getWriter().write( res );
	}

	private void viewAddress_book(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String ID = request.getParameter("ID");
		String modal = request.getParameter("modal");
		RequestDispatcher rd;
		if(modal != null && modal.equalsIgnoreCase("1"))
		{
			 rd = request.getRequestDispatcher("address_book/address_bookViewModal.jsp?ID=" + ID);
		}
		else
		{
			 rd = request.getRequestDispatcher("address_book/address_bookView.jsp?ID=" + ID);
		}
		rd.forward(request, response);
		
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ADDRESS_BOOK_ADD))
				{
					System.out.println("going to  addAddress_book ");
					addAddress_book(request, response, true, userDTO, true);
				}
				/*else
				{
					System.out.println("Not going to  addAddress_book ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}*/
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ADDRESS_BOOK_ADD))
				{
					getDTO(request, response);					
				}
				/*else
				{
					System.out.println("Not going to  addAddress_book ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}*/
				
			}
			else if(actionType.equals("edit"))
			{
				
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ADDRESS_BOOK_UPDATE))
				{					
					addAddress_book(request, response, false, userDTO, isPermanentTable);					
				}
				/*else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}*/
			}
			else if(actionType.equals("delete"))
			{								
				deleteAddress_book(request, response, userDTO);				
			}	
			else if(actionType.equals("search"))
			{
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ADDRESS_BOOK_SEARCH))
				{
					searchAddress_book(request, response, true, "");
				}
				/*else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}*/
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Address_bookDTO address_bookDTO = (Address_bookDTO)address_bookDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(address_bookDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addAddress_book(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addAddress_book");
			String path = getServletContext().getRealPath("/img2/");
			Address_bookDTO address_bookDTO;
						
			if(addFlag == true)
			{
				address_bookDTO = new Address_bookDTO();
			}
			else
			{
				address_bookDTO = (Address_bookDTO)address_bookDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("nameEn");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEn = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				address_bookDTO.nameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nameBn");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameBn = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				address_bookDTO.nameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("createdBy");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("createdBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				address_bookDTO.createdBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addAddress_book dto = " + address_bookDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				address_bookDAO.setIsDeleted(address_bookDTO.iD, CommonDTO.OUTDATED);
				returnedID = address_bookDAO.add(address_bookDTO);
				address_bookDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = address_bookDAO.manageWriteOperations(address_bookDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = address_bookDAO.manageWriteOperations(address_bookDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			List<AddressBookDetailDTO> addressBookDetailDTOList = createAddressBookDetailDTOListByRequest(request);			
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(addressBookDetailDTOList != null)
				{				
					for(AddressBookDetailDTO addressBookDetailDTO: addressBookDetailDTOList)
					{
						addressBookDetailDTO.addressBookId = address_bookDTO.iD; 
						addressBookDetailDAO.add(addressBookDetailDTO);
					}
				}
			
			}
			else
			{
				List<Long> childIdsFromRequest = addressBookDetailDAO.getChildIdsFromRequest(request, "addressBookDetail");
				//delete the removed children
				addressBookDetailDAO.deleteChildrenNotInList("address_book", "address_book_detail", address_bookDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = addressBookDetailDAO.getChilIds("address_book", "address_book_detail", address_bookDTO.iD);
				
				
				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							AddressBookDetailDTO addressBookDetailDTO =  createAddressBookDetailDTOByRequestAndIndex(request, false, i);
							addressBookDetailDTO.addressBookId = address_bookDTO.iD; 
							addressBookDetailDAO.update(addressBookDetailDTO);
						}
						else
						{
							AddressBookDetailDTO addressBookDetailDTO =  createAddressBookDetailDTOByRequestAndIndex(request, true, i);
							addressBookDetailDTO.addressBookId = address_bookDTO.iD; 
							addressBookDetailDAO.add(addressBookDetailDTO);
						}
					}
				}
				else
				{
					addressBookDetailDAO.deleteAddressBookDetailByAddressBookID(address_bookDTO.iD);
				}
				
			}
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getAddress_book(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Address_bookServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(address_bookDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	private List<AddressBookDetailDTO> createAddressBookDetailDTOListByRequest(HttpServletRequest request) throws Exception{ 
		List<AddressBookDetailDTO> addressBookDetailDTOList = new ArrayList<AddressBookDetailDTO>();
		if(request.getParameterValues("addressBookDetail.iD") != null) 
		{
			int addressBookDetailItemNo = request.getParameterValues("addressBookDetail.iD").length;
			
			
			for(int index=0;index<addressBookDetailItemNo;index++){
				AddressBookDetailDTO addressBookDetailDTO = createAddressBookDetailDTOByRequestAndIndex(request,true,index);
				addressBookDetailDTOList.add(addressBookDetailDTO);
			}
			
			return addressBookDetailDTOList;
		}
		return null;
	}
	
	
	private AddressBookDetailDTO createAddressBookDetailDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{
	
		AddressBookDetailDTO addressBookDetailDTO;
		if(addFlag == true )
		{
			addressBookDetailDTO = new AddressBookDetailDTO();
		}
		else
		{
			addressBookDetailDTO = (AddressBookDetailDTO)addressBookDetailDAO.getDTOByID(Long.parseLong(request.getParameterValues("addressBookDetail.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		
		
		
				
		String Value = "";
		Value = request.getParameterValues("addressBookDetail.addressBookId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		addressBookDetailDTO.addressBookId = Long.parseLong(request.getParameterValues("addressBookDetail.addressBookId")[index]);
		Value = request.getParameterValues("addressBookDetail.employeeOrganogramId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		
		if(request.getParameterValues("addressBookDetail.employeeOrganogramId")[index]!= null &&
				!request.getParameterValues("addressBookDetail.employeeOrganogramId")[index].equalsIgnoreCase(""))
		{

			addressBookDetailDTO.employeeOrganogramId = Long.parseLong(request.getParameterValues("addressBookDetail.employeeOrganogramId")[index]);
			Value = request.getParameterValues("addressBookDetail.employeeDesignation")[index];
	
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
		}

		addressBookDetailDTO.employeeDesignation = (request.getParameterValues("addressBookDetail.employeeDesignation")[index]);
		Value = request.getParameterValues("addressBookDetail.employeeOfficeUnit")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		addressBookDetailDTO.employeeOfficeUnit = (request.getParameterValues("addressBookDetail.employeeOfficeUnit")[index]);
		Value = request.getParameterValues("addressBookDetail.employeeEmail")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		addressBookDetailDTO.employeeEmail = (request.getParameterValues("addressBookDetail.employeeEmail")[index]);
		Value = request.getParameterValues("addressBookDetail.name")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		addressBookDetailDTO.name = (request.getParameterValues("addressBookDetail.name")[index]);
		Value = request.getParameterValues("addressBookDetail.phoneNumber")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		addressBookDetailDTO.phoneNumber = (request.getParameterValues("addressBookDetail.phoneNumber")[index]);
		return addressBookDetailDTO;
	
	}
	
	
	

	private void deleteAddress_book(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				
				Address_bookDTO address_bookDTO = (Address_bookDTO)address_bookDAO.getDTOByID(id);
				address_bookDAO.manageWriteOperations(address_bookDTO, SessionConstants.DELETE, id, userDTO);
				addressBookDetailDAO.deleteAddressBookDetailByAddressBookID(id);
				response.sendRedirect("Address_bookServlet?actionType=search");
				
			}			
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		
	}

	private void getAddress_book(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getAddress_book");
		Address_bookDTO address_bookDTO = null;
		try 
		{
			address_bookDTO = (Address_bookDTO)address_bookDAO.getDTOByID(id);
			request.setAttribute("ID", address_bookDTO.iD);
			request.setAttribute("address_bookDTO",address_bookDTO);
			request.setAttribute("address_bookDAO",address_bookDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "address_book/address_bookInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "address_book/address_bookSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "address_book/address_bookEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "address_book/address_bookEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getAddress_book(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getAddress_book(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchAddress_book(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchAddress_book 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_ADDRESS_BOOK,
			request,
			address_bookDAO,
			SessionConstants.VIEW_ADDRESS_BOOK,
			SessionConstants.SEARCH_ADDRESS_BOOK,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("address_bookDAO",address_bookDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to address_book/address_bookApproval.jsp");
	        	rd = request.getRequestDispatcher("address_book/address_bookApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to address_book/address_bookApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("address_book/address_bookApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to address_book/address_bookSearch.jsp");
	        	rd = request.getRequestDispatcher("address_book/address_bookSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to address_book/address_bookSearchForm.jsp");
	        	rd = request.getRequestDispatcher("address_book/address_bookSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
}

