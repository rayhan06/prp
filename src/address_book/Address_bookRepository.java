package address_book;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Address_bookRepository implements Repository {
	Address_bookDAO address_bookDAO = null;
	
	public void setDAO(Address_bookDAO address_bookDAO)
	{
		this.address_bookDAO = address_bookDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Address_bookRepository.class);
	Map<Long, Address_bookDTO>mapOfAddress_bookDTOToiD;
	Map<String, Set<Address_bookDTO> >mapOfAddress_bookDTOTonameEn;
	Map<String, Set<Address_bookDTO> >mapOfAddress_bookDTOTonameBn;
	Map<String, Set<Address_bookDTO> >mapOfAddress_bookDTOTocreatedBy;
	Map<Long, Set<Address_bookDTO> >mapOfAddress_bookDTOTolastModificationTime;


	static Address_bookRepository instance = null;  
	private Address_bookRepository(){
		mapOfAddress_bookDTOToiD = new ConcurrentHashMap<>();
		mapOfAddress_bookDTOTonameEn = new ConcurrentHashMap<>();
		mapOfAddress_bookDTOTonameBn = new ConcurrentHashMap<>();
		mapOfAddress_bookDTOTocreatedBy = new ConcurrentHashMap<>();
		mapOfAddress_bookDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Address_bookRepository getInstance(){
		if (instance == null){
			instance = new Address_bookRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(address_bookDAO == null)
		{
			return;
		}
		try {
			List<Address_bookDTO> address_bookDTOs = address_bookDAO.getAllAddress_book(reloadAll);
			for(Address_bookDTO address_bookDTO : address_bookDTOs) {
				Address_bookDTO oldAddress_bookDTO = getAddress_bookDTOByID(address_bookDTO.iD);
				if( oldAddress_bookDTO != null ) {
					mapOfAddress_bookDTOToiD.remove(oldAddress_bookDTO.iD);
				
					if(mapOfAddress_bookDTOTonameEn.containsKey(oldAddress_bookDTO.nameEn)) {
						mapOfAddress_bookDTOTonameEn.get(oldAddress_bookDTO.nameEn).remove(oldAddress_bookDTO);
					}
					if(mapOfAddress_bookDTOTonameEn.get(oldAddress_bookDTO.nameEn).isEmpty()) {
						mapOfAddress_bookDTOTonameEn.remove(oldAddress_bookDTO.nameEn);
					}
					
					if(mapOfAddress_bookDTOTonameBn.containsKey(oldAddress_bookDTO.nameBn)) {
						mapOfAddress_bookDTOTonameBn.get(oldAddress_bookDTO.nameBn).remove(oldAddress_bookDTO);
					}
					if(mapOfAddress_bookDTOTonameBn.get(oldAddress_bookDTO.nameBn).isEmpty()) {
						mapOfAddress_bookDTOTonameBn.remove(oldAddress_bookDTO.nameBn);
					}
					
					if(mapOfAddress_bookDTOTocreatedBy.containsKey(oldAddress_bookDTO.createdBy)) {
						mapOfAddress_bookDTOTocreatedBy.get(oldAddress_bookDTO.createdBy).remove(oldAddress_bookDTO);
					}
					if(mapOfAddress_bookDTOTocreatedBy.get(oldAddress_bookDTO.createdBy).isEmpty()) {
						mapOfAddress_bookDTOTocreatedBy.remove(oldAddress_bookDTO.createdBy);
					}
					
					if(mapOfAddress_bookDTOTolastModificationTime.containsKey(oldAddress_bookDTO.lastModificationTime)) {
						mapOfAddress_bookDTOTolastModificationTime.get(oldAddress_bookDTO.lastModificationTime).remove(oldAddress_bookDTO);
					}
					if(mapOfAddress_bookDTOTolastModificationTime.get(oldAddress_bookDTO.lastModificationTime).isEmpty()) {
						mapOfAddress_bookDTOTolastModificationTime.remove(oldAddress_bookDTO.lastModificationTime);
					}
					
					
				}
				if(address_bookDTO.isDeleted == 0) 
				{
					
					mapOfAddress_bookDTOToiD.put(address_bookDTO.iD, address_bookDTO);
				
					if( ! mapOfAddress_bookDTOTonameEn.containsKey(address_bookDTO.nameEn)) {
						mapOfAddress_bookDTOTonameEn.put(address_bookDTO.nameEn, new HashSet<>());
					}
					mapOfAddress_bookDTOTonameEn.get(address_bookDTO.nameEn).add(address_bookDTO);
					
					if( ! mapOfAddress_bookDTOTonameBn.containsKey(address_bookDTO.nameBn)) {
						mapOfAddress_bookDTOTonameBn.put(address_bookDTO.nameBn, new HashSet<>());
					}
					mapOfAddress_bookDTOTonameBn.get(address_bookDTO.nameBn).add(address_bookDTO);
					
					if( ! mapOfAddress_bookDTOTocreatedBy.containsKey(address_bookDTO.createdBy)) {
						mapOfAddress_bookDTOTocreatedBy.put(address_bookDTO.createdBy, new HashSet<>());
					}
					mapOfAddress_bookDTOTocreatedBy.get(address_bookDTO.createdBy).add(address_bookDTO);
					
					if( ! mapOfAddress_bookDTOTolastModificationTime.containsKey(address_bookDTO.lastModificationTime)) {
						mapOfAddress_bookDTOTolastModificationTime.put(address_bookDTO.lastModificationTime, new HashSet<>());
					}
					mapOfAddress_bookDTOTolastModificationTime.get(address_bookDTO.lastModificationTime).add(address_bookDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Address_bookDTO> getAddress_bookList() {
		List <Address_bookDTO> address_books = new ArrayList<Address_bookDTO>(this.mapOfAddress_bookDTOToiD.values());
		return address_books;
	}
	
	
	public Address_bookDTO getAddress_bookDTOByID( long ID){
		return mapOfAddress_bookDTOToiD.get(ID);
	}
	
	
	public List<Address_bookDTO> getAddress_bookDTOByname_en(String name_en) {
		return new ArrayList<>( mapOfAddress_bookDTOTonameEn.getOrDefault(name_en,new HashSet<>()));
	}
	
	
	public List<Address_bookDTO> getAddress_bookDTOByname_bn(String name_bn) {
		return new ArrayList<>( mapOfAddress_bookDTOTonameBn.getOrDefault(name_bn,new HashSet<>()));
	}
	
	
	public List<Address_bookDTO> getAddress_bookDTOBycreated_by(String created_by) {
		return new ArrayList<>( mapOfAddress_bookDTOTocreatedBy.getOrDefault(created_by,new HashSet<>()));
	}
	
	
	public List<Address_bookDTO> getAddress_bookDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfAddress_bookDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "address_book";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


