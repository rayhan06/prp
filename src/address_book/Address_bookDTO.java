package address_book;
import java.util.*; 
import util.*; 


public class Address_bookDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
    public String createdBy = "";
	
	public List<AddressBookDetailDTO> addressBookDetailDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Address_bookDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " createdBy = " + createdBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}