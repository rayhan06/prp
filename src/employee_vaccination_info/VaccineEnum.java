package employee_vaccination_info;


import pb.OptionDTO;
import pb.Utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum VaccineEnum {
    OXFORD("Oxford–AstraZeneca", "অক্সফোর্ড-এস্ট্রোজেনিকা", 1),
    PFIZER("Pfizer–BioNTech", "ফাইজার-বায়োনটেক", 2),
    JANSSEN("Janssen", "জেনসন", 3),
    SINOPHARM("Sinopharm(Verocell)", "সিনোফার্ম(ভেরোসেল)", 4),
    SPUTNIK("Sputnik V", "স্পুটনিক ভি", 5),
    CORONAVAC("CoronaVac", "করোনা ভ্যাক", 6),
    COVAXIN("Covaxin", "কোভ্যাক্সিন", 7),
    COVISHIELD("Covishield", "কোভিশিল্ড", 8),
    MODERNA("Moderna", "মডার্না", 9);

    private final String engText;
    private final String bngText;
    private final int value;

    VaccineEnum(String engText, String bngText, int value) {
        this.engText = engText;
        this.bngText = bngText;
        this.value = value;
    }

    public String getEngText() {
        return engText;
    }

    public String getBngText() {
        return bngText;
    }

    public int getValue() {
        return value;
    }

    private static Map<Integer, VaccineEnum> mapEnumByValue = null;

    private static void loadMap() {
        if (mapEnumByValue == null) {
            synchronized (VaccineEnum.class) {
                if (mapEnumByValue == null) {
                    mapEnumByValue = Stream.of(VaccineEnum.values())
                            .collect(Collectors.toMap(e -> e.value, e -> e));
                }
            }
        }
    }

    public static String getBuildOptions(String language, int selectedId) {

        List<OptionDTO> list = Arrays.stream(VaccineEnum.values())
                .map(statusEnum -> new OptionDTO(statusEnum.engText, statusEnum.bngText, String.valueOf(statusEnum.value)))
                .collect(Collectors.toList());
        return "English".equalsIgnoreCase(language) ? Utils.buildOptions(list, "English", String.valueOf(selectedId)) : Utils.buildOptions(list, "Bangla", String.valueOf(selectedId));
    }

    public static String getTextByValue(String language, int value) {

        if (mapEnumByValue == null) {
            loadMap();
        }
        return mapEnumByValue.get(value) == null ? "" : "English".equalsIgnoreCase(language) ? mapEnumByValue.get(value).engText : mapEnumByValue.get(value).bngText;
    }

    public static VaccineEnum getByValue(int value) {
        if (mapEnumByValue == null) {
            loadMap();
        }
        return mapEnumByValue.get(value);
    }
}