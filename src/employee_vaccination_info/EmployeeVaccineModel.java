package employee_vaccination_info;

public class EmployeeVaccineModel {
    public int doseNumber;
    public long doseTime;
    public String certificateNumber = "";
    public int vaccineName = -1;
    public String vaccineCenter = "";
    public Employee_vaccination_infoDTO dto;
}
