package employee_vaccination_info;

import common.BaseServlet;
import common.EmployeeServletService;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import pb.Utils;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;


@WebServlet("/Employee_vaccination_infoServlet")
@MultipartConfig
public class Employee_vaccination_infoServlet extends BaseServlet implements EmployeeServletService {
    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        long currentTime = System.currentTimeMillis();
        Employee_vaccination_infoDTO vaccinationInfoDTO;
        if (addFlag) {
            vaccinationInfoDTO = new Employee_vaccination_infoDTO();
            vaccinationInfoDTO.insertedBy = userDTO.ID;
            vaccinationInfoDTO.insertionTime = currentTime;
        } else {
            vaccinationInfoDTO = Employee_vaccination_infoDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        vaccinationInfoDTO.modifiedBy = userDTO.ID;
        vaccinationInfoDTO.lastModificationTime = currentTime;
        vaccinationInfoDTO.employeeRecordsId = Utils.parseMandatoryLong(
                request.getParameter("employeeRecordsId"),
                isLangEng ? "No Employee found!" : "কর্মকর্তা/কর্মচারীর তথ্য পাওয়া যায়নি"
        );
        Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().getById(vaccinationInfoDTO.employeeRecordsId);
        vaccinationInfoDTO.employeeNameEn = employee_recordsDTO.nameEng;
        vaccinationInfoDTO.employeeNameBn = employee_recordsDTO.nameBng;

        vaccinationInfoDTO.doseNumber = Utils.parseMandatoryInt(
                request.getParameter("doseNumber"),
                -1,
                isLangEng ? "Enter dose number" : "কত তম ডোস সেটা লিখুন"
        );
        vaccinationInfoDTO.doseTime = Utils.parseMandatoryDate(
                request.getParameter("doseTime"),
                isLangEng ? "Select Date of Vaccination" : "ভ্যাক্সিন গ্রহণের তারিখ বাছাই করুন"
        );
        vaccinationInfoDTO.vaccineName = Utils.parseMandatoryInt(
                request.getParameter("vaccineName"),
                -1,
                isLangEng ? "Select Vaccine Name" : "ভ্যাক্সিনের নাম বাছাই করুন"
        );
        vaccinationInfoDTO.certificateNumber = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("certificateNumber"));
        vaccinationInfoDTO.vaccineCenter = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("vaccineCenter"));

        if (addFlag) {
            Employee_vaccination_infoDAO.getInstance().add(vaccinationInfoDTO);
        } else {
            Employee_vaccination_infoDAO.getInstance().update(vaccinationInfoDTO);
        }
        return vaccinationInfoDTO;
    }

    @Override
    public String getTableName() {
        return Employee_vaccination_infoDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Employee_vaccination_infoServlet";
    }

    @Override
    public Employee_vaccination_infoDAO getCommonDAOService() {
        return Employee_vaccination_infoDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.EMPLOYEE_VACCINATION_INFO_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.EMPLOYEE_VACCINATION_INFO_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.EMPLOYEE_VACCINATION_INFO_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Employee_vaccination_infoServlet.class;
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request, 6, "vaccine");
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request, 6, "vaccine");
    }

    @Override
    public String getDeleteRedirectURL(HttpServletRequest request) {
        return getAddOrEditOrDeleteRedirectURL(request, 6, "vaccine");
    }

    @Override
    public boolean getEditPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getViewPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getEditPermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getAddPermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    @Override
    public boolean getAddPagePermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }
}

