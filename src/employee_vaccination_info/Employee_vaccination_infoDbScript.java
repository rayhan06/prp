package employee_vaccination_info;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Employee_vaccination_infoDbScript {
    private static final Logger logger = Logger.getLogger(Employee_vaccination_infoDbScript.class);
    private static final Gson GSON = new Gson();
    private static final Employee_vaccination_infoDAO DAO = Employee_vaccination_infoDAO.getInstance();
    private static final String undoAddQuery = "update employee_vaccination_info " +
                                               "set isDeleted = 1 " +
                                               "where ID = %d;";
    private static final String undoUpdateQuery = "update employee_vaccination_info " +
                                                  "set dose_time = -62135791200000, " +
                                                  "dose_number = -1, " +
                                                  "lastModificationTime = %d " +
                                                  "where ID = %d;";


    private static Employee_vaccination_infoDTO clone(Employee_vaccination_infoDTO dto) {
        return GSON.fromJson(GSON.toJson(dto), Employee_vaccination_infoDTO.class);
    }

    private static List<Employee_vaccination_infoDTO> getOtherDosesInfo(Employee_vaccination_infoDTO infoDto) {
        List<Employee_vaccination_infoDTO> list = new ArrayList<>();
        infoDto.doseTime = infoDto.dose1Time;
        infoDto.doseNumber = 1;
        list.add(infoDto);
        if (infoDto.dose2Time != SessionConstants.MIN_DATE) {
            Employee_vaccination_infoDTO dose2 = clone(infoDto);
            dose2.iD = -1;
            infoDto.doseTime = infoDto.dose2Time;
            infoDto.doseNumber = 2;
            list.add(dose2);
        }
        if (infoDto.boosterTime != SessionConstants.MIN_DATE) {
            Employee_vaccination_infoDTO dose3 = clone(infoDto);
            dose3.iD = -1;
            infoDto.doseTime = infoDto.boosterTime;
            infoDto.doseNumber = 3;
            list.add(dose3);
        }
        return list;
    }

    private static String addOrUpdate(Employee_vaccination_infoDTO dto) {
        try {
            if (dto.iD < 0) {
                DAO.add(dto);
                return String.format(undoAddQuery, dto.iD);
            } else {
                DAO.update(dto);
                return String.format(undoUpdateQuery, System.currentTimeMillis(), dto.iD);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return null;
        }
    }

    public static void main(String[] args) throws IOException {
        String undoScript = DAO.getAllDTOs()
                               .stream()
                               .map(Employee_vaccination_infoDbScript::getOtherDosesInfo)
                               .flatMap(Collection::stream)
                               .map(Employee_vaccination_infoDbScript::addOrUpdate)
                               .filter(Objects::nonNull)
                               .collect(Collectors.joining("\n"));
        String fileName = "./src/employee_vaccination_info/undo_script_"
                          + System.currentTimeMillis()
                          + ".sql";
        try (FileWriter fileWriter = new FileWriter(fileName)) {
            fileWriter.write(undoScript);
        }
    }
}
