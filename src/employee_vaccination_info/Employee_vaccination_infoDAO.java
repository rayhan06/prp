package employee_vaccination_info;

import common.EmployeeCommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class Employee_vaccination_infoDAO implements EmployeeCommonDAOService<Employee_vaccination_infoDTO> {
    private static final Logger logger = Logger.getLogger(Employee_vaccination_infoDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (employee_records_id,employee_name_en,employee_name_bn,dose_number,dose_time,"
                    .concat("certificate_number,vaccine_name,vaccine_center,search_column,modified_by,lastModificationTime,")
                    .concat("inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET employee_records_id=?,employee_name_en=?,employee_name_bn=?,dose_number=?,dose_time=?,"
                    .concat("certificate_number=?,vaccine_name=?,vaccine_center=?,search_column=?,")
                    .concat("modified_by=?,lastModificationTime=? WHERE ID=?");

    private final Map<String, String> searchMap = new HashMap<>();

    private Employee_vaccination_infoDAO() {
        searchMap.put("employee_records_id_internal", " and (employee_records_id = ?)");
        searchMap.put("employee_records_id", " and (employee_records_id = ?)");
        searchMap.put("employee_name_en", " and (employee_name_en like ?)");
        searchMap.put("employee_name_bn", " and (employee_name_bn like ?)");
        searchMap.put("vaccine_center", " and (vaccine_center like ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }


    private static class LazyLoader {
        static final Employee_vaccination_infoDAO INSTANCE = new Employee_vaccination_infoDAO();
    }

    public static Employee_vaccination_infoDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Employee_vaccination_infoDTO employee_vaccination_infoDTO) {
        employee_vaccination_infoDTO.searchColumn =
                employee_vaccination_infoDTO.employeeNameEn + " "
                + employee_vaccination_infoDTO.employeeNameBn + " "
                + employee_vaccination_infoDTO.certificateNumber + " "
                + employee_vaccination_infoDTO.vaccineCenter + " ";
    }

    @Override
    public void set(PreparedStatement ps, Employee_vaccination_infoDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(dto);
        ps.setLong(++index, dto.employeeRecordsId);
        ps.setObject(++index, dto.employeeNameEn);
        ps.setObject(++index, dto.employeeNameBn);
        ps.setInt(++index, dto.doseNumber);
        ps.setLong(++index, dto.doseTime);
        ps.setString(++index, dto.certificateNumber);
        ps.setInt(++index, dto.vaccineName);
        ps.setString(++index, dto.vaccineCenter);
        ps.setString(++index, dto.searchColumn);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0/* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Employee_vaccination_infoDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Employee_vaccination_infoDTO dto = new Employee_vaccination_infoDTO();
            dto.iD = rs.getLong("ID");
            dto.employeeRecordsId = rs.getLong("employee_records_id");
            dto.employeeNameEn = rs.getString("employee_name_en");
            dto.employeeNameBn = rs.getString("employee_name_bn");
            dto.isFullyVaccinated = rs.getBoolean("is_fully_vaccinated");
            dto.totalDoses = rs.getInt("total_doses");
            dto.dose1Time = rs.getLong("dose1_time");
            dto.dose2Time = rs.getLong("dose2_time");
            dto.boosterTime = rs.getLong("booster_time");
            dto.doseNumber = rs.getInt("dose_number");
            dto.doseTime = rs.getLong("dose_time");
            dto.certificateNumber = rs.getString("certificate_number");
            dto.vaccineName = rs.getInt("vaccine_name");
            dto.vaccineCenter = rs.getString("vaccine_center");
            dto.searchColumn = rs.getString("search_column");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Employee_vaccination_infoDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "employee_vaccination_info";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_vaccination_infoDTO) commonDTO, addSqlQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_vaccination_infoDTO) commonDTO, updateSqlQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

    public List<EmployeeVaccineModel> getEmployeeVaccineModelListByEmployeeId(long employeeId, Long selectedId) {
        return getByEmployeeId(employeeId)
                .stream()
                .filter(dto -> (selectedId == null || dto.iD != selectedId) && dto.isDeleted == 0)
                .sorted(Comparator.comparingInt(dto -> dto.doseNumber))
                .map(this::buildEmployeeVaccineModel)
                .collect(Collectors.toList());
    }

    private EmployeeVaccineModel buildEmployeeVaccineModel(Employee_vaccination_infoDTO vaccinationInfoDTO) {
        EmployeeVaccineModel model = new EmployeeVaccineModel();
        model.dto = vaccinationInfoDTO;
        model.doseNumber = vaccinationInfoDTO.doseNumber;
        model.doseTime = vaccinationInfoDTO.doseTime;
        model.certificateNumber = vaccinationInfoDTO.certificateNumber;
        model.vaccineCenter = vaccinationInfoDTO.vaccineCenter;
        model.vaccineName = vaccinationInfoDTO.vaccineName;
        return model;
    }
}
	