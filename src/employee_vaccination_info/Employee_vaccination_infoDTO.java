package employee_vaccination_info;

import sessionmanager.SessionConstants;
import util.CommonEmployeeDTO;


public class Employee_vaccination_infoDTO extends CommonEmployeeDTO {

    public String employeeNameEn = "";
    public String employeeNameBn = "";
    @Deprecated
    public boolean isFullyVaccinated = false;
    @Deprecated
    public int totalDoses = 0;
    @Deprecated
    public long dose1Time = SessionConstants.MIN_DATE;

    @Deprecated
    public long dose2Time = SessionConstants.MIN_DATE;
    @Deprecated
    public long boosterTime = SessionConstants.MIN_DATE;
    public int doseNumber = 0;
    public long doseTime = SessionConstants.MIN_DATE;
    public String certificateNumber = "";
    public int vaccineName = -1;
    public String vaccineCenter = "";
    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;


    @Override
    public String toString() {
        return "$Employee_vaccination_infoDTO[" +
               " iD = " + iD +
               " employeeRecordsId = " + employeeRecordsId +
               " employeeNameEn = " + employeeNameEn +
               " employeeNameBn = " + employeeNameBn +
               " isFullyVaccinated = " + isFullyVaccinated +
               " totalDoses = " + totalDoses +
               " dose1Time = " + dose1Time +
               " dose2Time = " + dose2Time +
               " boosterTime = " + boosterTime +
               " certificateNumber = " + certificateNumber +
               " vaccineName = " + vaccineName +
               " vaccineCenter = " + vaccineCenter +
               " searchColumn = " + searchColumn +
               " insertedBy = " + insertedBy +
               " insertionTime = " + insertionTime +
               " isDeleted = " + isDeleted +
               " modifiedBy = " + modifiedBy +
               " lastModificationTime = " + lastModificationTime +
               "]";
    }

}