package asset_list;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

import employee_asset_history.Employee_asset_historyDAO;
import employee_asset_history.Employee_asset_historyDTO;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import prescription_details.Constants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;

import java.util.StringTokenizer;

import com.google.gson.Gson;

import asset_category.Asset_categoryDAO;
import asset_category.Asset_categoryDTO;
import dashboard.DashboardService;
import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import files.*;



/**
 * Servlet implementation class Asset_listServlet
 */
@WebServlet("/Asset_listServlet")
@MultipartConfig
public class Asset_listServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Asset_listServlet.class);

    String tableName = "asset_list";

	Asset_listDAO asset_listDAO;
	CommonRequestHandler commonRequestHandler;
	FilesDAO filesDAO = new FilesDAO();
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Asset_listServlet() 
	{
        super();
    	try
    	{
			asset_listDAO = new Asset_listDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(asset_listDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LIST_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LIST_UPDATE))
				{
					getAsset_list(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("downloadDropzoneFile"))
			{
				commonRequestHandler.downloadDropzoneFile(request, response, filesDAO);					
			}
			else if(actionType.equals("DeleteFileFromDropZone"))
			{
				commonRequestHandler.deleteFileFromDropZone(request, response, filesDAO);
			}
			else if(actionType.equals("ASSIGNED_ASSET_COUNT_FILTER"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LIST_SEARCH))
				{
					String filter = DashboardService.ASSIGNED_ASSET_COUNT_FILTER;
					
					searchAsset_list(request, response, isPermanentTable, filter);
					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("FREE_ASSET_COUNT_FILTER"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LIST_SEARCH))
				{
					String filter = DashboardService.FREE_ASSET_COUNT_FILTER;
					
					searchAsset_list(request, response, isPermanentTable, filter);
					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("ACCESSORY_FILTER"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LIST_SEARCH))
				{
					String filter = DashboardService.ACCESSORY_FILTER;
					
					searchAsset_list(request, response, isPermanentTable, filter);
					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LIST_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchAsset_list(request, response, isPermanentTable, filter);
						}
						else
						{
							searchAsset_list(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchAsset_list(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LIST_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			} else if(actionType.equals("assignAssetPage")) {
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LIST_SEARCH))
				{
					gotoAssignAssetPage(request,response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			} else if(actionType.equals("revokeAssetPage")) {
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LIST_SEARCH))
				{
					gotoRevokeAssetPage(request,response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void gotoAssignAssetPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String ID = request.getParameter("ID");
		Asset_listDTO assetListDTO = Asset_listRepository.getInstance().getAsset_listDTOByID(Long.parseLong(ID));
		request.setAttribute("assetListDTO",assetListDTO);
		request.setAttribute("assignOrRevoke",1);
		RequestDispatcher rd;
		rd = request.getRequestDispatcher("asset_list/assign_revoke_asset.jsp");
		rd.forward(request, response);
	}
	private void gotoRevokeAssetPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String ID = request.getParameter("ID");
		Asset_listDTO assetListDTO = Asset_listRepository.getInstance().getAsset_listDTOByID(Long.parseLong(ID));
		request.setAttribute("assetListDTO",assetListDTO);
		request.setAttribute("assignOrRevoke",2);
		RequestDispatcher rd;
		rd = request.getRequestDispatcher("asset_list/assign_revoke_asset.jsp");
		rd.forward(request, response);
	}
	
	String getUnassignedOption(Asset_listDTO assset_listDTO, long id)
	{
		String text = 
				(assset_listDTO.iD + 100000) + ": "
			+ assset_listDTO.nameEn ;
		
		String option = 
		"<option value = '" +  assset_listDTO.iD + "'"
		+ (id ==  assset_listDTO.iD? "selected":"")	
		+ ">" + text + "</option>";
		
		return option;
	}
	
	String getPCOption(Asset_listDTO assset_listDTO, long id)
	{
		String text = 
				(assset_listDTO.iD + 100000) + ": "
			+ assset_listDTO.nameEn + ", CPU: " 
			+ assset_listDTO.cpu + ", RAM:" + assset_listDTO.ram;
		
		String option = 
		"<option value = '" +  assset_listDTO.iD + "'"
		+ (id ==  assset_listDTO.iD? "selected":"")
		+ " ram = '" + assset_listDTO.ram + "'"
		+ " hdd = '" + assset_listDTO.hdd + "'"
		+ " cpu = '" + assset_listDTO.cpu + "'"
		+ " pcMac = '" + assset_listDTO.mac + "'"
		+ " os = '" + assset_listDTO.os + "'"
		+ " has_preactivated_os = '" + assset_listDTO.hasPreactivatedOs + "'"
		+ " licenceKey = '" + assset_listDTO.osLicenceNumber + "'"
		+ ">" + text + "</option>";
		
		return option;
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LIST_ADD))
				{
					System.out.println("going to  addAsset_list ");
					addAsset_list(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addAsset_list ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("UploadFilesFromDropZone"))
			{
				commonRequestHandler.UploadFilesFromDropZone(request, response, userDTO);
			}
			else if(actionType.equals("getModelsWithCat"))
			{
				long cat = Long.parseLong(request.getParameter("cat"));
				String language = request.getParameter("language");
				String options = CommonDAO.getOptionsWithWhere(language, "asset_model", "asset_category_type = " + cat);
				options = "<option value = ''>"
						+ LM.getText(LC.HM_SELECT, loginDTO)
						+ "</option>" + options;
				response.getWriter().write(options);
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LIST_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addAsset_list ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LIST_UPDATE))
				{					
					addAsset_list(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getUnassignedPCs"))
			{
				long id = Long.parseLong(request.getParameter("id"));
				
				List<Asset_listDTO> unassignedPCs = asset_listDAO.getUnassignedPCs();
				String options = "";
				if(id == -1)
				{
					options = "<option value='-1'>" + LM.getText(LC.HM_SELECT, loginDTO) + "</option>";
				}
				else
				{
					Asset_listDTO assset_listDTO = asset_listDAO.getDTOByID(id);
					options += getPCOption(assset_listDTO, id);					
				}
				for(Asset_listDTO assset_listDTO: unassignedPCs)
				{
					options += getPCOption(assset_listDTO, id);	
				}
				options += "<option value='-1'>" + LM.getText(LC.HM_NONE) + "</option>";
				response.getWriter().write(options);
			}
			else if(actionType.equals("getUnassigned"))
			{
				long id = Long.parseLong(request.getParameter("id"));
				int cat = Integer.parseInt(request.getParameter("cat"));
				
				List<Asset_listDTO> unassigned = asset_listDAO.getUnassigned(cat);
				String options = "";
				if(id == -1)
				{
					options = "<option value='-1'>" + LM.getText(LC.HM_SELECT, loginDTO) + "</option>";
				}
				else
				{
					Asset_listDTO assset_listDTO = asset_listDAO.getDTOByID(id);
					options += getUnassignedOption(assset_listDTO, id);					
				}
				for(Asset_listDTO assset_listDTO: unassigned)
				{
					options += getUnassignedOption(assset_listDTO, id);	
				}
				options += "<option value='-1'>" + LM.getText(LC.HM_NONE) + "</option>";
				response.getWriter().write(options);
			}
			else if(actionType.equals("getExtra"))
			{
				long id = Long.parseLong(request.getParameter("id"));
				int cat = Integer.parseInt(request.getParameter("cat"));
				
				String options = CommonDAO.getOptionsWithWhere("English", "asset_list", id, 
						" (asset_category_type = " + cat + " and (quantity > 0 or id = " + id +  "))"
						);

				
				options += "<option value='-1'>" + LM.getText(LC.HM_NONE) + "</option>";
				response.getWriter().write(options);
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LIST_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LIST_SEARCH))
				{
					searchAsset_list(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			} else if(actionType.equals("assignAsset")) {
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LIST_SEARCH))
				{
					assignAsset(request,response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			} else if(actionType.equals("revokeAsset")) {
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LIST_SEARCH))
				{
					revokeAsset(request,response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void revokeAsset(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String value = "";

		value = request.getParameter("revokeEmployeeRecordsId");
		long employeeRecordsId = 0;
		if(value != null && !value.equalsIgnoreCase("nan"))
		{
			value = Jsoup.clean(value,Whitelist.simpleText());
			employeeRecordsId = Long.parseLong(value);
		}

		value = request.getParameter("assetListId");
		long assetListId = 0;
		if(value != null && !value.equalsIgnoreCase("nan"))
		{
			value = Jsoup.clean(value,Whitelist.simpleText());
			assetListId = Long.parseLong(value);
		}

		value = request.getParameter("remarks");
		if(value != null && !value.equalsIgnoreCase("nan"))
		{
			value = Jsoup.clean(value,Whitelist.simpleText());
		}

		addEmployeeAssetHistory(employeeRecordsId, assetListId, value, AssetListConstant.REVOKED);
		Asset_listDTO asset_listDTO = Asset_listRepository.getInstance().getAsset_listDTOByID(assetListId);
		asset_listDTO.isAssigned = false;
		asset_listDTO.employeeRecordsId = 0;
		asset_listDTO.status = AssetListConstant.REVOKED; // change status to revoke
		new Asset_listDAO().update(asset_listDTO);

		response.sendRedirect("Asset_listServlet?actionType=search");
	}

	private void assignAsset(HttpServletRequest request, HttpServletResponse response) throws Exception{
		String value = "";

		value = request.getParameter("employeeRecordsId");
		long employeeRecordsId = 0;
		if(value != null && !value.equalsIgnoreCase("nan"))
		{
			value = Jsoup.clean(value,Whitelist.simpleText());
			employeeRecordsId = Long.parseLong(value);
		}

		value = request.getParameter("assetListId");
		long assetListId = 0;
		if(value != null && !value.equalsIgnoreCase("nan"))
		{
			value = Jsoup.clean(value,Whitelist.simpleText());
			assetListId = Long.parseLong(value);
		}

		value = request.getParameter("remarks");
		if(value != null && !value.equalsIgnoreCase("nan"))
		{
			value = Jsoup.clean(value,Whitelist.simpleText());
		}

		addEmployeeAssetHistory(employeeRecordsId, assetListId, value, AssetListConstant.ASSIGNED);
		Asset_listDTO asset_listDTO = Asset_listRepository.getInstance().getAsset_listDTOByID(assetListId);
		asset_listDTO.isAssigned = true;
		asset_listDTO.employeeRecordsId = employeeRecordsId;
		asset_listDTO.status = AssetListConstant.ASSIGNED; // change status to assigned
		new Asset_listDAO().update(asset_listDTO);

		response.sendRedirect("Asset_listServlet?actionType=search");
	}

	private void addEmployeeAssetHistory(long employeeRecordsId, long assetListId, String remarks, int status) throws Exception {

		Employee_asset_historyDTO employee_asset_historyDTO = new Employee_asset_historyDTO();
		employee_asset_historyDTO.employeeRecordsId = employeeRecordsId;
		employee_asset_historyDTO.assetListId = assetListId;
		employee_asset_historyDTO.remarks = remarks;
		employee_asset_historyDTO.status = status;
		employee_asset_historyDTO.insertionDate = System.currentTimeMillis();

		new Employee_asset_historyDAO().add(employee_asset_historyDTO);
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Asset_listDTO asset_listDTO = asset_listDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(asset_listDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addAsset_list(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addAsset_list");
			String path = getServletContext().getRealPath("/img2/");
			Asset_listDTO asset_listDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				asset_listDTO = new Asset_listDTO();
			}
			else
			{
				asset_listDTO = asset_listDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("assetCategoryType");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("assetCategoryType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_listDTO.assetCategoryType = Long.parseLong(Value);
				Asset_categoryDAO asset_categoryDAO = new Asset_categoryDAO();
				Asset_categoryDTO asset_categoryDTO = asset_categoryDAO.getDTOByID(asset_listDTO.assetCategoryType);
				if(asset_categoryDTO != null)
				{
					asset_listDTO.isWholeSale = asset_categoryDTO.isAssignable;
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("assetModelType");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("assetModelType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_listDTO.assetModelType = Long.parseLong(Value);
				asset_listDTO.nameEn = CommonDAO.getName("english", "asset_model", asset_listDTO.assetModelType);
				asset_listDTO.nameBn = CommonDAO.getName("bangla", "asset_model", asset_listDTO.assetModelType);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("ram");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("ram = " + Value);
			if(Value != null)
			{
				asset_listDTO.ram = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("cpu");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("cpu = " + Value);
			if(Value != null)
			{
				asset_listDTO.cpu = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("hdd");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("hdd = " + Value);
			if(Value != null)
			{
				asset_listDTO.hdd = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("mac");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("mac = " + Value);
			if(Value != null)
			{
				asset_listDTO.mac = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("os");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("os = " + Value);
			if(Value != null)
			{
				asset_listDTO.os = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("hasPreactivatedOs");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("hasPreactivatedOs = " + Value);
            asset_listDTO.hasPreactivatedOs = Value != null && !Value.equalsIgnoreCase("");

			Value = request.getParameter("osLicenceNumber");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("osLicenceNumber = " + Value);
			if(Value != null)
			{
				asset_listDTO.osLicenceNumber = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("assetSupplierType");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("assetSupplierType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_listDTO.assetSupplierType = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("tag");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("tag = " + Value);
			if(Value != null)
			{
				asset_listDTO.tag = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("orderNo");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("orderNo = " + Value);
			if(Value != null)
			{
				asset_listDTO.orderNo = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("purchaseCost");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("purchaseCost = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_listDTO.purchaseCost = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("warranty");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("warranty = " + Value);
			if(Value != null)
			{
				asset_listDTO.warranty = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("description");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("description = " + Value);
			if(Value != null)
			{
				asset_listDTO.description = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("vendorName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("vendorName = " + Value);
			if(Value != null)
			{
				asset_listDTO.vendorName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("vendorPhone");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("vendorPhone = " + Value);
			if(Value != null)
			{
				asset_listDTO.vendorPhone = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("vendorEmail");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("vendorEmail = " + Value);
			if(Value != null)
			{
				asset_listDTO.vendorEmail = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("supportDuration");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("supportDuration = " + Value);
			if(Value != null)
			{
				asset_listDTO.supportDuration = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("imageDropzone");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("imageDropzone = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				System.out.println("imageDropzone = " + Value);
				
				asset_listDTO.imageDropzone = Long.parseLong(Value);
				
				
				if(addFlag == false)
				{
					String imageDropzoneFilesToDelete = request.getParameter("imageDropzoneFilesToDelete");
					String[] deleteArray = imageDropzoneFilesToDelete.split(",");
					for(int i = 0; i < deleteArray.length; i ++)
					{
						System.out.println("going to delete " + deleteArray[i]);
						if(i>0)
						{
							filesDAO.delete(Long.parseLong(deleteArray[i]));
						}
					}
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			

			Value = request.getParameter("employeeRecordsId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeRecordsId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_listDTO.employeeRecordsId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("status");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("status = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_listDTO.status = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("quantity");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("quantity = " + Value);
			if(Value != null)
			{
				asset_listDTO.quantity = Integer.parseInt(Value);
				asset_listDTO.remainingQuantity = asset_listDTO.quantity;
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				asset_listDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				asset_listDTO.insertedByOrganogramId = userDTO.organogramID;
			}
			
			Value = request.getParameter("purchaseDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("purchaseDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try 
				{
					Date d = f.parse(Value);
					asset_listDTO.purchaseDate = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				asset_listDTO.insertionDate = c.getTimeInMillis();
			}			


			asset_listDTO.lastModifierUser = userDTO.userName;

			
			System.out.println("Done adding  addAsset_list dto = " + asset_listDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				asset_listDAO.setIsDeleted(asset_listDTO.iD, CommonDTO.OUTDATED);
				returnedID = asset_listDAO.add(asset_listDTO);
				asset_listDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = asset_listDAO.manageWriteOperations(asset_listDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = asset_listDAO.manageWriteOperations(asset_listDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getAsset_list(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Asset_listServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(asset_listDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void getAsset_list(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getAsset_list");
		Asset_listDTO asset_listDTO = null;
		try 
		{
			asset_listDTO = asset_listDAO.getDTOByID(id);
			request.setAttribute("ID", asset_listDTO.iD);
			request.setAttribute("asset_listDTO",asset_listDTO);
			request.setAttribute("asset_listDAO",asset_listDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "asset_list/asset_listInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "asset_list/asset_listSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "asset_list/asset_listEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "asset_list/asset_listEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getAsset_list(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getAsset_list(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchAsset_list(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchAsset_list 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_ASSET_LIST,
			request,
			asset_listDAO,
			SessionConstants.VIEW_ASSET_LIST,
			SessionConstants.SEARCH_ASSET_LIST,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("asset_listDAO",asset_listDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to asset_list/asset_listApproval.jsp");
	        	rd = request.getRequestDispatcher("asset_list/asset_listApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to asset_list/asset_listApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("asset_list/asset_listApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to asset_list/asset_listSearch.jsp");
	        	rd = request.getRequestDispatcher("asset_list/asset_listSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to asset_list/asset_listSearchForm.jsp");
	        	rd = request.getRequestDispatcher("asset_list/asset_listSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

