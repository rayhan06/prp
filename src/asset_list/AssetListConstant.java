package asset_list;

public class AssetListConstant {
    public static final Integer READY_TO_DEPLOY = 1;
    public static final Integer ASSIGNED = 2;
    public static final Integer IN_MAINTENANCE = 3;
    public static final Integer REVOKED = 4;
    public static final Integer LOST_OR_STOLEN = 5;
    public static final Integer ARCHIVED = 6;

}
