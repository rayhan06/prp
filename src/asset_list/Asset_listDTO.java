package asset_list;
import java.util.*;

import sessionmanager.SessionConstants;
import util.*; 


public class Asset_listDTO extends CommonDTO
{

	public long assetCategoryType = -1;
	public long assetModelType = -1;
    public String ram = "";
    public String cpu = "";
    public String hdd = "";
    public String mac = "";
    public String os = "";
	public boolean hasPreactivatedOs = false;
    public String osLicenceNumber = "";
	public long assetSupplierType = -1;
    public String tag = "";
    public String orderNo = "";
	public double purchaseCost = -1;
    public String warranty = "";
    public String description = "";
    public String vendorName = "";
    public String vendorPhone = "";
    public String vendorEmail = "";
    public String supportDuration = "";
	public long imageDropzone = -1;
	public boolean isAssigned = false;
	public long employeeRecordsId = -1;
	public int status = SessionConstants.ASSET_STATUS_READY_TO_DEPLOY;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	public long purchaseDate = 0;
    public String lastModifierUser = "";
    
    public int remainingQuantity = 0;
    public boolean isWholeSale = false;
    
    
    public String nameEn = "";
    public String nameBn = "";
    
    public int quantity = 1;
    
	
	
    @Override
	public String toString() {
            return "$Asset_listDTO[" +
            " iD = " + iD +
            " assetCategoryType = " + assetCategoryType +
            " assetModelType = " + assetModelType +
            " ram = " + ram +
            " cpu = " + cpu +
            " hdd = " + hdd +
            " mac = " + mac +
            " hasPreactivatedOs = " + hasPreactivatedOs +
            " osLicenceNumber = " + osLicenceNumber +
            " assetSupplierType = " + assetSupplierType +
            " tag = " + tag +
            " orderNo = " + orderNo +
            " purchaseCost = " + purchaseCost +
            " warranty = " + warranty +
            " description = " + description +
            " vendorName = " + vendorName +
            " vendorPhone = " + vendorPhone +
            " vendorEmail = " + vendorEmail +
            " supportDuration = " + supportDuration +
            " imageDropzone = " + imageDropzone +
            " isAssigned = " + isAssigned +
            " employeeRecordsId = " + employeeRecordsId +
            " status = " + status +
            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}