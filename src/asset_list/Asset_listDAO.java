package asset_list;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import appointment.KeyCountDTO;
import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Asset_listDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Asset_listDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Asset_listMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"asset_category_type",
			"asset_model_type",
			"ram",
			"cpu",
			"hdd",
			"mac",
			"has_preactivated_os",
			"os_licence_number",
			"asset_supplier_type",
			"tag",
			"order_no",
			"purchase_cost",
			"warranty",
			"description",
			"vendor_name",
			"vendor_phone",
			"vendor_email",
			"support_duration",
			"image_dropzone",
			"is_assigned",
			"employee_records_id",
			"status",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"os",
			"name_en",
			"name_bn",
			"quantity",

			"purchase_date",
			"remaining_quantity",
			"is_wholesale",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Asset_listDAO()
	{
		this("asset_list");		
	}
	
	public void setSearchColumn(Asset_listDTO asset_listDTO)
	{
		asset_listDTO.searchColumn = "";
		asset_listDTO.searchColumn += CommonDAO.getName("English", "asset_category", asset_listDTO.assetCategoryType) + " " + CommonDAO.getName("Bangla", "asset_category", asset_listDTO.assetCategoryType) + " ";
		asset_listDTO.searchColumn += CommonDAO.getName("English", "asset_model", asset_listDTO.assetModelType) + " " + CommonDAO.getName("Bangla", "asset_model", asset_listDTO.assetModelType) + " ";

		asset_listDTO.searchColumn += CommonDAO.getName("English", "asset_supplier", asset_listDTO.assetSupplierType) + " " + CommonDAO.getName("Bangla", "asset_supplier", asset_listDTO.assetSupplierType) + " ";
		asset_listDTO.searchColumn += asset_listDTO.tag + " ";
		asset_listDTO.searchColumn += asset_listDTO.orderNo + " ";

		asset_listDTO.searchColumn += asset_listDTO.vendorName + " ";
		asset_listDTO.searchColumn += asset_listDTO.vendorPhone + " ";
		asset_listDTO.searchColumn += asset_listDTO.vendorEmail + " ";

		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Asset_listDTO asset_listDTO = (Asset_listDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(asset_listDTO);
		if(isInsert)
		{
			ps.setObject(index++,asset_listDTO.iD);
		}
		ps.setObject(index++,asset_listDTO.assetCategoryType);
		ps.setObject(index++,asset_listDTO.assetModelType);
		ps.setObject(index++,asset_listDTO.ram);
		ps.setObject(index++,asset_listDTO.cpu);
		ps.setObject(index++,asset_listDTO.hdd);
		ps.setObject(index++,asset_listDTO.mac);
		ps.setObject(index++,asset_listDTO.hasPreactivatedOs);
		ps.setObject(index++,asset_listDTO.osLicenceNumber);
		ps.setObject(index++,asset_listDTO.assetSupplierType);
		ps.setObject(index++,asset_listDTO.tag);
		ps.setObject(index++,asset_listDTO.orderNo);
		ps.setObject(index++,asset_listDTO.purchaseCost);
		ps.setObject(index++,asset_listDTO.warranty);
		ps.setObject(index++,asset_listDTO.description);
		ps.setObject(index++,asset_listDTO.vendorName);
		ps.setObject(index++,asset_listDTO.vendorPhone);
		ps.setObject(index++,asset_listDTO.vendorEmail);
		ps.setObject(index++,asset_listDTO.supportDuration);
		ps.setObject(index++,asset_listDTO.imageDropzone);
		ps.setObject(index++,asset_listDTO.isAssigned);
		ps.setObject(index++,asset_listDTO.employeeRecordsId);
		ps.setObject(index++,asset_listDTO.status);
		ps.setObject(index++,asset_listDTO.searchColumn);
		ps.setObject(index++,asset_listDTO.insertedByUserId);
		ps.setObject(index++,asset_listDTO.insertedByOrganogramId);
		ps.setObject(index++,asset_listDTO.insertionDate);
		ps.setObject(index++,asset_listDTO.lastModifierUser);
		ps.setObject(index++,asset_listDTO.os);
		ps.setObject(index++,asset_listDTO.nameEn);
		ps.setObject(index++,asset_listDTO.nameBn);
		ps.setObject(index++,asset_listDTO.quantity);

		ps.setObject(index++,asset_listDTO.purchaseDate);
		ps.setObject(index++,asset_listDTO.remainingQuantity);
		ps.setObject(index++,asset_listDTO.isWholeSale);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Asset_listDTO build(ResultSet rs)
	{
		try
		{
			Asset_listDTO asset_listDTO = new Asset_listDTO();
			asset_listDTO.iD = rs.getLong("ID");
			asset_listDTO.assetCategoryType = rs.getLong("asset_category_type");
			asset_listDTO.assetModelType = rs.getLong("asset_model_type");
			asset_listDTO.ram = rs.getString("ram");
			asset_listDTO.cpu = rs.getString("cpu");
			asset_listDTO.hdd = rs.getString("hdd");
			asset_listDTO.mac = rs.getString("mac");
			asset_listDTO.hasPreactivatedOs = rs.getBoolean("has_preactivated_os");
			asset_listDTO.osLicenceNumber = rs.getString("os_licence_number");
			asset_listDTO.assetSupplierType = rs.getLong("asset_supplier_type");
			asset_listDTO.tag = rs.getString("tag");
			asset_listDTO.orderNo = rs.getString("order_no");
			asset_listDTO.purchaseCost = rs.getDouble("purchase_cost");
			asset_listDTO.warranty = rs.getString("warranty");
			asset_listDTO.description = rs.getString("description");
			asset_listDTO.vendorName = rs.getString("vendor_name");
			asset_listDTO.vendorPhone = rs.getString("vendor_phone");
			asset_listDTO.vendorEmail = rs.getString("vendor_email");
			asset_listDTO.supportDuration = rs.getString("support_duration");
			asset_listDTO.imageDropzone = rs.getLong("image_dropzone");
			asset_listDTO.isAssigned = rs.getBoolean("is_assigned");
			asset_listDTO.employeeRecordsId = rs.getLong("employee_records_id");
			asset_listDTO.status = rs.getInt("status");
			asset_listDTO.searchColumn = rs.getString("search_column");
			asset_listDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			asset_listDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			asset_listDTO.insertionDate = rs.getLong("insertion_date");
			asset_listDTO.lastModifierUser = rs.getString("last_modifier_user");
			asset_listDTO.os = rs.getString("os");
			asset_listDTO.nameEn = rs.getString("name_en");
			asset_listDTO.nameBn = rs.getString("name_bn");
			asset_listDTO.quantity = rs.getInt("quantity");

			asset_listDTO.purchaseDate = rs.getLong("purchase_date");
			asset_listDTO.remainingQuantity = rs.getInt("remaining_quantity");
			asset_listDTO.isWholeSale = rs.getBoolean("is_wholesale");
			asset_listDTO.isDeleted = rs.getInt("isDeleted");
			asset_listDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return asset_listDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

	public Asset_listDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Asset_listDTO asset_listDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return asset_listDTO;
	}
	
	public int getSum(ResultSet rs)
	{
		try {
			return rs.getInt("sum(quantity)");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	
	}

	public int getSumQuantity (String filter)
	{
		String sql = "SELECT sum(quantity) ";

		sql += " FROM " + tableName;
		
        sql += " WHERE isDeleted = 0 ";
        
        if(filter != null && !filter.equalsIgnoreCase(""))
        {
        	sql += " and " + filter;
        }
        
        return ConnectionAndStatementUtil.getT(sql,this::getSum, 0);
	}
	
	public int getSumRemaining(ResultSet rs)
	{
		try {
			return rs.getInt("sum(remaining_quantity)");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	
	}
	
	public KeyCountDTO getAssetCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("is_assigned");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public List<KeyCountDTO> getAssetStatusCount()
    {
		String sql = "SELECT is_assigned, count(id) FROM asset_list where isdeleted = 0 and is_wholesale = 0 group by is_assigned";				
		return ConnectionAndStatementUtil.getListOfT(sql,this::getAssetCount);	
    }
	
	public int getSumRemainingQuantity (String filter)
	{
		String sql = "SELECT sum(remaining_quantity) ";

		sql += " FROM " + tableName;
		
        sql += " WHERE isDeleted = 0 ";
        
        if(filter != null && !filter.equalsIgnoreCase(""))
        {
        	sql += " and " + filter;
        }
		
        if(filter != null && !filter.equalsIgnoreCase(""))
        {
        	sql += " and " + filter;
        }
        
        return ConnectionAndStatementUtil.getT(sql,this::getSumRemaining, 0);
	}
	
	public long unassign(long ID) throws Exception {

		long lastModificationTime = System.currentTimeMillis();
		String sql = "UPDATE " + tableName;

		sql += " SET is_assigned = 0, lastModificationTime = " + lastModificationTime + ", remaining_quantity = remaining_quantity + 1, "
				+ " employee_records_id = -1, "
				+ " status =  " + SessionConstants.ASSET_STATUS_READY_TO_DEPLOY
				+ " WHERE ID = " + ID + " and is_assigned = 1";
		return (long) ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            Connection connection = model.getConnection();
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                ps.executeUpdate();
                recordUpdateTime(connection, lastModificationTime);
                return ID;
            } catch (SQLException ex) {
                logger.error(ex);
                return -1L;
            }
        }, sql);
	}
	
	public long assign(long ID, long organogramId) throws Exception {
		long lastModificationTime = System.currentTimeMillis();
		String sql = "UPDATE " + tableName;

		sql += " SET is_assigned = 1, lastModificationTime = " + lastModificationTime + ", remaining_quantity = remaining_quantity - 1, "
				+ " employee_records_id = " + organogramId + ","
				+ " status =  " + SessionConstants.ASSET_STATUS_ASSIGNED
				+ " WHERE ID = " + ID + " and is_assigned = 0";

		printSql(sql);
		return (long) ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            Connection connection = model.getConnection();
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                ps.executeUpdate();
                recordUpdateTime(connection, lastModificationTime);
                return ID;
            } catch (SQLException ex) {
                logger.error(ex);
                return -1L;
            }
        }, sql);
	}
	
	public long increaseCount(long ID) throws Exception {
		long lastModificationTime = System.currentTimeMillis();
		String sql = "UPDATE " + tableName;

		sql += " SET  lastModificationTime = " + lastModificationTime + ", quantity = quantity + 1"
				+ " WHERE ID = " + ID;

		printSql(sql);
		return (long) ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            Connection connection = model.getConnection();
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                ps.executeUpdate();
                recordUpdateTime(connection, lastModificationTime);
                return ID;
            } catch (SQLException ex) {
                logger.error(ex);
                return -1L;
            }
        }, sql);
	}
	
	public long reduceCount(long ID) throws Exception {

		long lastModificationTime = System.currentTimeMillis();
		String sql = "UPDATE " + tableName;

		sql += " SET  lastModificationTime = " + lastModificationTime + ", remaining_quantity = remaining_quantity - 1"
				+ " WHERE ID = " + ID + " and quantity > 0";
		printSql(sql);
		
		return (long) ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            Connection connection = model.getConnection();
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                ps.executeUpdate();
                recordUpdateTime(connection, lastModificationTime);
                return ID;
            } catch (SQLException ex) {
                logger.error(ex);
                return -1L;
            }
        }, sql);		
	}
	
	public List<Asset_listDTO> getUnassigned(int asset_category_type){
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
        
        sql += " WHERE isDeleted = 0 and is_assigned = 0 and asset_category_type =  " + asset_category_type;
        printSql(sql);

		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	
	}
	
	public List<Asset_listDTO> getUnassignedPCs(){
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
        
        sql += " WHERE isDeleted = 0 and is_assigned = 0 and asset_category_type in ( ";

        sql += SessionConstants.ASSET_CAT_LAPTOP + ", " + SessionConstants.ASSET_CAT_DESKTOP + ", " + SessionConstants.ASSET_CAT_TABLET + ")";
		
		
		printSql(sql);

		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	
	}

	
	public List<Asset_listDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<Asset_listDTO> getAllAsset_list (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Asset_listDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Asset_listDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("asset_category_type")
						|| str.equals("asset_model_type")
						|| str.equals("employee_records_id")
						
						|| str.equals("tag")
						|| str.equals("order_no")
					
						|| str.equals("vendor_name")
						|| str.equals("vendor_phone")
						
						|| str.equals("status")
						|| str.equals("asset_supplier_type")

						|| str.equals("support_duration")
						|| str.equals("purchase_date_start")
						|| str.equals("purchase_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("asset_category_type"))
					{
						AllFieldSql += "" + tableName + ".asset_category_type = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("asset_model_type"))
					{
						AllFieldSql += "" + tableName + ".asset_model_type = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("employee_records_id"))
					{
						AllFieldSql += "" + tableName + ".employee_records_id = '" + p_searchCriteria.get(str) + "'";
						i ++;
					}
					else if(str.equals("status"))
					{
						AllFieldSql += "" + tableName + ".status = '" + p_searchCriteria.get(str) + "'";
						i ++;
					}
					else if(str.equals("asset_supplier_type"))
					{
						AllFieldSql += "" + tableName + ".asset_supplier_type = '" + p_searchCriteria.get(str) + "'";
						i ++;
					}
					
					else if(str.equals("tag"))
					{
						AllFieldSql += "" + tableName + ".tag like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("order_no"))
					{
						AllFieldSql += "" + tableName + ".order_no like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					
					else if(str.equals("vendor_name"))
					{
						AllFieldSql += "" + tableName + ".vendor_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("vendor_phone"))
					{
						AllFieldSql += "" + tableName + ".vendor_phone like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					
					else if(str.equals("support_duration"))
					{
						AllFieldSql += "" + tableName + ".support_duration like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("purchase_date_start"))
					{
						AllFieldSql += "" + tableName + ".purchase_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("purchase_date_end"))
					{
						AllFieldSql += "" + tableName + ".purchase_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	