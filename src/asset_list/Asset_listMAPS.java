package asset_list;
import java.util.*; 
import util.*;


public class Asset_listMAPS extends CommonMaps
{	
	public Asset_listMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("assetCategoryType".toLowerCase(), "assetCategoryType".toLowerCase());
		java_DTO_map.put("assetModelType".toLowerCase(), "assetModelType".toLowerCase());
		java_DTO_map.put("ram".toLowerCase(), "ram".toLowerCase());
		java_DTO_map.put("cpu".toLowerCase(), "cpu".toLowerCase());
		java_DTO_map.put("hdd".toLowerCase(), "hdd".toLowerCase());
		java_DTO_map.put("mac".toLowerCase(), "mac".toLowerCase());
		java_DTO_map.put("hasPreactivatedOs".toLowerCase(), "hasPreactivatedOs".toLowerCase());
		java_DTO_map.put("osLicenceNumber".toLowerCase(), "osLicenceNumber".toLowerCase());
		java_DTO_map.put("assetSupplierType".toLowerCase(), "assetSupplierType".toLowerCase());
		java_DTO_map.put("tag".toLowerCase(), "tag".toLowerCase());
		java_DTO_map.put("orderNo".toLowerCase(), "orderNo".toLowerCase());
		java_DTO_map.put("purchaseCost".toLowerCase(), "purchaseCost".toLowerCase());
		java_DTO_map.put("warranty".toLowerCase(), "warranty".toLowerCase());
		java_DTO_map.put("description".toLowerCase(), "description".toLowerCase());
		java_DTO_map.put("vendorName".toLowerCase(), "vendorName".toLowerCase());
		java_DTO_map.put("vendorPhone".toLowerCase(), "vendorPhone".toLowerCase());
		java_DTO_map.put("vendorEmail".toLowerCase(), "vendorEmail".toLowerCase());
		java_DTO_map.put("supportDuration".toLowerCase(), "supportDuration".toLowerCase());
		java_DTO_map.put("imageDropzone".toLowerCase(), "imageDropzone".toLowerCase());
		java_DTO_map.put("isAssigned".toLowerCase(), "isAssigned".toLowerCase());
		java_DTO_map.put("employeeRecordsId".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("lastModifierUser".toLowerCase(), "lastModifierUser".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("asset_category_type".toLowerCase(), "assetCategoryType".toLowerCase());
		java_SQL_map.put("asset_model_type".toLowerCase(), "assetModelType".toLowerCase());
		java_SQL_map.put("ram".toLowerCase(), "ram".toLowerCase());
		java_SQL_map.put("cpu".toLowerCase(), "cpu".toLowerCase());
		java_SQL_map.put("hdd".toLowerCase(), "hdd".toLowerCase());
		java_SQL_map.put("mac".toLowerCase(), "mac".toLowerCase());
		java_SQL_map.put("has_preactivated_os".toLowerCase(), "hasPreactivatedOs".toLowerCase());
		java_SQL_map.put("os_licence_number".toLowerCase(), "osLicenceNumber".toLowerCase());
		java_SQL_map.put("asset_supplier_type".toLowerCase(), "assetSupplierType".toLowerCase());
		java_SQL_map.put("tag".toLowerCase(), "tag".toLowerCase());
		java_SQL_map.put("order_no".toLowerCase(), "orderNo".toLowerCase());
		java_SQL_map.put("purchase_cost".toLowerCase(), "purchaseCost".toLowerCase());
		java_SQL_map.put("warranty".toLowerCase(), "warranty".toLowerCase());
		java_SQL_map.put("description".toLowerCase(), "description".toLowerCase());
		java_SQL_map.put("vendor_name".toLowerCase(), "vendorName".toLowerCase());
		java_SQL_map.put("vendor_phone".toLowerCase(), "vendorPhone".toLowerCase());
		java_SQL_map.put("vendor_email".toLowerCase(), "vendorEmail".toLowerCase());
		java_SQL_map.put("support_duration".toLowerCase(), "supportDuration".toLowerCase());
		java_SQL_map.put("image_dropzone".toLowerCase(), "imageDropzone".toLowerCase());
		java_SQL_map.put("is_assigned".toLowerCase(), "isAssigned".toLowerCase());
		java_SQL_map.put("employee_records_id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_SQL_map.put("status".toLowerCase(), "status".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Asset Category".toLowerCase(), "assetCategoryType".toLowerCase());
		java_Text_map.put("Asset Model".toLowerCase(), "assetModelType".toLowerCase());
		java_Text_map.put("Ram".toLowerCase(), "ram".toLowerCase());
		java_Text_map.put("Cpu".toLowerCase(), "cpu".toLowerCase());
		java_Text_map.put("Hdd".toLowerCase(), "hdd".toLowerCase());
		java_Text_map.put("Mac".toLowerCase(), "mac".toLowerCase());
		java_Text_map.put("Has Preactivated Os".toLowerCase(), "hasPreactivatedOs".toLowerCase());
		java_Text_map.put("Os Licence Number".toLowerCase(), "osLicenceNumber".toLowerCase());
		java_Text_map.put("Asset Supplier".toLowerCase(), "assetSupplierType".toLowerCase());
		java_Text_map.put("Tag".toLowerCase(), "tag".toLowerCase());
		java_Text_map.put("Order No".toLowerCase(), "orderNo".toLowerCase());
		java_Text_map.put("Purchase Cost".toLowerCase(), "purchaseCost".toLowerCase());
		java_Text_map.put("Warranty".toLowerCase(), "warranty".toLowerCase());
		java_Text_map.put("Description".toLowerCase(), "description".toLowerCase());
		java_Text_map.put("Vendor Name".toLowerCase(), "vendorName".toLowerCase());
		java_Text_map.put("Vendor Phone".toLowerCase(), "vendorPhone".toLowerCase());
		java_Text_map.put("Vendor Email".toLowerCase(), "vendorEmail".toLowerCase());
		java_Text_map.put("Support Duration".toLowerCase(), "supportDuration".toLowerCase());
		java_Text_map.put("Image Dropzone".toLowerCase(), "imageDropzone".toLowerCase());
		java_Text_map.put("Is Assigned".toLowerCase(), "isAssigned".toLowerCase());
		java_Text_map.put("Employee Records Id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Last Modifier User".toLowerCase(), "lastModifierUser".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}