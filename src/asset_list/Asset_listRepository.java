package asset_list;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Asset_listRepository implements Repository {
	Asset_listDAO asset_listDAO = null;
	
	public void setDAO(Asset_listDAO asset_listDAO)
	{
		this.asset_listDAO = asset_listDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Asset_listRepository.class);
	Map<Long, Asset_listDTO>mapOfAsset_listDTOToiD;
	Map<Long, Set<Asset_listDTO> >mapOfAsset_listDTOToassetCategoryType;
	Map<Long, Set<Asset_listDTO> >mapOfAsset_listDTOToassetModelType;
	Map<String, Set<Asset_listDTO> >mapOfAsset_listDTOToram;
	Map<String, Set<Asset_listDTO> >mapOfAsset_listDTOTocpu;
	Map<String, Set<Asset_listDTO> >mapOfAsset_listDTOTohdd;
	Map<String, Set<Asset_listDTO> >mapOfAsset_listDTOTomac;
	Map<Boolean, Set<Asset_listDTO> >mapOfAsset_listDTOTohasPreactivatedOs;
	Map<String, Set<Asset_listDTO> >mapOfAsset_listDTOToosLicenceNumber;
	Map<Long, Set<Asset_listDTO> >mapOfAsset_listDTOToassetSupplierType;
	Map<String, Set<Asset_listDTO> >mapOfAsset_listDTOTotag;
	Map<String, Set<Asset_listDTO> >mapOfAsset_listDTOToorderNo;
	Map<Double, Set<Asset_listDTO> >mapOfAsset_listDTOTopurchaseCost;
	Map<String, Set<Asset_listDTO> >mapOfAsset_listDTOTowarranty;
	Map<String, Set<Asset_listDTO> >mapOfAsset_listDTOTodescription;
	Map<String, Set<Asset_listDTO> >mapOfAsset_listDTOTovendorName;
	Map<String, Set<Asset_listDTO> >mapOfAsset_listDTOTovendorPhone;
	Map<String, Set<Asset_listDTO> >mapOfAsset_listDTOTovendorEmail;
	Map<String, Set<Asset_listDTO> >mapOfAsset_listDTOTosupportDuration;
	Map<Long, Set<Asset_listDTO> >mapOfAsset_listDTOToimageDropzone;
	Map<Boolean, Set<Asset_listDTO> >mapOfAsset_listDTOToisAssigned;
	Map<Long, Set<Asset_listDTO> >mapOfAsset_listDTOToemployeeRecordsId;
	Map<Integer, Set<Asset_listDTO> >mapOfAsset_listDTOTostatus;
	Map<String, Set<Asset_listDTO> >mapOfAsset_listDTOTosearchColumn;
	Map<Long, Set<Asset_listDTO> >mapOfAsset_listDTOToinsertedByUserId;
	Map<Long, Set<Asset_listDTO> >mapOfAsset_listDTOToinsertedByOrganogramId;
	Map<Long, Set<Asset_listDTO> >mapOfAsset_listDTOToinsertionDate;
	Map<String, Set<Asset_listDTO> >mapOfAsset_listDTOTolastModifierUser;
	Map<Long, Set<Asset_listDTO> >mapOfAsset_listDTOTolastModificationTime;


	static Asset_listRepository instance = null;  
	private Asset_listRepository(){
		mapOfAsset_listDTOToiD = new ConcurrentHashMap<>();
		mapOfAsset_listDTOToassetCategoryType = new ConcurrentHashMap<>();
		mapOfAsset_listDTOToassetModelType = new ConcurrentHashMap<>();
		mapOfAsset_listDTOToram = new ConcurrentHashMap<>();
		mapOfAsset_listDTOTocpu = new ConcurrentHashMap<>();
		mapOfAsset_listDTOTohdd = new ConcurrentHashMap<>();
		mapOfAsset_listDTOTomac = new ConcurrentHashMap<>();
		mapOfAsset_listDTOTohasPreactivatedOs = new ConcurrentHashMap<>();
		mapOfAsset_listDTOToosLicenceNumber = new ConcurrentHashMap<>();
		mapOfAsset_listDTOToassetSupplierType = new ConcurrentHashMap<>();
		mapOfAsset_listDTOTotag = new ConcurrentHashMap<>();
		mapOfAsset_listDTOToorderNo = new ConcurrentHashMap<>();
		mapOfAsset_listDTOTopurchaseCost = new ConcurrentHashMap<>();
		mapOfAsset_listDTOTowarranty = new ConcurrentHashMap<>();
		mapOfAsset_listDTOTodescription = new ConcurrentHashMap<>();
		mapOfAsset_listDTOTovendorName = new ConcurrentHashMap<>();
		mapOfAsset_listDTOTovendorPhone = new ConcurrentHashMap<>();
		mapOfAsset_listDTOTovendorEmail = new ConcurrentHashMap<>();
		mapOfAsset_listDTOTosupportDuration = new ConcurrentHashMap<>();
		mapOfAsset_listDTOToimageDropzone = new ConcurrentHashMap<>();
		mapOfAsset_listDTOToisAssigned = new ConcurrentHashMap<>();
		mapOfAsset_listDTOToemployeeRecordsId = new ConcurrentHashMap<>();
		mapOfAsset_listDTOTostatus = new ConcurrentHashMap<>();
		mapOfAsset_listDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfAsset_listDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfAsset_listDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfAsset_listDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfAsset_listDTOTolastModifierUser = new ConcurrentHashMap<>();
		mapOfAsset_listDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Asset_listRepository getInstance(){
		if (instance == null){
			instance = new Asset_listRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(asset_listDAO == null)
		{
			return;
		}
		try {
			List<Asset_listDTO> asset_listDTOs = asset_listDAO.getAllAsset_list(reloadAll);
			for(Asset_listDTO asset_listDTO : asset_listDTOs) {
				Asset_listDTO oldAsset_listDTO = getAsset_listDTOByID(asset_listDTO.iD);
				if( oldAsset_listDTO != null ) {
					mapOfAsset_listDTOToiD.remove(oldAsset_listDTO.iD);
				
					if(mapOfAsset_listDTOToassetCategoryType.containsKey(oldAsset_listDTO.assetCategoryType)) {
						mapOfAsset_listDTOToassetCategoryType.get(oldAsset_listDTO.assetCategoryType).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOToassetCategoryType.get(oldAsset_listDTO.assetCategoryType).isEmpty()) {
						mapOfAsset_listDTOToassetCategoryType.remove(oldAsset_listDTO.assetCategoryType);
					}
					
					if(mapOfAsset_listDTOToassetModelType.containsKey(oldAsset_listDTO.assetModelType)) {
						mapOfAsset_listDTOToassetModelType.get(oldAsset_listDTO.assetModelType).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOToassetModelType.get(oldAsset_listDTO.assetModelType).isEmpty()) {
						mapOfAsset_listDTOToassetModelType.remove(oldAsset_listDTO.assetModelType);
					}
					
					if(mapOfAsset_listDTOToram.containsKey(oldAsset_listDTO.ram)) {
						mapOfAsset_listDTOToram.get(oldAsset_listDTO.ram).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOToram.get(oldAsset_listDTO.ram).isEmpty()) {
						mapOfAsset_listDTOToram.remove(oldAsset_listDTO.ram);
					}
					
					if(mapOfAsset_listDTOTocpu.containsKey(oldAsset_listDTO.cpu)) {
						mapOfAsset_listDTOTocpu.get(oldAsset_listDTO.cpu).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOTocpu.get(oldAsset_listDTO.cpu).isEmpty()) {
						mapOfAsset_listDTOTocpu.remove(oldAsset_listDTO.cpu);
					}
					
					if(mapOfAsset_listDTOTohdd.containsKey(oldAsset_listDTO.hdd)) {
						mapOfAsset_listDTOTohdd.get(oldAsset_listDTO.hdd).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOTohdd.get(oldAsset_listDTO.hdd).isEmpty()) {
						mapOfAsset_listDTOTohdd.remove(oldAsset_listDTO.hdd);
					}
					
					if(mapOfAsset_listDTOTomac.containsKey(oldAsset_listDTO.mac)) {
						mapOfAsset_listDTOTomac.get(oldAsset_listDTO.mac).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOTomac.get(oldAsset_listDTO.mac).isEmpty()) {
						mapOfAsset_listDTOTomac.remove(oldAsset_listDTO.mac);
					}
					
					if(mapOfAsset_listDTOTohasPreactivatedOs.containsKey(oldAsset_listDTO.hasPreactivatedOs)) {
						mapOfAsset_listDTOTohasPreactivatedOs.get(oldAsset_listDTO.hasPreactivatedOs).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOTohasPreactivatedOs.get(oldAsset_listDTO.hasPreactivatedOs).isEmpty()) {
						mapOfAsset_listDTOTohasPreactivatedOs.remove(oldAsset_listDTO.hasPreactivatedOs);
					}
					
					if(mapOfAsset_listDTOToosLicenceNumber.containsKey(oldAsset_listDTO.osLicenceNumber)) {
						mapOfAsset_listDTOToosLicenceNumber.get(oldAsset_listDTO.osLicenceNumber).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOToosLicenceNumber.get(oldAsset_listDTO.osLicenceNumber).isEmpty()) {
						mapOfAsset_listDTOToosLicenceNumber.remove(oldAsset_listDTO.osLicenceNumber);
					}
					
					if(mapOfAsset_listDTOToassetSupplierType.containsKey(oldAsset_listDTO.assetSupplierType)) {
						mapOfAsset_listDTOToassetSupplierType.get(oldAsset_listDTO.assetSupplierType).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOToassetSupplierType.get(oldAsset_listDTO.assetSupplierType).isEmpty()) {
						mapOfAsset_listDTOToassetSupplierType.remove(oldAsset_listDTO.assetSupplierType);
					}
					
					if(mapOfAsset_listDTOTotag.containsKey(oldAsset_listDTO.tag)) {
						mapOfAsset_listDTOTotag.get(oldAsset_listDTO.tag).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOTotag.get(oldAsset_listDTO.tag).isEmpty()) {
						mapOfAsset_listDTOTotag.remove(oldAsset_listDTO.tag);
					}
					
					if(mapOfAsset_listDTOToorderNo.containsKey(oldAsset_listDTO.orderNo)) {
						mapOfAsset_listDTOToorderNo.get(oldAsset_listDTO.orderNo).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOToorderNo.get(oldAsset_listDTO.orderNo).isEmpty()) {
						mapOfAsset_listDTOToorderNo.remove(oldAsset_listDTO.orderNo);
					}
					
					if(mapOfAsset_listDTOTopurchaseCost.containsKey(oldAsset_listDTO.purchaseCost)) {
						mapOfAsset_listDTOTopurchaseCost.get(oldAsset_listDTO.purchaseCost).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOTopurchaseCost.get(oldAsset_listDTO.purchaseCost).isEmpty()) {
						mapOfAsset_listDTOTopurchaseCost.remove(oldAsset_listDTO.purchaseCost);
					}
					
					if(mapOfAsset_listDTOTowarranty.containsKey(oldAsset_listDTO.warranty)) {
						mapOfAsset_listDTOTowarranty.get(oldAsset_listDTO.warranty).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOTowarranty.get(oldAsset_listDTO.warranty).isEmpty()) {
						mapOfAsset_listDTOTowarranty.remove(oldAsset_listDTO.warranty);
					}
					
					if(mapOfAsset_listDTOTodescription.containsKey(oldAsset_listDTO.description)) {
						mapOfAsset_listDTOTodescription.get(oldAsset_listDTO.description).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOTodescription.get(oldAsset_listDTO.description).isEmpty()) {
						mapOfAsset_listDTOTodescription.remove(oldAsset_listDTO.description);
					}
					
					if(mapOfAsset_listDTOTovendorName.containsKey(oldAsset_listDTO.vendorName)) {
						mapOfAsset_listDTOTovendorName.get(oldAsset_listDTO.vendorName).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOTovendorName.get(oldAsset_listDTO.vendorName).isEmpty()) {
						mapOfAsset_listDTOTovendorName.remove(oldAsset_listDTO.vendorName);
					}
					
					if(mapOfAsset_listDTOTovendorPhone.containsKey(oldAsset_listDTO.vendorPhone)) {
						mapOfAsset_listDTOTovendorPhone.get(oldAsset_listDTO.vendorPhone).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOTovendorPhone.get(oldAsset_listDTO.vendorPhone).isEmpty()) {
						mapOfAsset_listDTOTovendorPhone.remove(oldAsset_listDTO.vendorPhone);
					}
					
					if(mapOfAsset_listDTOTovendorEmail.containsKey(oldAsset_listDTO.vendorEmail)) {
						mapOfAsset_listDTOTovendorEmail.get(oldAsset_listDTO.vendorEmail).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOTovendorEmail.get(oldAsset_listDTO.vendorEmail).isEmpty()) {
						mapOfAsset_listDTOTovendorEmail.remove(oldAsset_listDTO.vendorEmail);
					}
					
					if(mapOfAsset_listDTOTosupportDuration.containsKey(oldAsset_listDTO.supportDuration)) {
						mapOfAsset_listDTOTosupportDuration.get(oldAsset_listDTO.supportDuration).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOTosupportDuration.get(oldAsset_listDTO.supportDuration).isEmpty()) {
						mapOfAsset_listDTOTosupportDuration.remove(oldAsset_listDTO.supportDuration);
					}
					
					if(mapOfAsset_listDTOToimageDropzone.containsKey(oldAsset_listDTO.imageDropzone)) {
						mapOfAsset_listDTOToimageDropzone.get(oldAsset_listDTO.imageDropzone).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOToimageDropzone.get(oldAsset_listDTO.imageDropzone).isEmpty()) {
						mapOfAsset_listDTOToimageDropzone.remove(oldAsset_listDTO.imageDropzone);
					}
					
					if(mapOfAsset_listDTOToisAssigned.containsKey(oldAsset_listDTO.isAssigned)) {
						mapOfAsset_listDTOToisAssigned.get(oldAsset_listDTO.isAssigned).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOToisAssigned.get(oldAsset_listDTO.isAssigned).isEmpty()) {
						mapOfAsset_listDTOToisAssigned.remove(oldAsset_listDTO.isAssigned);
					}
					
					if(mapOfAsset_listDTOToemployeeRecordsId.containsKey(oldAsset_listDTO.employeeRecordsId)) {
						mapOfAsset_listDTOToemployeeRecordsId.get(oldAsset_listDTO.employeeRecordsId).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOToemployeeRecordsId.get(oldAsset_listDTO.employeeRecordsId).isEmpty()) {
						mapOfAsset_listDTOToemployeeRecordsId.remove(oldAsset_listDTO.employeeRecordsId);
					}
					
					if(mapOfAsset_listDTOTostatus.containsKey(oldAsset_listDTO.status)) {
						mapOfAsset_listDTOTostatus.get(oldAsset_listDTO.status).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOTostatus.get(oldAsset_listDTO.status).isEmpty()) {
						mapOfAsset_listDTOTostatus.remove(oldAsset_listDTO.status);
					}
					
					if(mapOfAsset_listDTOTosearchColumn.containsKey(oldAsset_listDTO.searchColumn)) {
						mapOfAsset_listDTOTosearchColumn.get(oldAsset_listDTO.searchColumn).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOTosearchColumn.get(oldAsset_listDTO.searchColumn).isEmpty()) {
						mapOfAsset_listDTOTosearchColumn.remove(oldAsset_listDTO.searchColumn);
					}
					
					if(mapOfAsset_listDTOToinsertedByUserId.containsKey(oldAsset_listDTO.insertedByUserId)) {
						mapOfAsset_listDTOToinsertedByUserId.get(oldAsset_listDTO.insertedByUserId).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOToinsertedByUserId.get(oldAsset_listDTO.insertedByUserId).isEmpty()) {
						mapOfAsset_listDTOToinsertedByUserId.remove(oldAsset_listDTO.insertedByUserId);
					}
					
					if(mapOfAsset_listDTOToinsertedByOrganogramId.containsKey(oldAsset_listDTO.insertedByOrganogramId)) {
						mapOfAsset_listDTOToinsertedByOrganogramId.get(oldAsset_listDTO.insertedByOrganogramId).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOToinsertedByOrganogramId.get(oldAsset_listDTO.insertedByOrganogramId).isEmpty()) {
						mapOfAsset_listDTOToinsertedByOrganogramId.remove(oldAsset_listDTO.insertedByOrganogramId);
					}
					
					if(mapOfAsset_listDTOToinsertionDate.containsKey(oldAsset_listDTO.insertionDate)) {
						mapOfAsset_listDTOToinsertionDate.get(oldAsset_listDTO.insertionDate).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOToinsertionDate.get(oldAsset_listDTO.insertionDate).isEmpty()) {
						mapOfAsset_listDTOToinsertionDate.remove(oldAsset_listDTO.insertionDate);
					}
					
					if(mapOfAsset_listDTOTolastModifierUser.containsKey(oldAsset_listDTO.lastModifierUser)) {
						mapOfAsset_listDTOTolastModifierUser.get(oldAsset_listDTO.lastModifierUser).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOTolastModifierUser.get(oldAsset_listDTO.lastModifierUser).isEmpty()) {
						mapOfAsset_listDTOTolastModifierUser.remove(oldAsset_listDTO.lastModifierUser);
					}
					
					if(mapOfAsset_listDTOTolastModificationTime.containsKey(oldAsset_listDTO.lastModificationTime)) {
						mapOfAsset_listDTOTolastModificationTime.get(oldAsset_listDTO.lastModificationTime).remove(oldAsset_listDTO);
					}
					if(mapOfAsset_listDTOTolastModificationTime.get(oldAsset_listDTO.lastModificationTime).isEmpty()) {
						mapOfAsset_listDTOTolastModificationTime.remove(oldAsset_listDTO.lastModificationTime);
					}
					
					
				}
				if(asset_listDTO.isDeleted == 0) 
				{
					
					mapOfAsset_listDTOToiD.put(asset_listDTO.iD, asset_listDTO);
				
					if( ! mapOfAsset_listDTOToassetCategoryType.containsKey(asset_listDTO.assetCategoryType)) {
						mapOfAsset_listDTOToassetCategoryType.put(asset_listDTO.assetCategoryType, new HashSet<>());
					}
					mapOfAsset_listDTOToassetCategoryType.get(asset_listDTO.assetCategoryType).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOToassetModelType.containsKey(asset_listDTO.assetModelType)) {
						mapOfAsset_listDTOToassetModelType.put(asset_listDTO.assetModelType, new HashSet<>());
					}
					mapOfAsset_listDTOToassetModelType.get(asset_listDTO.assetModelType).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOToram.containsKey(asset_listDTO.ram)) {
						mapOfAsset_listDTOToram.put(asset_listDTO.ram, new HashSet<>());
					}
					mapOfAsset_listDTOToram.get(asset_listDTO.ram).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOTocpu.containsKey(asset_listDTO.cpu)) {
						mapOfAsset_listDTOTocpu.put(asset_listDTO.cpu, new HashSet<>());
					}
					mapOfAsset_listDTOTocpu.get(asset_listDTO.cpu).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOTohdd.containsKey(asset_listDTO.hdd)) {
						mapOfAsset_listDTOTohdd.put(asset_listDTO.hdd, new HashSet<>());
					}
					mapOfAsset_listDTOTohdd.get(asset_listDTO.hdd).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOTomac.containsKey(asset_listDTO.mac)) {
						mapOfAsset_listDTOTomac.put(asset_listDTO.mac, new HashSet<>());
					}
					mapOfAsset_listDTOTomac.get(asset_listDTO.mac).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOTohasPreactivatedOs.containsKey(asset_listDTO.hasPreactivatedOs)) {
						mapOfAsset_listDTOTohasPreactivatedOs.put(asset_listDTO.hasPreactivatedOs, new HashSet<>());
					}
					mapOfAsset_listDTOTohasPreactivatedOs.get(asset_listDTO.hasPreactivatedOs).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOToosLicenceNumber.containsKey(asset_listDTO.osLicenceNumber)) {
						mapOfAsset_listDTOToosLicenceNumber.put(asset_listDTO.osLicenceNumber, new HashSet<>());
					}
					mapOfAsset_listDTOToosLicenceNumber.get(asset_listDTO.osLicenceNumber).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOToassetSupplierType.containsKey(asset_listDTO.assetSupplierType)) {
						mapOfAsset_listDTOToassetSupplierType.put(asset_listDTO.assetSupplierType, new HashSet<>());
					}
					mapOfAsset_listDTOToassetSupplierType.get(asset_listDTO.assetSupplierType).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOTotag.containsKey(asset_listDTO.tag)) {
						mapOfAsset_listDTOTotag.put(asset_listDTO.tag, new HashSet<>());
					}
					mapOfAsset_listDTOTotag.get(asset_listDTO.tag).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOToorderNo.containsKey(asset_listDTO.orderNo)) {
						mapOfAsset_listDTOToorderNo.put(asset_listDTO.orderNo, new HashSet<>());
					}
					mapOfAsset_listDTOToorderNo.get(asset_listDTO.orderNo).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOTopurchaseCost.containsKey(asset_listDTO.purchaseCost)) {
						mapOfAsset_listDTOTopurchaseCost.put(asset_listDTO.purchaseCost, new HashSet<>());
					}
					mapOfAsset_listDTOTopurchaseCost.get(asset_listDTO.purchaseCost).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOTowarranty.containsKey(asset_listDTO.warranty)) {
						mapOfAsset_listDTOTowarranty.put(asset_listDTO.warranty, new HashSet<>());
					}
					mapOfAsset_listDTOTowarranty.get(asset_listDTO.warranty).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOTodescription.containsKey(asset_listDTO.description)) {
						mapOfAsset_listDTOTodescription.put(asset_listDTO.description, new HashSet<>());
					}
					mapOfAsset_listDTOTodescription.get(asset_listDTO.description).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOTovendorName.containsKey(asset_listDTO.vendorName)) {
						mapOfAsset_listDTOTovendorName.put(asset_listDTO.vendorName, new HashSet<>());
					}
					mapOfAsset_listDTOTovendorName.get(asset_listDTO.vendorName).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOTovendorPhone.containsKey(asset_listDTO.vendorPhone)) {
						mapOfAsset_listDTOTovendorPhone.put(asset_listDTO.vendorPhone, new HashSet<>());
					}
					mapOfAsset_listDTOTovendorPhone.get(asset_listDTO.vendorPhone).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOTovendorEmail.containsKey(asset_listDTO.vendorEmail)) {
						mapOfAsset_listDTOTovendorEmail.put(asset_listDTO.vendorEmail, new HashSet<>());
					}
					mapOfAsset_listDTOTovendorEmail.get(asset_listDTO.vendorEmail).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOTosupportDuration.containsKey(asset_listDTO.supportDuration)) {
						mapOfAsset_listDTOTosupportDuration.put(asset_listDTO.supportDuration, new HashSet<>());
					}
					mapOfAsset_listDTOTosupportDuration.get(asset_listDTO.supportDuration).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOToimageDropzone.containsKey(asset_listDTO.imageDropzone)) {
						mapOfAsset_listDTOToimageDropzone.put(asset_listDTO.imageDropzone, new HashSet<>());
					}
					mapOfAsset_listDTOToimageDropzone.get(asset_listDTO.imageDropzone).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOToisAssigned.containsKey(asset_listDTO.isAssigned)) {
						mapOfAsset_listDTOToisAssigned.put(asset_listDTO.isAssigned, new HashSet<>());
					}
					mapOfAsset_listDTOToisAssigned.get(asset_listDTO.isAssigned).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOToemployeeRecordsId.containsKey(asset_listDTO.employeeRecordsId)) {
						mapOfAsset_listDTOToemployeeRecordsId.put(asset_listDTO.employeeRecordsId, new HashSet<>());
					}
					mapOfAsset_listDTOToemployeeRecordsId.get(asset_listDTO.employeeRecordsId).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOTostatus.containsKey(asset_listDTO.status)) {
						mapOfAsset_listDTOTostatus.put(asset_listDTO.status, new HashSet<>());
					}
					mapOfAsset_listDTOTostatus.get(asset_listDTO.status).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOTosearchColumn.containsKey(asset_listDTO.searchColumn)) {
						mapOfAsset_listDTOTosearchColumn.put(asset_listDTO.searchColumn, new HashSet<>());
					}
					mapOfAsset_listDTOTosearchColumn.get(asset_listDTO.searchColumn).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOToinsertedByUserId.containsKey(asset_listDTO.insertedByUserId)) {
						mapOfAsset_listDTOToinsertedByUserId.put(asset_listDTO.insertedByUserId, new HashSet<>());
					}
					mapOfAsset_listDTOToinsertedByUserId.get(asset_listDTO.insertedByUserId).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOToinsertedByOrganogramId.containsKey(asset_listDTO.insertedByOrganogramId)) {
						mapOfAsset_listDTOToinsertedByOrganogramId.put(asset_listDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfAsset_listDTOToinsertedByOrganogramId.get(asset_listDTO.insertedByOrganogramId).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOToinsertionDate.containsKey(asset_listDTO.insertionDate)) {
						mapOfAsset_listDTOToinsertionDate.put(asset_listDTO.insertionDate, new HashSet<>());
					}
					mapOfAsset_listDTOToinsertionDate.get(asset_listDTO.insertionDate).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOTolastModifierUser.containsKey(asset_listDTO.lastModifierUser)) {
						mapOfAsset_listDTOTolastModifierUser.put(asset_listDTO.lastModifierUser, new HashSet<>());
					}
					mapOfAsset_listDTOTolastModifierUser.get(asset_listDTO.lastModifierUser).add(asset_listDTO);
					
					if( ! mapOfAsset_listDTOTolastModificationTime.containsKey(asset_listDTO.lastModificationTime)) {
						mapOfAsset_listDTOTolastModificationTime.put(asset_listDTO.lastModificationTime, new HashSet<>());
					}
					mapOfAsset_listDTOTolastModificationTime.get(asset_listDTO.lastModificationTime).add(asset_listDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Asset_listDTO> getAsset_listList() {
		List <Asset_listDTO> asset_lists = new ArrayList<Asset_listDTO>(this.mapOfAsset_listDTOToiD.values());
		return asset_lists;
	}
	
	
	public Asset_listDTO getAsset_listDTOByID( long ID){
		return mapOfAsset_listDTOToiD.get(ID);
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOByasset_category_type(long asset_category_type) {
		return new ArrayList<>( mapOfAsset_listDTOToassetCategoryType.getOrDefault(asset_category_type,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOByasset_model_type(long asset_model_type) {
		return new ArrayList<>( mapOfAsset_listDTOToassetModelType.getOrDefault(asset_model_type,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOByram(String ram) {
		return new ArrayList<>( mapOfAsset_listDTOToram.getOrDefault(ram,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOBycpu(String cpu) {
		return new ArrayList<>( mapOfAsset_listDTOTocpu.getOrDefault(cpu,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOByhdd(String hdd) {
		return new ArrayList<>( mapOfAsset_listDTOTohdd.getOrDefault(hdd,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOBymac(String mac) {
		return new ArrayList<>( mapOfAsset_listDTOTomac.getOrDefault(mac,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOByhas_preactivated_os(boolean has_preactivated_os) {
		return new ArrayList<>( mapOfAsset_listDTOTohasPreactivatedOs.getOrDefault(has_preactivated_os,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOByos_licence_number(String os_licence_number) {
		return new ArrayList<>( mapOfAsset_listDTOToosLicenceNumber.getOrDefault(os_licence_number,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOByasset_supplier_type(long asset_supplier_type) {
		return new ArrayList<>( mapOfAsset_listDTOToassetSupplierType.getOrDefault(asset_supplier_type,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOBytag(String tag) {
		return new ArrayList<>( mapOfAsset_listDTOTotag.getOrDefault(tag,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOByorder_no(String order_no) {
		return new ArrayList<>( mapOfAsset_listDTOToorderNo.getOrDefault(order_no,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOBypurchase_cost(double purchase_cost) {
		return new ArrayList<>( mapOfAsset_listDTOTopurchaseCost.getOrDefault(purchase_cost,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOBywarranty(String warranty) {
		return new ArrayList<>( mapOfAsset_listDTOTowarranty.getOrDefault(warranty,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOBydescription(String description) {
		return new ArrayList<>( mapOfAsset_listDTOTodescription.getOrDefault(description,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOByvendor_name(String vendor_name) {
		return new ArrayList<>( mapOfAsset_listDTOTovendorName.getOrDefault(vendor_name,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOByvendor_phone(String vendor_phone) {
		return new ArrayList<>( mapOfAsset_listDTOTovendorPhone.getOrDefault(vendor_phone,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOByvendor_email(String vendor_email) {
		return new ArrayList<>( mapOfAsset_listDTOTovendorEmail.getOrDefault(vendor_email,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOBysupport_duration(String support_duration) {
		return new ArrayList<>( mapOfAsset_listDTOTosupportDuration.getOrDefault(support_duration,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOByimage_dropzone(long image_dropzone) {
		return new ArrayList<>( mapOfAsset_listDTOToimageDropzone.getOrDefault(image_dropzone,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOByis_assigned(boolean is_assigned) {
		return new ArrayList<>( mapOfAsset_listDTOToisAssigned.getOrDefault(is_assigned,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOByemployee_records_id(long employee_records_id) {
		return new ArrayList<>( mapOfAsset_listDTOToemployeeRecordsId.getOrDefault(employee_records_id,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOBystatus(int status) {
		return new ArrayList<>( mapOfAsset_listDTOTostatus.getOrDefault(status,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOBysearch_column(String search_column) {
		return new ArrayList<>( mapOfAsset_listDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfAsset_listDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfAsset_listDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfAsset_listDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOBylast_modifier_user(String last_modifier_user) {
		return new ArrayList<>( mapOfAsset_listDTOTolastModifierUser.getOrDefault(last_modifier_user,new HashSet<>()));
	}
	
	
	public List<Asset_listDTO> getAsset_listDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfAsset_listDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "asset_list";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


