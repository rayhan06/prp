package vm_route_travel_payment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import common.ConnectionAndStatementUtil;
import procurement_package.ProcurementGoodsTypeRepository;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;
import vm_route_travel_payment.Vm_route_travel_paymentDTO;

public class Vm_route_travel_paymentDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Vm_route_travel_paymentDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Vm_route_travel_paymentMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"search_column",
			"route_travel_id",
			"fiscal_year_id",
			"is_paid",
			"month",
				"isDeleted",
				"lastModificationTime"
		};
	}
	
	public Vm_route_travel_paymentDAO()
	{
		this("vm_route_travel_payment");		
	}
	
	public void setSearchColumn(Vm_route_travel_paymentDTO vm_route_travel_paymentDTO)
	{
		vm_route_travel_paymentDTO.searchColumn = "";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Vm_route_travel_paymentDTO vm_route_travel_paymentDTO = (Vm_route_travel_paymentDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(vm_route_travel_paymentDTO);
		if(isInsert)
		{
			ps.setObject(index++,vm_route_travel_paymentDTO.iD);
		}
		ps.setObject(index++,vm_route_travel_paymentDTO.insertedByUserId);
		ps.setObject(index++,vm_route_travel_paymentDTO.insertedByOrganogramId);
		ps.setObject(index++,vm_route_travel_paymentDTO.insertionDate);
		ps.setObject(index++,vm_route_travel_paymentDTO.searchColumn);
		ps.setObject(index++,vm_route_travel_paymentDTO.routeTravelId);
		ps.setObject(index++,vm_route_travel_paymentDTO.fiscalYearId);
		ps.setObject(index++,vm_route_travel_paymentDTO.isPaid);
		ps.setObject(index++,vm_route_travel_paymentDTO.month);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Vm_route_travel_paymentDTO vm_route_travel_paymentDTO, ResultSet rs) throws SQLException
	{
		vm_route_travel_paymentDTO.iD = rs.getLong("ID");
		vm_route_travel_paymentDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		vm_route_travel_paymentDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
		vm_route_travel_paymentDTO.insertionDate = rs.getLong("insertion_date");
		vm_route_travel_paymentDTO.isDeleted = rs.getInt("isDeleted");
		vm_route_travel_paymentDTO.lastModificationTime = rs.getLong("lastModificationTime");
		vm_route_travel_paymentDTO.searchColumn = rs.getString("search_column");
		vm_route_travel_paymentDTO.routeTravelId = rs.getLong("route_travel_id");
		vm_route_travel_paymentDTO.fiscalYearId = rs.getLong("fiscal_year_id");
		vm_route_travel_paymentDTO.isPaid = rs.getBoolean("is_paid");
		vm_route_travel_paymentDTO.month = rs.getInt("month");
	}


	public Vm_route_travel_paymentDTO build(ResultSet rs)
	{
		try
		{
			Vm_route_travel_paymentDTO vm_route_travel_paymentDTO =  new Vm_route_travel_paymentDTO();
			vm_route_travel_paymentDTO.iD = rs.getLong("ID");
			vm_route_travel_paymentDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			vm_route_travel_paymentDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			vm_route_travel_paymentDTO.insertionDate = rs.getLong("insertion_date");
			vm_route_travel_paymentDTO.isDeleted = rs.getInt("isDeleted");
			vm_route_travel_paymentDTO.lastModificationTime = rs.getLong("lastModificationTime");
			vm_route_travel_paymentDTO.searchColumn = rs.getString("search_column");
			vm_route_travel_paymentDTO.routeTravelId = rs.getLong("route_travel_id");
			vm_route_travel_paymentDTO.fiscalYearId = rs.getLong("fiscal_year_id");
			vm_route_travel_paymentDTO.isPaid = rs.getBoolean("is_paid");
			vm_route_travel_paymentDTO.month = rs.getInt("month");
			return vm_route_travel_paymentDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
		
	

	//need another getter for repository
	public Vm_route_travel_paymentDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		Vm_route_travel_paymentDTO vm_route_travel_paymentDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
		return vm_route_travel_paymentDTO;
	}




	public List<Vm_route_travel_paymentDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	}
	
	

	
	
	
	//add repository
	public List<Vm_route_travel_paymentDTO> getAllVm_route_travel_payment (boolean isFirstReload)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}

	
	public List<Vm_route_travel_paymentDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}


	public List<Vm_route_travel_paymentDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		List<Object> objectList = new ArrayList<Object>();

		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat, objectList);
		return ConnectionAndStatementUtil.getListOfT(sql,objectList,this::build);
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ?";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }

	public void hardDelete(long routeTravelId, long fiscalYearId, int month) throws Exception {
		StringBuilder sqlBuilder = new StringBuilder("delete from  ")
				.append(tableName)
				.append(" WHERE route_travel_id = " + routeTravelId)
				.append(" AND fiscal_year_id = " + fiscalYearId)
				.append(" AND `month` = " + month);
		ConnectionAndStatementUtil.getWriteStatement(st -> {
			String sql = sqlBuilder.toString();
			try {
				logger.debug(sql);
				st.execute(sql);
				Vm_route_travel_paymentRepository.getInstance().deleteByMonthFiscalRouteTravelId(month, fiscalYearId, routeTravelId);
			} catch (SQLException ex) {
				logger.error(ex);
			}
		});
	}
				
}
	