package vm_route_travel_payment;
import java.util.*; 
import util.*;


public class Vm_route_travel_paymentMAPS extends CommonMaps
{	
	public Vm_route_travel_paymentMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("routeTravelId".toLowerCase(), "routeTravelId".toLowerCase());
		java_DTO_map.put("fiscalYearId".toLowerCase(), "fiscalYearId".toLowerCase());
		java_DTO_map.put("isPaid".toLowerCase(), "isPaid".toLowerCase());
		java_DTO_map.put("month".toLowerCase(), "month".toLowerCase());

		java_SQL_map.put("route_travel_id".toLowerCase(), "routeTravelId".toLowerCase());
		java_SQL_map.put("fiscal_year_id".toLowerCase(), "fiscalYearId".toLowerCase());
		java_SQL_map.put("is_paid".toLowerCase(), "isPaid".toLowerCase());
		java_SQL_map.put("month".toLowerCase(), "month".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Route Travel Id".toLowerCase(), "routeTravelId".toLowerCase());
		java_Text_map.put("Fiscal Year Id".toLowerCase(), "fiscalYearId".toLowerCase());
		java_Text_map.put("Is Paid".toLowerCase(), "isPaid".toLowerCase());
		java_Text_map.put("Month".toLowerCase(), "month".toLowerCase());
			
	}

}