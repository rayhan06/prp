package vm_route_travel_payment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Vm_route_travel_paymentRepository implements Repository {
	Vm_route_travel_paymentDAO vm_route_travel_paymentDAO = null;
	Gson gson;

	public void setDAO(Vm_route_travel_paymentDAO vm_route_travel_paymentDAO)
	{
		this.vm_route_travel_paymentDAO = vm_route_travel_paymentDAO;
		gson = new Gson();

	}
	
	
	static Logger logger = Logger.getLogger(Vm_route_travel_paymentRepository.class);
	Map<Long, Vm_route_travel_paymentDTO>mapOfVm_route_travel_paymentDTOToiD;
//	Map<Long, Set<Vm_route_travel_paymentDTO> >mapOfVm_route_travel_paymentDTOToinsertedByUserId;
//	Map<Long, Set<Vm_route_travel_paymentDTO> >mapOfVm_route_travel_paymentDTOToinsertedByOrganogramId;
//	Map<Long, Set<Vm_route_travel_paymentDTO> >mapOfVm_route_travel_paymentDTOToinsertionDate;
//	Map<Long, Set<Vm_route_travel_paymentDTO> >mapOfVm_route_travel_paymentDTOTolastModificationTime;
//	Map<String, Set<Vm_route_travel_paymentDTO> >mapOfVm_route_travel_paymentDTOTosearchColumn;
	Map<Long, Set<Vm_route_travel_paymentDTO> >mapOfVm_route_travel_paymentDTOTorouteTravelId;
	Map<Long, Set<Vm_route_travel_paymentDTO> >mapOfVm_route_travel_paymentDTOTofiscalYearId;
//	Map<Boolean, Set<Vm_route_travel_paymentDTO> >mapOfVm_route_travel_paymentDTOToisPaid;
	Map<Integer, Set<Vm_route_travel_paymentDTO> >mapOfVm_route_travel_paymentDTOTomonth;


	static Vm_route_travel_paymentRepository instance = null;  
	private Vm_route_travel_paymentRepository(){
		mapOfVm_route_travel_paymentDTOToiD = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_paymentDTOToinsertedByUserId = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_paymentDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_paymentDTOToinsertionDate = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_paymentDTOTolastModificationTime = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_paymentDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfVm_route_travel_paymentDTOTorouteTravelId = new ConcurrentHashMap<>();
		mapOfVm_route_travel_paymentDTOTofiscalYearId = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_paymentDTOToisPaid = new ConcurrentHashMap<>();
		mapOfVm_route_travel_paymentDTOTomonth = new ConcurrentHashMap<>();

		setDAO(new Vm_route_travel_paymentDAO());

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vm_route_travel_paymentRepository getInstance(){
		if (instance == null){
			instance = new Vm_route_travel_paymentRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(vm_route_travel_paymentDAO == null)
		{
			return;
		}
		try {
			List<Vm_route_travel_paymentDTO> vm_route_travel_paymentDTOs = vm_route_travel_paymentDAO.getAllVm_route_travel_payment(reloadAll);
			for(Vm_route_travel_paymentDTO vm_route_travel_paymentDTO : vm_route_travel_paymentDTOs) {
				Vm_route_travel_paymentDTO oldVm_route_travel_paymentDTO = getVm_route_travel_paymentDTOByIDWithoutClone(vm_route_travel_paymentDTO.iD);
				if( oldVm_route_travel_paymentDTO != null ) {
					mapOfVm_route_travel_paymentDTOToiD.remove(oldVm_route_travel_paymentDTO.iD);
				
//					if(mapOfVm_route_travel_paymentDTOToinsertedByUserId.containsKey(oldVm_route_travel_paymentDTO.insertedByUserId)) {
//						mapOfVm_route_travel_paymentDTOToinsertedByUserId.get(oldVm_route_travel_paymentDTO.insertedByUserId).remove(oldVm_route_travel_paymentDTO);
//					}
//					if(mapOfVm_route_travel_paymentDTOToinsertedByUserId.get(oldVm_route_travel_paymentDTO.insertedByUserId).isEmpty()) {
//						mapOfVm_route_travel_paymentDTOToinsertedByUserId.remove(oldVm_route_travel_paymentDTO.insertedByUserId);
//					}
//
//					if(mapOfVm_route_travel_paymentDTOToinsertedByOrganogramId.containsKey(oldVm_route_travel_paymentDTO.insertedByOrganogramId)) {
//						mapOfVm_route_travel_paymentDTOToinsertedByOrganogramId.get(oldVm_route_travel_paymentDTO.insertedByOrganogramId).remove(oldVm_route_travel_paymentDTO);
//					}
//					if(mapOfVm_route_travel_paymentDTOToinsertedByOrganogramId.get(oldVm_route_travel_paymentDTO.insertedByOrganogramId).isEmpty()) {
//						mapOfVm_route_travel_paymentDTOToinsertedByOrganogramId.remove(oldVm_route_travel_paymentDTO.insertedByOrganogramId);
//					}
//
//					if(mapOfVm_route_travel_paymentDTOToinsertionDate.containsKey(oldVm_route_travel_paymentDTO.insertionDate)) {
//						mapOfVm_route_travel_paymentDTOToinsertionDate.get(oldVm_route_travel_paymentDTO.insertionDate).remove(oldVm_route_travel_paymentDTO);
//					}
//					if(mapOfVm_route_travel_paymentDTOToinsertionDate.get(oldVm_route_travel_paymentDTO.insertionDate).isEmpty()) {
//						mapOfVm_route_travel_paymentDTOToinsertionDate.remove(oldVm_route_travel_paymentDTO.insertionDate);
//					}
//
//					if(mapOfVm_route_travel_paymentDTOTolastModificationTime.containsKey(oldVm_route_travel_paymentDTO.lastModificationTime)) {
//						mapOfVm_route_travel_paymentDTOTolastModificationTime.get(oldVm_route_travel_paymentDTO.lastModificationTime).remove(oldVm_route_travel_paymentDTO);
//					}
//					if(mapOfVm_route_travel_paymentDTOTolastModificationTime.get(oldVm_route_travel_paymentDTO.lastModificationTime).isEmpty()) {
//						mapOfVm_route_travel_paymentDTOTolastModificationTime.remove(oldVm_route_travel_paymentDTO.lastModificationTime);
//					}
//
//					if(mapOfVm_route_travel_paymentDTOTosearchColumn.containsKey(oldVm_route_travel_paymentDTO.searchColumn)) {
//						mapOfVm_route_travel_paymentDTOTosearchColumn.get(oldVm_route_travel_paymentDTO.searchColumn).remove(oldVm_route_travel_paymentDTO);
//					}
//					if(mapOfVm_route_travel_paymentDTOTosearchColumn.get(oldVm_route_travel_paymentDTO.searchColumn).isEmpty()) {
//						mapOfVm_route_travel_paymentDTOTosearchColumn.remove(oldVm_route_travel_paymentDTO.searchColumn);
//					}
					
					if(mapOfVm_route_travel_paymentDTOTorouteTravelId.containsKey(oldVm_route_travel_paymentDTO.routeTravelId)) {
						mapOfVm_route_travel_paymentDTOTorouteTravelId.get(oldVm_route_travel_paymentDTO.routeTravelId).remove(oldVm_route_travel_paymentDTO);
					}
					if(mapOfVm_route_travel_paymentDTOTorouteTravelId.get(oldVm_route_travel_paymentDTO.routeTravelId).isEmpty()) {
						mapOfVm_route_travel_paymentDTOTorouteTravelId.remove(oldVm_route_travel_paymentDTO.routeTravelId);
					}
					
					if(mapOfVm_route_travel_paymentDTOTofiscalYearId.containsKey(oldVm_route_travel_paymentDTO.fiscalYearId)) {
						mapOfVm_route_travel_paymentDTOTofiscalYearId.get(oldVm_route_travel_paymentDTO.fiscalYearId).remove(oldVm_route_travel_paymentDTO);
					}
					if(mapOfVm_route_travel_paymentDTOTofiscalYearId.get(oldVm_route_travel_paymentDTO.fiscalYearId).isEmpty()) {
						mapOfVm_route_travel_paymentDTOTofiscalYearId.remove(oldVm_route_travel_paymentDTO.fiscalYearId);
					}
//
//					if(mapOfVm_route_travel_paymentDTOToisPaid.containsKey(oldVm_route_travel_paymentDTO.isPaid)) {
//						mapOfVm_route_travel_paymentDTOToisPaid.get(oldVm_route_travel_paymentDTO.isPaid).remove(oldVm_route_travel_paymentDTO);
//					}
//					if(mapOfVm_route_travel_paymentDTOToisPaid.get(oldVm_route_travel_paymentDTO.isPaid).isEmpty()) {
//						mapOfVm_route_travel_paymentDTOToisPaid.remove(oldVm_route_travel_paymentDTO.isPaid);
//					}
//
					if(mapOfVm_route_travel_paymentDTOTomonth.containsKey(oldVm_route_travel_paymentDTO.month)) {
						mapOfVm_route_travel_paymentDTOTomonth.get(oldVm_route_travel_paymentDTO.month).remove(oldVm_route_travel_paymentDTO);
					}
					if(mapOfVm_route_travel_paymentDTOTomonth.get(oldVm_route_travel_paymentDTO.month).isEmpty()) {
						mapOfVm_route_travel_paymentDTOTomonth.remove(oldVm_route_travel_paymentDTO.month);
					}
					
					
				}
				if(vm_route_travel_paymentDTO.isDeleted == 0) 
				{
					
					mapOfVm_route_travel_paymentDTOToiD.put(vm_route_travel_paymentDTO.iD, vm_route_travel_paymentDTO);
				
//					if( ! mapOfVm_route_travel_paymentDTOToinsertedByUserId.containsKey(vm_route_travel_paymentDTO.insertedByUserId)) {
//						mapOfVm_route_travel_paymentDTOToinsertedByUserId.put(vm_route_travel_paymentDTO.insertedByUserId, new HashSet<>());
//					}
//					mapOfVm_route_travel_paymentDTOToinsertedByUserId.get(vm_route_travel_paymentDTO.insertedByUserId).add(vm_route_travel_paymentDTO);
//
//					if( ! mapOfVm_route_travel_paymentDTOToinsertedByOrganogramId.containsKey(vm_route_travel_paymentDTO.insertedByOrganogramId)) {
//						mapOfVm_route_travel_paymentDTOToinsertedByOrganogramId.put(vm_route_travel_paymentDTO.insertedByOrganogramId, new HashSet<>());
//					}
//					mapOfVm_route_travel_paymentDTOToinsertedByOrganogramId.get(vm_route_travel_paymentDTO.insertedByOrganogramId).add(vm_route_travel_paymentDTO);
//
//					if( ! mapOfVm_route_travel_paymentDTOToinsertionDate.containsKey(vm_route_travel_paymentDTO.insertionDate)) {
//						mapOfVm_route_travel_paymentDTOToinsertionDate.put(vm_route_travel_paymentDTO.insertionDate, new HashSet<>());
//					}
//					mapOfVm_route_travel_paymentDTOToinsertionDate.get(vm_route_travel_paymentDTO.insertionDate).add(vm_route_travel_paymentDTO);
//
//					if( ! mapOfVm_route_travel_paymentDTOTolastModificationTime.containsKey(vm_route_travel_paymentDTO.lastModificationTime)) {
//						mapOfVm_route_travel_paymentDTOTolastModificationTime.put(vm_route_travel_paymentDTO.lastModificationTime, new HashSet<>());
//					}
//					mapOfVm_route_travel_paymentDTOTolastModificationTime.get(vm_route_travel_paymentDTO.lastModificationTime).add(vm_route_travel_paymentDTO);
//
//					if( ! mapOfVm_route_travel_paymentDTOTosearchColumn.containsKey(vm_route_travel_paymentDTO.searchColumn)) {
//						mapOfVm_route_travel_paymentDTOTosearchColumn.put(vm_route_travel_paymentDTO.searchColumn, new HashSet<>());
//					}
//					mapOfVm_route_travel_paymentDTOTosearchColumn.get(vm_route_travel_paymentDTO.searchColumn).add(vm_route_travel_paymentDTO);
//
					if( ! mapOfVm_route_travel_paymentDTOTorouteTravelId.containsKey(vm_route_travel_paymentDTO.routeTravelId)) {
						mapOfVm_route_travel_paymentDTOTorouteTravelId.put(vm_route_travel_paymentDTO.routeTravelId, new HashSet<>());
					}
					mapOfVm_route_travel_paymentDTOTorouteTravelId.get(vm_route_travel_paymentDTO.routeTravelId).add(vm_route_travel_paymentDTO);
					
					if( ! mapOfVm_route_travel_paymentDTOTofiscalYearId.containsKey(vm_route_travel_paymentDTO.fiscalYearId)) {
						mapOfVm_route_travel_paymentDTOTofiscalYearId.put(vm_route_travel_paymentDTO.fiscalYearId, new HashSet<>());
					}
					mapOfVm_route_travel_paymentDTOTofiscalYearId.get(vm_route_travel_paymentDTO.fiscalYearId).add(vm_route_travel_paymentDTO);
//
//					if( ! mapOfVm_route_travel_paymentDTOToisPaid.containsKey(vm_route_travel_paymentDTO.isPaid)) {
//						mapOfVm_route_travel_paymentDTOToisPaid.put(vm_route_travel_paymentDTO.isPaid, new HashSet<>());
//					}
//					mapOfVm_route_travel_paymentDTOToisPaid.get(vm_route_travel_paymentDTO.isPaid).add(vm_route_travel_paymentDTO);
//
					if( ! mapOfVm_route_travel_paymentDTOTomonth.containsKey(vm_route_travel_paymentDTO.month)) {
						mapOfVm_route_travel_paymentDTOTomonth.put(vm_route_travel_paymentDTO.month, new HashSet<>());
					}
					mapOfVm_route_travel_paymentDTOTomonth.get(vm_route_travel_paymentDTO.month).add(vm_route_travel_paymentDTO);

				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public Vm_route_travel_paymentDTO clone(Vm_route_travel_paymentDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Vm_route_travel_paymentDTO.class);
	}

	public List<Vm_route_travel_paymentDTO> clone(List<Vm_route_travel_paymentDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public Vm_route_travel_paymentDTO getVm_route_travel_paymentDTOByIDWithoutClone( long ID){
		return mapOfVm_route_travel_paymentDTOToiD.get(ID);
	}
	
	public List<Vm_route_travel_paymentDTO> getVm_route_travel_paymentList() {
		List <Vm_route_travel_paymentDTO> vm_route_travel_payments = new ArrayList<Vm_route_travel_paymentDTO>(this.mapOfVm_route_travel_paymentDTOToiD.values());
		return vm_route_travel_payments;
	}
	
	
	public Vm_route_travel_paymentDTO getVm_route_travel_paymentDTOByID( long ID){
		return clone(mapOfVm_route_travel_paymentDTOToiD.get(ID));
	}
	
	
//	public List<Vm_route_travel_paymentDTO> getVm_route_travel_paymentDTOByinserted_by_user_id(long inserted_by_user_id) {
//		return new ArrayList<>( mapOfVm_route_travel_paymentDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travel_paymentDTO> getVm_route_travel_paymentDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
//		return new ArrayList<>( mapOfVm_route_travel_paymentDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travel_paymentDTO> getVm_route_travel_paymentDTOByinsertion_date(long insertion_date) {
//		return new ArrayList<>( mapOfVm_route_travel_paymentDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travel_paymentDTO> getVm_route_travel_paymentDTOBylastModificationTime(long lastModificationTime) {
//		return new ArrayList<>( mapOfVm_route_travel_paymentDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travel_paymentDTO> getVm_route_travel_paymentDTOBysearch_column(String search_column) {
//		return new ArrayList<>( mapOfVm_route_travel_paymentDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
//	}
	
	
	public List<Vm_route_travel_paymentDTO> getVm_route_travel_paymentDTOByroute_travel_id(long route_travel_id) {
		return clone(new ArrayList<>( mapOfVm_route_travel_paymentDTOTorouteTravelId.getOrDefault(route_travel_id,new HashSet<>())));
	}

	public List<Vm_route_travel_paymentDTO> getVm_route_travel_paymentDTOByroute_travel_idWithoutClone(long route_travel_id) {
		return (new ArrayList<>( mapOfVm_route_travel_paymentDTOTorouteTravelId.getOrDefault(route_travel_id,new HashSet<>())));
	}

	public List<Vm_route_travel_paymentDTO> getVm_route_travel_paymentDTOByfiscal_year_id(long fiscal_year_id) {
		return clone(new ArrayList<>( mapOfVm_route_travel_paymentDTOTofiscalYearId.getOrDefault(fiscal_year_id,new HashSet<>())));
	}
//
//
//	public List<Vm_route_travel_paymentDTO> getVm_route_travel_paymentDTOByis_paid(boolean is_paid) {
//		return new ArrayList<>( mapOfVm_route_travel_paymentDTOToisPaid.getOrDefault(is_paid,new HashSet<>()));
//	}
//
//
	public List<Vm_route_travel_paymentDTO> getVm_route_travel_paymentDTOBymonth(int month) {
		return clone(new ArrayList<>( mapOfVm_route_travel_paymentDTOTomonth.getOrDefault(month,new HashSet<>())));
	}

	public void deleteByMonthFiscalRouteTravelId(int month, long fisclYearId, long vmRouteTravelId) {
		List<Vm_route_travel_paymentDTO> vm_route_travel_paymentDTOList = getVm_route_travel_paymentDTOByroute_travel_idWithoutClone(vmRouteTravelId);
		List<Vm_route_travel_paymentDTO> toBeDeleted =
				vm_route_travel_paymentDTOList
				.stream()
				.filter(vm_route_travel_paymentDTO -> (vm_route_travel_paymentDTO.fiscalYearId == fisclYearId
						&& vm_route_travel_paymentDTO.month == month))
				.collect(Collectors.toList());

		toBeDeleted
				.forEach(oldVm_route_travel_paymentDTO -> {
					if(mapOfVm_route_travel_paymentDTOTorouteTravelId.containsKey(oldVm_route_travel_paymentDTO.routeTravelId)) {
						mapOfVm_route_travel_paymentDTOTorouteTravelId.get(oldVm_route_travel_paymentDTO.routeTravelId).remove(oldVm_route_travel_paymentDTO);
					}
					if(mapOfVm_route_travel_paymentDTOTofiscalYearId.containsKey(oldVm_route_travel_paymentDTO.fiscalYearId)) {
						mapOfVm_route_travel_paymentDTOTofiscalYearId.get(oldVm_route_travel_paymentDTO.fiscalYearId).remove(oldVm_route_travel_paymentDTO);
					}
					if(mapOfVm_route_travel_paymentDTOTomonth.containsKey(oldVm_route_travel_paymentDTO.month)) {
						mapOfVm_route_travel_paymentDTOTomonth.get(oldVm_route_travel_paymentDTO.month).remove(oldVm_route_travel_paymentDTO);
					}
				});

	}
	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_route_travel_payment";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


