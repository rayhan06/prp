package vm_route_travel_payment;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import vm_route_travel_approver.Vm_route_travel_approverRepository;


/**
 * Servlet implementation class Vm_route_travel_paymentServlet
 */
@WebServlet("/Vm_route_travel_paymentServlet")
@MultipartConfig
public class Vm_route_travel_paymentServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Vm_route_travel_paymentServlet.class);

    String tableName = "vm_route_travel_payment";

	Vm_route_travel_paymentDAO vm_route_travel_paymentDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vm_route_travel_paymentServlet()
	{
        super();
    	try
    	{
			vm_route_travel_paymentDAO = new Vm_route_travel_paymentDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(vm_route_travel_paymentDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_PAYMENT_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_PAYMENT_UPDATE))
				{
					getVm_route_travel_payment(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_PAYMENT_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchVm_route_travel_payment(request, response, isPermanentTable, filter);
						}
						else
						{
							searchVm_route_travel_payment(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchVm_route_travel_payment(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_PAYMENT_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_PAYMENT_ADD))
				{
					System.out.println("going to  addVm_route_travel_payment ");
					addVm_route_travel_payment(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addVm_route_travel_payment ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_PAYMENT_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addVm_route_travel_payment ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_PAYMENT_UPDATE))
				{
					addVm_route_travel_payment(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_PAYMENT_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_PAYMENT_SEARCH))
				{
					searchVm_route_travel_payment(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Vm_route_travel_paymentDTO vm_route_travel_paymentDTO = Vm_route_travel_paymentRepository.getInstance().getVm_route_travel_paymentDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(vm_route_travel_paymentDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addVm_route_travel_payment(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addVm_route_travel_payment");
			String path = getServletContext().getRealPath("/img2/");
			Vm_route_travel_paymentDTO vm_route_travel_paymentDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				vm_route_travel_paymentDTO = new Vm_route_travel_paymentDTO();
			}
			else
			{
				vm_route_travel_paymentDTO = Vm_route_travel_paymentRepository.getInstance().getVm_route_travel_paymentDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			if(addFlag)
			{
				vm_route_travel_paymentDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				vm_route_travel_paymentDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				vm_route_travel_paymentDTO.insertionDate = c.getTimeInMillis();
			}


			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				vm_route_travel_paymentDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("routeTravelId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("routeTravelId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travel_paymentDTO.routeTravelId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("fiscalYearId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("fiscalYearId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travel_paymentDTO.fiscalYearId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("isPaid");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("isPaid = " + Value);
            vm_route_travel_paymentDTO.isPaid = Value != null && !Value.equalsIgnoreCase("");

			Value = request.getParameter("month");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("month = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travel_paymentDTO.month = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addVm_route_travel_payment dto = " + vm_route_travel_paymentDTO);
			long[] returnedID = {-1};

			Utils.handleTransactionForNavigationService4(()->{
				if(isPermanentTable == false) //add new row for validation and make the old row outdated
				{
					vm_route_travel_paymentDAO.setIsDeleted(vm_route_travel_paymentDTO.iD, CommonDTO.OUTDATED);
					returnedID[0] = vm_route_travel_paymentDAO.add(vm_route_travel_paymentDTO);
					vm_route_travel_paymentDAO.setIsDeleted(returnedID[0], CommonDTO.WAITING_FOR_APPROVAL);
				}
				else if(addFlag == true)
				{
					returnedID[0] = vm_route_travel_paymentDAO.manageWriteOperations(vm_route_travel_paymentDTO, SessionConstants.INSERT, -1, userDTO);
				}
				else
				{
					returnedID[0] = vm_route_travel_paymentDAO.manageWriteOperations(vm_route_travel_paymentDTO, SessionConstants.UPDATE, -1, userDTO);
				}
			});









			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getVm_route_travel_payment(request, response, returnedID[0]);
				}
				else
				{
					response.sendRedirect("Vm_route_travel_paymentServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(Vm_route_travel_paymentRepository.getInstance().getVm_route_travel_paymentDTOByID(returnedID[0]), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}









	private void getVm_route_travel_payment(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getVm_route_travel_payment");
		Vm_route_travel_paymentDTO vm_route_travel_paymentDTO = null;
		try
		{
			vm_route_travel_paymentDTO = Vm_route_travel_paymentRepository.getInstance().getVm_route_travel_paymentDTOByID(id);
			request.setAttribute("ID", vm_route_travel_paymentDTO.iD);
			request.setAttribute("vm_route_travel_paymentDTO",vm_route_travel_paymentDTO);
			request.setAttribute("vm_route_travel_paymentDAO",vm_route_travel_paymentDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "vm_route_travel_payment/vm_route_travel_paymentInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "vm_route_travel_payment/vm_route_travel_paymentSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "vm_route_travel_payment/vm_route_travel_paymentEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "vm_route_travel_payment/vm_route_travel_paymentEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getVm_route_travel_payment(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getVm_route_travel_payment(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchVm_route_travel_payment(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchVm_route_travel_payment 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_VM_ROUTE_TRAVEL_PAYMENT,
			request,
			vm_route_travel_paymentDAO,
			SessionConstants.VIEW_VM_ROUTE_TRAVEL_PAYMENT,
			SessionConstants.SEARCH_VM_ROUTE_TRAVEL_PAYMENT,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("vm_route_travel_paymentDAO",vm_route_travel_paymentDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_route_travel_payment/vm_route_travel_paymentApproval.jsp");
	        	rd = request.getRequestDispatcher("vm_route_travel_payment/vm_route_travel_paymentApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_route_travel_payment/vm_route_travel_paymentApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("vm_route_travel_payment/vm_route_travel_paymentApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_route_travel_payment/vm_route_travel_paymentSearch.jsp");
	        	rd = request.getRequestDispatcher("vm_route_travel_payment/vm_route_travel_paymentSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_route_travel_payment/vm_route_travel_paymentSearchForm.jsp");
	        	rd = request.getRequestDispatcher("vm_route_travel_payment/vm_route_travel_paymentSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

