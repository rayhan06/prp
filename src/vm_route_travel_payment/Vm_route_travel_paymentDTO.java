package vm_route_travel_payment;
import java.util.*; 
import util.*; 


public class Vm_route_travel_paymentDTO extends CommonDTO
{

	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	public long routeTravelId = -1;
	public long fiscalYearId = -1;
	public boolean isPaid = false;
	public int month = -1;
	
	
    @Override
	public String toString() {
            return "$Vm_route_travel_paymentDTO[" +
            " iD = " + iD +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            " routeTravelId = " + routeTravelId +
            " fiscalYearId = " + fiscalYearId +
            " isPaid = " + isPaid +
            " month = " + month +
            "]";
    }

}