package budget_code_selection;

import budget.BudgetDAO;
import budget.BudgetUtils;
import budget_selection_info.BudgetSelectionInfoDAO;
import budget_selection_info.BudgetSelectionInfoDTO;
import com.google.gson.Gson;
import economic_sub_code.EconomicSubCodeModel;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.RecordNavigationManager4;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@WebServlet("/BudgetCodeSelectionServlet")
@MultipartConfig
public class BudgetCodeSelectionServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(BudgetCodeSelectionServlet.class);
    private final Gson gson = new Gson();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (dontHavePermission(userDTO)) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "budgetCodeSelection":
                    request.getRequestDispatcher("budget_code_selection/budget_code_selectionEdit.jsp").forward(request, response);
                    return;
                case "budgetCodeSelectionPreview":
                    request.getRequestDispatcher("budget_code_selection/budget_code_selection_preview.jsp").forward(request, response);
                    return;
                case "search":
                    searchEconomic_code_selection(request, response, loginDTO, userDTO);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (dontHavePermission(userDTO)) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "saveEconomicSubCodes": {
                    String currentYear = BudgetUtils.getEconomicYearForCodeSelection(System.currentTimeMillis());
                    BudgetSelectionInfoDTO budgetSelectionInfoDTO = BudgetSelectionInfoDAO.getInstance().getDTOByEconomicYear(currentYear);
                    boolean isCodeSubmitted = budgetSelectionInfoDTO != null && budgetSelectionInfoDTO.isSubmitted;
                    if (!isCodeSubmitted) {
                        saveEconomicSubCodes(request, response, userDTO);
                        return;
                    }
                    break;
                }
                case "economicCodeFinalSubmission":
                    economicCodeFinalSubmission(response, userDTO);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private boolean dontHavePermission(UserDTO userDTO) {
        return userDTO == null || !PermissionRepository.checkPermissionByRoleIDAndMenuID(
                userDTO.roleID, MenuConstants.BUDGET_CODE_SELECTION
        );
    }

    private void saveEconomicSubCodes(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception {
        long currentTime = System.currentTimeMillis();

        String economicYear = BudgetUtils.getEconomicYearForCodeSelection(currentTime);
        long budgetSelectionInfoId = BudgetSelectionInfoDAO.getInstance().getOrCreateAndGetDTO(economicYear, userDTO).iD;

        long budgetMappingId = Long.parseLong(request.getParameter("budgetMappingId").trim());

        String Value = request.getParameter("economicSubCodes");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        List<EconomicSubCodeModel> economicSubCodeModelList = Arrays.asList(gson.fromJson(Value, EconomicSubCodeModel[].class));

        long requestedBy = userDTO.ID;
        BudgetDAO budgetDAO = BudgetDAO.getInstance();
        Map<Boolean, List<EconomicSubCodeModel>> alreadyAddedMap = budgetDAO.deleteDeletedInfoAndReturnAMap(
                economicSubCodeModelList,
                budgetSelectionInfoId, budgetMappingId, requestedBy, currentTime
        );
        budgetDAO.insertSelectedCodeList(
                alreadyAddedMap.get(false),
                budgetSelectionInfoId, budgetMappingId, requestedBy, currentTime
        );

        String resJson = "{\"success\": true}";
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.print(resJson);
        out.flush();
    }

    private void economicCodeFinalSubmission(HttpServletResponse response, UserDTO userDTO) throws Exception {
        BudgetSelectionInfoDAO budgetSelectionInfoDAO = BudgetSelectionInfoDAO.getInstance();
        boolean flag = false;

        String currentYear = BudgetUtils.getEconomicYearForCodeSelection(System.currentTimeMillis());
        BudgetSelectionInfoDTO budgetSelectionInfoDTO = budgetSelectionInfoDAO.getDTOByEconomicYear(currentYear);

        boolean isAbleToSubmitCode = budgetSelectionInfoDTO != null && !budgetSelectionInfoDTO.isSubmitted;
        if (isAbleToSubmitCode) {
            budgetSelectionInfoDTO.isSubmitted = true;
            budgetSelectionInfoDTO.modifiedBy = userDTO.ID;
            budgetSelectionInfoDTO.lastModificationTime = System.currentTimeMillis();
            budgetSelectionInfoDAO.update(budgetSelectionInfoDTO);
            flag = true;
        }

        PrintWriter out = response.getWriter();
        String encoded = this.gson.toJson(flag);
        out.print(encoded);
        out.flush();
    }

    private void searchEconomic_code_selection(HttpServletRequest request, HttpServletResponse response,
                                               LoginDTO loginDTO, UserDTO userDTO) throws Exception {

        String ajax = request.getParameter("ajax");
        boolean hasAjax = StringUtils.isNotBlank(ajax);
        logger.debug("ajax = " + ajax + " hasAjax = " + hasAjax);
        BudgetDAO budgetDAO = BudgetDAO.getInstance();
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_BUDGET_CODE_SELECTION,
                request,
                budgetDAO,
                SessionConstants.VIEW_BUDGET_CODE_SELECTION,
                SessionConstants.SEARCH_BUDGET_CODE_SELECTION,
                "budget",
                true,
                userDTO,
                "",
                true
        );

        rnManager.doJob(loginDTO);

        request.setAttribute("budgetDAO", budgetDAO);

        if (!hasAjax) {
            logger.debug("Going to budget_code_selection/budget_code_selectionSearch.jsp");
            request.getRequestDispatcher("budget_code_selection/budget_code_selectionSearch.jsp").forward(request, response);
        } else {
            logger.debug("Going to budget_code_selection/budget_code_selectionSearchForm.jsp");
            request.getRequestDispatcher("budget_code_selection/budget_code_selectionSearchForm.jsp").forward(request, response);
        }
    }
}

