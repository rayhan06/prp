package bangla_date_converter;

import java.time.LocalDate;
import java.util.Calendar;

import static util.StringUtils.convertToBanNumber;

public class BanglaDateConverter {
    private static final MonthDetail[] BANGLA_MONTH_DETAILS = {
            new MonthDetail("Boishakh", "বৈশাখ", 31, 0),
            new MonthDetail("Jyoishtho", "জ্যৈষ্ঠ", 31, 0),
            new MonthDetail("Asharh", "আষাঢ়", 31, 0),
            new MonthDetail("Srabon", "শ্রাবণ", 31, 0),
            new MonthDetail("Bhadro", "ভাদ্র", 31, 0),
            new MonthDetail("Ashshin", "আশ্বিন", 31, 0),
            new MonthDetail("Kartik", "কার্তিক", 30, 0),
            new MonthDetail("Ogrohayon", "অগ্রহায়ণ", 30, 0),
            new MonthDetail("Poush", "পৌষ", 30, 0),
            new MonthDetail("Magh", "মাঘ", 30, 0),
            new MonthDetail("Falgoon", "ফাল্গুন", 29, 1),
            new MonthDetail("Choitro", "চৈত্র", 30, 0)
    };

    public static DateKey getBanglaDate(DateKey englishDateKey) {
        LocalDate englishDate = LocalDate.of(englishDateKey.year, englishDateKey.month, englishDateKey.day);
        LocalDate banglaDate = LocalDate.of(englishDateKey.year, 4, 14);

        boolean isBeforeBanglaNewYear = englishDate.isBefore(banglaDate);
        if (isBeforeBanglaNewYear)
            banglaDate = LocalDate.of(englishDateKey.year - 1, 4, 14);

        int monthIndex = 0, dayOfMonth = 1;
        while (!banglaDate.equals(englishDate)) {
            banglaDate = banglaDate.plusDays(1);
            ++dayOfMonth;
            if (dayOfMonth > BANGLA_MONTH_DETAILS[monthIndex].getDays(banglaDate.isLeapYear())) {
                dayOfMonth = 1;
                ++monthIndex;
            }
        }

        final int yearDifference = isBeforeBanglaNewYear ? 594 : 593;
        return new DateKey(
                dayOfMonth,
                monthIndex + 1,
                englishDateKey.year - yearDifference
        );
    }

    public static DateKey getDateKeyFromMillis(long timeInMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMillis);
        return new DateKey(
                calendar.get(Calendar.DAY_OF_MONTH),
                calendar.get(Calendar.MONTH) + 1,
                calendar.get(Calendar.YEAR)
        );
    }

    /*
        FORMAT
            d     - day
            dd    - day with fixed two digits and leading 0 if needed
            m     - month
            mm    - month with fixed two digits and leading 0 if needed
            y     - year
            yy    - last 2 digits of year
            month - month name
    * */
    public static String getFormattedBanglaDate(DateKey banglaDate, String format, boolean isLangBn) {
        String d = String.format("%d", banglaDate.day);
        String dd = String.format("%02d", banglaDate.day);
        String m = String.format("%d", banglaDate.month);
        String mm = String.format("%02d", banglaDate.month);
        String y = String.format("%d", banglaDate.year);
        String yy = String.format("%02d", banglaDate.year % 100);
        String month = isLangBn
                       ? BANGLA_MONTH_DETAILS[banglaDate.month - 1].nameBn
                       : BANGLA_MONTH_DETAILS[banglaDate.month - 1].nameEn;
        format = format.replaceAll("dd", dd);
        format = format.replaceAll("d", d);
        format = format.replaceAll("month", month);
        format = format.replaceAll("mm", mm);
        format = format.replaceAll("m", m);
        format = format.replaceAll("yy", yy);
        format = format.replaceAll("y", y);
        return isLangBn ? convertToBanNumber(format) : format;
    }

    public static String getFormattedBanglaDate(DateKey banglaDate, boolean isLangBn) {
        return getFormattedBanglaDate(banglaDate, "d month, y", isLangBn);
    }

    public static String getFormattedBanglaDate(long timeInMillis, boolean isLangBn) {
        return getFormattedBanglaDate(
                getBanglaDate(getDateKeyFromMillis(timeInMillis)),
                isLangBn
        );
    }

    public static String getFormattedBanglaDate(long timeInMillis, String format, boolean isLangBn) {
        return getFormattedBanglaDate(
                getBanglaDate(getDateKeyFromMillis(timeInMillis)),
                format,
                isLangBn
        );
    }
}
