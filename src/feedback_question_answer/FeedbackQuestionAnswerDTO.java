package feedback_question_answer;

import util.CommonDTO;

public class FeedbackQuestionAnswerDTO extends CommonDTO {

    public long trainingCalenderDetailsId = 0;
    public long feedbackMappingId = 0;
    public int feedbackAnswerPoint = 0;
    public long insertionTime;
    public String insertBy;
    public String modifiedBy;


    @Override
    public String toString() {
        return "FeedbackQuestionAnswerDTO[" +
                " iD = " + iD +
                " trainingCalenderDetailsId = " + trainingCalenderDetailsId +
                " feedbackMappingId = " + feedbackMappingId +
                " feedbackAnswerPoint = " + feedbackAnswerPoint +
                " insertionTime = " + insertionTime +
                " insertBy = " + insertBy +
                " modifiedBy = " + modifiedBy +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}
