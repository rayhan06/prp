package feedback_question_answer;

import common.BaseServlet;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import training_calendar_details.Training_calendar_detailsDAO;
import training_calendar_details.Training_calendar_detailsDTO;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
@WebServlet("/FeedbackQuestionAnswerServlet")
public class FeedbackQuestionAnswerServlet extends BaseServlet {
    private static final Logger logger = Logger.getLogger(FeedbackQuestionAnswerServlet.class);

    @Override
    public String getTableName() {
        return "feedback_question_answer";
    }

    @Override
    public String getServletName() {
        return "FeedbackQuestionAnswerServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return null;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return FeedbackQuestionAnswerServlet.class;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        long trainCalId = Long.parseLong(request.getParameter("trainCalId"));
        Training_calendar_detailsDTO trainingCalendarDetailsDTO = new Training_calendar_detailsDAO().getByTrainingCalendarIdAndEmpId(trainCalId, userDTO.employee_record_id);
        if (trainingCalendarDetailsDTO == null || trainingCalendarDetailsDTO.isFeedBackDone) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        request.getRequestDispatcher("feedback_question_answer/FeedbackQuestionAnswer.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long trainCalId = Long.parseLong(request.getParameter("trainCalId"));
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        Training_calendar_detailsDAO trainingCalendarDetailsDAO = new Training_calendar_detailsDAO();
        Training_calendar_detailsDTO trainingCalendarDetailsDTO = trainingCalendarDetailsDAO.getByTrainingCalendarIdAndEmpId(trainCalId, userDTO.employee_record_id);
        if (trainingCalendarDetailsDTO == null || trainingCalendarDetailsDTO.isFeedBackDone) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        long employeeRecordId = userDTO.employee_record_id;
        long trainingCalendarId = Long.parseLong(request.getParameter("trainCalId"));
        String metadataOne = request.getParameter("feedback-text-answer-1");
        String metadataTwo = request.getParameter("feedback-text-answer-2");
        String feedbackPointData = request.getParameter("feedback-point");

        trainingCalendarDetailsDAO.updateMetadata(employeeRecordId, trainingCalendarId, metadataOne, metadataTwo, 1);

        List<FeedbackQuestionAnswerDTO> feedbackQuestionAnswerDTOList = parseFeedbackPointData(feedbackPointData);
        FeedbackQuestionAnswerDAO.getInstance().multiUpdate(feedbackQuestionAnswerDTOList, String.valueOf(userDTO.employee_record_id));
        if (request.getParameter("data") == null) {
            response.sendRedirect(request.getContextPath() + "/EnrollmentServlet?actionType=searchEnrolmentHistory");
        } else {
            response.sendRedirect(request.getContextPath() + "/Employee_recordsServlet?actionType=viewMultiForm&tab=4&" +
                    "ID=" + userDTO.employee_record_id + "&data=" + request.getParameter("data"));
        }
    }


    private List<FeedbackQuestionAnswerDTO> parseFeedbackPointData(String feedbackPointData) {
        if (feedbackPointData == null || feedbackPointData.equals(""))
            return new ArrayList<>();
        String[] detailsMappingPoint = feedbackPointData.split(";");
        if (detailsMappingPoint.length > 0) {
            return Arrays.stream(detailsMappingPoint)
                    .map(this::buildFeedbackQuestionAnswerDTO)
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    private FeedbackQuestionAnswerDTO buildFeedbackQuestionAnswerDTO(String answerStr) {
        FeedbackQuestionAnswerDTO dto = new FeedbackQuestionAnswerDTO();
        String[] values = answerStr.split(",");
        for (int i = 0; i < values.length; i++) {
            switch (i) {
                case 0:
                    dto.trainingCalenderDetailsId = Long.parseLong(values[i]);
                    break;
                case 1:
                    dto.feedbackMappingId = Long.parseLong(values[i]);
                    break;
                case 2:
                    dto.feedbackAnswerPoint = Integer.parseInt(values[i]);
                    break;
            }
        }
        return dto;
    }
}
