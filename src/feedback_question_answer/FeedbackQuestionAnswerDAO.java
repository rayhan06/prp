package feedback_question_answer;

import common.ConnectionAndStatementUtil;
import dbm.DBMW;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FeedbackQuestionAnswerDAO {
    private static final Logger logger = Logger.getLogger(FeedbackQuestionAnswerDAO.class);

    private static final String addQuery = "INSERT INTO feedback_question_answer (training_calendar_details_id, tran_cal_fb_ques_mapping_id, feedback_ques_ans_point_cat, " +
            "modified_by, lastModificationTime, insertion_date, insert_by, isDeleted, id) VALUES(?,?,?,?,?,?,?,?,?)";

    private static final String updateQuery = "UPDATE feedback_question_answer SET training_calendar_details_id=?, tran_cal_fb_ques_mapping_id=?, feedback_ques_ans_point_cat=?, " +
            "modified_by=?, lastModificationTime=? WHERE id = ?";

    private static final String getQuery = "SELECT * FROM feedback_question_answer WHERE training_calendar_details_id = %d AND isDeleted = 0";

    private FeedbackQuestionAnswerDAO() {

    }

    private static class LazyLoader {
        static final FeedbackQuestionAnswerDAO INSTANCE = new FeedbackQuestionAnswerDAO();
    }

    public static FeedbackQuestionAnswerDAO getInstance() {
        return FeedbackQuestionAnswerDAO.LazyLoader.INSTANCE;
    }


    public void set(PreparedStatement ps, FeedbackQuestionAnswerDTO feedbackQuestionAnswerDTO, boolean isInsert) throws Exception {
        int index = 0;
        ps.setObject(++index, feedbackQuestionAnswerDTO.trainingCalenderDetailsId);
        ps.setObject(++index, feedbackQuestionAnswerDTO.feedbackMappingId);
        ps.setObject(++index, feedbackQuestionAnswerDTO.feedbackAnswerPoint);
        ps.setObject(++index, feedbackQuestionAnswerDTO.modifiedBy);
        ps.setObject(++index, feedbackQuestionAnswerDTO.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, feedbackQuestionAnswerDTO.insertionTime);
            ps.setObject(++index, feedbackQuestionAnswerDTO.insertBy);
            ps.setObject(++index, feedbackQuestionAnswerDTO.isDeleted);
            feedbackQuestionAnswerDTO.iD = DBMW.getInstance().getNextSequenceId("feedback_question_answer");
        }
        ps.setObject(++index, feedbackQuestionAnswerDTO.iD);
    }


    private FeedbackQuestionAnswerDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            FeedbackQuestionAnswerDTO dto = new FeedbackQuestionAnswerDTO();
            dto.trainingCalenderDetailsId = rs.getLong("training_calendar_details_id");
            dto.feedbackMappingId = rs.getLong("tran_cal_fb_ques_mapping_id");
            dto.feedbackAnswerPoint = rs.getInt("feedback_ques_ans_point_cat");
            dto.iD = rs.getLong("id");
            dto.insertBy = rs.getString("insert_by");
            dto.insertionTime = rs.getLong("insertion_date");
            dto.modifiedBy = rs.getString("modified_by");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }


    public void add(FeedbackQuestionAnswerDTO dto) {
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                set(ps, dto, true);
                logger.debug(ps);
                ps.execute();
            } catch (Exception e) {
                logger.error(e);
            }
        }, addQuery);
    }


    public void update(FeedbackQuestionAnswerDTO dto) {
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                set(ps, dto, false);
                logger.debug(ps);
                ps.executeUpdate();
            } catch (Exception e) {
                logger.error(e);
            }
        }, updateQuery);
    }

    public void multiUpdate(List<FeedbackQuestionAnswerDTO> feedbackQuestionAnswerDTOList, String insertBy) {
        if (feedbackQuestionAnswerDTOList == null || feedbackQuestionAnswerDTOList.size() == 0) {
            return;
        }
        Map<Long, FeedbackQuestionAnswerDTO> mapByFeedbackMappingId = getFeedbackQuestionAnswerDTOByTrainingCalendarDetailsId(feedbackQuestionAnswerDTOList.get(0).trainingCalenderDetailsId);
        long currentTime = System.currentTimeMillis();
        feedbackQuestionAnswerDTOList.forEach(dto -> {
            dto.modifiedBy = insertBy;
            dto.lastModificationTime = currentTime;
            if (mapByFeedbackMappingId.containsKey(dto.feedbackMappingId)) {
                dto.iD = mapByFeedbackMappingId.get(dto.feedbackMappingId).iD;
                update(dto);
            } else {
                dto.insertBy = insertBy;
                dto.insertionTime = currentTime;
                add(dto);
            }
        });
    }

    public Map<Long, FeedbackQuestionAnswerDTO> getFeedbackQuestionAnswerDTOByTrainingCalendarDetailsId(long trainingCalendarDetailsId) {
        String sql = String.format(getQuery, trainingCalendarDetailsId);
        List<FeedbackQuestionAnswerDTO> answerList = ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
        return answerList.stream()
                .collect(Collectors.toMap(dto -> dto.feedbackMappingId, dto -> dto));
    }

}


