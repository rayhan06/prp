package grade_wise_pay_scale;

import util.CommonDTO;


public class Grade_wise_pay_scaleDTO extends CommonDTO {

    public int gradeTypeId = -1;
    public long nationalPayScaleID = -1;
    public int gradeYear = 0;
    public int salary = 0;
    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;


    @Override
    public String toString() {
        return "$Grade_wise_pay_scaleDTO[" +
                " iD = " + iD +
                " nationaPayScaleID = " + nationalPayScaleID +
                " gradeYear = " + gradeYear +
                " salary = " + salary +
                " searchColumn = " + searchColumn +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " isDeleted = " + isDeleted +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}