package grade_wise_pay_scale;

import com.google.gson.Gson;
import national_pay_scale.National_pay_scaleDTO;
import national_pay_scale.National_pay_scaleRepository;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class Grade_wise_pay_scaleRepository implements Repository {
    Grade_wise_pay_scaleDAO grade_wise_pay_scaleDAO;

    static Logger logger = Logger.getLogger(Grade_wise_pay_scaleRepository.class);
    Map<Long, Grade_wise_pay_scaleDTO> mapOfGrade_wise_pay_scaleDTOToiD;
    Gson gson;


    private Grade_wise_pay_scaleRepository() {
        grade_wise_pay_scaleDAO = Grade_wise_pay_scaleDAO.getInstance();
        mapOfGrade_wise_pay_scaleDTOToiD = new ConcurrentHashMap<>();
        gson = new Gson();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Grade_wise_pay_scaleRepository INSTANCE = new Grade_wise_pay_scaleRepository();
    }

    public static Grade_wise_pay_scaleRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        try {
            List<Grade_wise_pay_scaleDTO> grade_wise_pay_scaleDTOs = grade_wise_pay_scaleDAO.getAllDTOs(reloadAll);
            for (Grade_wise_pay_scaleDTO grade_wise_pay_scaleDTO : grade_wise_pay_scaleDTOs) {
                Grade_wise_pay_scaleDTO oldGrade_wise_pay_scaleDTO = getGrade_wise_pay_scaleDTOByiD(grade_wise_pay_scaleDTO.iD);
                if (oldGrade_wise_pay_scaleDTO != null) {
                    mapOfGrade_wise_pay_scaleDTOToiD.remove(oldGrade_wise_pay_scaleDTO.iD);


                }
                if (grade_wise_pay_scaleDTO.isDeleted == 0) {

                    mapOfGrade_wise_pay_scaleDTOToiD.put(grade_wise_pay_scaleDTO.iD, grade_wise_pay_scaleDTO);

                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public Grade_wise_pay_scaleDTO clone(Grade_wise_pay_scaleDTO dto) {
        String raw = gson.toJson(dto);
        return gson.fromJson(raw, Grade_wise_pay_scaleDTO.class);
    }


    public List<Grade_wise_pay_scaleDTO> getGrade_wise_pay_scaleList() {
        return new ArrayList<>(this.mapOfGrade_wise_pay_scaleDTOToiD.values());
    }

    public List<Grade_wise_pay_scaleDTO> copyGrade_wise_pay_scaleList() {
        List<Grade_wise_pay_scaleDTO> grade_wise_pay_scales = getGrade_wise_pay_scaleList();
        return grade_wise_pay_scales
                .stream()
                .map(this::clone)
                .collect(Collectors.toList());
    }


    public Grade_wise_pay_scaleDTO getGrade_wise_pay_scaleDTOByiD(long iD) {
        return mapOfGrade_wise_pay_scaleDTOToiD.get(iD);
    }

    public Grade_wise_pay_scaleDTO copyGrade_wise_pay_scaleDTOByiD(long iD) {
        return clone(mapOfGrade_wise_pay_scaleDTOToiD.get(iD));
    }


    @Override
    public String getTableName() {
        return grade_wise_pay_scaleDAO.getTableName();
    }

    public boolean isAlreadyAdded(long nationalPayScaleId, int gradeTypeId, int gradeYear) {
        Grade_wise_pay_scaleDTO grade_wise_pay_scaleDTO = copyGrade_wise_pay_scaleList().stream()
                .filter(dto -> dto.nationalPayScaleID == nationalPayScaleId && dto.gradeTypeId == gradeTypeId && dto.gradeYear == gradeYear)
                .findAny().orElse(null);
        return grade_wise_pay_scaleDTO != null;
    }

    public String buildOptions(String language, int gradeTypeId, Long selectedId) {
        National_pay_scaleDTO national_pay_scaleDTO = National_pay_scaleRepository.getInstance().getActivePayScale();
        if (national_pay_scaleDTO == null) {
            return "";
        }
        List<OptionDTO> optionDTOList = null;
        List<Grade_wise_pay_scaleDTO> grade_wise_pay_scaleDTOList = copyGrade_wise_pay_scaleList().stream()
                .filter(dto -> dto.nationalPayScaleID == national_pay_scaleDTO.iD)
                .filter(dto -> dto.gradeTypeId == gradeTypeId).collect(Collectors.toList());
        if (grade_wise_pay_scaleDTOList.size() > 0) {
            optionDTOList = grade_wise_pay_scaleDTOList.stream()
                    .map(dto -> new OptionDTO(Utils.getDigits(dto.salary + "/-", "English"),
                            Utils.getDigits(dto.salary + "/-", "Bangla"), String.valueOf(dto.iD)))
                    .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }
    
    public String buildOptions(String language, Long selectedId) {
        National_pay_scaleDTO national_pay_scaleDTO = National_pay_scaleRepository.getInstance().getActivePayScale();
        if (national_pay_scaleDTO == null) {
            return "";
        }
        List<OptionDTO> optionDTOList = null;
        List<Grade_wise_pay_scaleDTO> grade_wise_pay_scaleDTOList = copyGrade_wise_pay_scaleList().stream()
                .filter(dto -> dto.nationalPayScaleID == national_pay_scaleDTO.iD).collect(Collectors.toList());
        if (grade_wise_pay_scaleDTOList.size() > 0) {
            optionDTOList = grade_wise_pay_scaleDTOList.stream()
                    .map(dto -> new OptionDTO(Utils.getDigits(dto.salary + "/-", "English"),
                            Utils.getDigits(dto.salary + "/-", "Bangla"), String.valueOf(dto.iD)))
                    .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String getText(String Language, long gradeLevelId) {
        Grade_wise_pay_scaleDTO dto = copyGrade_wise_pay_scaleDTOByiD(gradeLevelId);
        if (dto != null) {
            return Language.equalsIgnoreCase("English") ? Utils.getDigits(dto.salary + "/-", "English") :
                    Utils.getDigits(dto.salary + "/-", "Bangla");
        } else {
            return "";
        }
    }
    public int getPayScale(long gradeLevelId) {
        Grade_wise_pay_scaleDTO dto = copyGrade_wise_pay_scaleDTOByiD(gradeLevelId);
        if (dto != null) {
            return dto.salary;
        } else {
            return -1;
        }
    }
}


