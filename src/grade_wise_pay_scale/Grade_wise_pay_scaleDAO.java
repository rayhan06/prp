package grade_wise_pay_scale;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import pb.CatRepository;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Grade_wise_pay_scaleDAO implements CommonDAOService<Grade_wise_pay_scaleDTO> {

    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private final String[] columnNames;

    private Grade_wise_pay_scaleDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "national_pay_scale_ID",
                        "grade_type_id",
                        "grade_year",
                        "salary",
                        "search_column",
                        "inserted_by",
                        "insertion_time",
                        "isDeleted",
                        "modified_by",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("gradeTypeId", " and (grade_type_id = ?)");
        searchMap.put("nationalPayScaleID", " and (national_pay_scale_ID = ?)");
        searchMap.put("grade_year", " and (grade_year = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Grade_wise_pay_scaleDAO INSTANCE = new Grade_wise_pay_scaleDAO();
    }

    public static Grade_wise_pay_scaleDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Grade_wise_pay_scaleDTO grade_wise_pay_scaleDTO) {
        grade_wise_pay_scaleDTO.searchColumn = "";
        grade_wise_pay_scaleDTO.searchColumn += CatRepository.getInstance().getText("English", "job_grade", grade_wise_pay_scaleDTO.gradeTypeId) + " ";
        grade_wise_pay_scaleDTO.searchColumn += CatRepository.getInstance().getText("Bangla", "job_grade", grade_wise_pay_scaleDTO.gradeTypeId) + " ";
    }

    @Override
    public void set(PreparedStatement ps, Grade_wise_pay_scaleDTO grade_wise_pay_scaleDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(grade_wise_pay_scaleDTO);
        if (isInsert) {
            ps.setObject(++index, grade_wise_pay_scaleDTO.iD);
        }
        ps.setObject(++index, grade_wise_pay_scaleDTO.nationalPayScaleID);
        ps.setObject(++index, grade_wise_pay_scaleDTO.gradeTypeId);
        ps.setObject(++index, grade_wise_pay_scaleDTO.gradeYear);
        ps.setObject(++index, grade_wise_pay_scaleDTO.salary);
        ps.setObject(++index, grade_wise_pay_scaleDTO.searchColumn);
        ps.setObject(++index, grade_wise_pay_scaleDTO.insertedBy);
        ps.setObject(++index, grade_wise_pay_scaleDTO.insertionTime);
        if (isInsert) {
            ps.setObject(++index, grade_wise_pay_scaleDTO.isDeleted);
        }
        ps.setObject(++index, grade_wise_pay_scaleDTO.modifiedBy);
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, grade_wise_pay_scaleDTO.iD);
        }
    }

    @Override
    public Grade_wise_pay_scaleDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Grade_wise_pay_scaleDTO grade_wise_pay_scaleDTO = new Grade_wise_pay_scaleDTO();
            int i = 0;
            grade_wise_pay_scaleDTO.iD = rs.getLong(columnNames[i++]);
            grade_wise_pay_scaleDTO.nationalPayScaleID = rs.getLong(columnNames[i++]);
            grade_wise_pay_scaleDTO.gradeTypeId = rs.getInt(columnNames[i++]);
            grade_wise_pay_scaleDTO.gradeYear = rs.getInt(columnNames[i++]);
            grade_wise_pay_scaleDTO.salary = rs.getInt(columnNames[i++]);
            grade_wise_pay_scaleDTO.searchColumn = rs.getString(columnNames[i++]);
            grade_wise_pay_scaleDTO.insertedBy = rs.getLong(columnNames[i++]);
            grade_wise_pay_scaleDTO.insertionTime = rs.getLong(columnNames[i++]);
            grade_wise_pay_scaleDTO.isDeleted = rs.getInt(columnNames[i++]);
            grade_wise_pay_scaleDTO.modifiedBy = rs.getLong(columnNames[i++]);
            grade_wise_pay_scaleDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            return grade_wise_pay_scaleDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Grade_wise_pay_scaleDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "grade_wise_pay_scale";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Grade_wise_pay_scaleDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Grade_wise_pay_scaleDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

}
	