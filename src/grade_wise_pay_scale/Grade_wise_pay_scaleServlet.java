package grade_wise_pay_scale;

import common.BaseServlet;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;


/**
 * Servlet implementation class Grade_wise_pay_scaleServlet
 */
@WebServlet("/Grade_wise_pay_scaleServlet")
@MultipartConfig
public class Grade_wise_pay_scaleServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Grade_wise_pay_scaleServlet.class);

    @Override
    public String getTableName() {
        return Grade_wise_pay_scaleDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Grade_wise_pay_scaleServlet";
    }

    @Override
    public Grade_wise_pay_scaleDAO getCommonDAOService() {
        return Grade_wise_pay_scaleDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.GRADE_WISE_PAY_SCALE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.GRADE_WISE_PAY_SCALE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.GRADE_WISE_PAY_SCALE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Grade_wise_pay_scaleServlet.class;
    }


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {


        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Grade_wise_pay_scaleDTO grade_wise_pay_scaleDTO;

        if (addFlag) {
            grade_wise_pay_scaleDTO = new Grade_wise_pay_scaleDTO();
            grade_wise_pay_scaleDTO.insertedBy = userDTO.employee_record_id;
            grade_wise_pay_scaleDTO.insertionTime = new Date().getTime();
        } else {
            grade_wise_pay_scaleDTO = Grade_wise_pay_scaleDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        grade_wise_pay_scaleDTO.modifiedBy = userDTO.employee_record_id;
        grade_wise_pay_scaleDTO.lastModificationTime = new Date().getTime();

        String Value;
        Value = request.getParameter("nationalPayScaleID");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {
            grade_wise_pay_scaleDTO.nationalPayScaleID = Long.parseLong(Value);
        } else {
            throw new Exception(isLangEng ? "National pay scale is not selected" : "জাতীয় বেতন স্কেল বাছাই করা হয় নি");
        }

        Value = request.getParameter("gradeTypeId");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {
            grade_wise_pay_scaleDTO.gradeTypeId = Integer.parseInt(Value);
        } else {
            throw new Exception(isLangEng ? "Grade type is not selected" : "গ্রেড বাছাই করা হয় নি");
        }


        Value = request.getParameter("gradeYear");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {
            grade_wise_pay_scaleDTO.gradeYear = Integer.parseInt(Value);
        } else {
            throw new Exception(isLangEng ? "Grade year not found" : "বছরের স্তর পাওয়া যায় নি");
        }


        if (addFlag && Grade_wise_pay_scaleRepository.getInstance().isAlreadyAdded(grade_wise_pay_scaleDTO.nationalPayScaleID, grade_wise_pay_scaleDTO.gradeTypeId, grade_wise_pay_scaleDTO.gradeYear)) {
            throw new Exception(isLangEng ? "Already added data" : "ইতিমধ্যেই যুক্ত করা হয়েছে");
        }
        Value = request.getParameter("salary");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        System.out.println("salary = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            grade_wise_pay_scaleDTO.salary = Integer.parseInt(Value);
        } else {
            throw new Exception(isLangEng ? "Basic salary not found" : "মূল বেতন পাওয়া যায় নি");
        }

        System.out.println("Done adding  addGrade_wise_pay_scale dto = " + grade_wise_pay_scaleDTO);

        if (addFlag) {
            Grade_wise_pay_scaleDAO.getInstance().add(grade_wise_pay_scaleDTO);
        } else {
            Grade_wise_pay_scaleDAO.getInstance().update(grade_wise_pay_scaleDTO);
        }


        return grade_wise_pay_scaleDTO;

    }
}

