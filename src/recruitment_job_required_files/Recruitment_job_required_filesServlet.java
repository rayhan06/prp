package recruitment_job_required_files;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import common.ApiResponse;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Recruitment_job_required_filesServlet
 */
@WebServlet("/Recruitment_job_required_filesServlet")
@MultipartConfig
public class Recruitment_job_required_filesServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Recruitment_job_required_filesServlet.class);

    String tableName = "recruitment_job_required_files";

	Recruitment_job_required_filesDAO recruitment_job_required_filesDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Recruitment_job_required_filesServlet()
	{
        super();
    	try
    	{
			recruitment_job_required_filesDAO = new Recruitment_job_required_filesDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(recruitment_job_required_filesDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_REQUIRED_FILES_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_REQUIRED_FILES_UPDATE))
				{
					getRecruitment_job_required_files(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_REQUIRED_FILES_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchRecruitment_job_required_files(request, response, isPermanentTable, filter);
						}
						else
						{
							searchRecruitment_job_required_files(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchRecruitment_job_required_files(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_REQUIRED_FILES_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_REQUIRED_FILES_ADD))
				{
					System.out.println("going to  addRecruitment_job_required_files ");
					addRecruitment_job_required_files(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addRecruitment_job_required_files ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_REQUIRED_FILES_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addRecruitment_job_required_files ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_REQUIRED_FILES_UPDATE))
				{
					addRecruitment_job_required_files(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				deleteRecruitment_job_required_files(request, response, userDTO);
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_REQUIRED_FILES_SEARCH))
				{
					searchRecruitment_job_required_files(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Recruitment_job_required_filesDTO recruitment_job_required_filesDTO = Recruitment_job_required_filesRepository.getInstance().
					getRecruitment_job_required_filesDTOByID(Long.parseLong(request.getParameter("ID")));
//					(Recruitment_job_required_filesDTO)recruitment_job_required_filesDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(recruitment_job_required_filesDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addRecruitment_job_required_files(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		ApiResponse apiResponse;
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addRecruitment_job_required_files");
			String path = getServletContext().getRealPath("/img2/");
			Recruitment_job_required_filesDTO recruitment_job_required_filesDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				recruitment_job_required_filesDTO = new Recruitment_job_required_filesDTO();
			}
			else
			{
				recruitment_job_required_filesDTO = Recruitment_job_required_filesRepository.getInstance().
						getRecruitment_job_required_filesDTOByID(Long.parseLong(request.getParameter("iD")));
//						(Recruitment_job_required_filesDTO)recruitment_job_required_filesDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("nameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameBn = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				recruitment_job_required_filesDTO.nameBn = (Value);
			}
			else
			{
				throw new Exception(" Invalid name Bn");
			}

			Value = request.getParameter("nameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEn = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				recruitment_job_required_filesDTO.nameEn = (Value);
			}
			else
			{
				throw new Exception(" Invalid name En");			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				recruitment_job_required_filesDTO.insertionDate = c.getTimeInMillis();
			}


			if(addFlag)
			{
				recruitment_job_required_filesDTO.insertedByUserId = userDTO.ID;
			}


			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				recruitment_job_required_filesDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addRecruitment_job_required_files dto = " + recruitment_job_required_filesDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				recruitment_job_required_filesDAO.setIsDeleted(recruitment_job_required_filesDTO.iD, CommonDTO.OUTDATED);
				returnedID = recruitment_job_required_filesDAO.add(recruitment_job_required_filesDTO);
				recruitment_job_required_filesDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = recruitment_job_required_filesDAO.manageWriteOperations(recruitment_job_required_filesDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = recruitment_job_required_filesDAO.manageWriteOperations(recruitment_job_required_filesDTO, SessionConstants.UPDATE, -1, userDTO);
			}




			apiResponse = ApiResponse.makeSuccessResponse("Recruitment_job_required_filesServlet?actionType=search");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			apiResponse = ApiResponse.makeErrorResponse(e.getMessage());
		}

		PrintWriter pw = response.getWriter();
		pw.write(apiResponse.getJSONString());
		pw.flush();
		pw.close();
	}









	private void deleteRecruitment_job_required_files(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException
	{
		try
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);


				Recruitment_job_required_filesDTO recruitment_job_required_filesDTO = Recruitment_job_required_filesRepository.getInstance().
						getRecruitment_job_required_filesDTOByID(id);
//						(Recruitment_job_required_filesDTO)recruitment_job_required_filesDAO.getDTOByID(id);
				recruitment_job_required_filesDAO.manageWriteOperations(recruitment_job_required_filesDTO, SessionConstants.DELETE, id, userDTO);
				response.sendRedirect("Recruitment_job_required_filesServlet?actionType=search");

			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

	}

	private void getRecruitment_job_required_files(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getRecruitment_job_required_files");
		Recruitment_job_required_filesDTO recruitment_job_required_filesDTO = null;
		try
		{
			recruitment_job_required_filesDTO = Recruitment_job_required_filesRepository.getInstance().
					getRecruitment_job_required_filesDTOByID(id);
//					(Recruitment_job_required_filesDTO)recruitment_job_required_filesDAO.getDTOByID(id);
			request.setAttribute("ID", recruitment_job_required_filesDTO.iD);
			request.setAttribute("recruitment_job_required_filesDTO",recruitment_job_required_filesDTO);
			request.setAttribute("recruitment_job_required_filesDAO",recruitment_job_required_filesDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "recruitment_job_required_files/recruitment_job_required_filesInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "recruitment_job_required_files/recruitment_job_required_filesSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "recruitment_job_required_files/recruitment_job_required_filesEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "recruitment_job_required_files/recruitment_job_required_filesEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getRecruitment_job_required_files(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getRecruitment_job_required_files(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchRecruitment_job_required_files(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchRecruitment_job_required_files 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_RECRUITMENT_JOB_REQUIRED_FILES,
			request,
			recruitment_job_required_filesDAO,
			SessionConstants.VIEW_RECRUITMENT_JOB_REQUIRED_FILES,
			SessionConstants.SEARCH_RECRUITMENT_JOB_REQUIRED_FILES,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("recruitment_job_required_filesDAO",recruitment_job_required_filesDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to recruitment_job_required_files/recruitment_job_required_filesApproval.jsp");
	        	rd = request.getRequestDispatcher("recruitment_job_required_files/recruitment_job_required_filesApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to recruitment_job_required_files/recruitment_job_required_filesApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("recruitment_job_required_files/recruitment_job_required_filesApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to recruitment_job_required_files/recruitment_job_required_filesSearch.jsp");
	        	rd = request.getRequestDispatcher("recruitment_job_required_files/recruitment_job_required_filesSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to recruitment_job_required_files/recruitment_job_required_filesSearchForm.jsp");
	        	rd = request.getRequestDispatcher("recruitment_job_required_files/recruitment_job_required_filesSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

