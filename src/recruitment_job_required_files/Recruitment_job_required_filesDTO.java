package recruitment_job_required_files;
import java.util.*; 
import util.*; 


public class Recruitment_job_required_filesDTO extends CommonDTO
{

    public String nameBn = "";
    public String nameEn = "";
	public long insertionDate = 0;
	public long insertedByUserId = 0;
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Recruitment_job_required_filesDTO[" +
            " iD = " + iD +
            " nameBn = " + nameBn +
            " nameEn = " + nameEn +
            " insertionDate = " + insertionDate +
            " insertedByUserId = " + insertedByUserId +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}