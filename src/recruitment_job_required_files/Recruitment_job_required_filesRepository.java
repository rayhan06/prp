package recruitment_job_required_files;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import common.NameDTO;
import org.apache.log4j.Logger;

import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import vm_route.VmRouteStoppageDTO;


public class Recruitment_job_required_filesRepository implements Repository {
	Recruitment_job_required_filesDAO recruitment_job_required_filesDAO = null;
	Gson gson = new Gson();
	
	public void setDAO(Recruitment_job_required_filesDAO recruitment_job_required_filesDAO)
	{
		this.recruitment_job_required_filesDAO = recruitment_job_required_filesDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Recruitment_job_required_filesRepository.class);
	Map<Long, Recruitment_job_required_filesDTO>mapOfRecruitment_job_required_filesDTOToiD;


	static Recruitment_job_required_filesRepository instance = null;  
	private Recruitment_job_required_filesRepository(){
		mapOfRecruitment_job_required_filesDTOToiD = new ConcurrentHashMap<>();
		recruitment_job_required_filesDAO = new Recruitment_job_required_filesDAO();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Recruitment_job_required_filesRepository getInstance(){
		if (instance == null){
			instance = new Recruitment_job_required_filesRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(recruitment_job_required_filesDAO == null)
		{
			return;
		}
		try {
			List<Recruitment_job_required_filesDTO> recruitment_job_required_filesDTOs =(List<Recruitment_job_required_filesDTO>) recruitment_job_required_filesDAO.getAll(reloadAll);
			for(Recruitment_job_required_filesDTO recruitment_job_required_filesDTO : recruitment_job_required_filesDTOs) {
				Recruitment_job_required_filesDTO oldRecruitment_job_required_filesDTO =
						getRecruitment_job_required_filesDTOByIDWithoutClone(recruitment_job_required_filesDTO.iD);
				if( oldRecruitment_job_required_filesDTO != null ) {
					mapOfRecruitment_job_required_filesDTOToiD.remove(oldRecruitment_job_required_filesDTO.iD);
					
					
				}
				if(recruitment_job_required_filesDTO.isDeleted == 0) 
				{
					
					mapOfRecruitment_job_required_filesDTOToiD.put(recruitment_job_required_filesDTO.iD, recruitment_job_required_filesDTO);

				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Recruitment_job_required_filesDTO> getRecruitment_job_required_filesList() {
		List <Recruitment_job_required_filesDTO> recruitment_job_required_filess = new ArrayList<Recruitment_job_required_filesDTO>(this.mapOfRecruitment_job_required_filesDTOToiD.values());
		return clone(recruitment_job_required_filess);
	}
	
	
	public Recruitment_job_required_filesDTO getRecruitment_job_required_filesDTOByIDWithoutClone( long ID){
		return mapOfRecruitment_job_required_filesDTOToiD.get(ID);
	}

	public Recruitment_job_required_filesDTO getRecruitment_job_required_filesDTOByID( long ID){
		return clone(mapOfRecruitment_job_required_filesDTOToiD.get(ID));
	}

	public Recruitment_job_required_filesDTO clone(Recruitment_job_required_filesDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Recruitment_job_required_filesDTO.class);
	}

	public List<Recruitment_job_required_filesDTO> clone(List<Recruitment_job_required_filesDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "recruitment_job_required_files";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}

	public String getBuildOptions(String Language, String selectedId ){
		List<OptionDTO> optionDTOList = getRecruitment_job_required_filesList()
				.stream()
				.map(e->new OptionDTO(e.nameEn,e.nameBn,String.valueOf(e.iD)))
				.collect(Collectors.toList());

		return Utils.buildOptionsWithoutSelectWithSelectId(optionDTOList, Language, selectedId );

	}

	public String getText(String language, long id) {
		Recruitment_job_required_filesDTO dto = this.mapOfRecruitment_job_required_filesDTOToiD.get(id);
		return dto == null ? ""
				: (language.equalsIgnoreCase("English")) ? (dto.nameEn == null ? "" : dto.nameEn)
				: (dto.nameBn == null ? "" : dto.nameBn);
	}
}


