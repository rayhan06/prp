package recruitment_job_required_files;

import org.apache.log4j.Logger;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

public class Recruitment_job_required_filesDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public Recruitment_job_required_filesDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Recruitment_job_required_filesMAPS(tableName);
		columnNames = new String[]
		{
			"ID",
			"name_bn",
			"name_en",
			"insertion_date",
			"inserted_by_user_id",
			"modified_by",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
	}


	
	public Recruitment_job_required_filesDAO()
	{
		this("recruitment_job_required_files");		
	}
	
	public void setSearchColumn(Recruitment_job_required_filesDTO recruitment_job_required_filesDTO)
	{
		recruitment_job_required_filesDTO.searchColumn = "";
		recruitment_job_required_filesDTO.searchColumn += recruitment_job_required_filesDTO.nameBn + " ";
		recruitment_job_required_filesDTO.searchColumn += recruitment_job_required_filesDTO.nameEn + " ";
		recruitment_job_required_filesDTO.searchColumn += recruitment_job_required_filesDTO.modifiedBy + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Recruitment_job_required_filesDTO recruitment_job_required_filesDTO = (Recruitment_job_required_filesDTO)commonDTO;

		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(recruitment_job_required_filesDTO);
		if(isInsert)
		{
			ps.setObject(index++,recruitment_job_required_filesDTO.iD);
		}
		ps.setObject(index++,recruitment_job_required_filesDTO.nameBn);
		ps.setObject(index++,recruitment_job_required_filesDTO.nameEn);
		ps.setObject(index++,recruitment_job_required_filesDTO.insertionDate);
		ps.setObject(index++,recruitment_job_required_filesDTO.insertedByUserId);
		ps.setObject(index++,recruitment_job_required_filesDTO.modifiedBy);
		ps.setObject(index++,recruitment_job_required_filesDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Recruitment_job_required_filesDTO recruitment_job_required_filesDTO, ResultSet rs) throws SQLException
	{
		recruitment_job_required_filesDTO.iD = rs.getLong("ID");
		recruitment_job_required_filesDTO.nameBn = rs.getString("name_bn");
		recruitment_job_required_filesDTO.nameEn = rs.getString("name_en");
		recruitment_job_required_filesDTO.insertionDate = rs.getLong("insertion_date");
		recruitment_job_required_filesDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		recruitment_job_required_filesDTO.modifiedBy = rs.getString("modified_by");
		recruitment_job_required_filesDTO.isDeleted = rs.getInt("isDeleted");
		recruitment_job_required_filesDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}

	public Recruitment_job_required_filesDTO build(ResultSet rs)
	{
		try
		{
			Recruitment_job_required_filesDTO recruitment_job_required_filesDTO = new Recruitment_job_required_filesDTO();
			recruitment_job_required_filesDTO.iD = rs.getLong("ID");
			recruitment_job_required_filesDTO.nameBn = rs.getString("name_bn");
			recruitment_job_required_filesDTO.nameEn = rs.getString("name_en");
			recruitment_job_required_filesDTO.insertionDate = rs.getLong("insertion_date");
			recruitment_job_required_filesDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			recruitment_job_required_filesDTO.modifiedBy = rs.getString("modified_by");
			recruitment_job_required_filesDTO.isDeleted = rs.getInt("isDeleted");
			recruitment_job_required_filesDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return recruitment_job_required_filesDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	

		
	

	//need another getter for repository
	
	

	
	
	
	//add repository

	

	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ? ";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name_bn")
						|| str.equals("name_en")
//						|| str.equals("insertion_date_start")
//						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name_bn"))
					{
						AllFieldSql += "" + tableName + ".name_bn like ? ";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".name_en like ? ";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
//					else if(str.equals("insertion_date_start"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("insertion_date_end"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
//						i ++;
//					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	

				
}
	