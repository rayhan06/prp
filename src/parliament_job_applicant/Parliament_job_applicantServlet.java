package parliament_job_applicant;

import com.google.gson.Gson;
import common.ApiResponse;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pb.Utils;
import pbReport.GenerateExcelReportService;
import permission.MenuConstants;
import public_users.PublicUserDAO;
import public_users.PublicUserDTO;
import public_users.PublicUserRepository;
import sessionmanager.SessionConstants;
import sms.SmsService;
import user.UserDTO;
import user.UserRepository;
import util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static common.ApiResponse.API_RESPONSE_OK;


/**
 * Servlet implementation class Parliament_job_applicantServlet
 */
@WebServlet("/Parliament_job_applicantServlet")
@MultipartConfig
public class Parliament_job_applicantServlet extends HttpServlet implements GenerateExcelReportService {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Parliament_job_applicantServlet.class);

    String tableName = "parliament_job_applicant";

    Parliament_job_applicantDAO parliament_job_applicantDAO;
    CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Parliament_job_applicantServlet() {
        super();
        try {
            parliament_job_applicantDAO = new Parliament_job_applicantDAO(tableName);
            commonRequestHandler = new CommonRequestHandler(parliament_job_applicantDAO);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PARLIAMENT_JOB_APPLICANT_SEARCH};
    }



    public boolean getSearchPagePermission(HttpServletRequest request) {
        return true;
    }






    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        boolean isPermanentTable = true;

        try {
            CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
            String actionType = request.getParameter("actionType");
            if (actionType.equals("search")) {
                if (Utils.checkPermission(commonLoginData.userDTO, getSearchMenuConstants()) && getSearchPagePermission(request)) {
                    String filter = request.getParameter("filter");
                    System.out.println("filter = " + filter);
                    if (filter != null) {
                        filter = ""; //shouldn't be directly used, rather manipulate it.
                        search_users(request, response, isPermanentTable, filter);
                    } else {
                        search_users(request, response, isPermanentTable, "");
                    }
                }
            }
            else {
                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In dopost request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        boolean isPermanentTable = true;

        try {
            CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
            String actionType = request.getParameter("actionType");
            if (actionType.equals("updatePassword")) {
                if (Utils.checkPermission(commonLoginData.userDTO, getSearchMenuConstants()) && getSearchPagePermission(request)) {
                    Long id = Long.valueOf(request.getParameter("parliamentJobApplicantId"));
                    String password = request.getParameter("password");
                    Parliament_job_applicantDTO parliamentJobApplicantDTO = Parliament_job_applicantRepository.getInstance().getParliament_job_applicantDTOByID(id);
                    if(parliamentJobApplicantDTO != null){
                        Long publicUserId = parliamentJobApplicantDTO.userId;
                        PublicUserDTO publicUserDTO = PublicUserRepository.getInstance().getPublicUserDtoById(publicUserId);
                        publicUserDTO.password = PasswordUtil.getInstance().encrypt(password);
                        publicUserDTO.lastModificationTime = System.currentTimeMillis();

                        PublicUserDAO publicUserDAO = new PublicUserDAO();
                        publicUserDAO.updatePublicUserPassword(publicUserDTO);
                        String message = "Success ";
                        String smsResponse = SmsService.send(publicUserDTO.phoneNo, "Dear user, your parliament recruitment portal password is successfully updated. New password is: "+password +" And username is: "+publicUserDTO.phoneNo);
                        System.out.println(smsResponse);
                        ApiResponse.sendSuccessResponse(response, message);
                    } else {
                        System.out.println("Parliament_job_applicantDTO null");
                    }
                }
            }
            else {
                ApiResponse.sendErrorResponse(response, "Parliament_job_applicantServlet?actionType=search");

                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void search_users(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  search_users");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_PARLIAMENT_JOB_APPLICANT,
                request,
                parliament_job_applicantDAO,
                SessionConstants.VIEW_PARLIAMENT_JOB_APPLICANT,
                SessionConstants.SEARCH_PARLIAMENT_JOB_APPLICANT,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("parliament_job_applicantDAO", parliament_job_applicantDAO);
        RequestDispatcher rd;

        if (!hasAjax ) {
            System.out.println("Going to recruitment_candidate_list/parliament_job_applicantSearch.jsp");
            rd = request.getRequestDispatcher("recruitment_candidate_list/parliament_job_applicantSearch.jsp");
        } else {
            System.out.println("Going to recruitment_candidate_list/parliament_job_applicantSearchForm.jsp");
            rd = request.getRequestDispatcher("recruitment_candidate_list/parliament_job_applicantSearchForm.jsp");
        }

        rd.forward(request, response);
    }




}

