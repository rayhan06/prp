package parliament_job_applicant;
import java.util.*;
import util.*;


public class Parliament_job_applicantDTO extends CommonDTO
{
	public long jobId = 0;
	public String nameBn = "";
	public String nameEn = "";
	public String fatherName = "";
	public String motherName = "";
	public long maritalStatusCat = 0;
	public String spouseName = "";
	public long dateOfBirth = 0;
	public int genderCat = 0;
	public int employmentStatusCat = 0;
	public int ethnicMinorityCat = 0;
	public int freedomFighterCat = 0;
	public int nationalityCat = 0;
	public int disabilityCat = 0;
	public int specialQuotaCat = 0;
	public double height = 0;
	public double weight = 0;
	public double chest = 0;
	public int identificationTypeCat = 0;
	public String identificationNo = "";
	public String permanentAddress = "";
	public String presentAddress = "";
	public String homeDistrictName = "";
	public String contactNumber = "";
	public String email = "";
	public String nid = "";
	public String birthRegistrationNo = "";
	public String careOfPresent = "";
	public String careOfPermanent = "";
	public String postCodePresent = "";
	public String postCodePermanent = "";
	public int jobApplicantCategoryCat = 0;
	public int jobApplicantCandidateCat = 0;
	public String jobApplicantCandidateText ="";
	public int jobApplicantReligionCat = 0;
	public String jobReferenceNo = "";
	public long jobReferenceNoDate = 0;
	public String currentPosition = "";
	public long physicallyFit = 0;
	public long experiencedInMotorDriving = 0;
	public long bloodGroupCat = 0;
	public long categoryFilesDropzone = 0;
	public String alternateContactNumber = "";
	public long userId = 0;

	public int sameAsPresent = 0;


	@Override
	public String toString() {
		return "$Parliament_job_applicantDTO[" +
				" iD = " + iD +
				" nameBn = " + nameBn +
				" fatherName = " + fatherName +
				" motherName = " + motherName +
				" maritalStatusCat = " + maritalStatusCat +
				" spouseName = " + spouseName +
				" dateOfBirth = " + dateOfBirth +
				" genderCat = " + genderCat +
				" employmentStatusCat = " + employmentStatusCat +
				" ethnicMinorityCat = " + ethnicMinorityCat +
				" freedomFighterCat = " + freedomFighterCat +
				" nationalityCat = " + nationalityCat +
				" disabilityCat = " + disabilityCat +
				" specialQuotaCat = " + specialQuotaCat +
				" height = " + height +
				" weight = " + weight +
				" chest = " + chest +
				" identificationTypeCat = " + identificationTypeCat +
				" identificationNo = " + identificationNo +
				" permanentAddress = " + permanentAddress +
				" presentAddress = " + presentAddress +
				" homeDistrictName = " + homeDistrictName +
				" contactNumber = " + contactNumber +
				" email = " + email +
				" isDeleted = " + isDeleted +
				" lastModificationTime = " + lastModificationTime +
				" userId = " + userId +
				" nid = " + nid +
				" birthRegistrationNo = " + birthRegistrationNo +
				" careOfPresent = " + careOfPresent +
				" careOfPermanent = " + careOfPermanent +
				" postCodePresent = " + postCodePresent +
				" postCodePermanent = " + postCodePermanent +
				" jobApplicantCategoryCat = " + jobApplicantCategoryCat +
				" jobApplicantCandidateCat = " + jobApplicantCandidateCat +
				" jobApplicantCandidateText = " + jobApplicantCandidateText +
				" jobApplicantReligionCat = " + jobApplicantReligionCat +
				" jobReferenceNo = " + jobReferenceNo +
				" jobReferenceNoDate = " + jobReferenceNoDate +
				" currentPosition = " + currentPosition +
				" physicallyFit = " + physicallyFit +
				" experiencedInMotorDriving = " + experiencedInMotorDriving +
				" sameAsPresent = " + sameAsPresent +
				"]";
	}

}