package parliament_job_applicant;

import common.ConnectionAndStatementUtil;
import job_applicant_application.Job_applicant_applicationDAO;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import repository.RepositoryManager;
import user.UserDTO;
import util.NavigationService4;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class Parliament_job_applicantDAO  extends NavigationService4
{

	Logger logger = Logger.getLogger(getClass());


	public Parliament_job_applicantDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
//		commonMaps = new Parliament_job_applicantMAPS(tableName);
	}

	public Parliament_job_applicantDAO()
	{
		this("parliament_job_applicant");
	}


	public void get(Parliament_job_applicantDTO parliament_job_applicantDTO, ResultSet rs) throws SQLException
	{
		parliament_job_applicantDTO.iD = rs.getLong("ID");
		parliament_job_applicantDTO.nameBn = rs.getString("name_bn");
		parliament_job_applicantDTO.nameEn = rs.getString("name_en");
		parliament_job_applicantDTO.fatherName = rs.getString("father_name");
		parliament_job_applicantDTO.motherName = rs.getString("mother_name");
		parliament_job_applicantDTO.maritalStatusCat = rs.getLong("marital_status_cat");
		parliament_job_applicantDTO.spouseName = rs.getString("spouse_name");
		parliament_job_applicantDTO.dateOfBirth = rs.getLong("date_of_birth");
		parliament_job_applicantDTO.genderCat = rs.getInt("gender_cat");
		parliament_job_applicantDTO.employmentStatusCat = rs.getInt("employment_status_cat");
		parliament_job_applicantDTO.ethnicMinorityCat = rs.getInt("ethnic_minority_cat");
		parliament_job_applicantDTO.freedomFighterCat = rs.getInt("freedom_fighter_cat");
		parliament_job_applicantDTO.nationalityCat = rs.getInt("nationality_cat");
		parliament_job_applicantDTO.disabilityCat = rs.getInt("disability_cat");
		parliament_job_applicantDTO.specialQuotaCat = rs.getInt("special_quota_cat");
		parliament_job_applicantDTO.height = rs.getDouble("height");
		parliament_job_applicantDTO.weight = rs.getDouble("weight");
		parliament_job_applicantDTO.chest = rs.getDouble("chest");
		parliament_job_applicantDTO.identificationTypeCat = rs.getInt("identification_type_cat");
		parliament_job_applicantDTO.identificationNo = rs.getString("identification_no");
		parliament_job_applicantDTO.permanentAddress = rs.getString("permanent_address");
		parliament_job_applicantDTO.presentAddress = rs.getString("present_address");
		parliament_job_applicantDTO.homeDistrictName = rs.getString("home_district_name");
		parliament_job_applicantDTO.contactNumber = rs.getString("contact_number");
		parliament_job_applicantDTO.email = rs.getString("email");
		parliament_job_applicantDTO.nid = rs.getString("nid");
		parliament_job_applicantDTO.careOfPresent = rs.getString("care_of_present");
		parliament_job_applicantDTO.careOfPermanent = rs.getString("care_of_permanent");
		parliament_job_applicantDTO.postCodePresent = rs.getString("post_code_present");
		parliament_job_applicantDTO.postCodePermanent = rs.getString("post_code_permanent");
		parliament_job_applicantDTO.jobApplicantCategoryCat = rs.getInt("job_applicant_category_cat");
		parliament_job_applicantDTO.jobApplicantCandidateCat = rs.getInt("job_applicant_candidate_cat");
		parliament_job_applicantDTO.jobApplicantCandidateText = rs.getString("job_applicant_candidate_text");
		parliament_job_applicantDTO.jobApplicantReligionCat = rs.getInt("job_applicant_religion_cat");
		parliament_job_applicantDTO.jobReferenceNo = rs.getString("job_reference_no");
		parliament_job_applicantDTO.jobReferenceNoDate = rs.getLong("job_reference_no_date");
		parliament_job_applicantDTO.currentPosition = rs.getString("current_position");
		parliament_job_applicantDTO.physicallyFit = rs.getLong("physically_fit");
		parliament_job_applicantDTO.experiencedInMotorDriving = rs.getLong("experienced_in_motor_driving");
		parliament_job_applicantDTO.bloodGroupCat = rs.getLong("blood_group_cat");
		parliament_job_applicantDTO.categoryFilesDropzone = rs.getLong("category_files_dropzone");
		parliament_job_applicantDTO.alternateContactNumber = rs.getString("alternate_contact_number");
		parliament_job_applicantDTO.userId = rs.getLong("user_id");
		parliament_job_applicantDTO.jobId = rs.getLong("job_id");
		parliament_job_applicantDTO.isDeleted = rs.getInt("isDeleted");
		parliament_job_applicantDTO.lastModificationTime = rs.getLong("lastModificationTime");
		parliament_job_applicantDTO.sameAsPresent = rs.getInt("same_as_present");
	}

	public Parliament_job_applicantDTO build(ResultSet rs)
	{
		try
		{
			Parliament_job_applicantDTO parliament_job_applicantDTO = new Parliament_job_applicantDTO();
			parliament_job_applicantDTO.iD = rs.getLong("ID");
			parliament_job_applicantDTO.nameBn = rs.getString("name_bn");
			parliament_job_applicantDTO.nameEn = rs.getString("name_en");
			parliament_job_applicantDTO.fatherName = rs.getString("father_name");
			parliament_job_applicantDTO.motherName = rs.getString("mother_name");
			parliament_job_applicantDTO.maritalStatusCat = rs.getLong("marital_status_cat");
			parliament_job_applicantDTO.spouseName = rs.getString("spouse_name");
			parliament_job_applicantDTO.dateOfBirth = rs.getLong("date_of_birth");
			parliament_job_applicantDTO.genderCat = rs.getInt("gender_cat");
			parliament_job_applicantDTO.employmentStatusCat = rs.getInt("employment_status_cat");
			parliament_job_applicantDTO.ethnicMinorityCat = rs.getInt("ethnic_minority_cat");
			parliament_job_applicantDTO.freedomFighterCat = rs.getInt("freedom_fighter_cat");
			parliament_job_applicantDTO.nationalityCat = rs.getInt("nationality_cat");
			parliament_job_applicantDTO.disabilityCat = rs.getInt("disability_cat");
			parliament_job_applicantDTO.specialQuotaCat = rs.getInt("special_quota_cat");
			parliament_job_applicantDTO.height = rs.getDouble("height");
			parliament_job_applicantDTO.weight = rs.getDouble("weight");
			parliament_job_applicantDTO.chest = rs.getDouble("chest");
			parliament_job_applicantDTO.identificationTypeCat = rs.getInt("identification_type_cat");
			parliament_job_applicantDTO.identificationNo = rs.getString("identification_no");
			parliament_job_applicantDTO.permanentAddress = rs.getString("permanent_address");
			parliament_job_applicantDTO.presentAddress = rs.getString("present_address");
			parliament_job_applicantDTO.homeDistrictName = rs.getString("home_district_name");
			parliament_job_applicantDTO.contactNumber = rs.getString("contact_number");
			parliament_job_applicantDTO.email = rs.getString("email");
			parliament_job_applicantDTO.nid = rs.getString("nid");
			parliament_job_applicantDTO.careOfPresent = rs.getString("care_of_present");
			parliament_job_applicantDTO.careOfPermanent = rs.getString("care_of_permanent");
			parliament_job_applicantDTO.postCodePresent = rs.getString("post_code_present");
			parliament_job_applicantDTO.postCodePermanent = rs.getString("post_code_permanent");
			parliament_job_applicantDTO.jobApplicantCategoryCat = rs.getInt("job_applicant_category_cat");
			parliament_job_applicantDTO.jobApplicantCandidateCat = rs.getInt("job_applicant_candidate_cat");
			parliament_job_applicantDTO.jobApplicantCandidateText = rs.getString("job_applicant_candidate_text");
			parliament_job_applicantDTO.jobApplicantReligionCat = rs.getInt("job_applicant_religion_cat");
			parliament_job_applicantDTO.jobReferenceNo = rs.getString("job_reference_no");
			parliament_job_applicantDTO.jobReferenceNoDate = rs.getLong("job_reference_no_date");
			parliament_job_applicantDTO.currentPosition = rs.getString("current_position");
			parliament_job_applicantDTO.physicallyFit = rs.getLong("physically_fit");
			parliament_job_applicantDTO.experiencedInMotorDriving = rs.getLong("experienced_in_motor_driving");
			parliament_job_applicantDTO.bloodGroupCat = rs.getLong("blood_group_cat");
			parliament_job_applicantDTO.categoryFilesDropzone = rs.getLong("category_files_dropzone");
			parliament_job_applicantDTO.alternateContactNumber = rs.getString("alternate_contact_number");
			parliament_job_applicantDTO.userId = rs.getLong("user_id");
			parliament_job_applicantDTO.jobId = rs.getLong("job_id");
			parliament_job_applicantDTO.isDeleted = rs.getInt("isDeleted");
			parliament_job_applicantDTO.lastModificationTime = rs.getLong("lastModificationTime");
			parliament_job_applicantDTO.sameAsPresent = rs.getInt("same_as_present");
			return parliament_job_applicantDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}




	//need another getter for repository
	public Parliament_job_applicantDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		Parliament_job_applicantDTO parliament_job_applicantDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
		return parliament_job_applicantDTO;
	}



	public List<Parliament_job_applicantDTO> getDTOs(Collection recordIDs){
		if(recordIDs.isEmpty()){
			return new ArrayList<>();
		}
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+= " ? ";
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);

	}






	//add repository
	public List<Parliament_job_applicantDTO> getAllParliament_job_applicant (boolean isFirstReload)
	{
		List<Parliament_job_applicantDTO> parliament_job_applicantDTOList = new ArrayList<>();

		String sql = "SELECT * FROM parliament_job_applicant";
		sql += " WHERE ";


		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;
		}
		sql += " order by parliament_job_applicant.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}


	public List<Parliament_job_applicantDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}


	public List<Parliament_job_applicantDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
													 String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
														  UserDTO userDTO, String filter, boolean tableHasJobCat)
	{
		boolean viewAll = false;

		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}

		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;



		String AnyfieldSql = "";
		String AllFieldSql = "";

		if(p_searchCriteria != null)
		{


			Enumeration names = p_searchCriteria.keys();
			String str, value;

			AnyfieldSql = "(";

			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);

			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
				System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						str.equals("name_bn")
								|| str.equals("name_en")
								|| str.equals("father_name")
								|| str.equals("mother_name")
								|| str.equals("marital_status_cat")
								|| str.equals("spouse_name")
								|| str.equals("date_of_birth_start")
								|| str.equals("date_of_birth_end")
								|| str.equals("gender_cat")
								|| str.equals("employment_status_cat")
								|| str.equals("ethnic_minority_cat")
								|| str.equals("freedom_fighter_cat")
								|| str.equals("nationality_cat")
								|| str.equals("disability_cat")
								|| str.equals("special_quota_cat")
								|| str.equals("height")
								|| str.equals("weight")
								|| str.equals("chest")
								|| str.equals("identification_type_cat")
								|| str.equals("identification_no")
								|| str.equals("home_district_name")
								|| str.equals("job_id")
								|| str.equals("email")
								|| str.equals("nid")
								|| str.equals("care_of_present")
								|| str.equals("care_of_permanent")
								|| str.equals("post_code_present")
								|| str.equals("post_code_permanent")
								|| str.equals("job_applicant_category_cat")
								|| str.equals("job_applicant_candidate_cat")
								|| str.equals("job_applicant_candidate_text")
								|| str.equals("job_applicant_religion_cat")
								|| str.equals("job_reference_no")
								|| str.equals("job_reference_no_date")
								|| str.equals("current_position")
								|| str.equals("physically_fit")
								|| str.equals("experienced_in_motor_driving")
								|| str.equals("blood_group_cat")
								|| str.equals("category_files_dropzone")
								|| str.equals("alternate_contact_number")
				)

				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}

					if(str.equals("name_bn"))
					{
						AllFieldSql += "" + tableName + ".name_bn like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".name_en like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("father_name"))
					{
						AllFieldSql += "" + tableName + ".father_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("mother_name"))
					{
						AllFieldSql += "" + tableName + ".mother_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("marital_status_cat"))
					{
						AllFieldSql += "" + tableName + ".marital_status_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("spouse_name"))
					{
						AllFieldSql += "" + tableName + ".spouse_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("date_of_birth_start"))
					{
						AllFieldSql += "" + tableName + ".date_of_birth >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("date_of_birth_end"))
					{
						AllFieldSql += "" + tableName + ".date_of_birth <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("gender_cat"))
					{
						AllFieldSql += "" + tableName + ".gender_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("employment_status_cat"))
					{
						AllFieldSql += "" + tableName + ".employment_status_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("ethnic_minority_cat"))
					{
						AllFieldSql += "" + tableName + ".ethnic_minority_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("freedom_fighter_cat"))
					{
						AllFieldSql += "" + tableName + ".freedom_fighter_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("nationality_cat"))
					{
						AllFieldSql += "" + tableName + ".nationality_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("disability_cat"))
					{
						AllFieldSql += "" + tableName + ".disability_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("special_quota_cat"))
					{
						AllFieldSql += "" + tableName + ".special_quota_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("height"))
					{
						AllFieldSql += "" + tableName + ".height like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("weight"))
					{
						AllFieldSql += "" + tableName + ".weight like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("chest"))
					{
						AllFieldSql += "" + tableName + ".chest like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("identification_type_cat"))
					{
						AllFieldSql += "" + tableName + ".identification_type_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("identification_no"))
					{
						AllFieldSql += "" + tableName + ".identification_no like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("home_district_name"))
					{
						AllFieldSql += "" + tableName + ".home_district_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("email"))
					{
						AllFieldSql += "" + tableName + ".email like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("nid"))
					{
						AllFieldSql += "" + tableName + ".nid like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("job_id"))
					{
						AllFieldSql += "" + tableName + ".job_id = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("care_of_present"))
					{
						AllFieldSql += "" + tableName + ".care_of_present = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("care_of_permanent"))
					{
						AllFieldSql += "" + tableName + ".care_of_permanent = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("post_code_present"))
					{
						AllFieldSql += "" + tableName + ".post_code_present = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("post_code_permanent"))
					{
						AllFieldSql += "" + tableName + ".post_code_permanent = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("job_applicant_category_cat"))
					{
						AllFieldSql += "" + tableName + ".job_applicant_category_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("job_applicant_candidate_cat"))
					{
						AllFieldSql += "" + tableName + ".job_applicant_candidate_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("job_applicant_religion_cat"))
					{
						AllFieldSql += "" + tableName + ".job_applicant_religion_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("job_applicant_candidate_text"))
					{
						AllFieldSql += "" + tableName + ".job_applicant_candidate_text = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("job_reference_no"))
					{
						AllFieldSql += "" + tableName + ".job_reference_no = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("job_reference_no_date"))
					{
						AllFieldSql += "" + tableName + ".job_reference_no_date = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("current_position"))
					{
						AllFieldSql += "" + tableName + ".current_position = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("physically_fit"))
					{
						AllFieldSql += "" + tableName + ".physically_fit = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("experienced_in_motor_driving"))
					{
						AllFieldSql += "" + tableName + ".experienced_in_motor_driving = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("blood_group_cat"))
					{
						AllFieldSql += "" + tableName + ".blood_group_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("category_files_dropzone"))
					{
						AllFieldSql += "" + tableName + ".category_files_dropzone = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("alternate_contact_number"))
					{
						AllFieldSql += "" + tableName + ".alternate_contact_number = " + p_searchCriteria.get(str);
						i ++;
					}


				}
			}

			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);


		}


		sql += " WHERE ";

		sql += " (" + tableName + ".isDeleted = 0 ";
		sql += ")";


		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}

		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;

		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{
			sql += " AND " + AllFieldSql;
		}



		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);

		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}

		System.out.println("-------------- sql = " + sql);

		return sql;
	}

	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
										   boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
	{
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
	}

	public List<Parliament_job_applicantDTO> getAllParliament_job_applicantByUserId (long userId)
	{
		String sql = "SELECT * FROM parliament_job_applicant";
		sql += " WHERE ";
		sql+=" isDeleted =  0 and user_id = ? ";
		sql += " order by parliament_job_applicant.lastModificationTime desc";
		printSql(sql);

		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(userId), this::build);
	}

	public Map<String,Integer> getGenderList(long jobId,String language){

		List<Long> applicantIds = new Job_applicant_applicationDAO().
				getApplicantIdsByJobIDAndLevelZero(jobId);

		List<Parliament_job_applicantDTO> job_applicantDTOS = new ArrayList<>();
		for(Long appID: applicantIds){
			Parliament_job_applicantDTO dto = Parliament_job_applicantRepository.getInstance().
					getParliament_job_applicantDTOByID(appID);
			if(dto != null){
				job_applicantDTOS.add(dto);
			}

		}
//				getDTOs(applicantIds);
		if(job_applicantDTOS.size() == 0){
			return new HashMap<>();
		}

		List<CategoryLanguageModel> languageModelList = CatRepository.getInstance().getCategoryLanguageModelList("gender");
		Map<Integer,CategoryLanguageModel> languageModelMap = languageModelList.stream()
				.collect(Collectors.toMap(model->model.categoryValue, model->model,(model1, model2)->model1));
		boolean isLanguageBangla = "Bangla".equalsIgnoreCase(language);
		Map<String,Integer> ageMap = job_applicantDTOS.stream()
				.map(employee-> getGender(languageModelMap.get(employee.genderCat),isLanguageBangla))
				.collect(Collectors.groupingBy(genderText->genderText,Collectors.collectingAndThen(Collectors.counting(),Long::intValue)));
		return new TreeMap<>(ageMap);

	}

	private String getGender(CategoryLanguageModel model,boolean isLanguageBangla){
		return model == null ? (isLanguageBangla ? "নির্ধারিত না" : "Not Assigned") : (isLanguageBangla ? model.banglaText : model.englishText);
	}

}
	