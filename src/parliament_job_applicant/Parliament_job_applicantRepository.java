package parliament_job_applicant;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Parliament_job_applicantRepository implements Repository {
	Parliament_job_applicantDAO parliament_job_applicantDAO = null;
	
	public void setDAO(Parliament_job_applicantDAO parliament_job_applicantDAO)
	{
		this.parliament_job_applicantDAO = parliament_job_applicantDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Parliament_job_applicantRepository.class);
	Map<Long, Parliament_job_applicantDTO>mapOfParliament_job_applicantDTOToiD;
//	Map<String, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOTonameBn;
//	Map<String, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOTofatherName;
//	Map<String, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOTomotherName;
//	Map<Long, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOTomaritalStatusCat;
//	Map<String, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOTospouseName;
//	Map<Long, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOTodateOfBirth;
//	Map<Integer, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOTogenderCat;
//	Map<Integer, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOToemploymentStatusCat;
//	Map<Integer, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOToethnicMinorityCat;
//	Map<Integer, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOTofreedomFighterCat;
//	Map<Integer, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOTonationalityCat;
//	Map<Integer, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOTodisabilityCat;
//	Map<Integer, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOTospecialQuotaCat;
//	Map<Double, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOToheight;
//	Map<Double, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOToweight;
//	Map<Double, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOTochest;
//	Map<Integer, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOToidentificationTypeCat;
//	Map<String, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOToidentificationNo;
//	Map<String, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOTopermanentAddress;
//	Map<String, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOTopresentAddress;
//	Map<String, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOTohomeDistrictName;
//	Map<String, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOTocontactNumber;
//	Map<String, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOToemail;
//	Map<Long, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOTolastModificationTime;
//	Map<Long, Set<Parliament_job_applicantDTO> >mapOfParliament_job_applicantDTOTouserId;


	static Parliament_job_applicantRepository instance = null;  
	private Parliament_job_applicantRepository(){
		mapOfParliament_job_applicantDTOToiD = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOTonameBn = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOTofatherName = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOTomotherName = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOTomaritalStatusCat = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOTospouseName = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOTodateOfBirth = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOTogenderCat = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOToemploymentStatusCat = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOToethnicMinorityCat = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOTofreedomFighterCat = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOTonationalityCat = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOTodisabilityCat = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOTospecialQuotaCat = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOToheight = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOToweight = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOTochest = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOToidentificationTypeCat = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOToidentificationNo = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOTopermanentAddress = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOTopresentAddress = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOTohomeDistrictName = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOTocontactNumber = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOToemail = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOTolastModificationTime = new ConcurrentHashMap<>();
//		mapOfParliament_job_applicantDTOTouserId = new ConcurrentHashMap<>();

		setDAO(new Parliament_job_applicantDAO());
		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Parliament_job_applicantRepository getInstance(){
		if (instance == null){
			instance = new Parliament_job_applicantRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(parliament_job_applicantDAO == null)
		{
			return;
		}
		try {
			List<Parliament_job_applicantDTO> parliament_job_applicantDTOs = parliament_job_applicantDAO.getAllParliament_job_applicant(reloadAll);
			for(Parliament_job_applicantDTO parliament_job_applicantDTO : parliament_job_applicantDTOs) {
				Parliament_job_applicantDTO oldParliament_job_applicantDTO = getParliament_job_applicantDTOByID(parliament_job_applicantDTO.iD);
				if( oldParliament_job_applicantDTO != null ) {
					mapOfParliament_job_applicantDTOToiD.remove(oldParliament_job_applicantDTO.iD);
				
//					if(mapOfParliament_job_applicantDTOTonameBn.containsKey(oldParliament_job_applicantDTO.nameBn)) {
//						mapOfParliament_job_applicantDTOTonameBn.get(oldParliament_job_applicantDTO.nameBn).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOTonameBn.get(oldParliament_job_applicantDTO.nameBn).isEmpty()) {
//						mapOfParliament_job_applicantDTOTonameBn.remove(oldParliament_job_applicantDTO.nameBn);
//					}
//
//					if(mapOfParliament_job_applicantDTOTofatherName.containsKey(oldParliament_job_applicantDTO.fatherName)) {
//						mapOfParliament_job_applicantDTOTofatherName.get(oldParliament_job_applicantDTO.fatherName).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOTofatherName.get(oldParliament_job_applicantDTO.fatherName).isEmpty()) {
//						mapOfParliament_job_applicantDTOTofatherName.remove(oldParliament_job_applicantDTO.fatherName);
//					}
//
//					if(mapOfParliament_job_applicantDTOTomotherName.containsKey(oldParliament_job_applicantDTO.motherName)) {
//						mapOfParliament_job_applicantDTOTomotherName.get(oldParliament_job_applicantDTO.motherName).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOTomotherName.get(oldParliament_job_applicantDTO.motherName).isEmpty()) {
//						mapOfParliament_job_applicantDTOTomotherName.remove(oldParliament_job_applicantDTO.motherName);
//					}
//
//					if(mapOfParliament_job_applicantDTOTomaritalStatusCat.containsKey(oldParliament_job_applicantDTO.maritalStatusCat)) {
//						mapOfParliament_job_applicantDTOTomaritalStatusCat.get(oldParliament_job_applicantDTO.maritalStatusCat).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOTomaritalStatusCat.get(oldParliament_job_applicantDTO.maritalStatusCat).isEmpty()) {
//						mapOfParliament_job_applicantDTOTomaritalStatusCat.remove(oldParliament_job_applicantDTO.maritalStatusCat);
//					}
//
//					if(mapOfParliament_job_applicantDTOTospouseName.containsKey(oldParliament_job_applicantDTO.spouseName)) {
//						mapOfParliament_job_applicantDTOTospouseName.get(oldParliament_job_applicantDTO.spouseName).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOTospouseName.get(oldParliament_job_applicantDTO.spouseName).isEmpty()) {
//						mapOfParliament_job_applicantDTOTospouseName.remove(oldParliament_job_applicantDTO.spouseName);
//					}
//
//					if(mapOfParliament_job_applicantDTOTodateOfBirth.containsKey(oldParliament_job_applicantDTO.dateOfBirth)) {
//						mapOfParliament_job_applicantDTOTodateOfBirth.get(oldParliament_job_applicantDTO.dateOfBirth).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOTodateOfBirth.get(oldParliament_job_applicantDTO.dateOfBirth).isEmpty()) {
//						mapOfParliament_job_applicantDTOTodateOfBirth.remove(oldParliament_job_applicantDTO.dateOfBirth);
//					}
//
//					if(mapOfParliament_job_applicantDTOTogenderCat.containsKey(oldParliament_job_applicantDTO.genderCat)) {
//						mapOfParliament_job_applicantDTOTogenderCat.get(oldParliament_job_applicantDTO.genderCat).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOTogenderCat.get(oldParliament_job_applicantDTO.genderCat).isEmpty()) {
//						mapOfParliament_job_applicantDTOTogenderCat.remove(oldParliament_job_applicantDTO.genderCat);
//					}
//
//					if(mapOfParliament_job_applicantDTOToemploymentStatusCat.containsKey(oldParliament_job_applicantDTO.employmentStatusCat)) {
//						mapOfParliament_job_applicantDTOToemploymentStatusCat.get(oldParliament_job_applicantDTO.employmentStatusCat).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOToemploymentStatusCat.get(oldParliament_job_applicantDTO.employmentStatusCat).isEmpty()) {
//						mapOfParliament_job_applicantDTOToemploymentStatusCat.remove(oldParliament_job_applicantDTO.employmentStatusCat);
//					}
//
//					if(mapOfParliament_job_applicantDTOToethnicMinorityCat.containsKey(oldParliament_job_applicantDTO.ethnicMinorityCat)) {
//						mapOfParliament_job_applicantDTOToethnicMinorityCat.get(oldParliament_job_applicantDTO.ethnicMinorityCat).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOToethnicMinorityCat.get(oldParliament_job_applicantDTO.ethnicMinorityCat).isEmpty()) {
//						mapOfParliament_job_applicantDTOToethnicMinorityCat.remove(oldParliament_job_applicantDTO.ethnicMinorityCat);
//					}
//
//					if(mapOfParliament_job_applicantDTOTofreedomFighterCat.containsKey(oldParliament_job_applicantDTO.freedomFighterCat)) {
//						mapOfParliament_job_applicantDTOTofreedomFighterCat.get(oldParliament_job_applicantDTO.freedomFighterCat).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOTofreedomFighterCat.get(oldParliament_job_applicantDTO.freedomFighterCat).isEmpty()) {
//						mapOfParliament_job_applicantDTOTofreedomFighterCat.remove(oldParliament_job_applicantDTO.freedomFighterCat);
//					}
//
//					if(mapOfParliament_job_applicantDTOTonationalityCat.containsKey(oldParliament_job_applicantDTO.nationalityCat)) {
//						mapOfParliament_job_applicantDTOTonationalityCat.get(oldParliament_job_applicantDTO.nationalityCat).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOTonationalityCat.get(oldParliament_job_applicantDTO.nationalityCat).isEmpty()) {
//						mapOfParliament_job_applicantDTOTonationalityCat.remove(oldParliament_job_applicantDTO.nationalityCat);
//					}
//
//					if(mapOfParliament_job_applicantDTOTodisabilityCat.containsKey(oldParliament_job_applicantDTO.disabilityCat)) {
//						mapOfParliament_job_applicantDTOTodisabilityCat.get(oldParliament_job_applicantDTO.disabilityCat).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOTodisabilityCat.get(oldParliament_job_applicantDTO.disabilityCat).isEmpty()) {
//						mapOfParliament_job_applicantDTOTodisabilityCat.remove(oldParliament_job_applicantDTO.disabilityCat);
//					}
//
//					if(mapOfParliament_job_applicantDTOTospecialQuotaCat.containsKey(oldParliament_job_applicantDTO.specialQuotaCat)) {
//						mapOfParliament_job_applicantDTOTospecialQuotaCat.get(oldParliament_job_applicantDTO.specialQuotaCat).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOTospecialQuotaCat.get(oldParliament_job_applicantDTO.specialQuotaCat).isEmpty()) {
//						mapOfParliament_job_applicantDTOTospecialQuotaCat.remove(oldParliament_job_applicantDTO.specialQuotaCat);
//					}
//
//					if(mapOfParliament_job_applicantDTOToheight.containsKey(oldParliament_job_applicantDTO.height)) {
//						mapOfParliament_job_applicantDTOToheight.get(oldParliament_job_applicantDTO.height).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOToheight.get(oldParliament_job_applicantDTO.height).isEmpty()) {
//						mapOfParliament_job_applicantDTOToheight.remove(oldParliament_job_applicantDTO.height);
//					}
//
//					if(mapOfParliament_job_applicantDTOToweight.containsKey(oldParliament_job_applicantDTO.weight)) {
//						mapOfParliament_job_applicantDTOToweight.get(oldParliament_job_applicantDTO.weight).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOToweight.get(oldParliament_job_applicantDTO.weight).isEmpty()) {
//						mapOfParliament_job_applicantDTOToweight.remove(oldParliament_job_applicantDTO.weight);
//					}
//
//					if(mapOfParliament_job_applicantDTOTochest.containsKey(oldParliament_job_applicantDTO.chest)) {
//						mapOfParliament_job_applicantDTOTochest.get(oldParliament_job_applicantDTO.chest).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOTochest.get(oldParliament_job_applicantDTO.chest).isEmpty()) {
//						mapOfParliament_job_applicantDTOTochest.remove(oldParliament_job_applicantDTO.chest);
//					}
//
//					if(mapOfParliament_job_applicantDTOToidentificationTypeCat.containsKey(oldParliament_job_applicantDTO.identificationTypeCat)) {
//						mapOfParliament_job_applicantDTOToidentificationTypeCat.get(oldParliament_job_applicantDTO.identificationTypeCat).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOToidentificationTypeCat.get(oldParliament_job_applicantDTO.identificationTypeCat).isEmpty()) {
//						mapOfParliament_job_applicantDTOToidentificationTypeCat.remove(oldParliament_job_applicantDTO.identificationTypeCat);
//					}
//
//					if(mapOfParliament_job_applicantDTOToidentificationNo.containsKey(oldParliament_job_applicantDTO.identificationNo)) {
//						mapOfParliament_job_applicantDTOToidentificationNo.get(oldParliament_job_applicantDTO.identificationNo).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOToidentificationNo.get(oldParliament_job_applicantDTO.identificationNo).isEmpty()) {
//						mapOfParliament_job_applicantDTOToidentificationNo.remove(oldParliament_job_applicantDTO.identificationNo);
//					}
//
//					if(mapOfParliament_job_applicantDTOTopermanentAddress.containsKey(oldParliament_job_applicantDTO.permanentAddress)) {
//						mapOfParliament_job_applicantDTOTopermanentAddress.get(oldParliament_job_applicantDTO.permanentAddress).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOTopermanentAddress.get(oldParliament_job_applicantDTO.permanentAddress).isEmpty()) {
//						mapOfParliament_job_applicantDTOTopermanentAddress.remove(oldParliament_job_applicantDTO.permanentAddress);
//					}
//
//					if(mapOfParliament_job_applicantDTOTopresentAddress.containsKey(oldParliament_job_applicantDTO.presentAddress)) {
//						mapOfParliament_job_applicantDTOTopresentAddress.get(oldParliament_job_applicantDTO.presentAddress).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOTopresentAddress.get(oldParliament_job_applicantDTO.presentAddress).isEmpty()) {
//						mapOfParliament_job_applicantDTOTopresentAddress.remove(oldParliament_job_applicantDTO.presentAddress);
//					}
//
//					if(mapOfParliament_job_applicantDTOTohomeDistrictName.containsKey(oldParliament_job_applicantDTO.homeDistrictName)) {
//						mapOfParliament_job_applicantDTOTohomeDistrictName.get(oldParliament_job_applicantDTO.homeDistrictName).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOTohomeDistrictName.get(oldParliament_job_applicantDTO.homeDistrictName).isEmpty()) {
//						mapOfParliament_job_applicantDTOTohomeDistrictName.remove(oldParliament_job_applicantDTO.homeDistrictName);
//					}
//
//					if(mapOfParliament_job_applicantDTOTocontactNumber.containsKey(oldParliament_job_applicantDTO.contactNumber)) {
//						mapOfParliament_job_applicantDTOTocontactNumber.get(oldParliament_job_applicantDTO.contactNumber).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOTocontactNumber.get(oldParliament_job_applicantDTO.contactNumber).isEmpty()) {
//						mapOfParliament_job_applicantDTOTocontactNumber.remove(oldParliament_job_applicantDTO.contactNumber);
//					}
//
//					if(mapOfParliament_job_applicantDTOToemail.containsKey(oldParliament_job_applicantDTO.email)) {
//						mapOfParliament_job_applicantDTOToemail.get(oldParliament_job_applicantDTO.email).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOToemail.get(oldParliament_job_applicantDTO.email).isEmpty()) {
//						mapOfParliament_job_applicantDTOToemail.remove(oldParliament_job_applicantDTO.email);
//					}
//
//					if(mapOfParliament_job_applicantDTOTolastModificationTime.containsKey(oldParliament_job_applicantDTO.lastModificationTime)) {
//						mapOfParliament_job_applicantDTOTolastModificationTime.get(oldParliament_job_applicantDTO.lastModificationTime).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOTolastModificationTime.get(oldParliament_job_applicantDTO.lastModificationTime).isEmpty()) {
//						mapOfParliament_job_applicantDTOTolastModificationTime.remove(oldParliament_job_applicantDTO.lastModificationTime);
//					}
//
//					if(mapOfParliament_job_applicantDTOTouserId.containsKey(oldParliament_job_applicantDTO.userId)) {
//						mapOfParliament_job_applicantDTOTouserId.get(oldParliament_job_applicantDTO.userId).remove(oldParliament_job_applicantDTO);
//					}
//					if(mapOfParliament_job_applicantDTOTouserId.get(oldParliament_job_applicantDTO.userId).isEmpty()) {
//						mapOfParliament_job_applicantDTOTouserId.remove(oldParliament_job_applicantDTO.userId);
//					}
					
					
				}
				if(parliament_job_applicantDTO.isDeleted == 0) 
				{
					
					mapOfParliament_job_applicantDTOToiD.put(parliament_job_applicantDTO.iD, parliament_job_applicantDTO);
				
//					if( ! mapOfParliament_job_applicantDTOTonameBn.containsKey(parliament_job_applicantDTO.nameBn)) {
//						mapOfParliament_job_applicantDTOTonameBn.put(parliament_job_applicantDTO.nameBn, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOTonameBn.get(parliament_job_applicantDTO.nameBn).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOTofatherName.containsKey(parliament_job_applicantDTO.fatherName)) {
//						mapOfParliament_job_applicantDTOTofatherName.put(parliament_job_applicantDTO.fatherName, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOTofatherName.get(parliament_job_applicantDTO.fatherName).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOTomotherName.containsKey(parliament_job_applicantDTO.motherName)) {
//						mapOfParliament_job_applicantDTOTomotherName.put(parliament_job_applicantDTO.motherName, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOTomotherName.get(parliament_job_applicantDTO.motherName).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOTomaritalStatusCat.containsKey(parliament_job_applicantDTO.maritalStatusCat)) {
//						mapOfParliament_job_applicantDTOTomaritalStatusCat.put(parliament_job_applicantDTO.maritalStatusCat, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOTomaritalStatusCat.get(parliament_job_applicantDTO.maritalStatusCat).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOTospouseName.containsKey(parliament_job_applicantDTO.spouseName)) {
//						mapOfParliament_job_applicantDTOTospouseName.put(parliament_job_applicantDTO.spouseName, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOTospouseName.get(parliament_job_applicantDTO.spouseName).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOTodateOfBirth.containsKey(parliament_job_applicantDTO.dateOfBirth)) {
//						mapOfParliament_job_applicantDTOTodateOfBirth.put(parliament_job_applicantDTO.dateOfBirth, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOTodateOfBirth.get(parliament_job_applicantDTO.dateOfBirth).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOTogenderCat.containsKey(parliament_job_applicantDTO.genderCat)) {
//						mapOfParliament_job_applicantDTOTogenderCat.put(parliament_job_applicantDTO.genderCat, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOTogenderCat.get(parliament_job_applicantDTO.genderCat).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOToemploymentStatusCat.containsKey(parliament_job_applicantDTO.employmentStatusCat)) {
//						mapOfParliament_job_applicantDTOToemploymentStatusCat.put(parliament_job_applicantDTO.employmentStatusCat, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOToemploymentStatusCat.get(parliament_job_applicantDTO.employmentStatusCat).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOToethnicMinorityCat.containsKey(parliament_job_applicantDTO.ethnicMinorityCat)) {
//						mapOfParliament_job_applicantDTOToethnicMinorityCat.put(parliament_job_applicantDTO.ethnicMinorityCat, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOToethnicMinorityCat.get(parliament_job_applicantDTO.ethnicMinorityCat).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOTofreedomFighterCat.containsKey(parliament_job_applicantDTO.freedomFighterCat)) {
//						mapOfParliament_job_applicantDTOTofreedomFighterCat.put(parliament_job_applicantDTO.freedomFighterCat, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOTofreedomFighterCat.get(parliament_job_applicantDTO.freedomFighterCat).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOTonationalityCat.containsKey(parliament_job_applicantDTO.nationalityCat)) {
//						mapOfParliament_job_applicantDTOTonationalityCat.put(parliament_job_applicantDTO.nationalityCat, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOTonationalityCat.get(parliament_job_applicantDTO.nationalityCat).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOTodisabilityCat.containsKey(parliament_job_applicantDTO.disabilityCat)) {
//						mapOfParliament_job_applicantDTOTodisabilityCat.put(parliament_job_applicantDTO.disabilityCat, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOTodisabilityCat.get(parliament_job_applicantDTO.disabilityCat).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOTospecialQuotaCat.containsKey(parliament_job_applicantDTO.specialQuotaCat)) {
//						mapOfParliament_job_applicantDTOTospecialQuotaCat.put(parliament_job_applicantDTO.specialQuotaCat, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOTospecialQuotaCat.get(parliament_job_applicantDTO.specialQuotaCat).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOToheight.containsKey(parliament_job_applicantDTO.height)) {
//						mapOfParliament_job_applicantDTOToheight.put(parliament_job_applicantDTO.height, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOToheight.get(parliament_job_applicantDTO.height).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOToweight.containsKey(parliament_job_applicantDTO.weight)) {
//						mapOfParliament_job_applicantDTOToweight.put(parliament_job_applicantDTO.weight, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOToweight.get(parliament_job_applicantDTO.weight).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOTochest.containsKey(parliament_job_applicantDTO.chest)) {
//						mapOfParliament_job_applicantDTOTochest.put(parliament_job_applicantDTO.chest, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOTochest.get(parliament_job_applicantDTO.chest).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOToidentificationTypeCat.containsKey(parliament_job_applicantDTO.identificationTypeCat)) {
//						mapOfParliament_job_applicantDTOToidentificationTypeCat.put(parliament_job_applicantDTO.identificationTypeCat, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOToidentificationTypeCat.get(parliament_job_applicantDTO.identificationTypeCat).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOToidentificationNo.containsKey(parliament_job_applicantDTO.identificationNo)) {
//						mapOfParliament_job_applicantDTOToidentificationNo.put(parliament_job_applicantDTO.identificationNo, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOToidentificationNo.get(parliament_job_applicantDTO.identificationNo).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOTopermanentAddress.containsKey(parliament_job_applicantDTO.permanentAddress)) {
//						mapOfParliament_job_applicantDTOTopermanentAddress.put(parliament_job_applicantDTO.permanentAddress, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOTopermanentAddress.get(parliament_job_applicantDTO.permanentAddress).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOTopresentAddress.containsKey(parliament_job_applicantDTO.presentAddress)) {
//						mapOfParliament_job_applicantDTOTopresentAddress.put(parliament_job_applicantDTO.presentAddress, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOTopresentAddress.get(parliament_job_applicantDTO.presentAddress).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOTohomeDistrictName.containsKey(parliament_job_applicantDTO.homeDistrictName)) {
//						mapOfParliament_job_applicantDTOTohomeDistrictName.put(parliament_job_applicantDTO.homeDistrictName, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOTohomeDistrictName.get(parliament_job_applicantDTO.homeDistrictName).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOTocontactNumber.containsKey(parliament_job_applicantDTO.contactNumber)) {
//						mapOfParliament_job_applicantDTOTocontactNumber.put(parliament_job_applicantDTO.contactNumber, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOTocontactNumber.get(parliament_job_applicantDTO.contactNumber).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOToemail.containsKey(parliament_job_applicantDTO.email)) {
//						mapOfParliament_job_applicantDTOToemail.put(parliament_job_applicantDTO.email, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOToemail.get(parliament_job_applicantDTO.email).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOTolastModificationTime.containsKey(parliament_job_applicantDTO.lastModificationTime)) {
//						mapOfParliament_job_applicantDTOTolastModificationTime.put(parliament_job_applicantDTO.lastModificationTime, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOTolastModificationTime.get(parliament_job_applicantDTO.lastModificationTime).add(parliament_job_applicantDTO);
//
//					if( ! mapOfParliament_job_applicantDTOTouserId.containsKey(parliament_job_applicantDTO.userId)) {
//						mapOfParliament_job_applicantDTOTouserId.put(parliament_job_applicantDTO.userId, new HashSet<>());
//					}
//					mapOfParliament_job_applicantDTOTouserId.get(parliament_job_applicantDTO.userId).add(parliament_job_applicantDTO);
//
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Parliament_job_applicantDTO> getParliament_job_applicantList() {
		List <Parliament_job_applicantDTO> parliament_job_applicants = new ArrayList<Parliament_job_applicantDTO>(this.mapOfParliament_job_applicantDTOToiD.values());
		return parliament_job_applicants;
	}
	
	
	public Parliament_job_applicantDTO getParliament_job_applicantDTOByID( long ID){
		return mapOfParliament_job_applicantDTOToiD.get(ID);
	}
	
	
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOByname_bn(String name_bn) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOTonameBn.getOrDefault(name_bn,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOByfather_name(String father_name) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOTofatherName.getOrDefault(father_name,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOBymother_name(String mother_name) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOTomotherName.getOrDefault(mother_name,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOBymarital_status_cat(long marital_status_cat) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOTomaritalStatusCat.getOrDefault(marital_status_cat,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOByspouse_name(String spouse_name) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOTospouseName.getOrDefault(spouse_name,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOBydate_of_birth(long date_of_birth) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOTodateOfBirth.getOrDefault(date_of_birth,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOBygender_cat(int gender_cat) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOTogenderCat.getOrDefault(gender_cat,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOByemployment_status_cat(int employment_status_cat) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOToemploymentStatusCat.getOrDefault(employment_status_cat,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOByethnic_minority_cat(int ethnic_minority_cat) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOToethnicMinorityCat.getOrDefault(ethnic_minority_cat,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOByfreedom_fighter_cat(int freedom_fighter_cat) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOTofreedomFighterCat.getOrDefault(freedom_fighter_cat,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOBynationality_cat(int nationality_cat) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOTonationalityCat.getOrDefault(nationality_cat,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOBydisability_cat(int disability_cat) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOTodisabilityCat.getOrDefault(disability_cat,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOByspecial_quota_cat(int special_quota_cat) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOTospecialQuotaCat.getOrDefault(special_quota_cat,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOByheight(double height) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOToheight.getOrDefault(height,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOByweight(double weight) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOToweight.getOrDefault(weight,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOBychest(double chest) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOTochest.getOrDefault(chest,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOByidentification_type_cat(int identification_type_cat) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOToidentificationTypeCat.getOrDefault(identification_type_cat,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOByidentification_no(String identification_no) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOToidentificationNo.getOrDefault(identification_no,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOBypermanent_address(String permanent_address) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOTopermanentAddress.getOrDefault(permanent_address,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOBypresent_address(String present_address) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOTopresentAddress.getOrDefault(present_address,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOByhome_district_name(String home_district_name) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOTohomeDistrictName.getOrDefault(home_district_name,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOBycontact_number(String contact_number) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOTocontactNumber.getOrDefault(contact_number,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOByemail(String email) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOToemail.getOrDefault(email,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOBylastModificationTime(long lastModificationTime) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
//	}
//
//
//	public List<Parliament_job_applicantDTO> getParliament_job_applicantDTOByuser_id(long user_id) {
//		return new ArrayList<>( mapOfParliament_job_applicantDTOTouserId.getOrDefault(user_id,new HashSet<>()));
//	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "parliament_job_applicant";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


