package worldcup;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class WorldcupDAO  implements CommonDAOService<WorldcupDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private WorldcupDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("name_en"," and (name_en like ?)");
		searchMap.put("name_bn"," and (name_bn like ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final WorldcupDAO INSTANCE = new WorldcupDAO();
	}

	public static WorldcupDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(WorldcupDTO worldcupDTO)
	{
		worldcupDTO.searchColumn = "";
		worldcupDTO.searchColumn += worldcupDTO.nameEn + " ";
		worldcupDTO.searchColumn += worldcupDTO.nameBn + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, WorldcupDTO worldcupDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(worldcupDTO);
		if(isInsert)
		{
			ps.setObject(++index,worldcupDTO.iD);
		}
		ps.setObject(++index,worldcupDTO.nameEn);
		ps.setObject(++index,worldcupDTO.nameBn);
		ps.setObject(++index,worldcupDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(++index,worldcupDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,worldcupDTO.iD);
		}
	}
	
	@Override
	public WorldcupDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			WorldcupDTO worldcupDTO = new WorldcupDTO();
			int i = 0;
			worldcupDTO.iD = rs.getLong(columnNames[i++]);
			worldcupDTO.nameEn = rs.getString(columnNames[i++]);
			worldcupDTO.nameBn = rs.getString(columnNames[i++]);
			worldcupDTO.searchColumn = rs.getString(columnNames[i++]);
			worldcupDTO.isDeleted = rs.getInt(columnNames[i++]);
			worldcupDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return worldcupDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public WorldcupDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "worldcup";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((WorldcupDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((WorldcupDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	