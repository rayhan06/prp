package worldcup;
import java.util.*; 
import util.*; 


public class WorldcupDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
	
	
    @Override
	public String toString() {
            return "$WorldcupDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}