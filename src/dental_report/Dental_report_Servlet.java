package dental_report;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import language.LC;
import language.LM;
import login.LoginDTO;
import pbReport.ReportService;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.ReportRequestHandler;

@WebServlet("/Dental_report_Servlet")
public class Dental_report_Servlet extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{	
		{"criteria","","dt_er_id","=","","String","","","any","dt_user_name",LC.HM_DENTAL_TECHNOLIST + "", "userNameToEmployeeRecordId"},		
		{"criteria","","dental_action_cat","=","AND","int","","","any","dentalActionCat",LC.HM_DENTAL_ACTION + ""},
		{"criteria","","prescription_date",">=","AND","long","","",1 + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","prescription_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},
	};
	
	String[][] Display =
	{
		{"display","","dt_er_id","erIdToName",""},		
		{"display","","dental_action_cat","cat",""},		
		{"display","","count(id)","int",""}
	};
	
	String GroupBy = "dental_action_cat, dt_er_id";
	String OrderBY = "  ";
	
	ReportRequestHandler reportRequestHandler;
	
	public Dental_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = " dental_activities ";

		Display[0][4] = LM.getText(LC.HM_DENTAL_TECHNOLIST, loginDTO);
		Display[1][4] = LM.getText(LC.HM_DENTAL_ACTION, loginDTO);
		Display[2][4] = LM.getText(LC.HM_COUNT, loginDTO);
	
		String reportName = language.equalsIgnoreCase("english")?"Dental Report":"দন্ত বিষয়ক রিপোর্ট";

		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "dental_report",
				MenuConstants.DER, language, reportName, "dental_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}

}
