package patient_measurement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Patient_measurementDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Patient_measurementDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Patient_measurementMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"appointment_id",
			"name",
			"date_of_birth",
			"age",
			"height",
			"weight",
			"blood_pressure_diastole",
			"blood_pressure_systole",
			"pulse",
			"temperature",
			"oxygen_saturation",
			"bmi",
			"blood_sugar",
			"search_column",
			"measurer_user_name",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Patient_measurementDAO()
	{
		this("patient_measurement");		
	}
	
	public void setSearchColumn(Patient_measurementDTO patient_measurementDTO)
	{
		patient_measurementDTO.searchColumn = "";
		patient_measurementDTO.searchColumn += patient_measurementDTO.name + " ";
		patient_measurementDTO.searchColumn += patient_measurementDTO.age + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Patient_measurementDTO patient_measurementDTO = (Patient_measurementDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(patient_measurementDTO);
		if(isInsert)
		{
			ps.setObject(index++,patient_measurementDTO.iD);
		}
		ps.setObject(index++,patient_measurementDTO.appointmentId);
		ps.setObject(index++,patient_measurementDTO.name);
		ps.setObject(index++,patient_measurementDTO.dateOfBirth);
		ps.setObject(index++,patient_measurementDTO.age);
		ps.setObject(index++,patient_measurementDTO.height);
		ps.setObject(index++,patient_measurementDTO.weight);
		ps.setObject(index++,patient_measurementDTO.bloodPressureDiastole);
		ps.setObject(index++,patient_measurementDTO.bloodPressureSystole);
		ps.setObject(index++,patient_measurementDTO.pulse);
		ps.setObject(index++,patient_measurementDTO.temperature);
		ps.setObject(index++,patient_measurementDTO.oxygenSaturation);
		ps.setObject(index++,patient_measurementDTO.bmi);
		ps.setObject(index++,patient_measurementDTO.sugar);
		ps.setObject(index++,patient_measurementDTO.searchColumn);
		ps.setObject(index++,patient_measurementDTO.measurerUserName);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Patient_measurementDTO build(ResultSet rs)
	{
		try
		{
			Patient_measurementDTO patient_measurementDTO = new Patient_measurementDTO();
			patient_measurementDTO.iD = rs.getLong("ID");
			patient_measurementDTO.appointmentId = rs.getLong("appointment_id");
			patient_measurementDTO.name = rs.getString("name");
			patient_measurementDTO.dateOfBirth = rs.getLong("date_of_birth");
			patient_measurementDTO.age = rs.getLong("age");
			patient_measurementDTO.height = rs.getDouble("height");
			patient_measurementDTO.weight = rs.getDouble("weight");
			patient_measurementDTO.bloodPressureDiastole = rs.getDouble("blood_pressure_diastole");
			patient_measurementDTO.bloodPressureSystole = rs.getDouble("blood_pressure_systole");
			patient_measurementDTO.pulse = rs.getDouble("pulse");
			patient_measurementDTO.temperature = rs.getDouble("temperature");
			patient_measurementDTO.oxygenSaturation = rs.getDouble("oxygen_saturation");
			patient_measurementDTO.bmi = rs.getDouble("bmi");
			patient_measurementDTO.sugar = rs.getString("blood_sugar");
			patient_measurementDTO.searchColumn = rs.getString("search_column");
			patient_measurementDTO.measurerUserName = rs.getString("measurer_user_name");
			patient_measurementDTO.isDeleted = rs.getInt("isDeleted");
			patient_measurementDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return patient_measurementDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

	public Patient_measurementDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Patient_measurementDTO patient_measurementDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return patient_measurementDTO;
	}

	public Patient_measurementDTO getDTOByAppointmentId (long appointmentId)
	{
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
		
        sql += " WHERE isDeleted = 0 and appointment_id=" + appointmentId + " order by id desc limit 1";
        Patient_measurementDTO patient_measurementDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return patient_measurementDTO;		
	}
	
	public Patient_measurementDTO getLatestDTO (String userName, int who_is_the_patient_cat)
	{
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
		
        sql += " WHERE isDeleted = 0 and appointment_id in ("
        		+ "select id from appointment where employee_user_name = '" + userName
        		+ "' and who_is_the_patient_cat = " + who_is_the_patient_cat
        		+ ")"
        		+ "  order by lastModificationTime desc limit 1";
        Patient_measurementDTO patient_measurementDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return patient_measurementDTO;		
	}

	
	public List<Patient_measurementDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<Patient_measurementDTO> getAllPatient_measurement (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Patient_measurementDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Patient_measurementDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name")
						|| str.equals("date_of_birth_start")
						|| str.equals("date_of_birth_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name"))
					{
						AllFieldSql += "" + tableName + ".name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("date_of_birth_start"))
					{
						AllFieldSql += "" + tableName + ".date_of_birth >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("date_of_birth_end"))
					{
						AllFieldSql += "" + tableName + ".date_of_birth <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	