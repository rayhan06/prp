package patient_measurement;
import java.util.*; 
import util.*; 


public class Patient_measurementDTO extends CommonDTO
{

	public long appointmentId = -1;
    public String name = "";
	public long dateOfBirth = System.currentTimeMillis();
	public long age = -1;
	public double height = -1;
	public double weight = -1;
	public double bloodPressureDiastole = -1;
	public double bloodPressureSystole = -1;
	public double pulse = -1;
	public double temperature = -1;
	public double oxygenSaturation = -1;
	public double bmi = -1;
	public String measurerUserName = "";
	public String sugar = "";
	
	
    @Override
	public String toString() {
            return "$Patient_measurementDTO[" +
            " iD = " + iD +
            " appointmentId = " + appointmentId +
            " name = " + name +
            " dateOfBirth = " + dateOfBirth +
            " age = " + age +
            " height = " + height +
            " weight = " + weight +
            " bloodPressureDiastole = " + bloodPressureDiastole +
            " bloodPressureSystole = " + bloodPressureSystole +
            " pulse = " + pulse +
            " temperature = " + temperature +
            " oxygenSaturation = " + oxygenSaturation +
            " bmi = " + bmi +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}