package patient_measurement;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Patient_measurementRepository implements Repository {
	Patient_measurementDAO patient_measurementDAO = null;
	
	public void setDAO(Patient_measurementDAO patient_measurementDAO)
	{
		this.patient_measurementDAO = patient_measurementDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Patient_measurementRepository.class);
	Map<Long, Patient_measurementDTO>mapOfPatient_measurementDTOToiD;
	Map<Long, Set<Patient_measurementDTO> >mapOfPatient_measurementDTOToappointmentId;
	Map<String, Set<Patient_measurementDTO> >mapOfPatient_measurementDTOToname;
	Map<Long, Set<Patient_measurementDTO> >mapOfPatient_measurementDTOTodateOfBirth;
	Map<Long, Set<Patient_measurementDTO> >mapOfPatient_measurementDTOToage;
	Map<Double, Set<Patient_measurementDTO> >mapOfPatient_measurementDTOToheight;
	Map<Double, Set<Patient_measurementDTO> >mapOfPatient_measurementDTOToweight;
	Map<Double, Set<Patient_measurementDTO> >mapOfPatient_measurementDTOTobloodPressureDiastole;
	Map<Double, Set<Patient_measurementDTO> >mapOfPatient_measurementDTOTobloodPressureSystole;
	Map<Double, Set<Patient_measurementDTO> >mapOfPatient_measurementDTOTopulse;
	Map<Double, Set<Patient_measurementDTO> >mapOfPatient_measurementDTOTotemperature;
	Map<Double, Set<Patient_measurementDTO> >mapOfPatient_measurementDTOTooxygenSaturation;
	Map<Double, Set<Patient_measurementDTO> >mapOfPatient_measurementDTOTobmi;
	Map<String, Set<Patient_measurementDTO> >mapOfPatient_measurementDTOTosearchColumn;
	Map<Long, Set<Patient_measurementDTO> >mapOfPatient_measurementDTOTolastModificationTime;


	static Patient_measurementRepository instance = null;  
	private Patient_measurementRepository(){
		mapOfPatient_measurementDTOToiD = new ConcurrentHashMap<>();
		mapOfPatient_measurementDTOToappointmentId = new ConcurrentHashMap<>();
		mapOfPatient_measurementDTOToname = new ConcurrentHashMap<>();
		mapOfPatient_measurementDTOTodateOfBirth = new ConcurrentHashMap<>();
		mapOfPatient_measurementDTOToage = new ConcurrentHashMap<>();
		mapOfPatient_measurementDTOToheight = new ConcurrentHashMap<>();
		mapOfPatient_measurementDTOToweight = new ConcurrentHashMap<>();
		mapOfPatient_measurementDTOTobloodPressureDiastole = new ConcurrentHashMap<>();
		mapOfPatient_measurementDTOTobloodPressureSystole = new ConcurrentHashMap<>();
		mapOfPatient_measurementDTOTopulse = new ConcurrentHashMap<>();
		mapOfPatient_measurementDTOTotemperature = new ConcurrentHashMap<>();
		mapOfPatient_measurementDTOTooxygenSaturation = new ConcurrentHashMap<>();
		mapOfPatient_measurementDTOTobmi = new ConcurrentHashMap<>();
		mapOfPatient_measurementDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfPatient_measurementDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Patient_measurementRepository getInstance(){
		if (instance == null){
			instance = new Patient_measurementRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(patient_measurementDAO == null)
		{
			return;
		}
		try {
			List<Patient_measurementDTO> patient_measurementDTOs = patient_measurementDAO.getAllPatient_measurement(reloadAll);
			for(Patient_measurementDTO patient_measurementDTO : patient_measurementDTOs) {
				Patient_measurementDTO oldPatient_measurementDTO = getPatient_measurementDTOByID(patient_measurementDTO.iD);
				if( oldPatient_measurementDTO != null ) {
					mapOfPatient_measurementDTOToiD.remove(oldPatient_measurementDTO.iD);
				
					if(mapOfPatient_measurementDTOToappointmentId.containsKey(oldPatient_measurementDTO.appointmentId)) {
						mapOfPatient_measurementDTOToappointmentId.get(oldPatient_measurementDTO.appointmentId).remove(oldPatient_measurementDTO);
					}
					if(mapOfPatient_measurementDTOToappointmentId.get(oldPatient_measurementDTO.appointmentId).isEmpty()) {
						mapOfPatient_measurementDTOToappointmentId.remove(oldPatient_measurementDTO.appointmentId);
					}
					
					if(mapOfPatient_measurementDTOToname.containsKey(oldPatient_measurementDTO.name)) {
						mapOfPatient_measurementDTOToname.get(oldPatient_measurementDTO.name).remove(oldPatient_measurementDTO);
					}
					if(mapOfPatient_measurementDTOToname.get(oldPatient_measurementDTO.name).isEmpty()) {
						mapOfPatient_measurementDTOToname.remove(oldPatient_measurementDTO.name);
					}
					
					if(mapOfPatient_measurementDTOTodateOfBirth.containsKey(oldPatient_measurementDTO.dateOfBirth)) {
						mapOfPatient_measurementDTOTodateOfBirth.get(oldPatient_measurementDTO.dateOfBirth).remove(oldPatient_measurementDTO);
					}
					if(mapOfPatient_measurementDTOTodateOfBirth.get(oldPatient_measurementDTO.dateOfBirth).isEmpty()) {
						mapOfPatient_measurementDTOTodateOfBirth.remove(oldPatient_measurementDTO.dateOfBirth);
					}
					
					if(mapOfPatient_measurementDTOToage.containsKey(oldPatient_measurementDTO.age)) {
						mapOfPatient_measurementDTOToage.get(oldPatient_measurementDTO.age).remove(oldPatient_measurementDTO);
					}
					if(mapOfPatient_measurementDTOToage.get(oldPatient_measurementDTO.age).isEmpty()) {
						mapOfPatient_measurementDTOToage.remove(oldPatient_measurementDTO.age);
					}
					
					if(mapOfPatient_measurementDTOToheight.containsKey(oldPatient_measurementDTO.height)) {
						mapOfPatient_measurementDTOToheight.get(oldPatient_measurementDTO.height).remove(oldPatient_measurementDTO);
					}
					if(mapOfPatient_measurementDTOToheight.get(oldPatient_measurementDTO.height).isEmpty()) {
						mapOfPatient_measurementDTOToheight.remove(oldPatient_measurementDTO.height);
					}
					
					if(mapOfPatient_measurementDTOToweight.containsKey(oldPatient_measurementDTO.weight)) {
						mapOfPatient_measurementDTOToweight.get(oldPatient_measurementDTO.weight).remove(oldPatient_measurementDTO);
					}
					if(mapOfPatient_measurementDTOToweight.get(oldPatient_measurementDTO.weight).isEmpty()) {
						mapOfPatient_measurementDTOToweight.remove(oldPatient_measurementDTO.weight);
					}
					
					if(mapOfPatient_measurementDTOTobloodPressureDiastole.containsKey(oldPatient_measurementDTO.bloodPressureDiastole)) {
						mapOfPatient_measurementDTOTobloodPressureDiastole.get(oldPatient_measurementDTO.bloodPressureDiastole).remove(oldPatient_measurementDTO);
					}
					if(mapOfPatient_measurementDTOTobloodPressureDiastole.get(oldPatient_measurementDTO.bloodPressureDiastole).isEmpty()) {
						mapOfPatient_measurementDTOTobloodPressureDiastole.remove(oldPatient_measurementDTO.bloodPressureDiastole);
					}
					
					if(mapOfPatient_measurementDTOTobloodPressureSystole.containsKey(oldPatient_measurementDTO.bloodPressureSystole)) {
						mapOfPatient_measurementDTOTobloodPressureSystole.get(oldPatient_measurementDTO.bloodPressureSystole).remove(oldPatient_measurementDTO);
					}
					if(mapOfPatient_measurementDTOTobloodPressureSystole.get(oldPatient_measurementDTO.bloodPressureSystole).isEmpty()) {
						mapOfPatient_measurementDTOTobloodPressureSystole.remove(oldPatient_measurementDTO.bloodPressureSystole);
					}
					
					if(mapOfPatient_measurementDTOTopulse.containsKey(oldPatient_measurementDTO.pulse)) {
						mapOfPatient_measurementDTOTopulse.get(oldPatient_measurementDTO.pulse).remove(oldPatient_measurementDTO);
					}
					if(mapOfPatient_measurementDTOTopulse.get(oldPatient_measurementDTO.pulse).isEmpty()) {
						mapOfPatient_measurementDTOTopulse.remove(oldPatient_measurementDTO.pulse);
					}
					
					if(mapOfPatient_measurementDTOTotemperature.containsKey(oldPatient_measurementDTO.temperature)) {
						mapOfPatient_measurementDTOTotemperature.get(oldPatient_measurementDTO.temperature).remove(oldPatient_measurementDTO);
					}
					if(mapOfPatient_measurementDTOTotemperature.get(oldPatient_measurementDTO.temperature).isEmpty()) {
						mapOfPatient_measurementDTOTotemperature.remove(oldPatient_measurementDTO.temperature);
					}
					
					if(mapOfPatient_measurementDTOTooxygenSaturation.containsKey(oldPatient_measurementDTO.oxygenSaturation)) {
						mapOfPatient_measurementDTOTooxygenSaturation.get(oldPatient_measurementDTO.oxygenSaturation).remove(oldPatient_measurementDTO);
					}
					if(mapOfPatient_measurementDTOTooxygenSaturation.get(oldPatient_measurementDTO.oxygenSaturation).isEmpty()) {
						mapOfPatient_measurementDTOTooxygenSaturation.remove(oldPatient_measurementDTO.oxygenSaturation);
					}
					
					if(mapOfPatient_measurementDTOTobmi.containsKey(oldPatient_measurementDTO.bmi)) {
						mapOfPatient_measurementDTOTobmi.get(oldPatient_measurementDTO.bmi).remove(oldPatient_measurementDTO);
					}
					if(mapOfPatient_measurementDTOTobmi.get(oldPatient_measurementDTO.bmi).isEmpty()) {
						mapOfPatient_measurementDTOTobmi.remove(oldPatient_measurementDTO.bmi);
					}
					
					if(mapOfPatient_measurementDTOTosearchColumn.containsKey(oldPatient_measurementDTO.searchColumn)) {
						mapOfPatient_measurementDTOTosearchColumn.get(oldPatient_measurementDTO.searchColumn).remove(oldPatient_measurementDTO);
					}
					if(mapOfPatient_measurementDTOTosearchColumn.get(oldPatient_measurementDTO.searchColumn).isEmpty()) {
						mapOfPatient_measurementDTOTosearchColumn.remove(oldPatient_measurementDTO.searchColumn);
					}
					
					if(mapOfPatient_measurementDTOTolastModificationTime.containsKey(oldPatient_measurementDTO.lastModificationTime)) {
						mapOfPatient_measurementDTOTolastModificationTime.get(oldPatient_measurementDTO.lastModificationTime).remove(oldPatient_measurementDTO);
					}
					if(mapOfPatient_measurementDTOTolastModificationTime.get(oldPatient_measurementDTO.lastModificationTime).isEmpty()) {
						mapOfPatient_measurementDTOTolastModificationTime.remove(oldPatient_measurementDTO.lastModificationTime);
					}
					
					
				}
				if(patient_measurementDTO.isDeleted == 0) 
				{
					
					mapOfPatient_measurementDTOToiD.put(patient_measurementDTO.iD, patient_measurementDTO);
				
					if( ! mapOfPatient_measurementDTOToappointmentId.containsKey(patient_measurementDTO.appointmentId)) {
						mapOfPatient_measurementDTOToappointmentId.put(patient_measurementDTO.appointmentId, new HashSet<>());
					}
					mapOfPatient_measurementDTOToappointmentId.get(patient_measurementDTO.appointmentId).add(patient_measurementDTO);
					
					if( ! mapOfPatient_measurementDTOToname.containsKey(patient_measurementDTO.name)) {
						mapOfPatient_measurementDTOToname.put(patient_measurementDTO.name, new HashSet<>());
					}
					mapOfPatient_measurementDTOToname.get(patient_measurementDTO.name).add(patient_measurementDTO);
					
					if( ! mapOfPatient_measurementDTOTodateOfBirth.containsKey(patient_measurementDTO.dateOfBirth)) {
						mapOfPatient_measurementDTOTodateOfBirth.put(patient_measurementDTO.dateOfBirth, new HashSet<>());
					}
					mapOfPatient_measurementDTOTodateOfBirth.get(patient_measurementDTO.dateOfBirth).add(patient_measurementDTO);
					
					if( ! mapOfPatient_measurementDTOToage.containsKey(patient_measurementDTO.age)) {
						mapOfPatient_measurementDTOToage.put(patient_measurementDTO.age, new HashSet<>());
					}
					mapOfPatient_measurementDTOToage.get(patient_measurementDTO.age).add(patient_measurementDTO);
					
					if( ! mapOfPatient_measurementDTOToheight.containsKey(patient_measurementDTO.height)) {
						mapOfPatient_measurementDTOToheight.put(patient_measurementDTO.height, new HashSet<>());
					}
					mapOfPatient_measurementDTOToheight.get(patient_measurementDTO.height).add(patient_measurementDTO);
					
					if( ! mapOfPatient_measurementDTOToweight.containsKey(patient_measurementDTO.weight)) {
						mapOfPatient_measurementDTOToweight.put(patient_measurementDTO.weight, new HashSet<>());
					}
					mapOfPatient_measurementDTOToweight.get(patient_measurementDTO.weight).add(patient_measurementDTO);
					
					if( ! mapOfPatient_measurementDTOTobloodPressureDiastole.containsKey(patient_measurementDTO.bloodPressureDiastole)) {
						mapOfPatient_measurementDTOTobloodPressureDiastole.put(patient_measurementDTO.bloodPressureDiastole, new HashSet<>());
					}
					mapOfPatient_measurementDTOTobloodPressureDiastole.get(patient_measurementDTO.bloodPressureDiastole).add(patient_measurementDTO);
					
					if( ! mapOfPatient_measurementDTOTobloodPressureSystole.containsKey(patient_measurementDTO.bloodPressureSystole)) {
						mapOfPatient_measurementDTOTobloodPressureSystole.put(patient_measurementDTO.bloodPressureSystole, new HashSet<>());
					}
					mapOfPatient_measurementDTOTobloodPressureSystole.get(patient_measurementDTO.bloodPressureSystole).add(patient_measurementDTO);
					
					if( ! mapOfPatient_measurementDTOTopulse.containsKey(patient_measurementDTO.pulse)) {
						mapOfPatient_measurementDTOTopulse.put(patient_measurementDTO.pulse, new HashSet<>());
					}
					mapOfPatient_measurementDTOTopulse.get(patient_measurementDTO.pulse).add(patient_measurementDTO);
					
					if( ! mapOfPatient_measurementDTOTotemperature.containsKey(patient_measurementDTO.temperature)) {
						mapOfPatient_measurementDTOTotemperature.put(patient_measurementDTO.temperature, new HashSet<>());
					}
					mapOfPatient_measurementDTOTotemperature.get(patient_measurementDTO.temperature).add(patient_measurementDTO);
					
					if( ! mapOfPatient_measurementDTOTooxygenSaturation.containsKey(patient_measurementDTO.oxygenSaturation)) {
						mapOfPatient_measurementDTOTooxygenSaturation.put(patient_measurementDTO.oxygenSaturation, new HashSet<>());
					}
					mapOfPatient_measurementDTOTooxygenSaturation.get(patient_measurementDTO.oxygenSaturation).add(patient_measurementDTO);
					
					if( ! mapOfPatient_measurementDTOTobmi.containsKey(patient_measurementDTO.bmi)) {
						mapOfPatient_measurementDTOTobmi.put(patient_measurementDTO.bmi, new HashSet<>());
					}
					mapOfPatient_measurementDTOTobmi.get(patient_measurementDTO.bmi).add(patient_measurementDTO);
					
					if( ! mapOfPatient_measurementDTOTosearchColumn.containsKey(patient_measurementDTO.searchColumn)) {
						mapOfPatient_measurementDTOTosearchColumn.put(patient_measurementDTO.searchColumn, new HashSet<>());
					}
					mapOfPatient_measurementDTOTosearchColumn.get(patient_measurementDTO.searchColumn).add(patient_measurementDTO);
					
					if( ! mapOfPatient_measurementDTOTolastModificationTime.containsKey(patient_measurementDTO.lastModificationTime)) {
						mapOfPatient_measurementDTOTolastModificationTime.put(patient_measurementDTO.lastModificationTime, new HashSet<>());
					}
					mapOfPatient_measurementDTOTolastModificationTime.get(patient_measurementDTO.lastModificationTime).add(patient_measurementDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Patient_measurementDTO> getPatient_measurementList() {
		List <Patient_measurementDTO> patient_measurements = new ArrayList<Patient_measurementDTO>(this.mapOfPatient_measurementDTOToiD.values());
		return patient_measurements;
	}
	
	
	public Patient_measurementDTO getPatient_measurementDTOByID( long ID){
		return mapOfPatient_measurementDTOToiD.get(ID);
	}
	
	
	public List<Patient_measurementDTO> getPatient_measurementDTOByappointment_id(long appointment_id) {
		return new ArrayList<>( mapOfPatient_measurementDTOToappointmentId.getOrDefault(appointment_id,new HashSet<>()));
	}
	
	
	public List<Patient_measurementDTO> getPatient_measurementDTOByname(String name) {
		return new ArrayList<>( mapOfPatient_measurementDTOToname.getOrDefault(name,new HashSet<>()));
	}
	
	
	public List<Patient_measurementDTO> getPatient_measurementDTOBydate_of_birth(long date_of_birth) {
		return new ArrayList<>( mapOfPatient_measurementDTOTodateOfBirth.getOrDefault(date_of_birth,new HashSet<>()));
	}
	
	
	public List<Patient_measurementDTO> getPatient_measurementDTOByage(long age) {
		return new ArrayList<>( mapOfPatient_measurementDTOToage.getOrDefault(age,new HashSet<>()));
	}
	
	
	public List<Patient_measurementDTO> getPatient_measurementDTOByheight(double height) {
		return new ArrayList<>( mapOfPatient_measurementDTOToheight.getOrDefault(height,new HashSet<>()));
	}
	
	
	public List<Patient_measurementDTO> getPatient_measurementDTOByweight(double weight) {
		return new ArrayList<>( mapOfPatient_measurementDTOToweight.getOrDefault(weight,new HashSet<>()));
	}
	
	
	public List<Patient_measurementDTO> getPatient_measurementDTOByblood_pressure_diastole(double blood_pressure_diastole) {
		return new ArrayList<>( mapOfPatient_measurementDTOTobloodPressureDiastole.getOrDefault(blood_pressure_diastole,new HashSet<>()));
	}
	
	
	public List<Patient_measurementDTO> getPatient_measurementDTOByblood_pressure_systole(double blood_pressure_systole) {
		return new ArrayList<>( mapOfPatient_measurementDTOTobloodPressureSystole.getOrDefault(blood_pressure_systole,new HashSet<>()));
	}
	
	
	public List<Patient_measurementDTO> getPatient_measurementDTOBypulse(double pulse) {
		return new ArrayList<>( mapOfPatient_measurementDTOTopulse.getOrDefault(pulse,new HashSet<>()));
	}
	
	
	public List<Patient_measurementDTO> getPatient_measurementDTOBytemperature(double temperature) {
		return new ArrayList<>( mapOfPatient_measurementDTOTotemperature.getOrDefault(temperature,new HashSet<>()));
	}
	
	
	public List<Patient_measurementDTO> getPatient_measurementDTOByoxygen_saturation(double oxygen_saturation) {
		return new ArrayList<>( mapOfPatient_measurementDTOTooxygenSaturation.getOrDefault(oxygen_saturation,new HashSet<>()));
	}
	
	
	public List<Patient_measurementDTO> getPatient_measurementDTOBybmi(double bmi) {
		return new ArrayList<>( mapOfPatient_measurementDTOTobmi.getOrDefault(bmi,new HashSet<>()));
	}
	
	
	public List<Patient_measurementDTO> getPatient_measurementDTOBysearch_column(String search_column) {
		return new ArrayList<>( mapOfPatient_measurementDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
	}
	
	
	public List<Patient_measurementDTO> getPatient_measurementDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfPatient_measurementDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "patient_measurement";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


