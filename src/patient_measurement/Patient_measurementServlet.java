package patient_measurement;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;
import util.TimeConverter;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import language.LC;

import java.util.StringTokenizer;

import com.google.gson.Gson;

import appointment.AppointmentDAO;
import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Patient_measurementServlet
 */
@WebServlet("/Patient_measurementServlet")
@MultipartConfig
public class Patient_measurementServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Patient_measurementServlet.class);

    String tableName = "patient_measurement";

	Patient_measurementDAO patient_measurementDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Patient_measurementServlet() 
	{
        super();
    	try
    	{
			patient_measurementDAO = new Patient_measurementDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(patient_measurementDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PATIENT_MEASUREMENT_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PATIENT_MEASUREMENT_UPDATE))
				{
					getPatient_measurement(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PATIENT_MEASUREMENT_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							if(filter.equalsIgnoreCase("todaysMeasurements"))
							{
								filter = " measurer_user_name = '" + userDTO.userName + "' and lastModificationTime >= " + TimeConverter.getToday();
							}
							else
							{
								filter = ""; //shouldn't be directly used, rather manipulate it.
							}
							
							searchPatient_measurement(request, response, isPermanentTable, filter);
						}
						else
						{
							searchPatient_measurement(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchPatient_measurement(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("getFormattedAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PATIENT_MEASUREMENT_SEARCH))
				{
					String userName = Utils.getDigitEnglishFromBangla(request.getParameter("userName"));
					if(!Utils.isValidUserName(userName))
					{
						return;
					}
					int whoIsThePatientCat = Integer.parseInt(request.getParameter("whoIsThePatientCat"));
					AppointmentDAO appointmentDAO = new AppointmentDAO();
					long appointmentId = appointmentDAO.getMostRecentAppointmentIdTillToday(userName, whoIsThePatientCat);
					
					if(appointmentId == -1)
					{
						response.sendRedirect("Entry_pageServlet?actionType=getAddPage&error=" + LC.HM_YOU_MUST_CREATE_AN_APPOINTMENT_FIRST);
					}
					else
					{
						Patient_measurementDTO patient_measurementDTO = patient_measurementDAO.getLatestDTO(userName, whoIsThePatientCat);
						if(patient_measurementDTO != null)
						{
							response.sendRedirect("Patient_measurementServlet?actionType=getEditPage"
									+ "&ID=" + patient_measurementDTO.iD);
						}
						else
						{
							response.sendRedirect("Patient_measurementServlet?actionType=getAddPage"
									+ "&appointmentId=" + appointmentId);
						}
					}
					
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PATIENT_MEASUREMENT_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PATIENT_MEASUREMENT_ADD))
				{
					System.out.println("going to  addPatient_measurement ");
					addPatient_measurement(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addPatient_measurement ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PATIENT_MEASUREMENT_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addPatient_measurement ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PATIENT_MEASUREMENT_UPDATE))
				{					
					addPatient_measurement(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PATIENT_MEASUREMENT_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PATIENT_MEASUREMENT_SEARCH))
				{
					searchPatient_measurement(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Patient_measurementDTO patient_measurementDTO = patient_measurementDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(patient_measurementDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addPatient_measurement(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addPatient_measurement");
			String path = getServletContext().getRealPath("/img2/");
			Patient_measurementDTO patient_measurementDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				patient_measurementDTO = new Patient_measurementDTO();
			}
			else
			{
				patient_measurementDTO = patient_measurementDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("appointmentId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("appointmentId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				patient_measurementDTO.appointmentId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("name");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("name = " + Value);
			if(Value != null)
			{
				patient_measurementDTO.name = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("sugar");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("sugar = " + Value);
			if(Value != null)
			{
				patient_measurementDTO.sugar = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			patient_measurementDTO.measurerUserName = userDTO.userName;

			Value = request.getParameter("dateOfBirth");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("dateOfBirth = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				try 
				{
					Date d = f.parse(Value);
					patient_measurementDTO.dateOfBirth = d.getTime();
					
					Date now = new Date();
                    long diffInMillies = Math.abs(now.getTime() - d.getTime());
                    patient_measurementDTO.age =   (diffInMillies 
                                    / (1000l * 60 * 60 * 24 * 365)); 
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			

			Value = request.getParameter("height");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("height = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				patient_measurementDTO.height = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("weight");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("weight = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				patient_measurementDTO.weight = Double.parseDouble(Value);
				if(patient_measurementDTO.height != 0)
				{
					patient_measurementDTO.bmi = patient_measurementDTO.weight * 10000 
							/ (patient_measurementDTO.height * patient_measurementDTO.height);
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("bloodPressureDiastole");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("bloodPressureDiastole = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				patient_measurementDTO.bloodPressureDiastole = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("bloodPressureSystole");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("bloodPressureSystole = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				patient_measurementDTO.bloodPressureSystole = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("pulse");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("pulse = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				patient_measurementDTO.pulse = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("temperature");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("temperature = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				patient_measurementDTO.temperature = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("oxygenSaturation");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("oxygenSaturation = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				patient_measurementDTO.oxygenSaturation = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			
			
			System.out.println("Done adding  addPatient_measurement dto = " + patient_measurementDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				patient_measurementDAO.setIsDeleted(patient_measurementDTO.iD, CommonDTO.OUTDATED);
				returnedID = patient_measurementDAO.add(patient_measurementDTO);
				patient_measurementDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = patient_measurementDAO.manageWriteOperations(patient_measurementDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = patient_measurementDAO.manageWriteOperations(patient_measurementDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getPatient_measurement(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Patient_measurementServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(patient_measurementDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void getPatient_measurement(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getPatient_measurement");
		Patient_measurementDTO patient_measurementDTO = null;
		try 
		{
			patient_measurementDTO = patient_measurementDAO.getDTOByID(id);
			request.setAttribute("ID", patient_measurementDTO.iD);
			request.setAttribute("patient_measurementDTO",patient_measurementDTO);
			request.setAttribute("patient_measurementDAO",patient_measurementDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "patient_measurement/patient_measurementInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "patient_measurement/patient_measurementSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "patient_measurement/patient_measurementEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "patient_measurement/patient_measurementEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getPatient_measurement(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getPatient_measurement(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchPatient_measurement(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchPatient_measurement 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_PATIENT_MEASUREMENT,
			request,
			patient_measurementDAO,
			SessionConstants.VIEW_PATIENT_MEASUREMENT,
			SessionConstants.SEARCH_PATIENT_MEASUREMENT,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("patient_measurementDAO",patient_measurementDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to patient_measurement/patient_measurementApproval.jsp");
	        	rd = request.getRequestDispatcher("patient_measurement/patient_measurementApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to patient_measurement/patient_measurementApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("patient_measurement/patient_measurementApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to patient_measurement/patient_measurementSearch.jsp");
	        	rd = request.getRequestDispatcher("patient_measurement/patient_measurementSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to patient_measurement/patient_measurementSearchForm.jsp");
	        	rd = request.getRequestDispatcher("patient_measurement/patient_measurementSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

