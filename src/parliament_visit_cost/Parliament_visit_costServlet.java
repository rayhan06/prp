package parliament_visit_cost;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Parliament_visit_costServlet
 */
@WebServlet("/Parliament_visit_costServlet")
@MultipartConfig
public class Parliament_visit_costServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Parliament_visit_costServlet.class);

    String tableName = "parliament_visit_cost";

	Parliament_visit_costDAO parliament_visit_costDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Parliament_visit_costServlet()
	{
        super();
    	try
    	{
			parliament_visit_costDAO = new Parliament_visit_costDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(parliament_visit_costDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_VISIT_COST_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_VISIT_COST_UPDATE))
				{
					getParliament_visit_cost(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_VISIT_COST_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchParliament_visit_cost(request, response, isPermanentTable, filter);
						}
						else
						{
							searchParliament_visit_cost(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchParliament_visit_cost(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_VISIT_COST_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_VISIT_COST_ADD))
				{
					System.out.println("going to  addParliament_visit_cost ");
					addParliament_visit_cost(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addParliament_visit_cost ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_VISIT_COST_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addParliament_visit_cost ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_VISIT_COST_UPDATE))
				{
					addParliament_visit_cost(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_VISIT_COST_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_VISIT_COST_SEARCH))
				{
					searchParliament_visit_cost(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Parliament_visit_costDTO parliament_visit_costDTO = parliament_visit_costDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(parliament_visit_costDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addParliament_visit_cost(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addParliament_visit_cost");
			String path = getServletContext().getRealPath("/img2/");
			Parliament_visit_costDTO parliament_visit_costDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				parliament_visit_costDTO = new Parliament_visit_costDTO();
			}
			else
			{
				parliament_visit_costDTO = parliament_visit_costDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("typeOfVisitor1");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("typeOfVisitor1 = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				parliament_visit_costDTO.typeOfVisitor = 1;
			}
			else{
				parliament_visit_costDTO.typeOfVisitor = 0;
			}

			Value = request.getParameter("costPerPerson");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("costPerPerson = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				parliament_visit_costDTO.costPerPerson = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("startDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("startDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					parliament_visit_costDTO.startDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("endDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("endDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					parliament_visit_costDTO.endDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("isActive");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("isActive = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				parliament_visit_costDTO.isActive = 1;
			}
			else {
				parliament_visit_costDTO.isActive = 0;
			}

			if(addFlag)
			{
				parliament_visit_costDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				parliament_visit_costDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				parliament_visit_costDTO.insertionDate = c.getTimeInMillis();
			}


			parliament_visit_costDTO.lastModifierUser = userDTO.userName;


			System.out.println("Done adding  addParliament_visit_cost dto = " + parliament_visit_costDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				parliament_visit_costDAO.setIsDeleted(parliament_visit_costDTO.iD, CommonDTO.OUTDATED);
				returnedID = parliament_visit_costDAO.add(parliament_visit_costDTO);
				parliament_visit_costDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = parliament_visit_costDAO.manageWriteOperations(parliament_visit_costDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = parliament_visit_costDAO.manageWriteOperations(parliament_visit_costDTO, SessionConstants.UPDATE, -1, userDTO);
			}









			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getParliament_visit_cost(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Parliament_visit_costServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(parliament_visit_costDAO.getDTOByID(returnedID), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}









	private void getParliament_visit_cost(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getParliament_visit_cost");
		Parliament_visit_costDTO parliament_visit_costDTO = null;
		try
		{
			parliament_visit_costDTO = parliament_visit_costDAO.getDTOByID(id);
			request.setAttribute("ID", parliament_visit_costDTO.iD);
			request.setAttribute("parliament_visit_costDTO",parliament_visit_costDTO);
			request.setAttribute("parliament_visit_costDAO",parliament_visit_costDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "parliament_visit_cost/parliament_visit_costInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "parliament_visit_cost/parliament_visit_costSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "parliament_visit_cost/parliament_visit_costEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "parliament_visit_cost/parliament_visit_costEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getParliament_visit_cost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getParliament_visit_cost(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchParliament_visit_cost(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchParliament_visit_cost 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_PARLIAMENT_VISIT_COST,
			request,
			parliament_visit_costDAO,
			SessionConstants.VIEW_PARLIAMENT_VISIT_COST,
			SessionConstants.SEARCH_PARLIAMENT_VISIT_COST,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("parliament_visit_costDAO",parliament_visit_costDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to parliament_visit_cost/parliament_visit_costApproval.jsp");
	        	rd = request.getRequestDispatcher("parliament_visit_cost/parliament_visit_costApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to parliament_visit_cost/parliament_visit_costApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("parliament_visit_cost/parliament_visit_costApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to parliament_visit_cost/parliament_visit_costSearch.jsp");
	        	rd = request.getRequestDispatcher("parliament_visit_cost/parliament_visit_costSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to parliament_visit_cost/parliament_visit_costSearchForm.jsp");
	        	rd = request.getRequestDispatcher("parliament_visit_cost/parliament_visit_costSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

