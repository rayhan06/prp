package parliament_visit_cost;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Parliament_visit_costDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Parliament_visit_costDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Parliament_visit_costMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"type_of_visitor",
			"cost_per_person",
			"start_date",
			"end_date",
			"is_active",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Parliament_visit_costDAO()
	{
		this("parliament_visit_cost");		
	}
	
	public void setSearchColumn(Parliament_visit_costDTO parliament_visit_costDTO)
	{
		parliament_visit_costDTO.searchColumn = "";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Parliament_visit_costDTO parliament_visit_costDTO = (Parliament_visit_costDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(parliament_visit_costDTO);
		if(isInsert)
		{
			ps.setObject(index++,parliament_visit_costDTO.iD);
		}
		ps.setObject(index++,parliament_visit_costDTO.typeOfVisitor);
		ps.setObject(index++,parliament_visit_costDTO.costPerPerson);
		ps.setObject(index++,parliament_visit_costDTO.startDate);
		ps.setObject(index++,parliament_visit_costDTO.endDate);
		ps.setObject(index++,parliament_visit_costDTO.isActive);
		ps.setObject(index++,parliament_visit_costDTO.insertedByUserId);
		ps.setObject(index++,parliament_visit_costDTO.insertedByOrganogramId);
		ps.setObject(index++,parliament_visit_costDTO.insertionDate);
		ps.setObject(index++,parliament_visit_costDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Parliament_visit_costDTO build(ResultSet rs)
	{
		try
		{
			Parliament_visit_costDTO parliament_visit_costDTO = new Parliament_visit_costDTO();
			parliament_visit_costDTO.iD = rs.getLong("ID");
			parliament_visit_costDTO.typeOfVisitor = rs.getInt("type_of_visitor");
			parliament_visit_costDTO.costPerPerson = rs.getInt("cost_per_person");
			parliament_visit_costDTO.startDate = rs.getLong("start_date");
			parliament_visit_costDTO.endDate = rs.getLong("end_date");
			parliament_visit_costDTO.isActive = rs.getInt("is_active");
			parliament_visit_costDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			parliament_visit_costDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			parliament_visit_costDTO.insertionDate = rs.getLong("insertion_date");
			parliament_visit_costDTO.lastModifierUser = rs.getString("last_modifier_user");
			parliament_visit_costDTO.isDeleted = rs.getInt("isDeleted");
			parliament_visit_costDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return parliament_visit_costDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

	public Parliament_visit_costDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Parliament_visit_costDTO parliament_visit_costDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return parliament_visit_costDTO;
	}

	
	public List<Parliament_visit_costDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<Parliament_visit_costDTO> getAllParliament_visit_cost (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Parliament_visit_costDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Parliament_visit_costDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("start_date_start")
						|| str.equals("start_date_end")
						|| str.equals("end_date_start")
						|| str.equals("end_date_end")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("start_date_start"))
					{
						AllFieldSql += "" + tableName + ".start_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("start_date_end"))
					{
						AllFieldSql += "" + tableName + ".start_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("end_date_start"))
					{
						AllFieldSql += "" + tableName + ".end_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("end_date_end"))
					{
						AllFieldSql += "" + tableName + ".end_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	