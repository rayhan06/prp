package parliament_visit_cost;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Parliament_visit_costRepository implements Repository {
	Parliament_visit_costDAO parliament_visit_costDAO = new Parliament_visit_costDAO();
	
	public void setDAO(Parliament_visit_costDAO parliament_visit_costDAO)
	{
		this.parliament_visit_costDAO = parliament_visit_costDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Parliament_visit_costRepository.class);
	Map<Long, Parliament_visit_costDTO>mapOfParliament_visit_costDTOToiD;
	Map<Integer, Set<Parliament_visit_costDTO> >mapOfParliament_visit_costDTOTotypeOfVisitor;
	Map<Integer, Set<Parliament_visit_costDTO> >mapOfParliament_visit_costDTOTocostPerPerson;
	Map<Long, Set<Parliament_visit_costDTO> >mapOfParliament_visit_costDTOTostartDate;
	Map<Long, Set<Parliament_visit_costDTO> >mapOfParliament_visit_costDTOToendDate;
	Map<Integer, Set<Parliament_visit_costDTO> >mapOfParliament_visit_costDTOToisActive;
	Map<Long, Set<Parliament_visit_costDTO> >mapOfParliament_visit_costDTOToinsertedByUserId;
	Map<Long, Set<Parliament_visit_costDTO> >mapOfParliament_visit_costDTOToinsertedByOrganogramId;
	Map<Long, Set<Parliament_visit_costDTO> >mapOfParliament_visit_costDTOToinsertionDate;
	Map<String, Set<Parliament_visit_costDTO> >mapOfParliament_visit_costDTOTolastModifierUser;
	Map<Long, Set<Parliament_visit_costDTO> >mapOfParliament_visit_costDTOTolastModificationTime;


	static Parliament_visit_costRepository instance = null;  
	private Parliament_visit_costRepository(){
		mapOfParliament_visit_costDTOToiD = new ConcurrentHashMap<>();
		mapOfParliament_visit_costDTOTotypeOfVisitor = new ConcurrentHashMap<>();
		mapOfParliament_visit_costDTOTocostPerPerson = new ConcurrentHashMap<>();
		mapOfParliament_visit_costDTOTostartDate = new ConcurrentHashMap<>();
		mapOfParliament_visit_costDTOToendDate = new ConcurrentHashMap<>();
		mapOfParliament_visit_costDTOToisActive = new ConcurrentHashMap<>();
		mapOfParliament_visit_costDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfParliament_visit_costDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfParliament_visit_costDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfParliament_visit_costDTOTolastModifierUser = new ConcurrentHashMap<>();
		mapOfParliament_visit_costDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Parliament_visit_costRepository getInstance(){
		if (instance == null){
			instance = new Parliament_visit_costRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(parliament_visit_costDAO == null)
		{
			return;
		}
		try {
			List<Parliament_visit_costDTO> parliament_visit_costDTOs = parliament_visit_costDAO.getAllParliament_visit_cost(reloadAll);
			for(Parliament_visit_costDTO parliament_visit_costDTO : parliament_visit_costDTOs) {
				Parliament_visit_costDTO oldParliament_visit_costDTO = getParliament_visit_costDTOByID(parliament_visit_costDTO.iD);
				if( oldParliament_visit_costDTO != null ) {
					mapOfParliament_visit_costDTOToiD.remove(oldParliament_visit_costDTO.iD);
				
					if(mapOfParliament_visit_costDTOTotypeOfVisitor.containsKey(oldParliament_visit_costDTO.typeOfVisitor)) {
						mapOfParliament_visit_costDTOTotypeOfVisitor.get(oldParliament_visit_costDTO.typeOfVisitor).remove(oldParliament_visit_costDTO);
					}
					if(mapOfParliament_visit_costDTOTotypeOfVisitor.get(oldParliament_visit_costDTO.typeOfVisitor).isEmpty()) {
						mapOfParliament_visit_costDTOTotypeOfVisitor.remove(oldParliament_visit_costDTO.typeOfVisitor);
					}
					
					if(mapOfParliament_visit_costDTOTocostPerPerson.containsKey(oldParliament_visit_costDTO.costPerPerson)) {
						mapOfParliament_visit_costDTOTocostPerPerson.get(oldParliament_visit_costDTO.costPerPerson).remove(oldParliament_visit_costDTO);
					}
					if(mapOfParliament_visit_costDTOTocostPerPerson.get(oldParliament_visit_costDTO.costPerPerson).isEmpty()) {
						mapOfParliament_visit_costDTOTocostPerPerson.remove(oldParliament_visit_costDTO.costPerPerson);
					}
					
					if(mapOfParliament_visit_costDTOTostartDate.containsKey(oldParliament_visit_costDTO.startDate)) {
						mapOfParliament_visit_costDTOTostartDate.get(oldParliament_visit_costDTO.startDate).remove(oldParliament_visit_costDTO);
					}
					if(mapOfParliament_visit_costDTOTostartDate.get(oldParliament_visit_costDTO.startDate).isEmpty()) {
						mapOfParliament_visit_costDTOTostartDate.remove(oldParliament_visit_costDTO.startDate);
					}
					
					if(mapOfParliament_visit_costDTOToendDate.containsKey(oldParliament_visit_costDTO.endDate)) {
						mapOfParliament_visit_costDTOToendDate.get(oldParliament_visit_costDTO.endDate).remove(oldParliament_visit_costDTO);
					}
					if(mapOfParliament_visit_costDTOToendDate.get(oldParliament_visit_costDTO.endDate).isEmpty()) {
						mapOfParliament_visit_costDTOToendDate.remove(oldParliament_visit_costDTO.endDate);
					}
					
					if(mapOfParliament_visit_costDTOToisActive.containsKey(oldParliament_visit_costDTO.isActive)) {
						mapOfParliament_visit_costDTOToisActive.get(oldParliament_visit_costDTO.isActive).remove(oldParliament_visit_costDTO);
					}
					if(mapOfParliament_visit_costDTOToisActive.get(oldParliament_visit_costDTO.isActive).isEmpty()) {
						mapOfParliament_visit_costDTOToisActive.remove(oldParliament_visit_costDTO.isActive);
					}
					
					if(mapOfParliament_visit_costDTOToinsertedByUserId.containsKey(oldParliament_visit_costDTO.insertedByUserId)) {
						mapOfParliament_visit_costDTOToinsertedByUserId.get(oldParliament_visit_costDTO.insertedByUserId).remove(oldParliament_visit_costDTO);
					}
					if(mapOfParliament_visit_costDTOToinsertedByUserId.get(oldParliament_visit_costDTO.insertedByUserId).isEmpty()) {
						mapOfParliament_visit_costDTOToinsertedByUserId.remove(oldParliament_visit_costDTO.insertedByUserId);
					}
					
					if(mapOfParliament_visit_costDTOToinsertedByOrganogramId.containsKey(oldParliament_visit_costDTO.insertedByOrganogramId)) {
						mapOfParliament_visit_costDTOToinsertedByOrganogramId.get(oldParliament_visit_costDTO.insertedByOrganogramId).remove(oldParliament_visit_costDTO);
					}
					if(mapOfParliament_visit_costDTOToinsertedByOrganogramId.get(oldParliament_visit_costDTO.insertedByOrganogramId).isEmpty()) {
						mapOfParliament_visit_costDTOToinsertedByOrganogramId.remove(oldParliament_visit_costDTO.insertedByOrganogramId);
					}
					
					if(mapOfParliament_visit_costDTOToinsertionDate.containsKey(oldParliament_visit_costDTO.insertionDate)) {
						mapOfParliament_visit_costDTOToinsertionDate.get(oldParliament_visit_costDTO.insertionDate).remove(oldParliament_visit_costDTO);
					}
					if(mapOfParliament_visit_costDTOToinsertionDate.get(oldParliament_visit_costDTO.insertionDate).isEmpty()) {
						mapOfParliament_visit_costDTOToinsertionDate.remove(oldParliament_visit_costDTO.insertionDate);
					}
					
					if(mapOfParliament_visit_costDTOTolastModifierUser.containsKey(oldParliament_visit_costDTO.lastModifierUser)) {
						mapOfParliament_visit_costDTOTolastModifierUser.get(oldParliament_visit_costDTO.lastModifierUser).remove(oldParliament_visit_costDTO);
					}
					if(mapOfParliament_visit_costDTOTolastModifierUser.get(oldParliament_visit_costDTO.lastModifierUser).isEmpty()) {
						mapOfParliament_visit_costDTOTolastModifierUser.remove(oldParliament_visit_costDTO.lastModifierUser);
					}
					
					if(mapOfParliament_visit_costDTOTolastModificationTime.containsKey(oldParliament_visit_costDTO.lastModificationTime)) {
						mapOfParliament_visit_costDTOTolastModificationTime.get(oldParliament_visit_costDTO.lastModificationTime).remove(oldParliament_visit_costDTO);
					}
					if(mapOfParliament_visit_costDTOTolastModificationTime.get(oldParliament_visit_costDTO.lastModificationTime).isEmpty()) {
						mapOfParliament_visit_costDTOTolastModificationTime.remove(oldParliament_visit_costDTO.lastModificationTime);
					}
					
					
				}
				if(parliament_visit_costDTO.isDeleted == 0) 
				{
					
					mapOfParliament_visit_costDTOToiD.put(parliament_visit_costDTO.iD, parliament_visit_costDTO);
				
					if( ! mapOfParliament_visit_costDTOTotypeOfVisitor.containsKey(parliament_visit_costDTO.typeOfVisitor)) {
						mapOfParliament_visit_costDTOTotypeOfVisitor.put(parliament_visit_costDTO.typeOfVisitor, new HashSet<>());
					}
					mapOfParliament_visit_costDTOTotypeOfVisitor.get(parliament_visit_costDTO.typeOfVisitor).add(parliament_visit_costDTO);
					
					if( ! mapOfParliament_visit_costDTOTocostPerPerson.containsKey(parliament_visit_costDTO.costPerPerson)) {
						mapOfParliament_visit_costDTOTocostPerPerson.put(parliament_visit_costDTO.costPerPerson, new HashSet<>());
					}
					mapOfParliament_visit_costDTOTocostPerPerson.get(parliament_visit_costDTO.costPerPerson).add(parliament_visit_costDTO);
					
					if( ! mapOfParliament_visit_costDTOTostartDate.containsKey(parliament_visit_costDTO.startDate)) {
						mapOfParliament_visit_costDTOTostartDate.put(parliament_visit_costDTO.startDate, new HashSet<>());
					}
					mapOfParliament_visit_costDTOTostartDate.get(parliament_visit_costDTO.startDate).add(parliament_visit_costDTO);
					
					if( ! mapOfParliament_visit_costDTOToendDate.containsKey(parliament_visit_costDTO.endDate)) {
						mapOfParliament_visit_costDTOToendDate.put(parliament_visit_costDTO.endDate, new HashSet<>());
					}
					mapOfParliament_visit_costDTOToendDate.get(parliament_visit_costDTO.endDate).add(parliament_visit_costDTO);
					
					if( ! mapOfParliament_visit_costDTOToisActive.containsKey(parliament_visit_costDTO.isActive)) {
						mapOfParliament_visit_costDTOToisActive.put(parliament_visit_costDTO.isActive, new HashSet<>());
					}
					mapOfParliament_visit_costDTOToisActive.get(parliament_visit_costDTO.isActive).add(parliament_visit_costDTO);
					
					if( ! mapOfParliament_visit_costDTOToinsertedByUserId.containsKey(parliament_visit_costDTO.insertedByUserId)) {
						mapOfParliament_visit_costDTOToinsertedByUserId.put(parliament_visit_costDTO.insertedByUserId, new HashSet<>());
					}
					mapOfParliament_visit_costDTOToinsertedByUserId.get(parliament_visit_costDTO.insertedByUserId).add(parliament_visit_costDTO);
					
					if( ! mapOfParliament_visit_costDTOToinsertedByOrganogramId.containsKey(parliament_visit_costDTO.insertedByOrganogramId)) {
						mapOfParliament_visit_costDTOToinsertedByOrganogramId.put(parliament_visit_costDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfParliament_visit_costDTOToinsertedByOrganogramId.get(parliament_visit_costDTO.insertedByOrganogramId).add(parliament_visit_costDTO);
					
					if( ! mapOfParliament_visit_costDTOToinsertionDate.containsKey(parliament_visit_costDTO.insertionDate)) {
						mapOfParliament_visit_costDTOToinsertionDate.put(parliament_visit_costDTO.insertionDate, new HashSet<>());
					}
					mapOfParliament_visit_costDTOToinsertionDate.get(parliament_visit_costDTO.insertionDate).add(parliament_visit_costDTO);
					
					if( ! mapOfParliament_visit_costDTOTolastModifierUser.containsKey(parliament_visit_costDTO.lastModifierUser)) {
						mapOfParliament_visit_costDTOTolastModifierUser.put(parliament_visit_costDTO.lastModifierUser, new HashSet<>());
					}
					mapOfParliament_visit_costDTOTolastModifierUser.get(parliament_visit_costDTO.lastModifierUser).add(parliament_visit_costDTO);
					
					if( ! mapOfParliament_visit_costDTOTolastModificationTime.containsKey(parliament_visit_costDTO.lastModificationTime)) {
						mapOfParliament_visit_costDTOTolastModificationTime.put(parliament_visit_costDTO.lastModificationTime, new HashSet<>());
					}
					mapOfParliament_visit_costDTOTolastModificationTime.get(parliament_visit_costDTO.lastModificationTime).add(parliament_visit_costDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Parliament_visit_costDTO> getParliament_visit_costList() {
		List <Parliament_visit_costDTO> parliament_visit_costs = new ArrayList<Parliament_visit_costDTO>(this.mapOfParliament_visit_costDTOToiD.values());
		return parliament_visit_costs;
	}
	
	
	public Parliament_visit_costDTO getParliament_visit_costDTOByID( long ID){
		return mapOfParliament_visit_costDTOToiD.get(ID);
	}
	
	
	public List<Parliament_visit_costDTO> getParliament_visit_costDTOBytype_of_visitor(int type_of_visitor) {
		return new ArrayList<>( mapOfParliament_visit_costDTOTotypeOfVisitor.getOrDefault(type_of_visitor,new HashSet<>()));
	}
	
	
	public List<Parliament_visit_costDTO> getParliament_visit_costDTOBycost_per_person(int cost_per_person) {
		return new ArrayList<>( mapOfParliament_visit_costDTOTocostPerPerson.getOrDefault(cost_per_person,new HashSet<>()));
	}
	
	
	public List<Parliament_visit_costDTO> getParliament_visit_costDTOBystart_date(long start_date) {
		return new ArrayList<>( mapOfParliament_visit_costDTOTostartDate.getOrDefault(start_date,new HashSet<>()));
	}
	
	
	public List<Parliament_visit_costDTO> getParliament_visit_costDTOByend_date(long end_date) {
		return new ArrayList<>( mapOfParliament_visit_costDTOToendDate.getOrDefault(end_date,new HashSet<>()));
	}
	
	
	public List<Parliament_visit_costDTO> getParliament_visit_costDTOByis_active(int is_active) {
		return new ArrayList<>( mapOfParliament_visit_costDTOToisActive.getOrDefault(is_active,new HashSet<>()));
	}
	
	
	public List<Parliament_visit_costDTO> getParliament_visit_costDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfParliament_visit_costDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Parliament_visit_costDTO> getParliament_visit_costDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfParliament_visit_costDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Parliament_visit_costDTO> getParliament_visit_costDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfParliament_visit_costDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Parliament_visit_costDTO> getParliament_visit_costDTOBylast_modifier_user(String last_modifier_user) {
		return new ArrayList<>( mapOfParliament_visit_costDTOTolastModifierUser.getOrDefault(last_modifier_user,new HashSet<>()));
	}
	
	
	public List<Parliament_visit_costDTO> getParliament_visit_costDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfParliament_visit_costDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "parliament_visit_cost";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


