package parliament_visit_cost;
import java.util.*; 
import util.*;


public class Parliament_visit_costMAPS extends CommonMaps
{	
	public Parliament_visit_costMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("typeOfVisitor".toLowerCase(), "typeOfVisitor".toLowerCase());
		java_DTO_map.put("costPerPerson".toLowerCase(), "costPerPerson".toLowerCase());
		java_DTO_map.put("startDate".toLowerCase(), "startDate".toLowerCase());
		java_DTO_map.put("endDate".toLowerCase(), "endDate".toLowerCase());
		java_DTO_map.put("isActive".toLowerCase(), "isActive".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("lastModifierUser".toLowerCase(), "lastModifierUser".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("type_of_visitor".toLowerCase(), "typeOfVisitor".toLowerCase());
		java_SQL_map.put("start_date".toLowerCase(), "startDate".toLowerCase());
		java_SQL_map.put("end_date".toLowerCase(), "endDate".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Type Of Visitor".toLowerCase(), "typeOfVisitor".toLowerCase());
		java_Text_map.put("Cost Per Person".toLowerCase(), "costPerPerson".toLowerCase());
		java_Text_map.put("Start Date".toLowerCase(), "startDate".toLowerCase());
		java_Text_map.put("End Date".toLowerCase(), "endDate".toLowerCase());
		java_Text_map.put("Is Active".toLowerCase(), "isActive".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Last Modifier User".toLowerCase(), "lastModifierUser".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}