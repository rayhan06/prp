package parliament_visit_cost;
import java.util.*; 
import util.*; 


public class Parliament_visit_costDTO extends CommonDTO
{

	public int typeOfVisitor = -1;
	public int costPerPerson = -1;
	public long startDate = System.currentTimeMillis();
	public long endDate = System.currentTimeMillis();
	public int isActive = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	
	
    @Override
	public String toString() {
            return "$Parliament_visit_costDTO[" +
            " iD = " + iD +
            " typeOfVisitor = " + typeOfVisitor +
            " costPerPerson = " + costPerPerson +
            " startDate = " + startDate +
            " endDate = " + endDate +
            " isActive = " + isActive +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}