package pi_purchase;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import fiscal_year.Fiscal_yearRepository;
import org.apache.log4j.Logger;
import pb.Utils;
import pi_app_request.Pi_app_requestDAO;
import pi_app_request_details.Pi_app_request_detailsDTO;
import pi_package_vendor_items.Pi_package_vendor_itemsDAO;
import pi_package_vendor_items.Pi_package_vendor_itemsDTO;
import pi_unique_item.PiActionTypeEnum;
import pi_unique_item.PiStageEnum;
import pi_unique_item.PiUniqueItemAssignmentDAO;
import procurement_goods.Procurement_goodsDAO;
import procurement_goods.Procurement_goodsDTO;
import procurement_goods.Procurement_goodsRepository;
import procurement_package.ProcurementGoodsTypeDTO;
import procurement_package.ProcurementGoodsTypeRepository;
import procurement_package.Procurement_packageDTO;
import procurement_package.Procurement_packageRepository;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Pi_purchaseDAO  implements CommonDAOService<Pi_purchaseDTO> {

	Logger logger = Logger.getLogger(getClass());

	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private final String getDTOsForStockView = "SELECT * FROM pi_purchase WHERE isDeleted = 0 AND office_unit_id=%d AND fiscal_year_id=%d AND procurement_goods_id=%d AND purchase_date>=%d ORDER BY purchase_date ASC";
	private final String getDTOByStartAndEnd = "SELECT * FROM pi_purchase WHERE isDeleted = 0 AND office_unit_id= ? AND purchase_date >= ? AND purchase_date < ? ORDER BY purchase_date ASC";
	private final String getPurchaseDetailsByItemId = "SELECT * FROM pi_purchase WHERE isDeleted = 0 AND office_unit_id= ?" +
			" AND procurement_goods_id = ? ORDER BY purchase_date ASC";

	private final String getPurchaseDetailsByOnlyItemId = "SELECT * FROM pi_purchase WHERE isDeleted = 0 AND  procurement_goods_id = ? ORDER BY purchase_date ASC";
	private final String getPurchaseCountByFiscalAndUnitAndItem = "SELECT sum(amount) FROM pi_purchase WHERE isDeleted = 0 AND office_unit_id= %d AND procurement_goods_id = %d And fiscal_year_id = %d ";

	public Pi_purchaseDAO() {
		columnNames = new String[]
				{
						"ID",
						"office_unit_id",
						"fiscal_year_type",
						"pi_purchase_parent_id",
						"pi_package_final_id",
						"pi_lot_final_id",
						"pi_vendor_auctioneer_details_type",
						"sarok_number",
						"purchase_order_number",
						"purchase_date",
						"remarks",
						"files_dropzone",
						"total_bill",
						"procurement_package_id",
						"procurement_goods_type_id",
						"procurement_goods_id",
						"amount",
						"unit_price",
						"total_value",
						"allocated_app",
						"expiry_date",
						"purchase_item_remarks",
						"inserted_by",
						"insertion_time",
						"modified_by",
						"assigned_office_unit_ids",
						"isDeleted",
						"lastModificationTime"
				};
		updateQuery = getUpdateQuery(columnNames);
		addQuery = getAddQuery(columnNames);

		searchMap.put("fiscal_year_id", " and (fiscal_year_id = ?)");
		searchMap.put("pi_purchase_parent_id", " and (pi_purchase_parent_id = ?)");
		searchMap.put("package_id", " and (package_id = ?)");
		searchMap.put("sarok_number", " and (sarok_number like ?)");
		searchMap.put("purchase_date_start", " and (purchase_date >= ?)");
		searchMap.put("purchase_date_end", " and (purchase_date <= ?)");
		searchMap.put("item_type_id", " and (item_type_id = ?)");
		searchMap.put("procurement_goods_id", " and (procurement_goods_id = ?)");
		searchMap.put("expiry_date_start", " and (expiry_date >= ?)");
		searchMap.put("expiry_date_end", " and (expiry_date <= ?)");
		searchMap.put("AnyField", " and (search_column like ?)");
	}

	private final Map<String, String> searchMap = new HashMap<>();

	private static class LazyLoader {
		static final Pi_purchaseDAO INSTANCE = new Pi_purchaseDAO();
	}

	public static Pi_purchaseDAO getInstance() {
		return LazyLoader.INSTANCE;
	}

	public void setSearchColumn(Pi_purchaseDTO pi_purchaseDTO) {
		pi_purchaseDTO.searchColumn = "";
	}

	@Override
	public void set(PreparedStatement ps, Pi_purchaseDTO pi_purchaseDTO, boolean isInsert) throws SQLException {
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();
		setSearchColumn(pi_purchaseDTO);
		if (isInsert) {
			ps.setObject(++index, pi_purchaseDTO.iD);
		}
		ps.setObject(++index, pi_purchaseDTO.officeUnitId);
		ps.setObject(++index, pi_purchaseDTO.fiscalYearId);
		ps.setObject(++index, pi_purchaseDTO.parentId);
		ps.setObject(++index, pi_purchaseDTO.packageId);
		ps.setObject(++index, pi_purchaseDTO.lotId);
		ps.setObject(++index, pi_purchaseDTO.vendorId);
		ps.setObject(++index, pi_purchaseDTO.sarokNumber);
		ps.setObject(++index, pi_purchaseDTO.purchaseOrderNumber);
		ps.setObject(++index, pi_purchaseDTO.purchaseDate);
		ps.setObject(++index, pi_purchaseDTO.remarks);
		ps.setObject(++index, pi_purchaseDTO.filesDropzone);
		ps.setObject(++index, pi_purchaseDTO.totalBill);
		ps.setObject(++index, pi_purchaseDTO.itemGroupId);
		ps.setObject(++index, pi_purchaseDTO.itemTypeId);
		ps.setObject(++index, pi_purchaseDTO.itemId);
		ps.setObject(++index, pi_purchaseDTO.amount);
		ps.setObject(++index, pi_purchaseDTO.unitPrice);
		ps.setObject(++index, pi_purchaseDTO.totalValue);
		ps.setObject(++index, pi_purchaseDTO.allocatedApp);
		ps.setObject(++index, pi_purchaseDTO.expiryDate);
		ps.setObject(++index, pi_purchaseDTO.purchaseItemRemarks);
		ps.setObject(++index, pi_purchaseDTO.insertedBy);
		ps.setObject(++index, pi_purchaseDTO.insertionTime);
		ps.setObject(++index, pi_purchaseDTO.modifiedBy);
		ps.setObject(++index, pi_purchaseDTO.assignedOfcUnitIds);
		if (isInsert) {
			ps.setObject(++index, pi_purchaseDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if (!isInsert) {
			ps.setObject(++index, pi_purchaseDTO.iD);
		}
	}

	@Override
	public Pi_purchaseDTO buildObjectFromResultSet(ResultSet rs) {
		try {
			Pi_purchaseDTO pi_purchaseDTO = new Pi_purchaseDTO();
			int i = 0;
			pi_purchaseDTO.iD = rs.getLong(columnNames[i++]);
			pi_purchaseDTO.officeUnitId = rs.getLong(columnNames[i++]);
			pi_purchaseDTO.fiscalYearId = rs.getLong(columnNames[i++]);
			pi_purchaseDTO.parentId = rs.getLong(columnNames[i++]);
			pi_purchaseDTO.packageId = rs.getLong(columnNames[i++]);
			pi_purchaseDTO.lotId = rs.getLong(columnNames[i++]);
			pi_purchaseDTO.vendorId = rs.getLong(columnNames[i++]);
			pi_purchaseDTO.sarokNumber = rs.getString(columnNames[i++]);
			pi_purchaseDTO.purchaseOrderNumber = rs.getString(columnNames[i++]);
			pi_purchaseDTO.purchaseDate = rs.getLong(columnNames[i++]);
			pi_purchaseDTO.remarks = rs.getString(columnNames[i++]);
			pi_purchaseDTO.filesDropzone = rs.getLong(columnNames[i++]);
			pi_purchaseDTO.totalBill = rs.getDouble(columnNames[i++]);
			pi_purchaseDTO.itemGroupId = rs.getLong(columnNames[i++]);
			pi_purchaseDTO.itemTypeId = rs.getLong(columnNames[i++]);
			pi_purchaseDTO.itemId = rs.getLong(columnNames[i++]);
			pi_purchaseDTO.amount = rs.getDouble(columnNames[i++]);
			pi_purchaseDTO.unitPrice = rs.getDouble(columnNames[i++]);
			pi_purchaseDTO.totalValue = rs.getDouble(columnNames[i++]);
			pi_purchaseDTO.allocatedApp = rs.getDouble(columnNames[i++]);
			pi_purchaseDTO.expiryDate = rs.getLong(columnNames[i++]);
			pi_purchaseDTO.purchaseItemRemarks = rs.getString(columnNames[i++]);
			pi_purchaseDTO.insertedBy = rs.getLong(columnNames[i++]);
			pi_purchaseDTO.insertionTime = rs.getLong(columnNames[i++]);
			pi_purchaseDTO.modifiedBy = rs.getLong(columnNames[i++]);
			pi_purchaseDTO.assignedOfcUnitIds = rs.getString(columnNames[i++]);
			pi_purchaseDTO.isDeleted = rs.getInt(columnNames[i++]);
			pi_purchaseDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return pi_purchaseDTO;
		} catch (SQLException ex) {
			logger.error(ex);
			return null;
		}
	}

	public Pi_purchaseDTO getDTOByID(long id) {
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "pi_purchase";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Pi_purchaseDTO) commonDTO, addQuery, true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Pi_purchaseDTO) commonDTO, updateQuery, false);
	}

	@Override
	public String getLastModifierUser() {
		return "modified_by";
	}

	public List<Pi_purchaseDTO> getDTOsForStockModel(long officeUnitId, long fiscalYearId, long itemId) {
		long fiscalFirstDate = Fiscal_yearRepository.getInstance().getFirstDateofFiscalYear(fiscalYearId);
		return getDTOs(String.format(getDTOsForStockView, officeUnitId, fiscalYearId, itemId, fiscalFirstDate));
	}

	public List<Pi_purchaseDTO> getByStartAndEndTime(long officeUnitId, long startTime, long endTime) {
		return getDTOs(getDTOByStartAndEnd, Arrays.asList(officeUnitId, startTime, endTime));
	}

	public List<Pi_purchaseDTO> getPurchaseDetailsByItemId(long officeUnitId, long productId) {
		return getDTOs(getPurchaseDetailsByItemId, Arrays.asList(officeUnitId, productId));
	}

	public List<Pi_purchaseDTO> getPurchaseDetailsByOnlyItemId(long productId) {
		return getDTOs(getPurchaseDetailsByOnlyItemId, Arrays.asList(productId));
	}

	public int getPurchaseCountByFiscalAndUnitAndItem(long fiscalYearId, long officeUnitId, long itemId) {
		String sql = String.format(String.format(getPurchaseCountByFiscalAndUnitAndItem, officeUnitId, itemId, fiscalYearId));
		return ConnectionAndStatementUtil.getT(sql, rs -> {
			try {
				return rs.getInt(1);
			} catch (SQLException ex) {
				ex.printStackTrace();
				return null;
			}
		}, 0);
	}

	public List<Pi_purchaseDTO> getDTOsByPurchaseOrderNumber(String purchaseOrderNumber){
		purchaseOrderNumber = purchaseOrderNumber.trim();
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM pi_purchase WHERE isDeleted = 0 ")
				.append("AND purchase_order_number='")
				.append(purchaseOrderNumber)
				.append("' ORDER BY purchase_date ASC");
		return getDTOs(sql.toString());
	}
}
	