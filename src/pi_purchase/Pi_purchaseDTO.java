package pi_purchase;
import java.util.*; 
import util.*; 


public class Pi_purchaseDTO extends CommonDTO
{

	public long officeUnitId = -1;
	public long fiscalYearId = -1;
	public long parentId = -1;
	public long packageId = -1;   // currently use as package final id
	public long lotId = -1;   // currently use as lot final id
	public long vendorId = -1;
    public String sarokNumber = "";
    public String purchaseOrderNumber = "";
	public long purchaseDate = System.currentTimeMillis();
	public long filesDropzone = -1;
	public double totalBill = 0;
	public long itemGroupId = -1;
	public long itemTypeId = -1;
	public long itemId = -1;
	public double amount = 0;
	public double totalValue = 0;
	public long expiryDate = System.currentTimeMillis();
	public long insertedBy = -1;
	public long insertionTime = -1;
	public long modifiedBy = -1;
	public double remainingStock = 0.0;
	public double unitPrice = 0.0;
	public double allocatedApp = 0.0;

	public String assignedOfcUnitIds = "";
	public String purchaseItemRemarks = "";

	
	
    @Override
	public String toString() {
            return "$Pi_purchaseDTO[" +
            " iD = " + iD +
            " officeUnitId = " + officeUnitId +
            " fiscalYearId = " + fiscalYearId +
            " packageId = " + packageId +
            " vendorId = " + vendorId +
            " sarokNumber = " + sarokNumber +
            " purchaseDate = " + purchaseDate +
            " remarks = " + remarks +
            " filesDropzone = " + filesDropzone +
            " totalBill = " + totalBill +
            " itemTypeId = " + itemTypeId +
            " itemId = " + itemId +
            " amount = " + amount +
            " totalValue = " + totalValue +
            " expiryDate = " + expiryDate +
            " insertedBy = " + insertedBy +
            " insertionTime = " + insertionTime +
            " isDeleted = " + isDeleted +
            " modifiedBy = " + modifiedBy +
            " lastModificationTime = " + lastModificationTime +
			" assignedOfcUnitIds = " + assignedOfcUnitIds +
            "]";
    }

}