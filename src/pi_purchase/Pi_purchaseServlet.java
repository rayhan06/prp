package pi_purchase;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import am_parliament_building_block.Am_parliament_building_blockRepository;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import pi_office_unit_store_keeper_mapping.Pi_office_unit_store_keeper_mappingDAO;
import pi_package_vendor_items.OptionsWithCountDTO;
import pi_package_vendor_items.Pi_package_vendor_itemsDAO;
import pi_package_vendor_items.Pi_package_vendor_itemsDTO;
import pi_purchase_parent.Pi_purchase_parentDAO;
import pi_purchase_parent.Pi_purchase_parentDTO;
import procurement_goods.Procurement_goodsDAO;
import procurement_goods.Procurement_goodsDTO;
import procurement_goods.Procurement_goodsRepository;
import procurement_package.*;
import role.PermissionRepository;

import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;
import javax.servlet.http.*;
import java.util.*;


import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;
import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import files.*;



/**
 * Servlet implementation class Pi_purchaseServlet
 */
@WebServlet("/Pi_purchaseServlet")
@MultipartConfig
public class Pi_purchaseServlet extends BaseServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_purchaseServlet.class);

    @Override
    public String getTableName() {
        return Pi_purchaseDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_purchaseServlet";
    }

    @Override
    public Pi_purchaseDAO getCommonDAOService() {
        return Pi_purchaseDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.PI_PURCHASE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.PI_PURCHASE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.PI_PURCHASE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_purchaseServlet.class;
    }
	FilesDAO filesDAO = new FilesDAO();
    private final Gson gson = new Gson();


	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception
	{
		// TODO Auto-generated method stub
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addPi_purchase");
			String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
			Pi_purchaseDTO pi_purchaseDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				pi_purchaseDTO = new Pi_purchaseDTO();
			}
			else
			{
				pi_purchaseDTO = Pi_purchaseDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("officeUnitId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("officeUnitId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				pi_purchaseDTO.officeUnitId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("fiscalYearId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("fiscalYearId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				pi_purchaseDTO.fiscalYearId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("packageId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("packageId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				pi_purchaseDTO.packageId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("vendorId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("vendorId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				pi_purchaseDTO.vendorId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("sarokNumber");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("sarokNumber = " + Value);
			if(Value != null)
			{
				pi_purchaseDTO.sarokNumber = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("purchaseDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("purchaseDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					pi_purchaseDTO.purchaseDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw new Exception(LM.getText(LC.PI_PURCHASE_ADD_PURCHASEDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("remarks");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("remarks = " + Value);
			if(Value != null)
			{
				pi_purchaseDTO.remarks = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("filesDropzone");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("filesDropzone = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				System.out.println("filesDropzone = " + Value);

				pi_purchaseDTO.filesDropzone = Long.parseLong(Value);


				if(addFlag == false)
				{
					String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
					String[] deleteArray = filesDropzoneFilesToDelete.split(",");
					for(int i = 0; i < deleteArray.length; i ++)
					{
						System.out.println("going to delete " + deleteArray[i]);
						if(i>0)
						{
							filesDAO.delete(Long.parseLong(deleteArray[i]));
						}
					}
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			final long[] parentId = {-1};

			String[] Values = request.getParameterValues("itemId");

			for (int index=0;index < Values.length; index++) {

				Value = request.getParameterValues("selected")[index];
				System.out.println("selected = " + Value);

				Boolean selected = Boolean.parseBoolean(Value);
				if (selected == null || !selected)continue;

				Value = request.getParameterValues("totalBill")[index];

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("totalBill = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					pi_purchaseDTO.totalBill = Double.parseDouble(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = request.getParameterValues("itemTypeId")[index];

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("itemTypeId = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					pi_purchaseDTO.itemTypeId = Long.parseLong(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = request.getParameterValues("itemId")[index];

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("itemId = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					pi_purchaseDTO.itemId = Long.parseLong(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = request.getParameterValues("amount")[index];

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("amount = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					pi_purchaseDTO.amount = Double.parseDouble(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				pi_purchaseDTO.totalValue = pi_purchaseDTO.totalBill;

				Value = request.getParameterValues("expiryDate")[index];

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("expiryDate = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					try
					{
						Date d = f.parse(Value);
						pi_purchaseDTO.expiryDate = d.getTime();
					}
					catch (Exception e)
					{
						e.printStackTrace();
						throw new Exception(LM.getText(LC.PI_PURCHASE_ADD_EXPIRYDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
					}
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = request.getParameterValues("insertedBy")[index];

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("insertedBy = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					pi_purchaseDTO.insertedBy = Long.parseLong(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = request.getParameterValues("insertionTime")[index];

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("insertionTime = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					pi_purchaseDTO.insertionTime = Long.parseLong(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = request.getParameterValues("modifiedBy")[index];

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("modifiedBy = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					pi_purchaseDTO.modifiedBy = Long.parseLong(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				System.out.println("Done adding  addPi_purchase dto = " + pi_purchaseDTO);
				long[] returnedID = {-1};


				int finalIndex = index;
				Utils.handleTransaction(()->{
					if(addFlag == true)
					{
						if (finalIndex == 0) {
							Pi_purchase_parentDTO parentDTO = gson.fromJson(
									gson.toJson(pi_purchaseDTO),
									Pi_purchase_parentDTO.class
							);
							parentId[0] = Pi_purchase_parentDAO.getInstance().add(parentDTO);
						}
						pi_purchaseDTO.parentId = parentId[0];
						Pi_purchaseDAO.getInstance().add(pi_purchaseDTO);
					}
					else
					{
						Pi_purchaseDAO.getInstance().update(pi_purchaseDTO);
					}
				});

			}


			return pi_purchaseDTO;

		}

	}

}

