package holidays;

import dbm.DBMR;
import dbm.DBMW;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;

public class HolidaysDAO extends NavigationService4 {

    Logger logger = Logger.getLogger(getClass());


    public HolidaysDAO(String tableName) {
        super(tableName);
        joinSQL = "";

        commonMaps = new HolidaysMAPS(tableName);
    }

    public HolidaysDAO() {
        this("holidays");
    }


    public long add(CommonDTO commonDTO) throws Exception {

        HolidaysDTO holidaysDTO = (HolidaysDTO) commonDTO;

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();

        try {
            connection = DBMW.getInstance().getConnection();

            if (connection == null) {
                System.out.println("nullconn");
            }

            holidaysDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

            String sql = "INSERT INTO " + tableName;

            sql += " (";
            sql += "ID";
            sql += ", ";
            sql += "holiday_date";
            sql += ", ";
            sql += "year";
            sql += ", ";
            sql += "description";
            sql += ", ";
            sql += "isDeleted";
            sql += ", ";
            sql += "lastModificationTime";

            sql += ")";


            sql += " VALUES(";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";

            sql += ")";


            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, holidaysDTO.iD);
            ps.setObject(index++, holidaysDTO.holidayDate);
            ps.setObject(index++, holidaysDTO.year);
            ps.setObject(index++, holidaysDTO.description);
            ps.setObject(index++, holidaysDTO.isDeleted);
            ps.setObject(index++, lastModificationTime);

            System.out.println(ps);
            ps.execute();


            recordUpdateTime(connection, lastModificationTime);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return holidaysDTO.iD;
    }


    //need another getter for repository
    public CommonDTO getDTOByID(long ID) throws Exception {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        HolidaysDTO holidaysDTO = null;
        try {

            String sql = "SELECT * ";

            sql += " FROM " + tableName;

            sql += " WHERE ID=" + ID;

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                holidaysDTO = new HolidaysDTO();

                holidaysDTO.iD = rs.getLong("ID");
                holidaysDTO.holidayDate = rs.getLong("holiday_date");
                holidaysDTO.year = rs.getInt("year");
                holidaysDTO.description = rs.getString("description");
                holidaysDTO.isDeleted = rs.getInt("isDeleted");
                holidaysDTO.lastModificationTime = rs.getLong("lastModificationTime");

            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return holidaysDTO;
    }

    public long update(CommonDTO commonDTO) throws Exception {
        HolidaysDTO holidaysDTO = (HolidaysDTO) commonDTO;

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            connection = DBMW.getInstance().getConnection();

            String sql = "UPDATE " + tableName;

            sql += " SET ";
            sql += "holiday_date=?";
            sql += ", ";
            sql += "year=?";
            sql += ", ";
            sql += "description=?";
            sql += ", lastModificationTime = " + lastModificationTime + "";
            sql += " WHERE ID = " + holidaysDTO.iD;


            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, holidaysDTO.holidayDate);
            ps.setObject(index++, holidaysDTO.year);
            ps.setObject(index++, holidaysDTO.description);
            System.out.println(ps);
            ps.executeUpdate();


            recordUpdateTime(connection, lastModificationTime);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return holidaysDTO.iD;
    }


    public List<HolidaysDTO> getDTOs(Collection recordIDs) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        HolidaysDTO holidaysDTO = null;
        List<HolidaysDTO> holidaysDTOList = new ArrayList<>();
        if (recordIDs.isEmpty()) {
            return holidaysDTOList;
        }
        try {

            String sql = "SELECT * ";

            sql += " FROM " + tableName;

            sql += " WHERE ID IN ( ";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql += ",";
                }
                sql += ((ArrayList) recordIDs).get(i);
            }
            sql += ")  order by lastModificationTime desc";

            printSql(sql);

            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                holidaysDTO = new HolidaysDTO();
                holidaysDTO.iD = rs.getLong("ID");
                holidaysDTO.holidayDate = rs.getLong("holiday_date");
                holidaysDTO.year = rs.getInt("year");
                holidaysDTO.description = rs.getString("description");
                holidaysDTO.isDeleted = rs.getInt("isDeleted");
                holidaysDTO.lastModificationTime = rs.getLong("lastModificationTime");
                System.out.println("got this DTO: " + holidaysDTO);

                holidaysDTOList.add(holidaysDTO);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return holidaysDTOList;

    }


    //add repository
    public List<HolidaysDTO> getAllHolidays(boolean isFirstReload) {
        List<HolidaysDTO> holidaysDTOList = new ArrayList<>();

        String sql = "SELECT * FROM holidays";
        sql += " WHERE ";


        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        sql += " order by holidays.lastModificationTime desc";
        printSql(sql);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;


        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);


            while (rs.next()) {
                HolidaysDTO holidaysDTO = new HolidaysDTO();
                holidaysDTO.iD = rs.getLong("ID");
                holidaysDTO.holidayDate = rs.getLong("holiday_date");
                holidaysDTO.year = rs.getInt("year");
                holidaysDTO.description = rs.getString("description");
                holidaysDTO.isDeleted = rs.getInt("isDeleted");
                holidaysDTO.lastModificationTime = rs.getLong("lastModificationTime");

                holidaysDTOList.add(holidaysDTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return holidaysDTOList;
    }


    public List<HolidaysDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }


    public List<HolidaysDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
                                     String filter, boolean tableHasJobCat) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        List<HolidaysDTO> holidaysDTOList = new ArrayList<>();

        try {

            String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                HolidaysDTO holidaysDTO = new HolidaysDTO();
                holidaysDTO.iD = rs.getLong("ID");
                holidaysDTO.holidayDate = rs.getLong("holiday_date");
                holidaysDTO.year = rs.getInt("year");
                holidaysDTO.description = rs.getString("description");
                holidaysDTO.isDeleted = rs.getInt("isDeleted");
                holidaysDTO.lastModificationTime = rs.getLong("lastModificationTime");

                holidaysDTOList.add(holidaysDTO);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return holidaysDTOList;

    }

}
