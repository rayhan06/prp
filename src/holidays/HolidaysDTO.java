package holidays;

import util.CommonDTO;


public class HolidaysDTO extends CommonDTO {

    public long holidayDate = 0;
    public int year = 0;
    public String description = "";


    @Override
    public String toString() {
        return "$HolidaysDTO[" +
                " iD = " + iD +
                " holidayDate = " + holidayDate +
                " year = " + year +
                " description = " + description +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}