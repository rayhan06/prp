package holidays;

import dbm.DBMR;
import dbm.DBMW;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PopulateHoliday {
    public static void main(String[] arrgs){

        Date date = new Date(2034-1900,11-1,10);
        long miliseconds;
        long constDay = 86400000;
        for(int i = 0; i < 30; i++){
            System.out.println(date);

            miliseconds = date.getTime();
            //System.out.println(miliseconds);
            insertOrUpdateHoliday(miliseconds,"");
            date.setTime(date.getTime()+constDay);  //increase 1 day

            miliseconds = date.getTime();
            //System.out.println(miliseconds);
            insertOrUpdateHoliday(miliseconds,"");
            date.setTime(date.getTime()+constDay*6);   //increase 6 days
        }
//        date = new Date(2021-1900,0,1);
//        System.out.println(date.getDate());
//        date.setTime(date.getTime()+86400000);
//        System.out.println(date.getDate());

    }

    private static void insertHolidayValue(long date, int value, String note) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        Long id = 1000L;

        try {
            id = DBMW.getInstance().getNextSequenceId( "holiday" );
        } catch (Exception e) {
            e.printStackTrace();
        }

        String sql = "insert into holiday(ID,current_day,is_holiday,description) values(?,?,?,?)";
        try {
            con = DBMW.getInstance().getConnection();
            ps = con.prepareStatement(sql);
            int index = 0;
            ps.setLong(++index, id);
            ps.setLong(++index, date);
            ps.setInt(++index, value);
            ps.setString(++index, note);
            ps.executeUpdate();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        finally {
            try {
                if(con != null) {
                    DBMW.getInstance().freeConnection(con);
                }
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
            try {
                if(ps != null) {
                    ps.close();
                }
            }
            catch(Exception ex) {
                ex.printStackTrace();
            }
            try {
                if(rs != null) {
                    rs.close();
                }
            }
            catch(Exception ex) {
                ex.printStackTrace();
            }

        }
    }

    private static void insertOrUpdateHoliday(long date, String note) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBMR.getInstance().getConnection();
            String sql = "select * from holiday where current_day = ?";
            ps = con.prepareStatement(sql);
            int index = 0;
            ps.setLong(++index, date);
            rs = ps.executeQuery();
            if(rs.next()) {

            }
            else {
                //this.insertHolidayValue(date, 0);
                insertHolidayValue(date, 1, note);
            }
            //System.out.println(" printing rs : " + rs);
            //System.out.println(" printing rs.next() : " + rs.next());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            if(con != null) {
                try{
                    DBMR.getInstance().freeConnection(con);
                }
                catch(Exception ex) {
                    ex.printStackTrace();
                }
            }
            if(ps != null) {
                try {
                    ps.close();
                }
                catch(Exception ex) {
                    ex.printStackTrace();
                }
            }
            if(rs != null) {
                try {
                    rs.close();
                }
                catch(Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

    }
}
