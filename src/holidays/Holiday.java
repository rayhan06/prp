package holidays;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import dbm.DBMR;
import dbm.DBMW;
import org.json.JSONArray;
import org.json.JSONObject;


public class Holiday {
	private int currentHolidayStatus = -1;
	
	private void updateHolidayValue(long date, int value, String note) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		String sql = "update holiday set is_holiday = ?, description = ? where current_day = ?";
		if(1 == value) {
			value = 0;
		}
		else if(0 == value) {
			value = 1;
		}
		else if(-1 == value) {
			value = 1;
		}
		
		try {
			con = DBMW.getInstance().getConnection();
			ps = con.prepareStatement(sql);
			int index = 0;
			ps.setInt(++index, value);
			ps.setString(++index, note);
			ps.setLong(++index, date);
			ps.executeUpdate();
			currentHolidayStatus = value;
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		finally {
			try {
				if(con != null) {
					DBMW.getInstance().freeConnection(con);
				}
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
			try {
				if(rs != null) {
					rs.close();
				}
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
			try {
				if(ps != null) {
					ps.close();
				}
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	private void insertHolidayValue(long date, int value, String note) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		Long id = 1000L;

		try {
			id = DBMW.getInstance().getNextSequenceId( "holiday" );
		} catch (Exception e) {
			e.printStackTrace();
		}

		String sql = "insert into holiday(ID,current_day,is_holiday,description) values(?,?,?,?)";
		try {
			con = DBMW.getInstance().getConnection();
			ps = con.prepareStatement(sql);
			int index = 0;
			ps.setLong(++index, id);
			ps.setLong(++index, date);
			ps.setInt(++index, value);
			ps.setString(++index, note);
			ps.executeUpdate();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		finally {
			try {
				if(con != null) {
					DBMW.getInstance().freeConnection(con);
				}	
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
			try {
				if(ps != null) {
					ps.close();
				}
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
			try {
				if(rs != null) {
					rs.close();
				}
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
			
		}
	}
	
	public void toggleOrInsert(long date, String note) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DBMR.getInstance().getConnection();
			String sql = "select * from holiday where current_day = ?";
			ps = con.prepareStatement(sql);
			int index = 0;
			ps.setLong(++index, date);
			rs = ps.executeQuery();
			if(rs.next()) {
				// already contains date
				// just toggle the date value
				int isHoliday = rs.getInt("is_holiday");
				this.updateHolidayValue(date, isHoliday, note);
			}
			else {
				//this.insertHolidayValue(date, 0);
				this.insertHolidayValue(date, 1, note);
			}
			System.out.println(" printing rs : " + rs);
			System.out.println(" printing rs.next() : " + rs.next());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			if(con != null) {
				try{
					DBMR.getInstance().freeConnection(con);
				}
				catch(Exception ex) {
					ex.printStackTrace();
				}
			}
			if(ps != null) {
				try {
					ps.close();
				}
				catch(Exception ex) {
					ex.printStackTrace();
				}
			}
			if(rs != null) {
				try {
					rs.close();
				}
				catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		
	}
	
	public int getCurrentHolidayStatus(long date) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "select * from holiday where current_day = ?";
		try {
			con = DBMR.getInstance().getConnection();
			ps = con.prepareStatement(sql);
			int index = 0;
			ps.setLong(++index, date);
			rs = ps.executeQuery();
			if(rs.next()) {
				return rs.getInt("is_holiday");	
			}			
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		finally {
			if(con != null) {
				try {
					DBMR.getInstance().freeConnection(con);
				}
				catch(Exception ex) {
					ex.printStackTrace();
				}				
			}
			if(ps != null) {
				try {
					ps.close();	
				}
				catch(Exception ex) {
					ex.printStackTrace();
				}				
			}
			if(rs != null) {
				try {
					rs.close();
				}
				catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		return -1;
	}

	public String getHolidayForDNCalendar() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "select * from holiday where is_holiday = 1";
		JSONArray jsonArray = new JSONArray();
		
		try {
			con = DBMR.getInstance().getConnection();
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			
			while(rs.next()) {
				JSONObject jsonObject = new JSONObject();
				long date = rs.getLong("current_day");
				String note = rs.getString("description");
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(date);
				
				int year = calendar.get(Calendar.YEAR);
				int month = calendar.get(Calendar.MONTH)+1;
				int day = calendar.get(Calendar.DAY_OF_MONTH);
				
				String dateStr = year + "-" + month + "-" + day;

				jsonObject.put("date", dateStr);
				
				JSONArray noteArray = new JSONArray();
				noteArray.put(note);
				jsonObject.put("note", noteArray);
				
				jsonArray.put(jsonObject);
			}
			
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		finally {
			if(con != null) {
				try {
					DBMR.getInstance().freeConnection(con);
				}
				catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		
		return jsonArray.toString();
	}

	public String getNotesForDNCalendar() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "select * from holiday where is_holiday = 1";
		JSONArray jsonArray = new JSONArray();

		try {
			con = DBMR.getInstance().getConnection();
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while(rs.next()) {

				long date = rs.getLong("current_day");
				String note = rs.getString("description");
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(date);

				int year = calendar.get(Calendar.YEAR);
				int month = calendar.get(Calendar.MONTH)+1;
				int day = calendar.get(Calendar.DAY_OF_MONTH);

				String dateStr = year + "-" + month + "-" + day;

				if (!note.equalsIgnoreCase("")) {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("date", dateStr);

					JSONArray noteArray = new JSONArray();
					noteArray.put(note);
					jsonObject.put("note", noteArray);

					jsonArray.put(jsonObject);
				}
			}

		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		finally {
			if(con != null) {
				try {
					DBMR.getInstance().freeConnection(con);
				}
				catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		}

		return jsonArray.toString();
	}

	public Map<Long,String> getAllHoliday() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "select * from holiday where is_holiday = 1";
		Map<Long,String> holidayMap = new HashMap<>();

		try {
			con = DBMR.getInstance().getConnection();
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while(rs.next()) {

				long date = rs.getLong("current_day");
				String note = rs.getString("description");
				if(note==null || note.isEmpty()) note = "Weekend";
				holidayMap.put(date,note);
			}

		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		finally {
			if(con != null) {
				try {
					DBMR.getInstance().freeConnection(con);
				}
				catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		}

		return holidayMap;
	}

	public String getWorkDaysForDNCalendar() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "select * from holiday where is_holiday = 0";
		JSONArray jsonArray = new JSONArray();
		
		try {
			con = DBMR.getInstance().getConnection();
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			
			while(rs.next()) {
				JSONObject jsonObject = new JSONObject();
				long date = rs.getLong("current_day");
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(date);
				
				int year = calendar.get(Calendar.YEAR);
				int month = calendar.get(Calendar.MONTH)+1;
				int day = calendar.get(Calendar.DAY_OF_MONTH);
				
				String dateStr = year + "-" + month + "-" + day;

				jsonObject.put("date", dateStr);
				
				JSONArray noteArray = new JSONArray();
				noteArray.put("workday");
				jsonObject.put("note", noteArray);
				
				jsonArray.put(jsonObject);
			}
			
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		finally {
			if(con != null) {
				try {
					DBMR.getInstance().freeConnection(con);
				}
				catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		
		return jsonArray.toString();
	}
}
