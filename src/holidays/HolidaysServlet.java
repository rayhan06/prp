package holidays;

import com.google.gson.Gson;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import pbReport.DateUtils;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.RecordNavigationManager4;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;


/**
 * Servlet implementation class HolidaysServlet
 */
@WebServlet("/HolidaysServlet")
@MultipartConfig
public class HolidaysServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(HolidaysServlet.class);
    private final CalenderUtil calenderUtil = new CalenderUtil();

    String tableName = "holidays";

    HolidaysDAO holidaysDAO;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public HolidaysServlet() {
        super();
        try {
            holidaysDAO = new HolidaysDAO(tableName);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getAddPage")) {
               // if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.HOLIDAYS_ADD)) 
                {
                    getAddPage(request, response);
                } 
               
            } else if (actionType.equals("getEditPage")) {
              //  if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.HOLIDAYS_UPDATE)) 
                {
                    getHolidays(request, response);
                } 
            } else if (actionType.equals("getUploadPage")) {
               // if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.HOLIDAYS_UPLOAD)) 
                {
                    geUploadPage(request, response);
                }
            } else if (actionType.equals("getURL")) {
                String URL = request.getParameter("URL");
                System.out.println("URL = " + URL);
                response.sendRedirect(URL);
            } else if (actionType.equals("search")) {
                System.out.println("search requested");
               // if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.HOLIDAYS_SEARCH)) 
                {

                    if (isPermanentTable) {
                        searchHolidays(request, response, isPermanentTable);
                    } else {
                        //searchHolidays(request, response, tempTableName, isPermanentTable);
                    }
                }
            } else {
                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("ID", -1L);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("holidays/holidaysEdit.jsp");
        requestDispatcher.forward(request, response);
    }

    private void geUploadPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("Getting upload page");
        request.setAttribute("ID", -1L);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("holidays/holidaysUpload.jsp");
        requestDispatcher.forward(request, response);
    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        System.out.println("Part Header = {0}" + partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    public ArrayList<HolidaysDTO> ReadXLsToArraylist(HttpServletRequest request, String fileName) throws IOException {
        String path = getServletContext().getRealPath("/img2/");
        File excelFile = new File(path + File.separator
                + fileName);
        FileInputStream fis = new FileInputStream(excelFile);


        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIt = sheet.iterator();
        ArrayList<String> Rows = new ArrayList<String>();
        ArrayList<HolidaysDTO> holidaysDTOs = new ArrayList<HolidaysDTO>();


        HolidaysDTO holidaysDTO;

        String failureMessage = "";
        int i = 0;

        HolidaysMAPS holidaysMAPS = new HolidaysMAPS("holidays");
        while (rowIt.hasNext()) {
            Row row = rowIt.next();

            Iterator<Cell> cellIterator = row.cellIterator();

            holidaysDTO = new HolidaysDTO();

            int j = 0;

            try {
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();


                    if (i == 0) {
                        Rows.add(cell.toString());
                        System.out.println("Rows found: " + cell + " ");
                    } else {

                        String rowName = Rows.get(j).toLowerCase();
                        System.out.println("rowname: " + rowName + " rowname from map = " + holidaysMAPS.java_Text_map.get(rowName));

                        if (rowName == null || rowName.equalsIgnoreCase("")) {
                            System.out.println("null row name");
                            break;
                        }
                        if (cell == null || cell.toString().equalsIgnoreCase("")) {
                            System.out.println("null cell");
                            j++;
                            continue;
                        } else {
                            System.out.println("Inserting Value = " + cell + " to row " + rowName);

                        }
                        if (holidaysMAPS.java_Text_map.get(rowName) != null && holidaysMAPS.java_Text_map.get(rowName).equalsIgnoreCase("holidayDate")) {
                            holidaysDTO.holidayDate = DateUtils.stringToDate(cell.toString());
                        } else if (holidaysMAPS.java_Text_map.get(rowName) != null && holidaysMAPS.java_Text_map.get(rowName).equalsIgnoreCase("year")) {
                            holidaysDTO.year = (int) Double.parseDouble(cell.toString());
                        } else if (holidaysMAPS.java_Text_map.get(rowName) != null && holidaysMAPS.java_Text_map.get(rowName).equalsIgnoreCase("description")) {
                            holidaysDTO.description = (cell.toString());
                        }
                    }
                    j++;

                }


                if (i != 0) {
                    System.out.println("INSERTING to the list: " + holidaysDTO);
                    holidaysDTOs.add(holidaysDTO);
                }
            } catch (Exception e) {
                e.printStackTrace();
                failureMessage += (i + 1) + " ";
            }
            i++;

            System.out.println();

        }
        if (failureMessage.equalsIgnoreCase("")) {
            failureMessage = " Successfully parsed all rows";
        } else {
            failureMessage = " Failed on rows: " + failureMessage;
        }
        request.setAttribute("failureMessage", failureMessage);

        workbook.close();
        fis.close();
        return holidaysDTOs;

    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        System.out.println("doPost");
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("add")) {

              //  if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.HOLIDAYS_ADD))
                {
                    System.out.println("going to  addHolidays ");
                    addHolidays(request, response, true, userDTO, true);
                } 

            }

            if (actionType.equals("getDTO")) {

                //if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.HOLIDAYS_ADD)) 
                {
                    getDTO(request, response);
                } 

            } else if (actionType.equals("edit")) {

                //if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.HOLIDAYS_UPDATE))
                {
                    addHolidays(request, response, false, userDTO, isPermanentTable);
                }
            } else if (actionType.equals("upload")) {

                //if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.HOLIDAYS_UPLOAD))
                {
                    uploadHolidays(request, response, false);
                } 
            } else if (actionType.equals("uploadConfirmed")) {

               // if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.HOLIDAYS_UPLOAD))
                {
                    System.out.println("uploadConfirmed");
                    addHolidayss(request, response);
                } 
            } else if (actionType.equals("delete")) {
                deleteHolidays(request, response, userDTO);
            } else if (actionType.equals("search")) {
               // if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.HOLIDAYS_SEARCH)) 
                {
                    searchHolidays(request, response, true);
                } 
            } else if (actionType.equals("getGeo")) {
                System.out.println("going to geoloc ");
                request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getDTO(HttpServletRequest request, HttpServletResponse response) {
        try {
            System.out.println("In getDTO");
            HolidaysDTO holidaysDTO = (HolidaysDTO) holidaysDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            String encoded = this.gson.toJson(holidaysDTO);
            System.out.println("json encoded data = " + encoded);
            out.print(encoded);
            out.flush();
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void uploadHolidays(HttpServletRequest request, HttpServletResponse response, Boolean addFlag) throws IOException {
        System.out.println("%%%% ajax upload called");
        Part filePart_holidaysDatabase;
        try {
            filePart_holidaysDatabase = request.getPart("holidaysDatabase");
            String Value = getFileName(filePart_holidaysDatabase);
            System.out.println("holidaysDatabase = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                if (Value != null && !Value.equalsIgnoreCase("")) {
                    String FileName = "holidaysdatabase";
                    String path = getServletContext().getRealPath("/img2/");
                    Utils.uploadFile(filePart_holidaysDatabase, FileName, path);
                    ArrayList<HolidaysDTO> holidaysDTOs = ReadXLsToArraylist(request, FileName);
                    HttpSession session = request.getSession(true);
                    session.setAttribute("holidaysDTOs", holidaysDTOs);

                    RequestDispatcher rd = request.getRequestDispatcher("holidays/holidaysReview.jsp?actionType=edit");
                    rd.forward(request, response);
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
        } catch (ServletException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void addHolidayss(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String[] paramValues = request.getParameterValues("iD");
        for (int i = 0; i < paramValues.length; i++) {
            String paramValue = paramValues[i];
            HolidaysDTO holidaysDTO = new HolidaysDTO();
            try {
                String Value = "";

                if (request.getParameterValues("holidayDate") != null) {
                    Value = request.getParameterValues("holidayDate")[i];
                    if (Value != null) {
                        Value = Jsoup.clean(Value, Whitelist.simpleText());
                    }
                    System.out.println("holidayDate = " + Value);
                    if (Value != null && !Value.equalsIgnoreCase("")) {
                        holidaysDTO.holidayDate = Long.parseLong(Value);
                    } else {
                        System.out.println("FieldName has a null Value, not updating" + " = " + Value);
                    }
                }
                if (request.getParameterValues("year") != null) {
                    Value = request.getParameterValues("year")[i];
                    if (Value != null) {
                        Value = Jsoup.clean(Value, Whitelist.simpleText());
                    }
                    System.out.println("year = " + Value);
                    if (Value != null && !Value.equalsIgnoreCase("")) {
                        holidaysDTO.year = Integer.parseInt(Value);
                    } else {
                        System.out.println("FieldName has a null Value, not updating" + " = " + Value);
                    }
                }
                if (request.getParameterValues("description") != null) {
                    Value = request.getParameterValues("description")[i];
                    if (Value != null) {
                        Value = Jsoup.clean(Value, Whitelist.simpleText());
                    }
                    System.out.println("description = " + Value);
                    if (Value != null && !Value.equalsIgnoreCase("")) {
                        holidaysDTO.description = (Value);
                    } else {
                        System.out.println("FieldName has a null Value, not updating" + " = " + Value);
                    }
                }


                System.out.println("Done adding  addHolidays dto = " + holidaysDTO);


                holidaysDAO.add(holidaysDTO);


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        calenderUtil.updateGovtHolidays();
        response.sendRedirect("HolidaysServlet?actionType=search");
    }

    private void approveHolidays(HttpServletRequest request, HttpServletResponse response, boolean b, UserDTO userDTO, boolean approveOrReject) {
        try {
            long id = Long.parseLong(request.getParameter("idToApprove"));
            HolidaysDTO holidaysDTO = (HolidaysDTO) holidaysDAO.getDTOByID(id);
            holidaysDTO.remarks = request.getParameter("remarks");
            holidaysDTO.fileID = Long.parseLong(request.getParameter("fileID"));
            if (approveOrReject) {
                holidaysDAO.manageWriteOperations(holidaysDTO, SessionConstants.APPROVE, id, userDTO);
            } else {
                holidaysDAO.manageWriteOperations(holidaysDTO, SessionConstants.REJECT, id, userDTO);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addHolidays(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addHolidays");
            String path = getServletContext().getRealPath("/img2/");
            HolidaysDTO holidaysDTO;
            String FileNamePrefix;
            if (addFlag == true) {
                holidaysDTO = new HolidaysDTO();
                FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
            } else {
                holidaysDTO = (HolidaysDTO) holidaysDAO.getDTOByID(Long.parseLong(request.getParameter("identity")));
                FileNamePrefix = request.getParameter("identity");
            }

            String Value = "";

            Value = request.getParameter("holidayDate");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("holidayDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                holidaysDTO.holidayDate = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("year");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("year = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                holidaysDTO.year = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("description");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("description = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                holidaysDTO.description = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            System.out.println("Done adding  addHolidays dto = " + holidaysDTO);
            long returnedID = -1;

            if (addFlag == true) {
                returnedID = holidaysDAO.manageWriteOperations(holidaysDTO, SessionConstants.INSERT, -1, userDTO);
            } else {
                if (isPermanentTable) {
                    returnedID = holidaysDAO.manageWriteOperations(holidaysDTO, SessionConstants.UPDATE, -1, userDTO);
                } else {
                    returnedID = holidaysDAO.manageWriteOperations(holidaysDTO, SessionConstants.VALIDATE, -1, userDTO);
                }

            }

            calenderUtil.updateGovtHolidays();
            String inPlaceSubmit = request.getParameter("inplacesubmit");

            if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                getHolidays(request, response, returnedID);
            } else {
                response.sendRedirect("HolidaysServlet?actionType=search");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void deleteHolidays(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException {
        try {
            String[] IDsToDelete = request.getParameterValues("ID");
            for (int i = 0; i < IDsToDelete.length; i++) {
                long id = Long.parseLong(IDsToDelete[i]);
                System.out.println("------ DELETING " + IDsToDelete[i]);


                HolidaysDTO holidaysDTO = (HolidaysDTO) holidaysDAO.getDTOByID(id);
                holidaysDAO.manageWriteOperations(holidaysDTO, SessionConstants.DELETE, id, userDTO);
                response.sendRedirect("HolidaysServlet?actionType=search");

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void getHolidays(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {
        System.out.println("in getHolidays");
        HolidaysDTO holidaysDTO = null;
        try {
            holidaysDTO = (HolidaysDTO) holidaysDAO.getDTOByID(id);
            request.setAttribute("ID", holidaysDTO.iD);
            request.setAttribute("holidaysDTO", holidaysDTO);
            request.setAttribute("holidaysDAO", holidaysDAO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "holidays/holidaysInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "holidays/holidaysSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "holidays/holidaysEditBody.jsp?actionType=edit";
                } else {
                    URL = "holidays/holidaysEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getHolidays(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getHolidays(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void searchHolidays(HttpServletRequest request, HttpServletResponse response, boolean isPermanent) throws ServletException, IOException {
        System.out.println("in  searchHolidays 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_HOLIDAYS,
                request,
                holidaysDAO,
                SessionConstants.VIEW_HOLIDAYS,
                SessionConstants.SEARCH_HOLIDAYS,
                tableName,
                isPermanent,
                userDTO,
                "",
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("holidaysDAO", holidaysDAO);
        RequestDispatcher rd;
        if (hasAjax == false) {
            System.out.println("Going to holidays/holidaysSearch.jsp");
            rd = request.getRequestDispatcher("holidays/holidaysSearch.jsp");
        } else {
            System.out.println("Going to holidays/holidaysSearchForm.jsp");
            rd = request.getRequestDispatcher("holidays/holidaysSearchForm.jsp");
        }
        rd.forward(request, response);
    }

}

