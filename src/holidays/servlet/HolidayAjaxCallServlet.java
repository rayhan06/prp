package holidays.servlet;

import holidays.Holiday;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/HolidayAjaxCallServlet")
public class HolidayAjaxCallServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String responseString = "";
        String actionType = request.getParameter("actionType");

        if( actionType.equals( "toggle" ) ){

            String holidayDate = request.getParameter("date");
            System.out.println("----------------------------------  date " + holidayDate);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = null;
            try {
                date = sdf.parse(holidayDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long miliseconds = date.getTime();

            System.out.println(" +++++++++++++++++++++ date milis : " + miliseconds);

            Holiday holiday = new Holiday();
            holiday.toggleOrInsert(miliseconds, request.getParameter("note"));

            responseString = Integer.toString( holiday.getCurrentHolidayStatus( miliseconds ) );

        }
        else if( actionType.equals( "check_status" )){
            String holidayDate = request.getParameter("date");
            System.out.println("---------------------------------- else date " + holidayDate);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = null;
            try {
                date = sdf.parse(holidayDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long miliseconds = date.getTime();

            System.out.println(" +++++++++++++++++++++ else date milis : " + miliseconds);

            Holiday holiday = new Holiday();

            responseString = Integer.toString( holiday.getCurrentHolidayStatus( miliseconds ) );
        }

        response.getWriter().print( responseString );
    }

}
