package holidays;

import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


public class HolidaysRepository implements Repository {
    static Logger logger = Logger.getLogger(HolidaysRepository.class);
    static HolidaysRepository instance = null;
    HolidaysDAO holidaysDAO = null;
    Map<Long, HolidaysDTO> mapOfHolidaysDTOToiD;
    Map<Long, Set<HolidaysDTO>> mapOfHolidaysDTOToholidayDate;
    Map<Integer, Set<HolidaysDTO>> mapOfHolidaysDTOToyear;
    Map<String, Set<HolidaysDTO>> mapOfHolidaysDTOTodescription;
    Map<Long, Set<HolidaysDTO>> mapOfHolidaysDTOTolastModificationTime;


    private HolidaysRepository() {
        mapOfHolidaysDTOToiD = new ConcurrentHashMap<>();
        mapOfHolidaysDTOToholidayDate = new ConcurrentHashMap<>();
        mapOfHolidaysDTOToyear = new ConcurrentHashMap<>();
        mapOfHolidaysDTOTodescription = new ConcurrentHashMap<>();
        mapOfHolidaysDTOTolastModificationTime = new ConcurrentHashMap<>();

        RepositoryManager.getInstance().addRepository(this);
    }

    public synchronized static HolidaysRepository getInstance() {
        if (instance == null) {
            instance = new HolidaysRepository();
        }
        return instance;
    }

    public void setDAO(HolidaysDAO holidaysDAO) {
        this.holidaysDAO = holidaysDAO;
    }

    public void reload(boolean reloadAll) {
        if (holidaysDAO == null) {
            return;
        }
        try {
            List<HolidaysDTO> holidaysDTOs = holidaysDAO.getAllHolidays(reloadAll);
            for (HolidaysDTO holidaysDTO : holidaysDTOs) {
                HolidaysDTO oldHolidaysDTO = getHolidaysDTOByID(holidaysDTO.iD);
                if (oldHolidaysDTO != null) {
                    mapOfHolidaysDTOToiD.remove(oldHolidaysDTO.iD);

                    if (mapOfHolidaysDTOToholidayDate.containsKey(oldHolidaysDTO.holidayDate)) {
                        mapOfHolidaysDTOToholidayDate.get(oldHolidaysDTO.holidayDate).remove(oldHolidaysDTO);
                    }
                    if (mapOfHolidaysDTOToholidayDate.get(oldHolidaysDTO.holidayDate).isEmpty()) {
                        mapOfHolidaysDTOToholidayDate.remove(oldHolidaysDTO.holidayDate);
                    }

                    if (mapOfHolidaysDTOToyear.containsKey(oldHolidaysDTO.year)) {
                        mapOfHolidaysDTOToyear.get(oldHolidaysDTO.year).remove(oldHolidaysDTO);
                    }
                    if (mapOfHolidaysDTOToyear.get(oldHolidaysDTO.year).isEmpty()) {
                        mapOfHolidaysDTOToyear.remove(oldHolidaysDTO.year);
                    }

                    if (mapOfHolidaysDTOTodescription.containsKey(oldHolidaysDTO.description)) {
                        mapOfHolidaysDTOTodescription.get(oldHolidaysDTO.description).remove(oldHolidaysDTO);
                    }
                    if (mapOfHolidaysDTOTodescription.get(oldHolidaysDTO.description).isEmpty()) {
                        mapOfHolidaysDTOTodescription.remove(oldHolidaysDTO.description);
                    }

                    if (mapOfHolidaysDTOTolastModificationTime.containsKey(oldHolidaysDTO.lastModificationTime)) {
                        mapOfHolidaysDTOTolastModificationTime.get(oldHolidaysDTO.lastModificationTime).remove(oldHolidaysDTO);
                    }
                    if (mapOfHolidaysDTOTolastModificationTime.get(oldHolidaysDTO.lastModificationTime).isEmpty()) {
                        mapOfHolidaysDTOTolastModificationTime.remove(oldHolidaysDTO.lastModificationTime);
                    }


                }
                if (holidaysDTO.isDeleted == 0) {

                    mapOfHolidaysDTOToiD.put(holidaysDTO.iD, holidaysDTO);

                    if (!mapOfHolidaysDTOToholidayDate.containsKey(holidaysDTO.holidayDate)) {
                        mapOfHolidaysDTOToholidayDate.put(holidaysDTO.holidayDate, new HashSet<>());
                    }
                    mapOfHolidaysDTOToholidayDate.get(holidaysDTO.holidayDate).add(holidaysDTO);

                    if (!mapOfHolidaysDTOToyear.containsKey(holidaysDTO.year)) {
                        mapOfHolidaysDTOToyear.put(holidaysDTO.year, new HashSet<>());
                    }
                    mapOfHolidaysDTOToyear.get(holidaysDTO.year).add(holidaysDTO);

                    if (!mapOfHolidaysDTOTodescription.containsKey(holidaysDTO.description)) {
                        mapOfHolidaysDTOTodescription.put(holidaysDTO.description, new HashSet<>());
                    }
                    mapOfHolidaysDTOTodescription.get(holidaysDTO.description).add(holidaysDTO);

                    if (!mapOfHolidaysDTOTolastModificationTime.containsKey(holidaysDTO.lastModificationTime)) {
                        mapOfHolidaysDTOTolastModificationTime.put(holidaysDTO.lastModificationTime, new HashSet<>());
                    }
                    mapOfHolidaysDTOTolastModificationTime.get(holidaysDTO.lastModificationTime).add(holidaysDTO);

                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public List<HolidaysDTO> getHolidaysList() {
        List<HolidaysDTO> holidayss = new ArrayList<HolidaysDTO>(this.mapOfHolidaysDTOToiD.values());
        return holidayss;
    }


    public HolidaysDTO getHolidaysDTOByID(long ID) {
        return mapOfHolidaysDTOToiD.get(ID);
    }


    public List<HolidaysDTO> getHolidaysDTOByholiday_date(long holiday_date) {
        return new ArrayList<>(mapOfHolidaysDTOToholidayDate.getOrDefault(holiday_date, new HashSet<>()));
    }


    public List<HolidaysDTO> getHolidaysDTOByyear(int year) {
        return new ArrayList<>(mapOfHolidaysDTOToyear.getOrDefault(year, new HashSet<>()));
    }


    public List<HolidaysDTO> getHolidaysDTOBydescription(String description) {
        return new ArrayList<>(mapOfHolidaysDTOTodescription.getOrDefault(description, new HashSet<>()));
    }


    public List<HolidaysDTO> getHolidaysDTOBylastModificationTime(long lastModificationTime) {
        return new ArrayList<>(mapOfHolidaysDTOTolastModificationTime.getOrDefault(lastModificationTime, new HashSet<>()));
    }


    @Override
    public String getTableName() {
        String tableName = "";
        try {
            tableName = "holidays";
        } catch (Exception ex) {
            logger.debug("FATAL", ex);
        }
        return tableName;
    }
}


