package holidays;

import util.CommonMaps;


public class HolidaysMAPS extends CommonMaps {
    public HolidaysMAPS(String tableName) {

        java_allfield_type_map.put("holiday_date".toLowerCase(), "Long");
        java_allfield_type_map.put("year".toLowerCase(), "Integer");
        java_allfield_type_map.put("description".toLowerCase(), "String");


        java_anyfield_search_map.put(tableName + ".holiday_date".toLowerCase(), "Long");
        java_anyfield_search_map.put(tableName + ".year".toLowerCase(), "Integer");
        java_anyfield_search_map.put(tableName + ".description".toLowerCase(), "String");

        java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
        java_DTO_map.put("holidayDate".toLowerCase(), "holidayDate".toLowerCase());
        java_DTO_map.put("year".toLowerCase(), "year".toLowerCase());
        java_DTO_map.put("description".toLowerCase(), "description".toLowerCase());
        java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

        java_SQL_map.put("holiday_date".toLowerCase(), "holidayDate".toLowerCase());
        java_SQL_map.put("year".toLowerCase(), "year".toLowerCase());
        java_SQL_map.put("description".toLowerCase(), "description".toLowerCase());

        java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
        java_Text_map.put("Holiday Date".toLowerCase(), "holidayDate".toLowerCase());
        java_Text_map.put("Year".toLowerCase(), "year".toLowerCase());
        java_Text_map.put("Description".toLowerCase(), "description".toLowerCase());
        java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

    }

}