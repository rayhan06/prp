package holidays;

import holiday.HolidayDAO;
import holiday.HolidayDTO;

import javax.annotation.PostConstruct;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

public class CalenderUtil {
    private static List<Date> yearlyHolidayMapping = new ArrayList<>();
    private final HolidayDAO holidaysDAO = new HolidayDAO();

    public static synchronized List<Date> getYearlyHolidayMapping() {
        return yearlyHolidayMapping;
    }

    public static synchronized void setYearlyHolidayMapping(List<Date> dates) {
        yearlyHolidayMapping = dates;
    }


    public static Date getDateBefore(Date fromDate, Integer workDays) {
        Date beforeDate = getDateAllochronically(fromDate, -workDays);
        Integer holidayCount = getHolidayCountBetweenDate(beforeDate, fromDate);
        fromDate = beforeDate;
        while (holidayCount > 0) {
            beforeDate = getDateAllochronically(fromDate, -holidayCount);
            holidayCount = getHolidayCountBetweenDate(beforeDate, fromDate);
            fromDate = beforeDate;
        }
        return fromDate;
    }

    public static Date getDateAfter(Date fromDate, Integer workDays) {
        Date afterDate = getDateAllochronically(fromDate, workDays);
        Integer holidayCount = getHolidayCountBetweenDate(fromDate, afterDate);
        fromDate = afterDate;
        while (holidayCount > 0) {
            afterDate = getDateAllochronically(fromDate, holidayCount);
            holidayCount = getHolidayCountBetweenDate(fromDate, afterDate);
            fromDate = afterDate;
        }
        return fromDate;
    }

    public static final Long getWorkDaysCountBefore(Date fromDate, Integer days) {
        Date finalDate = getDateBefore(fromDate, days);
        long diff = fromDate.getTime() - finalDate.getTime();
        return (diff / (1000 * 60 * 60 * 24));
    }

    public static Long getWorkDaysCountAfter(Date fromDate, Integer days) {
        Date finalDate = getDateAfter(fromDate, days);
        long diff = finalDate.getTime() - fromDate.getTime();
        return (diff / (1000 * 60 * 60 * 24));
    }

    public static Date getDateAllochronically(Date date, Integer count) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, count);
        return calendar.getTime();
    }

    public static Integer getHolidayCountBetweenDate(Date fromDate, Date toDate) {
        List<Date> holidays = getYearlyHolidayMapping();
        List<Date> sandwichDates = holidays.stream().filter(date -> date.after(fromDate) && date.before(toDate)).collect(Collectors.toList());
        return sandwichDates.size();
    }


    public List<Date> getHolidays() throws ParseException {
        ArrayList<Date> holidays = new ArrayList<>();
        List<HolidayDTO> holidaysDTOS = holidaysDAO.getAllHoliday(true);
        for (HolidayDTO holiday : holidaysDTOS) {
            Date date = new Date(holiday.currentDay);
            holidays.add(date);
        }
        return holidays;
    }

    @PostConstruct
    public void updateGovtHolidays() {
        List<Date> holidays = null;
        try {
            holidays = getHolidays();
            Collections.sort(holidays);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        setYearlyHolidayMapping(holidays);
    }
}
