package monthwise_nurse_report;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;
import login.LoginDTO;
import pbReport.ReportService;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.ReportRequestHandler;

@WebServlet("/Monthwise_nurse_report_Servlet")
public class Monthwise_nurse_report_Servlet  extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","action_date",">=","","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","action_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},			
		{"criteria","","nurse_action_cat","=","AND","int","","","any","nurseActionCat", LC.HM_ACTION + ""}		
	};
	
	String[][] Display =
	{
		{"display","","DATE_FORMAT(FROM_UNIXTIME(action_date / 1000),'%Y-%m')","text",""},		
		{"display","","nurse_action_cat","cat",""},		
		{"display","","COUNT(id)","text",""}		
	};
	
	String GroupBy = "col_0, nurse_action_cat";
	String OrderBY = "col_0 desc";
	
	ReportRequestHandler reportRequestHandler;
	
	public Monthwise_nurse_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "nurse_action_details";

		Display[0][4] = language.equalsIgnoreCase("english")?
	    		   "Year-Month"
	    		   :
	    		           "বছর-মাস";

		Display[1][4] = LM.getText(LC.HM_ACTION, loginDTO);
		Display[2][4] = LM.getText(LC.HM_COUNT, loginDTO);

		
	       String reportName = language.equalsIgnoreCase("english")?
	    		   "Monthwise Nurse Action Report"
	    		   :
	    		   "মাসভিত্তিক নাসের কাজের  রিপোর্ট";
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(2, ReportRequestHandler.RIGHT_ALIGN_INT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "monthwise_nurse_report",
				MenuConstants.MNA_REPORT, language, reportName, "monthwise_nurse_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
