package role_actions;
import java.util.*; 
import util.*;


public class ActionDescriptionMAPS extends CommonMaps
{	
	public ActionDescriptionMAPS(String tableName)
	{
		
		java_allfield_type_map.put("role_actions_id".toLowerCase(), "Long");
		java_allfield_type_map.put("name_en".toLowerCase(), "String");
		java_allfield_type_map.put("name_bn".toLowerCase(), "String");
		java_allfield_type_map.put("url".toLowerCase(), "String");


		java_anyfield_search_map.put(tableName + ".role_actions_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".name_en".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".url".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("roleActionsId".toLowerCase(), "roleActionsId".toLowerCase());
		java_DTO_map.put("nameEn".toLowerCase(), "nameEn".toLowerCase());
		java_DTO_map.put("nameBn".toLowerCase(), "nameBn".toLowerCase());
		java_DTO_map.put("url".toLowerCase(), "url".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("role_actions_id".toLowerCase(), "roleActionsId".toLowerCase());
		java_SQL_map.put("name_en".toLowerCase(), "nameEn".toLowerCase());
		java_SQL_map.put("name_bn".toLowerCase(), "nameBn".toLowerCase());
		java_SQL_map.put("url".toLowerCase(), "url".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Role Actions Id".toLowerCase(), "roleActionsId".toLowerCase());
		java_Text_map.put("English Name".toLowerCase(), "nameEn".toLowerCase());
		java_Text_map.put("Bangla Name".toLowerCase(), "nameBn".toLowerCase());
		java_Text_map.put("Url".toLowerCase(), "url".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}