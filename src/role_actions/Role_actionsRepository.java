package role_actions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Role_actionsRepository implements Repository {
	Role_actionsDAO role_actionsDAO = null;
	
	public void setDAO(Role_actionsDAO role_actionsDAO)
	{
		this.role_actionsDAO = role_actionsDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Role_actionsRepository.class);
	Map<Long, Role_actionsDTO>mapOfRole_actionsDTOToiD;
	Map<Long, Set<Role_actionsDTO> >mapOfRole_actionsDTOToroleType;
	Map<Long, Set<Role_actionsDTO> >mapOfRole_actionsDTOTolastModificationTime;


	static Role_actionsRepository instance = null;  
	private Role_actionsRepository(){
		mapOfRole_actionsDTOToiD = new ConcurrentHashMap<>();
		mapOfRole_actionsDTOToroleType = new ConcurrentHashMap<>();
		mapOfRole_actionsDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Role_actionsRepository getInstance(){
		if (instance == null){
			instance = new Role_actionsRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(role_actionsDAO == null)
		{
			return;
		}
		try {
			List<Role_actionsDTO> role_actionsDTOs = role_actionsDAO.getAllRole_actions(reloadAll);
			for(Role_actionsDTO role_actionsDTO : role_actionsDTOs) {
				Role_actionsDTO oldRole_actionsDTO = getRole_actionsDTOByID(role_actionsDTO.iD);
				if( oldRole_actionsDTO != null ) {
					mapOfRole_actionsDTOToiD.remove(oldRole_actionsDTO.iD);
				
					if(mapOfRole_actionsDTOToroleType.containsKey(oldRole_actionsDTO.roleType)) {
						mapOfRole_actionsDTOToroleType.get(oldRole_actionsDTO.roleType).remove(oldRole_actionsDTO);
					}
					if(mapOfRole_actionsDTOToroleType.get(oldRole_actionsDTO.roleType).isEmpty()) {
						mapOfRole_actionsDTOToroleType.remove(oldRole_actionsDTO.roleType);
					}
					
					if(mapOfRole_actionsDTOTolastModificationTime.containsKey(oldRole_actionsDTO.lastModificationTime)) {
						mapOfRole_actionsDTOTolastModificationTime.get(oldRole_actionsDTO.lastModificationTime).remove(oldRole_actionsDTO);
					}
					if(mapOfRole_actionsDTOTolastModificationTime.get(oldRole_actionsDTO.lastModificationTime).isEmpty()) {
						mapOfRole_actionsDTOTolastModificationTime.remove(oldRole_actionsDTO.lastModificationTime);
					}
					
					
				}
				if(role_actionsDTO.isDeleted == 0) 
				{
					
					mapOfRole_actionsDTOToiD.put(role_actionsDTO.iD, role_actionsDTO);
				
					if( ! mapOfRole_actionsDTOToroleType.containsKey(role_actionsDTO.roleType)) {
						mapOfRole_actionsDTOToroleType.put(role_actionsDTO.roleType, new HashSet<>());
					}
					mapOfRole_actionsDTOToroleType.get(role_actionsDTO.roleType).add(role_actionsDTO);
					
					if( ! mapOfRole_actionsDTOTolastModificationTime.containsKey(role_actionsDTO.lastModificationTime)) {
						mapOfRole_actionsDTOTolastModificationTime.put(role_actionsDTO.lastModificationTime, new HashSet<>());
					}
					mapOfRole_actionsDTOTolastModificationTime.get(role_actionsDTO.lastModificationTime).add(role_actionsDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Role_actionsDTO> getRole_actionsList() {
		List <Role_actionsDTO> role_actionss = new ArrayList<Role_actionsDTO>(this.mapOfRole_actionsDTOToiD.values());
		return role_actionss;
	}
	
	
	public Role_actionsDTO getRole_actionsDTOByID( long ID){
		return mapOfRole_actionsDTOToiD.get(ID);
	}
	
	
	public List<Role_actionsDTO> getRole_actionsDTOByrole_type(long role_type) {
		return new ArrayList<>( mapOfRole_actionsDTOToroleType.getOrDefault(role_type,new HashSet<>()));
	}
	
	
	public List<Role_actionsDTO> getRole_actionsDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfRole_actionsDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "role_actions";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


