package role_actions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Role_actionsDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public Role_actionsDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		joinSQL += " join role on " + tableName + ".role_type = role.ID ";

		commonMaps = new Role_actionsMAPS(tableName);
	}
	
	public Role_actionsDAO()
	{
		this("role_actions");		
	}
	
	
	
	public long add(CommonDTO commonDTO) throws Exception
	{
		
		Role_actionsDTO role_actionsDTO = (Role_actionsDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			role_actionsDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "role_type";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
		
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
			sql += ")";
				

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,role_actionsDTO.iD);
			ps.setObject(index++,role_actionsDTO.roleType);
			ps.setObject(index++,role_actionsDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return role_actionsDTO.iD;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Role_actionsDTO role_actionsDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				role_actionsDTO = new Role_actionsDTO();

				role_actionsDTO.iD = rs.getLong("ID");
				role_actionsDTO.roleType = rs.getLong("role_type");
				role_actionsDTO.isDeleted = rs.getInt("isDeleted");
				role_actionsDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
			
			ActionDescriptionDAO actionDescriptionDAO = new ActionDescriptionDAO("action_description");			
			List<ActionDescriptionDTO> actionDescriptionDTOList = actionDescriptionDAO.getActionDescriptionDTOListByRoleActionsID(role_actionsDTO.iD);
			role_actionsDTO.actionDescriptionDTOList = actionDescriptionDTOList;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return role_actionsDTO;
	}
	
	public long update(CommonDTO commonDTO) throws Exception
	{		
		Role_actionsDTO role_actionsDTO = (Role_actionsDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "role_type=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + role_actionsDTO.iD;
				

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,role_actionsDTO.roleType);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return role_actionsDTO.iD;
	}
	
	
	public List<Role_actionsDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Role_actionsDTO role_actionsDTO = null;
		List<Role_actionsDTO> role_actionsDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return role_actionsDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				role_actionsDTO = new Role_actionsDTO();
				role_actionsDTO.iD = rs.getLong("ID");
				role_actionsDTO.roleType = rs.getLong("role_type");
				role_actionsDTO.isDeleted = rs.getInt("isDeleted");
				role_actionsDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + role_actionsDTO);
				
				role_actionsDTOList.add(role_actionsDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return role_actionsDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Role_actionsDTO> getAllRole_actions (boolean isFirstReload)
    {
		List<Role_actionsDTO> role_actionsDTOList = new ArrayList<>();

		String sql = "SELECT * FROM role_actions";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by role_actions.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Role_actionsDTO role_actionsDTO = new Role_actionsDTO();
				role_actionsDTO.iD = rs.getLong("ID");
				role_actionsDTO.roleType = rs.getLong("role_type");
				role_actionsDTO.isDeleted = rs.getInt("isDeleted");
				role_actionsDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				role_actionsDTOList.add(role_actionsDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return role_actionsDTOList;
    }

	
	public List<Role_actionsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Role_actionsDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Role_actionsDTO> role_actionsDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Role_actionsDTO role_actionsDTO = new Role_actionsDTO();
				role_actionsDTO.iD = rs.getLong("ID");
				role_actionsDTO.roleType = rs.getLong("role_type");
				role_actionsDTO.isDeleted = rs.getInt("isDeleted");
				role_actionsDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				role_actionsDTOList.add(role_actionsDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return role_actionsDTOList;
	
	}
				
}
	