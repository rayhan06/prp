package role_actions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class ActionDescriptionDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public ActionDescriptionDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";

		commonMaps = new ActionDescriptionMAPS(tableName);
	}
	
	public ActionDescriptionDAO()
	{
		this("action_description");		
	}
	
	
	
	public long add(CommonDTO commonDTO) throws Exception
	{
		
		ActionDescriptionDTO actiondescriptionDTO = (ActionDescriptionDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			actiondescriptionDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "role_actions_id";
			sql += ", ";
			sql += "name_en";
			sql += ", ";
			sql += "name_bn";
			sql += ", ";
			sql += "url";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
		
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
			sql += ")";
				

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,actiondescriptionDTO.iD);
			ps.setObject(index++,actiondescriptionDTO.roleActionsId);
			ps.setObject(index++,actiondescriptionDTO.nameEn);
			ps.setObject(index++,actiondescriptionDTO.nameBn);
			ps.setObject(index++,actiondescriptionDTO.url);
			ps.setObject(index++,actiondescriptionDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return actiondescriptionDTO.iD;		
	}
		
	
	public void deleteActionDescriptionByRoleActionsID(long roleActionsID) throws Exception{
		
		
		Connection connection = null;
		Statement stmt = null;
		try{
			
			//String sql = "UPDATE action_description SET isDeleted=0 WHERE role_actions_id="+roleActionsID;
			String sql = "delete from action_description WHERE role_actions_id=" + roleActionsID;
			logger.debug("sql " + sql);
			connection = DBMW.getInstance().getConnection();
			stmt = connection.createStatement();
			stmt.execute(sql);
		}catch(Exception ex){
			logger.debug("fatal",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMW.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
	}		
   
	public List<ActionDescriptionDTO> getActionDescriptionDTOListByRoleActionsID(long roleActionsID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		ActionDescriptionDTO actiondescriptionDTO = null;
		List<ActionDescriptionDTO> actiondescriptionDTOList = new ArrayList<>();
		
		try{
			
			String sql = "SELECT * FROM action_description where isDeleted=0 and role_actions_id="+roleActionsID+" order by action_description.lastModificationTime";
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);

			while(rs.next()){
				actiondescriptionDTO = new ActionDescriptionDTO();
				actiondescriptionDTO.iD = rs.getLong("ID");
				actiondescriptionDTO.roleActionsId = rs.getLong("role_actions_id");
				actiondescriptionDTO.nameEn = rs.getString("name_en");
				actiondescriptionDTO.nameBn = rs.getString("name_bn");
				actiondescriptionDTO.url = rs.getString("url");
				actiondescriptionDTO.isDeleted = rs.getInt("isDeleted");
				actiondescriptionDTO.lastModificationTime = rs.getLong("lastModificationTime");
				actiondescriptionDTOList.add(actiondescriptionDTO);

			}			
			
		}catch(Exception ex){
			logger.debug("fatal",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return actiondescriptionDTOList;
	}

	//need another getter for repository
	public ActionDescriptionDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		ActionDescriptionDTO actiondescriptionDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				actiondescriptionDTO = new ActionDescriptionDTO();

				actiondescriptionDTO.iD = rs.getLong("ID");
				actiondescriptionDTO.roleActionsId = rs.getLong("role_actions_id");
				actiondescriptionDTO.nameEn = rs.getString("name_en");
				actiondescriptionDTO.nameBn = rs.getString("name_bn");
				actiondescriptionDTO.url = rs.getString("url");
				actiondescriptionDTO.isDeleted = rs.getInt("isDeleted");
				actiondescriptionDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return actiondescriptionDTO;
	}
	
	public long update(CommonDTO commonDTO) throws Exception
	{		
		ActionDescriptionDTO actiondescriptionDTO = (ActionDescriptionDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "role_actions_id=?";
			sql += ", ";
			sql += "name_en=?";
			sql += ", ";
			sql += "name_bn=?";
			sql += ", ";
			sql += "url=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + actiondescriptionDTO.iD;
				

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,actiondescriptionDTO.roleActionsId);
			ps.setObject(index++,actiondescriptionDTO.nameEn);
			ps.setObject(index++,actiondescriptionDTO.nameBn);
			ps.setObject(index++,actiondescriptionDTO.url);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return actiondescriptionDTO.iD;
	}
	
	
	public List<ActionDescriptionDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		ActionDescriptionDTO actiondescriptionDTO = null;
		List<ActionDescriptionDTO> actiondescriptionDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return actiondescriptionDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				actiondescriptionDTO = new ActionDescriptionDTO();
				actiondescriptionDTO.iD = rs.getLong("ID");
				actiondescriptionDTO.roleActionsId = rs.getLong("role_actions_id");
				actiondescriptionDTO.nameEn = rs.getString("name_en");
				actiondescriptionDTO.nameBn = rs.getString("name_bn");
				actiondescriptionDTO.url = rs.getString("url");
				actiondescriptionDTO.isDeleted = rs.getInt("isDeleted");
				actiondescriptionDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + actiondescriptionDTO);
				
				actiondescriptionDTOList.add(actiondescriptionDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return actiondescriptionDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<ActionDescriptionDTO> getAllActionDescription (boolean isFirstReload)
    {
		List<ActionDescriptionDTO> actiondescriptionDTOList = new ArrayList<>();

		String sql = "SELECT * FROM action_description";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by actiondescription.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				ActionDescriptionDTO actiondescriptionDTO = new ActionDescriptionDTO();
				actiondescriptionDTO.iD = rs.getLong("ID");
				actiondescriptionDTO.roleActionsId = rs.getLong("role_actions_id");
				actiondescriptionDTO.nameEn = rs.getString("name_en");
				actiondescriptionDTO.nameBn = rs.getString("name_bn");
				actiondescriptionDTO.url = rs.getString("url");
				actiondescriptionDTO.isDeleted = rs.getInt("isDeleted");
				actiondescriptionDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				actiondescriptionDTOList.add(actiondescriptionDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return actiondescriptionDTOList;
    }

	
	public List<ActionDescriptionDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<ActionDescriptionDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<ActionDescriptionDTO> actiondescriptionDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				ActionDescriptionDTO actiondescriptionDTO = new ActionDescriptionDTO();
				actiondescriptionDTO.iD = rs.getLong("ID");
				actiondescriptionDTO.roleActionsId = rs.getLong("role_actions_id");
				actiondescriptionDTO.nameEn = rs.getString("name_en");
				actiondescriptionDTO.nameBn = rs.getString("name_bn");
				actiondescriptionDTO.url = rs.getString("url");
				actiondescriptionDTO.isDeleted = rs.getInt("isDeleted");
				actiondescriptionDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				actiondescriptionDTOList.add(actiondescriptionDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return actiondescriptionDTOList;
	
	}
				
}
	