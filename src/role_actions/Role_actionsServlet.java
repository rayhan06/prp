package role_actions;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;

import role_actions.Constants;




import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Role_actionsServlet
 */
@WebServlet("/Role_actionsServlet")
@MultipartConfig
public class Role_actionsServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Role_actionsServlet.class);

    String tableName = "role_actions";

	Role_actionsDAO role_actionsDAO;
	CommonRequestHandler commonRequestHandler;
	ActionDescriptionDAO actionDescriptionDAO;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Role_actionsServlet() 
	{
        super();
    	try
    	{
			role_actionsDAO = new Role_actionsDAO(tableName);
			actionDescriptionDAO = new ActionDescriptionDAO("action_description");
			commonRequestHandler = new CommonRequestHandler(role_actionsDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ROLE_ACTIONS_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ROLE_ACTIONS_ADD))
				{
					getRole_actions(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
									
			}			
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ROLE_ACTIONS_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchRole_actions(request, response, isPermanentTable, filter);
						}
						else
						{
							searchRole_actions(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchRole_actions(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ROLE_ACTIONS_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ROLE_ACTIONS_ADD))
				{
					System.out.println("going to  addRole_actions ");
					addRole_actions(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addRole_actions ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ROLE_ACTIONS_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addRole_actions ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ROLE_ACTIONS_ADD))
				{					
					addRole_actions(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{								
				deleteRole_actions(request, response, userDTO);				
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ROLE_ACTIONS_SEARCH))
				{
					searchRole_actions(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Role_actionsDTO role_actionsDTO = (Role_actionsDTO)role_actionsDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(role_actionsDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addRole_actions(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addRole_actions");
			String path = getServletContext().getRealPath("/img2/");
			Role_actionsDTO role_actionsDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				role_actionsDTO = new Role_actionsDTO();
			}
			else
			{
				role_actionsDTO = (Role_actionsDTO)role_actionsDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("roleType");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("roleType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				role_actionsDTO.roleType = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addRole_actions dto = " + role_actionsDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				role_actionsDAO.setIsDeleted(role_actionsDTO.iD, CommonDTO.OUTDATED);
				returnedID = role_actionsDAO.add(role_actionsDTO);
				role_actionsDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = role_actionsDAO.manageWriteOperations(role_actionsDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = role_actionsDAO.manageWriteOperations(role_actionsDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			List<ActionDescriptionDTO> actionDescriptionDTOList = createActionDescriptionDTOListByRequest(request);			
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(actionDescriptionDTOList != null)
				{				
					for(ActionDescriptionDTO actionDescriptionDTO: actionDescriptionDTOList)
					{
						actionDescriptionDTO.roleActionsId = role_actionsDTO.iD; 
						actionDescriptionDAO.add(actionDescriptionDTO);
					}
				}
			
			}
			else
			{
				List<Long> childIdsFromRequest = actionDescriptionDAO.getChildIdsFromRequest(request, "actionDescription");
				//delete the removed children
				actionDescriptionDAO.deleteChildrenNotInList("role_actions", "action_description", role_actionsDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = actionDescriptionDAO.getChilIds("role_actions", "action_description", role_actionsDTO.iD);
				
				
				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							ActionDescriptionDTO actionDescriptionDTO =  createActionDescriptionDTOByRequestAndIndex(request, false, i);
							actionDescriptionDTO.roleActionsId = role_actionsDTO.iD; 
							actionDescriptionDAO.update(actionDescriptionDTO);
						}
						else
						{
							ActionDescriptionDTO actionDescriptionDTO =  createActionDescriptionDTOByRequestAndIndex(request, true, i);
							actionDescriptionDTO.roleActionsId = role_actionsDTO.iD; 
							actionDescriptionDAO.add(actionDescriptionDTO);
						}
					}
				}
				else
				{
					actionDescriptionDAO.deleteActionDescriptionByRoleActionsID(role_actionsDTO.iD);
				}
				
			}
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getRole_actions(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Role_actionsServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(role_actionsDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	private List<ActionDescriptionDTO> createActionDescriptionDTOListByRequest(HttpServletRequest request) throws Exception{ 
		List<ActionDescriptionDTO> actionDescriptionDTOList = new ArrayList<ActionDescriptionDTO>();
		if(request.getParameterValues("actionDescription.iD") != null) 
		{
			int actionDescriptionItemNo = request.getParameterValues("actionDescription.iD").length;
			
			
			for(int index=0;index<actionDescriptionItemNo;index++){
				ActionDescriptionDTO actionDescriptionDTO = createActionDescriptionDTOByRequestAndIndex(request,true,index);
				actionDescriptionDTOList.add(actionDescriptionDTO);
			}
			
			return actionDescriptionDTOList;
		}
		return null;
	}
	
	
	private ActionDescriptionDTO createActionDescriptionDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{
	
		ActionDescriptionDTO actionDescriptionDTO;
		if(addFlag == true )
		{
			actionDescriptionDTO = new ActionDescriptionDTO();
		}
		else
		{
			actionDescriptionDTO = actionDescriptionDAO.getDTOByID(Long.parseLong(request.getParameterValues("actionDescription.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		
				
		String Value = "";
		Value = request.getParameterValues("actionDescription.roleActionsId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		actionDescriptionDTO.roleActionsId = Long.parseLong(Value);
		Value = request.getParameterValues("actionDescription.nameEn")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		actionDescriptionDTO.nameEn = (Value);
		Value = request.getParameterValues("actionDescription.nameBn")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		actionDescriptionDTO.nameBn = (Value);
		Value = request.getParameterValues("actionDescription.url")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText()).replaceAll("&amp;", "&");
		}

		actionDescriptionDTO.url = (Value);
		return actionDescriptionDTO;
	
	}
	
	
	

	private void deleteRole_actions(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				
				Role_actionsDTO role_actionsDTO = (Role_actionsDTO)role_actionsDAO.getDTOByID(id);
				role_actionsDAO.manageWriteOperations(role_actionsDTO, SessionConstants.DELETE, id, userDTO);
				actionDescriptionDAO.deleteActionDescriptionByRoleActionsID(id);
				response.sendRedirect("Role_actionsServlet?actionType=search");
				
			}			
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		
	}

	private void getRole_actions(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getRole_actions");
		Role_actionsDTO role_actionsDTO = null;
		try 
		{
			role_actionsDTO = (Role_actionsDTO)role_actionsDAO.getDTOByID(id);
			request.setAttribute("ID", role_actionsDTO.iD);
			request.setAttribute("role_actionsDTO",role_actionsDTO);
			request.setAttribute("role_actionsDAO",role_actionsDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "role_actions/role_actionsInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "role_actions/role_actionsSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "role_actions/role_actionsEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "role_actions/role_actionsEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getRole_actions(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getRole_actions(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchRole_actions(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchRole_actions 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_ROLE_ACTIONS,
			request,
			role_actionsDAO,
			SessionConstants.VIEW_ROLE_ACTIONS,
			SessionConstants.SEARCH_ROLE_ACTIONS,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("role_actionsDAO",role_actionsDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to role_actions/role_actionsApproval.jsp");
	        	rd = request.getRequestDispatcher("role_actions/role_actionsApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to role_actions/role_actionsApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("role_actions/role_actionsApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to role_actions/role_actionsSearch.jsp");
	        	rd = request.getRequestDispatcher("role_actions/role_actionsSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to role_actions/role_actionsSearchForm.jsp");
	        	rd = request.getRequestDispatcher("role_actions/role_actionsSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

