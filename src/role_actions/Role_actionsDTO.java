package role_actions;
import java.util.*; 
import util.*; 


public class Role_actionsDTO extends CommonDTO
{

	public long roleType = 0;
	
	public List<ActionDescriptionDTO> actionDescriptionDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Role_actionsDTO[" +
            " iD = " + iD +
            " roleType = " + roleType +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}