package role_actions;
import java.util.*; 
import util.*;


public class Role_actionsMAPS extends CommonMaps
{	
	public Role_actionsMAPS(String tableName)
	{
		
		java_allfield_type_map.put("role_type".toLowerCase(), "Long");


		java_anyfield_search_map.put("role.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("role.name_bn".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("roleType".toLowerCase(), "roleType".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("role_type".toLowerCase(), "roleType".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Role".toLowerCase(), "roleType".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}