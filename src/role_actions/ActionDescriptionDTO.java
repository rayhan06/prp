package role_actions;
import java.util.*; 
import util.*; 


public class ActionDescriptionDTO extends CommonDTO
{

	public long roleActionsId = 0;
    public String nameEn = "";
    public String nameBn = "";
    public String url = "";
	
	public List<ActionDescriptionDTO> actionDescriptionDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$ActionDescriptionDTO[" +
            " iD = " + iD +
            " roleActionsId = " + roleActionsId +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " url = " + url +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}