package feedback_question_training_calendar_mapping;

/*
 * @author Md. Erfan Hossain
 * @created 11/03/2021 - 4:23 PM
 * @project parliament
 */

import util.CommonDTO;

public class FeedbackQuestionTrainingCalenderMappingDTO extends CommonDTO {
    public long feedbackQuestionId;
    public long trainingCalendarId;
    public int sequence;
    public long insertionTime;
    public String insertBy;
    public String modifiedBy;
}
