package feedback_question_training_calendar_mapping;

import common.BaseServlet;
import common.CommonDAOService;
import login.LoginDTO;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/FeedbackQuestionTrainingCalenderMappingServlet")
@MultipartConfig
public class FeedbackQuestionTrainingCalenderMappingServlet extends BaseServlet {
    private static final Logger logger = Logger.getLogger(FeedbackQuestionTrainingCalenderMappingServlet.class);

    private final FeedbackQuestionTrainingCalenderMappingDAO dao = FeedbackQuestionTrainingCalenderMappingDAO.getInstance();

    @Override
    public String getTableName() {
        return "tran_cal_fb_ques_mapping";
    }

    @Override
    public String getServletName() {
        return "FeedbackQuestionTrainingCalenderMappingServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return null;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return FeedbackQuestionTrainingCalenderMappingServlet.class;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        request.getRequestDispatcher("feedback_question_training_calendar_mapping/FeedBackQuestionTrainingCalendarMapping.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        long trainingCalendarId = Long.parseLong(request.getParameter("training_calendar_id"));
        List<FeedbackQuestionTrainingCalenderMappingDTO> oldDTOList = dao.getByTrainingId(trainingCalendarId);
        String questionIds = request.getParameter("question_ids");
        if (questionIds == null && (oldDTOList == null || oldDTOList.size() == 0)) {
            response.sendRedirect("Training_calenderServlet?actionType=search");
            return;
        }
        List<Long> questionIdList;
        if (questionIds != null) {
            questionIdList = Arrays.stream(questionIds.split(","))
                    .filter(x -> x.length() > 0)
                    .map(Long::parseLong)
                    .collect(Collectors.toList());
        } else {
            questionIdList = new ArrayList<>();
        }

        try {
            if (oldDTOList == null || oldDTOList.size() == 0) {
                dao.addList(questionIdList, trainingCalendarId, userDTO.userName);
            } else {
                dao.updateList(questionIdList, oldDTOList, trainingCalendarId, userDTO.userName);
            }
            response.sendRedirect("Training_calenderServlet?actionType=search");
        } catch (Exception e) {
            logger.error(e);
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
        }
    }
}
