package feedback_question_training_calendar_mapping;


import common.ConnectionAndStatementUtil;
import dbm.DBMW;
import org.apache.log4j.Logger;
import pb.Utils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FeedbackQuestionTrainingCalenderMappingDAO {
    public static final Logger logger = Logger.getLogger(FeedbackQuestionTrainingCalenderMappingDAO.class);

    public static final String addSqlQuery = "INSERT INTO tran_cal_fb_ques_mapping (feedback_question_id,training_calendar_id,sequence," +
            "modified_by,last_modification_time,insert_by,insertion_date,isDeleted,id) values (?,?,?,?,?,?,?,?,?)";

    public static final String updateSqlQuery = "UPDATE tran_cal_fb_ques_mapping SET feedback_question_id = ?, training_calendar_id = ?," +
            "sequence = ?,modified_by = ?, last_modification_time = ? WHERE id = ?";

    public static final String getByTrainingId = "SELECT * FROM tran_cal_fb_ques_mapping WHERE isDeleted = 0 AND training_calendar_id = %d order by sequence";

    public static final String deleteSqlQuery = "UPDATE tran_cal_fb_ques_mapping SET isDeleted = 1 WHERE id IN (%s)";

    private FeedbackQuestionTrainingCalenderMappingDAO() {

    }

    private static class LazyLoader {
        static final FeedbackQuestionTrainingCalenderMappingDAO INSTANCE = new FeedbackQuestionTrainingCalenderMappingDAO();
    }

    public static FeedbackQuestionTrainingCalenderMappingDAO getInstance() {
        return FeedbackQuestionTrainingCalenderMappingDAO.LazyLoader.INSTANCE;
    }


    public void set(PreparedStatement ps, FeedbackQuestionTrainingCalenderMappingDTO dto, boolean isInsert) throws Exception {
        int index = 0;
        ps.setObject(++index, dto.feedbackQuestionId);
        ps.setObject(++index, dto.trainingCalendarId);
        ps.setObject(++index, dto.sequence);
        ps.setObject(++index, dto.modifiedBy);
        ps.setObject(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, dto.insertBy);
            ps.setObject(++index, dto.insertionTime);
            ps.setObject(++index, 0);
            dto.iD = DBMW.getInstance().getNextSequenceId("tran_cal_fb_ques_mapping");
        }
        ps.setObject(++index, dto.iD);
    }


    public FeedbackQuestionTrainingCalenderMappingDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            FeedbackQuestionTrainingCalenderMappingDTO dto = new FeedbackQuestionTrainingCalenderMappingDTO();
            dto.feedbackQuestionId = rs.getLong("feedback_question_id");
            dto.trainingCalendarId = rs.getLong("training_calendar_id");
            dto.sequence = rs.getInt("sequence");
            dto.modifiedBy = rs.getString("modified_by");
            dto.lastModificationTime = rs.getLong("last_modification_time");
            dto.insertBy = rs.getString("insert_by");
            dto.insertionTime = rs.getLong("insertion_date");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.iD = rs.getLong("id");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public List<FeedbackQuestionTrainingCalenderMappingDTO> getByTrainingId(long trainingCalendarId) {
        return ConnectionAndStatementUtil.getListOfT(String.format(getByTrainingId, trainingCalendarId), this::buildObjectFromResultSet);
    }

    private void add(FeedbackQuestionTrainingCalenderMappingDTO dto) {
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                set(ps, dto, true);
                logger.debug(ps);
                ps.execute();
            } catch (Exception e) {
                logger.error(e);
            }
        }, addSqlQuery);
    }

    private void update(FeedbackQuestionTrainingCalenderMappingDTO dto) {
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                set(ps, dto, false);
                logger.debug(ps);
                ps.executeUpdate();
            } catch (Exception e) {
                logger.error(e);
            }
        }, updateSqlQuery);
    }


    public void addList(List<Long> feedbackQuestionIdList, long trainingCalendarId, String userName) throws Exception {
        if (!Utils.isValidUserName(userName)) {
            return;
        }
        if (feedbackQuestionIdList == null || feedbackQuestionIdList.size() == 0) {
            return;
        }
        long requestTime = System.currentTimeMillis();
        for (int i = 0; i < feedbackQuestionIdList.size(); i++) {
            FeedbackQuestionTrainingCalenderMappingDTO dto = buildDTO(feedbackQuestionIdList.get(i), trainingCalendarId, userName, requestTime, i + 1);
            add(dto);
        }
    }

    public void updateList(List<Long> latestFeedbackQuestionIdList, List<FeedbackQuestionTrainingCalenderMappingDTO> oldDTOList, long trainingCalendarId, String userName) throws Exception {
        if (!Utils.isValidUserName(userName)) {
            return;
        }
        Map<Long, FeedbackQuestionTrainingCalenderMappingDTO> mapByQuestionId = oldDTOList.stream()
                .collect(Collectors.toMap(dto -> dto.feedbackQuestionId, dto -> dto));
        List<Long> oldFeedbackQuestionsIdList = new ArrayList<>(mapByQuestionId.keySet());
        List<Long> deletedList = Utils.getFilteredIdList(oldFeedbackQuestionsIdList, questionId -> !latestFeedbackQuestionIdList.contains(questionId));
        if (deletedList != null && deletedList.size() > 0) {
            String ids = deletedList.stream()
                    .map(id -> mapByQuestionId.get(id).iD)
                    .map(String::valueOf)
                    .collect(Collectors.joining(","));
            ConnectionAndStatementUtil.getWriteStatement(st -> {
                String sql = String.format(deleteSqlQuery, ids);
                try {
                    st.executeUpdate(sql);
                } catch (SQLException ex) {
                    logger.error(ex);
                }
            });
        }
        long requestTime = System.currentTimeMillis();
        for (int i = 0; i < latestFeedbackQuestionIdList.size(); i++) {
            FeedbackQuestionTrainingCalenderMappingDTO dto = mapByQuestionId.get(latestFeedbackQuestionIdList.get(i));
            if (dto == null) {
                add(buildDTO(latestFeedbackQuestionIdList.get(i), trainingCalendarId, userName, requestTime, i + 1));
            } else {
                dto.sequence = i + 1;
                dto.modifiedBy = userName;
                dto.lastModificationTime = requestTime;
                update(dto);
            }
        }
    }

    private FeedbackQuestionTrainingCalenderMappingDTO buildDTO(Long feedbackQuestionId, long trainingCalendarId, String userName, long requestTime, int sequence) {
        if (!Utils.isValidUserName(userName)) {
            return null;
        }
        FeedbackQuestionTrainingCalenderMappingDTO dto = new FeedbackQuestionTrainingCalenderMappingDTO();
        dto.trainingCalendarId = trainingCalendarId;
        dto.feedbackQuestionId = feedbackQuestionId;
        dto.insertBy = userName;
        dto.modifiedBy = userName;
        dto.insertionTime = requestTime;
        dto.lastModificationTime = requestTime;
        dto.sequence = sequence;
        return dto;
    }
}
