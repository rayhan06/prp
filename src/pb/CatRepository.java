package pb;

import category.CategoryDTO;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

public class CatRepository implements Repository {
    private static final Logger logger = Logger.getLogger(CatRepository.class);
    private final CatDAO catDAO = new CatDAO();
    private final Map<String, Map<Integer, CategoryLanguageModel>> map = new ConcurrentHashMap<>();
    
    public static final int DEFAULT_SORT = 0;
    public static final int SORT_BY_NAMEEN = 1;
    public static final int SORT_BY_ORDERING = 2;

    private final Map<String, List<CategoryLanguageModel>> mapSortedListByCategoryValue = new ConcurrentHashMap<>();

    private CatRepository() {
        RepositoryManager.getInstance().addRepository(this);
    }

    public static CatRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    private static class LazyLoader {
        static final CatRepository INSTANCE = new CatRepository();
    }

    @Override
    public String getTableName() {
        return "category";
    }

    @Override
    public synchronized void reload(boolean reloadAll) {
        logger.debug("CatRepository reload is started for reloadAll = " + reloadAll);
        List<CategoryLanguageModel> list = catDAO.getCategoryLanguageModelList(reloadAll);
        doCaching(list);
        logger.debug("CatRepository reload has been done for reloadAll = " + reloadAll);
    }

    @Override
    public void reloadWithExactModificationTime(long time) {
        List<CategoryLanguageModel> list = catDAO.getCategoryLanguageModelByLastModificationTime(time);
        doCaching(list);
    }

    private void doCaching(List<CategoryLanguageModel> list) {
        if(list !=null && list.size()>0){
            list.stream()
                    .peek(this::removeModel)
                    .filter(model -> model.isDeleted == 0)
                    .forEach(this::addToMap);
            list.stream()
                    .map(model -> model.domainName)
                    .collect(Collectors.toSet())
                    .forEach(this::putToSortedList);
        }
    }

    private void putToSortedList(String domainName) {
        if (map.get(domainName) != null && map.get(domainName).values().size() > 0) {
            List<CategoryLanguageModel> models = new ArrayList<>(map.get(domainName).values());
            models.sort(Comparator.comparing(o -> o.categoryValue));
            mapSortedListByCategoryValue.put(domainName, models);
        }
    }

    private void addToMap(CategoryLanguageModel model) {
        Map<Integer, CategoryLanguageModel> modelMap = map.getOrDefault(model.domainName, new ConcurrentHashMap<>());
        modelMap.put(model.categoryValue, model);
        map.put(model.domainName, modelMap);
    }

    private void removeModel(CategoryLanguageModel model) {
        if (map.get(model.domainName) != null && map.get(model.domainName).get(model.categoryValue) != null) {
            map.get(model.domainName).remove(model.categoryValue);
        }
    }

    public CategoryLanguageModel getCategoryLanguageModel(String domainName, Integer value) {
        if(value == null){
            return null;
        }
        if (value > 0 && (map.get(domainName) == null || map.get(domainName).get(value) == null)) {
            synchronized (this) {
                if (map.get(domainName) == null || map.get(domainName).get(value) == null) {
                    CategoryLanguageModel model = catDAO.getCategoryLanguageModel(domainName, value);
                    if (model != null) {
                        Map<Integer, CategoryLanguageModel> modelMap = map.getOrDefault(model.domainName, new ConcurrentHashMap<>());
                        modelMap.put(model.categoryValue, model);
                        map.put(model.domainName, modelMap);
                        putToSortedList(domainName);
                    }
                }
            }
        }
        if (map.get(domainName) != null) {
            return map.get(domainName).get(value);
        } else {
            return null;
        }

    }

    public String getText(String language, String domainName, long value) {
        return getText(language, domainName, (int) value);
    }

    public String getText(String language, String domainName, int value) {
         return getText(language.equalsIgnoreCase("English"),domainName,value);
    }

    public String getText(boolean isLangEng, String domainName, int value) {
        CategoryLanguageModel model = getCategoryLanguageModel(domainName, value);
        return model == null ? "" :
                (isLangEng ? (model.englishText == null ? "" : model.englishText)
                        : (model.banglaText) == null ? "" : model.banglaText);
    }

    public List<CategoryLanguageModel> getCategoryLanguageModelList(String domainName) {
        if (map.get(domainName) == null) {
            synchronized (this) {
                if (map.get(domainName) == null) {
                    List<CategoryLanguageModel> list = catDAO.getCategoryLanguageModelListByDomainName(domainName);
                    if (list == null || list.size() == 0) {
                        return new ArrayList<>();
                    }
                    map.put(domainName, list.stream().collect(Collectors.toMap(e -> e.categoryValue, e -> e)));
                    mapSortedListByCategoryValue.put(domainName, list);
                    return list;
                }
            }
        }
        return mapSortedListByCategoryValue.get(domainName);
    }
    
    public List<CategoryLanguageModel> getCategoryLanguageModelListANdParentValue(String domainName, int parentVal) {
    	List<CategoryLanguageModel> cats = getCategoryLanguageModelList(domainName);
    	for(CategoryLanguageModel cat: cats)
    	{
    		if(cat.parentDomainVal != parentVal)
    		{
    			cats.remove(cat);
    		}
    	}
    	return cats;
    }


    //------------- getCategoryDTOList function from CatDAO similar implementation starts--------------------------------

    public List<CategoryDTO> getCategoryDTOList(String domainName) {
        List<CategoryLanguageModel> categoryLanguageModelList = getCategoryLanguageModelList(domainName);
        return categoryLanguageModelList
                .stream()
                .map(model -> {
                    CategoryDTO catDTO = new CategoryDTO();
                    catDTO.value = model.categoryValue;
                    catDTO.domainName = domainName;
                    catDTO.nameBn = model.banglaText;
                    catDTO.nameEn = model.englishText;
                    catDTO.languageId = model.languageId;
                    catDTO.ordering = model.ordering;
                    catDTO.parentDomain =  model.parentDomain;
                    catDTO.parentDomainVal = model.parentDomainVal;
                    catDTO.iD = model.id;
                    return catDTO;
                })
                .collect(Collectors.toList());
    }

    public CategoryDTO getCategoryDTOListByValue(String domainName, int value) {
        List<CategoryLanguageModel> categoryLanguageModelList = getCategoryLanguageModelList(domainName);
        return categoryLanguageModelList
                .stream()
                .filter(model -> model.categoryValue == value)
                .map(model -> {
                    CategoryDTO catDTO = new CategoryDTO();
                    catDTO.value = model.categoryValue;
                    catDTO.domainName = domainName;
                    catDTO.nameBn = model.banglaText;
                    catDTO.nameEn = model.englishText;
                    catDTO.languageId = model.languageId;
                    catDTO.ordering = model.ordering;
                    catDTO.parentDomain =  model.parentDomain;
                    catDTO.parentDomainVal = model.parentDomainVal;
                    catDTO.iD = model.id;
                    return catDTO;
                })
                .findAny()
                .orElse(null);
    }

    //------------- getCategoryDTOList function from CatDAO similar implementation ends--------------------------------


    //------------- getOptions function from CatDAO similar implementation starts--------------------------------

    public static String getOptions(String Language, String TableName, long DefaultValue) {
        return getOptions(Language, TableName, (int) DefaultValue);
    }
    
    public static String getOptions(String language, String domainName, int defaultValue, int sortingType) {
        logger.debug("language : " + language + "  domainName : " + domainName + "  defaultValue : " + defaultValue);
        String smLanguage = language.toLowerCase();
        if (!smLanguage.equalsIgnoreCase("bangla") && !smLanguage.equalsIgnoreCase("english")) {
            language = "bangla";
        }
        if (!domainName.matches("^[a-zA-Z0-9_]*$") || domainName.length() > 50) {
            return "";
        }
        List<CatDTO> catDTOs = getDTOs(domainName);
        if(sortingType == SORT_BY_NAMEEN)
        {
        	catDTOs.sort(Comparator.comparing(CatDTO::getNameEn));
        }
        else if(sortingType == SORT_BY_ORDERING)
        {
        	catDTOs.sort(Comparator.comparing(CatDTO::getOrdering));
        }
        

        String sOptions = "";
        if (defaultValue == CatDTO.CATDEFAULT) {
            sOptions += getOption(null, language);
        } else if (defaultValue == CatDTO.CATDEFAULTNOTREQUIRED) {
            if (language.equalsIgnoreCase("English")) {
                sOptions += "<option value = '" + CatDTO.CATDEFAULTNOTREQUIRED + "'>Select</option>";
            } else {
                sOptions += "<option value = '" + CatDTO.CATDEFAULTNOTREQUIRED + "'>বাছাই করুন</option>";
            }
        }

        for (int i = 0; i < catDTOs.size(); i++) {
            CatDTO catDto = catDTOs.get(i);
            sOptions += getOption(catDto, language);
            if (defaultValue >= 0 && catDto.value == defaultValue) {
                int index = sOptions.lastIndexOf("<option");
                String s1 = sOptions.substring(0, index);
                String s2 = sOptions.substring(index);
                s2 = s2.replaceFirst("<option", "<option selected ");
                sOptions = s1 + s2;
            }
        }

        return sOptions;
    }

    public static String getOptions(String language, String domainName, int defaultValue) {
        return getOptions(language,  domainName,  defaultValue, DEFAULT_SORT);
    }

    public static List<CatDTO> getDTOs(String domainName) {
        logger.debug("domainName : " + domainName);
        List<CatDTO> catDTOS = new ArrayList<>();
        List<CategoryLanguageModel> categoryLanguageModels = CatRepository.getInstance().getCategoryLanguageModelList(domainName);
        categoryLanguageModels
                .forEach(model -> {
                    CatDTO catDTO = new CatDTO();
                    catDTO.value = model.categoryValue;
                    catDTO.domainName = domainName;
                    catDTO.languageTextBangla = model.banglaText;
                    catDTO.languageTextEnglish = model.englishText;
                    catDTO.ordering = model.ordering;
                    catDTO.nameEn = catDTO.languageTextEnglish;
                    catDTO.nameBn = catDTO.languageTextBangla;
                    catDTOS.add(catDTO);
                });
        return catDTOS;
    }
    
    public static List<CatDTO> getDTOs(String domainName, int sortingType) {
        logger.debug("domainName : " + domainName);
        List<CatDTO> catDTOS = new ArrayList<>();
        List<CategoryLanguageModel> categoryLanguageModels = CatRepository.getInstance().getCategoryLanguageModelList(domainName);
        categoryLanguageModels
                .forEach(model -> {
                    CatDTO catDTO = new CatDTO();
                    catDTO.value = model.categoryValue;
                    catDTO.domainName = domainName;
                    catDTO.languageTextBangla = model.banglaText;
                    catDTO.languageTextEnglish = model.englishText;
                    catDTO.ordering = model.ordering;
                    catDTO.nameEn = catDTO.languageTextEnglish;
                    catDTO.nameBn = catDTO.languageTextBangla;
                    
                    catDTOS.add(catDTO);
                });
        if(sortingType == SORT_BY_NAMEEN)
        {
        	catDTOS.sort(Comparator.comparing(CatDTO::getNameEn));
        }
        else if(sortingType == SORT_BY_ORDERING)
        {
        	catDTOS.sort(Comparator.comparing(CatDTO::getOrdering));
        }
        return catDTOS;
    }

    public static String getOption(CatDTO catDTO, String Language) {
        String sOption;
        if (catDTO != null) {
            sOption = "<option value = '" + catDTO.value + "'>";
            if (Language.equals("English")) {
                sOption += catDTO.languageTextEnglish;
            } else {
                sOption += catDTO.languageTextBangla;
            }
            sOption += "</option>";
        } else {
            if (Language.equals("English")) {
                sOption = "<option value = ''>Select</option>";
            } else {
                sOption = "<option value = ''>বাছাই করুন</option>";
            }
        }

        return sOption;
    }

    //------------- getOptions function from CatDAO similar implementation ends--------------------------------


    //------------- getName function from CatDAO similar implementation starts--------------------------------


    public static String getName(String Language, String domainName, int Value) {
        CatDTO catDTO = getDTO(domainName, Value);
        return catDTO == null ? "" : ("Bangla".equalsIgnoreCase(Language) ? catDTO.languageTextBangla : catDTO.languageTextEnglish);
    }

    public static String getName(String Language, String domainName, long value) {
        return getName(Language, domainName, Long.valueOf(value).intValue());
    }

    public static CatDTO getDTO(String domainName, int value) {
        //logger.debug("domainName : " + domainName + "  value : " + value);
        CategoryLanguageModel model = CatRepository.getInstance().getCategoryLanguageModel(domainName, value);
        if(model == null)
        {
        	return null;
        }
        CatDTO catDTO = new CatDTO();
        catDTO.value = model.categoryValue;
        catDTO.domainName = domainName;
        catDTO.languageTextBangla = model.banglaText;
        catDTO.languageTextEnglish = model.englishText;
        catDTO.ordering = model.ordering;
        catDTO.nameEn = catDTO.languageTextEnglish;
        catDTO.nameBn = catDTO.languageTextBangla;
        return catDTO;
    }

    //------------- getName function from CatDAO similar implementation ends--------------------------------


    public String buildOptions(String domainName, String language, Integer selectedId, OrderByEnum orderByEnum) {
        return buildOptions(domainName, language, (selectedId == null || selectedId == 0) ? null : String.valueOf(selectedId), false, optionDTO -> true, orderByEnum);
    }

    public String buildOptionsForMultipleSelection(String domainName, String language, String selectedIds, OrderByEnum orderByEnum) {
        return buildOptions(domainName, language, selectedIds, true, optionDTO -> true, orderByEnum);
    }

    public String buildOptions(String domainName, String language, Integer selectedId) {
        return buildOptions(domainName, language, (selectedId == null || selectedId == 0) ? null : String.valueOf(selectedId), false, optionDTO -> true, OrderByEnum.ASC);
    }

    public String buildOptionsWithoutSelectOption(String domainName, String language, Integer selectedId) {
        return buildOptions(domainName, language, (selectedId == null || selectedId == 0) ? null : String.valueOf(selectedId), false, optionDTO -> true, OrderByEnum.ASC, false);
    }

    public String buildOptionsForMultipleSelection(String domainName, String language, String selectedIds) {
        return buildOptions(domainName, language, selectedIds, true, optionDTO -> true, OrderByEnum.ASC);
    }

    private String buildOptions(String domainName, String language, String selectedId, boolean isMultiSelect, Predicate<OptionDTO> filterFunction, OrderByEnum orderByEnum) {
        return buildOptions(domainName, language, selectedId, isMultiSelect, filterFunction, orderByEnum, true);
    }

    private String buildOptions(String domainName, String language, String selectedId, boolean isMultiSelect, Predicate<OptionDTO> filterFunction, OrderByEnum orderByEnum, boolean withSelectOption) {
        List<OptionDTO> optionDTOList = null;
        List<CategoryLanguageModel> modelList = mapSortedListByCategoryValue.get(domainName);
        if (modelList != null && !modelList.isEmpty()) {
            optionDTOList = modelList.stream()
                    .map(model -> new OptionDTO(model.englishText, model.banglaText, String.valueOf(model.categoryValue)))
                    .filter(filterFunction)
                    .collect(Collectors.toList());
        }
        if (optionDTOList != null && orderByEnum == OrderByEnum.DSC)
            Collections.reverse(optionDTOList);
        if (isMultiSelect) {
            return Utils.buildOptionsMultipleSelection(optionDTOList, language, selectedId);
        } else {
            if (withSelectOption) {

                String options = Utils.buildOptions(optionDTOList, language, selectedId);
                logger.info(options);
                return options;
            } else {
                return Utils.buildOptionsWithoutSelectWithSelectId(optionDTOList, language, selectedId);
            }
        }
    }

    public String buildOptionsWithFiler(String domainName, String language, Integer selectedId, List<Integer> filterList) {
        String selectedIdStr = (selectedId == null || selectedId == 0) ? null : String.valueOf(selectedId);
        if (filterList == null)
            return buildOptions(domainName, language, selectedIdStr,
                    false, optionDTO -> true, OrderByEnum.ASC);
        return buildOptions(domainName, language, selectedIdStr,
                false, optionDTO -> !(filterList.contains(Integer.parseInt(optionDTO.value))), OrderByEnum.ASC);
    }

    public String buildOptionsWithFiler(String domainName, String language, Integer selectedId, List<Integer> filterList, OrderByEnum orderByEnum) {
        String selectedIdStr = selectedId == null ? null : String.valueOf(selectedId);
        if (filterList == null)
            return buildOptions(domainName, language, selectedIdStr,
                    false, optionDTO -> true, OrderByEnum.ASC);
        return buildOptions(domainName, language, selectedIdStr,
                false, optionDTO -> !(filterList.contains(Integer.parseInt(optionDTO.value))), orderByEnum);
    }

    public String commaSeparateFreedomFighter(String ids, String language) {
        logger.debug("ids : " + ids);
        if (ids == null || ids.trim().length() == 0) {
            return "";
        }
        return Arrays.stream(ids.split(","))
                .map(String::trim)
                .filter(val -> !val.equals(""))
                .map(Utils::convertToInteger)
                .filter(Objects::nonNull)
                .map(val -> getText(language, "freedom_fighter_info", val))
                .collect(joining(", "));
    }
}