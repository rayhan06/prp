package pb;

/*
 * @author Md. Erfan Hossain
 * @created 30/12/2021 - 12:51 AM
 * @project parliament
 */

public class NameModel {
    public String nameEn;
    public String nameBn;

    public NameModel(String nameEn, String nameBn) {
        this.nameEn = nameEn;
        this.nameBn = nameBn;
    }

    public NameModel() {
    }
}
