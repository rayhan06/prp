package pb;

import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class PBNameRepository implements Repository {


    private static final Logger logger = Logger.getLogger(PBNameRepository.class);
    Map<String, Map<Long, PBNameDTO>> idTableNameMap;

    @Override
    public String getTableName() {
        // TODO Auto-generated method stub
        return "pbNameTable";
    }

    static PBNameRepository instance = null;

    private PBNameRepository() {
        idTableNameMap = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    PBNameDAO pBNameDAO = null;

    public void setDAO(PBNameDAO pBNameDAO) {
        this.pBNameDAO = pBNameDAO;
    }

    public synchronized static PBNameRepository getInstance() {
        if (instance == null) {
            instance = new PBNameRepository();
            System.out.println("1st reloading PBNameRepository");
            instance.reload(true); //auto reload fails
        }
        return instance;
    }

    @Override
    public void reload(boolean realoadAll) {

        if (pBNameDAO == null) {
            pBNameDAO = new PBNameDAO();
        }
        try {
            List<PBNameDTO> pBNameDTOs = pBNameDAO.getAll(realoadAll);
            for (PBNameDTO pBNameDTO : pBNameDTOs) {
                if (!realoadAll) {
                    PBNameDTO oldPBNameDTO = pBNameDAO.getDTOByTableAndID(pBNameDTO.tableName, pBNameDTO.iD);
                    if (oldPBNameDTO != null) {
                        if (idTableNameMap.containsKey(pBNameDTO.tableName)) {
                            Map<Long, PBNameDTO> localMap = idTableNameMap.getOrDefault(pBNameDTO.tableName, null);
                            if (localMap != null) {
                                localMap.remove(pBNameDTO.iD);
                            }
                        }
                    }
                }
                if (pBNameDTO.isDeleted == 0) {
                    if (idTableNameMap.containsKey(pBNameDTO.tableName)) {
                        Map<Long, PBNameDTO> localMap = idTableNameMap.getOrDefault(pBNameDTO.tableName, null);
                        localMap.put(pBNameDTO.iD, pBNameDTO);
                    } else {
                        Map<Long, PBNameDTO> localMap = new ConcurrentHashMap<>();
                        localMap.put(pBNameDTO.iD, pBNameDTO);
                        idTableNameMap.put(pBNameDTO.tableName, localMap);
                    }
                }
            }

        } catch (Exception ex) {
            logger.error("", ex);
        }

        if (realoadAll) {
            System.out.println("idTableNameMap size " + idTableNameMap.size());
        }

    }

    public PBNameDTO getNameDTO(String table, long id) {
        if (idTableNameMap.containsKey(table)) {
            Map<Long, PBNameDTO> localMap = idTableNameMap.getOrDefault(table, null);
            return localMap.getOrDefault(id, null);
        }
        return null;
    }

    public Map<Long, PBNameDTO> getNameDTOs(String table) {
        return idTableNameMap.getOrDefault(table, null);
    }

    public String getOptions(String language, String table) {
        return getOptions(language, table, CatDTO.CATDEFAULTNOTREQUIRED);
    }

    public String getOptions(String language, String table, long defaultValue) {
        return getOptions(language, table, defaultValue, CatRepository.DEFAULT_SORT);
    }
    
    public String getOptions(String language, String table, long defaultValue, int sortingType) {
        return getOptions(language, table, defaultValue, CatRepository.DEFAULT_SORT, "");
    }
    
    public String getOptionsWIthExtraColumnVal(String language, String table, String extraColumnVal) {
        return getOptions(language, table, -1, CatRepository.DEFAULT_SORT, extraColumnVal);
    }
    
    public String getOptionsWithExtraColumnVal(String language, String table, long extraColumnVal) {
        return getOptions(language, table, -1, CatRepository.DEFAULT_SORT, extraColumnVal + "");
    }

    public String getOptions(String language, String table, long defaultValue, int sortingType, String extraColumnVal) {
        String sOptions = "";

        if (defaultValue == CatDTO.CATDEFAULTNOTREQUIRED) {
            if (language.equalsIgnoreCase("English")) {
                sOptions += "<option value = '" + CatDTO.CATDEFAULTNOTREQUIRED + "'>Select</option>";
            } else {
                sOptions += "<option value = '" + CatDTO.CATDEFAULTNOTREQUIRED + "'>বাছাই করুন</option>";
            }
        } else if (defaultValue == CatDTO.CATDEFAULT) {
            if (language.equalsIgnoreCase("English")) {
                sOptions += "<option value = ''>Select</option>";
            } else {
                sOptions += "<option value = ''>বাছাই করুন</option>";
            }
        }

        Map<Long, PBNameDTO> nameDTOS = getNameDTOs(table);

        List<PBNameDTO> nameDTOList = new ArrayList<PBNameDTO>(nameDTOS.values());
        if (sortingType == CatRepository.SORT_BY_NAMEEN) {
            nameDTOList.sort(Comparator.comparing(PBNameDTO::getNameEn));
        }


        if (nameDTOList != null) {
            for (PBNameDTO pBNameDTO : nameDTOList) {
            	
                if (pBNameDTO != null) {
                	if(!extraColumnVal.equals("") && !pBNameDTO.extraColumnVal.equals(extraColumnVal))
                	{
                		continue;
                	}
                    String name = "";
                    if (language.equalsIgnoreCase("english")) {
                        name = pBNameDTO.nameEn;
                    } else {
                        name = pBNameDTO.nameBn;
                    }
                    String sOption = "<option value = '" + pBNameDTO.iD + "'";
                    if (defaultValue != CatDTO.CATDEFAULTNOTREQUIRED && pBNameDTO.iD == defaultValue) {
                        sOption += " selected ";
                    }
                    sOption += ">"
                            + name
                            + "</option>";
                    sOptions += sOption;
                }
            }
        }
        return sOptions;
    }

    public String getName(String language, String table, long id) {
        PBNameDTO pBNameDTO = getNameDTO(table, id);
        if (pBNameDTO != null) {
            if (language.equalsIgnoreCase("english")) {
                return pBNameDTO.nameEn;
            } else {
                return pBNameDTO.nameBn;
            }
        } else {
            System.out.println(table + ", id = " + id + " is not in name repo, returning null");
        }
        return "";
    }

}
