package pb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import common.ConnectionAndStatementUtil;

import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

public class PBNameDAO extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public PBNameDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = null;
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public PBNameDTO build(ResultSet rs)
	{
		try
		{
			PBNameDTO pbNameDTO = new PBNameDTO();
			pbNameDTO.iD = rs.getLong("ID");
			pbNameDTO.nameEn = rs.getString("name_en");
			pbNameDTO.nameBn = rs.getString("name_bn");
			pbNameDTO.isDeleted = rs.getInt("isDeleted");
			pbNameDTO.lastModificationTime = rs.getLong("lastModificationTime");
			
			return pbNameDTO;
		}
		catch (SQLException ex)
		{
			logger.error("",ex);
			return null;
		}
	}
	
	public PBNameDTO buildWithExtraColumn(ResultSet rs, String extraColumnName)
	{
		try
		{
			PBNameDTO pbNameDTO = new PBNameDTO();
			pbNameDTO.iD = rs.getLong("ID");
			pbNameDTO.nameEn = rs.getString("name_en");
			pbNameDTO.nameBn = rs.getString("name_bn");
			pbNameDTO.extraColumnVal = rs.getString(extraColumnName);
			pbNameDTO.isDeleted = rs.getInt("isDeleted");
			pbNameDTO.lastModificationTime = rs.getLong("lastModificationTime");
			
			return pbNameDTO;
		}
		catch (SQLException ex)
		{
			logger.error("",ex);
			return null;
		}
	}
	
	public PBNameDAO()
	{
		this("pb_name");		
	}
	
	public List<PBNameDTO> getAll (boolean isFirstReload)
    {
		List<PBNameDTO> pBNameDTOs = new ArrayList<PBNameDTO>();
		for(PBNameDTO.Table table: PBNameDTO.Table.values())
		{
			String sql = "SELECT id, name_en, name_bn, ";
			if(table.getExtraColumnName() != null)
			{
				sql += table.getExtraColumnName() + ", ";
			}
			sql +=  "isDeleted, lastModificationTime FROM " + table.getTableName() + " WHERE ";
			if(isFirstReload){
				sql+=" isDeleted =  0";
			}
			else
			{
				sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
			}
			
			sql += " order by " + table.getTableName() + ".id asc";
			
			
			List<PBNameDTO> pBNameDTOLocals;
			if(table.getExtraColumnName() == null)
			{
				pBNameDTOLocals =  ConnectionAndStatementUtil.getListOfT(sql,this::build);
			}
			else
			{
				pBNameDTOLocals =  ConnectionAndStatementUtil.getListOfT2(sql, table.getExtraColumnName(),this::buildWithExtraColumn);
			}
			
			
			for(PBNameDTO pBNameDTOLocal : pBNameDTOLocals)
			{
				pBNameDTOLocal.tableName = table.getTableName();
				pBNameDTOs.add(pBNameDTOLocal);
			}
		}
		
		return 	pBNameDTOs;
    }

	@Override
	public Collection getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable,
			UserDTO userDTO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CommonDTO getDTOByID(long ID) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	public PBNameDTO getDTOByTableAndID (String table, Long id)
	{
		String sql = "SELECT id, name_en, name_bn, isDeleted, lastModificationTime FROM " + table + " WHERE ID = " + id;		
		return ConnectionAndStatementUtil.getT(sql,this::build);
	}

}
