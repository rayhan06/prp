package pb;

import util.CommonDTO;

public class PBNameDTO extends CommonDTO{
	
    public String nameEn = "";
    public String nameBn = "";
    public String tableName = "";
    public String extraColumnVal = "";
    
    public String getNameEn()
	{
		return nameEn;
	}

    
    public enum Table {
        APPROVAL_PATH("approval_path"),
        ASSET_CATEGORY("asset_category"),
        ASSET_LIST("asset_list"),
        ASSET_MANUFACTURER("asset_manufacturer"),
        ASSET_MODEL("asset_model"),
        ASSET_SUPPLIER("asset_supplier"),
        ASSET_TYPE("asset_type"),
        BANK_NAME("bank_name"),
        BRAND("brand"),
        DESIGNATIONS("designations"),
        RELATION("relation"),
        MEDICINE_GENERIC_NAME("medicine_generic_name"),
        PHARMA_COMPANY_NAME("pharma_company_name"),
        ASSET_MEDICAL_EQUIPMENT_NAME("medical_equipment_name"),
        THERAPY_TYPE("therapy_type"),
        TICKET_ISSUES("ticket_issues", "ticket_issues_id"),
        TICKET_ISSUES_SUBTYPE("ticket_issues_subtype"),
        OTHER_OFFICE("other_office"),
        OTHER_OFFICE_DEPARTMENT("other_office_department"),
        PI_UNIT("pi_unit"),
        PI_PROCUREMENT_GOODS_GROUP("procurement_package"),
        PI_PROCUREMENT_GOODS_TYPE("procurement_goods_type"),
        PI_PROCUREMENT_GOODS("procurement_goods"),
    	LAB_TEST("lab_test"),
    	BUILDING("building"),
    	BUILDING_BLOCK("building_block"),
    	PARLIAMENT_ITEM("parliament_item"),
    	FISCAL_YEAR("fiscal_year"),
    	LAB_TEST_LIST("lab_test_list");

        private final String tableName;
        private final String exTraColumnName;

        Table(String tableName) {
            this.tableName = tableName;
            this.exTraColumnName = null;
        }
        
        Table(String tableName, String extraColumnName) {
            this.tableName = tableName;
            this.exTraColumnName = extraColumnName;
        }
        
        
        public String getTableName(){
            return tableName;
        }
        
        public String getExtraColumnName(){
            return exTraColumnName;
        }
        
        public static boolean contains(String test) {

            for (Table c : Table.values()) {
                if (c.getTableName().equals(test)) {
                    return true;
                }
            }
            return false;
        }
    }


}
