package pb;

import category.CategoryDAO;
import category.CategoryDTO;
import common.ConnectionAndStatementUtil;
import language.LanguageDAO;
import language.LanguageTextDTO;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import sessionmanager.SessionConstants;
import user.UserDTO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings({"unused","Duplicates"})
public class CatDAO {

    public static long CATEGORY_MENU = 238900;
    public static LanguageDAO languageDAO = new LanguageDAO();
    public static CategoryDAO categoryDAO =  CategoryDAO.getInstance();
    public static String language = "english";

    private static final Logger logger = Logger.getLogger(CatDAO.class);

    private static final String sqlGetMaxValue = "SELECT max(value) FROM category where domain_name = ? and isDeleted = 0 ";

    private static final String sqlGetDTOByDomainNameAndValue = "SELECT value,ordering, name_en, name_bn, parent_domain, parent_domain_val FROM"
            .concat(" category  WHERE isDeleted = 0  and ")
            .concat(" domain_name = ? and value = ?  order by value asc");

    private static final String sqlGetDTOByDomainName = "SELECT value,ordering, name_en, name_bn, parent_domain, parent_domain_val FROM"
            .concat(" category where isDeleted = 0 ")
            .concat(" and domain_name = ? order by value asc");

    private static final String getCategoryLanguageModelQueryForTrue = "SELECT id,domain_name,value,ordering,isDeleted, name_en, name_bn, parent_domain, parent_domain_val FROM category where isDeleted =  0";

    private static final String getCategoryLanguageModelQueryForFalse = "SELECT id,domain_name,value,ordering,isDeleted, name_en, name_bn, parent_domain, parent_domain_val FROM category where lastModificationTime >= %d";

    private static final String getCategoryByDomainNameAndValue = "SELECT * FROM category WHERE domain_name = ? AND value = ? AND isDeleted = 0";

    private static final String getCategoryByName = "SELECT * FROM category WHERE domain_name = ? AND isDeleted = 0 order by value";

    private static final String getCategoryById = "SELECT * FROM category WHERE id = %d AND isDeleted = 0";

    private static final String getCategoryByLastModificationTime = "SELECT * FROM category WHERE lastModificationTime = %d";

    public static CatDTO getDTO(String domainName, int value) {
        //logger.debug("domainName : " + domainName + "  value : " + value);
        return ConnectionAndStatementUtil.getT(sqlGetDTOByDomainNameAndValue, ps -> {
            try {
                ps.setString(1, domainName);
                ps.setInt(2, value);
            } catch (SQLException ex) {
                logger.error("",ex);
            }
        }, rs -> buildDTOFromResultSet(rs, domainName));
    }

    public static List<CatDTO> getJobcat(UserDTO userDTO) {

        String sql = "SELECT \r\n" + "    value,ordering, approval_path.name_en, approval_path.name_bn\r\n" + "FROM\r\n"
                + "    category\r\n";

        sql += " join approval_path on (approval_path.job_cat = category.value and approval_path.isDeleted = 0 and approval_path.approval_path_cat = 3)";

        sql += "WHERE\r\n" + " category.isDeleted = 0 and approval_path.isDeleted = 0 and  domain_name = 'job'";
        if (userDTO.roleID != SessionConstants.ADMIN_ROLE && userDTO.unitID != 78040L) {
            sql += " and (approval_path.office_unit_id = -1 or approval_path.office_unit_id = " + userDTO.unitID + ")";
        }
        sql += " order by value asc";

        String sqlQuery = sql;
        return ConnectionAndStatementUtil.getListOfT(sqlQuery, rs -> buildDTOFromResultSet(rs, "job"));
    }

    public static List<CatDTO> getDTOs(String domainName) {
        logger.debug("domainName : " + domainName);
        return ConnectionAndStatementUtil.getListOfT(sqlGetDTOByDomainName, ps -> {
            try {
                ps.setString(1, domainName);
            } catch (SQLException ex) {
                logger.error("",ex);
            }
        }, rs -> buildDTOFromResultSet(rs, domainName));
    }

    private static CatDTO buildDTOFromResultSet(ResultSet rs, String tableName) {
        try {
            CatDTO catDTO = new CatDTO();
            catDTO.value = rs.getInt("value");
            catDTO.domainName = tableName;
            catDTO.ordering = rs.getInt("ordering");
            catDTO.languageTextBangla = rs.getString("name_bn");
            catDTO.languageTextEnglish = rs.getString("name_en");
            catDTO.parentDomain = rs.getString("parent_domain");
            catDTO.parentDomainVal = rs.getInt("parent_domain_val");
            return catDTO;
        } catch (SQLException ex) {
            logger.error("",ex);
            return null;
        }
    }


    public static String getOption(CatDTO catDTO, String Language) {
    	String smLanguage = language.toLowerCase();
    	if(!smLanguage.equalsIgnoreCase("bangla") && !smLanguage.equalsIgnoreCase("english"))
    	{
    		language = "bangla";
    	}
        String sOption;
        if (catDTO != null) {
            sOption = "<option value = '" + catDTO.value + "'>";
            if (Language.equals("English")) {
                sOption += catDTO.languageTextEnglish;
            } else {
                sOption += catDTO.languageTextBangla;
            }
            sOption += "</option>";
        } else {
        	 if (Language.equals("English"))
        	 {
        		 sOption = "<option value = ''>Select</option>";
        	 }
        	 else
        	 {
        		 sOption = "<option value = ''>বাছাই করুন</option>";
        	 }
        }

        return sOption;
    }

    public static String getOptions(String Language, String TableName, long defaultValue) {
        return getOptions(Language, TableName, (int) defaultValue, -1);
    }
    
    public static String getOptions(String language, String domainName, int defaultValue)
    {
    	return getOptions(language, domainName, defaultValue, -1);
    }

    public static String getOptions(String language, String domainName, int defaultValue, int without) {
    	//logger.debug("language : " + language + "  domainName : " + domainName + "  defaultValue : " + defaultValue);
    	if(defaultValue != -2 && without < 0)
    	{
    		return CatRepository.getOptions(language, domainName, defaultValue);
    	}
        
        String smLanguage = language.toLowerCase();
    	if(!smLanguage.equalsIgnoreCase("bangla") && !smLanguage.equalsIgnoreCase("english"))
    	{
    		language = "bangla";
    	}
    	if(!domainName.matches("^[a-zA-Z0-9_]*$") || domainName.length() > 50)
		{
    		return "";
		}
        List<CatDTO> catDTOs = getDTOs(domainName);

        StringBuilder sOptions = new StringBuilder();
        if (defaultValue == CatDTO.CATDEFAULT) {
            sOptions.append(getOption(null, language));
        }
        else if (defaultValue == CatDTO.CATDEFAULTNOTREQUIRED) {
        	if (language.equalsIgnoreCase("English"))
	       	 {
        		sOptions.append("<option value = '").append(CatDTO.CATDEFAULTNOTREQUIRED).append("'>Select</option>");
	       	 }
	       	 else
	       	 {
	       		sOptions.append("<option value = '").append(CatDTO.CATDEFAULTNOTREQUIRED).append("'>বাছাই করুন</option>");
	       	 }
        }

        for (CatDTO catDto : catDTOs) {
        	if(catDto.value != without)
        	{
        		sOptions.append(getOption(catDto, language));
        		if (defaultValue > 0 && catDto.value == defaultValue) 
        		{
	              int index = sOptions.lastIndexOf("<option");
	              String s1 = sOptions.substring(0, index);
	              String s2 = sOptions.substring(index);
	              s2 = s2.replaceFirst("<option", "<option selected ");
	              sOptions = new StringBuilder(s1 + s2);
        		}
        	}
          
        }

        return sOptions.toString();
    }

    public static String getJobs(UserDTO userDTO, String TableName, int DefaultValue) {
        List<CatDTO> catDTOs = getJobcat(userDTO);
        String sOptions = "";
        if (DefaultValue > 0) {
            CatDTO defaultDTO = getDTO(TableName, DefaultValue);
            sOptions += getOption(defaultDTO, "English");
        } else if (DefaultValue == -2) {
            sOptions += getOption(null, "English");
        }

        return catDTOs.stream()
                .map(dto -> getOption(dto, "English"))
                .collect(Collectors.joining("", sOptions, ""));
    }

    public static String getName(String Language, String domainName, int Value) {
    	return CatRepository.getName(Language, domainName, Value);
    }

    public static String getName(String Language, String domainName, long value) {
        return getName(Language, domainName, Long.valueOf(value).intValue());
    }

    private static long insertIntoLanguage(String tableName, String nameEn, String nameBN) {
        LanguageTextDTO languageTextDTO = new LanguageTextDTO();
        languageTextDTO.languageTextEnglish = nameEn;
        languageTextDTO.languageTextBangla = nameBN;

        languageTextDTO.languageConstant = nameEn.replace(" ", "_").toUpperCase();
        languageTextDTO.languageConstantPrefix = tableName.replace(" ", "_").toUpperCase();

        languageTextDTO.menuID = CATEGORY_MENU;

        System.out.println("Language DTO = " + languageTextDTO);
        return languageDAO.addLanguageText(languageTextDTO);
    }

    public static int getMaxValue(String tablename) {
        Object obj = ConnectionAndStatementUtil.getReadPrepareStatement(ps -> {
            int max = -1;
            try {
                ps.setString(1, tablename);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    max = rs.getInt("max(value)");
                }
            } catch (SQLException e) {
                logger.debug(e);
            }
            return max;
        }, sqlGetMaxValue);
        return (Integer) obj;
    }

    public static int add(String tableName, String nameEn, String nameBn) throws Exception {
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.domainName = tableName;
        categoryDTO.languageId = insertIntoLanguage(tableName, nameEn, nameBn);
        categoryDTO.value = getMaxValue(tableName) + 1;
        categoryDAO.add(categoryDTO);
        return categoryDTO.value;
    }

    public static int addFromOthers(String tableName, String nameEn, String nameBn) throws Exception {
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.domainName = tableName;
        categoryDTO.nameEn = nameEn;
        categoryDTO.nameBn = nameBn;
        categoryDTO.value = getMaxValue(tableName) + 1;
        categoryDTO.ordering = 1;
        categoryDAO.add(categoryDTO);
        return categoryDTO.value;
    }

    public List<CategoryLanguageModel> getCategoryLanguageModelList(boolean isFirstReload) {
        if (isFirstReload) {
            return ConnectionAndStatementUtil.getListOfT(getCategoryLanguageModelQueryForTrue, this::buildCategoryLanguageModel);
        } else {
            return ConnectionAndStatementUtil.getListOfT(String.format(getCategoryLanguageModelQueryForFalse, RepositoryManager.lastModifyTime),
                    this::buildCategoryLanguageModel);
        }
    }

    private CategoryLanguageModel buildCategoryLanguageModel(ResultSet rs) {
        try {
            CategoryLanguageModel model = new CategoryLanguageModel();
            model.id = rs.getLong("id");
            model.domainName = rs.getString("domain_name");
            model.categoryValue = rs.getInt("value");
            model.englishText = rs.getString("name_en");
            model.banglaText = rs.getString("name_bn");
            model.isDeleted = rs.getInt("isDeleted");
            model.ordering = rs.getInt("ordering");
            return model;
        } catch (SQLException ex) {
            logger.error("",ex);
            return null;
        }
    }

    public CategoryLanguageModel getCategoryLanguageModel(String domainName, int value) {
        return ConnectionAndStatementUtil.getT(getCategoryByDomainNameAndValue, Arrays.asList(domainName, value), this::buildCategoryLanguageModel);
    }

    public List<CategoryLanguageModel> getCategoryLanguageModelListByDomainName(String domainName) {
        return ConnectionAndStatementUtil.getListOfT(getCategoryByName, Collections.singletonList(domainName), this::buildCategoryLanguageModel);
    }

    public CategoryLanguageModel getCategoryLanguageModelById(long id) {
        return ConnectionAndStatementUtil.getT(String.format(getCategoryById, id), this::buildCategoryLanguageModel);
    }

    public List<CategoryLanguageModel> getCategoryLanguageModelByLastModificationTime(long lastModificationTime) {
        return ConnectionAndStatementUtil.getListOfT(String.format(getCategoryByLastModificationTime, lastModificationTime), this::buildCategoryLanguageModel);
    }
}