package pb;

import java.awt.Color;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import employee_pay_scale.Employee_pay_scaleDAO;
import employee_pay_scale.Employee_pay_scaleDTO;
import employee_pay_scale.Employee_pay_scaleRepository;
import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;

import dbm.*;
import drug_information.Drug_informationDTO;
import drug_information.Drug_informationRepository;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_offices.EmployeeOfficesDAO;
import employee_offices.Employee_officesDTO;
import medical_equipment_name.Medical_equipment_nameDTO;
import medical_equipment_name.Medical_equipment_nameRepository;
import medical_reagent_name.Medical_reagent_nameDTO;
import medical_reagent_name.Medical_reagent_nameRepository;
import medicine_generic_name.Medicine_generic_nameDTO;
import medicine_generic_name.Medicine_generic_nameRepository;
import user.UserDTO;
import user.UserRepository;
import workflow.WorkflowController;
import sessionmanager.SessionConstants;


public class CommonDAO 
{
	private static final Logger logger = Logger.getLogger(CommonDAO.class);

	public static int  getID (String sql)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		int idToReturn = -1;
		
		
		try
		{
									
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);
			
			

			if(rs.next())
			{
				idToReturn = rs.getInt("id");
				////logger.debug("found Option = " + Option);					 

			}			
			
		}
		catch(Exception ex)
		{
			//logger.debug(ex);
		}
		finally
		{
			try
			{ 
				if (stmt != null) 
				{
					stmt.close();
				}
			} 
			catch (Exception e)
			{
				//logger.debug(e);
			}
			
			try
			{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}
			catch(Exception ex2)
			{
				//logger.debug(ex2);
			}
		}
		return idToReturn;
	}
	public static List<Integer>  getIDs (String sql)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Integer> Options = new ArrayList<Integer>();
		
		
		try
		{
			//logger.debug(" sql = " + sql);	
									
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);
			
			

			while(rs.next())
			{
				int Option = rs.getInt("id");
				////logger.debug("found Option = " + Option);					 
				Options.add(Option);

			}			
			
		}
		catch(Exception ex)
		{
			logger.error("",ex);
		}
		finally
		{
			try
			{ 
				if (stmt != null) 
				{
					stmt.close();
				}
			} 
			catch (Exception e)
			{
				//logger.debug(e);
			}
			
			try
			{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}
			catch(Exception ex2)
			{
				//logger.debug(ex2);
			}
		}
		return Options;
	}
	
	public static List<Integer>  getIDs (String TableName, String id, String whereColumn)
	{
		String sql = "select  " + id + "  from " + TableName + " where isDeleted = 0";
		
		if(!whereColumn.equals(""))
		{
			sql += " and " + whereColumn;
		}
		
		return getIDs(sql);
	}
	
	public static List<Integer>  getIDsOrderBy (String TableName, String id, String whereColumn)
	{
		String sql = "select  " + id + "  from " + TableName + " where isDeleted = 0";
		
		if(!whereColumn.equals(""))
		{
			sql += " and " + whereColumn;
		}
		sql += " order by "+id;
		
		return getIDs(sql);
	}
	
	public static List<Integer>  getIDsWithoutIsDeleted (String TableName, String id, String whereColumn)
	{
		String sql = "select  " + id + "  from " + TableName /*+ " where isDeleted = 0"*/;
		
		if(!whereColumn.equals(""))
		{
			sql += " where " + whereColumn;
		}
		
		return getIDs(sql);
	}
	
	public static String  getName (int id, String TableName)
	{
		return getName (id, TableName, "name_en", "id");
	}

	public static String  getName (String Language, String TableName, long value)
	{
		return getName(Language, TableName, (int)value);
	}
	public static String  getNameDecidedByLanguage (LoginDTO loginDTO, String TableName, long value)
	{
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		int languageID = LM.getLanguageIDByUserDTO(userDTO);
		String Language = languageID == 1?"english":"bangla";
		return getName(Language, TableName, (int)value);
	}
	public static String  getName (String Language, String tableName, int value)
	{
		if(PBNameDTO.Table.contains(tableName))
		{
			PBNameRepository pBNameRepository = PBNameRepository.getInstance();
			return pBNameRepository.getName(language, tableName, value);
		}
		if(Language.equalsIgnoreCase("english"))
		{
			return getName (value, tableName, "name_en", "id");
		}
		else
		{
			return getName (value, tableName, "name_bn", "id");
		}
	}
	
	public static String  getNameFromDbOnly (String Language, String tableName, long id)
	{
		
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		String option = "";
		String name = "name_bn";
		if(name.equalsIgnoreCase(""))
		{
			name = "name_en";
		}
		
		String sql = "select " + name + " from " + tableName + " where id = " + id;
		//System.out.println("match sql: "+sql);
		logger.debug(sql);
		
		try
		{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
	
	
			rs = stmt.executeQuery(sql);
			
			
	
			if(rs.next())
			{
				 ////logger.debug(rs.getString("name_en") + "  " + rs.getInt("id"));
				option = rs.getString(name);
				////logger.debug("found Option = " + Option);					 
	
			}
			
		}
		catch(Exception ex)
		{
			logger.error("",ex);
		}
		finally
		{
			try
			{ 
				if (stmt != null) 
				{
					stmt.close();
				}
			} 
			catch (Exception e)
			{
				//logger.debug(e);
			}
			
			try
			{ 
				if (connection != null)
				{ 

					DBMR.getInstance().freeConnection(connection);
				} 
			}
			catch(Exception ex2)
			{
				//logger.debug(ex2);
			}
		}
		return option;
	}
	public static String  getName (long value, String TableName, String name)
	{
		return getName((int) value, TableName, name, "id");
	}
	public static String  getName (long id, String TableName, String name, String matchColumn)
	{
		return getName((int) id, TableName, name, matchColumn);
	}

	
	public static String  getName (int id, String TableName, String name, String matchColumn)
	{
		if(PBNameDTO.Table.contains(TableName) && matchColumn.equalsIgnoreCase("id") 
				&& (name.equalsIgnoreCase("name_en") || name.equalsIgnoreCase("name_bn")))
		{
			PBNameRepository pBNameRepository = PBNameRepository.getInstance();
			if(name.equalsIgnoreCase("name_en"))
			{
				return pBNameRepository.getName("english", TableName, id);
			}
			else
			{
				return pBNameRepository.getName("bangla", TableName, id);
			}			
		}		
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		String Option = "";
		
		if(name.equalsIgnoreCase(""))
		{
			name = "name_en";
		}
		
		try
		{
			
			String sql = "select " + name + " from " + TableName + " where " + matchColumn + " = " + id;
			//System.out.println("match sql: "+sql);
			logger.debug(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);
			
			

			if(rs.next())
			{
				 ////logger.debug(rs.getString("name_en") + "  " + rs.getInt("id"));
				Option = rs.getString(name);
				////logger.debug("found Option = " + Option);					 

			}			
			
		}
		catch(Exception ex)
		{
			logger.error("",ex);
		}
		finally
		{
			try
			{ 
				if (stmt != null) 
				{
					stmt.close();
				}
			} 
			catch (Exception e)
			{
				//logger.debug(e);
			}
			
			try
			{ 
				if (connection != null)
				{ 

					DBMR.getInstance().freeConnection(connection);
				} 
			}
			catch(Exception ex2)
			{
				//logger.debug(ex2);
			}
		}
		return Option;
	}
	
	public static long  getIDfromName (String TableName, String name)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		PreparedStatement ps = null;

		
		long id = -1;
		
		try
		{
			
			String sql = "select id from " + TableName + " where name_en=? or name_bn=?"; 
			
			//logger.debug(sql);
			

			connection = DBMR.getInstance().getConnection();
			ps = connection.prepareStatement(sql);
			
			int index = 1;
			
			ps.setObject(index++,name);
			ps.setObject(index++,name);
			
			rs = ps.executeQuery();
		

			if(rs.next())
			{				
				id = rs.getLong("id");								 

			}			
			
		}
		catch(Exception ex)
		{
			logger.error("",ex);
		}
		finally
		{
			try
			{ 
				if (stmt != null) 
				{
					stmt.close();
				}
			} 
			catch (Exception e)
			{
				//logger.debug(e);
			}
			
			try
			{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}
			catch(Exception ex2)
			{
				//logger.debug(ex2);
			}
		}
		//logger.debug("returning id = " + id);
		return id;
	}
	
	public static String  getOptionsWithMultiDefault (String Language, String TableName, String nameColumn, String defaultValue, String whereColumn)
	{
		if(nameColumn.equals(""))
		{
			if(Language.equals("English"))
			{
				nameColumn = "name_en";
			}
			else if(Language.equals("Bangla"))
			{
				nameColumn = "name_bn";
			}
		}
		
		
		String	idColumn = "id";
		
		
		List<Integer> Options = getIDs (TableName, idColumn, whereColumn);
		String sOptions = "";
		String[] defaults = defaultValue.split(", ");

		if(!Options.isEmpty())
		{
			for(int i = 0; i <Options.size(); i ++)
			{
				int nOption = Options.get(i);
				String name = getName(nOption, TableName, nameColumn, idColumn);
				String value;
				

				value = nOption + "";


				sOptions += "<option  value = '" + value + "' ";
				for(String def: defaults)
				{
					if(value.equalsIgnoreCase(def))
					{
						sOptions += "selected";
					}
				}
				sOptions += ">";
				sOptions += name;
				sOptions += "</option>";

			}
		}
		////logger.debug("Options: " + sOptions);
		return sOptions;	
	}
	
	
	
	public static String  getOptions (String Language, String HtmlType, String TableName, String rawHtmlID, String htmlClass, String htmlName,
	String idColumn, String nameColumn, String DefaultValue, String whereColumn)
	{
		if(nameColumn.equals(""))
		{
			if(Language.equalsIgnoreCase("English"))
			{
				nameColumn = "name_en";
			}
			else
			{
				nameColumn = "name_bn";
			}
		}
		
		boolean orderBy = false;
		if(idColumn.equals(""))
		{
			idColumn = "id";
			orderBy = true;
		}
		
		List<Integer> Options;
		if(orderBy) {
			Options = getIDsOrderBy(TableName, idColumn, whereColumn);
		}else {
			Options = getIDs (TableName, idColumn, whereColumn);
		}
		String sOptions = "";
		String DefaultValueOption = "";
		
		if(!DefaultValue.equals(""))
		{
			if(DefaultValue.equals("any"))
			{
				DefaultValueOption = "";
			}
			else
			{
				DefaultValueOption = getName(Integer.parseInt(DefaultValue), TableName, nameColumn, idColumn);
			}
			String htmlID = rawHtmlID + "_0";
			if(HtmlType.equals("radio"))
			{
				sOptions += "<input type='radio' class = '" + htmlClass + "' name = '" + htmlName + "' id = '" + htmlID + "' value = '" + DefaultValue + "'>";
				sOptions += DefaultValueOption;
				sOptions += " <br>";				
			}
			else if(DefaultValueOption.equalsIgnoreCase(""))
			{
				if(Language.equalsIgnoreCase("English"))
				{
					sOptions += "<option value = ''>Select</option>";
				}
				else
				{
					sOptions += "<option value = ''>বাছাই করুন</option>";
				}
				
			}
		}
			
	
		if(!Options.isEmpty())
		{
			for(int i = 0; i <Options.size(); i ++)
			{
				int nOption = Options.get(i);
				String name = getName(nOption, TableName, nameColumn, idColumn);
				String value;
				

				value = nOption + "";
				
				
				if(value.equals(DefaultValue) && !HtmlType.equals("select"))
				{
					continue;
				}
				
							
				String htmlID = rawHtmlID + "_" + (nOption + 1);
				if(HtmlType.equals("radio"))
				{
					sOptions += "<input type='radio' class = '" + htmlClass + "' name = '" + htmlName + "' id = '" + htmlID + "' value = '" + value + "'>";
					sOptions += name;
					sOptions += " <br>";					
				}
				else if(HtmlType.equals("select"))
				{
					if(value.equals(DefaultValue)) {						
						sOptions += "<option selected class = '" + htmlClass + "' value = '" + value + "'>";
					}else {						
						sOptions += "<option class = '" + htmlClass + "' value = '" + value + "'>";
					}
					sOptions += name;
					sOptions += "</option>";
				}
				
			
			}
		}
		////logger.debug("Options: " + sOptions);
		return sOptions;	
	}
	
	
	public static String  getOrganograms()
	{
		return getOrganograms(-1);
	}
	
	public static String language = "english";
	public static String  getOrganograms(long defaultValue)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;

		String sOptions = "";
		
		String defaultOption = "<option></option>";
		String nameCol = "employee_records.name_eng";
		String designationCol = "designation_eng";
		if(!language.equalsIgnoreCase("english"))
		{
			nameCol = "employee_records.name_bng";
			designationCol = "designation_bng";
		}
		
		try{
			
			String sql = "SELECT \r\n" + 
					"    users.id,\r\n" + 
					"    " + designationCol + ",\r\n" + 
					"    " + nameCol + "\r\n" + 
					"FROM\r\n" + 
					"    office_unit_organograms\r\n" + 
					"        JOIN\r\n" + 
					"    employee_offices ON (employee_offices.office_unit_organogram_id = office_unit_organograms.id\r\n" + 
					"        AND employee_offices.status = 1)\r\n" + 
					"        JOIN\r\n" + 
					"    employee_records ON employee_offices.employee_record_id = employee_records.id"
					+ "  join users on users.employee_record_id = employee_records.id";
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);
			


			while (rs.next()){
				
				String sOption =  "<option value = '" + rs.getLong("users.id") + "'>" 
			+ rs.getString(nameCol) + ", " 
			+ rs.getString(designationCol) 
			+ "</option>";
			   sOptions += sOption;
			   
			   if(defaultValue != -1 && rs.getLong("users.id") == defaultValue)
			   {
				   defaultOption = sOption;
			   }

			}	
			
			////logger.debug("########### DTO Count = " + i);
			
		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		if(defaultValue == -1)
		{
			return defaultOption + sOptions;	
		}
		else
		{
			return  sOptions;	
		}
		
	}
	
	public static String  getOrganogramsByOrganogramID(long id)
	{
		return getOrganogramsByOrganogramID(id, "");
	}
	
	public static String  getDoctorsByOrganogramID(long id)
	{
		return getDoctorsByOrganogramID(id, "", 1, false);
	}
	
	public static String  getDoctorsByOrganogramID()
	{
		return getDoctorsByOrganogramID(-1, "", 1, false);
	}
	
	public static String  getOrganogramsByOrganogramID()
	{
		return getOrganogramsByOrganogramID(-1, "");
	}
	
	
	public static String  getOrganogramsByOrganogramID(String where)
	{
		return getOrganogramsByOrganogramID(-1, where);
	}
	
	public static String getDrugOptions(List<Drug_informationDTO> drug_informationDTOs, String sOptions, long defaultValue)
	{
		for(Drug_informationDTO drug_informationDTO : drug_informationDTOs)
		{
			if(drug_informationDTO != null)
			{
				Medicine_generic_nameDTO medicine_generic_nameDTO = Medicine_generic_nameRepository.getInstance().getMedicine_generic_nameDTOByID(drug_informationDTO.medicineGenericNameType);
				if(medicine_generic_nameDTO != null)
				{
					String gname = medicine_generic_nameDTO.nameEn;
					String sOption = "<option "
							+ "value = '" + drug_informationDTO.iD + "'"
							+ "stock = '" + drug_informationDTO.availableStock + "'"
							+ "name = '" + drug_informationDTO.nameEn + "'"
							+ "form = '" + drug_informationDTO.drugFormCat + "'"
							+ "formStr = '" + CatDAO.getName("English", "drug_form", drug_informationDTO.drugFormCat) + "'"
							+ "strength = '" + drug_informationDTO.strength + "'"
							+ "alertLevel = '" + drug_informationDTO.minimulLevelForAlert + "'"
							+ "gname = '" + gname + "'";
					
					if(defaultValue == drug_informationDTO.iD)
					{
						sOption		+= "selected";
					}
							
					sOption		+= ">" 
							+ gname 
							+ "(" 
							+ drug_informationDTO.nameEn + " " + CatDAO.getName("English", "drug_form", drug_informationDTO.drugFormCat) + "," 
							+ drug_informationDTO.strength 
							+ ")"
							+ "</option>";
					
					 sOptions += sOption;
				}
			}
			
		}

		return sOptions;	
	}
	
	public static String getReagentOptions(List<Medical_reagent_nameDTO> medical_reagent_nameDTOs, String sOptions, long defaultValue)
	{
		for(Medical_reagent_nameDTO medical_reagent_nameDTO : medical_reagent_nameDTOs)
		{
			if(medical_reagent_nameDTO != null)
			{
				String sOption = "<option "
						+ "value = '" + medical_reagent_nameDTO.iD + "'"
						+ "stock = '" + medical_reagent_nameDTO.currentStock + "'"
						+ "unit = '" + CatDAO.getName(language, "quantity_unit", medical_reagent_nameDTO.quantityUnitCat) + "'";
				if(language.equalsIgnoreCase("english"))
				{
					sOption += "name = '" + medical_reagent_nameDTO.nameEn + "'";
				}
				else
				{
					sOption += "name = '" + medical_reagent_nameDTO.nameBn + "'";
				}
				
						
				
				if(defaultValue == medical_reagent_nameDTO.iD)
				{
					sOption		+= "selected";
				}
				
				if(language.equalsIgnoreCase("english"))
				{
					sOption		+= ">" 
							+ medical_reagent_nameDTO.nameEn
							+ "</option>";
				}
				else
				{
					sOption		+= ">" 
							+ medical_reagent_nameDTO.nameBn
							+ "</option>";
				}
				
				 sOptions += sOption;
			}
			
		}

		return sOptions;	
	}
	
	public static String getEquipmentOptions(List<Medical_equipment_nameDTO> medical_reagent_nameDTOs, String sOptions, long defaultValue)
	{
		for(Medical_equipment_nameDTO medical_equipment_nameDTO : medical_reagent_nameDTOs)
		{
			String sOption = "<option "
					+ "value = '" + medical_equipment_nameDTO.iD + "'"
					+ "stock = '" + medical_equipment_nameDTO.currentStock + "'";
			if(language.equalsIgnoreCase("english"))
			{
				sOption += "name = '" + medical_equipment_nameDTO.nameEn + "'";
			}
			else
			{
				sOption += "name = '" + medical_equipment_nameDTO.nameBn + "'";
			}
					
			
			if(defaultValue == medical_equipment_nameDTO.iD)
			{
				sOption		+= "selected";
			}
			
			if(language.equalsIgnoreCase("english"))
			{
				sOption		+= ">" 
						+ medical_equipment_nameDTO.nameEn
						+ "</option>";
			}
			else
			{
				sOption		+= ">" 
						+ medical_equipment_nameDTO.nameBn
						+ "</option>";
			}
			
			 sOptions += sOption;
			
		}

		return sOptions;	
	}
	
	public static String  getDrugsByGenericName(long defaultValue, long gNameCat)
	{

		String sOptions = "";

		if(defaultValue == -1)
		{
			if(language.equalsIgnoreCase("English"))
			{
				sOptions = "<option value = '-1'>Select</option>";
			}
			else
			{
				sOptions = "<option value = '-1'>বাছাই করুন</option>";
			}
		}
		List<Drug_informationDTO> drug_informationDTOs = Drug_informationRepository.getInstance().getDrug_informationDTOBymedicine_generic_name_type(gNameCat);
		
		return getDrugOptions(drug_informationDTOs, sOptions, defaultValue);
		
	}
	
	public static String  getDrugs()
	{

		return getDrugs(-1);
	}
	
	public static String  getDrugs(long defaultValue)
	{

		String sOptions = "";

		if(defaultValue == -1)
		{
			if(language.equalsIgnoreCase("english"))
			{
				sOptions = "<option value='-1'>Select</option>";
			}
			else
			{
				sOptions = "<option value='-1'>বাছাই করুন</option>";
			}
			
		}
		List<Drug_informationDTO> drug_informationDTOs = Drug_informationRepository.getInstance().getDrug_informationList();
		
		return getDrugOptions(drug_informationDTOs, sOptions, defaultValue);
	}
	
	public static String  getReagents(long defaultValue)
	{

		String sOptions = "";

		if(defaultValue == -1)
		{
			if(language.equalsIgnoreCase("English"))
			{
				sOptions = "<option value = '-1'>Select</option>";
			}
			else
			{
				sOptions = "<option value = '-1'>বাছাই করুন</option>";
			}
		}
		List<Medical_reagent_nameDTO> medical_reagent_nameDTOs = Medical_reagent_nameRepository.getInstance().getMedical_reagent_nameList();
		
		return getReagentOptions(medical_reagent_nameDTOs, sOptions, defaultValue);
	}
	
	public static String  getReagentsByDept(long defaultValue, int department_cat)
	{

		String sOptions = "";

		if(defaultValue == -1)
		{
			if(language.equalsIgnoreCase("English"))
			{
				sOptions = "<option value = '-1'>Select</option>";
			}
			else
			{
				sOptions = "<option value = '-1'>বাছাই করুন</option>";
			}
		}
		List<Medical_reagent_nameDTO> medical_reagent_nameDTOs = Medical_reagent_nameRepository.getInstance().getMedical_reagent_nameDTOBydepartment_cat(department_cat);
		
		return getReagentOptions(medical_reagent_nameDTOs, sOptions, defaultValue);
		
	}
	
	public static String  getEquipments(long defaultValue)
	{

		String sOptions = "";

		if(defaultValue == -1)
		{
			if(language.equalsIgnoreCase("English"))
			{
				sOptions = "<option value = '-1'>Select</option>";
			}
			else
			{
				sOptions = "<option value = '-1'>বাছাই করুন</option>";
			}
		}
		List<Medical_equipment_nameDTO> medical_equipment_nameDTOs = Medical_equipment_nameRepository.getInstance().getMedical_equipment_nameList();
		
		return getEquipmentOptions(medical_equipment_nameDTOs, sOptions, defaultValue);
	}
	
	public static final int DR_AND_THERAPIST = 1;
	public static final int DR = 2;
	public static final int THERAPIST = 3;
	public static final int LAB_TECHNITIAN = 4;
	
	public static String  getDoctorsByUserName()
	{
		String options = "<option value = '' ></option>";
		Set<Long> drs = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.DOCTOR_ROLE);
		for(Long dr: drs)
		{
			UserDTO drDTO = UserRepository.getUserDTOByOrganogramID(dr);
			if(drDTO != null)
			{
				options += "<option value = '" + drDTO.userName + "' >" 
						+ WorkflowController.getNameFromUserName(drDTO.userName, language) 
						+ "</option>";
			}
		}
		return options;
	}
	
	public static String  getTherapistsByUserName()
	{
		String options = "<option value = '' ></option>";
		Set<Long> drs = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.PHYSIOTHERAPIST_ROLE);
		for(Long dr: drs)
		{
			UserDTO drDTO = UserRepository.getUserDTOByOrganogramID(dr);
			if(drDTO != null)
			{
				options += "<option value = '" + drDTO.userName + "' >" 
						+ WorkflowController.getNameFromUserName(drDTO.userName, language) 
						+ "</option>";
			}
		}
		return options;
	}
	
	public static String  getDentalTechnologists(long defaultValue, long drId)
	{
		String options = "<option value = '' ></option>";
		Set<Long> drs = new HashSet<Long>();
		drs.add(drId);
		drs.addAll(EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.DENTAL_TECHNOLOGIST_ROLE));
		for(Long dr: drs)
		{
			logger.debug("dr id = " + dr);
			UserDTO drDTO = UserRepository.getUserDTOByOrganogramID(dr);
			if(drDTO != null)
			{
				EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(dr);
				options += "<option value = '" + dr + "'";
			
				if(dr == defaultValue)
				{
					options += " selected ";
				}
				options += " >" ;
				options += WorkflowController.getNameFromUserName(drDTO.userName, language) ;						
				options += "</option>";
			}
			else
			{
				logger.debug("userdto null for dr id = " + dr);
			}
		}
		return options;
	}
	
	
	public static String  getDoctorsAndTherapistsByOrganogramID(long defaultValue)
	{
		String options = "<option value = '' ></option>";
		Set<Long> drs = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.DOCTOR_ROLE);
		Set<Long> phys = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.PHYSIOTHERAPIST_ROLE);
		Set<Long> drsAndPhys = new HashSet<Long>();
		drsAndPhys.addAll(drs);
		drsAndPhys.addAll(phys);
		for(Long dr: drsAndPhys)
		{
			logger.debug("dr id = " + dr);
			UserDTO drDTO = UserRepository.getUserDTOByOrganogramID(dr);
			if(drDTO != null)
			{
				EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(dr);
				options += "<option value = '" + dr + "'";
				options +=  " medical_dept_cat = '" + employeeOfficeDTO.medicalDeptCat + "' ";
				options += " role = '" + (drs.contains(dr)?SessionConstants.DOCTOR_ROLE:SessionConstants.PHYSIOTHERAPIST_ROLE) + "' ";
				options += " dept = '" + employeeOfficeDTO.dept + "' ";
				if(dr == defaultValue)
				{
					options += " selected ";
				}
				options += " >" ;
				options += WorkflowController.getNameFromUserName(drDTO.userName, language) ;
				
				
				if(employeeOfficeDTO.medicalDeptCat >= 0)
				{
					options += ", " 
							+ CatDAO.getName(language, "medical_dept", employeeOfficeDTO.medicalDeptCat);
				}
				
						
				options += "</option>";
			}
			else
			{
				logger.debug("userdto null for dr id = " + dr);
			}
		}
		return options;
	}
	
	public static String  getDoctorsByOrganogramID(long defaultValue, String filter, int drType, boolean showAvailableOnly)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;

		String sOptions = "";
		String defaultOption = "<option></option>";
		
		String nameCol = "employee_records.name_eng";
		String designationCol = "designation_eng";
		if(!language.equalsIgnoreCase("english"))
		{
			nameCol = "employee_records.name_bng";
			designationCol = "designation_bng";
		}
		
		
		try{
			
			String sql = "SELECT distinct\r\n" + 
					"    office_unit_organograms.id,\r\n" + 
					"    " + designationCol +",\r\n" + 
					"    " + nameCol + ",\r\n" + 
					"    office_units.unit_name_eng,\r\n" + 
					"    employee_records.personal_mobile,\r\n" + 
					"    employee_records.personal_email,\r\n" + 
					"    employee_offices.dept,\r\n" + 
					"    employee_offices.medical_dept_cat,\r\n" + 
					"    employee_offices.organogram_role\r\n" + 
					"FROM\r\n" + 
					"    office_unit_organograms\r\n" + 
					"        JOIN\r\n" + 
					"    employee_offices ON (employee_offices.office_unit_organogram_id = office_unit_organograms.id\r\n" + 
					"        AND employee_offices.status = 1)\r\n" + 
					"        JOIN\r\n" + 
					"    employee_records ON employee_offices.employee_record_id = employee_records.id\r\n" + 
					"    join office_units on employee_offices.office_unit_id = office_units.id"
					+ " where employee_offices.isDeleted = 0 and ";
			if(drType == DR_AND_THERAPIST)
			{
				sql+= "(organogram_role = " + SessionConstants.DOCTOR_ROLE + " or organogram_role = " + SessionConstants.PHYSIOTHERAPIST_ROLE + ")";
			}
			else if(drType == DR) //only drs
			{
				sql+= "(organogram_role = " + SessionConstants.DOCTOR_ROLE + ")";
			}
			else if(drType == THERAPIST) //only therapists
			{
				sql+= "(organogram_role = " + SessionConstants.PHYSIOTHERAPIST_ROLE + ")";
			}
			else if(drType == LAB_TECHNITIAN) //only therapists
			{
				sql+= "(organogram_role = " + SessionConstants.LAB_TECHNITIAN_ROLE + ")";
			}
			
			if(showAvailableOnly)
			{
				sql+= " and isAvailable = 1";
			}
			
			
			
			if(!filter.equalsIgnoreCase(""))
			{
				sql+= " and " + filter;
			}
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			logger.debug(sql);

			rs = stmt.executeQuery(sql);
			


			while (rs.next())
			{
				int medicalDept = rs.getInt("employee_offices.medical_dept_cat");
				String sOption = "<option "
						+ "value = '" + rs.getLong("office_unit_organograms.id") + "'"
						+ "empname = '" + rs.getString(nameCol) + "'"
						+ "dept = '" + rs.getInt("employee_offices.dept") + "'"
						+ "medical_dept_cat = '" + medicalDept + "'"
						+ "role = '" + rs.getInt("employee_offices.organogram_role") + "'"
						+ "designation = '" + rs.getString(designationCol) + "'"
						+ "unitname = '" + rs.getString("office_units.unit_name_eng") + "'"
						+ "phonenumber = '" + rs.getString("employee_records.personal_mobile") + "'"
						+ "email = '" + rs.getString("employee_records.personal_email") + "'"
						+ ">" ;
			
				sOption += rs.getString(nameCol);
				if(medicalDept >= 0)
				{
					sOption += ", " 
							+ CatDAO.getName(language, "medical_dept", rs.getInt("employee_offices.medical_dept_cat"));
				}
				
				sOption += "</option>";
				
				 sOptions += sOption;
				   
			   if(defaultValue != -1 && rs.getLong("office_unit_organograms.id") == defaultValue)
			   {
				   defaultOption = sOption;
			   }
			}	
			
			
			
		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return defaultOption + sOptions;	
	}
	
	public static String  getEmployeesByOrganogramIDAndRole(long defaultValue, int role) 
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;

		String sOptions = "";
		String defaultOption = "<option></option>";
		
		String nameCol = "employee_records.name_eng";
		String designationCol = "designation_eng";
		if(!language.equalsIgnoreCase("english"))
		{
			nameCol = "employee_records.name_bng";
			designationCol = "designation_bng";
		}
		
		
		try{
			
			String sql = "SELECT \r\n" + 
					"    office_unit_organograms.id,\r\n" + 
					"    " + designationCol +",\r\n" + 
					"    " + nameCol + ",\r\n" + 
					"    office_units.unit_name_eng,\r\n" + 
					"    employee_records.personal_mobile,\r\n" + 
					"    employee_records.personal_email,\r\n" + 
					"    employee_offices.dept\r\n" + 
					"FROM\r\n" + 
					"    office_unit_organograms\r\n" + 
					"        JOIN\r\n" + 
					"    employee_offices ON (employee_offices.office_unit_organogram_id = office_unit_organograms.id\r\n" + 
					"        AND employee_offices.status = 1)\r\n" + 
					"        JOIN\r\n" + 
					"    employee_records ON employee_offices.employee_record_id = employee_records.id\r\n" + 
					"    join office_units on employee_offices.office_unit_id = office_units.id"
					+ " where "
					+ "(organogram_role = " + role + ")";
			
	
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			logger.debug(sql);

			rs = stmt.executeQuery(sql);
			


			while (rs.next())
			{
				String sOption = "<option "
						+ "value = '" + rs.getLong("office_unit_organograms.id") + "'"
						+ "empname = '" + rs.getString(nameCol) + "'"
						+ "dept = '" + rs.getInt("employee_offices.dept") + "'"
						+ "designation = '" + rs.getString(designationCol) + "'"
						+ "unitname = '" + rs.getString("office_units.unit_name_eng") + "'"
						+ "phonenumber = '" + rs.getString("employee_records.personal_mobile") + "'"
						+ "email = '" + rs.getString("employee_records.personal_email") + "'"
						+ ">" 
						+ rs.getString(nameCol) + ", " 
						+ rs.getString(designationCol) 
						+ "</option>";
				
				 sOptions += sOption;
				   
			   if(defaultValue != -1 && rs.getLong("office_unit_organograms.id") == defaultValue)
			   {
				   defaultOption = sOption;
			   }
			}	
			
			
			
		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return defaultOption + sOptions;	
	}
	
	public static String  getOrganogramsByOrganogramID(long defaultValue, String filter)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;

		String sOptions = "";
		String defaultOption = "<option></option>";
		
		String nameCol = "employee_records.name_eng";
		String designationCol = "designation_eng";
		if(!language.equalsIgnoreCase("english"))
		{
			nameCol = "employee_records.name_bng";
			designationCol = "designation_bng";
		}
		
		
		try{
			
			String sql = "SELECT distinct \r\n" + 
					"    office_unit_organograms.id,\r\n" + 
					"    " + designationCol +",\r\n" + 
					"    " + nameCol + ",\r\n" + 
					"    office_units.unit_name_eng,\r\n" + 
					"    employee_records.personal_mobile,\r\n" + 
					"    employee_records.personal_email,\r\n" + 
					"    employee_offices.dept\r\n" + 
					"FROM\r\n" + 
					"    office_unit_organograms\r\n" + 
					"        JOIN\r\n" + 
					"    employee_offices ON (employee_offices.office_unit_organogram_id = office_unit_organograms.id\r\n" + 
					"        AND employee_offices.status = 1)\r\n" + 
					"        JOIN\r\n" + 
					"    employee_records ON employee_offices.employee_record_id = employee_records.id\r\n" + 
					"    join office_units on employee_offices.office_unit_id = office_units.id";
			
			
			if(!filter.equalsIgnoreCase(""))
			{
				sql+= " where " + filter;
			}
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			logger.debug(sql);

			rs = stmt.executeQuery(sql);
			


			while (rs.next())
			{
				String sOption = "<option "
						+ "value = '" + rs.getLong("office_unit_organograms.id") + "'"
						+ "empname = '" + rs.getString(nameCol) + "'"
						+ "dept = '" + rs.getInt("employee_offices.dept") + "'"
						+ "designation = '" + rs.getString(designationCol) + "'"
						+ "unitname = '" + rs.getString("office_units.unit_name_eng") + "'"
						+ "phonenumber = '" + rs.getString("employee_records.personal_mobile") + "'"
						+ "email = '" + rs.getString("employee_records.personal_email") + "'";
				if(defaultValue != -1 && rs.getLong("office_unit_organograms.id") == defaultValue)
				{
					sOption += " selected ";
				}
				sOption += ">" 
						+ rs.getString(nameCol) + ", " 
						+ rs.getString(designationCol) 
						+ "</option>";
				
				 sOptions += sOption;
			}	
			
			
			
		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		System.out.println("..............>");
		System.out.println(defaultOption + sOptions);
		System.out.println("..............>");
		return defaultOption + sOptions;
	}
	
	public static String  getOptions (String Language, String HtmlType, String TableName, String rawHtmlID, String htmlClass, String htmlName)
	{
		return getOptions (Language, HtmlType, TableName, rawHtmlID, htmlClass, htmlName, "", "", "", "");
	}
	
	public static String  getOptions (String Language, String HtmlType, String TableName, String rawHtmlID, String htmlClass, String htmlName, String DefaultValue)
	{
		return getOptions (Language, HtmlType, TableName, rawHtmlID, htmlClass, htmlName, "", "", DefaultValue, "");
	}
	public static String  getOptions (String HtmlType, String TableName, String rawHtmlID, String htmlClass, String htmlName, String DefaultValue, boolean dummy)
	{
		return getOptions ("English", HtmlType, TableName, rawHtmlID, htmlClass, htmlName, "", "name", DefaultValue, "");
	}
	public static String  getOptions (String HtmlType, String TableName, String rawHtmlID, String htmlClass, String htmlName, String DefaultValue, String nameColumn, boolean bummy)
	{
		return getOptions ("English", HtmlType, TableName, rawHtmlID, htmlClass, htmlName, "", nameColumn, DefaultValue, "");
	}
	
	public static String  getOptions (String Language, String TableName, String engColumnName, String bngColumnName)
	{
		if(Language.equals("English"))
		{
			return getOptions ("English", "select", TableName, "", "", "", "", engColumnName, "", "");
		}
		else
		{
			return getOptions ("Bangla", "select", TableName, "", "", "", "", bngColumnName, "", "");
		}
	}
	
	public static String  getOptions (String Language, String TableName)
	{
		if(Language.equals("English"))
		{
			return getOptions ("English", "select", TableName, "", "", "", "", "name_en", "", "");
		}
		else
		{
			return getOptions ("Bangla", "select", TableName, "", "", "", "", "name_bn", "", "");
		}
	}
	
	public static String  getOptionsWithNameColumn (String nameCol, String tableName, long defaultValue)
	{
		return getOptionsWithNameColumn ( nameCol,  tableName,  defaultValue, "");
	}
	
	
	
	public static String  getOptionsWithNameColumn (String nameCol, String tableName, long defaultValue, String filter)
	{
		
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;

		String sOptions = "";
		
		if (defaultValue == CatDTO.CATDEFAULTNOTREQUIRED) 
		{
        	if (language.equalsIgnoreCase("English"))
	       	 {
        		sOptions += "<option value = '" + CatDTO.CATDEFAULTNOTREQUIRED + "'>Select</option>";
	       	 }
	       	 else
	       	 {
	       		sOptions += "<option value = '" + CatDTO.CATDEFAULTNOTREQUIRED + "'>বাছাই করুন</option>";
	       	 }
        }
		else if (defaultValue == CatDTO.CATDEFAULT) 
		{
        	if (language.equalsIgnoreCase("English"))
	       	 {
        		sOptions += "<option value = ''>Select</option>";
	       	 }
	       	 else
	       	 {
	       		sOptions += "<option value = ''>বাছাই করুন</option>";
	       	 }
        }
		
		try{
			
			String sql = "SELECT \r\n" + 
					"    id,\r\n" + 
					"    " + nameCol + "\r\n" + 
					"FROM\r\n" + 
					"    " + tableName + "\r\n" + 
					"    where isDeleted = 0";
			
			if(filter != null && !filter.equalsIgnoreCase(""))
			{
				sql += " and " + filter;
			}
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while (rs.next())
			{
				String sOption =  "<option value = '" + rs.getLong("id") + "'";
				if(defaultValue != CatDTO.CATDEFAULTNOTREQUIRED && rs.getLong("id") == defaultValue)
				{
					sOption += " selected ";
				}
				sOption += ">" 
				+ rs.getString(nameCol)
				+ "</option>";
				sOptions += sOption;
			}
			
		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return sOptions;		
	}
	
	public static String getOptions (String language, String tableName, long defaultValue, int sortingOption)
	{
		if(PBNameDTO.Table.contains(tableName))
		{
			System.out.println("Getting options from repo for table = " + tableName);
			PBNameRepository pBNameRepository = PBNameRepository.getInstance();
			return pBNameRepository.getOptions(language, tableName, defaultValue, sortingOption);
		}
		System.out.println("Not Getting options from repo for table = " + tableName);
		String nameCol = "name_en";
		if(!language.equalsIgnoreCase("english"))
		{
			nameCol = "name_bn";
		}
		return getOptionsWithNameColumn(nameCol, tableName, defaultValue);
	}
	
	public static String getOptions (String language, String tableName, long defaultValue)
	{		
		return getOptions(language, tableName, defaultValue, CatRepository.DEFAULT_SORT);
	}
	
	public static String getOptionsWithWhere (String language, String tableName, long defaultValue, String filter)
	{
		String nameCol = "name_en";
		if(!language.equalsIgnoreCase("english"))
		{
			nameCol = "name_bn";
		}
		return getOptionsWithNameColumn(nameCol, tableName, defaultValue, filter);
	}
	
	public static String  getOptions (String Language, String tableName, long defaultValue, String engColumnName, String bngColumnName)
	{
		String nameCol = engColumnName;
		if(!language.equalsIgnoreCase("english"))
		{
			nameCol = bngColumnName;
		}
		return getOptionsWithNameColumn(nameCol, tableName, defaultValue);
	}
	
	public static String  getOptionsWithTableName (String TableName, String engColumnName)
	{
		
		return getOptions ("English", "select", TableName, "", "", "", "", engColumnName, "", "");
		
	}
	
	public static String  getOptions (String TableName)
	{
		
		return getOptions ("English", "select", TableName, "", "", "", "", "name_en", "", "");
		
	}
	
	public static String  getOptionsWithDefault (String TableName, long defaultVal)
	{
		
		return getOptions ("English", "select", TableName, "", "", "", "", "name_en", defaultVal + "", "");
		
	}
	
	public static String getOptionsWithMultiDefault(String TableName, String defaultVal)
	{
	
		return getOptionsWithMultiDefault ("English", TableName, "name_en", defaultVal, "");
	}
	
	public static String  getOptionsWithDefault (String TableName, String defaultVal)
	{
		
		return getOptions ("English", "select", TableName, "", "", "", "", "name_en", defaultVal + "", "");
		
	}
	
	public static String  getOptionsWithDefault (String TableName, String engColumnName, String defaultValue)
	{
		
		return getOptions ("English", "select", TableName, "", "", "", "", engColumnName, defaultValue, "");
		
	}
	
	public static String  getOptionsWithDefault (String TableName, String engColumnName, long defaultValue)
	{
		
		return getOptions ("English", "select", TableName, "", "", "", "", engColumnName, defaultValue + "", "");
		
	}
	
	public static String  getOptionsWithDefault (String TableName, String engColumnName, int defaultValue)
	{
		
		return getOptions ("English", "select", TableName, "", "", "", "", engColumnName, defaultValue + "", "");
		
	}
	
	public static String  getOptionsWithWhere (String Language, String TableName, String whereClause)
	{
		if(Language.equals("English"))
		{
			return getOptions ("English", "select", TableName, "", "", "", "", "name_en", "", whereClause);
		}
		else
		{
			return getOptions ("Bangla", "select", TableName, "", "", "", "", "name_bn", "", whereClause);
		}
	}
	
	public static String  getPagesForRole (String Language, long roleId)
	{
		String whereClause = " role_actions_id = (select id from role_actions where role_type = " + roleId 
				+ " order by lastModificationTime desc limit 1)";
		if(Language.equals("English"))
		{
			return getOptions ("English", "select", "action_description", "", "", "", "", "name_en", "", whereClause);
		}
		else
		{
			return getOptions ("Bangla", "select", "action_description", "", "", "", "", "name_bn", "", whereClause);
		}
	}
	
	public static String  getOptions (String Language, String TableName, String engColumnName, String bngColumnName, String DefaultValue)
	{
		if(Language.equals("English"))
		{
			return getOptions ("English", "select", TableName, "", "", "", "", engColumnName, DefaultValue, "");
		}
		else
		{
			return getOptions ("Bangla", "select", TableName, "", "", "", "", bngColumnName, DefaultValue, "");
		}
	}
	
	public static String  parseName (String Language, String Value)
	{
		StringTokenizer tok3=new StringTokenizer(Value, ":");
		int i = 0;
		String name_en = "", name_bn = "";
		while(tok3.hasMoreElements())
		{
			if(i == 0)
			{
				name_en = tok3.nextElement() + "";
			}
			else
			{
				name_bn = tok3.nextElement() + "";
			}

			i ++;
		}
		if(Language.equals("English"))
		{
			return name_en;
		}
		else
		{
			return name_bn;
		}
	}

	public static String  getEmpPayScale(long defaultValue, String Language)
	{
		String sOptions = "";
		if (Language.equals("English")) {
			sOptions = "<option value = ''>---Select---</option>";
		} else {
			sOptions = "<option value = ''>অনুগ্রহ করে নির্বাচন করুন</option>";
		}
		Employee_pay_scaleDAO employee_pay_scaleDAO = new Employee_pay_scaleDAO();
		List<Employee_pay_scaleDTO> employee_pay_scaleDTOS =
				employee_pay_scaleDAO.getAllEmployee_pay_scale(true);
//				Employee_pay_scaleRepository.getInstance().getEmployee_pay_scaleList();




		return getEmpPayScaleOptions(employee_pay_scaleDTOS, sOptions, defaultValue, language);
	}

	public static String getEmpPayScaleOptions
			(List<Employee_pay_scaleDTO> employee_pay_scaleDTOS, String sOptions, long defaultValue, String language)
	{
		for(Employee_pay_scaleDTO employee_pay_scaleDTO : employee_pay_scaleDTOS)
		{
			String sOption = "<option "
					+ "value = '" + employee_pay_scaleDTO.iD + "'";

			if(defaultValue == employee_pay_scaleDTO.iD)
			{
				sOption		+= "selected";
			}

			String data = CatRepository.getName(language, "national_pay_scale_type", employee_pay_scaleDTO.nationalPayScaleCat);
            sOption		+= ">" + data + "</option>";
			sOptions += sOption;

		}

		return sOptions;
	}

	public static String  getEmpPayScaleByjobGradeCat(long defaultValue, String Language, Integer jobGradeCat) {
		String sOptions = "";
		if (Language.equals("English")) {
			sOptions = "<option value = ''>Select</option>";
		} else {
			sOptions = "<option value = ''>অনুগ্রহ করে নির্বাচন করুন</option>";
		}


		List<Employee_pay_scaleDTO> employee_pay_scaleDTOS =
				Employee_pay_scaleRepository.getInstance().getEmployee_pay_scaleList()
						.stream()
						.filter(i -> i.isActive == 1 && i.jobGradeCat == jobGradeCat)
						.collect(Collectors.toList());

		return getEmpPayScaleOptions(employee_pay_scaleDTOS, sOptions, defaultValue, language);
	}

	public static String getNameFromCategoryTable(String Language, String domainName, int value) throws SQLException {
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		String previouslySeletedOption = "";

		String languagetextColumn = Language.equalsIgnoreCase("English") ? "languageTextEnglish" : "languageTextBangla";
		try{

			 final String getOptionSQL = "SELECT "+ languagetextColumn + " FROM language_text " +
					"WHERE ID=(SELECT language_id from category WHERE domain_name=\""+ domainName +"\" AND value="+ value +")";

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			logger.debug(getOptionSQL);

			rs = stmt.executeQuery(getOptionSQL);
			if (rs.next()) {
				logger.debug("getNameFromCategoryTable: " + rs.getString(languagetextColumn));
				previouslySeletedOption =  rs.getString(languagetextColumn);
			}

		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null){
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}

		return previouslySeletedOption;
		
	}



}
