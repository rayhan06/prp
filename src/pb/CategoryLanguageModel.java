package pb;

public class CategoryLanguageModel {
    public long id;
    public String domainName;
    public long languageId;
    public int categoryValue;
    public String englishText;
    public String banglaText;
    public int isDeleted;
    public int ordering = 1;
    public String parentDomain = "";
	public int parentDomainVal = -1;
}
