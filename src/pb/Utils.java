package pb;

import common.*;
import files.FilesDAO;
import files.FilesDTO;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import repository.Repository;
import repository.RepositoryManager;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.HttpRequestUtils;
import util.NavigationService4;
import util.StringUtils;

import javax.imageio.ImageIO;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Connection;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.List;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@SuppressWarnings({"unused", "Duplicates"})
public class Utils {
    private static final ExecutorService cachedThreadPool = Executors.newCachedThreadPool((task) -> {
        Thread thread = new Thread(task);
        thread.setDaemon(true);
        return thread;
    });

    private static final ExecutorService singleThreadPool = Executors.newSingleThreadExecutor((task) -> {
        Thread thread = new Thread(task);
        thread.setDaemon(true);
        return thread;
    });
    private static final Logger logger = Logger.getLogger(Utils.class);
    private static final char[] banglaDigits = {'০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯'};
    private static final char[] englishDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    private static final Pattern MOBILE_PATTERN = Pattern.compile("^8801[3-9][0-9]{8}$");
    private static final Pattern DIGIT_ONLY_OR_EMPTY_PATTERN = Pattern.compile("[0-9]*");
    private static final Pattern EMAIL_PATTERN = Pattern.compile("([a-zA-Z0-9_\\-.]+)@([a-zA-Z0-9_\\-.]+)\\.([a-zA-Z]{2,5})");
    public static String[] reservedSQLWords = {" and ", " where ", "select ", " union ", " update ", " alter ", " delete ", " database ",
            " drop ", " create ", " insert ", " join "};
    public static final String selectEngText = "Select";
    public static final String selectBngText = "বাছাই করুন";
    public static final long HOUR6_IN_MILLIS = 21600000;
    public static boolean isValidSearchString(String str) {
        if ((str.matches("^[a-zA-Z0-9_@/.,\\u0980-\\u09FF ]*$")) && str.length() <= 100) {
            String smallStr = str.toLowerCase();
            return Arrays.stream(reservedSQLWords)
                         .filter(smallStr::contains)
                         .findAny()
                         .orElse(null) == null;
        }
        return false;
    }


    public static boolean isValidReportSearchString(String paramName, String str) {
        if (paramName.contentEquals("columns")) {
            return true;
        }
        if ((str.matches("^-?[,a-zA-Z0-9_@/.\\u0980-\\u09FF ]*$")) && str.length() <= 50) {
            String smallStr = str.toLowerCase();
            return Arrays.stream(reservedSQLWords)
                         .filter(smallStr::contains)
                         .findAny()
                         .orElse(null) == null;
        }
        return false;
    }

    public static boolean isValidUserName(String userName) {
        return userName.equalsIgnoreCase("superadmin") ||
               (userName.length() <= 10 && StringUtils.isEngNumber(StringUtils.convertToEngNumber(userName)));
    }
    
    public static String ArraylistToString(List<Long> list)
    {
    	String str = "";
    	if(list == null || list.isEmpty())
    	{
    		return "";
    	}
    	for(Long l: list)
    	{
    		str += l + ", ";
    	}
    	return str;
    }
    
    public static boolean isPrivillegedHRUser(UserDTO userDTO)
    {
        return userDTO.roleID == RoleEnum.ADMIN.getRoleId()
                || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId()
                || userDTO.roleID == RoleEnum.DATA_ENTRY.getRoleId()
                || userDTO.roleID == SessionConstants.ADMIN_ROLE;
    }
    
    public static List<Long> StringToArrayList(String str)
    {
    	if(str != null)
    	{
    		str = str.replaceAll(" ", "");
    	}
    	
    	if(str == null || str.equals("") )
    	{
    		return new ArrayList<Long>();
    	}
    	String []strArr = str.split(",");
    	List<Long> longs = new ArrayList<Long>();
    	for(String elem: strArr)
    	{
    		longs.add(Long.parseLong(elem));
    	}
    	return longs;
    }

    public static boolean isValidPhone(String phone) {
        return phone.length() <= 13 && org.apache.commons.lang3.StringUtils.isNumeric(phone);
    }

    public static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    public static String getDigitBanglaFromEnglish(String number) {
        if (number == null)
            return "";
        StringBuilder builder = new StringBuilder();
        try {
            for (int i = 0; i < number.length(); i++) {
                if (Character.isDigit(number.charAt(i))) {
                    if (((int) (number.charAt(i)) - 48) <= 9) {
                        builder.append(banglaDigits[(int) (number.charAt(i)) - 48]);
                    } else {
                        builder.append(number.charAt(i));
                    }
                } else {
                    builder.append(number.charAt(i));
                }
            }
        } catch (Exception e) {
            // logger.debug("getDigitBanglaFromEnglish: ",e);
            return "";
        }
        String str = builder.toString();
        str = str.replace(" AM", " (সকাল)");
        str = str.replace(" PM", " (বিকাল)");

        str = str.replace("Jan", "জানু");
        str = str.replace("Feb", "ফেব্");
        str = str.replace("Mar", "মার্চ");
        str = str.replace("Apr", "এপ্রি");
        str = str.replace("May", "মে");
        str = str.replace("Jun", "জুন");
        str = str.replace("Jul", "জুলা");
        str = str.replace("Aug", "আগ");
        str = str.replace("Sep", "সেপ্টে");
        str = str.replace("Oct", "অক্ট");
        str = str.replace("Nov", "নভে");
        str = str.replace("Dec", "ডিসে");

        return str;
    }

    public static final String getMonth(int month, boolean isLangEng) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, month - 1);
        String sMonth = simpleDateFormat.format(cal.getTime());
        if (isLangEng) {
            return sMonth;
        } else {
            return getDigitBanglaFromEnglish(sMonth);
        }
    }

    public static boolean isValidBanglaCharacter(int characterInt) {
        char c = (char) characterInt;
        if (Character.UnicodeBlock.of(c) == Character.UnicodeBlock.BENGALI) return true;
        return (32 <= c && c <= 47)
               || (58 <= c && c <= 64)
               || (91 <= c && c <= 96)
               || (123 <= c && c <= 126);
    }

    public static String getDigitEnglishFromBangla(String number) {
        if (number == null)
            return "";
        StringBuilder builder = new StringBuilder();
        try {
            for (int i = 0; i < number.length(); i++) {
                if (number.charAt(i) >= '০' && number.charAt(i) <= '৯') {
                    builder.append(englishDigits[(int) (number.charAt(i)) - '০']);
                } else {
                    builder.append(number.charAt(i));
                }
            }
        } catch (Exception e) {
            return "";
        }
        return builder.toString();
    }

    public static String camelToSnake(String str) {
        StringBuilder result = new StringBuilder();
        char c = str.charAt(0);
        result.append(Character.toLowerCase(c));
        for (int i = 1; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (Character.isUpperCase(ch)) {
                result.append('_');
                result.append(Character.toLowerCase(ch));
            } else {
                result.append(ch);
            }
        }
        return result.toString();
    }

    public static String getDigits(int number, String Language, boolean addComma) {
        if (addComma) {
            return getDigits(String.format("%,d", number), Language);
        } else {
            return getDigits(String.valueOf(number), Language);
        }

    }

    public static String getDigits(int number, String Language) {
        return getDigits(String.valueOf(number), Language);
    }

    public static String getDigits(long number, String Language) {
        return getDigits(String.valueOf(number), Language);
    }

    public static String getDigits(long number, boolean isLangEng) {
        return getDigits(String.valueOf(number), isLangEng);
    }

    public static String getDigits(float number, String Language) {
        return getDigits(number + "", Language);
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static String getDigits(double number, String Language) {
        //DecimalFormat formatter = new DecimalFormat("#,###.00");
        return getDigits(String.format("%,.2f", number) + "", Language);
    }

    public static String translateIfNeed(String word, String language) {
        if (!language.equalsIgnoreCase("english")) {
            if (word.equalsIgnoreCase("Absent"))
                return "অনুপস্থিত";
            else return "";
        }
        return word;
    }


    public static String getDigits(String number, String Language) {
        return getDigits(number, Language.equalsIgnoreCase("english"));
    }

    public static String getDigits(String number, boolean isLangEng) {
        if (number == null || number.equalsIgnoreCase("null")) {
            return "";
        }
        if (isLangEng) {
            return getDigitEnglishFromBangla(number);
        } else {
            return getDigitBanglaFromEnglish(number);
        }
    }

    public static String getPhoneNumberWithout88(String number, String Language) {
        if (number == null || number.length() < 2 || !org.apache.commons.lang3.StringUtils.isNumeric(number)) {
            return "";
        }
        number = number.substring(2);
        if (Language.equalsIgnoreCase("english")) {
            return getDigitEnglishFromBangla(number);
        } else {
            return getDigitBanglaFromEnglish(number);
        }
    }

    public static String getYesNo(String flag, String Language) {
        return getYesNo(Boolean.parseBoolean(flag), Language);
    }

    public static String getApprovalStatus(int val, String Language) {
        if (Language.equalsIgnoreCase("english")) {
            if (val == 0) {
                return "Pending";
            } else if (val == 1) {
                return "Approved";
            } else {
                return "Rejected";
            }
        } else {
            if (val == 0) {
                return "অনুমোদনের অপেক্ষায়";
            } else if (val == 1) {
                return "অনুমোদিত";
            } else {
                return "বাতিল";
            }
        }
    }

    public static String capitalizeFirstLetter(String str) {
        str = str.toLowerCase();
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    public static String capitalizeFirstLettersOfWords(String message) {
        char[] charArray = message.toLowerCase().toCharArray();
        boolean foundSpace = true;

        for (int i = 0; i < charArray.length; i++) {
            if (Character.isLetter(charArray[i])) {
                if (foundSpace) {
                    charArray[i] = Character.toUpperCase(charArray[i]);
                    foundSpace = false;
                }
            } else {
                foundSpace = true;
            }
        }
        message = String.valueOf(charArray);
        return message;
    }

    public static String getYesNo(boolean flag, String Language) {
        return StringUtils.getYesNo(Language, flag);
    }

    public static void ProcessFile(HttpServletRequest request, HttpServletResponse response, String sourceName, FilesDTO filesDTO) {

        try {
            String mimeType = filesDTO.fileTypes;
            response.setContentType(mimeType);
            response.setContentLength(filesDTO.inputStream.available());
            String headerKey = "Content-Disposition";
            String headerValue = String.format("inline; filename=\"%s\"", sourceName);
            response.setHeader(headerKey, headerValue);

            try {
                OutputStream outStream = response.getOutputStream();
                byte[] buf = new byte[1024];
                int len;
                while ((len = filesDTO.inputStream.read(buf)) != -1) {
                    outStream.write(buf, 0, len);
                }
                outStream.close();
            } catch (Exception ex) {
                logger.error("",ex);
            }

        } catch (Exception e) {
            logger.error("",e);
        }
    }

    public static void ProcessFileInline(HttpServletRequest request, HttpServletResponse response, String sourceName, byte[] sourceData) {
        try {
            String mimeType = "application/pdf";
            response.setContentType(mimeType);
            response.setContentLength(sourceData.length);
            String headerKey = "Content-Disposition";
            String headerValue = String.format("inline; filename=\"%s\"", sourceName);
            response.setHeader(headerKey, headerValue);

            try {
                OutputStream outStream = response.getOutputStream();
                outStream.write(sourceData);
                outStream.close();
            } catch (Exception ex) {
                logger.error("",ex);
            }
        } catch (Exception e) {
            logger.error("",e);
        }
    }

    public static void ProcessFile(HttpServletRequest request, HttpServletResponse response, String sourceName, byte[] sourceData) {
        try {
            String mimeType = "application/octet-stream";
            response.setContentType(mimeType);
            response.setContentLength(sourceData.length);
            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"", sourceName);
            response.setHeader(headerKey, headerValue);
            try {
                OutputStream outStream = response.getOutputStream();
                outStream.write(sourceData);
                outStream.close();
            } catch (Exception ignored) {
            }
        } catch (Exception ignored) {
        }
    }

    public static void setFileName(HttpServletRequest request, HttpServletResponse response, String fileName) {

        String userAgent = request.getHeader("user-agent");
        boolean isInternetExplorer = (userAgent.indexOf("MSIE") > -1);

        try {
            byte[] fileNameBytes = fileName.getBytes((isInternetExplorer) ? ("windows-1250") : ("utf-8"));
            String dispositionFileName = "";
            for (byte b : fileNameBytes) dispositionFileName += (char) (b & 0xff);

            String disposition = "attachment; filename=\"" + dispositionFileName + "\"";
            response.setHeader("Content-disposition", disposition);
        } catch (UnsupportedEncodingException ence) {
            // ... handle exception ...
        }
    }


    public static void ProcessFile(HttpServletRequest request, HttpServletResponse response, String sourceName, InputStream sourceData) {

        try {

            String mimeType = "application/octet-stream";
            logger.debug("started");

            // set content properties and header attributes for the response
            response.setContentType(mimeType);
            response.setContentLength(sourceData.available());
            //String headerKey = "Content-Disposition";
            //String headerValue = String.format("attachment; filename*=UTF-8''%s", Uri.EscapeDataString(sourceName));
            //response.setHeader(headerKey, headerValue);
            setFileName(request, response, sourceName);
            try {
                // writes the file to the client
                OutputStream outStream = response.getOutputStream();

                IOUtils.copy(sourceData, outStream);
                sourceData.close();
                outStream.close();
                logger.debug("ended");

            } catch (Exception ignored) {

            }

        } catch (Exception ignored) {

        }

    }

    public static void setFileNameForDownload(HttpServletResponse response, String sourceName) {

        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", sourceName);
        response.setHeader(headerKey, headerValue);
    }

    public static void writeZipToStreamFromFilesDTOList(List<FilesDTO> filesDTOList, OutputStream os) throws IOException {

        ZipOutputStream zos = new ZipOutputStream(os);
        byte[] buffer = new byte[1024];

        for (FilesDTO filesDTO : filesDTOList) {

            zos.putNextEntry(new ZipEntry(filesDTO.iD + "_" + filesDTO.fileTitle));

            int lenght;

            while ((lenght = filesDTO.inputStream.read(buffer)) > 0) {

                zos.write(buffer, 0, lenght);
            }
            zos.closeEntry();
            filesDTO.inputStream.close();
        }

        zos.close();
    }

    public static BufferedImage createImageFromBytes(byte[] imageData) {
        ByteArrayInputStream bais = new ByteArrayInputStream(imageData);
        try {
            return ImageIO.read(bais);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static BufferedImage resize(BufferedImage img, int newW, int newH) {
        Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
        BufferedImage dimg = new BufferedImage(newW, newH, img.getType());

        Graphics2D g2d = dimg.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();

        return dimg;
    }

    public static byte[] getIcon(byte[] fileBlob, String fileType) {
        BufferedImage img = createImageFromBytes(fileBlob); // load image
        double ratio = img.getHeight() * 1.0 / img.getWidth();
        BufferedImage resized = resize(img, 100, (int) (100 * ratio));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ImageIO.write(resized, fileType, baos);
            baos.flush();
            byte[] imageInByte = baos.toByteArray();
            baos.close();
            return imageInByte;
        } catch (IOException e) {
            logger.error("",e);
        }
        return null;
    }

    private static String getFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        logger.debug("content-disposition header= " + contentDisp);
        String[] tokens = contentDisp.split(";");
        for (String token : tokens) {
            if (token.trim().startsWith("filename")) {
                return token.substring(token.indexOf("=") + 2, token.length() - 1);
            }
        }
        return "";
    }

    public static long uploadSingleFile(Part part, long userID, String servletName) {
        FilesDAO filesDAO = new FilesDAO();
        return uploadSingleFile(part, filesDAO, userID, -1, servletName);
    }

    public static int MAX_FILE_SIZE = 10_485_760;
    public static String[] allowedExtensions = {"png", "jpeg", "gif", "doc", "docx", "pdf",
            "txt", "xls", "xlsx", "mp3", "MP3", "mp4", "avi", "mkv", "jpg", "bmp"};
    public static List<String> allowedContentTypes = Arrays.asList(
            "image/png",
            "image/jpeg",
            "image/gif",
            "application/pdf",
            "text/plain",
            "application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "application/vnd.ms-excel",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "audio/mpeg",
            "video/mp4",
            "video/x-msvideo",
            "video/x-matroska",
            "image/bmp"
    );

    public static long uploadSingleFile(Part part, FilesDAO filesDAO, long userID, long fileID, String servletName) {
        String fileName = getFileName(part);

        logger.debug("part name and file name and file id : " + part.getName() + ", " + part.getName()
                     + ", " + fileID);


        FilesDTO filesDTO = new FilesDTO();

        filesDTO.fileTitle = fileName;
        boolean validExtension = false;
        for (String allowedExtension : allowedExtensions) {
            if (filesDTO.fileTitle.toLowerCase().endsWith("." + allowedExtension)) {
                validExtension = true;
                break;
            }
        }

        if (!validExtension) {
            logger.debug("Invalid extension");
            return -1;
        }

        if (!allowedContentTypes.contains(part.getContentType())) {
            logger.debug("Invalid content type");
            return -1;
        }

        filesDTO.userId = userID;
        filesDTO.fileID = fileID;
        try {
            filesDTO.inputStream = part.getInputStream();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        filesDTO.createdAt = System.currentTimeMillis();
        filesDTO.fileTypes = part.getContentType();
        filesDTO.fileTag = servletName;
        try {
            if (filesDTO.inputStream.available() > 0) {
                if (filesDTO.inputStream.available() > MAX_FILE_SIZE) {
                    logger.debug("Too big file");
                    return -5; // tag for file size exceeds
                }
                logger.debug("adding to filesDTO");

                String[] fileParams = filesDTO.fileTypes.split("/");

                for (int i = 0; i < fileParams.length; i++) {
                    logger.debug("fileParams[" + i + "] = " + fileParams[i]);
                }
                if (fileParams.length >= 2) {
                    if (fileParams[0].equals("image")) {
                        filesDTO.thumbnailBlob = getIcon(Utils.uploadFileToByteAray(part), fileParams[1]);
                    }
                }
                try {
                    return filesDAO.add(filesDTO);
                } catch (Exception e) {
                    logger.error("",e);
                }
            }
        } catch (IOException e) {
            logger.error("",e);
        }
        return -1;
    }

    public static void UploadFilesFromDropZone(HttpServletRequest request, HttpServletResponse response, long userID, long fileID, String servletName) {

        try {
            FilesDAO filesDAO = new FilesDAO();
            //List<FilesDTO> filesDTOList = new ArrayList();

            logger.debug("In UploadFilesFromDropZone method");
            int partcount = 0;

            for (Part part : request.getParts()) {
                partcount++;


                if (part.getContentType() != null) {

                    long id = uploadSingleFile(part, filesDAO, userID, fileID, servletName);

                    if (id != -1 && id != -5) {
                        String deleteURL = servletName + "?actionType=DeleteFileFromDropZone&id=" + id;
                        response.getWriter().write(deleteURL);
                    } else {
                        if (id == -5) {
                            response.getWriter().write("max_file_size_exceeds");
                        }
                        logger.debug("File upload failed");
                    }

                } else {
                    logger.debug("part.getContentType() is null");
                }
            }
            logger.debug("partcount = " + partcount);
        } catch (Exception e) {
            logger.error("",e);
        }

    }

    public static byte[] readFile(String file) {
        ByteArrayOutputStream bos = null;
        try {
            File f = new File(file);
            FileInputStream fis = new FileInputStream(f);
            byte[] buffer = new byte[1024];
            bos = new ByteArrayOutputStream();
            for (int len; (len = fis.read(buffer)) != -1; ) {
                bos.write(buffer, 0, len);
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return bos != null ? bos.toByteArray() : null;
    }

    public static String uploadFileByIndex(String fieldName, String path, int index, HttpServletRequest request) {
        int j = 0;
        String uploadedFileName = "";
        try {
            for (Part part : request.getParts()) {
                String fileName = part.getName();
                String name = part.getName();
                if (name.equalsIgnoreCase(fieldName)) {
                    if (j == index) {
                        if (fileName != null && !fileName.equalsIgnoreCase("")) {
                            fileName = Jsoup.clean(fileName, Whitelist.simpleText());
                            uploadedFileName = name + "_" + index + "_" + fileName;
                            logger.debug("... UPLOADING " + fileName + ", j = " + j + " name = " + name);
                            Utils.uploadFile(part, uploadedFileName, path);
                        }
                    }

                    j++;
                }

            }
        } catch (IOException | ServletException e) {
            logger.error("",e);
        }
        return uploadedFileName;
    }

    public static boolean isValidInputFile(final Part part) {
        String fileName = getFileNameFromPart(part);
        if (fileName != null) {
            return fileName.toLowerCase().endsWith(".jpg") || fileName.toLowerCase().endsWith(".png")
                   || fileName.toLowerCase().endsWith(".gif") || fileName.toLowerCase().endsWith(".bmp")
                   || fileName.toLowerCase().endsWith(".ico");
        }
        return false;
    }

    public static String getFileNameFromPart(final Part part) {
        if (part == null) {
            return null;
        }
        final String partHeader = part.getHeader("content-disposition");
        logger.debug("Part Header = {0}" + partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    public static byte[] uploadFileToByteAray(Part filePart) {

        OutputStream out = null;
        InputStream filecontent = null;

        try {
            filecontent = filePart.getInputStream();
            return IOUtils.toByteArray(filecontent);

        } catch (IOException fne) {
            logger.debug("You either did not specify a file to upload or are "
                         + "trying to upload a file to a protected or nonexistent "
                         + "location.");
            logger.debug("ERROR: " + fne.getMessage());
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    logger.error("",e);
                }
            }
            if (filecontent != null) {
                try {
                    filecontent.close();
                } catch (IOException e) {
                    logger.error("",e);
                }
            }
        }
        return null;
    }

    public static void uploadFile(Part filePart, String fileName, String path) {

        OutputStream out = null;
        InputStream filecontent = null;


        File dir = new File(path);

        if (!dir.exists()) {
            dir.mkdir();
            logger.debug("created directory " + path);
        }

        try {
            out = new FileOutputStream(path + File.separator
                                       + fileName);
            filecontent = filePart.getInputStream();

            int read;
            final byte[] bytes = new byte[1024];

            while ((read = filecontent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            logger.debug("New file " + fileName + " created at " + path);

        } catch (IOException fne) {
            logger.debug("You either did not specify a file to upload or are "
                         + "trying to upload a file to a protected or nonexistent "
                         + "location.");
            logger.debug("ERROR: " + fne.getMessage());
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    logger.error("",e);
                }
            }
            if (filecontent != null) {
                try {
                    filecontent.close();
                } catch (IOException e) {
                    logger.error("",e);
                }
            }
        }
    }

    public static void addSuccessMessage(HttpServletRequest request, String msg) {

        request.getSession().setAttribute(SessionConstants.SUCCESS_MESSAGE, msg);
    }

    public static void addErrorMessage(HttpServletRequest request, String msg) {

        request.getSession().setAttribute(SessionConstants.ERROR_MESSAGE, msg);
    }

    public static String buildSelectOption(boolean isLanguageEnglish) {
        return buildSelectOption(isLanguageEnglish, selectEngText, selectBngText);
    }

    public static String buildSelectOption(boolean isLanguageEnglish, String selectTextEng, String selectTextBng) {
        StringBuilder selectOption = new StringBuilder();
        selectOption.append("<option value = ''>");
        if (isLanguageEnglish) {
            selectOption.append(selectTextEng).append("</option>");
        } else {
            selectOption.append(selectTextBng).append("</option>");
        }
        return selectOption.toString();
    }

    private static String buildOption(OptionDTO optionDTO, boolean isLanguageEng, String selectedId) {
        StringBuilder option = new StringBuilder("<option");
        if (selectedId != null && selectedId.equals(optionDTO.value)) {
            option.append(" selected");
        }
        return option.append(" title = '")
                     .append(optionDTO.title)
                     .append("'")
                     .append(" value = '")
                     .append(optionDTO.value)
                     .append("'>")
                     .append(isLanguageEng ? optionDTO.englishText : optionDTO.banglaText)
                     .append("</option>")
                     .toString();
    }

    public static String buildOptions(List<OptionDTO> optionDTOList, String language, String selectedId) {
        return buildOptions(optionDTOList, language, selectedId, selectEngText, selectBngText);
    }

    public static String buildOptions(List<OptionDTO> optionDTOList, String language, String selectedId, String selectTextEng, String selectTextBng) {
        boolean isLanguageEng = language.equalsIgnoreCase("English");
        if (optionDTOList != null && optionDTOList.size() > 0) {
            return optionDTOList.stream()
                                .map(option -> buildOption(option, isLanguageEng, selectedId))
                                .collect(Collectors.joining(" ", buildSelectOption(isLanguageEng, selectTextEng, selectTextBng), ""));
        }
        return buildSelectOption(isLanguageEng, selectTextEng, selectTextBng);
    }

    public static String buildOptionsWithoutSelectWithSelectId(List<OptionDTO> optionDTOList, String language, String selectedId) {
        boolean isLanguageEng = language.equalsIgnoreCase("English");
        if (optionDTOList != null && optionDTOList.size() > 0) {
            return optionDTOList.stream()
                                .map(option -> buildOption(option, isLanguageEng, selectedId))
                                .collect(Collectors.joining());
        }
        return buildSelectOption(isLanguageEng);
    }

    private static String buildOptionForMultipleSelection(String englishText, String banglaText, String value, boolean isLanguageEng, Set<String> ids) {
        StringBuilder option = new StringBuilder();
        if (ids != null) {
            if (ids.contains(value)) {
                option.append("<option selected value = '").append(value).append("'>");
            } else {
                option.append("<option value = '").append(value).append("'>");
            }
        } else {
            option.append("<option value = '").append(value).append("'>");
        }
        option.append(isLanguageEng ? englishText : banglaText).append("</option>");
        return option.toString();
    }

    public static String buildOptionsMultipleSelection(List<OptionDTO> optionDTOList, String language, String selectedIds) {
        boolean isLanguageEng = language.equalsIgnoreCase("English");
        Set<String> ids = null;
        if (selectedIds != null) {
            ids = Arrays.stream(selectedIds.split(","))
                        .map(String::trim)
                        .collect(Collectors.toSet());
        }
        Set<String> ids2 = ids;
        if (optionDTOList != null && optionDTOList.size() > 0) {
            return optionDTOList.stream()
                                .map(option -> buildOptionForMultipleSelection(option.englishText, option.banglaText, option.value, isLanguageEng, ids2))
                                .collect(Collectors.joining(" ", "<option></option>", ""));
        }
        return buildSelectOption(isLanguageEng);
    }

    public static String buildOptionsWithoutSelectOption(List<OptionDTO> optionDTOList, String language) {
        if (optionDTOList != null && optionDTOList.size() > 0) {
            boolean isLanguageEng = language.equalsIgnoreCase("English");
            return optionDTOList.stream()
                                .map(option -> buildOption(option.englishText, option.banglaText, option.value, isLanguageEng))
                                .collect(Collectors.joining(" "));
        }
        return "";
    }

    private static String buildOption(String englishText, String banglaText, String value, boolean isLanguageEng) {
        return "<option value = '" +
               value + "'>" +
               (isLanguageEng ? englishText : banglaText) +
               "</option>";
    }

    public static Long convertToLong(String id) {
        if (id == null) {
            return null;
        }
        try {
            return Long.parseLong(id);
        } catch (NumberFormatException ex) {
            logger.error(ex);
            logger.error("",ex);
            return null;
        }
    }

    public static Integer convertToInteger(String id) {
        try {
            return Integer.parseInt(id.trim());
        } catch (Exception ex) {
            logger.error(ex);
            logger.error("",ex);
            return null;
        }
    }

    public static Integer calculateAge(Long birthDate) {
        if (birthDate == null || birthDate <= SessionConstants.MIN_DATE) {
            return null;
        }
        return Period.between(new Date(birthDate).toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), LocalDate.now()).getYears();
    }

    public static <E> List<List<E>> getSubList(List<E> list, int size) {
        if (list == null || list.size() == 0) {
            return new ArrayList<>();
        }
        if (list.size() <= size) {
            return Collections.singletonList(list);
        }

        int startIndex = 0;
        int endIndex = size;
        List<List<E>> result = new ArrayList<>();
        while (true) {
            result.add(list.subList(startIndex, endIndex));
            startIndex = endIndex;
            if (startIndex >= list.size()) {
                break;
            }
            if (list.size() - startIndex > size) {
                endIndex = startIndex + size;
            } else {
                endIndex = list.size();
            }
        }
        return result;
    }

    public static <T> List<T> getFilteredIdList(List<T> list, Predicate<T> filterFunction) {
        return list.stream()
                   .filter(filterFunction)
                   .collect(Collectors.toList());
    }

    public static <T, R> List<R> getFilteredIdList(List<T> list, Predicate<T> filterFunction, Function<T, R> mapFunction) {
        return list.stream()
                   .filter(filterFunction)
                   .map(mapFunction)
                   .collect(Collectors.toList());
    }

    public static byte[] toByteArrayAutoClosable(BufferedImage image, String type) throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            ImageIO.write(image, type, out);
            return out.toByteArray();
        }
    }

    public static boolean checkPermission(String empId, UserDTO userDTO, int... menuConstantArray) {
        boolean hasMenuConstantPermission = checkPermission(userDTO, menuConstantArray);
        if (!hasMenuConstantPermission) {
            return false;
        }
        if (userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId() || userDTO.roleID == RoleEnum.ADMIN.getRoleId()) {
            return true;
        }
        if (userDTO.roleID == RoleEnum.EMPLOYEE_OF_PARLIAMENT.getRoleId()) {
            return isCurrentUserRequested(empId, userDTO);
        } else {
            return true;
        }
    }

    public static boolean checkPermission(UserDTO userDTO, int... menuConstantArray) {
        if (menuConstantArray.length == 0) {
            return true;
        }
        for (int menuConstant : menuConstantArray) {
            if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, menuConstant)) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkPermission(int... menuConstantArray) {
        for (int menuConstant : menuConstantArray) {
            if (PermissionRepository.checkPermissionByRoleIDAndMenuID(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.roleID, menuConstant)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isCurrentUserRequested(String empId, UserDTO userDTO) {
        logger.debug("empId : " + empId + "  userDTO.employee_record_id : " + userDTO.employee_record_id);
        if (empId == null || empId.isEmpty()) return false;
        return Long.parseLong(empId) == userDTO.employee_record_id;
    }

    public static String calculateCompleteAge(long dob, String language) {
        LocalDate birthday = new Date(dob).toInstant()
                                          .atZone(ZoneId.systemDefault())
                                          .toLocalDate();
        Period p = Period.between(birthday, LocalDate.now());
        if ("English".equalsIgnoreCase(language)) {
            return p.getYears() + "y " + p.getMonths() + "m " + p.getDays() + "d";
        } else {
            return StringUtils.convertToBanNumber(p.getYears() + "ব " + p.getMonths() + "মা " + p.getDays() + "দি");
        }
    }

    public static long getPoysha(double amount) {
        return (long) ((amount - (long) amount) * 100.0);
    }

    public static void runIOTaskInASeparateThread(Supplier<Runnable> runnableConsumer) {
        cachedThreadPool.execute(runnableConsumer.get());
    }

    public static void runIOTaskInSingleThread(Supplier<Runnable> runnableConsumer) {
        singleThreadPool.execute(runnableConsumer.get());
    }

    public static byte[] getByteArrayFromInputStream(InputStream is) {
        if (is != null) {
            try {
                return IOUtils.toByteArray(is);
            } catch (IOException e) {
                logger.error(e);
                logger.error("",e);
            }
        }
        return null;
    }

    public static boolean isMobileNumberValid(String mobileNo) {
        return MOBILE_PATTERN.matcher(mobileNo).matches();
    }

    public static boolean isEmailValid(String email) {
        return email != null && EMAIL_PATTERN.matcher(email).matches();
    }

    public static boolean digitOnlyOrEmptyValid(String digits) {
        return DIGIT_ONLY_OR_EMPTY_PATTERN.matcher(digits).matches();
    }

    public static String calculateCompleteAgeInFullFormat(long dob, String language) {

        if (dob ==  SessionConstants.MIN_DATE) return "";
        LocalDate birthday = new Date(dob).toInstant()
                                          .atZone(ZoneId.systemDefault())
                                          .toLocalDate();
        Period p = Period.between(birthday, LocalDate.now());
        if ("English".equalsIgnoreCase(language)) {
            return p.getYears() + " years " + p.getMonths() + " months " + p.getDays() + " days";
        } else {
            return StringUtils.convertToBanNumber(p.getYears() + " বছর " + p.getMonths() + " মাস " +
                                                  p.getDays() + " দিন");
        }
    }

    public static boolean checkValidAddress(String address) {
        if (address == null) {
            return false;
        }
        String[] tokens = address.split("@@");
        if (tokens.length != 5) {
            return false;
        }
        try {
            Integer.parseInt(tokens[0]);
            Integer.parseInt(tokens[1]);
            Integer.parseInt(tokens[2]);
            Integer.parseInt(tokens[3]);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public static String doJsoupCleanOrReturnEmpty(String value) {
        return doJsoupCleanOrReturnDefault(value, "");
    }

    public static String doJsoupCleanOrReturnDefault(String value, String defaultValue) {
        return value != null ? Jsoup.clean(value.trim(), Whitelist.simpleText()) : defaultValue;
    }

    public static String cleanAndGetOptionalString(String value) {
        value = doJsoupCleanOrReturnEmpty(value);
        return (value != null) ? value : "";
    }

    public static String cleanAndGetMandatoryString(String value, String exceptionMessage) throws Exception {
        value = doJsoupCleanOrReturnEmpty(value);
        if (StringUtils.isBlank(value) && exceptionMessage != null)
            throw new Exception(exceptionMessage);
        return value;
    }

    public static long parseMandatoryDate(String dateStr, String exceptionMessage) throws Exception {
        try {
            return new SimpleDateFormat("dd/MM/yyyy").parse(dateStr).getTime();
        } catch (ParseException ex) {
            if (exceptionMessage != null) throw new Exception(exceptionMessage);
        }
        return SessionConstants.MIN_DATE;
    }

    public static Integer parseInt(String intSrt, Integer defaultValue) {
        if (intSrt == null || intSrt.trim().isEmpty()) return defaultValue;
        try {
            return Integer.parseInt(intSrt.trim());
        } catch (Exception e) {
            return defaultValue;
        }
    }
    public static Integer parseOptionalInt(String intSrt, Integer defaultValue, String exceptionMessage) throws Exception {
        if (intSrt == null || intSrt.trim().isEmpty()) return defaultValue;
        try {
            return Integer.parseInt(intSrt);
        } catch (Exception e) {
            if (exceptionMessage != null) throw new Exception(exceptionMessage);
        }
        return defaultValue;
    }

    public static Integer parseMandatoryInt(String intSrt, Integer defaultValue, String exceptionMessage) throws Exception {
        try {
            return Integer.parseInt(intSrt);
        } catch (Exception e) {
            if (exceptionMessage != null) throw new Exception(exceptionMessage);
        }
        return defaultValue;
    }

    public static Long parseMandatoryLong(String longSrt, String exceptionMessage) throws Exception {
        try {
            return Long.parseLong(longSrt);
        } catch (Exception e) {
            if (exceptionMessage != null) throw new Exception(exceptionMessage);
        }
        return Long.MAX_VALUE;
    }

    public static Long parseOptionalLong(String longSrt, Long defaultValue, String exceptionMessage) {
        if (longSrt == null || longSrt.trim().isEmpty()) return defaultValue;
        try {
            return Long.parseLong(longSrt);
        } catch (Exception e) {
            if (exceptionMessage != null) throw new CustomException(exceptionMessage);
        }
        return defaultValue;
    }

    public static Long parseOptionalLong(String longSrt, Long defaultValue) {
        if (longSrt == null || longSrt.trim().isEmpty()) return defaultValue;
        try {
            return Long.parseLong(longSrt);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static Double parseOptionalDouble(String doubleSrt, Double defaultValue, String exceptionMessage) throws Exception {
        if (doubleSrt == null || doubleSrt.trim().isEmpty()) return defaultValue;
        try {
            return Double.parseDouble(doubleSrt);
        } catch (Exception e) {
            if (exceptionMessage != null) throw new Exception(exceptionMessage);
        }
        return defaultValue;
    }

    public static String parseMandatoryMobileNumber(String mobileNumber, String exceptionMessage) throws Exception {
        mobileNumber = "88" + StringUtils.convertToEngNumber(mobileNumber);
        if (!isMobileNumberValid(mobileNumber)) {
            if (exceptionMessage != null) throw new Exception(exceptionMessage);
            return "";
        }
        return mobileNumber;
    }

    public static String parseOptionalMobileNumber(String mobileNumber, String exceptionMessage) throws Exception {
        if (mobileNumber == null || mobileNumber.trim().isEmpty()) return "";
        return parseMandatoryMobileNumber(mobileNumber, exceptionMessage);
    }

    private static final Map<Integer, NameModel> ageIndexWiseMap = new HashMap<>();

    static {
        ageIndexWiseMap.put(1, new NameModel("Not Defined", "সংজ্ঞায়িত নয়"));
        ageIndexWiseMap.put(2, new NameModel("1-10", "১-১০"));
        ageIndexWiseMap.put(3, new NameModel("11-20", "১১-২০"));
        ageIndexWiseMap.put(4, new NameModel("21-30", "২১-৩০"));
        ageIndexWiseMap.put(5, new NameModel("31-40", "৩১-৪০"));
        ageIndexWiseMap.put(6, new NameModel("41-50", "৪১-৫০"));
        ageIndexWiseMap.put(7, new NameModel("51-60", "৫১-৬০"));
        ageIndexWiseMap.put(8, new NameModel("61-70", "৬১-৭০"));
        ageIndexWiseMap.put(9, new NameModel("71-Above", "৭১-তদুর্ধ"));
    }

    public static String getAgeRangeTextByIndex(int index, boolean isLangEng) {
        NameModel nameModel = ageIndexWiseMap.get(index);
        return nameModel == null ? "" : (isLangEng ? nameModel.nameEn : nameModel.nameBn);
    }

    public static String calculateAgeRange(Integer x, boolean isLangEng) {
        if (x == null || x < 0) {
            return isLangEng ? "Not Defined" : "সংজ্ঞায়িত নয়";
        }
        if (x <= 10) {
            return isLangEng ? "1-10" : "১-১০";
        }
        if (x <= 20) {
            return isLangEng ? "11-20" : "১১-২০";
        }
        if (x <= 30) {
            return isLangEng ? "21-30" : "২১-৩০";
        }
        if (x <= 40) {
            return isLangEng ? "31-40" : "৩১-৪০";
        }
        if (x <= 50) {
            return isLangEng ? "41-50" : "৪১-৫০";
        }
        if (x <= 60) {
            return isLangEng ? "51-60" : "৫১-৬০";
        }
        if (x <= 70) {
            return isLangEng ? "61-70" : "৬১-৭০";
        }
        return isLangEng ? "71-Above" : "৭১-তদুর্ধ";
    }

    public static Integer getCalculateAgeRangeIndex(Integer x) {
        if (x == null || x < 0) {
            return 1;
        }
        if (x <= 10) {
            return 2;
        }
        if (x <= 20) {
            return 3;
        }
        if (x <= 30) {
            return 4;
        }
        if (x <= 40) {
            return 5;
        }
        if (x <= 50) {
            return 6;
        }
        if (x <= 60) {
            return 7;
        }
        if (x <= 70) {
            return 8;
        }
        return 9;
    }

    public static String randomGenerator(String chars, int len) {
        SecureRandom random = new SecureRandom();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < len; i++) {
            sb.append(chars.charAt(random.nextInt(chars.length())));
        }
        return sb.toString();
    }

    public static String randomNumberGenerator(int len) {
        return randomGenerator("0123456789", len);
    }

    public static String randomStringWithNumberGenerator(int len) {
        return randomGenerator("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", len);
    }

    public static String randomStringWithNumberAndSpecialCharacterGenerator(int len) {
        return randomGenerator("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@#$&", len);
    }

    public static boolean notEnglish(String str) {
        return !str.matches("^[\\u0000-\\u007F]+$");
    }

    public static boolean notBangla(String str) {
        return !str.chars().allMatch(Utils::isValidBanglaCharacter);
    }

    public static void handleTransaction(TransactionHandler transactionHandler) throws Exception {
        handleTransaction(CommonDAOService.CONNECTION_THREAD_LOCAL, transactionHandler);
    }

    public static void handleTransactionForCommonDTOService(TransactionHandler transactionHandler) throws Exception {
        handleTransaction(CommonDTOService.CONNECTION_THREAD_LOCAL, transactionHandler);
    }

    public static void handleTransactionForNavigationService4(TransactionHandler transactionHandler) throws Exception {
        handleTransaction(NavigationService4.CONNECTION_THREAD_LOCAL, transactionHandler);
    }

    public static void handleTransaction(ThreadLocal<Connection> threadLocal, TransactionHandler transactionHandler) throws Exception {
        if (threadLocal.get() == null) {
            Connection connection = ConnectionAndStatementUtil.getConnection(ConnectionType.WRITE);
            threadLocal.set(connection);
            try {
                connection.setAutoCommit(false);
                transactionHandler.transaction();
                connection.commit();
            } catch (Exception ex) {
                connection.rollback();
                logger.error("",ex);
                throw ex;
            } finally {
                connection.setAutoCommit(true);
                threadLocal.remove();
                ConnectionAndStatementUtil.closeConnection(connection, ConnectionType.WRITE);
            }
        } else {
            transactionHandler.transaction();
        }
    }

    private static void addToRepoList(String tableName, CacheUpdateModel model) {
        if (tableName == null) {
            return;
        }
        if (model != null) {
            Repository repository = RepositoryManager.getInstance().getRepository(tableName);
            if (repository != null) {
                model.repositoryList.add(repository);
            }
        }
    }

    public static void addToRepoList(String tableName) {
        addToRepoList(tableName, CommonDAOService.CACHE_UPDATE_MODEL_THREAD_LOCAL.get());
    }

    public static void runExactTimeRepoMethod(String tableName, long modificationTime) {
        if (tableName == null) {
            return;
        }
        Repository repository = RepositoryManager.getInstance().getRepository(tableName);
        if (repository != null) {
            repository.reloadWithExactModificationTime(modificationTime);
        }
    }

    private static void executeCache(CacheUpdateModel model) {
        if (model == null) {
            return;
        }
        logger.debug("CACHE REPO SIZE : " + model.repositoryList.size() + " TIME : " + model.time);
        if (model.repositoryList.size() > 0) {
            model.repositoryList
                    .stream()
                    .filter(Objects::nonNull)
                    .distinct()
                    .peek(e -> logger.debug("TABLE NAME : " + e.getTableName()))
                    .forEach(e -> e.reloadWithExactModificationTime(model.time));
        }
    }

    private static void executeCacheAsync(CacheUpdateModel model) {
        CacheUpdateModel cloneModel = CacheUpdateModel.clone(model);
        cachedThreadPool.execute(() -> executeCache(cloneModel));
    }

    public static void executeCache() {
        executeCache(CommonDAOService.CACHE_UPDATE_MODEL_THREAD_LOCAL.get());
    }

    public static void executeCacheAsync() {
        executeCacheAsync(CommonDAOService.CACHE_UPDATE_MODEL_THREAD_LOCAL.get());
    }

    public static void addToRepoListForCommonDTOService(String tableName) {
        addToRepoList(tableName, CommonDTOService.CACHE_UPDATE_MODEL_THREAD_LOCAL.get());
    }

    public static void executeCacheForCommonDTOService() {
        executeCache(CommonDTOService.CACHE_UPDATE_MODEL_THREAD_LOCAL.get());
    }

    public static void executeCacheAsyncForCommonDTOService() {
        executeCacheAsync(CommonDTOService.CACHE_UPDATE_MODEL_THREAD_LOCAL.get());
    }

    public static void addToRepoListForNavigationService4(String tableName) {
        addToRepoList(tableName, NavigationService4.CACHE_UPDATE_MODEL_THREAD_LOCAL.get());
    }

    public static void executeCacheForNavigationService4() {
        executeCache(NavigationService4.CACHE_UPDATE_MODEL_THREAD_LOCAL.get());
    }

    public static void executeCacheAsyncForNavigationService4() {
        executeCacheAsync(NavigationService4.CACHE_UPDATE_MODEL_THREAD_LOCAL.get());
    }

    public static void wrapWithAtomicReference(Consumer<AtomicReference<Exception>> consumer) throws Exception {
        AtomicReference<Exception> ar = new AtomicReference<>();
        consumer.accept(ar);
        if (ar.get() != null) {
            throw ar.get();
        }
    }

    public static <R> R wrapWithAtomicReferenceAndReturnR(Function<AtomicReference<Exception>, R> function) throws Exception {
        AtomicReference<Exception> ar = new AtomicReference<>();
        R r = function.apply(ar);
        if (ar.get() != null) {
            throw ar.get();
        }
        return r;
    }

    public static Class<?> getClassByClassName(String className) {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            logger.error("",e);
            throw new RuntimeException("no class found with className=" + className);
        }
    }


}