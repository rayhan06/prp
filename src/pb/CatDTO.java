package pb;

public class CatDTO {
	
	public String domainName = "";
	public String nameEn = "";
	public String nameBn = "";
	public int value = 0;
	public int ordering = 1;
	public String parentDomain = "";
	public int parentDomainVal = -1;
	public String languageTextEnglish = "";
	public String languageTextBangla = "";
	
	public String getNameEn()
	{
		return nameEn;
	}
	
	public int getOrdering()
	{
		return ordering;
	}
	
	public static int CATDEFAULT = -2;
	public static int CATDEFAULTNOTREQUIRED= -1;

}
