package pb;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import dbm.*;
import org.apache.log4j.Logger;

public class GeoTree {
	private static final Logger logger = Logger.getLogger(GeoTree.class);
	public static String DatabaseWithDot = "";
	
	public static List<Integer> getChildIDs(String geoName, String parentGeoName, int parentID)
	{
		String sql = "select id from " + DatabaseWithDot + "geo_" + geoName + "s where geo_" + parentGeoName + "_id = " + parentID;
		return CommonDAO.getIDs(sql);
	}
	
	public static List<Integer> getTopIDs(String geoName)
	{
		String sql = "select id from  " + DatabaseWithDot + "geo_" + geoName + "s";
		return CommonDAO.getIDs(sql);
	}
	
	public static int getParent(String parentTable, int parentID)
	{
		String sql = "select id from " + parentTable + " where id  = " + parentID;
		return CommonDAO.getID(sql);
	}
	
	public static String getName(String geoName, int id, String Language)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		
		String nameColumn = "";
		if(Language.equalsIgnoreCase("english"))
		{
			nameColumn = geoName + "_name_eng";
		}
		else
		{
			nameColumn = geoName + "_name_bng";
		}
		
		String name = "";
		try
		{
			
			String sql = "select " + nameColumn + " from  " + DatabaseWithDot + "geo_" + geoName + "s where id = " + id; 
			
			System.out.println(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);
			
			

			if(rs.next())
			{
				 //System.out.println(rs.getString("name_en") + "  " + rs.getInt("id"));
				name = rs.getString(nameColumn);
				//System.out.println("found Option = " + Option);					 

			}			
			
		}
		catch(Exception ex)
		{
			logger.error("",ex);
		}
		finally
		{
			try
			{ 
				if (stmt != null) 
				{
					stmt.close();
				}
			} 
			catch (Exception e)
			{
				System.out.println(e);
			}
			
			try
			{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}
			catch(Exception ex2)
			{
				System.out.println(ex2);
			}
		}
		return name;
	}


}
