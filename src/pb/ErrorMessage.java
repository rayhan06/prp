package pb;

public class ErrorMessage {

    public static String getEmptyMessage(String language) {
        return language.equalsIgnoreCase("english") ? "cannot be empty." : "খালি থাকতে পারে না।";
    }

    public static String getSuccessMessage(String language) {
        return language.equalsIgnoreCase("english") ? "Successfully Added." : "সফলতার সাথে যোগ হয়েছে।";
    }

    public static String getInvalidMessage(String language) {
        return language.equalsIgnoreCase("english") ? "has an invalid value." : "এর মান বৈধ নয়।";
    }

    public static String getGenericInvalidMessage(String language) {
        return language.equalsIgnoreCase("english") ? "Invalid input." : "অবৈধ ইনপুট।";
    }
}