package pb;

import java.util.Comparator;

public class OptionDTO {
    public static final Comparator<OptionDTO> defaultOrderingComparator = Comparator.comparing(optionDTO -> optionDTO.orderingString);

    public String englishText;
    public String banglaText;
    public String value;
    public String title;
    public String orderingString;
    public final Comparator<OptionDTO> orderingComparator;

    public OptionDTO() {
        this("", "", "", "", "", defaultOrderingComparator);
    }

    public OptionDTO(String englishText, String banglaText, String value) {
        this(englishText, banglaText, value, "", "", defaultOrderingComparator);
    }

    public OptionDTO(String englishText, String banglaText, String value, String title) {
        this(englishText, banglaText, value, title, "", defaultOrderingComparator);
    }

    public OptionDTO(String englishText, String banglaText, String value, String title, String orderingString) {
        this(englishText, banglaText, value, title, orderingString, defaultOrderingComparator);
    }

    public OptionDTO(String englishText, String banglaText, String value,
                     String title, String orderingString, Comparator<OptionDTO> orderingComparator) {
        this.englishText = englishText;
        this.banglaText = banglaText;
        this.value = value;
        this.title = title;
        this.orderingString = orderingString;
        this.orderingComparator = orderingComparator;
    }

    @Override
    public String toString() {
        return "OptionDTO{" +
               "englishText='" + englishText + '\'' +
               ", banglaText='" + banglaText + '\'' +
               ", value='" + value + '\'' +
               ", title='" + title + '\'' +
               '}';
    }
}
