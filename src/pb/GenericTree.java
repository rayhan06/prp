package pb;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import dbm.*;
import org.apache.log4j.Logger;

public class GenericTree {
	private static final Logger logger = Logger.getLogger(GenericTree.class);
	public static String DatabaseWithDot = "";
	
	public static List<Integer> getChildIDs(String tableName, String parentIdColumn, int parentID)
	{
		if(tableName.equalsIgnoreCase(""))
		{
			return null;
		}
		String sql = "select id from " + DatabaseWithDot +  tableName + " where " + parentIdColumn + " = " + parentID;
		return CommonDAO.getIDs(sql);
	}
	
	public static List<Integer> getChildIDs(String tableName, String parentIdColumn, int parentID, String filter)
	{
		if(tableName.equalsIgnoreCase(""))
		{
			return null;
		}
		String sql = "select id from " + DatabaseWithDot +  tableName + " where " + parentIdColumn + " = " + parentID + " and " + filter;
		return CommonDAO.getIDs(sql);
	}
	
	
	public static List<Integer> getTopIDs(String tableName)
	{
		if(tableName.equalsIgnoreCase(""))
		{
			return null;
		}
		String sql = "select id from  " + DatabaseWithDot + tableName;
		return CommonDAO.getIDs(sql);
	}
	
	public static List<Integer> getRecurviveIDs(String tableName, String sameTableParentColumnName, int sameTableParentID, String parentTableColumnName, int parentTableID)
	{
		if(tableName.equalsIgnoreCase(""))
		{
			return null;
		}
		String sql = "select id from  " + DatabaseWithDot + tableName 
				+ " where " + sameTableParentColumnName + " = " + sameTableParentID 
				+ " and " + parentTableColumnName + " = " + parentTableID;
		return CommonDAO.getIDs(sql);
	}
	
	public static List<Integer> getTopIDs(String tableName, String filter)
	{
		if(tableName.equalsIgnoreCase(""))
		{
			return null;
		}
		String sql = "select id from  " + DatabaseWithDot + tableName  + " and " + filter;
		return CommonDAO.getIDs(sql);
	}
	
	public static int getParent(String parentTable, int parentID)
	{
		if(parentTable.equalsIgnoreCase(""))
		{
			return -1;
		}
		String sql = "select id from " + parentTable + " where id  = " + parentID;
		return CommonDAO.getID(sql);
	}
	
	
	public static String getNameFromChildID(int id, String Language, String geoName)
	{
		if(geoName.equalsIgnoreCase(""))
		{
			return "";
		}
		////System.out.println("getNameFromChildID geoName = " + geoName);
		if(!geoName.equals("office_unit_organograms"))
		{
			return "";
		}
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		
		String nameColumn = "";
		if(Language.equalsIgnoreCase("english"))
		{
			nameColumn = "name_eng";
		}
		else
		{
			nameColumn = "name_bng";
		}
		
		String name = "";
		try
		{
			
			String sql = "SELECT " + nameColumn + " FROM " +  DatabaseWithDot + "employee_records where id in "
					+ "(SELECT employee_record_id FROM " + DatabaseWithDot + "employee_offices where office_unit_organogram_id = "
					+ id
					+ " and " +  DatabaseWithDot + "employee_offices.status = 1)";
			
			////System.out.println(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);
			
			

			if(rs.next())
			{
				 ////System.out.println(rs.getString("name_en") + "  " + rs.getInt("id"));
				name = rs.getString(nameColumn);
				////System.out.println("found Option = " + Option);					 

			}			
			
		}
		catch(Exception ex)
		{
			logger.error("",ex);
		}
		finally
		{
			try
			{ 
				if (stmt != null) 
				{
					stmt.close();
				}
			} 
			catch (Exception e)
			{
				//System.out.println(e);
			}
			
			try
			{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}
			catch(Exception ex2)
			{
				//System.out.println(ex2);
			}
		}
		return name;
	}
	
	public static String getName(String tableName, int id, String Language, String nameColumnEng, String nameColumnBng)
	{
		if(tableName.equalsIgnoreCase(""))
		{
			return "";
		}
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		
		String nameColumn = "";
		if(Language.equalsIgnoreCase("english"))
		{
			nameColumn = nameColumnEng;
		}
		else
		{
			nameColumn = nameColumnBng;
		}
		
		String name = "";
		try
		{
			
			String sql = "select " + nameColumn + " from  " + DatabaseWithDot + tableName + " where id = " + id; 
			
			//System.out.println(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);
			
			

			if(rs.next())
			{
				 ////System.out.println(rs.getString("name_en") + "  " + rs.getInt("id"));
				name = rs.getString(nameColumn);
				////System.out.println("found Option = " + Option);					 

			}			
			
		}
		catch(Exception ex)
		{
			logger.error("",ex);
		}
		finally
		{
			try
			{ 
				if (stmt != null) 
				{
					stmt.close();
				}
			} 
			catch (Exception e)
			{
				//System.out.println(e);
			}
			
			try
			{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}
			catch(Exception ex2)
			{
				//System.out.println(ex2);
			}
		}
		return name;
	}

}
