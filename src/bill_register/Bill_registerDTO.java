package bill_register;

import sessionmanager.SessionConstants;
import util.CommonDTO;

public class Bill_registerDTO extends CommonDTO {
    public long budgetRegisterId = -1;
    public long billDate = SessionConstants.MIN_DATE;
    public String recipientName = "";
    public String description = "";
    public long billAmount = 0;
    public String tokenNumber = "";
    public long tokenDate = SessionConstants.MIN_DATE;
    public String chequeNumber = "";
    public long chequeDate = SessionConstants.MIN_DATE;
    public long chequeAmount = 0;
    public long budgetSelectionInfoId = 0;
    public long budgetOfficeId = 0;
    public long budgetMappingId = 0;
    public long economicSubCodeId = 0;
    public int cashType=0;
    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;

    @Override
    public String toString() {
        return "Bill_registerDTO{" +
                "budgetRegisterId=" + budgetRegisterId +
                ", billDate=" + billDate +
                ", description='" + description + '\'' +
                ", billAmount=" + billAmount +
                ", tokenNumber='" + tokenNumber + '\'' +
                ", tokenDate=" + tokenDate +
                ", chequeNumber='" + chequeNumber + '\'' +
                ", chequeDate=" + chequeDate +
                ", chequeAmount=" + chequeAmount +
                ", budgetSelectionInfoId=" + budgetSelectionInfoId +
                ", budgetOfficeId=" + budgetOfficeId +
                ", budgetMappingId=" + budgetMappingId +
                ", economicSubCodeId=" + economicSubCodeId +
                ", modifiedBy=" + modifiedBy +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                '}';
    }
}
