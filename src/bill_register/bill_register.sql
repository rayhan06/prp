CREATE TABLE bill_register
(
    ID                       BIGINT(20) PRIMARY KEY, -- Bill No
    bill_date                BIGINT(20) DEFAULT -62135791200000,             -- Date তাং
    description              VARCHAR(4096),
    bill_amount              BIGINT(20),             -- Bill Amount বিলের টাকার পরিমাণ
    token_number             VARCHAR(64),             -- Token No
    token_date               BIGINT(20) DEFAULT -62135791200000,             -- Token Date
    cheque_number            VARCHAR(64),            -- cheque number
    cheque_date              BIGINT(20) DEFAULT -62135791200000, -- cheque date
    cheque_amount            BIGINT(20),             -- cheque taka amount
    budget_selection_info_id BIGINT(20),             -- Economic Year অর্থবছর
    budget_office_id         BIGINT(20),             -- Office কার্যালয়
    budget_mapping_id        BIGINT(20),             -- Budget Operation Code বাজেট অপারেশন কোড
    economic_sub_code_id     BIGINT(20),             -- Economic Code অর্থনৈতিক কোড
    modified_by              BIGINT(20),
    lastModificationTime     BIGINT(20) DEFAULT -62135791200000,
    inserted_by              BIGINT(20),
    insertion_time           BIGINT(20) DEFAULT -62135791200000,
    isDeleted                INT(11)    DEFAULT 0
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

INSERT INTO vbSequencer (table_name, next_id, table_LastModificationTime) VALUES ('bill_register', 1, 0);