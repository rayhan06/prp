package bill_register;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Bill_registerDAO implements CommonDAOService<Bill_registerDTO> {
    private static final Logger logger = Logger.getLogger(Bill_registerDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (budget_register_id,bill_date,recipient_name,description, bill_amount, token_number, token_date, cheque_number,cheque_date,"
                    .concat("cheque_amount,budget_selection_info_id, budget_office_id, budget_mapping_id, economic_sub_code_id,cash_type,")
                    .concat("modified_by, lastModificationTime, inserted_by, insertion_time, isDeleted, ID) ")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET budget_register_id=?,bill_date=?,recipient_name=?,description=?, bill_amount=?, token_number=?, token_date=?, cheque_number=?, cheque_date=?,"
                    .concat("cheque_amount=?, budget_selection_info_id=?, budget_office_id=?, budget_mapping_id=?,")
                    .concat("economic_sub_code_id=?, cash_type=?, modified_by=?, lastModificationTime=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private Bill_registerDAO() {
        searchMap.put("budget_selection_info_id", " AND (budget_selection_info_id = ?)");
        searchMap.put("budget_mapping_id", " AND (budget_mapping_id = ?)");
        searchMap.put("economic_sub_code_id", " AND (economic_sub_code_id = ?)");
        searchMap.put("date_from", " AND (bill_date >= ?)");
        searchMap.put("date_to", " AND (bill_date <= ?)");
        searchMap.put("bill_number", " AND (ID LIKE ?)");
        searchMap.put("cheque_number", " AND (cheque_number LIKE ?)");
        searchMap.put("token_number", " AND (token_number LIKE ?)");
    }

    private static class LazyLoader {
        static final Bill_registerDAO INSTANCE = new Bill_registerDAO();
    }

    public static Bill_registerDAO getInstance() {
        return Bill_registerDAO.LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Bill_registerDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.budgetRegisterId);
        ps.setLong(++index, dto.billDate);
        ps.setString(++index, dto.recipientName);
        ps.setString(++index, dto.description);
        ps.setLong(++index, dto.billAmount);
        ps.setString(++index, dto.tokenNumber);
        ps.setLong(++index, dto.tokenDate);
        ps.setString(++index, dto.chequeNumber);
        ps.setLong(++index, dto.chequeDate);
        ps.setLong(++index, dto.chequeAmount);
        ps.setLong(++index, dto.budgetSelectionInfoId);
        ps.setLong(++index, dto.budgetOfficeId);
        ps.setLong(++index, dto.budgetMappingId);
        ps.setLong(++index, dto.economicSubCodeId);
        ps.setLong(++index, dto.cashType);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Bill_registerDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Bill_registerDTO dto = new Bill_registerDTO();
            dto.iD = rs.getLong("ID");
            dto.budgetRegisterId = rs.getLong("budget_register_id");
            dto.billDate = rs.getLong("bill_date");
            dto.recipientName = rs.getString("recipient_name");
            dto.description = rs.getString("description");
            dto.billAmount = rs.getLong("bill_amount");
            dto.tokenNumber = rs.getString("token_number");
            dto.tokenDate = rs.getLong("token_date");
            dto.chequeNumber = rs.getString("cheque_number");
            dto.chequeDate = rs.getLong("cheque_date");
            dto.chequeAmount = rs.getLong("cheque_amount");
            dto.budgetSelectionInfoId = rs.getLong("budget_selection_info_id");
            dto.budgetOfficeId = rs.getLong("budget_office_id");
            dto.budgetMappingId = rs.getLong("budget_mapping_id");
            dto.economicSubCodeId = rs.getLong("economic_sub_code_id");
            dto.cashType = rs.getInt("cash_type");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "bill_register";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Bill_registerDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Bill_registerDTO) commonDTO, updateSqlQuery, false);
    }

    private static final String getByBudgetRegisterId =
            "SELECT * FROM bill_register WHERE budget_register_id=%d AND isDeleted=0";

    public Bill_registerDTO getDTOByBudgetRegisterId(long budgetRegisterId) {
        return ConnectionAndStatementUtil.getT(
                String.format(getByBudgetRegisterId, budgetRegisterId),
                this::buildObjectFromResultSet
        );
    }
}