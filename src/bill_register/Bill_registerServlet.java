package bill_register;

import budget_register.Budget_registerDTO;
import cheque_register.Cheque_registerDTO;
import common.BaseServlet;
import finance.CashTypeEnum;
import login.LoginDTO;
import org.apache.log4j.Logger;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Bill_registerServlet")
@MultipartConfig
@SuppressWarnings({"Duplicates","unused"})
public class Bill_registerServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(Bill_registerServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        String actionType = request.getParameter("actionType");
        // only handles search from user
        if ("search".equals(actionType)) {
            super.doGet(request, response);
            return;
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // no POST request support from user end
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }

    public static Bill_registerDTO addBillRegister(Budget_registerDTO budgetRegisterDTO,
                                                   CashTypeEnum cashTypeEnum, boolean addFlag) throws Exception {
        Bill_registerDTO billRegisterDTO;
        if (addFlag) {
            billRegisterDTO = new Bill_registerDTO();
            billRegisterDTO.budgetRegisterId = budgetRegisterDTO.iD;
            billRegisterDTO.insertedBy = budgetRegisterDTO.insertedBy;
            billRegisterDTO.insertionTime = budgetRegisterDTO.insertionTime;
        } else {
            billRegisterDTO = Bill_registerDAO.getInstance().getDTOByBudgetRegisterId(budgetRegisterDTO.iD);
        }
        billRegisterDTO.billDate = System.currentTimeMillis();
        billRegisterDTO.recipientName = budgetRegisterDTO.recipientName;
        billRegisterDTO.description = budgetRegisterDTO.description;
        billRegisterDTO.billAmount = budgetRegisterDTO.billAmount;
        billRegisterDTO.budgetSelectionInfoId = budgetRegisterDTO.budgetSelectionInfoId;
        billRegisterDTO.budgetOfficeId = budgetRegisterDTO.budgetOfficeId;
        billRegisterDTO.budgetMappingId = budgetRegisterDTO.budgetMappingId;
        billRegisterDTO.economicSubCodeId = budgetRegisterDTO.economicSubCodeId;
        billRegisterDTO.cashType = cashTypeEnum.getValue();
        billRegisterDTO.insertedBy = budgetRegisterDTO.insertedBy;
        billRegisterDTO.modifiedBy = budgetRegisterDTO.modifiedBy;
        billRegisterDTO.lastModificationTime = budgetRegisterDTO.lastModificationTime;
        billRegisterDTO.insertionTime = budgetRegisterDTO.insertionTime;
        if (addFlag) {
            Bill_registerDAO.getInstance().add(billRegisterDTO);
        } else {
            Bill_registerDAO.getInstance().update(billRegisterDTO);
        }
        return billRegisterDTO;
    }

    public static void updateBillRegister(Cheque_registerDTO cheque_registerDTO) throws Exception {
        Bill_registerDTO bill_registerDTO = Bill_registerDAO.getInstance().getDTOFromID(cheque_registerDTO.billRegisterId);
        if (bill_registerDTO == null) throw new Exception("Bill Register Id not present");
        bill_registerDTO.chequeAmount = cheque_registerDTO.chequeAmount;
        bill_registerDTO.tokenNumber = cheque_registerDTO.tokenNumber;
        bill_registerDTO.tokenDate = cheque_registerDTO.tokenDate;
        bill_registerDTO.chequeNumber = cheque_registerDTO.chequeNumber;
        bill_registerDTO.chequeDate = cheque_registerDTO.chequeDate;
        bill_registerDTO.lastModificationTime = cheque_registerDTO.lastModificationTime;
        bill_registerDTO.modifiedBy = cheque_registerDTO.modifiedBy;
        Bill_registerDAO.getInstance().update(bill_registerDTO);
    }

    @Override
    public String getTableName() {
        return Bill_registerDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Bill_registerServlet";
    }

    @Override
    public Bill_registerDAO getCommonDAOService() {
        return Bill_registerDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        // no add support from user
        return new int[]{};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        // no edit support from user
        return new int[]{};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.BILL_REGISTER_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Bill_registerServlet.class;
    }
}
