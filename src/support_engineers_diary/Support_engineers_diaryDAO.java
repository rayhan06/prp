package support_engineers_diary;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;

import appointment.AppointmentDTO;
import util.*;
import pb.*;

public class Support_engineers_diaryDAO  implements CommonDAOService<Support_engineers_diaryDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Support_engineers_diaryDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"support_ticket_id",
			"history_id",
			"support_engineer_org",
			"support_engineer_eid",
			"boss_eid",
			"boss_org",
			"assignment_time",
			"due_time",
			"done_time",
			"support_job_cat",
			"done_history_id",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("support_engineer_org"," and (support_engineer_org like ?)");
		searchMap.put("support_engineer_eid"," and (support_engineer_eid like ?)");
		searchMap.put("boss_eid"," and (boss_eid like ?)");
		searchMap.put("boss_org"," and (boss_org like ?)");
		searchMap.put("support_job_cat"," and (support_job_cat = ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Support_engineers_diaryDAO INSTANCE = new Support_engineers_diaryDAO();
	}

	public static Support_engineers_diaryDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	
	
	@Override
	public void set(PreparedStatement ps, Support_engineers_diaryDTO support_engineers_diaryDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	

		if(isInsert)
		{
			ps.setObject(++index,support_engineers_diaryDTO.iD);
		}
		ps.setObject(++index,support_engineers_diaryDTO.supportTicketId);
		ps.setObject(++index,support_engineers_diaryDTO.historyId);
		ps.setObject(++index,support_engineers_diaryDTO.supportEngineerOrg);
		ps.setObject(++index,support_engineers_diaryDTO.supportEngineerEid);
		ps.setObject(++index,support_engineers_diaryDTO.bossEid);
		ps.setObject(++index,support_engineers_diaryDTO.bossOrg);
		ps.setObject(++index,support_engineers_diaryDTO.assignmentTime);
		ps.setObject(++index,support_engineers_diaryDTO.dueTime);
		ps.setObject(++index,support_engineers_diaryDTO.doneTime);
		ps.setObject(++index,support_engineers_diaryDTO.supportJobCat);
		ps.setObject(++index,support_engineers_diaryDTO.doneHistoryId);
		if(isInsert)
		{
			ps.setObject(++index,support_engineers_diaryDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,support_engineers_diaryDTO.iD);
		}
	}
	
	@Override
	public Support_engineers_diaryDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Support_engineers_diaryDTO support_engineers_diaryDTO = new Support_engineers_diaryDTO();
			int i = 0;
			support_engineers_diaryDTO.iD = rs.getLong(columnNames[i++]);
			support_engineers_diaryDTO.supportTicketId = rs.getLong(columnNames[i++]);
			support_engineers_diaryDTO.historyId = rs.getLong(columnNames[i++]);
			support_engineers_diaryDTO.supportEngineerOrg = rs.getLong(columnNames[i++]);
			support_engineers_diaryDTO.supportEngineerEid = rs.getLong(columnNames[i++]);
			support_engineers_diaryDTO.bossEid = rs.getLong(columnNames[i++]);
			support_engineers_diaryDTO.bossOrg = rs.getLong(columnNames[i++]);
			support_engineers_diaryDTO.assignmentTime = rs.getLong(columnNames[i++]);
			support_engineers_diaryDTO.dueTime = rs.getLong(columnNames[i++]);
			support_engineers_diaryDTO.doneTime = rs.getLong(columnNames[i++]);
			support_engineers_diaryDTO.supportJobCat = rs.getInt(columnNames[i++]);
			support_engineers_diaryDTO.doneHistoryId = rs.getLong(columnNames[i++]);
			support_engineers_diaryDTO.isDeleted = rs.getInt(columnNames[i++]);
			support_engineers_diaryDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return support_engineers_diaryDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Support_engineers_diaryDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "support_engineers_diary";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Support_engineers_diaryDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Support_engineers_diaryDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
	
	public Support_engineers_diaryDTO getBySupportManJob (long support_ticket_id, long support_engineer_org, int support_job_cat) throws Exception
	{
		String sql = "SELECT * FROM " + getTableName() + " WHERE  isDeleted = 0 and  support_ticket_id =" + support_ticket_id 
				+ " and support_engineer_org = " + support_engineer_org 
				+ " and support_job_cat = " + support_job_cat
				+ " order by lastModificationTime desc limit 1";
        return ConnectionAndStatementUtil.getT(sql,this::buildObjectFromResultSet);
	}
	
	public Support_engineers_diaryDTO getBySupportManJobEid (long support_ticket_id, long support_engineer_eid, int support_job_cat) throws Exception
	{
		String sql = "SELECT * FROM " + getTableName() + " WHERE  isDeleted = 0 and  support_ticket_id =" + support_ticket_id 
				+ " and support_engineer_eid = " + support_engineer_eid 
				+ " and support_job_cat = " + support_job_cat
				+ " order by lastModificationTime desc limit 1";
        return ConnectionAndStatementUtil.getT(sql,this::buildObjectFromResultSet);
	}
	
	public List<Support_engineers_diaryDTO> getPending (long support_ticket_id, long support_engineer_eid) throws Exception
	{
		String sql = "SELECT * FROM " + getTableName() + " WHERE isDeleted = 0 and support_ticket_id =" + support_ticket_id 
				+ " and support_engineer_eid = " + support_engineer_eid 
				+ " and done_time <= 0 ";
        return ConnectionAndStatementUtil.getListOfT(sql,this::buildObjectFromResultSet);
	}
				
}
	