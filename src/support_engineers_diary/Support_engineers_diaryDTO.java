package support_engineers_diary;


import support_ticket.Support_ticketDAO;
import support_ticket.Support_ticketDTO;
import ticket_forward_history.Ticket_forward_historyDTO;
import util.*;
import workflow.WorkflowController; 


public class Support_engineers_diaryDTO extends CommonDTO
{

	public long supportTicketId = -1;
	public long historyId = -1;
	public long supportEngineerOrg = -1;
	public long supportEngineerEid = -1;
	public long bossEid = -1;
	public long bossOrg = -1;
	public long assignmentTime = -1;
	public long dueTime = -1;
	public long doneTime = -1;
	public int supportJobCat = -1;
	public long doneHistoryId = -1;
	
	public Support_engineers_diaryDTO() {}
	Support_ticketDAO support_ticketDAO = new Support_ticketDAO();
	
	public Support_engineers_diaryDTO(Support_ticketDTO support_ticketDTO, long supportEngineerOrg, 
			Ticket_forward_historyDTO ticket_forward_historyDTO, int  supportJobCat)
	{
		supportTicketId = support_ticketDTO.iD;
		historyId = ticket_forward_historyDTO.iD;
		this.supportEngineerOrg = supportEngineerOrg;
		supportEngineerEid = WorkflowController.getEmployeeRecordIDFromOrganogramID(supportEngineerOrg);
		bossOrg = support_ticketDTO.currentAssignedOrganoramId;
		bossEid = WorkflowController.getEmployeeRecordIDFromOrganogramID(bossOrg);
		assignmentTime = System.currentTimeMillis();
		this.supportJobCat = supportJobCat;
		dueTime= support_ticketDAO.getCrossTime(support_ticketDTO);
	}
	
	
    @Override
	public String toString() {
            return "$Support_engineers_diaryDTO[" +
            " iD = " + iD +
            " supportTicketId = " + supportTicketId +
            " historyId = " + historyId +
            " supportEngineerOrg = " + supportEngineerOrg +
            " supportEngineerEid = " + supportEngineerEid +
            " bossEid = " + bossEid +
            " bossOrg = " + bossOrg +
            " assignmentTime = " + assignmentTime +
            " dueTime = " + dueTime +
            " doneTime = " + doneTime +
            " supportJobCat = " + supportJobCat +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}