package department_requisition_lot;
import java.util.*; 
import util.*; 


public class DepartmentRequisitionItemDTO extends CommonDTO
{

	public long departmentRequisitionLotId = -1;
	public long drugInformationType = -1;
	public int medicalItemCat = -1;
	public int quantity = -1;
	public int isApproved = 0;

	
    @Override
	public String toString() {
            return "$DepartmentRequisitionItemDTO[" +
            " iD = " + iD +
            " departmentRequisitionLotId = " + departmentRequisitionLotId +
            " drugInformationType = " + drugInformationType +
            " medicalItemCat = " + medicalItemCat +
            " quantity = " + quantity +
            " remarks = " + remarks +
            " isApproved = " + isApproved +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}