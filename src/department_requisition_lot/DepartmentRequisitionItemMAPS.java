package department_requisition_lot;
import java.util.*; 
import util.*;


public class DepartmentRequisitionItemMAPS extends CommonMaps
{	
	public DepartmentRequisitionItemMAPS(String tableName)
	{
		


		java_SQL_map.put("department_requisition_lot_id".toLowerCase(), "departmentRequisitionLotId".toLowerCase());
		java_SQL_map.put("drug_information_type".toLowerCase(), "drugInformationType".toLowerCase());
		java_SQL_map.put("medical_item_cat".toLowerCase(), "medicalItemCat".toLowerCase());
		java_SQL_map.put("quantity".toLowerCase(), "quantity".toLowerCase());
		java_SQL_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_SQL_map.put("is_approved".toLowerCase(), "isApproved".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Department Requisition Lot Id".toLowerCase(), "departmentRequisitionLotId".toLowerCase());
		java_Text_map.put("Drug Information".toLowerCase(), "drugInformationType".toLowerCase());
		java_Text_map.put("Medical Item".toLowerCase(), "medicalItemCat".toLowerCase());
		java_Text_map.put("Quantity".toLowerCase(), "quantity".toLowerCase());
		java_Text_map.put("Remarks".toLowerCase(), "remarks".toLowerCase());
		java_Text_map.put("Is Approved".toLowerCase(), "isApproved".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}