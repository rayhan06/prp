package department_requisition_lot;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Department_requisition_lotDAO  implements CommonDAOService<Department_requisition_lotDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Department_requisition_lotDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"medical_dept_cat",
			"description",
			"is_approved",
			"approvar_user_name",
			"approver_organogram_id",			
			"approval_time",
			"approval_message",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"search_column",
			"is_delivered",
			"deliverer_user_name",
			"deliverer_organogram_id",
			"delivery_date",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("medical_dept_cat"," and (medical_dept_cat = ?)");
		searchMap.put("description"," and (description like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Department_requisition_lotDAO INSTANCE = new Department_requisition_lotDAO();
	}

	public static Department_requisition_lotDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Department_requisition_lotDTO department_requisition_lotDTO)
	{
		department_requisition_lotDTO.searchColumn = "";
		department_requisition_lotDTO.searchColumn += CatDAO.getName("English", "medical_dept", department_requisition_lotDTO.medicalDeptCat) + " " + CatDAO.getName("Bangla", "medical_dept", department_requisition_lotDTO.medicalDeptCat) + " ";
		department_requisition_lotDTO.searchColumn += department_requisition_lotDTO.description + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Department_requisition_lotDTO department_requisition_lotDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(department_requisition_lotDTO);
		if(isInsert)
		{
			ps.setObject(++index,department_requisition_lotDTO.iD);
		}
		ps.setObject(++index,department_requisition_lotDTO.medicalDeptCat);
		ps.setObject(++index,department_requisition_lotDTO.description);
		ps.setObject(++index,department_requisition_lotDTO.isApproved);
		ps.setObject(++index,department_requisition_lotDTO.approverUserName);
		ps.setObject(++index,department_requisition_lotDTO.approverOrganogramId);
		ps.setObject(++index,department_requisition_lotDTO.approvalTime);
		ps.setObject(++index,department_requisition_lotDTO.approvalMessage);
		ps.setObject(++index,department_requisition_lotDTO.insertedByUserId);
		ps.setObject(++index,department_requisition_lotDTO.insertedByOrganogramId);
		ps.setObject(++index,department_requisition_lotDTO.insertionDate);
		ps.setObject(++index,department_requisition_lotDTO.lastModifierUser);
		ps.setObject(++index,department_requisition_lotDTO.searchColumn);
		ps.setObject(++index,department_requisition_lotDTO.isDelivered);
		ps.setObject(++index,department_requisition_lotDTO.delivererUserName);
		ps.setObject(++index,department_requisition_lotDTO.delivererOrganogramId);
		ps.setObject(++index,department_requisition_lotDTO.deliveryDate);
		if(isInsert)
		{
			ps.setObject(++index,department_requisition_lotDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,department_requisition_lotDTO.iD);
		}
	}
	
	@Override
	public Department_requisition_lotDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Department_requisition_lotDTO department_requisition_lotDTO = new Department_requisition_lotDTO();
			int i = 0;
			department_requisition_lotDTO.iD = rs.getLong(columnNames[i++]);
			department_requisition_lotDTO.medicalDeptCat = rs.getInt(columnNames[i++]);
			department_requisition_lotDTO.description = rs.getString(columnNames[i++]);
			department_requisition_lotDTO.isApproved = rs.getInt(columnNames[i++]);
			department_requisition_lotDTO.approverUserName = rs.getString(columnNames[i++]);
			department_requisition_lotDTO.approverOrganogramId = rs.getLong(columnNames[i++]);
			department_requisition_lotDTO.approvalTime = rs.getLong(columnNames[i++]);
			department_requisition_lotDTO.approvalMessage = rs.getString(columnNames[i++]);
			department_requisition_lotDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			department_requisition_lotDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			department_requisition_lotDTO.insertionDate = rs.getLong(columnNames[i++]);
			department_requisition_lotDTO.lastModifierUser = rs.getString(columnNames[i++]);
			department_requisition_lotDTO.searchColumn = rs.getString(columnNames[i++]);
			
			department_requisition_lotDTO.isDelivered = rs.getBoolean(columnNames[i++]);
			department_requisition_lotDTO.delivererUserName = rs.getString(columnNames[i++]);
			department_requisition_lotDTO.delivererOrganogramId = rs.getLong(columnNames[i++]);
			department_requisition_lotDTO.deliveryDate = rs.getLong(columnNames[i++]);
			
			department_requisition_lotDTO.isDeleted = rs.getInt(columnNames[i++]);
			department_requisition_lotDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return department_requisition_lotDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Department_requisition_lotDTO getDTOByID (long id)
	{
		Department_requisition_lotDTO department_requisition_lotDTO = null;
		try 
		{
			department_requisition_lotDTO = getDTOFromID(id);
			if(department_requisition_lotDTO != null)
			{
				DepartmentRequisitionItemDAO departmentRequisitionItemDAO = DepartmentRequisitionItemDAO.getInstance();				
				List<DepartmentRequisitionItemDTO> departmentRequisitionItemDTOList = (List<DepartmentRequisitionItemDTO>)departmentRequisitionItemDAO.getDTOsByParent("department_requisition_lot_id", department_requisition_lotDTO.iD);
				department_requisition_lotDTO.departmentRequisitionItemDTOList = departmentRequisitionItemDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return department_requisition_lotDTO;
	}

	@Override
	public String getTableName() {
		return "department_requisition_lot";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Department_requisition_lotDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Department_requisition_lotDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
				
}
	