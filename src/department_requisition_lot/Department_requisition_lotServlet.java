package department_requisition_lot;


import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDAO;
import user.UserDTO;
import user.UserRepository;
import util.*;

import javax.servlet.http.*;
import java.util.*;

import common.ApiResponse;
import common.BaseServlet;
import deliver_or_return_medicine.DeliverOrReturnDetailsDAO;
import deliver_or_return_medicine.DeliverOrReturnDetailsDTO;
import deliver_or_return_medicine.Deliver_or_return_medicineDAO;
import deliver_or_return_medicine.Deliver_or_return_medicineDTO;
import doctor_time_slot.Doctor_time_slotDAO;
import doctor_time_slot.Doctor_time_slotDTO;
import drug_information.Drug_informationDAO;
import drug_information.Drug_informationDTO;
import employee_offices.EmployeeOfficeRepository;
import medical_inventory_lot.MedicalInventoryInDAO;
import medical_inventory_lot.MedicalInventoryInDTO;
import medical_inventory_out.Medical_inventory_outDAO;
import medical_inventory_out.Medical_inventory_outDTO;
import pb.ErrorMessage;
import pb.Utils;
import pb_notifications.Pb_notificationsDAO;

import com.google.gson.Gson;

import borrow_medicine.PersonalStockDAO;
import borrow_medicine.PersonalStockDTO;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Department_requisition_lotServlet
 */
@WebServlet("/Department_requisition_lotServlet")
@MultipartConfig
public class Department_requisition_lotServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Department_requisition_lotServlet.class);
    Medical_inventory_outDAO medical_inventory_outDAO = new Medical_inventory_outDAO();
    MedicalInventoryInDAO medicalInventoryInDAO = new MedicalInventoryInDAO();

    @Override
    public String getTableName() {
        return Department_requisition_lotDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Department_requisition_lotServlet";
    }

    @Override
    public Department_requisition_lotDAO getCommonDAOService() {
        return Department_requisition_lotDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.DEPARTMENT_REQUISITION_LOT_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.DEPARTMENT_REQUISITION_LOT_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.DEPARTMENT_REQUISITION_LOT_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Department_requisition_lotServlet.class;
    }
	DepartmentRequisitionItemDAO departmentRequisitionItemDAO = DepartmentRequisitionItemDAO.getInstance();
    private final Gson gson = new Gson();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        init(request);
        String actionType = request.getParameter("actionType");
		if ("getApprovalPage".equals(actionType) && 
				(commonLoginData.userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE 
				|| commonLoginData.userDTO.roleID == SessionConstants.ADMIN_ROLE )) {
			try {
				request.getRequestDispatcher(commonPartOfDispatchURL()+"Approve.jsp").forward(request,response);
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else if ("deliverFromMyDepartmentalStock".equals(actionType)) {
			try {
				Doctor_time_slotDAO doctor_time_slotDAO = new Doctor_time_slotDAO();
				Doctor_time_slotDTO doctor_time_slotDTO = doctor_time_slotDAO.get1stDTOByDoctor(commonLoginData.userDTO.organogramID);
				if(doctor_time_slotDTO == null)
				{
					ApiResponse.sendErrorResponse(response, "Permission Issues");
				}
				request.setAttribute("medicalDeptCat", doctor_time_slotDTO.medicalDeptCat);
				request.setAttribute("isDelivery", true);

				request.getRequestDispatcher(getTableName() + "/" + "departmental_stockView.jsp").forward(request,response);
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else if ("viewDepartmentalStock".equals(actionType) && 
				(commonLoginData.userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE 
				|| commonLoginData.userDTO.roleID == SessionConstants.ADMIN_ROLE
				|| commonLoginData.userDTO.roleID == SessionConstants.NURSE_ROLE
				|| commonLoginData.userDTO.roleID == SessionConstants.DOCTOR_ROLE
				|| commonLoginData.userDTO.roleID == SessionConstants.PHARMACY_PERSON)) {
			try {
				request.setAttribute("medicalDeptCat", Integer.parseInt(request.getParameter("medicalDeptCat")));
				request.setAttribute("isDelivery", false);

				request.getRequestDispatcher(getTableName() + "/" + "departmental_stockView.jsp").forward(request,response);
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else if ("deliverFromShop".equals(actionType) && 
				(commonLoginData.userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE 
				|| commonLoginData.userDTO.roleID == SessionConstants.ADMIN_ROLE
				|| commonLoginData.userDTO.roleID == SessionConstants.NURSE_ROLE
				|| commonLoginData.userDTO.roleID == SessionConstants.PHARMACY_PERSON)) {
			try {
				setDTOForJsp(request);
                finalize(request);
				request.setAttribute("isDeliveryFromShop", true);
				request.getRequestDispatcher(commonPartOfDispatchURL() + "View.jsp?ID=" + getId(request)).forward(request, response);
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else if ("view".equals(actionType) &&
				(commonLoginData.userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE 
				|| commonLoginData.userDTO.roleID == SessionConstants.ADMIN_ROLE
				|| commonLoginData.userDTO.roleID == SessionConstants.NURSE_ROLE
				|| commonLoginData.userDTO.roleID == SessionConstants.DOCTOR_ROLE
				|| commonLoginData.userDTO.roleID == SessionConstants.PHARMACY_PERSON))
		{
			logger.debug("view requested");
            if (Utils.checkPermission(commonLoginData.userDTO, getViewMenuConstants()) && getViewPagePermission(request)) {
                try {
					setDTOForJsp(request);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                finalize(request);
                request.setAttribute("isDeliveryFromShop", false);
                request.getRequestDispatcher(commonPartOfDispatchURL() + "View.jsp?ID=" + getId(request)).forward(request, response);
                return;
            }
		}
		else if ("deliverDepartmentalStock".equals(actionType) && 
				(commonLoginData.userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE 
				|| commonLoginData.userDTO.roleID == SessionConstants.ADMIN_ROLE
				|| commonLoginData.userDTO.roleID == SessionConstants.NURSE_ROLE
				|| commonLoginData.userDTO.roleID == SessionConstants.DOCTOR_ROLE)) {
			try {
				request.setAttribute("medicalDeptCat", Integer.parseInt(request.getParameter("medicalDeptCat")));
				request.setAttribute("isDelivery", true);
				request.getRequestDispatcher(getTableName() + "/" + "departmental_stockView.jsp").forward(request,response);
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else if ("confirmDelivery".equals(actionType) && 
				(commonLoginData.userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE 
				|| commonLoginData.userDTO.roleID == SessionConstants.ADMIN_ROLE 
				|| commonLoginData.userDTO.roleID == SessionConstants.PHARMACY_PERSON )) {
			try {
				deliverFromShop(request, response, commonLoginData.userDTO);
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else
		{
			super.doGet(request,response);
		}
    }
    
    private void sendRequisitionRequest(Department_requisition_lotDTO department_requisition_lotDTO)
	{
		String URL = "";
		String englishText = "";
		String banglaText = "";
		
		URL = "Department_requisition_lotServlet?actionType=view&ID=" + department_requisition_lotDTO.iD;
		englishText = "Departmental Medical Requisition.";
		banglaText = "দপ্তরভিত্তিক ঔষধপত্রের রিকুইজিশন।";
		
		Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();
		Set<Long> medicalAdmins = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.MEDICAL_ADMIN_ROLE);
		for(Long medicalAdmin: medicalAdmins)
		{
			pb_notificationsDAO.addPb_notificationsAndSendMailSMS(medicalAdmin, System.currentTimeMillis(), URL, englishText, banglaText, "Departmental Medical Requisition", SessionConstants.MEDICAL_ADMIN_ROLE);
		}
	}
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        init(request);
        String actionType = request.getParameter("actionType");
		if ("approve".equals(actionType) && 
				(commonLoginData.userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE 
				|| commonLoginData.userDTO.roleID == SessionConstants.ADMIN_ROLE )) {
			try {
				approveOrReject(request, response, commonLoginData.userDTO);
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else if ("deliver".equals(actionType) && 
				(commonLoginData.userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE 
				|| commonLoginData.userDTO.roleID == SessionConstants.ADMIN_ROLE
				|| commonLoginData.userDTO.roleID == SessionConstants.NURSE_ROLE
				|| commonLoginData.userDTO.roleID == SessionConstants.DOCTOR_ROLE )) {
			try {
				deliver(request, response, commonLoginData.userDTO);
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		
		else
		{
			super.doPost(request,response);
		}
    }
 	

	private void deliver(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception {
		// TODO Auto-generated method stub
		int  medicalDeptCat = Integer.parseInt(request.getParameter("medicalDeptCat"));
		Deliver_or_return_medicineDAO deliver_or_return_medicineDAO = Deliver_or_return_medicineDAO.getInstance();
		DeliverOrReturnDetailsDAO deliverOrReturnDetailsDAO = DeliverOrReturnDetailsDAO.getInstance();
		
		
		PersonalStockDAO personalStockDAO = PersonalStockDAO.getInstance();
		Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
     	List<PersonalStockDTO> personalStockDTOs = (List<PersonalStockDTO>)personalStockDAO.getDTOsByParent("medical_dept_cat", medicalDeptCat);
     	
     	String userName = Utils.getDigitEnglishFromBangla(
     			Jsoup.clean(request.getParameter("userName"),Whitelist.simpleText()));
     	String name = Jsoup.clean(request.getParameter("name"),Whitelist.simpleText());
     	String phone = Utils.getDigitEnglishFromBangla(Jsoup.clean(request.getParameter("phone"),Whitelist.simpleText()));
     	UserDTO patientDTO = UserRepository.getUserDTOByUserName(userName);
     	if(patientDTO == null)
     	{
     		UserDAO userDAO = new UserDAO();
     		patientDTO = userDAO.getUserDTOByUsernameFromOnlyUsers(userName);
     	}
     	System.out.println("medicalDeptCat = " + medicalDeptCat + " list size = " + personalStockDTOs.size() + " patientDTO = " + patientDTO);
     	
     	if(patientDTO != null)
     	{
     		int i = 0;
     		Deliver_or_return_medicineDTO deliver_or_return_medicineDTO = new Deliver_or_return_medicineDTO(userDTO, patientDTO);
     		deliver_or_return_medicineDAO.add(deliver_or_return_medicineDTO);
     		
     		for(PersonalStockDTO personalStockDTO: personalStockDTOs)
         	{
     			int quantity = Integer.parseInt(request.getParameterValues("quantity")[i]);
				Drug_informationDTO drug_informationDTO = drug_informationDAO.getDTOByID(personalStockDTO.drugInformationType);
				System.out.println("quantity = " + quantity);
				if(quantity > 0 && quantity <= personalStockDTO.quantity)
				{
					
					//Give back to stock
					personalStockDTO.quantity -= quantity;				
					PersonalStockDAO.getInstance().update(personalStockDTO);
					
					MedicalInventoryInDTO medicalInventoryInDTO = new MedicalInventoryInDTO(medicalDeptCat, drug_informationDTO,
							quantity, userDTO, patientDTO);
					
					medicalInventoryInDAO.add(medicalInventoryInDTO);
					
					
					//Give to patient
					
					PersonalStockDTO patientStockDTO = PersonalStockDAO.getInstance().getDTOByUserNameAndDrugId(patientDTO.userName,
							personalStockDTO.drugInformationType, SessionConstants.MEDICAL_ITEM_DRUG);
					if(patientStockDTO == null)
					{
						
						patientStockDTO = new PersonalStockDTO(patientDTO, drug_informationDTO, quantity, SessionConstants.MEDICAL_ITEM_DRUG);
						PersonalStockDAO.getInstance().add(patientStockDTO);
					}
					else
					{
						patientStockDTO.quantity += quantity;
						PersonalStockDAO.getInstance().update(patientStockDTO);
					}
					
					Medical_inventory_outDTO medical_inventory_outDTO = new Medical_inventory_outDTO(drug_informationDTO,
							quantity, userDTO, MedicalInventoryInDTO.TR_RECEIVE_MEDICINE,
							patientDTO, SessionConstants.MEDICAL_ITEM_DRUG,
							name, phone, -1, -1);				
					medical_inventory_outDAO.add(medical_inventory_outDTO);
					
					DeliverOrReturnDetailsDTO deliverOrReturnDetailsDTO = new DeliverOrReturnDetailsDTO(deliver_or_return_medicineDTO, drug_informationDTO, quantity);
					deliverOrReturnDetailsDAO.add(deliverOrReturnDetailsDTO);
					
				}

     			i++;
         	}
     		
     	}
     	ApiResponse.sendSuccessResponse(response,  "Transaction_report_Servlet?actionType=reportPage");
     	
	}
	
	private void deliverFromShop(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO)
	{
		Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
		long id = Long.parseLong(request.getParameter("id"));
		
		Department_requisition_lotDTO department_requisition_lotDTO = Department_requisition_lotDAO.getInstance().getDTOFromID(id);
		if(department_requisition_lotDTO.isApproved == 1 && !department_requisition_lotDTO.isDelivered)
		{
			int i = 0;
			List<DepartmentRequisitionItemDTO> departmentRequisitionItemDTOs = (List<DepartmentRequisitionItemDTO>)departmentRequisitionItemDAO.getDTOsByParent("department_requisition_lot_id", department_requisition_lotDTO.iD);

			for(DepartmentRequisitionItemDTO dri: departmentRequisitionItemDTOs)
			{
				
				Drug_informationDTO drug_informationDTO = drug_informationDAO.getDTOByID(dri.drugInformationType);
				System.out.println("quantity = " + dri.quantity);
				if(dri.quantity > drug_informationDTO.availableStock)
				{
					dri.quantity = drug_informationDTO.availableStock;
					try {
						departmentRequisitionItemDAO.update(dri);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if(dri.quantity > 0)
				{
					Medical_inventory_outDTO medical_inventory_outDTO = new Medical_inventory_outDTO(department_requisition_lotDTO, dri,
							drug_informationDTO, userDTO);				
					try {
						medical_inventory_outDAO.add(medical_inventory_outDTO);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					PersonalStockDTO personalStockDTO = PersonalStockDAO.getInstance().getDTOByDeptAndDrugId(department_requisition_lotDTO.medicalDeptCat,
							dri.drugInformationType, SessionConstants.MEDICAL_ITEM_DRUG);
					if(personalStockDTO == null)
					{
						personalStockDTO = new PersonalStockDTO(department_requisition_lotDTO, dri, drug_informationDTO);
						try {
							PersonalStockDAO.getInstance().add(personalStockDTO);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					else
					{
						personalStockDTO.quantity += dri.quantity;
						if(personalStockDTO.drugInformationType == Drug_informationDTO.ACCU_CHECK_ID)
						{
							personalStockDTO.detailedCount += Drug_informationDTO.ACCU_SIZE *dri.quantity ;
						}
						try {
							PersonalStockDAO.getInstance().update(personalStockDTO);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
				}
				
				i ++;
			}
			
			department_requisition_lotDTO.delivererUserName = userDTO.userName;
			department_requisition_lotDTO.delivererOrganogramId = userDTO.organogramID;
			department_requisition_lotDTO.isDelivered = true;
			department_requisition_lotDTO.deliveryDate = System.currentTimeMillis();
			try {
				Department_requisition_lotDAO.getInstance().update(department_requisition_lotDTO);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			response.sendRedirect(getAddRedirectURL(request, department_requisition_lotDTO));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void approveOrReject(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
		Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
		long id = Long.parseLong(request.getParameter("id"));
		int  approvalAction = Integer.parseInt(request.getParameter("approvalAction"));
		if(approvalAction != 1 && approvalAction != 2)
		{
			approvalAction = 2;
		}
		Department_requisition_lotDTO department_requisition_lotDTO = Department_requisition_lotDAO.getInstance().getDTOFromID(id);
		if(department_requisition_lotDTO != null)
		{
			department_requisition_lotDTO.isApproved = approvalAction;
		}
		department_requisition_lotDTO.approvalMessage = request.getParameter("approvalMessage");
		department_requisition_lotDTO.approverUserName = userDTO.userName;
		department_requisition_lotDTO.approverOrganogramId = userDTO.organogramID;
		department_requisition_lotDTO.approvalTime = System.currentTimeMillis();
		if(department_requisition_lotDTO.isApproved == 1)
		{
			int i = 0;
			List<DepartmentRequisitionItemDTO> departmentRequisitionItemDTOs = (List<DepartmentRequisitionItemDTO>)departmentRequisitionItemDAO.getDTOsByParent("department_requisition_lot_id", department_requisition_lotDTO.iD);

			for(DepartmentRequisitionItemDTO dri: departmentRequisitionItemDTOs)
			{
				int quantity = Integer.parseInt(request.getParameterValues("departmentRequisitionItem.quantity")[i]);
				Drug_informationDTO drug_informationDTO = drug_informationDAO.getDTOByID(dri.drugInformationType);
				System.out.println("quantity = " + quantity);
				if(quantity > 0 && quantity <= drug_informationDTO.availableStock)
				{
					dri.quantity = quantity;
					dri.isApproved = 1;
					try {
						departmentRequisitionItemDAO.update(dri);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				i ++;
			}
		}

		
		
		try {
			Department_requisition_lotDAO.getInstance().update(department_requisition_lotDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		try {
			ApiResponse.sendSuccessResponse(response, getAjaxAddRedirectURL(request, department_requisition_lotDTO));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addDepartment_requisition_lot");
		String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		Department_requisition_lotDTO department_requisition_lotDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
		if(addFlag)
		{
			department_requisition_lotDTO = new Department_requisition_lotDTO();
		}
		else
		{
			department_requisition_lotDTO = Department_requisition_lotDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		Value = request.getParameter("medicalDeptCat");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("medicalDeptCat = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			department_requisition_lotDTO.medicalDeptCat = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("description");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("description = " + Value);
		if(Value != null)
		{
			department_requisition_lotDTO.description = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		department_requisition_lotDTO.isApproved = 0;

	

		if(addFlag)
		{
			department_requisition_lotDTO.insertedByUserId = userDTO.ID;
		}


		if(addFlag)
		{
			department_requisition_lotDTO.insertedByOrganogramId = userDTO.organogramID;
		}


		if(addFlag)
		{				
			department_requisition_lotDTO.insertionDate = TimeConverter.getToday();
		}			


		department_requisition_lotDTO.lastModifierUser = userDTO.userName;

		System.out.println("Done adding  addDepartment_requisition_lot dto = " + department_requisition_lotDTO);

		if(addFlag == true)
		{
			Department_requisition_lotDAO.getInstance().add(department_requisition_lotDTO);
		}
		else
		{				
			Department_requisition_lotDAO.getInstance().update(department_requisition_lotDTO);										
		}
		
		List<DepartmentRequisitionItemDTO> departmentRequisitionItemDTOList = createDepartmentRequisitionItemDTOListByRequest(request, language, department_requisition_lotDTO, userDTO);
		for(DepartmentRequisitionItemDTO departmentRequisitionItemDTO: departmentRequisitionItemDTOList)
		{
			if(departmentRequisitionItemDTO.drugInformationType == Drug_informationDTO.ACCU_CHECK_ID)
			{
				if(userDTO.roleID != SessionConstants.NURSE_ROLE && userDTO.roleID != SessionConstants.ADMIN_ROLE)
				{
					Department_requisition_lotDAO.getInstance().hardDelete(department_requisition_lotDTO.iD);
					throw new Exception("You cannot request for accu-check");
					
				}
				if(department_requisition_lotDTO.medicalDeptCat != Department_requisition_lotDTO.EMERGENCY)
				{
					Department_requisition_lotDAO.getInstance().hardDelete(department_requisition_lotDTO.iD);
					throw new Exception("Department " + ErrorMessage.getInvalidMessage(language));
				}
			}
		}
		

		if(addFlag == true) //add or validate
		{
			if(departmentRequisitionItemDTOList != null)
			{				
				for(DepartmentRequisitionItemDTO departmentRequisitionItemDTO: departmentRequisitionItemDTOList)
				{
					departmentRequisitionItemDAO.add(departmentRequisitionItemDTO);
				}
			}
		
		}			
		else
		{
			List<Long> childIdsFromRequest = departmentRequisitionItemDAO.getChildIdsFromRequest(request, "departmentRequisitionItem");
			//delete the removed children
			departmentRequisitionItemDAO.deleteChildrenNotInList("department_requisition_lot", "department_requisition_item", department_requisition_lotDTO.iD, childIdsFromRequest);
			List<Long> childIDsInDatabase = departmentRequisitionItemDAO.getChilIds("department_requisition_lot", "department_requisition_item", department_requisition_lotDTO.iD);
			
			
			if(childIdsFromRequest != null)
			{
				for(int i = 0; i < childIdsFromRequest.size(); i ++)
				{
					Long childIDFromRequest = childIdsFromRequest.get(i);
					if(childIDsInDatabase.contains(childIDFromRequest))
					{
						DepartmentRequisitionItemDTO departmentRequisitionItemDTO =  createDepartmentRequisitionItemDTOByRequestAndIndex(request, false, i, language, department_requisition_lotDTO, userDTO);
						departmentRequisitionItemDAO.update(departmentRequisitionItemDTO);
					}
					else
					{
						DepartmentRequisitionItemDTO departmentRequisitionItemDTO =  createDepartmentRequisitionItemDTOByRequestAndIndex(request, true, i, language, department_requisition_lotDTO, userDTO);
						departmentRequisitionItemDAO.add(departmentRequisitionItemDTO);
					}
				}
			}
			else
			{
				departmentRequisitionItemDAO.deleteChildrenByParent(department_requisition_lotDTO.iD, "department_requisition_lot_id");
			}
			
		}					
		return department_requisition_lotDTO;

	}
	private List<DepartmentRequisitionItemDTO> createDepartmentRequisitionItemDTOListByRequest(HttpServletRequest request, String language, Department_requisition_lotDTO department_requisition_lotDTO, UserDTO userDTO) throws Exception{ 
		List<DepartmentRequisitionItemDTO> departmentRequisitionItemDTOList = new ArrayList<DepartmentRequisitionItemDTO>();
		if(request.getParameterValues("departmentRequisitionItem.iD") != null) 
		{
			int departmentRequisitionItemItemNo = request.getParameterValues("departmentRequisitionItem.iD").length;
			
			
			for(int index=0;index<departmentRequisitionItemItemNo;index++){
				DepartmentRequisitionItemDTO departmentRequisitionItemDTO = createDepartmentRequisitionItemDTOByRequestAndIndex(request,true,index, language, department_requisition_lotDTO, userDTO);
				departmentRequisitionItemDTOList.add(departmentRequisitionItemDTO);
			}
			
			return departmentRequisitionItemDTOList;
		}
		return null;
	}
	
	private DepartmentRequisitionItemDTO createDepartmentRequisitionItemDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, String language, Department_requisition_lotDTO department_requisition_lotDTO, UserDTO userDTO) throws Exception{
	
		DepartmentRequisitionItemDTO departmentRequisitionItemDTO;
		if(addFlag == true )
		{
			departmentRequisitionItemDTO = new DepartmentRequisitionItemDTO();
		}
		else
		{
			departmentRequisitionItemDTO = departmentRequisitionItemDAO.getDTOByID(Long.parseLong(request.getParameterValues("departmentRequisitionItem.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		departmentRequisitionItemDTO.departmentRequisitionLotId = department_requisition_lotDTO.iD; 

		String Value = "";

		Value = request.getParameterValues("departmentRequisitionItem.drugInformationType")[index];

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("drugInformationType = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			departmentRequisitionItemDTO.drugInformationType = Long.parseLong(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		departmentRequisitionItemDTO.medicalItemCat = SessionConstants.MEDICAL_ITEM_DRUG;

		Value = request.getParameterValues("departmentRequisitionItem.quantity")[index];

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("quantity = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			departmentRequisitionItemDTO.quantity = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameterValues("departmentRequisitionItem.remarks")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("remarks = " + Value);
		if(Value != null)
		{
			departmentRequisitionItemDTO.remarks = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		departmentRequisitionItemDTO.isApproved = 0;

		
		return departmentRequisitionItemDTO;
	
	}	
}

