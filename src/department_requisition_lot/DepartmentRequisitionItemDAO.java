package department_requisition_lot;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class DepartmentRequisitionItemDAO  implements CommonDAOService<DepartmentRequisitionItemDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private DepartmentRequisitionItemDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"department_requisition_lot_id",
			"drug_information_type",
			"medical_item_cat",
			"quantity",
			"remarks",
			"is_approved",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("drug_information_type"," and (drug_information_type = ?)");
		searchMap.put("medical_item_cat"," and (medical_item_cat = ?)");
		searchMap.put("remarks"," and (remarks like ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final DepartmentRequisitionItemDAO INSTANCE = new DepartmentRequisitionItemDAO();
	}

	public static DepartmentRequisitionItemDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(DepartmentRequisitionItemDTO departmentrequisitionitemDTO)
	{
		departmentrequisitionitemDTO.searchColumn = "";
		departmentrequisitionitemDTO.searchColumn += CommonDAO.getName("English", "drug_information", departmentrequisitionitemDTO.drugInformationType) + " " + CommonDAO.getName("Bangla", "drug_information", departmentrequisitionitemDTO.drugInformationType) + " ";
		departmentrequisitionitemDTO.searchColumn += CatDAO.getName("English", "medical_item", departmentrequisitionitemDTO.medicalItemCat) + " " + CatDAO.getName("Bangla", "medical_item", departmentrequisitionitemDTO.medicalItemCat) + " ";
		departmentrequisitionitemDTO.searchColumn += departmentrequisitionitemDTO.remarks + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, DepartmentRequisitionItemDTO departmentrequisitionitemDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(departmentrequisitionitemDTO);
		if(isInsert)
		{
			ps.setObject(++index,departmentrequisitionitemDTO.iD);
		}
		ps.setObject(++index,departmentrequisitionitemDTO.departmentRequisitionLotId);
		ps.setObject(++index,departmentrequisitionitemDTO.drugInformationType);
		ps.setObject(++index,departmentrequisitionitemDTO.medicalItemCat);
		ps.setObject(++index,departmentrequisitionitemDTO.quantity);
		ps.setObject(++index,departmentrequisitionitemDTO.remarks);
		ps.setObject(++index,departmentrequisitionitemDTO.isApproved);
		if(isInsert)
		{
			ps.setObject(++index,departmentrequisitionitemDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,departmentrequisitionitemDTO.iD);
		}
	}
	
	@Override
	public DepartmentRequisitionItemDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			DepartmentRequisitionItemDTO departmentrequisitionitemDTO = new DepartmentRequisitionItemDTO();
			int i = 0;
			departmentrequisitionitemDTO.iD = rs.getLong(columnNames[i++]);
			departmentrequisitionitemDTO.departmentRequisitionLotId = rs.getLong(columnNames[i++]);
			departmentrequisitionitemDTO.drugInformationType = rs.getLong(columnNames[i++]);
			departmentrequisitionitemDTO.medicalItemCat = rs.getInt(columnNames[i++]);
			departmentrequisitionitemDTO.quantity = rs.getInt(columnNames[i++]);
			departmentrequisitionitemDTO.remarks = rs.getString(columnNames[i++]);
			departmentrequisitionitemDTO.isApproved = rs.getInt(columnNames[i++]);
			departmentrequisitionitemDTO.isDeleted = rs.getInt(columnNames[i++]);
			departmentrequisitionitemDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return departmentrequisitionitemDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public DepartmentRequisitionItemDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "department_requisition_item";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((DepartmentRequisitionItemDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((DepartmentRequisitionItemDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	