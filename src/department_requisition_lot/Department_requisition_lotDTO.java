package department_requisition_lot;
import java.util.*; 
import util.*; 


public class Department_requisition_lotDTO extends CommonDTO
{

	public int medicalDeptCat = -1;
    public String description = "";
	public int isApproved = 0;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
    public String approvalMessage = "";
    public String approverUserName = "";
    public long approverOrganogramId = -1;
    public long approvalTime = -1;
    
    public boolean isDelivered = false;
    public String delivererUserName = "";
    public long delivererOrganogramId = -1;
    public long deliveryDate = -1;
    
    public static final long EMERGENCY = 17;
	
	public List<DepartmentRequisitionItemDTO> departmentRequisitionItemDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Department_requisition_lotDTO[" +
            " iD = " + iD +
            " medicalDeptCat = " + medicalDeptCat +
            " description = " + description +
            " isApproved = " + isApproved +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}