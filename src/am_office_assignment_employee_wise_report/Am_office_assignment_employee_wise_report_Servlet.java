package am_office_assignment_employee_wise_report;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;


@WebServlet("/Am_office_assignment_employee_wise_report_Servlet")
public class Am_office_assignment_employee_wise_report_Servlet extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    String[][] Criteria =
            {
                    {"criteria", "tr", "assignment_date", ">=", "", "long", "", "", Long.MIN_VALUE + "", "startDate", LC.HM_START_DATE + ""},
                    {"criteria", "tr", "assignment_date", "<=", "AND", "long", "", "", Long.MAX_VALUE + "", "endDate", LC.HM_END_DATE + ""},
                    {"criteria", "tr", "withdrawal_date", ">=", "AND", "long", "", "", Long.MIN_VALUE + "", "withdrawalStartDate", LC.HM_END_DATE + ""},
                    {"criteria", "tr", "withdrawal_date", "<=", "AND", "long", "", "", Long.MAX_VALUE + "", "withdrawalEndDate", LC.HM_END_DATE + ""},
                    {"criteria", "tr", "requester_emp_id", "=", "AND", "String", "", "", "any", "requesterEmpId", LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_WHERE_REQUESTEREMPID + ""},
                    {"criteria", "tr", "building_type_cat", "=", "AND", "int", "", "", "any", "buildingTypeCat", LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_WHERE_BUILDINGTYPECAT + ""},
                    {"criteria", "tr", "isDeleted", "=", "AND", "String", "", "", "0", "isDeleted", LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_WHERE_ISDELETED + ""}
            };

    String[][] Display =
            {
                    {"display", "tr", "requester_emp_id", "erIdToUserName", ""},
                    {"display", "tr", "requester_emp_id", "employee_records_id", ""},
                    {"display", "tr", "building_type_cat", "cat", ""},
                    {"display", "tr", "concatedOfcTypeAndOfcUnit", "concated_to_office", ""},
                    {"display", "tr", "status", "am_office_status_check", ""},
                    {"display", "tr", "assignment_date", "date", ""},
                    {"display", "tr", "withdrawal_date", "dateConverterForMinusOne", ""},
                    {"display", "tr", "assignment_details_en", "text", ""},
                    {"display", "tr", "assignment_details_bn", "text", ""}
            };

    String[][] Display2 =
            {
                    {"display", "tr", "requester_emp_id", "erIdToUserName", ""},
                    {"display", "tr", "requester_emp_id", "employee_records_id", ""},
                    {"display", "tr", "building_type_cat", "cat", ""},
                    {"display", "tr", "concatedOfcTypeAndOfcUnit", "concated_to_office", ""},
                    {"display", "tr", "status", "am_office_status_check", ""},
                    {"display", "tr", "assignment_date", "date", ""},
                    {"display", "tr", "withdrawal_date", "dateConverterForMinusOne", ""},
                    {"display", "tr", "assignment_details_en", "text", ""},
                    {"display", "tr", "assignment_details_bn", "text", ""}
            };

    String GroupBy = "";
    String OrderBY = "";

    ReportRequestHandler reportRequestHandler;

    public Am_office_assignment_employee_wise_report_Servlet() {

    }

    private final ReportService reportService = new ReportService();

    private String sql;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String language = "english";
        if (userDTO != null && userDTO.languageID == SessionConstants.BANGLA) {
            language = "bangla";
        }


        String actionType = request.getParameter("actionType");

        sql = "am_office_assignment tr ";


        if (language.equalsIgnoreCase("bangla")) {
            List<String[]> l = new ArrayList<String[]>(Arrays.asList(Display2));
            l.remove(7);
            Display = l.toArray(new String[][]{});
        } else {
            List<String[]> l = new ArrayList<String[]>(Arrays.asList(Display2));
            l.remove(8);
            Display = l.toArray(new String[][]{});
        }

        int i = 0;
        Display[i++][4] = LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_SELECT_USERNAME, loginDTO);
        Display[i++][4] = LM.getText(LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_SELECT_REQUESTEREMPID, loginDTO);
        Display[i++][4] = LM.getText(LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_SELECT_BUILDINGTYPECAT, loginDTO);
        Display[i++][4] = LM.getText(LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_SELECT_OFFICEUNITID, loginDTO);
        Display[i++][4] = LM.getText(LC.FUND_APPROVAL_MAPPING_ADD_FUNDAPPLICATIONSTATUSCAT, loginDTO);
        Display[i++][4] = LM.getText(LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_SELECT_ASSIGNMENTDATE, loginDTO);
        Display[i++][4] = LM.getText(LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_SELECT_WITHDRAWALDATE, loginDTO);
        Display[i++][4] = LM.getText(LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_SELECT_ASSIGNMENTDETAILSEN, loginDTO);

        String reportName = LM.getText(LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_OTHER_AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT, loginDTO);

        reportRequestHandler = new ReportRequestHandler(null,
                Criteria, Display, GroupBy, OrderBY, sql,
                reportService);


        reportRequestHandler.handleReportGet(request, response, userDTO, "am_office_assignment_employee_wise_report",
                MenuConstants.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_DETAILS, language, reportName, "am_office_assignment_employee_wise_report");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
