package pi_package_auctioneer;
import java.util.*; 
import util.*;


public class PiPackageAuctioneerChildrenMAPS extends CommonMaps
{	
	public PiPackageAuctioneerChildrenMAPS(String tableName)
	{
		


		java_SQL_map.put("name".toLowerCase(), "name".toLowerCase());
		java_SQL_map.put("address".toLowerCase(), "address".toLowerCase());
		java_SQL_map.put("mobile".toLowerCase(), "mobile".toLowerCase());
		java_SQL_map.put("is_chosen".toLowerCase(), "isChosen".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Pi Package Auctioneer Id".toLowerCase(), "piPackageAuctioneerId".toLowerCase());
		java_Text_map.put("Auctioneer Name".toLowerCase(), "name".toLowerCase());
		java_Text_map.put("Address".toLowerCase(), "address".toLowerCase());
		java_Text_map.put("Mobile Number".toLowerCase(), "mobile".toLowerCase());
		java_Text_map.put("Tender Winner".toLowerCase(), "isChosen".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
			
	}

}