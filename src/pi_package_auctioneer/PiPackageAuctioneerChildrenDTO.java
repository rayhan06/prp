package pi_package_auctioneer;
import java.util.*; 
import util.*; 


public class PiPackageAuctioneerChildrenDTO extends CommonDTO
{

	public long piPackageAuctioneerId = -1;
    public String name = "";
    public String address = "";
    public String mobile = "";
	public boolean isChosen = false;
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
    public int auctioneerStatus = -1;
    public long actualAuctioneerId = -1;
	
	public List<PiPackageAuctioneerChildrenDTO> piPackageAuctioneerChildrenDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$PiPackageAuctioneerChildrenDTO[" +
            " iD = " + iD +
            " piPackageAuctioneerId = " + piPackageAuctioneerId +
            " name = " + name +
            " address = " + address +
            " mobile = " + mobile +
            " isChosen = " + isChosen +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            " auctioneerStatus = " + auctioneerStatus +
            " actualAuctioneerId = " + actualAuctioneerId +
            "]";
    }

}