package pi_package_auctioneer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class PiPackageAuctioneerChildrenRepository implements Repository {
	PiPackageAuctioneerChildrenDAO pipackageauctioneerchildrenDAO = null;
	
	static Logger logger = Logger.getLogger(PiPackageAuctioneerChildrenRepository.class);
	Map<Long, PiPackageAuctioneerChildrenDTO>mapOfPiPackageAuctioneerChildrenDTOToiD;
	Map<Long, Set<PiPackageAuctioneerChildrenDTO> >mapOfPiPackageAuctioneerChildrenDTOTopiPackageAuctioneerId;
	Gson gson;

  
	private PiPackageAuctioneerChildrenRepository(){
		pipackageauctioneerchildrenDAO = PiPackageAuctioneerChildrenDAO.getInstance();
		mapOfPiPackageAuctioneerChildrenDTOToiD = new ConcurrentHashMap<>();
		mapOfPiPackageAuctioneerChildrenDTOTopiPackageAuctioneerId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static PiPackageAuctioneerChildrenRepository INSTANCE = new PiPackageAuctioneerChildrenRepository();
    }

    public static PiPackageAuctioneerChildrenRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<PiPackageAuctioneerChildrenDTO> pipackageauctioneerchildrenDTOs = pipackageauctioneerchildrenDAO.getAllDTOs(reloadAll);
			for(PiPackageAuctioneerChildrenDTO pipackageauctioneerchildrenDTO : pipackageauctioneerchildrenDTOs) {
				PiPackageAuctioneerChildrenDTO oldPiPackageAuctioneerChildrenDTO = getPiPackageAuctioneerChildrenDTOByiD(pipackageauctioneerchildrenDTO.iD);
				if( oldPiPackageAuctioneerChildrenDTO != null ) {
					mapOfPiPackageAuctioneerChildrenDTOToiD.remove(oldPiPackageAuctioneerChildrenDTO.iD);
				
					if(mapOfPiPackageAuctioneerChildrenDTOTopiPackageAuctioneerId.containsKey(oldPiPackageAuctioneerChildrenDTO.piPackageAuctioneerId)) {
						mapOfPiPackageAuctioneerChildrenDTOTopiPackageAuctioneerId.get(oldPiPackageAuctioneerChildrenDTO.piPackageAuctioneerId).remove(oldPiPackageAuctioneerChildrenDTO);
					}
					if(mapOfPiPackageAuctioneerChildrenDTOTopiPackageAuctioneerId.get(oldPiPackageAuctioneerChildrenDTO.piPackageAuctioneerId).isEmpty()) {
						mapOfPiPackageAuctioneerChildrenDTOTopiPackageAuctioneerId.remove(oldPiPackageAuctioneerChildrenDTO.piPackageAuctioneerId);
					}
					
					
				}
				if(pipackageauctioneerchildrenDTO.isDeleted == 0) 
				{
					
					mapOfPiPackageAuctioneerChildrenDTOToiD.put(pipackageauctioneerchildrenDTO.iD, pipackageauctioneerchildrenDTO);
				
					if( ! mapOfPiPackageAuctioneerChildrenDTOTopiPackageAuctioneerId.containsKey(pipackageauctioneerchildrenDTO.piPackageAuctioneerId)) {
						mapOfPiPackageAuctioneerChildrenDTOTopiPackageAuctioneerId.put(pipackageauctioneerchildrenDTO.piPackageAuctioneerId, new HashSet<>());
					}
					mapOfPiPackageAuctioneerChildrenDTOTopiPackageAuctioneerId.get(pipackageauctioneerchildrenDTO.piPackageAuctioneerId).add(pipackageauctioneerchildrenDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public PiPackageAuctioneerChildrenDTO clone(PiPackageAuctioneerChildrenDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, PiPackageAuctioneerChildrenDTO.class);
	}
	
	
	public List<PiPackageAuctioneerChildrenDTO> getPiPackageAuctioneerChildrenList() {
		List <PiPackageAuctioneerChildrenDTO> pipackageauctioneerchildrens = new ArrayList<PiPackageAuctioneerChildrenDTO>(this.mapOfPiPackageAuctioneerChildrenDTOToiD.values());
		return pipackageauctioneerchildrens;
	}
	
	public List<PiPackageAuctioneerChildrenDTO> copyPiPackageAuctioneerChildrenList() {
		List <PiPackageAuctioneerChildrenDTO> pipackageauctioneerchildrens = getPiPackageAuctioneerChildrenList();
		return pipackageauctioneerchildrens
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public PiPackageAuctioneerChildrenDTO getPiPackageAuctioneerChildrenDTOByiD( long iD){
		return mapOfPiPackageAuctioneerChildrenDTOToiD.get(iD);
	}
	
	public PiPackageAuctioneerChildrenDTO copyPiPackageAuctioneerChildrenDTOByiD( long iD){
		return clone(mapOfPiPackageAuctioneerChildrenDTOToiD.get(iD));
	}
	
	
	public List<PiPackageAuctioneerChildrenDTO> getPiPackageAuctioneerChildrenDTOBypiPackageAuctioneerId(long piPackageAuctioneerId) {
		return new ArrayList<>( mapOfPiPackageAuctioneerChildrenDTOTopiPackageAuctioneerId.getOrDefault(piPackageAuctioneerId,new HashSet<>()));
	}
	
	public List<PiPackageAuctioneerChildrenDTO> copyPiPackageAuctioneerChildrenDTOBypiPackageAuctioneerId(long piPackageAuctioneerId)
	{
		List <PiPackageAuctioneerChildrenDTO> pipackageauctioneerchildrens = getPiPackageAuctioneerChildrenDTOBypiPackageAuctioneerId(piPackageAuctioneerId);
		return pipackageauctioneerchildrens
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return pipackageauctioneerchildrenDAO.getTableName();
	}
}


