package pi_package_auctioneer;
import java.util.*; 
import util.*; 


public class Pi_package_auctioneerDTO extends CommonDTO
{

	public long fiscalYearId = -1;
	public long piAuctionId = -1;
    public String momentCodeTime = "12:00 PM";
	public String tenderAdvertiseCat = "";
	public long tenderAdvertiseDate = System.currentTimeMillis();
    public String tenderAdvertiseTime = "12:00 PM";
	public long tenderOpeningDate = System.currentTimeMillis();
    public String tenderOpeningTime = "12:00 PM";
	public long noaDate = System.currentTimeMillis();
    public String noaTime = "12:00 PM";
	public long agreementDate = System.currentTimeMillis();
    public String agreementTime = "12:00 PM";
	public long agreementEndingDate = System.currentTimeMillis();
    public String agreementEndingTime = "12:00 PM";
	public long filesDropzone = -1;
	public int auctioneerStatus = -1;
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";

	public long officeUnitId = -1;


	public long winnerAuctioneerId = -1;
	public String winnerAuctioneerName = "";
	public String winnerAuctioneerAddress = "";
	public String winnerAuctioneerMobile = "";
	public long actualWinnerAuctioneerId = -1;
	
	public List<PiPackageAuctioneerChildrenDTO> piPackageAuctioneerChildrenDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Pi_package_auctioneerDTO[" +
            " iD = " + iD +
            " fiscalYearId = " + fiscalYearId +
            " piAuctionId = " + piAuctionId +
            " momentCodeTime = " + momentCodeTime +
            " tenderAdvertiseCat = " + tenderAdvertiseCat +
            " tenderAdvertiseDate = " + tenderAdvertiseDate +
            " tenderAdvertiseTime = " + tenderAdvertiseTime +
            " tenderOpeningDate = " + tenderOpeningDate +
            " tenderOpeningTime = " + tenderOpeningTime +
            " noaDate = " + noaDate +
            " noaTime = " + noaTime +
            " agreementDate = " + agreementDate +
            " agreementTime = " + agreementTime +
            " agreementEndingDate = " + agreementEndingDate +
            " agreementEndingTime = " + agreementEndingTime +
            " filesDropzone = " + filesDropzone +
            " auctioneerStatus = " + auctioneerStatus +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
			" officeUnitId = " + officeUnitId +
			" winnerAuctioneerId = " + winnerAuctioneerId +
			" winnerAuctioneerName = " + winnerAuctioneerName +
			" winnerAuctioneerAddress = " + winnerAuctioneerAddress +
			" winnerAuctioneerMobile = " + winnerAuctioneerMobile +
			" actualWinnerAuctioneerId = " + actualWinnerAuctioneerId +
            "]";
    }

}