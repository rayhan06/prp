package pi_package_auctioneer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import user.UserDTO;
import util.*;
import pb.*;

public class PiPackageAuctioneerChildrenDAO  implements CommonDAOService<PiPackageAuctioneerChildrenDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private static final String deletePiPackageAuctioneerChildren =
			"UPDATE pi_package_auctioneer_children SET isDeleted = %d,modified_by = %d,lastModificationTime = %d WHERE pi_package_auctioneer_id = %d ";

	private PiPackageAuctioneerChildrenDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"pi_package_auctioneer_id",
			"name",
			"address",
			"mobile",
			"is_chosen",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"search_column",
			"auctioneer_status",
			"actual_auctioneer_id",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("name"," and (name like ?)");
		searchMap.put("mobile"," and (mobile like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final PiPackageAuctioneerChildrenDAO INSTANCE = new PiPackageAuctioneerChildrenDAO();
	}

	public static PiPackageAuctioneerChildrenDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(PiPackageAuctioneerChildrenDTO pipackageauctioneerchildrenDTO)
	{
		pipackageauctioneerchildrenDTO.searchColumn = "";
		pipackageauctioneerchildrenDTO.searchColumn += pipackageauctioneerchildrenDTO.name + " ";
		pipackageauctioneerchildrenDTO.searchColumn += pipackageauctioneerchildrenDTO.mobile + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, PiPackageAuctioneerChildrenDTO pipackageauctioneerchildrenDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(pipackageauctioneerchildrenDTO);
		if(isInsert)
		{
			ps.setObject(++index,pipackageauctioneerchildrenDTO.iD);
		}
		ps.setObject(++index,pipackageauctioneerchildrenDTO.piPackageAuctioneerId);
		ps.setObject(++index,pipackageauctioneerchildrenDTO.name);
		ps.setObject(++index,pipackageauctioneerchildrenDTO.address);
		ps.setObject(++index,pipackageauctioneerchildrenDTO.mobile);
		ps.setObject(++index,pipackageauctioneerchildrenDTO.isChosen);
		ps.setObject(++index,pipackageauctioneerchildrenDTO.insertionDate);
		ps.setObject(++index,pipackageauctioneerchildrenDTO.insertedBy);
		ps.setObject(++index,pipackageauctioneerchildrenDTO.modifiedBy);
		ps.setObject(++index,pipackageauctioneerchildrenDTO.searchColumn);
		ps.setObject(++index,pipackageauctioneerchildrenDTO.auctioneerStatus);
		ps.setObject(++index,pipackageauctioneerchildrenDTO.actualAuctioneerId);
		if(isInsert)
		{
			ps.setObject(++index,pipackageauctioneerchildrenDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,pipackageauctioneerchildrenDTO.iD);
		}
	}
	
	@Override
	public PiPackageAuctioneerChildrenDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			PiPackageAuctioneerChildrenDTO pipackageauctioneerchildrenDTO = new PiPackageAuctioneerChildrenDTO();
			int i = 0;
			pipackageauctioneerchildrenDTO.iD = rs.getLong(columnNames[i++]);
			pipackageauctioneerchildrenDTO.piPackageAuctioneerId = rs.getLong(columnNames[i++]);
			pipackageauctioneerchildrenDTO.name = rs.getString(columnNames[i++]);
			pipackageauctioneerchildrenDTO.address = rs.getString(columnNames[i++]);
			pipackageauctioneerchildrenDTO.mobile = rs.getString(columnNames[i++]);
			pipackageauctioneerchildrenDTO.isChosen = rs.getBoolean(columnNames[i++]);
			pipackageauctioneerchildrenDTO.insertionDate = rs.getLong(columnNames[i++]);
			pipackageauctioneerchildrenDTO.insertedBy = rs.getString(columnNames[i++]);
			pipackageauctioneerchildrenDTO.modifiedBy = rs.getString(columnNames[i++]);
			pipackageauctioneerchildrenDTO.searchColumn = rs.getString(columnNames[i++]);
			pipackageauctioneerchildrenDTO.auctioneerStatus = rs.getInt(columnNames[i++]);
			pipackageauctioneerchildrenDTO.actualAuctioneerId = rs.getLong(columnNames[i++]);
			pipackageauctioneerchildrenDTO.isDeleted = rs.getInt(columnNames[i++]);
			pipackageauctioneerchildrenDTO.lastModificationTime = rs.getLong(columnNames[i++]);

			return pipackageauctioneerchildrenDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public PiPackageAuctioneerChildrenDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "pi_package_auctioneer_children";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((PiPackageAuctioneerChildrenDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((PiPackageAuctioneerChildrenDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }

	public void deletePiPackageAuctioneerChildren(long pi_package_auctioneer_id, UserDTO userDTO) {

		String sql = String.format(
				deletePiPackageAuctioneerChildren, 1,
				userDTO.employee_record_id, System.currentTimeMillis(), pi_package_auctioneer_id
		);


		boolean res = (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
			try {
				st.executeUpdate(sql);
				return true;
			} catch (SQLException ex) {
				logger.error(ex);
				return false;
			}
		});
	}
				
}
	