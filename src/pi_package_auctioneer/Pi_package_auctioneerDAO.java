package pi_package_auctioneer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import pi_auction.Pi_auctionRepository;
import util.*;
import pb.*;

public class Pi_package_auctioneerDAO  implements CommonDAOService<Pi_package_auctioneerDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Pi_package_auctioneerDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"fiscal_year_id",
			"pi_auction_id",
			"moment_code_time",
			"tender_advertise_cat",
			"tender_advertise_date",
			"tender_advertise_time",
			"tender_opening_date",
			"tender_opening_time",
			"noa_date",
			"noa_time",
			"agreement_date",
			"agreement_time",
			"agreement_ending_date",
			"agreement_ending_time",
			"files_dropzone",
			"auctioneer_status",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"search_column",
			"office_unit_id",
			"winner_auctioneer_id",
			"winner_auctioneer_name",
			"winner_auctioneer_address",
			"winner_auctioneer_mobile",
			"actual_winner_auctioneer_id",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("tender_advertise_cat"," and (tender_advertise_cat = ?)");
		searchMap.put("tender_advertise_date_start"," and (tender_advertise_date >= ?)");
		searchMap.put("tender_advertise_date_end"," and (tender_advertise_date <= ?)");
		searchMap.put("tender_opening_date_start"," and (tender_opening_date >= ?)");
		searchMap.put("tender_opening_date_end"," and (tender_opening_date <= ?)");
		searchMap.put("noa_date_start"," and (noa_date >= ?)");
		searchMap.put("noa_date_end"," and (noa_date <= ?)");
		searchMap.put("agreement_date_start"," and (agreement_date >= ?)");
		searchMap.put("agreement_date_end"," and (agreement_date <= ?)");
		searchMap.put("agreement_ending_date_start"," and (agreement_ending_date >= ?)");
		searchMap.put("agreement_ending_date_end"," and (agreement_ending_date <= ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");

		searchMap.put("officeUnitId", " AND (office_unit_id = ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Pi_package_auctioneerDAO INSTANCE = new Pi_package_auctioneerDAO();
	}

	public static Pi_package_auctioneerDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Pi_package_auctioneerDTO pi_package_auctioneerDTO)
	{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		pi_package_auctioneerDTO.searchColumn = "";
		pi_package_auctioneerDTO.searchColumn += Pi_auctionRepository.getInstance().getPi_auctionDTOByiD(pi_package_auctioneerDTO.piAuctionId).auctionPackageName+ " ";
		pi_package_auctioneerDTO.searchColumn += pi_package_auctioneerDTO.winnerAuctioneerName+ " ";
		pi_package_auctioneerDTO.searchColumn += pi_package_auctioneerDTO.winnerAuctioneerAddress+ " ";
		pi_package_auctioneerDTO.searchColumn += pi_package_auctioneerDTO.winnerAuctioneerMobile+ " ";
		pi_package_auctioneerDTO.searchColumn += simpleDateFormat.format(new Date(pi_package_auctioneerDTO.agreementDate))+ " ";
		pi_package_auctioneerDTO.searchColumn += simpleDateFormat.format(new Date(pi_package_auctioneerDTO.agreementEndingDate))+ " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Pi_package_auctioneerDTO pi_package_auctioneerDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(pi_package_auctioneerDTO);
		if(isInsert)
		{
			ps.setObject(++index,pi_package_auctioneerDTO.iD);
		}
		ps.setObject(++index,pi_package_auctioneerDTO.fiscalYearId);
		ps.setObject(++index,pi_package_auctioneerDTO.piAuctionId);
		ps.setObject(++index,pi_package_auctioneerDTO.momentCodeTime);
		ps.setObject(++index,pi_package_auctioneerDTO.tenderAdvertiseCat);
		ps.setObject(++index,pi_package_auctioneerDTO.tenderAdvertiseDate);
		ps.setObject(++index,pi_package_auctioneerDTO.tenderAdvertiseTime);
		ps.setObject(++index,pi_package_auctioneerDTO.tenderOpeningDate);
		ps.setObject(++index,pi_package_auctioneerDTO.tenderOpeningTime);
		ps.setObject(++index,pi_package_auctioneerDTO.noaDate);
		ps.setObject(++index,pi_package_auctioneerDTO.noaTime);
		ps.setObject(++index,pi_package_auctioneerDTO.agreementDate);
		ps.setObject(++index,pi_package_auctioneerDTO.agreementTime);
		ps.setObject(++index,pi_package_auctioneerDTO.agreementEndingDate);
		ps.setObject(++index,pi_package_auctioneerDTO.agreementEndingTime);
		ps.setObject(++index,pi_package_auctioneerDTO.filesDropzone);
		ps.setObject(++index,pi_package_auctioneerDTO.auctioneerStatus);
		ps.setObject(++index,pi_package_auctioneerDTO.insertionDate);
		ps.setObject(++index,pi_package_auctioneerDTO.insertedBy);
		ps.setObject(++index,pi_package_auctioneerDTO.modifiedBy);
		ps.setObject(++index,pi_package_auctioneerDTO.searchColumn);
		ps.setObject(++index,pi_package_auctioneerDTO.officeUnitId);

		ps.setObject(++index,pi_package_auctioneerDTO.winnerAuctioneerId);
		ps.setObject(++index,pi_package_auctioneerDTO.winnerAuctioneerName);
		ps.setObject(++index,pi_package_auctioneerDTO.winnerAuctioneerAddress);
		ps.setObject(++index,pi_package_auctioneerDTO.winnerAuctioneerMobile);
		ps.setObject(++index,pi_package_auctioneerDTO.actualWinnerAuctioneerId);
		if(isInsert)
		{
			ps.setObject(++index,pi_package_auctioneerDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,pi_package_auctioneerDTO.iD);
		}
	}
	
	@Override
	public Pi_package_auctioneerDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Pi_package_auctioneerDTO pi_package_auctioneerDTO = new Pi_package_auctioneerDTO();
			int i = 0;
			pi_package_auctioneerDTO.iD = rs.getLong(columnNames[i++]);
			pi_package_auctioneerDTO.fiscalYearId = rs.getLong(columnNames[i++]);
			pi_package_auctioneerDTO.piAuctionId = rs.getLong(columnNames[i++]);
			pi_package_auctioneerDTO.momentCodeTime = rs.getString(columnNames[i++]);
			pi_package_auctioneerDTO.tenderAdvertiseCat = rs.getString(columnNames[i++]);
			pi_package_auctioneerDTO.tenderAdvertiseDate = rs.getLong(columnNames[i++]);
			pi_package_auctioneerDTO.tenderAdvertiseTime = rs.getString(columnNames[i++]);
			pi_package_auctioneerDTO.tenderOpeningDate = rs.getLong(columnNames[i++]);
			pi_package_auctioneerDTO.tenderOpeningTime = rs.getString(columnNames[i++]);
			pi_package_auctioneerDTO.noaDate = rs.getLong(columnNames[i++]);
			pi_package_auctioneerDTO.noaTime = rs.getString(columnNames[i++]);
			pi_package_auctioneerDTO.agreementDate = rs.getLong(columnNames[i++]);
			pi_package_auctioneerDTO.agreementTime = rs.getString(columnNames[i++]);
			pi_package_auctioneerDTO.agreementEndingDate = rs.getLong(columnNames[i++]);
			pi_package_auctioneerDTO.agreementEndingTime = rs.getString(columnNames[i++]);
			pi_package_auctioneerDTO.filesDropzone = rs.getLong(columnNames[i++]);
			pi_package_auctioneerDTO.auctioneerStatus = rs.getInt(columnNames[i++]);
			pi_package_auctioneerDTO.insertionDate = rs.getLong(columnNames[i++]);
			pi_package_auctioneerDTO.insertedBy = rs.getString(columnNames[i++]);
			pi_package_auctioneerDTO.modifiedBy = rs.getString(columnNames[i++]);
			pi_package_auctioneerDTO.searchColumn = rs.getString(columnNames[i++]);
			pi_package_auctioneerDTO.officeUnitId = rs.getLong(columnNames[i++]);
			pi_package_auctioneerDTO.winnerAuctioneerId = rs.getLong(columnNames[i++]);
			pi_package_auctioneerDTO.winnerAuctioneerName = rs.getString(columnNames[i++]);
			pi_package_auctioneerDTO.winnerAuctioneerAddress = rs.getString(columnNames[i++]);
			pi_package_auctioneerDTO.winnerAuctioneerMobile = rs.getString(columnNames[i++]);
			pi_package_auctioneerDTO.actualWinnerAuctioneerId = rs.getLong(columnNames[i++]);
			pi_package_auctioneerDTO.isDeleted = rs.getInt(columnNames[i++]);
			pi_package_auctioneerDTO.lastModificationTime = rs.getLong(columnNames[i++]);

			return pi_package_auctioneerDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Pi_package_auctioneerDTO getDTOByID (long id)
	{
		Pi_package_auctioneerDTO pi_package_auctioneerDTO = null;
		try 
		{
			pi_package_auctioneerDTO = getDTOFromID(id);
			if(pi_package_auctioneerDTO != null)
			{
				PiPackageAuctioneerChildrenDAO piPackageAuctioneerChildrenDAO = PiPackageAuctioneerChildrenDAO.getInstance();				
				List<PiPackageAuctioneerChildrenDTO> piPackageAuctioneerChildrenDTOList = (List<PiPackageAuctioneerChildrenDTO>)piPackageAuctioneerChildrenDAO.getDTOsByParent("pi_package_auctioneer_id", pi_package_auctioneerDTO.iD);
				pi_package_auctioneerDTO.piPackageAuctioneerChildrenDTOList = piPackageAuctioneerChildrenDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return pi_package_auctioneerDTO;
	}

	@Override
	public String getTableName() {
		return "pi_package_auctioneer";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Pi_package_auctioneerDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Pi_package_auctioneerDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }
				
}
	