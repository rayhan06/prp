package pi_package_auctioneer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Pi_package_auctioneerRepository implements Repository {
	Pi_package_auctioneerDAO pi_package_auctioneerDAO = null;
	
	static Logger logger = Logger.getLogger(Pi_package_auctioneerRepository.class);
	Map<Long, Pi_package_auctioneerDTO>mapOfPi_package_auctioneerDTOToiD;
	Gson gson;

  
	private Pi_package_auctioneerRepository(){
		pi_package_auctioneerDAO = Pi_package_auctioneerDAO.getInstance();
		mapOfPi_package_auctioneerDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Pi_package_auctioneerRepository INSTANCE = new Pi_package_auctioneerRepository();
    }

    public static Pi_package_auctioneerRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Pi_package_auctioneerDTO> pi_package_auctioneerDTOs = pi_package_auctioneerDAO.getAllDTOs(reloadAll);
			for(Pi_package_auctioneerDTO pi_package_auctioneerDTO : pi_package_auctioneerDTOs) {
				Pi_package_auctioneerDTO oldPi_package_auctioneerDTO = getPi_package_auctioneerDTOByiD(pi_package_auctioneerDTO.iD);
				if( oldPi_package_auctioneerDTO != null ) {
					mapOfPi_package_auctioneerDTOToiD.remove(oldPi_package_auctioneerDTO.iD);
				
					
				}
				if(pi_package_auctioneerDTO.isDeleted == 0) 
				{
					
					mapOfPi_package_auctioneerDTOToiD.put(pi_package_auctioneerDTO.iD, pi_package_auctioneerDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Pi_package_auctioneerDTO clone(Pi_package_auctioneerDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Pi_package_auctioneerDTO.class);
	}
	
	
	public List<Pi_package_auctioneerDTO> getPi_package_auctioneerList() {
		List <Pi_package_auctioneerDTO> pi_package_auctioneers = new ArrayList<Pi_package_auctioneerDTO>(this.mapOfPi_package_auctioneerDTOToiD.values());
		return pi_package_auctioneers;
	}
	
	public List<Pi_package_auctioneerDTO> copyPi_package_auctioneerList() {
		List <Pi_package_auctioneerDTO> pi_package_auctioneers = getPi_package_auctioneerList();
		return pi_package_auctioneers
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Pi_package_auctioneerDTO getPi_package_auctioneerDTOByiD( long iD){
		return mapOfPi_package_auctioneerDTOToiD.get(iD);
	}
	
	public Pi_package_auctioneerDTO copyPi_package_auctioneerDTOByiD( long iD){
		return clone(mapOfPi_package_auctioneerDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return pi_package_auctioneerDAO.getTableName();
	}
}


