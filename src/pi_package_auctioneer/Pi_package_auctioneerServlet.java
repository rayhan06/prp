package pi_package_auctioneer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

import category.CategoryDTO;
import common.CommonDAOService;
import fiscal_year.Fiscal_yearDAO;
import fiscal_year.Fiscal_yearDTO;
import org.apache.log4j.Logger;
import login.LoginDTO;
import permission.MenuConstants;
import pi_auction.Pi_auctionDAO;
import pi_auction.Pi_auctionDTO;
import pi_package_auctioneer_items.Pi_package_auctioneer_itemsDAO;
import pi_package_auctioneer_items.Pi_package_auctioneer_itemsDTO;
import pi_unique_item.Pi_unique_item_transactionDAO;
import pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsDTO;
import pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsRepository;
import role.PermissionRepository;
import role.RoleDTO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;

import javax.servlet.http.*;
import java.util.*;

import language.LC;
import language.LM;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import files.*;
import vm_requisition.CommonApprovalStatus;

@WebServlet("/Pi_package_auctioneerServlet")
@MultipartConfig
public class Pi_package_auctioneerServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_package_auctioneerServlet.class);

    @Override
    public String getTableName() {
        return Pi_package_auctioneerDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_package_auctioneerServlet";
    }

    @Override
    public Pi_package_auctioneerDAO getCommonDAOService() {
        return Pi_package_auctioneerDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PI_PACKAGE_AUCTIONEER_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PI_PACKAGE_AUCTIONEER_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PI_PACKAGE_AUCTIONEER_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_package_auctioneerServlet.class;
    }

    FilesDAO filesDAO = new FilesDAO();
    PiPackageAuctioneerChildrenDAO piPackageAuctioneerChildrenDAO = PiPackageAuctioneerChildrenDAO.getInstance();
    private final Gson gson = new Gson();


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        Pi_package_auctioneerDTO pi_package_auctioneerDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();

        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLanEng = Language.equalsIgnoreCase("English");

        if (addFlag == true) {
            pi_package_auctioneerDTO = new Pi_package_auctioneerDTO();
            pi_package_auctioneerDTO.insertionDate = pi_package_auctioneerDTO.lastModificationTime = System.currentTimeMillis();
            pi_package_auctioneerDTO.insertedBy = pi_package_auctioneerDTO.modifiedBy = String.valueOf(userDTO.ID);
        } else {
            pi_package_auctioneerDTO = Pi_package_auctioneerDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));

            if (pi_package_auctioneerDTO == null) {
                throw new Exception(isLanEng ? "Auctioneer information is not found" : "নিলামে অংশগ্রহণকারীর তথ্য পাওয়া যায়নি");
            }
            pi_package_auctioneerDTO.lastModificationTime = System.currentTimeMillis();
            pi_package_auctioneerDTO.modifiedBy = String.valueOf(userDTO.ID);
        }

        String Value = "";

        Fiscal_yearDTO cur_fis_dto = fiscal_yearDAO.getFiscalYearBYDateLong(System.currentTimeMillis());
        pi_package_auctioneerDTO.fiscalYearId = cur_fis_dto.id;

        Value = request.getParameter("piAuctionId");
        if (Value == null || Value.equals("-1") || Value.equals("")) {
            throw new Exception(isLanEng ? "Auction request information is not found" : "নিলামের অনুরোধের তথ্য পাওয়া যায়নি");
        }
        Pi_auctionDTO pi_auctionDTO = Pi_auctionDAO.getInstance().getDTOByID(Long.parseLong(Value));
        if (pi_auctionDTO == null) {
            throw new Exception(isLanEng ? "Auction request information is not found" : "নিলামের অনুরোধের তথ্য পাওয়া যায়নি");
        }
        if (addFlag == true && pi_auctionDTO != null && pi_auctionDTO.auctionStatus == CommonApprovalStatus.AUCTIONED.getValue()) {
            throw new Exception(isLanEng ? "You already auctioned this product" : "এই প্যাকেজের নিলাম ইতিমধ্যে হয়ে গিয়েছে");
        }

        pi_package_auctioneerDTO.piAuctionId = Long.parseLong(Value);

        Value = request.getParameter("officeUnitId");
        if (Value == null || Value.equals("-1") || Value.equals("")) {
            throw new Exception(isLanEng ? "Office information is not found" : "অফিসের তথ্য খুঁজে পাওয়া যায়নি");
        }
        pi_package_auctioneerDTO.officeUnitId = Long.parseLong(Value);

        int submittedPriceLen = request.getParameterValues("submittedAuctioneerPrice").length;
        for (int i = 0; i < submittedPriceLen; i++) {
            String val = "";
            val = request.getParameterValues("submittedAuctioneerPrice")[i];
            if (val == null || val.equals("") || val.equals("-1") || val.contains("-") || val.isEmpty()) {
                throw new Exception(isLanEng ? "Please provide valid price" : "অনুগ্রহপূর্বক বৈধ দর প্রদান করুন");
            }
        }

        Value = request.getParameter("tenderAdvertiseDate");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide tender advertise date" : "অনুগ্রহপূর্বক টেন্ডার বিজ্ঞাপনের তারিখ প্রদান করুন");
        }
        Date d = f.parse(Value);
        pi_package_auctioneerDTO.tenderAdvertiseDate = d.getTime();

        Value = request.getParameter("tenderAdvertiseTime");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide tender advertise time" : "অনুগ্রহপূর্বক টেন্ডার বিজ্ঞাপনের সময় প্রদান করুন");
        }
        pi_package_auctioneerDTO.tenderAdvertiseTime = (Value);


        Value = request.getParameter("tenderOpeningDate");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide tender opening date" : "অনুগ্রহপূর্বক টেন্ডার ওপেনিং ডেট প্রদান করুন");
        }
        d = f.parse(Value);
        pi_package_auctioneerDTO.tenderOpeningDate = d.getTime();

        Value = request.getParameter("tenderOpeningTime");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide tender opening time" : "অনুগ্রহপূর্বক টেন্ডার ওপেনিং সময় প্রদান করুন");
        }
        pi_package_auctioneerDTO.tenderOpeningTime = (Value);

        Value = request.getParameter("noaDate");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide notification of award date" : "অনুগ্রহপূর্বক নোটিফিকেশন অফ এওয়ার্ড ডেট প্রদান করুন");
        }
        d = f.parse(Value);
        pi_package_auctioneerDTO.noaDate = d.getTime();

        Value = request.getParameter("noaTime");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide notification of award time" : "অনুগ্রহপূর্বক নোটিফিকেশন অফ এওয়ার্ড সময় প্রদান করুন");
        }
        pi_package_auctioneerDTO.noaTime = (Value);

        Value = request.getParameter("agreementDate");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide agreement date" : "অনুগ্রহপূর্বক চুক্তি স্বাক্ষরের তারিখ প্রদান করুন");
        }
        d = f.parse(Value);
        pi_package_auctioneerDTO.agreementDate = d.getTime();

        Value = request.getParameter("agreementTime");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide agreement time" : "অনুগ্রহপূর্বক চুক্তি স্বাক্ষরের সময় প্রদান করুন");
        }
        pi_package_auctioneerDTO.agreementTime = (Value);


        Value = request.getParameter("agreementEndingDate");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide agreement ending date" : "অনুগ্রহপূর্বক চুক্তি শেষের তারিখ প্রদান করুন");
        }
        d = f.parse(Value);
        pi_package_auctioneerDTO.agreementEndingDate = d.getTime();

        Value = request.getParameter("agreementEndingTime");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide agreement ending time" : "অনুগ্রহপূর্বক চুক্তি শেষের সময় প্রদান করুন");
        }
        pi_package_auctioneerDTO.agreementEndingTime = (Value);

        Value = request.getParameter("tenderAdvertiseCat");
        if (Value == null || Value.equals("-1") || Value.equals("")) {
            throw new Exception(isLanEng ? "Please provide tender advertise medium" : "অনুগ্রহপূর্বক বিজ্ঞাপনের মাধ্যম প্রদান করুন");
        }
        pi_package_auctioneerDTO.tenderAdvertiseCat = Value;


        Value = request.getParameter("filesDropzone");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        logger.debug("filesDropzone = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {

            logger.debug("filesDropzone = " + Value);

            pi_package_auctioneerDTO.filesDropzone = Long.parseLong(Value);


            if (addFlag == false) {
                String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    logger.debug("going to delete " + deleteArray[i]);
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        } else {
            logger.debug("FieldName has a null Value, not updating" + " = " + Value);
        }


        logger.debug("Done adding  addPi_package_auctioneer dto = " + pi_package_auctioneerDTO);
        long returnedID = -1;

        List<PiPackageAuctioneerChildrenDTO> piPackageAuctioneerChildrenDTOList = createPiPackageAuctioneerChildrenDTOListByRequest(request, language);

        Utils.handleTransaction(()->{
            if (addFlag) {
                Pi_package_auctioneerDAO.getInstance().add(pi_package_auctioneerDTO);

                pi_auctionDTO.auctionStatus = CommonApprovalStatus.AUCTIONED.getValue();
                pi_auctionDTO.lastModificationTime = pi_package_auctioneerDTO.lastModificationTime;
                Pi_auctionDAO.getInstance().update(pi_auctionDTO);
            } else {
                Pi_package_auctioneerDAO.getInstance().update(pi_package_auctioneerDTO);

                PiPackageAuctioneerChildrenDAO.getInstance().deletePiPackageAuctioneerChildren(pi_package_auctioneerDTO.iD, userDTO);
                Pi_package_auctioneer_itemsDAO.getInstance().deletePiPackageAuctioneerChildrenItems(pi_package_auctioneerDTO, userDTO);
            }


            if (piPackageAuctioneerChildrenDTOList != null) {
                for (PiPackageAuctioneerChildrenDTO piPackageAuctioneerChildrenDTO : piPackageAuctioneerChildrenDTOList) {
                    piPackageAuctioneerChildrenDTO.piPackageAuctioneerId = pi_package_auctioneerDTO.iD;
                    long childrenId = piPackageAuctioneerChildrenDAO.add(piPackageAuctioneerChildrenDTO);

                    if (piPackageAuctioneerChildrenDTO.isChosen) {
                        pi_package_auctioneerDTO.winnerAuctioneerId = childrenId;
                        pi_package_auctioneerDTO.winnerAuctioneerName = piPackageAuctioneerChildrenDTO.name;
                        pi_package_auctioneerDTO.winnerAuctioneerAddress = piPackageAuctioneerChildrenDTO.address;
                        pi_package_auctioneerDTO.winnerAuctioneerMobile = piPackageAuctioneerChildrenDTO.mobile;
                        pi_package_auctioneerDTO.actualWinnerAuctioneerId = piPackageAuctioneerChildrenDTO.actualAuctioneerId;
                        Pi_package_auctioneerDAO.getInstance().update(pi_package_auctioneerDTO);

                    }
                }

                addProductAuctioneerAndPrice(request, piPackageAuctioneerChildrenDTOList, pi_package_auctioneerDTO.iD,
                        pi_package_auctioneerDTO.piAuctionId);
            }

            if(addFlag){
                long vendorId = -1;
                if(!(piPackageAuctioneerChildrenDTOList == null || piPackageAuctioneerChildrenDTOList.isEmpty())){
                    vendorId = piPackageAuctioneerChildrenDTOList.get(0).actualAuctioneerId;
                }
                Pi_unique_item_transactionDAO.getInstance()
                        .recordMultipleItemTransactionAfterAuction(pi_package_auctioneerDTO.piAuctionId,
                                vendorId, userDTO);
            }
        });

        return pi_package_auctioneerDTO;


    }

    private List<PiPackageAuctioneerChildrenDTO> createPiPackageAuctioneerChildrenDTOListByRequest(HttpServletRequest request, String language) throws Exception {
        List<PiPackageAuctioneerChildrenDTO> piPackageAuctioneerChildrenDTOList = new ArrayList<PiPackageAuctioneerChildrenDTO>();
        if (request.getParameterValues("piPackageAuctioneerChildren.iD") != null) {
            int piPackageAuctioneerChildrenItemNo = request.getParameterValues("piPackageAuctioneerChildren.iD").length;
            for (int index = 0; index < piPackageAuctioneerChildrenItemNo; index++) {
                PiPackageAuctioneerChildrenDTO piPackageAuctioneerChildrenDTO = createPiPackageAuctioneerChildrenDTOByRequestAndIndex(request, true, index, language);
                piPackageAuctioneerChildrenDTOList.add(piPackageAuctioneerChildrenDTO);
            }

            return piPackageAuctioneerChildrenDTOList;
        }
        return null;
    }

    private PiPackageAuctioneerChildrenDTO createPiPackageAuctioneerChildrenDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, String language) throws Exception {

        PiPackageAuctioneerChildrenDTO piPackageAuctioneerChildrenDTO;
        if (addFlag) {
            piPackageAuctioneerChildrenDTO = new PiPackageAuctioneerChildrenDTO();
        } else {
            piPackageAuctioneerChildrenDTO = piPackageAuctioneerChildrenDAO.getDTOByID(Long.parseLong(request.getParameterValues("piPackageAuctioneerChildren.iD")[index]));
        }
        String path = getServletContext().getRealPath("/img2/");
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");


        String Value = "";
        Value = request.getParameterValues("piPackageAuctioneerChildren.piPackageAuctioneerId")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_PI_PACKAGE_AUCTIONEER_CHILDREN_PIPACKAGEAUCTIONEERID, language) + " " + ErrorMessage.getInvalidMessage(language));
        }

        piPackageAuctioneerChildrenDTO.piPackageAuctioneerId = Long.parseLong(Value);
        Value = request.getParameterValues("piPackageAuctioneerChildren.name")[index];


        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_PI_PACKAGE_AUCTIONEER_CHILDREN_NAME, language) + " " + ErrorMessage.getInvalidMessage(language));
        }

        Pi_vendor_auctioneer_detailsDTO pi_vendor_auctioneer_detailsDTO = Pi_vendor_auctioneer_detailsRepository
                .getInstance()
                .getPi_vendor_auctioneer_detailsDTOByiD(Long.parseLong(Value));

        if (pi_vendor_auctioneer_detailsDTO == null) {
            throw new Exception(LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN_NAME, language) + " " + ErrorMessage.getInvalidMessage(language));
        }

        piPackageAuctioneerChildrenDTO.actualAuctioneerId = Long.parseLong(Value);
        piPackageAuctioneerChildrenDTO.name = pi_vendor_auctioneer_detailsDTO.nameBn;
        piPackageAuctioneerChildrenDTO.address = pi_vendor_auctioneer_detailsDTO.address;
        piPackageAuctioneerChildrenDTO.mobile = pi_vendor_auctioneer_detailsDTO.mobile;

        piPackageAuctioneerChildrenDTO.isChosen = false;
        piPackageAuctioneerChildrenDTO.auctioneerStatus = CommonApprovalStatus.INACTIVE.getValue();
        String checkBoxValue = request.getParameterValues("piPackageAuctioneerChildren.isChosenVal")[index];
        if (checkBoxValue.equals("1")) {
            piPackageAuctioneerChildrenDTO.isChosen = true;
            piPackageAuctioneerChildrenDTO.auctioneerStatus = CommonApprovalStatus.ACTIVE.getValue();
        }


        if (addFlag) {
            piPackageAuctioneerChildrenDTO.insertedBy = piPackageAuctioneerChildrenDTO.modifiedBy = String.valueOf(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.ID);
            piPackageAuctioneerChildrenDTO.insertionDate = piPackageAuctioneerChildrenDTO.lastModificationTime = System.currentTimeMillis();
        } else {
            piPackageAuctioneerChildrenDTO.modifiedBy = String.valueOf(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.ID);
            piPackageAuctioneerChildrenDTO.lastModificationTime = System.currentTimeMillis();
        }
        return piPackageAuctioneerChildrenDTO;

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getAllAdvertiseMedium":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_PACKAGE_AUCTIONEER_ADD)) {

                        List<OptionDTO> OptionDTOList = new ArrayList<>();
                        OptionDTO optionDTO;
                        List<CategoryDTO> otherOfficeDTOList = CatRepository.getInstance().getCategoryDTOList("tender_advertise");

                        for (CategoryDTO dto : otherOfficeDTOList) {

                            optionDTO = new OptionDTO(dto.nameEn, dto.nameBn, dto.value + "");
                            OptionDTOList.add(optionDTO);
                        }

                        response.getWriter().write(gson.toJson(OptionDTOList));
                        response.getWriter().flush();
                        response.getWriter().close();

                        return;
                    }
                case "search":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_AUCTION_SEARCH)) {

                        RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
                        boolean isAdmin = role.ID == SessionConstants.INVENTORY_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;
                        Map<String, String> extraCriteriaMap = new HashMap<>();
                        if (isAdmin) {
                            request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                            search(request, response);
                            return;
                        } else {
                            extraCriteriaMap.put("officeUnitId", String.valueOf(userDTO.unitID));
                            request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                            search(request, response);
                            return;
                        }
                    }

                    break;


                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void addProductAuctioneerAndPrice(HttpServletRequest request, List<PiPackageAuctioneerChildrenDTO> piPackageAuctioneerChildrenDTOS, Long piPackageAuctioneerId,
                                              Long piAuctionId) throws Exception {

        String Value = "";

        int submittedPriceLen = request.getParameterValues("submittedAuctioneerPrice").length;
        int submittedPriceIndex = 0;

        for (PiPackageAuctioneerChildrenDTO piPackageAuctioneerChildrenDTO : piPackageAuctioneerChildrenDTOS) {

            Pi_package_auctioneer_itemsDTO pi_package_auctioneer_itemsDTO = new Pi_package_auctioneer_itemsDTO();

            pi_package_auctioneer_itemsDTO.piPackageAuctioneerId = piPackageAuctioneerId;
            pi_package_auctioneer_itemsDTO.piPackageAuctioneerChildrenId = piPackageAuctioneerChildrenDTO.iD;
            pi_package_auctioneer_itemsDTO.piAuctionId = piAuctionId;

            pi_package_auctioneer_itemsDTO.insertedBy = pi_package_auctioneer_itemsDTO.modifiedBy = piPackageAuctioneerChildrenDTO.insertedBy;
            pi_package_auctioneer_itemsDTO.insertionDate = pi_package_auctioneer_itemsDTO.lastModificationTime = piPackageAuctioneerChildrenDTO.insertionDate;

            pi_package_auctioneer_itemsDTO.actualAuctioneerId = piPackageAuctioneerChildrenDTO.actualAuctioneerId;

            Value = request.getParameterValues("submittedAuctioneerPrice")[submittedPriceIndex++];
            if (Value == null || Value.equals("") || Value.equals("-1") || Value.contains("-")) {
                //throw new Exception("Please provide price");
            } else {
                pi_package_auctioneer_itemsDTO.price = Double.parseDouble((Jsoup.clean(
                        Value, Whitelist.simpleText()
                )));
            }

            Pi_package_auctioneer_itemsDAO.getInstance().add(pi_package_auctioneer_itemsDTO);
        }
    }
}

