package employee_seniority;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import employee_office_report.InChargeLevelEnum;
import org.apache.log4j.Logger;
import pb.Utils;
import util.CommonDTO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class Employee_seniorityDAO implements CommonDAOService<Employee_seniorityDTO> {
    private static final Logger logger = Logger.getLogger(Employee_seniorityDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (employee_record_id,organogram_id,office_unit_id,employee_class_cat,job_grade_type_cat,"
                    .concat("salary_grade_type,salary,joining_date,serial,modified_by,lastModificationTime,")
                    .concat("inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET employee_record_id=?,organogram_id=?,office_unit_id=?,employee_class_cat=?,job_grade_type_cat=?,"
                    .concat("salary_grade_type=?,salary=?,joining_date=?,serial=?,modified_by=?,lastModificationTime=? ")
                    .concat("WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private Employee_seniorityDAO() {
    }

    private static class LazyLoader {
        static final Employee_seniorityDAO INSTANCE = new Employee_seniorityDAO();
    }

    public static Employee_seniorityDAO getInstance() {
        return Employee_seniorityDAO.LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Employee_seniorityDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.employeeRecordId);
        ps.setLong(++index, dto.organogramId);
        ps.setLong(++index, dto.officeUnitId);
        ps.setInt(++index, dto.employeeClassCat);
        ps.setInt(++index, dto.jobGradeTypeCat);
        ps.setLong(++index, dto.salaryGradeType);
        ps.setInt(++index, dto.salary);
        ps.setLong(++index, dto.joiningDate);
        ps.setInt(++index, dto.serial);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Employee_seniorityDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Employee_seniorityDTO dto = new Employee_seniorityDTO();
            dto.iD = rs.getLong("ID");
            dto.employeeRecordId = rs.getLong("employee_record_id");
            dto.organogramId = rs.getLong("organogram_id");
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.employeeClassCat = rs.getInt("employee_class_cat");
            dto.jobGradeTypeCat = rs.getInt("job_grade_type_cat");
            dto.salaryGradeType = rs.getLong("salary_grade_type");
            dto.salary = rs.getInt("salary");
            dto.joiningDate = rs.getLong("joining_date");
            dto.serial = rs.getInt("serial");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "employee_seniority";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_seniorityDTO) commonDTO, addSqlQuery, true, false);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_seniorityDTO) commonDTO, updateSqlQuery, false, false);
    }

    private static final String loadSeniorityInfoSql =
            "select er.id," +
            "       eo.office_unit_organogram_id," +
            "       eo.office_unit_id," +
            "       er.employee_class_cat," +
            "       eo.job_grade_type_cat," +
            "       eo.salary_grade_type," +
            "       gwps.salary," +
            "       er.joining_date " +
            "from employee_records er " +
            "  join employee_offices eo on er.id = eo.employee_record_id " +
            "  join grade_wise_pay_scale gwps on eo.grade_type_level = gwps.id " +
            "where er.isDeleted = 0 " +
            "  and er.is_mp = 2 " +
            "  and eo.incharge_label = '%s' " +
            "  and eo.status = 1 " +
            "  and eo.isDeleted = 0 " +
            "  and eo.is_default_role = 1 ";

    private List<Employee_seniorityDTO> loadSeniorityInfoFromDb(List<Long> employeeIds) {
        String sqlQuery = loadSeniorityInfoSql;
        if (employeeIds != null) {
            if (employeeIds.isEmpty()) {
                return Collections.emptyList();
            }
            String employeeIdsStr =
                    employeeIds.stream()
                               .map(Objects::toString)
                               .collect(Collectors.joining(","));
            sqlQuery += String.format(" and er.id in (%s)", employeeIdsStr);
        }
        return ConnectionAndStatementUtil.getListOfT(
                String.format(sqlQuery, InChargeLevelEnum.ROUTINE_RESPONSIBILITY.getValue()),
                this::buildFromSeniorityInfoResultsSet
        );
    }

    private Employee_seniorityDTO buildFromSeniorityInfoResultsSet(ResultSet rs) {
        try {
            Employee_seniorityDTO dto = new Employee_seniorityDTO();
            dto.employeeRecordId = rs.getLong("er.id");
            dto.organogramId = rs.getLong("eo.office_unit_organogram_id");
            dto.officeUnitId = rs.getLong("eo.office_unit_id");
            dto.employeeClassCat = rs.getInt("er.employee_class_cat");
            dto.jobGradeTypeCat = rs.getInt("eo.job_grade_type_cat");
            dto.salaryGradeType = rs.getLong("eo.salary_grade_type");
            dto.salary = rs.getInt("gwps.salary");
            dto.joiningDate = rs.getLong("er.joining_date");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public void reloadAllSenioritySerial(List<Long> employeeIds, long modifiedBy) throws Exception {
        List<Employee_seniorityDTO> addedDTOs = getAllDTOs();

        List<Employee_seniorityDTO> updatedDTOs = loadSeniorityInfoFromDb(employeeIds);
        Map<Long, Employee_seniorityDTO> updatedDTOByEmployeeId =
                updatedDTOs.stream()
                           .collect(Collectors.toMap(
                                   dto -> dto.employeeRecordId,
                                   Function.identity(),
                                   (e1, e2) -> e1
                           ));
        for (Employee_seniorityDTO addedDTO : addedDTOs) {
            Employee_seniorityDTO updatedDTO = updatedDTOByEmployeeId.get(addedDTO.employeeRecordId);
            if (updatedDTO != null) {
                addedDTO.updateWithUpdatedDTO(updatedDTO);
            }
        }

        long currentTimeMillis = System.currentTimeMillis();
        Set<Long> employeeIdInAddedDTOs =
                addedDTOs.stream()
                         .map(dto -> dto.employeeRecordId)
                         .collect(Collectors.toSet());
        for (Employee_seniorityDTO updatedDTO : updatedDTOs) {
            if (!employeeIdInAddedDTOs.contains(updatedDTO.employeeRecordId)) {
                updatedDTO.iD = -1;
                updatedDTO.insertedBy = modifiedBy;
                updatedDTO.insertionTime = currentTimeMillis;
                addedDTOs.add(updatedDTO);
            }
        }

        Collections.sort(addedDTOs);

        Utils.handleTransaction(() -> {
            int serial = 1;
            for (Employee_seniorityDTO addedDTO : addedDTOs) {
                addedDTO.serial = serial++;
                addedDTO.modifiedBy = modifiedBy;
                addedDTO.lastModificationTime = currentTimeMillis;

                if (addedDTO.iD < 0) {
                    add(addedDTO);
                } else {
                    update(addedDTO);
                }
            }
        });
    }

    private static final String updateSerialWithIdSql = "update employee_seniority set serial=%d,lastModificationTime=%d where id = %d";

    public void saveEmployeeSeniorityInput(EmployeeSeniorityInput seniorityInput) throws Exception {
        if (CONNECTION_THREAD_LOCAL.get() != null) {
            saveEmployeeSeniorityInput(seniorityInput, CONNECTION_THREAD_LOCAL.get());
        } else {
            Utils.handleTransaction(() -> saveEmployeeSeniorityInput(seniorityInput, CONNECTION_THREAD_LOCAL.get()));
        }
    }

    private void saveEmployeeSeniorityInput(EmployeeSeniorityInput seniorityInput, Connection connection) throws Exception {
        String sql = String.format(
                updateSerialWithIdSql,
                seniorityInput.serial, seniorityInput.modificationTime, seniorityInput.id
        );
        Utils.wrapWithAtomicReference(ar ->
                ConnectionAndStatementUtil.getWriteStatement(st -> {
                    try {
                        logger.debug(sql);
                        st.executeUpdate(sql);
                        recordUpdateTime(connection, getTableName(), seniorityInput.modificationTime);
                    } catch (SQLException e) {
                        logger.error(e.getMessage());
                        e.printStackTrace();
                        ar.set(e);
                    }
                }, connection)
        );
    }
}
