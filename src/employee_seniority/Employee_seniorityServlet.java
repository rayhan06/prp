package employee_seniority;

import com.google.gson.Gson;
import common.CustomException;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pb.Utils;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/Employee_seniorityServlet")
@MultipartConfig
public class Employee_seniorityServlet extends HttpServlet {
    private final Logger logger = Logger.getLogger(Employee_seniorityServlet.class);
    public final Gson gson = new Gson();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        String actionType = request.getParameter("actionType");
        try {
            if ("ajax_reloadAllSenioritySerial".equals(actionType)) {
                Map<String, Object> res = reloadAllSenioritySerial(userDTO);
                response.setCharacterEncoding("UTF-8");
                response.getWriter().println(gson.toJson(res));
                return;
            } else if ("ajax_saveSenioritySerial".equals(actionType)) {
                Map<String, Object> res = new HashMap<>();
                try {
                    saveSenioritySerial(request, userDTO, isLangEn);
                    res.put("success", true);
                } catch (CustomException ce) {
                    logger.error(ce.getMessage());
                    res.put("success", false);
                    res.put("message", ce.getMessage());
                } catch (Exception ex) {
                    logger.error(ex.getMessage());
                    res.put("success", false);
                    res.put("message", isLangEn ? "Server error" : "সার্ভারে সমস্যা");
                }
                response.setCharacterEncoding("UTF-8");
                response.getWriter().println(gson.toJson(res));
                return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private Map<String, Object> reloadAllSenioritySerial(UserDTO userDTO) {
        boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Map<String, Object> res = new HashMap<>();
        try {
            Employee_seniorityDAO.getInstance().reloadAllSenioritySerial(null, userDTO.ID);
            res.put("success", true);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            res.put("success", false);
            res.put("message", isLangEn ? "Server error" : "সার্ভারে সমস্যা");
        }
        return res;
    }

    private void saveSenioritySerial(HttpServletRequest request, UserDTO userDTO, boolean isLangEn) throws Exception {
        EmployeeSeniorityInput[] senioritySerials;
        try {
            senioritySerials = gson.fromJson(request.getParameter("senioritySerials"), EmployeeSeniorityInput[].class);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new CustomException(isLangEn ? "Invalid Input" : "ভুল ইনপুট");
        }
        Employee_seniorityDAO employeeSeniorityDAO = Employee_seniorityDAO.getInstance();
        long currentTime = System.currentTimeMillis();
        Utils.handleTransaction(() -> {
            for (EmployeeSeniorityInput senioritySerial : senioritySerials) {
                senioritySerial.modifiedBy = userDTO.ID;
                senioritySerial.modificationTime = currentTime;
                employeeSeniorityDAO.saveEmployeeSeniorityInput(senioritySerial);
            }
        });
    }
}
