package employee_seniority;

public class EmployeeSeniorityInput {
    public long id;
    public int serial;
    public long modifiedBy;
    public long modificationTime;
}
