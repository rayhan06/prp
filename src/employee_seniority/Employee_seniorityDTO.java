package employee_seniority;

import util.CommonDTO;

import java.util.Comparator;

import static java.util.Comparator.comparingInt;

public class Employee_seniorityDTO extends CommonDTO implements Comparable<Employee_seniorityDTO> {
    private static final Comparator<Employee_seniorityDTO> seniorityComparator =
            comparingInt((Employee_seniorityDTO dto) -> dto.employeeClassCat)
                    .thenComparingInt(dto -> dto.jobGradeTypeCat)
                    .thenComparingLong(dto -> dto.salaryGradeType)
                    .thenComparing(comparingInt((Employee_seniorityDTO dto) -> dto.salary).reversed())
                    .thenComparingLong(dto -> dto.joiningDate)
                    .thenComparingInt(dto -> dto.serial);

    public long employeeRecordId;
    public long organogramId;
    public long officeUnitId;

    public int employeeClassCat;
    public int jobGradeTypeCat;
    public long salaryGradeType;
    public int salary;
    public long joiningDate;
    public int serial;

    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = -1;

    @Override
    public int compareTo(Employee_seniorityDTO other) {
        return seniorityComparator.compare(this, other);
    }

    public void updateWithUpdatedDTO(Employee_seniorityDTO updatedDTO) {
        employeeRecordId = updatedDTO.employeeRecordId;
        organogramId = updatedDTO.organogramId;
        officeUnitId = updatedDTO.officeUnitId;
        employeeClassCat = updatedDTO.employeeClassCat;
        jobGradeTypeCat = updatedDTO.jobGradeTypeCat;
        salaryGradeType = updatedDTO.salaryGradeType;
        salary = updatedDTO.salary;
        joiningDate = updatedDTO.joiningDate;
    }
}
