package approval_execution_table;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Approval_execution_tableRepository implements Repository {
	Approval_execution_tableDAO approval_execution_tableDAO = null;
	
	public void setDAO(Approval_execution_tableDAO approval_execution_tableDAO)
	{
		this.approval_execution_tableDAO = approval_execution_tableDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Approval_execution_tableRepository.class);
	Map<Long, Approval_execution_tableDTO>mapOfApproval_execution_tableDTOToiD;
	Map<String, Set<Approval_execution_tableDTO> >mapOfApproval_execution_tableDTOTotableName;
	Map<Long, Set<Approval_execution_tableDTO> >mapOfApproval_execution_tableDTOTopreviousRowId;
	Map<Long, Set<Approval_execution_tableDTO> >mapOfApproval_execution_tableDTOToupdatedRowId;
	Map<Integer, Set<Approval_execution_tableDTO> >mapOfApproval_execution_tableDTOTooperation;
	Map<Integer, Set<Approval_execution_tableDTO> >mapOfApproval_execution_tableDTOToaction;
	Map<Integer, Set<Approval_execution_tableDTO> >mapOfApproval_execution_tableDTOToapprovalStatusCat;
	Map<Long, Set<Approval_execution_tableDTO> >mapOfApproval_execution_tableDTOTouserId;
	Map<String, Set<Approval_execution_tableDTO> >mapOfApproval_execution_tableDTOTouserIp;
	Map<Long, Set<Approval_execution_tableDTO> >mapOfApproval_execution_tableDTOToapprovalPathId;
	Map<Integer, Set<Approval_execution_tableDTO> >mapOfApproval_execution_tableDTOToapprovalPathOrder;
	Map<String, Set<Approval_execution_tableDTO> >mapOfApproval_execution_tableDTOToremarks;
	Map<Long, Set<Approval_execution_tableDTO> >mapOfApproval_execution_tableDTOTofileDropzone;
	Map<Long, Set<Approval_execution_tableDTO> >mapOfApproval_execution_tableDTOTolastModificationTime;


	static Approval_execution_tableRepository instance = null;  
	private Approval_execution_tableRepository(){
		mapOfApproval_execution_tableDTOToiD = new ConcurrentHashMap<>();
		mapOfApproval_execution_tableDTOTotableName = new ConcurrentHashMap<>();
		mapOfApproval_execution_tableDTOTopreviousRowId = new ConcurrentHashMap<>();
		mapOfApproval_execution_tableDTOToupdatedRowId = new ConcurrentHashMap<>();
		mapOfApproval_execution_tableDTOTooperation = new ConcurrentHashMap<>();
		mapOfApproval_execution_tableDTOToaction = new ConcurrentHashMap<>();
		mapOfApproval_execution_tableDTOToapprovalStatusCat = new ConcurrentHashMap<>();
		mapOfApproval_execution_tableDTOTouserId = new ConcurrentHashMap<>();
		mapOfApproval_execution_tableDTOTouserIp = new ConcurrentHashMap<>();
		mapOfApproval_execution_tableDTOToapprovalPathId = new ConcurrentHashMap<>();
		mapOfApproval_execution_tableDTOToapprovalPathOrder = new ConcurrentHashMap<>();
		mapOfApproval_execution_tableDTOToremarks = new ConcurrentHashMap<>();
		mapOfApproval_execution_tableDTOTofileDropzone = new ConcurrentHashMap<>();
		mapOfApproval_execution_tableDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Approval_execution_tableRepository getInstance(){
		if (instance == null){
			instance = new Approval_execution_tableRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(approval_execution_tableDAO == null)
		{
			return;
		}
		try {
			List<Approval_execution_tableDTO> approval_execution_tableDTOs = approval_execution_tableDAO.getAllApproval_execution_table(reloadAll);
			for(Approval_execution_tableDTO approval_execution_tableDTO : approval_execution_tableDTOs) {
				Approval_execution_tableDTO oldApproval_execution_tableDTO = getApproval_execution_tableDTOByID(approval_execution_tableDTO.iD);
				if( oldApproval_execution_tableDTO != null ) {
					mapOfApproval_execution_tableDTOToiD.remove(oldApproval_execution_tableDTO.iD);
				
					if(mapOfApproval_execution_tableDTOTotableName.containsKey(oldApproval_execution_tableDTO.tableName)) {
						mapOfApproval_execution_tableDTOTotableName.get(oldApproval_execution_tableDTO.tableName).remove(oldApproval_execution_tableDTO);
					}
					if(mapOfApproval_execution_tableDTOTotableName.get(oldApproval_execution_tableDTO.tableName).isEmpty()) {
						mapOfApproval_execution_tableDTOTotableName.remove(oldApproval_execution_tableDTO.tableName);
					}
					
					if(mapOfApproval_execution_tableDTOTopreviousRowId.containsKey(oldApproval_execution_tableDTO.previousRowId)) {
						mapOfApproval_execution_tableDTOTopreviousRowId.get(oldApproval_execution_tableDTO.previousRowId).remove(oldApproval_execution_tableDTO);
					}
					if(mapOfApproval_execution_tableDTOTopreviousRowId.get(oldApproval_execution_tableDTO.previousRowId).isEmpty()) {
						mapOfApproval_execution_tableDTOTopreviousRowId.remove(oldApproval_execution_tableDTO.previousRowId);
					}
					
					if(mapOfApproval_execution_tableDTOToupdatedRowId.containsKey(oldApproval_execution_tableDTO.updatedRowId)) {
						mapOfApproval_execution_tableDTOToupdatedRowId.get(oldApproval_execution_tableDTO.updatedRowId).remove(oldApproval_execution_tableDTO);
					}
					if(mapOfApproval_execution_tableDTOToupdatedRowId.get(oldApproval_execution_tableDTO.updatedRowId).isEmpty()) {
						mapOfApproval_execution_tableDTOToupdatedRowId.remove(oldApproval_execution_tableDTO.updatedRowId);
					}
					
					if(mapOfApproval_execution_tableDTOTooperation.containsKey(oldApproval_execution_tableDTO.operation)) {
						mapOfApproval_execution_tableDTOTooperation.get(oldApproval_execution_tableDTO.operation).remove(oldApproval_execution_tableDTO);
					}
					if(mapOfApproval_execution_tableDTOTooperation.get(oldApproval_execution_tableDTO.operation).isEmpty()) {
						mapOfApproval_execution_tableDTOTooperation.remove(oldApproval_execution_tableDTO.operation);
					}
					
					if(mapOfApproval_execution_tableDTOToaction.containsKey(oldApproval_execution_tableDTO.action)) {
						mapOfApproval_execution_tableDTOToaction.get(oldApproval_execution_tableDTO.action).remove(oldApproval_execution_tableDTO);
					}
					if(mapOfApproval_execution_tableDTOToaction.get(oldApproval_execution_tableDTO.action).isEmpty()) {
						mapOfApproval_execution_tableDTOToaction.remove(oldApproval_execution_tableDTO.action);
					}
					
					if(mapOfApproval_execution_tableDTOToapprovalStatusCat.containsKey(oldApproval_execution_tableDTO.approvalStatusCat)) {
						mapOfApproval_execution_tableDTOToapprovalStatusCat.get(oldApproval_execution_tableDTO.approvalStatusCat).remove(oldApproval_execution_tableDTO);
					}
					if(mapOfApproval_execution_tableDTOToapprovalStatusCat.get(oldApproval_execution_tableDTO.approvalStatusCat).isEmpty()) {
						mapOfApproval_execution_tableDTOToapprovalStatusCat.remove(oldApproval_execution_tableDTO.approvalStatusCat);
					}
					
					if(mapOfApproval_execution_tableDTOTouserId.containsKey(oldApproval_execution_tableDTO.userId)) {
						mapOfApproval_execution_tableDTOTouserId.get(oldApproval_execution_tableDTO.userId).remove(oldApproval_execution_tableDTO);
					}
					if(mapOfApproval_execution_tableDTOTouserId.get(oldApproval_execution_tableDTO.userId).isEmpty()) {
						mapOfApproval_execution_tableDTOTouserId.remove(oldApproval_execution_tableDTO.userId);
					}
					
					if(mapOfApproval_execution_tableDTOTouserIp.containsKey(oldApproval_execution_tableDTO.userIp)) {
						mapOfApproval_execution_tableDTOTouserIp.get(oldApproval_execution_tableDTO.userIp).remove(oldApproval_execution_tableDTO);
					}
					if(mapOfApproval_execution_tableDTOTouserIp.get(oldApproval_execution_tableDTO.userIp).isEmpty()) {
						mapOfApproval_execution_tableDTOTouserIp.remove(oldApproval_execution_tableDTO.userIp);
					}
					
					if(mapOfApproval_execution_tableDTOToapprovalPathId.containsKey(oldApproval_execution_tableDTO.approvalPathId)) {
						mapOfApproval_execution_tableDTOToapprovalPathId.get(oldApproval_execution_tableDTO.approvalPathId).remove(oldApproval_execution_tableDTO);
					}
					if(mapOfApproval_execution_tableDTOToapprovalPathId.get(oldApproval_execution_tableDTO.approvalPathId).isEmpty()) {
						mapOfApproval_execution_tableDTOToapprovalPathId.remove(oldApproval_execution_tableDTO.approvalPathId);
					}
					
					if(mapOfApproval_execution_tableDTOToapprovalPathOrder.containsKey(oldApproval_execution_tableDTO.approvalPathOrder)) {
						mapOfApproval_execution_tableDTOToapprovalPathOrder.get(oldApproval_execution_tableDTO.approvalPathOrder).remove(oldApproval_execution_tableDTO);
					}
					if(mapOfApproval_execution_tableDTOToapprovalPathOrder.get(oldApproval_execution_tableDTO.approvalPathOrder).isEmpty()) {
						mapOfApproval_execution_tableDTOToapprovalPathOrder.remove(oldApproval_execution_tableDTO.approvalPathOrder);
					}
					
					if(mapOfApproval_execution_tableDTOToremarks.containsKey(oldApproval_execution_tableDTO.remarks)) {
						mapOfApproval_execution_tableDTOToremarks.get(oldApproval_execution_tableDTO.remarks).remove(oldApproval_execution_tableDTO);
					}
					if(mapOfApproval_execution_tableDTOToremarks.get(oldApproval_execution_tableDTO.remarks).isEmpty()) {
						mapOfApproval_execution_tableDTOToremarks.remove(oldApproval_execution_tableDTO.remarks);
					}
					
					if(mapOfApproval_execution_tableDTOTofileDropzone.containsKey(oldApproval_execution_tableDTO.fileDropzone)) {
						mapOfApproval_execution_tableDTOTofileDropzone.get(oldApproval_execution_tableDTO.fileDropzone).remove(oldApproval_execution_tableDTO);
					}
					if(mapOfApproval_execution_tableDTOTofileDropzone.get(oldApproval_execution_tableDTO.fileDropzone).isEmpty()) {
						mapOfApproval_execution_tableDTOTofileDropzone.remove(oldApproval_execution_tableDTO.fileDropzone);
					}
					
					if(mapOfApproval_execution_tableDTOTolastModificationTime.containsKey(oldApproval_execution_tableDTO.lastModificationTime)) {
						mapOfApproval_execution_tableDTOTolastModificationTime.get(oldApproval_execution_tableDTO.lastModificationTime).remove(oldApproval_execution_tableDTO);
					}
					if(mapOfApproval_execution_tableDTOTolastModificationTime.get(oldApproval_execution_tableDTO.lastModificationTime).isEmpty()) {
						mapOfApproval_execution_tableDTOTolastModificationTime.remove(oldApproval_execution_tableDTO.lastModificationTime);
					}
					
					
				}
				if(approval_execution_tableDTO.isDeleted == 0) 
				{
					
					mapOfApproval_execution_tableDTOToiD.put(approval_execution_tableDTO.iD, approval_execution_tableDTO);
				
					if( ! mapOfApproval_execution_tableDTOTotableName.containsKey(approval_execution_tableDTO.tableName)) {
						mapOfApproval_execution_tableDTOTotableName.put(approval_execution_tableDTO.tableName, new HashSet<>());
					}
					mapOfApproval_execution_tableDTOTotableName.get(approval_execution_tableDTO.tableName).add(approval_execution_tableDTO);
					
					if( ! mapOfApproval_execution_tableDTOTopreviousRowId.containsKey(approval_execution_tableDTO.previousRowId)) {
						mapOfApproval_execution_tableDTOTopreviousRowId.put(approval_execution_tableDTO.previousRowId, new HashSet<>());
					}
					mapOfApproval_execution_tableDTOTopreviousRowId.get(approval_execution_tableDTO.previousRowId).add(approval_execution_tableDTO);
					
					if( ! mapOfApproval_execution_tableDTOToupdatedRowId.containsKey(approval_execution_tableDTO.updatedRowId)) {
						mapOfApproval_execution_tableDTOToupdatedRowId.put(approval_execution_tableDTO.updatedRowId, new HashSet<>());
					}
					mapOfApproval_execution_tableDTOToupdatedRowId.get(approval_execution_tableDTO.updatedRowId).add(approval_execution_tableDTO);
					
					if( ! mapOfApproval_execution_tableDTOTooperation.containsKey(approval_execution_tableDTO.operation)) {
						mapOfApproval_execution_tableDTOTooperation.put(approval_execution_tableDTO.operation, new HashSet<>());
					}
					mapOfApproval_execution_tableDTOTooperation.get(approval_execution_tableDTO.operation).add(approval_execution_tableDTO);
					
					if( ! mapOfApproval_execution_tableDTOToaction.containsKey(approval_execution_tableDTO.action)) {
						mapOfApproval_execution_tableDTOToaction.put(approval_execution_tableDTO.action, new HashSet<>());
					}
					mapOfApproval_execution_tableDTOToaction.get(approval_execution_tableDTO.action).add(approval_execution_tableDTO);
					
					if( ! mapOfApproval_execution_tableDTOToapprovalStatusCat.containsKey(approval_execution_tableDTO.approvalStatusCat)) {
						mapOfApproval_execution_tableDTOToapprovalStatusCat.put(approval_execution_tableDTO.approvalStatusCat, new HashSet<>());
					}
					mapOfApproval_execution_tableDTOToapprovalStatusCat.get(approval_execution_tableDTO.approvalStatusCat).add(approval_execution_tableDTO);
					
					if( ! mapOfApproval_execution_tableDTOTouserId.containsKey(approval_execution_tableDTO.userId)) {
						mapOfApproval_execution_tableDTOTouserId.put(approval_execution_tableDTO.userId, new HashSet<>());
					}
					mapOfApproval_execution_tableDTOTouserId.get(approval_execution_tableDTO.userId).add(approval_execution_tableDTO);
					
					if( ! mapOfApproval_execution_tableDTOTouserIp.containsKey(approval_execution_tableDTO.userIp)) {
						mapOfApproval_execution_tableDTOTouserIp.put(approval_execution_tableDTO.userIp, new HashSet<>());
					}
					mapOfApproval_execution_tableDTOTouserIp.get(approval_execution_tableDTO.userIp).add(approval_execution_tableDTO);
					
					if( ! mapOfApproval_execution_tableDTOToapprovalPathId.containsKey(approval_execution_tableDTO.approvalPathId)) {
						mapOfApproval_execution_tableDTOToapprovalPathId.put(approval_execution_tableDTO.approvalPathId, new HashSet<>());
					}
					mapOfApproval_execution_tableDTOToapprovalPathId.get(approval_execution_tableDTO.approvalPathId).add(approval_execution_tableDTO);
					
					if( ! mapOfApproval_execution_tableDTOToapprovalPathOrder.containsKey(approval_execution_tableDTO.approvalPathOrder)) {
						mapOfApproval_execution_tableDTOToapprovalPathOrder.put(approval_execution_tableDTO.approvalPathOrder, new HashSet<>());
					}
					mapOfApproval_execution_tableDTOToapprovalPathOrder.get(approval_execution_tableDTO.approvalPathOrder).add(approval_execution_tableDTO);
					
					if( ! mapOfApproval_execution_tableDTOToremarks.containsKey(approval_execution_tableDTO.remarks)) {
						mapOfApproval_execution_tableDTOToremarks.put(approval_execution_tableDTO.remarks, new HashSet<>());
					}
					mapOfApproval_execution_tableDTOToremarks.get(approval_execution_tableDTO.remarks).add(approval_execution_tableDTO);
					
					if( ! mapOfApproval_execution_tableDTOTofileDropzone.containsKey(approval_execution_tableDTO.fileDropzone)) {
						mapOfApproval_execution_tableDTOTofileDropzone.put(approval_execution_tableDTO.fileDropzone, new HashSet<>());
					}
					mapOfApproval_execution_tableDTOTofileDropzone.get(approval_execution_tableDTO.fileDropzone).add(approval_execution_tableDTO);
					
					if( ! mapOfApproval_execution_tableDTOTolastModificationTime.containsKey(approval_execution_tableDTO.lastModificationTime)) {
						mapOfApproval_execution_tableDTOTolastModificationTime.put(approval_execution_tableDTO.lastModificationTime, new HashSet<>());
					}
					mapOfApproval_execution_tableDTOTolastModificationTime.get(approval_execution_tableDTO.lastModificationTime).add(approval_execution_tableDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Approval_execution_tableDTO> getApproval_execution_tableList() {
		List <Approval_execution_tableDTO> approval_execution_tables = new ArrayList<Approval_execution_tableDTO>(this.mapOfApproval_execution_tableDTOToiD.values());
		return approval_execution_tables;
	}
	
	
	public Approval_execution_tableDTO getApproval_execution_tableDTOByID( long ID){
		return mapOfApproval_execution_tableDTOToiD.get(ID);
	}
	
	
	public List<Approval_execution_tableDTO> getApproval_execution_tableDTOBytable_name(String table_name) {
		return new ArrayList<>( mapOfApproval_execution_tableDTOTotableName.getOrDefault(table_name,new HashSet<>()));
	}
	
	
	public List<Approval_execution_tableDTO> getApproval_execution_tableDTOByprevious_row_id(long previous_row_id) {
		return new ArrayList<>( mapOfApproval_execution_tableDTOTopreviousRowId.getOrDefault(previous_row_id,new HashSet<>()));
	}
	
	
	public List<Approval_execution_tableDTO> getApproval_execution_tableDTOByupdated_row_id(long updated_row_id) {
		return new ArrayList<>( mapOfApproval_execution_tableDTOToupdatedRowId.getOrDefault(updated_row_id,new HashSet<>()));
	}
	
	
	public List<Approval_execution_tableDTO> getApproval_execution_tableDTOByoperation(int operation) {
		return new ArrayList<>( mapOfApproval_execution_tableDTOTooperation.getOrDefault(operation,new HashSet<>()));
	}
	
	
	public List<Approval_execution_tableDTO> getApproval_execution_tableDTOByaction(int action) {
		return new ArrayList<>( mapOfApproval_execution_tableDTOToaction.getOrDefault(action,new HashSet<>()));
	}
	
	
	public List<Approval_execution_tableDTO> getApproval_execution_tableDTOByapproval_status_cat(int approval_status_cat) {
		return new ArrayList<>( mapOfApproval_execution_tableDTOToapprovalStatusCat.getOrDefault(approval_status_cat,new HashSet<>()));
	}
	
	
	public List<Approval_execution_tableDTO> getApproval_execution_tableDTOByuser_id(long user_id) {
		return new ArrayList<>( mapOfApproval_execution_tableDTOTouserId.getOrDefault(user_id,new HashSet<>()));
	}
	
	
	public List<Approval_execution_tableDTO> getApproval_execution_tableDTOByuser_ip(String user_ip) {
		return new ArrayList<>( mapOfApproval_execution_tableDTOTouserIp.getOrDefault(user_ip,new HashSet<>()));
	}
	
	
	public List<Approval_execution_tableDTO> getApproval_execution_tableDTOByapproval_path_id(long approval_path_id) {
		return new ArrayList<>( mapOfApproval_execution_tableDTOToapprovalPathId.getOrDefault(approval_path_id,new HashSet<>()));
	}
	
	
	public List<Approval_execution_tableDTO> getApproval_execution_tableDTOByapproval_path_order(int approval_path_order) {
		return new ArrayList<>( mapOfApproval_execution_tableDTOToapprovalPathOrder.getOrDefault(approval_path_order,new HashSet<>()));
	}
	
	
	public List<Approval_execution_tableDTO> getApproval_execution_tableDTOByremarks(String remarks) {
		return new ArrayList<>( mapOfApproval_execution_tableDTOToremarks.getOrDefault(remarks,new HashSet<>()));
	}
	
	
	public List<Approval_execution_tableDTO> getApproval_execution_tableDTOByfile_dropzone(long file_dropzone) {
		return new ArrayList<>( mapOfApproval_execution_tableDTOTofileDropzone.getOrDefault(file_dropzone,new HashSet<>()));
	}
	
	
	public List<Approval_execution_tableDTO> getApproval_execution_tableDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfApproval_execution_tableDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "approval_execution_table";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


