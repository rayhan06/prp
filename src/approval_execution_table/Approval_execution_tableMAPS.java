package approval_execution_table;
import java.util.*; 
import util.*;


public class Approval_execution_tableMAPS extends CommonMaps
{	
	public Approval_execution_tableMAPS(String tableName)
	{
		
		java_allfield_type_map.put("table_name".toLowerCase(), "String");
		java_allfield_type_map.put("previous_row_id".toLowerCase(), "Long");
		java_allfield_type_map.put("updated_row_id".toLowerCase(), "Long");
		java_allfield_type_map.put("operation".toLowerCase(), "Integer");
		java_allfield_type_map.put("action".toLowerCase(), "Integer");
		java_allfield_type_map.put("approval_status_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("user_id".toLowerCase(), "Long");
		java_allfield_type_map.put("user_ip".toLowerCase(), "String");
		java_allfield_type_map.put("approval_path_id".toLowerCase(), "Long");
		java_allfield_type_map.put("approval_path_order".toLowerCase(), "Integer");
		java_allfield_type_map.put("remarks".toLowerCase(), "String");

		java_anyfield_search_map.put("language_text.languageTextEnglish", "String");
		java_anyfield_search_map.put("language_text.languageTextBangla", "String");

		java_anyfield_search_map.put(tableName + ".table_name".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".previous_row_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".updated_row_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".operation".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".action".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".user_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".user_ip".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".approval_path_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".approval_path_order".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".remarks".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("tableName".toLowerCase(), "tableName".toLowerCase());
		java_DTO_map.put("previousRowId".toLowerCase(), "previousRowId".toLowerCase());
		java_DTO_map.put("updatedRowId".toLowerCase(), "updatedRowId".toLowerCase());
		java_DTO_map.put("operation".toLowerCase(), "operation".toLowerCase());
		java_DTO_map.put("action".toLowerCase(), "action".toLowerCase());
		java_DTO_map.put("approvalStatusCat".toLowerCase(), "approvalStatusCat".toLowerCase());
		java_DTO_map.put("userId".toLowerCase(), "userId".toLowerCase());
		java_DTO_map.put("userIp".toLowerCase(), "userIp".toLowerCase());
		java_DTO_map.put("approvalPathId".toLowerCase(), "approvalPathId".toLowerCase());
		java_DTO_map.put("approvalPathOrder".toLowerCase(), "approvalPathOrder".toLowerCase());
		java_DTO_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_DTO_map.put("fileDropzone".toLowerCase(), "fileDropzone".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("table_name".toLowerCase(), "tableName".toLowerCase());
		java_SQL_map.put("previous_row_id".toLowerCase(), "previousRowId".toLowerCase());
		java_SQL_map.put("updated_row_id".toLowerCase(), "updatedRowId".toLowerCase());
		java_SQL_map.put("operation".toLowerCase(), "operation".toLowerCase());
		java_SQL_map.put("action".toLowerCase(), "action".toLowerCase());
		java_SQL_map.put("approval_status_cat".toLowerCase(), "approvalStatusCat".toLowerCase());
		java_SQL_map.put("user_id".toLowerCase(), "userId".toLowerCase());
		java_SQL_map.put("user_ip".toLowerCase(), "userIp".toLowerCase());
		java_SQL_map.put("approval_path_id".toLowerCase(), "approvalPathId".toLowerCase());
		java_SQL_map.put("approval_path_order".toLowerCase(), "approvalPathOrder".toLowerCase());
		java_SQL_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_SQL_map.put("file_dropzone".toLowerCase(), "fileDropzone".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Table Name".toLowerCase(), "tableName".toLowerCase());
		java_Text_map.put("Previous Row Id".toLowerCase(), "previousRowId".toLowerCase());
		java_Text_map.put("Updated Row Id".toLowerCase(), "updatedRowId".toLowerCase());
		java_Text_map.put("Operation".toLowerCase(), "operation".toLowerCase());
		java_Text_map.put("Action".toLowerCase(), "action".toLowerCase());
		java_Text_map.put("Approval Status Cat".toLowerCase(), "approvalStatusCat".toLowerCase());
		java_Text_map.put("User Id".toLowerCase(), "userId".toLowerCase());
		java_Text_map.put("User Ip".toLowerCase(), "userIp".toLowerCase());
		java_Text_map.put("Approval Path Id".toLowerCase(), "approvalPathId".toLowerCase());
		java_Text_map.put("Approval Path Order".toLowerCase(), "approvalPathOrder".toLowerCase());
		java_Text_map.put("Remarks".toLowerCase(), "remarks".toLowerCase());
		java_Text_map.put("File Dropzone".toLowerCase(), "fileDropzone".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}