package approval_execution_table;

import java.io.IOException;
import java.io.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;

import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;

import approval_execution_table.Constants;
import dashboard.DashboardService;
import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import files.*;

/**
 * Servlet implementation class Approval_execution_tableServlet
 */
@WebServlet("/Approval_execution_tableServlet")
@MultipartConfig
public class Approval_execution_tableServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Approval_execution_tableServlet.class);

    String tableName = "approval_execution_table";

	Approval_execution_tableDAO approval_execution_tableDAO;
	FilesDAO filesDAO = new FilesDAO();
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Approval_execution_tableServlet() 
	{
        super();
    	try
    	{
			approval_execution_tableDAO = new Approval_execution_tableDAO(tableName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_EXECUTION_TABLE_ADD))
				{
					getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_EXECUTION_TABLE_UPDATE))
				{
					getApproval_execution_table(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("downloadDropzoneFile"))
			{
				long id = Long.parseLong(request.getParameter("id"));

				FilesDTO filesDTO = (FilesDTO) filesDAO.getDTOByID(id);
				
				Utils.ProcessFile(request, response, filesDTO.fileTitle, filesDTO.inputStream);
				
			}
			else if(actionType.equals("DeleteFileFromDropZone"))
			{
				long id = Long.parseLong(request.getParameter("id"));
			
				
				System.out.println("In delete file");
				filesDAO.hardDeleteByID(id);
				response.getWriter().write("Deleted");
							
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				doPost(request, response);
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		request.setAttribute("ID", -1L);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("approval_execution_table/approval_execution_tableEdit.jsp");
		requestDispatcher.forward(request, response);
	}
	private String getFileName(final Part part) 
	{
	    final String partHeader = part.getHeader("content-disposition");
	    System.out.println("Part Header = {0}" +  partHeader);
	    for (String content : part.getHeader("content-disposition").split(";")) {
	        if (content.trim().startsWith("filename")) {
	            return content.substring(
	                    content.indexOf('=') + 1).trim().replace("\"", "");
	        }
	    }
	    return null;
	}

	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_EXECUTION_TABLE_ADD))
				{
					System.out.println("going to  addApproval_execution_table ");
					addApproval_execution_table(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addApproval_execution_table ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("UploadFilesFromDropZone"))
			{
				String Column = request.getParameter("columnName");
				long ColumnID = Long.parseLong(request.getParameter("ColumnID"));
				String pageType = request.getParameter("pageType");
				
				System.out.println("In " + pageType);				
				Utils.UploadFilesFromDropZone(request, response, userDTO.ID, ColumnID, "Approval_execution_tableServlet");	
			}

			if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_EXECUTION_TABLE_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addApproval_execution_table ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_EXECUTION_TABLE_UPDATE))
				{					
					addApproval_execution_table(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{								
				deleteApproval_execution_table(request, response, userDTO);				
			}	
			else if(actionType.equals("search"))
			{
				//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_EXECUTION_TABLE_SEARCH))
				{
					String tableName = request.getParameter("tableName");
					String compactView = request.getParameter("compactView");
					String status = request.getParameter("status");
					String previousRowId = request.getParameter("previousRowId");
					
					System.out.println("In Search tableName = " + tableName 
							+ " previousRowId = " + previousRowId
							+ " compactView " + compactView);
					
					if(compactView != null && !compactView.equalsIgnoreCase(""))
					{
						/*String filter = DashboardService.aeCompactFilter;
						
						if(status.equalsIgnoreCase("pending"))
						{
							filter += "AND " + DashboardService.aePendingFilter  + "=" + userDTO.organogramID;
						}
						else if (status.equalsIgnoreCase("completed"))
						{
							filter += " AND " + userDTO.organogramID + DashboardService.aeCompletedFilter 
									+ " AND" + DashboardService.aePendingFilter + "!=" + userDTO.organogramID;
						}
						else
						{
							filter += "AND ((" + DashboardService.aePendingFilter + "=" + + userDTO.organogramID + ") OR ("
									+ userDTO.organogramID + DashboardService.aeCompletedFilter + "))";
						}
						searchApproval_execution_table(request, response, true, filter);*/
					}
					else
					{					
						if(tableName == null || tableName.equalsIgnoreCase("") || previousRowId.equalsIgnoreCase(""))
						{
							searchApproval_execution_table(request, response, true, "");
						}
						else
						{
							String filter = " table_name = '" + tableName + "' and previous_row_id = " + previousRowId;
							searchApproval_execution_table(request, response, true, filter);
						}
					}
					
				}
				
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Approval_execution_tableDTO approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(approval_execution_tableDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	private void approveApproval_execution_table(HttpServletRequest request, HttpServletResponse response, boolean b, UserDTO userDTO, boolean approveOrReject) 
	{
		try
		{
			long id = Long.parseLong(request.getParameter("idToApprove"));
			Approval_execution_tableDTO approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getDTOByID(id);
			if(approveOrReject)
			{
				approval_execution_tableDAO.manageWriteOperations(approval_execution_tableDTO, SessionConstants.APPROVE, id, userDTO);
			}
			else
			{
				approval_execution_tableDAO.manageWriteOperations(approval_execution_tableDTO, SessionConstants.REJECT, id, userDTO);
			}			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
	}
	private void addApproval_execution_table(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addApproval_execution_table");
			String path = getServletContext().getRealPath("/img2/");
			Approval_execution_tableDTO approval_execution_tableDTO;
			String FileNamePrefix;			
			if(addFlag == true)
			{
				approval_execution_tableDTO = new Approval_execution_tableDTO();
				FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
			}
			else
			{
				approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getDTOByID(Long.parseLong(request.getParameter("identity")));
				FileNamePrefix = request.getParameter("identity");
			}
			
			String Value = "";

			Value = request.getParameter("tableName");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("tableName = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				approval_execution_tableDTO.tableName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("previousRowId");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("previousRowId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				approval_execution_tableDTO.previousRowId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("updatedRowId");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("updatedRowId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				approval_execution_tableDTO.updatedRowId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("operation");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("operation = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				approval_execution_tableDTO.operation = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("action");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("action = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				approval_execution_tableDTO.action = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approvalStatusCat");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approvalStatusCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				approval_execution_tableDTO.approvalStatusCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("userId");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("userId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				approval_execution_tableDTO.userId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("userIp");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("userIp = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				approval_execution_tableDTO.userIp = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approvalPathId");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approvalPathId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				approval_execution_tableDTO.approvalPathId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approvalPathOrder");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approvalPathOrder = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				approval_execution_tableDTO.approvalPathOrder = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("remarks");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("remarks = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				approval_execution_tableDTO.remarks = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("fileDropzone");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("fileDropzone = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				if(addFlag == true)
				{
					System.out.println("fileDropzone = " + Value);
					if(Value != null && !Value.equalsIgnoreCase(""))
					{
						approval_execution_tableDTO.fileDropzone = Long.parseLong(Value);
					}
					else
					{
						System.out.println("FieldName has a null Value, not updating" + " = " + Value);
					}
				}
				else
				{
					String fileDropzoneFilesToDelete = request.getParameter("fileDropzoneFilesToDelete");
					String[] deleteArray = fileDropzoneFilesToDelete.split(",");
					for(int i = 0; i < deleteArray.length; i ++)
					{
						System.out.println("going to delete " + deleteArray[i]);
						if(i>0)
						{
							filesDAO.delete(Long.parseLong(deleteArray[i]));
						}
					}
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addApproval_execution_table dto = " + approval_execution_tableDTO);
			long returnedID = -1;
			
			if(addFlag == true)
			{
				returnedID = approval_execution_tableDAO.manageWriteOperations(approval_execution_tableDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				if(isPermanentTable)
				{
					returnedID = approval_execution_tableDAO.manageWriteOperations(approval_execution_tableDTO, SessionConstants.UPDATE, -1, userDTO);
				}
				else
				{
					returnedID = approval_execution_tableDAO.manageWriteOperations(approval_execution_tableDTO, SessionConstants.VALIDATE, -1, userDTO);
				}				
				
			}
			
			
			
			
			
			
			
			
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			
			if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				getApproval_execution_table(request, response, returnedID);
			}
			else
			{
				response.sendRedirect("Approval_execution_tableServlet?actionType=search");
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void deleteApproval_execution_table(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				
				Approval_execution_tableDTO approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getDTOByID(id);
				approval_execution_tableDAO.manageWriteOperations(approval_execution_tableDTO, SessionConstants.DELETE, id, userDTO);
				response.sendRedirect("Approval_execution_tableServlet?actionType=search");
				
			}			
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		
	}

	private void getApproval_execution_table(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getApproval_execution_table");
		Approval_execution_tableDTO approval_execution_tableDTO = null;
		try 
		{
			approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getDTOByID(id);
			request.setAttribute("ID", approval_execution_tableDTO.iD);
			request.setAttribute("approval_execution_tableDTO",approval_execution_tableDTO);
			request.setAttribute("approval_execution_tableDAO",approval_execution_tableDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "approval_execution_table/approval_execution_tableInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "approval_execution_table/approval_execution_tableSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "approval_execution_table/approval_execution_tableEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "approval_execution_table/approval_execution_tableEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getApproval_execution_table(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getApproval_execution_table(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchApproval_execution_table(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchApproval_execution_table 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		System.out.println("in search servlet filter = " + filter);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_APPROVAL_EXECUTION_TABLE,
			request,
			approval_execution_tableDAO,
			SessionConstants.VIEW_APPROVAL_EXECUTION_TABLE,
			SessionConstants.SEARCH_APPROVAL_EXECUTION_TABLE,
			tableName,
			isPermanent,
			userDTO,
			filter,
			false);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("approval_execution_tableDAO",approval_execution_tableDAO);
        RequestDispatcher rd;
        if(hasAjax == false)
        {
        	System.out.println("Going to approval_execution_table/approval_execution_tableSearch.jsp");
        	rd = request.getRequestDispatcher("approval_execution_table/approval_execution_tableSearch.jsp");
        }
        else
        {
        	System.out.println("Going to approval_execution_table/approval_execution_tableSearchForm.jsp");
        	rd = request.getRequestDispatcher("approval_execution_table/approval_execution_tableSearchForm.jsp");
        }
		rd.forward(request, response);
	}
	
}

