package approval_execution_table;
import java.util.*; 
import util.*; 


public class Approval_execution_tableDTO extends CommonDTO
{

    public String tableName = "";
	public long previousRowId = 0;
	public long updatedRowId = 0;
	public int operation = 0;
	public int action = 2;
	public int approvalStatusCat = 0;
	public long userId = 0;
	public long organogramID = 0;
    public String userIp = "";
	public long approvalPathId = 0;
	public int approvalPathOrder = 0;
    public String remarks = "";
	public long fileDropzone = 0;
	
	
	public static int APPROVED = 0;
	public static int REJECTED = 1;
	public static int PENDING = 2;
	public static int APPROVAL_NOT_NEEDED = 3;
	public static int TERMINATED = 4;
	
	
    @Override
	public String toString() {
            return "$Approval_execution_tableDTO[" +
            " iD = " + iD +
            " tableName = " + tableName +
            " previousRowId = " + previousRowId +
            " updatedRowId = " + updatedRowId +
            " operation = " + operation +
            " action = " + action +
            " approvalStatusCat = " + approvalStatusCat +
            " userId = " + userId +
            " userIp = " + userIp +
            " approvalPathId = " + approvalPathId +
            " approvalPathOrder = " + approvalPathOrder +
            " remarks = " + remarks +
            " fileDropzone = " + fileDropzone +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}