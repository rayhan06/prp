package approval_execution_table;

import dbm.DBMR;
import dbm.DBMW;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;

public class Approval_execution_tableDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	
	public Approval_execution_tableDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";

		joinSQL += " join category on (";
		joinSQL += " (" + tableName + ".approval_status_cat = category.value  and category.domain_name = 'approval_status')";
		joinSQL += " )";
		joinSQL += " join language_text on category.language_id = language_text.id";		
		commonMaps = new Approval_execution_tableMAPS(tableName);
	}
	
	public Approval_execution_tableDAO()
	{
		this("approval_execution_table");		
	}
	
	
	
	public long add(CommonDTO commonDTO)
	{
		
		Approval_execution_tableDTO approval_execution_tableDTO = (Approval_execution_tableDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				logger.debug("nullconn");
			}

			approval_execution_tableDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "table_name";
			sql += ", ";
			sql += "previous_row_id";
			sql += ", ";
			sql += "updated_row_id";
			sql += ", ";
			sql += "operation";
			sql += ", ";
			sql += "action";
			sql += ", ";
			sql += "approval_status_cat";
			sql += ", ";
			sql += "user_id";
			sql += ", ";
			sql += "user_ip";
			sql += ", ";
			sql += "approval_path_id";
			sql += ", ";
			sql += "approval_path_order";
			sql += ", ";
			sql += "remarks";
			sql += ", ";
			sql += "file_dropzone";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
			sql += ", ";
			sql += "organogram_id";
		
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
			sql += ")";
				

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,approval_execution_tableDTO.iD);
			ps.setObject(index++,approval_execution_tableDTO.tableName);
			ps.setObject(index++,approval_execution_tableDTO.previousRowId);
			ps.setObject(index++,approval_execution_tableDTO.updatedRowId);
			ps.setObject(index++,approval_execution_tableDTO.operation);
			ps.setObject(index++,approval_execution_tableDTO.action);
			ps.setObject(index++,approval_execution_tableDTO.approvalStatusCat);
			ps.setObject(index++,approval_execution_tableDTO.userId);
			ps.setObject(index++,approval_execution_tableDTO.userIp);
			ps.setObject(index++,approval_execution_tableDTO.approvalPathId);
			ps.setObject(index++,approval_execution_tableDTO.approvalPathOrder);
			ps.setObject(index++,approval_execution_tableDTO.remarks);
			ps.setObject(index++,approval_execution_tableDTO.fileDropzone);
			ps.setObject(index++,approval_execution_tableDTO.isDeleted);			
			ps.setObject(index++, lastModificationTime);
			ps.setObject(index++,approval_execution_tableDTO.organogramID);
			
			logger.debug(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return approval_execution_tableDTO.iD;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Approval_execution_tableDTO approval_execution_tableDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				approval_execution_tableDTO = new Approval_execution_tableDTO();

				approval_execution_tableDTO.iD = rs.getLong("ID");
				approval_execution_tableDTO.tableName = rs.getString("table_name");
				approval_execution_tableDTO.previousRowId = rs.getLong("previous_row_id");
				approval_execution_tableDTO.updatedRowId = rs.getLong("updated_row_id");
				approval_execution_tableDTO.operation = rs.getInt("operation");
				approval_execution_tableDTO.action = rs.getInt("action");
				approval_execution_tableDTO.approvalStatusCat = rs.getInt("approval_status_cat");
				approval_execution_tableDTO.userId = rs.getLong("user_id");
				approval_execution_tableDTO.userIp = rs.getString("user_ip");
				approval_execution_tableDTO.approvalPathId = rs.getLong("approval_path_id");
				approval_execution_tableDTO.approvalPathOrder = rs.getInt("approval_path_order");
				approval_execution_tableDTO.remarks = rs.getString("remarks");
				approval_execution_tableDTO.fileDropzone = rs.getLong("file_dropzone");
				approval_execution_tableDTO.isDeleted = rs.getInt("isDeleted");
				approval_execution_tableDTO.lastModificationTime = rs.getLong("lastModificationTime");
				approval_execution_tableDTO.organogramID = rs.getLong("organogram_id");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return approval_execution_tableDTO;
	}
	
	public void deleteByUpdatedRowId (String tableNameToSearch, long updatedRowId) throws Exception{
		Connection connection = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		
		long lastModificationTime = System.currentTimeMillis();	
		try{
			String sql = "UPDATE " + tableName;
			
			sql += " SET isDeleted= 1, lastModificationTime = "+ lastModificationTime +" WHERE table_name = '" + tableNameToSearch + "'"
					+ " AND updated_row_id = " + updatedRowId;
			
			printSql(sql);

			connection = DBMW.getInstance().getConnection();
			stmt  = connection.createStatement();
			stmt.execute(sql);
			

			
			recordUpdateTime(connection, lastModificationTime);

			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMW.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
	}

	public CommonDTO getMostRecentDTOByUpdatedRowId (String tableNameToSearch, long updatedRowId) 
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Approval_execution_tableDTO approval_execution_tableDTO = null;
		try{
			
			String sql = "SELECT \r\n" + 
					"    *\r\n" + 
					"FROM\r\n" + 
					"    approval_execution_table\r\n" + 
					"WHERE\r\n" + 
					"    id = (SELECT \r\n" + 
					"            MAX(id)\r\n" + 
					"        FROM\r\n" + 
					"            approval_execution_table\r\n" + 
					"        WHERE\r\n" + 
					"            table_name = '" + tableNameToSearch + "'\r\n" + 
					"                AND updated_row_id = " + updatedRowId + " and isDeleted = 0);" ;
			
			//printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();

			logger.debug(sql);
			rs = stmt.executeQuery(sql);

			if(rs.next()){
				approval_execution_tableDTO = new Approval_execution_tableDTO();

				approval_execution_tableDTO.iD = rs.getLong("ID");
				approval_execution_tableDTO.tableName = rs.getString("table_name");
				approval_execution_tableDTO.previousRowId = rs.getLong("previous_row_id");
				approval_execution_tableDTO.updatedRowId = rs.getLong("updated_row_id");
				approval_execution_tableDTO.operation = rs.getInt("operation");
				approval_execution_tableDTO.action = rs.getInt("action");
				approval_execution_tableDTO.approvalStatusCat = rs.getInt("approval_status_cat");
				approval_execution_tableDTO.userId = rs.getLong("user_id");
				approval_execution_tableDTO.userIp = rs.getString("user_ip");
				approval_execution_tableDTO.approvalPathId = rs.getLong("approval_path_id");
				approval_execution_tableDTO.approvalPathOrder = rs.getInt("approval_path_order");
				approval_execution_tableDTO.remarks = rs.getString("remarks");
				approval_execution_tableDTO.fileDropzone = rs.getLong("file_dropzone");
				approval_execution_tableDTO.isDeleted = rs.getInt("isDeleted");
				approval_execution_tableDTO.lastModificationTime = rs.getLong("lastModificationTime");
				approval_execution_tableDTO.organogramID = rs.getLong("organogram_id");

			}
			else
			{
				logger.debug("No execution table found");
			}
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return approval_execution_tableDTO;
	}
	
	public CommonDTO getMostRecentDTOByUpdatedRowsPreviousRowId (String tableNameToSearch, long updatedRowId) 
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Approval_execution_tableDTO approval_execution_tableDTO = null;
		try{
			
			String sql = "SELECT \r\n" + 
					"    *\r\n" + 
					"FROM\r\n" + 
					"    approval_execution_table\r\n" + 
					"WHERE\r\n" + 
					"    id = (SELECT \r\n" + 
					"            MAX(id)\r\n" + 
					"        FROM\r\n" + 
					"            approval_execution_table\r\n" + 
					"        WHERE\r\n" + 
					"            table_name = '" + tableNameToSearch + "'\r\n" + 
					"                AND previous_row_id = (SELECT \r\n" + 
					"                    max(previous_row_id)\r\n" + 
					"                FROM\r\n" + 
					"                    approval_execution_table\r\n" + 
					"                WHERE\r\n" + 
					"                    table_name = '" + tableNameToSearch  + "'\r\n" + 
					"                        AND updated_row_id = " + updatedRowId + ")\r\n" + 
					"                AND isDeleted = 0);" ;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				approval_execution_tableDTO = new Approval_execution_tableDTO();

				approval_execution_tableDTO.iD = rs.getLong("ID");
				approval_execution_tableDTO.tableName = rs.getString("table_name");
				approval_execution_tableDTO.previousRowId = rs.getLong("previous_row_id");
				approval_execution_tableDTO.updatedRowId = rs.getLong("updated_row_id");
				approval_execution_tableDTO.operation = rs.getInt("operation");
				approval_execution_tableDTO.action = rs.getInt("action");
				approval_execution_tableDTO.approvalStatusCat = rs.getInt("approval_status_cat");
				approval_execution_tableDTO.userId = rs.getLong("user_id");
				approval_execution_tableDTO.userIp = rs.getString("user_ip");
				approval_execution_tableDTO.approvalPathId = rs.getLong("approval_path_id");
				approval_execution_tableDTO.approvalPathOrder = rs.getInt("approval_path_order");
				approval_execution_tableDTO.remarks = rs.getString("remarks");
				approval_execution_tableDTO.fileDropzone = rs.getLong("file_dropzone");
				approval_execution_tableDTO.isDeleted = rs.getInt("isDeleted");
				approval_execution_tableDTO.lastModificationTime = rs.getLong("lastModificationTime");
				approval_execution_tableDTO.organogramID = rs.getLong("organogram_id");

			}
			else
			{
				logger.debug("No execution table found");
			}
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return approval_execution_tableDTO;
	}

	
	
	public CommonDTO getMostRecentDTOByPreviousRowID (String tableNameToSearch, long previousRowId) 
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Approval_execution_tableDTO approval_execution_tableDTO = null;
		try{
			
			String sql = "SELECT \r\n" + 
					"    *\r\n" + 
					"FROM\r\n" + 
					"    approval_execution_table\r\n" + 
					"WHERE\r\n" + 
					"    id = (SELECT \r\n" + 
					"            MAX(id)\r\n" + 
					"        FROM\r\n" + 
					"            approval_execution_table\r\n" + 
					"        WHERE\r\n" + 
					"            table_name = '" + tableNameToSearch + "'\r\n" + 
					"                AND previous_row_id = " + previousRowId + " and isDeleted = 0);" ;
			
			//printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				approval_execution_tableDTO = new Approval_execution_tableDTO();

				approval_execution_tableDTO.iD = rs.getLong("ID");
				approval_execution_tableDTO.tableName = rs.getString("table_name");
				approval_execution_tableDTO.previousRowId = rs.getLong("previous_row_id");
				approval_execution_tableDTO.updatedRowId = rs.getLong("updated_row_id");
				approval_execution_tableDTO.operation = rs.getInt("operation");
				approval_execution_tableDTO.action = rs.getInt("action");
				approval_execution_tableDTO.approvalStatusCat = rs.getInt("approval_status_cat");
				approval_execution_tableDTO.userId = rs.getLong("user_id");
				approval_execution_tableDTO.userIp = rs.getString("user_ip");
				approval_execution_tableDTO.approvalPathId = rs.getLong("approval_path_id");
				approval_execution_tableDTO.approvalPathOrder = rs.getInt("approval_path_order");
				approval_execution_tableDTO.remarks = rs.getString("remarks");
				approval_execution_tableDTO.fileDropzone = rs.getLong("file_dropzone");
				approval_execution_tableDTO.isDeleted = rs.getInt("isDeleted");
				approval_execution_tableDTO.lastModificationTime = rs.getLong("lastModificationTime");
				approval_execution_tableDTO.organogramID = rs.getLong("organogram_id");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return approval_execution_tableDTO;
	}
	
	public long getMostRecentExecutionIDByPreviousRowID (String tableNameToSearch, long previousRowId) 
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		long id = -1;
		try{
			
			String sql = "SELECT \r\n" + 
					"    id\r\n" + 
					"FROM\r\n" + 
					"    approval_execution_table\r\n" + 
					"WHERE\r\n" + 
					"    id = (SELECT \r\n" + 
					"            MAX(id)\r\n" + 
					"        FROM\r\n" + 
					"            approval_execution_table\r\n" + 
					"        WHERE\r\n" + 
					"            table_name = '" + tableNameToSearch + "'\r\n" + 
					"                AND previous_row_id = " + previousRowId + " and isDeleted = 0);" ;
			
			//printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){

				id = rs.getLong("ID");				
			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return id;
	}
	
	
	public CommonDTO getInitiationDTOByUpdatedRowId (String tableNameToSearch, long updatedRowId) 
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Approval_execution_tableDTO approval_execution_tableDTO = null;
		try{
			
			String sql = "SELECT \r\n" + 
					"    *\r\n" + 
					"FROM\r\n" + 
					"    approval_execution_table\r\n" + 
					"WHERE\r\n" + 
					"    id = (SELECT \r\n" + 
					"            MAX(id)\r\n" + 
					"        FROM\r\n" + 
					"            approval_execution_table\r\n" + 
					"        WHERE\r\n" + 
					"            table_name = '" + tableNameToSearch + "'\r\n" + 
					"                AND updated_row_id = " + updatedRowId + " and isDeleted = 0"
							+ " and action = " + SessionConstants.INITIATE + " );" ;
			
			//printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				approval_execution_tableDTO = new Approval_execution_tableDTO();

				approval_execution_tableDTO.iD = rs.getLong("ID");
				approval_execution_tableDTO.tableName = rs.getString("table_name");
				approval_execution_tableDTO.previousRowId = rs.getLong("previous_row_id");
				approval_execution_tableDTO.updatedRowId = rs.getLong("updated_row_id");
				approval_execution_tableDTO.operation = rs.getInt("operation");
				approval_execution_tableDTO.action = rs.getInt("action");
				approval_execution_tableDTO.approvalStatusCat = rs.getInt("approval_status_cat");
				approval_execution_tableDTO.userId = rs.getLong("user_id");
				approval_execution_tableDTO.userIp = rs.getString("user_ip");
				approval_execution_tableDTO.approvalPathId = rs.getLong("approval_path_id");
				approval_execution_tableDTO.approvalPathOrder = rs.getInt("approval_path_order");
				approval_execution_tableDTO.remarks = rs.getString("remarks");
				approval_execution_tableDTO.fileDropzone = rs.getLong("file_dropzone");
				approval_execution_tableDTO.isDeleted = rs.getInt("isDeleted");
				approval_execution_tableDTO.lastModificationTime = rs.getLong("lastModificationTime");
				approval_execution_tableDTO.organogramID = rs.getLong("organogram_id");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return approval_execution_tableDTO;
	}
	
	public CommonDTO getInitiationDTOByPreviousRowId (String tableNameToSearch, long previousRowId) 
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Approval_execution_tableDTO approval_execution_tableDTO = null;
		try{
			
			String sql = "SELECT \r\n" + 
					"    *\r\n" + 
					"FROM\r\n" + 
					"    approval_execution_table\r\n" + 
					"WHERE\r\n" + 
					"    id = (SELECT \r\n" + 
					"            MAX(id)\r\n" + 
					"        FROM\r\n" + 
					"            approval_execution_table\r\n" + 
					"        WHERE\r\n" + 
					"            table_name = '" + tableNameToSearch + "'\r\n" + 
					"                AND previous_row_id = " + previousRowId + " and isDeleted = 0"
							+ " and action = " + SessionConstants.INITIATE + " );" ;
			
			//printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				approval_execution_tableDTO = new Approval_execution_tableDTO();

				approval_execution_tableDTO.iD = rs.getLong("ID");
				approval_execution_tableDTO.tableName = rs.getString("table_name");
				approval_execution_tableDTO.previousRowId = rs.getLong("previous_row_id");
				approval_execution_tableDTO.updatedRowId = rs.getLong("updated_row_id");
				approval_execution_tableDTO.operation = rs.getInt("operation");
				approval_execution_tableDTO.action = rs.getInt("action");
				approval_execution_tableDTO.approvalStatusCat = rs.getInt("approval_status_cat");
				approval_execution_tableDTO.userId = rs.getLong("user_id");
				approval_execution_tableDTO.userIp = rs.getString("user_ip");
				approval_execution_tableDTO.approvalPathId = rs.getLong("approval_path_id");
				approval_execution_tableDTO.approvalPathOrder = rs.getInt("approval_path_order");
				approval_execution_tableDTO.remarks = rs.getString("remarks");
				approval_execution_tableDTO.fileDropzone = rs.getLong("file_dropzone");
				approval_execution_tableDTO.isDeleted = rs.getInt("isDeleted");
				approval_execution_tableDTO.lastModificationTime = rs.getLong("lastModificationTime");
				approval_execution_tableDTO.organogramID = rs.getLong("organogram_id");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return approval_execution_tableDTO;
	}
	

	
	
	
	public long update(CommonDTO commonDTO) throws Exception
	{		
		Approval_execution_tableDTO approval_execution_tableDTO = (Approval_execution_tableDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "table_name=?";
			sql += ", ";
			sql += "previous_row_id=?";
			sql += ", ";
			sql += "updated_row_id=?";
			sql += ", ";
			sql += "operation=?";
			sql += ", ";
			sql += "action=?";
			sql += ", ";
			sql += "approval_status_cat=?";
			sql += ", ";
			sql += "user_id=?";
			sql += ", ";
			sql += "user_ip=?";
			sql += ", ";
			sql += "approval_path_id=?";
			sql += ", ";
			sql += "approval_path_order=?";
			sql += ", ";
			sql += "remarks=?";
			sql += ", ";
			sql += "organogram_id=?";
			sql += ", ";
			sql += "file_dropzone=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + approval_execution_tableDTO.iD;
				

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,approval_execution_tableDTO.tableName);
			ps.setObject(index++,approval_execution_tableDTO.previousRowId);
			ps.setObject(index++,approval_execution_tableDTO.updatedRowId);
			ps.setObject(index++,approval_execution_tableDTO.operation);
			ps.setObject(index++,approval_execution_tableDTO.action);
			ps.setObject(index++,approval_execution_tableDTO.approvalStatusCat);
			ps.setObject(index++,approval_execution_tableDTO.userId);
			ps.setObject(index++,approval_execution_tableDTO.userIp);
			ps.setObject(index++,approval_execution_tableDTO.approvalPathId);
			ps.setObject(index++,approval_execution_tableDTO.approvalPathOrder);
			ps.setObject(index++,approval_execution_tableDTO.remarks);
			ps.setObject(index++,approval_execution_tableDTO.organogramID);
			ps.setObject(index++,approval_execution_tableDTO.fileDropzone);
			logger.debug(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return approval_execution_tableDTO.iD;
	}
	
	
	public List<Approval_execution_tableDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Approval_execution_tableDTO approval_execution_tableDTO = null;
		List<Approval_execution_tableDTO> approval_execution_tableDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return approval_execution_tableDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				approval_execution_tableDTO = new Approval_execution_tableDTO();
				approval_execution_tableDTO.iD = rs.getLong("ID");
				approval_execution_tableDTO.tableName = rs.getString("table_name");
				approval_execution_tableDTO.previousRowId = rs.getLong("previous_row_id");
				approval_execution_tableDTO.updatedRowId = rs.getLong("updated_row_id");
				approval_execution_tableDTO.operation = rs.getInt("operation");
				approval_execution_tableDTO.action = rs.getInt("action");
				approval_execution_tableDTO.approvalStatusCat = rs.getInt("approval_status_cat");
				approval_execution_tableDTO.userId = rs.getLong("user_id");
				approval_execution_tableDTO.userIp = rs.getString("user_ip");
				approval_execution_tableDTO.approvalPathId = rs.getLong("approval_path_id");
				approval_execution_tableDTO.approvalPathOrder = rs.getInt("approval_path_order");
				approval_execution_tableDTO.remarks = rs.getString("remarks");
				approval_execution_tableDTO.fileDropzone = rs.getLong("file_dropzone");
				approval_execution_tableDTO.isDeleted = rs.getInt("isDeleted");
				approval_execution_tableDTO.lastModificationTime = rs.getLong("lastModificationTime");
				approval_execution_tableDTO.organogramID = rs.getLong("organogram_id");
				logger.debug("got this DTO: " + approval_execution_tableDTO);
				
				approval_execution_tableDTOList.add(approval_execution_tableDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return approval_execution_tableDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Approval_execution_tableDTO> getAllApproval_execution_table (boolean isFirstReload)
    {
		List<Approval_execution_tableDTO> approval_execution_tableDTOList = new ArrayList<>();

		String sql = "SELECT * FROM approval_execution_table";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by approval_execution_table.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Approval_execution_tableDTO approval_execution_tableDTO = new Approval_execution_tableDTO();
				approval_execution_tableDTO.iD = rs.getLong("ID");
				approval_execution_tableDTO.tableName = rs.getString("table_name");
				approval_execution_tableDTO.previousRowId = rs.getLong("previous_row_id");
				approval_execution_tableDTO.updatedRowId = rs.getLong("updated_row_id");
				approval_execution_tableDTO.operation = rs.getInt("operation");
				approval_execution_tableDTO.action = rs.getInt("action");
				approval_execution_tableDTO.approvalStatusCat = rs.getInt("approval_status_cat");
				approval_execution_tableDTO.userId = rs.getLong("user_id");
				approval_execution_tableDTO.userIp = rs.getString("user_ip");
				approval_execution_tableDTO.approvalPathId = rs.getLong("approval_path_id");
				approval_execution_tableDTO.approvalPathOrder = rs.getInt("approval_path_order");
				approval_execution_tableDTO.remarks = rs.getString("remarks");
				approval_execution_tableDTO.fileDropzone = rs.getLong("file_dropzone");
				approval_execution_tableDTO.isDeleted = rs.getInt("isDeleted");
				approval_execution_tableDTO.lastModificationTime = rs.getLong("lastModificationTime");
				approval_execution_tableDTO.organogramID = rs.getLong("organogram_id");
				
				approval_execution_tableDTOList.add(approval_execution_tableDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return approval_execution_tableDTOList;
    }
	
	public List<Approval_execution_tableDTO> getDtosByTableNameAndUpdatedRowId (String tableName, long updatedRowId)
    {
		List<Approval_execution_tableDTO> approval_execution_tableDTOList = new ArrayList<>();

		String sql = "SELECT \r\n" + 
				"    *\r\n" + 
				"FROM\r\n" + 
				"    approval_execution_table\r\n" + 
				"WHERE\r\n" + 
				"    table_name = '" + tableName  + "'\r\n" + 
				"        AND previous_row_id = (SELECT \r\n" + 
				"            previous_row_id\r\n" + 
				"        FROM\r\n" + 
				"            approval_execution_table\r\n" + 
				"        WHERE\r\n" + 
				"            table_name = '" + tableName + "'\r\n" + 
				"                AND updated_row_id = " + updatedRowId + "\r\n" + 
				"        LIMIT 1)\r\n" + 
				"ORDER BY id DESC;";


		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Approval_execution_tableDTO approval_execution_tableDTO = new Approval_execution_tableDTO();
				approval_execution_tableDTO.iD = rs.getLong("ID");
				approval_execution_tableDTO.tableName = rs.getString("table_name");
				approval_execution_tableDTO.previousRowId = rs.getLong("previous_row_id");
				approval_execution_tableDTO.updatedRowId = rs.getLong("updated_row_id");
				approval_execution_tableDTO.operation = rs.getInt("operation");
				approval_execution_tableDTO.action = rs.getInt("action");
				approval_execution_tableDTO.approvalStatusCat = rs.getInt("approval_status_cat");
				approval_execution_tableDTO.userId = rs.getLong("user_id");
				approval_execution_tableDTO.userIp = rs.getString("user_ip");
				approval_execution_tableDTO.approvalPathId = rs.getLong("approval_path_id");
				approval_execution_tableDTO.approvalPathOrder = rs.getInt("approval_path_order");
				approval_execution_tableDTO.remarks = rs.getString("remarks");
				approval_execution_tableDTO.fileDropzone = rs.getLong("file_dropzone");
				approval_execution_tableDTO.isDeleted = rs.getInt("isDeleted");
				approval_execution_tableDTO.lastModificationTime = rs.getLong("lastModificationTime");
				approval_execution_tableDTO.organogramID = rs.getLong("organogram_id");
				
				approval_execution_tableDTOList.add(approval_execution_tableDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return approval_execution_tableDTOList;
    }
	
	public List<Approval_execution_tableDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Approval_execution_tableDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Approval_execution_tableDTO> approval_execution_tableDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Approval_execution_tableDTO approval_execution_tableDTO = new Approval_execution_tableDTO();
				approval_execution_tableDTO.iD = rs.getLong("ID");
				approval_execution_tableDTO.tableName = rs.getString("table_name");
				approval_execution_tableDTO.previousRowId = rs.getLong("previous_row_id");
				approval_execution_tableDTO.updatedRowId = rs.getLong("updated_row_id");
				approval_execution_tableDTO.operation = rs.getInt("operation");
				approval_execution_tableDTO.action = rs.getInt("action");
				approval_execution_tableDTO.approvalStatusCat = rs.getInt("approval_status_cat");
				approval_execution_tableDTO.userId = rs.getLong("user_id");
				approval_execution_tableDTO.userIp = rs.getString("user_ip");
				approval_execution_tableDTO.approvalPathId = rs.getLong("approval_path_id");
				approval_execution_tableDTO.approvalPathOrder = rs.getInt("approval_path_order");
				approval_execution_tableDTO.remarks = rs.getString("remarks");
				approval_execution_tableDTO.fileDropzone = rs.getLong("file_dropzone");
				approval_execution_tableDTO.isDeleted = rs.getInt("isDeleted");
				approval_execution_tableDTO.lastModificationTime = rs.getLong("lastModificationTime");
				approval_execution_tableDTO.organogramID = rs.getLong("organogram_id");
				
				approval_execution_tableDTOList.add(approval_execution_tableDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return approval_execution_tableDTOList;
	
	}
				
}
	