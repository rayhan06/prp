package common;

/*
 * @author Md. Erfan Hossain
 * @created 30/03/2021 - 3:15 PM
 * @project parliament
 */

public enum RoleEnum {

    EMPLOYEE_OF_PARLIAMENT(10401),
    STORE_KEEPER(11501),
    HR_ADMIN(10402),
    DATA_ENTRY(11302),
    ADMIN(1),
    TRAINING(11906),
    MP(10201)
    ;

    private final int roleId;

    RoleEnum(int value) {
        roleId = value;
    }

    public int getRoleId(){
        return roleId;
    }
}
