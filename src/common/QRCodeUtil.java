package common;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Hashtable;

public class QRCodeUtil {
    private static final int BLACK = 0xFF000000;
    private static final int WHITE = 0xFFFFFFFF;

    private QRCodeUtil() {
    }


    /**
     * Generate QR code
     *
     * @param content
     * @return
     * @throws Exception
     */
    public static InputStream createQrcodeImage(String content) throws Exception {

        int width = 200; // QR code image width
        int height = 200; // QR code image height
        String format = "jpg"; // The image format of the QR code
        Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
        hints.put (EncodeHintType.CHARACTER_SET, "utf-8"); // character set encoding used for content
        BitMatrix bitMatrix = new MultiFormatWriter().encode(content,
                BarcodeFormat.QR_CODE, width, height, hints);
        BufferedImage image= QRCodeUtil.toBufferedImage(bitMatrix);
        InputStream is=QRCodeUtil.toInputStream(image);
        return is;

    }

    /**
     * @Description: Convert to base64 encoded string according to the picture address
     * @Author:
     * @CreateTime:
     * @return
     */
    public static String getImageStr(InputStream inputStream) {
        byte[] data = null;
        try {
            data = new byte[inputStream.available()];
            inputStream.read(data);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // encryption
        BASE64Encoder encoder = new BASE64Encoder();
        return "data:image/jpeg;base64," + encoder.encode( data ).replace( "\r\n", "" );
    }

    public static BufferedImage toBufferedImage(BitMatrix matrix) {
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        BufferedImage image = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                image.setRGB(x, y, matrix.get(x, y) ? BLACK : WHITE);
            }
        }
        return image;
    }

    public static InputStream toInputStream(BufferedImage image) throws Exception {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", os);
        InputStream is = new ByteArrayInputStream(os.toByteArray());
        return is;
    }

    public static void writeToFile(BitMatrix matrix, String format, File file)
            throws IOException {
        BufferedImage image = toBufferedImage(matrix);
        if (!ImageIO.write(image, format, file)) {
            throw new IOException("Could not write an image of format "
                    + format + " to " + file);
        }
    }

    public static void writeToStream(BitMatrix matrix, String format,
                                     OutputStream stream) throws IOException {
        BufferedImage image = toBufferedImage(matrix);
        if (!ImageIO.write(image, format, stream)) {
            throw new IOException("Could not write an image of format " + format);
        }
    }   
    public static String getBarCodeAsBase64( String text ){

        try{

            int width = 500; // QR code image width
            int height = 100; // QR code image height
            String format = "jpg"; // The image format of the QR code

            Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
            hints.put (EncodeHintType.CHARACTER_SET, "utf-8"); // character set encoding used for content

            BitMatrix bitMatrix = new MultiFormatWriter().encode(text, BarcodeFormat.CODABAR, width, height, hints);
            BufferedImage image= QRCodeUtil.toBufferedImage(bitMatrix);
            InputStream is=QRCodeUtil.toInputStream(image);

            String data = QRCodeUtil.getImageStr( is );
            return data;

        } catch (Exception e) {

            System.out.println(e);
        }

        return "";
    }


    public static String getQrCodeAsBase64( String text ){

        try{

            int width = 200; // QR code image width
            int height = 200; // QR code image height
            String format = "jpg"; // The image format of the QR code

            Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
            hints.put (EncodeHintType.CHARACTER_SET, "utf-8"); // character set encoding used for content

            BitMatrix bitMatrix = new MultiFormatWriter().encode(text, BarcodeFormat.QR_CODE, width, height, hints);
            BufferedImage image= QRCodeUtil.toBufferedImage(bitMatrix);
            InputStream is=QRCodeUtil.toInputStream(image);

            String data = QRCodeUtil.getImageStr( is );
            return data;

        } catch (Exception e) {

            System.out.println(e);
        }

        return "";
    }

    public static String getCode128ImageAsBase64( String text ){

        try{

            int width = 200; // QR code image width
            int height = 200; // QR code image height
            String format = "jpg"; // The image format of the QR code

            Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
            hints.put (EncodeHintType.CHARACTER_SET, "utf-8"); // character set encoding used for content

            BitMatrix bitMatrix = new MultiFormatWriter().encode(text, BarcodeFormat.CODE_128, width, height, hints);
            BufferedImage image= QRCodeUtil.toBufferedImage(bitMatrix);
            InputStream is=QRCodeUtil.toInputStream(image);

            String data = QRCodeUtil.getImageStr( is );
            return data;

        } catch (Exception e) {

            System.out.println(e);
        }

        return "";
    }

    public static void main(String[] args) throws Exception {
        try{
            String text = "http://www.baidu.com"; // QR code content
            int width = 200; // QR code image width
            int height = 200; // QR code image height
            String format = "jpg"; // The image format of the QR code

            Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
            hints.put (EncodeHintType.CHARACTER_SET, "utf-8"); // character set encoding used for content

            BitMatrix bitMatrix = new MultiFormatWriter().encode(text, BarcodeFormat.QR_CODE, width, height, hints);
            BufferedImage image= QRCodeUtil.toBufferedImage(bitMatrix);
            InputStream is=QRCodeUtil.toInputStream(image);

            String data = QRCodeUtil.getImageStr( is );

        } catch (Exception e) {
            System.out.println ("Exception ==" + e.getMessage ());
            e.printStackTrace();
        }
    }
}
