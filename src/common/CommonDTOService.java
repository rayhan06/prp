package common;

import audit.log.AuditLogDAO;
import audit.log.LogEvent;
import dbm.DBMW;
import org.apache.log4j.Logger;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.NavigationService4;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Deprecated
@SuppressWarnings({"rawtypes", "unchecked", "unused", "Duplicates"})
public interface CommonDTOService<T extends CommonDTO> extends VbSequencerService {
    Logger logger = Logger.getLogger(CommonDTOService.class);

    ThreadLocal<Connection> CONNECTION_THREAD_LOCAL = new ThreadLocal<>();

    ThreadLocal<CacheUpdateModel> CACHE_UPDATE_MODEL_THREAD_LOCAL = new ThreadLocal<>();

    String getByIDQuery = "SELECT * FROM %s WHERE ID= %d AND isDeleted = 0";

    String getById = "SELECT * FROM %s WHERE ID= %d";

    String getAllDTOsForTrue = "SELECT * FROM %s order by lastModificationTime desc";

    String getAllDTOsForFalse = "SELECT * FROM %s WHERE lastModificationTime >= %d order by lastModificationTime desc";

    String getDTOsByIds = "SELECT * FROM {tableName} WHERE ID IN ( %s )  order by lastModificationTime desc";

    String getDTOsByIdsNotSorted = "SELECT * FROM {tableName} WHERE ID IN ( %s ) ";

    String deleteByIdSQLQuery = "UPDATE {tableName} SET isDeleted = 1,lastModificationTime = %d,modified_by = %d WHERE id = %d";

    String deleteByIdSQLQuery2 = "UPDATE {tableName} SET isDeleted = 1,lastModificationTime = %d WHERE id = %d";

    void set(PreparedStatement ps, T t, boolean isInsert) throws SQLException;

    T buildObjectFromResultSet(ResultSet rs);

    String getTableName();

    default T getDTOFromID(long ID) {
        String sql = String.format(getByIDQuery, getTableName(), ID);
        List<T> list = getDTOs(sql);
        return list.size() == 0 ? null : list.get(0);
    }

    default T getDTOFromIdDeletedOrNot(long ID) {
        String sql = String.format(getById, getTableName(), ID);
        List<T> list = getDTOs(sql);
        return list.size() == 0 ? null : list.get(0);
    }

    default List<T> getDTOs(List<Long> ids) {
        if (ids.isEmpty()) {
            return new ArrayList<>();
        }
        String sql = getDTOsByIds.replace("{tableName}", getTableName());
        return ConnectionAndStatementUtil.getDTOListByNumbers(sql, ids, this::buildObjectFromResultSet);
    }

    default List<T> getDTOsNotSorted(List<Long> ids) {
        if (ids.isEmpty()) {
            return new ArrayList<>();
        }
        String sql = getDTOsByIdsNotSorted.replace("{tableName}", getTableName());
        return ConnectionAndStatementUtil.getDTOListByNumbers(sql, ids, this::buildObjectFromResultSet);
    }

    default List<T> getAllDTOs(boolean isFirstReload) {
        return getAllDTOs(isFirstReload, RepositoryManager.lastModifyTime);
    }

    default List<T> getAllDTOs(boolean isFirstReload, long lastModifyTime) {
        String sql;
        if (isFirstReload) {
            sql = String.format(getAllDTOsForTrue, getTableName());
        } else {
            sql = String.format(getAllDTOsForFalse, getTableName(), lastModifyTime);
        }
        return getDTOs(sql);
    }

    default List<T> getDTOs(String sql) {
        return getDTOs(sql, this::buildObjectFromResultSet);
    }

    default List<T> getDTOs(String sql, List<Object> objectList) {
        return getDTOs(sql, objectList, this::buildObjectFromResultSet);
    }

    default <TT> List<TT> getDTOs(String sql, Function<ResultSet, TT> func) {
        return ConnectionAndStatementUtil.getListOfT(sql, func);
    }

    default <TT> List<TT> getDTOs(String sql, List<Object> objectList, Function<ResultSet, TT> func) {
        return ConnectionAndStatementUtil.getListOfT(sql, objectList, func);
    }

    default <TT> List<TT> getDTOs(String sql, Consumer<PreparedStatement> consumer, Function<ResultSet, TT> func) {
        return ConnectionAndStatementUtil.getListOfT(sql, consumer, func);
    }

    default List<T> getDTOs(String sql, Consumer<PreparedStatement> consumer) {
        return ConnectionAndStatementUtil.getListOfT(sql, consumer, this::buildObjectFromResultSet);
    }

    default long executeAddOrUpdateQuery(T t, String sql, boolean isInsert) throws Exception {
        T oldT = null;
        if (!isInsert) {
            oldT = getDTOFromID(t.iD);
        }
        return executeAddOrUpdateQuery(t, sql, isInsert, oldT);
    }

    default long executeAddOrUpdateQuery(T t, String sql, boolean isInsert, boolean doAudit) throws Exception {
        T oldT = null;
        if (!isInsert) {
            oldT = getDTOFromID(t.iD);
        }
        return executeAddOrUpdateQuery(t, sql, isInsert, oldT, doAudit);
    }

    default long executeAddOrUpdateQuery(T t, String sql, boolean isInsert, T oldT) throws Exception {
        return executeAddOrUpdateQuery(t, sql, isInsert, oldT, true);
    }

    default long executeAddOrUpdateQuery(T t, String sql, boolean isInsert, T oldT, boolean doAudit) throws Exception {
        if (CONNECTION_THREAD_LOCAL.get() != null) {
            return executeAddOrUpdateQuery(t, sql, isInsert, oldT, CONNECTION_THREAD_LOCAL.get(), doAudit);
        } else {
            Utils.handleTransactionForCommonDTOService(() -> {
                CACHE_UPDATE_MODEL_THREAD_LOCAL.set(new CacheUpdateModel(t.lastModificationTime == 0 ? System.currentTimeMillis() : t.lastModificationTime));
                executeAddOrUpdateQuery(t, sql, isInsert, oldT, CONNECTION_THREAD_LOCAL.get(), doAudit);
                Utils.executeCacheAsyncForCommonDTOService();
            });
            return t.iD;
        }
    }

    default long executeAddOrUpdateQuery(T t, String sql, boolean isInsert, T oldT, Connection connection, boolean doAudit) throws Exception {
        if (isInsert && t.iD <= 0) {
            t.iD = DBMW.getInstance().getNextSequenceId(getTableName());
        }
        sql = sql.replace("{tableName}", getTableName());
        AtomicReference<Exception> atomicReference = new AtomicReference<>();
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                set(ps, t, isInsert);
                logger.debug(ps);
                if (isInsert) {
                    ps.execute();
                } else {
                    ps.executeUpdate();
                }
                if (doAudit) {
                    AuditLogDAO.add(oldT, t, isInsert ? LogEvent.ADD : LogEvent.UPDATE, getTableName(), HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().ipAddress,
                            HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.ID, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.organogramID);
                }
                Repository repository = RepositoryManager.getInstance().getRepository(getTableName());
                if (repository != null) {
                    if (t.lastModificationTime == 0) {
                        recordUpdateTime(connection, getTableName(), System.currentTimeMillis());
                    } else {
                        recordUpdateTime(connection, getTableName(), t.lastModificationTime);
                    }

                    if (CACHE_UPDATE_MODEL_THREAD_LOCAL.get() != null) {
                        CACHE_UPDATE_MODEL_THREAD_LOCAL.get().repositoryList.add(repository);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
                atomicReference.set(e);
            }
        }, connection, sql);
        if (atomicReference.get() != null) {
            throw atomicReference.get();
        }
        return t.iD;
    }

    default String getSearchQuery(String tableName, Hashtable p_searchCriteria, int limit, int offset, int category, Map<String, String> searchMap, String tableJoinClause, String whereClause, List<Object> objectList) {
        return getSearchQuery(tableName, p_searchCriteria, limit, offset, category, searchMap, tableJoinClause, whereClause, null, objectList);
    }

    default String getSearchQuery(String tableName, Hashtable p_searchCriteria, int limit, int offset,
                                  int category, Map<String, String> searchMap, String tableJoinClause, String whereClause, String sortingClause, List<Object> objectList) {
        StringBuilder sqlBuilder = new StringBuilder();
        boolean isNotCountQuery = true;
        switch (category) {
            case NavigationService4.GETIDS:
                sqlBuilder.append("SELECT ").append(tableName).append(".ID FROM ").append(tableName).append(" ");
                break;
            case NavigationService4.GETDTOS:
                sqlBuilder.append("SELECT * FROM ").append(tableName).append(" ");
                break;
            case NavigationService4.GETCOUNT:
                sqlBuilder.append("SELECT count(*) ").append(" as countID FROM  ").append(tableName).append(" ");
                isNotCountQuery = false;
                break;
            default:
                return null;
        }
        if (tableJoinClause != null && tableJoinClause.trim().length() > 0) {
            sqlBuilder.append(tableJoinClause);
        }
        sqlBuilder.append(" WHERE (").append(tableName).append(".isDeleted = 0)");
        if (p_searchCriteria != null && p_searchCriteria.size() > 0 && searchMap != null && searchMap.size() > 0) {
            Map<String, String> map = new HashMap<>(p_searchCriteria);
            String searchString = map.entrySet()
                    .stream()
                    .filter(entry -> entry.getValue() != null && entry.getValue().trim().length() > 0 && searchMap.get(entry.getKey()) != null)
                    .map(entry -> {
                        if (objectList == null) {
                            return searchMap.get(entry.getKey()).replace("{}", entry.getValue());
                        } else {
                            String[] tokens = searchMap.get(entry.getKey()).split(" ");
                            List<String> ls = Stream.of(tokens)
                                    .filter(e -> e.length() > 0)
                                    .collect(Collectors.toList());
                            for (int i = 1; i < ls.size(); i++) {
                                if (ls.get(i).startsWith("?")) {
                                    switch (ls.get(i - 1).toUpperCase()) {
                                        case "LIKE":
                                            objectList.add("%" + entry.getValue() + "%");
                                            break;
                                        case "IN":
                                            String[] questions = entry.getValue().split(",");
                                            String quesStr = Stream.generate(() -> "?")
                                                    .limit(questions.length)
                                                    .collect(Collectors.joining(",", "(", ")"));
                                            objectList.addAll(Arrays.asList(questions));
                                            return searchMap.get(entry.getKey()).replace("?", quesStr);
                                        default:
                                            objectList.add(entry.getValue());
                                    }
                                }
                            }
                            return searchMap.get(entry.getKey());
                        }
                    })
                    .collect(Collectors.joining(" "));
            if (searchString.trim().length() > 0) {
                sqlBuilder.append(searchString);
            }

        }
        if (whereClause != null && whereClause.trim().length() > 0) {
            sqlBuilder.append(whereClause).append(" ");
        }
        if (isNotCountQuery) {
            if (sortingClause == null || sortingClause.trim().length() == 0) {
                sqlBuilder.append(" order by ").append(tableName).append(".lastModificationTime desc");
            } else {
                sqlBuilder.append(" order by ").append(sortingClause);
            }
            if (limit >= 0) {
                sqlBuilder.append(" limit ").append(limit);
            }
            if (offset >= 0) {
                sqlBuilder.append(" offset ").append(offset);
            }
        }

        logger.debug("-------------->> sql = " + sqlBuilder);
        return sqlBuilder.toString();
    }

    default String getSearchQuery(String tableName, Hashtable p_searchCriteria, int limit, int offset, int category, Map<String, String> searchMap, List<Object> objectList) {
        return getSearchQuery(tableName, p_searchCriteria, limit, offset, category, searchMap, null, null, objectList);
    }

    default boolean deleteById(long id, long modificationTime, long modifiedBy) {
        String sql = String.format(deleteByIdSQLQuery, modificationTime, modifiedBy, id);
        sql = sql.replace("{tableName}", getTableName());
        return delete(sql, modificationTime);
    }

    default boolean deleteById2(long id, long modificationTime) {
        String sql = String.format(deleteByIdSQLQuery2, modificationTime, id);
        sql = sql.replace("{tableName}", getTableName());
        return delete(sql, modificationTime);
    }

    default boolean delete(String sql, long modificationTime) {
        return (Boolean) ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {

            try {
                model.getStatement().executeUpdate(sql);
                recordUpdateTime(model.getConnection(), getTableName(), modificationTime);
                return true;
            } catch (SQLException ex) {
                ex.printStackTrace();
                return false;
            }
        });
    }

    default boolean deleteById(long id, long modifiedBy) {
        return deleteById(id, System.currentTimeMillis(), modifiedBy);
    }

    default boolean deleteByIdAndLastModificationTime(long id, long modificationTime) {
        return deleteById(id, modificationTime, 0);
    }

    default boolean deleteById(long id) {
        return deleteById2(id, System.currentTimeMillis());
    }
}
