package common;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.log4j.Logger;

import annotation.Transactional;
import dbm.*;
//import util.DatabaseConnectionFactory;
import util.FormPopulator;
import util.TransactionType;

public class UniversalDTOService {
	Logger logger = Logger.getLogger(getClass());	
	private String getTableName(Class<?> clazz) throws Exception{
		return clazz.getSimpleName();
	}
	
	@Transactional(transactionType=util.TransactionType.READONLY)
	public <T> T get(Class<T> clazz) throws Exception{
		
		Connection connection = null;
		Statement stmt = null;
		
		T returnObject = clazz.newInstance();
		
		try
		{
			
			connection = DBMR.getInstance().getConnection();
			stmt  = connection.createStatement();
		
		String sql = "select columnName,value from at_universal_table where tableName = '"+getTableName(clazz)+"'";
		ResultSet resultSet =stmt.executeQuery(sql);
		
		
		while(resultSet.next()){
			FormPopulator.populate(returnObject, resultSet.getString("columnName"), resultSet.getString("value"));
		}
		
		}
		catch(Exception ex){
			logger.fatal("",ex);
			throw ex;
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
						
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		
		return returnObject;
	}
	@Transactional(transactionType=TransactionType.INDIVIDUAL_TRANSACTION)
	public void update(Object object) throws Exception{
		
		Connection connection = null;
		Statement stmt = null;
		try
		{
			connection = DBMW.getInstance().getConnection();
			stmt  = connection.createStatement();
		String tableName = getTableName(object.getClass());
		for(Field field: object.getClass().getDeclaredFields()){
			field.setAccessible(true);
			String sql = "update at_universal_table set value='"+field.get(object)+"' where tableName = '"+tableName+"' and columnName = '"+field.getName()+"'";
			stmt.executeUpdate(sql);
		}
		
		}
		catch(Exception ex){
			logger.fatal("",ex);
			throw ex;
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
						
			try{ 
				if (connection != null){ 
					DBMW.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
	}
}
