package common;

import util.CommonDTO;

public class NameDTO extends CommonDTO {
    public String nameEn = "";
    public String nameBn = "";
    public long insertedBy = 0;
    public long modifiedBy = 0;
    public long insertionDate = 0;

    public String getText(String language){
        return language.equalsIgnoreCase("english") ? nameEn : nameBn;
    }

    @Override
    public String toString() {
        return "NameDTO{" +
                "nameEn='" + nameEn + '\'' +
                ", nameBn='" + nameBn + '\'' +
                ", insertBy=" + insertedBy +
                ", modifiedBy=" + modifiedBy +
                ", insertionTime=" + insertionDate +
                ", iD=" + iD +
                '}';
    }
}
