package common;

import util.CommonDTO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings({"unused","Duplicates"})
public interface EmployeeCommonDAOService<T extends CommonDTO> extends CommonDAOService<T> {
    String getByEmployeeId = "SELECT * FROM %s WHERE employee_records_id = %d AND isDeleted = 0 ORDER BY lastModificationTime DESC";

    String getGetByEmployeeIdAndId = "SELECT * FROM %s WHERE employee_records_id = %d AND ID = %d  AND isDeleted = 0";

    String getIdsByEmployeeIdAndIds = "SELECT * FROM {tableName} WHERE employee_records_id = {empId} AND ID IN (%s) AND isDeleted = 0";

    String getByEmployeeIdDeletedOrNot = "SELECT * FROM %s WHERE employee_records_id = %d ORDER BY lastModificationTime DESC";

    default List<T> getByEmployeeId(long employeeId) {
        return getDTOs(String.format(getByEmployeeId, getTableName(), employeeId));
    }

    default List<T> getByEmployeeIdDeletedOrNot(long employeeId) {
        return getDTOs(String.format(getByEmployeeIdDeletedOrNot, getTableName(), employeeId));
    }

    default T getByEmployeeIdAndId(long employeeId, long id) {
        String sql = String.format(getGetByEmployeeIdAndId, getTableName(), employeeId, id);
        return ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
    }

    default List<Long> getIdsByEmployeeIdAndIds(long employeeId, String[] idArrays) {
        if (idArrays == null || idArrays.length == 0) {
            return new ArrayList<>();
        }
        String sql = getIdsByEmployeeIdAndIds.replace("{tableName}", getTableName()).replace("{empId}", String.valueOf(employeeId));
        List<String> idList = Stream.of(idArrays)
                .collect(Collectors.toList());
        return ConnectionAndStatementUtil.getDTOList(sql, idList, rs -> {
            try {
                return rs.getLong("ID");
            } catch (SQLException ex) {
                loggerCommonDAOService.error("",ex);
                return null;
            }
        });
    }

    default <R> List<R> getModelList(long employeeId, Function<T, R> transform) {
        return getByEmployeeId(employeeId)
                .stream()
                .map(transform)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
