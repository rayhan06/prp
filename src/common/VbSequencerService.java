package common;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.function.Consumer;

class VbSequencerUpdate{
    String tableName;
    long lastModificationTime;
    PreparedStatement ps;
    VbSequencerUpdate(String tableName,long lastModificationTime,PreparedStatement ps){
        this.tableName = tableName;
        this.lastModificationTime = lastModificationTime;
        this.ps = ps;
    }
}
public interface VbSequencerService {
    String updateLastModificationTime = "UPDATE vbSequencer SET table_LastModificationTime=? WHERE table_name=?";
    Logger loggerVbSequencerService = Logger.getLogger(VbSequencerService.class);

    Consumer<VbSequencerUpdate> consumerPreparedStatement = (vbSequencerUpdate)->{
        try {
            vbSequencerUpdate.ps.setLong(1, vbSequencerUpdate.lastModificationTime);
            vbSequencerUpdate.ps.setString(2, vbSequencerUpdate.tableName);
            loggerVbSequencerService.debug(vbSequencerUpdate.ps);
            vbSequencerUpdate.ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            loggerVbSequencerService.error(e);
        }
    };

    default void recordUpdateTime(Connection connection, String tableName){
        ConnectionAndStatementUtil.getWritePrepareStatement(ps->{
            consumerPreparedStatement.accept(new VbSequencerUpdate(tableName,System.currentTimeMillis(),ps));
        }, connection, updateLastModificationTime);
    }

    default void recordUpdateTime(String tableName){
        ConnectionAndStatementUtil.getWritePrepareStatement(ps->{
            consumerPreparedStatement.accept(new VbSequencerUpdate(tableName,System.currentTimeMillis(),ps));
        }, updateLastModificationTime);
    }

    default void recordUpdateTime(Connection connection, String tableName,long lastModificationTime){
        ConnectionAndStatementUtil.getWritePrepareStatement(ps->{
            consumerPreparedStatement.accept(new VbSequencerUpdate(tableName,lastModificationTime,ps));
        }, connection, updateLastModificationTime);
    }

    default void recordUpdateTime(String tableName,long lastModificationTime){
        ConnectionAndStatementUtil.getWritePrepareStatement(ps->{
            consumerPreparedStatement.accept(new VbSequencerUpdate(tableName,lastModificationTime,ps));
        }, updateLastModificationTime);
    }

}
