package common;

import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused","Duplicates"})
public abstract class NameDao implements CommonDAOService<NameDTO>{
    private static final Logger logger = Logger.getLogger(NameDao.class);

    private static final String addQuery = "INSERT INTO {tableName} (name_en,name_bn,lastModificationTime,modified_by," +
            " inserted_by,insertion_date,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?)";

    private static final String updateQuery = "UPDATE {tableName} SET name_en=?,name_bn=?,lastModificationTime =?," +
            " modified_by = ? WHERE ID = ?";

    private final Map<String,String> searchMap = new HashMap<>();
    
    private final String tableName;

    public NameDao(String tableName) {
        this.tableName = tableName;
        searchMap.put("nameId"," AND (ID = ?)");
        searchMap.put("AnyField"," AND (name_en LIKE ? OR name_bn LIKE ?)");
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public String getTableName(){
        return tableName;
    }

    public void set(PreparedStatement ps, NameDTO dto, boolean isInsert) throws SQLException {
        long currentTime = System.currentTimeMillis();
        int index = 0;
        ps.setObject(++index, dto.nameEn);
        ps.setObject(++index, dto.nameBn);
        ps.setObject(++index, dto.lastModificationTime!=0 ? dto.lastModificationTime:currentTime);
        ps.setObject(++index, dto.modifiedBy);
        if (isInsert) {
            ps.setObject(++index, dto.insertedBy);
            ps.setObject(++index, dto.insertionDate!=0 ? dto.insertionDate:currentTime);
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, dto.iD);
    }

    public NameDTO buildObjectFromResultSet(ResultSet rs){
        try{
            NameDTO nameDTO = new NameDTO();
            nameDTO.iD = rs.getLong("ID");
            nameDTO.nameBn = rs.getString("name_bn");
            nameDTO.nameEn = rs.getString("name_en");
            nameDTO.isDeleted = rs.getInt("isDeleted");
            nameDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return nameDTO;
        }catch (SQLException ex){
            ex.printStackTrace();
            return null;
        }
    }

    
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((NameDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((NameDTO) commonDTO, updateQuery, false);
    }

    
    public CommonDTO getDTOByID(long ID) throws Exception {
        return getDTOFromID(ID);
    }

    public List<NameDTO> getAllDTO(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }
}