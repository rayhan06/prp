package common;

/*
 * @author Md. Erfan Hossain
 * @created 28/03/2021 - 4:51 PM
 * @project parliament
 */

import language.LM;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import user.UserDTO;
import util.CommonConstant;
import util.CommonDTO;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;

public interface NameInterface {

    int getSearchTitleValue();

    String get_p_navigatorName();

    String get_p_dtoCollectionName();

    NameRepository getNameRepository();

    int getAddTitleValue();

    int getEditTitleValue();

    default CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO, NameDao nameDao) throws Exception {
        boolean isLangEng = LM.getLanguageIDByUserDTO(userDTO) == CommonConstant.Language_ID_English;
        NameDTO nameDTO;
        long currentTime = System.currentTimeMillis();
        if (addFlag) {
            nameDTO = new NameDTO();
            nameDTO.insertedBy = userDTO.employee_record_id;
            nameDTO.insertionDate = currentTime;
        } else {
            nameDTO = nameDao.getDTOFromID(Long.parseLong(request.getParameter("id")));
            if(nameDTO == null){
                throw new Exception(isLangEng?"Information is not found":"তথ্য পাওয়া যায়নি");
            }
        }
        String nameEn = Jsoup.clean(request.getParameter("nameEn"), Whitelist.simpleText());
        if(nameEn == null || nameEn.trim().length() == 0){
            throw new Exception(isLangEng?"English name is not found":"ইংরেজি নামের তথ্য পাওয়া যায়নি");
        }
        String nameBn = Jsoup.clean(request.getParameter("nameBn"), Whitelist.simpleText());
        if(nameBn == null || nameBn.trim().length() == 0){
            throw new Exception(isLangEng?"Bangla name is not found":"বাংলা নামের তথ্য পাওয়া যায়নি");
        }
        nameDTO.nameEn = nameEn;
        nameDTO.nameBn = nameBn;
        nameDTO.modifiedBy = userDTO.employee_record_id;
        nameDTO.lastModificationTime = currentTime;
        if (addFlag) {
            nameDao.add(nameDTO);
        } else {
            nameDao.update(nameDTO);
        }
        return nameDTO;
    }

    default void setCommonAttributes(HttpServletRequest request,String servletName,NameDao nameDao){
        request.setAttribute("servletName",servletName);
        request.setAttribute("nameDao", nameDao);
        request.setAttribute("NAV_NAME",get_p_navigatorName());
        request.setAttribute("VIEW_NAME",get_p_dtoCollectionName());
        if (request.getParameter("actionType").equals("getAddPage")) {
            request.setAttribute("titleValue", getAddTitleValue());
        }else{
            request.setAttribute("titleValue", getEditTitleValue());
        }
        request.setAttribute("searchPageTitleValue",getSearchTitleValue());
        request.setAttribute("NameRepository",getNameRepository());
        request.setAttribute("navName","../common/nameNav.jsp");
        request.setAttribute("formName","../common/nameSearchForm.jsp");
    }
}