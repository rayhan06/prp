package common;

/*
 * @author Md. Erfan Hossain
 * @created 14/10/2021 - 10:49 PM
 * @project parliament
 */

@FunctionalInterface
public interface TransactionHandler {
    void transaction() throws Exception;
}
