package common;

/*
 * @author Md. Erfan Hossain
 * @created 09/07/2021 - 11:51 AM
 * @project parliament
 */

import annotation.*;
import com.google.gson.Gson;
import election_constituency.Election_constituencyRepository;
import election_details.Election_detailsDTO;
import election_details.Election_detailsRepository;
import files.FilesDAO;
import files.FilesDTO;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.Utils;
import political_party.Political_partyRepository;
import user.UserDTO;
import util.StringUtils;
import util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings({"unchecked", "unused", "Duplicates"})
public abstract class BaseServlet extends HttpServlet {
    public abstract String getTableName();

    public abstract String getServletName();

    public abstract CommonDAOService<? extends CommonDTO> getCommonDAOService();

    public abstract CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception;

    public abstract int[] getAddPageMenuConstants();

    public abstract int[] getEditPageMenuConstants();

    public abstract int[] getSearchMenuConstants();

    public abstract Class<? extends HttpServlet> getClazz();

    public final FilesDAO filesDAO = new FilesDAO();
    public final Gson gson = new Gson();
    protected final Logger logger = Logger.getLogger(getClazz());

    public static final String DTO_FOR_JSP = "baseServletCommonDTOForJsp";

    public static final String RECORD_NAVIGATOR = "recordNavigator";

    public static final String DELETED_ID_LIST = "__deleted_id_list__";

    public static final String DELETED_TIME = "__deleted_time__";

    public void init(HttpServletRequest request) throws ServletException {
    }

    public void finalize(HttpServletRequest request) {
    }

    public String commonPartOfDispatchURL() {
        return getTableName() + "/" + getTableName();
    }

    public String getAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return request.getContextPath() + "/" + getServletName() + "?actionType=search";
    }

    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getServletName() + "?actionType=search";
    }

    public String getEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return request.getContextPath() + "/" + getServletName() + "?actionType=search";
    }

    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getServletName() + "?actionType=search";
    }

    public String getWhereClause(HttpServletRequest request) {
        return null;
    }

    public String getTableJoinClause(HttpServletRequest request) {
        return null;
    }

    public String getSortClause(HttpServletRequest request) {
        return null;
    }

    public String defaultNonAjaxSearchPageURL() {
        return "pb/search.jsp";
    }

    public String getDeleteRedirectURL(HttpServletRequest request) {
        return getServletName() + "?actionType=search";
    }

    public boolean getAddPagePermission(HttpServletRequest request) {
        return true;
    }

    public boolean getEditPagePermission(HttpServletRequest request) {
        return true;
    }

    public boolean getViewPagePermission(HttpServletRequest request) {
        return true;
    }

    public boolean getSearchPagePermission(HttpServletRequest request) {
        return true;
    }

    public boolean getAddPermission(HttpServletRequest request) {
        return true;
    }

    public boolean getEditPermission(HttpServletRequest request) {
        return true;
    }

    public boolean getDeletePermission(HttpServletRequest request) {
        return true;
    }

    public int[] getViewMenuConstants() {
        return getSearchMenuConstants();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        init(request);
        try {
            switch (request.getParameter("actionType")) {
                case "getAddPage":
                    if (Utils.checkPermission(commonLoginData.userDTO, getAddPageMenuConstants()) && getAddPagePermission(request)) {
                        finalize(request);
                        request.getRequestDispatcher(commonPartOfDispatchURL() + "Edit.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "getEditPage":
                    if (Utils.checkPermission(commonLoginData.userDTO, getEditPageMenuConstants()) && getEditPagePermission(request)) {
                        setDTOForJsp(request);
                        finalize(request);
                        request.getRequestDispatcher(commonPartOfDispatchURL() + "Edit.jsp?actionType=edit").forward(request, response);
                        return;
                    }
                    break;
                case "downloadDropzoneFile": {
                    long id = Long.parseLong(request.getParameter("id"));
                    FilesDTO filesDTO = (FilesDTO) filesDAO.getDTOByID(id);
                    Utils.ProcessFile(request, response, filesDTO.fileTitle, filesDTO.inputStream);
                    return;
                }
                case "DeleteFileFromDropZone": {
                    long id = Long.parseLong(request.getParameter("id"));
                    logger.debug("In delete file");
                    filesDAO.hardDeleteByID(id);
                    response.getWriter().write("Deleted");
                    return;
                }
                case "getURL":
                    String URL = request.getParameter("URL");
                    response.sendRedirect(URL);
                    return;
                case "search":
                    if (Utils.checkPermission(commonLoginData.userDTO, getSearchMenuConstants()) && getSearchPagePermission(request)) {
                        search(request, response);
                        return;
                    }
                    break;
                case "view":
                    logger.debug("view requested");
                    if (Utils.checkPermission(commonLoginData.userDTO, getViewMenuConstants()) && getViewPagePermission(request)) {
                        setDTOForJsp(request);
                        finalize(request);
                        request.getRequestDispatcher(commonPartOfDispatchURL() + "View.jsp?ID=" + getId(request)).forward(request, response);
                        return;
                    }
                    break;
            }
        } catch (Exception ex) {
            logger.error("Exception : ",ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        init(request);
        try {
            switch (request.getParameter("actionType")) {
                case "add":
                    if (Utils.checkPermission(commonLoginData.userDTO, getAddPageMenuConstants()) && getAddPermission(request)) {
                        CommonDTO commonDTO = addT(request, true, commonLoginData.userDTO);
                        finalize(request);
                        response.sendRedirect(getAddRedirectURL(request, commonDTO));
                        return;
                    }
                    break;
                case "ajax_add":
                    if (Utils.checkPermission(commonLoginData.userDTO, getAddPageMenuConstants()) && getAddPermission(request)) {
                        try {
                            CommonDTO commonDTO = addT(request, true, commonLoginData.userDTO);
                            finalize(request);
                            ApiResponse.sendSuccessResponse(response, getAjaxAddRedirectURL(request, commonDTO));
                        } catch (Exception ex) {
                            logger.error("Exception : ",ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                    } else {
                        ApiResponse.sendErrorResponse(response, commonLoginData.isLangEng ? "You do not have permission to do this task" : "আপনার এই ক্রিয়া সম্পাদনের অনুমতি নেই");
                    }
                    return;
                case "UploadFilesFromDropZone":
                    long ColumnID = Long.parseLong(request.getParameter("ColumnID"));
                    Utils.UploadFilesFromDropZone(request, response, commonLoginData.userDTO.ID, ColumnID, getServletName());
                    return;
                case "getDTO":
                    if (Utils.checkPermission(commonLoginData.userDTO, getAddPageMenuConstants())) {
                        getDTO(request, response);
                        return;
                    }
                    break;
                case "ajax_edit":
                    if (Utils.checkPermission(commonLoginData.userDTO, getEditPageMenuConstants())
                            && getEditPermission(request)) {
                        try {
                            CommonDTO commonDTO = addT(request, false, commonLoginData.userDTO);
                            finalize(request);
                            ApiResponse.sendSuccessResponse(response, getAjaxEditRedirectURL(request, commonDTO));
                        } catch (Exception ex) {
                            logger.error("Exception : ",ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                    } else {
                        ApiResponse.sendErrorResponse(response, commonLoginData.isLangEng ? "You do not have permission to do this task" : "আপনার এই ক্রিয়া সম্পাদনের অনুমতি নেই");
                    }
                    return;
                case "edit":
                    if (Utils.checkPermission(commonLoginData.userDTO, getEditPageMenuConstants()) && getEditPermission(request)) {
                        CommonDTO commonDTO = addT(request, false, commonLoginData.userDTO);
                        finalize(request);
                        response.sendRedirect(getEditRedirectURL(request, commonDTO));
                        return;
                    }
                    break;
                case "delete":
                    if (getDeletePermission(request)) {
                        long deleteTime = System.currentTimeMillis();
                        List<Long> idList = getDeleteIdList(request);
                        deleteT(idList, commonLoginData.userDTO);
                        request.setAttribute(DELETED_ID_LIST,idList);
                        request.setAttribute(DELETED_TIME,deleteTime);
                        finalize(request);
                        response.sendRedirect(getDeleteRedirectURL(request));
                        return;
                    }
                    break;
                case "getGeo":
                    request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
                    return;
            }

        } catch (Exception ex) {
            logger.error("Exception : ",ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    protected void setDTOForJsp(HttpServletRequest request) throws Exception {
        CommonDTO commonDTO = getCommonDAOService().getDTOFromID(getId(request));
        if (commonDTO == null)
            throw new FileNotFoundException("no DTO found");

        request.setAttribute(BaseServlet.DTO_FOR_JSP, commonDTO);
    }

    protected <T extends CommonDTO> void getDTO(HttpServletRequest request, HttpServletResponse response) throws IOException {
        long id;
        if (request.getParameter("ID") != null) {
            id = Long.parseLong(request.getParameter("ID"));
        } else {
            id = Long.parseLong(request.getParameter("iD"));
        }
        T t = (T) getCommonDAOService().getDTOFromID(id);
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        String encoded = this.gson.toJson(t);
        out.print(encoded);
        out.flush();
    }

    protected void deleteT(List<Long> ids,UserDTO userDTO) throws Exception {
        if (ids.size() > 0) {
            getCommonDAOService().deletePb(userDTO.employee_record_id, ids);
        }
    }

    protected void deleteT(HttpServletRequest request, UserDTO userDTO) {
        List<Long> ids = getDeleteIdList(request);
        if (ids.size() > 0) {
            getCommonDAOService().deletePb(userDTO.employee_record_id, ids);
        }
    }

    protected void deleteT(HttpServletRequest request, UserDTO userDTO,long deleteTime) {
        List<Long> ids = getDeleteIdList(request);
        if (ids.size() > 0) {
            getCommonDAOService().deletePb(userDTO.employee_record_id, ids,deleteTime);
        }
    }

    protected void deleteT(List<Long> ids, UserDTO userDTO,long deleteTime) {
        if (ids.size() > 0) {
            getCommonDAOService().deletePb(userDTO.employee_record_id, ids,deleteTime);
        }
    }

    protected List<Long> getDeleteIdList(HttpServletRequest request) {
        String[] IDsToDelete = request.getParameterValues("ID");
        if (IDsToDelete.length > 0) {
            return Stream.of(IDsToDelete)
                    .map(Long::parseLong)
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }
    
    public Map<String, String> buildRequestParams(HttpServletRequest request)
    {
    	return HttpRequestUtils.buildRequestParams(request);
    }


    protected void search(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, String> params = buildRequestParams(request);
        if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) != null) {
            Map<String, String> extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
            params.putAll(extraCriteriaMap);
        }
        RecordNavigator recordNavigator = getCommonDAOService().getRecordNavigator(params, getTableJoinClause(request), getWhereClause(request), getSortClause(request));
        request.setAttribute(RECORD_NAVIGATOR, recordNavigator);
        String ajax = request.getParameter("ajax");
        String url;
        if (ajax != null && ajax.equalsIgnoreCase("true")) {
            url = commonPartOfDispatchURL() + "SearchForm.jsp";
        } else {
            url = defaultNonAjaxSearchPageURL();
        }
        logger.debug("url : " + url);
        request.getRequestDispatcher(url).forward(request, response);
    }

    public final long getId(HttpServletRequest request) {
        long id;
        if (request.getParameter("ID") != null) {
            id = Long.parseLong(request.getParameter("ID"));
        } else {
            id = Long.parseLong(request.getParameter("iD"));
        }
        return id;
    }

    public <T> void validateAndSet(T t, HttpServletRequest request, boolean isLangEng) throws Exception {
        Class<?> clazz1 = t.getClass();
        do {
            Field[] fields = clazz1.getDeclaredFields();
            String value = null;
            for (Field field : fields) {
                field.setAccessible(true);
                String fieldName = field.getName();
                if (field.getAnnotations().length > 0) {
                    if (field.isAnnotationPresent(NotEmpty.class)) {
                        NotEmpty annotation = field.getAnnotation(NotEmpty.class);
                        value = getValue(fieldName, annotation.name(), request, isLangEng);
                        if (value.length() == 0) {
                            throw new Exception(isLangEng ? (annotation.engError().length() > 0 ? annotation.engError() : fieldName + " is null or empty")
                                    : (annotation.bngError().length() > 0 ? annotation.bngError() : fieldName + " is null or empty"));
                        }
                        if (value.length() < annotation.minLength()) {
                            throw new Exception("Minimum require length is " + annotation.minLength() + " for " + fieldName);
                        }
                        if (value.length() > annotation.maxLength()) {
                            throw new Exception("Exceed maximum, maximum length is " + annotation.minLength() + " for " + fieldName);
                        }
                        setFieldValue(field, value, t);
                    }

                    if (field.isAnnotationPresent(ValidMobileNumber.class)) {
                        ValidMobileNumber annotation = field.getAnnotation(ValidMobileNumber.class);
                        if (value == null) {
                            value = getValue(fieldName, annotation.name(), request, isLangEng);
                        }
                        if (value.length() == 0 && !annotation.isMandatory()) {
                            field.set(t, "");
                        } else {
                            if (!value.startsWith("88")) {
                                value = "88" + value;
                            }
                            if (!Utils.isMobileNumberValid(value)) {
                                throw new Exception(isLangEng ? annotation.engError() : annotation.bngError());
                            }
                            setFieldValue(field, value, t);
                        }
                    } else if (field.isAnnotationPresent(CategoryValidation.class)) {
                        CategoryValidation annotation = field.getAnnotation(CategoryValidation.class);
                        String errorText = "Please select for " + fieldName;
                        if (value == null) {
                            value = getValue(fieldName, annotation.name(), request, isLangEng);
                        }
                        if (!annotation.mandatory() && value.length() == 0) {
                            field.set(t, annotation.value());
                        } else {
                            try {
                                if (value.length() == 0) {
                                    throw new Exception(isLangEng ? (annotation.engError().length() > 0 ? annotation.engError() : errorText)
                                            : (annotation.bngError().length() > 0 ? annotation.bngError() : errorText));
                                }
                                Integer catValue = Integer.parseInt(value);
                                if (CatRepository.getInstance().getCategoryLanguageModel(annotation.domainName(), catValue) == null) {
                                    throw new Exception(isLangEng ? (annotation.properEngError().length() == 0 ? (annotation.engError().length() > 0 ? annotation.engError() : errorText) : annotation.properEngError())
                                            : (annotation.properBngError().length() == 0 ? (annotation.bngError().length() > 0 ? annotation.bngError() : errorText) : annotation.properBngError()));
                                }
                                field.set(t, catValue);
                            } catch (NumberFormatException ex) {
                                throw new Exception(isLangEng ? (annotation.properEngError().length() == 0 ? (annotation.engError().length() > 0 ? annotation.engError() : errorText) : annotation.properEngError())
                                        : (annotation.properBngError().length() == 0 ? (annotation.bngError().length() > 0 ? annotation.bngError() : errorText) : annotation.properBngError()));
                            }
                        }
                    } else if (field.isAnnotationPresent(DateValidation.class)) {
                        DateValidation annotation = field.getAnnotation(DateValidation.class);
                        if (value == null) {
                            value = getValue(fieldName, annotation.name(), request, isLangEng);
                        }
                        SimpleDateFormat sdf = new SimpleDateFormat(annotation.pattern());
                        if (!annotation.mandatory() && value.length() == 0) {
                            field.set(t, annotation.value());
                        } else {
                            try {
                                field.set(t, sdf.parse(value).getTime());
                            } catch (Exception ex) {
                                throw new Exception(isLangEng ? (annotation.engError().length() > 0 ? annotation.engError() : "Please select date for " + fieldName)
                                        : (annotation.bngError().length() > 0 ? annotation.bngError() : "Please select date for " + fieldName));
                            }
                        }
                    } else if (field.isAnnotationPresent(EmailValidation.class)) {
                        EmailValidation annotation = field.getAnnotation(EmailValidation.class);
                        if (value == null) {
                            value = getValue(fieldName, annotation.name(), request, isLangEng);
                        }
                        if (value.length() == 0 && !annotation.isMandatory()) {
                            field.set(t, "");
                        } else {
                            if (value.length() == 0) {
                                throw new Exception(isLangEng ? annotation.engError() : annotation.bngError());
                            }
                            if (!Utils.isEmailValid(value)) {
                                throw new Exception(isLangEng ? annotation.properEngError() : annotation.properBngError());
                            }
                            setFieldValue(field, value, t);
                        }
                    } else if (field.isAnnotationPresent(ParliamentNumberValidation.class)) {
                        ParliamentNumberValidation annotation = field.getAnnotation(ParliamentNumberValidation.class);
                        if (value == null) {
                            value = getValue(fieldName, annotation.name(), request, isLangEng);
                        }
                        if (value.length() == 0 && !annotation.isMandatory()) {
                            field.set(t, annotation.value());
                        } else {
                            if (value.length() == 0) {
                                throw new Exception(isLangEng ? annotation.engError() : annotation.bngError());
                            }
                            try {
                                long electionDetailsId = Long.parseLong(request.getParameter("electionDetailsId").trim());
                                Election_detailsDTO electionDetailsDTO = Election_detailsRepository.getInstance().getElectionDetailsDTOByID(electionDetailsId);
                                if (electionDetailsDTO == null) {
                                    logger.error("Election details is not found for : " + electionDetailsId);
                                    throw new Exception(isLangEng ? annotation.properEngError() : annotation.properBngError());
                                }
                                if (annotation.isRunningParliamentMandatory()) {
                                    if (electionDetailsDTO.parliamentStatusCat == 0) {
                                        throw new Exception(isLangEng ? "Parliament election - " + electionDetailsDTO.parliamentNumber + " is activated yet"
                                                : "জাতীয় সংসদ নির্বাচন - " + StringUtils.convertToBanNumber(String.valueOf(electionDetailsDTO.parliamentNumber)) + " এখন পর্যন্ত সক্রিয় করা হয় নি");
                                    }

                                    if (electionDetailsDTO.parliamentStatusCat == 2) {
                                        throw new Exception(isLangEng ? "Already parliament election - " + electionDetailsDTO.parliamentNumber + " was being breakdown"
                                                : "ইতিমধ্যে জাতীয় সংসদ নির্বাচন - " + StringUtils.convertToBanNumber(String.valueOf(electionDetailsDTO.parliamentNumber)) + " ভেঙ্গে ফেলা হয়েছে");
                                    }
                                }
                            } catch (Exception ex) {
                                throw new Exception(ex.getMessage());
                            }
                            setFieldValue(field, value, t);
                        }
                    } else if (field.isAnnotationPresent(ElectionConstituencyValidation.class)) {
                        ElectionConstituencyValidation annotation = field.getAnnotation(ElectionConstituencyValidation.class);
                        if (value == null) {
                            value = getValue(fieldName, annotation.name(), request, isLangEng);
                        }
                        if (value.length() == 0 && !annotation.isMandatory()) {
                            field.set(t, annotation.value());
                        } else {
                            if (value.length() == 0) {
                                throw new Exception(isLangEng ? annotation.engError() : annotation.bngError());
                            }
                            try {
                                long electionConstituencyId = Long.parseLong(request.getParameter("electionConstituencyId").trim());
                                if (Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(electionConstituencyId) == null) {
                                    throw new Exception(isLangEng ? annotation.properEngError() : annotation.properBngError());
                                }
                            } catch (Exception ex) {
                                throw new Exception(isLangEng ? annotation.engError() : annotation.bngError());
                            }
                            setFieldValue(field, value, t);
                        }
                    } else if (field.isAnnotationPresent(PoliticalPartyAnnotation.class)) {
                        PoliticalPartyAnnotation annotation = field.getAnnotation(PoliticalPartyAnnotation.class);
                        if (value == null) {
                            value = getValue(fieldName, annotation.name(), request, isLangEng);
                        }
                        if (value.length() == 0 && !annotation.isMandatory()) {
                            field.set(t, annotation.value());
                        } else {
                            if (value.length() == 0) {
                                throw new Exception(isLangEng ? annotation.engError() : annotation.bngError());
                            }
                            try {
                                long politicalPartyId = Long.parseLong(request.getParameter("politicalPartyId").trim());
                                if (Political_partyRepository.getInstance().getPolitical_partyDTOByID(politicalPartyId) == null) {
                                    throw new Exception(isLangEng ? annotation.properEngError() : annotation.properBngError());
                                }
                            } catch (Exception ex) {
                                throw new Exception(isLangEng ? annotation.engError() : annotation.bngError());
                            }
                            setFieldValue(field, value, t);
                        }
                    }
                    value = null;
                } else {
                    if (request.getParameter(fieldName) != null) {
                        setFieldValue(field, Utils.doJsoupCleanOrReturnEmpty(request.getParameter(fieldName)), t);
                    }
                }
            }
            clazz1 = clazz1.getSuperclass();
        } while (clazz1 != null);
    }

    private String getValue(String fieldName, String name, HttpServletRequest request, boolean isLangEng) throws Exception {
        if (request.getParameter(fieldName) == null) {
            fieldName = name;
        }
        if (request.getParameter(fieldName) == null) {
            throw new Exception(fieldName + " is missing");
        }
        String value = request.getParameter(fieldName);
        return Utils.doJsoupCleanOrReturnEmpty(value.trim());
    }

    private <T> void setFieldValue(Field field, String value, T t) throws IllegalAccessException {
        switch (field.getType().getName()) {
            case "int":
            case "java.lang.Integer":
                field.setInt(t, Integer.parseInt(value));
                break;
            case "float":
            case "java.lang.Float":
                field.setFloat(t, Float.parseFloat(value));
                break;
            case "long":
            case "java.lang.Long":
                field.setLong(t, Long.parseLong(value));
                break;
            case "double":
            case "java.lang.Double":
                field.setDouble(t, Double.parseDouble(value));
                break;
            case "java.lang.String":
                field.set(t, value);
                break;
        }
    }
}