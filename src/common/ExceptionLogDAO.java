package common;


import pb.Utils;

public class ExceptionLogDAO {

	private static final String addQuery = "insert into exception_log(stacktrace,time) VALUES(?,?)";
	
	public void insertException(ExceptionDTO exceptionDTO){
		Utils.runIOTaskInSingleThread(()->()->ConnectionAndStatementUtil.getLogWritePrepareStatement(ps->{
			try {
				ps.setObject(1,exceptionDTO.stacktrace);
				ps.setObject(2,System.currentTimeMillis());
				ps.execute();
			} catch (Exception e) {
				e.printStackTrace();
			}
		},addQuery));
	}
}