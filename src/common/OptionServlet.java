package common;

import education_level.DegreeExamRepository;
import geolocation.GeoDistrictRepository;
import geolocation.GeoLocationRepository;
import geolocation.GeoThanaRepository;
import login.LoginDTO;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsRepository;
import pb.CatRepository;
import sessionmanager.SessionConstants;
import ticket_issues.TicketIssueSubTypeRepository;
import user.UserDAO;
import user.UserDTO;
import user.UserRepository;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.apache.log4j.Logger;

import java.util.stream.Collectors;

/*
 * @author Md. Erfan Hossain
 * @created 16/04/2021 - 11:54 PM
 * @project parliament
 */

@WebServlet("/option_Servlet")
@MultipartConfig
public class OptionServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(OptionServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        if (userDTO != null) {
            String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
            String actionType = request.getParameter("actionType");
            logger.debug("actionType : " + actionType);
            String options = null;
            Long selectedValue = request.getParameter("selectedValue") != null ? Long.parseLong(request.getParameter("selectedValue")) : null;
            switch (actionType) {
                case "ajax_type": {
                    String type = request.getParameter("type");
                    switch (type) {
                        case "district": {
                            Long divisionId = request.getParameter("division") == null ? null : Long.parseLong(request.getParameter("division"));
                            options = GeoDistrictRepository.getInstance().buildOptionsByDivision(language, divisionId, selectedValue);
                        }
                        break;
                        case "thana": {
                            Long districtId = request.getParameter("district") == null ? null : Long.parseLong(request.getParameter("district"));
                            options = GeoThanaRepository.getInstance().buildOptionsByDistrict(language, districtId, selectedValue);
                        }
                        break;
                        case "thanas": {
                            Long districtId = request.getParameter("district") == null ? null : Long.parseLong(request.getParameter("district"));
                            options = GeoThanaRepository.getInstance().buildOptionsByDistrict(language, districtId, request.getParameter("selectedValues"));
                        }
                        break;
                        case "degree": {
                            long educationLevelId = Long.parseLong(request.getParameter("education_level_id"));
                            options = DegreeExamRepository.getInstance().buildOption(language, educationLevelId, selectedValue);
                        }
                        break;
                        case "geo_location": {
                            int parentGeoId = Integer.parseInt(request.getParameter("parent_geo_id"));
                            Integer selectedId = request.getParameter("selected_id") == null ? null : Integer.parseInt(request.getParameter("selected_id"));
                            language = request.getParameter("language");
                            options = GeoLocationRepository.getInstance().buildOption(language, parentGeoId, selectedId);
                        }
                        break;
                    }
                }
                break;
                case "category": {
                    String category = request.getParameter("category");
                    options = CatRepository.getInstance().buildOptions(category, language, selectedValue == null ? null : selectedValue.intValue());
                }
                break;
                case "ajax_designation": {
                    long id = Long.parseLong(request.getParameter("officeUnitId"));
                    options = OfficeUnitOrganogramsRepository.getInstance().buildOptionsForDesignationWithParentDesignation(language, null, id);
                }
                break;
                case "ajax_parents": {
                    long officeUnitId = Long.parseLong(request.getParameter("officeUnitId"));
                    List<Long> parents = Office_unitsRepository.getInstance().getAllParentIds(officeUnitId);
                    options = parents.stream()
                            .map(String::valueOf)
                            .collect(Collectors.joining(","));
                }
                break;
                case "ticket_issues_subtype":
                    long ticketIssuesId = Long.parseLong(request.getParameter("ticket_issues"));
                    long ticketIssuesSubTypeId = Long.parseLong(request.getParameter("ticket_issues_sub_type"));
                    options = TicketIssueSubTypeRepository.getInstance().buildOptions(language, ticketIssuesId, ticketIssuesSubTypeId);
                    break;

                case "superadmin_pw_reset":
                    options = "Password is not updated";
                    if (HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.userName.equals("superadmin")) {
                        String pw = request.getParameter("password");
                        logger.debug("password : " + pw);
                        if (pw != null && pw.trim().length() >= 8) {
                            try {
                                new UserDAO().updateSuperAdminPassword(pw);
                                options = "Password is updated successfully";
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    break;
            }
            if (options != null) {
                PrintWriter out = response.getWriter();
                out.println(options);
                out.close();
            }
        }
    }
}