package common;


import pb.Utils;

public interface PerformanceLogDAO {

	String addQuery = "insert into performance_log(URI,requestTime,ipAddress,portNumber,totalServiceTime,userID) VALUES(?,?,?,?,?,?)";

	static void insert(PerformanceLog log){
		Utils.runIOTaskInSingleThread(()->()->ConnectionAndStatementUtil.getLogWritePrepareStatement(ps->{
			try {
				ps.setObject(1,log.URI);
				ps.setObject(2,log.requestTime);
				ps.setObject(3,log.ipAddress);
				ps.setObject(4,log.portNumber);
				ps.setObject(5,log.totalServiceTime);
				ps.setObject(6,log.userID);
				//ps.execute();
			} catch (Exception e) {
				e.printStackTrace();
			}
		},addQuery));
	}
}