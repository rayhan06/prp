package common;

/*
 * @author Md. Erfan Hossain
 * @created 10/04/2021 - 3:37 PM
 * @project parliament
 */

import education_level.DegreeExamRepository;

import java.util.HashMap;
import java.util.Map;

public class TextInterfaceMap {
    private static final Map<String,? super TextInterface> mapByTableName = new HashMap<>();

    static {
        mapByTableName.put("degree_exam", DegreeExamRepository.getInstance());
    }

    public static TextInterface getByTableName(String tableName){
        return (TextInterface) mapByTableName.get(tableName);
    }
}
