package common;

/*
 * @author Md. Erfan Hossain
 * @created 09/07/2021 - 12:06 AM
 * @project parliament
 */

import audit.log.AuditLogDAO;
import audit.log.LogEvent;
import dbm.DBMW;
import org.apache.log4j.Logger;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.CommonDTO;
import util.CommonLoginData;
import util.HttpRequestUtils;
import util.RecordNavigator;

import javax.servlet.http.HttpServletRequest;
import java.sql.*;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings({"Duplicates", "unused", "rawtypes"})
public interface CommonDAOService<T extends CommonDTO> extends VbSequencerService {
    String CRITERIA_MAP_KEY = "CRITERIA_MAP_KEY";
    String FETCH_ALL = "fetchAll";

    ThreadLocal<Connection> CONNECTION_THREAD_LOCAL = new ThreadLocal<>();

    ThreadLocal<CacheUpdateModel> CACHE_UPDATE_MODEL_THREAD_LOCAL = new ThreadLocal<>();

    Logger loggerCommonDAOService = Logger.getLogger(CommonDAOService.class);

    String getByIDQuery = "SELECT * FROM %s WHERE ID= %d AND isDeleted = 0";
    String getAllSqlQuery = "SELECT * FROM %s WHERE isDeleted = 0";

    String getById = "SELECT * FROM %s WHERE ID= %d";

    String getAllDTOsForTrue = "SELECT * FROM %s order by lastModificationTime desc";

    String getAllDTOsForFalse = "SELECT * FROM %s WHERE lastModificationTime >= %d order by lastModificationTime desc";

    String getAllDTOsExactLastModificationTime = "SELECT * FROM %s WHERE lastModificationTime = %d";

    String getAllDTOsByLastModificationTimeGreaterThan = "SELECT * FROM %s WHERE lastModificationTime >= %d";

    String getDTOsByIds = "SELECT * FROM {tableName} WHERE ID IN ( %s )  order by lastModificationTime desc";

    String deleteByIdSQLQuery = "UPDATE %s SET isDeleted = 1,lastModificationTime = %d,modified_by = %d WHERE id IN (%s)";

    String deleteByWhereClause = "UPDATE %s SET isDeleted = 1,lastModificationTime = %d,modified_by = %d WHERE %s";


    default String getUpdateQuery(String[] columnNames) {
        StringBuilder sqlBuilder = new StringBuilder("UPDATE {tableName} SET ");
        int i = 0;
        for (String columnName : columnNames) {
            if (!columnName.equals("ID") && !columnName.equals("isDeleted")) {
                sqlBuilder.append(columnName).append(" = ?");
                if (i < columnNames.length - 3) {
                    sqlBuilder.append(", ");
                }
                i++;
            }
        }
        sqlBuilder.append(" WHERE ID = ?");

        return sqlBuilder.toString();
    }

    default String getUpdateQuery2(String[] columnNames) {
        String[] temp = new String[columnNames.length - 2];
        System.arraycopy(columnNames, 0, temp, 0, columnNames.length - 2);
        return Stream.of(temp)
                .map(e -> e + " = ?")
                .collect(Collectors.joining(",",
                        "UPDATE {tableName} SET ", " WHERE " + columnNames[columnNames.length - 1] + " = ?"));
    }

    default String getUpdateQueryWithDeletedUpdate(String[] columnNames) {
        String[] temp = new String[columnNames.length - 1];
        System.arraycopy(columnNames, 0, temp, 0, columnNames.length - 1);
        return Stream.of(temp)
                .map(e -> e + " = ?")
                .collect(Collectors.joining(",",
                        "UPDATE {tableName} SET ", " WHERE " + columnNames[columnNames.length - 1] + " = ?"));
    }

    default String getAddQuery2(String[] columnNames) {
        String[] temp = new String[columnNames.length];
        System.arraycopy(columnNames, 0, temp, 0, columnNames.length);
        String addPrefix = Stream.of(temp)
                .collect(Collectors.joining(", ", "INSERT INTO {tableName} (", " ) VALUES ( "));
        String[] questionMarksArr = new String[columnNames.length];
        Arrays.fill(questionMarksArr, "?");
        return Stream.of(questionMarksArr)
                .collect(Collectors.joining(", ", addPrefix, " )"));
    }

    default String getAddQuery(String[] columnNames) {
        StringBuilder sqlBuilder = new StringBuilder("INSERT INTO {tableName} ")
                .append(" (")
                .append(String.join(",", columnNames))
                .append(") VALUES(");
        String[] questionMarksArr = new String[columnNames.length];
        Arrays.fill(questionMarksArr, "?");
        sqlBuilder.append(String.join(",", questionMarksArr)).append(")");
        return sqlBuilder.toString();
    }

    void set(PreparedStatement ps, T t, boolean isInsert) throws SQLException;

    T buildObjectFromResultSet(ResultSet rs);

    String getTableName();

    Map<String, String> getSearchMap();

    long add(CommonDTO commonDTO) throws Exception;

    long update(CommonDTO commonDTO) throws Exception;

    default long update(CommonDTO commonDTO, T oldT) throws Exception {
        return update(commonDTO);
    }

    default String getLastModifierUser() {
        return "modified_by";
    }

    default T getDTOFromID(long ID) {
        String sql = String.format(getByIDQuery, getTableName(), ID);
        List<T> list = getDTOs(sql);
        return list.size() == 0 ? null : list.get(0);
    }

    default List<T> getAllDTOs() {
        return getDTOs(String.format(getAllSqlQuery, getTableName()));
    }

    default T getDTOFromIdDeletedOrNot(long ID) {
        String sql = String.format(getById, getTableName(), ID);
        List<T> list = getDTOs(sql);
        return list.size() == 0 ? null : list.get(0);
    }

    default List<T> getDTOs(List<Long> ids) {
        if (ids.isEmpty()) {
            return new ArrayList<>();
        }
        String sql = getDTOsByIds.replace("{tableName}", getTableName());
        return ConnectionAndStatementUtil.getDTOListByNumbers(sql, ids, this::buildObjectFromResultSet);
    }

    default List<T> getAllDTOs(boolean isFirstReload) {
        String sql;
        if (isFirstReload) {
            sql = String.format(getAllDTOsForTrue, getTableName());
        } else {
            sql = String.format(getAllDTOsForFalse, getTableName(), RepositoryManager.lastModifyTime);
        }
        return getDTOs(sql);
    }

    default List<T> getAllDTOsExactLastModificationTime(long time) {
        return getDTOs(String.format(getAllDTOsExactLastModificationTime, getTableName(), time));
    }

    default List<T> getAllDTOsByLastModificationTimeGreaterThan(long time) {
        return getDTOs(String.format(getAllDTOsByLastModificationTimeGreaterThan, getTableName(), time));
    }

    default List<T> getDTOs(String sql) {
        return getDTOs(sql, this::buildObjectFromResultSet);
    }

    default List<T> getDTOs(String sql, List<Object> objectList) {
        return getDTOs(sql, objectList, this::buildObjectFromResultSet);
    }

    default <TT> List<TT> getDTOs(String sql, Function<ResultSet, TT> func) {
        return ConnectionAndStatementUtil.getListOfT(sql, func);
    }

    default <TT> List<TT> getDTOs(String sql, List<Object> objectList, Function<ResultSet, TT> func) {
        return ConnectionAndStatementUtil.getListOfT(sql, objectList, func);
    }

    default <TT> List<TT> getDTOs(String sql, Consumer<PreparedStatement> consumer, Function<ResultSet, TT> func) {
        return ConnectionAndStatementUtil.getListOfT(sql, consumer, func);
    }

    default List<T> getDTOs(String sql, Consumer<PreparedStatement> consumer) {
        return ConnectionAndStatementUtil.getListOfT(sql, consumer, this::buildObjectFromResultSet);
    }

    default long executeAddOrUpdateQuery(T t, String sql, boolean isInsert) throws Exception {
        T oldT = null;
        if (!isInsert) {
            oldT = getDTOFromID(t.iD);
        }
        return executeAddOrUpdateQuery(t, sql, isInsert, oldT);
    }

    default Collection getDTOsByParent(String parentColumnName, long parentId) {
        String sql = "SELECT * FROM " + getTableName() + " where isDeleted=0 and " + parentColumnName + " = " + parentId + " order by " + getTableName() + ".id asc";
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    default Collection getDTOsByParent(String parentColumnName, List<Long> parentIds) {
        String idStr = parentIds.stream()
                .distinct()
                .map(String::valueOf)
                .collect(Collectors.joining(","));
        String sql = "SELECT * FROM " + getTableName() + " where isDeleted=0 and " + parentColumnName + " in (" + idStr + ") order by " + getTableName() + ".id asc";
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }
    
    default Collection getDTOsByParentTimeDesc(String parentColumnName, long parentId) {
    	String sql = "SELECT * FROM " + getTableName() + " where isDeleted=0 and " + parentColumnName + " = " + parentId + " order by " 
    			+ getTableName()  + ".lastModificationTime desc";
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
      
    }

    default void deleteChildrenNotInList(String parentName, String childName, long parentID, List<Long> chilIds) throws Exception {
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            String sql = "update " + childName + " set isDeleted = 1, lastModificationTime = " + System.currentTimeMillis()
                    + " WHERE " + parentName + "_id=" + parentID;
            if (chilIds != null && chilIds.size() > 0) {
                String ids = chilIds.stream()
                        .map(String::valueOf)
                        .collect(Collectors.joining(","));
                sql += " and id not in (" + ids + ")";
            }
            loggerCommonDAOService.debug("sql " + sql);
            try {
                st.execute(sql);
            } catch (SQLException ex) {
                loggerCommonDAOService.error(ex);
            }
        });
    }

    default List<Long> getChilIds(String parentName, String childName, long parentID) throws Exception {
        String sql = "select id from " + childName + " WHERE " + parentName + "_id=" + parentID + " and isDeleted = 0";
        return ConnectionAndStatementUtil.getListOfT(sql, rs -> {
            try {
                return rs.getLong("id");
            } catch (SQLException ex) {
                loggerCommonDAOService.error(ex);
                return null;
            }
        });
    }

    default List<Long> getChilIds(String parentName, String childName, List<Long> parentIDs) throws Exception {
        String idStr = parentIDs.stream()
                .distinct()
                .map(String::valueOf)
                .collect(Collectors.joining(","));
        String sql = "select id from " + childName + " WHERE " + parentName + "_id in (" + idStr + ") and isDeleted = 0";
        return ConnectionAndStatementUtil.getListOfT(sql, rs -> {
            try {
                return rs.getLong("id");
            } catch (SQLException ex) {
                loggerCommonDAOService.error(ex);
                return null;
            }
        });
    }

    default List<Long> getDistinctColumnIds(String parentName, String childName, String columnName, List<Long> parentIDs) throws Exception {
        String idStr = parentIDs.stream()
                .distinct()
                .map(String::valueOf)
                .collect(Collectors.joining(","));
        String sql = "select distinct " + columnName + " as id from " + childName + " WHERE " + parentName + "_id in (" + idStr + ") and isDeleted = 0";
        return ConnectionAndStatementUtil.getListOfT(sql, rs -> {
            try {
                return rs.getLong("id");
            } catch (SQLException ex) {
                loggerCommonDAOService.error(ex);
                return null;
            }
        });
    }

    default long hardDelete(long id) throws Exception {
        long lastModificationTime = System.currentTimeMillis();
        StringBuilder sqlBuilder = new StringBuilder("delete from ")
                .append(getTableName())
                .append(" WHERE id = ")
                .append(id);
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            String sql = sqlBuilder.toString();
            Connection connection = model.getConnection();
            Statement stmt = model.getStatement();
            try {
                loggerCommonDAOService.debug(sql);
                stmt.execute(sql);
                recordUpdateTime(model.getConnection(), getTableName(), lastModificationTime);
            } catch (SQLException ex) {
                loggerCommonDAOService.error(ex);
            }
        });
        return id;
    }

    default void hardDeleteChildrenByParent(long parentId, String parentColName) throws Exception {
        long lastModificationTime = System.currentTimeMillis();
        StringBuilder sqlBuilder = new StringBuilder("delete from ")
                .append(getTableName())
                .append(" WHERE ")
                .append(parentColName).append(" = ")
                .append(parentId);
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            String sql = sqlBuilder.toString();
            Connection connection = model.getConnection();
            Statement stmt = model.getStatement();
            try {
                loggerCommonDAOService.debug(sql);
                stmt.execute(sql);
                recordUpdateTime(model.getConnection(), getTableName(), lastModificationTime);
            } catch (SQLException ex) {
                loggerCommonDAOService.error(ex);
            }
        });
    }

    default long deleteChildrenByParent(long parentId, String parentColName) throws Exception {
        return deleteChildrenByParent(parentId, parentColName, System.currentTimeMillis());
    }

    default long deleteChildrenByParent(long parentId, String parentColName, long lastModificationTime) throws Exception {
        if (CONNECTION_THREAD_LOCAL.get() != null) {
            return deleteChildrenByParent(parentId,parentColName,lastModificationTime,CONNECTION_THREAD_LOCAL.get());
        } else {
            Utils.handleTransaction(()->{
                CACHE_UPDATE_MODEL_THREAD_LOCAL.set(new CacheUpdateModel(lastModificationTime == 0 ? System.currentTimeMillis() : lastModificationTime));
                deleteChildrenByParent(parentId,parentColName,lastModificationTime,CONNECTION_THREAD_LOCAL.get());
                Utils.executeCache();
            });
        }
        return parentId;
    }

    default long deleteChildrenByParent(long parentId, String parentColName, long lastModificationTime, Connection connection) throws Exception {
        StringBuilder sqlBuilder = new StringBuilder("UPDATE ")
                .append(getTableName())
                .append(" SET isDeleted=1,lastModificationTime=")
                .append(lastModificationTime).append(" WHERE ")
                .append(parentColName).append(" = ")
                .append(parentId);
        Utils.wrapWithAtomicReference(ar ->
                ConnectionAndStatementUtil.getWriteStatement(stmt -> {
                    String sql = sqlBuilder.toString();
                    try {
                        loggerCommonDAOService.debug(sql);
                        stmt.execute(sql);
                        recordUpdateTime(connection, getTableName(), lastModificationTime);
                    } catch (SQLException ex) {
                        loggerCommonDAOService.error(ex);
                        ar.set(ex);
                    }
                }, connection));

        return parentId;
    }

    default List<Long> getChildIdsFromRequest(HttpServletRequest request, String childDTOName) throws Exception {
        String[] values = request.getParameterValues(childDTOName + ".iD");
        if (values == null || values.length == 0) {
            return null;
        }
        return Stream.of(values)
                .map(Long::parseLong)
                .collect(Collectors.toList());
    }

    default long executeAddOrUpdateQuery(T t, String sql, boolean isInsert, T oldT) throws Exception {
        return executeAddOrUpdateQuery(t, sql, isInsert, oldT, true);
    }

    default long executeAddOrUpdateQuery(T t, String sql, boolean isInsert, boolean doAudit) throws Exception {
        return executeAddOrUpdateQuery(t, sql, isInsert, null, doAudit);
    }

    default long executeAddOrUpdateQuery(T t, String sql, boolean isInsert, T oldT, boolean doAudit) throws Exception {
        if (CONNECTION_THREAD_LOCAL.get() != null) {
            return executeAddOrUpdateQuery(t, sql, isInsert, oldT, CONNECTION_THREAD_LOCAL.get(), doAudit);
        } else {
            Utils.handleTransaction(() -> {
                CACHE_UPDATE_MODEL_THREAD_LOCAL.set(new CacheUpdateModel(t.lastModificationTime == 0 ? System.currentTimeMillis() : t.lastModificationTime));
                executeAddOrUpdateQuery(t, sql, isInsert, oldT, CONNECTION_THREAD_LOCAL.get(), doAudit);
                Utils.executeCache();
            });
            return t.iD;
        }
    }

    default long executeAddOrUpdateQuery(T t, String sql, boolean isInsert, Connection connection) throws Exception {
        T oldT = null;
        if (!isInsert) {
            oldT = getDTOFromID(t.iD);
        }
        return executeAddOrUpdateQuery(t, sql, isInsert, oldT, connection, true);
    }

    default long executeAddOrUpdateQuery(T t, String sql, boolean isInsert, T oldT, Connection connection) throws Exception {
        return executeAddOrUpdateQuery(t, sql, isInsert, oldT, connection, true);
    }

    default long executeAddOrUpdateQuery(T t, String sql, boolean isInsert, T oldT, Connection connection, boolean doAudit) throws Exception {
        if (isInsert) {
            t.iD = DBMW.getInstance().getNextSequenceId(getTableName());
        }
        String sql2 = sql.replace("{tableName}", getTableName());
        Utils.wrapWithAtomicReference(ar ->
                ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
                    try {
                        set(ps, t, isInsert);
                        loggerCommonDAOService.debug(ps);
                        if (isInsert) {
                            ps.execute();
                        } else {
                            ps.executeUpdate();
                        }
                        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
                        if (doAudit && commonLoginData != null) {
                            AuditLogDAO.add(oldT, t, isInsert ? LogEvent.ADD : LogEvent.UPDATE, getTableName(), HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().ipAddress,
                                    HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.ID, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.organogramID);
                        }
                        Repository repository = RepositoryManager.getInstance().getRepository(getTableName());
                        if (repository != null) {
                            if (t.lastModificationTime == 0) {
                                recordUpdateTime(connection, getTableName(), System.currentTimeMillis());
                            } else {
                                recordUpdateTime(connection, getTableName(), t.lastModificationTime);
                            }
                            if (CACHE_UPDATE_MODEL_THREAD_LOCAL.get() != null) {
                                CACHE_UPDATE_MODEL_THREAD_LOCAL.get().repositoryList.add(repository);
                            }
                        }
                    } catch (SQLException e) {
                        loggerCommonDAOService.error("",e);
                        ar.set(e);
                    }
                }, connection, sql2));
        return t.iD;
    }

    default RecordNavigator getRecordNavigatorLimitless(Map<String, String> params, String tableJoinClause, String whereClause, String sortingClause) {
        RecordNavigator recordNavigator = new RecordNavigator();
        recordNavigator.m_tableName = getTableName();

        List<Object> objectList = new ArrayList<>();
        String whereClauseStr = buildWhereClause(params, whereClause, objectList);

        StringBuilder selectQueryBuilder = new StringBuilder("SELECT * FROM ").append(getTableName());
        if (tableJoinClause != null && tableJoinClause.trim().length() > 0) {
            selectQueryBuilder.append(" ").append(tableJoinClause);
        }
        selectQueryBuilder.append(whereClauseStr);
        if (sortingClause == null || sortingClause.trim().length() == 0) {
            selectQueryBuilder.append(" ORDER BY ").append(getTableName()).append(".lastModificationTime DESC");
        } else {
            selectQueryBuilder.append(" ORDER BY ").append(sortingClause);
        }

        recordNavigator.list = ConnectionAndStatementUtil.getListOfT(selectQueryBuilder.toString(), objectList, this::buildObjectFromResultSet);

        return recordNavigator;
    }


    default RecordNavigator getRecordNavigator(Map<String, String> params, String tableJoinClause, String whereClause, String sortingClause) {
        RecordNavigator recordNavigator = new RecordNavigator();
        recordNavigator.m_tableName = getTableName();
        int pageNo;
        try {
            pageNo = Integer.parseInt(params.getOrDefault("pageno", "1"));
        } catch (Exception e) {
            pageNo = 1;
        }
        if (pageNo <= 0 || params.get("go") == null) {
            pageNo = 1;
        }
        int pageSize;
        try {
            pageSize = Integer.parseInt(params.getOrDefault("RECORDS_PER_PAGE", "10"));
        } catch (Exception e) {
            pageSize = 10;
        }

        if (pageSize <= 0) {
            pageSize = 10;
        }
        params.remove("pageno");
        params.remove("RECORDS_PER_PAGE");
        List<Object> objectList = new ArrayList<>();
        String whereClauseStr = buildWhereClause(params, whereClause, objectList);
        StringBuilder countQueryBuilder = new StringBuilder("SELECT count(*) as countID FROM ").append(getTableName());
        if (tableJoinClause != null && tableJoinClause.trim().length() > 0) {
            countQueryBuilder.append(" ").append(tableJoinClause);
        }
        countQueryBuilder.append(" ").append(whereClauseStr);
        int count = ConnectionAndStatementUtil.getT(countQueryBuilder.toString(), objectList, rs -> {
            try {
                return rs.getInt("countID");
            } catch (SQLException ex) {
                loggerCommonDAOService.error(ex);
                return 0;
            }
        });
        recordNavigator.setTotalRecords(count);
        if (params.getOrDefault(FETCH_ALL, "false").equals("true")) {
            pageSize = count;
        }
        if (count == 0) {
            recordNavigator.list = null;
            recordNavigator.setCurrentPageNo(1);
            recordNavigator.setTotalPages(1);
        } else {
            int totalPage = count / pageSize;
            if (count % pageSize != 0) {
                ++totalPage;
            }
            recordNavigator.setTotalPages(totalPage);
            recordNavigator.setPageSize(pageSize);
            if (pageNo > totalPage) {
                pageNo = totalPage;
            }
            int offset = (pageNo - 1) * pageSize;
            recordNavigator.setCurrentPageNo(pageNo);
            StringBuilder selectQueryBuilder = new StringBuilder("SELECT * FROM ").append(getTableName());
            if (tableJoinClause != null && tableJoinClause.trim().length() > 0) {
                selectQueryBuilder.append(" ").append(tableJoinClause);
            }
            selectQueryBuilder.append(whereClauseStr);
            if (sortingClause == null || sortingClause.trim().length() == 0) {
                selectQueryBuilder.append(" ORDER BY ").append(getTableName()).append(".lastModificationTime DESC");
            } else {
                selectQueryBuilder.append(" ORDER BY ").append(sortingClause);
            }
            selectQueryBuilder.append(" LIMIT ").append(pageSize);
            if (offset > 0) {
                selectQueryBuilder.append(" OFFSET ").append(offset);
            }
            recordNavigator.list = ConnectionAndStatementUtil.getListOfT(selectQueryBuilder.toString(), objectList, this::buildObjectFromResultSet);
        }

        return recordNavigator;
    }

    default String buildWhereClause(Map<String, String> params, String whereClause, List<Object> objectList) {
        StringBuilder sqlBuilder = new StringBuilder();
        Map<String, String> searchMap = getSearchMap();
        sqlBuilder.append(" WHERE (").append(getTableName()).append(".isDeleted = 0)");
        if (params != null && params.size() > 0 && searchMap != null && searchMap.size() > 0) {
            String searchString = params.entrySet()
                    .stream()
                    .filter(entry -> entry.getValue() != null && entry.getValue().trim().length() > 0 && searchMap.get(entry.getKey()) != null)
                    .map(entry -> {
                        if (objectList == null) {
                            return searchMap.get(entry.getKey()).replace("{}", entry.getValue());
                        } else {
                            String[] tokens = searchMap.get(entry.getKey()).split(" ");
                            List<String> ls = Stream.of(tokens)
                                    .filter(e -> e.length() > 0)
                                    .collect(Collectors.toList());
                            for (int i = 1; i < ls.size(); i++) {
                                if (ls.get(i).startsWith("?")) {
                                    switch (ls.get(i - 1).toUpperCase()) {
                                        case "LIKE":
                                            objectList.add("%" + entry.getValue() + "%");
                                            break;
                                        case "IN":
                                            String[] questions = entry.getValue().split(",");
                                            String quesStr = Stream.generate(() -> "?")
                                                    .limit(questions.length)
                                                    .collect(Collectors.joining(",", "(", ")"));
                                            objectList.addAll(Arrays.asList(questions));
                                            return searchMap.get(entry.getKey()).replace("?", quesStr);
                                        default:
                                            objectList.add(entry.getValue());
                                    }
                                }
                            }
                            return searchMap.get(entry.getKey());
                        }
                    })
                    .collect(Collectors.joining(" "));
            if (searchString.trim().length() > 0) {
                sqlBuilder.append(searchString);
            }
        }
        if (whereClause != null && whereClause.trim().length() > 0) {
            sqlBuilder.append(" ").append(whereClause).append(" ");
        }

        loggerCommonDAOService.debug("where clause check : " + sqlBuilder);
        return sqlBuilder.toString();
    }

    default boolean delete(long modifiedBy, Long id) {
        return delete(modifiedBy, Collections.singletonList(id));
    }

    default boolean delete(long modifiedBy, Long id, long deleteTime) {
        return delete(modifiedBy, Collections.singletonList(id), deleteTime);
    }

    default boolean delete(long modifiedBy, List<Long> ids) {
        return delete(modifiedBy, ids, System.currentTimeMillis());
    }

    default boolean delete(long modifiedBy, List<Long> ids, long deleteTime) {
        String idStr = ids.stream()
                .distinct()
                .map(String::valueOf)
                .collect(Collectors.joining(","));
        String sql = String.format(deleteByIdSQLQuery, getTableName(), deleteTime, modifiedBy, idStr);
        loggerCommonDAOService.debug(sql);
        return (boolean) ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            try {
                model.getStatement().executeUpdate(sql);
                recordUpdateTime(model.getConnection(), getTableName(), deleteTime);
                return true;
            } catch (SQLException ex) {
                loggerCommonDAOService.error("",ex);
                return false;
            }
        });
    }

    default boolean delete(long modifiedBy, String whereClause, long deleteTime) {
        String sql = String.format(deleteByWhereClause, getTableName(), deleteTime, modifiedBy, whereClause);
        loggerCommonDAOService.debug(sql);
        return (boolean) ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            try {
                model.getStatement().executeUpdate(sql);
                recordUpdateTime(model.getConnection(), getTableName(), deleteTime);
                return true;
            } catch (SQLException ex) {
                loggerCommonDAOService.error("",ex);
                return false;
            }
        });
    }

    default boolean deletePb(long modifiedBy, List<Long> ids) {
        return deletePb(modifiedBy, ids, System.currentTimeMillis());
    }

    default boolean deletePb(long modifiedBy, List<Long> ids, long deleteTime) {
        String idStr = ids.stream()
                .distinct()
                .map(String::valueOf)
                .collect(Collectors.joining(","));
        String sql = "UPDATE " + getTableName() + " SET isDeleted = 1, lastModificationTime = " + deleteTime;
        if (!getLastModifierUser().equalsIgnoreCase("")) {
            sql += ", " + getLastModifierUser() + " = " + modifiedBy;
        }
        sql += " WHERE id IN (" + idStr + ")";

        final String deleteSql = sql;
        loggerCommonDAOService.debug(sql);
        return (boolean) ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            try {
                model.getStatement().executeUpdate(deleteSql);
                recordUpdateTime(model.getConnection(), getTableName(), deleteTime);
                return true;
            } catch (SQLException ex) {
                loggerCommonDAOService.error("",ex);
                return false;
            }
        });
    }

    default int getCount(Map<String, String> params, String tableJoinClause, String whereClause) {
        List<Object> objectList = new ArrayList<>();
        String whereClauseStr = buildWhereClause(params, whereClause, objectList);
        String countQuery = "SELECT count(*) as countID FROM " + getTableName() + whereClauseStr;
        return ConnectionAndStatementUtil.getT(countQuery, objectList, rs -> {
            try {
                return rs.getInt("countID");
            } catch (SQLException ex) {
                loggerCommonDAOService.error("",ex);
                return 0;
            }
        }, 0);
    }

    default int getCount(String whereClause) {
        List<Object> objectList = new ArrayList<>();
        String countQuery = "SELECT count(*) as countID FROM " + getTableName() + " where isDeleted = 0 ";
        if (!whereClause.equalsIgnoreCase("")) {
            countQuery += " and " + whereClause;
        }
        return ConnectionAndStatementUtil.getT(countQuery, objectList, rs -> {
            try {
                return rs.getInt("countID");
            } catch (SQLException ex) {
                loggerCommonDAOService.error("",ex);
                return 0;
            }
        }, 0);
    }
}