package common;

import dbm.DBML;
import dbm.DBMR;
import dbm.DBMW;
import org.apache.log4j.Logger;
import pb.Utils;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

@SuppressWarnings({"unchecked","unused","Duplicates"})
public class ConnectionAndStatementUtil {
	
	private static final Logger logger = Logger.getLogger(ConnectionAndStatementUtil.class);
	
	//READ
	public static Object getReadStatement(Function<Statement, Object> func){
		return getStatement(func,ConnectionType.READ);
	}
	
	public static void getReadStatement(Consumer<Statement> consumer){
		getStatement(consumer,ConnectionType.READ);
	}
	
	public static Object getReadStatement(Function<Statement, Object> func,Connection con){
		return getStatement(func,con);
	}
	
	public static void getReadStatement(Consumer<Statement> consumer,Connection con){
		getStatement(consumer,con);
	}
	
	public static Object getReadConnectionAnStatement(Function<ConnectionAndStatementModel, Object> func){
		return getConnectionAndStatement(func,ConnectionType.READ);
	}
	
	public static void getReadConnectionAndStatement(Consumer<ConnectionAndStatementModel> consumer) {
		getConnectionAndStatement(consumer,ConnectionType.READ);
	}
	
	public static Object getReadPrepareStatement(Function<PreparedStatement, Object> func,String sqlQuery){
		return getPrepareStatement(func,sqlQuery,ConnectionType.READ);
	}
	
	public static void getReadPrepareStatement(Consumer<PreparedStatement> consumer,String sqlQuery){
		getPrepareStatement(consumer,sqlQuery,ConnectionType.READ);
	}
	
	public static Object getReadPrepareStatement(Function<PreparedStatement, Object> func,Connection con,String sqlQuery) {
		return getPrepareStatement(func,con,sqlQuery);
	}
	
	public static void getReadPrepareStatement(Consumer<PreparedStatement> consumer,Connection con,String sqlQuery){
		getPrepareStatement(consumer,con,sqlQuery);
	}
	
	public static Object getReadConnectionAndPrepareStatement(Function<ConnectionAndStatementModel, Object> func,String sqlQuery){
		return getConnectionAndPrepareStatement(func,sqlQuery,ConnectionType.READ);
	}
	
	public static void getReadConnectionAndPrepareStatement(Consumer<ConnectionAndStatementModel> consumer,String sqlQuery){
		getConnectionAndPrepareStatement(consumer,sqlQuery,ConnectionType.READ);
	}
	
	
	//WRITE
	public static Object getWriteStatement(Function<Statement, Object> func){
		return getStatement(func,ConnectionType.WRITE);
	}
	
	public static void getWriteStatement(Consumer<Statement> consumer){
		getStatement(consumer,ConnectionType.WRITE);
	}
	
	public static Object getWriteStatement(Function<Statement, Object> func,Connection con){
		return getStatement(func,con);
	}
	
	public static void getWriteStatement(Consumer<Statement> consumer,Connection con){
		getStatement(consumer,con);
	}
	
	public static Object getWriteConnectionAndStatement(Function<ConnectionAndStatementModel, Object> func){
		return getConnectionAndStatement(func,ConnectionType.WRITE);
	}
	
	public static void getWriteConnectionAndStatement(Consumer<ConnectionAndStatementModel> consumer){
		getConnectionAndStatement(consumer,ConnectionType.WRITE);
	}
	
	public static Object getWritePrepareStatement(Function<PreparedStatement, Object> func,String sqlQuery){
		return getPrepareStatement(func, sqlQuery, ConnectionType.WRITE);
	}
	
	public static void getWritePrepareStatement(Consumer<PreparedStatement> consumer,String sqlQuery){
		getPrepareStatement(consumer, sqlQuery, ConnectionType.WRITE);
	}
	
	public static Object getWritePrepareStatement(Function<PreparedStatement, Object> func,Connection con,String sqlQuery) {
		return getPrepareStatement(func,con,sqlQuery);
	}
	
	public static void getWritePrepareStatement(Consumer<PreparedStatement> consumer,Connection con,String sqlQuery){
		getPrepareStatement(consumer,con,sqlQuery);
	}
	
	public static Object getWriteConnectionAndPrepareStatement(Function<ConnectionAndStatementModel, Object> func,String sqlQuery){
		return getConnectionAndPrepareStatement(func,sqlQuery,ConnectionType.WRITE);
	}
	
	public static void getWriteConnectionAndPrepareStatement(Consumer<ConnectionAndStatementModel> consumer,String sqlQuery){
		getConnectionAndPrepareStatement(consumer,sqlQuery,ConnectionType.WRITE);
	}
	
	
	//LOG
	public static Object getLogWriteStatement(Function<Statement, Object> func){
		return getStatement(func,ConnectionType.LOG);
	}
	
	public static void getLogWriteStatement(Consumer<Statement> consumer){
		getStatement(consumer,ConnectionType.LOG);
	}
	
	public static Object getLogWriteStatement(Function<Statement, Object> func,Connection con){
		return getStatement(func,con);
	}
	
	public static void getLogWriteStatement(Consumer<Statement> consumer,Connection con){
		getStatement(consumer,con);
	}
	
	public static Object getLogWriteConnectionAndStatement(Function<ConnectionAndStatementModel, Object> func){
		return getConnectionAndStatement(func,ConnectionType.LOG);
	}
	
	public static void getLogWriteConnectionAndStatement(Consumer<ConnectionAndStatementModel> consumer){
		getConnectionAndStatement(consumer,ConnectionType.LOG);
	}
	
	public static Object getLogWritePrepareStatement(Function<PreparedStatement, Object> func,String sqlQuery){
		return getPrepareStatement(func, sqlQuery, ConnectionType.LOG);
	}
	
	public static void getLogWritePrepareStatement(Consumer<PreparedStatement> consumer,String sqlQuery){
		getPrepareStatement(consumer, sqlQuery, ConnectionType.LOG);
	}
	
	public static Object getLogWritePrepareStatement(Function<PreparedStatement, Object> func,Connection con,String sqlQuery) {
		return getPrepareStatement(func,con,sqlQuery);
	}
	
	public static void getLogWritePrepareStatement(Consumer<PreparedStatement> consumer,Connection con,String sqlQuery){
		getPrepareStatement(consumer,con,sqlQuery);
	}
	
	public static Object getLogWriteConnectionAndPrepareStatement(Function<ConnectionAndStatementModel, Object> func,String sqlQuery){
		return getConnectionAndPrepareStatement(func,sqlQuery,ConnectionType.LOG);
	}
	
	public static void getLogWriteConnectionAndPrepareStatement(Consumer<ConnectionAndStatementModel> consumer,String sqlQuery){
		getConnectionAndPrepareStatement(consumer,sqlQuery,ConnectionType.LOG);
	}
	
	private static Object getStatement(Function<Statement, Object> func,Connection con){
		Statement st = null;
        try {
            st = con.createStatement();
			//logger.debug("Successfully Open Statement");
            return func.apply(st);
        }catch (Exception ex) {
			ex.printStackTrace();
        	logger.error(ex);
		} finally {
            if (st != null) {
                try {
                    st.close();
                    //logger.debug("Successfully Closed Statement");
                } catch (SQLException ex) {
					ex.printStackTrace();
					logger.error("Failed to Closed Statement "+ex);
                	ex.printStackTrace();
                }
            }
        }
        logger.error("Failed to create Statement");
		return null;
    }

	private static Object getStatement(Function<Statement, Object> func,Connection con,ConnectionType connectionType){
		Statement st = null;
		try {
			st = con.createStatement();
			//logger.debug("Successfully Open Statement for "+connectionType);
			return func.apply(st);
		}catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
		} finally {
			if (st != null) {
				try {
					st.close();
					//logger.debug("Successfully Closed Statement for "+connectionType);
				} catch (SQLException ex) {
					logger.error("Failed to Closed Statement "+ex);
					ex.printStackTrace();
				}
			}
		}
		logger.error("Failed to create Statement");
		return null;
	}
	
	private static void getStatement(Consumer<Statement> consumer,Connection con){
		Statement st = null;
        try {
            st = con.createStatement();
			//logger.debug("Successfully Open Statement");
            consumer.accept(st);
        }catch (Exception ex) {
        	logger.error(ex);
			ex.printStackTrace();
		} finally {
            if (st != null) {
                try {
                    st.close();
                    //logger.debug("Successfully Closed Statement");
                } catch (SQLException ex) {
					logger.error("Failed to Closed Statement "+ex);
					ex.printStackTrace();
                }
            }
        }
    }

	private static void getStatement(Consumer<Statement> consumer,Connection con,ConnectionType connectionType){
		Statement st = null;
		try {
			st = con.createStatement();
			//logger.debug("Successfully Open Statement for "+connectionType);
			consumer.accept(st);
		}catch (Exception ex) {
			logger.error(ex);
			ex.printStackTrace();
		} finally {
			if (st != null) {
				try {
					st.close();
					//logger.debug("Successfully Closed Statement for "+connectionType);
				} catch (SQLException ex) {
					logger.error("Failed to Closed Statement "+ex);
					ex.printStackTrace();
				}
			}
		}
	}
	
	private static Object getPrepareStatement(Function<PreparedStatement, Object> func,Connection con,String sqlQuery){
		logger.debug(sqlQuery);
		PreparedStatement st = null;
        try {
            st = con.prepareStatement(sqlQuery);
			//logger.debug("Successfully Open PrepareStatement");
            return func.apply(st);
        }catch (Exception ex) {
        	logger.error(ex);
			ex.printStackTrace();
		}finally {
            if (st != null) {
                try {
                    st.close();
                    //logger.debug("Successfully Closed PrepareStatement");
                } catch (SQLException ex) {
					logger.error("Failed to Closed PrepareStatement "+ex);
					ex.printStackTrace();
                }
            }
        }
        logger.error("Failed to create PrepareStatement");
		return null;
	}

	private static Object getPrepareStatement(Function<PreparedStatement, Object> func,Connection con,String sqlQuery,ConnectionType connectionType){
		logger.debug(sqlQuery);
		PreparedStatement st = null;
		try {
			st = con.prepareStatement(sqlQuery);
			//logger.debug("Successfully Open PrepareStatement for "+connectionType);
			return func.apply(st);
		}catch (Exception ex) {
			logger.error(ex);
			ex.printStackTrace();
		}finally {
			if (st != null) {
				try {
					st.close();
					//logger.debug("Successfully Closed PrepareStatement for "+connectionType);
				} catch (SQLException ex) {
					logger.error("Failed to Closed PrepareStatement "+ex);
					ex.printStackTrace();
				}
			}
		}
		logger.error("Failed to create PrepareStatement");
		return null;
	}
	
	private static void getPrepareStatement(Consumer<PreparedStatement> consumer,Connection con,String sqlQuery){
		logger.debug(sqlQuery);
		PreparedStatement st = null;
        try {
            st = con.prepareStatement(sqlQuery);
			//logger.debug("Successfully Open PrepareStatement");
            consumer.accept(st);
        }catch (Exception ex) {
        	logger.error(ex);
			ex.printStackTrace();
		}finally {
            if (st != null) {
                try {
                    st.close();
                    //logger.debug("Successfully Closed PrepareStatement");
                } catch (SQLException ex) {
					logger.error("Failed to Closed PrepareStatement : "+ex);
					ex.printStackTrace();
                }
            }
        }
	}

	private static void getPrepareStatement(Consumer<PreparedStatement> consumer,Connection con,String sqlQuery,ConnectionType connectionType){
		logger.debug(sqlQuery);
		PreparedStatement st = null;
		try {
			st = con.prepareStatement(sqlQuery);
			//logger.debug("Successfully Open PrepareStatement "+connectionType);
			consumer.accept(st);
		}catch (Exception ex) {
			logger.error(ex);
			ex.printStackTrace();
		}finally {
			if (st != null) {
				try {
					st.close();
					//logger.debug("Successfully Closed PrepareStatement "+connectionType);
				} catch (SQLException ex) {
					logger.error("Failed to Closed PrepareStatement : "+ex);
					ex.printStackTrace();
				}
			}
		}
	}
	
	private static Object getConnectionAndPrepareStatement(Function<ConnectionAndStatementModel, Object> func,String sqlQuery,ConnectionType connectionType) {
		return getConnection(con->{
			return getPrepareStatement(ps->{
				return func.apply(new ConnectionAndStatementModel(con,ps));
			},con, sqlQuery);
		}, connectionType);
	}
	
	private static void getConnectionAndPrepareStatement(Consumer<ConnectionAndStatementModel> consumer,String sqlQuery,ConnectionType connectionType) {
		 getConnection(con->{
			 getPrepareStatement(ps->{
				 consumer.accept(new ConnectionAndStatementModel(con,ps));
			},con, sqlQuery);
		}, connectionType);
	}
	
	private static Object getConnectionAndStatement(Function<ConnectionAndStatementModel, Object> func,ConnectionType connectionType) {
		return getConnection(con->{
			return getStatement(ps->{
				return func.apply(new ConnectionAndStatementModel(con,ps));
			},con);
		}, connectionType);
	}
	
	private static void getConnectionAndStatement(Consumer<ConnectionAndStatementModel> consumer,ConnectionType connectionType) {
		 getConnection(con->{
			 getStatement(ps->{
				 consumer.accept(new ConnectionAndStatementModel(con,ps));
			},con);
		}, connectionType);
	}
	
	private static Object getStatement(Function<Statement, Object> func,ConnectionType connectionType){
        return getConnection(con -> {
        	return getStatement(func,con);
        },connectionType);
    }
	
	private static void getStatement(Consumer<Statement> consumer,ConnectionType connectionType){
         getConnection(con -> {
        	 getStatement(consumer,con);
        },connectionType);
    }
	
	
	private static Object getPrepareStatement(Function<PreparedStatement, Object> func,String sqlQuery,ConnectionType connectionType){
        return getConnection(con -> {
        	return getPrepareStatement(func,con,sqlQuery);
        },connectionType);
    }
	
	private static void getPrepareStatement(Consumer<PreparedStatement> consumer,String sqlQuery,ConnectionType connectionType){
         getConnection(con -> {
        	 getPrepareStatement(consumer,con,sqlQuery);
        },connectionType);
    }
	
	private static Object getConnection(Function<Connection, Object> func,ConnectionType connectionType) {
        Connection connection = null;
        try{
        	do {
				connection = getConnection(connectionType);
			}while (connection == null);
            return func.apply(connection);
        }catch(Exception ex) {
        	logger.error(ex);
			ex.printStackTrace();
        }finally {
			closeConnection(connection, connectionType);
		}
        logger.error("Failed to get Connection");
		return null;
    }
	
	private static void getConnection(Consumer<Connection> consumer,ConnectionType connectionType) {
        Connection connection = null;
        try{
			do {
				connection = getConnection(connectionType);
			}while (connection == null);
            consumer.accept(connection);
        }catch(Exception ex) {
        	logger.error(ex);
			ex.printStackTrace();
        }finally {
			closeConnection(connection, connectionType);
		}
    }
	
	public static Connection getConnection(ConnectionType connectionType) throws Exception{
		Connection connection ;
		switch(connectionType) {
    	case READ:
			connection = DBMR.getInstance().getConnection();
			break;
    	case WRITE:
			connection = DBMW.getInstance().getConnection();
			break;
    	case LOG:
			connection = DBML.getInstance().getConnection();
			break;
		default:
			throw new Exception("Invalid connection type : "+connectionType);
    	}
		//logger.debug("Open Connection For "+connectionType.name());
		return connection;
	}
	
	public static void closeConnection(Connection connection,ConnectionType connectionType) {
		if(connection!=null){
        	try {
        		switch(connectionType) {
            	case READ:
            		DBMR.getInstance().freeConnection(connection);
            		break;
            	case WRITE:
					DBMW.getInstance().freeConnection(connection);
            		break;
            	case LOG:
            		DBML.getInstance().freeConnection(connection);
            		break;
            	}
				//logger.debug("Closed Connection For "+connectionType.name());
			} catch (Exception e) {
				logger.error("Exception is occurred during to do free connection "+e);
				e.printStackTrace();
			}
        }
	}

	public static <T> T getT(String sql, Consumer<PreparedStatement> psConsumer, Function<ResultSet,T> transformToT){
		List<T> list = getListOfT(sql,psConsumer,transformToT);
		return list.size() == 0 ? null : list.get(0);
	}

	public static <T> List<T> getListOfT(String sql,Consumer<PreparedStatement> psConsumer, Function<ResultSet,T> transformToT){
		return (List<T>)ConnectionAndStatementUtil.getReadPrepareStatement(ps->{
			List<T> list = new ArrayList<>();
			psConsumer.accept(ps);
			try {
				logger.debug(ps);
				ResultSet rs = ps.executeQuery();
				while (rs.next()){
					T t = transformToT.apply(rs);
					if(t!=null){
						list.add(t);
					}
				}
			} catch (SQLException ex) {
				logger.error(ex);
				ex.printStackTrace();
			}
			return list;
		},sql);
	}

	public static <T> T getT(String sql, Function<ResultSet,T> transformToT){
		return getT(sql,transformToT,null);
	}

	public static <T> T getT(String sql, Function<ResultSet,T> transformToT,T defaultValue){
		List<T> list = getListOfT(sql,transformToT);
		return list.size() == 0 ? defaultValue : list.get(0);
	}

	public static <T> T getT(String sql,List<Object>objectList, Function<ResultSet,T> transformToT){
		return getT(sql, objectList, transformToT,null);
	}

	public static <T> T getT(String sql,List<Object>objectList, Function<ResultSet,T> transformToT,T defaultValue){
		List<T> list = getListOfT(sql,objectList,transformToT);
		return list.size() == 0 ? defaultValue : list.get(0);
	}

	public static <T> List<T> getListOfT(String sql, Function<ResultSet,T> transformToT){
		return (List<T>)ConnectionAndStatementUtil.getReadStatement(st->{
			List<T> list = new ArrayList<>();
			try {
				logger.debug("sql : "+sql);
				ResultSet rs = st.executeQuery(sql);
				while (rs.next()){
					T t = transformToT.apply(rs);
					if(t!=null){
						list.add(t);
					}
				}
			} catch (SQLException ex) {
				logger.error(ex);
				ex.printStackTrace();
			}
			return list;
		});
	}
	
	public static <T> List<T> getListOfT2(String sql, String extraColumnName, BiFunction<ResultSet, String, T> transformToT){
		return (List<T>)ConnectionAndStatementUtil.getReadStatement(st->{
			List<T> list = new ArrayList<>();
			try {
				logger.debug("sql : "+sql);
				ResultSet rs = st.executeQuery(sql);
				while (rs.next()){
					T t = transformToT.apply(rs, extraColumnName);
					if(t!=null){
						list.add(t);
					}
				}
			} catch (SQLException ex) {
				logger.error(ex);
				ex.printStackTrace();
			}
			return list;
		});
	}
	

	public static <T> List<T> getListOfT(String sql,List<Object>objectList, Function<ResultSet,T> transformToT){
		return (List<T>)ConnectionAndStatementUtil.getReadPrepareStatement(ps->{
			List<T> list = new ArrayList<>();
			try {
				if(objectList!=null && objectList.size()>0){
					for(int i = 1;i<=objectList.size();i++){
						ps.setObject(i,objectList.get(i-1));
					}
				}
				logger.debug(ps);
				ResultSet rs = ps.executeQuery();
				while (rs.next()){
					T t = transformToT.apply(rs);
					if(t!=null){
						list.add(t);
					}
				}
			} catch (SQLException ex) {
				logger.error(ex);
				ex.printStackTrace();
			}
			return list;
		},sql);
	}

	public static <T> List<T> getDTOListByNumbers(String sqlQuery,List<? extends Number>list, Function<ResultSet,T> transformToT) {
		return Utils.getSubList(list,500)
				.stream()
				.map(ls->ls.stream().map(String::valueOf).collect(Collectors.joining(",")))
				.map(ids->String.format(sqlQuery, ids))
				.map(sql->ConnectionAndStatementUtil.getListOfT(sql, transformToT))
				.flatMap(Collection::stream)
				.collect(Collectors.toList());
	}

	public static <T,U> List<T> getDTOList(String sqlQuery,List<U>list, Function<ResultSet,T> transformToT) {
		return Utils.getSubList(list,500)
				.stream()
				.map(ConnectionAndStatementUtil::joinByComma)
				.map(ids->String.format(sqlQuery, ids))
				.map(sql->ConnectionAndStatementUtil.getListOfT(sql, transformToT))
				.flatMap(Collection::stream)
				.collect(Collectors.toList());
	}

	public static <U> String joinByComma(List<U>list){
		return list.stream()
				.map(String::valueOf)
				.map(e->"'".concat(e).concat("'"))
				.collect(Collectors.joining(","));
	}

	public static void executeByNumberList(String sqlQuery,List<? extends Number>list){
		Utils.getSubList(list,500)
				.stream()
				.map(ls->ls.stream().map(String::valueOf).collect(Collectors.joining(",")))
				.map(ids->String.format(sqlQuery, ids))
				.forEach(sql-> ConnectionAndStatementUtil.getWriteStatement(st->{
					try {
						st.executeUpdate(sql);
					} catch (SQLException ex) {
						logger.error(ex);
						ex.printStackTrace();
					}
				}));
	}

	public static <U>void executeByList(String sqlQuery,List<U>list){
		Utils.getSubList(list,500)
				.stream()
				.map(ConnectionAndStatementUtil::joinByComma)
				.map(ids->String.format(sqlQuery, ids))
				.forEach(sql-> ConnectionAndStatementUtil.getWriteStatement(st->{
					try {
						st.executeUpdate(sql);
					} catch (SQLException ex) {
						logger.error(ex);
						ex.printStackTrace();
					}
				}));
	}
}
