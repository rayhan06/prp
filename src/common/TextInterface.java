package common;

/*
 * @author Md. Erfan Hossain
 * @created 10/04/2021 - 3:34 PM
 * @project parliament
 */

public interface TextInterface {
    String getText(String language,long id);
}
