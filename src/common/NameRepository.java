package common;

import geolocation.GeoCountryRepository;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.OrderByEnum;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.CommonDTO;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings({"rawtypes", "unused", "Duplicates"})
public abstract class NameRepository implements Repository {
    private final NameDao nameDao;
    private final Map<Long, NameDTO> mapOfNameDTOToiD;
    protected List<NameDTO> nameDTOList;
    private final Logger logger;

    public NameRepository(NameDao nameDao, Class clazz) {
        this.nameDao = nameDao;
        mapOfNameDTOToiD = new ConcurrentHashMap<>();
        logger = Logger.getLogger(clazz);
        RepositoryManager.getInstance().addRepository(this);
    }

    public synchronized void reload(boolean reloadAll) {
        logger.debug("reload is started for " + nameDao.getTableName() + " reloadAll = " + reloadAll);
        List<NameDTO> nameDTOS = this.nameDao.getAllDTO(reloadAll);
        if (nameDTOS != null && nameDTOS.size() > 0) {
            nameDTOS.stream()
                    .peek(this::removeDtoFromMap)
                    .filter(nameDTO -> nameDTO.isDeleted == 0)
                    .forEach(nameDTO -> mapOfNameDTOToiD.put(nameDTO.iD, nameDTO));
            nameDTOList = (new ArrayList<>(this.mapOfNameDTOToiD.values()))
                    .stream()
                    .sorted(Comparator.comparingLong(o -> o.iD))
                    .collect(Collectors.toList());
        }
        logger.debug("reload has been done for reloadAll = " + reloadAll);
    }

    private void removeDtoFromMap(NameDTO dto) {
        if (mapOfNameDTOToiD.get(dto.iD) != null) {
            mapOfNameDTOToiD.remove(dto.iD);
        }
    }

    public List<NameDTO> getNameDTOList() {
        return nameDTOList;
    }

    public NameDTO getDTOByID(long ID) {
        if (mapOfNameDTOToiD.get(ID) == null && ID > 0) {
            synchronized (this) {
                if (mapOfNameDTOToiD.get(ID) == null) {
                    try {
                        CommonDTO dto = nameDao.getDTOByID(ID);
                        if (dto != null) {
                            mapOfNameDTOToiD.put(ID, (NameDTO) dto);
                        }
                    } catch (Exception e) {
                        logger.error(e);
                    }
                }
            }
        }
        return mapOfNameDTOToiD.get(ID);
    }

    @Override
    public String getTableName() {
        return nameDao.getTableName();
    }


    public String buildOptions(String language, Long selectedId) {
        return buildOptions(language, (selectedId == null || selectedId == 0L) ? null : String.valueOf(selectedId), OrderByEnum.ASC, true, false);
    }

    public String buildOptions(String language, Long selectedId, OrderByEnum orderByEnum) {
        return buildOptions(language, (selectedId == null || selectedId == 0L) ? null : String.valueOf(selectedId), orderByEnum, true, false);
    }

    public String buildOptionsWithoutSelectOption(String language, Long selectedId) {
        return buildOptions(language, (selectedId == null || selectedId == 0L) ? null : String.valueOf(selectedId), OrderByEnum.ASC, false, false);
    }

    public String buildOptionsWithoutSelectOption(String language, Long selectedId, OrderByEnum orderByEnum) {
        return buildOptions(language, (selectedId == null || selectedId == 0L) ? null : String.valueOf(selectedId), orderByEnum, false, false);
    }

    public String buildOptionsForMultiSelection(String language, Long selectedId) {
        return buildOptions(language, (selectedId == null || selectedId == 0L) ? null : String.valueOf(selectedId), OrderByEnum.ASC, true, true);
    }

    public String buildOptionsForMultiSelection(String language, Long selectedId, OrderByEnum orderByEnum) {
        return buildOptions(language, (selectedId == null || selectedId == 0L) ? null : String.valueOf(selectedId), orderByEnum, true, true);
    }

    public String buildOptions(String language, String selectedId, OrderByEnum orderByEnum, boolean withSelectOption, boolean isMultiSelect) {
        List<OptionDTO> optionDTOList = null;
        if (nameDTOList != null && nameDTOList.size() > 0) {
            optionDTOList = nameDTOList.stream()
                    .map(dto -> new OptionDTO(dto.nameEn, dto.nameBn, String.valueOf(dto.iD)))
                    .collect(Collectors.toList());
        }
        if (optionDTOList != null && orderByEnum == OrderByEnum.DSC) {
            Collections.reverse(optionDTOList);
        }
        if (isMultiSelect) {
            return Utils.buildOptionsMultipleSelection(optionDTOList, language, selectedId);
        } else {
            if (withSelectOption) {
                return Utils.buildOptions(optionDTOList, language, selectedId);
            } else {
                return Utils.buildOptionsWithoutSelectWithSelectId(optionDTOList, language, selectedId);
            }
        }
    }

    public String getText(String language, long id) {
        return getText(language.equalsIgnoreCase("English"), id);
    }

    public String getText(boolean isLangEng, long id) {
        NameDTO dto = getDTOByID(id);
        return dto == null ? ""
                : isLangEng ? (dto.nameEn == null ? "" : dto.nameEn)
                : (dto.nameBn == null ? "" : dto.nameBn);
    }

    public String getTexts(String language, String ids) {
        Set<NameDTO> dtos = Stream.of(ids.split(","))
                .filter(option -> option.length() > 0)
                .map(String::trim)
                .map(Utils::convertToLong)
                .filter(Objects::nonNull)
                .map(val -> GeoCountryRepository.getInstance().getDTOByID(val))
                .collect(Collectors.toSet());
        StringBuilder banglaText = new StringBuilder();
        StringBuilder englishText = new StringBuilder();
        for (NameDTO dto : dtos) {
            if (dto != null) {
                if (dto.nameEn != null && dto.nameEn.trim().length() > 0) {
                    englishText.append(dto.nameEn.trim()).append(",");
                }
                if (dto.nameBn != null && dto.nameBn.length() > 0) {
                    banglaText.append(dto.nameBn.trim()).append(",");
                }
            }
        }
        if (englishText.length() > 0) {
            englishText = new StringBuilder(englishText.substring(0, englishText.length() - 1));
        }

        if (banglaText.length() > 0) {
            banglaText = new StringBuilder(banglaText.substring(0, banglaText.length() - 1));
        }

        return (language.equalsIgnoreCase("English")) ? englishText.toString()
                : banglaText.toString();
    }

    public Map<Long, NameDTO> getMapOfNameDTOToiD() {
        return mapOfNameDTOToiD;
    }
}
