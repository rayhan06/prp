package common;

/*
 * @author Md. Erfan Hossain
 * @created 10/04/2021 - 2:00 PM
 * @project parliament
 */


import bank_name.Bank_nameRepository;
import board.BoardRepository;
import complain_action.Complain_actionRepository;
import education_level.Education_levelRepository;
import geolocation.GeoCountryRepository;
import incident.IncidentRepository;
import nationality.NationalityRepository;
import occupation.OccupationRepository;
import profession.ProfessionRepository;
import relation.RelationRepository;
import religion.ReligionRepository;
import result_exam.Result_examRepository;

import java.util.HashMap;
import java.util.Map;


public class NameRepositoryMap {
    private static final Map<String,? super NameRepository> mapByTableName = new HashMap<>();

    static {
        mapByTableName.put("bank_name", Bank_nameRepository.getInstance());
        mapByTableName.put("board", BoardRepository.getInstance());
        mapByTableName.put("complain_action", Complain_actionRepository.getInstance());
        mapByTableName.put("education_level", Education_levelRepository.getInstance());
        mapByTableName.put("geo_countries", GeoCountryRepository.getInstance());
        mapByTableName.put("incident", IncidentRepository.getInstance());
        mapByTableName.put("nationality", NationalityRepository.getInstance());
        mapByTableName.put("occupation", OccupationRepository.getInstance());
        mapByTableName.put("profession", ProfessionRepository.getInstance());
        mapByTableName.put("relation", RelationRepository.getInstance());
        mapByTableName.put("religion", ReligionRepository.getInstance());
        mapByTableName.put("result_exam", Result_examRepository.getInstance());
    }

    public static NameRepository getByTableName(String tableName){
        return (NameRepository) mapByTableName.get(tableName);
    }
}
