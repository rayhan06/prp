package common;

import java.sql.Connection;
import java.sql.Statement;

public class ConnectionAndStatementModel {
	private final Connection connection;
	private final Statement statement;
	
	public ConnectionAndStatementModel(Connection connection, Statement statement) {
		this.connection = connection;
		this.statement = statement;
	}

	public Connection getConnection() {
		return connection;
	}

	public Statement getStatement() {
		return statement;
	}
	
}
