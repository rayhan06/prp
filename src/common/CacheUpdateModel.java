package common;

/*
 * @author Md. Erfan Hossain
 * @created 21/07/2022 - 11:57 AM
 * @project parliament
 */

import repository.Repository;

import java.util.ArrayList;
import java.util.List;

public class CacheUpdateModel {
    public long time;
    public List<Repository> repositoryList = new ArrayList<>();

    public CacheUpdateModel(long time) {
        this.time = time;
    }

    public static CacheUpdateModel clone(CacheUpdateModel model) {
        if (model == null) {
            return null;
        }
        CacheUpdateModel cloneModel = new CacheUpdateModel(model.time);
        cloneModel.repositoryList = new ArrayList<>(model.repositoryList);
        return cloneModel;
    }
}