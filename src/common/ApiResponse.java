package common;

import com.google.gson.Gson;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ApiResponse {
	
	public static final int API_RESPONSE_SUCCESS = 1;
	public static final int API_RESPONSE_ERROR = 0;
	public static final int API_RESPONSE_OK = 200;
	public static final int API_RESPONSE_REDIRECT = 302;
	public static final int API_RESPONSE_BAD_REQUEST = 400;
	public static final int API_RESPONSE_UNAUTHORIZED_REQUEST = 401;
	public static final int API_RESPONSE_RESOURCE_NOT_FOUND = 404;
	public static final int API_RESPONSE_INTERNAL_SERVER_ERROR = 500;
	
	private final int responseCode;
	private final Object payload;

	public int getResponseCode() {
		return responseCode;
	}

	public Object getPayload() {
		return payload;
	}

	public String getMsg() {
		return msg;
	}

	private final String msg;

	public ApiResponse(int responseCode,Object payload,String msg){
		this.responseCode = responseCode;
		this.payload = payload;
		this.msg = msg;
	}
	public static ApiResponse makeErrorResponse(String msg) {
		return new ApiResponse( API_RESPONSE_ERROR, null, msg );
	}

	public static ApiResponse makeResponseToBadRequest(String msg) {
		return new ApiResponse(API_RESPONSE_BAD_REQUEST, null, msg);
	}

	public static ApiResponse makeResponseToUnauthorizedRequest(String msg) {
		return new ApiResponse(API_RESPONSE_UNAUTHORIZED_REQUEST, null, msg);
	}

	public static ApiResponse makeResponseToResourceNotFound(String msg) {
		return new ApiResponse(API_RESPONSE_RESOURCE_NOT_FOUND, null, msg);
	}

	public static ApiResponse makeResponseToInternalServerError(String msg) {
		return new ApiResponse(API_RESPONSE_INTERNAL_SERVER_ERROR, null, msg);
	}

	public static ApiResponse makeSuccessResponse(String msg) {
		return new ApiResponse( API_RESPONSE_OK, null, msg );
	}

	public static ApiResponse makeRedirectResponse(String msg) {
		return new ApiResponse( API_RESPONSE_REDIRECT, null, msg );
	}

	public static void sendSuccessResponse(HttpServletResponse response, String msg) throws IOException {
		PrintWriter pw = response.getWriter();
		pw.write(makeSuccessResponse(msg).getJSONString());
		pw.flush();
		pw.close();
	}

	public static void sendRedirectResponse(HttpServletResponse response, String msg) throws IOException {
		PrintWriter pw = response.getWriter();
		pw.write(makeRedirectResponse(msg).getJSONString());
		pw.flush();
		pw.close();
	}

	public static void sendErrorResponse(HttpServletResponse response, String msg) throws IOException {
		PrintWriter pw = response.getWriter();
		pw.write(makeErrorResponse(msg).getJSONString());
		pw.flush();
		pw.close();
	}

	public static void sendInternalServerErrorResponse(HttpServletResponse response,String msg) throws IOException {
		PrintWriter pw = response.getWriter();
		pw.write(new ApiResponse(API_RESPONSE_INTERNAL_SERVER_ERROR, null, msg).getJSONString());
		pw.flush();
		pw.close();
	}

	public static ApiResponse makeSuccessResponse(Object object,String msg) {
		return new ApiResponse( API_RESPONSE_OK, object, msg );
	}
	public String getJSONString(){
		return new Gson().toJson(this);
	}
}