package common;

/*
 * @author Md. Erfan Hossain
 * @created 19/07/2021 - 4:56 PM
 * @project parliament
 */

import org.apache.log4j.Logger;
import user.UserDTO;
import util.CommonEmployeeDTO;
import util.HttpRequestUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates", "unchecked"})
public interface EmployeeServletService {

    CommonDAOService<? extends CommonEmployeeDTO> getCommonDAOService();

    String getServletName();

    Logger employeeServletServiceLogger = Logger.getLogger(EmployeeServletService.class);

    default String getAddOrEditOrDeleteRedirectURL(HttpServletRequest request, int tab, String data) {
        if (request.getParameter("tab") != null) {
            long empId = Long.parseLong(request.getParameter("empId"));
            String userId = request.getParameter("userId");
            UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
            if (userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId() || userDTO.roleID == RoleEnum.DATA_ENTRY.getRoleId()) {
                return "Employee_recordsServlet?actionType=viewMultiForm&data=" + data + "&tab=" + tab + "&ID=" + empId + "&userId=" + userId;
            } else {
                return "Employee_recordsServlet?actionType=viewMyProfile&data=" + data + "&tab=" + tab + "&ID=" + empId + "&userId=" + userId;
            }
        } else {
            return getServletName() + "?actionType=search";
        }
    }

    default boolean checkAddPermission(HttpServletRequest request) {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        if (userDTO.roleID == RoleEnum.ADMIN.getRoleId()
                || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId()
                || userDTO.roleID == RoleEnum.DATA_ENTRY.getRoleId()) {
            return true;
        }
        long empId = userDTO.employee_record_id;
        if(request.getParameter("empId") != null)
        {
        	empId= Long.parseLong(request.getParameter("empId"));
        }
        		
        boolean permission = empId == userDTO.employee_record_id;
        if (!permission) {
            employeeServletServiceLogger.error("empId doesn't match, empId : " + empId + " userDTO.employee_record_id : " + userDTO.employee_record_id);
        }
        return permission;
    }

    default boolean checkEditOrViewPagePermission(HttpServletRequest request) {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        long empId = Long.parseLong(request.getParameter("empId"));
        boolean isAdmin = userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId() || userDTO.roleID == RoleEnum.DATA_ENTRY.getRoleId();
        CommonEmployeeDTO commonEmployeeDTO = getCommonDAOService().getDTOFromID(extractId(request));
        boolean permission = commonEmployeeDTO != null && commonEmployeeDTO.employeeRecordsId == empId && (isAdmin || empId == userDTO.employee_record_id);
        if (!permission) {
            if (commonEmployeeDTO == null) {
                employeeServletServiceLogger.error("commonEmployeeDTO is null");
            } else if (commonEmployeeDTO.employeeRecordsId != empId) {
                employeeServletServiceLogger.error("commonEmployeeDTO.employeeRecordsId : " + commonEmployeeDTO.employeeRecordsId + " empId : " + empId);
            } else {
                employeeServletServiceLogger.error("isAdmin : " + false);
                employeeServletServiceLogger.error("empId : " + empId + " userDTO.employee_record_id " + userDTO.employee_record_id);
            }
        }
        return permission;
    }

    default void search(HttpServletRequest request) {
        if ("search".equals(request.getParameter("actionType"))) {
            UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
            boolean isAdmin = userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId() || userDTO.roleID == RoleEnum.DATA_ENTRY.getRoleId();
            if (!isAdmin) {
                Map<String, String> extraCriteriaMap;
                if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) == null) {
                    extraCriteriaMap = new HashMap<>();
                } else {
                    extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
                }
                extraCriteriaMap.put("employee_records_id_internal", String.valueOf(userDTO.employee_record_id));
                request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
            }
        }
    }

    default long extractId(HttpServletRequest request) {
        long id;
        if (request.getParameter("iD") != null) {
            id = Long.parseLong(request.getParameter("iD"));
        } else {
            id = Long.parseLong(request.getParameter("ID"));
        }
        return id;
    }
}