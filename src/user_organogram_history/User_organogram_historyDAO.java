package user_organogram_history;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import approval_module_map.*;

public class User_organogram_historyDAO  extends NavigationService3
{
	
	Logger logger = Logger.getLogger(getClass());

	
	public User_organogram_historyDAO(String tableName, String tempTableName, Approval_module_mapDTO approval_module_mapDTO)
	{
		super(tableName, tempTableName, approval_module_mapDTO);		
	}
	
	
	
	public long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception
	{
		
		User_organogram_historyDTO user_organogram_historyDTO = (User_organogram_historyDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			user_organogram_historyDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "user_organogram_id";
			sql += ", ";
			sql += "username";
			sql += ", ";
			sql += "access_date";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
			if(tempTableDTO!=null)
			{
				sql += ", permanent_table_id, operation_type, approval_path_type, approval_order";
			}
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			if(tempTableDTO!=null)
			{
				sql += ", " + tempTableDTO.permanent_table_id + ", " + tempTableDTO.operation_type + ", " + tempTableDTO.approval_path_type + ", " + tempTableDTO.approval_order;
			}
			sql += ")";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,user_organogram_historyDTO.iD);
			ps.setObject(index++,user_organogram_historyDTO.userOrganogramId);
			ps.setObject(index++,user_organogram_historyDTO.username);
			ps.setObject(index++,user_organogram_historyDTO.accessDate);
			ps.setObject(index++,user_organogram_historyDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime, tableName);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return user_organogram_historyDTO.iD;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID, String tableName) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		User_organogram_historyDTO user_organogram_historyDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				user_organogram_historyDTO = new User_organogram_historyDTO();

				user_organogram_historyDTO.iD = rs.getLong("ID");
				user_organogram_historyDTO.userOrganogramId = rs.getLong("user_organogram_id");
				user_organogram_historyDTO.username = rs.getString("username");
				user_organogram_historyDTO.accessDate = rs.getLong("access_date");
				user_organogram_historyDTO.isDeleted = rs.getInt("isDeleted");
				user_organogram_historyDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return user_organogram_historyDTO;
	}
	
	public long update(CommonDTO commonDTO, String tableName) throws Exception
	{		
		User_organogram_historyDTO user_organogram_historyDTO = (User_organogram_historyDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "user_organogram_id=?";
			sql += ", ";
			sql += "username=?";
			sql += ", ";
			sql += "access_date=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + user_organogram_historyDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,user_organogram_historyDTO.userOrganogramId);
			ps.setObject(index++,user_organogram_historyDTO.username);
			ps.setObject(index++,user_organogram_historyDTO.accessDate);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, lastModificationTime, tableName);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return user_organogram_historyDTO.iD;
	}
	
	public List<User_organogram_historyDTO> getDTOs(Collection recordIDs)
	{
		return getDTOs(recordIDs, tableName);
	}
	
	public List<User_organogram_historyDTO> getDTOs(Collection recordIDs, String tableName){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		User_organogram_historyDTO user_organogram_historyDTO = null;
		List<User_organogram_historyDTO> user_organogram_historyDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return user_organogram_historyDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				user_organogram_historyDTO = new User_organogram_historyDTO();
				user_organogram_historyDTO.iD = rs.getLong("ID");
				user_organogram_historyDTO.userOrganogramId = rs.getLong("user_organogram_id");
				user_organogram_historyDTO.username = rs.getString("username");
				user_organogram_historyDTO.accessDate = rs.getLong("access_date");
				user_organogram_historyDTO.isDeleted = rs.getInt("isDeleted");
				user_organogram_historyDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + user_organogram_historyDTO);
				
				user_organogram_historyDTOList.add(user_organogram_historyDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return user_organogram_historyDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<User_organogram_historyDTO> getAllUser_organogram_history (boolean isFirstReload)
    {
		List<User_organogram_historyDTO> user_organogram_historyDTOList = new ArrayList<>();

		String sql = "SELECT * FROM user_organogram_history";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by user_organogram_history.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				User_organogram_historyDTO user_organogram_historyDTO = new User_organogram_historyDTO();
				user_organogram_historyDTO.iD = rs.getLong("ID");
				user_organogram_historyDTO.userOrganogramId = rs.getLong("user_organogram_id");
				user_organogram_historyDTO.username = rs.getString("username");
				user_organogram_historyDTO.accessDate = rs.getLong("access_date");
				user_organogram_historyDTO.isDeleted = rs.getInt("isDeleted");
				user_organogram_historyDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				user_organogram_historyDTOList.add(user_organogram_historyDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return user_organogram_historyDTOList;
    }
	
	public List<User_organogram_historyDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset)
	{
		return getDTOs(p_searchCriteria, limit, offset, tableName, true, 0);
	}
	
	public List<User_organogram_historyDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<User_organogram_historyDTO> user_organogram_historyDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, tableName, isPermanentTable, userApprovalPathType);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				User_organogram_historyDTO user_organogram_historyDTO = new User_organogram_historyDTO();
				user_organogram_historyDTO.iD = rs.getLong("ID");
				user_organogram_historyDTO.userOrganogramId = rs.getLong("user_organogram_id");
				user_organogram_historyDTO.username = rs.getString("username");
				user_organogram_historyDTO.accessDate = rs.getLong("access_date");
				user_organogram_historyDTO.isDeleted = rs.getInt("isDeleted");
				user_organogram_historyDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				user_organogram_historyDTOList.add(user_organogram_historyDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return user_organogram_historyDTOList;
	
	}

		
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType)
    {
		User_organogram_historyMAPS maps = new User_organogram_historyMAPS(tableName);
		String joinSQL = "";
		return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, tableName, isPermanentTable, userApprovalPathType, maps, joinSQL);
    }			
}
	