package user_organogram_history;
import java.util.*; 
import util.*; 


public class User_organogram_historyDTO extends CommonDTO
{

	public long userOrganogramId = 0;
    public String username = "";
	public long accessDate = 0;
	
	
    @Override
	public String toString() {
            return "$User_organogram_historyDTO[" +
            " iD = " + iD +
            " userOrganogramId = " + userOrganogramId +
            " username = " + username +
            " accessDate = " + accessDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}