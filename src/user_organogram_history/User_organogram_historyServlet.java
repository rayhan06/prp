package user_organogram_history;

import java.io.IOException;
import java.io.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;

import util.RecordNavigationManager3;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;

import user_organogram_history.Constants;
import approval_module_map.*;


import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

/**
 * Servlet implementation class User_organogram_historyServlet
 */
@WebServlet("/User_organogram_historyServlet")
@MultipartConfig
public class User_organogram_historyServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(User_organogram_historyServlet.class);
	Approval_module_mapDAO approval_module_mapDAO = new Approval_module_mapDAO();
    Approval_module_mapDTO approval_module_mapDTO;
    String tableName = "user_organogram_history";
    String tempTableName = "user_organogram_history_temp";
	User_organogram_historyDAO user_organogram_historyDAO;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public User_organogram_historyServlet() 
	{
        super();
    	try
    	{
			approval_module_mapDTO = approval_module_mapDAO.getApproval_module_mapDTOByTableName("user_organogram_history");
			user_organogram_historyDAO = new User_organogram_historyDAO(tableName, tempTableName, approval_module_mapDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ORGANOGRAM_HISTORY_ADD))
				{
					getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ORGANOGRAM_HISTORY_UPDATE))
				{
					if(isPermanentTable)
					{
						getUser_organogram_history(request, response, tableName);
					}
					else
					{
						getUser_organogram_history(request, response, tempTableName);
					}
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ORGANOGRAM_HISTORY_SEARCH))
				{
					
					if(isPermanentTable)
					{
						searchUser_organogram_history(request, response, tableName, isPermanentTable);
					}
					else
					{
						searchUser_organogram_history(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("getApprovalPage"))
			{
				System.out.println("getApprovalPage requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ORGANOGRAM_HISTORY_SEARCH))
				{
					searchUser_organogram_history(request, response, tempTableName, false);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		request.setAttribute("ID", -1L);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("user_organogram_history/user_organogram_historyEdit.jsp");
		requestDispatcher.forward(request, response);
	}
	private String getFileName(final Part part) 
	{
	    final String partHeader = part.getHeader("content-disposition");
	    System.out.println("Part Header = {0}" +  partHeader);
	    for (String content : part.getHeader("content-disposition").split(";")) {
	        if (content.trim().startsWith("filename")) {
	            return content.substring(
	                    content.indexOf('=') + 1).trim().replace("\"", "");
	        }
	    }
	    return null;
	}

	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ORGANOGRAM_HISTORY_ADD))
				{
					System.out.println("going to  addUser_organogram_history ");
					addUser_organogram_history(request, response, true, userDTO, tableName, true);
				}
				else
				{
					System.out.println("Not going to  addUser_organogram_history ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			if(actionType.equals("approve"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ORGANOGRAM_HISTORY_ADD))
				{					
					approveUser_organogram_history(request, response, true, userDTO);
				}
				else
				{
					System.out.println("Not going to  addUser_organogram_history ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ORGANOGRAM_HISTORY_ADD))
				{
					if(isPermanentTable)
					{
						getDTO(request, response, tableName);
					}
					else
					{
						getDTO(request, response, tempTableName);
					}
				}
				else
				{
					System.out.println("Not going to  addUser_organogram_history ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ORGANOGRAM_HISTORY_UPDATE))
				{
					if(isPermanentTable)
					{
						addUser_organogram_history(request, response, false, userDTO, tableName, isPermanentTable);
					}
					else
					{
						addUser_organogram_history(request, response, false, userDTO, tempTableName, isPermanentTable);
					}
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{								
				deleteUser_organogram_history(request, response, userDTO, isPermanentTable);				
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ORGANOGRAM_HISTORY_SEARCH))
				{
					searchUser_organogram_history(request, response, tableName, true);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response, String tableName) 
	{
		try 
		{
			System.out.println("In getDTO");
			User_organogram_historyDTO user_organogram_historyDTO = (User_organogram_historyDTO)user_organogram_historyDAO.getDTOByID(Long.parseLong(request.getParameter("ID")), tableName);
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(user_organogram_historyDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	private void approveUser_organogram_history(HttpServletRequest request, HttpServletResponse response, boolean b, UserDTO userDTO) 
	{
		try
		{
			long id = Long.parseLong(request.getParameter("idToApprove"));
			User_organogram_historyDTO user_organogram_historyDTO = (User_organogram_historyDTO)user_organogram_historyDAO.getDTOByID(id, tempTableName);
			user_organogram_historyDAO.manageWriteOperations(user_organogram_historyDTO, SessionConstants.APPROVE, id, userDTO);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
	}
	private void addUser_organogram_history(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, String tableName, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addUser_organogram_history");
			String path = getServletContext().getRealPath("/img2/");
			User_organogram_historyDTO user_organogram_historyDTO;
			String FileNamePrefix;
			if(addFlag == true)
			{
				user_organogram_historyDTO = new User_organogram_historyDTO();
				FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
			}
			else
			{
				user_organogram_historyDTO = (User_organogram_historyDTO)user_organogram_historyDAO.getDTOByID(Long.parseLong(request.getParameter("identity")), tableName);
				FileNamePrefix = request.getParameter("identity");
			}
			
			String Value = "";
			Value = request.getParameter("userOrganogramId");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("userOrganogramId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				user_organogram_historyDTO.userOrganogramId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("username");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("username = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				user_organogram_historyDTO.username = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("accessDate");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("accessDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				user_organogram_historyDTO.accessDate = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addUser_organogram_history dto = " + user_organogram_historyDTO);
			
			if(addFlag == true)
			{
				user_organogram_historyDAO.manageWriteOperations(user_organogram_historyDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				if(isPermanentTable)
				{
					user_organogram_historyDAO.manageWriteOperations(user_organogram_historyDTO, SessionConstants.UPDATE, -1, userDTO);
				}
				else
				{
					user_organogram_historyDAO.manageWriteOperations(user_organogram_historyDTO, SessionConstants.VALIDATE, -1, userDTO);
				}
				
			}
			
			
			
			
			
			
			
			
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			
			if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				getUser_organogram_history(request, response, tableName);
			}
			else
			{
				response.sendRedirect("User_organogram_historyServlet?actionType=search");
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void deleteUser_organogram_history(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, boolean deleteOrReject) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				if(deleteOrReject)
				{
					User_organogram_historyDTO user_organogram_historyDTO = (User_organogram_historyDTO)user_organogram_historyDAO.getDTOByID(id);
					user_organogram_historyDAO.manageWriteOperations(user_organogram_historyDTO, SessionConstants.DELETE, id, userDTO);
					response.sendRedirect("User_organogram_historyServlet?actionType=search");
				}
				else
				{
					User_organogram_historyDTO user_organogram_historyDTO = (User_organogram_historyDTO)user_organogram_historyDAO.getDTOByID(id, tempTableName);
					user_organogram_historyDAO.manageWriteOperations(user_organogram_historyDTO, SessionConstants.REJECT, id, userDTO);
					response.sendRedirect("User_organogram_historyServlet?actionType=getApprovalPage");
				}
			}			
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		
	}

	private void getUser_organogram_history(HttpServletRequest request, HttpServletResponse response, String tableName) throws ServletException, IOException
	{
		System.out.println("in getUser_organogram_history");
		User_organogram_historyDTO user_organogram_historyDTO = null;
		try 
		{
			user_organogram_historyDTO = (User_organogram_historyDTO)user_organogram_historyDAO.getDTOByID(Long.parseLong(request.getParameter("ID")), tableName);
			request.setAttribute("ID", user_organogram_historyDTO.iD);
			request.setAttribute("user_organogram_historyDTO",user_organogram_historyDTO);
			request.setAttribute("user_organogram_historyDAO",user_organogram_historyDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "user_organogram_history/user_organogram_historyInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "user_organogram_history/user_organogram_historySearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "user_organogram_history/user_organogram_historyEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "user_organogram_history/user_organogram_historyEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	private void searchUser_organogram_history(HttpServletRequest request, HttpServletResponse response, String tableName, boolean isPermanent) throws ServletException, IOException
	{
		System.out.println("in  searchUser_organogram_history 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager3 rnManager = new RecordNavigationManager3(
			SessionConstants.NAV_USER_ORGANOGRAM_HISTORY,
			request,
			user_organogram_historyDAO,
			SessionConstants.VIEW_USER_ORGANOGRAM_HISTORY,
			SessionConstants.SEARCH_USER_ORGANOGRAM_HISTORY,
			tableName,
			isPermanent,
			userDTO.approvalPathID);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("user_organogram_historyDAO",user_organogram_historyDAO);
        RequestDispatcher rd;
        if(hasAjax == false)
        {
        	System.out.println("Going to user_organogram_history/user_organogram_historySearch.jsp");
        	rd = request.getRequestDispatcher("user_organogram_history/user_organogram_historySearch.jsp");
        }
        else
        {
        	System.out.println("Going to user_organogram_history/user_organogram_historySearchForm.jsp");
        	rd = request.getRequestDispatcher("user_organogram_history/user_organogram_historySearchForm.jsp");
        }
		rd.forward(request, response);
	}
	
}

