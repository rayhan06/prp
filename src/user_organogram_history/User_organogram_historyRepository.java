package user_organogram_history;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class User_organogram_historyRepository implements Repository {
	User_organogram_historyDAO user_organogram_historyDAO = null;
	
	public void setDAO(User_organogram_historyDAO user_organogram_historyDAO)
	{
		this.user_organogram_historyDAO = user_organogram_historyDAO;
	}
	
	
	static Logger logger = Logger.getLogger(User_organogram_historyRepository.class);
	Map<Long, User_organogram_historyDTO>mapOfUser_organogram_historyDTOToiD;
	Map<Long, Set<User_organogram_historyDTO> >mapOfUser_organogram_historyDTOTouserOrganogramId;
	Map<String, Set<User_organogram_historyDTO> >mapOfUser_organogram_historyDTOTousername;
	Map<Long, Set<User_organogram_historyDTO> >mapOfUser_organogram_historyDTOToaccessDate;
	Map<Long, Set<User_organogram_historyDTO> >mapOfUser_organogram_historyDTOTolastModificationTime;


	static User_organogram_historyRepository instance = null;  
	private User_organogram_historyRepository(){
		mapOfUser_organogram_historyDTOToiD = new ConcurrentHashMap<>();
		mapOfUser_organogram_historyDTOTouserOrganogramId = new ConcurrentHashMap<>();
		mapOfUser_organogram_historyDTOTousername = new ConcurrentHashMap<>();
		mapOfUser_organogram_historyDTOToaccessDate = new ConcurrentHashMap<>();
		mapOfUser_organogram_historyDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static User_organogram_historyRepository getInstance(){
		if (instance == null){
			instance = new User_organogram_historyRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(user_organogram_historyDAO == null)
		{
			return;
		}
		try {
			List<User_organogram_historyDTO> user_organogram_historyDTOs = user_organogram_historyDAO.getAllUser_organogram_history(reloadAll);
			for(User_organogram_historyDTO user_organogram_historyDTO : user_organogram_historyDTOs) {
				User_organogram_historyDTO oldUser_organogram_historyDTO = getUser_organogram_historyDTOByID(user_organogram_historyDTO.iD);
				if( oldUser_organogram_historyDTO != null ) {
					mapOfUser_organogram_historyDTOToiD.remove(oldUser_organogram_historyDTO.iD);
				
					if(mapOfUser_organogram_historyDTOTouserOrganogramId.containsKey(oldUser_organogram_historyDTO.userOrganogramId)) {
						mapOfUser_organogram_historyDTOTouserOrganogramId.get(oldUser_organogram_historyDTO.userOrganogramId).remove(oldUser_organogram_historyDTO);
					}
					if(mapOfUser_organogram_historyDTOTouserOrganogramId.get(oldUser_organogram_historyDTO.userOrganogramId).isEmpty()) {
						mapOfUser_organogram_historyDTOTouserOrganogramId.remove(oldUser_organogram_historyDTO.userOrganogramId);
					}
					
					if(mapOfUser_organogram_historyDTOTousername.containsKey(oldUser_organogram_historyDTO.username)) {
						mapOfUser_organogram_historyDTOTousername.get(oldUser_organogram_historyDTO.username).remove(oldUser_organogram_historyDTO);
					}
					if(mapOfUser_organogram_historyDTOTousername.get(oldUser_organogram_historyDTO.username).isEmpty()) {
						mapOfUser_organogram_historyDTOTousername.remove(oldUser_organogram_historyDTO.username);
					}
					
					if(mapOfUser_organogram_historyDTOToaccessDate.containsKey(oldUser_organogram_historyDTO.accessDate)) {
						mapOfUser_organogram_historyDTOToaccessDate.get(oldUser_organogram_historyDTO.accessDate).remove(oldUser_organogram_historyDTO);
					}
					if(mapOfUser_organogram_historyDTOToaccessDate.get(oldUser_organogram_historyDTO.accessDate).isEmpty()) {
						mapOfUser_organogram_historyDTOToaccessDate.remove(oldUser_organogram_historyDTO.accessDate);
					}
					
					if(mapOfUser_organogram_historyDTOTolastModificationTime.containsKey(oldUser_organogram_historyDTO.lastModificationTime)) {
						mapOfUser_organogram_historyDTOTolastModificationTime.get(oldUser_organogram_historyDTO.lastModificationTime).remove(oldUser_organogram_historyDTO);
					}
					if(mapOfUser_organogram_historyDTOTolastModificationTime.get(oldUser_organogram_historyDTO.lastModificationTime).isEmpty()) {
						mapOfUser_organogram_historyDTOTolastModificationTime.remove(oldUser_organogram_historyDTO.lastModificationTime);
					}
					
					
				}
				if(user_organogram_historyDTO.isDeleted == 0) 
				{
					
					mapOfUser_organogram_historyDTOToiD.put(user_organogram_historyDTO.iD, user_organogram_historyDTO);
				
					if( ! mapOfUser_organogram_historyDTOTouserOrganogramId.containsKey(user_organogram_historyDTO.userOrganogramId)) {
						mapOfUser_organogram_historyDTOTouserOrganogramId.put(user_organogram_historyDTO.userOrganogramId, new HashSet<>());
					}
					mapOfUser_organogram_historyDTOTouserOrganogramId.get(user_organogram_historyDTO.userOrganogramId).add(user_organogram_historyDTO);
					
					if( ! mapOfUser_organogram_historyDTOTousername.containsKey(user_organogram_historyDTO.username)) {
						mapOfUser_organogram_historyDTOTousername.put(user_organogram_historyDTO.username, new HashSet<>());
					}
					mapOfUser_organogram_historyDTOTousername.get(user_organogram_historyDTO.username).add(user_organogram_historyDTO);
					
					if( ! mapOfUser_organogram_historyDTOToaccessDate.containsKey(user_organogram_historyDTO.accessDate)) {
						mapOfUser_organogram_historyDTOToaccessDate.put(user_organogram_historyDTO.accessDate, new HashSet<>());
					}
					mapOfUser_organogram_historyDTOToaccessDate.get(user_organogram_historyDTO.accessDate).add(user_organogram_historyDTO);
					
					if( ! mapOfUser_organogram_historyDTOTolastModificationTime.containsKey(user_organogram_historyDTO.lastModificationTime)) {
						mapOfUser_organogram_historyDTOTolastModificationTime.put(user_organogram_historyDTO.lastModificationTime, new HashSet<>());
					}
					mapOfUser_organogram_historyDTOTolastModificationTime.get(user_organogram_historyDTO.lastModificationTime).add(user_organogram_historyDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<User_organogram_historyDTO> getUser_organogram_historyList() {
		List <User_organogram_historyDTO> user_organogram_historys = new ArrayList<User_organogram_historyDTO>(this.mapOfUser_organogram_historyDTOToiD.values());
		return user_organogram_historys;
	}
	
	
	public User_organogram_historyDTO getUser_organogram_historyDTOByID( long ID){
		return mapOfUser_organogram_historyDTOToiD.get(ID);
	}
	
	
	public List<User_organogram_historyDTO> getUser_organogram_historyDTOByuser_organogram_id(long user_organogram_id) {
		return new ArrayList<>( mapOfUser_organogram_historyDTOTouserOrganogramId.getOrDefault(user_organogram_id,new HashSet<>()));
	}
	
	
	public List<User_organogram_historyDTO> getUser_organogram_historyDTOByusername(String username) {
		return new ArrayList<>( mapOfUser_organogram_historyDTOTousername.getOrDefault(username,new HashSet<>()));
	}
	
	
	public List<User_organogram_historyDTO> getUser_organogram_historyDTOByaccess_date(long access_date) {
		return new ArrayList<>( mapOfUser_organogram_historyDTOToaccessDate.getOrDefault(access_date,new HashSet<>()));
	}
	
	
	public List<User_organogram_historyDTO> getUser_organogram_historyDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfUser_organogram_historyDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "user_organogram_history";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


