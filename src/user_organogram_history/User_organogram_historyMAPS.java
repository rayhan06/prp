package user_organogram_history;
import java.util.*; 
import util.*;


public class User_organogram_historyMAPS extends CommonMaps
{	
	public User_organogram_historyMAPS(String tableName)
	{
		
		java_allfield_type_map.put("user_organogram_id".toLowerCase(), "Long");
		java_allfield_type_map.put("username".toLowerCase(), "String");
		java_allfield_type_map.put("access_date".toLowerCase(), "Long");

		java_anyfield_search_map.put(tableName + ".user_organogram_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".username".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".access_date".toLowerCase(), "Long");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("userOrganogramId".toLowerCase(), "userOrganogramId".toLowerCase());
		java_DTO_map.put("username".toLowerCase(), "username".toLowerCase());
		java_DTO_map.put("accessDate".toLowerCase(), "accessDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("user_organogram_id".toLowerCase(), "userOrganogramId".toLowerCase());
		java_SQL_map.put("username".toLowerCase(), "username".toLowerCase());
		java_SQL_map.put("access_date".toLowerCase(), "accessDate".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("User Organogram Id".toLowerCase(), "userOrganogramId".toLowerCase());
		java_Text_map.put("Username".toLowerCase(), "username".toLowerCase());
		java_Text_map.put("Access Date".toLowerCase(), "accessDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}