package approval_role;

import dbm.DBMR;
import dbm.DBMW;
import login.LoginDTO;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.NavigationService;

import java.sql.*;
import java.util.*;


public class Approval_roleDAO implements NavigationService {

    Logger logger = Logger.getLogger(getClass());


    private void printSql(String sql) {
        logger.debug("sql: " + sql);
    }


    private void recordUpdateTime(Connection connection, long lastModificationTime) throws SQLException {
        recordUpdateTime(connection,"approval_role",lastModificationTime);
    }


    public void addApproval_role(Approval_roleDTO approval_roleDTO) throws Exception {

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();

        try {
            connection = DBMW.getInstance().getConnection();

            if (connection == null) {
                logger.debug("nullconn");
            }

            approval_roleDTO.iD = DBMW.getInstance().getNextSequenceId("Approval_role");

            String sql = "INSERT INTO approval_role";

            sql += " (";
            sql += "ID";
            sql += ", ";
            sql += "name_en";
            sql += ", ";
            sql += "name_bn";
            sql += ", ";
            sql += "description";
            sql += ", ";
            sql += "isDeleted";
            sql += ", lastModificationTime)";


            sql += " VALUES(";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ?)";

            printSql(sql);

            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, approval_roleDTO.iD);
            ps.setObject(index++, approval_roleDTO.nameEn);
            ps.setObject(index++, approval_roleDTO.nameBn);
            ps.setObject(index++, approval_roleDTO.description);
            ps.setObject(index++, approval_roleDTO.isDeleted);
            ps.setObject(index++, lastModificationTime);

            logger.debug(ps);
            ps.execute();


            recordUpdateTime(connection, lastModificationTime);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
    }


    //need another getter for repository
    public Approval_roleDTO getApproval_roleDTOByID(long ID) throws Exception {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Approval_roleDTO approval_roleDTO = null;
        try {

            String sql = "SELECT * ";

            sql += " FROM approval_role";

            sql += " WHERE ID=" + ID;

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                approval_roleDTO = new Approval_roleDTO();

                approval_roleDTO.iD = rs.getLong("ID");
                approval_roleDTO.nameEn = rs.getString("name_en");
                approval_roleDTO.nameBn = rs.getString("name_bn");
                approval_roleDTO.description = rs.getString("description");
                approval_roleDTO.isDeleted = rs.getBoolean("isDeleted");

            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return approval_roleDTO;
    }

    public void updateApproval_role(Approval_roleDTO approval_roleDTO) throws Exception {

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            connection = DBMW.getInstance().getConnection();

            String sql = "UPDATE approval_role";

            sql += " SET ";
            sql += "name_en=?";
            sql += ", ";
            sql += "name_bn=?";
            sql += ", ";
            sql += "description=?";
            sql += ", ";
            sql += "isDeleted=?";
            sql += ", lastModificationTime = " + lastModificationTime + "";
            sql += " WHERE ID = " + approval_roleDTO.iD;

            printSql(sql);

            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, approval_roleDTO.nameEn);
            ps.setObject(index++, approval_roleDTO.nameBn);
            ps.setObject(index++, approval_roleDTO.description);
            ps.setObject(index++, approval_roleDTO.isDeleted);
            logger.debug(ps);
            ps.executeUpdate();


            recordUpdateTime(connection, lastModificationTime);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

    }

    public void deleteApproval_roleByID(long ID) throws Exception {
        Connection connection = null;
        Statement stmt = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            String sql = "UPDATE approval_role";

            sql += " SET isDeleted=1,lastModificationTime=" + lastModificationTime + " WHERE ID = " + ID;

            printSql(sql);

            connection = DBMW.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.execute(sql);


            recordUpdateTime(connection, lastModificationTime);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
    }


    public List<Approval_roleDTO> getDTOs(Collection recordIDs, LoginDTO loginDTO) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Approval_roleDTO approval_roleDTO = null;
        List<Approval_roleDTO> approval_roleDTOList = new ArrayList<>();
        if (recordIDs.isEmpty()) {
            return approval_roleDTOList;
        }
        try {

            String sql = "SELECT * ";

            sql += " FROM approval_role";

            sql += " WHERE ID IN ( ";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql += ",";
                }
                sql += ((ArrayList) recordIDs).get(i);
            }
            sql += ")  order by ID desc";

            printSql(sql);

            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                approval_roleDTO = new Approval_roleDTO();
                approval_roleDTO.iD = rs.getLong("ID");
                approval_roleDTO.nameEn = rs.getString("name_en");
                approval_roleDTO.nameBn = rs.getString("name_bn");
                approval_roleDTO.description = rs.getString("description");
                approval_roleDTO.isDeleted = rs.getBoolean("isDeleted");
                logger.debug("got this DTO: " + approval_roleDTO);

                approval_roleDTOList.add(approval_roleDTO);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return approval_roleDTOList;

    }


    public Collection getIDs(LoginDTO loginDTO) {
        Collection data = new ArrayList();
        Connection connection = null;
        Statement stmt = null;
        ResultSet resultSet = null;

        String sql = "SELECT ID FROM approval_role";

        sql += " WHERE isDeleted = 0  order by ID desc ";

        printSql(sql);

        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();

            for (resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("ID"))) ;

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("DAO " + e, e);
        } finally {
            try {
                if (resultSet != null && !resultSet.isClosed()) {
                    resultSet.close();
                }
            } catch (Exception ex) {

            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception e) {
                logger.fatal("DAO finally :" + e);
            }
        }
        return data;
    }

    //add repository
    public List<Approval_roleDTO> getAllApproval_role(boolean isFirstReload) {
        List<Approval_roleDTO> approval_roleDTOList = new ArrayList<>();

        String sql = "SELECT * FROM approval_role";
        sql += " WHERE ";


        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        printSql(sql);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;


        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);


            while (rs.next()) {
                Approval_roleDTO approval_roleDTO = new Approval_roleDTO();
                approval_roleDTO.iD = rs.getLong("ID");
                approval_roleDTO.nameEn = rs.getString("name_en");
                approval_roleDTO.nameBn = rs.getString("name_bn");
                approval_roleDTO.description = rs.getString("description");
                approval_roleDTO.isDeleted = rs.getBoolean("isDeleted");
                int i = 0;
                long primaryKey = approval_roleDTO.iD;
                while (i < approval_roleDTOList.size() && approval_roleDTOList.get(i).iD < primaryKey) {
                    i++;
                }
                approval_roleDTOList.add(i, approval_roleDTO);
                //approval_roleDTOList.add(approval_roleDTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return approval_roleDTOList;
    }


    public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO) throws Exception {
        logger.debug("table: " + p_searchCriteria);
        List<Long> idList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement ps = null;

        try {

            String sql = "SELECT distinct approval_role.ID as ID FROM approval_role ";


            Enumeration names = p_searchCriteria.keys();
            String str, value;

            String AnyfieldSql = "(";

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                int i = 0;
                Iterator it = Approval_roleMAPS.GetInstance().java_anyfield_search_map.entrySet().iterator();
                while (it.hasNext()) {
                    if (i > 0) {
                        AnyfieldSql += " OR  ";
                    }
                    Map.Entry pair = (Map.Entry) it.next();
                    AnyfieldSql += pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
                    i++;
                }
            }
            AnyfieldSql += ")";
            logger.debug("AnyfieldSql = " + AnyfieldSql);

            String AllFieldSql = "(";
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                logger.debug(str + ": " + value);
                if (Approval_roleMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()) != null && !Approval_roleMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
                        && !str.equalsIgnoreCase("AnyField")
                        && value != null && !value.equalsIgnoreCase("")) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql += " AND  ";
                    }
                    if (Approval_roleMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equals("String")) {
                        AllFieldSql += "approval_role." + str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
                    } else {
                        AllFieldSql += "approval_role." + str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
                    }
                    i++;
                }
            }

            AllFieldSql += ")";

            logger.debug("AllFieldSql = " + AllFieldSql);


            sql += " WHERE ";
            sql += " approval_role.isDeleted = false";


            if (!AnyfieldSql.equals("()")) {
                sql += " AND " + AnyfieldSql;

            }
            if (!AllFieldSql.equals("()")) {
                sql += " AND " + AllFieldSql;
            }

            sql += " order by approval_role.ID desc ";

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                idList.add(rs.getLong("ID"));
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return idList;
    }


}
	