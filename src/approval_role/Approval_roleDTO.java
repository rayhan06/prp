package approval_role;
import java.util.*; 


public class Approval_roleDTO {

	public long iD = 0;
    public String nameEn = "";
    public String nameBn = "";
    public String description = "";
	public boolean isDeleted = false;
	
	
    @Override
	public String toString() {
            return "$Approval_roleDTO[" +
            " id = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " description = " + description +
            " isDeleted = " + isDeleted +
            "]";
    }

}