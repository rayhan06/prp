package approval_role;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Approval_roleRepository implements Repository {
	Approval_roleDAO approval_roleDAO = new Approval_roleDAO();
	
	
	static Logger logger = Logger.getLogger(Approval_roleRepository.class);
	Map<Long, Approval_roleDTO>mapOfApproval_roleDTOToiD;
	Map<String, Set<Approval_roleDTO> >mapOfApproval_roleDTOTonameEn;
	Map<String, Set<Approval_roleDTO> >mapOfApproval_roleDTOTonameBn;
	Map<String, Set<Approval_roleDTO> >mapOfApproval_roleDTOTodescription;


	static Approval_roleRepository instance = null;  
	private Approval_roleRepository(){
		mapOfApproval_roleDTOToiD = new ConcurrentHashMap<>();
		mapOfApproval_roleDTOTonameEn = new ConcurrentHashMap<>();
		mapOfApproval_roleDTOTonameBn = new ConcurrentHashMap<>();
		mapOfApproval_roleDTOTodescription = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Approval_roleRepository getInstance(){
		if (instance == null){
			instance = new Approval_roleRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		try {
			List<Approval_roleDTO> approval_roleDTOs = approval_roleDAO.getAllApproval_role(reloadAll);
			for(Approval_roleDTO approval_roleDTO : approval_roleDTOs) {
				Approval_roleDTO oldApproval_roleDTO = getApproval_roleDTOByID(approval_roleDTO.iD);
				if( oldApproval_roleDTO != null ) {
					mapOfApproval_roleDTOToiD.remove(oldApproval_roleDTO.iD);
				
					if(mapOfApproval_roleDTOTonameEn.containsKey(oldApproval_roleDTO.nameEn)) {
						mapOfApproval_roleDTOTonameEn.get(oldApproval_roleDTO.nameEn).remove(oldApproval_roleDTO);
					}
					if(mapOfApproval_roleDTOTonameEn.get(oldApproval_roleDTO.nameEn).isEmpty()) {
						mapOfApproval_roleDTOTonameEn.remove(oldApproval_roleDTO.nameEn);
					}
					
					if(mapOfApproval_roleDTOTonameBn.containsKey(oldApproval_roleDTO.nameBn)) {
						mapOfApproval_roleDTOTonameBn.get(oldApproval_roleDTO.nameBn).remove(oldApproval_roleDTO);
					}
					if(mapOfApproval_roleDTOTonameBn.get(oldApproval_roleDTO.nameBn).isEmpty()) {
						mapOfApproval_roleDTOTonameBn.remove(oldApproval_roleDTO.nameBn);
					}
					
					if(mapOfApproval_roleDTOTodescription.containsKey(oldApproval_roleDTO.description)) {
						mapOfApproval_roleDTOTodescription.get(oldApproval_roleDTO.description).remove(oldApproval_roleDTO);
					}
					if(mapOfApproval_roleDTOTodescription.get(oldApproval_roleDTO.description).isEmpty()) {
						mapOfApproval_roleDTOTodescription.remove(oldApproval_roleDTO.description);
					}
					
					
				}
				if(approval_roleDTO.isDeleted == false) 
				{
					
					mapOfApproval_roleDTOToiD.put(approval_roleDTO.iD, approval_roleDTO);
				
					if( ! mapOfApproval_roleDTOTonameEn.containsKey(approval_roleDTO.nameEn)) {
						mapOfApproval_roleDTOTonameEn.put(approval_roleDTO.nameEn, new HashSet<>());
					}
					mapOfApproval_roleDTOTonameEn.get(approval_roleDTO.nameEn).add(approval_roleDTO);
					
					if( ! mapOfApproval_roleDTOTonameBn.containsKey(approval_roleDTO.nameBn)) {
						mapOfApproval_roleDTOTonameBn.put(approval_roleDTO.nameBn, new HashSet<>());
					}
					mapOfApproval_roleDTOTonameBn.get(approval_roleDTO.nameBn).add(approval_roleDTO);
					
					if( ! mapOfApproval_roleDTOTodescription.containsKey(approval_roleDTO.description)) {
						mapOfApproval_roleDTOTodescription.put(approval_roleDTO.description, new HashSet<>());
					}
					mapOfApproval_roleDTOTodescription.get(approval_roleDTO.description).add(approval_roleDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Approval_roleDTO> getApproval_roleList() {
		List <Approval_roleDTO> approval_roles = new ArrayList<Approval_roleDTO>(this.mapOfApproval_roleDTOToiD.values());
		return approval_roles;
	}
	
	
	public Approval_roleDTO getApproval_roleDTOByID( long ID){
		return mapOfApproval_roleDTOToiD.get(ID);
	}
	
	
	public List<Approval_roleDTO> getApproval_roleDTOByname_en(String name_en) {
		return new ArrayList<>( mapOfApproval_roleDTOTonameEn.getOrDefault(name_en,new HashSet<>()));
	}
	
	
	public List<Approval_roleDTO> getApproval_roleDTOByname_bn(String name_bn) {
		return new ArrayList<>( mapOfApproval_roleDTOTonameBn.getOrDefault(name_bn,new HashSet<>()));
	}
	
	
	public List<Approval_roleDTO> getApproval_roleDTOBydescription(String description) {
		return new ArrayList<>( mapOfApproval_roleDTOTodescription.getOrDefault(description,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "approval_role";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


