package files;
import java.util.*; 
import util.*;


public class FilesMAPS extends CommonMaps
{	
	public FilesMAPS(String tableName)
	{
		
		java_allfield_type_map.put("file_id".toLowerCase(), "Long");
		java_allfield_type_map.put("file_types".toLowerCase(), "String");
		java_allfield_type_map.put("file_tag".toLowerCase(), "String");
		java_allfield_type_map.put("file_title".toLowerCase(), "String");
		java_allfield_type_map.put("user_id".toLowerCase(), "Long");
		java_allfield_type_map.put("source".toLowerCase(), "String");
		java_allfield_type_map.put("created_at".toLowerCase(), "Long");


		java_anyfield_search_map.put(tableName + ".file_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".file_types".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".file_tag".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".file_title".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".user_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".source".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".created_at".toLowerCase(), "Long");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("fileId".toLowerCase(), "fileId".toLowerCase());
		java_DTO_map.put("fileTypes".toLowerCase(), "fileTypes".toLowerCase());
		java_DTO_map.put("fileBlob".toLowerCase(), "fileBlob".toLowerCase());
		java_DTO_map.put("thumbnailBlob".toLowerCase(), "thumbnailBlob".toLowerCase());
		java_DTO_map.put("fileTag".toLowerCase(), "fileTag".toLowerCase());
		java_DTO_map.put("fileTitle".toLowerCase(), "fileTitle".toLowerCase());
		java_DTO_map.put("userId".toLowerCase(), "userId".toLowerCase());
		java_DTO_map.put("source".toLowerCase(), "source".toLowerCase());
		java_DTO_map.put("createdAt".toLowerCase(), "createdAt".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("file_id".toLowerCase(), "fileId".toLowerCase());
		java_SQL_map.put("file_types".toLowerCase(), "fileTypes".toLowerCase());
		java_SQL_map.put("file_blob".toLowerCase(), "fileBlob".toLowerCase());
		java_SQL_map.put("thumbnail_blob".toLowerCase(), "thumbnailBlob".toLowerCase());
		java_SQL_map.put("file_tag".toLowerCase(), "fileTag".toLowerCase());
		java_SQL_map.put("file_title".toLowerCase(), "fileTitle".toLowerCase());
		java_SQL_map.put("user_id".toLowerCase(), "userId".toLowerCase());
		java_SQL_map.put("source".toLowerCase(), "source".toLowerCase());
		java_SQL_map.put("created_at".toLowerCase(), "createdAt".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("File Id".toLowerCase(), "fileId".toLowerCase());
		java_Text_map.put("File Types".toLowerCase(), "fileTypes".toLowerCase());
		java_Text_map.put("File Blob".toLowerCase(), "fileBlob".toLowerCase());
		java_Text_map.put("Thumbnail Blob".toLowerCase(), "thumbnailBlob".toLowerCase());
		java_Text_map.put("File Tag".toLowerCase(), "fileTag".toLowerCase());
		java_Text_map.put("File Title".toLowerCase(), "fileTitle".toLowerCase());
		java_Text_map.put("User Id".toLowerCase(), "userId".toLowerCase());
		java_Text_map.put("Source".toLowerCase(), "source".toLowerCase());
		java_Text_map.put("Created At".toLowerCase(), "createdAt".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}