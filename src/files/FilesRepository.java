package files;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class FilesRepository implements Repository {
	FilesDAO filesDAO = null;
	
	public void setDAO(FilesDAO filesDAO)
	{
		this.filesDAO = filesDAO;
	}
	
	
	static Logger logger = Logger.getLogger(FilesRepository.class);
	Map<Long, FilesDTO>mapOfFilesDTOToiD;
	Map<Long, Set<FilesDTO> >mapOfFilesDTOTofileId;
	Map<String, Set<FilesDTO> >mapOfFilesDTOTofileTypes;
	Map<String, Set<FilesDTO> >mapOfFilesDTOTofileTag;
	Map<String, Set<FilesDTO> >mapOfFilesDTOTofileTitle;
	Map<Long, Set<FilesDTO> >mapOfFilesDTOTouserId;
	Map<String, Set<FilesDTO> >mapOfFilesDTOTosource;
	Map<Long, Set<FilesDTO> >mapOfFilesDTOTocreatedAt;
	Map<Long, Set<FilesDTO> >mapOfFilesDTOTolastModificationTime;


	static FilesRepository instance = null;  
	private FilesRepository(){
		mapOfFilesDTOToiD = new ConcurrentHashMap<>();
		mapOfFilesDTOTofileId = new ConcurrentHashMap<>();
		mapOfFilesDTOTofileTypes = new ConcurrentHashMap<>();
		mapOfFilesDTOTofileTag = new ConcurrentHashMap<>();
		mapOfFilesDTOTofileTitle = new ConcurrentHashMap<>();
		mapOfFilesDTOTouserId = new ConcurrentHashMap<>();
		mapOfFilesDTOTosource = new ConcurrentHashMap<>();
		mapOfFilesDTOTocreatedAt = new ConcurrentHashMap<>();
		mapOfFilesDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static FilesRepository getInstance(){
		if (instance == null){
			instance = new FilesRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(filesDAO == null)
		{
			return;
		}
		try {
			List<FilesDTO> filesDTOs = filesDAO.getAllFiles(reloadAll);
			for(FilesDTO filesDTO : filesDTOs) {
				FilesDTO oldFilesDTO = getFilesDTOByID(filesDTO.iD);
				if( oldFilesDTO != null ) {
					mapOfFilesDTOToiD.remove(oldFilesDTO.iD);
				
					if(mapOfFilesDTOTofileId.containsKey(oldFilesDTO.fileID)) {
						mapOfFilesDTOTofileId.get(oldFilesDTO.fileID).remove(oldFilesDTO);
					}
					if(mapOfFilesDTOTofileId.get(oldFilesDTO.fileID).isEmpty()) {
						mapOfFilesDTOTofileId.remove(oldFilesDTO.fileID);
					}
					
					if(mapOfFilesDTOTofileTypes.containsKey(oldFilesDTO.fileTypes)) {
						mapOfFilesDTOTofileTypes.get(oldFilesDTO.fileTypes).remove(oldFilesDTO);
					}
					if(mapOfFilesDTOTofileTypes.get(oldFilesDTO.fileTypes).isEmpty()) {
						mapOfFilesDTOTofileTypes.remove(oldFilesDTO.fileTypes);
					}
					
					if(mapOfFilesDTOTofileTag.containsKey(oldFilesDTO.fileTag)) {
						mapOfFilesDTOTofileTag.get(oldFilesDTO.fileTag).remove(oldFilesDTO);
					}
					if(mapOfFilesDTOTofileTag.get(oldFilesDTO.fileTag).isEmpty()) {
						mapOfFilesDTOTofileTag.remove(oldFilesDTO.fileTag);
					}
					
					if(mapOfFilesDTOTofileTitle.containsKey(oldFilesDTO.fileTitle)) {
						mapOfFilesDTOTofileTitle.get(oldFilesDTO.fileTitle).remove(oldFilesDTO);
					}
					if(mapOfFilesDTOTofileTitle.get(oldFilesDTO.fileTitle).isEmpty()) {
						mapOfFilesDTOTofileTitle.remove(oldFilesDTO.fileTitle);
					}
					
					if(mapOfFilesDTOTouserId.containsKey(oldFilesDTO.userId)) {
						mapOfFilesDTOTouserId.get(oldFilesDTO.userId).remove(oldFilesDTO);
					}
					if(mapOfFilesDTOTouserId.get(oldFilesDTO.userId).isEmpty()) {
						mapOfFilesDTOTouserId.remove(oldFilesDTO.userId);
					}
					
					if(mapOfFilesDTOTosource.containsKey(oldFilesDTO.source)) {
						mapOfFilesDTOTosource.get(oldFilesDTO.source).remove(oldFilesDTO);
					}
					if(mapOfFilesDTOTosource.get(oldFilesDTO.source).isEmpty()) {
						mapOfFilesDTOTosource.remove(oldFilesDTO.source);
					}
					
					if(mapOfFilesDTOTocreatedAt.containsKey(oldFilesDTO.createdAt)) {
						mapOfFilesDTOTocreatedAt.get(oldFilesDTO.createdAt).remove(oldFilesDTO);
					}
					if(mapOfFilesDTOTocreatedAt.get(oldFilesDTO.createdAt).isEmpty()) {
						mapOfFilesDTOTocreatedAt.remove(oldFilesDTO.createdAt);
					}
					
					if(mapOfFilesDTOTolastModificationTime.containsKey(oldFilesDTO.lastModificationTime)) {
						mapOfFilesDTOTolastModificationTime.get(oldFilesDTO.lastModificationTime).remove(oldFilesDTO);
					}
					if(mapOfFilesDTOTolastModificationTime.get(oldFilesDTO.lastModificationTime).isEmpty()) {
						mapOfFilesDTOTolastModificationTime.remove(oldFilesDTO.lastModificationTime);
					}
					
					
				}
				if(filesDTO.isDeleted == 0) 
				{
					
					mapOfFilesDTOToiD.put(filesDTO.iD, filesDTO);
				
					if( ! mapOfFilesDTOTofileId.containsKey(filesDTO.fileID)) {
						mapOfFilesDTOTofileId.put(filesDTO.fileID, new HashSet<>());
					}
					mapOfFilesDTOTofileId.get(filesDTO.fileID).add(filesDTO);
					
					if( ! mapOfFilesDTOTofileTypes.containsKey(filesDTO.fileTypes)) {
						mapOfFilesDTOTofileTypes.put(filesDTO.fileTypes, new HashSet<>());
					}
					mapOfFilesDTOTofileTypes.get(filesDTO.fileTypes).add(filesDTO);
					
					if( ! mapOfFilesDTOTofileTag.containsKey(filesDTO.fileTag)) {
						mapOfFilesDTOTofileTag.put(filesDTO.fileTag, new HashSet<>());
					}
					mapOfFilesDTOTofileTag.get(filesDTO.fileTag).add(filesDTO);
					
					if( ! mapOfFilesDTOTofileTitle.containsKey(filesDTO.fileTitle)) {
						mapOfFilesDTOTofileTitle.put(filesDTO.fileTitle, new HashSet<>());
					}
					mapOfFilesDTOTofileTitle.get(filesDTO.fileTitle).add(filesDTO);
					
					if( ! mapOfFilesDTOTouserId.containsKey(filesDTO.userId)) {
						mapOfFilesDTOTouserId.put(filesDTO.userId, new HashSet<>());
					}
					mapOfFilesDTOTouserId.get(filesDTO.userId).add(filesDTO);
					
					if( ! mapOfFilesDTOTosource.containsKey(filesDTO.source)) {
						mapOfFilesDTOTosource.put(filesDTO.source, new HashSet<>());
					}
					mapOfFilesDTOTosource.get(filesDTO.source).add(filesDTO);
					
					if( ! mapOfFilesDTOTocreatedAt.containsKey(filesDTO.createdAt)) {
						mapOfFilesDTOTocreatedAt.put(filesDTO.createdAt, new HashSet<>());
					}
					mapOfFilesDTOTocreatedAt.get(filesDTO.createdAt).add(filesDTO);
					
					if( ! mapOfFilesDTOTolastModificationTime.containsKey(filesDTO.lastModificationTime)) {
						mapOfFilesDTOTolastModificationTime.put(filesDTO.lastModificationTime, new HashSet<>());
					}
					mapOfFilesDTOTolastModificationTime.get(filesDTO.lastModificationTime).add(filesDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<FilesDTO> getFilesList() {
		List <FilesDTO> filess = new ArrayList<FilesDTO>(this.mapOfFilesDTOToiD.values());
		return filess;
	}
	
	
	public FilesDTO getFilesDTOByID( long ID){
		return mapOfFilesDTOToiD.get(ID);
	}
	
	
	public List<FilesDTO> getFilesDTOByfile_id(long file_id) {
		return new ArrayList<>( mapOfFilesDTOTofileId.getOrDefault(file_id,new HashSet<>()));
	}
	
	
	public List<FilesDTO> getFilesDTOByfile_types(String file_types) {
		return new ArrayList<>( mapOfFilesDTOTofileTypes.getOrDefault(file_types,new HashSet<>()));
	}
	
	
	public List<FilesDTO> getFilesDTOByfile_tag(String file_tag) {
		return new ArrayList<>( mapOfFilesDTOTofileTag.getOrDefault(file_tag,new HashSet<>()));
	}
	
	
	public List<FilesDTO> getFilesDTOByfile_title(String file_title) {
		return new ArrayList<>( mapOfFilesDTOTofileTitle.getOrDefault(file_title,new HashSet<>()));
	}
	
	
	public List<FilesDTO> getFilesDTOByuser_id(long user_id) {
		return new ArrayList<>( mapOfFilesDTOTouserId.getOrDefault(user_id,new HashSet<>()));
	}
	
	
	public List<FilesDTO> getFilesDTOBysource(String source) {
		return new ArrayList<>( mapOfFilesDTOTosource.getOrDefault(source,new HashSet<>()));
	}
	
	
	public List<FilesDTO> getFilesDTOBycreated_at(long created_at) {
		return new ArrayList<>( mapOfFilesDTOTocreatedAt.getOrDefault(created_at,new HashSet<>()));
	}
	
	
	public List<FilesDTO> getFilesDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfFilesDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "files";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


