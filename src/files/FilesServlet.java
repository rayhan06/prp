package files;

import java.io.IOException;
import java.io.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;

import util.RecordNavigationManager3;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;

import files.Constants;
import approval_module_map.*;



import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;


/**
 * Servlet implementation class FilesServlet
 */
@WebServlet("/FilesServlet")
@MultipartConfig
public class FilesServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(FilesServlet.class);
	Approval_module_mapDAO approval_module_mapDAO = new Approval_module_mapDAO();
    Approval_module_mapDTO approval_module_mapDTO;
    String tableName = "files";
    String tempTableName = "files_temp";
	FilesDAO filesDAO;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FilesServlet() 
	{
        super();
    	try
    	{
			approval_module_mapDTO = approval_module_mapDAO.getApproval_module_mapDTOByTableName("files");
			filesDAO = new FilesDAO(tableName, tempTableName, approval_module_mapDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("",e);
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FILES_ADD))
				{
					getAddPage(request, response);
				}
				
			}
			else if(actionType.equals("getEditPage"))
			{
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FILES_UPDATE))
				{
					if(isPermanentTable)
					{
						getFiles(request, response, tableName);
					}
					else
					{
						getFiles(request, response, tempTableName);
					}
				}
									
			}
			else if(actionType.equals("downloadBlob"))
			{
				long id = Long.parseLong(request.getParameter("id"));
				String name = request.getParameter("name");
				FilesDTO filesDTO = (FilesDTO)filesDAO.getDTOByID(id);
				if(name.equalsIgnoreCase("fileBlob"))
				{
					Utils.ProcessFile(request, response, filesDTO.fileTitle, filesDTO.inputStream);
				}
				if(name.equalsIgnoreCase("thumbnailBlob"))
				{
					Utils.ProcessFile(request, response, "thumbnailBlob.jpg", filesDTO.thumbnailBlob);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FILES_SEARCH))
				{
					
					if(isPermanentTable)
					{
						searchFiles(request, response, tableName, isPermanentTable);
					}
					else
					{
						searchFiles(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("getApprovalPage"))
			{
				System.out.println("getApprovalPage requested");
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FILES_SEARCH))
				{
					searchFiles(request, response, tempTableName, false);
				}
							
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			logger.error("",ex);
			
		}
	}

	private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		request.setAttribute("ID", -1L);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("files/filesEdit.jsp");
		requestDispatcher.forward(request, response);
	}
	private String getFileName(final Part part) 
	{
	    final String partHeader = part.getHeader("content-disposition");
	    System.out.println("Part Header = {0}" +  partHeader);
	    for (String content : part.getHeader("content-disposition").split(";")) {
	        if (content.trim().startsWith("filename")) {
	            return content.substring(
	                    content.indexOf('=') + 1).trim().replace("\"", "");
	        }
	    }
	    return null;
	}

	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FILES_ADD))
				{
					System.out.println("going to  addFiles ");
					addFiles(request, response, true, userDTO, tableName, true);
				}
				
				
			}
			if(actionType.equals("approve"))
			{
				
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FILES_ADD))
				{					
					approveFiles(request, response, true, userDTO);
				}
			
				
			}
			if(actionType.equals("getDTO"))
			{
				
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FILES_ADD))
				{
					if(isPermanentTable)
					{
						getDTO(request, response, tableName);
					}
					else
					{
						getDTO(request, response, tempTableName);
					}
				}
				
				
			}
			else if(actionType.equals("edit"))
			{
				
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FILES_UPDATE))
				{
					if(isPermanentTable)
					{
						addFiles(request, response, false, userDTO, tableName, isPermanentTable);
					}
					else
					{
						addFiles(request, response, false, userDTO, tempTableName, isPermanentTable);
					}
				}
				
			}
			else if(actionType.equals("delete"))
			{								
				deleteFiles(request, response, userDTO, isPermanentTable);				
			}	
			else if(actionType.equals("search"))
			{
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FILES_SEARCH))
				{
					searchFiles(request, response, tableName, true);
				}
			
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			logger.error("",ex);
			
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response, String tableName) 
	{
		try 
		{
			System.out.println("In getDTO");
			FilesDTO filesDTO = (FilesDTO)filesDAO.getDTOByID(Long.parseLong(request.getParameter("ID")), tableName);
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(filesDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			logger.error("",e);
		}

	}
	private void approveFiles(HttpServletRequest request, HttpServletResponse response, boolean b, UserDTO userDTO) 
	{
		try
		{
			long id = Long.parseLong(request.getParameter("idToApprove"));
			FilesDTO filesDTO = (FilesDTO)filesDAO.getDTOByID(id, tempTableName);
			filesDAO.manageWriteOperations(filesDTO, SessionConstants.APPROVE, id, userDTO);
		}
		catch (Exception e) 
		{
			logger.error("",e);
		}
		
	}
	private void addFiles(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, String tableName, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addFiles");
			String path = getServletContext().getRealPath("/img2/");
			FilesDTO filesDTO;
			String FileNamePrefix;			
			String DTOID = "";
			if(addFlag == true)
			{
				filesDTO = new FilesDTO();
				FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
				DTOID = request.getParameter("DTOID");
			}
			else
			{
				filesDTO = (FilesDTO)filesDAO.getDTOByID(Long.parseLong(request.getParameter("identity")), tableName);
				FileNamePrefix = request.getParameter("identity");
			}
			
			String Value = "";
			Value = request.getParameter("fileId");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("fileId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				filesDTO.fileID = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("fileTypes");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("fileTypes = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				filesDTO.fileTypes = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Part filePart_fileBlob =  request.getPart("fileBlob");
			Value = getFileName(filePart_fileBlob);
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("fileBlob = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				if(Value.toLowerCase().endsWith(".jpg") || Value.toLowerCase().endsWith(".png") 
					|| Value.toLowerCase().endsWith(".gif") || Value.toLowerCase().endsWith(".bmp") || Value.toLowerCase().endsWith(".ico"))
				{
					//filesDTO.fileBlob = Utils.uploadFileToByteAray(filePart_fileBlob);
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Part filePart_thumbnailBlob =  request.getPart("thumbnailBlob");
			Value = getFileName(filePart_thumbnailBlob);
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("thumbnailBlob = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				if(Value.toLowerCase().endsWith(".jpg") || Value.toLowerCase().endsWith(".png") 
					|| Value.toLowerCase().endsWith(".gif") || Value.toLowerCase().endsWith(".bmp") || Value.toLowerCase().endsWith(".ico"))
				{
					filesDTO.thumbnailBlob = Utils.uploadFileToByteAray(filePart_thumbnailBlob);
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("fileTag");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("fileTag = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				filesDTO.fileTag = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("fileTitle");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("fileTitle = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				filesDTO.fileTitle = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("userId");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("userId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				filesDTO.userId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("source");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("source = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				filesDTO.source = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("createdAt");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("createdAt = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				filesDTO.createdAt = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addFiles dto = " + filesDTO);
			
			if(addFlag == true)
			{
				filesDAO.manageWriteOperations(filesDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				if(isPermanentTable)
				{
					filesDAO.manageWriteOperations(filesDTO, SessionConstants.UPDATE, -1, userDTO);
				}
				else
				{
					filesDAO.manageWriteOperations(filesDTO, SessionConstants.VALIDATE, -1, userDTO);
				}				
				
			}
			
			
			
			
			
			
			
			
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			
			if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				getFiles(request, response, tableName);
			}
			else
			{
				response.sendRedirect("FilesServlet?actionType=search");
			}
					
		}
		catch (Exception e) 
		{
			logger.error("",e);
		}
	}
	
	



	
	
	

	private void deleteFiles(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, boolean deleteOrReject) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				if(deleteOrReject)
				{
					FilesDTO filesDTO = (FilesDTO)filesDAO.getDTOByID(id);
					filesDAO.manageWriteOperations(filesDTO, SessionConstants.DELETE, id, userDTO);
					response.sendRedirect("FilesServlet?actionType=search");
				}
				else
				{
					FilesDTO filesDTO = (FilesDTO)filesDAO.getDTOByID(id, tempTableName);
					filesDAO.manageWriteOperations(filesDTO, SessionConstants.REJECT, id, userDTO);					
					response.sendRedirect("FilesServlet?actionType=getApprovalPage");
				}
			}			
		}
		catch (Exception ex) 
		{
			logger.error("",ex);
		}
		
	}

	private void getFiles(HttpServletRequest request, HttpServletResponse response, String tableName) throws ServletException, IOException
	{
		System.out.println("in getFiles");
		FilesDTO filesDTO = null;
		try 
		{
			filesDTO = (FilesDTO)filesDAO.getDTOByID(Long.parseLong(request.getParameter("ID")), tableName);
			request.setAttribute("ID", filesDTO.iD);
			request.setAttribute("filesDTO",filesDTO);
			request.setAttribute("filesDAO",filesDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "files/filesInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "files/filesSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "files/filesEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "files/filesEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			logger.error("",e);
		}
		catch (Exception e) 
		{
			logger.error("",e);
		}
	}
	
	private void searchFiles(HttpServletRequest request, HttpServletResponse response, String tableName, boolean isPermanent) throws ServletException, IOException
	{
		System.out.println("in  searchFiles 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager3 rnManager = new RecordNavigationManager3(
			SessionConstants.NAV_FILES,
			request,
			filesDAO,
			SessionConstants.VIEW_FILES,
			SessionConstants.SEARCH_FILES,
			tableName,
			isPermanent,
			userDTO.approvalPathID);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("filesDAO",filesDAO);
        RequestDispatcher rd;
        if(hasAjax == false)
        {
        	System.out.println("Going to files/filesSearch.jsp");
        	rd = request.getRequestDispatcher("files/filesSearch.jsp");
        }
        else
        {
        	System.out.println("Going to files/filesSearchForm.jsp");
        	rd = request.getRequestDispatcher("files/filesSearchForm.jsp");
        }
		rd.forward(request, response);
	}
	
}

