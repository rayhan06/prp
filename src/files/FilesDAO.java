package files;

import java.io.InputStream;
import java.sql.*;
import java.util.*;

import common.ConnectionAndStatementUtil;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import approval_module_map.*;

public class FilesDAO  extends NavigationService3
{
	
	Logger logger = Logger.getLogger(getClass());
	private static final String getByFileId = "select count(*) from files where file_id = %d and isDeleted = 0;";
	
	public FilesDAO(String tableName, String tempTableName, Approval_module_mapDTO approval_module_mapDTO)
	{
		super(tableName, tempTableName, approval_module_mapDTO);		
	}
	
	public FilesDAO()
	{
		super("files", "", null);		
	}
	
	
	
	public long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception
	{
		
		FilesDTO filesDTO = (FilesDTO)commonDTO;
		System.out.println("arrived file_id = " + filesDTO.fileID);
		System.out.println("arrived userID = " + filesDTO.userId);
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			filesDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "file_id";
			sql += ", ";
			sql += "file_types";
			sql += ", ";
			sql += "file_blob";
			sql += ", ";
			sql += "thumbnail_blob";
			sql += ", ";
			sql += "file_tag";
			sql += ", ";
			sql += "file_title";
			sql += ", ";
			sql += "user_id";
			sql += ", ";
			sql += "source";
			sql += ", ";
			sql += "created_at";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
			if(tempTableDTO!=null)
			{
				sql += ", permanent_table_id, operation_type, approval_path_type, approval_order";
			}
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			if(tempTableDTO!=null)
			{
				sql += ", " + tempTableDTO.permanent_table_id + ", " + tempTableDTO.operation_type + ", " + tempTableDTO.approval_path_type + ", " + tempTableDTO.approval_order;
			}
			sql += ")";
				

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,filesDTO.iD);
			ps.setObject(index++,filesDTO.fileID);
			ps.setObject(index++,filesDTO.fileTypes);
			ps.setBinaryStream(index++, filesDTO.inputStream, filesDTO.inputStream.available());
			ps.setBytes(index++,filesDTO.thumbnailBlob);
			ps.setObject(index++,filesDTO.fileTag);
			ps.setObject(index++,filesDTO.fileTitle);
			ps.setObject(index++,filesDTO.userId);
			ps.setObject(index++,filesDTO.source);
			ps.setObject(index++,filesDTO.createdAt);
			ps.setObject(index++,filesDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println("adding file_id = " + filesDTO.fileID);
			System.out.println("adding userID = " + filesDTO.userId);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime, tableName);

		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return filesDTO.iD;		
	}
	
	public List<FilesDTO> getMiniDTOsByFileID (long ID) throws Exception
	{
		if(ID == -1)
		{
			return new ArrayList<>();
		}
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		FilesDTO filesDTO = null;
		List<FilesDTO> filesDTOList = new ArrayList<>();
		try{
			
			String sql = "SELECT ID, file_id, file_types, thumbnail_blob, file_tag, file_title ";

			sql += " FROM " + tableName;
			
            sql += " WHERE isDeleted = 0 and file_id=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				filesDTO = new FilesDTO();

				filesDTO.iD = rs.getLong("ID");
				filesDTO.fileID = rs.getLong("file_id");
				filesDTO.fileTypes = rs.getString("file_types");
				
				InputStream inputStream = rs.getBinaryStream("thumbnail_blob");
				if(inputStream != null)
				{
					filesDTO.thumbnailBlob = IOUtils.toByteArray(inputStream);
				}
				filesDTO.fileTag = rs.getString("file_tag");
				filesDTO.fileTitle = rs.getString("file_title");

				
				filesDTOList.add(filesDTO);

			}			
			
			
		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return filesDTOList;
	}
	
	public FilesDTO getMiniDTOById (long ID) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		FilesDTO filesDTO = null;
		try{
			
			String sql = "SELECT ID, file_id, file_types, thumbnail_blob, file_tag, file_title ";

			sql += " FROM " + tableName;
			
            sql += " WHERE isDeleted = 0 and id=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				filesDTO = new FilesDTO();

				filesDTO.iD = rs.getLong("ID");
				filesDTO.fileID = rs.getLong("file_id");
				filesDTO.fileTypes = rs.getString("file_types");
				
				InputStream inputStream = rs.getBinaryStream("thumbnail_blob");
				if(inputStream != null)
				{
					filesDTO.thumbnailBlob = IOUtils.toByteArray(inputStream);
				}
				filesDTO.fileTag = rs.getString("file_tag");
				filesDTO.fileTitle = rs.getString("file_title");


			}			
			
			
		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return filesDTO;
	}
		
	public List<FilesDTO> getDTOsByFileID (long ID) throws Exception
	{
		if(ID == -1)
		{
			return new ArrayList<>();
		}
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		FilesDTO filesDTO = null;
		List<FilesDTO> filesDTOList = new ArrayList<>();
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE isDeleted = 0 and file_id=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				filesDTO = new FilesDTO();

				filesDTO.iD = rs.getLong("ID");
				filesDTO.fileID = rs.getLong("file_id");
				filesDTO.fileTypes = rs.getString("file_types");
				filesDTO.inputStream = rs.getBinaryStream("file_blob");
				filesDTO.fileBlob = IOUtils.toByteArray(filesDTO.inputStream);
				InputStream inputStream = rs.getBinaryStream("thumbnail_blob");
				if(inputStream != null)
				{
					filesDTO.thumbnailBlob = IOUtils.toByteArray(inputStream);
				}
				filesDTO.fileTag = rs.getString("file_tag");
				filesDTO.fileTitle = rs.getString("file_title");
				filesDTO.userId = rs.getLong("user_id");
				filesDTO.source = rs.getString("source");
				filesDTO.createdAt = rs.getLong("created_at");
				filesDTO.isDeleted = rs.getInt("isDeleted");
				filesDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				filesDTOList.add(filesDTO);

			}			
			
			
		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return filesDTOList;
	}

	//need another getter for repository
	public CommonDTO getDTOByID (long ID, String tableName) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		FilesDTO filesDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);
			
			System.out.println("query executed");

			if(rs.next()){
				filesDTO = new FilesDTO();

				filesDTO.iD = rs.getLong("ID");
				filesDTO.fileID = rs.getLong("file_id");
				filesDTO.fileTypes = rs.getString("file_types");
				filesDTO.inputStream = rs.getBinaryStream("file_blob");
				InputStream inputStream = rs.getBinaryStream("thumbnail_blob");
				if(inputStream != null)
				{
					filesDTO.thumbnailBlob = IOUtils.toByteArray(inputStream);
				}
				filesDTO.fileTag = rs.getString("file_tag");
				filesDTO.fileTitle = rs.getString("file_title");
				filesDTO.userId = rs.getLong("user_id");
				filesDTO.source = rs.getString("source");
				filesDTO.createdAt = rs.getLong("created_at");
				filesDTO.isDeleted = rs.getInt("isDeleted");
				filesDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				System.out.println("Got DTO");

			}			
			
			
		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return filesDTO;
	}
	
	public long update(CommonDTO commonDTO, String tableName) throws Exception
	{		
		System.out.println("Update not supported");
		return -1;
	}
	
	public List<FilesDTO> getDTOs(Collection recordIDs)
	{
		return getDTOs(recordIDs, tableName);
	}
	
	public List<FilesDTO> getDTOs(Collection recordIDs, String tableName){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		FilesDTO filesDTO = null;
		List<FilesDTO> filesDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return filesDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				filesDTO = new FilesDTO();
				filesDTO.iD = rs.getLong("ID");
				filesDTO.fileID = rs.getLong("file_id");
				filesDTO.fileTypes = rs.getString("file_types");
				filesDTO.inputStream = rs.getBinaryStream("file_blob");
				InputStream inputStream = rs.getBinaryStream("thumbnail_blob");
				if(inputStream != null)
				{
					filesDTO.thumbnailBlob = IOUtils.toByteArray(inputStream);
				}
				filesDTO.fileTag = rs.getString("file_tag");
				filesDTO.fileTitle = rs.getString("file_title");
				filesDTO.userId = rs.getLong("user_id");
				filesDTO.source = rs.getString("source");
				filesDTO.createdAt = rs.getLong("created_at");
				filesDTO.isDeleted = rs.getInt("isDeleted");
				filesDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + filesDTO);
				
				filesDTOList.add(filesDTO);

			}			
			
		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return filesDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<FilesDTO> getAllFiles (boolean isFirstReload)
    {
		List<FilesDTO> filesDTOList = new ArrayList<>();

		String sql = "SELECT * FROM files";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by files.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				FilesDTO filesDTO = new FilesDTO();
				filesDTO.iD = rs.getLong("ID");
				filesDTO.fileID = rs.getLong("file_id");
				filesDTO.fileTypes = rs.getString("file_types");
				filesDTO.inputStream = rs.getBinaryStream("file_blob");
				InputStream inputStream = rs.getBinaryStream("thumbnail_blob");
				if(inputStream != null)
				{
					filesDTO.thumbnailBlob = IOUtils.toByteArray(inputStream);
				}
				filesDTO.fileTag = rs.getString("file_tag");
				filesDTO.fileTitle = rs.getString("file_title");
				filesDTO.userId = rs.getLong("user_id");
				filesDTO.source = rs.getString("source");
				filesDTO.createdAt = rs.getLong("created_at");
				filesDTO.isDeleted = rs.getInt("isDeleted");
				filesDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				filesDTOList.add(filesDTO);
			}			
		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return filesDTOList;
    }
	
	public List<FilesDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset)
	{
		return getDTOs(p_searchCriteria, limit, offset, tableName, true, 0);
	}
	
	public List<FilesDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<FilesDTO> filesDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, tableName, isPermanentTable, userApprovalPathType);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				FilesDTO filesDTO = new FilesDTO();
				filesDTO.iD = rs.getLong("ID");
				filesDTO.fileID = rs.getLong("file_id");
				filesDTO.fileTypes = rs.getString("file_types");
				filesDTO.inputStream = rs.getBinaryStream("file_blob");
				InputStream inputStream = rs.getBinaryStream("thumbnail_blob");
				if(inputStream != null)
				{
					filesDTO.thumbnailBlob = IOUtils.toByteArray(inputStream);
				}
				filesDTO.fileTag = rs.getString("file_tag");
				filesDTO.fileTitle = rs.getString("file_title");
				filesDTO.userId = rs.getLong("user_id");
				filesDTO.source = rs.getString("source");
				filesDTO.createdAt = rs.getLong("created_at");
				filesDTO.isDeleted = rs.getInt("isDeleted");
				filesDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				filesDTOList.add(filesDTO);
			}					
			
		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return filesDTOList;
	
	}


	public long addWithBlob(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception
	{

		FilesDTO filesDTO = (FilesDTO)commonDTO;
		System.out.println("arrived file_id = " + filesDTO.fileID);
		System.out.println("arrived userID = " + filesDTO.userId);

		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();

		try{
			connection = DBMW.getInstance().getConnection();

			if(connection == null)
			{
				System.out.println("nullconn");
			}

			filesDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;

			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "file_id";
			sql += ", ";
			sql += "file_types";
			sql += ", ";
			sql += "file_blob";
			sql += ", ";
			sql += "thumbnail_blob";
			sql += ", ";
			sql += "file_tag";
			sql += ", ";
			sql += "file_title";
			sql += ", ";
			sql += "user_id";
			sql += ", ";
			sql += "source";
			sql += ", ";
			sql += "created_at";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
			if(tempTableDTO!=null)
			{
				sql += ", permanent_table_id, operation_type, approval_path_type, approval_order";
			}
			sql += ")";


			sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			if(tempTableDTO!=null)
			{
				sql += ", " + tempTableDTO.permanent_table_id + ", " + tempTableDTO.operation_type + ", " + tempTableDTO.approval_path_type + ", " + tempTableDTO.approval_order;
			}
			sql += ")";


			ps = connection.prepareStatement(sql);




			int index = 1;


			ps.setObject(index++,filesDTO.iD);
			ps.setObject(index++,filesDTO.fileID);
			ps.setObject(index++,filesDTO.fileTypes);
			ps.setBytes(index++,filesDTO.fileBlob);
			ps.setBytes(index++,filesDTO.thumbnailBlob);
			ps.setObject(index++,filesDTO.fileTag);
			ps.setObject(index++,filesDTO.fileTitle);
			ps.setObject(index++,filesDTO.userId);
			ps.setObject(index++,filesDTO.source);
			ps.setObject(index++,filesDTO.createdAt);
			ps.setObject(index++,filesDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);

			System.out.println("adding file_id = " + filesDTO.fileID);
			System.out.println("adding userID = " + filesDTO.userId);
			ps.execute();


			recordUpdateTime(connection, lastModificationTime, tableName);

		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return filesDTO.iD;
	}

	public long addWithBlob(CommonDTO commonDTO) throws Exception
	{
		return addWithBlob(commonDTO, tableName, null);
	}

	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType)
    {
		FilesMAPS maps = new FilesMAPS(tableName);
		String joinSQL = "";

		return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, tableName, isPermanentTable, userApprovalPathType, maps, joinSQL);
    }

    public long getCountByFileId(long fileId){
		String sql = String.format(getByFileId,fileId);
		return (Long)ConnectionAndStatementUtil.getT(sql,rs->{
			try {
				return rs.getLong(1);
			} catch (SQLException ex) {
				logger.error(ex);
				logger.error("",ex);
				return 0;
			}
		});
	}
}
	