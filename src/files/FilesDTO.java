package files;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*; 
import util.*; 


public class FilesDTO extends CommonDTO
{

    public String fileTypes = "";
	public InputStream  inputStream = null;
	public byte[] thumbnailBlob = null;	
    public String fileTag = "";
    public String fileTitle = "";
	public long userId = 0;
    public String source = "";
	public long createdAt = 0;
    public byte[] fileBlob;


    @Override
	public String toString() {
            return "$FilesDTO[" +
            " iD = " + iD +
            " fileId = " + fileID +
            " fileTypes = " + fileTypes +
            " fileTag = " + fileTag +
            " fileTitle = " + fileTitle +
            " userId = " + userId +
            " source = " + source +
            " createdAt = " + createdAt +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}