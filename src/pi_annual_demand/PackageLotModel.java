package pi_annual_demand;

public class PackageLotModel {
    private long serialNum = -1;
    private long officeUnitId = -1;
    private String officeNameEn = "";
    private String officeNameBn = "";
    private long fiscalYearId = -1;
    private String fiscalYear = "";
    private long packageId = -1;
    private String packageEn = "";
    private String packageBn = "";
    private long lotId = -1;
    private String lotEn = "";
    private String lotBn = "";
    private String creationDate = "";
    private String nothiNo = "";

    public void setSerialNum(long serialNum) {
        this.serialNum = serialNum;
    }

    public void setOfficeUnitId(long officeUnitId) {
        this.officeUnitId = officeUnitId;
    }

    public void setOfficeNameEn(String officeNameEn) {
        this.officeNameEn = officeNameEn;
    }

    public void setOfficeNameBn(String officeNameBn) {
        this.officeNameBn = officeNameBn;
    }

    public void setFiscalYearId(long fiscalYearId) {
        this.fiscalYearId = fiscalYearId;
    }

    public void setFiscalYear(String fiscalYear) {
        this.fiscalYear = fiscalYear;
    }

    public void setPackageId(long packageId) {
        this.packageId = packageId;
    }

    public void setPackageEn(String packageEn) {
        this.packageEn = packageEn;
    }

    public void setPackageBn(String packageBn) {
        this.packageBn = packageBn;
    }

    public void setLotId(long lotId) {
        this.lotId = lotId;
    }

    public void setLotEn(String lotEn) {
        this.lotEn = lotEn;
    }

    public void setLotBn(String lotBn) {
        this.lotBn = lotBn;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public void setNothiNo(String nothiNo) {
        this.nothiNo = nothiNo;
    }
}
