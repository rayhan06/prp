package pi_annual_demand;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class PiAnnualDemandChildRepository implements Repository {
	PiAnnualDemandChildDAO piannualdemandchildDAO = null;
	
	static Logger logger = Logger.getLogger(PiAnnualDemandChildRepository.class);
	Map<Long, PiAnnualDemandChildDTO>mapOfPiAnnualDemandChildDTOToiD;
	Map<Long, Set<PiAnnualDemandChildDTO> >mapOfPiAnnualDemandChildDTOTopiAnnualDemandId;
	Map<Long, Set<PiAnnualDemandChildDTO> >mapOfPiAnnualDemandChildDTOTofiscalYear;
	Map<Long, Set<PiAnnualDemandChildDTO> >mapOfPiAnnualDemandChildDTOToofficeUnitId;
	Map<Long, Set<PiAnnualDemandChildDTO> >mapOfPiAnnualDemandChildDTOTopiPackageNewId;
	Map<Long, Set<PiAnnualDemandChildDTO> >mapOfPiAnnualDemandChildDTOTopiLotId;
	Map<Long, Set<PiAnnualDemandChildDTO> >mapOfPiAnnualDemandChildDTOToitemGroupId;
	Map<Long, Set<PiAnnualDemandChildDTO> >mapOfPiAnnualDemandChildDTOToitemTypeId;
	Map<Long, Set<PiAnnualDemandChildDTO> >mapOfPiAnnualDemandChildDTOToitemId;
	Gson gson;

  
	private PiAnnualDemandChildRepository(){
		piannualdemandchildDAO = PiAnnualDemandChildDAO.getInstance();
		mapOfPiAnnualDemandChildDTOToiD = new ConcurrentHashMap<>();
		mapOfPiAnnualDemandChildDTOTopiAnnualDemandId = new ConcurrentHashMap<>();
		mapOfPiAnnualDemandChildDTOTofiscalYear = new ConcurrentHashMap<>();
		mapOfPiAnnualDemandChildDTOToofficeUnitId = new ConcurrentHashMap<>();
		mapOfPiAnnualDemandChildDTOTopiPackageNewId = new ConcurrentHashMap<>();
		mapOfPiAnnualDemandChildDTOTopiLotId = new ConcurrentHashMap<>();
		mapOfPiAnnualDemandChildDTOToitemGroupId = new ConcurrentHashMap<>();
		mapOfPiAnnualDemandChildDTOToitemTypeId = new ConcurrentHashMap<>();
		mapOfPiAnnualDemandChildDTOToitemId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static PiAnnualDemandChildRepository INSTANCE = new PiAnnualDemandChildRepository();
    }

    public static PiAnnualDemandChildRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<PiAnnualDemandChildDTO> piannualdemandchildDTOs = piannualdemandchildDAO.getAllDTOs(reloadAll);
			for(PiAnnualDemandChildDTO piannualdemandchildDTO : piannualdemandchildDTOs) {
				PiAnnualDemandChildDTO oldPiAnnualDemandChildDTO = getPiAnnualDemandChildDTOByiD(piannualdemandchildDTO.iD);
				if( oldPiAnnualDemandChildDTO != null ) {
					mapOfPiAnnualDemandChildDTOToiD.remove(oldPiAnnualDemandChildDTO.iD);
				
					if(mapOfPiAnnualDemandChildDTOTopiAnnualDemandId.containsKey(oldPiAnnualDemandChildDTO.piAnnualDemandId)) {
						mapOfPiAnnualDemandChildDTOTopiAnnualDemandId.get(oldPiAnnualDemandChildDTO.piAnnualDemandId).remove(oldPiAnnualDemandChildDTO);
					}
					if(mapOfPiAnnualDemandChildDTOTopiAnnualDemandId.get(oldPiAnnualDemandChildDTO.piAnnualDemandId).isEmpty()) {
						mapOfPiAnnualDemandChildDTOTopiAnnualDemandId.remove(oldPiAnnualDemandChildDTO.piAnnualDemandId);
					}
					
					if(mapOfPiAnnualDemandChildDTOTofiscalYear.containsKey(oldPiAnnualDemandChildDTO.fiscalYear)) {
						mapOfPiAnnualDemandChildDTOTofiscalYear.get(oldPiAnnualDemandChildDTO.fiscalYear).remove(oldPiAnnualDemandChildDTO);
					}
					if(mapOfPiAnnualDemandChildDTOTofiscalYear.get(oldPiAnnualDemandChildDTO.fiscalYear).isEmpty()) {
						mapOfPiAnnualDemandChildDTOTofiscalYear.remove(oldPiAnnualDemandChildDTO.fiscalYear);
					}
					
					if(mapOfPiAnnualDemandChildDTOToofficeUnitId.containsKey(oldPiAnnualDemandChildDTO.officeUnitId)) {
						mapOfPiAnnualDemandChildDTOToofficeUnitId.get(oldPiAnnualDemandChildDTO.officeUnitId).remove(oldPiAnnualDemandChildDTO);
					}
					if(mapOfPiAnnualDemandChildDTOToofficeUnitId.get(oldPiAnnualDemandChildDTO.officeUnitId).isEmpty()) {
						mapOfPiAnnualDemandChildDTOToofficeUnitId.remove(oldPiAnnualDemandChildDTO.officeUnitId);
					}
					
					if(mapOfPiAnnualDemandChildDTOTopiPackageNewId.containsKey(oldPiAnnualDemandChildDTO.piPackageNewId)) {
						mapOfPiAnnualDemandChildDTOTopiPackageNewId.get(oldPiAnnualDemandChildDTO.piPackageNewId).remove(oldPiAnnualDemandChildDTO);
					}
					if(mapOfPiAnnualDemandChildDTOTopiPackageNewId.get(oldPiAnnualDemandChildDTO.piPackageNewId).isEmpty()) {
						mapOfPiAnnualDemandChildDTOTopiPackageNewId.remove(oldPiAnnualDemandChildDTO.piPackageNewId);
					}
					
					if(mapOfPiAnnualDemandChildDTOTopiLotId.containsKey(oldPiAnnualDemandChildDTO.piLotId)) {
						mapOfPiAnnualDemandChildDTOTopiLotId.get(oldPiAnnualDemandChildDTO.piLotId).remove(oldPiAnnualDemandChildDTO);
					}
					if(mapOfPiAnnualDemandChildDTOTopiLotId.get(oldPiAnnualDemandChildDTO.piLotId).isEmpty()) {
						mapOfPiAnnualDemandChildDTOTopiLotId.remove(oldPiAnnualDemandChildDTO.piLotId);
					}
					
					if(mapOfPiAnnualDemandChildDTOToitemGroupId.containsKey(oldPiAnnualDemandChildDTO.itemGroupId)) {
						mapOfPiAnnualDemandChildDTOToitemGroupId.get(oldPiAnnualDemandChildDTO.itemGroupId).remove(oldPiAnnualDemandChildDTO);
					}
					if(mapOfPiAnnualDemandChildDTOToitemGroupId.get(oldPiAnnualDemandChildDTO.itemGroupId).isEmpty()) {
						mapOfPiAnnualDemandChildDTOToitemGroupId.remove(oldPiAnnualDemandChildDTO.itemGroupId);
					}
					
					if(mapOfPiAnnualDemandChildDTOToitemTypeId.containsKey(oldPiAnnualDemandChildDTO.itemTypeId)) {
						mapOfPiAnnualDemandChildDTOToitemTypeId.get(oldPiAnnualDemandChildDTO.itemTypeId).remove(oldPiAnnualDemandChildDTO);
					}
					if(mapOfPiAnnualDemandChildDTOToitemTypeId.get(oldPiAnnualDemandChildDTO.itemTypeId).isEmpty()) {
						mapOfPiAnnualDemandChildDTOToitemTypeId.remove(oldPiAnnualDemandChildDTO.itemTypeId);
					}
					
					if(mapOfPiAnnualDemandChildDTOToitemId.containsKey(oldPiAnnualDemandChildDTO.itemId)) {
						mapOfPiAnnualDemandChildDTOToitemId.get(oldPiAnnualDemandChildDTO.itemId).remove(oldPiAnnualDemandChildDTO);
					}
					if(mapOfPiAnnualDemandChildDTOToitemId.get(oldPiAnnualDemandChildDTO.itemId).isEmpty()) {
						mapOfPiAnnualDemandChildDTOToitemId.remove(oldPiAnnualDemandChildDTO.itemId);
					}
					
					
				}
				if(piannualdemandchildDTO.isDeleted == 0) 
				{
					
					mapOfPiAnnualDemandChildDTOToiD.put(piannualdemandchildDTO.iD, piannualdemandchildDTO);
				
					if( ! mapOfPiAnnualDemandChildDTOTopiAnnualDemandId.containsKey(piannualdemandchildDTO.piAnnualDemandId)) {
						mapOfPiAnnualDemandChildDTOTopiAnnualDemandId.put(piannualdemandchildDTO.piAnnualDemandId, new HashSet<>());
					}
					mapOfPiAnnualDemandChildDTOTopiAnnualDemandId.get(piannualdemandchildDTO.piAnnualDemandId).add(piannualdemandchildDTO);
					
					if( ! mapOfPiAnnualDemandChildDTOTofiscalYear.containsKey(piannualdemandchildDTO.fiscalYear)) {
						mapOfPiAnnualDemandChildDTOTofiscalYear.put(piannualdemandchildDTO.fiscalYear, new HashSet<>());
					}
					mapOfPiAnnualDemandChildDTOTofiscalYear.get(piannualdemandchildDTO.fiscalYear).add(piannualdemandchildDTO);
					
					if( ! mapOfPiAnnualDemandChildDTOToofficeUnitId.containsKey(piannualdemandchildDTO.officeUnitId)) {
						mapOfPiAnnualDemandChildDTOToofficeUnitId.put(piannualdemandchildDTO.officeUnitId, new HashSet<>());
					}
					mapOfPiAnnualDemandChildDTOToofficeUnitId.get(piannualdemandchildDTO.officeUnitId).add(piannualdemandchildDTO);
					
					if( ! mapOfPiAnnualDemandChildDTOTopiPackageNewId.containsKey(piannualdemandchildDTO.piPackageNewId)) {
						mapOfPiAnnualDemandChildDTOTopiPackageNewId.put(piannualdemandchildDTO.piPackageNewId, new HashSet<>());
					}
					mapOfPiAnnualDemandChildDTOTopiPackageNewId.get(piannualdemandchildDTO.piPackageNewId).add(piannualdemandchildDTO);
					
					if( ! mapOfPiAnnualDemandChildDTOTopiLotId.containsKey(piannualdemandchildDTO.piLotId)) {
						mapOfPiAnnualDemandChildDTOTopiLotId.put(piannualdemandchildDTO.piLotId, new HashSet<>());
					}
					mapOfPiAnnualDemandChildDTOTopiLotId.get(piannualdemandchildDTO.piLotId).add(piannualdemandchildDTO);
					
					if( ! mapOfPiAnnualDemandChildDTOToitemGroupId.containsKey(piannualdemandchildDTO.itemGroupId)) {
						mapOfPiAnnualDemandChildDTOToitemGroupId.put(piannualdemandchildDTO.itemGroupId, new HashSet<>());
					}
					mapOfPiAnnualDemandChildDTOToitemGroupId.get(piannualdemandchildDTO.itemGroupId).add(piannualdemandchildDTO);
					
					if( ! mapOfPiAnnualDemandChildDTOToitemTypeId.containsKey(piannualdemandchildDTO.itemTypeId)) {
						mapOfPiAnnualDemandChildDTOToitemTypeId.put(piannualdemandchildDTO.itemTypeId, new HashSet<>());
					}
					mapOfPiAnnualDemandChildDTOToitemTypeId.get(piannualdemandchildDTO.itemTypeId).add(piannualdemandchildDTO);
					
					if( ! mapOfPiAnnualDemandChildDTOToitemId.containsKey(piannualdemandchildDTO.itemId)) {
						mapOfPiAnnualDemandChildDTOToitemId.put(piannualdemandchildDTO.itemId, new HashSet<>());
					}
					mapOfPiAnnualDemandChildDTOToitemId.get(piannualdemandchildDTO.itemId).add(piannualdemandchildDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public PiAnnualDemandChildDTO clone(PiAnnualDemandChildDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, PiAnnualDemandChildDTO.class);
	}
	
	
	public List<PiAnnualDemandChildDTO> getPiAnnualDemandChildList() {
		List <PiAnnualDemandChildDTO> piannualdemandchilds = new ArrayList<PiAnnualDemandChildDTO>(this.mapOfPiAnnualDemandChildDTOToiD.values());
		return piannualdemandchilds;
	}
	
	public List<PiAnnualDemandChildDTO> copyPiAnnualDemandChildList() {
		List <PiAnnualDemandChildDTO> piannualdemandchilds = getPiAnnualDemandChildList();
		return piannualdemandchilds
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public PiAnnualDemandChildDTO getPiAnnualDemandChildDTOByiD( long iD){
		return mapOfPiAnnualDemandChildDTOToiD.get(iD);
	}
	
	public PiAnnualDemandChildDTO copyPiAnnualDemandChildDTOByiD( long iD){
		return clone(mapOfPiAnnualDemandChildDTOToiD.get(iD));
	}
	
	
	public List<PiAnnualDemandChildDTO> getPiAnnualDemandChildDTOBypiAnnualDemandId(long piAnnualDemandId) {
		return new ArrayList<>( mapOfPiAnnualDemandChildDTOTopiAnnualDemandId.getOrDefault(piAnnualDemandId,new HashSet<>()));
	}
	
	public List<PiAnnualDemandChildDTO> copyPiAnnualDemandChildDTOBypiAnnualDemandId(long piAnnualDemandId)
	{
		List <PiAnnualDemandChildDTO> piannualdemandchilds = getPiAnnualDemandChildDTOBypiAnnualDemandId(piAnnualDemandId);
		return piannualdemandchilds
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<PiAnnualDemandChildDTO> getPiAnnualDemandChildDTOByfiscalYear(long fiscalYear) {
		return new ArrayList<>( mapOfPiAnnualDemandChildDTOTofiscalYear.getOrDefault(fiscalYear,new HashSet<>()));
	}
	
	public List<PiAnnualDemandChildDTO> copyPiAnnualDemandChildDTOByfiscalYear(long fiscalYear)
	{
		List <PiAnnualDemandChildDTO> piannualdemandchilds = getPiAnnualDemandChildDTOByfiscalYear(fiscalYear);
		return piannualdemandchilds
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<PiAnnualDemandChildDTO> getPiAnnualDemandChildDTOByofficeUnitId(long officeUnitId) {
		return new ArrayList<>( mapOfPiAnnualDemandChildDTOToofficeUnitId.getOrDefault(officeUnitId,new HashSet<>()));
	}
	
	public List<PiAnnualDemandChildDTO> copyPiAnnualDemandChildDTOByofficeUnitId(long officeUnitId)
	{
		List <PiAnnualDemandChildDTO> piannualdemandchilds = getPiAnnualDemandChildDTOByofficeUnitId(officeUnitId);
		return piannualdemandchilds
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<PiAnnualDemandChildDTO> getPiAnnualDemandChildDTOBypiPackageNewId(long piPackageNewId) {
		return new ArrayList<>( mapOfPiAnnualDemandChildDTOTopiPackageNewId.getOrDefault(piPackageNewId,new HashSet<>()));
	}
	
	public List<PiAnnualDemandChildDTO> copyPiAnnualDemandChildDTOBypiPackageNewId(long piPackageNewId)
	{
		List <PiAnnualDemandChildDTO> piannualdemandchilds = getPiAnnualDemandChildDTOBypiPackageNewId(piPackageNewId);
		return piannualdemandchilds
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<PiAnnualDemandChildDTO> getPiAnnualDemandChildDTOBypiLotId(long piLotId) {
		return new ArrayList<>( mapOfPiAnnualDemandChildDTOTopiLotId.getOrDefault(piLotId,new HashSet<>()));
	}
	
	public List<PiAnnualDemandChildDTO> copyPiAnnualDemandChildDTOBypiLotId(long piLotId)
	{
		List <PiAnnualDemandChildDTO> piannualdemandchilds = getPiAnnualDemandChildDTOBypiLotId(piLotId);
		return piannualdemandchilds
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<PiAnnualDemandChildDTO> getPiAnnualDemandChildDTOByitemGroupId(long itemGroupId) {
		return new ArrayList<>( mapOfPiAnnualDemandChildDTOToitemGroupId.getOrDefault(itemGroupId,new HashSet<>()));
	}
	
	public List<PiAnnualDemandChildDTO> copyPiAnnualDemandChildDTOByitemGroupId(long itemGroupId)
	{
		List <PiAnnualDemandChildDTO> piannualdemandchilds = getPiAnnualDemandChildDTOByitemGroupId(itemGroupId);
		return piannualdemandchilds
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<PiAnnualDemandChildDTO> getPiAnnualDemandChildDTOByitemTypeId(long itemTypeId) {
		return new ArrayList<>( mapOfPiAnnualDemandChildDTOToitemTypeId.getOrDefault(itemTypeId,new HashSet<>()));
	}
	
	public List<PiAnnualDemandChildDTO> copyPiAnnualDemandChildDTOByitemTypeId(long itemTypeId)
	{
		List <PiAnnualDemandChildDTO> piannualdemandchilds = getPiAnnualDemandChildDTOByitemTypeId(itemTypeId);
		return piannualdemandchilds
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<PiAnnualDemandChildDTO> getPiAnnualDemandChildDTOByitemId(long itemId) {
		return new ArrayList<>( mapOfPiAnnualDemandChildDTOToitemId.getOrDefault(itemId,new HashSet<>()));
	}
	
	public List<PiAnnualDemandChildDTO> copyPiAnnualDemandChildDTOByitemId(long itemId)
	{
		List <PiAnnualDemandChildDTO> piannualdemandchilds = getPiAnnualDemandChildDTOByitemId(itemId);
		return piannualdemandchilds
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return piannualdemandchildDAO.getTableName();
	}
}


