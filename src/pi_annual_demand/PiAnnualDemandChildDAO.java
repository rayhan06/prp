package pi_annual_demand;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class PiAnnualDemandChildDAO implements CommonDAOService<PiAnnualDemandChildDTO> {

    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private String[] columnNames = null;

    private PiAnnualDemandChildDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "pi_annual_demand_id",
                        "fiscal_year",
                        "office_unit_id",
                        "serial_number",
                        "pi_package_new_id",
                        "pi_lot_id",
                        "item_group_id",
                        "item_type_id",
                        "item_id",
                        "description",
                        "item_quantity",
                        "pi_unit_id",
                        "item_unit_price",
                        "item_total_price",
                        "pi_package_final_id",
                        "pi_lot_final_id",
                        "search_column",
                        "inserted_by",
                        "modified_by",
                        "insertion_date",
                        "isDeleted",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("description", " and (description like ?)");
        searchMap.put("item_unit_price", " and (item_unit_price like ?)");
        searchMap.put("inserted_by", " and (inserted_by like ?)");
        searchMap.put("modified_by", " and (modified_by like ?)");
        searchMap.put("insertion_date_start", " and (insertion_date >= ?)");
        searchMap.put("insertion_date_end", " and (insertion_date <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final PiAnnualDemandChildDAO INSTANCE = new PiAnnualDemandChildDAO();
    }

    public static PiAnnualDemandChildDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(PiAnnualDemandChildDTO piannualdemandchildDTO) {
        piannualdemandchildDTO.searchColumn = "";
        piannualdemandchildDTO.searchColumn += piannualdemandchildDTO.description + " ";
        piannualdemandchildDTO.searchColumn += piannualdemandchildDTO.itemQuantity + " ";
        piannualdemandchildDTO.searchColumn += piannualdemandchildDTO.itemUnitPrice + " ";
        piannualdemandchildDTO.searchColumn += piannualdemandchildDTO.itemTotalPrice + " ";
        piannualdemandchildDTO.searchColumn += piannualdemandchildDTO.insertedBy + " ";
        piannualdemandchildDTO.searchColumn += piannualdemandchildDTO.modifiedBy + " ";
    }

    @Override
    public void set(PreparedStatement ps, PiAnnualDemandChildDTO piannualdemandchildDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(piannualdemandchildDTO);
        if (isInsert) {
            ps.setObject(++index, piannualdemandchildDTO.iD);
        }
        ps.setObject(++index, piannualdemandchildDTO.piAnnualDemandId);
        ps.setObject(++index, piannualdemandchildDTO.fiscalYear);
        ps.setObject(++index, piannualdemandchildDTO.officeUnitId);
        ps.setObject(++index, piannualdemandchildDTO.serialNumber);
        ps.setObject(++index, piannualdemandchildDTO.piPackageNewId);
        ps.setObject(++index, piannualdemandchildDTO.piLotId);
        ps.setObject(++index, piannualdemandchildDTO.itemGroupId);
        ps.setObject(++index, piannualdemandchildDTO.itemTypeId);
        ps.setObject(++index, piannualdemandchildDTO.itemId);
        ps.setObject(++index, piannualdemandchildDTO.description);
        ps.setObject(++index, piannualdemandchildDTO.itemQuantity);
        ps.setObject(++index, piannualdemandchildDTO.piUnitId);
        ps.setObject(++index, piannualdemandchildDTO.itemUnitPrice);
        ps.setObject(++index, piannualdemandchildDTO.itemTotalPrice);
        ps.setObject(++index, piannualdemandchildDTO.piPackageFinalId);
        ps.setObject(++index, piannualdemandchildDTO.piLotFinalId);
        ps.setObject(++index, piannualdemandchildDTO.searchColumn);
        ps.setObject(++index, piannualdemandchildDTO.insertedBy);
        ps.setObject(++index, piannualdemandchildDTO.modifiedBy);
        ps.setObject(++index, piannualdemandchildDTO.insertionDate);
        if (isInsert) {
            ps.setObject(++index, piannualdemandchildDTO.isDeleted);
        }
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, piannualdemandchildDTO.iD);
        }
    }

    @Override
    public PiAnnualDemandChildDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            PiAnnualDemandChildDTO piannualdemandchildDTO = new PiAnnualDemandChildDTO();
            int i = 0;
            piannualdemandchildDTO.iD = rs.getLong(columnNames[i++]);
            piannualdemandchildDTO.piAnnualDemandId = rs.getLong(columnNames[i++]);
            piannualdemandchildDTO.fiscalYear = rs.getLong(columnNames[i++]);
            piannualdemandchildDTO.officeUnitId = rs.getLong(columnNames[i++]);
            piannualdemandchildDTO.serialNumber = rs.getLong(columnNames[i++]);
            piannualdemandchildDTO.piPackageNewId = rs.getLong(columnNames[i++]);
            piannualdemandchildDTO.piLotId = rs.getLong(columnNames[i++]);
            piannualdemandchildDTO.itemGroupId = rs.getLong(columnNames[i++]);
            piannualdemandchildDTO.itemTypeId = rs.getLong(columnNames[i++]);
            piannualdemandchildDTO.itemId = rs.getLong(columnNames[i++]);
            piannualdemandchildDTO.description = rs.getString(columnNames[i++]);
            piannualdemandchildDTO.itemQuantity = rs.getLong(columnNames[i++]);
            piannualdemandchildDTO.piUnitId = rs.getLong(columnNames[i++]);
            piannualdemandchildDTO.itemUnitPrice = rs.getLong(columnNames[i++]);
            piannualdemandchildDTO.itemTotalPrice = rs.getString(columnNames[i++]);
            piannualdemandchildDTO.piPackageFinalId = rs.getLong(columnNames[i++]);
            piannualdemandchildDTO.piLotFinalId = rs.getLong(columnNames[i++]);
            piannualdemandchildDTO.searchColumn = rs.getString(columnNames[i++]);
            piannualdemandchildDTO.insertedBy = rs.getString(columnNames[i++]);
            piannualdemandchildDTO.modifiedBy = rs.getString(columnNames[i++]);
            piannualdemandchildDTO.insertionDate = rs.getLong(columnNames[i++]);
            piannualdemandchildDTO.isDeleted = rs.getInt(columnNames[i++]);
            piannualdemandchildDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            return piannualdemandchildDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public PiAnnualDemandChildDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "pi_annual_demand_child";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((PiAnnualDemandChildDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((PiAnnualDemandChildDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

}
	