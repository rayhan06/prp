package pi_annual_demand;

import com.google.gson.Gson;
import common.ApiResponse;
import common.BaseServlet;
import common.CommonDAOService;
import fiscal_year.Fiscal_yearDAO;
import fiscal_year.Fiscal_yearRepository;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import pi_package_item_map.PiPackageItemMapChildDAO;
import pi_package_item_map.PiPackageItemMapChildDTO;
import pi_package_lot.Pi_package_lotDAO;
import pi_package_lot.Pi_package_lotDTO;
import pi_package_new.Pi_package_newDTO;
import pi_package_new.Pi_package_newRepository;
import pi_unit.Pi_unitDTO;
import pi_unit.Pi_unitRepository;
import procurement_goods.Procurement_goodsDTO;
import procurement_goods.Procurement_goodsRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@WebServlet("/Pi_annual_demandServlet")
@MultipartConfig
public class Pi_annual_demandServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_annual_demandServlet.class);

    @Override
    public String getTableName() {
        return Pi_annual_demandDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_annual_demandServlet";
    }

    @Override
    public Pi_annual_demandDAO getCommonDAOService() {
        return Pi_annual_demandDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PI_ANNUAL_DEMAND_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PI_ANNUAL_DEMAND_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PI_ANNUAL_DEMAND_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_annual_demandServlet.class;
    }

    private final Gson gson = new Gson();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        init(request);
        try {
            switch (request.getParameter("actionType")) {
                case "search":
                    if (Utils.checkPermission(commonLoginData.userDTO, getSearchMenuConstants()) && getSearchPagePermission(request)) {
                        long roleId = commonLoginData.userDTO.roleID;
                        if (roleId != SessionConstants.INVENTORY_ADMIN_ROLE && roleId != SessionConstants.ADMIN_ROLE) {
                            Map<String, String> extraCriteriaMap;
                            if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) == null) {
                                extraCriteriaMap = new HashMap<>();
                            } else {
                                extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
                            }
                            extraCriteriaMap.put("office_unit_id", String.valueOf(commonLoginData.userDTO.unitID));
                            request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                        }

                        search(request, response);
                        return;
                    }
                    break;
                case "saveFinalPackageLot":
                    if (Utils.checkPermission(commonLoginData.userDTO, getAddPageMenuConstants()) && getAddPagePermission(request)) {
                        String annualDemandChildId = request.getParameter("annualDemandChildId");
                        String finalPackageId = request.getParameter("finalPackageId");
                        String finalLotId = request.getParameter("finalLotId");
                        finalPackageId = finalPackageId == null || finalPackageId.equals("") ? "-1" : finalPackageId;
                        finalLotId = finalLotId == null || finalLotId.equals("") ? "-1" : finalLotId;
                        PiAnnualDemandChildDTO dto = PiAnnualDemandChildRepository.getInstance()
                                .getPiAnnualDemandChildDTOByiD(Long.parseLong(annualDemandChildId));
                        dto.piPackageFinalId = Long.parseLong(finalPackageId);
                        dto.piLotFinalId = Long.parseLong(finalLotId);
                        dto.lastModificationTime = System.currentTimeMillis();
                        PiAnnualDemandChildDAO.getInstance().update(dto);
                        return;
                    }
                    break;
                case "getPackageLotListForModal":
                    if (Utils.checkPermission(commonLoginData.userDTO, getAddPageMenuConstants()) && getAddPagePermission(request)) {
                        String officeUnitId = request.getParameter("officeUnitId");
                        if (officeUnitId == null || officeUnitId == "") {
                            return;
                        }
                        List<PackageLotModel> packageLotModels = new ArrayList<>();
                        List<Pi_annual_demandDTO> models = (List<Pi_annual_demandDTO>) Pi_annual_demandDAO.getInstance()
                                .getDTOsByParent("office_unit_id", Long.parseLong(officeUnitId));
                        int serialNum = 1;
                        for (Pi_annual_demandDTO model : models) {
                            Pi_package_newDTO packageModel = Pi_package_newRepository.getInstance()
                                    .getPi_package_newDTOByiD(model.piPackageNewId);
                            // IF ONE OFFICE MAKE PACKAGE, IT CAN NOT MAKE LIST WITHOUT PACKAGE AGAIN
                            // BUT IF IT HAPPENS PACKAGE MODEL WILL BE NULL
                            if (packageModel == null) continue;
                            Pi_package_lotDTO lotModel = Pi_package_lotDAO.getInstance().getDTOByID(model.piLotId);
                            String formattedDate = new SimpleDateFormat("dd-MM-yyyy")
                                    .format(new Date(model.creationDate));
                            formattedDate = Utils.getDigits(formattedDate, Language);

                            PackageLotModel packageLotModel = new PackageLotModel();
                            packageLotModel.setSerialNum(serialNum++);
                            packageLotModel.setOfficeUnitId(model.officeUnitId);
                            packageLotModel.setOfficeNameEn(Office_unitsRepository.getInstance()
                                    .geText("English", model.officeUnitId));
                            packageLotModel.setOfficeNameEn(Office_unitsRepository.getInstance()
                                    .geText("Bangla", model.officeUnitId));
                            packageLotModel.setPackageId(model.piPackageNewId);
                            String packageName = packageModel == null ? "" :
                                    packageModel.packageNumberEn + (packageModel.packageNameEn.equals("") ? "" : "- " + packageModel.packageNameEn);
                            packageLotModel.setPackageEn(packageName);
                            packageName = packageModel == null ? "" :
                                    packageModel.packageNumberBn + (packageModel.packageNameBn.equals("") ? "" : "- (" + packageModel.packageNameBn);
                            packageLotModel.setPackageBn(packageName);
                            packageLotModel.setCreationDate(formattedDate);
                            packageLotModel.setNothiNo(model.nothiNo);
                            packageLotModel.setLotId(model.piLotId);
                            String lotName = lotModel == null ? "" :
                                    lotModel.lotNumberEn + (lotModel.lotNameEn.equals("") ? "" : "- " + lotModel.lotNameEn);
                            packageLotModel.setLotEn(lotName);
                            lotName = lotModel == null ? "" :
                                    lotModel.lotNumberBn + (lotModel.lotNameBn.equals("") ? "" : "- " + lotModel.lotNameBn);
                            packageLotModel.setLotBn(lotName);
                            packageLotModel.setFiscalYearId(model.fiscalYear);
                            packageLotModel.setFiscalYear(Fiscal_yearRepository.getInstance().getText(model.fiscalYear, Language));
                            packageLotModels.add(packageLotModel);
                        }
                        ApiResponse.sendSuccessResponse(response, gson.toJson(packageLotModels));
                        return;
                    }
                    break;
                case "getItemListByOfficeUnitId":
                    if (Utils.checkPermission(commonLoginData.userDTO, getAddPageMenuConstants()) && getAddPagePermission(request)) {
                        String officeUnitIdStr = request.getParameter("officeUnitId");
                        long currFiscalYearId = new Fiscal_yearDAO().getFiscalYearBYDateLong(System.currentTimeMillis()).id;

                        List<PiAnnualDemandChildDTO> models = PiAnnualDemandChildRepository.getInstance()
                                .getPiAnnualDemandChildDTOByofficeUnitId(Long.parseLong(officeUnitIdStr))
                                .stream().filter(model -> model.fiscalYear == currFiscalYearId).collect(Collectors.toList());

                        List<ItemModel> itemModels = new ArrayList<>();
                        int serial = 1;
                        for (PiAnnualDemandChildDTO childDTO : models) {
                            ItemModel newItem = new ItemModel();
                            newItem.serialNumber = serial++;
                            newItem.fiscalYear = childDTO.fiscalYear;
                            newItem.officeUnitId = childDTO.officeUnitId;
                            newItem.officeUnitText = Office_unitsRepository.getInstance().geText(Language, childDTO.officeUnitId);
                            newItem.piPackageNewId = childDTO.piPackageNewId;
                            newItem.piLotId = childDTO.piLotId;
                            newItem.itemGroupId = childDTO.itemGroupId;
                            newItem.itemTypeId = childDTO.itemTypeId;
                            newItem.itemId = childDTO.itemId;
                            Procurement_goodsDTO goodModel = Procurement_goodsRepository.getInstance().getProcurement_goodsDTOByID(childDTO.itemId);
                            newItem.descriptionEn = goodModel.nameEn + (!goodModel.description.equals("") ? ", " + goodModel.description : "");
                            newItem.descriptionBn = goodModel.nameBn + (!goodModel.description.equals("") ? ", " + goodModel.description : "");
                            Pi_unitDTO unitModel = Pi_unitRepository.getInstance().getPi_unitDTOByiD(childDTO.piUnitId);
                            newItem.piUnitId = childDTO.piUnitId;
                            newItem.piUnitNameEn = unitModel.nameEn;
                            newItem.piUnitNameBn = unitModel.nameBn;
                            newItem.itemQuantity = childDTO.itemQuantity;
                            newItem.itemUnitPrice = childDTO.itemUnitPrice;
                            newItem.itemTotalPrice = childDTO.itemTotalPrice;
                            newItem.piPackageFinalId = childDTO.piPackageFinalId;
                            newItem.piLotFinalId = childDTO.piLotFinalId;
                            newItem.annualDemandId = childDTO.piAnnualDemandId;
                            newItem.annualDemandChildId = childDTO.iD;
                            itemModels.add(newItem);
                        }
                        ApiResponse.sendSuccessResponse(response, gson.toJson(itemModels));
                    }
                    break;
                case "getItemListByPackageLot":
                    if (Utils.checkPermission(commonLoginData.userDTO, getAddPageMenuConstants()) && getAddPagePermission(request)) {
                        String packageIdStr = request.getParameter("packageId");
                        String lotIdStr = request.getParameter("lotId") == null ? "-1" : request.getParameter("lotId");
                        long currFiscalYearId = new Fiscal_yearDAO().getFiscalYearBYDateLong(System.currentTimeMillis()).id;
                        List<PiAnnualDemandChildDTO> models = PiAnnualDemandChildRepository.getInstance()
                                .getPiAnnualDemandChildDTOBypiPackageNewId(Long.parseLong(packageIdStr))
                                .stream().filter(model -> model.piLotId == Long.parseLong(lotIdStr))
                                .filter(model -> model.fiscalYear == currFiscalYearId).collect(Collectors.toList());

                        List<ItemModel> itemModels = new ArrayList<>();
                        int serial = 1;
                        for (PiAnnualDemandChildDTO childDTO : models) {
                            ItemModel newItem = new ItemModel();
                            newItem.serialNumber = serial++;
                            newItem.fiscalYear = childDTO.fiscalYear;
                            newItem.officeUnitId = childDTO.officeUnitId;
                            newItem.officeUnitText = Office_unitsRepository.getInstance().geText(Language, childDTO.officeUnitId);
                            newItem.piPackageNewId = childDTO.piPackageNewId;
                            newItem.piLotId = childDTO.piLotId;
                            newItem.itemGroupId = childDTO.itemGroupId;
                            newItem.itemTypeId = childDTO.itemTypeId;
                            newItem.itemId = childDTO.itemId;
                            Procurement_goodsDTO goodModel = Procurement_goodsRepository.getInstance().getProcurement_goodsDTOByID(childDTO.itemId);
                            newItem.descriptionEn = goodModel.nameEn + (!goodModel.description.equals("") ? ", " + goodModel.description : "");
                            newItem.descriptionBn = goodModel.nameBn + (!goodModel.description.equals("") ? ", " + goodModel.description : "");
                            Pi_unitDTO unitModel = Pi_unitRepository.getInstance().getPi_unitDTOByiD(childDTO.piUnitId);
                            newItem.piUnitId = childDTO.piUnitId;
                            newItem.piUnitNameEn = unitModel.nameEn;
                            newItem.piUnitNameBn = unitModel.nameBn;
                            newItem.itemQuantity = childDTO.itemQuantity;
                            newItem.itemUnitPrice = childDTO.itemUnitPrice;
                            newItem.itemTotalPrice = childDTO.itemTotalPrice;
                            newItem.piPackageFinalId = childDTO.piPackageFinalId;
                            newItem.piLotFinalId = childDTO.piLotFinalId;
                            newItem.annualDemandId = childDTO.piAnnualDemandId;
                            newItem.annualDemandChildId = childDTO.iD;
                            itemModels.add(newItem);
                        }
                        ApiResponse.sendSuccessResponse(response, gson.toJson(itemModels));
                    }
                    break;
                case "deleteAnnualDemandWithValidation":
                    if (getDeletePermission(request)) {
                        String annualDemandIdStr = request.getParameter("annualDemandId");
                        if (annualDemandIdStr == null) {
                            String errorMessage = UtilCharacter.getDataByLanguage(Language, "বার্ষিক চাহিদা ডিলিট ব্যর্থ হয়েছে!", "Annual Demand Delete Failed!");
                            ApiResponse.sendErrorResponse(response, errorMessage);
                        }
                        long annualDemandId = Long.parseLong(annualDemandIdStr);
                        List<PiPackageItemMapChildDTO> DTOs = (List<PiPackageItemMapChildDTO>) PiPackageItemMapChildDAO.getInstance()
                                .getDTOsByParent("annual_demand_id", annualDemandId);
                        if (DTOs != null && DTOs.size() == 0) {
                            List<PiAnnualDemandChildDTO> childDTOS = (List<PiAnnualDemandChildDTO>) PiAnnualDemandChildDAO
                                    .getInstance().getDTOsByParent("pi_annual_demand_id", annualDemandId);
                            for (PiAnnualDemandChildDTO childDTO : childDTOS) {
                                PiAnnualDemandChildDAO.getInstance().delete(commonLoginData.userDTO.employee_record_id, childDTO.iD);
                            }
                            Pi_annual_demandDAO.getInstance().delete(commonLoginData.userDTO.employee_record_id, annualDemandId);
                            String successMessage = UtilCharacter.getDataByLanguage(Language, "বার্ষিক চাহিদা ডিলিট সফল হয়েছে", "Annual Demand Delete Successful");
                            ApiResponse.sendSuccessResponse(response, successMessage);
                        } else {
                            String errorMessage = UtilCharacter.getDataByLanguage(Language,
                                    "ডিলিট করা যাবে না। কারণ এটা প্যাকেজ আইটেম ম্যাপে ব্যবহার হচ্ছে!",
                                    "Can not be deleted. Because it is used in package item map!");
                            ApiResponse.sendErrorResponse(response, errorMessage);
                        }
                    }
                    break;
                case "deleteAnnualDemandChildWithValidation":
                    if (getDeletePermission(request)) {
                        String annualDemandChildIdStr = request.getParameter("annualDemandChildId");
                        if (annualDemandChildIdStr == null) {
                            String errorMessage = UtilCharacter.getDataByLanguage(Language, "আইটেম ডিলিট ব্যর্থ হয়েছে!", "Item Delete Failed!");
                            ApiResponse.sendErrorResponse(response, errorMessage);
                        }
                        long annualDemandChildId = Long.parseLong(annualDemandChildIdStr);
                        List<PiPackageItemMapChildDTO> DTOs = (List<PiPackageItemMapChildDTO>) PiPackageItemMapChildDAO.getInstance()
                                .getDTOsByParent("annual_demand_child_id", annualDemandChildId);
                        if (DTOs != null && DTOs.size() == 0) {
                            Pi_annual_demandDAO.getInstance().delete(commonLoginData.userDTO.employee_record_id, annualDemandChildId);
                            String successMessage = UtilCharacter.getDataByLanguage(Language, "আইটেম ডিলিট সফল হয়েছে", "Item Delete Successful");
                            ApiResponse.sendSuccessResponse(response, successMessage);
                        } else {
                            String errorMessage = UtilCharacter.getDataByLanguage(Language,
                                    "ডিলিট করা যাবে না। কারণ এটা প্যাকেজ আইটেম ম্যাপে ব্যবহার হচ্ছে!",
                                    "Can not be deleted. Because it is used in package item map!");
                            ApiResponse.sendErrorResponse(response, errorMessage);
                        }
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        super.doGet(request, response);
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        request.setAttribute("failureMessage", "");
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        Pi_annual_demandDTO pi_annual_demandDTO;
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

        if (addFlag) {
            pi_annual_demandDTO = new Pi_annual_demandDTO();
            pi_annual_demandDTO.insertedBy = pi_annual_demandDTO.modifiedBy = String.valueOf(userDTO.employee_record_id);
            pi_annual_demandDTO.insertionDate = pi_annual_demandDTO.lastModificationTime = System.currentTimeMillis();
        } else {
            String iD = request.getParameter("iD");
            pi_annual_demandDTO = Pi_annual_demandDAO.getInstance().getDTOByID(Long.parseLong(iD));
            pi_annual_demandDTO.modifiedBy = String.valueOf(userDTO.employee_record_id);
            pi_annual_demandDTO.lastModificationTime = System.currentTimeMillis();
        }

        String Value = "";

        Value = request.getParameter("nothiNo");
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            pi_annual_demandDTO.nothiNo = (Value);
        }
        logger.debug("nothiNo = " + Value);


        Value = request.getParameter("creationDate");
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            Date date = dateFormatter.parse(Value);
            pi_annual_demandDTO.creationDate = date.getTime();
        } else {
            throw new Exception(UtilCharacter.getDataByLanguage(Language, "অনুগ্রহপূর্বক তারিখ নির্বাচন করুন", "Please select date"));
        }
        logger.debug("creationDate = " + Value);


        Value = request.getParameter("hasPackageLot");
        pi_annual_demandDTO.hasPackageLot = isValueValid(Value);
        logger.debug("hasPackageLot = " + Value);


        if (addFlag) {
            Value = request.getParameter("fiscalYear");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_annual_demandDTO.fiscalYear = Long.parseLong(Value);
            } else {
                throw new Exception(UtilCharacter.getDataByLanguage(Language, "অনুগ্রহপূর্বক অর্থবছর নির্বাচন করুন", "Please select fiscal year"));
            }
            logger.debug("fiscalYear = " + Value);


            Value = request.getParameter("officeUnitId");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_annual_demandDTO.officeUnitId = Long.parseLong(Value);
            } else {
                throw new Exception(UtilCharacter.getDataByLanguage(Language, "অনুগ্রহপূর্বক দপ্তর নির্বাচন করুন", "Please select office"));
            }
            logger.debug("officeUnitId = " + Value);


            Value = request.getParameter("packageList");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_annual_demandDTO.piPackageNewId = Long.parseLong(Value);
                validatePackage(pi_annual_demandDTO.fiscalYear, pi_annual_demandDTO.piPackageNewId, Language);
            } else {
                if (pi_annual_demandDTO.hasPackageLot)
                    throw new Exception(UtilCharacter.getDataByLanguage(Language, "অনুগ্রহপূর্বক প্যাকেজ নির্বাচন করুন", "Please select package"));
                pi_annual_demandDTO.piPackageNewId = -1;
            }
            logger.debug("piPackageNewId = " + Value);


            Value = request.getParameter("lotList");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_annual_demandDTO.piLotId = Long.parseLong(Value);
                validateLot(pi_annual_demandDTO.fiscalYear, pi_annual_demandDTO.piLotId, Language);
            } else {
                validateIfLotExistInsideThisPackage(pi_annual_demandDTO.piPackageNewId, Language);
                pi_annual_demandDTO.piLotId = -1;
            }
            logger.debug("lotList = " + Value);
        }


        Value = request.getParameter("filesDropzone");
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            pi_annual_demandDTO.filesDropZone = Long.parseLong(Value);
            if (!addFlag) {
                String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    logger.debug("going to delete " + deleteArray[i]);
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        }
        logger.debug("filesDropzone = " + Value);


        validateAnnualDemandChild(request, pi_annual_demandDTO);

        Utils.handleTransaction(() -> {
            long returnedID = -1;

            if (addFlag) {
                returnedID = Pi_annual_demandDAO.getInstance().add(pi_annual_demandDTO);
            } else {
                returnedID = Pi_annual_demandDAO.getInstance().update(pi_annual_demandDTO);
            }


            List<PiAnnualDemandChildDTO> newItems = createPiAnnualDemandChildDTOListByRequest(request, pi_annual_demandDTO);
            List<PiAnnualDemandChildDTO> oldItems = (List<PiAnnualDemandChildDTO>) PiAnnualDemandChildDAO.getInstance()
                    .getDTOsByParent("pi_annual_demand_id", pi_annual_demandDTO.iD);
            // IDS WITH -1 ARE FOR ADD
            List<PiAnnualDemandChildDTO> itemsToAdd = newItems.stream()
                    .filter(dto -> dto.iD == -1).collect(Collectors.toList());
            // COMMON DTOS ARE FOR EDIT
            List<PiAnnualDemandChildDTO> itemsToEdit = newItems.stream()
                    .filter(dto -> fieldContains(oldItems, dto.iD)).collect(Collectors.toList());
            // IDS NOT COMMON AND NOT -1 ARE FOR DELETE
            List<PiAnnualDemandChildDTO> itemsToDelete = oldItems.stream()
                    .filter(dto -> !fieldContains(itemsToEdit, dto.iD)).collect(Collectors.toList());

            for (PiAnnualDemandChildDTO lotDTO : itemsToAdd) {
                lotDTO.insertedBy = lotDTO.modifiedBy = String.valueOf(userDTO.employee_record_id);
                lotDTO.insertionDate = lotDTO.lastModificationTime = System.currentTimeMillis();
                PiAnnualDemandChildDAO.getInstance().add(lotDTO);
            }
            for (PiAnnualDemandChildDTO lotDTO : itemsToEdit) {
                lotDTO.modifiedBy = String.valueOf(userDTO.employee_record_id);
                lotDTO.lastModificationTime = System.currentTimeMillis();
                PiAnnualDemandChildDAO.getInstance().update(lotDTO);
            }
            for (PiAnnualDemandChildDTO lotDTO : itemsToDelete) {
                PiAnnualDemandChildDAO.getInstance()
                        .delete(userDTO.employee_record_id, lotDTO.iD, System.currentTimeMillis());
            }
        });

        return pi_annual_demandDTO;
    }

    private void validateAnnualDemandChild(HttpServletRequest request, Pi_annual_demandDTO piAnnualDemandDTO)
            throws Exception {
        List<PiAnnualDemandChildDTO> childDTOs = createPiAnnualDemandChildDTOListByRequest(request, piAnnualDemandDTO);

        if (childDTOs.isEmpty()) {
            UtilCharacter.throwException("অন্তত একটা আইটেম এন্ট্রি করুন!", "Give at least 1 item entry!");
        }

        for (PiAnnualDemandChildDTO childDTO : childDTOs) {
            if (childDTO.itemQuantity == -1) {
                UtilCharacter.throwException("আইটেমের সংখ্যা পাওয়া যায়নি!", "Give item amount not found!");
            } else if (childDTO.piUnitId == -1) {
                UtilCharacter.throwException("আইটেমের এককের আইডি পাওয়া যায়নি!", "Item unit id not found!");
            } else if (childDTO.itemGroupId == -1) {
                UtilCharacter.throwException("আইটেমের গ্রুপ আইডি পাওয়া যায়নি!", "Item group id not found!");
            } else if (childDTO.itemTypeId == -1) {
                UtilCharacter.throwException("আইটেমের টাইপ আইডি পাওয়া যায়নি!", "Item type id not found!");
            } else if (childDTO.itemId == -1) {
                UtilCharacter.throwException("আইটেমের আইডি পাওয়া যায়নি!", "Item id not found!");
            } else if (childDTO.serialNumber == -1) {
                UtilCharacter.throwException("আইটেমের সিরিয়াল নাম্বার পাওয়া যায়নি!", "Item serial number not found!");
            }
        }
    }

    private List<PiAnnualDemandChildDTO> createPiAnnualDemandChildDTOListByRequest(HttpServletRequest request,
                                                                                   Pi_annual_demandDTO annual_demandDTO) {
        List<PiAnnualDemandChildDTO> piAnnualDemandChildDTOList = new ArrayList<>();
        if (request.getParameterValues("piAnnualDemandChild.iD") != null) {
            int piAnnualDemandChildItemNo = request.getParameterValues("piAnnualDemandChild.iD").length;

            for (int index = 0; index < piAnnualDemandChildItemNo; index++) {
                PiAnnualDemandChildDTO piAnnualDemandChildDTO =
                        createPiAnnualDemandChildDTOByRequestAndIndex(request, index);
                piAnnualDemandChildDTO.piAnnualDemandId = annual_demandDTO.iD;
                piAnnualDemandChildDTO.officeUnitId = annual_demandDTO.officeUnitId;
                piAnnualDemandChildDTO.fiscalYear = annual_demandDTO.fiscalYear;
                piAnnualDemandChildDTO.piPackageNewId = annual_demandDTO.piPackageNewId;
                piAnnualDemandChildDTO.piLotId = annual_demandDTO.piLotId;
                piAnnualDemandChildDTOList.add(piAnnualDemandChildDTO);
            }
        }
        return piAnnualDemandChildDTOList;
    }

    private PiAnnualDemandChildDTO createPiAnnualDemandChildDTOByRequestAndIndex(HttpServletRequest request, int index) {
        PiAnnualDemandChildDTO piAnnualDemandChildDTO;
        piAnnualDemandChildDTO = new PiAnnualDemandChildDTO();


        String Value = "";
        Value = request.getParameterValues("piAnnualDemandChild.iD")[index];
        if (StringUtils.isValidString(Value)) {
            piAnnualDemandChildDTO.iD = Long.parseLong(Value);
        } else {
            piAnnualDemandChildDTO.iD = -1;
        }


        Value = request.getParameterValues("piAnnualDemandChild.piUnitId")[index];
        if (StringUtils.isValidString(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            piAnnualDemandChildDTO.piUnitId = Long.parseLong(Value);
        }


        Value = request.getParameterValues("piAnnualDemandChild.piItemGroupId")[index];
        if (StringUtils.isValidString(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            piAnnualDemandChildDTO.itemGroupId = Long.parseLong(Value);
        }


        Value = request.getParameterValues("piAnnualDemandChild.piItemTypeId")[index];
        if (StringUtils.isValidString(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            piAnnualDemandChildDTO.itemTypeId = Long.parseLong(Value);
        }


        Value = request.getParameterValues("piAnnualDemandChild.piItemId")[index];
        if (StringUtils.isValidString(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            piAnnualDemandChildDTO.itemId = Long.parseLong(Value);
        }


        Value = request.getParameterValues("piAnnualDemandChild.serialNumber")[index];
        if (StringUtils.isValidString(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            piAnnualDemandChildDTO.serialNumber = Long.parseLong(Value);
        }


        Value = request.getParameterValues("piAnnualDemandChild.itemDescription")[index];
        if (StringUtils.isValidString(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            piAnnualDemandChildDTO.description = (Value);
        }


        Value = request.getParameterValues("piAnnualDemandChild.itemQuantity")[index];
        if (StringUtils.isValidString(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            piAnnualDemandChildDTO.itemQuantity = Long.parseLong(Value);
        }


        Value = request.getParameterValues("piAnnualDemandChild.itemUnitPrice")[index];
        if (StringUtils.isValidString(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            piAnnualDemandChildDTO.itemUnitPrice = Long.parseLong(Value);
        } else {
            piAnnualDemandChildDTO.itemUnitPrice = 0;
        }


        Value = request.getParameterValues("piAnnualDemandChild.itemTotalPrice")[index];
        if (StringUtils.isValidString(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            piAnnualDemandChildDTO.itemTotalPrice = Value;
        } else {
            piAnnualDemandChildDTO.itemTotalPrice = "0";
        }


        return piAnnualDemandChildDTO;
    }

    private boolean isValueValid(String value) {
        return !(value == null || value.equalsIgnoreCase("-1") || value.equalsIgnoreCase(""));
    }

    private boolean fieldContains(List<PiAnnualDemandChildDTO> annualDemandChildDTOS, Long id) {
        List<PiAnnualDemandChildDTO> dto = annualDemandChildDTOS.stream().filter(lotDTO -> lotDTO.iD == id).collect(Collectors.toList());
        return dto.size() > 0;
    }

    private void validatePackage(long fiscalYear, long packageId, String Language) throws Exception {
        // IF ANY PACKAGE HAS LOT, THEN LOT VALIDATION WILL BE EXECUTED
        List<Pi_package_lotDTO> lotDTOs = Pi_package_lotDAO.getInstance().getLotsByPackageId(packageId);
        if (lotDTOs != null && !lotDTOs.isEmpty()) {
            return;
        }

        List<Pi_annual_demandDTO> annual_demandDTOS = Pi_annual_demandDAO.getInstance()
                .getAnnualDemandsByFiscalYearAndPackage(fiscalYear, packageId);
        // THE PACKAGE IS NOT VALID, IF THIS IS ALREADY USED IN THIS FISCAL YEAR
        if (!(annual_demandDTOS == null || annual_demandDTOS.isEmpty())) {
            throw new Exception(UtilCharacter
                    .getDataByLanguage(Language, "এই প্যাকেজটি এই অর্থবছরে ইতিমধ্যে ব্যাবহৃত হচ্ছে!",
                            "This package is already used in this fiscal year!"));
        }
    }

    private void validateLot(long fiscalYear, long lotId, String Language) throws Exception {
        List<Pi_annual_demandDTO> annual_demandDTOS = Pi_annual_demandDAO.getInstance()
                .getAnnualDemandsByFiscalYearAndLot(fiscalYear, lotId);
        // THE LOT IS NOT VALID, IF THIS IS ALREADY USED IN THIS FISCAL YEAR
        if (!(annual_demandDTOS == null || annual_demandDTOS.isEmpty())) {
            throw new Exception(UtilCharacter
                    .getDataByLanguage(Language, "এই লট এই অর্থবছরে ইতিমধ্যে ব্যাবহৃত হচ্ছে!",
                            "This lot is already used in this fiscal year!"));
        }
    }

    private void validateIfLotExistInsideThisPackage(long packageId, String Language) throws Exception {
        List<Pi_package_lotDTO> lotDTOs = Pi_package_lotDAO.getInstance().getLotsByPackageId(packageId);
        if (lotDTOs != null && !lotDTOs.isEmpty()) {
            throw new Exception(UtilCharacter
                    .getDataByLanguage(Language, "অনুগ্রহপূর্বক লট নির্বাচন করুন!", "Please select lot!"));
        }
    }
}

