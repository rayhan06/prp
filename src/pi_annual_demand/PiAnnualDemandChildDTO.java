package pi_annual_demand;

import java.util.*;

import util.*;

public class PiAnnualDemandChildDTO extends CommonDTO {
    public long piAnnualDemandId = -1;
    public long fiscalYear = -1;
    public long officeUnitId = -1;
    public long serialNumber = -1;
    public long piPackageNewId = -1;
    public long piLotId = -1;
    public long itemGroupId = -1;
    public long itemTypeId = -1;
    public long itemId = -1;
    public String description = "";
    public long itemQuantity = -1;
    public long piUnitId = -1;
    public long itemUnitPrice = -1;
    public String itemTotalPrice = "";
    public long piPackageFinalId = -1;
    public long piLotFinalId = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
    public long insertionDate = -1;

    public List<PiAnnualDemandChildDTO> piAnnualDemandChildDTOList = new ArrayList<>();

    @Override
    public String toString() {
        return "$PiAnnualDemandChildDTO[" +
                " iD = " + iD +
                " piAnnualDemandId = " + piAnnualDemandId +
                " fiscalYear = " + fiscalYear +
                " officeUnitId = " + officeUnitId +
                " piPackageNewId = " + piPackageNewId +
                " piLotId = " + piLotId +
                " itemGroupId = " + itemGroupId +
                " itemTypeId = " + itemTypeId +
                " itemId = " + itemId +
                " description = " + description +
                " itemQuantity = " + itemQuantity +
                " piUnitId = " + piUnitId +
                " itemUnitPrice = " + itemUnitPrice +
                " itemTotalPrice = " + itemTotalPrice +
                " piPackageFinalId = " + piPackageFinalId +
                " piLotFinalId = " + piLotFinalId +
                " searchColumn = " + searchColumn +
                " insertedBy = " + insertedBy +
                " modifiedBy = " + modifiedBy +
                " insertionDate = " + insertionDate +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }
}