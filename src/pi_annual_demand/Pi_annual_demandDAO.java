package pi_annual_demand;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Pi_annual_demandDAO implements CommonDAOService<Pi_annual_demandDTO> {
    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private static final String getAnnualDemandsByFiscalYearAndPackage =
            "SELECT * FROM pi_annual_demand WHERE isDeleted = 0 " +
                    "AND fiscal_year=%d " +
                    "AND pi_package_new_id=%d " +
                    "ORDER BY lastModificationTime DESC";
    private static final String getAnnualDemandsByFiscalYearAndLot =
            "SELECT * FROM pi_annual_demand WHERE isDeleted = 0 " +
                    "AND fiscal_year=%d " +
                    "AND pi_lot_id=%d " +
                    "ORDER BY lastModificationTime DESC";

    private String[] columnNames = null;

    private Pi_annual_demandDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "nothi_no",
                        "creation_date",
                        "fiscal_year",
                        "office_unit_id",
                        "has_package_lot",
                        "pi_package_new_id",
                        "pi_lot_id",
                        "files_dropZone",
                        "search_column",
                        "inserted_by",
                        "modified_by",
                        "insertion_date",
                        "isDeleted",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("nothi_no", " and (nothi_no like ?)");
        searchMap.put("office_unit_id", " and (office_unit_id = ?)");
        searchMap.put("creation_date_start", " and (creation_date >= ?)");
        searchMap.put("creation_date_end", " and (creation_date <= ?)");
        searchMap.put("insertion_date_start", " and (insertion_date >= ?)");
        searchMap.put("insertion_date_end", " and (insertion_date <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Pi_annual_demandDAO INSTANCE = new Pi_annual_demandDAO();
    }

    public static Pi_annual_demandDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Pi_annual_demandDTO pi_annual_demandDTO) {
        pi_annual_demandDTO.searchColumn = "";
        pi_annual_demandDTO.searchColumn += pi_annual_demandDTO.nothiNo + " ";
        pi_annual_demandDTO.searchColumn += pi_annual_demandDTO.insertedBy + " ";
        pi_annual_demandDTO.searchColumn += pi_annual_demandDTO.modifiedBy + " ";
    }

    @Override
    public void set(PreparedStatement ps, Pi_annual_demandDTO pi_annual_demandDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(pi_annual_demandDTO);
        if (isInsert) {
            ps.setObject(++index, pi_annual_demandDTO.iD);
        }
        ps.setObject(++index, pi_annual_demandDTO.nothiNo);
        ps.setObject(++index, pi_annual_demandDTO.creationDate);
        ps.setObject(++index, pi_annual_demandDTO.fiscalYear);
        ps.setObject(++index, pi_annual_demandDTO.officeUnitId);
        ps.setObject(++index, pi_annual_demandDTO.hasPackageLot);
        ps.setObject(++index, pi_annual_demandDTO.piPackageNewId);
        ps.setObject(++index, pi_annual_demandDTO.piLotId);
        ps.setObject(++index, pi_annual_demandDTO.filesDropZone);
        ps.setObject(++index, pi_annual_demandDTO.searchColumn);
        ps.setObject(++index, pi_annual_demandDTO.insertedBy);
        ps.setObject(++index, pi_annual_demandDTO.modifiedBy);
        ps.setObject(++index, pi_annual_demandDTO.insertionDate);
        if (isInsert) {
            ps.setObject(++index, pi_annual_demandDTO.isDeleted);
        }
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, pi_annual_demandDTO.iD);
        }
    }

    @Override
    public Pi_annual_demandDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Pi_annual_demandDTO pi_annual_demandDTO = new Pi_annual_demandDTO();
            int i = 0;
            pi_annual_demandDTO.iD = rs.getLong(columnNames[i++]);
            pi_annual_demandDTO.nothiNo = rs.getString(columnNames[i++]);
            pi_annual_demandDTO.creationDate = rs.getLong(columnNames[i++]);
            pi_annual_demandDTO.fiscalYear = rs.getLong(columnNames[i++]);
            pi_annual_demandDTO.officeUnitId = rs.getLong(columnNames[i++]);
            pi_annual_demandDTO.hasPackageLot = rs.getBoolean(columnNames[i++]);
            pi_annual_demandDTO.piPackageNewId = rs.getLong(columnNames[i++]);
            pi_annual_demandDTO.piLotId = rs.getLong(columnNames[i++]);
            pi_annual_demandDTO.filesDropZone = rs.getLong(columnNames[i++]);
            pi_annual_demandDTO.searchColumn = rs.getString(columnNames[i++]);
            pi_annual_demandDTO.insertedBy = rs.getString(columnNames[i++]);
            pi_annual_demandDTO.modifiedBy = rs.getString(columnNames[i++]);
            pi_annual_demandDTO.insertionDate = rs.getLong(columnNames[i++]);
            pi_annual_demandDTO.isDeleted = rs.getInt(columnNames[i++]);
            pi_annual_demandDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            return pi_annual_demandDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Pi_annual_demandDTO getDTOByID(long id) {
        Pi_annual_demandDTO pi_annual_demandDTO = null;
        try {
            pi_annual_demandDTO = getDTOFromID(id);
            if (pi_annual_demandDTO != null) {
                PiAnnualDemandChildDAO piAnnualDemandChildDAO = PiAnnualDemandChildDAO.getInstance();
                List<PiAnnualDemandChildDTO> piAnnualDemandChildDTOList = (List<PiAnnualDemandChildDTO>) piAnnualDemandChildDAO.getDTOsByParent("pi_annual_demand_id", pi_annual_demandDTO.iD);
                pi_annual_demandDTO.piAnnualDemandChildDTOList = piAnnualDemandChildDTOList;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return pi_annual_demandDTO;
    }

    @Override
    public String getTableName() {
        return "pi_annual_demand";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_annual_demandDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_annual_demandDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

    public List<Pi_annual_demandDTO> getAnnualDemandsByFiscalYearAndPackage(long fiscalYearId, long packageId) {
        String sql = String.format(String.format(getAnnualDemandsByFiscalYearAndPackage, fiscalYearId, packageId));
        return Pi_annual_demandDAO.getInstance().getDTOs(sql);
    }

    public List<Pi_annual_demandDTO> getAnnualDemandsByFiscalYearAndLot(long fiscalYearId, long lotId) {
        String sql = String.format(String.format(getAnnualDemandsByFiscalYearAndLot, fiscalYearId, lotId));
        return Pi_annual_demandDAO.getInstance().getDTOs(sql);
    }
}
	