package pi_annual_demand;
import java.util.*; 
import util.*;


public class PiAnnualDemandChildMAPS extends CommonMaps
{	
	public PiAnnualDemandChildMAPS(String tableName)
	{
		


		java_SQL_map.put("pi_annual_demand_id".toLowerCase(), "piAnnualDemandId".toLowerCase());
		java_SQL_map.put("fiscal_year".toLowerCase(), "fiscalYear".toLowerCase());
		java_SQL_map.put("office_unit_id".toLowerCase(), "officeUnitId".toLowerCase());
		java_SQL_map.put("pi_package_new_id".toLowerCase(), "piPackageNewId".toLowerCase());
		java_SQL_map.put("pi_lot_id".toLowerCase(), "piLotId".toLowerCase());
		java_SQL_map.put("item_group_id".toLowerCase(), "itemGroupId".toLowerCase());
		java_SQL_map.put("item_type_id".toLowerCase(), "itemTypeId".toLowerCase());
		java_SQL_map.put("item_id".toLowerCase(), "itemId".toLowerCase());
		java_SQL_map.put("description".toLowerCase(), "description".toLowerCase());
		java_SQL_map.put("item_quantity".toLowerCase(), "itemQuantity".toLowerCase());
		java_SQL_map.put("pi_unit_id".toLowerCase(), "piUnitId".toLowerCase());
		java_SQL_map.put("item_unit_price".toLowerCase(), "itemUnitPrice".toLowerCase());
		java_SQL_map.put("item_total_price".toLowerCase(), "itemTotalPrice".toLowerCase());
		java_SQL_map.put("inserted_by".toLowerCase(), "insertedBy".toLowerCase());
		java_SQL_map.put("modified_by".toLowerCase(), "modifiedBy".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Pi Annual Demand Id".toLowerCase(), "piAnnualDemandId".toLowerCase());
		java_Text_map.put("Fiscal Year".toLowerCase(), "fiscalYear".toLowerCase());
		java_Text_map.put("Office Unit Id".toLowerCase(), "officeUnitId".toLowerCase());
		java_Text_map.put("Pi Package New Id".toLowerCase(), "piPackageNewId".toLowerCase());
		java_Text_map.put("Pi Lot Id".toLowerCase(), "piLotId".toLowerCase());
		java_Text_map.put("Item Group Id".toLowerCase(), "itemGroupId".toLowerCase());
		java_Text_map.put("Item Type Id".toLowerCase(), "itemTypeId".toLowerCase());
		java_Text_map.put("Item Id".toLowerCase(), "itemId".toLowerCase());
		java_Text_map.put("Description".toLowerCase(), "description".toLowerCase());
		java_Text_map.put("Item Quantity".toLowerCase(), "itemQuantity".toLowerCase());
		java_Text_map.put("Pi Unit Id".toLowerCase(), "piUnitId".toLowerCase());
		java_Text_map.put("Item Unit Price".toLowerCase(), "itemUnitPrice".toLowerCase());
		java_Text_map.put("Item Total Price".toLowerCase(), "itemTotalPrice".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}