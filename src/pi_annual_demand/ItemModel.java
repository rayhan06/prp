package pi_annual_demand;

public class ItemModel {
    public long serialNumber = -1;
    public long fiscalYear = -1;
    public long officeUnitId = -1;
    public String officeUnitText = "";
    public long piPackageNewId = -1;
    public long piLotId = -1;
    public long itemGroupId = -1;
    public long itemTypeId = -1;
    public long itemId = -1;
    public String descriptionEn = "";
    public String descriptionBn = "";
    public long itemQuantity = -1;
    public long piUnitId = -1;
    String piUnitNameEn = "";
    String piUnitNameBn = "";
    public long itemUnitPrice = -1;
    public String itemTotalPrice = "";
    public long annualDemandId = -1;
    public long annualDemandChildId = -1;
    public long piPackageFinalId = -1;
    public long piLotFinalId = -1;
}
