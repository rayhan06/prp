select menuid from menu where constantName = 'PI_ANNUAL_DEMAND';
select menuid from menu where constantName = 'PI_ANNUAL_DEMAND_ADD';
Delete from menu where menuid = 1557441;
Delete from menu_permission where menuid = 1557441;
select menuid from menu where constantName = 'PI_ANNUAL_DEMAND_UPDATE';
Delete from menu where menuid = 1557442;
Delete from menu_permission where menuid = 1557442;
select menuid from menu where constantName = 'PI_ANNUAL_DEMAND_SEARCH';
Delete from menu where menuid = 1557443;
Delete from menu_permission where menuid = 1557443;
select menuid from menu where constantName = 'PI_ANNUAL_DEMAND_UPLOAD';
select menuid from menu where constantName = 'PI_ANNUAL_DEMAND_APPROVE';
INSERT INTO `menu` (`menuID`, `parentMenuID`, `menuName`, `menuNameBangla`, `languageTextID`, `orderIndex`, `selectedMenuID`, `isVisible`, `requestMethodType`, `hyperLink`, `icon`, `isAPI`, `constantName`)
VALUES ('1558440','-1','PI ANNUAL DEMAND','অজানা বার্ষিক দাবী করা','-1','1','-1','1','1','','fa fa-arrows','0','PI_ANNUAL_DEMAND'),
       ('1558441','1558441','ADD','যোগ করুন','-1','1','-1','1','1','','fa fa-arrows','0','PI_ANNUAL_DEMAND_ADD'),
       ('1558442','1558441','UPDATE','আপডেট','-1','1','-1','1','1','','fa fa-arrows','0','PI_ANNUAL_DEMAND_UPDATE'),
       ('1558443','1558441','SEARCH','খুঁজন','-1','1','-1','1','1','','fa fa-arrows','0','PI_ANNUAL_DEMAND_SEARCH');

INSERT INTO menu_permission VALUES(1, 1558440);
INSERT INTO menu_permission VALUES(1, 1558441);
INSERT INTO menu_permission VALUES(1, 1558442);
INSERT INTO menu_permission VALUES(1, 1558443);

UPDATE prp_live.menu SET parentMenuID = 1257704, menuName = 'ANNUAL DEMAND', menuNameBangla = 'বার্ষিক চাহিদা', languageTextID = -1, orderIndex = 1, selectedMenuID = -1, isVisible = 1, requestMethodType = 1, hyperLink = '', icon = 'fa fa-arrows', isAPI = 0, constantName = 'PI_ANNUAL_DEMAND', isDeleted = 0, lastModificationTime = 0 WHERE menuID = 1558440;
UPDATE prp_live.menu SET parentMenuID = 1558440, menuName = 'ADD', menuNameBangla = 'যোগ করুন', languageTextID = -1, orderIndex = 1, selectedMenuID = -1, isVisible = 1, requestMethodType = 1, hyperLink = 'Pi_annual_demandServlet?actionType=getAddPage', icon = 'fa fa-arrows', isAPI = 0, constantName = 'PI_ANNUAL_DEMAND_ADD', isDeleted = 0, lastModificationTime = 0 WHERE menuID = 1558441;
UPDATE prp_live.menu SET parentMenuID = 1558440, menuName = 'UPDATE', menuNameBangla = 'আপডেট', languageTextID = -1, orderIndex = 1, selectedMenuID = -1, isVisible = -1, requestMethodType = 1, hyperLink = 'Pi_annual_demandServlet?actionType=edit', icon = 'fa fa-arrows', isAPI = 0, constantName = 'PI_ANNUAL_DEMAND_UPDATE', isDeleted = 0, lastModificationTime = 0 WHERE menuID = 1558442;
UPDATE prp_live.menu SET parentMenuID = 1558440, menuName = 'SEARCH', menuNameBangla = 'খুঁজন', languageTextID = -1, orderIndex = 1, selectedMenuID = -1, isVisible = 1, requestMethodType = 1, hyperLink = 'Pi_annual_demandServlet?actionType=search', icon = 'fa fa-arrows', isAPI = 0, constantName = 'PI_ANNUAL_DEMAND_SEARCH', isDeleted = 0, lastModificationTime = 0 WHERE menuID = 1558443;
