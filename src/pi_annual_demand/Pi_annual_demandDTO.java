package pi_annual_demand;
import java.util.*; 
import util.*; 


public class Pi_annual_demandDTO extends CommonDTO
{

    public String nothiNo = "";
	public long creationDate = System.currentTimeMillis();
	public long fiscalYear = -1;
	public long officeUnitId = -1;
	public boolean hasPackageLot = false;
	public long piPackageNewId = -1;
	public long piLotId = -1;
	public long filesDropZone = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
	public long insertionDate = -1;
	
	public List<PiAnnualDemandChildDTO> piAnnualDemandChildDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Pi_annual_demandDTO[" +
            " iD = " + iD +
            " nothiNo = " + nothiNo +
            " creationDate = " + creationDate +
            " fiscalYear = " + fiscalYear +
            " officeUnitId = " + officeUnitId +
            " hasPackageLot = " + hasPackageLot +
            " piPackageNewId = " + piPackageNewId +
            " piLotId = " + piLotId +
            " filesDropZone = " + filesDropZone +
            " searchColumn = " + searchColumn +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}