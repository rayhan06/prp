package pi_annual_demand;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Pi_annual_demandRepository implements Repository {
	Pi_annual_demandDAO pi_annual_demandDAO = null;
	
	static Logger logger = Logger.getLogger(Pi_annual_demandRepository.class);
	Map<Long, Pi_annual_demandDTO>mapOfPi_annual_demandDTOToiD;
	Map<Long, Set<Pi_annual_demandDTO> >mapOfPi_annual_demandDTOTofiscalYear;
	Map<Long, Set<Pi_annual_demandDTO> >mapOfPi_annual_demandDTOToofficeUnitId;
	Map<Boolean, Set<Pi_annual_demandDTO> >mapOfPi_annual_demandDTOTohasPackageLot;
	Map<Long, Set<Pi_annual_demandDTO> >mapOfPi_annual_demandDTOTopiPackageNewId;
	Map<Long, Set<Pi_annual_demandDTO> >mapOfPi_annual_demandDTOTopiLotId;
	Gson gson;

  
	private Pi_annual_demandRepository(){
		pi_annual_demandDAO = Pi_annual_demandDAO.getInstance();
		mapOfPi_annual_demandDTOToiD = new ConcurrentHashMap<>();
		mapOfPi_annual_demandDTOTofiscalYear = new ConcurrentHashMap<>();
		mapOfPi_annual_demandDTOToofficeUnitId = new ConcurrentHashMap<>();
		mapOfPi_annual_demandDTOTohasPackageLot = new ConcurrentHashMap<>();
		mapOfPi_annual_demandDTOTopiPackageNewId = new ConcurrentHashMap<>();
		mapOfPi_annual_demandDTOTopiLotId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Pi_annual_demandRepository INSTANCE = new Pi_annual_demandRepository();
    }

    public static Pi_annual_demandRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Pi_annual_demandDTO> pi_annual_demandDTOs = pi_annual_demandDAO.getAllDTOs(reloadAll);
			for(Pi_annual_demandDTO pi_annual_demandDTO : pi_annual_demandDTOs) {
				Pi_annual_demandDTO oldPi_annual_demandDTO = getPi_annual_demandDTOByiD(pi_annual_demandDTO.iD);
				if( oldPi_annual_demandDTO != null ) {
					mapOfPi_annual_demandDTOToiD.remove(oldPi_annual_demandDTO.iD);
				
					if(mapOfPi_annual_demandDTOTofiscalYear.containsKey(oldPi_annual_demandDTO.fiscalYear)) {
						mapOfPi_annual_demandDTOTofiscalYear.get(oldPi_annual_demandDTO.fiscalYear).remove(oldPi_annual_demandDTO);
					}
					if(mapOfPi_annual_demandDTOTofiscalYear.get(oldPi_annual_demandDTO.fiscalYear).isEmpty()) {
						mapOfPi_annual_demandDTOTofiscalYear.remove(oldPi_annual_demandDTO.fiscalYear);
					}
					
					if(mapOfPi_annual_demandDTOToofficeUnitId.containsKey(oldPi_annual_demandDTO.officeUnitId)) {
						mapOfPi_annual_demandDTOToofficeUnitId.get(oldPi_annual_demandDTO.officeUnitId).remove(oldPi_annual_demandDTO);
					}
					if(mapOfPi_annual_demandDTOToofficeUnitId.get(oldPi_annual_demandDTO.officeUnitId).isEmpty()) {
						mapOfPi_annual_demandDTOToofficeUnitId.remove(oldPi_annual_demandDTO.officeUnitId);
					}
					
					if(mapOfPi_annual_demandDTOTohasPackageLot.containsKey(oldPi_annual_demandDTO.hasPackageLot)) {
						mapOfPi_annual_demandDTOTohasPackageLot.get(oldPi_annual_demandDTO.hasPackageLot).remove(oldPi_annual_demandDTO);
					}
					if(mapOfPi_annual_demandDTOTohasPackageLot.get(oldPi_annual_demandDTO.hasPackageLot).isEmpty()) {
						mapOfPi_annual_demandDTOTohasPackageLot.remove(oldPi_annual_demandDTO.hasPackageLot);
					}
					
					if(mapOfPi_annual_demandDTOTopiPackageNewId.containsKey(oldPi_annual_demandDTO.piPackageNewId)) {
						mapOfPi_annual_demandDTOTopiPackageNewId.get(oldPi_annual_demandDTO.piPackageNewId).remove(oldPi_annual_demandDTO);
					}
					if(mapOfPi_annual_demandDTOTopiPackageNewId.get(oldPi_annual_demandDTO.piPackageNewId).isEmpty()) {
						mapOfPi_annual_demandDTOTopiPackageNewId.remove(oldPi_annual_demandDTO.piPackageNewId);
					}
					
					if(mapOfPi_annual_demandDTOTopiLotId.containsKey(oldPi_annual_demandDTO.piLotId)) {
						mapOfPi_annual_demandDTOTopiLotId.get(oldPi_annual_demandDTO.piLotId).remove(oldPi_annual_demandDTO);
					}
					if(mapOfPi_annual_demandDTOTopiLotId.get(oldPi_annual_demandDTO.piLotId).isEmpty()) {
						mapOfPi_annual_demandDTOTopiLotId.remove(oldPi_annual_demandDTO.piLotId);
					}
					
					
				}
				if(pi_annual_demandDTO.isDeleted == 0) 
				{
					
					mapOfPi_annual_demandDTOToiD.put(pi_annual_demandDTO.iD, pi_annual_demandDTO);
				
					if( ! mapOfPi_annual_demandDTOTofiscalYear.containsKey(pi_annual_demandDTO.fiscalYear)) {
						mapOfPi_annual_demandDTOTofiscalYear.put(pi_annual_demandDTO.fiscalYear, new HashSet<>());
					}
					mapOfPi_annual_demandDTOTofiscalYear.get(pi_annual_demandDTO.fiscalYear).add(pi_annual_demandDTO);
					
					if( ! mapOfPi_annual_demandDTOToofficeUnitId.containsKey(pi_annual_demandDTO.officeUnitId)) {
						mapOfPi_annual_demandDTOToofficeUnitId.put(pi_annual_demandDTO.officeUnitId, new HashSet<>());
					}
					mapOfPi_annual_demandDTOToofficeUnitId.get(pi_annual_demandDTO.officeUnitId).add(pi_annual_demandDTO);
					
					if( ! mapOfPi_annual_demandDTOTohasPackageLot.containsKey(pi_annual_demandDTO.hasPackageLot)) {
						mapOfPi_annual_demandDTOTohasPackageLot.put(pi_annual_demandDTO.hasPackageLot, new HashSet<>());
					}
					mapOfPi_annual_demandDTOTohasPackageLot.get(pi_annual_demandDTO.hasPackageLot).add(pi_annual_demandDTO);
					
					if( ! mapOfPi_annual_demandDTOTopiPackageNewId.containsKey(pi_annual_demandDTO.piPackageNewId)) {
						mapOfPi_annual_demandDTOTopiPackageNewId.put(pi_annual_demandDTO.piPackageNewId, new HashSet<>());
					}
					mapOfPi_annual_demandDTOTopiPackageNewId.get(pi_annual_demandDTO.piPackageNewId).add(pi_annual_demandDTO);
					
					if( ! mapOfPi_annual_demandDTOTopiLotId.containsKey(pi_annual_demandDTO.piLotId)) {
						mapOfPi_annual_demandDTOTopiLotId.put(pi_annual_demandDTO.piLotId, new HashSet<>());
					}
					mapOfPi_annual_demandDTOTopiLotId.get(pi_annual_demandDTO.piLotId).add(pi_annual_demandDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Pi_annual_demandDTO clone(Pi_annual_demandDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Pi_annual_demandDTO.class);
	}
	
	
	public List<Pi_annual_demandDTO> getPi_annual_demandList() {
		List <Pi_annual_demandDTO> pi_annual_demands = new ArrayList<Pi_annual_demandDTO>(this.mapOfPi_annual_demandDTOToiD.values());
		return pi_annual_demands;
	}
	
	public List<Pi_annual_demandDTO> copyPi_annual_demandList() {
		List <Pi_annual_demandDTO> pi_annual_demands = getPi_annual_demandList();
		return pi_annual_demands
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Pi_annual_demandDTO getPi_annual_demandDTOByiD( long iD){
		return mapOfPi_annual_demandDTOToiD.get(iD);
	}
	
	public Pi_annual_demandDTO copyPi_annual_demandDTOByiD( long iD){
		return clone(mapOfPi_annual_demandDTOToiD.get(iD));
	}
	
	
	public List<Pi_annual_demandDTO> getPi_annual_demandDTOByfiscalYear(long fiscalYear) {
		return new ArrayList<>( mapOfPi_annual_demandDTOTofiscalYear.getOrDefault(fiscalYear,new HashSet<>()));
	}
	
	public List<Pi_annual_demandDTO> copyPi_annual_demandDTOByfiscalYear(long fiscalYear)
	{
		List <Pi_annual_demandDTO> pi_annual_demands = getPi_annual_demandDTOByfiscalYear(fiscalYear);
		return pi_annual_demands
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<Pi_annual_demandDTO> getPi_annual_demandDTOByofficeUnitId(long officeUnitId) {
		return new ArrayList<>( mapOfPi_annual_demandDTOToofficeUnitId.getOrDefault(officeUnitId,new HashSet<>()));
	}
	
	public List<Pi_annual_demandDTO> copyPi_annual_demandDTOByofficeUnitId(long officeUnitId)
	{
		List <Pi_annual_demandDTO> pi_annual_demands = getPi_annual_demandDTOByofficeUnitId(officeUnitId);
		return pi_annual_demands
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<Pi_annual_demandDTO> getPi_annual_demandDTOByhasPackageLot(boolean hasPackageLot) {
		return new ArrayList<>( mapOfPi_annual_demandDTOTohasPackageLot.getOrDefault(hasPackageLot,new HashSet<>()));
	}
	
	public List<Pi_annual_demandDTO> copyPi_annual_demandDTOByhasPackageLot(boolean hasPackageLot)
	{
		List <Pi_annual_demandDTO> pi_annual_demands = getPi_annual_demandDTOByhasPackageLot(hasPackageLot);
		return pi_annual_demands
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<Pi_annual_demandDTO> getPi_annual_demandDTOBypiPackageNewId(long piPackageNewId) {
		return new ArrayList<>( mapOfPi_annual_demandDTOTopiPackageNewId.getOrDefault(piPackageNewId,new HashSet<>()));
	}
	
	public List<Pi_annual_demandDTO> copyPi_annual_demandDTOBypiPackageNewId(long piPackageNewId)
	{
		List <Pi_annual_demandDTO> pi_annual_demands = getPi_annual_demandDTOBypiPackageNewId(piPackageNewId);
		return pi_annual_demands
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<Pi_annual_demandDTO> getPi_annual_demandDTOBypiLotId(long piLotId) {
		return new ArrayList<>( mapOfPi_annual_demandDTOTopiLotId.getOrDefault(piLotId,new HashSet<>()));
	}
	
	public List<Pi_annual_demandDTO> copyPi_annual_demandDTOBypiLotId(long piLotId)
	{
		List <Pi_annual_demandDTO> pi_annual_demands = getPi_annual_demandDTOBypiLotId(piLotId);
		return pi_annual_demands
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return pi_annual_demandDAO.getTableName();
	}
}


