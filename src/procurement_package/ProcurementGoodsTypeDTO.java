package procurement_package;
import java.util.*; 
import util.*; 


public class ProcurementGoodsTypeDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
	public long procurementPackageId = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	public long modifiedBy = -1;

	public List<ProcurementGoodsTypeDTO> procurementGoodsTypeDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$ProcurementGoodsTypeDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " procurementPackageId = " + procurementPackageId +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            "]";
    }

}