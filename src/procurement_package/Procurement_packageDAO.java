package procurement_package;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pb.PBNameRepository;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Procurement_packageDAO extends NavigationService4 {
    Logger logger = Logger.getLogger(getClass());

    private Procurement_packageDAO(String tableName) {
        super(tableName);
        joinSQL = "";
        commonMaps = new Procurement_packageMAPS(tableName);
        columnNames = new String[]
                {
                        "ID",
                        "name_en",
                        "name_bn",
                        "inserted_by_user_id",
                        "inserted_by_organogram_id",
                        "insertion_date",
                        "search_column",
                        "modified_by",
                        "isDeleted",
                        "lastModificationTime"
                };
    }

    private Procurement_packageDAO() {
        this("procurement_package");
    }

    private static class LazyLoader {
        static final Procurement_packageDAO INSTANCE = new Procurement_packageDAO();
    }

    public static Procurement_packageDAO getInstance() {
        return Procurement_packageDAO.LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Procurement_packageDTO procurement_packageDTO) {
        procurement_packageDTO.searchColumn = "";
        procurement_packageDTO.searchColumn += procurement_packageDTO.nameEn + " ";
        procurement_packageDTO.searchColumn += procurement_packageDTO.nameBn + " ";

    }

    public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException {
        Procurement_packageDTO procurement_packageDTO = (Procurement_packageDTO) commonDTO;
        int index = 1;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(procurement_packageDTO);
        if (isInsert) {
            ps.setObject(index++, procurement_packageDTO.iD);
        }
        ps.setObject(index++, procurement_packageDTO.nameEn);
        ps.setObject(index++, procurement_packageDTO.nameBn);
        ps.setObject(index++, procurement_packageDTO.insertedByUserId);
        ps.setObject(index++, procurement_packageDTO.insertedByOrganogramId);
        ps.setObject(index++, procurement_packageDTO.insertionDate);
        ps.setObject(index++, procurement_packageDTO.searchColumn);
        ps.setObject(index++, procurement_packageDTO.modifiedBy);
        if (isInsert) {
            ps.setObject(index++, 0);
            ps.setObject(index++, lastModificationTime);
        }
    }

    public void get(Procurement_packageDTO procurement_packageDTO, ResultSet rs) throws SQLException {
        procurement_packageDTO.iD = rs.getLong("ID");
        procurement_packageDTO.nameEn = rs.getString("name_en");
        procurement_packageDTO.nameBn = rs.getString("name_bn");
        procurement_packageDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
        procurement_packageDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
        procurement_packageDTO.insertionDate = rs.getLong("insertion_date");
        procurement_packageDTO.isDeleted = rs.getInt("isDeleted");
        procurement_packageDTO.modifiedBy = rs.getLong("modified_by");
        procurement_packageDTO.lastModificationTime = rs.getLong("lastModificationTime");
        procurement_packageDTO.searchColumn = rs.getString("search_column");
    }


    public Procurement_packageDTO build(ResultSet rs) {
        try {
            Procurement_packageDTO procurement_packageDTO = new Procurement_packageDTO();
            procurement_packageDTO.iD = rs.getLong("ID");
            procurement_packageDTO.nameEn = rs.getString("name_en");
            procurement_packageDTO.nameBn = rs.getString("name_bn");
            procurement_packageDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
            procurement_packageDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
            procurement_packageDTO.insertionDate = rs.getLong("insertion_date");
            procurement_packageDTO.isDeleted = rs.getInt("isDeleted");
            procurement_packageDTO.modifiedBy = rs.getLong("modified_by");
            procurement_packageDTO.lastModificationTime = rs.getLong("lastModificationTime");
            procurement_packageDTO.searchColumn = rs.getString("search_column");
            return procurement_packageDTO;
        } catch (Exception ex) {
            logger.error(ex);
            return null;
        }
    }

    public Procurement_packageDTO buildWithChildren(ResultSet rs) {
        try {
            Procurement_packageDTO procurement_packageDTO = new Procurement_packageDTO();
            procurement_packageDTO.iD = rs.getLong("ID");
            procurement_packageDTO.nameEn = rs.getString("name_en");
            procurement_packageDTO.nameBn = rs.getString("name_bn");
            procurement_packageDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
            procurement_packageDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
            procurement_packageDTO.insertionDate = rs.getLong("insertion_date");
            procurement_packageDTO.isDeleted = rs.getInt("isDeleted");
            procurement_packageDTO.modifiedBy = rs.getLong("modified_by");
            procurement_packageDTO.lastModificationTime = rs.getLong("lastModificationTime");
            procurement_packageDTO.searchColumn = rs.getString("search_column");
            procurement_packageDTO.procurementGoodsTypeDTOList = ProcurementGoodsTypeRepository.getInstance().getProcurementGoodsTypeDTOByprocurementPackageId(procurement_packageDTO.iD);
            return procurement_packageDTO;
        } catch (Exception ex) {
            logger.error(ex);
            return null;
        }
    }

    //need another getter for repository
    public Procurement_packageDTO getDTOByID(long ID) {
        String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
        Procurement_packageDTO procurement_packageDTO =
                ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::buildWithChildren);
        return procurement_packageDTO;
    }


    public List<Procurement_packageDTO> getDTOs(Collection recordIDs) {
        String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
        for (int i = 0; i < recordIDs.size(); i++) {
            if (i != 0) {
                sql += ",";
            }
            sql += ((ArrayList) recordIDs).get(i);
        }
        sql += ")  order by lastModificationTime desc";
        printSql(sql);
        return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()), this::build);
    }


    //add repository
    public List<Procurement_packageDTO> getAllProcurement_package(boolean isFirstReload) {
        String sql = "SELECT * FROM " + tableName + " WHERE ";
        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        sql += " order by " + tableName + ".lastModificationTime desc";
        return ConnectionAndStatementUtil.getListOfT(sql, this::build);
    }


    public List<Procurement_packageDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }


    public List<Procurement_packageDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
                                                String filter, boolean tableHasJobCat) {
        List<Object> objectList = new ArrayList<Object>();

        String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat, objectList);
        return ConnectionAndStatementUtil.getListOfT(sql, objectList, this::build);
    }


    public void deleteAllChildren(long ID) {

        ProcurementGoodsTypeDAO procurementGoodsTypeDAO = ProcurementGoodsTypeDAO.getInstance();
        procurementGoodsTypeDAO.deleteAllByParentId(ID);

    }

    public void deletePackages(List<Long> iDsRequestedToDeleteFiltered, HttpServletRequest request, HttpServletResponse response) throws IOException {
        StringBuilder sql = new StringBuilder("UPDATE ")
                .append(tableName)
                .append(" Set isDeleted = 1,")
                .append(" lastModificationTime = ")
                .append(System.currentTimeMillis())
                .append(" WHERE ID in ( ");
        for (Long id : iDsRequestedToDeleteFiltered) {
            sql.append(id + ",");
        }

        if (iDsRequestedToDeleteFiltered.size() != 0) {
            int size = sql.toString().length();
            sql.replace(size - 1, size, ")");
        }

        ConnectionAndStatementUtil.getWriteStatement(st -> {
            try {
                logger.debug(sql);
                st.execute(sql.toString());
//				ProcurementGoodsTypeRepository.getInstance().deleteProcurementGoodsTypeDTOByprocurementPackageId(ID);

            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
//		RequestDispatcher rd = request.getRequestDispatcher("Procurement_packageServlet?actionType=search");
//		try {
//			rd.forward(request, response);
//		} catch (ServletException e) {
//			throw new RuntimeException(e);
//		}
        PBNameRepository.getInstance().reload(false);
        response.sendRedirect("Procurement_packageServlet?actionType=search");
    }


    public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                          UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList) {
        boolean viewAll = false;

        if (p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null) {
            System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
            viewAll = true;
        } else {
            System.out.println("ViewAll is null ");
        }

        String sql = "SELECT ";
        if (category == GETIDS) {
            sql += " " + tableName + ".ID as ID ";
        } else if (category == GETCOUNT) {
            sql += " count( " + tableName + ".ID) as countID ";
        } else if (category == GETDTOS) {
            sql += " distinct " + tableName + ".* ";
        }
        sql += "FROM " + tableName + " ";
        sql += joinSQL;


        String AnyfieldSql = "";
        String AllFieldSql = "";

        if (p_searchCriteria != null) {


            Enumeration names = p_searchCriteria.keys();
            String str, value;

            AnyfieldSql = "(";

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                AnyfieldSql += tableName + ".search_column like ?";
                objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
            }
            AnyfieldSql += ")";
            System.out.println("AnyfieldSql = " + AnyfieldSql);

            AllFieldSql = "(";
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                System.out.println(str + ": " + value);
                if (value != null && !value.equalsIgnoreCase("") && (
                        str.equals("name_en")
                                || str.equals("name_bn")
                                || str.equals("insertion_date_start")
                                || str.equals("insertion_date_end")
                )

                ) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql += " AND  ";
                    }

                    if (str.equals("name_en")) {
                        AllFieldSql += "" + tableName + ".name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
                        i++;
                    } else if (str.equals("name_bn")) {
                        AllFieldSql += "" + tableName + ".name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
                        i++;
                    } else if (str.equals("insertion_date_start")) {
                        AllFieldSql += "" + tableName + ".insertion_date >= " + "?";
                        objectList.add(Long.parseLong((String) p_searchCriteria.get(str)));
                        i++;
                    } else if (str.equals("insertion_date_end")) {
                        AllFieldSql += "" + tableName + ".insertion_date <= " + "?";
                        objectList.add(Long.parseLong((String) p_searchCriteria.get(str)));
                        i++;
                    }


                }
            }

            AllFieldSql += ")";
            System.out.println("AllFieldSql = " + AllFieldSql);


        }


        sql += " WHERE ";

        sql += " (" + tableName + ".isDeleted = 0 ";
        sql += ")";


        if (!filter.equalsIgnoreCase("")) {
            sql += " and " + filter + " ";
        }

        if (!AnyfieldSql.equals("()") && !AnyfieldSql.equals("")) {
            sql += " AND " + AnyfieldSql;

        }
        if (!AllFieldSql.equals("()") && !AllFieldSql.equals("")) {
            sql += " AND " + AllFieldSql;
        }


        sql += " order by " + tableName + ".lastModificationTime desc ";

        printSql(sql);

        if (limit >= 0) {
            sql += " limit " + limit;
        }
        if (offset >= 0) {
            sql += " offset " + offset;
        }

        System.out.println("-------------- sql = " + sql);

        return sql;
    }

}
	