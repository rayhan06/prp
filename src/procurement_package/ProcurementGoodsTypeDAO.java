package procurement_package;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import common.ConnectionAndStatementUtil;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class ProcurementGoodsTypeDAO  extends NavigationService4
{

	Logger logger = Logger.getLogger(getClass());
	private final String deleteWithUserAndModificationTime = ("UPDATE procurement_goods_type " +
			" SET modified_by = %d, lastModificationTime = %d , isDeleted = 1" +
			" WHERE ID = %d ");


	private ProcurementGoodsTypeDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new ProcurementGoodsTypeMAPS(tableName);
		columnNames = new String[]
		{
			"ID",
			"name_en",
			"name_bn",
			"procurement_package_id",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"search_column",
				"modified_by",
				"isDeleted",
				"lastModificationTime"
		};
	}

	private ProcurementGoodsTypeDAO()
	{
		this("procurement_goods_type");
	}

	private static class LazyLoader
	{
		static final ProcurementGoodsTypeDAO INSTANCE = new ProcurementGoodsTypeDAO();
	}

	public static ProcurementGoodsTypeDAO getInstance()
	{
		return ProcurementGoodsTypeDAO.LazyLoader.INSTANCE;
	}


	public void setSearchColumn(ProcurementGoodsTypeDTO procurementgoodstypeDTO)
	{
		procurementgoodstypeDTO.searchColumn = "";
		procurementgoodstypeDTO.searchColumn += procurementgoodstypeDTO.nameEn + " ";
		procurementgoodstypeDTO.searchColumn += procurementgoodstypeDTO.nameBn + " ";

	}

	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		ProcurementGoodsTypeDTO procurementgoodstypeDTO = (ProcurementGoodsTypeDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();
		setSearchColumn(procurementgoodstypeDTO);
		if(isInsert)
		{
			ps.setObject(index++,procurementgoodstypeDTO.iD);
		}
		ps.setObject(index++,procurementgoodstypeDTO.nameEn);
		ps.setObject(index++,procurementgoodstypeDTO.nameBn);
		ps.setObject(index++,procurementgoodstypeDTO.procurementPackageId);
		ps.setObject(index++,procurementgoodstypeDTO.insertedByUserId);
		ps.setObject(index++,procurementgoodstypeDTO.insertedByOrganogramId);
		ps.setObject(index++,procurementgoodstypeDTO.insertionDate);
		ps.setObject(index++,procurementgoodstypeDTO.searchColumn);
		ps.setObject(index++,procurementgoodstypeDTO.modifiedBy);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}

	public void get(ProcurementGoodsTypeDTO procurementgoodstypeDTO, ResultSet rs) throws SQLException
	{
		procurementgoodstypeDTO.iD = rs.getLong("ID");
		procurementgoodstypeDTO.nameEn = rs.getString("name_en");
		procurementgoodstypeDTO.nameBn = rs.getString("name_bn");
		procurementgoodstypeDTO.procurementPackageId = rs.getLong("procurement_package_id");
		procurementgoodstypeDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		procurementgoodstypeDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
		procurementgoodstypeDTO.insertionDate = rs.getLong("insertion_date");
		procurementgoodstypeDTO.isDeleted = rs.getInt("isDeleted");
		procurementgoodstypeDTO.modifiedBy = rs.getLong("modified_by");
		procurementgoodstypeDTO.lastModificationTime = rs.getLong("lastModificationTime");
		procurementgoodstypeDTO.searchColumn = rs.getString("search_column");
	}


	public ProcurementGoodsTypeDTO build(ResultSet rs)
	{
		try
		{
			ProcurementGoodsTypeDTO procurementgoodstypeDTO = new ProcurementGoodsTypeDTO();
			procurementgoodstypeDTO.iD = rs.getLong("ID");
			procurementgoodstypeDTO.nameEn = rs.getString("name_en");
			procurementgoodstypeDTO.nameBn = rs.getString("name_bn");
			procurementgoodstypeDTO.procurementPackageId = rs.getLong("procurement_package_id");
			procurementgoodstypeDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			procurementgoodstypeDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			procurementgoodstypeDTO.insertionDate = rs.getLong("insertion_date");
			procurementgoodstypeDTO.isDeleted = rs.getInt("isDeleted");
			procurementgoodstypeDTO.modifiedBy = rs.getLong("modified_by");
			procurementgoodstypeDTO.lastModificationTime = rs.getLong("lastModificationTime");
			procurementgoodstypeDTO.searchColumn = rs.getString("search_column");
			return procurementgoodstypeDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}



	public long deleteProcurementGoodsTypeByProcurementPackageID(long procurementPackageID) throws Exception {
		long lastModificationTime = System.currentTimeMillis();
		StringBuilder sqlBuilder = new StringBuilder("UPDATE ")
				.append(tableName)
				.append(" SET isDeleted=1,lastModificationTime=")
				.append(lastModificationTime)
				.append(" WHERE procurement_package_id = ")
				.append(procurementPackageID);
		ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
			String sql = sqlBuilder.toString();
			Connection connection = model.getConnection();
			Statement stmt = model.getStatement();
			try {
				logger.debug(sql);
				stmt.execute(sql);
				recordUpdateTime(connection, lastModificationTime);
			} catch (SQLException ex) {
				logger.error(ex);
			}
		});
		return procurementPackageID;
	}


//	public void deleteProcurementGoodsTypeByProcurementPackageIDPrevious(long procurementPackageID) throws Exception{
//
//
//		Connection connection = null;
//		Statement stmt = null;
//		try{
//
//			String sql = "UPDATE procurement_goods_type SET isDeleted=0 WHERE procurement_package_id="+procurementPackageID;
//			logger.debug("sql " + sql);
//			connection = DBMW.getInstance().getConnection();
//			stmt = connection.createStatement();
//			stmt.execute(sql);
//		}catch(Exception ex){
//			logger.debug("fatal",ex);
//		}finally{
//			try{
//				if (stmt != null) {
//					stmt.close();
//				}
//			} catch (Exception e){}
//
//			try{
//				if (connection != null){
//					DBMW.getInstance().freeConnection(connection);
//				}
//			}catch(Exception ex2){}
//		}
//	}

	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOListByProcurementPackageID(long procurementPackageID) throws Exception{
		List<ProcurementGoodsTypeDTO> procurementgoodstypeDTOList = new ArrayList<>();

		try{

			String sql = "SELECT * FROM procurement_goods_type where isDeleted=0 and procurement_package_id="+procurementPackageID+" order by procurement_goods_type.lastModificationTime";

			return ConnectionAndStatementUtil.getListOfT(sql,this::build);

		}catch(Exception ex){
			logger.debug("fatal",ex);
		}
		return procurementgoodstypeDTOList;
	}

	//need another getter for repository
	public ProcurementGoodsTypeDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		ProcurementGoodsTypeDTO procurementGoodsTypeDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
		return procurementGoodsTypeDTO;
	}




	public List<ProcurementGoodsTypeDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	}






	//add repository
	public List<ProcurementGoodsTypeDTO> getAllProcurementGoodsType (boolean isFirstReload)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}


	public List<ProcurementGoodsTypeDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}


	public List<ProcurementGoodsTypeDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		List<Object> objectList = new ArrayList<Object>();

		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat, objectList);
		return ConnectionAndStatementUtil.getListOfT(sql,objectList,this::build);
	}

	public long deleteAllByParentId(long ID){
		StringBuilder sqlBuilder = new StringBuilder("delete from  ")
				.append(tableName)
				.append(" WHERE procurement_package_id = ")
				.append(ID);
		ConnectionAndStatementUtil.getWriteStatement(st -> {
			String sql = sqlBuilder.toString();
			try {
				logger.debug(sql);
				st.execute(sql);
				ProcurementGoodsTypeRepository.getInstance().deleteProcurementGoodsTypeDTOByprocurementPackageId(ID);
			} catch (SQLException ex) {
				logger.error(ex);
			}
		});
		return ID;
	}

//	public long deleteAllByParentIdPrevious(long ID)  {
//		Connection connection = null;
//		Statement stmt = null;
//		PreparedStatement ps = null;
//
//		try {
//			String sql = "delete from " + tableName + " WHERE procurement_package_id = " + ID;
//
//			printSql(sql);
//
//			connection = DBMW.getInstance().getConnection();
//			stmt = connection.createStatement();
//			stmt.execute(sql);
//
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		} finally {
//			try {
//				if (stmt != null) {
//					stmt.close();
//				}
//			} catch (Exception e) {
//			}
//
//			try {
//				if (connection != null) {
//					DBMW.getInstance().freeConnection(connection);
//				}
//			} catch (Exception ex2) {
//			}
//		}
//		return ID;
//	}

	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
														  UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
	{
		boolean viewAll = false;

		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}

		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;



		String AnyfieldSql = "";
		String AllFieldSql = "";

		if(p_searchCriteria != null)
		{


			Enumeration names = p_searchCriteria.keys();
			String str, value;

			AnyfieldSql = "(";

			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ?";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);

			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
				System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						str.equals("name_en")
								|| str.equals("name_bn")
								|| str.equals("insertion_date_start")
								|| str.equals("insertion_date_end")
				)

				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}

					if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("name_bn"))
					{
						AllFieldSql += "" + tableName + ".name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}


				}
			}

			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);


		}


		sql += " WHERE ";

		sql += " (" + tableName + ".isDeleted = 0 ";
		sql += ")";


		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}

		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;

		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{
			sql += " AND " + AllFieldSql;
		}



		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);

		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}

		System.out.println("-------------- sql = " + sql);

		return sql;
	}

	public void deleteWithUserAndModificationTime(long id, long employeeRecordId, long lastModificationTime){
		String sql = String.format(deleteWithUserAndModificationTime, employeeRecordId, lastModificationTime, id);
		boolean res = (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
			try {
				st.executeUpdate(sql);
				ProcurementGoodsTypeRepository.getInstance().reload(true);
				return true;
			} catch (SQLException ex) {
				logger.error(ex);
				return false;
			}
		});
	}
}
	