package procurement_package;

import java.util.*;

import util.*;

public class Procurement_packageDTO extends CommonDTO {
    public String nameEn = "";
    public String nameBn = "";
    public long insertedByUserId = -1;
    public long insertedByOrganogramId = -1;
    public long insertionDate = -1;
    public long modifiedBy = -1;

    public List<ProcurementGoodsTypeDTO> procurementGoodsTypeDTOList = new ArrayList<>();

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Procurement_packageDTO{");
        sb.append("nameEn='").append(nameEn).append('\'');
        sb.append(", nameBn='").append(nameBn).append('\'');
        sb.append(", insertedByUserId=").append(insertedByUserId);
        sb.append(", insertedByOrganogramId=").append(insertedByOrganogramId);
        sb.append(", insertionDate=").append(insertionDate);
        sb.append(", modifiedBy=").append(modifiedBy);
        sb.append(", procurementGoodsTypeDTOList=").append(procurementGoodsTypeDTOList);
        sb.append('}');
        return sb.toString();
    }
}