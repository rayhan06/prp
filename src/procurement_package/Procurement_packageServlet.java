package procurement_package;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

import common.ApiResponse;
import language.LM;
import org.apache.log4j.Logger;
import login.LoginDTO;
import permission.MenuConstants;
import procurement_goods.Procurement_goodsDAO;
import procurement_goods.Procurement_goodsDTO;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;

import java.util.*;
import javax.servlet.http.*;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import pb.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

@WebServlet("/Procurement_packageServlet")
@MultipartConfig
public class Procurement_packageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Procurement_packageServlet.class);

    String tableName = "procurement_package";

    Procurement_packageDAO procurement_packageDAO;
    CommonRequestHandler commonRequestHandler;
    ProcurementGoodsTypeDAO procurementGoodsTypeDAO;
    private final Gson gson = new Gson();

    public Procurement_packageServlet() {
        super();
        try {
            procurement_packageDAO = Procurement_packageDAO.getInstance();
            procurementGoodsTypeDAO = ProcurementGoodsTypeDAO.getInstance();
            commonRequestHandler = new CommonRequestHandler(procurement_packageDAO);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getAddPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_PACKAGE_ADD)) {
                    commonRequestHandler.getAddPage(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getEditPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_PACKAGE_UPDATE)) {
                    getProcurement_package(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getByPackageId")) {
                getByPackageId(request, response);
            } else if (actionType.equals("getByItemTypeId")) {
                try {
                    long id = Long.parseLong(request.getParameter("ID"));
                    String language = LM.getLanguageIDByUserDTO(userDTO) == CommonConstant.Language_ID_English ? "English" : "Bangla";
                    String options = CommonDAO.getOptions(language, "select", "procurement_goods", "", "", "", "", "", "", "procurement_goods_type_id = " + id);

                    PrintWriter out = response.getWriter();
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    out.println(options);
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (actionType.equals("getURL")) {
                String URL = request.getParameter("URL");
                System.out.println("URL = " + URL);
                response.sendRedirect(URL);
            } else if (actionType.equals("search")) {
                System.out.println("search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_PACKAGE_SEARCH)) {
                    String filter = request.getParameter("filter");
                    System.out.println("filter = " + filter);
                    if (filter != null) {
                        filter = ""; //shouldn't be directly used, rather manipulate it.
                        searchProcurement_package(request, response, filter);
                    } else {
                        searchProcurement_package(request, response, "");
                    }
                }
            } else if (actionType.equals("view")) {
                System.out.println("view requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_PACKAGE_SEARCH)) {
                    commonRequestHandler.view(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else {
                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("add")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_PACKAGE_ADD)) {
                    System.out.println("going to  addProcurement_package ");
                    addPackageAndGoodTypes(request, response, true, userDTO);
                } else {
                    System.out.println("Not going to  addProcurement_package ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("getDTO")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_PACKAGE_ADD)) {
                    getDTO(request, response);
                } else {
                    System.out.println("Not going to  addProcurement_package ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("edit")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_PACKAGE_UPDATE)) {
                    addPackageAndGoodTypes(request, response, false, userDTO);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("delete")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_PACKAGE_UPDATE)) {
                    List<Long> IDsRequestedToDelete = Arrays.stream(request.getParameterValues("ID"))
                            .map(Long::parseLong).collect(Collectors.toList());

                    for (long id : IDsRequestedToDelete) {
                        if (!isGoodsPackageUsed(id)) {
                            procurement_packageDAO.delete(id);
                        }
                    }
                    PBNameRepository.getInstance().reload(false);
                    response.sendRedirect("Procurement_packageServlet?actionType=search");
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("search")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_PACKAGE_SEARCH)) {
                    searchProcurement_package(request, response, "");
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getDTO(HttpServletRequest request, HttpServletResponse response) {
        try {
            System.out.println("In getDTO");
            Procurement_packageDTO procurement_packageDTO = Procurement_packageRepository.getInstance().getProcurement_packageDTOByID(Long.parseLong(request.getParameter("ID")));
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            String encoded = this.gson.toJson(procurement_packageDTO);
            System.out.println("json encoded data = " + encoded);
            out.print(encoded);
            out.flush();
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void addPackageAndGoodTypes(HttpServletRequest request, HttpServletResponse response,
                                        Boolean addFlag, UserDTO userDTO) throws Exception {
        Procurement_packageDTO procurement_packageDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

        if (addFlag) {
            procurement_packageDTO = new Procurement_packageDTO();
            procurement_packageDTO.insertedByUserId = procurement_packageDTO.modifiedBy = userDTO.ID;
            procurement_packageDTO.insertedByOrganogramId = userDTO.organogramID;
            procurement_packageDTO.insertionDate = System.currentTimeMillis();
        } else {
            procurement_packageDTO = Procurement_packageRepository.getInstance()
                    .getProcurement_packageDTOByID(Long.parseLong(request.getParameter("iD")));
            procurement_packageDTO.modifiedBy = userDTO.ID;
            procurement_packageDTO.lastModificationTime = System.currentTimeMillis();
        }

        String Value = "";
        Value = request.getParameter("nameEn");
        if (StringUtils.isValidString(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            procurement_packageDTO.nameEn = Value;
        }

        Value = request.getParameter("nameBn");
        if (StringUtils.isValidString(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            procurement_packageDTO.nameBn = Value;
        }

        if (!isPackageValid(response, procurement_packageDTO, userDTO)) return;

        if (!isGoodTypesValid(createProcurementGoodsTypeDTOListByRequest(request), response)) return;

        Utils.handleTransaction(() -> {
            long parentId = -1;
            if (addFlag) {
                parentId = procurement_packageDAO.manageWriteOperations(procurement_packageDTO, SessionConstants.INSERT, -1, userDTO);
            } else {
                parentId = procurement_packageDAO.manageWriteOperations(procurement_packageDTO, SessionConstants.UPDATE, -1, userDTO);
            }

            List<ProcurementGoodsTypeDTO> newDTOs = createProcurementGoodsTypeDTOListByRequest(request);
            List<ProcurementGoodsTypeDTO> oldDTOs = (List<ProcurementGoodsTypeDTO>) ProcurementGoodsTypeDAO.getInstance()
                    .getDTOsByParent("procurement_package_id", procurement_packageDTO.iD);

            // IDS WITH -1 ARE FOR ADD
            List<ProcurementGoodsTypeDTO> dtosToAdd = newDTOs.stream().filter(dto -> dto.iD == -1).collect(Collectors.toList());
            // COMMON DTOS ARE FOR EDIT
            List<ProcurementGoodsTypeDTO> dtosToEdit = newDTOs.stream().filter(dto -> fieldContains(oldDTOs, dto.iD)).collect(Collectors.toList());
            // IDS NOT COMMON AND NOT -1 ARE FOR DELETE
            List<ProcurementGoodsTypeDTO> dtosToDelete = oldDTOs.stream().filter(dto -> !fieldContains(dtosToEdit, dto.iD)).collect(Collectors.toList());

            for (ProcurementGoodsTypeDTO typeDTO : dtosToAdd) {
                typeDTO.procurementPackageId = parentId;
                typeDTO.insertedByUserId = typeDTO.modifiedBy = userDTO.employee_record_id;
                typeDTO.insertionDate = typeDTO.lastModificationTime = System.currentTimeMillis();
                ProcurementGoodsTypeDAO.getInstance().add(typeDTO);
            }
            for (ProcurementGoodsTypeDTO typeDTO : dtosToEdit) {
                typeDTO.modifiedBy = userDTO.employee_record_id;
                typeDTO.lastModificationTime = System.currentTimeMillis();
                ProcurementGoodsTypeDAO.getInstance().update(typeDTO);
            }
            for (ProcurementGoodsTypeDTO typeDTO : dtosToDelete) {
                if (!isGoodsTypeUsed(typeDTO.iD)) {
                    ProcurementGoodsTypeDAO.getInstance()
                            .deleteWithUserAndModificationTime(typeDTO.iD, userDTO.employee_record_id,
                                    System.currentTimeMillis());
                }

            }
        });

        reloadNecessaryRepository();

        ApiResponse.sendSuccessResponse(response, "Success");
    }

    private static void reloadNecessaryRepository() {
        PBNameRepository.getInstance().reload(false);
        Procurement_packageRepository.getInstance().reload(false);
        ProcurementGoodsTypeRepository.getInstance().reload(false);
    }

    private boolean isPackageValid(HttpServletResponse response, Procurement_packageDTO procurement_packageDTO,
                                   UserDTO userDTO) throws Exception {
        String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        if (isPackageNameEnInvalid(procurement_packageDTO)) {
            String errorMsg = UtilCharacter.getDataByLanguage(language, "প্যাকেজের ইংরেজি নাম লিখুন!",
                    "Write english package name!");
            ApiResponse.sendErrorResponse(response, errorMsg);
            return false;
        } else if (isPackageNameBnInvalid(procurement_packageDTO)) {
            String errorMsg = UtilCharacter.getDataByLanguage(language, "প্যাকেজের বাংলা নাম লিখুন!",
                    "Write bangla package name!");
            ApiResponse.sendErrorResponse(response, errorMsg);
            return false;
        } else if (isPackageNameDuplicate(procurement_packageDTO, userDTO)) {
            String errorMsg = UtilCharacter.getDataByLanguage(language, "এই নাম দিয়ে অন্য একটি প্যাকেজ বিদ্যমান!",
                    "Package with this name already exists!");
            ApiResponse.sendErrorResponse(response, errorMsg);
            return false;
        }
        return true;
    }

    private boolean isPackageNameBnInvalid(Procurement_packageDTO procurementPackageDTO) {
        return Utils.notBangla(procurementPackageDTO.nameBn);
    }

    private boolean isPackageNameEnInvalid(Procurement_packageDTO procurementPackageDTO) {
        return Utils.notEnglish(procurementPackageDTO.nameEn);
    }

    private boolean isPackageNameDuplicate(Procurement_packageDTO procurement_packageDTO, UserDTO userDTO) {
        String filter = " (name_en = '" + procurement_packageDTO.nameEn + "' OR " + " name_bn = '" + procurement_packageDTO.nameBn + "') AND ID != " + procurement_packageDTO.iD + " ";
        List<Procurement_packageDTO> existing = procurement_packageDAO.getDTOs(null, 1, 0, true, userDTO, filter, false);
        return !(existing == null || existing.isEmpty());
    }

    private boolean isGoodTypesValid(List<ProcurementGoodsTypeDTO> goodTypes, HttpServletResponse response)
            throws Exception {
        String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        if (goodTypes == null || goodTypes.isEmpty()) {
            String errorMsg = UtilCharacter.getDataByLanguage(language, "অন্তত একটা আইটেম টাইপ এন্ট্রি করুন!",
                    "Give at least one item type entry!");
            ApiResponse.sendErrorResponse(response, errorMsg);
            return false;
        } else if (isGoodTypesNameEmpty(goodTypes)) {
            String errorMsg = UtilCharacter.getDataByLanguage(language, "আইটেম টাইপের নাম লিখুন!",
                    "Write name of item type!");
            ApiResponse.sendErrorResponse(response, errorMsg);
            return false;
        } else if (isGoodTypesNameEnInValid(goodTypes)) {
            String errorMsg = UtilCharacter.getDataByLanguage(language, "আইটেম টাইপের ইংরেজি নাম লিখুন!",
                    "Write English name of item type!");
            ApiResponse.sendErrorResponse(response, errorMsg);
            return false;
        } else if (isGoodTypesNameBnInValid(goodTypes)) {
            String errorMsg = UtilCharacter.getDataByLanguage(language, "আইটেম টাইপের বাংলা নাম লিখুন!",
                    "Write Bangla name of item type!");
            ApiResponse.sendErrorResponse(response, errorMsg);
            return false;
        }
        return true;
    }

    private boolean isGoodTypesNameEmpty(List<ProcurementGoodsTypeDTO> goodTypes) {
        for (ProcurementGoodsTypeDTO goodsType : goodTypes) {
            if (goodsType.nameEn.equals("") || goodsType.nameBn.equals("")) {
                return true;
            }
        }
        return false;
    }

    private boolean isGoodTypesNameEnInValid(List<ProcurementGoodsTypeDTO> goodTypes) {
        for (ProcurementGoodsTypeDTO goodsType : goodTypes) {
            if (Utils.notEnglish(goodsType.nameEn)) {
                return true;
            }
        }
        return false;
    }

    private boolean isGoodTypesNameBnInValid(List<ProcurementGoodsTypeDTO> goodTypes) {
        for (ProcurementGoodsTypeDTO goodsType : goodTypes) {
            if (Utils.notBangla(goodsType.nameBn)) {
                return true;
            }
        }
        return false;
    }

    private boolean isGoodsTypeUsed(long typeId) {
        List<Procurement_goodsDTO> goods = (List<Procurement_goodsDTO>) Procurement_goodsDAO.getInstance()
                .getDTOsByParent("procurement_goods_type_type", typeId);
        return goods != null && !goods.isEmpty();
    }

    private boolean isGoodsPackageUsed(long packageId) {
        List<Procurement_goodsDTO> goods = (List<Procurement_goodsDTO>) Procurement_goodsDAO.getInstance()
                .getDTOsByParent("procurement_package_type", packageId);
        return goods != null && !goods.isEmpty();
    }

    private List<ProcurementGoodsTypeDTO> createProcurementGoodsTypeDTOListByRequest(HttpServletRequest request) throws Exception {
        List<ProcurementGoodsTypeDTO> procurementGoodsTypeDTOList = new ArrayList<ProcurementGoodsTypeDTO>();
        if (request.getParameterValues("procurementGoodsType.iD") != null) {
            int procurementGoodsTypeItemNo = request.getParameterValues("procurementGoodsType.iD").length;


            for (int index = 0; index < procurementGoodsTypeItemNo; index++) {
                ProcurementGoodsTypeDTO procurementGoodsTypeDTO = createProcurementGoodsTypeDTOByRequestAndIndex(request, true, index);
                procurementGoodsTypeDTOList.add(procurementGoodsTypeDTO);
            }

            return procurementGoodsTypeDTOList;
        }
        return null;
    }

    private ProcurementGoodsTypeDTO createProcurementGoodsTypeDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index) throws Exception {

        ProcurementGoodsTypeDTO procurementGoodsTypeDTO;
        if (addFlag == true) {
            procurementGoodsTypeDTO = new ProcurementGoodsTypeDTO();
        } else {
            procurementGoodsTypeDTO = ProcurementGoodsTypeRepository.getInstance().
                    getProcurementGoodsTypeDTOByID(Long.parseLong(request.getParameterValues("procurementGoodsType.iD")[index]));
        }
        String path = getServletContext().getRealPath("/img2/");
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");


        String Value = "";
        Value = request.getParameterValues("procurementGoodsType.iD")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        procurementGoodsTypeDTO.iD = Long.parseLong(Value);

        Value = "";
        Value = request.getParameterValues("procurementGoodsType.nameEn")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        procurementGoodsTypeDTO.nameEn = (Value);
        Value = request.getParameterValues("procurementGoodsType.nameBn")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        procurementGoodsTypeDTO.nameBn = (Value);
        Value = request.getParameterValues("procurementGoodsType.procurementPackageId")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        procurementGoodsTypeDTO.procurementPackageId = Long.parseLong(Value);
        Value = request.getParameterValues("procurementGoodsType.insertedByUserId")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        procurementGoodsTypeDTO.insertedByUserId = Long.parseLong(Value);
        Value = request.getParameterValues("procurementGoodsType.insertedByOrganogramId")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        procurementGoodsTypeDTO.insertedByOrganogramId = Long.parseLong(Value);
        Value = request.getParameterValues("procurementGoodsType.insertionDate")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        procurementGoodsTypeDTO.insertionDate = Long.parseLong(Value);
        Value = request.getParameterValues("procurementGoodsType.searchColumn")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        procurementGoodsTypeDTO.searchColumn = (Value);
        return procurementGoodsTypeDTO;

    }

    private void getProcurement_package(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {
        System.out.println("in getProcurement_package");
        Procurement_packageDTO procurement_packageDTO = null;
        try {
            procurement_packageDTO = Procurement_packageRepository.getInstance().getProcurement_packageDTOByID(id);
            request.setAttribute("ID", procurement_packageDTO.iD);
            request.setAttribute("procurement_packageDTO", procurement_packageDTO);
            request.setAttribute("procurement_packageDAO", procurement_packageDAO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "procurement_package/procurement_packageInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "procurement_package/procurement_packageSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "procurement_package/procurement_packageEditBody.jsp?actionType=edit";
                } else {
                    URL = "procurement_package/procurement_packageEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getProcurement_package(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getProcurement_package(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void getByPackageId(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {
        System.out.println("in getProcurement_package");
        Procurement_packageDTO procurement_packageDTO = null;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
            UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
            String language = LM.getLanguageIDByUserDTO(userDTO) == CommonConstant.Language_ID_English ? "English" : "Bangla";
            Procurement_packageDAO procurement_packageDAO = Procurement_packageDAO.getInstance();
            String options = CommonDAO.getOptionsWithWhere(language, "procurement_goods_type", -1, "procurement_package_id = " + id);

            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            out.println(options);
            out.close();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getByPackageId(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getByPackageId(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void searchProcurement_package(HttpServletRequest request, HttpServletResponse response, String filter)
            throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_PROCUREMENT_PACKAGE,
                request,
                procurement_packageDAO,
                SessionConstants.VIEW_PROCUREMENT_PACKAGE,
                SessionConstants.SEARCH_PROCUREMENT_PACKAGE,
                tableName,
                true,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("procurement_packageDAO", procurement_packageDAO);
        RequestDispatcher rd;
        if (!hasAjax) {
            System.out.println("Going to procurement_package/procurement_packageSearch.jsp");
            rd = request.getRequestDispatcher("procurement_package/procurement_packageSearch.jsp");
        } else {
            System.out.println("Going to procurement_package/procurement_packageSearchForm.jsp");
            rd = request.getRequestDispatcher("procurement_package/procurement_packageSearchForm.jsp");
        }
        rd.forward(request, response);
    }

    private boolean fieldContains(List<ProcurementGoodsTypeDTO> childDTOs, Long id) {
        List<ProcurementGoodsTypeDTO> dto = childDTOs.stream().filter(lotDTO -> lotDTO.iD == id).collect(Collectors.toList());
        return dto.size() > 0;
    }
}

