package procurement_package;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import pb.CatDTO;
import procurement_goods.Procurement_goodsDTO;
import repository.Repository;
import repository.RepositoryManager;
import util.StringUtils;
import util.UtilCharacter;
import vm_route_travel.Vm_route_travelDAO;


public class Procurement_packageRepository implements Repository {
	Procurement_packageDAO procurement_packageDAO = null;
	Gson gson;

	public void setDAO(Procurement_packageDAO procurement_packageDAO)
	{
		this.procurement_packageDAO = procurement_packageDAO;
		gson = new Gson();
	}
	
	
	static Logger logger = Logger.getLogger(Procurement_packageRepository.class);
	Map<Long, Procurement_packageDTO>mapOfProcurement_packageDTOToiD;
//	Map<String, Set<Procurement_packageDTO> >mapOfProcurement_packageDTOTonameEn;
//	Map<String, Set<Procurement_packageDTO> >mapOfProcurement_packageDTOTonameBn;
//	Map<Long, Set<Procurement_packageDTO> >mapOfProcurement_packageDTOToinsertedByUserId;
//	Map<Long, Set<Procurement_packageDTO> >mapOfProcurement_packageDTOToinsertedByOrganogramId;
//	Map<Long, Set<Procurement_packageDTO> >mapOfProcurement_packageDTOToinsertionDate;
//	Map<Long, Set<Procurement_packageDTO> >mapOfProcurement_packageDTOTolastModificationTime;
//	Map<String, Set<Procurement_packageDTO> >mapOfProcurement_packageDTOTosearchColumn;


	static Procurement_packageRepository instance = null;  
	private Procurement_packageRepository(){
		mapOfProcurement_packageDTOToiD = new ConcurrentHashMap<>();
//		mapOfProcurement_packageDTOTonameEn = new ConcurrentHashMap<>();
//		mapOfProcurement_packageDTOTonameBn = new ConcurrentHashMap<>();
//		mapOfProcurement_packageDTOToinsertedByUserId = new ConcurrentHashMap<>();
//		mapOfProcurement_packageDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
//		mapOfProcurement_packageDTOToinsertionDate = new ConcurrentHashMap<>();
//		mapOfProcurement_packageDTOTolastModificationTime = new ConcurrentHashMap<>();
//		mapOfProcurement_packageDTOTosearchColumn = new ConcurrentHashMap<>();

		setDAO(Procurement_packageDAO.getInstance());

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Procurement_packageRepository getInstance(){
		if (instance == null){
			instance = new Procurement_packageRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(procurement_packageDAO == null)
		{
			return;
		}
		try {
			List<Procurement_packageDTO> procurement_packageDTOs = procurement_packageDAO.getAllProcurement_package(reloadAll);
			for(Procurement_packageDTO procurement_packageDTO : procurement_packageDTOs) {
				Procurement_packageDTO oldProcurement_packageDTO = getProcurement_packageDTOByIDWithoutClone(procurement_packageDTO.iD);
				if( oldProcurement_packageDTO != null ) {
					mapOfProcurement_packageDTOToiD.remove(oldProcurement_packageDTO.iD);
				
//					if(mapOfProcurement_packageDTOTonameEn.containsKey(oldProcurement_packageDTO.nameEn)) {
//						mapOfProcurement_packageDTOTonameEn.get(oldProcurement_packageDTO.nameEn).remove(oldProcurement_packageDTO);
//					}
//					if(mapOfProcurement_packageDTOTonameEn.get(oldProcurement_packageDTO.nameEn).isEmpty()) {
//						mapOfProcurement_packageDTOTonameEn.remove(oldProcurement_packageDTO.nameEn);
//					}
//
//					if(mapOfProcurement_packageDTOTonameBn.containsKey(oldProcurement_packageDTO.nameBn)) {
//						mapOfProcurement_packageDTOTonameBn.get(oldProcurement_packageDTO.nameBn).remove(oldProcurement_packageDTO);
//					}
//					if(mapOfProcurement_packageDTOTonameBn.get(oldProcurement_packageDTO.nameBn).isEmpty()) {
//						mapOfProcurement_packageDTOTonameBn.remove(oldProcurement_packageDTO.nameBn);
//					}
//
//					if(mapOfProcurement_packageDTOToinsertedByUserId.containsKey(oldProcurement_packageDTO.insertedByUserId)) {
//						mapOfProcurement_packageDTOToinsertedByUserId.get(oldProcurement_packageDTO.insertedByUserId).remove(oldProcurement_packageDTO);
//					}
//					if(mapOfProcurement_packageDTOToinsertedByUserId.get(oldProcurement_packageDTO.insertedByUserId).isEmpty()) {
//						mapOfProcurement_packageDTOToinsertedByUserId.remove(oldProcurement_packageDTO.insertedByUserId);
//					}
//
//					if(mapOfProcurement_packageDTOToinsertedByOrganogramId.containsKey(oldProcurement_packageDTO.insertedByOrganogramId)) {
//						mapOfProcurement_packageDTOToinsertedByOrganogramId.get(oldProcurement_packageDTO.insertedByOrganogramId).remove(oldProcurement_packageDTO);
//					}
//					if(mapOfProcurement_packageDTOToinsertedByOrganogramId.get(oldProcurement_packageDTO.insertedByOrganogramId).isEmpty()) {
//						mapOfProcurement_packageDTOToinsertedByOrganogramId.remove(oldProcurement_packageDTO.insertedByOrganogramId);
//					}
//
//					if(mapOfProcurement_packageDTOToinsertionDate.containsKey(oldProcurement_packageDTO.insertionDate)) {
//						mapOfProcurement_packageDTOToinsertionDate.get(oldProcurement_packageDTO.insertionDate).remove(oldProcurement_packageDTO);
//					}
//					if(mapOfProcurement_packageDTOToinsertionDate.get(oldProcurement_packageDTO.insertionDate).isEmpty()) {
//						mapOfProcurement_packageDTOToinsertionDate.remove(oldProcurement_packageDTO.insertionDate);
//					}
//
//					if(mapOfProcurement_packageDTOTolastModificationTime.containsKey(oldProcurement_packageDTO.lastModificationTime)) {
//						mapOfProcurement_packageDTOTolastModificationTime.get(oldProcurement_packageDTO.lastModificationTime).remove(oldProcurement_packageDTO);
//					}
//					if(mapOfProcurement_packageDTOTolastModificationTime.get(oldProcurement_packageDTO.lastModificationTime).isEmpty()) {
//						mapOfProcurement_packageDTOTolastModificationTime.remove(oldProcurement_packageDTO.lastModificationTime);
//					}
//
//					if(mapOfProcurement_packageDTOTosearchColumn.containsKey(oldProcurement_packageDTO.searchColumn)) {
//						mapOfProcurement_packageDTOTosearchColumn.get(oldProcurement_packageDTO.searchColumn).remove(oldProcurement_packageDTO);
//					}
//					if(mapOfProcurement_packageDTOTosearchColumn.get(oldProcurement_packageDTO.searchColumn).isEmpty()) {
//						mapOfProcurement_packageDTOTosearchColumn.remove(oldProcurement_packageDTO.searchColumn);
//					}
					
					
				}
				if(procurement_packageDTO.isDeleted == 0) 
				{
					
					mapOfProcurement_packageDTOToiD.put(procurement_packageDTO.iD, procurement_packageDTO);
				
//					if( ! mapOfProcurement_packageDTOTonameEn.containsKey(procurement_packageDTO.nameEn)) {
//						mapOfProcurement_packageDTOTonameEn.put(procurement_packageDTO.nameEn, new HashSet<>());
//					}
//					mapOfProcurement_packageDTOTonameEn.get(procurement_packageDTO.nameEn).add(procurement_packageDTO);
//
//					if( ! mapOfProcurement_packageDTOTonameBn.containsKey(procurement_packageDTO.nameBn)) {
//						mapOfProcurement_packageDTOTonameBn.put(procurement_packageDTO.nameBn, new HashSet<>());
//					}
//					mapOfProcurement_packageDTOTonameBn.get(procurement_packageDTO.nameBn).add(procurement_packageDTO);
//
//					if( ! mapOfProcurement_packageDTOToinsertedByUserId.containsKey(procurement_packageDTO.insertedByUserId)) {
//						mapOfProcurement_packageDTOToinsertedByUserId.put(procurement_packageDTO.insertedByUserId, new HashSet<>());
//					}
//					mapOfProcurement_packageDTOToinsertedByUserId.get(procurement_packageDTO.insertedByUserId).add(procurement_packageDTO);
//
//					if( ! mapOfProcurement_packageDTOToinsertedByOrganogramId.containsKey(procurement_packageDTO.insertedByOrganogramId)) {
//						mapOfProcurement_packageDTOToinsertedByOrganogramId.put(procurement_packageDTO.insertedByOrganogramId, new HashSet<>());
//					}
//					mapOfProcurement_packageDTOToinsertedByOrganogramId.get(procurement_packageDTO.insertedByOrganogramId).add(procurement_packageDTO);
//
//					if( ! mapOfProcurement_packageDTOToinsertionDate.containsKey(procurement_packageDTO.insertionDate)) {
//						mapOfProcurement_packageDTOToinsertionDate.put(procurement_packageDTO.insertionDate, new HashSet<>());
//					}
//					mapOfProcurement_packageDTOToinsertionDate.get(procurement_packageDTO.insertionDate).add(procurement_packageDTO);
//
//					if( ! mapOfProcurement_packageDTOTolastModificationTime.containsKey(procurement_packageDTO.lastModificationTime)) {
//						mapOfProcurement_packageDTOTolastModificationTime.put(procurement_packageDTO.lastModificationTime, new HashSet<>());
//					}
//					mapOfProcurement_packageDTOTolastModificationTime.get(procurement_packageDTO.lastModificationTime).add(procurement_packageDTO);
//
//					if( ! mapOfProcurement_packageDTOTosearchColumn.containsKey(procurement_packageDTO.searchColumn)) {
//						mapOfProcurement_packageDTOTosearchColumn.put(procurement_packageDTO.searchColumn, new HashSet<>());
//					}
//					mapOfProcurement_packageDTOTosearchColumn.get(procurement_packageDTO.searchColumn).add(procurement_packageDTO);
//
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public Procurement_packageDTO clone(Procurement_packageDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Procurement_packageDTO.class);
	}

	public List<Procurement_packageDTO> clone(List<Procurement_packageDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public Procurement_packageDTO getProcurement_packageDTOByIDWithoutClone(long ID){
		return mapOfProcurement_packageDTOToiD.get(ID);
	}

	public List<Procurement_packageDTO> getProcurement_packageList() {
		List <Procurement_packageDTO> procurement_packages = new ArrayList<Procurement_packageDTO>(this.mapOfProcurement_packageDTOToiD.values());
		return procurement_packages;
	}
	
	
	public Procurement_packageDTO getProcurement_packageDTOByID( long ID){
		boolean isNull = mapOfProcurement_packageDTOToiD.get(ID) == null;
		if(isNull) return null;
		Procurement_packageDTO procurement_packageDTO = clone(mapOfProcurement_packageDTOToiD.get(ID));
		procurement_packageDTO.procurementGoodsTypeDTOList = ProcurementGoodsTypeRepository.getInstance().getProcurementGoodsTypeDTOByprocurementPackageId(procurement_packageDTO.iD);
		return procurement_packageDTO;
	}

	public List<Procurement_packageDTO> getDTOsLikeName(String name) {
		if (!StringUtils.isValidString(name)) return getProcurement_packageList();
		return getProcurement_packageList()
				.stream()
				.filter(dto ->
						dto.nameEn.contains(name) || dto.nameBn.contains(name))
				.collect(Collectors.toList());
	}

	public List<Procurement_packageDTO> getDTOsByIds(List<Long> ids){
		List<Procurement_packageDTO> dtos = new ArrayList<>();
		ids.forEach(i -> {
			Procurement_packageDTO dto = getProcurement_packageDTOByID(i);
			if(dto != null){
				dtos.add(dto);
			}
		});
		return dtos;
	}

	public String getOptions (String language, long defaultValue)
	{

		String sOptions = "";

		if (defaultValue == CatDTO.CATDEFAULTNOTREQUIRED)
		{
			if (language.equalsIgnoreCase("English"))
			{
				sOptions += "<option value = '" + CatDTO.CATDEFAULTNOTREQUIRED + "'>Select</option>";
			}
			else
			{
				sOptions += "<option value = '" + CatDTO.CATDEFAULTNOTREQUIRED + "'>বাছাই করুন</option>";
			}
		}
		else if (defaultValue == CatDTO.CATDEFAULT)
		{
			if (language.equalsIgnoreCase("English"))
			{
				sOptions += "<option value = ''>Select</option>";
			}
			else
			{
				sOptions += "<option value = ''>বাছাই করুন</option>";
			}
		}

			List<Procurement_packageDTO> dtos = clone(getProcurement_packageList());

			for (Procurement_packageDTO dto : dtos) {
				String sOption =  "<option value = '" + dto.iD + "'";
				if(defaultValue != CatDTO.CATDEFAULTNOTREQUIRED && dto.iD == defaultValue)
				{
					sOption += " selected ";
				}
				String name = language.equals("English") ? dto.nameEn : dto.nameBn;
				sOption += ">"
						+ name
						+ "</option>";
				sOptions += sOption;
			}

        return sOptions;
	}

	public String getName (String language, long defaultValue)
	{
		Procurement_packageDTO dto = getProcurement_packageDTOByID(defaultValue);
		return dto == null ? "" : UtilCharacter.getDataByLanguage(language, dto.nameBn, dto.nameEn);
	}
	
//	public List<Procurement_packageDTO> getProcurement_packageDTOByname_en(String name_en) {
//		return new ArrayList<>( mapOfProcurement_packageDTOTonameEn.getOrDefault(name_en,new HashSet<>()));
//	}
//
//
//	public List<Procurement_packageDTO> getProcurement_packageDTOByname_bn(String name_bn) {
//		return new ArrayList<>( mapOfProcurement_packageDTOTonameBn.getOrDefault(name_bn,new HashSet<>()));
//	}
//
//
//	public List<Procurement_packageDTO> getProcurement_packageDTOByinserted_by_user_id(long inserted_by_user_id) {
//		return new ArrayList<>( mapOfProcurement_packageDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
//	}
//
//
//	public List<Procurement_packageDTO> getProcurement_packageDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
//		return new ArrayList<>( mapOfProcurement_packageDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
//	}
//
//
//	public List<Procurement_packageDTO> getProcurement_packageDTOByinsertion_date(long insertion_date) {
//		return new ArrayList<>( mapOfProcurement_packageDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
//	}
//
//
//	public List<Procurement_packageDTO> getProcurement_packageDTOBylastModificationTime(long lastModificationTime) {
//		return new ArrayList<>( mapOfProcurement_packageDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
//	}
//
//
//	public List<Procurement_packageDTO> getProcurement_packageDTOBysearch_column(String search_column) {
//		return new ArrayList<>( mapOfProcurement_packageDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
//	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "procurement_package";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}

	public String getText(long id,String language){
		Procurement_packageDTO dto = getProcurement_packageDTOByID(id);
		return dto==null?"": ("Bangla".equalsIgnoreCase(language)?dto.nameBn: dto.nameEn);
	}
}


