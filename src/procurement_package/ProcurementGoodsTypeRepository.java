package procurement_package;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import pb.CatDTO;
import pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsDTO;
import repository.Repository;
import repository.RepositoryManager;
import util.StringUtils;
import util.UtilCharacter;
import vm_route_travel.Vm_route_travelDAO;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class ProcurementGoodsTypeRepository implements Repository {
	ProcurementGoodsTypeDAO procurement_goods_typeDAO = null;
	Gson gson;

	public void setDAO(ProcurementGoodsTypeDAO procurement_goods_typeDAO)
	{
		this.procurement_goods_typeDAO = procurement_goods_typeDAO;
		gson = new Gson();
	}
	
	
	static Logger logger = Logger.getLogger(ProcurementGoodsTypeRepository.class);
	Map<Long, ProcurementGoodsTypeDTO>mapOfProcurementGoodsTypeDTOToiD;
//	Map<String, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOTonameEn;
//	Map<String, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOTonameBn;
	Map<Long, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOToprocurementPackageId;
//	Map<Double, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOTounitPrice;
//	Map<Double, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOToestCost;
//	Map<Integer, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOTomethodAndTypeCat;
//	Map<String, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOToapprovingAuthority;
//	Map<Integer, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOTosourceOfFundCat;
//	Map<String, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOTotimeCode;
//	Map<String, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOTotender;
//	Map<String, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOTotenderOpening;
//	Map<String, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOTotenderEvaluation;
//	Map<String, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOToapprovalToward;
//	Map<String, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOToawardNotification;
//	Map<String, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOTosiginingOfContract;
//	Map<Long, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOTocontractSignatureTime;
//	Map<Long, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOTocontractCompletionTime;
//	Map<Long, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOToinsertedByUserId;
//	Map<Long, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOToinsertedByOrganogramId;
//	Map<Long, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOToinsertionDate;
//	Map<Long, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOTolastModificationTime;
//	Map<String, Set<ProcurementGoodsTypeDTO> >mapOfProcurementGoodsTypeDTOTosearchColumn;


	static ProcurementGoodsTypeRepository instance = null;  
	private ProcurementGoodsTypeRepository(){
		mapOfProcurementGoodsTypeDTOToiD = new ConcurrentHashMap<>();
// 		mapOfProcurementGoodsTypeDTOTonameEn = new ConcurrentHashMap<>();
//		mapOfProcurementGoodsTypeDTOTonameBn = new ConcurrentHashMap<>();
		mapOfProcurementGoodsTypeDTOToprocurementPackageId = new ConcurrentHashMap<>();
//		mapOfProcurementGoodsTypeDTOTounitPrice = new ConcurrentHashMap<>();
//		mapOfProcurementGoodsTypeDTOToestCost = new ConcurrentHashMap<>();
//		mapOfProcurementGoodsTypeDTOTomethodAndTypeCat = new ConcurrentHashMap<>();
//		mapOfProcurementGoodsTypeDTOToapprovingAuthority = new ConcurrentHashMap<>();
//		mapOfProcurementGoodsTypeDTOTosourceOfFundCat = new ConcurrentHashMap<>();
//		mapOfProcurementGoodsTypeDTOTotimeCode = new ConcurrentHashMap<>();
//		mapOfProcurementGoodsTypeDTOTotender = new ConcurrentHashMap<>();
//		mapOfProcurementGoodsTypeDTOTotenderOpening = new ConcurrentHashMap<>();
//		mapOfProcurementGoodsTypeDTOTotenderEvaluation = new ConcurrentHashMap<>();
//		mapOfProcurementGoodsTypeDTOToapprovalToward = new ConcurrentHashMap<>();
//		mapOfProcurementGoodsTypeDTOToawardNotification = new ConcurrentHashMap<>();
//		mapOfProcurementGoodsTypeDTOTosiginingOfContract = new ConcurrentHashMap<>();
//		mapOfProcurementGoodsTypeDTOTocontractSignatureTime = new ConcurrentHashMap<>();
//		mapOfProcurementGoodsTypeDTOTocontractCompletionTime = new ConcurrentHashMap<>();
//		mapOfProcurementGoodsTypeDTOToinsertedByUserId = new ConcurrentHashMap<>();
//		mapOfProcurementGoodsTypeDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
//		mapOfProcurementGoodsTypeDTOToinsertionDate = new ConcurrentHashMap<>();
//		mapOfProcurementGoodsTypeDTOTolastModificationTime = new ConcurrentHashMap<>();
//		mapOfProcurementGoodsTypeDTOTosearchColumn = new ConcurrentHashMap<>();

		setDAO(ProcurementGoodsTypeDAO.getInstance());

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static ProcurementGoodsTypeRepository getInstance(){
		if (instance == null){
			instance = new ProcurementGoodsTypeRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(procurement_goods_typeDAO == null)
		{
			return;
		}
		try {
			List<ProcurementGoodsTypeDTO> procurement_goods_typeDTOs = procurement_goods_typeDAO.getAllProcurementGoodsType(reloadAll);
			for(ProcurementGoodsTypeDTO procurement_goods_typeDTO : procurement_goods_typeDTOs) {
				ProcurementGoodsTypeDTO oldProcurementGoodsTypeDTO = getProcurementGoodsTypeDTOByIDWithoutClone(procurement_goods_typeDTO.iD);
				if( oldProcurementGoodsTypeDTO != null ) {
					mapOfProcurementGoodsTypeDTOToiD.remove(oldProcurementGoodsTypeDTO.iD);
					
// 					if(mapOfProcurementGoodsTypeDTOTonameEn.containsKey(oldProcurementGoodsTypeDTO.nameEn)) {
//						mapOfProcurementGoodsTypeDTOTonameEn.get(oldProcurementGoodsTypeDTO.nameEn).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOTonameEn.get(oldProcurementGoodsTypeDTO.nameEn).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOTonameEn.remove(oldProcurementGoodsTypeDTO.nameEn);
//					}
//
//					if(mapOfProcurementGoodsTypeDTOTonameBn.containsKey(oldProcurementGoodsTypeDTO.nameBn)) {
//						mapOfProcurementGoodsTypeDTOTonameBn.get(oldProcurementGoodsTypeDTO.nameBn).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOTonameBn.get(oldProcurementGoodsTypeDTO.nameBn).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOTonameBn.remove(oldProcurementGoodsTypeDTO.nameBn);
//					}
//
					if(mapOfProcurementGoodsTypeDTOToprocurementPackageId.containsKey(oldProcurementGoodsTypeDTO.procurementPackageId)) {
						mapOfProcurementGoodsTypeDTOToprocurementPackageId.get(oldProcurementGoodsTypeDTO.procurementPackageId).remove(oldProcurementGoodsTypeDTO);
					}
					if(mapOfProcurementGoodsTypeDTOToprocurementPackageId.get(oldProcurementGoodsTypeDTO.procurementPackageId).isEmpty()) {
						mapOfProcurementGoodsTypeDTOToprocurementPackageId.remove(oldProcurementGoodsTypeDTO.procurementPackageId);
					}
//
//					if(mapOfProcurementGoodsTypeDTOTounitPrice.containsKey(oldProcurementGoodsTypeDTO.unitPrice)) {
//						mapOfProcurementGoodsTypeDTOTounitPrice.get(oldProcurementGoodsTypeDTO.unitPrice).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOTounitPrice.get(oldProcurementGoodsTypeDTO.unitPrice).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOTounitPrice.remove(oldProcurementGoodsTypeDTO.unitPrice);
//					}
//
//					if(mapOfProcurementGoodsTypeDTOToestCost.containsKey(oldProcurementGoodsTypeDTO.estCost)) {
//						mapOfProcurementGoodsTypeDTOToestCost.get(oldProcurementGoodsTypeDTO.estCost).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOToestCost.get(oldProcurementGoodsTypeDTO.estCost).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOToestCost.remove(oldProcurementGoodsTypeDTO.estCost);
//					}
//
//					if(mapOfProcurementGoodsTypeDTOTomethodAndTypeCat.containsKey(oldProcurementGoodsTypeDTO.methodAndTypeCat)) {
//						mapOfProcurementGoodsTypeDTOTomethodAndTypeCat.get(oldProcurementGoodsTypeDTO.methodAndTypeCat).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOTomethodAndTypeCat.get(oldProcurementGoodsTypeDTO.methodAndTypeCat).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOTomethodAndTypeCat.remove(oldProcurementGoodsTypeDTO.methodAndTypeCat);
//					}
//
//					if(mapOfProcurementGoodsTypeDTOToapprovingAuthority.containsKey(oldProcurementGoodsTypeDTO.approvingAuthority)) {
//						mapOfProcurementGoodsTypeDTOToapprovingAuthority.get(oldProcurementGoodsTypeDTO.approvingAuthority).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOToapprovingAuthority.get(oldProcurementGoodsTypeDTO.approvingAuthority).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOToapprovingAuthority.remove(oldProcurementGoodsTypeDTO.approvingAuthority);
//					}
//
//					if(mapOfProcurementGoodsTypeDTOTosourceOfFundCat.containsKey(oldProcurementGoodsTypeDTO.sourceOfFundCat)) {
//						mapOfProcurementGoodsTypeDTOTosourceOfFundCat.get(oldProcurementGoodsTypeDTO.sourceOfFundCat).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOTosourceOfFundCat.get(oldProcurementGoodsTypeDTO.sourceOfFundCat).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOTosourceOfFundCat.remove(oldProcurementGoodsTypeDTO.sourceOfFundCat);
//					}
//
//					if(mapOfProcurementGoodsTypeDTOTotimeCode.containsKey(oldProcurementGoodsTypeDTO.timeCode)) {
//						mapOfProcurementGoodsTypeDTOTotimeCode.get(oldProcurementGoodsTypeDTO.timeCode).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOTotimeCode.get(oldProcurementGoodsTypeDTO.timeCode).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOTotimeCode.remove(oldProcurementGoodsTypeDTO.timeCode);
//					}
//
//					if(mapOfProcurementGoodsTypeDTOTotender.containsKey(oldProcurementGoodsTypeDTO.tender)) {
//						mapOfProcurementGoodsTypeDTOTotender.get(oldProcurementGoodsTypeDTO.tender).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOTotender.get(oldProcurementGoodsTypeDTO.tender).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOTotender.remove(oldProcurementGoodsTypeDTO.tender);
//					}
//
//					if(mapOfProcurementGoodsTypeDTOTotenderOpening.containsKey(oldProcurementGoodsTypeDTO.tenderOpening)) {
//						mapOfProcurementGoodsTypeDTOTotenderOpening.get(oldProcurementGoodsTypeDTO.tenderOpening).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOTotenderOpening.get(oldProcurementGoodsTypeDTO.tenderOpening).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOTotenderOpening.remove(oldProcurementGoodsTypeDTO.tenderOpening);
//					}
//
//					if(mapOfProcurementGoodsTypeDTOTotenderEvaluation.containsKey(oldProcurementGoodsTypeDTO.tenderEvaluation)) {
//						mapOfProcurementGoodsTypeDTOTotenderEvaluation.get(oldProcurementGoodsTypeDTO.tenderEvaluation).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOTotenderEvaluation.get(oldProcurementGoodsTypeDTO.tenderEvaluation).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOTotenderEvaluation.remove(oldProcurementGoodsTypeDTO.tenderEvaluation);
//					}
//
//					if(mapOfProcurementGoodsTypeDTOToapprovalToward.containsKey(oldProcurementGoodsTypeDTO.approvalToward)) {
//						mapOfProcurementGoodsTypeDTOToapprovalToward.get(oldProcurementGoodsTypeDTO.approvalToward).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOToapprovalToward.get(oldProcurementGoodsTypeDTO.approvalToward).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOToapprovalToward.remove(oldProcurementGoodsTypeDTO.approvalToward);
//					}
//
//					if(mapOfProcurementGoodsTypeDTOToawardNotification.containsKey(oldProcurementGoodsTypeDTO.awardNotification)) {
//						mapOfProcurementGoodsTypeDTOToawardNotification.get(oldProcurementGoodsTypeDTO.awardNotification).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOToawardNotification.get(oldProcurementGoodsTypeDTO.awardNotification).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOToawardNotification.remove(oldProcurementGoodsTypeDTO.awardNotification);
//					}
//
//					if(mapOfProcurementGoodsTypeDTOTosiginingOfContract.containsKey(oldProcurementGoodsTypeDTO.siginingOfContract)) {
//						mapOfProcurementGoodsTypeDTOTosiginingOfContract.get(oldProcurementGoodsTypeDTO.siginingOfContract).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOTosiginingOfContract.get(oldProcurementGoodsTypeDTO.siginingOfContract).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOTosiginingOfContract.remove(oldProcurementGoodsTypeDTO.siginingOfContract);
//					}
//
//					if(mapOfProcurementGoodsTypeDTOTocontractSignatureTime.containsKey(oldProcurementGoodsTypeDTO.contractSignatureTime)) {
//						mapOfProcurementGoodsTypeDTOTocontractSignatureTime.get(oldProcurementGoodsTypeDTO.contractSignatureTime).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOTocontractSignatureTime.get(oldProcurementGoodsTypeDTO.contractSignatureTime).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOTocontractSignatureTime.remove(oldProcurementGoodsTypeDTO.contractSignatureTime);
//					}
//
//					if(mapOfProcurementGoodsTypeDTOTocontractCompletionTime.containsKey(oldProcurementGoodsTypeDTO.contractCompletionTime)) {
//						mapOfProcurementGoodsTypeDTOTocontractCompletionTime.get(oldProcurementGoodsTypeDTO.contractCompletionTime).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOTocontractCompletionTime.get(oldProcurementGoodsTypeDTO.contractCompletionTime).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOTocontractCompletionTime.remove(oldProcurementGoodsTypeDTO.contractCompletionTime);
//					}
//
//					if(mapOfProcurementGoodsTypeDTOToinsertedByUserId.containsKey(oldProcurementGoodsTypeDTO.insertedByUserId)) {
//						mapOfProcurementGoodsTypeDTOToinsertedByUserId.get(oldProcurementGoodsTypeDTO.insertedByUserId).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOToinsertedByUserId.get(oldProcurementGoodsTypeDTO.insertedByUserId).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOToinsertedByUserId.remove(oldProcurementGoodsTypeDTO.insertedByUserId);
//					}
//
//					if(mapOfProcurementGoodsTypeDTOToinsertedByOrganogramId.containsKey(oldProcurementGoodsTypeDTO.insertedByOrganogramId)) {
//						mapOfProcurementGoodsTypeDTOToinsertedByOrganogramId.get(oldProcurementGoodsTypeDTO.insertedByOrganogramId).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOToinsertedByOrganogramId.get(oldProcurementGoodsTypeDTO.insertedByOrganogramId).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOToinsertedByOrganogramId.remove(oldProcurementGoodsTypeDTO.insertedByOrganogramId);
//					}
//
//					if(mapOfProcurementGoodsTypeDTOToinsertionDate.containsKey(oldProcurementGoodsTypeDTO.insertionDate)) {
//						mapOfProcurementGoodsTypeDTOToinsertionDate.get(oldProcurementGoodsTypeDTO.insertionDate).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOToinsertionDate.get(oldProcurementGoodsTypeDTO.insertionDate).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOToinsertionDate.remove(oldProcurementGoodsTypeDTO.insertionDate);
//					}
//
//					if(mapOfProcurementGoodsTypeDTOTolastModificationTime.containsKey(oldProcurementGoodsTypeDTO.lastModificationTime)) {
//						mapOfProcurementGoodsTypeDTOTolastModificationTime.get(oldProcurementGoodsTypeDTO.lastModificationTime).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOTolastModificationTime.get(oldProcurementGoodsTypeDTO.lastModificationTime).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOTolastModificationTime.remove(oldProcurementGoodsTypeDTO.lastModificationTime);
//					}
//
//					if(mapOfProcurementGoodsTypeDTOTosearchColumn.containsKey(oldProcurementGoodsTypeDTO.searchColumn)) {
//						mapOfProcurementGoodsTypeDTOTosearchColumn.get(oldProcurementGoodsTypeDTO.searchColumn).remove(oldProcurementGoodsTypeDTO);
//					}
//					if(mapOfProcurementGoodsTypeDTOTosearchColumn.get(oldProcurementGoodsTypeDTO.searchColumn).isEmpty()) {
//						mapOfProcurementGoodsTypeDTOTosearchColumn.remove(oldProcurementGoodsTypeDTO.searchColumn);
//					}
					
					
				}
				if(procurement_goods_typeDTO.isDeleted == 0) 
				{
					
					mapOfProcurementGoodsTypeDTOToiD.put(procurement_goods_typeDTO.iD, procurement_goods_typeDTO);

					// 					if( ! mapOfProcurementGoodsTypeDTOTonameEn.containsKey(procurement_goods_typeDTO.nameEn)) {
//						mapOfProcurementGoodsTypeDTOTonameEn.put(procurement_goods_typeDTO.nameEn, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOTonameEn.get(procurement_goods_typeDTO.nameEn).add(procurement_goods_typeDTO);
//
//					if( ! mapOfProcurementGoodsTypeDTOTonameBn.containsKey(procurement_goods_typeDTO.nameBn)) {
//						mapOfProcurementGoodsTypeDTOTonameBn.put(procurement_goods_typeDTO.nameBn, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOTonameBn.get(procurement_goods_typeDTO.nameBn).add(procurement_goods_typeDTO);
//
					if( ! mapOfProcurementGoodsTypeDTOToprocurementPackageId.containsKey(procurement_goods_typeDTO.procurementPackageId)) {
						mapOfProcurementGoodsTypeDTOToprocurementPackageId.put(procurement_goods_typeDTO.procurementPackageId, new HashSet<>());
					}
					mapOfProcurementGoodsTypeDTOToprocurementPackageId.get(procurement_goods_typeDTO.procurementPackageId).add(procurement_goods_typeDTO);
//
//					if( ! mapOfProcurementGoodsTypeDTOTounitPrice.containsKey(procurement_goods_typeDTO.unitPrice)) {
//						mapOfProcurementGoodsTypeDTOTounitPrice.put(procurement_goods_typeDTO.unitPrice, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOTounitPrice.get(procurement_goods_typeDTO.unitPrice).add(procurement_goods_typeDTO);
//
//					if( ! mapOfProcurementGoodsTypeDTOToestCost.containsKey(procurement_goods_typeDTO.estCost)) {
//						mapOfProcurementGoodsTypeDTOToestCost.put(procurement_goods_typeDTO.estCost, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOToestCost.get(procurement_goods_typeDTO.estCost).add(procurement_goods_typeDTO);
//
//					if( ! mapOfProcurementGoodsTypeDTOTomethodAndTypeCat.containsKey(procurement_goods_typeDTO.methodAndTypeCat)) {
//						mapOfProcurementGoodsTypeDTOTomethodAndTypeCat.put(procurement_goods_typeDTO.methodAndTypeCat, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOTomethodAndTypeCat.get(procurement_goods_typeDTO.methodAndTypeCat).add(procurement_goods_typeDTO);
//
//					if( ! mapOfProcurementGoodsTypeDTOToapprovingAuthority.containsKey(procurement_goods_typeDTO.approvingAuthority)) {
//						mapOfProcurementGoodsTypeDTOToapprovingAuthority.put(procurement_goods_typeDTO.approvingAuthority, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOToapprovingAuthority.get(procurement_goods_typeDTO.approvingAuthority).add(procurement_goods_typeDTO);
//
//					if( ! mapOfProcurementGoodsTypeDTOTosourceOfFundCat.containsKey(procurement_goods_typeDTO.sourceOfFundCat)) {
//						mapOfProcurementGoodsTypeDTOTosourceOfFundCat.put(procurement_goods_typeDTO.sourceOfFundCat, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOTosourceOfFundCat.get(procurement_goods_typeDTO.sourceOfFundCat).add(procurement_goods_typeDTO);
//
//					if( ! mapOfProcurementGoodsTypeDTOTotimeCode.containsKey(procurement_goods_typeDTO.timeCode)) {
//						mapOfProcurementGoodsTypeDTOTotimeCode.put(procurement_goods_typeDTO.timeCode, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOTotimeCode.get(procurement_goods_typeDTO.timeCode).add(procurement_goods_typeDTO);
//
//					if( ! mapOfProcurementGoodsTypeDTOTotender.containsKey(procurement_goods_typeDTO.tender)) {
//						mapOfProcurementGoodsTypeDTOTotender.put(procurement_goods_typeDTO.tender, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOTotender.get(procurement_goods_typeDTO.tender).add(procurement_goods_typeDTO);
//
//					if( ! mapOfProcurementGoodsTypeDTOTotenderOpening.containsKey(procurement_goods_typeDTO.tenderOpening)) {
//						mapOfProcurementGoodsTypeDTOTotenderOpening.put(procurement_goods_typeDTO.tenderOpening, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOTotenderOpening.get(procurement_goods_typeDTO.tenderOpening).add(procurement_goods_typeDTO);
//
//					if( ! mapOfProcurementGoodsTypeDTOTotenderEvaluation.containsKey(procurement_goods_typeDTO.tenderEvaluation)) {
//						mapOfProcurementGoodsTypeDTOTotenderEvaluation.put(procurement_goods_typeDTO.tenderEvaluation, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOTotenderEvaluation.get(procurement_goods_typeDTO.tenderEvaluation).add(procurement_goods_typeDTO);
//
//					if( ! mapOfProcurementGoodsTypeDTOToapprovalToward.containsKey(procurement_goods_typeDTO.approvalToward)) {
//						mapOfProcurementGoodsTypeDTOToapprovalToward.put(procurement_goods_typeDTO.approvalToward, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOToapprovalToward.get(procurement_goods_typeDTO.approvalToward).add(procurement_goods_typeDTO);
//
//					if( ! mapOfProcurementGoodsTypeDTOToawardNotification.containsKey(procurement_goods_typeDTO.awardNotification)) {
//						mapOfProcurementGoodsTypeDTOToawardNotification.put(procurement_goods_typeDTO.awardNotification, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOToawardNotification.get(procurement_goods_typeDTO.awardNotification).add(procurement_goods_typeDTO);
//
//					if( ! mapOfProcurementGoodsTypeDTOTosiginingOfContract.containsKey(procurement_goods_typeDTO.siginingOfContract)) {
//						mapOfProcurementGoodsTypeDTOTosiginingOfContract.put(procurement_goods_typeDTO.siginingOfContract, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOTosiginingOfContract.get(procurement_goods_typeDTO.siginingOfContract).add(procurement_goods_typeDTO);
//
//					if( ! mapOfProcurementGoodsTypeDTOTocontractSignatureTime.containsKey(procurement_goods_typeDTO.contractSignatureTime)) {
//						mapOfProcurementGoodsTypeDTOTocontractSignatureTime.put(procurement_goods_typeDTO.contractSignatureTime, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOTocontractSignatureTime.get(procurement_goods_typeDTO.contractSignatureTime).add(procurement_goods_typeDTO);
//
//					if( ! mapOfProcurementGoodsTypeDTOTocontractCompletionTime.containsKey(procurement_goods_typeDTO.contractCompletionTime)) {
//						mapOfProcurementGoodsTypeDTOTocontractCompletionTime.put(procurement_goods_typeDTO.contractCompletionTime, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOTocontractCompletionTime.get(procurement_goods_typeDTO.contractCompletionTime).add(procurement_goods_typeDTO);
//
//					if( ! mapOfProcurementGoodsTypeDTOToinsertedByUserId.containsKey(procurement_goods_typeDTO.insertedByUserId)) {
//						mapOfProcurementGoodsTypeDTOToinsertedByUserId.put(procurement_goods_typeDTO.insertedByUserId, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOToinsertedByUserId.get(procurement_goods_typeDTO.insertedByUserId).add(procurement_goods_typeDTO);
//
//					if( ! mapOfProcurementGoodsTypeDTOToinsertedByOrganogramId.containsKey(procurement_goods_typeDTO.insertedByOrganogramId)) {
//						mapOfProcurementGoodsTypeDTOToinsertedByOrganogramId.put(procurement_goods_typeDTO.insertedByOrganogramId, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOToinsertedByOrganogramId.get(procurement_goods_typeDTO.insertedByOrganogramId).add(procurement_goods_typeDTO);
//
//					if( ! mapOfProcurementGoodsTypeDTOToinsertionDate.containsKey(procurement_goods_typeDTO.insertionDate)) {
//						mapOfProcurementGoodsTypeDTOToinsertionDate.put(procurement_goods_typeDTO.insertionDate, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOToinsertionDate.get(procurement_goods_typeDTO.insertionDate).add(procurement_goods_typeDTO);
//
//					if( ! mapOfProcurementGoodsTypeDTOTolastModificationTime.containsKey(procurement_goods_typeDTO.lastModificationTime)) {
//						mapOfProcurementGoodsTypeDTOTolastModificationTime.put(procurement_goods_typeDTO.lastModificationTime, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOTolastModificationTime.get(procurement_goods_typeDTO.lastModificationTime).add(procurement_goods_typeDTO);
//
//					if( ! mapOfProcurementGoodsTypeDTOTosearchColumn.containsKey(procurement_goods_typeDTO.searchColumn)) {
//						mapOfProcurementGoodsTypeDTOTosearchColumn.put(procurement_goods_typeDTO.searchColumn, new HashSet<>());
//					}
//					mapOfProcurementGoodsTypeDTOTosearchColumn.get(procurement_goods_typeDTO.searchColumn).add(procurement_goods_typeDTO);
//
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public ProcurementGoodsTypeDTO clone(ProcurementGoodsTypeDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, ProcurementGoodsTypeDTO.class);
	}

	public List<ProcurementGoodsTypeDTO> clone(List<ProcurementGoodsTypeDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public ProcurementGoodsTypeDTO  getProcurementGoodsTypeDTOByIDWithoutClone(long ID){
		return mapOfProcurementGoodsTypeDTOToiD.get(ID);
	}

	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeList() {
		List <ProcurementGoodsTypeDTO> procurement_goods_types = new ArrayList<ProcurementGoodsTypeDTO>(this.mapOfProcurementGoodsTypeDTOToiD.values());
		return procurement_goods_types;
	}
	
	
	public ProcurementGoodsTypeDTO getProcurementGoodsTypeDTOByID( long ID){
		return clone(mapOfProcurementGoodsTypeDTOToiD.get(ID));
	}

	public List<ProcurementGoodsTypeDTO> getDTOsLikeName(String name) {
		if (!StringUtils.isValidString(name)) return getProcurementGoodsTypeList();
		return getProcurementGoodsTypeList()
				.stream()
				.filter(dto ->
						dto.nameEn.contains(name) || dto.nameBn.contains(name))
				.collect(Collectors.toList());
	}

	public String getOptions (String language, long defaultValue)
	{

		String sOptions = "";

		if (defaultValue == CatDTO.CATDEFAULTNOTREQUIRED)
		{
			if (language.equalsIgnoreCase("English"))
			{
				sOptions += "<option value = '" + CatDTO.CATDEFAULTNOTREQUIRED + "'>Select</option>";
			}
			else
			{
				sOptions += "<option value = '" + CatDTO.CATDEFAULTNOTREQUIRED + "'>বাছাই করুন</option>";
			}
		}
		else if (defaultValue == CatDTO.CATDEFAULT)
		{
			if (language.equalsIgnoreCase("English"))
			{
				sOptions += "<option value = ''>Select</option>";
			}
			else
			{
				sOptions += "<option value = ''>বাছাই করুন</option>";
			}
		}

		List<ProcurementGoodsTypeDTO> dtos = clone(getProcurementGoodsTypeList());

		for (ProcurementGoodsTypeDTO dto : dtos) {
			String sOption =  "<option value = '" + dto.iD + "'";
			if(defaultValue != CatDTO.CATDEFAULTNOTREQUIRED && dto.iD == defaultValue)
			{
				sOption += " selected ";
			}
			String name = language.equals("English") ? dto.nameEn : dto.nameBn;
			sOption += ">"
					+ name
					+ "</option>";
			sOptions += sOption;
		}

        return sOptions;
	}

	public String getName (String language, long defaultValue)
	{
		ProcurementGoodsTypeDTO dto = getProcurementGoodsTypeDTOByID(defaultValue);
		return dto == null ? "" : UtilCharacter.getDataByLanguage(language, dto.nameBn, dto.nameEn);
	}
	
// 	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOByname_en(String name_en) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOTonameEn.getOrDefault(name_en,new HashSet<>()));
//	}
//
//
//	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOByname_bn(String name_bn) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOTonameBn.getOrDefault(name_bn,new HashSet<>()));
//	}
//
//
	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOByprocurementPackageId(long procurementPackageId) {
		return clone(new ArrayList<>( mapOfProcurementGoodsTypeDTOToprocurementPackageId.getOrDefault(procurementPackageId,new HashSet<>())));
	}


	public void deleteProcurementGoodsTypeDTOByprocurementPackageId(long procurementPackageId) {
        mapOfProcurementGoodsTypeDTOToprocurementPackageId.remove(procurementPackageId);
	}

//
//
//	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOByunit_price(double unit_price) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOTounitPrice.getOrDefault(unit_price,new HashSet<>()));
//	}
//
//
//	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOByest_cost(double est_cost) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOToestCost.getOrDefault(est_cost,new HashSet<>()));
//	}
//
//
//	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOBymethod_and_type_cat(long method_and_type_cat) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOTomethodAndTypeCat.getOrDefault(method_and_type_cat,new HashSet<>()));
//	}
//
//
//	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOByapproving_authority(String approving_authority) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOToapprovingAuthority.getOrDefault(approving_authority,new HashSet<>()));
//	}
//
//
//	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOBysource_of_fund_cat(long source_of_fund_cat) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOTosourceOfFundCat.getOrDefault(source_of_fund_cat,new HashSet<>()));
//	}
//
//
//	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOBytime_code(String time_code) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOTotimeCode.getOrDefault(time_code,new HashSet<>()));
//	}
//
//
//	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOBytender(String tender) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOTotender.getOrDefault(tender,new HashSet<>()));
//	}
//
//
//	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOBytender_opening(String tender_opening) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOTotenderOpening.getOrDefault(tender_opening,new HashSet<>()));
//	}
//
//
//	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOBytender_evaluation(String tender_evaluation) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOTotenderEvaluation.getOrDefault(tender_evaluation,new HashSet<>()));
//	}
//
//
//	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOByapproval_toward(String approval_toward) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOToapprovalToward.getOrDefault(approval_toward,new HashSet<>()));
//	}
//
//
//	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOByaward_notification(String award_notification) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOToawardNotification.getOrDefault(award_notification,new HashSet<>()));
//	}
//
//
//	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOBysigining_of_contract(String sigining_of_contract) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOTosiginingOfContract.getOrDefault(sigining_of_contract,new HashSet<>()));
//	}
//
//
//	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOBycontract_signature_time(long contract_signature_time) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOTocontractSignatureTime.getOrDefault(contract_signature_time,new HashSet<>()));
//	}
//
//
//	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOBycontract_completion_time(long contract_completion_time) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOTocontractCompletionTime.getOrDefault(contract_completion_time,new HashSet<>()));
//	}
//
//
//	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOByinserted_by_user_id(long inserted_by_user_id) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
//	}
//
//
//	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
//	}
//
//
//	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOByinsertion_date(long insertion_date) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
//	}
//
//
//	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOBylastModificationTime(long lastModificationTime) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
//	}
//
//
//	public List<ProcurementGoodsTypeDTO> getProcurementGoodsTypeDTOBysearch_column(String search_column) {
//		return new ArrayList<>( mapOfProcurementGoodsTypeDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
//	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "procurement_goods_type";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}

	public String getText(long id,String language){
		ProcurementGoodsTypeDTO dto = getProcurementGoodsTypeDTOByID(id);
		return dto==null?"": ("Bangla".equalsIgnoreCase(language)?dto.nameBn: dto.nameEn);
	}
}


