package employee_medical_report;


import java.io.IOException;
import java.util.ArrayList;

import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;

import login.LoginDTO;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Employee_medical_report_Servlet")
public class Employee_medical_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria;
	
	String[][] Display;
	
	
	String GroupBy = " employee_record_id ";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Employee_medical_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = " prescription_details\r\n";
		

		
		ArrayList<String[]> criteriaList = new ArrayList<String[]>();
		
		String [] sCrit = {"criteria","","isDeleted","=","","int","","","0","none", ""};
		criteriaList.add(sCrit);
		
		
		
		String startDate = request.getParameter("startDate");
		if(startDate != null && !startDate.equalsIgnoreCase("") && !startDate.equalsIgnoreCase("0"))
		{
			
			criteriaList.add(new String[] {"criteria","","visit_date",">=","AND","long","","",1 + "","startDate", LC.HM_START_DATE + ""});

		}
		String endDate = request.getParameter("endDate");
		if(endDate != null && !endDate.equalsIgnoreCase("")&& !endDate.equalsIgnoreCase("1651341600000"))
		{
			
			criteriaList.add(new String[] {"criteria","","visit_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""});
		}
		
		String userName = request.getParameter("userName");
		if(userName != null && !userName.equalsIgnoreCase(""))
		{
			criteriaList.add(new String[] {"criteria","","employee_record_id","=","AND","String","","","any","userName",LC.PHARMACY_REPORT_WHERE_EMPLOYEEID + "", "userNameToEmployeeRecordId"});
		}
		
		String officeUnitId = request.getParameter("officeUnitId");
		if(officeUnitId != null && !officeUnitId.equalsIgnoreCase(""))
		{
			Set<Long> organogramsList = OfficeUnitOrganogramsRepository.getInstance().getAllDescentsOrganogramIdsInclusive(Long.parseLong(officeUnitId));
			String sOrg = "";
			int i = 0;
			for(Long officeUnitOrganogram: organogramsList)
			{
				if(i > 0)
				{
					sOrg += ", ";
				}
				sOrg += officeUnitOrganogram;
				i++;
			}
			if(!sOrg.equalsIgnoreCase(""))
			{
				criteriaList.add(new String[] {"criteria","","organogram_id","in","AND","String","","",sOrg,"userName", ""});
			}
		}
		
		String mp = request.getParameter("mp");
		if(mp != null && !mp.equalsIgnoreCase(""))
		{
			criteriaList.add(new String[] {"criteria","","employee_user_name","like","AND","exact","","","0%","none",""});
		}
		
		Criteria = new String[criteriaList.size()][];
		int i = 0;
		for(String [] sCrtiFromList: criteriaList)
		{
			Criteria[i++] = sCrtiFromList;
		}
		
		
		Display= new String[5][4];
		
		Display[0] = new String[]{"display","",
				"employee_record_id",
				"erIdToOffice",
				LM.getText(LC.HM_OFFICE, loginDTO)};
		Display[1] = new String[]{"display","",
				"employee_record_id",
				"erIdToUserName",
				language.equalsIgnoreCase("english")?"Reference UserID":"রেফারেন্স ইউজারআইডি"};
		Display[2] = new String[]{"display","",
				"employee_record_id",
				"erIdToName",
				LM.getText(LC.HM_REFERENCE_EMPLOYEE, loginDTO)};
		Display[3] = new String[]{"display","",
				"employee_record_id",
				"erIdToOrganogram",
				LM.getText(LC.HM_DESIGNATION, loginDTO)};
		Display[4] = new String[]{"display","",
				"COUNT(distinct prescription_details.id)",
				"int",
				LM.getText(LC.EMPLOYEE_MEDICAL_REPORT_SELECT_VISITCOUNT, loginDTO)};
	




		
		String reportName = LM.getText(LC.EMPLOYEE_MEDICAL_REPORT_OTHER_EMPLOYEE_MEDICAL_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(4, ReportRequestHandler.RIGHT_ALIGN_INT));
		

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "employee_medical_report",
				MenuConstants.EMPLOYEE_MEDICAL_REPORT_DETAILS, language, reportName, "employee_medical_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
