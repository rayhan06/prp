package police_verification;

import sessionmanager.SessionConstants;
import util.CommonEmployeeDTO;


public class Police_verificationDTO extends CommonEmployeeDTO {
    public int verificationCat = -1;
    public long cardInfoId = -1;
    public long fileDropzone = -1;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;
    public long modifiedBy = -1;


    @Override
    public String toString() {
        return "Police_verificationDTO{" +
                "verificationCat=" + verificationCat +
                ", cardInfoId=" + cardInfoId +
                ", employeeRecordId=" + employeeRecordsId +
                ", fileDropzone=" + fileDropzone +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                ", iD=" + iD +
                ", isDeleted=" + isDeleted +
                '}';
    }

}