package police_verification;

import common.ConnectionAndStatementUtil;
import common.EmployeeCommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

@SuppressWarnings({"unused","Duplicates"})
public class Police_verificationDAO implements EmployeeCommonDAOService<Police_verificationDTO> {
    private static final Logger logger = Logger.getLogger(Police_verificationDAO.class);
    private static final String addQuery = "INSERT INTO {tableName} (verification_cat, file_dropzone, modified_by, lastModificationTime,card_info_id, " +
            " employee_record_id,inserted_by, insertion_time, isDeleted, ID) VALUES(?,?,?,?,?,?,?,?,?,?)";

    private static final String updateQuery = "UPDATE {tableName} SET verification_cat = ?, file_dropzone = ?,  " +
            "modified_by = ?, lastModificationTime = ?  WHERE ID = ?";

    private static final String getByEmployeeRecordId = "SELECT * FROM police_verification WHERE verification_cat = 1 AND employee_record_id = %d";

    private static final String getByCardInfoId = "SELECT * FROM police_verification WHERE verification_cat = 1 AND card_info_id = %d";

    private static Police_verificationDAO INSTANCE = null;
    private Police_verificationDAO() {

    }

    public static Police_verificationDAO getInstance(){
        if(INSTANCE == null){
            synchronized (Police_verificationDAO.class){
                if(INSTANCE == null){
                    INSTANCE = new Police_verificationDAO();
                }
            }
        }
        return INSTANCE;
    }


    public void set(PreparedStatement ps, Police_verificationDTO police_verificationDTO, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setObject(++index, police_verificationDTO.verificationCat);
        ps.setObject(++index, police_verificationDTO.fileDropzone);
        ps.setLong(++index, police_verificationDTO.modifiedBy);
        ps.setLong(++index, police_verificationDTO.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, police_verificationDTO.cardInfoId);
            ps.setObject(++index, police_verificationDTO.employeeRecordsId);
            ps.setLong(++index, police_verificationDTO.insertedBy);
            ps.setLong(++index, police_verificationDTO.insertionTime);
            ps.setInt(++index, 0);
        }
        ps.setLong(++index, police_verificationDTO.iD);
    }

    public Police_verificationDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Police_verificationDTO police_verificationDTO = new Police_verificationDTO();

            police_verificationDTO.iD = rs.getLong("ID");
            police_verificationDTO.verificationCat = rs.getInt("verification_cat");
            police_verificationDTO.cardInfoId = rs.getLong("card_info_id");
            police_verificationDTO.fileDropzone = rs.getLong("file_dropzone");
            police_verificationDTO.insertedBy = rs.getLong("inserted_by");
            police_verificationDTO.insertionTime = rs.getLong("insertion_time");
            police_verificationDTO.isDeleted = rs.getInt("isDeleted");
            police_verificationDTO.modifiedBy = rs.getLong("modified_by");
            police_verificationDTO.lastModificationTime = rs.getLong("lastModificationTime");
            police_verificationDTO.employeeRecordsId = rs.getLong("employee_record_id");
            return police_verificationDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "police_verification";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return null;
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Police_verificationDTO) commonDTO, updateQuery, false);
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Police_verificationDTO) commonDTO, addQuery, true);
    }

    public Police_verificationDTO getByEmployeeRecordIdWhichPoliceVerificationIsDone(long employeeRecordId){
        String sql = String.format(getByEmployeeRecordId,employeeRecordId);
        return ConnectionAndStatementUtil.getT(sql,this::buildObjectFromResultSet);
    }

    public Police_verificationDTO getByCardInfoId(long cardInfoId){
        String sql = String.format(getByCardInfoId,cardInfoId);
        return ConnectionAndStatementUtil.getT(sql,this::buildObjectFromResultSet);
    }
}