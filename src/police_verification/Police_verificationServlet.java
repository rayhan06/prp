package police_verification;

import card_info.CardInfoDAO;
import card_info.Card_infoDTO;
import common.BaseServlet;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

@WebServlet("/Police_verificationServlet")
@MultipartConfig
public class Police_verificationServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    private final Police_verificationDAO policeVerificationDAO = Police_verificationDAO.getInstance();

    @Override
    public String getTableName() {
        return policeVerificationDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Police_verificationServlet";
    }

    @Override
    public Police_verificationDAO getCommonDAOService() {
        return policeVerificationDAO;
    }

    @Override
    public String getAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrAjaxAddRedirectURL(commonDTO);
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrAjaxAddRedirectURL(commonDTO);
    }

    private String getAddOrAjaxAddRedirectURL(CommonDTO commonDTO){
        Police_verificationDTO police_verificationDTO = (Police_verificationDTO) commonDTO;
        if (police_verificationDTO.verificationCat == 1) {
            return "Card_approval_mappingServlet?actionType=ajax_approvedCard&cardInfoId=" + police_verificationDTO.cardInfoId;
        } else {
            return "Card_approval_mappingServlet?actionType=ajax_rejectCard&cardInfoId=" + police_verificationDTO.cardInfoId;
        }
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Police_verificationDTO police_verificationDTO = new Police_verificationDTO();
        police_verificationDTO.verificationCat = Integer.parseInt(request.getParameter("verificationCat"));
        police_verificationDTO.cardInfoId = Long.parseLong(request.getParameter("cardInfoId"));
        Card_infoDTO cardInfoDTO = CardInfoDAO.getInstance().getDTOFromID(police_verificationDTO.cardInfoId);
        police_verificationDTO.employeeRecordsId = cardInfoDTO.employeeRecordsId;
        police_verificationDTO.fileDropzone = Long.parseLong(request.getParameter("fileDropzone"));
        police_verificationDTO.insertedBy = police_verificationDTO.modifiedBy = userDTO.ID;
        police_verificationDTO.insertionTime = police_verificationDTO.lastModificationTime = System.currentTimeMillis();
        policeVerificationDAO.add(police_verificationDTO);
        return police_verificationDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.CARD_APPROVAL_MAPPING_SEARCH};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.CARD_APPROVAL_MAPPING_SEARCH};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Police_verificationServlet.class;
    }
}