package job_applicant_qualifications;

import common.ConnectionAndStatementUtil;
import dbm.DBMR;
import dbm.DBMW;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO;

import java.sql.*;
import java.util.*;

public class Job_applicant_qualificationDAO extends NavigationService4
{

	Logger logger = Logger.getLogger(getClass());


	public Job_applicant_qualificationDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		// commonMaps = new Job_applicant_documentsMAPS(tableName);
	}

	@Override
	public Collection getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
		return null;
	}

	public Job_applicant_qualificationDAO()
	{
		this("job_applicant_qualifications");
	}
//
	public void get(Job_applicant_qualificationDTO job_applicant_qualificationDTO, ResultSet rs) throws SQLException
	{
		job_applicant_qualificationDTO.iD = rs.getLong("ID");
		job_applicant_qualificationDTO.jobApplicantId = rs.getLong("job_applicant_id");
		job_applicant_qualificationDTO.recruitmentJobSpecificCertId = rs.getLong("recruitment_job_specific_certs_id");
		job_applicant_qualificationDTO.jobId = rs.getLong("job_id");
		job_applicant_qualificationDTO.isChecked = rs.getBoolean("is_checked");
		job_applicant_qualificationDTO.filesDropzone = rs.getLong("files_dropzone");
		job_applicant_qualificationDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		job_applicant_qualificationDTO.insertionDate = rs.getLong("insertion_date");
		job_applicant_qualificationDTO.modifiedBy = rs.getString("modified_by");
		job_applicant_qualificationDTO.isDeleted = rs.getInt("isDeleted");
		job_applicant_qualificationDTO.lastModificationTime = rs.getLong("lastModificationTime");
		job_applicant_qualificationDTO.valid_upto = rs.getLong("valid_upto");
		job_applicant_qualificationDTO.credential_id = rs.getString("credential_id");
	}

	public Job_applicant_qualificationDTO build(ResultSet rs)
	{
		try
		{
			Job_applicant_qualificationDTO job_applicant_qualificationDTO = new Job_applicant_qualificationDTO();
			job_applicant_qualificationDTO.iD = rs.getLong("ID");
			job_applicant_qualificationDTO.jobApplicantId = rs.getLong("job_applicant_id");
			job_applicant_qualificationDTO.recruitmentJobSpecificCertId = rs.getLong("recruitment_job_specific_certs_id");
			job_applicant_qualificationDTO.jobId = rs.getLong("job_id");
			job_applicant_qualificationDTO.isChecked = rs.getBoolean("is_checked");
			job_applicant_qualificationDTO.filesDropzone = rs.getLong("files_dropzone");
			job_applicant_qualificationDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			job_applicant_qualificationDTO.insertionDate = rs.getLong("insertion_date");
			job_applicant_qualificationDTO.modifiedBy = rs.getString("modified_by");
			job_applicant_qualificationDTO.isDeleted = rs.getInt("isDeleted");
			job_applicant_qualificationDTO.lastModificationTime = rs.getLong("lastModificationTime");
			job_applicant_qualificationDTO.valid_upto = rs.getLong("valid_upto");
			job_applicant_qualificationDTO.credential_id = rs.getString("credential_id");
			return job_applicant_qualificationDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	public List<Job_applicant_qualificationDTO> getJob_applicant_qualificationDTOListByjobApplicantIdAndJobId(long appId, long jobId) throws Exception{

		String sql = "SELECT * FROM job_applicant_qualifications where isDeleted=0 and job_applicant_id = ? "
				+ " and job_id = ? ";

		logger.debug("sql " + sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(appId, jobId),this::build);
	}

		
	

	//need another getter for repository
	public Job_applicant_qualificationDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		Job_applicant_qualificationDTO qualificationDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
		return qualificationDTO;
	}
	
	
	public List<Job_applicant_qualificationDTO> getDTOs(Collection recordIDs){
		if(recordIDs.isEmpty()){
			return new ArrayList<>();
		}
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+= " ? ";
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	
	}
	
	

	
	
	
	//add repository
	public List<Job_applicant_qualificationDTO> getAllJob_applicant_qualifications (boolean isFirstReload)
    {

		String sql = "SELECT * FROM job_applicant_qualifications";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by job_applicant_qualifications.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }

	public List<Job_applicant_qualificationDTO> getDTOByJobApplicantID (long ID)
	{

		String sql = "SELECT * FROM job_applicant_qualifications";

		sql += " WHERE job_applicant_id = ? ";

		sql+=" AND isDeleted =  0";

		sql += " order by job_applicant_qualifications.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(ID), this::build);
	}
				
}
	