package job_applicant_qualifications;

import files.FilesDTO;
import util.CommonDTO;

import java.util.ArrayList;
import java.util.List;


public class Job_applicant_qualificationSummaryDTO extends CommonDTO
{
    public Boolean isChecked = false;
    public long jobApplicantId = 0;
    public long jobId = 0;
    public long recruitmentJobSpecificCertId = 0;
    public long filesDropzone = -1;
    public long insertedByUserId = 0;
    public long insertionDate = 0;
    public String modifiedBy = "";
    public long valid_upto = 0;
    public String credential_id = "";

    public List<FilesDTO> filesDropzoneFiles = new ArrayList();

    @Override
    public String toString() {
        return "Job_applicant_qualificationDTO[" +
                " iD = " + iD +
                " jobApplicantId = " + jobApplicantId +
                " jobId = " + jobId +
                " recruitmentJobSpecificCertId = " + recruitmentJobSpecificCertId +
                " isChecked = " + isChecked +
                " filesDropzone = " + filesDropzone +
                " insertedByUserId = " + insertedByUserId +
                " insertionDate = " + insertionDate +
                " modifiedBy = " + modifiedBy +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}