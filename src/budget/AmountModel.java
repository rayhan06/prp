package budget;

import budget_selection_info.BudgetSelectionInfoRepository;

public class AmountModel {
    public String economicYear;
    public String budget="0.0";
    public String revisedBudget="0.0";
    public String expenditure="0.0";
    public String revisedExpenditure="0.0";
    public AmountModel(){
    }

    public AmountModel(BudgetDTO dto){
        economicYear= BudgetSelectionInfoRepository.getInstance().getDTOByID(dto.budgetSelectionInfoId).economicYear;
        budget=String.valueOf(dto.finalAmount);
        revisedBudget=String.valueOf(dto.revisedFinalAmount);
        expenditure=String.valueOf(dto.expenditureAmount);
        revisedExpenditure=String.valueOf(dto.revisedExpenditureAmount);
    }
}
