package budget;

import common.NameDTO;
import economic_code.Economic_codeDTO;
import economic_code.Economic_codeRepository;
import economic_group.EconomicGroupRepository;
import economic_sub_code.Economic_sub_codeDTO;
import economic_sub_code.Economic_sub_codeRepository;
import util.StringUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class BudgetModel {
    public String id = "";
    public String economicCodeId = "";
    public String economicGroupId = "";
    public String name = "";
    public String code = ""; // 7 digit economic code - converted for language
    // these code needed for js logic
    public String economicGroup = ""; // 2 digit economic code
    public String economicGroupName = "";
    public String economicCode = ""; // 4 digit economic code
    public String economicCodeName = "";
    public String economicSubCode = ""; // 7 digit economic code
    public String economicSubCodeId = "";
    public String budgetAmount = "";
    public String budgetComment = "";
    public String budgetOperationId = "";
    public String revisedBudgetAmount = "";
    public String revisedBudgetComment = "";
    public String finalBudgetAmount = "";
    public String finalBudgetComment = "";
    public String revisedFinalBudgetAmount = "";
    public String revisedFinalBudgetComment = "";
    public String projection1="";
    public String projection2="";
    public String preparationComment="";
    List<AmountModel> previousAmounts;

    public String expenditureAmount = "";
    public String revisedExpenditureAmount = "";


    public BudgetModel() {
        this.previousAmounts = new ArrayList<>();
    }

    public BudgetModel(BudgetDTO budgetDTO, String language) {
        Economic_sub_codeDTO subCodeDTO = Economic_sub_codeRepository.getInstance().getDTOByID(budgetDTO.economicSubCodeId);
        id = String.valueOf(budgetDTO.iD);
        boolean isLanguageEnglish = "English".equalsIgnoreCase(language);
        economicSubCodeId = String.valueOf(subCodeDTO.iD);
        name = isLanguageEnglish ? subCodeDTO.descriptionEn : subCodeDTO.descriptionBn;
        code = isLanguageEnglish ? subCodeDTO.code : StringUtils.convertToBanNumber(subCodeDTO.code);
        economicSubCode = subCodeDTO.code;

        Economic_codeDTO economicCodeDTO = Economic_codeRepository.getInstance().getById(subCodeDTO.economicCodeId);
        economicCodeId = String.valueOf(economicCodeDTO.iD);
        economicCode = economicCodeDTO.code;
        economicCodeName = isLanguageEnglish ? economicCodeDTO.descriptionEn : economicCodeDTO.descriptionBn;

        NameDTO economicGroupDTO = EconomicGroupRepository.getInstance().getDTOByID(economicCodeDTO.economicGroupType);
        economicGroup = economicGroupId = String.valueOf(economicGroupDTO.iD);
        economicGroupName = isLanguageEnglish ? economicGroupDTO.nameEn : economicGroupDTO.nameBn;

        //budgetAmount=isLanguageEnglish?String.valueOf(budgetDTO.amount):StringUtils.convertToBanNumber(String.valueOf(budgetDTO.amount));
        budgetAmount = String.valueOf(budgetDTO.amount);
        budgetComment = budgetDTO.amountComment;

        expenditureAmount = String.valueOf(budgetDTO.expenditureAmount);
        revisedExpenditureAmount = String.valueOf(budgetDTO.revisedExpenditureAmount);

        revisedBudgetAmount=String.valueOf(budgetDTO.revisedAmount);
        revisedBudgetComment=budgetDTO.revisedComment;

        budgetOperationId = String.valueOf(budgetDTO.budgetOperationId);
        revisedBudgetAmount = String.valueOf(budgetDTO.revisedAmount);
        revisedBudgetComment = budgetDTO.revisedComment;


        finalBudgetAmount = String.valueOf(budgetDTO.finalAmount);
        finalBudgetComment=budgetDTO.finalAmountComment;
        revisedFinalBudgetAmount = String.valueOf(budgetDTO.revisedFinalAmount);
        revisedFinalBudgetComment = budgetDTO.revisedFinalComment;
        projection1=String.valueOf(budgetDTO.nextYear1Projection);
        projection2=String.valueOf(budgetDTO.nextYear2Projection);
        preparationComment=budgetDTO.preparationComment==null?"":budgetDTO.preparationComment;
        this.previousAmounts = new ArrayList<>();
    }

    public void setPreviousAmounts(List<Map<Long, AmountModel>> listOfPreviousMaps) {
        for (Map<Long, AmountModel> previousMap : listOfPreviousMaps) {
            if (previousMap.get(Long.valueOf(economicSubCodeId)) != null) {
                this.previousAmounts.add(previousMap.get(Long.valueOf(economicSubCodeId)));
            } else {
                AmountModel amountModel = new AmountModel();
                Iterator iterator = previousMap.entrySet().iterator();
                if (iterator.hasNext()) {
                    Map.Entry mapEntry = (Map.Entry) iterator.next();
                    amountModel.economicYear = previousMap.get(mapEntry.getKey()).economicYear;
                }
                this.previousAmounts.add(amountModel);
            }
        }
    }

    @Override
    public String toString() {
        return "BudgetModel{" +
                "id='" + id + '\'' +
                ", economicCodeId='" + economicCodeId + '\'' +
                ", economicGroupId='" + economicGroupId + '\'' +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", economicGroup='" + economicGroup + '\'' +
                ", economicGroupName='" + economicGroupName + '\'' +
                ", economicCode='" + economicCode + '\'' +
                ", economicCodeName='" + economicCodeName + '\'' +
                ", economicSubCode='" + economicSubCode + '\'' +
                ", economicSubCodeId='" + economicSubCodeId + '\'' +
                ", budgetAmount='" + budgetAmount + '\'' +
                ", budgetOperationId='" + budgetOperationId + '\'' +
                ", expenditureAmount='" + expenditureAmount + '\'' +
                ", revisedExpenditureAmount='" + revisedExpenditureAmount + '\'' +
                ", revisedBudgetAmount='" + revisedBudgetAmount + '\'' +
                ", revisedBudgetComment='" + revisedBudgetComment + '\'' +
                ", finalBudgetAmount='" + finalBudgetAmount + '\'' +
                ", revisedFinalBudgetAmount='" + revisedFinalBudgetAmount + '\'' +
                ", revisedFinalBudgetComment='" + revisedFinalBudgetComment + '\'' +
                '}';
    }
}
