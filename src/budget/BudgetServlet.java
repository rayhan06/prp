package budget;

import budget_submission_info.Budget_submission_infoDAO;
import com.google.gson.Gson;
import common.ApiResponse;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pb.Utils;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.RecordNavigationManager4;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/BudgetServlet")
@MultipartConfig
public class BudgetServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(BudgetServlet.class);
    private static final String tableName = "budget";

    private final BudgetDAO budgetDAO;

    public BudgetServlet() {
        super();
        budgetDAO = BudgetDAO.getInstance();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        if (userDTO != null) {
            try {
                long budgetSelectionInfoId;
                long budgetOfficeId;
                long budgetOperationId;
                long budgetMappingId;
                int budgetTypeCat;
                List<BudgetDTO> budgetDTOList;
                String options;
                String language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
                String actionType = request.getParameter("actionType");
                switch (actionType) {
                    case "ajax_buildEconomicGroupsWithBudgetMapping": {
                        try {
                            String parameter = request.getParameter("budgetSelectionInfoId");
                            budgetSelectionInfoId = Utils.parseMandatoryLong(
                                    parameter,
                                    "budgetSelectionInfoId=" + parameter + " is invalid"
                            );
                            parameter = request.getParameter("budgetMappingId");
                            budgetMappingId = Utils.parseMandatoryLong(
                                    parameter,
                                    "budgetMappingId=" + parameter + " is invalid"
                            );
                            budgetDTOList = budgetDAO.getByMappingId(budgetSelectionInfoId, budgetMappingId);
                            options = budgetDAO.buildEconomicGroups(language, null, budgetDTOList);
                        } catch (Exception e) {
                            logger.error(e);
                            options = "";
                        }
                        response.setContentType("text/html; charset=UTF-8");
                        response.getWriter().println(options);
                        return;
                    }
                    case "buildEconomicGroups":
                        budgetSelectionInfoId = Long.parseLong(request.getParameter("budgetSelectionInfoId").trim());
                        budgetOfficeId = Long.parseLong(request.getParameter("budgetOfficeId").trim());
                        budgetOperationId = Long.parseLong(request.getParameter("budgetOperationId").trim());
                        budgetDTOList = budgetDAO.getByBudgetOfficeAndOperationId(budgetSelectionInfoId, budgetOfficeId, budgetOperationId);
                        options = budgetDAO.buildEconomicGroups(language, null, budgetDTOList);
                        logger.debug(options);
                        response.setContentType("text/html; charset=UTF-8");
                        response.getWriter().println(options);
                        return;
                    case "buildEconomicCodes": {
                        budgetSelectionInfoId = Long.parseLong(request.getParameter("budgetSelectionInfoId").trim());
                        budgetOfficeId = Long.parseLong(request.getParameter("budgetOfficeId").trim());
                        budgetOperationId = Long.parseLong(request.getParameter("budgetOperationId").trim());
                        long economicGroupId = Long.parseLong(request.getParameter("economicGroupId").trim());
                        budgetDTOList = budgetDAO.getByBudgetOfficeAndOperationAndEconomicGroup(budgetSelectionInfoId, budgetOfficeId, budgetOperationId, economicGroupId);
                        options = budgetDAO.buildEconomicCodes(language, null, budgetDTOList);
                        logger.debug(options);
                        response.setContentType("text/html; charset=UTF-8");
                        response.getWriter().println(options);
                        return;
                    }
                    case "ajax_buildEconomicCodesWithBudgetMapping": {
                        try {
                            String parameter = request.getParameter("budgetSelectionInfoId");
                            budgetSelectionInfoId = Utils.parseMandatoryLong(
                                    parameter,
                                    "budgetSelectionInfoId=" + parameter + " is invalid"
                            );
                            parameter = request.getParameter("budgetMappingId");
                            budgetMappingId = Utils.parseMandatoryLong(
                                    parameter,
                                    "budgetMappingId=" + parameter + " is invalid"
                            );
                            parameter = request.getParameter("economicGroupId");
                            long economicGroupId = Utils.parseMandatoryLong(
                                    parameter,
                                    "economicGroupId=" + parameter + " is invalid"
                            );
                            budgetDTOList = budgetDAO.getByMappingIdAndEconomicGroup(
                                    budgetSelectionInfoId,
                                    budgetMappingId,
                                    economicGroupId
                            );
                            options = budgetDAO.buildEconomicCodes(language, null, budgetDTOList);
                        } catch (Exception e) {
                            logger.error(e);
                            options = "";
                        }
                        response.setContentType("text/html; charset=UTF-8");
                        response.getWriter().println(options);
                        return;
                    }
                    case "ajax_buildEconomicSubCodesWithBudgetMapping": {
                        try {
                            String parameter = request.getParameter("budgetSelectionInfoId");
                            budgetSelectionInfoId = Utils.parseMandatoryLong(
                                    parameter,
                                    "budgetSelectionInfoId=" + parameter + " is invalid"
                            );
                            parameter = request.getParameter("budgetMappingId");
                            budgetMappingId = Utils.parseMandatoryLong(
                                    parameter,
                                    "budgetMappingId=" + parameter + " is invalid"
                            );
                            parameter = request.getParameter("economicCodeId");
                            long economicCodeId = Utils.parseMandatoryLong(
                                    parameter,
                                    "economicCodeId=" + parameter + " is invalid"
                            );
                            budgetDTOList = budgetDAO.getByMappingIdAndEconomicCode(
                                    budgetSelectionInfoId,
                                    budgetMappingId,
                                    economicCodeId
                            );
                            options = budgetDAO.buildEconomicSubCodes(language, null, budgetDTOList);
                        } catch (Exception e) {
                            logger.error(e);
                            options = "";
                        }
                        response.setContentType("text/html; charset=UTF-8");
                        response.getWriter().println(options);
                        return;
                    }
                    case "getEconomicCodesForOffice": {
                        // mandatory param
                        budgetSelectionInfoId = Long.parseLong(request.getParameter("budgetSelectionInfoId").trim());
                        budgetOfficeId = Long.parseLong(request.getParameter("budgetOfficeId").trim());
                        budgetOperationId = Long.parseLong(request.getParameter("budgetOperationId").trim());
                        // optional param
                        Long economicGroupId = StringUtils.parseNullableLong(request.getParameter("economicGroupId"));
                        Long economicCodeId = StringUtils.parseNullableLong(request.getParameter("economicCodeId"));
                        budgetDTOList = budgetDAO.getByBudgetOfficeAndOperationAndCodeAndSelectionIds(
                                budgetOfficeId,
                                budgetOperationId,
                                economicGroupId,
                                economicCodeId,
                                Collections.singletonList(budgetSelectionInfoId)
                        );
                        List<BudgetModel> subCodeModelList =
                                budgetDTOList
                                        .stream()
                                        .map(dto -> new BudgetModel(dto, language))
                                        .sorted(Comparator.comparing(a -> a.economicSubCode))
                                        .collect(Collectors.toList());
                        response.setContentType("application/json");
                        response.setCharacterEncoding("UTF-8");
                        response.getWriter().println(new Gson().toJson(subCodeModelList));
                        return;
                    }
                    case "getEconomicCodesForExpenditure": {
                        budgetSelectionInfoId = Long.parseLong(request.getParameter("budgetSelectionInfoId").trim());
                        budgetOfficeId = Long.parseLong(request.getParameter("budgetOfficeId").trim());
                        budgetOperationId = Long.parseLong(request.getParameter("budgetOperationId").trim());
                        long economicCodeId = Long.parseLong(request.getParameter("economicCodeId").trim());
                        budgetTypeCat = Integer.parseInt(request.getParameter("budgetTypeCat"));

                        budgetDTOList = budgetDAO.getByBudgetOfficeAndOperationAndEconomicCode(budgetSelectionInfoId, budgetOfficeId, budgetOperationId, economicCodeId);
                        List<BudgetModel> subCodeModelList = budgetDTOList.stream()
                                                                          .map(dto -> new BudgetModel(dto, language))
                                                                          .sorted(Comparator.comparing(a -> a.economicSubCode))
                                                                          .collect(Collectors.toList());

                        boolean isAbleToSubmit = Budget_submission_infoDAO.getInstance().isAbleToSubmitExpenditure(
                                budgetSelectionInfoId, BudgetUtils.FINANCE_SECTION_OFFICE_ID, budgetTypeCat
                        );

                        response.setContentType("application/json");
                        response.setCharacterEncoding("UTF-8");
                        response.getWriter().println(getJson(isAbleToSubmit, subCodeModelList));
                        return;
                    }
                    case "buildBudgetType":
                        budgetSelectionInfoId = Long.parseLong(request.getParameter("budgetSelectionInfoId").trim());
                        options = budgetDAO.buildBudgetTypeForExpenditure(language, budgetSelectionInfoId, null);
                        logger.debug("Eligible Budget Types: \n" + options);
                        response.setContentType("text/html; charset=UTF-8");
                        response.getWriter().println(options);
                        return;
                    case "search":
                        if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LEAVE_RELIEVER_MAPPING_SEARCH)) {
                            searchBudget(request, response, loginDTO, userDTO);
                            return;
                        }
                    case "delete":
                        ApiResponse.sendErrorResponse(response, "Delete Not Allowed");
                        return;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                logger.debug(ex);
            }
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private String getJson(boolean isAbleToSubmit, List<BudgetModel> subCodeModelList) {
        return "{\"isAbleToSubmit\": " + isAbleToSubmit
               + ", \"budgetModels\": " + new Gson().toJson(subCodeModelList) + "}";
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void searchBudget(HttpServletRequest request, HttpServletResponse response,
                              LoginDTO loginDTO, UserDTO userDTO) throws Exception {
        String ajax = request.getParameter("ajax");
        boolean hasAjax = StringUtils.isNotBlank(ajax);
        logger.debug("ajax = " + ajax + " hasAjax = " + hasAjax);
        BudgetDAO budgetDAO = BudgetDAO.getInstance();
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_BUDGET,
                request,
                budgetDAO,
                SessionConstants.VIEW_BUDGET,
                SessionConstants.SEARCH_BUDGET,
                "budget",
                true,
                userDTO,
                "",
                true
        );

        rnManager.doJob(loginDTO);

        request.setAttribute("budgetDAO", budgetDAO);
        if (!hasAjax) {
            logger.debug("Going to budget/budgetSearch.jsp");
            request.getRequestDispatcher("budget/budgetSearch.jsp").forward(request, response);
        } else {
            logger.debug("Going to budget/budgetSearchForm.jsp");
            request.getRequestDispatcher("budget/budgetSearchForm.jsp").forward(request, response);
        }
    }
}