DELETE FROM language_text WHERE menuID = 1314501;
DELETE FROM language_text WHERE menuID = 1314502;
DELETE FROM language_text WHERE menuID = 1314503;
DELETE FROM language_text WHERE languageConstantPrefix = 'BUDGET_ADD';
DELETE FROM language_text WHERE languageConstantPrefix = 'BUDGET_EDIT';
DELETE FROM language_text WHERE languageConstantPrefix = 'BUDGET_SEARCH';
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853047','1314501','ID','????','BUDGET_ADD','ID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853048','1314501','Budget Selection Info Id','????? ????? ???? ????','BUDGET_ADD','BUDGETSELECTIONINFOID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853049','1314501','Budget Mapping Id','????? ????? ?? ????','BUDGET_ADD','BUDGETMAPPINGID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853050','1314501','Economic Group Id','????????? ?????? ??? ????','BUDGET_ADD','ECONOMICGROUPID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853051','1314501','Economic Code Id','????????? ??? ????','BUDGET_ADD','ECONOMICCODEID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853052','1314501','Economic Sub Code Id','????????? ??? ??? ????','BUDGET_ADD','ECONOMICSUBCODEID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853053','1314501','Budget Office Id','????? ????? ????','BUDGET_ADD','BUDGETOFFICEID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853054','1314501','Insert By','?????? ??????','BUDGET_ADD','INSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853055','1314501','Insertion Time','?????? ???','BUDGET_ADD','INSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853056','1314501','Amount','??????','BUDGET_ADD','AMOUNT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853057','1314501','Amount Insert By','?????? ?????? ??????','BUDGET_ADD','AMOUNTINSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853058','1314501','Amount InsertionTime','?????? ?????','BUDGET_ADD','AMOUNTINSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853059','1314501','Final Amount','??????? ??????','BUDGET_ADD','FINALAMOUNT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853060','1314501','Final Amount Insert By','??????? ?????? ?????? ??????','BUDGET_ADD','FINALAMOUNTINSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853061','1314501','Final Amount InsertionTime','??????? ?????? ?????','BUDGET_ADD','FINALAMOUNTINSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853062','1314501','Revised Amount','????? ??????','BUDGET_ADD','REVISEDAMOUNT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853063','1314501','Revised Comment','????? ??????? ???','BUDGET_ADD','REVISEDCOMMENT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853064','1314501','Revised Amount Insert By','????? ?????? ?????? ??????','BUDGET_ADD','REVISEDAMOUNTINSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853065','1314501','Revised Amount InsertionTime','????? ?????? ?????','BUDGET_ADD','REVISEDAMOUNTINSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853066','1314501','Revised Final Amount','????? ??????? ??????','BUDGET_ADD','REVISEDFINALAMOUNT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853067','1314501','Revised Final Comment','????? ??????? ??????? ???','BUDGET_ADD','REVISEDFINALCOMMENT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853068','1314501','Revised Final Amount Insert By','????? ??????? ?????? ?????? ??????','BUDGET_ADD','REVISEDFINALAMOUNTINSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853069','1314501','Revised Final Amount InsertionTime','????? ??????? ?????? ?????','BUDGET_ADD','REVISEDFINALAMOUNTINSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853070','1314501','Expenditure Amount','??? ??????','BUDGET_ADD','EXPENDITUREAMOUNT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853071','1314501','Expenditure Insert By','??? ?????? ??????','BUDGET_ADD','EXPENDITUREINSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853072','1314501','Expenditure InsertionTime','??? ?????','BUDGET_ADD','EXPENDITUREINSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853073','1314501','Revised Expenditure Amount','????? ??? ??????','BUDGET_ADD','REVISEDEXPENDITUREAMOUNT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853074','1314501','Revised Expenditure Insert By','????? ??? ?????? ??????','BUDGET_ADD','REVISEDEXPENDITUREINSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853075','1314501','Revised Expenditure InsertionTime','????? ??? ?????','BUDGET_ADD','REVISEDEXPENDITUREINSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853076','1314501','IsDeleted','????? ?????','BUDGET_ADD','ISDELETED');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853077','1314501','Modified By','????????? ??????','BUDGET_ADD','MODIFIEDBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853078','1314501','LastModificationTime','??? ?????????? ???','BUDGET_ADD','LASTMODIFICATIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853079','1314502','ID','????','BUDGET_EDIT','ID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853080','1314502','Budget Selection Info Id','????? ????? ???? ????','BUDGET_EDIT','BUDGETSELECTIONINFOID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853081','1314502','Budget Mapping Id','????? ????? ?? ????','BUDGET_EDIT','BUDGETMAPPINGID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853082','1314502','Economic Group Id','????????? ?????? ??? ????','BUDGET_EDIT','ECONOMICGROUPID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853083','1314502','Economic Code Id','????????? ??? ????','BUDGET_EDIT','ECONOMICCODEID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853084','1314502','Economic Sub Code Id','????????? ??? ??? ????','BUDGET_EDIT','ECONOMICSUBCODEID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853085','1314502','Budget Office Id','????? ????? ????','BUDGET_EDIT','BUDGETOFFICEID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853086','1314502','Insert By','?????? ??????','BUDGET_EDIT','INSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853087','1314502','Insertion Time','?????? ???','BUDGET_EDIT','INSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853088','1314502','Amount','??????','BUDGET_EDIT','AMOUNT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853089','1314502','Amount Insert By','?????? ?????? ??????','BUDGET_EDIT','AMOUNTINSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853090','1314502','Amount InsertionTime','?????? ?????','BUDGET_EDIT','AMOUNTINSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853091','1314502','Final Amount','??????? ??????','BUDGET_EDIT','FINALAMOUNT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853092','1314502','Final Amount Insert By','??????? ?????? ?????? ??????','BUDGET_EDIT','FINALAMOUNTINSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853093','1314502','Final Amount InsertionTime','??????? ?????? ?????','BUDGET_EDIT','FINALAMOUNTINSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853094','1314502','Revised Amount','????? ??????','BUDGET_EDIT','REVISEDAMOUNT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853095','1314502','Revised Comment','????? ??????? ???','BUDGET_EDIT','REVISEDCOMMENT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853096','1314502','Revised Amount Insert By','????? ?????? ?????? ??????','BUDGET_EDIT','REVISEDAMOUNTINSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853097','1314502','Revised Amount InsertionTime','????? ?????? ?????','BUDGET_EDIT','REVISEDAMOUNTINSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853098','1314502','Revised Final Amount','????? ??????? ??????','BUDGET_EDIT','REVISEDFINALAMOUNT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853099','1314502','Revised Final Comment','????? ??????? ??????? ???','BUDGET_EDIT','REVISEDFINALCOMMENT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853100','1314502','Revised Final Amount Insert By','????? ??????? ?????? ?????? ??????','BUDGET_EDIT','REVISEDFINALAMOUNTINSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853101','1314502','Revised Final Amount InsertionTime','????? ??????? ?????? ?????','BUDGET_EDIT','REVISEDFINALAMOUNTINSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853102','1314502','Expenditure Amount','??? ??????','BUDGET_EDIT','EXPENDITUREAMOUNT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853103','1314502','Expenditure Insert By','??? ?????? ??????','BUDGET_EDIT','EXPENDITUREINSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853104','1314502','Expenditure InsertionTime','??? ?????','BUDGET_EDIT','EXPENDITUREINSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853105','1314502','Revised Expenditure Amount','????? ??? ??????','BUDGET_EDIT','REVISEDEXPENDITUREAMOUNT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853106','1314502','Revised Expenditure Insert By','????? ??? ?????? ??????','BUDGET_EDIT','REVISEDEXPENDITUREINSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853107','1314502','Revised Expenditure InsertionTime','????? ??? ?????','BUDGET_EDIT','REVISEDEXPENDITUREINSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853108','1314502','IsDeleted','????? ?????','BUDGET_EDIT','ISDELETED');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853109','1314502','Modified By','????????? ??????','BUDGET_EDIT','MODIFIEDBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853110','1314502','LastModificationTime','??? ?????????? ???','BUDGET_EDIT','LASTMODIFICATIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853111','1314503','ID','????','BUDGET_SEARCH','ID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853112','1314503','Budget Selection Info Id','????? ????? ???? ????','BUDGET_SEARCH','BUDGETSELECTIONINFOID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853113','1314503','Budget Mapping Id','????? ????? ?? ????','BUDGET_SEARCH','BUDGETMAPPINGID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853114','1314503','Economic Group Id','????????? ?????? ??? ????','BUDGET_SEARCH','ECONOMICGROUPID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853115','1314503','Economic Code Id','????????? ??? ????','BUDGET_SEARCH','ECONOMICCODEID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853116','1314503','Economic Sub Code Id','????????? ??? ??? ????','BUDGET_SEARCH','ECONOMICSUBCODEID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853117','1314503','Budget Office Id','????? ????? ????','BUDGET_SEARCH','BUDGETOFFICEID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853118','1314503','Insert By','?????? ??????','BUDGET_SEARCH','INSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853119','1314503','Insertion Time','?????? ???','BUDGET_SEARCH','INSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853120','1314503','Amount','??????','BUDGET_SEARCH','AMOUNT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853121','1314503','Amount Insert By','?????? ?????? ??????','BUDGET_SEARCH','AMOUNTINSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853122','1314503','Amount InsertionTime','?????? ?????','BUDGET_SEARCH','AMOUNTINSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853123','1314503','Final Amount','??????? ??????','BUDGET_SEARCH','FINALAMOUNT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853124','1314503','Final Amount Insert By','??????? ?????? ?????? ??????','BUDGET_SEARCH','FINALAMOUNTINSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853125','1314503','Final Amount InsertionTime','??????? ?????? ?????','BUDGET_SEARCH','FINALAMOUNTINSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853126','1314503','Revised Amount','????? ??????','BUDGET_SEARCH','REVISEDAMOUNT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853127','1314503','Revised Comment','????? ??????? ???','BUDGET_SEARCH','REVISEDCOMMENT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853128','1314503','Revised Amount Insert By','????? ?????? ?????? ??????','BUDGET_SEARCH','REVISEDAMOUNTINSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853129','1314503','Revised Amount InsertionTime','????? ?????? ?????','BUDGET_SEARCH','REVISEDAMOUNTINSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853130','1314503','Revised Final Amount','????? ??????? ??????','BUDGET_SEARCH','REVISEDFINALAMOUNT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853131','1314503','Revised Final Comment','????? ??????? ??????? ???','BUDGET_SEARCH','REVISEDFINALCOMMENT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853132','1314503','Revised Final Amount Insert By','????? ??????? ?????? ?????? ??????','BUDGET_SEARCH','REVISEDFINALAMOUNTINSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853133','1314503','Revised Final Amount InsertionTime','????? ??????? ?????? ?????','BUDGET_SEARCH','REVISEDFINALAMOUNTINSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853134','1314503','Expenditure Amount','??? ??????','BUDGET_SEARCH','EXPENDITUREAMOUNT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853135','1314503','Expenditure Insert By','??? ?????? ??????','BUDGET_SEARCH','EXPENDITUREINSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853136','1314503','Expenditure InsertionTime','??? ?????','BUDGET_SEARCH','EXPENDITUREINSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853137','1314503','Revised Expenditure Amount','????? ??? ??????','BUDGET_SEARCH','REVISEDEXPENDITUREAMOUNT');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853138','1314503','Revised Expenditure Insert By','????? ??? ?????? ??????','BUDGET_SEARCH','REVISEDEXPENDITUREINSERTBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853139','1314503','Revised Expenditure InsertionTime','????? ??? ?????','BUDGET_SEARCH','REVISEDEXPENDITUREINSERTIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853140','1314503','IsDeleted','????? ?????','BUDGET_SEARCH','ISDELETED');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853141','1314503','Modified By','????????? ??????','BUDGET_SEARCH','MODIFIEDBY');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853142','1314503','LastModificationTime','??? ?????????? ???','BUDGET_SEARCH','LASTMODIFICATIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853143','1314501','BUDGET','?????','BUDGET_ADD','BUDGET_ADD_FORMNAME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853144','1314501','ADD','??? ????','BUDGET_ADD','BUDGET_ADD_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853145','1314501','SUBMIT','?????? ????','BUDGET_ADD','BUDGET_SUBMIT_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853146','1314501','CANCEL','????? ????','BUDGET_ADD','BUDGET_CANCEL_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853147','1314502','BUDGET EDIT','????? ???? ????','BUDGET_EDIT','BUDGET_EDIT_FORMNAME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853148','1314502','SUBMIT','?????? ????','BUDGET_EDIT','BUDGET_SUBMIT_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853149','1314502','CANCEL','????? ????','BUDGET_EDIT','BUDGET_CANCEL_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853150','1314502','English','Bangla','BUDGET_EDIT','LANGUAGE');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853151','1314503','BUDGET SEARCH','????? ??????','BUDGET_SEARCH','BUDGET_SEARCH_FORMNAME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853152','1314503','SEARCH','??????','BUDGET_SEARCH','BUDGET_SEARCH_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853153','1314503','DELETE','????? ????','BUDGET_SEARCH','BUDGET_DELETE_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853154','1314503','EDIT','???? ????','BUDGET_SEARCH','BUDGET_EDIT_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853155','1314503','CANCEL','????? ????','BUDGET_SEARCH','BUDGET_CANCEL_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('1853156','1314503','BUDGET','?????','BUDGET_SEARCH','ANYFIELD');
