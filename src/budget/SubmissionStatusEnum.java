package budget;

public enum SubmissionStatusEnum {
    NOT_SUBMITTED(0),
    SUBMITTED(1),
    NOT_APPLICABLE(-1);
    private final int value;

    SubmissionStatusEnum(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }

}
