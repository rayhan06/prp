package budget;

public enum BudgetCategoryEnum {
    OPERATIONAL(1, "Operational Budget", "পরিচলন বাজেট"),
    DEVELOPMENT(2, "Development Budget", "উন্নয়ন বাজেট"),
    REVENUE(3, "Revenue Budget", "রাজস্ব বাজেট");

    private final int value;
    private final String textEn;
    private final String textBn;

    BudgetCategoryEnum(int value, String textEn, String textBn) {
        this.value = value;
        this.textEn = textEn;
        this.textBn = textBn;
    }

    public int getValue() {
        return value;
    }

    public String getText(String language) {
        return "English".equalsIgnoreCase(language) ? textEn : textBn;
    }
}
