package budget;

import budget_selection_info.BudgetSelectionInfoRepository;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class BudgetRepository implements Repository {
    private static final Logger logger = Logger.getLogger(BudgetSelectionInfoRepository.class);
    private final BudgetDAO budgetDAO;
    private final Map<Long, BudgetDTO> mapById;

    private BudgetRepository() {
        budgetDAO = BudgetDAO.getInstance();
        mapById = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        static final BudgetRepository INSTANCE = new BudgetRepository();
    }

    public static BudgetRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void reload(boolean reloadAll) {
        logger.debug("BudgetRepository reload start for, reloadAll : " + reloadAll);
        List<BudgetDTO> dtoList = budgetDAO.getAllBudget(reloadAll);
        if (dtoList != null && dtoList.size() > 0) {
            dtoList.stream()
                   .peek(this::removeIfPresent)
                   .filter(dto -> dto.isDeleted == 0)
                   .forEach(dto -> mapById.put(dto.iD, dto));
        }
        logger.debug("BudgetRepository reload end for, reloadAll : " + reloadAll);
    }

    private void removeIfPresent(BudgetDTO newDTO) {
        if (newDTO == null) return;
        if (mapById.get(newDTO.iD) != null) {
            mapById.remove(newDTO.iD);
        }
    }

    public List<BudgetDTO> getBudgetList() {
        return new ArrayList<>(mapById.values());
    }

    public BudgetDTO getDTOByID(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "BudgetRepoGDBI")) {
                if (mapById.get(id) == null) {
                    BudgetDTO dto = budgetDAO.getDTOByID(id);
                    if (dto != null) {
                        mapById.put(dto.iD, dto);
                    }
                }
            }
        }
        return mapById.get(id);
    }

    @Override
    public String getTableName() {
        return "budget";
    }

    public Set<Long> getBudgetMappingId(long budgetSelectionInfoId) {
        return mapById.values()
                      .parallelStream()
                      .filter(e -> e.budgetSelectionInfoId == budgetSelectionInfoId)
                      .map(e -> e.budgetMappingId)
                      .collect(Collectors.toSet());
    }
}


