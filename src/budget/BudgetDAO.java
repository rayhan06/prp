package budget;

import budget_mapping.Budget_mappingDTO;
import budget_mapping.Budget_mappingRepository;
import budget_selection_info.BudgetSelectionInfoDTO;
import budget_selection_info.BudgetSelectionInfoRepository;
import common.CommonDTOService;
import common.ConnectionAndStatementUtil;
import common.NameDTO;
import economic_code.Economic_codeDTO;
import economic_code.Economic_codeRepository;
import economic_group.EconomicGroupRepository;
import economic_sub_code.EconomicSubCodeModel;
import economic_sub_code.Economic_sub_codeDTO;
import economic_sub_code.Economic_sub_codeRepository;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import pb.OptionDTO;
import pb.Utils;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;
import util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

import static budget.BudgetUtils.getEconomicYearBeginYear;
import static java.util.stream.Collectors.*;

@SuppressWarnings({"rawtypes", "unchecked", "Duplicates"})
public class BudgetDAO extends NavigationService4 implements CommonDTOService<BudgetDTO> {
    private static final Logger logger = Logger.getLogger(BudgetDAO.class);
    private static final int NOT_APPLICABLE = -1;

    private static final String addQuery = "INSERT INTO {tableName} (budget_selection_info_id,budget_mapping_id,economic_group_id, economic_code_id,economic_sub_code_id,budget_office_id,budget_operation_id,"
            .concat("amount,amount_comment,amount_insert_by,amount_insertionTime,final_amount,final_amount_comment,final_amount_insert_by,final_amount_insertionTime,")
            .concat("revised_amount,revised_comment,revised_amount_insert_by,revised_amount_insertionTime,")
            .concat("revised_final_amount,revised_final_comment,revised_final_amount_insert_by,revised_final_amount_insertionTime,")
            .concat("expenditure_amount,expenditure_insert_by,expenditure_insertionTime,revised_expenditure_amount,revised_expenditure_insert_by,revised_expenditure_insertionTime,")
            .concat("next_year1_projection,next_year1_projection_insert_by,next_year1_projection_insertionTime,next_year2_projection,next_year2_projection_insert_by,next_year2_projection_insertionTime,preparation_comment,")
            .concat("initial_expenditure_amount,modified_by,lastModificationTime,insertion_time,insert_by,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery = "UPDATE {tableName} SET budget_selection_info_id=?,budget_mapping_id=?,economic_group_id=?, economic_code_id=?,economic_sub_code_id=?,budget_office_id=?,budget_operation_id=?,"
            .concat("amount=?,amount_comment=?,amount_insert_by=?,amount_insertionTime=?,final_amount=?,final_amount_comment=?,final_amount_insert_by=?,final_amount_insertionTime=?,")
            .concat("revised_amount=?,revised_comment=?,revised_amount_insert_by=?,revised_amount_insertionTime=?,")
            .concat("revised_final_amount=?,revised_final_comment=?,revised_final_amount_insert_by=?,revised_final_amount_insertionTime=?,")
            .concat("expenditure_amount=?,expenditure_insert_by=?,expenditure_insertionTime=?,revised_expenditure_amount=?,revised_expenditure_insert_by=?,revised_expenditure_insertionTime=?,")
            .concat("next_year1_projection=?,next_year1_projection_insert_by=?,next_year1_projection_insertionTime=?,next_year2_projection=?,next_year2_projection_insert_by=?,next_year2_projection_insertionTime=?,preparation_comment=?,")
            .concat("initial_expenditure_amount=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final String deleteByIds = "UPDATE %s SET isDeleted = 1,modified_by= '%d',lastModificationTime = %d WHERE id IN (%s)";

    private static final String updateAmountById = "UPDATE budget SET amount=%f,final_amount=%f,amount_comment='%s',amount_insert_by=%d,amount_insertionTime=%d,modified_by=%d,lastModificationTime = %d WHERE ID=%d";

    private static final String updateRevisedAmountById = "UPDATE budget SET revised_amount=%f,revised_final_amount=%f,revised_comment='%s',revised_amount_insert_by=%d,revised_amount_insertionTime=%d,modified_by=%d,lastModificationTime = %d WHERE ID=%d";

    private static final String updateFinalAmountById = "UPDATE budget SET final_amount=%f,final_amount_comment='%s',final_amount_insert_by=%d,final_amount_insertionTime=%d,modified_by=%d,lastModificationTime = %d WHERE ID=%d";

    private static final String updateRevisedFinalAmountById = "UPDATE budget SET revised_final_amount=%f,revised_final_comment='%s',revised_final_amount_insert_by=%d,revised_final_amount_insertionTime=%d,modified_by=%d,lastModificationTime = %d WHERE ID=%d";

    private static final String updateExpenditureAmountById = "UPDATE budget SET expenditure_amount=%f,expenditure_insert_by=%d,expenditure_insertionTime=%d,modified_by='%d',lastModificationTime = %d WHERE ID=%d";

    private static final String updateRevisedExpenditureAmountById = "UPDATE budget SET revised_expenditure_amount=%f,revised_expenditure_insert_by=%d,revised_expenditure_insertionTime=%d,modified_by='%d',lastModificationTime = %d WHERE ID=%d";

    private static final String updateProjectedAmountById = "UPDATE budget SET next_year1_projection=%f,next_year1_projection_insert_by=%d,next_year1_projection_insertionTime=%d,"
            .concat("next_year2_projection=%f,next_year2_projection_insert_by=%d,next_year2_projection_insertionTime=%d,preparation_comment='%s',modified_by=%d,lastModificationTime = %d WHERE ID=%d");

    private static final Map<String, String> searchMap = new HashMap<>();


    private BudgetDAO() {
        super("budget");
        useSafeSearch = true;
        searchMap.put("training_type_cat", " and (training_calender.training_type_cat = ?)");
        searchMap.put("budget_selection_info_id", " AND budget_selection_info_id = ? ");
        searchMap.put("budget_selection_info_ids", " AND budget_selection_info_id IN ? ");
        searchMap.put("budget_office_id", " AND budget_office_id = ? ");
        searchMap.put("budget_operation_id", " AND budget_operation_id = ? ");
        searchMap.put("economic_sub_code_id", " AND economic_sub_code_id = ? ");
        searchMap.put("economic_code_id", " AND economic_code_id = ? ");
        searchMap.put("economic_group_id", " AND economic_group_id = ? ");
        searchMap.put("budget_mapping_id", " AND budget_mapping_id = ? ");
        searchMap.put("budget_mapping_ids", " AND budget_mapping_id IN ? ");
    }

    private static class LazyLoader {
        static final BudgetDAO INSTANCE = new BudgetDAO();
    }

    public static BudgetDAO getInstance() {
        return BudgetDAO.LazyLoader.INSTANCE;
    }

    public void setSearchColumn(BudgetDTO budgetDTO) {
        budgetDTO.searchColumn = "";
    }

    @Override
    public void set(PreparedStatement ps, BudgetDTO budgetDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(budgetDTO);
        ps.setLong(++index, budgetDTO.budgetSelectionInfoId);
        ps.setLong(++index, budgetDTO.budgetMappingId);
        ps.setLong(++index, budgetDTO.economicGroupId);
        ps.setLong(++index, budgetDTO.economicCodeId);
        ps.setLong(++index, budgetDTO.economicSubCodeId);
        ps.setLong(++index, budgetDTO.budgetOfficeId);
        ps.setLong(++index, budgetDTO.budgetOperationId);
        ps.setDouble(++index, budgetDTO.amount);
        ps.setString(++index, budgetDTO.amountComment);
        ps.setLong(++index, budgetDTO.amountInsertBy);
        ps.setLong(++index, budgetDTO.amountInsertionTime);
        ps.setDouble(++index, budgetDTO.finalAmount);
        ps.setString(++index, budgetDTO.finalAmountComment);
        ps.setLong(++index, budgetDTO.finalAmountInsertBy);
        ps.setLong(++index, budgetDTO.finalAmountInsertionTime);
        ps.setDouble(++index, budgetDTO.revisedAmount);
        ps.setString(++index, budgetDTO.revisedComment);
        ps.setLong(++index, budgetDTO.revisedAmountInsertBy);
        ps.setLong(++index, budgetDTO.revisedAmountInsertionTime);
        ps.setDouble(++index, budgetDTO.revisedFinalAmount);
        ps.setString(++index, budgetDTO.revisedFinalComment);
        ps.setLong(++index, budgetDTO.revisedFinalAmountInsertBy);
        ps.setLong(++index, budgetDTO.revisedFinalAmountInsertionTime);
        ps.setDouble(++index, budgetDTO.expenditureAmount);
        ps.setLong(++index, budgetDTO.expenditureInsertBy);
        ps.setLong(++index, budgetDTO.expenditureInsertionTime);
        ps.setDouble(++index, budgetDTO.revisedExpenditureAmount);
        ps.setLong(++index, budgetDTO.revisedExpenditureInsertBy);
        ps.setLong(++index, budgetDTO.revisedExpenditureInsertionTime);
        ps.setDouble(++index, budgetDTO.nextYear1Projection);
        ps.setLong(++index, budgetDTO.nextYear1ProjectionInsertBy);
        ps.setLong(++index, budgetDTO.nextYear1ProjectionInsertionTime);
        ps.setDouble(++index, budgetDTO.nextYear2Projection);
        ps.setLong(++index, budgetDTO.nextYear2ProjectionInsertBy);
        ps.setLong(++index, budgetDTO.nextYear2ProjectionInsertionTime);
        ps.setString(++index, budgetDTO.preparationComment);
        ps.setDouble(++index, budgetDTO.initialExpenditureAmount);
        ps.setLong(++index, budgetDTO.modifiedBy);
        ps.setLong(++index, budgetDTO.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, budgetDTO.insertionTime);
            ps.setLong(++index, budgetDTO.insertBy);
            ps.setInt(++index, 0);
        }
        ps.setLong(++index, budgetDTO.iD);
    }

    @Override
    public BudgetDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            BudgetDTO budgetDTO = new BudgetDTO();
            budgetDTO.iD = rs.getLong("ID");
            budgetDTO.budgetSelectionInfoId = rs.getLong("budget_selection_info_id");
            budgetDTO.budgetMappingId = rs.getLong("budget_mapping_id");
            budgetDTO.economicGroupId = rs.getLong("economic_group_id");
            budgetDTO.economicCodeId = rs.getLong("economic_code_id");
            budgetDTO.economicSubCodeId = rs.getLong("economic_sub_code_id");
            budgetDTO.budgetOfficeId = rs.getLong("budget_office_id");
            budgetDTO.budgetOperationId = rs.getLong("budget_operation_id");
            budgetDTO.insertBy = rs.getLong("insert_by");
            budgetDTO.insertionTime = rs.getLong("insertion_time");
            budgetDTO.amount = rs.getDouble("amount");
            budgetDTO.amountComment = rs.getString("amount_comment");
            budgetDTO.amountInsertBy = rs.getLong("amount_insert_by");
            budgetDTO.amountInsertionTime = rs.getLong("amount_insertionTime");
            budgetDTO.finalAmount = rs.getDouble("final_amount");
            budgetDTO.finalAmountComment = rs.getString("final_amount_comment");
            budgetDTO.finalAmountInsertBy = rs.getLong("final_amount_insert_by");
            budgetDTO.finalAmountInsertionTime = rs.getLong("final_amount_insertionTime");
            budgetDTO.revisedAmount = rs.getDouble("revised_amount");
            budgetDTO.revisedComment = rs.getString("revised_comment");
            budgetDTO.revisedAmountInsertBy = rs.getLong("revised_amount_insert_by");
            budgetDTO.revisedAmountInsertionTime = rs.getLong("revised_amount_insertionTime");
            budgetDTO.revisedFinalAmount = rs.getDouble("revised_final_amount");
            budgetDTO.revisedFinalComment = rs.getString("revised_final_comment");
            budgetDTO.revisedFinalAmountInsertBy = rs.getLong("revised_final_amount_insert_by");
            budgetDTO.revisedFinalAmountInsertionTime = rs.getLong("revised_final_amount_insertionTime");
            budgetDTO.initialExpenditureAmount = rs.getDouble("initial_expenditure_amount");
            budgetDTO.expenditureAmount = rs.getDouble("expenditure_amount");
            budgetDTO.expenditureInsertBy = rs.getLong("expenditure_insert_by");
            budgetDTO.expenditureInsertionTime = rs.getLong("expenditure_insertionTime");
            budgetDTO.revisedExpenditureAmount = rs.getDouble("revised_expenditure_amount");
            budgetDTO.revisedExpenditureInsertBy = rs.getLong("revised_expenditure_insert_by");
            budgetDTO.revisedExpenditureInsertionTime = rs.getLong("revised_expenditure_insertionTime");
            budgetDTO.nextYear1Projection = rs.getDouble("next_year1_projection");
            budgetDTO.nextYear1ProjectionInsertBy = rs.getLong("next_year1_projection_insert_by");
            budgetDTO.nextYear1ProjectionInsertionTime = rs.getLong("next_year1_projection_insertionTime");
            budgetDTO.nextYear2Projection = rs.getDouble("next_year2_projection");
            budgetDTO.nextYear2ProjectionInsertBy = rs.getLong("next_year2_projection_insert_by");
            budgetDTO.nextYear2ProjectionInsertionTime = rs.getLong("next_year2_projection_insertionTime");
            budgetDTO.preparationComment = rs.getString("preparation_comment");
            budgetDTO.isDeleted = rs.getInt("isDeleted");
            budgetDTO.modifiedBy = rs.getLong("modified_by");
            budgetDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return budgetDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((BudgetDTO) commonDTO, updateQuery, false);
    }

    public List<BudgetDTO> getDTOsBySelectionInfo(long budgetSelectionInfoId) {
        Hashtable<String, String> criteriaMap = new Hashtable<>();
        criteriaMap.put("budget_selection_info_id", String.valueOf(budgetSelectionInfoId));

        List<Object> objectList = new ArrayList<>();
        String sql = getSearchQuery(criteriaMap, objectList);
        return getDTOs(sql, objectList);
    }

    public List<BudgetDTO> getAllBudget(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((BudgetDTO) commonDTO, addQuery, true);
    }

    public BudgetDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }

    public List<BudgetDTO> getDTOsBySelectionInfoAndBudgetMapping(long budgetSelectionInfoId, long budgetMappingId) {
        Hashtable<String, String> criteriaMap = new Hashtable<>();
        criteriaMap.put("budget_selection_info_id", String.valueOf(budgetSelectionInfoId));
        criteriaMap.put("budget_mapping_id", String.valueOf(budgetMappingId));

        List<Object> objectList = new ArrayList<>();
        String sql = getSearchQuery(criteriaMap, objectList);
        return getDTOs(sql, objectList);
    }

    public BudgetDTO getDTOByBudgetMappingAndSubCode(long budgetSelectionInfoId,
                                                     long budgetMappingId,
                                                     long economicSubCodeId) {
        Hashtable<String, String> criteriaMap = new Hashtable<>();
        criteriaMap.put("budget_selection_info_id", String.valueOf(budgetSelectionInfoId));
        criteriaMap.put("budget_mapping_id", String.valueOf(budgetMappingId));
        criteriaMap.put("economic_sub_code_id", String.valueOf(economicSubCodeId));
        List<Object> objectList = new ArrayList<>();
        String sql = getSearchQuery(criteriaMap, objectList);
        return getDTOs(sql, objectList)
                .stream()
                .findAny()
                .orElse(null);
    }

    public List<BudgetDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }


    public List<BudgetDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable,
                                   UserDTO userDTO, String filter, boolean tableHasJobCat) {
        List<Object> objectList = new ArrayList<>();
        String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat, objectList);
        return getDTOs(sql, objectList);
    }

    public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                          UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList) {
        limit = NOT_APPLICABLE;
        offset = NOT_APPLICABLE;
        if (p_searchCriteria.get("budget_selection_info_id") == null || p_searchCriteria.get("budget_selection_info_id") == "") {
            Long currentSelectionId = BudgetSelectionInfoRepository.getInstance().getRunningYearSelectionId();
            if (currentSelectionId != null) {
                p_searchCriteria.put("budget_selection_info_id", String.valueOf(currentSelectionId));
            }
        }
        if ((p_searchCriteria.get("budget_institional_group_id") != null && p_searchCriteria.get("budget_institional_group_id") != "") ||
            (p_searchCriteria.get("budget_cat") != null && p_searchCriteria.get("budget_cat") != "")) {
            String searchBudgetMapIds = getSearchMappingIds(p_searchCriteria).stream()
                                                                             .map(String::valueOf)
                                                                             .collect(joining(","));
            logger.debug(searchBudgetMapIds);
            p_searchCriteria.put("budget_mapping_ids", searchBudgetMapIds);
            if (searchBudgetMapIds.length() == 0) {
                p_searchCriteria.put("budget_mapping_ids", "-1");
            }
        }
        return getSearchQuery(tableName, p_searchCriteria, limit, offset, category, searchMap, objectList);
    }

    private List<Long> getSearchMappingIds(Hashtable p_searchCriteria) {
        List<Budget_mappingDTO> allMappingDTOs = Budget_mappingRepository.getInstance().getBudgetMappingList();
        if (p_searchCriteria.get("budget_institional_group_id") != null && p_searchCriteria.get("budget_institional_group_id") != "") {
            long paramGroupId = Long.parseLong((String) p_searchCriteria.get("budget_institional_group_id"));
            allMappingDTOs = allMappingDTOs.stream()
                                           .filter(dto -> dto.budgetInstitutionalGroupId == paramGroupId)
                                           .collect(toList());
        }
        if (p_searchCriteria.get("budget_cat") != null && p_searchCriteria.get("budget_cat") != "") {
            long paramBudgetCat = Long.parseLong((String) p_searchCriteria.get("budget_cat"));
            allMappingDTOs = allMappingDTOs.stream()
                                           .filter(dto -> dto.budgetCat == paramBudgetCat)
                                           .collect(toList());
        }
        return allMappingDTOs.stream().map(dto -> dto.iD).collect(toList());
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
                                           boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList) {
        return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat, objectList);
    }

    public void deleteByIds(List<Long> idList, long requestBy, long requestTime) {
        String ids = idList.stream().map(String::valueOf).collect(joining(","));
        String sql = String.format(deleteByIds, getTableName(), requestBy, requestTime, ids);
        logger.debug("sql : " + sql);
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            try {
                st.executeUpdate(sql);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
    }

    public Map<Boolean, List<EconomicSubCodeModel>> deleteDeletedInfoAndReturnAMap(List<EconomicSubCodeModel> modelList, long budgetSelectionInfoId, long budgetMappingId, long requestBy, long requestTime) {
        List<BudgetDTO> oldDTOList = getDTOsBySelectionInfoAndBudgetMapping(budgetSelectionInfoId, budgetMappingId);
        if (modelList == null || modelList.size() == 0) {
            if (oldDTOList != null && oldDTOList.size() > 0) {
                List<Long> ids = oldDTOList.stream()
                                           .map(e -> e.iD)
                                           .collect(toList());
                deleteByIds(ids, requestBy, requestTime);
            }
            return new HashMap<>();
        } else if (oldDTOList == null || oldDTOList.size() == 0) {
            Map<Boolean, List<EconomicSubCodeModel>> booleanListMap = new HashMap<>();
            booleanListMap.put(false, modelList);
            return booleanListMap;
        } else {
            Map<Long, Long> map = oldDTOList.stream()
                                            .collect(toMap(dto -> dto.economicSubCodeId, dto -> dto.iD));
            Set<Long> oldSet = map.keySet();

            Set<Long> latestSet = modelList.stream()
                                           .map(model -> Long.parseLong(model.id))
                                           .collect(toSet());

            List<Long> deletedIds = oldDTOList.stream()
                                              .filter(dto -> !latestSet.contains(dto.economicSubCodeId))
                                              .map(dto -> map.get(dto.economicSubCodeId))
                                              .collect(toList());
            deleteByIds(deletedIds, requestBy, requestTime);
            return modelList.stream()
                            .collect(partitioningBy(
                                    dto -> oldSet.contains(Long.parseLong(dto.id)),
                                    mapping(e -> e, toList())
                            ));
        }
    }

    public void insertSelectedCodeList(List<EconomicSubCodeModel> modelList, long budgetSelectionInfoId, long budgetMappingId, long requestBy, long requestTime) {
        modelList.forEach(model -> {
            try {
                BudgetDTO budgetDTO = new BudgetDTO();
                budgetDTO.budgetSelectionInfoId = budgetSelectionInfoId;
                budgetDTO.budgetMappingId = budgetMappingId;
                budgetDTO.economicGroupId = Long.parseLong(model.economicGroupId);
                budgetDTO.economicCodeId = Long.parseLong(model.economicCodeId);
                budgetDTO.economicSubCodeId = Long.parseLong(model.id);

                Budget_mappingDTO mappingDTO = Budget_mappingRepository.getInstance().getById(budgetMappingId);
                budgetDTO.budgetOfficeId = mappingDTO.budgetOfficeId;
                budgetDTO.budgetOperationId = mappingDTO.budgetOperationId;
                budgetDTO.insertBy = requestBy;
                budgetDTO.insertionTime = requestTime;
                budgetDTO.lastModificationTime = requestTime;
                add(budgetDTO);
            } catch (Exception ex) {
                logger.error(ex);
            }
        });
    }

    public List<EconomicSubCodeModel> getEconomicSubCodeModels(String language, String economicYear, long budgetMappingId) {
        BudgetSelectionInfoDTO selectionInfoDTO = BudgetSelectionInfoRepository.getInstance().getDTOByEconomicYear(economicYear);
        List<BudgetDTO> budgetDTOs = null;

        if (selectionInfoDTO != null)
            budgetDTOs = getDTOsBySelectionInfoAndBudgetMapping(selectionInfoDTO.iD, budgetMappingId);

        if (budgetDTOs == null || budgetDTOs.size() == 0) return new ArrayList<>();

        return budgetDTOs.stream()
                         .map(budgetDTO -> budgetDTO.economicSubCodeId)
                         .map(subCodeId -> Economic_sub_codeRepository.getInstance().getDTOByID(subCodeId))
                         .map(subCodeDTO -> new EconomicSubCodeModel(subCodeDTO, language))
                         .collect(toList());
    }

    public List<BudgetDTO> getDTOsByBudgetOffice(long budgetSelectionInfoId, long budgetOfficeId) {
        Hashtable<String, String> criteriaMap = new Hashtable<>();
        criteriaMap.put("budget_selection_info_id", String.valueOf(budgetSelectionInfoId));
        criteriaMap.put("budget_office_id", String.valueOf(budgetOfficeId));

        List<Object> objectList = new ArrayList<>();
        String sql = getSearchQuery(criteriaMap, objectList);
        return getDTOs(sql, objectList);
    }

    public List<BudgetDTO> getByBudgetOfficeAndOperationId(long budgetSelectionInfoId, long budgetOfficeId, long budgetOperationId) {
        Hashtable<String, String> criteriaMap = new Hashtable<>();
        criteriaMap.put("budget_selection_info_id", String.valueOf(budgetSelectionInfoId));
        criteriaMap.put("budget_office_id", String.valueOf(budgetOfficeId));
        criteriaMap.put("budget_operation_id", String.valueOf(budgetOperationId));

        List<Object> objectList = new ArrayList<>();
        String sql = getSearchQuery(criteriaMap, objectList);
        return getDTOs(sql, objectList);
    }

    public List<BudgetDTO> getByMappingId(long budgetSelectionInfoId, long budgetMappingId) {
        Hashtable<String, String> criteriaMap = new Hashtable<>();
        criteriaMap.put("budget_selection_info_id", String.valueOf(budgetSelectionInfoId));
        criteriaMap.put("budget_mapping_id", String.valueOf(budgetMappingId));

        List<Object> objectList = new ArrayList<>();
        String sql = getSearchQuery(criteriaMap, objectList);
        return getDTOs(sql, objectList);
    }

    public List<BudgetDTO> getByMappingIdAndEconomicGroup(long budgetSelectionInfoId,
                                                          long budgetMappingId,
                                                          long economicGroupId) {
        Hashtable<String, String> criteriaMap = new Hashtable<>();
        criteriaMap.put("budget_selection_info_id", String.valueOf(budgetSelectionInfoId));
        criteriaMap.put("budget_mapping_id", String.valueOf(budgetMappingId));
        criteriaMap.put("economic_group_id", String.valueOf(economicGroupId));

        List<Object> objectList = new ArrayList<>();
        String sql = getSearchQuery(criteriaMap, objectList);
        return getDTOs(sql, objectList);
    }

    public List<BudgetDTO> getByMappingIdAndEconomicCode(long budgetSelectionInfoId,
                                                         long budgetMappingId,
                                                         long economicCodeId) {
        Hashtable<String, String> criteriaMap = new Hashtable<>();
        criteriaMap.put("budget_selection_info_id", String.valueOf(budgetSelectionInfoId));
        criteriaMap.put("budget_mapping_id", String.valueOf(budgetMappingId));
        criteriaMap.put("economic_code_id", String.valueOf(economicCodeId));

        List<Object> objectList = new ArrayList<>();
        String sql = getSearchQuery(criteriaMap, objectList);
        return getDTOs(sql, objectList);
    }

    public List<BudgetDTO> getByBudgetOfficeAndOperationAndEconomicGroup(long budgetSelectionInfoId, long budgetOfficeId, long budgetOperationId, long economicGroupId) {
        Hashtable<String, String> criteriaMap = new Hashtable<>();
        criteriaMap.put("budget_selection_info_id", String.valueOf(budgetSelectionInfoId));
        criteriaMap.put("budget_office_id", String.valueOf(budgetOfficeId));
        criteriaMap.put("budget_operation_id", String.valueOf(budgetOperationId));
        criteriaMap.put("economic_group_id", String.valueOf(economicGroupId));

        List<Object> objectList = new ArrayList<>();
        String sql = getSearchQuery(criteriaMap, objectList);
        return getDTOs(sql, objectList);
    }

    public List<BudgetDTO> getByBudgetOfficeAndOperationAndEconomicCode(long budgetSelectionInfoId, long budgetOfficeId, long budgetOperationId, long economicCodeId) {
        Hashtable<String, String> criteriaMap = new Hashtable<>();
        criteriaMap.put("budget_selection_info_id", String.valueOf(budgetSelectionInfoId));
        criteriaMap.put("budget_office_id", String.valueOf(budgetOfficeId));
        criteriaMap.put("budget_operation_id", String.valueOf(budgetOperationId));
        criteriaMap.put("economic_code_id", String.valueOf(economicCodeId));

        List<Object> objectList = new ArrayList<>();
        String sql = getSearchQuery(criteriaMap, objectList);
        return getDTOs(sql, objectList);
    }

    public List<BudgetDTO> getByBudgetOfficeAndOperationAndCodeAndSelectionIds(long budgetOfficeId, long budgetOperationId,
                                                                               Long economicGroupId, Long economicCodeId,
                                                                               List<Long> selectionInfoIds) {
        Hashtable<String, String> criteriaMap = new Hashtable<>();
        criteriaMap.put("budget_selection_info_ids", selectionInfoIds.stream()
                                                                     .map(String::valueOf)
                                                                     .collect(joining(",")));
        criteriaMap.put("budget_office_id", String.valueOf(budgetOfficeId));
        criteriaMap.put("budget_operation_id", String.valueOf(budgetOperationId));

        if (economicGroupId != null) {
            criteriaMap.put("economic_group_id", String.valueOf(economicGroupId));
        }
        if (economicCodeId != null) {
            criteriaMap.put("economic_code_id", String.valueOf(economicCodeId));
        }
        List<Object> objectList = new ArrayList<>();
        String sql = getSearchQuery(criteriaMap, objectList);
        return getDTOs(sql, objectList);
    }

    public String buildEconomicGroups(String language, Long selectedId, List<BudgetDTO> budgetDTOS) {
        List<OptionDTO> optionDTOList =
                budgetDTOS.stream()
                          .map(dto -> dto.economicGroupId)
                          .distinct()
                          .map(groupId -> EconomicGroupRepository.getInstance().getDTOByID(groupId))
                          .sorted(Comparator.comparingLong(dto -> dto.iD))
                          .map(this::buildOptionDTO)
                          .collect(toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String buildEconomicCodes(String language, Long selectedId, List<BudgetDTO> budgetDTOS) {
        List<OptionDTO> optionDTOList =
                budgetDTOS.stream()
                          .map(dto -> dto.economicCodeId)
                          .distinct()
                          .map(codeId -> Economic_codeRepository.getInstance().getById(codeId))
                          .sorted(Comparator.comparing(dto -> dto.code))
                          .map(Economic_codeDTO::getOptionDTO)
                          .collect(toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String buildEconomicSubCodes(String language, Long selectedId, List<BudgetDTO> budgetDTOS) {
        List<OptionDTO> optionDTOList =
                budgetDTOS.stream()
                          .map(dto -> dto.economicSubCodeId)
                          .distinct()
                          .map(codeId -> Economic_sub_codeRepository.getInstance().getDTOByID(codeId))
                          .sorted(Comparator.comparing(dto -> dto.code))
                          .map(Economic_sub_codeDTO::getOptionDTO)
                          .collect(toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String buildBudgetTypeForExpenditure(String language, long budgetSelectionId, Long selectedId) throws ParseException {
        CatRepository catRepository = CatRepository.getInstance();
        List<CategoryLanguageModel> budgetTypeModels = new ArrayList<>();
        String economicYear = BudgetSelectionInfoRepository.getInstance().getDTOByID(budgetSelectionId).economicYear;
        long currentTime = System.currentTimeMillis();
        if (BudgetUtils.isExpenditureAvailableByYear(economicYear, currentTime)) {
            budgetTypeModels.add(catRepository.getCategoryLanguageModel("budget_type", BudgetTypeEnum.BUDGET.getValue()));
        }
        if (BudgetUtils.isRevisedExpenditureAvailableByYear(economicYear, currentTime)) {
            budgetTypeModels.add(catRepository.getCategoryLanguageModel("budget_type", BudgetTypeEnum.REVISED_BUDGET.getValue()));
        }
        List<OptionDTO> optionDTOList = budgetTypeModels.stream()
                                                        .map(dto -> new OptionDTO(dto.englishText, dto.banglaText, String.valueOf(dto.categoryValue)))
                                                        .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    private OptionDTO buildOptionDTO(NameDTO economicGroupType) {
        String textEn = String.valueOf(economicGroupType.iD).concat(" - ").concat(economicGroupType.nameEn);
        String textBn = StringUtils.convertToBanNumber(String.valueOf(economicGroupType.iD)).concat(" - ").concat(economicGroupType.nameBn);
        return new OptionDTO(textEn, textBn, String.valueOf(economicGroupType.iD));
    }

    public void updateAmountById(BudgetModel budgetModel, long requestBy, long requestTime) {
        Double amount = Double.valueOf(budgetModel.budgetAmount);
        if (amount.isInfinite() || amount.isNaN() || amount < .0) {
            logger.error("Invalid Budget Amount");
            return;
        }
        String cleanedComment = Utils.cleanAndGetOptionalString(budgetModel.budgetComment);
        String updateSql = String.format(updateAmountById, amount, amount, cleanedComment, requestBy, requestTime, requestBy, requestTime, Long.valueOf(budgetModel.id));
        ConnectionAndStatementUtil.getWriteStatement(e -> {
            try {
                e.executeUpdate(updateSql);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
    }

    public void updateRevisedAmountById(BudgetModel budgetModel, long requestBy, long requestTime) {
        Double revisedAmount = Double.valueOf(budgetModel.revisedBudgetAmount);
        if (revisedAmount.isInfinite() || revisedAmount.isNaN() || revisedAmount < .0) {
            logger.error("Invalid Budget Amount");
            return;
        }
        String cleanedComment = Utils.cleanAndGetOptionalString(budgetModel.revisedBudgetComment);
        String updateSql = String.format(updateRevisedAmountById, revisedAmount, revisedAmount, cleanedComment, requestBy, requestTime, requestBy, requestTime, Long.valueOf(budgetModel.id));
        ConnectionAndStatementUtil.getWriteStatement(e -> {
            try {
                e.executeUpdate(updateSql);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
    }

    public void updateFinalAmountById(BudgetModel budgetModel, long requestBy, long requestTime) {
        String updateSql = String.format(updateFinalAmountById, Double.valueOf(budgetModel.finalBudgetAmount), budgetModel.finalBudgetComment, requestBy, requestTime, requestBy, requestTime, Long.valueOf(budgetModel.id));
        logger.debug(updateSql);
        ConnectionAndStatementUtil.getWriteStatement(e -> {
            try {
                e.executeUpdate(updateSql);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
    }

    public void updateExpenditureAmountById(BudgetModel budgetModel, int budgetTypeCat, long requestBy, long requestTime) {
        String updateSql;
        if (budgetTypeCat == BudgetTypeEnum.BUDGET.getValue()) {
            updateSql = String.format(updateExpenditureAmountById, Double.valueOf(budgetModel.expenditureAmount), requestBy, requestTime, requestBy, requestTime, Long.valueOf(budgetModel.id));
        } else {
            updateSql = String.format(updateRevisedExpenditureAmountById, Double.valueOf(budgetModel.revisedExpenditureAmount), requestBy, requestTime, requestBy, requestTime, Long.valueOf(budgetModel.id));
        }
        logger.debug(updateSql);
        ConnectionAndStatementUtil.getWriteStatement(e -> {
            try {
                e.executeUpdate(updateSql);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
    }

    public void updateRevisedFinalAmountById(BudgetModel budgetModel, long requestBy, long requestTime) {
        String updateSql = String.format(updateRevisedFinalAmountById, Double.valueOf(budgetModel.revisedFinalBudgetAmount), budgetModel.revisedFinalBudgetComment, requestBy, requestTime, requestBy, requestTime, Long.valueOf(budgetModel.id));
        logger.debug(updateSql);
        ConnectionAndStatementUtil.getWriteStatement(e -> {
            try {
                e.executeUpdate(updateSql);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
    }

    public void updateProjectionById(BudgetModel budgetModel, long requestBy, long requestTime) {
        String updateSql = String.format(updateProjectedAmountById,
                Double.valueOf(budgetModel.projection1), requestBy, requestTime,
                Double.valueOf(budgetModel.projection2), requestBy, requestTime,
                budgetModel.preparationComment,
                requestBy, requestTime, Long.valueOf(budgetModel.id)
        );
        logger.debug(updateSql);
        ConnectionAndStatementUtil.getWriteStatement(e -> {
            try {
                e.executeUpdate(updateSql);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
    }

    private String getSearchQuery(Hashtable<String, String> criteriaMap, List<Object> outObjectList) {
        return getSearchQuery(
                tableName, criteriaMap, NOT_APPLICABLE, NOT_APPLICABLE,
                NavigationService4.GETDTOS, searchMap, outObjectList
        );
    }

    public List<BudgetDTO> getDTOsBySelectionInfoIds(List<Long> budgetSelectionInfoIds) {
        Hashtable<String, String> criteriaMap = new Hashtable<>();
        criteriaMap.put(
                "budget_selection_info_ids",
                budgetSelectionInfoIds.stream()
                                      .filter(Objects::nonNull)
                                      .map(String::valueOf)
                                      .collect(joining(","))
        );

        List<Object> objectList = new ArrayList<>();
        String sql = getSearchQuery(criteriaMap, objectList);
        return getDTOs(sql, objectList);
    }

    public Set<Long> getAllOffices(long selectionInfoId) {
        return getDTOsBySelectionInfo(selectionInfoId)
                .stream()
                .map(budgetDTO -> budgetDTO.budgetOfficeId)
                .collect(toSet());
    }

    public static long BUDGET_AMOUNT_UNIT = 1000L;

    public long getAllocatedBudget(long timeInMillis, long budgetMappingId, long economicSubCodeId) {
        String economicYear = BudgetInfo.getEconomicYear(getEconomicYearBeginYear(timeInMillis));
        Long selectionId = BudgetSelectionInfoRepository.getInstance().getId(economicYear);
        return getAllocatedBudget(timeInMillis, selectionId, budgetMappingId, economicSubCodeId).allocatedBudget;
    }

    public BudgetAmountModel getAllocatedBudget(long timeInMillis, Long budgetSelectionInfoId, long budgetMappingId, long economicSubCodeId) {
        BudgetAmountModel emptyInstance = BudgetAmountModel.getEmptyInstance();
        if (budgetSelectionInfoId == null) {
            return emptyInstance;
        }
        BudgetDTO budgetDTO = getDTOByBudgetMappingAndSubCode(
                budgetSelectionInfoId,
                budgetMappingId,
                economicSubCodeId
        );
        if (budgetDTO == null) {
            return emptyInstance;
        }
        return new BudgetAmountModel(budgetDTO, timeInMillis);
    }
}
	