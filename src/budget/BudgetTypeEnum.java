package budget;

public enum BudgetTypeEnum {
    BUDGET(1, "Budget", "বাজেট"),
    REVISED_BUDGET(2, "Revised Budget", "সংশোধিত বাজেট");

    private final int value;
    private final String textEn;
    private final String textBn;

    BudgetTypeEnum(int value, String textEn, String textBn) {
        this.value = value;
        this.textEn = textEn;
        this.textBn = textBn;
    }

    public int getValue() {
        return value;
    }

    public String getText(String language) {
        return "English".equalsIgnoreCase(language) ? textEn : textBn;
    }
}
