package budget;

import sessionmanager.SessionConstants;
import util.CommonDTO;


public class BudgetDTO extends CommonDTO {
    public long budgetSelectionInfoId = 0;
    public long budgetMappingId = 0;
    public long economicGroupId = 0;
    public long economicCodeId = 0;
    public long economicSubCodeId = 0;
    public long budgetOfficeId = 0;
    public long budgetOperationId = -1;
    public long insertBy = 0;
    public long insertionTime = SessionConstants.MIN_DATE;
    public double amount = 0;
    public String amountComment = "";
    public long amountInsertBy = 0;
    public long amountInsertionTime = SessionConstants.MIN_DATE;
    public double finalAmount = 0;
    public String finalAmountComment = "";
    public long finalAmountInsertBy = 0;
    public long finalAmountInsertionTime = SessionConstants.MIN_DATE;
    public double revisedAmount = 0;
    public String revisedComment = "";
    public long revisedAmountInsertBy = 0;
    public long revisedAmountInsertionTime = SessionConstants.MIN_DATE;
    public double revisedFinalAmount = 0;
    public String revisedFinalComment = "";
    public long revisedFinalAmountInsertBy = 0;
    public long revisedFinalAmountInsertionTime = SessionConstants.MIN_DATE;
    public double expenditureAmount = 0;
    public long expenditureInsertBy = 0;
    public long expenditureInsertionTime = SessionConstants.MIN_DATE;
    public double revisedExpenditureAmount = 0;
    public long revisedExpenditureInsertBy = 0;
    public long revisedExpenditureInsertionTime = SessionConstants.MIN_DATE;
    public double initialExpenditureAmount = 0;
    public long modifiedBy = 0;
    public double nextYear1Projection = 0;
    public long nextYear1ProjectionInsertBy = 0;
    public long nextYear1ProjectionInsertionTime = SessionConstants.MIN_DATE;
    public double nextYear2Projection = 0;
    public long nextYear2ProjectionInsertBy = 0;
    public long nextYear2ProjectionInsertionTime = SessionConstants.MIN_DATE;
    public String preparationComment = "";

    @Override
    public String toString() {
        return "$BudgetDTO[" +
               " iD = " + iD +
               " budgetSelectionInfoId = " + budgetSelectionInfoId +
               " budgetMappingId = " + budgetMappingId +
               " economicGroupId = " + economicGroupId +
               " economicCodeId = " + economicCodeId +
               " economicSubCodeId = " + economicSubCodeId +
               " budgetOfficeId = " + budgetOfficeId +
               " insertBy = " + insertBy +
               " insertionTime = " + insertionTime +
               " amount = " + amount +
               " amountInsertBy = " + amountInsertBy +
               " amountInsertionTime = " + amountInsertionTime +
               " finalAmount = " + finalAmount +
               " finalAmountInsertBy = " + finalAmountInsertBy +
               " finalAmountInsertionTime = " + finalAmountInsertionTime +
               " revisedAmount = " + revisedAmount +
               " revisedComment = " + revisedComment +
               " revisedAmountInsertBy = " + revisedAmountInsertBy +
               " revisedAmountInsertionTime = " + revisedAmountInsertionTime +
               " revisedFinalAmount = " + revisedFinalAmount +
               " revisedFinalComment = " + revisedFinalComment +
               " revisedFinalAmountInsertBy = " + revisedFinalAmountInsertBy +
               " revisedFinalAmountInsertionTime = " + revisedFinalAmountInsertionTime +
               " expenditureAmount = " + expenditureAmount +
               " expenditureInsertBy = " + expenditureInsertBy +
               " expenditureInsertionTime = " + expenditureInsertionTime +
               " revisedExpenditureAmount = " + revisedExpenditureAmount +
               " revisedExpenditureInsertBy = " + revisedExpenditureInsertBy +
               " revisedExpenditureInsertionTime = " + revisedExpenditureInsertionTime +
               " isDeleted = " + isDeleted +
               " modifiedBy = " + modifiedBy +
               " lastModificationTime = " + lastModificationTime +
               "]";
    }
}