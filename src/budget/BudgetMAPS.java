package budget;
import java.util.*; 
import util.*;


public class BudgetMAPS extends CommonMaps
{	
	public BudgetMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("budgetSelectionInfoId".toLowerCase(), "budgetSelectionInfoId".toLowerCase());
		java_DTO_map.put("budgetMappingId".toLowerCase(), "budgetMappingId".toLowerCase());
		java_DTO_map.put("economicGroupId".toLowerCase(), "economicGroupId".toLowerCase());
		java_DTO_map.put("economicCodeId".toLowerCase(), "economicCodeId".toLowerCase());
		java_DTO_map.put("economicSubCodeId".toLowerCase(), "economicSubCodeId".toLowerCase());
		java_DTO_map.put("budgetOfficeId".toLowerCase(), "budgetOfficeId".toLowerCase());
		java_DTO_map.put("insertBy".toLowerCase(), "insertBy".toLowerCase());
		java_DTO_map.put("insertionTime".toLowerCase(), "insertionTime".toLowerCase());
		java_DTO_map.put("amount".toLowerCase(), "amount".toLowerCase());
		java_DTO_map.put("amountInsertBy".toLowerCase(), "amountInsertBy".toLowerCase());
		java_DTO_map.put("amountInsertionTime".toLowerCase(), "amountInsertionTime".toLowerCase());
		java_DTO_map.put("finalAmount".toLowerCase(), "finalAmount".toLowerCase());
		java_DTO_map.put("finalAmountInsertBy".toLowerCase(), "finalAmountInsertBy".toLowerCase());
		java_DTO_map.put("finalAmountInsertionTime".toLowerCase(), "finalAmountInsertionTime".toLowerCase());
		java_DTO_map.put("revisedAmount".toLowerCase(), "revisedAmount".toLowerCase());
		java_DTO_map.put("revisedComment".toLowerCase(), "revisedComment".toLowerCase());
		java_DTO_map.put("revisedAmountInsertBy".toLowerCase(), "revisedAmountInsertBy".toLowerCase());
		java_DTO_map.put("revisedAmountInsertionTime".toLowerCase(), "revisedAmountInsertionTime".toLowerCase());
		java_DTO_map.put("revisedFinalAmount".toLowerCase(), "revisedFinalAmount".toLowerCase());
		java_DTO_map.put("revisedFinalComment".toLowerCase(), "revisedFinalComment".toLowerCase());
		java_DTO_map.put("revisedFinalAmountInsertBy".toLowerCase(), "revisedFinalAmountInsertBy".toLowerCase());
		java_DTO_map.put("revisedFinalAmountInsertionTime".toLowerCase(), "revisedFinalAmountInsertionTime".toLowerCase());
		java_DTO_map.put("expenditureAmount".toLowerCase(), "expenditureAmount".toLowerCase());
		java_DTO_map.put("expenditureInsertBy".toLowerCase(), "expenditureInsertBy".toLowerCase());
		java_DTO_map.put("expenditureInsertionTime".toLowerCase(), "expenditureInsertionTime".toLowerCase());
		java_DTO_map.put("revisedExpenditureAmount".toLowerCase(), "revisedExpenditureAmount".toLowerCase());
		java_DTO_map.put("revisedExpenditureInsertBy".toLowerCase(), "revisedExpenditureInsertBy".toLowerCase());
		java_DTO_map.put("revisedExpenditureInsertionTime".toLowerCase(), "revisedExpenditureInsertionTime".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("amount".toLowerCase(), "amount".toLowerCase());
		java_SQL_map.put("amount_insert_by".toLowerCase(), "amountInsertBy".toLowerCase());
		java_SQL_map.put("amount_insertionTime".toLowerCase(), "amountInsertionTime".toLowerCase());
		java_SQL_map.put("final_amount".toLowerCase(), "finalAmount".toLowerCase());
		java_SQL_map.put("final_amount_insert_by".toLowerCase(), "finalAmountInsertBy".toLowerCase());
		java_SQL_map.put("final_amount_insertionTime".toLowerCase(), "finalAmountInsertionTime".toLowerCase());
		java_SQL_map.put("revised_amount".toLowerCase(), "revisedAmount".toLowerCase());
		java_SQL_map.put("revised_comment".toLowerCase(), "revisedComment".toLowerCase());
		java_SQL_map.put("revised_amount_insert_by".toLowerCase(), "revisedAmountInsertBy".toLowerCase());
		java_SQL_map.put("revised_amount_insertionTime".toLowerCase(), "revisedAmountInsertionTime".toLowerCase());
		java_SQL_map.put("revised_final_amount".toLowerCase(), "revisedFinalAmount".toLowerCase());
		java_SQL_map.put("revised_final_comment".toLowerCase(), "revisedFinalComment".toLowerCase());
		java_SQL_map.put("revised_final_amount_insert_by".toLowerCase(), "revisedFinalAmountInsertBy".toLowerCase());
		java_SQL_map.put("revised_final_amount_insertionTime".toLowerCase(), "revisedFinalAmountInsertionTime".toLowerCase());
		java_SQL_map.put("expenditure_amount".toLowerCase(), "expenditureAmount".toLowerCase());
		java_SQL_map.put("expenditure_insert_by".toLowerCase(), "expenditureInsertBy".toLowerCase());
		java_SQL_map.put("expenditure_insertionTime".toLowerCase(), "expenditureInsertionTime".toLowerCase());
		java_SQL_map.put("revised_expenditure_amount".toLowerCase(), "revisedExpenditureAmount".toLowerCase());
		java_SQL_map.put("revised_expenditure_insert_by".toLowerCase(), "revisedExpenditureInsertBy".toLowerCase());
		java_SQL_map.put("revised_expenditure_insertionTime".toLowerCase(), "revisedExpenditureInsertionTime".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Budget Selection Info Id".toLowerCase(), "budgetSelectionInfoId".toLowerCase());
		java_Text_map.put("Budget Mapping Id".toLowerCase(), "budgetMappingId".toLowerCase());
		java_Text_map.put("Economic Group Id".toLowerCase(), "economicGroupId".toLowerCase());
		java_Text_map.put("Economic Code Id".toLowerCase(), "economicCodeId".toLowerCase());
		java_Text_map.put("Economic Sub Code Id".toLowerCase(), "economicSubCodeId".toLowerCase());
		java_Text_map.put("Budget Office Id".toLowerCase(), "budgetOfficeId".toLowerCase());
		java_Text_map.put("Insert By".toLowerCase(), "insertBy".toLowerCase());
		java_Text_map.put("Insertion Time".toLowerCase(), "insertionTime".toLowerCase());
		java_Text_map.put("Amount".toLowerCase(), "amount".toLowerCase());
		java_Text_map.put("Amount Insert By".toLowerCase(), "amountInsertBy".toLowerCase());
		java_Text_map.put("Amount InsertionTime".toLowerCase(), "amountInsertionTime".toLowerCase());
		java_Text_map.put("Final Amount".toLowerCase(), "finalAmount".toLowerCase());
		java_Text_map.put("Final Amount Insert By".toLowerCase(), "finalAmountInsertBy".toLowerCase());
		java_Text_map.put("Final Amount InsertionTime".toLowerCase(), "finalAmountInsertionTime".toLowerCase());
		java_Text_map.put("Revised Amount".toLowerCase(), "revisedAmount".toLowerCase());
		java_Text_map.put("Revised Comment".toLowerCase(), "revisedComment".toLowerCase());
		java_Text_map.put("Revised Amount Insert By".toLowerCase(), "revisedAmountInsertBy".toLowerCase());
		java_Text_map.put("Revised Amount InsertionTime".toLowerCase(), "revisedAmountInsertionTime".toLowerCase());
		java_Text_map.put("Revised Final Amount".toLowerCase(), "revisedFinalAmount".toLowerCase());
		java_Text_map.put("Revised Final Comment".toLowerCase(), "revisedFinalComment".toLowerCase());
		java_Text_map.put("Revised Final Amount Insert By".toLowerCase(), "revisedFinalAmountInsertBy".toLowerCase());
		java_Text_map.put("Revised Final Amount InsertionTime".toLowerCase(), "revisedFinalAmountInsertionTime".toLowerCase());
		java_Text_map.put("Expenditure Amount".toLowerCase(), "expenditureAmount".toLowerCase());
		java_Text_map.put("Expenditure Insert By".toLowerCase(), "expenditureInsertBy".toLowerCase());
		java_Text_map.put("Expenditure InsertionTime".toLowerCase(), "expenditureInsertionTime".toLowerCase());
		java_Text_map.put("Revised Expenditure Amount".toLowerCase(), "revisedExpenditureAmount".toLowerCase());
		java_Text_map.put("Revised Expenditure Insert By".toLowerCase(), "revisedExpenditureInsertBy".toLowerCase());
		java_Text_map.put("Revised Expenditure InsertionTime".toLowerCase(), "revisedExpenditureInsertionTime".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}