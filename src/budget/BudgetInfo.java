package budget;

import util.StringUtils;

public class BudgetInfo {
    public int beginYear = 0;
    public String economicYear = "";
    public BudgetTypeEnum budgetTypeEnum = BudgetTypeEnum.BUDGET;

    public BudgetInfo(){

    }

    public BudgetInfo(int beginYear, BudgetTypeEnum budgetTypeEnum) {
        this.beginYear = beginYear;
        this.economicYear = getEconomicYear(beginYear);
        this.budgetTypeEnum = budgetTypeEnum;
    }

    public String getText(String language) {
        return budgetTypeEnum.getText(language).concat(" - ")
                .concat(StringUtils.convertBanglaIfLanguageIsBangla(language, economicYear));
    }

    @Override
    public String toString() {
        return "BudgetInfo{" +
                "economicYear='" + economicYear + '\'' +
                ", budgetTypeEnum=" + budgetTypeEnum +
                '}';
    }

    public static String getEconomicYear(int beginYear){
        return beginYear + "-" + (beginYear + 1);
    }
}
