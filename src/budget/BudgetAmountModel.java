package budget;

import static budget.BudgetUtils.isInvalidAmount;
import static budget.BudgetUtils.isRevisedBudgetPeriod;

public class BudgetAmountModel {
    private static final long BUDGET_AMOUNT_UNIT = 1000L;

    public long allocatedBudget;
    public long initialExpenditureAmount;

    public static BudgetAmountModel getEmptyInstance() {
        return new BudgetAmountModel();
    }

    private BudgetAmountModel() {
        allocatedBudget = 0;
        initialExpenditureAmount = 0;
    }

    public BudgetAmountModel(BudgetDTO budgetDTO, long timeInMillis) {
        allocatedBudget = (long) budgetDTO.finalAmount;
        boolean toGetRevisedAmount = isRevisedBudgetPeriod(timeInMillis)
                                     && !isInvalidAmount(budgetDTO.revisedFinalAmount);
        if (toGetRevisedAmount) {
            allocatedBudget = (long) budgetDTO.revisedFinalAmount;
        }
        allocatedBudget *= BUDGET_AMOUNT_UNIT;

        initialExpenditureAmount = (long) budgetDTO.initialExpenditureAmount * BUDGET_AMOUNT_UNIT;
    }
}
