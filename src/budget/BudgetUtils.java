package budget;

import bangladehi_number_format_util.BangladeshiNumberFormatter;
import budget_selection_info.BudgetSelectionInfoRepository;
import economic_code.Economic_codeDTO;
import economic_code.Economic_codeRepository;
import economic_sub_code.Economic_sub_codeDTO;
import economic_sub_code.Economic_sub_codeRepository;
import util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.ToDoubleFunction;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static pbReport.DateUtils.*;
import static util.StringUtils.convertToBanNumber;

@SuppressWarnings({"Duplicates"})
public class BudgetUtils {
    public static final long FINANCE_SECTION_OFFICE_ID = -1L;
    public static long BUDGET_AMOUNT_UNIT = 1000L;

    public static int compareEconomicCodeId(long a, long b) {
        if(a == b) {
            return 0;
        }
        Economic_codeDTO aDTO = Economic_codeRepository.getInstance().getById(a);
        if(aDTO == null) {
            return -1;
        }
        Economic_codeDTO bDTO = Economic_codeRepository.getInstance().getById(b);
        return aDTO.code.compareTo(bDTO.code);
    }

    public static int compareEconomicSubCodeId(long a, long b) {
        if(a == b) {
            return 0;
        }
        Economic_sub_codeDTO aDTO = Economic_sub_codeRepository.getInstance().getDTOByID(a);
        if(aDTO == null) {
            return -1;
        }
        Economic_sub_codeDTO bDTO = Economic_sub_codeRepository.getInstance().getDTOByID(b);
        return aDTO.code.compareTo(bDTO.code);
    }

    public static int compareBudgetDTOonSubCode(BudgetDTO a, BudgetDTO b) {
        Economic_sub_codeDTO aSubCodeDTO = Economic_sub_codeRepository.getInstance().getDTOByID(a.economicSubCodeId);
        Economic_sub_codeDTO bSubCodeDTO = Economic_sub_codeRepository.getInstance().getDTOByID(b.economicSubCodeId);
        return aSubCodeDTO.code.compareTo(bSubCodeDTO.code);
    }

    public static boolean isInvalidAmount(double amount) {
        return Double.compare(amount, .0) <= 0;
    }

    public static String markInvalidAmount(double amount) {
        return isInvalidAmount(amount) ? "style='color: red; font-weight: bold;'" : "";
    }

    public static String getInvalidAmountWarning(List<BudgetDTO> budgetDTOs, ToDoubleFunction<BudgetDTO> extractDouble, String language) {
        if (budgetDTOs == null || budgetDTOs.isEmpty())
            return "";

        boolean hasInvalidAmount = budgetDTOs.stream()
                                             .mapToDouble(extractDouble)
                                             .anyMatch(BudgetUtils::isInvalidAmount);
        if (!hasInvalidAmount) return "";

        return "English".equalsIgnoreCase(language)
               ? "Some budget sectors have not been allocated!<br>Do you still want to submit?"
               : "কিছু বাজেট খাতে কোন বরাদ্দ করা হয়নি!<br>আপনি কি তার পরও জমা দিতে চান?";
    }

    public static List<Map<Long, AmountModel>> getPreviousAmountsGroupedBySubCode(int startingYearExclusive,
                                                                                  int numberOfPreviousYears,
                                                                                  Map<Long, List<BudgetDTO>> mapBySelectionInfoId) {

        return IntStream.rangeClosed(1, numberOfPreviousYears)
                        .mapToObj(difference -> BudgetInfo.getEconomicYear(startingYearExclusive - difference))
                        .map(economicYear -> BudgetSelectionInfoRepository.getInstance().getDTOByEconomicYear(economicYear))
                        .map(selectionInfoDTO -> selectionInfoDTO == null ? -1 : selectionInfoDTO.iD)
                        .map(selectionInfoId -> mapBySelectionInfoId.getOrDefault(selectionInfoId, new ArrayList<>()))
                        .map(budgetDTOs -> budgetDTOs.stream()
                                                     .collect(toMap(e -> e.economicSubCodeId, AmountModel::new)))
                        .collect(toList());
    }

    public static List<Long> getSelectionIds(int startingYearInclusive, int numberOfPreviousYears) throws Exception {
        return IntStream.rangeClosed(0, numberOfPreviousYears)
                        .mapToObj(difference -> BudgetInfo.getEconomicYear(startingYearInclusive - difference))
                        .map(economicYear -> BudgetSelectionInfoRepository.getInstance().getDTOByEconomicYear(economicYear))
                        .filter(Objects::nonNull)
                        .map(selectionInfoDTO -> selectionInfoDTO.iD)
                        .collect(toList());
    }

    // Code Selection
    //	1 July 2021 to 30th June 2022 --> 2022-2023 's code selection
    // Jan-June (year) to (year+1)
    // July-Dec (year+1) to (year+2)
    public static String getEconomicYearForCodeSelection(long timeMillis) {
        int year = getYear(timeMillis);
        return BudgetInfo.getEconomicYear(
                isMonthBetween(timeMillis, Calendar.JANUARY, Calendar.JUNE) ? year : (year + 1)
        );
    }

    // Example
    // 1 Jan   2021 - 28 Feb      2021 => 2020-2021 Revised Budget
    // 1 March 2021 - 30 June     2021 => 2021-2022 Budget
    // 1 July  2021 - 31 December 2021 => 2022-2023 Budget
    public static BudgetInfo getBudgetInfoForBudgetSubmission(long timeMillis) {
        int year = getYear(timeMillis);
        int month = getMonth(timeMillis);
        int beginYear;
        BudgetTypeEnum budgetTypeEnum;

        // JANUARY - FEBRUARY => running Economic Year Revised Budget
        //                    => (year - 1) to (year) Revised Budget
        // 1 Jan 2021 - 28 Feb 2021 => 2020-2021 Revised Budget
        if (isMonthBetween(month, Calendar.JANUARY, Calendar.FEBRUARY)) {
            beginYear = year - 1;
            budgetTypeEnum = BudgetTypeEnum.REVISED_BUDGET;
        }
        // MARCH - JUNE => Next Economic Year Budget
        //              => (year) to (year + 1) Budget
        // 1 March 2021 - 30 June 2021 => 2021-2022 Budget
        else if (isMonthBetween(month, Calendar.MARCH, Calendar.JUNE)) {
            beginYear = year;
            budgetTypeEnum = BudgetTypeEnum.BUDGET;
        }
        // JULY - DECEMBER => Next Economic Year Budget
        //                 => (year + 1) to (year + 2) Budget
        // 1 July 2021 - 31 December 2021 => 2022-2023 Budget
        else {
            beginYear = year + 1;
            budgetTypeEnum = BudgetTypeEnum.BUDGET;
        }

        return new BudgetInfo(beginYear, budgetTypeEnum);
    }

    public static int getEconomicYearBeginYear(long timeMillis) {
        int year = getYear(timeMillis);
        if (isMonthBetween(timeMillis, Calendar.JANUARY, Calendar.JUNE))
            return year - 1;
        return year;
    }

    public static String getEconomicYearText(long timeMillis, String language) {
        return StringUtils.convertBanglaIfLanguageIsBangla(
                language,
                BudgetInfo.getEconomicYear(getEconomicYearBeginYear(timeMillis))
        );
    }

    public static long getEconomicYearBeginTime(long timeMillis) {
        return getEconomicYearBeginTimeFromYear(
                getEconomicYearBeginYear(timeMillis)
        );
    }

    public static long getEconomicYearStartMonthYear(long timeMillis) {
        int year = getEconomicYearBeginYear(timeMillis);
        return parseMonthYearString(String.format("01/07/%04d", year));
    }

    public static long parseMonthYearString(String monthYearDateStr) {
        try{
            return new SimpleDateFormat("dd/MM/yyyy").parse(monthYearDateStr).getTime();
        } catch (Exception exception) {
            return Long.MIN_VALUE;
        }
    }

    public static long getEconomicYearEndTime(long timeMillis) {
        return getEconomicYearBeginTimeFromYear(getEconomicYearBeginYear(timeMillis) + 1) - 1;
    }

    public static long getEconomicYearBeginTimeFromYear(int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.MONTH, Calendar.JULY);
        calendar.set(Calendar.YEAR, year);
        return calendar.getTimeInMillis();
    }

    public static boolean isRevisedBudgetPeriod(long timeMillis) {
        return isMonthBetween(timeMillis, Calendar.JANUARY, Calendar.JUNE);
    }

    public static String getEconomicYear(long timeMillis) {
        return BudgetInfo.getEconomicYear(getEconomicYearBeginYear(timeMillis));
    }

    public static String getRunningEconomicYear() {
        return getEconomicYear(System.currentTimeMillis());
    }

    // TODO: 04-Jul-21 BudgetPreparation may have different timeline. Because current design allows budget
    //  to be prepared, NOT revised budget. Kept same as submission fot now but may change
    public static BudgetInfo getBudgetInfoForBudgetPreparation(long timeMillis) {
        return getBudgetInfoForBudgetSubmission(timeMillis);
    }

    public static boolean isExpenditureAvailableByYear(String economicYear, long currentTime) {
        String secondYear = economicYear.split("-")[1];
        String january1st = "01/01/" + secondYear; //After January 1st Revised expenditure can be submitted
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return currentTime > formatter.parse(january1st).getTime();
        } catch (ParseException ex) {
            return false;
        }
    }

    public static boolean isRevisedExpenditureAvailableByYear(String economicYear, long currentTime) {
        String secondYear = economicYear.split("-")[1];
        String july1st = "01/07/" + secondYear; //After July 1st Revised expenditure can be submitted
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return currentTime > formatter.parse(july1st).getTime();
        } catch (ParseException ex) {
            return false;
        }
    }

    public static String getFormattedAmount(String language, double amount) {
        String format = "%.2f";
        String numStr = "English".equalsIgnoreCase(language)
                        ? String.format(format, amount)
                        : convertToBanNumber(String.format(format, amount));
        return BangladeshiNumberFormatter.getFormattedNumber(numStr);
    }
}
