package vm_dashboard;

public class CountDTO {
    public String typeName = "";
    public String typeValue = "";
    public String driverValue = "";

    @Override
    public String toString() {
        return "CountDTO[" +
                " typeName = " + typeName +
                " typeValue = " + typeValue +
                "]";
    }
}
