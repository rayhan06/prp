package vm_dashboard;

import com.google.gson.Gson;
import dashboard.DashboardDTO;
import dashboard.DashboardService;
import employee_assign.EmployeeAssignDTO;
import employee_records.Employee_recordsRepository;
import login.LoginDTO;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import pb.Utils;
import sessionmanager.SessionConstants;
import util.TimeConverter;
import util.UtilCharacter;
import vm_maintenance.Vm_maintenanceDTO;
import vm_maintenance.Vm_maintenanceRepository;
import vm_requisition.CommonApprovalStatus;
import vm_requisition.Vm_requisitionDAO;
import vm_requisition.Vm_requisitionDTO;
import vm_requisition.Vm_requisitionRepository;
import vm_vehicle.Vm_vehicleDAO;
import vm_vehicle.Vm_vehicleDTO;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDAO;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;
import language.LC;
import language.LM;


/**
 * Servlet implementation class VehicleDashboardServlet
 */
@WebServlet("/VehicleDashboardServlet")
@MultipartConfig
public class VehicleDashboardServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(VehicleDashboardServlet.class);
	private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public VehicleDashboardServlet()
	{
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In do get request = " + request);
		try
		{

			String actionType = request.getParameter("actionType");
			if(actionType.equals("main"))
			{

				request.getRequestDispatcher("vm_dashboard/vehicle_dashboard.jsp").forward(request, response);

			}
			else if(actionType.equals("requisition_count_by_vehicle_type")){

				String data = "";
				String language = request.getParameter("language");
				Map<String, Integer> result = getRequisitionListByVehicleType(language);
				data = new Gson().toJson(result);
				response.setContentType("application/json");
				PrintWriter out = response.getWriter();
				out.print(data);
				out.flush();
				out.close();

			}
			else if(actionType.equals("vehicle_count_by_office")){

				String data = "";
				String language = request.getParameter("language");
				Map<String, Integer> result = getVehicleCountByOffice(language);
				data = new Gson().toJson(result);
				response.setContentType("application/json");
				PrintWriter out = response.getWriter();
				out.print(data);
				out.flush();
				out.close();

			}

			else if(actionType.equals("last_six_month_main_cost")){

				DashboardDTO dashboardDTO = new DashboardDTO();
				LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
				String language = LM.getText(LC.TRAINING_CALENDER_EDIT_LANGUAGE, loginDTO);

				List<Vm_maintenanceDTO> vm_maintenanceDTOS = Vm_maintenanceRepository.getInstance().getVm_maintenanceList()
						.stream()
						.filter(i -> (i.status == 5 || i.status == 6))
						.collect(Collectors.toList());

				for(int i = 0; i <= 6; i ++)
				{
					long startTime = TimeConverter.get1stDayOfNthtMonth(-i);
					long endTime = TimeConverter.get1stDayOfNthtMonth(-i + 1);

					List<Vm_maintenanceDTO> monthlyMaintenanceDTOS = vm_maintenanceDTOS
							.stream()
							.filter(j -> j.aoApproveDate >= startTime && j.aoApproveDate <endTime)
							.collect(Collectors.toList());

					dashboardDTO.lastSixMonthsDriverCost[i] = monthlyMaintenanceDTOS.stream().mapToDouble(j -> j.requested_total).sum();
					dashboardDTO.lastSixMonthsActualCost[i] = monthlyMaintenanceDTOS.stream().mapToDouble(j -> j.totalWithoutVat).sum();

				}

				ArrayUtils.reverse(dashboardDTO.lastSixMonthsDriverCost);
				ArrayUtils.reverse(dashboardDTO.lastSixMonthsActualCost);


				List<CountDTO> countDTOS = new ArrayList<>();
				CountDTO dto = new CountDTO();
				dto.typeName = LM.getText(LC.HM_MONTH, loginDTO);
				dto.driverValue = UtilCharacter.getDataByLanguage
						(language, "Estimated Cost", "Estimated Cost");
				dto.typeValue = UtilCharacter.getDataByLanguage(language,
						"Actual Cost", "Actual Cost");
				countDTOS.add(dto);

				for(int j = 0; j < 6; j ++)
				{
					CountDTO countDTO = new CountDTO();
					countDTO.typeName = Utils.getDigits(dashboardDTO.last6Months[j], language);
					countDTO.driverValue = dashboardDTO.lastSixMonthsDriverCost[j] + "";
					countDTO.typeValue = dashboardDTO.lastSixMonthsActualCost[j] + "";
					countDTOS.add(countDTO);
				}
				

				String data = "";
				data = new Gson().toJson(countDTOS);
				response.setContentType("application/json");
				PrintWriter out = response.getWriter();
				out.print(data);
				out.flush();
				out.close();

			}

			else if(actionType.equals("last_seven_days_req_count")){

				DashboardDTO dashboardDTO = new DashboardDTO();
				LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
				String language = LM.getText(LC.TRAINING_CALENDER_EDIT_LANGUAGE, loginDTO);

				List<Vm_requisitionDTO> vmRequisitionDTOS = Vm_requisitionRepository.getInstance().getVm_requisitionList()
						.stream()
						.filter(i -> i.status == CommonApprovalStatus.SATISFIED.getValue() ||
								i.status == CommonApprovalStatus.PAID.getValue())
						.collect(Collectors.toList());

				for(int i = 0; i < 7; i ++)
				{
					long startTime = TimeConverter.getNthDay(-i);
					long endTime = TimeConverter.getNthDay(-i + 1);

					dashboardDTO.lastSevenDaysRequisitions[i] =
							vmRequisitionDTOS
									.stream()
									.filter(j -> j.decisionOn >= startTime && j.decisionOn < endTime)
									.count();
				}

				ArrayUtils.reverse(dashboardDTO.lastSevenDaysRequisitions);

				List<CountDTO> countDTOS = new ArrayList<>();
				CountDTO dto = new CountDTO();
				dto.typeName = LM.getText(LC.HM_DAY, loginDTO);
				dto.typeValue = UtilCharacter.getDataByLanguage
						(language, "Requisition Count", "Requisition Count");
				countDTOS.add(dto);

				for(int j = 0; j < 7; j ++)
				{
					CountDTO countDTO = new CountDTO();
					countDTO.typeName = Utils.getDigits(dashboardDTO.last7Days[j], language);
					countDTO.typeValue = dashboardDTO.lastSevenDaysRequisitions[j] + "";
					countDTOS.add(countDTO);
				}

				String data = "";
				data = new Gson().toJson(countDTOS);
				response.setContentType("application/json");
				PrintWriter out = response.getWriter();
				out.print(data);
				out.flush();
				out.close();

			}

			else if(actionType.equals("vehicle_and_driver_count")){
				LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

				String language = LM.getText(LC.TRAINING_CALENDER_EDIT_LANGUAGE, loginDTO);
				DashboardService dashboardService = new DashboardService();
				List<Vm_vehicleDTO> vehicleDTOList = getVehicleCountByType(language);
				List<CountDTO> countDTOS = vehicleDTOList.stream().
						map(i -> {
							CountDTO dto = new CountDTO();
							dto.typeName = i.typeName;
							dto.typeValue = UtilCharacter.convertDataByLanguage(language, i.count + "");
							return dto;
						})
						.collect(Collectors.toList());

				int driverSize = 0;
				List<Long> commonDTOIds = Office_unitsRepository.getInstance().getOffice_unitsList()
						.stream()
						.filter(i -> i.unitNameEng.contains("TRANSPORT SECTION"))
						.map(i -> i.iD)
						.collect(Collectors.toList());

				List<EmployeeAssignDTO> employeeAssignDTOS = Employee_recordsRepository.getInstance().getDriverEmployeeAssignDTO();
				driverSize =  employeeAssignDTOS.size();

				CountDTO countDTO = new CountDTO();
				countDTO.typeValue = UtilCharacter.convertDataByLanguage(language, driverSize + "");
				countDTO.typeName = UtilCharacter.getDataByLanguage(language, "ড্রাইভার", "Driver");
				countDTOS.add(countDTO);



				String data = "";
				data = new Gson().toJson(countDTOS);
				response.setContentType("application/json");
				PrintWriter out = response.getWriter();
				out.print(data);
				out.flush();
				out.close();

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

	}



	private String getOfficeUnit(Office_unitsDTO model, boolean isLanguageBangla){
		return model == null ? (isLanguageBangla ? "অন্যান্য" : "Others") : (isLanguageBangla ? model.unitNameBng : model.unitNameEng);
	}

	public Map<String,Integer> getRequisitionListByVehicleType(String language){

		List<Vm_requisitionDTO> requisitionDTOS = new Vm_requisitionDAO().
				getAllVm_requisitionByVehicleType();

		if(requisitionDTOS.size() == 0){
			return new HashMap<>();
		}

		List<CategoryLanguageModel> languageModelList = CatRepository.getInstance().
				getCategoryLanguageModelList("vehicle_type");
		Map<Integer,CategoryLanguageModel> languageModelMap = languageModelList.stream()
				.collect(Collectors.toMap(model->model.categoryValue, model->model,(model1, model2)->model1));
		boolean isLanguageBangla = "Bangla".equalsIgnoreCase(language);
		Map<String,Integer> ageMap = requisitionDTOS.stream().
				collect(Collectors.toMap(m -> getCat(languageModelMap.get(m.givenVehicleType),isLanguageBangla)
						, m -> m.requisitionCount));

		return new TreeMap<>(ageMap);

	}


	public Map<String,Integer> getVehicleCountByOffice(String language){

		List<Vm_vehicle_office_assignmentDTO> officeAssignmentDTOS = new Vm_vehicle_office_assignmentDAO().
				getAllVm_vehicle_office_assignmentCount();

		if(officeAssignmentDTOS.size() == 0){
			return new HashMap<>();
		}

		List<Long> ids = officeAssignmentDTOS.stream().map(i -> i.officeId).collect(Collectors.toList());
		List<Office_unitsDTO> office_unitsDTOS =
				Office_unitsRepository.getInstance().getOffice_unitsList().stream().
						filter(i -> ids.contains(i.iD)).collect(Collectors.toList());
		Map<Long,Office_unitsDTO> officeUnitsDTOMap = office_unitsDTOS.stream()
				.collect(Collectors.toMap(model->model.iD, model->model));

		boolean isLanguageBangla = "Bangla".equalsIgnoreCase(language);
		Map<String,Integer> ageMap = officeAssignmentDTOS.stream().
				collect(Collectors.toMap(m -> getOfficeUnit(officeUnitsDTOMap.get(m.officeId),isLanguageBangla)
						, m -> m.count));

		return new TreeMap<>(ageMap);

	}

	public List<Vm_vehicleDTO> getVehicleCountByType(String language){

		List<Vm_vehicleDTO> vehicleDTOS = new Vm_vehicleDAO().getAllVm_vehicleCountByType();
		List<CategoryLanguageModel> languageModelList = CatRepository.getInstance().
				getCategoryLanguageModelList("vehicle_type");
		Map<Integer,CategoryLanguageModel> languageModelMap = languageModelList.stream()
				.collect(Collectors.toMap(model->model.categoryValue, model->model));
		boolean isLanguageBangla = "Bangla".equalsIgnoreCase(language);
		vehicleDTOS.forEach(i -> i.typeName = getCat(languageModelMap.get(i.vehicleTypeCat),isLanguageBangla));

		return vehicleDTOS;
	}

	private String getCat(CategoryLanguageModel model, boolean isLanguageBangla){
		return model == null ? (isLanguageBangla ? "অন্যান্য" : "Others") : (isLanguageBangla ? model.banglaText : model.englishText);
	}


}

