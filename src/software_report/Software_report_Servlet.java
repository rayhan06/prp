package software_report;


import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Software_report_Servlet")
public class Software_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","software_type","software_cat","=","","int","","","any","softwareCat", LC.SOFTWARE_REPORT_WHERE_SOFTWARECAT + ""},		
		{"criteria","software_type","software_sub_cat","=","AND","int","","","any","softwareSubCat", LC.SOFTWARE_REPORT_WHERE_SOFTWARESUBCAT + ""},		
		{"criteria","","has_license","=","AND","String","","","any","hasLicense", LC.SOFTWARE_REPORT_WHERE_HASLICENSE + ""},
		{"criteria","software_type","isDeleted","=","AND","int","","","0","zero",  ""},	
		{"criteria","software_subtype","isDeleted","=","AND","int","","","0","zero",  ""},	
	};
	
	String[][] Display =
	{
		{"display","software_type","software_cat","cat",""},		
		{"display","","CONCAT(software_type.software_cat, '_', software_type.software_sub_cat)","software_sub_cat",""},		
		{"display","","has_license","boolean",""},		
		{"display","","SUM(CASE WHEN assigned_to_org_id = -1 THEN 1 ELSE 0 END)","int",""},		
		{"display","","SUM(CASE WHEN assigned_to_org_id != -1 THEN 1 ELSE 0 END)","int",""},		
		{"display","","COUNT(software_subtype.id)","int",""}		
	};
	
	String GroupBy = "software_type.software_cat, software_type.software_sub_cat, has_license";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Software_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "software_subtype JOIN software_type ON software_type_id = software_type.id";

		Display[0][4] = LM.getText(LC.SOFTWARE_REPORT_SELECT_SOFTWARECAT, loginDTO);
		Display[1][4] = language.equalsIgnoreCase("english")?"Software Version":"সফটওয়্যার ভার্শন";
		Display[2][4] = LM.getText(LC.SOFTWARE_REPORT_SELECT_HASLICENSE, loginDTO);
		Display[3][4] = LM.getText(LC.ASSET_CATEGORY_REPORT_SELECT_REMAININGQUANTITY, loginDTO);
		Display[4][4] = LM.getText(LC.HM_ASSIGNED_QUANTITY, loginDTO);
		Display[5][4] = LM.getText(LC.ASSET_CATEGORY_REPORT_SELECT_TOTALQUANTITY, loginDTO);

		
		String reportName = LM.getText(LC.SOFTWARE_REPORT_OTHER_SOFTWARE_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(3, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(4, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(5, ReportRequestHandler.RIGHT_ALIGN_INT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "software_report",
				MenuConstants.SOFTWARE_REPORT_DETAILS, language, reportName, "software_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
