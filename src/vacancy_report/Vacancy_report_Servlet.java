package vacancy_report;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.google.gson.Gson;

import language.LC;
import login.LoginDTO;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsRepository;
import pbReport.ReportService;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.ReportRequestHandler;

@WebServlet("/Vacancy_report_Servlet")
public class Vacancy_report_Servlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria;
	
	
	String[][] Display =
	{
		{"display","","designation_bng","basic",""},
		{"display","","designation_eng","basic",""},				
		{"display","","count(id)","int",""},		
		{"display","","SUM(CASE\r\n" + 
				"        WHEN office_unit_organograms.isVacant = 1 THEN 1\r\n" + 
				"        ELSE 0\r\n" + 
				"    END)","int",""},		
		{"display","","SUM(CASE\r\n" + 
				"        WHEN office_unit_organograms.isVacant = 0 THEN 1\r\n" + 
				"        ELSE 0\r\n" + 
				"    END)","int",""}
	};
	
	
	
	
	String GroupBy = " designation_bng ";
	String OrderBY = " designation_bng ASC ";
	
	ReportRequestHandler reportRequestHandler;
	
	public Vacancy_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = " office_unit_organograms ";

		Display[0][4] = language.equalsIgnoreCase("english")?"Designation (Bangla)":"পদবী (বাংলায়)";
		Display[1][4] = language.equalsIgnoreCase("english")?"Designation (English)":"পদবী (ইংরেজিতে)";
		Display[2][4] = language.equalsIgnoreCase("english")?"Total Count":"মোট সংখ্যা";
		Display[3][4] = language.equalsIgnoreCase("english")?"Vacant Count":"শূন্যপদ সংখ্যা";
		Display[4][4] = language.equalsIgnoreCase("english")?"Assigned Count":"নিয়োগ সংখ্যা";
				
		
		ArrayList<String[]> criteriaList = new ArrayList<String[]>();
		
		String [] sCrit = {"criteria","","isDeleted","=","","int","","","0","none", ""};
		criteriaList.add(sCrit);
		if(request.getParameter("officeUnitIds") != null)
		{
			String filter = getOfficeIdsFromOfficeUnitIds(request).stream()
                    .map(String::valueOf)
                    .collect(Collectors.joining(","));
			
			criteriaList.add(new String[] {"criteria","","office_unit_id","in","AND","String","","",filter,"", ""});
			
		}
		
		criteriaList.add(new String[]{"criteria","","designation_bng","!=","and","String","","","","none", ""});
		criteriaList.add(new String[]{"criteria","","designation_bng","like","and","String","","","","designation_bng", LC.GLOBAL_DESIGNATION_BANGLA+ ""});
		criteriaList.add(new String[]{"criteria","","designation_eng","like","and","String","","","","designation_eng", LC.GLOBAL_DESIGNATION_ENGLISH + ""});
		
		Criteria = new String[criteriaList.size()][];
		int i = 0;
		for(String [] sCrtiFromList: criteriaList)
		{
			Criteria[i++] = sCrtiFromList;
		}
		
		String reportName = language.equalsIgnoreCase("english")?"Designation Summary Report":"পদবীর সারসংক্ষেপ রিপোর্ট";
				
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(2, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(3, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(4, ReportRequestHandler.RIGHT_ALIGN_INT));

		

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "vacancy_report",
				MenuConstants.VACANCY_REPORT, language, reportName, "vacancy_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
	
	protected Set<Long> getOfficeIdsFromOfficeUnitIds(HttpServletRequest request) {
		Long[] officeUnitIds = new Gson().fromJson(request.getParameter("officeUnitIds"), Long[].class);

		boolean onlySelectedOffice = Boolean.parseBoolean(request.getParameter("onlySelectedOffice"));

		if (onlySelectedOffice)
			return new HashSet<>(Arrays.asList(officeUnitIds));

		return Arrays.stream(officeUnitIds)
				.flatMap(officeUnitId -> Office_unitsRepository.getInstance()
						.getAllChildOfficeDTOWithParent(officeUnitId).stream())
				.map(e -> e.iD).collect(Collectors.toSet());
	}

}
