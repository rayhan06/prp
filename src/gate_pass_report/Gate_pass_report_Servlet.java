package gate_pass_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;


@WebServlet("/Gate_pass_report_Servlet")
public class Gate_pass_report_Servlet extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    String[][] Criteria =
            {
                    {"criteria", "", "visiting_date", ">=", "", "long", "", "", Long.MIN_VALUE + "", "startDate"},
                    {"criteria", "", "visiting_date", "<=", "AND", "long", "", "", Long.MAX_VALUE + "", "endDate"},
                    {"criteria", "", "gate_pass_type_id", "=", "AND", "long", "", "", "any", "gatePassTypeId"},
                    {"criteria", "", "gate_pass_sub_type_id", "=", "AND", "long", "", "", "0", "gatePassSubTypeId"},
                    {"criteria", "", "referrer_id", "=", "AND", "long", "", "", "any", "referrerId"}
            };

    String[][] Display =
            {
                    {"display", "", "gate_pass_visitor_id", "gate_pass_visitor_name", ""},
                    {"display", "", "gate_pass_visitor_id", "gate_pass_visitor_phone", ""},
                    {"display", "", "visiting_date", "date", ""},
                    {"display", "", "gate_pass_type_id", "gate_pass_type", ""},
                    {"display", "", "gate_pass_sub_type_id", "gate_pass_sub_type", ""},
                    {"display", "", "referrer_id", "employee_records_id_details", ""},
                    {"display", "", "first_layer_approval_status", "gate_pass_first_layer_approval", ""},
                    {"display", "", "second_layer_approval_status", "gate_pass_second_layer_approval", ""},
                    {"display", "", "second_layer_approved_by", "username_and_name", ""},
                    {"display", "", "second_layer_approval_time", "date", ""},
                    {"display", "", "is_issued", "gate_pass_is_issued", ""},
                    {"display", "", "issued_by", "username_and_name", ""},
                    {"display", "", "issue_date", "date", ""},
                    {"display","","id","invisible",""}
            };

    String GroupBy = "";
    String OrderBY = "";

    ReportRequestHandler reportRequestHandler;

    public Gate_pass_report_Servlet() {

    }

    private final ReportService reportService = new ReportService();

    private String sql;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String language = "english";
        if (userDTO != null && userDTO.languageID == SessionConstants.BANGLA) {
            language = "bangla";
        }

        boolean isLangEng = language.equalsIgnoreCase("english");

        String actionType = request.getParameter("actionType");

        System.out.println("In ssservlet doget, actiontype = " + actionType);

        sql = "gate_pass";

        Display[0][4] = LM.getText(LC.GATE_PASS_REPORT_SELECT_GATEPASSVISITORID, loginDTO);
        Display[1][4] = LM.getText(LC.GATE_PASS_REPORT_SELECT_VISITOR_PHONE_NUMBER, loginDTO);
        Display[2][4] = LM.getText(LC.GATE_PASS_REPORT_SELECT_VISITINGDATE, loginDTO);
        Display[3][4] = LM.getText(LC.GATE_PASS_REPORT_SELECT_GATEPASSTYPEID, loginDTO);
        Display[4][4] = LM.getText(LC.GATE_PASS_REPORT_SELECT_GATEPASSSUBTYPEID, loginDTO);
        Display[5][4] = LM.getText(LC.GATE_PASS_REPORT_SELECT_REFERRERID, loginDTO);
        Display[6][4] = LM.getText(LC.GATE_PASS_REPORT_SELECT_FIRSTLAYERAPPROVALSTATUS, loginDTO);
        Display[7][4] = LM.getText(LC.GATE_PASS_REPORT_SELECT_SECONDLAYERAPPROVALSTATUS, loginDTO);
        Display[8][4] = isLangEng ? "Approved By" : "অনুমোদনকারী";
        Display[9][4] = isLangEng ? "Approval Time" : "অনুমোদনের সময়";
        Display[10][4] = isLangEng ? "Issue Status" : "ইস্যুকৃত কিনা?";
        Display[11][4] = isLangEng ? "Issued By" : "ইস্যুকারী";
        Display[12][4] = isLangEng ? "Issued Time" : "ইস্যুর তারিখ";
        Display[13][4] = LM.getText(LC.GATE_PASS_ADD_ID, loginDTO);


        String reportName = LM.getText(LC.GATE_PASS_REPORT_OTHER_GATE_PASS_REPORT, loginDTO);

        reportRequestHandler = new ReportRequestHandler(null,
                Criteria, Display, GroupBy, OrderBY, sql,
                reportService);


        reportRequestHandler.handleReportGet(request, response, userDTO, "gate_pass_report",
                MenuConstants.GATE_PASS_REPORT_DETAILS, language, reportName, "gate_pass_report");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
