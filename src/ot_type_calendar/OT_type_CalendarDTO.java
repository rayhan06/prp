package ot_type_calendar;

import sessionmanager.SessionConstants;
import util.CommonDTO;

public class OT_type_CalendarDTO extends CommonDTO {
    public long otDate = SessionConstants.MIN_DATE;
    public long electionDetailsId = -1;
    public long parliamentSessionId = -1;
    public String description = "";
    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;

    @Override
    public String toString() {
        return "OT_type_CalendarDTO{" +
               "otDate=" + otDate +
               ", electionDetailsId=" + electionDetailsId +
               ", parliamentSessionId=" + parliamentSessionId +
               ", description='" + description + '\'' +
               ", modifiedBy=" + modifiedBy +
               ", insertedBy=" + insertedBy +
               ", insertionTime=" + insertionTime +
               ", iD=" + iD +
               ", isDeleted=" + isDeleted +
               ", lastModificationTime=" + lastModificationTime +
               '}';
    }
}
