package ot_type_calendar;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class OT_type_CalendarDAO implements CommonDAOService<OT_type_CalendarDTO> {
    private static final Logger logger = Logger.getLogger(OT_type_CalendarDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (ot_date,election_details_id,parliament_session_id,description,modified_by,"
                    .concat("lastModificationTime,inserted_by,insertion_time,isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET ot_date=?,election_details_id=?,parliament_session_id=?,description=?,modified_by=?,"
                    .concat("lastModificationTime=?,isDeleted=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private OT_type_CalendarDAO() {
    }

    private static class LazyLoader {
        static final OT_type_CalendarDAO INSTANCE = new OT_type_CalendarDAO();
    }

    public static OT_type_CalendarDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, OT_type_CalendarDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.otDate);
        ps.setLong(++index, dto.electionDetailsId);
        ps.setLong(++index, dto.parliamentSessionId);
        ps.setString(++index, dto.description);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
        }
        ps.setInt(++index, dto.isDeleted);
        ps.setLong(++index, dto.iD);
    }

    @Override
    public OT_type_CalendarDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            OT_type_CalendarDTO dto = new OT_type_CalendarDTO();
            dto.iD = rs.getLong("ID");
            dto.otDate = rs.getLong("ot_date");
            dto.electionDetailsId = rs.getLong("election_details_id");
            dto.parliamentSessionId = rs.getInt("parliament_session_id");
            dto.description = rs.getString("description");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "ot_type_calendar";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((OT_type_CalendarDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((OT_type_CalendarDTO) commonDTO, updateSqlQuery, false);
    }

    private static final String findByOtDateDeletedOrNotSql = "SELECT * FROM ot_type_calendar WHERE ot_date=%d";

    public OT_type_CalendarDTO findByOtDateDeletedOrNot(long otDate) {
        return ConnectionAndStatementUtil.getT(
                String.format(findByOtDateDeletedOrNotSql, otDate),
                this::buildObjectFromResultSet
        );
    }

    private static final String findAllSql = "SELECT * FROM ot_type_calendar WHERE isDeleted=0";

    public List<OT_type_CalendarDTO> findAll() {
        return ConnectionAndStatementUtil.getListOfT(findAllSql, this::buildObjectFromResultSet);
    }

    private static final String findByOtDateDeletedRangeSql =
            "SELECT * FROM ot_type_calendar WHERE ot_date >= %d AND ot_date < %d AND isDeleted = 0";

    public List<OT_type_CalendarDTO> findByOtDateRange(long startDateInclusive, long endDateExclusive) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(findByOtDateDeletedRangeSql, startDateInclusive, endDateExclusive),
                this::buildObjectFromResultSet
        );
    }

    private static final String findByParliamentInfoSql =
            "SELECT * FROM ot_type_calendar WHERE election_details_id=%d AND parliament_session_id =%d AND isDeleted = 0";

    public List<OT_type_CalendarDTO> findByParliamentInfoSortedByDate(long electionDetailsId, long parliamentSessionId) {
        return ConnectionAndStatementUtil
                .getListOfT(
                        String.format(findByParliamentInfoSql, electionDetailsId, parliamentSessionId),
                        this::buildObjectFromResultSet
                ).stream()
                .sorted(Comparator.comparingLong(otTypeCalendar -> otTypeCalendar.otDate))
                .collect(Collectors.toList());
    }

    public List<Long> getSortedBillDatedByParliamentInfo(long electionDetailsId, long parliamentSessionId) {
        return findByParliamentInfoSortedByDate(electionDetailsId, parliamentSessionId)
                .stream()
                .map(ot_type_calendarDTO -> ot_type_calendarDTO.otDate)
                .collect(Collectors.toList());
    }
}