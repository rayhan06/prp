CREATE TABLE ot_type_calendar
(
    ID                    BIGINT PRIMARY KEY,
    ot_date               BIGINT DEFAULT -62135791200000 UNIQUE,
    election_details_id   BIGINT,
    parliament_session_id BIGINT,
    description           VARCHAR(4096),
    modified_by           BIGINT,
    lastModificationTime  BIGINT DEFAULT -62135791200000,
    inserted_by           BIGINT,
    insertion_time        BIGINT DEFAULT -62135791200000,
    isDeleted             INT    DEFAULT 0
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

INSERT INTO vb_sequencer (table_name, next_id, table_LastModificationTime)
VALUES ('ot_type_calendar', 1, 0);