package ot_type_calendar;

import com.google.gson.Gson;
import holiday.HolidayDAO;
import holiday.HolidayDTO;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import overtime_bill.OvertimeInfo;
import overtime_bill.OvertimeType;
import parliament_session.Parliament_sessionDAO;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@WebServlet("/OT_type_CalendarServlet")
@MultipartConfig
@SuppressWarnings({"Duplicates", "unused"})
public class OT_type_CalendarServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(OT_type_CalendarServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        String actionType = request.getParameter("actionType");
        switch (actionType) {
            case "getAddPage":
                if (Utils.checkPermission(userDTO, MenuConstants.OT_TYPE_CALENDAR)) {
                    request.getRequestDispatcher("ot_type_calendar/OT_type_CalendarEdit.jsp").forward(request, response);
                    return;
                }
                break;
            case "ajax_getParliamentSession": {
                String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
                long electionDetailsId = Long.parseLong(request.getParameter("electionDetailsId"));
                String selectedId = request.getParameter("selectedId");
                String options = new Parliament_sessionDAO().buildOptionsWithDate(language, electionDetailsId, selectedId);
                response.setContentType("text/html; charset=UTF-8");
                response.getWriter().println(options);
                return;
            }
            default:
                super.doGet(request, response);
                return;
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        String actionType = request.getParameter("actionType");
        try {
            if ("ajax_add".equals(actionType)) {
                if (Utils.checkPermission(userDTO, MenuConstants.OT_TYPE_CALENDAR)) {
                    UserInput userInput = new UserInput();
                    userInput.dateStr = request.getParameter("dateStr");
                    userInput.electionDetailsId = Utils.parseOptionalLong(
                            request.getParameter("electionDetailsId"),
                            null,
                            null
                    );
                    userInput.parliamentSessionId = Utils.parseOptionalLong(
                            request.getParameter("parliamentSessionId"),
                            null,
                            null
                    );
                    userInput.modifiedBy = userDTO.ID;

                    JsonResponse res = toggleOtDateCalendar(userInput);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().println(new Gson().toJson(res));
                    return;
                }
                super.doGet(request, response);
                return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private JsonResponse toggleOtDateCalendar(UserInput userInput) {
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLanguageEnglish = "english".equalsIgnoreCase(Language);

        JsonResponse jsonResponse = new JsonResponse();

        try {
            userInput.otDate = parseDate(userInput.dateStr);
        } catch (Exception ex) {
            jsonResponse.errorMessage = isLanguageEnglish ? "Wrong Date Format" : "তারিখের ফরম্যাট ভুল";
            return jsonResponse;
        }

        boolean addFlag = false;
        OT_type_CalendarDTO otTypeCalendar = OT_type_CalendarDAO.getInstance().findByOtDateDeletedOrNot(userInput.otDate);
        if (otTypeCalendar == null) {
            addFlag = true;
            otTypeCalendar = getNewOtTypeCalendarDTO(userInput);
        } else if (otTypeCalendar.isDeleted == 0) {
            otTypeCalendar.isDeleted = 1;
            otTypeCalendar.modifiedBy = userInput.modifiedBy;
            otTypeCalendar.lastModificationTime = System.currentTimeMillis();
            try {
                OT_type_CalendarDAO.getInstance().update(otTypeCalendar);
            } catch (Exception ex) {
                jsonResponse.errorMessage = isLanguageEnglish ? "Server Error" : "সার্ভারে সমস্যা";
                return jsonResponse;
            }
            jsonResponse.success = true;
            jsonResponse.status = CalendarInsertionStatus.REMOVED;
            jsonResponse.createdNote = getJsonObjectForDNCalendar(otTypeCalendar);
            return jsonResponse;
        }

        String message = "";
        if (userInput.electionDetailsId == null) {
            message += (isLanguageEnglish ? "Select Parliament Number<br>" : "সংসদ নম্বর বাছাই করুন<br>");
        }
        if (userInput.parliamentSessionId == null) {
            message += (isLanguageEnglish ? "Select Parliament Session Number" : "সংসদ অধিবেশন নম্বর বাছাই করুন");
        }
        if (!message.isEmpty()) {
            jsonResponse.errorMessage = message;
            return jsonResponse;
        }
        otTypeCalendar.electionDetailsId = userInput.electionDetailsId;
        otTypeCalendar.parliamentSessionId = userInput.parliamentSessionId;
        otTypeCalendar.isDeleted = 0;
        try {
            if (addFlag) {
                OT_type_CalendarDAO.getInstance().add(otTypeCalendar);
            } else {
                OT_type_CalendarDAO.getInstance().update(otTypeCalendar);
            }
        } catch (Exception ex) {
            jsonResponse.errorMessage = isLanguageEnglish ? "Server Error" : "সার্ভারে সমস্যা";
            return jsonResponse;
        }
        jsonResponse.success = true;
        jsonResponse.status = CalendarInsertionStatus.CREATED;
        jsonResponse.createdNote = getJsonObjectForDNCalendar(otTypeCalendar);
        return jsonResponse;
    }

    private OT_type_CalendarDTO getNewOtTypeCalendarDTO(UserInput userInput) {
        OT_type_CalendarDTO otTypeCalendarDTO = new OT_type_CalendarDTO();
        otTypeCalendarDTO.otDate = userInput.otDate;
        otTypeCalendarDTO.description = "ক - সংসদ অধিবেশন";
        otTypeCalendarDTO.insertedBy = otTypeCalendarDTO.modifiedBy = userInput.modifiedBy;
        otTypeCalendarDTO.insertionTime = otTypeCalendarDTO.lastModificationTime = System.currentTimeMillis();
        otTypeCalendarDTO.isDeleted = 0;
        return otTypeCalendarDTO;
    }

    public static String getAllForDNCalendar() {
        List<OT_type_CalendarDTO> allDTOs = OT_type_CalendarDAO.getInstance().findAll();
        JSONArray jsonArray = new JSONArray();
        for (OT_type_CalendarDTO dto : allDTOs) {
            jsonArray.put(getJsonObjectForDNCalendar(dto));
        }
        return jsonArray.toString();
    }

    private static Map<String,Object> getJsonObjectForDNCalendar(OT_type_CalendarDTO dto) {
        Map<String,Object> jsonObject = new HashMap<>();
        long date = dto.otDate;
        String note = dto.description;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        String dateStr = year + "-" + month + "-" + day;
        jsonObject.put("date", dateStr);
        JSONArray noteArray = new JSONArray();
        noteArray.put(note);
        jsonObject.put("note", noteArray);
        return jsonObject;
    }


    public static long parseDate(String dateStr) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date date = sdf.parse(dateStr);
            return date.getTime();
        } catch (ParseException e) {
            logger.error(e);
        }
        return SessionConstants.MIN_DATE;
    }

    private static class UserInput {
        String dateStr;
        Long otDate;
        Long modifiedBy;
        Long electionDetailsId;
        Long parliamentSessionId;
    }

    private static class JsonResponse {
        boolean success = false;
        CalendarInsertionStatus status = CalendarInsertionStatus.ERROR;
        Map<String, Object> createdNote = new HashMap<>();
        String errorMessage = "";
    }

    public enum CalendarInsertionStatus {
        CREATED,
        REMOVED,
        ERROR,
    }

    public static final long ONE_DAY_IN_MILLIS = (24L * 60) * 60 * 1000;

    public static List<OvertimeInfo> getOtTypeWithDateListOfTimeRange(long startDateInclusive, long endDateExclusive) {
        Map<Long, List<OT_type_CalendarDTO>> otTypeCalendarDTOByDate =
                OT_type_CalendarDAO.getInstance()
                                   .findByOtDateRange(startDateInclusive, endDateExclusive)
                                   .stream()
                                   .collect(Collectors.groupingBy(
                                           ot_type_calendarDTO -> ot_type_calendarDTO.otDate
                                   ));
        Map<Long, List<HolidayDTO>> holidaysByDate = new HolidayDAO().findByTimeRangeGroupedByDate(startDateInclusive, endDateExclusive);
        List<OvertimeInfo> otTypeStringValueList = new ArrayList<>();
        while (startDateInclusive < endDateExclusive) {
            OvertimeType overtimeType;
            if (otTypeCalendarDTOByDate.containsKey(startDateInclusive)) {
                overtimeType = OvertimeType.KA;
            } else if (!holidaysByDate.containsKey(startDateInclusive)) {
                overtimeType = OvertimeType.KHA;
            } else {
                overtimeType = OvertimeType.NONE;
            }
            otTypeStringValueList.add(new OvertimeInfo(startDateInclusive, overtimeType));
            startDateInclusive += ONE_DAY_IN_MILLIS;
        }
        return otTypeStringValueList;
    }

    public static List<Integer> getSortedBillDateOtTypeListForFoodBill(List<OT_type_CalendarDTO> otTypeCalendarSortedByDate) {
        if (otTypeCalendarSortedByDate == null || otTypeCalendarSortedByDate.isEmpty()) {
            return new ArrayList<>();
        }
        long startDateInclusive = otTypeCalendarSortedByDate.get(0).otDate;
        long endDateExclusive = otTypeCalendarSortedByDate.get(otTypeCalendarSortedByDate.size() - 1).otDate + ONE_DAY_IN_MILLIS;
        Map<Long, List<HolidayDTO>> holidaysByDate = new HolidayDAO().findByTimeRangeGroupedByDate(
                startDateInclusive, endDateExclusive
        );

        List<Integer> sortedBillDateOtTypeList =
                Stream.generate(OvertimeType.NONE::getIntValue)
                      .limit(otTypeCalendarSortedByDate.size())
                      .collect(Collectors.toList());
        for (int i = 0; i < sortedBillDateOtTypeList.size(); ++i) {
            long otTypeCalendarDate = otTypeCalendarSortedByDate.get(i).otDate;
            if (holidaysByDate.containsKey(otTypeCalendarDate)) {
                sortedBillDateOtTypeList.set(i, OvertimeType.KA_NONE.getIntValue());
            } else {
                sortedBillDateOtTypeList.set(i, OvertimeType.KA.getIntValue());
            }
        }
        return sortedBillDateOtTypeList;
    }
}
