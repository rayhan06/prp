package asset_model;
import asset_category.Asset_categoryDAO;
import asset_category.Asset_categoryDTO;
import software_type.SoftwareSubtypeDTO;
import util.*; 


public class AssetAssigneeDTO extends CommonDTO
{
	public static final int STOCK_IN = -2;
	public static final int NOT_ASSIGNED = -1;
    public static final int ASSIGNED = 1;
    public static final int CONDEMNED = 2;
    public static final int OTHER = -3;
    public static final int ALLEXCEPTSOFT = -4;
	public static final int RELEASED = 3;
	public static final int WARRANTY = 4;
	public static final int SEND_TO_REPAIR = 5;
	public static final int ACTIVATE = 6;
	public static final int DEACTIVATE = 7;
	public static final int FORWARD = 8;
	public static final int CLOSE = 9;
	public static final int TICKET_ASSIGN = 10;
	public static final int ADD_COMMENTS = 11;
	public static final int SOLVE_TICKET = 12;
	public static final int EMPLOYEE_LEFT_POST = 20;
	public static final int EMPLOYEE_JOINED_POST = 21;
	public static final int ASSET_TRANFER = 22;
	
	public static final int NOC_VERIFICATION = 100;
	public static final int NOC_APPROVAL = 101;
	public static final int NOC_REJECTION = 102;
    
    public static final int NOT_DELIVERED = -1;
    public static final int DELIVERED = 1;
    
    
    public static final int OWNER_PERSON = 0;
    public static final int OWNER_ROOM = 1;
    public static final int OWNER_OFFICE = 2;
    

	public long assetModelId = -1;
	public long assetCategoryType = -1;
	public int brandType = -1;
    public String model = "";
    public String modelName = "";
    public String parentModelName = "";
    public long assetTypeId = -1;


	public long assignedOrganogramId = -1;

	public long deliveryDate = System.currentTimeMillis();
	public double costingValue = 0;
	public int assignmentStatus = NOT_ASSIGNED;
    public long purchaseDate = 0;
    public long purchasingUserId = -1;
    public long purchasingOrganogramId = -1;
    
    public long assignmentDate = 0;
    public int deliveryStatus = DELIVERED;
    
    
    public long employeeRecordId = -1;
    
    
    //laptop, cpu
    public String sl = "";
    public String mac = ""; //also ap
    public String ip = ""; //also ap
    /*public int osLicenceCat = -1;
    public int antivirusLicenceCat = -1;
    public int officeLicenceCat = -1;*/
    public int ramCat = -1; //laptop, cpu
    public int ramSizeCat = -1; //laptop, cpu
    public int hdCat = -1; //laptop, cpu
    public int hdSizeCat = -1; //laptop, cpu
    public int processorCat = -1; //laptop, cpu
    public int processorGenCat = -1; //laptop, cpu
    
    public String pcName = ""; //laptop, cpu
    
    
    /*public String osKey = "";//laptop, cpu
    public String officeKey = ""; //laptop, cpu
    public String antivirusKey = ""; //laptop, cpu
    public long osKeyId = -1;//laptop, cpu
    public long officeKeyId = -1; //laptop, cpu
    public long antivirusKeyId = -1; //laptop, cpu
    public int osVersionCat = -1;//laptop, cpu
    public int officeVersionCat = -1; //laptop, cpu
    public int antivirusVersionCat = -1; //laptop, cpu*/
    
	
	
	public String activeDirectoryId = "";
    public String mkBoxNumber = "";
	
    //Access Point
    public String idfNumber = "";
    public String host = "";
    public String location = "";
    public String apLicenceForUse = "";
    public String partNumber = "";
    
    public boolean hasWarranty = false;
	public boolean hasTOE = false;
    
    public String port = "";
    
    public boolean isActive = true;
    
    public long softwareTypeId = -1;
    public long softwareSubTypeId = -1;
    public long parentAssigneeId = -1;
    
    public int softwareCat = -1;
    public int softwareSubCat = -1;
    public String softwareKey = "";
    public long expiryDate = -1;
    String condemnationReason = "";
    
    public String parentCatgories = "";
    
    public int roomNoCat = -1;
    public int ownerCat = OWNER_PERSON;
    public long officeAssetOfficeId = -1;
    public long responsibleOrganogramId = -1;
    

	
	public AssetAssigneeDTO(Asset_modelDTO asset_modelDTO)
	{
		assetCategoryType = asset_modelDTO.assetCategoryType;
		Asset_categoryDTO asset_categoryDTO = Asset_categoryDAO.getInstance().getDTOByID(assetCategoryType);
		if(asset_categoryDTO != null)
		{
			parentCatgories = asset_categoryDTO.parentList;
		}
		assetTypeId = asset_modelDTO.assetTypeId;
		assetModelId = asset_modelDTO.iD;
		brandType = asset_modelDTO.brandType;
		model = asset_modelDTO.nameEn;
		modelName = model;
		sl = asset_modelDTO.sl;
		purchaseDate = asset_modelDTO.insertionDate;
		purchasingUserId = asset_modelDTO.insertedByUserId;
		purchasingOrganogramId = asset_modelDTO.insertedByOrganogramId;
		costingValue = asset_modelDTO.costingValue;
		partNumber = asset_modelDTO.partNumber;
		host = asset_modelDTO.hostName;
		mac = asset_modelDTO.mac;
		ramCat = asset_modelDTO.ramCat;
		ramSizeCat = asset_modelDTO.ramSizeCat;
		hdCat = asset_modelDTO.hdCat;
		hdSizeCat = asset_modelDTO.hdSizeCat;
		processorCat = asset_modelDTO.processorCat;
		processorGenCat = asset_modelDTO.processorGenCat;
		hasWarranty = asset_modelDTO.hasWarranty;
		hasTOE = asset_modelDTO.hasTOE;
		expiryDate = asset_modelDTO.expiryDate;
	}
	
	public AssetAssigneeDTO(Asset_modelDTO asset_modelDTO, SoftwareSubtypeDTO softwareSubtypeDTO)
	{
		this(asset_modelDTO);
		this.softwareCat = asset_modelDTO.softwareCat;
		this.softwareSubCat = asset_modelDTO.softwareSubCat;
		this.softwareKey = softwareSubtypeDTO.licenseKey;
		this.expiryDate = softwareSubtypeDTO.expiryDate;
		this.softwareTypeId = softwareSubtypeDTO.softwareTypeId;
		this.softwareSubTypeId = softwareSubtypeDTO.iD;
	}
	
	public AssetAssigneeDTO()
	{
		
	}
	
    @Override
	public String toString() {
            return "$AssetAssigneeDTO[" +
            " iD = " + iD +
            " assetModelId = " + assetModelId +
            " assetCategoryType = " + assetCategoryType +
            " brandType = " + brandType +
            " model = " + model +
            " sl = " + sl +
            " mac = " + mac +
            " ip = " + ip +

       
            " assignedOrganogramId = " + assignedOrganogramId +

            " deliveryDate = " + deliveryDate +
            " costingValue = " + costingValue +
            " deliveryStatus = " + assignmentStatus +
            " remarks = " + remarks +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}