package asset_model;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import asset_category.Asset_categoryDAO;
import asset_category.Asset_categoryDTO;
import asset_transaction.Asset_transactionDAO;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import pb.CatDAO;
import pb.CommonDAO;
import pb.Utils;
import sessionmanager.SessionConstants;
import software_type.SoftwareSubtypeRepository;
import util.CommonRequestHandler;
import workflow.WorkflowController;


public class AssetXL {
	
	Asset_modelDAO asset_modelDAO;
	AssetAssigneeDAO assetAssigneeDAO;
	Asset_categoryDAO asset_categoryDAO;
    Asset_transactionDAO asset_transactionDAO;
    CommonRequestHandler commonRequestHandler;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    
    public AssetXL(Asset_modelDAO asset_modelDAO, AssetAssigneeDAO assetAssigneeDAO,
    		Asset_categoryDAO asset_categoryDAO, Asset_transactionDAO asset_transactionDAO,
    		CommonRequestHandler commonRequestHandler) 
	{
       this.asset_categoryDAO = asset_categoryDAO;
       this.asset_modelDAO = asset_modelDAO;
       this.asset_transactionDAO = asset_transactionDAO;
       this.assetAssigneeDAO = assetAssigneeDAO;
       this.commonRequestHandler= commonRequestHandler;
    } 
    
    public List<String> getNormalCells(Asset_modelDTO asset_modelDTO, AssetAssigneeDTO assetAssigneeDTO, int index) throws Exception
    {
    	ArrayList<String> cells = new ArrayList <String>();
    	cells.add((index + 1) + "");
    	Asset_categoryDTO asset_categoryDTO = asset_categoryDAO.getDTOByID(asset_modelDTO.assetCategoryType);
    	if(asset_categoryDTO != null)
    	{
    		    		
    		if(asset_categoryDTO.isAssignable)
    		{
    			cells.add(asset_categoryDTO.nameEn);
    		}
    		else
    		{
    			cells.add(CatDAO.getName("English", "hardware_software", asset_modelDTO.hsCat));
    		}
    		
    		if(asset_categoryDTO.iD == Asset_categoryDTO.DATA_CENTRE 
    				|| asset_categoryDTO.iD == Asset_categoryDTO.CAMERA_AND_MONITORING 
    				|| asset_categoryDTO.iD == Asset_categoryDTO.SWITCH
    				|| asset_categoryDTO.iD == Asset_categoryDTO.NETWORK_ACCESSORIES)
    		{
    			cells.add(asset_modelDTO.productName);
    		}
    		
    		if(asset_categoryDTO.iD == Asset_categoryDTO.PRINTER)
    		{
    			cells.add(CatDAO.getName("English", "hardware_software", asset_modelDTO.hsCat));
    		}
    		
    		cells.add(CommonDAO.getName("English", "brand", asset_modelDTO.brandType));
    		cells.add(asset_modelDTO.nameEn);
    		
    		
    		cells.add(assetAssigneeDTO.sl);
    		
    		if(asset_categoryDTO.iD == Asset_categoryDTO.LAPTOP || asset_categoryDTO.iD == Asset_categoryDTO.CPU)
    		{
    			cells.add(asset_modelDTO.mac);
    	    	cells.add(CatDAO.getName("English", "processor", assetAssigneeDTO.processorCat) + " " + CatDAO.getName("English", "processor_gen", assetAssigneeDTO.processorGenCat));
    	    	cells.add(CatDAO.getName("English", "ram", assetAssigneeDTO.ramCat) + " " + CatDAO.getName("English", "ram_size", assetAssigneeDTO.ramSizeCat));
    	    	cells.add(CatDAO.getName("English", "hd", assetAssigneeDTO.hdCat));
    	    	cells.add(CatDAO.getName("English", "hd_size", assetAssigneeDTO.hdSizeCat));

    	    	
    	    	if(asset_categoryDTO.iD == Asset_categoryDTO.LAPTOP)
    	    	{
    	    		cells.add(CatDAO.getName("English", "display_size", asset_modelDTO.displaySizeCat));
    	    	}
    	    	if(asset_categoryDTO.iD == Asset_categoryDTO.CPU)
    	    	{
    	    		cells.add(assetAssigneeDTO.ip);
    	    	}
    	    	
    	    	/*cells.add(CatDAO.getName("English", "os_version", assetAssigneeDTO.osVersionCat));
    	    	cells.add(CatDAO.getName("English", "software_license", assetAssigneeDTO.osLicenceCat));
    	    	cells.add(assetAssigneeDTO.osKey);

    	    	cells.add(CatDAO.getName("English", "antivirus_version", assetAssigneeDTO.antivirusVersionCat));
    	    	cells.add(SoftwareSubtypeRepository.getInstance().getExpiryDateStr(assetAssigneeDTO.antivirusKeyId));
    	    	cells.add(CatDAO.getName("English", "office_version", assetAssigneeDTO.officeVersionCat));
    	    	cells.add(CatDAO.getName("English", "software_license", assetAssigneeDTO.officeLicenceCat));
    	    	cells.add(assetAssigneeDTO.officeKey);
    	    	cells.add(SoftwareSubtypeRepository.getInstance().getExpiryDateStr(assetAssigneeDTO.officeKeyId));*/
    		}
    		else if(asset_categoryDTO.iD == Asset_categoryDTO.MONITOR)
    		{
    			cells.add(CatDAO.getName("English", "display_size", asset_modelDTO.displaySizeCat));
    		}
    		else if(asset_categoryDTO.iD == Asset_categoryDTO.UPS)
    		{
    			cells.add(asset_modelDTO.capacity);
    			cells.add(asset_modelDTO.idfNumber);
    			cells.add(assetAssigneeDTO.deliveryDate > 0 ? simpleDateFormat.format(new Date(assetAssigneeDTO.deliveryDate)): "");
    		}
    		else if(asset_categoryDTO.iD == Asset_categoryDTO.PRINTER)
    		{
    			cells.add(assetAssigneeDTO.ip);
    		}
    		else if(asset_categoryDTO.iD == Asset_categoryDTO.ACCESS_POINT)
    		{
    			cells.add(assetAssigneeDTO.host);
    	    	cells.add(assetAssigneeDTO.ip);
    	    	cells.add(assetAssigneeDTO.mac);
    	    	cells.add(assetAssigneeDTO.idfNumber);
    	    	cells.add(assetAssigneeDTO.port);
    	    	cells.add(WorkflowController.getNameFromOrganogramId(assetAssigneeDTO.assignedOrganogramId, "English"));
    	    	cells.add(assetAssigneeDTO.location);
    	    	cells.add(assetAssigneeDTO.apLicenceForUse);
    	    	cells.add(assetAssigneeDTO.partNumber);
    		}
    		else if(asset_categoryDTO.iD == Asset_categoryDTO.DATA_CENTRE)
    		{
    	    	cells.add(asset_modelDTO.mac);    	  
    	    	cells.add(asset_modelDTO.location);
    	    	cells.add(asset_modelDTO.partNumber);
    		}
    		else if(asset_categoryDTO.iD == Asset_categoryDTO.SWITCH)
    		{
    			cells.add(asset_modelDTO.mac);    	  
    	    	cells.add(asset_modelDTO.location);
    	    	cells.add(asset_modelDTO.hostName);    	  
    	    	cells.add(asset_modelDTO.managementVlan);
    	    	cells.add(asset_modelDTO.ip);
    	    	cells.add(asset_modelDTO.partNumber);
    		}
    		else if(asset_categoryDTO.iD == Asset_categoryDTO.NETWORK_ACCESSORIES || asset_categoryDTO.iD == Asset_categoryDTO.CAMERA_AND_MONITORING)
    		{
    	    	cells.add(asset_modelDTO.quantity + "");    	  
    	    	cells.add(asset_modelDTO.location);
    	    	cells.add(asset_modelDTO.partNumber);
    		}
    		
    		
    		if(asset_categoryDTO.isAssignable && asset_categoryDTO.iD != Asset_categoryDTO.ACCESS_POINT && asset_categoryDTO.iD != Asset_categoryDTO.UPS)
    		{
    			cells.add(WorkflowController.getNameFromOrganogramId(assetAssigneeDTO.assignedOrganogramId, "English"));
    	    	cells.add(OfficeUnitOrganogramsRepository.getInstance().getDesignation("English", assetAssigneeDTO.assignedOrganogramId));
    	    	cells.add(WorkflowController.getOfficeTypeNameFromOrganogramId(assetAssigneeDTO.assignedOrganogramId, "English"));
    	    	cells.add(WorkflowController.getWingNameFromOrganogramId(assetAssigneeDTO.assignedOrganogramId, "English"));
    	    	cells.add(assetAssigneeDTO.deliveryDate > 0 ? simpleDateFormat.format(new Date(assetAssigneeDTO.deliveryDate)): "");
    		}
    		
    		cells.add(CatDAO.getName("English", "office_type", asset_modelDTO.assetSupplierType));
        	cells.add(asset_modelDTO.poNumber);
        	cells.add(asset_modelDTO.receivingDate > 0 ? simpleDateFormat.format(new Date(asset_modelDTO.receivingDate)): "");
        	cells.add(asset_modelDTO.receivingDate > 0 ? simpleDateFormat.format(new Date(asset_modelDTO.expiryDate)): "");
        	cells.add(CatDAO.getName("English", "procurement_type", asset_modelDTO.procurementTypeCat));
        	cells.add(asset_modelDTO.projectName);
        	if(asset_categoryDTO.iD == Asset_categoryDTO.NETWORK_ACCESSORIES || asset_categoryDTO.iD == Asset_categoryDTO.CAMERA_AND_MONITORING)
    		{
        		cells.add(asset_modelDTO.costingValue + "");   	  
        		cells.add((asset_modelDTO.costingValue * asset_modelDTO.quantity) + "");
    		}
        	else
        	{
        		cells.add(asset_modelDTO.costingValue + ""); 
        	}
        	
        	if(asset_categoryDTO.isAssignable)
        	{
        		cells.add(CatDAO.getName("English", "delivery_status", assetAssigneeDTO.deliveryStatus));
        		cells.add(assetAssigneeDTO.remarks);
        	}

    	}
    	return cells;
    }
    
    public List<String> getHeaderCells (Asset_modelDTO asset_modelDTO) throws Exception
    {
    	ArrayList<String> headers = new ArrayList <String>();
    	Asset_categoryDTO asset_categoryDTO = asset_categoryDAO.getDTOByID(asset_modelDTO.assetCategoryType);
    	
    	if(asset_categoryDTO != null)
    	{
	    	headers.add("Sl");
	    	headers.add("Category");
	    	if(asset_categoryDTO.iD == Asset_categoryDTO.DATA_CENTRE 
	    			|| asset_categoryDTO.iD == Asset_categoryDTO.CAMERA_AND_MONITORING 
	    			|| asset_categoryDTO.iD == Asset_categoryDTO.SWITCH
	    			|| asset_categoryDTO.iD == Asset_categoryDTO.NETWORK_ACCESSORIES)
    		{
    			headers.add("Product Name");
    		}
	    	if(asset_categoryDTO.iD == Asset_categoryDTO.PRINTER)
    		{
	    		headers.add("Type");
    		}
	    	headers.add("Brand");
	    	headers.add("Model");

    		
        	headers.add("Sl. No.");
    		if(asset_categoryDTO.iD == Asset_categoryDTO.LAPTOP || asset_categoryDTO.iD == Asset_categoryDTO.CPU)
    		{
    			headers.add("Mac");
    	    	headers.add("Processor");
    	    	headers.add("Ram");
    	    	headers.add("HD Type");
    	    	headers.add("HD Size");
    	    	
    	    	if(asset_categoryDTO.iD == Asset_categoryDTO.LAPTOP)
    	    	{
    	    		headers.add("Display Size");
    	    	}
    	    	if(asset_categoryDTO.iD == Asset_categoryDTO.CPU)
    	    	{
    	    		headers.add("IP");
    	    	}
    	    	
    	    	/*headers.add("OS version");
    	    	headers.add("OS License");
    	    	headers.add("OS License No.");

    	    	headers.add("Antivirus Name");
    	    	headers.add("License Expiry Date");
    	    	headers.add("Office Version");
    	    	headers.add("License");
    	    	headers.add("License Key");
    	    	headers.add("Expiry Date");*/
    		}
    		else if(asset_categoryDTO.iD == Asset_categoryDTO.MONITOR)
    		{
    			headers.add("Display Size");
    		}
    		else if(asset_categoryDTO.iD == Asset_categoryDTO.UPS)
    		{
    			headers.add("Capacity");
    			headers.add("IDF Number");
    			headers.add("Delivery Date");
    		}
    		else if(asset_categoryDTO.iD == Asset_categoryDTO.PRINTER)
    		{
    			headers.add("IP");
    		}
    		else if(asset_categoryDTO.iD == Asset_categoryDTO.ACCESS_POINT)
    		{
    			headers.add("Host Name");
    	    	headers.add("IP Address");
    	    	headers.add("MAC");
    	    	headers.add("IDF No.");
    	    	headers.add("Port No.");
    	    	headers.add("User");
    	    	headers.add("Location");
    	    	headers.add("AP Licence for Use");
    	    	headers.add("Part No.");
    		}
    		else if(asset_categoryDTO.iD == Asset_categoryDTO.DATA_CENTRE)
    		{
    	    	headers.add("MAC");    	  
    	    	headers.add("Location");
    	    	headers.add("Part No.");
    		}
    		else if(asset_categoryDTO.iD == Asset_categoryDTO.SWITCH)
    		{
    	    	headers.add("MAC");    	  
    	    	headers.add("Location");
    	    	headers.add("Host Name");    	  
    	    	headers.add("Management VLAN");
    	    	headers.add("IP");
    	    	headers.add("Part No.");
    		}
    		else if(asset_categoryDTO.iD == Asset_categoryDTO.NETWORK_ACCESSORIES || asset_categoryDTO.iD == Asset_categoryDTO.CAMERA_AND_MONITORING)
    		{
    	    	headers.add("Quantity");    	  
    	    	headers.add("Location");
    	    	headers.add("Part No.");
    		}
    		
    		
    		if(asset_categoryDTO.isAssignable && asset_categoryDTO.iD != Asset_categoryDTO.ACCESS_POINT && asset_categoryDTO.iD != Asset_categoryDTO.UPS)
    		{
    			headers.add("Receiver");
    	    	headers.add("Designation");
    	    	headers.add("Office Type");
    	    	headers.add("Wing");
    	    	headers.add("Delivery Date");
    		}
    		
    		headers.add("Vendor");
        	headers.add("PO Number");
        	headers.add("Receiving Date");
        	headers.add("Warranty Period");
        	headers.add("Procurement Type");
        	headers.add("Project Name");
        	if(asset_categoryDTO.iD == Asset_categoryDTO.NETWORK_ACCESSORIES || asset_categoryDTO.iD == Asset_categoryDTO.CAMERA_AND_MONITORING)
    		{
    	    	headers.add("Individual Price");    	  
    	    	headers.add("Total Price");
    		}
        	else
        	{
        		headers.add("Costing Value");
        	}
        	
        	headers.add("Status");
        	headers.add("Remarks");
    	}
    	
    	
    	
    	
    	return headers;
    }
    
    public XSSFCellStyle getHeaderStyle(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.LEFT);
        cellStyle.setBorderTop(BorderStyle.MEDIUM);
        cellStyle.setBorderBottom(BorderStyle.MEDIUM);
        cellStyle.setBorderLeft(BorderStyle.MEDIUM);
        cellStyle.setBorderRight(BorderStyle.MEDIUM);
        //cellStyle.setFillForegroundColor(IndexedColors.CORNFLOWER_BLUE.getIndex());
        //cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        XSSFFont font = wb.createFont();
        font.setFontHeightInPoints((short) 12);
        font.setBold(true);
        cellStyle.setFont(font);
        return cellStyle;
    }
    
    public XSSFCellStyle getTitleStyle(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        //cellStyle.setFillForegroundColor(IndexedColors.CORNFLOWER_BLUE.getIndex());
        //cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        XSSFFont font = wb.createFont();
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);
        cellStyle.setFont(font);
        return cellStyle;
    }
    
    public void autoSizeColumns(XSSFWorkbook workbook) {
        int numberOfSheets = workbook.getNumberOfSheets();
        for (int i = 0; i < numberOfSheets; i++) {
            Sheet sheet = workbook.getSheetAt(i);
            if (sheet.getPhysicalNumberOfRows() > 0) {
                Row row = sheet.getRow(sheet.getFirstRowNum());
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    int columnIndex = cell.getColumnIndex();
                    sheet.autoSizeColumn(columnIndex);
                }
            }
        }
    }
    
    public void getXl(List <Asset_modelDTO> asset_modelDTOs, HttpServletResponse response)
    {
    	XSSFWorkbook wb = new XSSFWorkbook();
        
        
        long lastCat = -2;
        List<String> headers = null;
        Sheet sheet = null;
        int modelIndex = 0;
        String tabName = "";
        for(Asset_modelDTO asset_modelDTO: asset_modelDTOs)
        {
        	if(asset_modelDTO.assetCategoryType != lastCat)
        	{        		
        		tabName = CommonDAO.getName("English", "asset_category", asset_modelDTO.assetCategoryType);
        		if(!tabName.equalsIgnoreCase(""))
        		{
        			modelIndex= 0;
        			sheet = wb.createSheet(tabName);
        			Row headerRow = sheet.createRow(1);
        			try {
						headers = getHeaderCells(asset_modelDTO);
						int cellIndex = 0;
						for(String header: headers)
						{
							Cell cell = headerRow.createCell(cellIndex);
							cell.setCellValue(header);
							cell.setCellStyle(getHeaderStyle(wb));
							
							cellIndex++;
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
        		}
        	}
        	
        	try {
				Asset_categoryDTO asset_categoryDTO = asset_categoryDAO.getDTOByID(asset_modelDTO.assetCategoryType);
				
				if(asset_categoryDTO.isAssignable)
				{
					List <AssetAssigneeDTO> assetAssigneeDTOs = asset_modelDTO.assetAssigneeDTOList;
        			
        			System.out.println("assetAssigneeDTOs size = " + assetAssigneeDTOs.size());
        			for(AssetAssigneeDTO assetAssigneeDTO: assetAssigneeDTOs)
        			{
        				Row normalRow = sheet.createRow(modelIndex + 2);
        				try {
							List<String> normalCells = getNormalCells(asset_modelDTO, assetAssigneeDTO, modelIndex);
							
							int cellIndex = 0;
							for(String normalCell: normalCells)
							{
								Cell cell = normalRow.createCell(cellIndex);
								cell.setCellValue(normalCell);
								
								cellIndex++;
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
        				modelIndex ++;
        			}
				}
				else
				{
					Row normalRow = sheet.createRow(modelIndex + 2);
    				try {
						List<String> normalCells = getNormalCells(asset_modelDTO, new AssetAssigneeDTO(), modelIndex);
						
						int cellIndex = 0;
						for(String normalCell: normalCells)
						{
							Cell cell = normalRow.createCell(cellIndex);
							cell.setCellValue(normalCell);
							
							cellIndex++;
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    				modelIndex ++;
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        	
        	if(asset_modelDTO.assetCategoryType != lastCat)
        	{
				autoSizeColumns(wb);
		        Row row = sheet.createRow(0);
		        Cell cell = row.createCell(0);
		        cell.setCellValue(tabName.toUpperCase());
		        cell.setCellStyle(getTitleStyle(wb));

		        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, headers.size() - 1));
		        lastCat = asset_modelDTO.assetCategoryType;
        	}
        }
        
        try {
        	String fileName =  "AssetReport.xlsx";
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            System.out.println("Writing xl");
			wb.write(response.getOutputStream());
			wb.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // Write workbook to
    }
	
	public void getXl(HttpServletRequest request, HttpServletResponse response) {
		Hashtable hashTable = null;
		hashTable  = commonRequestHandler.fillHashTable(hashTable, request);
		
		List <Asset_modelDTO> asset_modelDTOs = (List<Asset_modelDTO>) asset_modelDAO.getDTOs(hashTable, -1, -1, true, null);
		for(Asset_modelDTO asset_modelDTO: asset_modelDTOs)
		{
			asset_modelDTO.assetAssigneeDTOList = (List<AssetAssigneeDTO>) assetAssigneeDAO.getDTOsByParent("asset_model_id", asset_modelDTO.iD);
		}
		getXl(asset_modelDTOs, response);
		
	}
	
	public void getXlFromAssignee(HttpServletRequest request, HttpServletResponse response) {
		Hashtable hashTable = null;
		hashTable  = commonRequestHandler.fillHashTable(hashTable, request);
		
		List <Asset_modelDTO> asset_modelDTOs = new ArrayList<Asset_modelDTO>();
		List <AssetAssigneeDTO> assetAssigneeDTOs = (List<AssetAssigneeDTO>) assetAssigneeDAO.getDTOs(hashTable, -1, -1, true, null);
		
		for(AssetAssigneeDTO assetAssigneeDTO: assetAssigneeDTOs)
		{
			Asset_modelDTO asset_modelDTO = Asset_modelRepository.getInstance().getAsset_modelDTOByID(assetAssigneeDTO.assetModelId);
			if(asset_modelDTO != null && !asset_modelDTOs.contains(asset_modelDTO))
			{
				asset_modelDTO.assetAssigneeDTOList = new ArrayList<AssetAssigneeDTO>();
				asset_modelDTO.assetAssigneeDTOList.add(assetAssigneeDTO);
				asset_modelDTOs.add(asset_modelDTO);			
			}
			else if(asset_modelDTOs.indexOf(asset_modelDTO) != -1)
			{
				asset_modelDTO = asset_modelDTOs.get(asset_modelDTOs.indexOf(asset_modelDTO));
				asset_modelDTO.assetAssigneeDTOList.add(assetAssigneeDTO);
			}
		}
		getXl(asset_modelDTOs, response);
		
	}
	


}
