package asset_model;


import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import asset_category.Asset_categoryDAO;
import asset_category.Asset_categoryDTO;

import asset_transaction.Asset_transactionDAO;
import asset_transaction.Asset_transactionDTO;
import asset_type.Asset_typeDTO;
import language.LM;
import sessionmanager.SessionConstants;
import software_type.SoftwareSubtypeDAO;

import user.UserDTO;
import workflow.WorkflowController;


public class AssetAssignmentHadler {
	Asset_modelDAO asset_modelDAO;
	AssetAssigneeDAO assetAssigneeDAO;
	Asset_categoryDAO asset_categoryDAO;
    Asset_transactionDAO asset_transactionDAO;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    
    public AssetAssignmentHadler(Asset_modelDAO asset_modelDAO, AssetAssigneeDAO assetAssigneeDAO,
    		Asset_categoryDAO asset_categoryDAO, Asset_transactionDAO asset_transactionDAO) 
	{
       this.asset_categoryDAO = asset_categoryDAO;
       this.asset_modelDAO = asset_modelDAO;
       this.asset_transactionDAO = asset_transactionDAO;
       this.assetAssigneeDAO = assetAssigneeDAO;
    }
    
    public AssetAssigneeDTO assignAsset(AssetAssigneeDTO assetAssigneeDTO, long orgId, long assignmentDate,
    		Asset_modelDTO asset_modelDTO, List<Long> idsToAssign,
    		int roomNoCat, long officeAssetOfficeId, int ownerCat) throws Exception
    {
    	System.out.println("assignAsset 1 " + assetAssigneeDTO.iD);
    	if(assetAssigneeDTO.parentAssigneeId != -1 && !idsToAssign.contains(assetAssigneeDTO.parentAssigneeId)) //accessory or software, but user does not have its parent
    	{
    		return null;
    	}
    	System.out.println("assignAsset 2 " + assetAssigneeDTO.iD);
    	
    	assetAssigneeDTO.responsibleOrganogramId = orgId;
    	assetAssigneeDTO.roomNoCat = roomNoCat;
    	assetAssigneeDTO.ownerCat = ownerCat;
    	assetAssigneeDTO.officeAssetOfficeId = officeAssetOfficeId;
    	
    	if(assetAssigneeDTO.ownerCat == AssetAssigneeDTO.OWNER_PERSON)
    	{
    		assetAssigneeDTO.assignedOrganogramId = assetAssigneeDTO.responsibleOrganogramId;
    	}
    	else if (assetAssigneeDTO.ownerCat == AssetAssigneeDTO.OWNER_ROOM)
    	{
    		assetAssigneeDTO.assignedOrganogramId = assetAssigneeDTO.roomNoCat + SessionConstants.ROOM_OFFSET;
    	}
    	else if (assetAssigneeDTO.ownerCat == AssetAssigneeDTO.OWNER_OFFICE)
    	{
    		assetAssigneeDTO.assignedOrganogramId = assetAssigneeDTO.officeAssetOfficeId + SessionConstants.OFFICE_OFFSET;
    	}
    	
    	
    	assetAssigneeDTO.employeeRecordId = WorkflowController.getEmployeeRecordIDFromOrganogramID(orgId);
		assetAssigneeDTO.assignmentDate = assignmentDate;
		
		assetAssigneeDTO.assignmentStatus = AssetAssigneeDTO.ASSIGNED;
						
		AssetAssigneeDTO oldAssetAssigneeDTO = (AssetAssigneeDTO)assetAssigneeDAO.getDTOByID(assetAssigneeDTO.iD);
		assetAssigneeDAO.update(assetAssigneeDTO);
		if(oldAssetAssigneeDTO.sl != assetAssigneeDTO.sl)
		{
			asset_transactionDAO.updateSL(assetAssigneeDTO);
		}
		if(oldAssetAssigneeDTO.assignmentStatus == AssetAssigneeDTO.NOT_ASSIGNED && assetAssigneeDTO.assignmentStatus == AssetAssigneeDTO.ASSIGNED)
		{
			Asset_transactionDTO asset_transactionDTO = new Asset_transactionDTO(asset_modelDTO, assetAssigneeDTO, Asset_transactionDTO.ASSIGN);
			asset_transactionDAO.add(asset_transactionDTO);
		}

		if(assetAssigneeDTO.assetCategoryType == Asset_categoryDTO.SOFTWARE)
		{
			SoftwareSubtypeDAO.getInstance().assign(assetAssigneeDTO.softwareSubTypeId, assetAssigneeDTO.assignedOrganogramId);
		}
		return assetAssigneeDTO;
    }
	
	public AssetAssigneeDTO assignAsset(HttpServletRequest request, int index, String language,
			long orgId, long assignmentDate, List<Long> idsToAssign,
			int roomNoCat, long officeAssetOfficeId, int ownerCat) throws Exception
	{
		AssetAssigneeDTO assetAssigneeDTO = (AssetAssigneeDTO)assetAssigneeDAO.getDTOByID(Long.parseLong(request.getParameterValues("assetAssignee.iD")[index]));
		if(assetAssigneeDTO == null)
		{
			return null;
		}
		Asset_modelDTO asset_modelDTO = asset_modelDAO.getDTOByID(assetAssigneeDTO.assetModelId, false);
		if(asset_modelDTO == null)
		{
			return null;
		}
		readBasicFields(request, index, language, assetAssigneeDTO);
		assetAssigneeDTO = assignAsset(assetAssigneeDTO, orgId, assignmentDate, asset_modelDTO, idsToAssign,
				roomNoCat, officeAssetOfficeId, ownerCat);
		return assetAssigneeDTO;
	}
	
	public void unassignAsset(AssetAssigneeDTO oldUserAsset, boolean detachFromParent)
	{
		if(oldUserAsset.assignedOrganogramId == -1) //already unassigned
		{
			return;
		}
		oldUserAsset.assignmentStatus = AssetAssigneeDTO.NOT_ASSIGNED;
		oldUserAsset.assignedOrganogramId = -1;
		oldUserAsset.employeeRecordId = -1;
		oldUserAsset.roomNoCat = -1;
		oldUserAsset.responsibleOrganogramId = -1;
		oldUserAsset.officeAssetOfficeId = -1;
		if(detachFromParent)
		{
			oldUserAsset.parentAssigneeId = -1;
		}
		try {
			assetAssigneeDAO.update(oldUserAsset);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Asset_modelDTO asset_modelDTO = asset_modelDAO.getDTOByID(oldUserAsset.assetModelId, false);
		Asset_transactionDTO asset_transactionDTO = new Asset_transactionDTO(asset_modelDTO, oldUserAsset, Asset_transactionDTO.UNASSIGN);
		try {
			asset_transactionDAO.add(asset_transactionDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(oldUserAsset.assetCategoryType == Asset_categoryDTO.SOFTWARE)
		{
			SoftwareSubtypeDAO.getInstance().unassign(oldUserAsset.softwareSubTypeId);
		}
	}
	
	
	
	public void massAssign(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception
	{
		long orgId = Long.parseLong(request.getParameter("orgId"));
		int roomNoCat = Integer.parseInt(request.getParameter("roomNoCat"));
		int ownerCat = Integer.parseInt(request.getParameter("ownerCat"));
		long officeAssetOfficeId = -1;
		if(ownerCat == AssetAssigneeDTO.OWNER_OFFICE)
		{
			officeAssetOfficeId = WorkflowController.getOfficeIdFromOrganogramId(orgId);
		}
		
		System.out.println("orgId = " + orgId);
	
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		long assignmentDate = System.currentTimeMillis();
		try {
			assignmentDate = f.parse(request.getParameter("assignmentDate")).getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String language = LM.getLanguage(userDTO);

		String[] newAssetIdsStr = request.getParameterValues("assetAssignee.iD");
		int assetAssigneeItemNo = 0;
		if(newAssetIdsStr!= null)
		{
			assetAssigneeItemNo = newAssetIdsStr.length;
		}
		List<Long> newUserAssetIds = new ArrayList<Long> ();
		for(int index=0;index<assetAssigneeItemNo;index++)
		{
			newUserAssetIds.add(Long.parseLong(newAssetIdsStr[index]));
		}
		
		


		List<AssetAssigneeDTO> oldUserAssets = assetAssigneeDAO.getByOrganogramId(orgId);
		List<AssetAssigneeDTO> assetsToRemove = new ArrayList<AssetAssigneeDTO>();
		List<AssetAssigneeDTO> assetsToRetain = new ArrayList<AssetAssigneeDTO>();
		if(oldUserAssets != null)
		{
			for(AssetAssigneeDTO oldUserAsset: oldUserAssets)
			{
				
				boolean foundAsset = false;
				if(newAssetIdsStr != null)
				{
					for(long newAssetId: newUserAssetIds)
					{
						if(newAssetId == oldUserAsset.iD)
						{
							foundAsset = true;
						}
					}
				}
			
				if(!foundAsset)
				{
					assetsToRemove.add(oldUserAsset);
				}
				else
				{
					assetsToRetain.add(oldUserAsset);
				}
				
			}
		}
		
		for(AssetAssigneeDTO asset: assetsToRemove)
		{
            unassignAsset(asset, !assigneeListContainsId(assetsToRemove, asset.parentAssigneeId));
		}

		
		for(int index=0;index<assetAssigneeItemNo;index++){
			AssetAssigneeDTO assetAssigneeDTO = assignAsset(request, index, language, orgId, assignmentDate, newUserAssetIds,
					roomNoCat, officeAssetOfficeId, ownerCat);
		
			
			if(assetAssigneeDTO != null && assetAssigneeDTO.assetTypeId == Asset_typeDTO.HARDWARE 
					&& !assigneeListContainsId(assetsToRetain, assetAssigneeDTO.iD)) //new hardware assignment, add all his kids
			{
				System.out.println("Adding children of " + assetAssigneeDTO.iD);
				List<AssetAssigneeDTO> children = assetAssigneeDAO.getChildAssets(assetAssigneeDTO.iD);
				if(children != null)
				{
					for(AssetAssigneeDTO child: children)
					{
						Asset_modelDTO asset_modelDTO = asset_modelDAO.getDTOByID(child.assetModelId, false);
						assignAsset(child, orgId, assignmentDate, asset_modelDTO, newUserAssetIds, roomNoCat, officeAssetOfficeId, ownerCat);
					}
				}
			}
			else
			{
				System.out.println("Not Adding children of " + assetAssigneeDTO.iD);
			}

			
		}
			
		
		request.setAttribute("organogramId", orgId);
		request.setAttribute("ownerCat", ownerCat);
		request.setAttribute("roomNoCat", roomNoCat);
		request.getRequestDispatcher("asset_model/userAssets.jsp" ).forward(request, response);	
	}
	
	public boolean assigneeListContainsId(List<AssetAssigneeDTO> assetAssigneeDTOs, long id)
	{
		if(assetAssigneeDTOs == null)
		{
			return false;
		}
		for(AssetAssigneeDTO assetAssigneeDTO: assetAssigneeDTOs)
		{
			if(assetAssigneeDTO.iD == id)
			{
				return true;
			}
		}
		return false;
	}
	
	public AssetAssigneeDTO getAssetListWithId(List<AssetAssigneeDTO> assetAssigneeDTOs, long id)
	{
		if(assetAssigneeDTOs == null)
		{
			return null;
		}
		for(AssetAssigneeDTO assetAssigneeDTO: assetAssigneeDTOs)
		{
			if(assetAssigneeDTO.iD == id)
			{
				return assetAssigneeDTO;
			}
		}
		return null;
	}
	
	
	
	public void readBasicFields(HttpServletRequest request, int index, String language, AssetAssigneeDTO assetAssigneeDTO) throws Exception
	{
		String Value = request.getParameterValues("assetAssignee.sl")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.sl = (Value);
		}
		
		Value = request.getParameterValues("assetAssignee.parentAssigneeId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.parentAssigneeId = Long.parseLong(Value);
			if(assetAssigneeDTO.parentAssigneeId != -1)
			{
				AssetAssigneeDTO parentDTO = (AssetAssigneeDTO) assetAssigneeDAO.getDTOByID(assetAssigneeDTO.parentAssigneeId);
				if(parentDTO != null)
				{
					assetAssigneeDTO.parentModelName = parentDTO.modelName;
					if(!parentDTO.sl.equalsIgnoreCase(""))
					{
						assetAssigneeDTO.parentModelName += ": " + parentDTO.sl;
					}
				}
			}
		}

		Value = request.getParameterValues("assetAssignee.mac")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.mac = (Value);
		}
		
		Value = request.getParameterValues("assetAssignee.activeDirectoryId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.activeDirectoryId = (Value);
		}
		
		Value = request.getParameterValues("assetAssignee.mkBoxNumber")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.mkBoxNumber = (Value);
		}
		

		
		
		Value = request.getParameterValues("assetAssignee.ramCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.ramCat = Integer.parseInt(Value);
		}
		
		Value = request.getParameterValues("assetAssignee.ramSizeCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.ramSizeCat = Integer.parseInt(Value);
		}

		Value = request.getParameterValues("assetAssignee.hdCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.hdCat = Integer.parseInt(Value);
		}
				
		Value = request.getParameterValues("assetAssignee.hdSizeCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.hdSizeCat = Integer.parseInt(Value);
		}
		
		Value = request.getParameterValues("assetAssignee.pcName")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.pcName = (Value);
		}
		
		Value = request.getParameterValues("assetAssignee.processorCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.processorCat = Integer.parseInt(Value);
		}
		
		Value = request.getParameterValues("assetAssignee.processorGenCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.processorGenCat = Integer.parseInt(Value);
		}

		Value = request.getParameterValues("assetAssignee.ip")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.ip = (Value);
		}

		
		assetAssigneeDTO.idfNumber = (Value);
		Value = request.getParameterValues("assetAssignee.idfNumber")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.idfNumber = (Value);
		}
		assetAssigneeDTO.host = (Value);
		Value = request.getParameterValues("assetAssignee.host")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.host = (Value);
		}
		assetAssigneeDTO.location = (Value);
		Value = request.getParameterValues("assetAssignee.location")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.location = (Value);
		}
		
		assetAssigneeDTO.apLicenceForUse = (Value);
		Value = request.getParameterValues("assetAssignee.apLicenceForUse")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.apLicenceForUse = (Value);
		}

		Value = request.getParameterValues("assetAssignee.port")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.port = (Value);
		}
		
	}

	public void transfer(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
		long fromOrgId = Long.parseLong(request.getParameter("fromOrgId"));
		long toOrgId = Long.parseLong(request.getParameter("toOrgId"));
		
		List<AssetAssigneeDTO> assetAssigneeDTOs = assetAssigneeDAO.getAllResponsible(fromOrgId);
		if(assetAssigneeDTOs != null)
		{
			for(AssetAssigneeDTO assetAssigneeDTO: assetAssigneeDTOs)
			{
				Asset_transactionDTO asset_transactionDTO = new Asset_transactionDTO(assetAssigneeDTO, fromOrgId, toOrgId);
				try {
					asset_transactionDAO.add(asset_transactionDTO);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		assetAssigneeDAO.transfer(fromOrgId, toOrgId);
		request.setAttribute("organogramId", toOrgId);

		try {
			request.getRequestDispatcher("asset_model/userAssets.jsp" ).forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
	}
	

	
	
	
	/*public void assign(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
		
		Asset_modelDTO asset_modelDTO = (Asset_modelDTO)asset_modelDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
		String language = LM.getLanguage(userDTO);
		
		try {
			List<AssetAssigneeDTO> reqAssetAssigneeDTOList = createAssetAssigneeDTOListByRequest(request, language, asset_modelDTO);
			if(asset_modelDTO == null || reqAssetAssigneeDTOList.size() > assetAssigneeDAO.getActiveCount(asset_modelDTO.iD))
			{
				throw new Exception(LM.getText(LC.ASSET_MODEL_ADD_QUANTITY ,language) + " " + ErrorMessage.getInvalidMessage(language));
			}
			
			List<AssetAssigneeDTO> assignedAssetList = assetAssigneeDAO.getAssigned(asset_modelDTO.iD);
			if(assignedAssetList != null)
			{				
				for(AssetAssigneeDTO existingDTO: assignedAssetList)
				{
					boolean notInRequest = true;
					for(AssetAssigneeDTO reqDTO: reqAssetAssigneeDTOList) //remove if not in req
					{
						if(existingDTO.iD == reqDTO.iD)
						{
							notInRequest = false;
							break;
						}
					}
					if(notInRequest)
					{
						long existingId = existingDTO.iD;
						String sl = existingDTO.sl;
						existingDTO = new AssetAssigneeDTO(asset_modelDTO);
						existingDTO.iD = existingId;
						existingDTO.sl = sl;
						assetAssigneeDAO.update(existingDTO);
						Asset_transactionDTO asset_transactionDTO = new Asset_transactionDTO(asset_modelDTO, existingDTO, Asset_transactionDTO.UNASSIGN);
						asset_transactionDAO.add(asset_transactionDTO);
					}
				}
			}
			
			Asset_categoryDTO asset_categoryDTO = Asset_categoryRepository.getInstance().getAsset_categoryDTOByID(asset_modelDTO.assetCategoryType);
			if(asset_categoryDTO == null || asset_categoryDTO.isAssignable == false)
			{
				throw new Exception(LM.getText(LC.ASSET_MODEL_ADD_ASSETCATEGORYTYPE ,language) + " " + ErrorMessage.getInvalidMessage(language));

			}
			
			
			for(AssetAssigneeDTO assetAssigneeDTO: reqAssetAssigneeDTOList)
			{
				boolean canUpdate = false;
				AssetAssigneeDTO oldAssetAssigneeDTO = assetAssigneeDAO.getByIdAndParentId(assetAssigneeDTO.iD, asset_modelDTO.iD);
				System.out.println("assetAssigneeDTO.iD = " + assetAssigneeDTO.iD + " asset_modelDTO.iD = " + asset_modelDTO.iD + " oldAssetAssigneeDTO = " + oldAssetAssigneeDTO);
				
				if(oldAssetAssigneeDTO != null)
				{
					canUpdate = true;
				}
				else if (assetAssigneeDTO.iD == -1)
				{
					System.out.println("assetAssigneeDTO.iD == -1");
					AssetAssigneeDTO unassigned1stDTO = assetAssigneeDAO.get1stUnassigned(asset_modelDTO.iD);
					if(unassigned1stDTO == null)
					{
						throw new Exception(ErrorMessage.getGenericInvalidMessage(language));
					}
					assetAssigneeDTO.iD = unassigned1stDTO.iD;
					canUpdate = true;				
				}
				else
				{
					System.out.println("assetAssigneeDTO.iD is invalid");
					throw new Exception(ErrorMessage.getGenericInvalidMessage(language));
				}
				
				if(canUpdate)
				{
					assetAssigneeDAO.update(assetAssigneeDTO);
					if(oldAssetAssigneeDTO != null && (oldAssetAssigneeDTO.assignedOrganogramId != assetAssigneeDTO.assignedOrganogramId 
							|| !oldAssetAssigneeDTO.receiverNameEn.equalsIgnoreCase(assetAssigneeDTO.receiverNameEn)))
					{
						Asset_transactionDTO asset_transactionDTO = new Asset_transactionDTO(asset_modelDTO, oldAssetAssigneeDTO, Asset_transactionDTO.UNASSIGN);
						asset_transactionDAO.add(asset_transactionDTO);
					}
					
					if(assetAssigneeDTO.assignmentStatus == AssetAssigneeDTO.ASSIGNED && 
							(oldAssetAssigneeDTO == null || oldAssetAssigneeDTO.assignedOrganogramId != assetAssigneeDTO.assignedOrganogramId 
							|| !oldAssetAssigneeDTO.receiverNameEn.equalsIgnoreCase(assetAssigneeDTO.receiverNameEn)))
					{
						Asset_transactionDTO asset_transactionDTO = new Asset_transactionDTO(asset_modelDTO, assetAssigneeDTO, Asset_transactionDTO.ASSIGN);
						asset_transactionDAO.add(asset_transactionDTO);
					}
					asset_transactionDAO.updateSL(assetAssigneeDTO);

				}
			}
			
			if(!asset_modelDTO.isImmutable)
			{
				asset_modelDTO.isImmutable = true;
				asset_modelDAO.update(asset_modelDTO);
			}
			
			
			ApiResponse.sendSuccessResponse(response, "Asset_modelServlet?actionType=search");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				ApiResponse.sendErrorResponse(response, e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		// TODO Auto-generated method stub
		
	}*/
	
	/*public List<AssetAssigneeDTO> createAssetAssigneeDTOListByRequest(HttpServletRequest request, String language, Asset_modelDTO asset_modelDTO) throws Exception{ 
		List<AssetAssigneeDTO> assetAssigneeDTOList = new ArrayList<AssetAssigneeDTO>();
		if(request.getParameterValues("assetAssignee.iD") != null) 
		{
			int assetAssigneeItemNo = request.getParameterValues("assetAssignee.iD").length;
			
			
			for(int index=0;index<assetAssigneeItemNo;index++){
				AssetAssigneeDTO assetAssigneeDTO = createAssetAssigneeDTOByRequestAndIndex(request,false,index, language, asset_modelDTO);
				assetAssigneeDTOList.add(assetAssigneeDTO);
			}
			
			return assetAssigneeDTOList;
		}
		return null;
	}*/

	
	
	/*public AssetAssigneeDTO createAssetAssigneeDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, String language, Asset_modelDTO asset_modelDTO) throws Exception{
	
		AssetAssigneeDTO assetAssigneeDTO;
		if(addFlag == true )
		{
			assetAssigneeDTO = new AssetAssigneeDTO();
		}
		else
		{
			assetAssigneeDTO = (AssetAssigneeDTO)assetAssigneeDAO.getDTOByID(Long.parseLong(request.getParameterValues("assetAssignee.iD")[index]));
		}
			
		String Value = "";
		
		if(assetAssigneeDTO == null)
		{
			assetAssigneeDTO = new AssetAssigneeDTO(asset_modelDTO);
		}
		
		
		Value = request.getParameterValues("assetAssignee.deliveryStatus")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		else
		{
			throw new Exception(ErrorMessage.getGenericInvalidMessage(language));
		}
		assetAssigneeDTO.deliveryStatus = Integer.parseInt(Value);
	
		
		readBasicFields(request, index, language, assetAssigneeDTO);


		Value = request.getParameterValues("assetAssignee.assignedOrganogramId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		else
		{
			throw new Exception(LM.getText(LC.ASSET_MODEL_ADD_ASSET_ASSIGNEE_ASSIGNEDORGANOGRAMID, language) + " " + ErrorMessage.getInvalidMessage(language));
		}		
		assetAssigneeDTO.assignedOrganogramId = Long.parseLong(Value);
		readUserFields(request, index, language, assetAssigneeDTO);
		
		Value = request.getParameterValues("assetAssignee.receiverNameEn")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.receiverNameEn = (Value); //not empty value will overide the default
			assetAssigneeDTO.receiverNameBn = (Value); //not empty value will overide the default
		}
		
	
		
		Value = request.getParameterValues("assetAssignee.officeTypeCat")[index];

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.officeTypeCat = Integer.parseInt(Value);
		}


		if(assetAssigneeDTO.assignedOrganogramId == -1 && assetAssigneeDTO.receiverNameEn.equalsIgnoreCase(""))
		{
			assetAssigneeDTO.assignmentStatus = AssetAssigneeDTO.NOT_ASSIGNED;
		}
		else
		{
			assetAssigneeDTO.assignmentStatus = AssetAssigneeDTO.ASSIGNED;
		}
		
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		Value = request.getParameterValues("assetAssignee.deliveryDate")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		try 
		{
			Date d = f.parse(Value);
			assetAssigneeDTO.deliveryDate = d.getTime();
			System.out.println("found deliveryDate = " + Value);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		Value = request.getParameterValues("assetAssignee.assignmentDate")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		try 
		{
			Date d = f.parse(Value);
			System.out.println("found assignment date = " + Value);
			assetAssigneeDTO.assignmentDate = d.getTime();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		if(assetAssigneeDTO.deliveryDate > assetAssigneeDTO.assignmentDate)
		{
			throw new Exception(LM.getText(LC.HM_ASSIGNMENT_DATE, language) + " " + ErrorMessage.getInvalidMessage(language));

		}
		
		Value = request.getParameterValues("assetAssignee.remarks")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		else
		{
			throw new Exception(LM.getText(LC.ASSET_MODEL_ADD_ASSET_ASSIGNEE_REMARKS, language) + " " + ErrorMessage.getInvalidMessage(language));
		}

		assetAssigneeDTO.remarks = (Value);
		return assetAssigneeDTO;
	
	}*/
	
	/*public void readBasicSofts(HttpServletRequest request, int index, String language, AssetAssigneeDTO assetAssigneeDTO) throws Exception
	{
		String Value = request.getParameterValues("assetAssignee.osLicenceCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.osLicenceCat = Integer.parseInt(Value);
		}
		
		Value = request.getParameterValues("assetAssignee.osVersionCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.osVersionCat = Integer.parseInt(Value);			
		}
		
		Value = request.getParameterValues("assetAssignee.osKeyId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.osKeyId = Long.parseLong(Value);
			assetAssigneeDTO.osKey = SoftwareSubtypeRepository.getInstance().getKey(assetAssigneeDTO.osKeyId);
		}

		Value = request.getParameterValues("assetAssignee.antivirusLicenceCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.antivirusLicenceCat = Integer.parseInt(Value);
		}
		
		Value = request.getParameterValues("assetAssignee.antivirusVersionCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.antivirusVersionCat = Integer.parseInt(Value);			
		}
		
		Value = request.getParameterValues("assetAssignee.antivirusKeyId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.antivirusKeyId = Long.parseLong(Value);
			assetAssigneeDTO.antivirusKey = SoftwareSubtypeRepository.getInstance().getKey(assetAssigneeDTO.antivirusKeyId);
		}
		

		
		Value = request.getParameterValues("assetAssignee.officeLicenceCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.officeLicenceCat = Integer.parseInt(Value);
		}
		
		Value = request.getParameterValues("assetAssignee.officeVersionCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.officeVersionCat = Integer.parseInt(Value);			
		}
		
		Value = request.getParameterValues("assetAssignee.officeKeyId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			assetAssigneeDTO.officeKeyId = Long.parseLong(Value);
			assetAssigneeDTO.officeKey = SoftwareSubtypeRepository.getInstance().getKey(assetAssigneeDTO.officeKeyId);
		}
	}*/
	
	/*public void reAssignSoftwares(AssetAssigneeDTO oldAssetAssigneeDTO, AssetAssigneeDTO assetAssigneeDTO)
	{
		if(assetAssigneeDTO.assetCategoryType != Asset_categoryDTO.LAPTOP && assetAssigneeDTO.assetCategoryType != Asset_categoryDTO.CPU)
		{
			return;
		}
		if(oldAssetAssigneeDTO == null)
		{
			//Won't assign if keyId = -1, safe
			SoftwareSubtypeDAO.getInstance().assign(assetAssigneeDTO.osKeyId, assetAssigneeDTO.assignedOrganogramId);
			SoftwareSubtypeDAO.getInstance().assign(assetAssigneeDTO.antivirusKeyId, assetAssigneeDTO.assignedOrganogramId);
			SoftwareSubtypeDAO.getInstance().assign(assetAssigneeDTO.officeKeyId, assetAssigneeDTO.assignedOrganogramId);			
		}
		else
		{
			reassignByKeyId(oldAssetAssigneeDTO.osKeyId, assetAssigneeDTO.osKeyId, assetAssigneeDTO.assignedOrganogramId);
			reassignByKeyId(oldAssetAssigneeDTO.officeKeyId, assetAssigneeDTO.officeKeyId, assetAssigneeDTO.assignedOrganogramId);
			reassignByKeyId(oldAssetAssigneeDTO.antivirusKeyId, assetAssigneeDTO.antivirusKeyId, assetAssigneeDTO.assignedOrganogramId);
			
		}
	}
	
	public void reassignByKeyId(long oldId, long newId, long orgId)
	{
		if(oldId == newId)
		{
			return;
		}
		if(oldId == -1 && newId != -1)
		{
			SoftwareSubtypeDAO.getInstance().assign(newId, orgId);
		}
		else if(oldId != -1 && newId == -1)
		{
			SoftwareSubtypeDAO.getInstance().unassign(oldId);
		}
		else
		{
			SoftwareSubtypeDAO.getInstance().unassign(oldId);
			SoftwareSubtypeDAO.getInstance().assign(newId, orgId);
		}
	}
	
	public void unassignSoftwares(AssetAssigneeDTO assetAssigneeDTO)
	{
		if(assetAssigneeDTO.assetCategoryType != Asset_categoryDTO.LAPTOP && assetAssigneeDTO.assetCategoryType != Asset_categoryDTO.CPU)
		{
			return;
		}
		SoftwareSubtypeDAO.getInstance().unassign(assetAssigneeDTO.osKeyId);
		SoftwareSubtypeDAO.getInstance().unassign(assetAssigneeDTO.antivirusKeyId);
		SoftwareSubtypeDAO.getInstance().unassign(assetAssigneeDTO.officeKeyId);		
	}*/

}
