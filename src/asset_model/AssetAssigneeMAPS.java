package asset_model;
import java.util.*; 
import util.*;


public class AssetAssigneeMAPS extends CommonMaps
{	
	public AssetAssigneeMAPS(String tableName)
	{
		


		java_SQL_map.put("asset_model_id".toLowerCase(), "assetModelId".toLowerCase());
		java_SQL_map.put("asset_category_type".toLowerCase(), "assetCategoryType".toLowerCase());
		java_SQL_map.put("brand_type".toLowerCase(), "brandType".toLowerCase());
		java_SQL_map.put("model".toLowerCase(), "model".toLowerCase());
		java_SQL_map.put("sl".toLowerCase(), "sl".toLowerCase());
		java_SQL_map.put("mac".toLowerCase(), "mac".toLowerCase());
		java_SQL_map.put("ip".toLowerCase(), "ip".toLowerCase());
		java_SQL_map.put("os_licencee_key".toLowerCase(), "osLicenceeKey".toLowerCase());
		java_SQL_map.put("antivirus_licencee_key".toLowerCase(), "antivirusLicenceeKey".toLowerCase());
		java_SQL_map.put("office_licencee_key".toLowerCase(), "officeLicenceeKey".toLowerCase());
		java_SQL_map.put("office_unit_type".toLowerCase(), "officeUnitType".toLowerCase());
		java_SQL_map.put("wing_type".toLowerCase(), "wingType".toLowerCase());
		java_SQL_map.put("assigned_user_id".toLowerCase(), "assignedUserId".toLowerCase());
		java_SQL_map.put("assigned_organogram_id".toLowerCase(), "assignedOrganogramId".toLowerCase());
		java_SQL_map.put("receiver_name_en".toLowerCase(), "receiverNameEn".toLowerCase());
		java_SQL_map.put("receiver_name_bn".toLowerCase(), "receiverNameBn".toLowerCase());
		java_SQL_map.put("delivery_date".toLowerCase(), "deliveryDate".toLowerCase());
		java_SQL_map.put("costing_value".toLowerCase(), "costingValue".toLowerCase());
		java_SQL_map.put("delivery_status".toLowerCase(), "deliveryStatus".toLowerCase());
		java_SQL_map.put("remarks".toLowerCase(), "remarks".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Asset Model Id".toLowerCase(), "assetModelId".toLowerCase());
		java_Text_map.put("Asset Category".toLowerCase(), "assetCategoryType".toLowerCase());
		java_Text_map.put("Brand".toLowerCase(), "brandType".toLowerCase());
		java_Text_map.put("Model".toLowerCase(), "model".toLowerCase());
		java_Text_map.put("Sl".toLowerCase(), "sl".toLowerCase());
		java_Text_map.put("Mac".toLowerCase(), "mac".toLowerCase());
		java_Text_map.put("Ip".toLowerCase(), "ip".toLowerCase());
		java_Text_map.put("Os Licencee Key".toLowerCase(), "osLicenceeKey".toLowerCase());
		java_Text_map.put("Antivirus Licencee Key".toLowerCase(), "antivirusLicenceeKey".toLowerCase());
		java_Text_map.put("Office Licencee Key".toLowerCase(), "officeLicenceeKey".toLowerCase());
		java_Text_map.put("Office Unit".toLowerCase(), "officeUnitType".toLowerCase());
		java_Text_map.put("Wing".toLowerCase(), "wingType".toLowerCase());
		java_Text_map.put("Assigned User Id".toLowerCase(), "assignedUserId".toLowerCase());
		java_Text_map.put("Assigned Organogram Id".toLowerCase(), "assignedOrganogramId".toLowerCase());
		java_Text_map.put("Receiver Name En".toLowerCase(), "receiverNameEn".toLowerCase());
		java_Text_map.put("Receiver Name Bn".toLowerCase(), "receiverNameBn".toLowerCase());
		java_Text_map.put("Delivery Date".toLowerCase(), "deliveryDate".toLowerCase());
		java_Text_map.put("Costing Value".toLowerCase(), "costingValue".toLowerCase());
		java_Text_map.put("Delivery Status".toLowerCase(), "deliveryStatus".toLowerCase());
		java_Text_map.put("Remarks".toLowerCase(), "remarks".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}