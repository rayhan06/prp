package asset_model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import java.sql.Statement;

import asset_category.Asset_categoryDTO;
import asset_category.Asset_categoryRepository;
import asset_transaction.Asset_transactionDAO;
import asset_transaction.Asset_transactionDTO;
import brand.BrandRepository;
import common.ConnectionAndStatementUtil;
import employee_offices.EmployeeOfficeRepository;
import pb.CatDAO;
import pb.CommonDAO;
import pb_notifications.Pb_notificationsDAO;
import sessionmanager.SessionConstants;

import org.apache.log4j.Logger;

import appointment.KeyCountDTO;
import user.UserDTO;
import user.UserRepository;
import util.*;
import workflow.WorkflowController;


public class AssetAssigneeDAO  extends NavigationService4
{

	Logger logger = Logger.getLogger(AssetAssigneeDAO.class);

	public AssetAssigneeDAO(String tableName)
	{
		super(tableName);
		useSafeSearch = true;
		joinSQL = "";
		columnNames = new String[]
		{
			"ID",
			"asset_model_id",
			"asset_category_type",
			"asset_type_id",

			"ram_cat",
			"ram_size_cat",
			"hd_cat",
			"hd_size_cat",
			"processor_cat",
			"processor_gen_cat",

			"brand_type",
			"model",
			"model_name",
			"parent_model_name",
			"sl",
			"mac",
			"ip",
			/*"os_licence_cat",
			"antivirus_licence_cat",
			"office_licence_cat",*/

			"assigned_organogram_id",
			"purchasing_user_id",
			"purchasing_organogram_id",

			"delivery_date",
			"costing_value",
			"assignment_status",
			"remarks",
			"condemnation_reason",
			"search_column",

			/*"os_key",
			"office_key",
			"antivirus_key",

			"os_key_id",
			"office_key_id",
			"antivirus_key_id",

			"os_version_cat",
			"office_version_cat",
			"antivirus_version_cat",*/

			"idf_number",
			"host",
			"location",
			"ap_license_for_use",
			"part_number",

			"active_directory_id",
			"mk_box_number",

			"port",

			"has_warranty",
			"has_toe",
			"assignment_date",
			"delivery_status",

			"is_active",

			"software_type_id",
			"software_subtype_id",
			"software_cat",
			"software_sub_cat",
			"parent_assignee_id",
			"software_key",
			"expiry_date",
			
			"employee_record_id",

			"parent_categories",
			"pc_name",
			
			"room_no_cat",
			
			"owner_cat",
			"office_asset_office_id",
			"responsible_organogram_id",

			"isDeleted",
			"lastModificationTime"
		};
	}

	public AssetAssigneeDAO()
	{
		this("asset_assignee");
	}

	public void setSearchColumn(AssetAssigneeDTO assetassigneeDTO)
	{
		StringBuilder searchColumn = new StringBuilder();
		Asset_categoryDTO asset_categoryDTO = Asset_categoryRepository.getInstance().getAsset_categoryDTOByID(assetassigneeDTO.assetCategoryType);

		searchColumn.append(asset_categoryDTO.nameEn).append(" ").append(asset_categoryDTO.nameBn).append(assetassigneeDTO.model).append(" ");

		searchColumn.append(BrandRepository.getInstance().getBrandText(assetassigneeDTO.brandType, true)).
				append(" ").append(BrandRepository.getInstance().getBrandText(assetassigneeDTO.brandType, true)).append(" ").append(assetassigneeDTO.sl);

		assetassigneeDTO.searchColumn = searchColumn.toString();

	}

	public String getName(AssetAssigneeDTO assetassigneeDTO)
	{
		if(assetassigneeDTO == null)
		{
			return "";
		}
		String catName = Asset_categoryRepository.getInstance().getCategoryText(assetassigneeDTO.assetCategoryType,true);
		return  catName + ": " + assetassigneeDTO.model;
	}

	public String getName(long id)
	{
		AssetAssigneeDTO assetassigneeDTO;
		try {
			assetassigneeDTO = (AssetAssigneeDTO) getDTOByID(id);
			return getName(assetassigneeDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}

	}
	
	public String getQrContents(AssetAssigneeDTO assetAssigneeDTO)	
	{
		String Language = "english";
		String qrContents = "";		
		String assetValue = "";
		String NL = " \015";
		String justNL = "  \015";
		
		Asset_modelDTO asset_modelDTO = Asset_modelRepository.getInstance().getAsset_modelDTOByID(assetAssigneeDTO.assetModelId);
		
		assetValue = WorkflowController.getOrganogramName(assetAssigneeDTO.assignedOrganogramId, true);
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "Designation: " + assetValue + NL;
		}
		
		assetValue = WorkflowController.getOfficeNameFromOrganogramId(assetAssigneeDTO.assignedOrganogramId, true);
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "Office: " + assetValue + NL;
		}
		
		assetValue = CommonDAO.getName(Language, "asset_category", asset_modelDTO.assetCategoryType);
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "Category: " + assetValue + NL;
		}
		
		assetValue = asset_modelDTO.nameEn;
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "Model: " + assetValue + NL;
		}
		
		assetValue = CatDAO.getName(Language, "assignment_status", assetAssigneeDTO.assignmentStatus);
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "Status: " + assetValue + NL;
		}
		
		if(asset_modelDTO.brandType != -1)
		{
			assetValue = CommonDAO.getName(Language, "brand", asset_modelDTO.brandType);
			if(assetValue != null && !assetValue.equalsIgnoreCase(""))
			{
				qrContents += "Brand: " + assetValue + NL;
			}
		}
		
		if(assetAssigneeDTO.pcName != null && !assetAssigneeDTO.pcName.equalsIgnoreCase(""))
		{
			assetValue = assetAssigneeDTO.pcName;
			if(assetValue != null && !assetValue.equalsIgnoreCase(""))
			{
				qrContents += "PC Name: " + assetValue + NL;
			}
		}
		
		assetValue = assetAssigneeDTO.parentModelName;
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "Attached To: " + assetValue + NL;
		}
		
		assetValue = assetAssigneeDTO.softwareKey;
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "Key: " + assetValue + NL;
		}
		
		if(asset_modelDTO.assetCategoryType == Asset_categoryDTO.PRINTER)
		{
			assetValue = CatDAO.getName(Language, "printer_network_type", asset_modelDTO.printerNetworkTypeCat) 
					+ " " + CatDAO.getName(Language, "printer_type", asset_modelDTO.printerTypeCat);
			if(!assetValue.equalsIgnoreCase(" "))
			{
				qrContents += "Printer Type: " + assetValue + NL;
			}
		}
		
		if(asset_modelDTO.scannerTypeCat != -1 && asset_modelDTO.assetCategoryType == Asset_categoryDTO.SCANNER)
		{
			assetValue = CatDAO.getName(Language, "scanner_type", asset_modelDTO.scannerTypeCat);
			if(assetValue != null && !assetValue.equalsIgnoreCase(""))
			{
				qrContents += "Scanner Type: " + assetValue + NL;
			}
		}
		
		
		assetValue = assetAssigneeDTO.sl;
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "SL: " + assetValue + NL;
		}
		
		assetValue = CatDAO.getName(Language, "processor", assetAssigneeDTO.processorCat) + " " 
				+ CatDAO.getName(Language, "processor_gen", assetAssigneeDTO.processorGenCat);
		if(!assetValue.equalsIgnoreCase(" "))
		{
			qrContents += "Processor: " + assetValue + NL;
		}
		
		assetValue = CatDAO.getName(Language, "ram", assetAssigneeDTO.ramCat) + " " 
				+ CatDAO.getName(Language, "ram_size", assetAssigneeDTO.ramSizeCat);
		if(!assetValue.equalsIgnoreCase(" "))
		{
			qrContents += "RAM: " + assetValue + NL;
		}
		
		assetValue = CatDAO.getName(Language, "hd", assetAssigneeDTO.hdCat) + " " + CatDAO.getName(Language, "hd_size", assetAssigneeDTO.hdSizeCat);
		if(!assetValue.equalsIgnoreCase(" "))
		{
			qrContents += "HDD: " + assetValue + NL;
		}
		
		if(asset_modelDTO.monitorTypeCat != -1 && asset_modelDTO.assetCategoryType == Asset_categoryDTO.MONITOR)
		{
			assetValue = CatDAO.getName(Language, "monitor_type", asset_modelDTO.monitorTypeCat);
			if(assetValue != null && !assetValue.equalsIgnoreCase(""))
			{
				qrContents += "Monitor: " + assetValue + NL;
			}
		}
		
		assetValue = CatDAO.getName(Language, "display_size", asset_modelDTO.displaySizeCat);
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "Display Size: " + assetValue + NL;
		}
		
		assetValue = assetAssigneeDTO.mac;
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "MAC: " + assetValue + NL;
		}
		
		assetValue = assetAssigneeDTO.ip;
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "IP: " + assetValue + NL;
		}
		
		assetValue = assetAssigneeDTO.idfNumber;
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "IDF Number: " + assetValue + NL;
		}
		
		assetValue = assetAssigneeDTO.mkBoxNumber;
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "MKBOX Number: " + assetValue + NL;
		}
		
		assetValue = assetAssigneeDTO.activeDirectoryId;
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "Active Dirtectory ID: " + assetValue + NL;
		}
		
		assetValue = assetAssigneeDTO.host;
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "Host: " + assetValue + NL;
		}
		
		assetValue = assetAssigneeDTO.location;
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "Location: " + assetValue + NL;
		}
		
		assetValue = assetAssigneeDTO.apLicenceForUse;
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "AP Licence: " + assetValue + NL;
		}
		
		assetValue = assetAssigneeDTO.port;
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "Port: " + assetValue + NL;
		}
		
		assetValue = CatDAO.getName(Language, "ups_capacity", asset_modelDTO.upsCapacityCat);
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "UPS Capacity: " + assetValue + NL;
		}
		
		assetValue = CatDAO.getName(Language, "ups_type", asset_modelDTO.upsTypeCat);
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "UPS Type: " + assetValue + NL;
		}
		
		assetValue = asset_modelDTO.idfNumber;
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "IDF Number: " + assetValue + NL;
		}
		
		assetValue = asset_modelDTO.remarks;
		if(assetValue != null && !assetValue.equalsIgnoreCase(""))
		{
			qrContents += "Remarks: " + assetValue + NL;
		}
		
		qrContents += justNL;
		
		try {
			List<AssetAssigneeDTO> attached = getChildAssets(assetAssigneeDTO.iD);
			if(attached != null)
			{
				for(AssetAssigneeDTO childDTO: attached)
				{
					qrContents += getQrContents(childDTO);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return qrContents;
	}

	public long add(AssetAssigneeDTO assetAssigneeDTO)
	{
		long id = -1;
		try {
			id = super.add(assetAssigneeDTO);
			AssetAssigneeRepository.getInstance().reload(false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}

	public long update(AssetAssigneeDTO assetAssigneeDTO)
	{
		long id = -1;
		try {
			id = super.update(assetAssigneeDTO);
			AssetAssigneeRepository.getInstance().reload(false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}

	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		AssetAssigneeDTO assetassigneeDTO = (AssetAssigneeDTO)commonDTO;
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();
		setSearchColumn(assetassigneeDTO);
		if(isInsert)
		{
			ps.setObject(++index,assetassigneeDTO.iD);
		}
		ps.setObject(++index,assetassigneeDTO.assetModelId);
		ps.setObject(++index,assetassigneeDTO.assetCategoryType);
		ps.setObject(++index,assetassigneeDTO.assetTypeId);

		ps.setObject(++index,assetassigneeDTO.ramCat);
		ps.setObject(++index,assetassigneeDTO.ramSizeCat);
		ps.setObject(++index,assetassigneeDTO.hdCat);
		ps.setObject(++index,assetassigneeDTO.hdSizeCat);
		ps.setObject(++index,assetassigneeDTO.processorCat);
		ps.setObject(++index,assetassigneeDTO.processorGenCat);

		ps.setObject(++index,assetassigneeDTO.brandType);
		ps.setObject(++index,assetassigneeDTO.model);
		ps.setObject(++index,assetassigneeDTO.modelName);
		ps.setObject(++index,assetassigneeDTO.parentModelName);
		ps.setObject(++index,assetassigneeDTO.sl);
		ps.setObject(++index,assetassigneeDTO.mac);
		ps.setObject(++index,assetassigneeDTO.ip);
		/*ps.setObject(++index,assetassigneeDTO.osLicenceCat);
		ps.setObject(++index,assetassigneeDTO.antivirusLicenceCat);
		ps.setObject(++index,assetassigneeDTO.officeLicenceCat);*/

		ps.setObject(++index,assetassigneeDTO.assignedOrganogramId);
		ps.setObject(++index,assetassigneeDTO.purchasingUserId);
		ps.setObject(++index,assetassigneeDTO.purchasingOrganogramId);

		ps.setObject(++index,assetassigneeDTO.deliveryDate);
		ps.setObject(++index,assetassigneeDTO.costingValue);
		ps.setObject(++index,assetassigneeDTO.assignmentStatus);
		ps.setObject(++index,assetassigneeDTO.remarks);
		ps.setObject(++index,assetassigneeDTO.condemnationReason);
		ps.setObject(++index,assetassigneeDTO.searchColumn);
		
		/*ps.setObject(++index,assetassigneeDTO.osKey);
		ps.setObject(++index,assetassigneeDTO.officeKey);
		ps.setObject(++index,assetassigneeDTO.antivirusKey);
		ps.setObject(++index,assetassigneeDTO.osKeyId);
		ps.setObject(++index,assetassigneeDTO.officeKeyId);
		ps.setObject(++index,assetassigneeDTO.antivirusKeyId);
		ps.setObject(++index,assetassigneeDTO.osVersionCat);
		ps.setObject(++index,assetassigneeDTO.officeVersionCat);
		ps.setObject(++index,assetassigneeDTO.antivirusVersionCat);*/

		ps.setObject(++index,assetassigneeDTO.idfNumber);
		ps.setObject(++index,assetassigneeDTO.host);
		ps.setObject(++index,assetassigneeDTO.location);
		ps.setObject(++index,assetassigneeDTO.apLicenceForUse);
		ps.setObject(++index,assetassigneeDTO.partNumber);

		ps.setObject(++index,assetassigneeDTO.activeDirectoryId);
		ps.setObject(++index,assetassigneeDTO.mkBoxNumber);

		ps.setObject(++index,assetassigneeDTO.port);


		ps.setObject(++index,assetassigneeDTO.hasWarranty);
		ps.setObject(++index,assetassigneeDTO.hasTOE);
		ps.setObject(++index,assetassigneeDTO.assignmentDate);
		ps.setObject(++index,assetassigneeDTO.deliveryStatus);

		ps.setObject(++index,assetassigneeDTO.isActive);

		ps.setObject(++index,assetassigneeDTO.softwareTypeId);
		ps.setObject(++index,assetassigneeDTO.softwareSubTypeId);
		ps.setObject(++index,assetassigneeDTO.softwareCat);
		ps.setObject(++index,assetassigneeDTO.softwareSubCat);
		ps.setObject(++index,assetassigneeDTO.parentAssigneeId);
		ps.setObject(++index,assetassigneeDTO.softwareKey);
		ps.setObject(++index,assetassigneeDTO.expiryDate);
		
		ps.setObject(++index,assetassigneeDTO.employeeRecordId);

		ps.setObject(++index,assetassigneeDTO.parentCatgories);
		ps.setObject(++index,assetassigneeDTO.pcName);
		
		ps.setObject(++index,assetassigneeDTO.roomNoCat);
		
		ps.setObject(++index,assetassigneeDTO.ownerCat);
		ps.setObject(++index,assetassigneeDTO.officeAssetOfficeId);
		ps.setObject(++index,assetassigneeDTO.responsibleOrganogramId);


		if(isInsert)
		{
			ps.setObject(++index, 0);
			ps.setObject(++index, lastModificationTime);
		}
	}

	public AssetAssigneeDTO build(ResultSet rs)
	{
		try
		{
			AssetAssigneeDTO assetassigneeDTO = new AssetAssigneeDTO();
			int i = 0;
			assetassigneeDTO.iD = rs.getLong(columnNames[i++]);
			assetassigneeDTO.assetModelId = rs.getLong(columnNames[i++]);
			assetassigneeDTO.assetCategoryType = rs.getLong(columnNames[i++]);
			assetassigneeDTO.assetTypeId = rs.getLong(columnNames[i++]);

			assetassigneeDTO.ramCat = rs.getInt(columnNames[i++]);
			assetassigneeDTO.ramSizeCat = rs.getInt(columnNames[i++]);
			assetassigneeDTO.hdCat = rs.getInt(columnNames[i++]);
			assetassigneeDTO.hdSizeCat = rs.getInt(columnNames[i++]);
			assetassigneeDTO.processorCat = rs.getInt(columnNames[i++]);
			assetassigneeDTO.processorGenCat = rs.getInt(columnNames[i++]);

			assetassigneeDTO.brandType = rs.getInt(columnNames[i++]);
			assetassigneeDTO.model = rs.getString(columnNames[i++]);
			assetassigneeDTO.modelName = rs.getString(columnNames[i++]);
			assetassigneeDTO.parentModelName = rs.getString(columnNames[i++]);
			assetassigneeDTO.sl = rs.getString(columnNames[i++]);
			assetassigneeDTO.mac = rs.getString(columnNames[i++]);
			assetassigneeDTO.ip = rs.getString(columnNames[i++]);
			/*assetassigneeDTO.osLicenceCat = rs.getInt(columnNames[i++]);
			assetassigneeDTO.antivirusLicenceCat = rs.getInt(columnNames[i++]);
			assetassigneeDTO.officeLicenceCat = rs.getInt(columnNames[i++]);*/

			assetassigneeDTO.assignedOrganogramId = rs.getLong(columnNames[i++]);
			assetassigneeDTO.purchasingUserId = rs.getLong(columnNames[i++]);
			assetassigneeDTO.purchasingOrganogramId = rs.getLong(columnNames[i++]);

			assetassigneeDTO.deliveryDate = rs.getLong(columnNames[i++]);
			assetassigneeDTO.costingValue = rs.getDouble(columnNames[i++]);
			assetassigneeDTO.assignmentStatus = rs.getInt(columnNames[i++]);
			assetassigneeDTO.remarks = rs.getString(columnNames[i++]);
			assetassigneeDTO.condemnationReason = rs.getString(columnNames[i++]);
			assetassigneeDTO.searchColumn = rs.getString(columnNames[i++]);

			/*assetassigneeDTO.osKey = rs.getString(columnNames[i++]);
			assetassigneeDTO.officeKey = rs.getString(columnNames[i++]);
			assetassigneeDTO.antivirusKey = rs.getString(columnNames[i++]);
			assetassigneeDTO.osKeyId = rs.getLong(columnNames[i++]);
			assetassigneeDTO.officeKeyId = rs.getLong(columnNames[i++]);
			assetassigneeDTO.antivirusKeyId = rs.getLong(columnNames[i++]);
			assetassigneeDTO.osVersionCat = rs.getInt(columnNames[i++]);
			assetassigneeDTO.officeVersionCat = rs.getInt(columnNames[i++]);
			assetassigneeDTO.antivirusVersionCat = rs.getInt(columnNames[i++]);*/


			assetassigneeDTO.idfNumber = rs.getString(columnNames[i++]);
			assetassigneeDTO.host = rs.getString(columnNames[i++]);
			assetassigneeDTO.location = rs.getString(columnNames[i++]);
			assetassigneeDTO.apLicenceForUse = rs.getString(columnNames[i++]);
			assetassigneeDTO.partNumber = rs.getString(columnNames[i++]);

			assetassigneeDTO.activeDirectoryId = rs.getString(columnNames[i++]);
			assetassigneeDTO.mkBoxNumber = rs.getString(columnNames[i++]);

			assetassigneeDTO.port = rs.getString(columnNames[i++]);


			assetassigneeDTO.hasWarranty = rs.getBoolean(columnNames[i++]);
			assetassigneeDTO.hasTOE = rs.getBoolean(columnNames[i++]);
			assetassigneeDTO.assignmentDate = rs.getLong(columnNames[i++]);
			assetassigneeDTO.deliveryStatus = rs.getInt(columnNames[i++]);

			assetassigneeDTO.isActive = rs.getBoolean(columnNames[i++]);


			assetassigneeDTO.softwareTypeId = rs.getLong(columnNames[i++]);
			assetassigneeDTO.softwareSubTypeId = rs.getLong(columnNames[i++]);
			assetassigneeDTO.softwareCat = rs.getInt(columnNames[i++]);
			assetassigneeDTO.softwareSubCat = rs.getInt(columnNames[i++]);
			assetassigneeDTO.parentAssigneeId = rs.getLong(columnNames[i++]);
			assetassigneeDTO.softwareKey = rs.getString(columnNames[i++]);
			assetassigneeDTO.expiryDate = rs.getLong(columnNames[i++]);
			
			assetassigneeDTO.employeeRecordId = rs.getLong(columnNames[i++]);

			assetassigneeDTO.parentCatgories = rs.getString(columnNames[i++]);
			assetassigneeDTO.pcName = rs.getString(columnNames[i++]);
			
			assetassigneeDTO.roomNoCat = rs.getInt(columnNames[i++]);
			
			assetassigneeDTO.ownerCat = rs.getInt(columnNames[i++]);
			assetassigneeDTO.officeAssetOfficeId = rs.getLong(columnNames[i++]);
			assetassigneeDTO.responsibleOrganogramId = rs.getLong(columnNames[i++]);


			assetassigneeDTO.isDeleted = rs.getInt(columnNames[i++]);
			assetassigneeDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return assetassigneeDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	public KeyCountDTO getAssetCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("assignment_status");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public KeyCountDTO getAssetOrganogramCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("assigned_organogram_id");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public KeyCountDTO getAssetCategoryStatusCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key2 = rs.getLong("assignment_status");
			keyCountDTO.key = rs.getLong("asset_category_type");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}

	public int getCount(ResultSet rs)
	{
		int count = 0;
		try
		{
			count = rs.getInt("count(id)");
			return count;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return 0;
		}
	}

	public List<KeyCountDTO> getAssetStatusCount()
    {
		String sql = "SELECT assignment_status, count(id) FROM asset_assignee where isdeleted = 0 and asset_category_type != " + Asset_categoryDTO.SOFTWARE + " "
				+ "group by assignment_status";
		return ConnectionAndStatementUtil.getListOfT(sql,this::getAssetCount);
    }
	public List<KeyCountDTO> getAssetCategoryStatusCount()
    {
		String sql = "SELECT asset_category_type, assignment_status, count(id) FROM asset_assignee where isdeleted = 0 and asset_category_type != " + Asset_categoryDTO.SOFTWARE + " "
				+ " group by asset_category_type, assignment_status order by asset_category_type asc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::getAssetCategoryStatusCount);
    }
	
	public List<KeyCountDTO> getAssetOrganogramCount()
    {
		String sql = "SELECT assigned_organogram_id, count(id) FROM asset_assignee "
				+ "where isdeleted = 0 and asset_category_type != " + Asset_categoryDTO.SOFTWARE + " and assigned_organogram_id != -1 "
				+ "group by assigned_organogram_id";
		return ConnectionAndStatementUtil.getListOfT(sql,this::getAssetOrganogramCount);
    }

	public int getActiveCount(long parentID) throws Exception {
        String sql = "select count(id) from asset_assignee WHERE asset_model_id=" + parentID + " and is_active = 1 and isDeleted = 0";
        return ConnectionAndStatementUtil.getT(sql,this::getCount, 0);
    }

	public List<AssetAssigneeDTO> getUnassigned(long parentID) throws Exception {
        String sql = "select * from asset_assignee WHERE asset_model_id=" + parentID + " and assignment_status = " + AssetAssigneeDTO.NOT_ASSIGNED + " and isDeleted = 0";
        return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }

	public List<AssetAssigneeDTO> getAssigned(long parentID) throws Exception {
        String sql = "select * from asset_assignee WHERE asset_model_id=" + parentID + " and assignment_status = " + AssetAssigneeDTO.ASSIGNED + "  and isDeleted = 0";
        return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }

	public List<AssetAssigneeDTO> getByOrganogramId(long organogramId) throws Exception {
		if(organogramId == -1)
		{
			return null;
		}
        String sql = "select * from asset_assignee WHERE assigned_organogram_id =" + organogramId
        		+ " and isDeleted = 0 order by asset_type_id asc, asset_category_type asc"; //hardwares come first
        return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }
	
	public List<AssetAssigneeDTO> getByResponsibleIdRoomAndOwner(long organogramId, int roomNoCat, int ownerCat) throws Exception {
        String sql = "select * from asset_assignee WHERE responsible_organogram_id =" + organogramId
        		+ " and owner_cat = " + ownerCat + " and room_no_cat = " + roomNoCat
        		+ " and isDeleted = 0 order by asset_type_id asc, asset_category_type asc"; //hardwares come first
        return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }
	
	
	public List<AssetAssigneeDTO> getByEmployeeRecordId(long employeeRecordId) throws Exception {
        String sql = "select * from asset_assignee WHERE employee_record_id =" + employeeRecordId
        		+ " and isDeleted = 0 order by asset_type_id asc, asset_category_type asc"; //hardwares come first
        return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }
	
	public void unassignEmployeeFromPost(long organogramId)
	{
		Asset_modelDAO asset_modelDAO = new Asset_modelDAO();
		Asset_transactionDAO asset_transactionDAO = new Asset_transactionDAO();
		List<AssetAssigneeDTO> assets;
		try {
			assets = getByOrganogramId(organogramId);
			if(assets != null)
			{
				for(AssetAssigneeDTO assetAssigneeDTO: assets)
				{
					Asset_modelDTO asset_modelDTO = (Asset_modelDTO) asset_modelDAO.getDTOByID(assetAssigneeDTO.assetModelId);
					Asset_transactionDTO asset_transactionDTO = new Asset_transactionDTO(asset_modelDTO, assetAssigneeDTO, Asset_transactionDTO.EMPLOYEE_LEFT_POST);
					asset_transactionDAO.add(asset_transactionDTO);
					assetAssigneeDTO.employeeRecordId = -1;
					update(assetAssigneeDTO);
				}
			}
			
			Set<Long> ticketAdmins = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.TICKET_ADMIN_ROLE);
			for(Long admin: ticketAdmins)
			{
				Pb_notificationsDAO.getInstance().addPb_notificationsAndSendMailSMS
				(
						admin,
						System.currentTimeMillis(),
						"Asset_modelServlet?actionType=getUserAsset&organogramId=" + organogramId,
						WorkflowController.getNameFromOrganogramId(organogramId, true) + " resigned from " + WorkflowController.getOrganogramName(organogramId, true),
						WorkflowController.getNameFromOrganogramId(organogramId, false) + " এই পদ থেকে পদত্যাগ করেছেন: " + WorkflowController.getOrganogramName(organogramId, false),
						"Employee Resignation",
						"",
						-1,
						true,
						false
				);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public void assignEmployeeToPost(long organogramId, long employeeRecordId)
	{
		Asset_modelDAO asset_modelDAO = new Asset_modelDAO();
		Asset_transactionDAO asset_transactionDAO = new Asset_transactionDAO();
		List<AssetAssigneeDTO> assets;
		try {
			assets = getByOrganogramId(organogramId);
			if(assets != null)
			{
				for(AssetAssigneeDTO assetAssigneeDTO: assets)
				{
					assetAssigneeDTO.employeeRecordId = employeeRecordId;
					update(assetAssigneeDTO);
					
					Asset_modelDTO asset_modelDTO = (Asset_modelDTO) asset_modelDAO.getDTOByID(assetAssigneeDTO.assetModelId);
					Asset_transactionDTO asset_transactionDTO = new Asset_transactionDTO(asset_modelDTO, assetAssigneeDTO, Asset_transactionDTO.EMPLOYEE_JOINED_POST);
					asset_transactionDAO.add(asset_transactionDTO);
										
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	public List<AssetAssigneeDTO> getByList(String list) throws Exception {
        String sql = "select * from asset_assignee WHERE id in (" + list
        		+ ") and isDeleted = 0 order by asset_type_id asc, asset_category_type asc"; //hardwares come first
        return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }
	
	public List<AssetAssigneeDTO> getAllResponsible(long orgId){
        String sql = "select * from asset_assignee WHERE (responsible_organogram_id = " + orgId
        		+ " or assigned_organogram_id = " + orgId 
        		+ ") and isDeleted = 0 order by asset_type_id asc, asset_category_type asc"; //hardwares come first
        return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }

	public List<AssetAssigneeDTO> getChildAssets(long parentId) throws Exception {
        String sql = "select * from asset_assignee WHERE parent_assignee_id =" + parentId
        		+ " and isDeleted = 0 order by asset_type_id asc, asset_category_type asc, assigned_organogram_id asc";//hardwares come first
        return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }
	
	public int getCountByOrganogramId(long organogramId) throws Exception {
        String sql = "select count(id) from asset_assignee WHERE assigned_organogram_id =" + organogramId
        		+ " and isDeleted = 0";
        return ConnectionAndStatementUtil.getT(sql,this::getCount);
    }

	public AssetAssigneeDTO getLastByOrganogramId(long organogramId) throws Exception {
        String sql = "select * from asset_assignee WHERE assigned_organogram_id =" + organogramId
        		+ " and isDeleted = 0 order by id desc limit 1";
        return ConnectionAndStatementUtil.getT(sql,this::build);
    }

	public AssetAssigneeDTO get1stUnassigned(long parentID) throws Exception {
        String sql = "select * from asset_assignee WHERE asset_model_id=" + parentID + " and is_active = 1 and assignment_status =  " + AssetAssigneeDTO.NOT_ASSIGNED + " and isDeleted = 0 limit 1";
        return ConnectionAndStatementUtil.getT(sql,this::build);
    }

    public AssetAssigneeDTO getByIdAndParentId(long id, long parentID) throws Exception {
        String sql = "select * from asset_assignee WHERE asset_model_id=" + parentID + " and id = " + id + " and isDeleted = 0";
        return ConnectionAndStatementUtil.getT(sql,this::build);
    }

    public AssetAssigneeDTO getLastByModelId(long parentID) throws Exception {
        String sql = "select * from asset_assignee WHERE asset_model_id=" + parentID +
        		" and isDeleted = 0 order by id desc limit 1";
        return ConnectionAndStatementUtil.getT(sql,this::build);
    }

	public AssetAssigneeDTO get1stUnassignedByModelAndSl(long modelId, String sl, String usedIds) throws Exception {
        String sql = "select * from asset_assignee WHERE isDeleted = 0 and asset_model_id=" + modelId
        		+ " and is_active = 1 and assignment_status =  " + AssetAssigneeDTO.NOT_ASSIGNED;
        if(!sl.equalsIgnoreCase(""))
        {
        	sql += " and sl like '%" + sl + "%'";
        }
        if(!usedIds.equalsIgnoreCase(""))
        {
        	sql += " and id not in (" + usedIds + ")";
        }
        return ConnectionAndStatementUtil.getT(sql,this::build);
    }

	public AssetAssigneeDTO get1stUnassignedSoft(long softwareCat, long softwareSubCat, String usedIds, int licenseType) throws Exception
	{
        String sql = "select * from asset_assignee WHERE isDeleted = 0 and software_cat=" + softwareCat
        		+ " and software_sub_cat = " + softwareSubCat
        		+ " and is_active = 1 and assignment_status =  " + AssetAssigneeDTO.NOT_ASSIGNED;

        if(!usedIds.equalsIgnoreCase(""))
        {
        	sql += " and id not in (" + usedIds + ")";
        }
        if(licenseType == 0)
        {
        	sql += " and (software_key = '' or software_key is null)";
        }
        else if (licenseType == 1)
        {
        	sql += " and software_key != ''";
        }
        sql += " order by id asc limit 1";
        return ConnectionAndStatementUtil.getT(sql,this::build);
    }

	public List<AssetAssigneeDTO> getUnassignedSofts(long softwareCat, long softwareSubCat, String usedIds, int licenseType) throws Exception
	{
        String sql = "select * from asset_assignee WHERE isDeleted = 0 and software_cat=" + softwareCat
        		+ " and software_sub_cat = " + softwareSubCat
        		+ " and is_active = 1 and assignment_status =  " + AssetAssigneeDTO.NOT_ASSIGNED;

        if(!usedIds.equalsIgnoreCase(""))
        {
        	sql += " and id not in (" + usedIds + ")";
        }
        if(licenseType == 0)
        {
        	sql += " and (software_key = '' or software_key is null)";
        }
        else if (licenseType == 1)
        {
        	sql += " and software_key != ''";
        }
        sql += " order by id asc";
        return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }
	
	public void transfer(long fromOrgId, long toOrgId)
	{
		
        long lastModificationTime = System.currentTimeMillis();
        String sql = "update asset_assignee set lastModificationTime = " + System.currentTimeMillis()
        + ", assigned_organogram_id = " + toOrgId + " where assigned_organogram_id = " + fromOrgId;
       
        StringBuilder sb = new StringBuilder();
        sb.append(sql);

        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
        	String sql2 = sb.toString();
            Connection connection = model.getConnection();
            Statement stmt = model.getStatement();
            try {
                logger.debug(sql2);
                stmt.execute(sql2);
                recordUpdateTime(connection, lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
        
        
        sql = "update asset_assignee set lastModificationTime = " + System.currentTimeMillis()
        + ", responsible_organogram_id = " + toOrgId + " where responsible_organogram_id = " + fromOrgId;
       
        StringBuilder sb2 = new StringBuilder();
        sb2.append(sql);

        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
        	String sql2 = sb2.toString();
            Connection connection = model.getConnection();
            Statement stmt = model.getStatement();
            try {
                logger.debug(sql2);
                stmt.execute(sql2);
                recordUpdateTime(connection, lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
        
        
        AssetAssigneeRepository.getInstance().reload(false);
        

    }

	public long activate(ArrayList<Long> ids, boolean activate)
	{
		if(ids == null || ids.size() == 0)
		{
			return -1;
		}
        long lastModificationTime = System.currentTimeMillis();
        String sql = "update asset_assignee set lastModificationTime = " + System.currentTimeMillis()
        + ", is_active = " + activate + " where id in (";
        int i = 0;
        for(long id: ids)
        {
        	sql += " " + id;
        	if(i < ids.size() - 1)
        	{
        		sql += ",";
        	}
        	i++;
        }
        sql += ")";
        StringBuilder sb = new StringBuilder();
        sb.append(sql);

        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
        	String sql2 = sb.toString();
            Connection connection = model.getConnection();
            Statement stmt = model.getStatement();
            try {
                logger.debug(sql2);
                stmt.execute(sql2);
                recordUpdateTime(connection, lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
        AssetAssigneeRepository.getInstance().reload(false);
        return 1;

    }

	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
														  UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
	{
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;



		String AnyfieldSql = "";
		String AllFieldSql = "";

		if(p_searchCriteria != null)
		{


			Enumeration names = p_searchCriteria.keys();
			String str, value;

			AnyfieldSql = "(";

			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ?";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);

			AllFieldSql = "(";
			int i = 0;
			boolean hasWarrantyFlag = false;
			while(names.hasMoreElements())
			{
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
				System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						str.equals("asset_category_type")
								|| str.equals("brand_type")
								|| str.equals("name_en")
								|| str.equals("sl")
								|| str.equals("mac")
								|| str.equals("assignment_status")
								|| str.equals("assignedOrganogramId")
								|| str.equals("toe_status")
								|| str.equals("warranty_status")
								|| str.equals("warranty_from")
								|| str.equals("warranty_to")
								|| str.equals("cost")
								|| str.equals("is_assigned")
				)

				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}

					if(str.equals("asset_category_type"))
					{
						AllFieldSql += "" + tableName + ".asset_category_type = ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("brand_type"))
					{
						AllFieldSql += "" + tableName + ".brand_type = ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".model like ?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}

					else if(str.equals("sl"))
					{
						AllFieldSql += "" + tableName + ".sl like ? ";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					
					else if(str.equals("mac"))
					{
						AllFieldSql += "" + tableName + ".mac = ? ";
						objectList.add( p_searchCriteria.get(str).toString().replaceAll("mmmm", "-"));
						i ++;
					}

					else if(str.equals("cost"))
					{
						AllFieldSql += "" + tableName + ".costing_value = ? ";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}

					else if(str.equals("assignment_status"))
					{
						AllFieldSql += "" + tableName + ".assignment_status like ? ";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}

					else if(str.equals("assignedOrganogramId"))
					{
						AllFieldSql += "" + tableName + ".assigned_organogram_id = ? ";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}

					else if(str.equals("toe_status"))
					{
						AllFieldSql += "" + tableName + ".has_toe = ? ";
						objectList.add(Integer.parseInt((String)p_searchCriteria.get(str)));
						i ++;
					}

					else if(str.equals("warranty_status"))
					{
						AllFieldSql += "" + tableName + ".has_warranty = ? ";
						objectList.add(Integer.parseInt((String)p_searchCriteria.get(str)));
						i ++;
						hasWarrantyFlag = true;
					}

					else if(str.equals("warranty_from"))
					{
						if (!hasWarrantyFlag) {
							AllFieldSql += "" + tableName + ".has_warranty = ? AND ";
							objectList.add(1);
							i ++;
							hasWarrantyFlag = true;
						}

						AllFieldSql += "" + tableName + ".delivery_date >= ? ";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}

					else if(str.equals("warranty_to"))
					{
						if (!hasWarrantyFlag) {
							AllFieldSql += "" + tableName + ".has_warranty = ? AND ";
							objectList.add(1);
							i ++;
							hasWarrantyFlag = true;
						}

						AllFieldSql += "" + tableName + ".expiry_date <= ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}

					else if(str.equals("is_assigned"))
					{

						if (Integer.parseInt((String)p_searchCriteria.get(str)) == 1) {
							AllFieldSql += "" + tableName + ".assigned_organogram_id != ? ";
						} else {
							AllFieldSql += "" + tableName + ".assigned_organogram_id = ? ";
						}
						objectList.add(Integer.parseInt("-1"));
						i ++;
					}

				}
			}

			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);


		}


		sql += " WHERE ";

		sql += " (" + tableName + ".isDeleted = 0 ";
		sql += ")";


		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}

		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;

		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{
			sql += " AND " + AllFieldSql;
		}




		if(category == GETDTOS)
		{
			sql += " order by lastModificationTime desc ";

		}

		//printSql(sql);

		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}


		return sql;
	}
}
	