package asset_model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Asset_modelRepository implements Repository {
	Asset_modelDAO asset_modelDAO = null;
	
	public void setDAO(Asset_modelDAO asset_modelDAO)
	{
		this.asset_modelDAO = asset_modelDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Asset_modelRepository.class);
	Map<Long, Asset_modelDTO>mapOfAsset_modelDTOToiD;
	Map<String, Asset_modelDTO>mapOfAsset_modelDTOToNameEn;

  
	private Asset_modelRepository(){
		asset_modelDAO = new Asset_modelDAO();
		mapOfAsset_modelDTOToiD = new ConcurrentHashMap<>();
		mapOfAsset_modelDTOToNameEn = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Asset_modelRepository INSTANCE = new Asset_modelRepository();
    }

    public static Asset_modelRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Asset_modelDTO> asset_modelDTOs = (List<Asset_modelDTO>)asset_modelDAO.getAll(reloadAll);
			for(Asset_modelDTO asset_modelDTO : asset_modelDTOs) {
				Asset_modelDTO oldAsset_modelDTO = getAsset_modelDTOByID(asset_modelDTO.iD);
				if( oldAsset_modelDTO != null ) {
					mapOfAsset_modelDTOToiD.remove(oldAsset_modelDTO.iD);
					mapOfAsset_modelDTOToNameEn.remove(oldAsset_modelDTO.nameEn);
					
				}
				if(asset_modelDTO.isDeleted == 0) 
				{
					mapOfAsset_modelDTOToiD.put(asset_modelDTO.iD, asset_modelDTO);
					mapOfAsset_modelDTOToNameEn.put(asset_modelDTO.nameEn, asset_modelDTO);
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Asset_modelDTO> getAsset_modelList() {
		List <Asset_modelDTO> asset_models = new ArrayList<Asset_modelDTO>(this.mapOfAsset_modelDTOToiD.values());
		return asset_models;
	}
	
	public List<Asset_modelDTO> getAsset_modelListByNames() {
		List <Asset_modelDTO> asset_models = new ArrayList<Asset_modelDTO>(this.mapOfAsset_modelDTOToNameEn.values());
		return asset_models;
	}
	
	
	public Asset_modelDTO getAsset_modelDTOByID( long ID){
		return mapOfAsset_modelDTOToiD.getOrDefault(ID, null);
	}
	
	public Asset_modelDTO getAsset_modelDTOByNameEn( String nameEn){
		return mapOfAsset_modelDTOToNameEn.getOrDefault(nameEn, null);
	}

	
	@Override
	public String getTableName() {
		return "asset_model";
	}
}


