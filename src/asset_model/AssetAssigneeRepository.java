package asset_model;

import asset_type.Asset_typeDTO;
import election_constituency.Election_constituencyDAO;
import election_constituency.Election_constituencyDTO;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import workflow.WorkflowController;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class AssetAssigneeRepository implements Repository {
    private final AssetAssigneeDAO assetAssigneeDAO;

    private static final Logger logger = Logger.getLogger(AssetAssigneeRepository.class);

    private final Map<Long, AssetAssigneeDTO> mapById;

    private AssetAssigneeRepository() {
        mapById = new ConcurrentHashMap<>();

        assetAssigneeDAO = new AssetAssigneeDAO();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        static AssetAssigneeRepository INSTANCE = new AssetAssigneeRepository();
    }

    public synchronized static AssetAssigneeRepository getInstance() {
        return AssetAssigneeRepository.LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        logger.debug("AssetAssigneeRepository reload start reloadAll : " + reloadAll);
        List<AssetAssigneeDTO> list = (List<AssetAssigneeDTO>) assetAssigneeDAO.getAll(reloadAll);
        list.stream()
                .peek(this::removeIfPresent)
                .filter(dto -> dto.isDeleted == 0)
                .forEach(this::addToMaps);
        logger.debug("AssetAssigneeRepository reload end reloadAll : " + reloadAll);
    }

    private void addToMaps(AssetAssigneeDTO dto) {
        mapById.put(dto.iD, dto);
    }

    private void removeIfPresent(AssetAssigneeDTO dto) {
        AssetAssigneeDTO oldDTO = mapById.get(dto.iD);
        if (oldDTO != null) {
            mapById.remove(dto.iD);
        }
    }

    @Override
    public String getTableName() {
        return "asset_assignee";
    }


    public String getAssigneeText(long ID, boolean isLanguageEnglish) {
        AssetAssigneeDTO assetAssigneeDTO = mapById.get(ID);
        Employee_recordsDTO dto = null;
        try {
            dto = Employee_recordsRepository.getInstance().getById(WorkflowController.getEmployeeRecordIDFromOrganogramID(assetAssigneeDTO.assignedOrganogramId));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dto==null ? "" : isLanguageEnglish ? dto.nameEng : dto.nameBng;
    }

    public AssetAssigneeDTO getByID(long ID) {
        return mapById.get(ID);
    }
}
