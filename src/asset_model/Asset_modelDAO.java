package asset_model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import org.apache.log4j.Logger;

import common.ConnectionAndStatementUtil;
import util.*;
import pb.*;
import user.UserDTO;

public class Asset_modelDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Asset_modelDAO(String tableName)
	{
		super(tableName);
		useSafeSearch = true;
		joinSQL = "";
		columnNames = new String[] 
		{
			"ID",
			"asset_category_type",
			"lot",
			"brand_type",
			"name_en",
			"name_bn",
			"sl",
						
			"quantity",
			"processor_cat",
			"processor_gen_cat",
			"ram_cat",
			"ram_size_cat",
			"hd_cat",
			"hd_size_cat",
			"display_size_cat",
			
			"purchase_information_available",
						
			"capacity",
			"ups_capacity_cat",
			"idf_number",
			
			"mac",
			"ip",
			
			"printer_type_cat",
			"printer_network_type_cat",
			"scanner_type_cat",
			"ups_type_cat",
			"monitor_type_cat",
			
			"data_center_type_cat",

			"location",
			"part_number",
			
			"host_name",
			"management_vlan",
			
			"hardware_software_cat",
			
			"product_name",
									
			"asset_supplier_type",
			"po_number",
			"receiving_date",
			"procurement_type_cat",
			"has_warranty",
			"has_toe",
			"expiry_date",
			"project_name",
			"costing_value",
			"asset_model_status_cat",
			
			"software_type_id",
			"asset_type_id",
			
			"software_cat",
			"software_sub_cat",
			
			"warehouse_address",
			
			
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"is_immutable",
			"isDeleted",
			"lastModificationTime"
		};
	}

	
	public Asset_modelDAO()
	{
		this("asset_model");		
	}
	
	public void setSearchColumn(Asset_modelDTO asset_modelDTO)
	{
		asset_modelDTO.searchColumn = "";
		asset_modelDTO.searchColumn += CommonDAO.getName("English", "asset_category", asset_modelDTO.assetCategoryType) + " " + CommonDAO.getName("Bangla", "asset_category", asset_modelDTO.assetCategoryType) + " ";
		asset_modelDTO.searchColumn += asset_modelDTO.lot + " ";
		asset_modelDTO.searchColumn += CommonDAO.getName("English", "brand", asset_modelDTO.brandType) + " " + CommonDAO.getName("Bangla", "brand", asset_modelDTO.brandType) + " ";
		asset_modelDTO.searchColumn += asset_modelDTO.nameEn + " ";
		asset_modelDTO.searchColumn += asset_modelDTO.sl + " ";
		asset_modelDTO.searchColumn += CatDAO.getName("English", "procurement_type", asset_modelDTO.procurementTypeCat) + " " + CatDAO.getName("Bangla", "procurement_type", asset_modelDTO.procurementTypeCat) + " ";
		asset_modelDTO.searchColumn += asset_modelDTO.projectName + " ";
		asset_modelDTO.searchColumn += CatDAO.getName("English", "asset_model_status", asset_modelDTO.assetModelStatusCat) + " " + CatDAO.getName("Bangla", "asset_model_status", asset_modelDTO.assetModelStatusCat) + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Asset_modelDTO asset_modelDTO = (Asset_modelDTO)commonDTO;
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(asset_modelDTO);
		if(isInsert)
		{
			ps.setObject(++index,asset_modelDTO.iD);
		}
		ps.setObject(++index,asset_modelDTO.assetCategoryType);
		ps.setObject(++index,asset_modelDTO.lot);
		ps.setObject(++index,asset_modelDTO.brandType);
		ps.setObject(++index,asset_modelDTO.nameEn);
		ps.setObject(++index,asset_modelDTO.nameBn);
		ps.setObject(++index,asset_modelDTO.sl);
		ps.setObject(++index,asset_modelDTO.quantity);
		
		ps.setObject(++index,asset_modelDTO.processorCat);
		ps.setObject(++index,asset_modelDTO.processorGenCat);
		ps.setObject(++index,asset_modelDTO.ramCat);
		ps.setObject(++index,asset_modelDTO.ramSizeCat);
		ps.setObject(++index,asset_modelDTO.hdCat);
		ps.setObject(++index,asset_modelDTO.hdSizeCat);
		ps.setObject(++index,asset_modelDTO.displaySizeCat);
		
		ps.setObject(++index,asset_modelDTO.purchaseInformationAvailable);
		
		
		ps.setObject(++index,asset_modelDTO.capacity);
		ps.setObject(++index,asset_modelDTO.upsCapacityCat);
		ps.setObject(++index,asset_modelDTO.idfNumber);
		
		ps.setObject(++index,asset_modelDTO.mac);
		ps.setObject(++index,asset_modelDTO.ip);
		
		ps.setObject(++index,asset_modelDTO.printerTypeCat);
		ps.setObject(++index,asset_modelDTO.printerNetworkTypeCat);
		ps.setObject(++index,asset_modelDTO.scannerTypeCat);
		ps.setObject(++index,asset_modelDTO.upsTypeCat);
		ps.setObject(++index,asset_modelDTO.monitorTypeCat);
		
		ps.setObject(++index,asset_modelDTO.dataCenterTypeCat);
		
		ps.setObject(++index,asset_modelDTO.location);
		ps.setObject(++index,asset_modelDTO.partNumber);
		
		ps.setObject(++index,asset_modelDTO.hostName);
		ps.setObject(++index,asset_modelDTO.managementVlan);
		
		ps.setObject(++index,asset_modelDTO.hsCat);
		ps.setObject(++index,asset_modelDTO.productName);
		
		ps.setObject(++index,asset_modelDTO.assetSupplierType);
		ps.setObject(++index,asset_modelDTO.poNumber);
		ps.setObject(++index,asset_modelDTO.receivingDate);
		ps.setObject(++index,asset_modelDTO.procurementTypeCat);
		ps.setObject(++index,asset_modelDTO.hasWarranty);
		ps.setObject(++index,asset_modelDTO.hasTOE);
		ps.setObject(++index,asset_modelDTO.expiryDate);
		ps.setObject(++index,asset_modelDTO.projectName);
		ps.setObject(++index,asset_modelDTO.costingValue);
		ps.setObject(++index,asset_modelDTO.assetModelStatusCat);
		
		ps.setObject(++index,asset_modelDTO.softwareTypeId);
		ps.setObject(++index,asset_modelDTO.assetTypeId);
		
		ps.setObject(++index,asset_modelDTO.softwareCat);
		ps.setObject(++index,asset_modelDTO.softwareSubCat);
		
		ps.setObject(++index,asset_modelDTO.warehouseAddress);
		
		
		ps.setObject(++index,asset_modelDTO.searchColumn);
		ps.setObject(++index,asset_modelDTO.insertedByUserId);
		ps.setObject(++index,asset_modelDTO.insertedByOrganogramId);
		ps.setObject(++index,asset_modelDTO.insertionDate);
		ps.setObject(++index,asset_modelDTO.lastModifierUser);
		ps.setObject(++index,asset_modelDTO.isImmutable);
		if(isInsert)
		{
			ps.setObject(++index, 0);
			ps.setObject(++index, lastModificationTime);
		}
	}
	
	public Asset_modelDTO build(ResultSet rs)
	{
		try
		{
			Asset_modelDTO asset_modelDTO = new Asset_modelDTO();
			int i = 0;
			asset_modelDTO.iD = rs.getLong(columnNames[i++]);
			asset_modelDTO.assetCategoryType = rs.getLong(columnNames[i++]);
			asset_modelDTO.lot = rs.getString(columnNames[i++]);
			asset_modelDTO.brandType = rs.getInt(columnNames[i++]);
			asset_modelDTO.nameEn = rs.getString(columnNames[i++]);
			asset_modelDTO.nameBn = rs.getString(columnNames[i++]);
			asset_modelDTO.sl = rs.getString(columnNames[i++]);
			asset_modelDTO.quantity = rs.getInt(columnNames[i++]);
			
			asset_modelDTO.processorCat = rs.getInt(columnNames[i++]);
			asset_modelDTO.processorGenCat = rs.getInt(columnNames[i++]);
			asset_modelDTO.ramCat = rs.getInt(columnNames[i++]);
			asset_modelDTO.ramSizeCat = rs.getInt(columnNames[i++]);
			asset_modelDTO.hdCat = rs.getInt(columnNames[i++]);
			asset_modelDTO.hdSizeCat  = rs.getInt(columnNames[i++]);
			asset_modelDTO.displaySizeCat  = rs.getInt(columnNames[i++]);
			
			
			asset_modelDTO.purchaseInformationAvailable  = rs.getBoolean(columnNames[i++]);
			
			asset_modelDTO.capacity = rs.getString(columnNames[i++]);
			asset_modelDTO.upsCapacityCat = rs.getInt(columnNames[i++]);
			asset_modelDTO.idfNumber = rs.getString(columnNames[i++]);
			
			asset_modelDTO.mac = rs.getString(columnNames[i++]);
			asset_modelDTO.ip = rs.getString(columnNames[i++]);
			
			asset_modelDTO.printerTypeCat = rs.getInt(columnNames[i++]);
			asset_modelDTO.printerNetworkTypeCat = rs.getInt(columnNames[i++]);
			asset_modelDTO.scannerTypeCat = rs.getInt(columnNames[i++]);
			asset_modelDTO.upsTypeCat = rs.getInt(columnNames[i++]);
			asset_modelDTO.monitorTypeCat = rs.getInt(columnNames[i++]);
			
			asset_modelDTO.dataCenterTypeCat = rs.getInt(columnNames[i++]);
			
			asset_modelDTO.location = rs.getString(columnNames[i++]);
			asset_modelDTO.partNumber = rs.getString(columnNames[i++]);
			
			asset_modelDTO.hostName = rs.getString(columnNames[i++]);
			asset_modelDTO.managementVlan = rs.getString(columnNames[i++]);
			
			asset_modelDTO.hsCat = rs.getInt(columnNames[i++]);
			asset_modelDTO.productName = rs.getString(columnNames[i++]);
			
			asset_modelDTO.assetSupplierType = rs.getInt(columnNames[i++]);
			asset_modelDTO.poNumber = rs.getString(columnNames[i++]);
			asset_modelDTO.receivingDate = rs.getLong(columnNames[i++]);
			asset_modelDTO.procurementTypeCat = rs.getInt(columnNames[i++]);
			asset_modelDTO.hasWarranty = rs.getBoolean(columnNames[i++]);
			asset_modelDTO.hasTOE = rs.getBoolean(columnNames[i++]);
			asset_modelDTO.expiryDate = rs.getLong(columnNames[i++]);
			asset_modelDTO.projectName = rs.getString(columnNames[i++]);
			asset_modelDTO.costingValue = rs.getDouble(columnNames[i++]);
			asset_modelDTO.assetModelStatusCat = rs.getInt(columnNames[i++]);
			
			asset_modelDTO.softwareTypeId = rs.getLong(columnNames[i++]);
			asset_modelDTO.assetTypeId = rs.getLong(columnNames[i++]);
			
			asset_modelDTO.softwareCat = rs.getInt(columnNames[i++]);
			asset_modelDTO.softwareSubCat = rs.getInt(columnNames[i++]);
			
			asset_modelDTO.warehouseAddress = rs.getString(columnNames[i++]);
			if(asset_modelDTO.warehouseAddress == null)
			{
				asset_modelDTO.warehouseAddress = "";
			}
			
			asset_modelDTO.searchColumn = rs.getString(columnNames[i++]);
			asset_modelDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			asset_modelDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			asset_modelDTO.insertionDate = rs.getLong(columnNames[i++]);
			asset_modelDTO.lastModifierUser = rs.getString(columnNames[i++]);
			asset_modelDTO.isImmutable = rs.getBoolean(columnNames[i++]);
			asset_modelDTO.isDeleted = rs.getInt(columnNames[i++]);
			asset_modelDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return asset_modelDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
			
	public Asset_modelDTO getDTOByID (long id, boolean getChildren)
	{
		Asset_modelDTO asset_modelDTO = null;
		try 
		{
			asset_modelDTO = (Asset_modelDTO)super.getDTOByID(id);
			if(asset_modelDTO != null && getChildren)
			{
				AssetAssigneeDAO assetAssigneeDAO = new AssetAssigneeDAO("asset_assignee");				
				List<AssetAssigneeDTO> assetAssigneeDTOList = assetAssigneeDAO.getAssigned(asset_modelDTO.iD);
				asset_modelDTO.assetAssigneeDTOList = assetAssigneeDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return asset_modelDTO;
	}
	
	public Asset_modelDTO getJustDTOByID (long id)
	{
		Asset_modelDTO asset_modelDTO = null;
		try 
		{
			asset_modelDTO = (Asset_modelDTO)super.getDTOByID(id);
			
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return asset_modelDTO;
	}
	public long add(Asset_modelDTO asset_modelDTO)
	{
		long id = -1;
		try {
			id = super.add(asset_modelDTO);
			Asset_modelRepository.getInstance().reload(false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}
	
	public long update(Asset_modelDTO asset_modelDTO)
	{
		long id = -1;
		try {
			id = super.update(asset_modelDTO);
			Asset_modelRepository.getInstance().reload(false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}

	public Asset_modelDTO getParentDTOByID (long id)
	{
		Asset_modelDTO asset_modelDTO = null;
		try 
		{
			asset_modelDTO = (Asset_modelDTO)super.getDTOByID(id);			
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return asset_modelDTO;
	}
	
	 public Collection getDistinctModelNamesByCat(long cat) 
	 {
	    	String sql = "SELECT distinct name_en FROM " + tableName + " where isDeleted=0 and asset_category_type = " + cat ;
			return ConnectionAndStatementUtil.getListOfT(sql,this::getName);			
	 }
	 
	 public String getName(ResultSet rs)
	 {
		String name = "";
		try
		{				
			name = rs.getString("name_en");			
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return "";
		}
		return name;
	}

	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ?";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("asset_category_type")
						|| str.equals("lot")
						|| str.equals("brand_type")
						|| str.equals("name_en")
						|| str.equals("sl")
					
						|| str.equals("vendor_cat")
						|| str.equals("po_number")
						|| str.equals("receiving_date_start")
						|| str.equals("receiving_date_end")
						|| str.equals("procurement_type_cat")

						|| str.equals("project_name")

						|| str.equals("asset_model_status_cat")

				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("asset_category_type"))
					{
						AllFieldSql += "" + tableName + ".asset_category_type = ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("lot"))
					{
						AllFieldSql += "" + tableName + ".lot like ?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("brand_type"))
					{
						AllFieldSql += "" + tableName + ".brand_type = ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".name_en like ?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					
					else if(str.equals("sl"))
					{
						AllFieldSql += "" + tableName + ".sl like ?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					
					else if(str.equals("receiving_date_start"))
					{
						AllFieldSql += "" + tableName + ".receiving_date >= ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("receiving_date_end"))
					{
						AllFieldSql += "" + tableName + ".receiving_date <= ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("procurement_type_cat"))
					{
						AllFieldSql += "" + tableName + ".procurement_type_cat = ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
				
					else if(str.equals("project_name"))
					{
						AllFieldSql += "" + tableName + ".project_name like ?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					
					else if(str.equals("asset_model_status_cat"))
					{
						AllFieldSql += "" + tableName + ".asset_model_status_cat = ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase("") && !filter.equalsIgnoreCase("USE_DEFAULT_SORT"))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		
		if(category == GETDTOS)
		{
			if(filter.equalsIgnoreCase("USE_DEFAULT_SORT"))
			{
				sql += " order by lastModificationTime desc ";
			}
			else
			{
				sql += " order by asset_category_type asc, " + tableName + ".id desc ";
			}
			
		}

		//printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}

		
		return sql;
    }
	
				
}
	