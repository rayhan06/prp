package asset_model;
import java.util.*;

import asset_category.Asset_categoryDTO;
import asset_type.Asset_typeDTO;
import pb.CatDAO;
import software_type.Software_typeDAO;
import software_type.Software_typeDTO;
import util.*; 


public class Asset_modelDTO extends CommonDTO
{
	
	public static int HARDWARE = 0;
	public static int SOFTWARE = 1;

	public long assetCategoryType = -1;
    public String lot = "";
	public int brandType = -1;
    public String nameEn = "";
    public String nameBn = "";
    public String sl = "";
	public int quantity = 1;
	
	
    public int processorCat = -1; //laptop, cpu
    public int processorGenCat = -1; //laptop, cpu
    public int ramCat = -1; //laptop, cpu
    public int ramSizeCat = -1; //laptop, cpu
    public int hdCat = -1; //laptop, cpu
    public int hdSizeCat = -1; //laptop, cpu
    public int displaySizeCat = -1; //laptop, cpu
	
    public boolean purchaseInformationAvailable = false;
	
	
    public String capacity = ""; //ups
    public int upsCapacityCat = -1; //ups
    public String idfNumber = ""; //ups
    
    public String mac = ""; //data centre, switch
    public String ip = ""; //data centre, switch
    
    public String location = ""; //data centre, switch, network accessories, camera
    public String partNumber = ""; //data centre, switch, network accessories, camera
    
    public String hostName = ""; //switch
    public String managementVlan = ""; //switch
    
    public String productName = ""; //switch
    
    public int hsCat = HARDWARE;
    
    public int printerTypeCat = -1;
    public int printerNetworkTypeCat = -1;
    public int scannerTypeCat = -1;
    public int upsTypeCat = -1;
    public int monitorTypeCat = -1;
    
    public int dataCenterTypeCat = -1;

    
    
	public int assetSupplierType = -1;
    public String poNumber = "";
	public long receivingDate = System.currentTimeMillis();
	public long expiryDate = System.currentTimeMillis();
	public boolean hasWarranty = false;
	public boolean hasTOE = false;
	public int procurementTypeCat = -1;

    public String projectName = "";
	public double costingValue = 0;
	public int assetModelStatusCat = -1;
	
	public long softwareTypeId = -1;
	public long assetTypeId = -1;
	
	public int softwareCat = -1;
	public int softwareSubCat = -1;

	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
    public boolean isImmutable = false;
    
    public String warehouseAddress = "";
    
    public static final int PAGE_ASSET = 1;
    public static final int PAGE_QR = 2;
	
	public List<AssetAssigneeDTO> assetAssigneeDTOList = new ArrayList<>();
	
	public Asset_modelDTO()
	{
		
	}
	
	public Asset_modelDTO(Software_typeDTO software_typeDTO)
	{
		lot = software_typeDTO.lot;
		softwareTypeId = software_typeDTO.iD;
		assetTypeId = Asset_typeDTO.SOFTWARE;
		assetCategoryType = Asset_categoryDTO.SOFTWARE;
		
		String domainName = Software_typeDAO.getInstance().getDomainName(software_typeDTO.softwareCat);
		
		nameEn = CatDAO.getName("English", "software", software_typeDTO.softwareCat) + " " 
				+ CatDAO.getName("English", domainName, software_typeDTO.softwareSubCat);
		nameBn = CatDAO.getName("Bangla", "software", software_typeDTO.softwareCat) +  " " 
				+ CatDAO.getName("Bangla", domainName, software_typeDTO.softwareSubCat);
		
		insertedByUserId = software_typeDTO.insertedByUserId;
		insertedByOrganogramId = software_typeDTO.insertedByOrganogramId;
		insertionDate = software_typeDTO.insertionDate;
		lastModifierUser = software_typeDTO.lastModifierUser;
		
		softwareCat = software_typeDTO.softwareCat;
		softwareSubCat = software_typeDTO.softwareSubCat;
		expiryDate = software_typeDTO.expiryDate;
		
		if(software_typeDTO.hasLicense)
		{
			hasWarranty = true;
		}
		
		this.quantity = software_typeDTO.quantity;
		isImmutable = true;

	}
	
    @Override
	public String toString() {
            return "$Asset_modelDTO[" +
            " iD = " + iD +
            " assetCategoryType = " + assetCategoryType +
            " lot = " + lot +
            " brandType = " + brandType +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " sl = " + sl +
            " quantity = " + quantity +
           
            " capacity = " + capacity +
            " idfNumber = " + idfNumber +
            " vendorCat = " + assetSupplierType +
            " poNumber = " + poNumber +
            " receivingDate = " + receivingDate +
            " procurementTypeCat = " + procurementTypeCat +

            " projectName = " + projectName +
            " costingValue = " + costingValue +
            " assetModelStatusCat = " + assetModelStatusCat +
            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}