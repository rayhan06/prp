package asset_model;

import java.io.*;
import java.text.SimpleDateFormat;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

import employee_offices.EmployeeOfficeRepository;

import org.apache.log4j.Logger;
import login.LoginDTO;
import pb_notifications.Pb_notificationsDAO;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import support_ticket.Support_ticketDAO;
import support_ticket.Support_ticketDTO;

import ticket_forward_history.Ticket_forward_historyDAO;
import ticket_forward_history.Ticket_forward_historyDTO;
import user.UserDTO;
import user.UserRepository;
import util.*;
import java.util.*;
import javax.servlet.http.*;
import language.LC;
import language.LM;
import common.ApiResponse;
import com.google.gson.Gson;
import asset_category.Asset_categoryDAO;
import asset_category.Asset_categoryDTO;
import asset_category.Asset_categoryRepository;
import asset_transaction.Asset_transactionDAO;
import asset_transaction.Asset_transactionDTO;
import brand.BrandDAO;
import brand.BrandDTO;
import pb.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Asset_modelServlet
 */
@WebServlet("/Asset_modelServlet")
@MultipartConfig
public class Asset_modelServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Asset_modelServlet.class);

    String tableName = "asset_model";

	Asset_modelDAO asset_modelDAO;
	CommonRequestHandler commonRequestHandler;
	AssetAssigneeDAO assetAssigneeDAO;
	Asset_categoryDAO asset_categoryDAO = new Asset_categoryDAO();
    private final Gson gson = new Gson();
    Asset_transactionDAO asset_transactionDAO = new Asset_transactionDAO();
    Support_ticketDAO support_ticketDAO = new Support_ticketDAO();
    AssetXL assetXL;
    AssetAssignmentHadler assetAssignmentHadler;
    Ticket_forward_historyDAO ticket_forward_historyDAO = new Ticket_forward_historyDAO();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Asset_modelServlet() 
	{
        super();
    	try
    	{
			asset_modelDAO = new Asset_modelDAO(tableName);
			assetAssigneeDAO = new AssetAssigneeDAO("asset_assignee");
			commonRequestHandler = new CommonRequestHandler(asset_modelDAO);
			assetXL = new AssetXL(asset_modelDAO, assetAssigneeDAO,
		    		asset_categoryDAO, asset_transactionDAO, commonRequestHandler);
			assetAssignmentHadler = new AssetAssignmentHadler(asset_modelDAO, assetAssigneeDAO,
		    		asset_categoryDAO, asset_transactionDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_UPDATE))
				{
					getAsset_model(request, response, false);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getAssignmentPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_UPDATE))
				{
					getAsset_model(request, response, true);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getUserAsset"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH) 
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH))
				{
					long organogramId = Long.parseLong(request.getParameter("organogramId"));
					request.setAttribute("organogramId", organogramId);
					if(request.getParameter("assets") != null)
					{
						System.out.println("assets = " + request.getParameter("assets"));
						request.setAttribute("assets", request.getParameter("assets"));
					}
					else
					{
						System.out.println("assets = null");
					}
					request.getRequestDispatcher("asset_model/userAssets.jsp" ).forward(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("viewAssetAssignee"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH) 
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH))
				{
					request.getRequestDispatcher("asset_model/viewAssetAssignee.jsp" ).forward(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getUserAssetDetails"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH) 
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH))
				{
					
					long organogramId = Long.parseLong(request.getParameter("organogramId"));
					int ownerCat = Integer.parseInt(request.getParameter("ownerCat"));
					int roomNoCat = Integer.parseInt(request.getParameter("roomNoCat"));
					int type = Integer.parseInt(request.getParameter("type"));
					System.out.println("getUserAssetDetails organogramId = " + organogramId);
					request.setAttribute("organogramId", organogramId);
					request.setAttribute("ownerCat", ownerCat);
					request.setAttribute("roomNoCat", roomNoCat);
					request.setAttribute("type", type);
					request.getRequestDispatcher("asset_model/userAssetDetails.jsp" ).forward(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getUserAssetEdit"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH) 
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH))
				{					
					long organogramId = Long.parseLong(request.getParameter("organogramId"));
					int ownerCat = Integer.parseInt(request.getParameter("ownerCat"));
					int roomNoCat = Integer.parseInt(request.getParameter("roomNoCat"));
					System.out.println("getUserAssetDetails organogramId = " + organogramId);
					request.setAttribute("organogramId", organogramId);
					request.setAttribute("ownerCat", ownerCat);
					request.setAttribute("roomNoCat", roomNoCat);
					request.getRequestDispatcher("asset_model/userAssetsEdit.jsp" ).forward(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getModelsByCategory"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH) 
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH))
				{					
					long cat = Long.parseLong(request.getParameter("cat"));
					List<Asset_modelDTO> asset_modelDTOs = (List<Asset_modelDTO>)asset_modelDAO.getDTOsByParent("asset_category_type", cat);	
					String options = "<option value = '-1'>" + LM.getText(LC.HM_SELECT, loginDTO) + "</option>";
					for(Asset_modelDTO asset_modelDTO: asset_modelDTOs)
					{
						options += "<option value='" + asset_modelDTO.iD + "'>" + asset_modelDTO.nameEn + "</option>";
					}
					response.getWriter().write(options);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getModelsNameByCategory"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH) 
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH))
				{					
					long cat = Long.parseLong(request.getParameter("cat"));
					List<String> names = (List<String>)asset_modelDAO.getDistinctModelNamesByCat(cat);	
					String options = "<option value = ''>" + LM.getText(LC.HM_SELECT, loginDTO) + "</option>";
					for(String name: names)
					{
						options += "<option value='" + name + "'>" + name + "</option>";
					}
					response.getWriter().write(options);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getCategories"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH) 
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH))
				{					
					long type = Long.parseLong(request.getParameter("type"));
					List<Asset_categoryDTO> asset_categoryDTOs = (List<Asset_categoryDTO>)asset_categoryDAO.getDTOsByParent("asset_type_type", type);	
					String options = "<option value = '-1'>" + LM.getText(LC.HM_SELECT, loginDTO) + "</option>";
					for(Asset_categoryDTO asset_categoryDTO: asset_categoryDTOs)
					{
						options += "<option value='" + asset_categoryDTO.iD + "'>" + asset_categoryDTO.nameEn + "</option>";
					}
					response.getWriter().write(options);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getBrandsByCategory"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH) 
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH))
				{
					BrandDAO brandDAO = new BrandDAO();
					long cat = Long.parseLong(request.getParameter("cat"));
					List<BrandDTO> brandDTOs = (List<BrandDTO>)brandDAO.getDTOsByParent("asset_category_type", cat);	
					String options = "<option value = '-1'>" + LM.getText(LC.HM_SELECT, loginDTO) + "</option>";
					for(BrandDTO brandDTO: brandDTOs)
					{
						options += "<option value='" + brandDTO.iD + "'>" + brandDTO.nameEn + "</option>";
					}
					response.getWriter().write(options);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getOneDTOByModel"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH) 
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH))
				{					
					long model = Long.parseLong(request.getParameter("model"));
					String sl = request.getParameter("sl");
					String usedAssigneeIds = request.getParameter("usedAssigneeIds");
					if(usedAssigneeIds.endsWith(","))
					{
						usedAssigneeIds = usedAssigneeIds.substring(0, usedAssigneeIds.length() - 1);
					}
					System.out.println("getting dto for model = " + model + " sl = " + sl + " usedAssigneeIds= " + usedAssigneeIds);
					AssetAssigneeDTO assetAssigneeDTO = assetAssigneeDAO.get1stUnassignedByModelAndSl(model, sl, usedAssigneeIds);
					if(assetAssigneeDTO != null)
					{
						System.out.println("found assetAssigneeDTO id = " + assetAssigneeDTO.iD);
					}
					else
					{
						System.out.println("found assetAssigneeDTO id = null");
					}
					if(assetAssigneeDTO == null && !sl.equalsIgnoreCase(""))
					{
						System.out.println("Trying again with no sl");
						assetAssigneeDTO = assetAssigneeDAO.get1stUnassignedByModelAndSl(model, "", usedAssigneeIds);
						if(assetAssigneeDTO != null)
						{
							assetAssigneeDTO.sl = sl;
						}
						
					}
					String res = gson.toJson(assetAssigneeDTO);
					response.setContentType( "application/json" );
					response.getWriter().write( res );
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getOneSoftware"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH) 
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH))
				{					
					long softwareCat = Long.parseLong(request.getParameter("softwareCat"));
					long softwareSubCat = Long.parseLong(request.getParameter("softwareSubCat"));
					long assetAssigneeIdWithKey = Long.parseLong(request.getParameter("assetAssigneeIdWithKey"));
					int license = Integer.parseInt(request.getParameter("license"));
					String usedAssigneeIds = request.getParameter("usedAssigneeIds");
					if(usedAssigneeIds.endsWith(","))
					{
						usedAssigneeIds = usedAssigneeIds.substring(0, usedAssigneeIds.length() - 1);
					}
					System.out.println("getting dto for softwareCat = " + softwareCat + " softwareSubCat = " + softwareSubCat + " usedAssigneeIds= " + usedAssigneeIds);
					AssetAssigneeDTO assetAssigneeDTO = null;
					if(license == 0)
					{
						assetAssigneeDTO = assetAssigneeDAO.get1stUnassignedSoft(softwareCat, softwareSubCat, usedAssigneeIds, license);
					}
					else
					{
						assetAssigneeDTO = (AssetAssigneeDTO) assetAssigneeDAO.getDTOByID(assetAssigneeIdWithKey);
					}
					
					if(assetAssigneeDTO != null)
					{
						System.out.println("found assetAssigneeDTO id = " + assetAssigneeDTO.iD);
					}
					else
					{
						System.out.println("found assetAssigneeDTO id = null");
					}
					
					String res = gson.toJson(assetAssigneeDTO);
					response.setContentType( "application/json" );
					response.getWriter().write( res );
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getKeys"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH) 
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH))
				{					
					long softwareCat = Long.parseLong(request.getParameter("softwareCat"));
					long softwareSubCat = Long.parseLong(request.getParameter("softwareSubCat"));
					int license = Integer.parseInt(request.getParameter("license"));
					String usedAssigneeIds = request.getParameter("usedAssigneeIds");
					if(usedAssigneeIds.endsWith(","))
					{
						usedAssigneeIds = usedAssigneeIds.substring(0, usedAssigneeIds.length() - 1);
					}
					System.out.println("getting dto for softwareCat = " + softwareCat + " softwareSubCat = " + softwareSubCat + " usedAssigneeIds= " + usedAssigneeIds);
					String options = "<option value = '-1'>"
							+ LM.getText(LC.HM_SELECT, loginDTO)
							+ "</option>";
					if(license != 0)
					{
						List<AssetAssigneeDTO> assetAssigneeDTOs = assetAssigneeDAO.getUnassignedSofts(softwareCat, softwareSubCat, usedAssigneeIds, license);
						if(assetAssigneeDTOs != null)
						{
							for(AssetAssigneeDTO assetAssigneeDTO: assetAssigneeDTOs)
							{
								options += "<option value = '" + assetAssigneeDTO.iD + "'>" + assetAssigneeDTO.softwareKey + "</option>";
							}
						}
					}
					
					response.getWriter().write( options );
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("viewUserAssets"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH) 
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH))
				{
					request.getRequestDispatcher("asset_model/viewUserAssets.jsp" ).forward(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getAddUserAssetsPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_ADD) 
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_ADD))
				{
					request.getRequestDispatcher("asset_model/addUserAssets.jsp" ).forward(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("xl"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH))
				{
					assetXL.getXl(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("xlAssignee"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH))
				{
					assetXL.getXlFromAssignee(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = "USE_DEFAULT_SORT"; //shouldn't be directly used, rather manipulate it.
							searchAsset_model(request, response, isPermanentTable, filter);
						}
						else
						{
							searchAsset_model(request, response, isPermanentTable, "USE_DEFAULT_SORT");
						}
					}					
					else
					{
						//searchAsset_model(request, response, tempTableName, isPermanentTable);
					}
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("assetCondemnation"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH)
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH))
				{
					request.getRequestDispatcher("asset_model/assetCondemnation.jsp" ).forward(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("assetMaintenance"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH)
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH))
				{
					request.getRequestDispatcher("asset_assignee/assetMaintenance.jsp" ).forward(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("assetActivation"))
			{
				long assetAssigneeId = Long.parseLong(request.getParameter("asset_assignee_id"));
				activateCondemnedAsset(assetAssigneeId, userDTO);
				response.sendRedirect("Asset_modelServlet?actionType=searchAssetAssignee");
			}
			else if(actionType.equals("assetReturnFromWarranty"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH)
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH))
				{
					request.getRequestDispatcher("asset_assignee/returnFromRepair.jsp" ).forward(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("assetReturnFromRepair"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH)
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH))
				{
					request.getRequestDispatcher("asset_assignee/returnFromRepair.jsp" ).forward(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("assetRelease"))
			{
				long assetAssigneeId = Long.parseLong(request.getParameter("asset_assignee_id"));
				releaseAsset(assetAssigneeId, userDTO);
				response.sendRedirect("Asset_modelServlet?actionType=searchAssetAssignee");
			}
			else if(actionType.equals("transfer"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_UPDATE))
				{					
					assetAssignmentHadler.transfer(request, response, userDTO);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("searchAssetAssignee"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_ASSIGNEE_SEARCH))
				{
					String filter = request.getParameter("filter");
					if(filter == null)
					{
						filter = "";
					}
					else if(filter.equalsIgnoreCase(AssetAssigneeDTO.ALLEXCEPTSOFT + ""))
					{
						filter = " asset_category_type != " + Asset_categoryDTO.SOFTWARE + " ";
					}
					else if(filter.equalsIgnoreCase(AssetAssigneeDTO.OTHER + ""))
					{
						filter = " (assignment_status != " + AssetAssigneeDTO.ASSIGNED 
								+ " and assignment_status != "+ AssetAssigneeDTO.NOT_ASSIGNED 
								+ " and assignment_status != "+ AssetAssigneeDTO.CONDEMNED
								+ ")"
								+ " and asset_category_type != " + Asset_categoryDTO.SOFTWARE + " ";
					}
					else
					{
						filter = " assignment_status = " + Integer.parseInt(filter) + " and asset_category_type != " + Asset_categoryDTO.SOFTWARE + " ";
 					}
					searchAssetAssignee(request, response, filter);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_ADD))
				{
					System.out.println("going to  addAsset_model ");
					addAsset_model(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addAsset_model ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addAsset_model ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("getDTOByName"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_ADD))
				{
					getDTOByName(request, response);					
				}
				else
				{
					System.out.println("Not going to  addAsset_model ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_UPDATE))
				{					
					addAsset_model(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("assign"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_UPDATE))
				{					
					//assetAssignmentHadler.assign(request, response, userDTO);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("massAssign"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_UPDATE))
				{					
					assetAssignmentHadler.massAssign(request, response, userDTO);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			
			else if(actionType.equals("activate"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_UPDATE))
				{					
					activate(request, response, userDTO);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_UPDATE))
				{
					delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_MODEL_SEARCH))
				{
					searchAsset_model(request, response, true, "USE_DEFAULT_SORT");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}		
			
			else if(actionType.equals("addCondemnationReason"))
			{
				long assetAssigneeId = Long.parseLong(request.getParameter("asset_assignee_id"));
				String condemnationReason = request.getParameter("condemnationReason");
				updateAssetManagementTables(assetAssigneeId, condemnationReason, userDTO);
				response.sendRedirect("Asset_modelServlet?actionType=searchAssetAssignee");
			}
			else if(actionType.equals("addAssetMaintenance"))
			{
				long assetAssigneeId = Long.parseLong(request.getParameter("asset_assignee_id"));
				String vendor = request.getParameter("vendor");
				String isWarranty = request.getParameter("isWarranty");
				String comment = request.getParameter("comment");
				updateAssetMaintenance(assetAssigneeId, vendor, comment, isWarranty, userDTO);
				response.sendRedirect("Asset_modelServlet?actionType=searchAssetAssignee");
			}
			else if(actionType.equals("addReturnFromMaintenance"))
			{
				long assetAssigneeId = Long.parseLong(request.getParameter("asset_assignee_id"));
				String cost = request.getParameter("cost");
				String isWarranty = request.getParameter("isWarranty");
				String comment = request.getParameter("comment");
				returnFromMaintenance(assetAssigneeId, Double.parseDouble(cost), comment, Integer.parseInt(isWarranty), userDTO);
				response.sendRedirect("Asset_modelServlet?actionType=searchAssetAssignee");
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void updateAssetMaintenance(long assetAssigneeId, String vendor, String comment, String isWarranty, UserDTO userDTO) throws Exception {
		long vendor_id;
		AssetAssigneeDAO assetAssigneeDAO = new AssetAssigneeDAO();
		Asset_transactionDAO asset_transactionDAO = new Asset_transactionDAO();
		Ticket_forward_historyDAO ticket_forward_historyDAO = new Ticket_forward_historyDAO();
		Ticket_forward_historyDTO ticket_forward_historyDTO = null;

		AssetAssigneeDTO assetAssigneeDTO = AssetAssigneeRepository.getInstance().getByID(assetAssigneeId);
		Asset_modelDTO assetModelDTO = Asset_modelRepository.getInstance().getAsset_modelDTOByID(assetAssigneeDTO.assetModelId);

		if (isWarranty.equals("1")) {
			assetAssigneeDTO.assignmentStatus = AssetAssigneeDTO.WARRANTY;
		} else {
			assetAssigneeDTO.assignmentStatus = AssetAssigneeDTO.SEND_TO_REPAIR;
		}

		try {
			vendor_id = Long.parseLong(vendor);
		} catch(Exception ex) {
			logger.debug(ex);
			vendor_id = -1;
		}

		Asset_transactionDTO asset_transactionDTO = new Asset_transactionDTO(assetModelDTO, assetAssigneeDTO, assetAssigneeDTO.assignmentStatus, vendor_id , assetAssigneeDTO.assignmentStatus, comment);

		updateTicketStatus(assetAssigneeDTO, assetAssigneeDTO.assignmentStatus, userDTO);

		// Update the tables
		assetAssigneeDAO.update(assetAssigneeDTO);
		asset_transactionDAO.add(asset_transactionDTO);

	}


	private void returnFromMaintenance(long assetAssigneeId, double cost, String comment, int isWarranty, UserDTO userDTO) throws Exception {
		AssetAssigneeDTO assetAssigneeDTO = AssetAssigneeRepository.getInstance().getByID(assetAssigneeId);

		if(assetAssigneeDTO.assignedOrganogramId==-1){
			assetAssigneeDTO.assignmentStatus = AssetAssigneeDTO.NOT_ASSIGNED;
		} else {
			assetAssigneeDTO.assignmentStatus = AssetAssigneeDTO.ASSIGNED;
		}

		Asset_modelDTO assetModelDTO = Asset_modelRepository.getInstance().getAsset_modelDTOByID(assetAssigneeDTO.assetModelId);


		Asset_transactionDTO asset_transactionDTO = new Asset_transactionDTO(assetModelDTO, assetAssigneeDTO, Asset_transactionDTO.ACTIVATE, comment);
		if (isWarranty==0) {
			asset_transactionDTO.cost = cost;
		}

		updateTicketStatus(assetAssigneeDTO, Ticket_forward_historyDTO.ACTIVATE, userDTO);
		// Update the tables
		assetAssigneeDAO.update(assetAssigneeDTO);
		asset_transactionDAO.add(asset_transactionDTO);
		
	}

	private void releaseAsset(long assetAssigneeId, UserDTO userDTO) throws Exception {
		AssetAssigneeDTO assetAssigneeDTO = AssetAssigneeRepository.getInstance().getByID(assetAssigneeId);
		Asset_modelDTO assetModelDTO = Asset_modelRepository.getInstance().getAsset_modelDTOByID(assetAssigneeDTO.assetModelId);
		assetAssigneeDTO.assignmentStatus = AssetAssigneeDTO.RELEASED;

		Asset_transactionDTO asset_transactionDTO = new Asset_transactionDTO(assetModelDTO, assetAssigneeDTO, Asset_transactionDTO.RELEASE, "Asset Released");
		
		updateTicketStatus(assetAssigneeDTO, Ticket_forward_historyDTO.RELEASE, userDTO);

		// Update the tables
		assetAssigneeDAO.update(assetAssigneeDTO);
		asset_transactionDAO.add(asset_transactionDTO);
		
	}


	private void updateAssetManagementTables(long assetAssigneeId, String condemnationReason, UserDTO userDTO) throws Exception {
		AssetAssigneeDAO assetAssigneeDAO = new AssetAssigneeDAO();
		Asset_transactionDAO asset_transactionDAO = new Asset_transactionDAO();
		Ticket_forward_historyDAO ticket_forward_historyDAO = new Ticket_forward_historyDAO();
		Ticket_forward_historyDTO ticket_forward_historyDTO = null;

		AssetAssigneeDTO assetAssigneeDTO = AssetAssigneeRepository.getInstance().getByID(assetAssigneeId);
		Asset_modelDTO assetModelDTO = Asset_modelRepository.getInstance().getAsset_modelDTOByID(assetAssigneeDTO.assetModelId);
		assetAssigneeDTO.condemnationReason = condemnationReason;
		assetAssigneeDTO.isActive = false;
		assetAssigneeDTO.assignedOrganogramId = -1;    			// unassigned the asset holder
		assetAssigneeDTO.assignmentStatus = AssetAssigneeDTO.CONDEMNED;

		Asset_transactionDTO asset_transactionDTO = new Asset_transactionDTO(assetModelDTO, assetAssigneeDTO, Asset_transactionDTO.CONDEMN, assetAssigneeDTO.condemnationReason);
		updateTicketStatus(assetAssigneeDTO, Ticket_forward_historyDTO.CONDEMN, userDTO);


		// Update the tables
		assetAssigneeDAO.update(assetAssigneeDTO);
		asset_transactionDAO.add(asset_transactionDTO);
		
	}


	private void notifyTicketUsers(AssetAssigneeDTO dto, int status)
	{
		String URL = "Asset_modelServlet?actionType=searchAssetAssignee";
		String englishText;
		String banglaText;
		String subject;
		if (status==Ticket_forward_historyDTO.CONDEMN) {
			englishText = "An asset, Model:" + dto.model + ", has been condemned!";
			banglaText = dto.model + " মডেলের একটি সম্পদকে অব্যবহার্য বলে ঘোষণা করা হয়েছে!";
			subject = "Asset Condemnation Notification";
		} else if(status==Ticket_forward_historyDTO.SEND_TO_REPAIR){
			englishText = "An asset, Model:" + dto.model + ", has been sent to Maintenance!";
			banglaText = dto.model + " মডেলের একটি সম্পদকে মেন্টেনেন্সের জন্য পাঠানো হচ্ছে!";
			subject = "Asset Maintenance Notification";
		}
		else if(status==Ticket_forward_historyDTO.WARRANTY){
			englishText = "An asset, Model:" + dto.model + ", has been sent to Warranty Claim!";
			banglaText = dto.model + " মডেলের একটি সম্পদকে ওয়্যারেন্টিতে  পাঠানো হচ্ছে!";
			subject = "Asset Maintenance Notification";
		}
		else if(status==Ticket_forward_historyDTO.RELEASE){
			englishText = "An asset, Model:" + dto.model + ", has been released!";
			banglaText = dto.model + " মডেলের একটি সম্পদকে খালাস করা হয়েছে!";
			subject = "Asset Release Notification";
		}
		else {
		englishText = "An asset, Model:" + dto.model + ", has returned from Maintenance!";
		banglaText = dto.model + " মডেলের একটি সম্পদকে মেন্টেনেন্সের থেকে পাঠানো হচ্ছে!";
		subject = "Asset Maintenance Notification";
		}

		Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();
		Set<Long> ticketAdmins = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.TICKET_ADMIN_ROLE);
		for(Long ticketAdmin: ticketAdmins)
		{
			pb_notificationsDAO.addPb_notificationsAndSendMailSMS(ticketAdmin, System.currentTimeMillis(), URL, englishText, banglaText, subject, SessionConstants.TICKET_ADMIN_ROLE);
		}
		pb_notificationsDAO.addPb_notificationsAndSendMailSMS(dto.assignedOrganogramId, System.currentTimeMillis(), URL, englishText, banglaText, subject);
	}
	
	public void updateTicketStatus(AssetAssigneeDTO assetAssigneeDTO, int status, UserDTO userDTO)
	{
		/*Support_ticketDTO support_ticketDTO = support_ticketDAO.getSupportTicketDTOByAssigneeId(assetAssigneeDTO.iD);
		if (support_ticketDTO != null) {
			Ticket_forward_historyDTO ticket_forward_historyDTO = new Ticket_forward_historyDTO(userDTO.organogramID, "", 
					status, support_ticketDTO.iD);
			try {
				ticket_forward_historyDAO.add(ticket_forward_historyDTO);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
			System.out.println("support_ticketDTO is null");
		}
		
		notifyTicketUsers(assetAssigneeDTO, status);*/
	}

	private void activateCondemnedAsset(long assetAssigneeId, UserDTO userDTO) throws Exception {
		AssetAssigneeDAO assetAssigneeDAO = new AssetAssigneeDAO();
		Asset_transactionDAO asset_transactionDAO = new Asset_transactionDAO();
		
		Ticket_forward_historyDTO ticket_forward_historyDTO = null;

		AssetAssigneeDTO assetAssigneeDTO = AssetAssigneeRepository.getInstance().getByID(assetAssigneeId);
		Asset_modelDTO assetModelDTO = Asset_modelRepository.getInstance().getAsset_modelDTOByID(assetAssigneeDTO.assetModelId);
		assetAssigneeDTO.isActive = true;
		assetAssigneeDTO.assignmentStatus = AssetAssigneeDTO.NOT_ASSIGNED;

		Asset_transactionDTO asset_transactionDTO = new Asset_transactionDTO(assetModelDTO, assetAssigneeDTO, Asset_transactionDTO.ACTIVATE, "Asset is Activated from condemnation!");

		// Add an entry to ticket_forward_history if a ticket is raised
		Support_ticketDTO support_ticketDTO = support_ticketDAO.getSupportTicketDTOByAssigneeId(assetAssigneeId);
		if (support_ticketDTO != null) {
			ticket_forward_historyDTO = new Ticket_forward_historyDTO(userDTO.organogramID, "Asset is activated from condemnation!",
					Ticket_forward_historyDTO.ACTIVATE, support_ticketDTO.iD);
		}
		else
		{
			System.out.println("support_ticketDTO is null");
		}

		// Update the tables
		assetAssigneeDAO.update(assetAssigneeDTO);
		asset_transactionDAO.add(asset_transactionDTO);
		if (ticket_forward_historyDTO != null) {
			ticket_forward_historyDAO.add(ticket_forward_historyDTO);
		}
	}




	private void activate(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
		long id = Long.parseLong(request.getParameter("id"));
		Asset_modelDTO asset_modelDTO = asset_modelDAO.getDTOByID(id, false);
		ArrayList<Long> activeIds = new ArrayList<Long>();
		ArrayList<Long> deActiveIds = new ArrayList<Long>();
		if(request.getParameterValues("assigneeId") != null)
		{
			int i = 0;
			for(String assigneeIdStr: request.getParameterValues("assigneeId"))
			{
				long assignedId = Long.parseLong(assigneeIdStr);
				boolean isActive = Boolean.parseBoolean(request.getParameterValues("isActive")[i]);
				if(isActive)
				{
					activeIds.add(assignedId);
				}
				else
				{
					deActiveIds.add(assignedId);
				}
				i++;
			}
			ArrayList<AssetAssigneeDTO> assetAssigneeDTOs = (ArrayList<AssetAssigneeDTO>) assetAssigneeDAO.getDTOsByParent("asset_model_id", id);
			for(AssetAssigneeDTO assetAssigneeDTO: assetAssigneeDTOs)
			{
				if(assetAssigneeDTO.isActive && deActiveIds.contains(assetAssigneeDTO.iD))
				{
					Asset_transactionDTO asset_transactionDTO = new Asset_transactionDTO(asset_modelDTO, assetAssigneeDTO, Asset_transactionDTO.DEACTIVATE);
					try {
						asset_transactionDAO.add(asset_transactionDTO);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else if(!assetAssigneeDTO.isActive && activeIds.contains(assetAssigneeDTO.iD))
				{
					Asset_transactionDTO asset_transactionDTO = new Asset_transactionDTO(asset_modelDTO, assetAssigneeDTO, Asset_transactionDTO.ACTIVATE);
					try {
						asset_transactionDAO.add(asset_transactionDTO);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			assetAssigneeDAO.activate(activeIds, true);
			assetAssigneeDAO.activate(deActiveIds, false);
		}
		try {
			response.sendRedirect("Asset_modelServlet?actionType=view&ID=" + id);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void delete(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO)
	{
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);							
				Asset_modelDTO assetModelDTO = asset_modelDAO.getDTOByID(id, false);
				
				asset_modelDAO.manageWriteOperations(assetModelDTO, SessionConstants.DELETE, id, userDTO);
				assetAssigneeDAO.deleteChildrenByParent(assetModelDTO.iD, "asset_model_id");
				asset_transactionDAO.deleteChildrenByParent(assetModelDTO.iD, "asset_model_id");
								
			}
			response.sendRedirect("Asset_modelServlet?actionType=search");
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}

	}
	

	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Asset_modelDTO asset_modelDTO = (Asset_modelDTO)asset_modelDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(asset_modelDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addAsset_model(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addAsset_model");
			String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
			Asset_modelDTO asset_modelDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				asset_modelDTO = new Asset_modelDTO();
			}
			else
			{
				asset_modelDTO = (Asset_modelDTO)asset_modelDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));				
			}
			
			String Value = "";

			Value = request.getParameter("assetCategoryType");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("assetCategoryType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_modelDTO.assetCategoryType = Long.parseLong(Value);
				if(asset_modelDTO.assetCategoryType < 0)
				{
					throw new Exception(LM.getText(LC.ASSET_MODEL_ADD_ASSETCATEGORYTYPE, language) + " " + ErrorMessage.getEmptyMessage(language));
				}
				Asset_categoryDTO assetCategoryDTO = Asset_categoryRepository.getInstance().getAsset_categoryDTOByID(asset_modelDTO.assetCategoryType);
				if(assetCategoryDTO== null)
				{
					throw new Exception(LM.getText(LC.ASSET_MODEL_ADD_ASSETCATEGORYTYPE, language) + " " + ErrorMessage.getInvalidMessage(language));
				}
				asset_modelDTO.assetTypeId = assetCategoryDTO.assetTypeType;
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("lot");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("lot = " + Value);
			if(Value != null)
			{
				asset_modelDTO.lot = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("brandType");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("brandType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_modelDTO.brandType = Integer.parseInt(Value);
				
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEn = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_modelDTO.nameEn = (Value);
				asset_modelDTO.nameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				throw new Exception(LM.getText(LC.ASSET_MODEL_ADD_NAMEEN, language) + " " + ErrorMessage.getEmptyMessage(language));
			}

			Value = request.getParameter("sl");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("sl = " + Value);
			if(Value != null)
			{
				asset_modelDTO.sl = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("quantity");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("quantity = " + Value);
			int oldQuantity = asset_modelDTO.quantity;
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				if(!asset_modelDTO.isImmutable)
				{
					asset_modelDTO.quantity = Integer.parseInt(Value);
					if(asset_modelDTO.quantity < 0)
					{
						throw new Exception(LM.getText(LC.ASSET_MODEL_ADD_QUANTITY ,language) + " " + ErrorMessage.getInvalidMessage(language));
					}
				}
				
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			
			
			
			Value = request.getParameter("processorCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("processorCat = " + Value);
			if(Value != null)
			{
				asset_modelDTO.processorCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("processorGenCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("processorGenCat = " + Value);
			if(Value != null)
			{
				asset_modelDTO.processorGenCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("ramCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("ramCat = " + Value);
			if(Value != null)
			{
				asset_modelDTO.ramCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("ramSizeCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("ramSizeCat = " + Value);
			if(Value != null)
			{
				asset_modelDTO.ramSizeCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("hdCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("hdCat = " + Value);
			if(Value != null)
			{
				asset_modelDTO.hdCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("hdSizeCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("hdSizeCat = " + Value);
			if(Value != null)
			{
				asset_modelDTO.hdSizeCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("displaySizeCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("displaySizeCat = " + Value);
			if(Value != null)
			{
				asset_modelDTO.displaySizeCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("purchaseInformationAvailable");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("purchaseInformationAvailable = " + Value);
            asset_modelDTO.purchaseInformationAvailable = Value != null;

			

			
			Value = request.getParameter("hsCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("hsCat = " + Value);
			if(Value != null)
			{
				asset_modelDTO.hsCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}


			
			Value = request.getParameter("upsCapacityCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("upsCapacityCat = " + Value);
			if(Value != null)
			{
				asset_modelDTO.upsCapacityCat = Integer.parseInt(Value);
				asset_modelDTO.capacity = CatDAO.getName("English", "ups_capacity", asset_modelDTO.upsCapacityCat);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("printerTypeCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("printerTypeCat = " + Value);
			if(Value != null)
			{
				asset_modelDTO.printerTypeCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("printerNetworkTypeCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("printerNetworkTypeCat = " + Value);
			if(Value != null)
			{
				asset_modelDTO.printerNetworkTypeCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("scannerTypeCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("scannerTypeCat = " + Value);
			if(Value != null)
			{
				asset_modelDTO.scannerTypeCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("upsTypeCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("upsTypeCat = " + Value);
			if(Value != null)
			{
				asset_modelDTO.upsTypeCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("monitorTypeCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("monitorTypeCat = " + Value);
			if(Value != null)
			{
				asset_modelDTO.monitorTypeCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			

			Value = request.getParameter("idfNumber");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("idfNumber = " + Value);
			if(Value != null)
			{
				asset_modelDTO.idfNumber = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("mac");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("mac = " + Value);
			if(Value != null)
			{
				asset_modelDTO.mac = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("ip");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("ip = " + Value);
			if(Value != null)
			{
				asset_modelDTO.ip = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("location");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("location = " + Value);
			if(Value != null)
			{
				asset_modelDTO.location = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("partNumber");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("partNumber = " + Value);
			if(Value != null)
			{
				asset_modelDTO.partNumber = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("hostName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("hostName = " + Value);
			if(Value != null)
			{
				asset_modelDTO.hostName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("managementVlan");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("managementVlan = " + Value);
			if(Value != null)
			{
				asset_modelDTO.managementVlan = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("productName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("productName = " + Value);
			if(Value != null)
			{
				asset_modelDTO.productName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			
			
			
			
			
			Value = request.getParameter("vendorCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("vendorCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_modelDTO.assetSupplierType = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("poNumber");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("poNumber = " + Value);
			if(Value != null)
			{
				asset_modelDTO.poNumber = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("receivingDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("receivingDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try 
				{
					Date d = f.parse(Value);
					asset_modelDTO.receivingDate = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
					throw new Exception(LM.getText(LC.ASSET_MODEL_ADD_RECEIVINGDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("expiryDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("expiryDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try 
				{
					Date d = f.parse(Value);
					asset_modelDTO.expiryDate = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
					throw new Exception(ErrorMessage.getGenericInvalidMessage(language));
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("procurementTypeCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("procurementTypeCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_modelDTO.procurementTypeCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("hasWarranty");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("hasWarranty = " + Value);
            asset_modelDTO.hasWarranty = Value != null;
			
			Value = request.getParameter("hasTOE");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("hasTOE = " + Value);
            asset_modelDTO.hasTOE = Value != null;
			
			Value = request.getParameter("dataCenterTypeCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("dataCenterTypeCat = " + Value);
			if(Value != null)
			{
				asset_modelDTO.dataCenterTypeCat = Integer.parseInt(Value);
			}
			else
			{
				asset_modelDTO.hasTOE = false;
			}

			Value = request.getParameter("projectName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("projectName = " + Value);
			if(Value != null)
			{
				asset_modelDTO.projectName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("costingValue");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("costingValue = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_modelDTO.costingValue = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("warehouseAddress");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("warehouseAddress = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_modelDTO.warehouseAddress = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("assetModelStatusCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("assetModelStatusCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_modelDTO.assetModelStatusCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}


			if(addFlag)
			{
				asset_modelDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				asset_modelDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{				
				asset_modelDTO.insertionDate = TimeConverter.getToday();
			}			


			asset_modelDTO.lastModifierUser = userDTO.userName;

			
			System.out.println("Done adding  addAsset_model dto = " + asset_modelDTO);
			long returnedID = -1;
			
			
			Asset_categoryDTO asset_categoryDTO = Asset_categoryRepository.getInstance().getAsset_categoryDTOByID(asset_modelDTO.assetCategoryType);
			if(asset_categoryDTO == null)
			{
				throw new Exception(LM.getText(LC.ASSET_MODEL_ADD_ASSETCATEGORYTYPE, language) + " " + ErrorMessage.getInvalidMessage(language));
			}
			
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				asset_modelDAO.setIsDeleted(asset_modelDTO.iD, CommonDTO.OUTDATED);
				returnedID = asset_modelDAO.add(asset_modelDTO);
				asset_modelDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = asset_modelDAO.manageWriteOperations(asset_modelDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = asset_modelDAO.manageWriteOperations(asset_modelDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
		
			if(!asset_modelDTO.isImmutable)
			{
				if(addFlag == true || isPermanentTable == false) //add or validate
				{
					if(asset_categoryDTO.isAssignable)
					{
						for(int i = 0; i < asset_modelDTO.quantity; i ++)
						{
							AssetAssigneeDTO assetAssigneeDTO = new AssetAssigneeDTO(asset_modelDTO);
							assetAssigneeDAO.add(assetAssigneeDTO);
							Asset_transactionDTO asset_transactionDTO = new Asset_transactionDTO(asset_modelDTO, assetAssigneeDTO, Asset_transactionDTO.STOCK_IN);
							asset_transactionDAO.add(asset_transactionDTO);
						}	
					}
					else
					{
						AssetAssigneeDTO assetAssigneeDTO = new AssetAssigneeDTO(asset_modelDTO);
						assetAssigneeDAO.add(assetAssigneeDTO);
						Asset_transactionDTO asset_transactionDTO = new Asset_transactionDTO(asset_modelDTO, assetAssigneeDTO, Asset_transactionDTO.STOCK_IN);
						asset_transactionDAO.add(asset_transactionDTO);
					}
				}			
				else
				{
					Asset_transactionDTO asset_transactionDTO;
					if(asset_categoryDTO.isAssignable)
					{
						if(asset_modelDTO.quantity > oldQuantity)
						{
							for(int i = 0; i < asset_modelDTO.quantity - oldQuantity; i ++)
							{
								AssetAssigneeDTO assetAssigneeDTO = new AssetAssigneeDTO(asset_modelDTO);
								assetAssigneeDAO.add(assetAssigneeDTO);
								asset_transactionDTO = new Asset_transactionDTO(asset_modelDTO, assetAssigneeDTO, Asset_transactionDTO.STOCK_IN);
								asset_transactionDAO.add(asset_transactionDTO);
							}	
						}
						else if (asset_modelDTO.quantity < oldQuantity)
						{
							assetAssigneeDAO.deleteNChildrenByParent(asset_modelDTO.iD, "asset_model_id", oldQuantity - asset_modelDTO.quantity);
							asset_transactionDAO.deleteNChildrenByParent(asset_modelDTO.iD, "asset_model_id", oldQuantity - asset_modelDTO.quantity);
						}
					}
					else
					{
						AssetAssigneeDTO assetAssigneeDTO = assetAssigneeDAO.getLastByModelId(asset_modelDTO.iD);
						if(assetAssigneeDTO != null)
						{
							assetAssigneeDTO = new AssetAssigneeDTO(asset_modelDTO);
							assetAssigneeDAO.update(assetAssigneeDTO);
						}					
					}
				}			
			}
					
			
			ApiResponse.sendSuccessResponse(response, "Asset_modelServlet?actionType=search");
			
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			ApiResponse.sendErrorResponse(response, e.getMessage());
		}
	}
	
	private void getDTOByName(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			String nameEn = request.getParameter("nameEn");
			Asset_modelDTO asset_modelDTO = Asset_modelRepository.getInstance().getAsset_modelDTOByNameEn(nameEn);
			if(asset_modelDTO != null)
			{
				PrintWriter out = response.getWriter();
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				
				String encoded = this.gson.toJson(asset_modelDTO);
				System.out.println("json encoded data = " + encoded);
				out.print(encoded);
				out.flush();
			}
			
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	private void getAsset_model(HttpServletRequest request, HttpServletResponse response, long id, boolean isAssignment) throws ServletException, IOException
	{
		System.out.println("in getAsset_model");
		Asset_modelDTO asset_modelDTO = null;
		try 
		{
			asset_modelDTO = (Asset_modelDTO)asset_modelDAO.getDTOByID(id);
			request.setAttribute("ID", asset_modelDTO.iD);
			request.setAttribute("asset_modelDTO",asset_modelDTO);
			request.setAttribute("asset_modelDAO",asset_modelDAO);
			request.setAttribute("isAssignment",isAssignment);
			
			
			String URL= "";
			String getBodyOnly = request.getParameter("getBodyOnly");
						
			if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
			{
				URL = "asset_model/asset_modelEditBody.jsp?actionType=edit";
			}
			else
			{
				URL = "asset_model/asset_modelEdit.jsp?actionType=edit";
			}				
						
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getAsset_model(HttpServletRequest request, HttpServletResponse response, boolean isAssignment) throws ServletException, IOException
	{
		getAsset_model(request, response, Long.parseLong(request.getParameter("ID")), isAssignment);	
	}
	
	private void searchAsset_model(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchAsset_model 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		request.setAttribute("navigator", "navASSET_MODEL");
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			"navASSET_MODEL",
			request,
			asset_modelDAO,
			"viewASSET_MODEL",
			null,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("asset_modelDAO",asset_modelDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to approval_path/commonApproval.jsp");
	        	rd = request.getRequestDispatcher("approval_path/commonApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to asset_model/asset_modelApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("asset_model/asset_modelApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to pb/search.jsp");
	        	rd = request.getRequestDispatcher("pb/search.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to asset_model/asset_modelSearchForm.jsp");
	        	rd = request.getRequestDispatcher("asset_model/asset_modelSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}


	private void searchAssetAssignee(HttpServletRequest request, HttpServletResponse response, String filter) throws ServletException, IOException
	{
		System.out.println("in searchAssetAssignee");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		request.setAttribute("navigator", "navASSET_ASSIGNEE");
		RecordNavigationManager4 rnManager = new RecordNavigationManager4(
				"navASSET_ASSIGNEE",
				request,
				assetAssigneeDAO,
				"viewASSET_ASSIGNEE",
				null,
				"asset_assignee",
				true,
				userDTO,
				filter,
				true);
		try
		{
			System.out.println("trying to dojob");
			rnManager.doJob(loginDTO);
		}
		catch(Exception e)
		{
			System.out.println("failed to dojob" + e);
		}

		request.setAttribute("assetAssigneeDAO",assetAssigneeDAO);
		RequestDispatcher rd;

		if(!hasAjax)
		{
			rd = request.getRequestDispatcher("asset_assignee/asset_assigneeSearch.jsp");
		}
		else
		{
			rd = request.getRequestDispatcher("asset_assignee/asset_assigneeSearchForm.jsp");
		}

		rd.forward(request, response);
	}

}

