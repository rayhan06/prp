package relation;

import common.NameDao;


public class RelationDAO extends NameDao {

    private RelationDAO() {
        super("relation");
    }

    private static class LazyLoader{
        static final RelationDAO INSTANCE = new RelationDAO();
    }

    public static RelationDAO getInstance(){
        return LazyLoader.INSTANCE;
    }
}
	