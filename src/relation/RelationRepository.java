package relation;

import common.NameDTO;
import common.NameRepository;

public class RelationRepository extends NameRepository {

    private RelationRepository() {
        super(RelationDAO.getInstance(), RelationRepository.class);
    }

    private static class LazyLoader {
        static RelationRepository INSTANCE = new RelationRepository();
    }

    public static RelationRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public String getRelationNameById(long id, boolean isLanguageEnglish) {
        NameDTO relationDTO = this.getDTOByID(id);
        return relationDTO == null ? null : (isLanguageEnglish ? relationDTO.nameEn : relationDTO.nameBn);
    }
}


