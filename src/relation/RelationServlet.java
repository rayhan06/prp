package relation;

import common.BaseServlet;
import common.NameDao;
import common.NameInterface;
import common.NameRepository;
import language.LC;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;


/**
 * Servlet implementation class RelationServlet
 */
@SuppressWarnings({"rawtypes"})
@WebServlet("/RelationServlet")
@MultipartConfig
public class RelationServlet extends BaseServlet implements NameInterface {

	public String commonPartOfDispatchURL(){
		return  "common/name";
	}

	@Override
	public String getTableName() {
		return RelationDAO.getInstance().getTableName();
	}

	@Override
	public String getServletName() {
		return "RelationServlet";
	}

	@Override
	public NameDao getCommonDAOService() {
		return RelationDAO.getInstance();
	}

	@Override
	public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
		return addT(request,addFlag,userDTO,RelationDAO.getInstance());
	}

	@Override
	public int[] getAddPageMenuConstants() {
		return new int[] {MenuConstants.RELATION_ADD};
	}

	@Override
	public int[] getEditPageMenuConstants() {
		return new int[] {MenuConstants.RELATION_UPDATE};
	}

	@Override
	public int[] getSearchMenuConstants() {
		return new int[] {MenuConstants.RELATION_SEARCH};
	}

	@Override
	public Class<? extends HttpServlet> getClazz() {
		return RelationServlet.class;
	}

	@Override
	public int getSearchTitleValue() {
		return LC.RELATION_SEARCH_RELATION_SEARCH_FORMNAME;
	}

	@Override
	public String get_p_navigatorName() {
		return SessionConstants.NAV_RELATION;
	}

	@Override
	public String get_p_dtoCollectionName() {
		return SessionConstants.VIEW_RELATION;
	}

	@Override
	public NameRepository getNameRepository() {
		return RelationRepository.getInstance();
	}

	@Override
	public int getAddTitleValue() {
		return LC.RELATION_ADD_RELATION_ADD_FORMNAME;
	}

	@Override
	public int getEditTitleValue() {
		return LC.RELATION_EDIT_RELATION_EDIT_FORMNAME;
	}

	@Override
	public void init(HttpServletRequest request) throws ServletException {
		setCommonAttributes(request,getServletName(),getCommonDAOService());
	}
}