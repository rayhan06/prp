package pi_app_request_details;

import com.google.gson.Gson;
import common.BaseServlet;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import pi_package_item_map.PiPackageItemMapChildDAO;
import pi_package_item_map.PiPackageItemMapChildDTO;
import pi_package_item_map.Pi_package_item_mapDAO;
import pi_package_item_map.Pi_package_item_mapDTO;
import procurement_goods.Procurement_goodsDTO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.List;

@WebServlet("/Pi_app_request_detailsServlet")
@MultipartConfig
public class Pi_app_request_detailsServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_app_request_detailsServlet.class);

    @Override
    public String getTableName() {
        return Pi_app_request_detailsDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_app_request_detailsServlet";
    }

    @Override
    public Pi_app_request_detailsDAO getCommonDAOService() {
        return Pi_app_request_detailsDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PI_APP_REQUEST_DETAILS_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PI_APP_REQUEST_DETAILS_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PI_APP_REQUEST_DETAILS_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_app_request_detailsServlet.class;
    }

    private final Gson gson = new Gson();


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        try {
            request.setAttribute("failureMessage", "");
            logger.debug("%%%% addPi_app_request_details");
            String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
            Pi_app_request_detailsDTO pi_app_request_detailsDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

            if (addFlag) {
                pi_app_request_detailsDTO = new Pi_app_request_detailsDTO();
                pi_app_request_detailsDTO.insertionDate = pi_app_request_detailsDTO.lastModificationTime = System.currentTimeMillis();
                pi_app_request_detailsDTO.insertedByUserId = pi_app_request_detailsDTO.modifiedBy = userDTO.employee_record_id;
                pi_app_request_detailsDTO.insertedByOrganogramId = userDTO.organogramID;
            } else {
                pi_app_request_detailsDTO = Pi_app_request_detailsDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
                pi_app_request_detailsDTO.lastModificationTime = System.currentTimeMillis();
                pi_app_request_detailsDTO.modifiedBy = userDTO.employee_record_id;
            }

            String Value = "";
            Value = request.getParameter("serialNumber");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_app_request_detailsDTO.serialNum = Long.parseLong(Value);
            }
            logger.debug("serialNum = " + Value);


            Value = request.getParameter("packageItemsDescription");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_app_request_detailsDTO.packageItemsDescription = Value;
            }
            logger.debug("packageItemsDescription = " + Value);


            Value = request.getParameter("quantityItemsEn");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_app_request_detailsDTO.quantityItemsEn = Value;
            }
            logger.debug("quantityItemsEn = " + Value);


            Value = request.getParameter("quantityItemsBn");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_app_request_detailsDTO.quantityItemsBn = Value;
            }
            logger.debug("quantityItemsBn = " + Value);


            Value = request.getParameter("packageUnitPrice");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_app_request_detailsDTO.packageUnitPrice = Value;
            }
            logger.debug("packageUnitPrice = " + Value);


            Value = request.getParameter("estCost");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_app_request_detailsDTO.estCost = Long.parseLong(Value);
            }
            logger.debug("unitPrice = " + Value);


            Value = request.getParameter("estCostText");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_app_request_detailsDTO.estCostText = Value;
            }
            logger.debug("estCostText = " + Value);


            Value = request.getParameter("procurementMethodId");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_app_request_detailsDTO.procurementMethodId = Value;
            }
            logger.debug("procurementMethodId = " + Value);


            Value = request.getParameter("contractAppAuthId");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_app_request_detailsDTO.contractAppAuthId = Integer.parseInt(Value);
            }
            logger.debug("contractAppAuthId = " + Value);


            Value = request.getParameter("sourceFundId");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_app_request_detailsDTO.sourceFundId = Integer.parseInt(Value);
            }
            logger.debug("sourceFundId = " + Value);


            Value = request.getParameter("timeCodeForProcess");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_app_request_detailsDTO.timeCodeForProcess = Value;
            }
            logger.debug("timeCodeForProcess = " + Value);


            Value = request.getParameter("inviteAdvertiseTender");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_app_request_detailsDTO.inviteAdvertiseTender = Value;
            }
            logger.debug("inviteAdvertiseTender = " + Value);


            Value = request.getParameter("tenderOpening");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_app_request_detailsDTO.tenderOpening = Value;
            }
            logger.debug("tenderOpening = " + Value);


            Value = request.getParameter("tenderEvaluation");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_app_request_detailsDTO.tenderEvaluation = Value;
            }
            logger.debug("tenderEvaluation = " + Value);


            Value = request.getParameter("approvalToAward");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_app_request_detailsDTO.approvalToAward = Value;
            }
            logger.debug("approvalToAward = " + Value);


            Value = request.getParameter("notificationOfAward");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_app_request_detailsDTO.notificationOfAward = Value;
            }
            logger.debug("notificationOfAward = " + Value);


            Value = request.getParameter("signingOfContract");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_app_request_detailsDTO.signingOfContract = Value;
            }
            logger.debug("signingOfContract = " + Value);


            Value = request.getParameter("totalTimeToContractSignature");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_app_request_detailsDTO.totalTimeToContractSignature = Value;
            }
            logger.debug("totalTimeToContractSignature = " + Value);


            Value = request.getParameter("timeForCompletionOfContract");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_app_request_detailsDTO.timeForCompletionOfContract = Value;
            }
            logger.debug("timeForCompletionOfContract = " + Value);


            Value = request.getParameter("remark");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_app_request_detailsDTO.remark = Value;
            }
            logger.debug("remark = " + Value);


            long returnedID = -1;
            if (addFlag) {
                List<PiPackageItemMapChildDTO> childDTOS = (List<PiPackageItemMapChildDTO>) PiPackageItemMapChildDAO.getInstance()
                        .getDTOsByParent("pi_package_item_map_id", pi_app_request_detailsDTO.piPackageItemMapId);
                for (PiPackageItemMapChildDTO model : childDTOS) {
                    pi_app_request_detailsDTO.piPackageItemMapId = model.piPackageItemMapId;
                    pi_app_request_detailsDTO.fiscalYearId = model.fiscalYearId;
                    pi_app_request_detailsDTO.officeUnitId = model.officeUnitId;
                    pi_app_request_detailsDTO.packageFinalId = model.piPackageFinalId;
                    pi_app_request_detailsDTO.lotFinalId = model.piLotFinalId;
                    pi_app_request_detailsDTO.itemGroupId = model.itemGroupId;
                    pi_app_request_detailsDTO.itemTypeId = model.itemTypeId;
                    pi_app_request_detailsDTO.itemId = model.itemId;
                    pi_app_request_detailsDTO.quantity = model.itemQuantity;
                    pi_app_request_detailsDTO.itemUnitPrice = model.itemUnitPrice;
                    Pi_app_request_detailsDAO.getInstance().add(pi_app_request_detailsDTO);
                }
            } else {
                Pi_app_request_detailsDAO.getInstance().update(pi_app_request_detailsDTO);
            }
            return pi_app_request_detailsDTO;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean isValueValid(String value) {
        return value != null && !value.equals("");
    }
}

