package pi_app_request_details;

import util.CommonDTO;

public class Pi_app_request_detailsDTO extends CommonDTO {
    public long app_id = -1;
    public long piPackageItemMapId = -1;
    public long status = -1;
    public long serialNum = -1;
    public long fiscalYearId = -1;
    public long packageId = -1;
    public long lotId = -1;
    public long packageFinalId = -1;
    public long lotFinalId = -1;
    public long itemGroupId = -1;
    public long itemTypeId = -1;
    public long itemId = -1;
    public long officeUnitId = -1;
    public String packageItemsDescription = "";
    public long quantity = -1;
    public String quantityItemsEn = "";
    public String quantityItemsBn = "";
    public long itemUnitPrice = -1;
    public String packageUnitPrice = "";
    public long estCost = -1;
    public String estCostText = "";
    public String procurementMethodId = "";
    public long contractAppAuthId = -1;
    public long sourceFundId = -1;
    public String timeCodeForProcess = "";
    public String inviteAdvertiseTender = "";
    public String tenderOpening = "";
    public String tenderEvaluation = "";
    public String approvalToAward = "";
    public String notificationOfAward = "";
    public String signingOfContract = "";
    public String totalTimeToContractSignature = "";
    public String timeForCompletionOfContract = "";
    public String remark = "";
    public String itemName = "";
    public String packageName = "";
    public String condition = "";
    public String app_one_condition = "";
    public String app_two_condition = "";
    public String app_three_condition = "";
    public boolean isMaintenance = false;
    public boolean app_one_isMaintenance = false;
    public boolean app_two_isMaintenance = false;
    public boolean app_three_isMaintenance = false;
    public int isSelected = -1;

    /* isSelected

    -1  just created
    1: requester requested.. approver rejected
    2  requester requested.. approver approved
    3  approver created
    4  1st layer skip
     */
    public double requestedQuantity = 0;
    public double requestedUnitPrice = -1;
    public double requestedEstimatedCost = -1;
    public String requestedProcurementMethodId = "";
    public int requestedContractAppAuthId = -1;
    public int requestedSourceFundId = -1;
    public double approveOneQuantity = 0;
    public double approveOneUnitPrice = -1;
    public double approveOneEstimatedCost = -1;
    public String approveOneProcurementMethodId = "";
    public int approveOneContractAppAuthId = -1;
    public int approveOneSourceFundId = -1;
    public double approveTwoQuantity = 0;
    public double approveTwoUnitPrice = -1;
    public double approveTwoEstimatedCost = -1;
    public String approveTwoProcurementMethodId = "";
    public int approveTwoContractAppAuthId = -1;
    public int approveTwoSourceFundId = -1;
    public double approveThreeQuantity = 0;
    public double approveThreeUnitPrice = -1;
    public double approveThreeEstimatedCost = -1;
    public String approveThreeProcurementMethodId = "";
    public int approveThreeContractAppAuthId = -1;
    public int approveThreeSourceFundId = -1;
    public long insertedByUserId = -1;
    public long insertedByOrganogramId = -1;
    public long modifiedBy = -1;
    public long insertionDate = -1;


    @Override
    public String toString() {
        return "$Pi_app_request_detailsDTO[" +
                " iD = " + iD +
                " packageId = " + packageId +
                " itemTypeId = " + itemTypeId +
                " itemId = " + itemId +
                " condition = " + condition +
                " isMaintenance = " + isMaintenance +
                " isSelected = " + isSelected +
                " requestedQuantity = " + requestedQuantity +
                " requestedUnitPrice = " + requestedUnitPrice +
                " requestedEstimatedCost = " + requestedEstimatedCost +
                " requestedProcurementMethodId = " + requestedProcurementMethodId +
                " requestedContractAppAuthId = " + requestedContractAppAuthId +
                " requestedSourceFundId = " + requestedSourceFundId +
                " approveOneQuantity = " + approveOneQuantity +
                " approveOneUnitPrice = " + approveOneUnitPrice +
                " approveOneEstimatedCost = " + approveOneEstimatedCost +
                " approveOneProcurementMethodId = " + approveOneProcurementMethodId +
                " approveOneContractAppAuthId = " + approveOneContractAppAuthId +
                " approveOneSourceFundId = " + approveOneSourceFundId +
                " approveTwoQuantity = " + approveTwoQuantity +
                " approveTwoUnitPrice = " + approveTwoUnitPrice +
                " approveTwoEstimatedCost = " + approveTwoEstimatedCost +
                " approveTwoProcurementMethodId = " + approveTwoProcurementMethodId +
                " approveTwoContractAppAuthId = " + approveTwoContractAppAuthId +
                " approveTwoSourceFundId = " + approveTwoSourceFundId +
                " approveThreeQuantity = " + approveThreeQuantity +
                " approveThreeUnitPrice = " + approveThreeUnitPrice +
                " approveThreeEstimatedCost = " + approveThreeEstimatedCost +
                " approveThreeProcurementMethodId = " + approveThreeProcurementMethodId +
                " approveThreeContractAppAuthId = " + approveThreeContractAppAuthId +
                " approveThreeSourceFundId = " + approveThreeSourceFundId +
                " insertedByUserId = " + insertedByUserId +
                " insertedByOrganogramId = " + insertedByOrganogramId +
                " modifiedBy = " + modifiedBy +
                " insertionDate = " + insertionDate +
                " searchColumn = " + searchColumn +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }
}