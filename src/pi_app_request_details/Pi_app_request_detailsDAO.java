package pi_app_request_details;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class Pi_app_request_detailsDAO implements CommonDAOService<Pi_app_request_detailsDTO> {
    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private String[] columnNames = null;

    private static final String getByAppId =
            "SELECT * FROM pi_app_request_details WHERE app_id = %d AND isDeleted = 0";

    private static final String getByAppIdAndItemId =
            "SELECT * FROM pi_app_request_details WHERE app_id = %d AND item_id = %d AND isDeleted = 0";

    private static final String getByItemId =
            "SELECT * FROM pi_app_request_details WHERE item_id = %d AND isDeleted = 0";

    private Pi_app_request_detailsDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "app_id",
                        "fiscal_year_id",
                        "pi_package_item_map_id",
                        "status",
                        "serial_number",
                        "package_id",
                        "lot_id",
                        "package_final_id",
                        "lot_final_id",
                        "item_group_id",
                        "item_type_id",
                        "item_id",
                        "office_unit_id",
                        "package_items_description",
                        "quantity",
                        "quantity_items_en",
                        "quantity_items_bn",
                        "item_unit_price",
                        "package_unit_price",
                        "est_cost",
                        "est_cost_text",
                        "procurement_method_id",
                        "contract_app_auth_id",
                        "source_fund_id",
                        "time_code_for_process",
                        "invite_advertisement_tender",
                        "tender_opening",
                        "tender_evaluation",
                        "approval_to_award",
                        "notification_of_award",
                        "signing_of_contract",
                        "total_time_to_contract_signature",
                        "time_for_completion_of_contract",
                        "remark",

                        "approve_one_procurement_method_id",
                        "approve_one_contract_app_auth_id",
                        "approve_one_source_fund_id",

                        "approve_two_procurement_method_id",
                        "approve_two_contract_app_auth_id",
                        "approve_two_source_fund_id",

                        "approve_three_procurement_method_id",
                        "approve_three_contract_app_auth_id",
                        "approve_three_source_fund_id",

                        "inserted_by_user_id",
                        "inserted_by_organogram_id",
                        "modified_by",
                        "insertion_date",
                        "search_column",
                        "isDeleted",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("condition", " and (condition like ?)");
        searchMap.put("modified_by", " and (modified_by like ?)");
        searchMap.put("insertion_date_start", " and (insertion_date >= ?)");
        searchMap.put("insertion_date_end", " and (insertion_date <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Pi_app_request_detailsDAO INSTANCE = new Pi_app_request_detailsDAO();
    }

    public static Pi_app_request_detailsDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Pi_app_request_detailsDTO pi_app_request_detailsDTO) {
        pi_app_request_detailsDTO.searchColumn = "";
        pi_app_request_detailsDTO.searchColumn += pi_app_request_detailsDTO.condition + " ";
        pi_app_request_detailsDTO.searchColumn += pi_app_request_detailsDTO.modifiedBy + " ";
    }

    @Override
    public void set(PreparedStatement ps, Pi_app_request_detailsDTO pi_app_request_detailsDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(pi_app_request_detailsDTO);
        if (isInsert) {
            ps.setObject(++index, pi_app_request_detailsDTO.iD);
        }
        ps.setObject(++index, pi_app_request_detailsDTO.app_id);
        ps.setObject(++index, pi_app_request_detailsDTO.fiscalYearId);
        ps.setObject(++index, pi_app_request_detailsDTO.piPackageItemMapId);
        ps.setObject(++index, pi_app_request_detailsDTO.status);
        ps.setObject(++index, pi_app_request_detailsDTO.serialNum);
        ps.setObject(++index, pi_app_request_detailsDTO.packageId);
        ps.setObject(++index, pi_app_request_detailsDTO.lotId);
        ps.setObject(++index, pi_app_request_detailsDTO.packageFinalId);
        ps.setObject(++index, pi_app_request_detailsDTO.lotFinalId);
        ps.setObject(++index, pi_app_request_detailsDTO.itemGroupId);
        ps.setObject(++index, pi_app_request_detailsDTO.itemTypeId);
        ps.setObject(++index, pi_app_request_detailsDTO.itemId);
        ps.setObject(++index, pi_app_request_detailsDTO.officeUnitId);
        ps.setObject(++index, pi_app_request_detailsDTO.packageItemsDescription);
        ps.setObject(++index, pi_app_request_detailsDTO.quantity);
        ps.setObject(++index, pi_app_request_detailsDTO.quantityItemsEn);
        ps.setObject(++index, pi_app_request_detailsDTO.quantityItemsBn);
        ps.setObject(++index, pi_app_request_detailsDTO.itemUnitPrice);
        ps.setObject(++index, pi_app_request_detailsDTO.packageUnitPrice);
        ps.setObject(++index, pi_app_request_detailsDTO.estCost);
        ps.setObject(++index, pi_app_request_detailsDTO.estCostText);
        ps.setObject(++index, pi_app_request_detailsDTO.procurementMethodId);
        ps.setObject(++index, pi_app_request_detailsDTO.contractAppAuthId);
        ps.setObject(++index, pi_app_request_detailsDTO.sourceFundId);
        ps.setObject(++index, pi_app_request_detailsDTO.timeCodeForProcess);
        ps.setObject(++index, pi_app_request_detailsDTO.inviteAdvertiseTender);
        ps.setObject(++index, pi_app_request_detailsDTO.tenderOpening);
        ps.setObject(++index, pi_app_request_detailsDTO.tenderEvaluation);
        ps.setObject(++index, pi_app_request_detailsDTO.approvalToAward);
        ps.setObject(++index, pi_app_request_detailsDTO.notificationOfAward);
        ps.setObject(++index, pi_app_request_detailsDTO.signingOfContract);
        ps.setObject(++index, pi_app_request_detailsDTO.totalTimeToContractSignature);
        ps.setObject(++index, pi_app_request_detailsDTO.timeForCompletionOfContract);
        ps.setObject(++index, pi_app_request_detailsDTO.remark);

        ps.setObject(++index, pi_app_request_detailsDTO.approveOneProcurementMethodId);
        ps.setObject(++index, pi_app_request_detailsDTO.approveOneContractAppAuthId);
        ps.setObject(++index, pi_app_request_detailsDTO.approveOneSourceFundId);

        ps.setObject(++index, pi_app_request_detailsDTO.approveTwoProcurementMethodId);
        ps.setObject(++index, pi_app_request_detailsDTO.approveTwoContractAppAuthId);
        ps.setObject(++index, pi_app_request_detailsDTO.approveTwoSourceFundId);

        ps.setObject(++index, pi_app_request_detailsDTO.approveThreeProcurementMethodId);
        ps.setObject(++index, pi_app_request_detailsDTO.approveThreeContractAppAuthId);
        ps.setObject(++index, pi_app_request_detailsDTO.approveThreeSourceFundId);

        ps.setObject(++index, pi_app_request_detailsDTO.insertedByUserId);
        ps.setObject(++index, pi_app_request_detailsDTO.insertedByOrganogramId);
        ps.setObject(++index, pi_app_request_detailsDTO.modifiedBy);
        ps.setObject(++index, pi_app_request_detailsDTO.insertionDate);
        ps.setObject(++index, pi_app_request_detailsDTO.searchColumn);
        if (isInsert) {
            ps.setObject(++index, pi_app_request_detailsDTO.isDeleted);
        }
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, pi_app_request_detailsDTO.iD);
        }
    }

    @Override
    public Pi_app_request_detailsDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Pi_app_request_detailsDTO pi_app_request_detailsDTO = new Pi_app_request_detailsDTO();
            int i = 0;
            pi_app_request_detailsDTO.iD = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.app_id = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.fiscalYearId = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.piPackageItemMapId = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.status = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.serialNum = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.packageId = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.lotId = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.packageFinalId = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.lotFinalId = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.itemGroupId = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.itemTypeId = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.itemId = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.officeUnitId = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.packageItemsDescription = rs.getString(columnNames[i++]);
            pi_app_request_detailsDTO.quantity = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.quantityItemsEn = rs.getString(columnNames[i++]);
            pi_app_request_detailsDTO.quantityItemsBn = rs.getString(columnNames[i++]);
            pi_app_request_detailsDTO.itemUnitPrice = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.packageUnitPrice = rs.getString(columnNames[i++]);
            pi_app_request_detailsDTO.estCost = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.estCostText = rs.getString(columnNames[i++]);
            pi_app_request_detailsDTO.procurementMethodId = rs.getString(columnNames[i++]);
            pi_app_request_detailsDTO.contractAppAuthId = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.sourceFundId = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.timeCodeForProcess = rs.getString(columnNames[i++]);
            pi_app_request_detailsDTO.inviteAdvertiseTender = rs.getString(columnNames[i++]);
            pi_app_request_detailsDTO.tenderOpening = rs.getString(columnNames[i++]);
            pi_app_request_detailsDTO.tenderEvaluation = rs.getString(columnNames[i++]);
            pi_app_request_detailsDTO.approvalToAward = rs.getString(columnNames[i++]);
            pi_app_request_detailsDTO.notificationOfAward = rs.getString(columnNames[i++]);
            pi_app_request_detailsDTO.signingOfContract = rs.getString(columnNames[i++]);
            pi_app_request_detailsDTO.totalTimeToContractSignature = rs.getString(columnNames[i++]);
            pi_app_request_detailsDTO.timeForCompletionOfContract = rs.getString(columnNames[i++]);
            pi_app_request_detailsDTO.remark = rs.getString(columnNames[i++]);

            pi_app_request_detailsDTO.approveOneProcurementMethodId = rs.getString(columnNames[i++]);
            pi_app_request_detailsDTO.approveOneContractAppAuthId = rs.getInt(columnNames[i++]);
            pi_app_request_detailsDTO.approveOneSourceFundId = rs.getInt(columnNames[i++]);

            pi_app_request_detailsDTO.approveTwoProcurementMethodId = rs.getString(columnNames[i++]);
            pi_app_request_detailsDTO.approveTwoContractAppAuthId = rs.getInt(columnNames[i++]);
            pi_app_request_detailsDTO.approveTwoSourceFundId = rs.getInt(columnNames[i++]);

            pi_app_request_detailsDTO.approveThreeProcurementMethodId = rs.getString(columnNames[i++]);
            pi_app_request_detailsDTO.approveThreeContractAppAuthId = rs.getInt(columnNames[i++]);
            pi_app_request_detailsDTO.approveThreeSourceFundId = rs.getInt(columnNames[i++]);

            pi_app_request_detailsDTO.insertedByUserId = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.modifiedBy = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.insertionDate = rs.getLong(columnNames[i++]);
            pi_app_request_detailsDTO.searchColumn = rs.getString(columnNames[i++]);
            pi_app_request_detailsDTO.isDeleted = rs.getInt(columnNames[i++]);
            pi_app_request_detailsDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            return pi_app_request_detailsDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Pi_app_request_detailsDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "pi_app_request_details";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_app_request_detailsDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_app_request_detailsDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

    public List<Pi_app_request_detailsDTO> getByAppId(Long appId) {
        String sql = String.format(getByAppId, appId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public List<Pi_app_request_detailsDTO> getDataByColumn(List<Long> itemIds, String columnName) {
        StringBuilder sql = new StringBuilder("SELECT * FROM ")
                .append(getTableName())
                .append(" WHERE isDeleted = 0 ")
                .append("and ")
                .append(columnName)
                .append(" IN ( ");
        for (int i = 0; i < itemIds.size(); i++) {
            if (i != 0) {
                sql.append(",");
            }
            sql.append(itemIds.get(i));
        }
        sql.append(" ) ");
        return ConnectionAndStatementUtil.getListOfT(sql.toString(), this::buildObjectFromResultSet);
    }

    public List<Pi_app_request_detailsDTO> getApprovedItemByAppIdAndItemId(long appId, long itemId) {
        String sql = String.format(getByAppIdAndItemId, appId, itemId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet)
                .stream().filter(i -> allowedValue().contains(i.isSelected)).collect(Collectors.toList());
    }

    public List<Pi_app_request_detailsDTO> getByItemId(long itemId) {
        String sql = String.format(getByItemId, itemId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet)
                .stream().filter(i -> allowedValue().contains(i.isSelected)).collect(Collectors.toList());
    }

    public List<Integer> allowedValue() {
        return Arrays.asList(2, 3, 4);
    }

    public List<Pi_app_request_detailsDTO> getByAppIds(List<Long> appIds) {
        if (appIds.isEmpty()) return new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM ")
                .append(getTableName())
                .append(" WHERE isDeleted = 0 and app_id IN (  ");
        for (int i = 0; i < appIds.size(); i++) {
            if (i != 0) {
                sql.append(",");
            }
            sql.append(appIds.get(i));
        }
        sql.append(" ) ");
        return ConnectionAndStatementUtil.getListOfT(sql.toString(), this::buildObjectFromResultSet);

    }

    public String getProcurementMethodData(String idsInString, boolean isLanguageEnglish) {
        StringBuilder text = new StringBuilder();
        List<Long> ids = Arrays.stream(idsInString.split(","))
                .map(String::trim)
                .map(Long::parseLong)
                .collect(Collectors.toList());
        CategoryLanguageModel model;
        for (int i = 0; i < ids.size(); i++) {
            if (i != 0) {
                text.append(", ");
            }

            model = CatRepository.getInstance().getCategoryLanguageModel
                    ("procurement_method", ids.get(i).intValue());
            if (model != null) {
                text.append(isLanguageEnglish ? model.englishText : model.banglaText);
            }
        }

        return text.toString();
    }
}
	