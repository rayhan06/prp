package drug_information;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import common.ConnectionAndStatementUtil;
import util.CommonDTO;
import util.NavigationService4;

public class Drug_configurationDAO extends NavigationService4
{

	public Drug_configurationDAO(String tableName) {
		super(tableName);
		columnNames = new String[] 
		{
			"ID",
			"drug_information_id",
			"organogram_id",			
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Drug_configurationDAO() {
		this("drug_configuration");
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Drug_configurationDTO drug_configurationDTO = (Drug_configurationDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	

		if(isInsert)
		{
			ps.setObject(index++,drug_configurationDTO.iD);
		}
		ps.setObject(index++,drug_configurationDTO.drugInformationId);
		ps.setObject(index++,drug_configurationDTO.organogramId);
	
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
		
	}
	
	public Drug_configurationDTO build(ResultSet rs)
	{
		try
		{
			Drug_configurationDTO drug_configurationDTO = new Drug_configurationDTO();
			drug_configurationDTO.iD = rs.getLong("ID");
			drug_configurationDTO.drugInformationId = rs.getLong("drug_information_id");
			drug_configurationDTO.organogramId = rs.getLong("organogram_id");
			drug_configurationDTO.isDeleted = rs.getInt("isDeleted");
			drug_configurationDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return drug_configurationDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	public List<Drug_configurationDTO> getDTOsByDrugInfoId(long diId)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE isDeleted = 0 and drug_information_id = " + diId;
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	
}
