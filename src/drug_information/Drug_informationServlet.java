package drug_information;

import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import medical_stock_approval_history.Medical_stock_approval_historyDAO;
import medical_stock_approval_history.Medical_stock_approval_historyDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;


import javax.servlet.http.*;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Drug_informationServlet
 */
@WebServlet("/Drug_informationServlet")
@MultipartConfig
public class Drug_informationServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Drug_informationServlet.class);

    String tableName = "drug_information";

	Drug_informationDAO drug_informationDAO;
	Drug_configurationDAO drug_configurationDAO = new Drug_configurationDAO();
	Medical_stock_approval_historyDAO medical_stock_approval_historyDAO = Medical_stock_approval_historyDAO.getInstance();
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Drug_informationServlet() 
	{
        super();
    	try
    	{
			drug_informationDAO = new Drug_informationDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(drug_informationDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DRUG_INFORMATION_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DRUG_INFORMATION_UPDATE))
				{
					getDrug_information(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}
			else if(actionType.equals("getSuggested"))
			{
				String suggestion = request.getParameter("suggestion");
				long orgToFilter = Long.parseLong(request.getParameter("orgToFilter"));
				if(suggestion != null && suggestion.length() >= 2)
				{
					System.out.println("getSuggested called with : " + suggestion);
					List<Drug_informationDTO> drug_informationDTOs = drug_informationDAO.getDrugs(suggestion);
					PrintWriter out = response.getWriter();
					response.setContentType("application/json");
					response.setCharacterEncoding("UTF-8");
					
					String encoded = this.gson.toJson(drug_informationDTOs);
					System.out.println("json encoded data = " + encoded);
					out.print(encoded);
					out.flush();	
				}
				
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DRUG_INFORMATION_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							if(filter.equalsIgnoreCase("getCriticalMedicines"))
							{
								filter = "current_stock <= minimul_level_for_alert";
							}
							else if(filter.equalsIgnoreCase("getStockOutMedicines"))
							{
								filter = "current_stock <= 0";
							}
							else
							{
								filter = ""; //shouldn't be directly used, rather manipulate it.
							}

							searchDrug_information(request, response, isPermanentTable, filter);
						}
						else
						{
							searchDrug_information(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchDrug_information(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DRUG_INFORMATION_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DRUG_INFORMATION_ADD))
				{
					System.out.println("going to  addDrug_information ");
					addDrug_information(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addDrug_information ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DRUG_INFORMATION_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addDrug_information ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DRUG_INFORMATION_UPDATE))
				{					
					addDrug_information(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("editAvailability"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DRUG_INFORMATION_UPDATE)
						&& (userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE))
				{					
					editAvailability(request, response, userDTO);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("massEditAvailability"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DRUG_INFORMATION_UPDATE)
						&& (userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE))
				{					
					massEditAvailability(request, response, userDTO);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DRUG_INFORMATION_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DRUG_INFORMATION_SEARCH))
				{
					searchDrug_information(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void massEditAvailability(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception {
		long lotId = Long.parseLong(request.getParameter("lotId"));
		String[] ids = request.getParameterValues("id");
		if(ids !=null)
		{
			int i = 0;
			for(String sId: ids)
			{
				long id = Long.parseLong(sId);
				int availableStock = Integer.parseInt(request.getParameterValues("availableStock")[i]);
				boolean success = updateAvailability(id, availableStock, userDTO);
				if(!success)
				{
					request.setAttribute("failureMessage", "Failed to update some stocks. Please try again");
					request.getRequestDispatcher("medical_inventory_lot/medical_inventory_lotView.jsp?ID=" + lotId).forward(request, response);
					return;
				}
				i++;
			}
		}
		
		request.setAttribute("successMessage", "Seccess");
		request.getRequestDispatcher("medical_inventory_lot/medical_inventory_lotView.jsp?ID=" + lotId).forward(request, response);
		
	}
	
	public boolean updateAvailability(long id, int availableStock, UserDTO userDTO) throws Exception
	{
		Drug_informationDTO drug_informationDTO = drug_informationDAO.getDTOByID(id);
		if(drug_informationDTO == null)
		{			
			return false;
		}
		if(availableStock < 0 || availableStock > drug_informationDTO.currentStock)
		{
			return false;
		}
		drug_informationDTO.availableStock = availableStock;
		
		Medical_stock_approval_historyDTO medical_stock_approval_historyDTO = new Medical_stock_approval_historyDTO(drug_informationDTO, userDTO);
		medical_stock_approval_historyDAO.add(medical_stock_approval_historyDTO);
		
		if(drug_informationDTO.iD == Drug_informationDTO.ACCU_CHECK_ID)
		{
			drug_informationDTO.detailedCount = availableStock * Drug_informationDTO.ACCU_SIZE;
		}
		drug_informationDAO.update(drug_informationDTO);
		return true;
	}

	private void editAvailability(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception {
		long id = Long.parseLong(request.getParameter("id"));
		int availableStock = Integer.parseInt(request.getParameter("availableStock"));
		boolean success = updateAvailability(id, availableStock, userDTO);
		if(success)
		{
			response.sendRedirect("Drug_informationServlet?actionType=search");
		}
		else
		{
			request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
		}		
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Drug_informationDTO drug_informationDTO = drug_informationDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(drug_informationDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addDrug_information(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addDrug_information");
			String path = getServletContext().getRealPath("/img2/");
			Drug_informationDTO drug_informationDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				drug_informationDTO = new Drug_informationDTO();
			}
			else
			{
				drug_informationDTO = drug_informationDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("nameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEn = " + Value);
			if(Value != null)
			{
				drug_informationDTO.nameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameBn = " + Value);
			if(Value != null)
			{
				drug_informationDTO.nameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("medicineGenericNameType");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("medicineGenericNameType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				drug_informationDTO.medicineGenericNameType = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("pharmaCompanyNameType");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("pharmaCompanyNameType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				drug_informationDTO.pharmaCompanyNameType = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("drugFormCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("drugFormCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				drug_informationDTO.drugFormCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("strength");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("strength = " + Value);
			if(Value != null)
			{
				drug_informationDTO.strength = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("unitPrice");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("unitPrice = " + Value);
			if(Value != null)
			{
				drug_informationDTO.unitPrice = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("currentStock");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("currentStock = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				drug_informationDTO.currentStock = Integer.parseInt(Value);
				drug_informationDTO.availableStock = drug_informationDTO.currentStock;
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("minimulLevelForAlert");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("minimulLevelForAlert = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				drug_informationDTO.minimulLevelForAlert = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addDrug_information dto = " + drug_informationDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				drug_informationDAO.setIsDeleted(drug_informationDTO.iD, CommonDTO.OUTDATED);
				returnedID = drug_informationDAO.add(drug_informationDTO);
				drug_informationDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = drug_informationDAO.add(drug_informationDTO);
			}
			else
			{				
				returnedID = drug_informationDAO.update(drug_informationDTO);											
			}
			
			drug_configurationDAO.hardDeleteChildrenByParent(returnedID, "drug_information_id");
			String config = "";
			String[] orgs = request.getParameterValues("drugConfiguration.organogramId");
			ArrayList<Long> usedOrgs = new ArrayList<Long>();
			usedOrgs.add(-1L);
			if(orgs != null)
			{
				for(String org: orgs) 
				{
					long organogramId = Long.parseLong(org);
					if(!usedOrgs.contains(organogramId))
					{
						Drug_configurationDTO drug_configurationDTO = new Drug_configurationDTO();
						drug_configurationDTO.drugInformationId = returnedID;
						drug_configurationDTO.organogramId = organogramId;
						drug_configurationDAO.add(drug_configurationDTO);
						usedOrgs.add(drug_configurationDTO.organogramId);
						config += drug_configurationDTO.organogramId + ", ";
					}
				}
			}
			
			drug_informationDTO.config = config;
			drug_informationDAO.update(drug_informationDTO);

			
			
			response.sendRedirect("Drug_informationServlet?actionType=search");
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void getDrug_information(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getDrug_information");
		Drug_informationDTO drug_informationDTO = null;
		try 
		{
			drug_informationDTO = drug_informationDAO.getDTOByID(id);
			request.setAttribute("ID", drug_informationDTO.iD);
			request.setAttribute("drug_informationDTO",drug_informationDTO);
			request.setAttribute("drug_informationDAO",drug_informationDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "drug_information/drug_informationInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "drug_information/drug_informationSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "drug_information/drug_informationEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "drug_information/drug_informationEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getDrug_information(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getDrug_information(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchDrug_information(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchDrug_information 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_DRUG_INFORMATION,
			request,
			drug_informationDAO,
			SessionConstants.VIEW_DRUG_INFORMATION,
			SessionConstants.SEARCH_DRUG_INFORMATION,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("drug_informationDAO",drug_informationDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to drug_information/drug_informationApproval.jsp");
	        	rd = request.getRequestDispatcher("drug_information/drug_informationApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to drug_information/drug_informationApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("drug_information/drug_informationApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to drug_information/drug_informationSearch.jsp");
	        	rd = request.getRequestDispatcher("drug_information/drug_informationSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to drug_information/drug_informationSearchForm.jsp");
	        	rd = request.getRequestDispatcher("drug_information/drug_informationSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

