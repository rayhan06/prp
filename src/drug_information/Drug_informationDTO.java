package drug_information;



import java.util.List;

import util.*; 


public class Drug_informationDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
	public long medicineGenericNameType = 0;
	public long pharmaCompanyNameType = 0;
	public int drugFormCat = 0;
    public String strength = "";
	public int currentStock = 0;
	public int detailedCount = 0;
	public int minimulLevelForAlert = 0;
	public double unitPrice = 0;
	public String genericNameEn = "";
	public String formName = "";
	public String pharmaName = "";
    public static final int IN_STOCK = 1;
    public static final int BELOW_MINIMUM_LEVEL = 2;
    public static final int OUT_OF_STOCK = 3;
    
    public String drugText = "";
    public String drugTextWithoutGName = "";
    public String drugTextWithoutGNameBolded = "";
    public String stockText = "";
    public int drugTextLen = 0;
    public String config = "";
    
    public int availableStock = 0;
    public long lastApprovalHistoryId = -1;
    
    boolean showTick = true;
    
    public static final long ACCU_CHECK_ID = 34090;
    public static final int ACCU_SIZE = 50;
	
    public static final double DEFAULT_STOCK_ALERT_PERCENTAGE = 0.2;
    
    public List<Long> orgs = null;
	
    @Override
	public String toString() {
            return "$Drug_informationDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " medicineGenericNameType = " + medicineGenericNameType +
            " pharmaCompanyNameType = " + pharmaCompanyNameType +
            " drugFormCat = " + drugFormCat +
            " strength = " + strength +
            " currentStock = " + currentStock +
            " minimulLevelForAlert = " + minimulLevelForAlert +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}