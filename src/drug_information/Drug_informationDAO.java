package drug_information;


import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.*;


import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Drug_informationDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Drug_informationDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Drug_informationMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"medicine_generic_name_type",
			"pharma_company_name_type",
			"drug_form_cat",
			"strength",
			"current_stock",
			"detailed_count",
			"unit_price",
			"minimul_level_for_alert",
			"config",
			"search_column",
			"available_stock",
			"last_approval_history_id",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Drug_informationDAO()
	{
		this("drug_information");		
	}
	
	public void setSearchColumn(Drug_informationDTO drug_informationDTO)
	{
		drug_informationDTO.searchColumn = "";
		drug_informationDTO.searchColumn += drug_informationDTO.nameEn + " ";
		drug_informationDTO.searchColumn += drug_informationDTO.nameBn + " ";
		drug_informationDTO.searchColumn += CommonDAO.getName("English", "medicine_generic_name", drug_informationDTO.medicineGenericNameType) + " " + CommonDAO.getName("Bangla", "medicine_generic_name", drug_informationDTO.medicineGenericNameType) + " ";
		drug_informationDTO.searchColumn += CommonDAO.getName("English", "pharma_company_name", drug_informationDTO.pharmaCompanyNameType) + " " + CommonDAO.getName("Bangla", "pharma_company_name", drug_informationDTO.pharmaCompanyNameType) + " ";
		drug_informationDTO.searchColumn += CatDAO.getName("English", "drug_form", drug_informationDTO.drugFormCat) + " " + CatDAO.getName("Bangla", "drug_form", drug_informationDTO.drugFormCat) + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Drug_informationDTO drug_informationDTO = (Drug_informationDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(drug_informationDTO);
		if(isInsert)
		{
			ps.setObject(index++,drug_informationDTO.iD);
		}
		ps.setObject(index++,drug_informationDTO.nameEn);
		ps.setObject(index++,drug_informationDTO.nameBn);
		ps.setObject(index++,drug_informationDTO.medicineGenericNameType);
		ps.setObject(index++,drug_informationDTO.pharmaCompanyNameType);
		ps.setObject(index++,drug_informationDTO.drugFormCat);
		ps.setObject(index++,drug_informationDTO.strength);
		ps.setObject(index++,drug_informationDTO.currentStock);
		ps.setObject(index++,drug_informationDTO.detailedCount);
		ps.setObject(index++,drug_informationDTO.unitPrice);
		ps.setObject(index++,drug_informationDTO.minimulLevelForAlert);
		ps.setObject(index++,drug_informationDTO.config);
		ps.setObject(index++,drug_informationDTO.searchColumn);
		ps.setObject(index++,drug_informationDTO.availableStock);
		ps.setObject(index++,drug_informationDTO.lastApprovalHistoryId);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Drug_informationDTO build(ResultSet rs)
	{
		try
		{
			Drug_informationDTO drug_informationDTO = new Drug_informationDTO();
			drug_informationDTO.iD = rs.getLong("ID");
			drug_informationDTO.nameEn = rs.getString("name_en");
			drug_informationDTO.nameBn = rs.getString("name_bn");
			drug_informationDTO.medicineGenericNameType = rs.getLong("medicine_generic_name_type");
			drug_informationDTO.pharmaCompanyNameType = rs.getLong("pharma_company_name_type");
			drug_informationDTO.drugFormCat = rs.getInt("drug_form_cat");
			drug_informationDTO.strength = rs.getString("strength");
			drug_informationDTO.currentStock = rs.getInt("current_stock");
			drug_informationDTO.detailedCount = rs.getInt("detailed_count");
			drug_informationDTO.unitPrice = rs.getDouble("unit_price");
			drug_informationDTO.minimulLevelForAlert = rs.getInt("minimul_level_for_alert");
			drug_informationDTO.searchColumn = rs.getString("search_column");
			drug_informationDTO.config = rs.getString("config");
			if(drug_informationDTO.config == null)
			{
				drug_informationDTO.config = "";
			}
			if(!drug_informationDTO.config.equalsIgnoreCase(""))
			{
				drug_informationDTO.orgs = new ArrayList<Long> ();
				String [] sOrgs = drug_informationDTO.config.split(", ");
				for(String sOrg: sOrgs)
				{
					drug_informationDTO.orgs.add(Long.parseLong(sOrg));
				}
			}
			drug_informationDTO.availableStock = rs.getInt("available_stock");
			drug_informationDTO.lastApprovalHistoryId = rs.getLong("last_approval_history_id");
			drug_informationDTO.isDeleted = rs.getInt("isDeleted");
			drug_informationDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return drug_informationDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

	public Drug_informationDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Drug_informationDTO drug_informationDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return drug_informationDTO;
	}

	
	public List<Drug_informationDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public Drug_informationDTO getPopular(ResultSet rs)
	{
		try
		{
			Drug_informationDTO drug_informationDTO = new Drug_informationDTO();
			drug_informationDTO = new Drug_informationDTO();
			drug_informationDTO.iD = rs.getLong("drug_information_id");
			drug_informationDTO.nameEn = rs.getString("drug_information.name_en");
			drug_informationDTO.nameBn = rs.getString("drug_information.name_en");
			drug_informationDTO.medicineGenericNameType = rs.getLong("drug_information.medicine_generic_name_type");
			
			drug_informationDTO.currentStock = rs.getInt("SUM(stock_out_quantity)");
			return drug_informationDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	


	public List<Drug_informationDTO> getMostPopularDrugs(long startDate, long endDate)
	{
		String sql = "SELECT \r\n" + 
				"    drug_information_id,\r\n" + 
				"    SUM(stock_out_quantity),\r\n" + 
				"    drug_information.name_en,\r\n" + 
				"    drug_information.name_bn,\r\n" +
				"    drug_information.medicine_generic_name_type\r\n" +
				"FROM\r\n" + 
				"    medical_inventory_out\r\n" + 
				"        JOIN\r\n" + 
				"    drug_information ON drug_information.id = drug_information_id\r\n" + 
				"WHERE\r\n" + 
				"    medical_item_cat = 0\r\n" + 
				"        AND medical_inventory_out.insertion_date >= " + startDate + "\r\n" + 
				"        AND medical_inventory_out.insertion_date < " + endDate + "\r\n" + 
				"GROUP BY drug_information_id\r\n" + 
				"ORDER BY SUM(stock_out_quantity) DESC\r\n" + 
				"LIMIT 10;";
		return ConnectionAndStatementUtil.getListOfT(sql,this::getPopular);	
	}
	
	public List<Drug_informationDTO> getDrugs(String suggestion)
	{
		return getDrugs(suggestion, -1);
	}
	
	public boolean canReceiveThisDrug(Drug_informationDTO drug_informationDTO, long orgToFilter)
	{
		if(drug_informationDTO == null)
		{
			return false;
		}
        return drug_informationDTO.orgs == null
                || (drug_informationDTO.orgs != null && drug_informationDTO.orgs.contains(orgToFilter));
    }
	
	public List<Drug_informationDTO> getDrugs(String suggestion, long orgToFilter)
	{
		suggestion = suggestion.toLowerCase();
		String sql = "SELECT   \r\n" + 
				"				    drug_information.ID,  \r\n" + 
				"				    medicine_generic_name.id,  \r\n" + 
				"				    drug_information.name_en,  \r\n" + 
				"				    medicine_generic_name.name_en,  \r\n" + 
				"				    drug_information.current_stock,  \r\n" + 
				"				    drug_information.available_stock,  \r\n" + 
				"				    minimul_level_for_alert, \r\n" + 
				"				    category.name_en,  \r\n" + 
				"				    strength, \r\n" + 
				"				    config, \r\n" + 
				"				    category.value,\r\n" + 
				"                   pharma_company_name.name_en\r\n" + 
				"				FROM  \r\n" + 
				"				    drug_information  \r\n" + 
				"				        JOIN  \r\n" + 
				"				    medicine_generic_name ON (drug_information.medicine_generic_name_type = medicine_generic_name.id)  \r\n" + 
				"				        JOIN  \r\n" + 
				"				    category ON (category.domain_name = 'drug_form'  \r\n" + 
				"				        AND category.value = drug_information.drug_form_cat)\r\n" + 
				"                     join pharma_company_name on drug_information.pharma_company_name_type = pharma_company_name.ID\r\n" + 
				"				WHERE   \r\n" + 
				"								    drug_information.isDeleted = 0   \r\n" + 
				"								        AND medicine_generic_name.isDeleted = 0   \r\n" + 
				"				        				AND (medicine_generic_name.name_en  LIKE '" + suggestion + "%' or drug_information.name_en like '" + suggestion + "%')";
		
		List<Drug_informationDTO> suggestedDrugs =  ConnectionAndStatementUtil.getListOfT(sql,this::getSuggested);
		
		List<Drug_informationDTO> availableDrugs = new ArrayList<Drug_informationDTO> ();
		List<Drug_informationDTO> unavailableDrugs = new ArrayList<Drug_informationDTO> ();
		List<Drug_informationDTO> totalDrugs = new ArrayList<Drug_informationDTO> ();
		List<Drug_informationDTO> availablePharmaDrugs = new ArrayList<Drug_informationDTO> ();
		List<Drug_informationDTO> availableGenericDrugs = new ArrayList<Drug_informationDTO> ();
		List<Drug_informationDTO> unavailablePharmaDrugs = new ArrayList<Drug_informationDTO> ();
		List<Drug_informationDTO> unavailableGenericDrugs = new ArrayList<Drug_informationDTO> ();
		
		for(Drug_informationDTO drug_informationDTO: suggestedDrugs)
		{
			if(drug_informationDTO.availableStock > 0 && canReceiveThisDrug(drug_informationDTO, orgToFilter))
			{
				drug_informationDTO.showTick = true;
				availableDrugs.add(drug_informationDTO);
			}
			else
			{
				drug_informationDTO.showTick = false;
				unavailableDrugs.add(drug_informationDTO);
			}
		}
		
		for(Drug_informationDTO drug_informationDTO: availableDrugs)
		{
			if(drug_informationDTO.nameEn.toLowerCase().startsWith(suggestion))
			{
				availablePharmaDrugs.add(drug_informationDTO);
			}
			else
			{
				availableGenericDrugs.add(drug_informationDTO);
			}
		}
		
		for(Drug_informationDTO drug_informationDTO: unavailableDrugs)
		{
			if(drug_informationDTO.nameEn.toLowerCase().startsWith(suggestion))
			{
				unavailablePharmaDrugs.add(drug_informationDTO);
			}
			else
			{
				unavailableGenericDrugs.add(drug_informationDTO);
			}
		}
		
		totalDrugs.addAll(availablePharmaDrugs);
		totalDrugs.addAll(availableGenericDrugs);
		totalDrugs.addAll(unavailablePharmaDrugs);
		totalDrugs.addAll(unavailableGenericDrugs);
		return totalDrugs;
		
	}
	
	public Drug_informationDTO getDrugInfoDetailsById(long id)
	{
		String sql = "SELECT   \r\n" + 
				"				    drug_information.ID,  \r\n" + 
				"				    medicine_generic_name.id,  \r\n" + 
				"				    drug_information.name_en,  \r\n" + 
				"				    medicine_generic_name.name_en,  \r\n" + 
				"				    drug_information.current_stock,  \r\n" + 
				"				    drug_information.available_stock,  \r\n" + 
				"				    minimul_level_for_alert, \r\n" + 
				"				    category.name_en,  \r\n" + 
				"				    strength, \r\n" + 
				"				    config, \r\n" + 
				"				    category.value,\r\n" + 
				"                   pharma_company_name.name_en\r\n" + 
				"				FROM  \r\n" + 
				"				    drug_information  \r\n" + 
				"				        JOIN  \r\n" + 
				"				    medicine_generic_name ON (drug_information.medicine_generic_name_type = medicine_generic_name.id)  \r\n" + 
				"				        JOIN  \r\n" + 
				"				    category ON (category.domain_name = 'drug_form'  \r\n" + 
				"				        AND category.value = drug_information.drug_form_cat)\r\n" + 
				"                     join pharma_company_name on drug_information.pharma_company_name_type = pharma_company_name.ID\r\n" + 
				"				WHERE   \r\n" + 
				"								    drug_information.isDeleted = 0   \r\n" + 
				"								        AND medicine_generic_name.isDeleted = 0   \r\n" + 
				"				        				AND drug_information.id = " + id  ;
		Drug_informationDTO drug_informationDTO = ConnectionAndStatementUtil.getT(sql,this::getSuggested);
		if(drug_informationDTO != null)
		{
			drug_informationDTO.drugTextWithoutGNameBolded = "<b>" + drug_informationDTO.nameEn + "</b>"
					+ " | " + drug_informationDTO.strength + " | " + drug_informationDTO.formName 
					+ " | " + drug_informationDTO.pharmaName;
			drug_informationDTO.drugTextWithoutGName = drug_informationDTO.nameEn 
					+ " | " + drug_informationDTO.strength + " | " + drug_informationDTO.formName 
					+ " | " + drug_informationDTO.pharmaName;
			drug_informationDTO.drugText = drug_informationDTO.drugTextWithoutGName + " | "+ drug_informationDTO.genericNameEn;
			if(drug_informationDTO.availableStock > 0)
			{
				drug_informationDTO.stockText = "✔ ";
			}
			else
			{
				drug_informationDTO.stockText = "✖ ";
			}
			drug_informationDTO.drugTextLen = ((drug_informationDTO.nameEn + " | " 
			+ drug_informationDTO.strength + " | " + drug_informationDTO.formName + " | " ).length() + 1) * 8;			
		}
		return drug_informationDTO;
	}
	

	public Drug_informationDTO getSuggested(ResultSet rs)
	{
		try
		{
			Drug_informationDTO drug_informationDTO = new Drug_informationDTO();
			drug_informationDTO = new Drug_informationDTO();
			drug_informationDTO.iD = rs.getLong("drug_information.ID");
			drug_informationDTO.nameEn = drug_informationDTO.nameBn = rs.getString("drug_information.name_en");
			drug_informationDTO.medicineGenericNameType = rs.getLong("medicine_generic_name.ID");
			drug_informationDTO.genericNameEn = rs.getString("medicine_generic_name.name_en");
			drug_informationDTO.currentStock = rs.getInt("drug_information.current_stock");
			drug_informationDTO.availableStock = rs.getInt("drug_information.available_stock");
			drug_informationDTO.minimulLevelForAlert = rs.getInt("minimul_level_for_alert");
			drug_informationDTO.drugFormCat = rs.getInt("category.value");
			drug_informationDTO.formName = rs.getString("category.name_en");
			drug_informationDTO.strength = rs.getString("strength");
			drug_informationDTO.pharmaName = rs.getString("pharma_company_name.name_en");
			drug_informationDTO.config = rs.getString("config");
			if(drug_informationDTO.config == null)
			{
				drug_informationDTO.config = "";
			}
			if(!drug_informationDTO.config.equalsIgnoreCase(""))
			{
				drug_informationDTO.orgs = new ArrayList<Long> ();
				String [] sOrgs = drug_informationDTO.config.split(", ");
				for(String sOrg: sOrgs)
				{
					drug_informationDTO.orgs.add(Long.parseLong(sOrg));
				}
			}
			
			return drug_informationDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public Drug_informationDTO getProfitable(ResultSet rs)
	{
		try
		{
			Drug_informationDTO drug_informationDTO = new Drug_informationDTO();
			drug_informationDTO.iD = rs.getLong("drug_information_id");
			drug_informationDTO.nameEn = rs.getString("drug_information.name_en");
			drug_informationDTO.nameBn = rs.getString("drug_information.name_en");
			drug_informationDTO.medicineGenericNameType = rs.getLong("drug_information.medicine_generic_name_type");
			
			drug_informationDTO.unitPrice = rs.getDouble("SUM(stock_out_quantity * medical_inventory_out.unit_price)");

			return drug_informationDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public List<Drug_informationDTO> getMostProfitableDrugs(long startDate, long endDate)
	{
		String sql = "SELECT \r\n" + 
				"    drug_information_id,\r\n" + 
				"    SUM(stock_out_quantity * medical_inventory_out.unit_price),\r\n" + 
				"    drug_information.name_en,\r\n" + 
				"    drug_information.name_bn,\r\n" +
				"    drug_information.medicine_generic_name_type\r\n" +
				"FROM\r\n" + 
				"    medical_inventory_out\r\n" + 
				"        JOIN\r\n" + 
				"    drug_information ON drug_information.id = drug_information_id\r\n" + 
				"WHERE\r\n" + 
				"    medical_item_cat = 0\r\n" + 
				"        AND medical_inventory_out.insertion_date >= " + startDate + "\r\n" + 
				"        AND medical_inventory_out.insertion_date < " + endDate + "\r\n" + 
				"GROUP BY drug_information_id\r\n" + 
				"ORDER BY SUM(stock_out_quantity * medical_inventory_out.unit_price) DESC\r\n" + 
				"LIMIT 10;";
		return ConnectionAndStatementUtil.getListOfT(sql,this::getProfitable);			
	}
	
	public List<Drug_informationDTO> getAllDrug_information (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }
	

	
	public List<Drug_informationDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}
	
	public List<Drug_informationDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%' or ";
				AnyfieldSql+= tableName + ".name_en like '%" + p_searchCriteria.get("AnyField").toString() + "%'";	
			}
			
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name_en")
						|| str.equals("name_bn")
						|| str.equals("medicine_generic_name_type")
						|| str.equals("pharma_company_name_type")
						|| str.equals("drug_form_cat")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".name_en like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("name_bn"))
					{
						AllFieldSql += "" + tableName + ".name_bn like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("medicine_generic_name_type"))
					{
						AllFieldSql += "" + tableName + ".medicine_generic_name_type = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("pharma_company_name_type"))
					{
						AllFieldSql += "" + tableName + ".pharma_company_name_type = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("drug_form_cat"))
					{
						AllFieldSql += "" + tableName + ".drug_form_cat = " + p_searchCriteria.get(str);
						i ++;
					}

					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	