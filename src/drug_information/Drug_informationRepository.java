package drug_information;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Drug_informationRepository implements Repository {
	Drug_informationDAO drug_informationDAO = null;
	
	public void setDAO(Drug_informationDAO drug_informationDAO)
	{
		this.drug_informationDAO = drug_informationDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Drug_informationRepository.class);
	Map<Long, Drug_informationDTO>mapOfDrug_informationDTOToiD;
	Map<Long, Set<Drug_informationDTO> >mapOfDrug_informationDTOTomedicineGenericNameType;



	static Drug_informationRepository instance = null;  
	private Drug_informationRepository(){
		mapOfDrug_informationDTOToiD = new ConcurrentHashMap<>();
		mapOfDrug_informationDTOTomedicineGenericNameType = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Drug_informationRepository getInstance(){
		if (instance == null){
			instance = new Drug_informationRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(drug_informationDAO == null)
		{
			drug_informationDAO = new Drug_informationDAO();
		}
		try {
			List<Drug_informationDTO> drug_informationDTOs = drug_informationDAO.getAllDrug_information(reloadAll);
			for(Drug_informationDTO drug_informationDTO : drug_informationDTOs) {
				Drug_informationDTO oldDrug_informationDTO = getDrug_informationDTOByID(drug_informationDTO.iD);
				if( oldDrug_informationDTO != null ) {
					mapOfDrug_informationDTOToiD.remove(oldDrug_informationDTO.iD);
					
					if(mapOfDrug_informationDTOTomedicineGenericNameType.containsKey(oldDrug_informationDTO.medicineGenericNameType)) {
						mapOfDrug_informationDTOTomedicineGenericNameType.get(oldDrug_informationDTO.medicineGenericNameType).remove(oldDrug_informationDTO);
					}
					if(mapOfDrug_informationDTOTomedicineGenericNameType.get(oldDrug_informationDTO.medicineGenericNameType).isEmpty()) {
						mapOfDrug_informationDTOTomedicineGenericNameType.remove(oldDrug_informationDTO.medicineGenericNameType);
					}
				}
				if(drug_informationDTO.isDeleted == 0) 
				{
					mapOfDrug_informationDTOToiD.put(drug_informationDTO.iD, drug_informationDTO);
					if( ! mapOfDrug_informationDTOTomedicineGenericNameType.containsKey(drug_informationDTO.medicineGenericNameType)) {
						mapOfDrug_informationDTOTomedicineGenericNameType.put(drug_informationDTO.medicineGenericNameType, new HashSet<>());
						logger.debug("Created " + drug_informationDTO.medicineGenericNameType);
					}
					mapOfDrug_informationDTOTomedicineGenericNameType.get(drug_informationDTO.medicineGenericNameType).add(drug_informationDTO);
					logger.debug("added " + drug_informationDTO.nameEn);
	
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Drug_informationDTO> getDrug_informationList() {
		List <Drug_informationDTO> drug_informations = new ArrayList<Drug_informationDTO>(this.mapOfDrug_informationDTOToiD.values());
		return drug_informations;
	}
	
	
	public Drug_informationDTO getDrug_informationDTOByID( long ID){
		return mapOfDrug_informationDTOToiD.get(ID);
	}
	
	public List<Drug_informationDTO> getDrug_informationDTOBymedicine_generic_name_type(long medicine_generic_name_type) {
		return new ArrayList<>( mapOfDrug_informationDTOTomedicineGenericNameType.getOrDefault(medicine_generic_name_type,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "drug_information";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


