package drug_information;
import java.util.*; 
import util.*;


public class Drug_informationMAPS extends CommonMaps
{	
	public Drug_informationMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("nameEn".toLowerCase(), "nameEn".toLowerCase());
		java_DTO_map.put("nameBn".toLowerCase(), "nameBn".toLowerCase());
		java_DTO_map.put("medicineGenericNameType".toLowerCase(), "medicineGenericNameType".toLowerCase());
		java_DTO_map.put("pharmaCompanyNameType".toLowerCase(), "pharmaCompanyNameType".toLowerCase());
		java_DTO_map.put("drugFormCat".toLowerCase(), "drugFormCat".toLowerCase());
		java_DTO_map.put("strength".toLowerCase(), "strength".toLowerCase());
		java_DTO_map.put("currentStock".toLowerCase(), "currentStock".toLowerCase());
		java_DTO_map.put("minimulLevelForAlert".toLowerCase(), "minimulLevelForAlert".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("name_en".toLowerCase(), "nameEn".toLowerCase());
		java_SQL_map.put("name_bn".toLowerCase(), "nameBn".toLowerCase());
		java_SQL_map.put("medicine_generic_name_type".toLowerCase(), "medicineGenericNameType".toLowerCase());
		java_SQL_map.put("pharma_company_name_type".toLowerCase(), "pharmaCompanyNameType".toLowerCase());
		java_SQL_map.put("drug_form_cat".toLowerCase(), "drugFormCat".toLowerCase());
		java_SQL_map.put("strength".toLowerCase(), "strength".toLowerCase());
		java_SQL_map.put("current_stock".toLowerCase(), "currentStock".toLowerCase());
		java_SQL_map.put("minimul_level_for_alert".toLowerCase(), "minimulLevelForAlert".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("English Name".toLowerCase(), "nameEn".toLowerCase());
		java_Text_map.put("Bangla Name".toLowerCase(), "nameBn".toLowerCase());
		java_Text_map.put("Medicine Generic Name".toLowerCase(), "medicineGenericNameType".toLowerCase());
		java_Text_map.put("Pharma Company Name".toLowerCase(), "pharmaCompanyNameType".toLowerCase());
		java_Text_map.put("Drug Form".toLowerCase(), "drugFormCat".toLowerCase());
		java_Text_map.put("Strength".toLowerCase(), "strength".toLowerCase());
		java_Text_map.put("Current Stock".toLowerCase(), "currentStock".toLowerCase());
		java_Text_map.put("Minimul Level For Alert".toLowerCase(), "minimulLevelForAlert".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}