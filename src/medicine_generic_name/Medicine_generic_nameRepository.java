package medicine_generic_name;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Medicine_generic_nameRepository implements Repository {
	Medicine_generic_nameDAO medicine_generic_nameDAO = null;
	
	public void setDAO(Medicine_generic_nameDAO medicine_generic_nameDAO)
	{
		this.medicine_generic_nameDAO = medicine_generic_nameDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Medicine_generic_nameRepository.class);
	Map<Long, Medicine_generic_nameDTO>mapOfMedicine_generic_nameDTOToiD;



	static Medicine_generic_nameRepository instance = null;  
	private Medicine_generic_nameRepository(){
		mapOfMedicine_generic_nameDTOToiD = new ConcurrentHashMap<>();


		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Medicine_generic_nameRepository getInstance(){
		if (instance == null){
			instance = new Medicine_generic_nameRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(medicine_generic_nameDAO == null)
		{
			medicine_generic_nameDAO = new Medicine_generic_nameDAO();
		}
		try {
			List<Medicine_generic_nameDTO> medicine_generic_nameDTOs = medicine_generic_nameDAO.getAllMedicine_generic_name(reloadAll);
			for(Medicine_generic_nameDTO medicine_generic_nameDTO : medicine_generic_nameDTOs) {
				Medicine_generic_nameDTO oldMedicine_generic_nameDTO = getMedicine_generic_nameDTOByID(medicine_generic_nameDTO.iD);
				if( oldMedicine_generic_nameDTO != null ) {
					mapOfMedicine_generic_nameDTOToiD.remove(oldMedicine_generic_nameDTO.iD);
				
				
					
					
				}
				if(medicine_generic_nameDTO.isDeleted == 0) 
				{
					
					mapOfMedicine_generic_nameDTOToiD.put(medicine_generic_nameDTO.iD, medicine_generic_nameDTO);
				
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Medicine_generic_nameDTO> getMedicine_generic_nameList() {
		List <Medicine_generic_nameDTO> medicine_generic_names = new ArrayList<Medicine_generic_nameDTO>(this.mapOfMedicine_generic_nameDTOToiD.values());
		return medicine_generic_names;
	}
	
	
	public Medicine_generic_nameDTO getMedicine_generic_nameDTOByID( long ID){
		return mapOfMedicine_generic_nameDTOToiD.get(ID);
	}
	
	


	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "medicine_generic_name";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


