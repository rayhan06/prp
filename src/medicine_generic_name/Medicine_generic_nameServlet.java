package medicine_generic_name;

import java.io.IOException;
import java.io.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;

import medicine_generic_name.Constants;




import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Medicine_generic_nameServlet
 */
@WebServlet("/Medicine_generic_nameServlet")
@MultipartConfig
public class Medicine_generic_nameServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Medicine_generic_nameServlet.class);

    String tableName = "medicine_generic_name";

	Medicine_generic_nameDAO medicine_generic_nameDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Medicine_generic_nameServlet()
	{
        super();
    	try
    	{
			medicine_generic_nameDAO = new Medicine_generic_nameDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(medicine_generic_nameDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICINE_GENERIC_NAME_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICINE_GENERIC_NAME_UPDATE))
				{
					getMedicine_generic_name(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICINE_GENERIC_NAME_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchMedicine_generic_name(request, response, isPermanentTable, filter);
						}
						else
						{
							searchMedicine_generic_name(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchMedicine_generic_name(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICINE_GENERIC_NAME_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICINE_GENERIC_NAME_ADD))
				{
					System.out.println("going to  addMedicine_generic_name ");
					addMedicine_generic_name(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addMedicine_generic_name ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICINE_GENERIC_NAME_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addMedicine_generic_name ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICINE_GENERIC_NAME_UPDATE))
				{
					addMedicine_generic_name(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				deleteMedicine_generic_name(request, response, userDTO);
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICINE_GENERIC_NAME_SEARCH))
				{
					searchMedicine_generic_name(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Medicine_generic_nameDTO medicine_generic_nameDTO = medicine_generic_nameDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(medicine_generic_nameDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addMedicine_generic_name(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addMedicine_generic_name");
			String path = getServletContext().getRealPath("/img2/");
			Medicine_generic_nameDTO medicine_generic_nameDTO;

			if(addFlag == true)
			{
				medicine_generic_nameDTO = new Medicine_generic_nameDTO();
			}
			else
			{
				medicine_generic_nameDTO = medicine_generic_nameDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("nameEn");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEn = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				medicine_generic_nameDTO.nameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nameBn");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameBn = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				medicine_generic_nameDTO.nameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addMedicine_generic_name dto = " + medicine_generic_nameDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				medicine_generic_nameDAO.setIsDeleted(medicine_generic_nameDTO.iD, CommonDTO.OUTDATED);
				returnedID = medicine_generic_nameDAO.add(medicine_generic_nameDTO);
				medicine_generic_nameDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = medicine_generic_nameDAO.manageWriteOperations(medicine_generic_nameDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = medicine_generic_nameDAO.manageWriteOperations(medicine_generic_nameDTO, SessionConstants.UPDATE, -1, userDTO);
			}









			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getMedicine_generic_name(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Medicine_generic_nameServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(medicine_generic_nameDAO.getDTOByID(returnedID), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}









	private void deleteMedicine_generic_name(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException
	{
		try
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);


				Medicine_generic_nameDTO medicine_generic_nameDTO = medicine_generic_nameDAO.getDTOByID(id);
				medicine_generic_nameDAO.manageWriteOperations(medicine_generic_nameDTO, SessionConstants.DELETE, id, userDTO);
				response.sendRedirect("Medicine_generic_nameServlet?actionType=search");

			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

	}

	private void getMedicine_generic_name(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getMedicine_generic_name");
		Medicine_generic_nameDTO medicine_generic_nameDTO = null;
		try
		{
			medicine_generic_nameDTO = medicine_generic_nameDAO.getDTOByID(id);
			request.setAttribute("ID", medicine_generic_nameDTO.iD);
			request.setAttribute("medicine_generic_nameDTO",medicine_generic_nameDTO);
			request.setAttribute("medicine_generic_nameDAO",medicine_generic_nameDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "medicine_generic_name/medicine_generic_nameInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "medicine_generic_name/medicine_generic_nameSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "medicine_generic_name/medicine_generic_nameEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "medicine_generic_name/medicine_generic_nameEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getMedicine_generic_name(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getMedicine_generic_name(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchMedicine_generic_name(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchMedicine_generic_name 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_MEDICINE_GENERIC_NAME,
			request,
			medicine_generic_nameDAO,
			SessionConstants.VIEW_MEDICINE_GENERIC_NAME,
			SessionConstants.SEARCH_MEDICINE_GENERIC_NAME,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("medicine_generic_nameDAO",medicine_generic_nameDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to medicine_generic_name/medicine_generic_nameApproval.jsp");
	        	rd = request.getRequestDispatcher("medicine_generic_name/medicine_generic_nameApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to medicine_generic_name/medicine_generic_nameApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("medicine_generic_name/medicine_generic_nameApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to medicine_generic_name/medicine_generic_nameSearch.jsp");
	        	rd = request.getRequestDispatcher("medicine_generic_name/medicine_generic_nameSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to medicine_generic_name/medicine_generic_nameSearchForm.jsp");
	        	rd = request.getRequestDispatcher("medicine_generic_name/medicine_generic_nameSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

