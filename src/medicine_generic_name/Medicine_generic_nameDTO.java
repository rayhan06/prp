package medicine_generic_name;
import java.util.*; 
import util.*; 


public class Medicine_generic_nameDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
	
	
    @Override
	public String toString() {
            return "$Medicine_generic_nameDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}