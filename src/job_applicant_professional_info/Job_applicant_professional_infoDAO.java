package job_applicant_professional_info;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import common.ConnectionAndStatementUtil;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO;

public class Job_applicant_professional_infoDAO  extends NavigationService4
{

	Logger logger = Logger.getLogger(getClass());

//	public CommonMaps approvalMaps = new Job_applicant_professional_infoApprovalMAPS("job_applicant_professional_info");

	public Job_applicant_professional_infoDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
//		commonMaps = new Job_applicant_professional_infoMAPS(tableName);
	}

	@Override
	public Collection getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
		return null;
	}

	public Job_applicant_professional_infoDAO()
	{
		this("job_applicant_professional_info");
	}

	public void get(Job_applicant_professional_infoDTO job_applicant_professional_infoDTO, ResultSet rs) throws SQLException
	{
		job_applicant_professional_infoDTO.iD = rs.getLong("ID");
		job_applicant_professional_infoDTO.jobApplicantId = rs.getLong("job_applicant_id");
		job_applicant_professional_infoDTO.jobId = rs.getLong("job_id");
		job_applicant_professional_infoDTO.govtServiceStartDate = rs.getLong("govt_service_start_date");
		job_applicant_professional_infoDTO.gazettedDate = rs.getLong("gazetted_date");
		job_applicant_professional_infoDTO.encardmentDate = rs.getLong("encardment_date");
		job_applicant_professional_infoDTO.employeeCadreId = rs.getLong("employee_cadre_id");
		job_applicant_professional_infoDTO.employeeBatchesId = rs.getLong("employee_batches_id");
		job_applicant_professional_infoDTO.designation = rs.getString("designation");
		job_applicant_professional_infoDTO.department = rs.getString("department");
		job_applicant_professional_infoDTO.lprDate = rs.getLong("lpr_date");
		job_applicant_professional_infoDTO.isGovtJob = rs.getInt("is_govt_job");
		job_applicant_professional_infoDTO.nameOfEmployee = rs.getString("name_of_employee");
		job_applicant_professional_infoDTO.employeeAddress = rs.getString("employee_address");
		job_applicant_professional_infoDTO.employeeEmploymentType = rs.getInt("employee_employment_type");
		job_applicant_professional_infoDTO.servingFrom = rs.getLong("serving_from");
		job_applicant_professional_infoDTO.servingTo = rs.getLong("serving_to");
		job_applicant_professional_infoDTO.isCurrentlyWorking = rs.getInt("is_currently_working");
		job_applicant_professional_infoDTO.jobResponsibility = rs.getString("job_responsibility");
		job_applicant_professional_infoDTO.employeeWorkAreaType = rs.getInt("employee_work_area_type");
		job_applicant_professional_infoDTO.keyAcheivement = rs.getString("key_acheivement");
		job_applicant_professional_infoDTO.totalExperience = rs.getLong("total_experience");
		job_applicant_professional_infoDTO.serviceType = rs.getInt("service_type");
		job_applicant_professional_infoDTO.supervisorName = rs.getString("supervisor_name");
		job_applicant_professional_infoDTO.supervisorMobile = rs.getString("supervisor_mobile");
		job_applicant_professional_infoDTO.supervisorEmail = rs.getString("supervisor_email");
		job_applicant_professional_infoDTO.filesDropzone = rs.getLong("filesDropzone");
		job_applicant_professional_infoDTO.insertionDate = rs.getLong("insertion_date");
		job_applicant_professional_infoDTO.insertedBy = rs.getLong("inserted_by");
		job_applicant_professional_infoDTO.modifiedBy = rs.getLong("modified_by");
		job_applicant_professional_infoDTO.jobCat = rs.getInt("job_cat");
		job_applicant_professional_infoDTO.isDeleted = rs.getInt("isDeleted");
		job_applicant_professional_infoDTO.lastModificationTime = rs.getLong("lastModificationTime");
		job_applicant_professional_infoDTO.jobApplicantCandidateCat = rs.getInt("job_applicant_candidate_cat");
		job_applicant_professional_infoDTO.jobApplicantCandidateText = rs.getString("job_applicant_candidate_text");

	}

	public Job_applicant_professional_infoDTO build(ResultSet rs)
	{
		try
		{
			Job_applicant_professional_infoDTO job_applicant_professional_infoDTO = new Job_applicant_professional_infoDTO();
			job_applicant_professional_infoDTO.iD = rs.getLong("ID");
			job_applicant_professional_infoDTO.jobApplicantId = rs.getLong("job_applicant_id");
			job_applicant_professional_infoDTO.jobId = rs.getLong("job_id");
			job_applicant_professional_infoDTO.govtServiceStartDate = rs.getLong("govt_service_start_date");
			job_applicant_professional_infoDTO.gazettedDate = rs.getLong("gazetted_date");
			job_applicant_professional_infoDTO.encardmentDate = rs.getLong("encardment_date");
			job_applicant_professional_infoDTO.employeeCadreId = rs.getLong("employee_cadre_id");
			job_applicant_professional_infoDTO.employeeBatchesId = rs.getLong("employee_batches_id");
			job_applicant_professional_infoDTO.designation = rs.getString("designation");
			job_applicant_professional_infoDTO.department = rs.getString("department");
			job_applicant_professional_infoDTO.lprDate = rs.getLong("lpr_date");
			job_applicant_professional_infoDTO.isGovtJob = rs.getInt("is_govt_job");
			job_applicant_professional_infoDTO.nameOfEmployee = rs.getString("name_of_employee");
			job_applicant_professional_infoDTO.employeeAddress = rs.getString("employee_address");
			job_applicant_professional_infoDTO.employeeEmploymentType = rs.getInt("employee_employment_type");
			job_applicant_professional_infoDTO.servingFrom = rs.getLong("serving_from");
			job_applicant_professional_infoDTO.servingTo = rs.getLong("serving_to");
			job_applicant_professional_infoDTO.isCurrentlyWorking = rs.getInt("is_currently_working");
			job_applicant_professional_infoDTO.jobResponsibility = rs.getString("job_responsibility");
			job_applicant_professional_infoDTO.employeeWorkAreaType = rs.getInt("employee_work_area_type");
			job_applicant_professional_infoDTO.keyAcheivement = rs.getString("key_acheivement");
			job_applicant_professional_infoDTO.totalExperience = rs.getLong("total_experience");
			job_applicant_professional_infoDTO.serviceType = rs.getInt("service_type");
			job_applicant_professional_infoDTO.supervisorName = rs.getString("supervisor_name");
			job_applicant_professional_infoDTO.supervisorMobile = rs.getString("supervisor_mobile");
			job_applicant_professional_infoDTO.supervisorEmail = rs.getString("supervisor_email");
			job_applicant_professional_infoDTO.filesDropzone = rs.getLong("filesDropzone");
			job_applicant_professional_infoDTO.insertionDate = rs.getLong("insertion_date");
			job_applicant_professional_infoDTO.insertedBy = rs.getLong("inserted_by");
			job_applicant_professional_infoDTO.modifiedBy = rs.getLong("modified_by");
			job_applicant_professional_infoDTO.jobCat = rs.getInt("job_cat");
			job_applicant_professional_infoDTO.isDeleted = rs.getInt("isDeleted");
			job_applicant_professional_infoDTO.lastModificationTime = rs.getLong("lastModificationTime");
			job_applicant_professional_infoDTO.jobApplicantCandidateCat = rs.getInt("job_applicant_candidate_cat");
			job_applicant_professional_infoDTO.jobApplicantCandidateText = rs.getString("job_applicant_candidate_text");
			return job_applicant_professional_infoDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}



	//need another getter for repository
	public Job_applicant_professional_infoDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		Job_applicant_professional_infoDTO job_applicant_professional_infoDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
		return job_applicant_professional_infoDTO;
	}


	public List<Job_applicant_professional_infoDTO> getDTOs(Collection recordIDs){
		if(recordIDs.isEmpty()){
			return new ArrayList<>();
		}
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+= " ? ";
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);

	}






	//add repository
	public List<Job_applicant_professional_infoDTO> getAllJob_applicant_professional_info (boolean isFirstReload)
	{

		String sql = "SELECT * FROM job_applicant_professional_info";
		sql += " WHERE ";


		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;
		}
		sql += " order by job_applicant_professional_info.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}


	public List<Job_applicant_professional_infoDTO> getDTOByJobApplicantID (long ID)
	{

		String sql = "SELECT * FROM job_applicant_professional_info";
		sql += " WHERE job_applicant_id = ? ";

		sql+=" AND isDeleted =  0";

		sql += " order by job_applicant_professional_info.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(ID), this::build);
	}

}
	