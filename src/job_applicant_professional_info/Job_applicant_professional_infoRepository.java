package job_applicant_professional_info;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Job_applicant_professional_infoRepository implements Repository {
	Job_applicant_professional_infoDAO job_applicant_professional_infoDAO = null;
	
	public void setDAO(Job_applicant_professional_infoDAO job_applicant_professional_infoDAO)
	{
		this.job_applicant_professional_infoDAO = job_applicant_professional_infoDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Job_applicant_professional_infoRepository.class);
	Map<Long, Job_applicant_professional_infoDTO>mapOfJob_applicant_professional_infoDTOToiD;
	Map<Long, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOTojobApplicantId;
	Map<Long, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOTogovtServiceStartDate;
	Map<Long, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOTogazettedDate;
	Map<Long, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOToencardmentDate;
	Map<Long, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOToemployeeCadreId;
	Map<Long, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOToemployeeBatchesId;
	Map<String, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOTodesignation;
	Map<String, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOTodepartment;
	Map<Long, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOTolprDate;
	Map<Integer, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOToisGovtJob;
	Map<String, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOTonameOfEmployee;
	Map<String, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOToemployeeAddress;
	Map<Integer, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOToemployeeEmploymentType;
	Map<Long, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOToservingFrom;
	Map<Long, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOToservingTo;
	Map<Integer, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOToisCurrentlyWorking;
	Map<String, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOTojobResponsibility;
	Map<Integer, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOToemployeeWorkAreaType;
	Map<String, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOTokeyAcheivement;
	Map<Long, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOTototalExperience;
	Map<Integer, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOToserviceType;
	Map<String, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOTosupervisorName;
	Map<String, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOTosupervisorMobile;
	Map<String, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOTosupervisorEmail;
	Map<Long, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOTofilesDropzone;
	Map<Long, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOToinsertionDate;
	Map<Long, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOToinsertedBy;
	Map<Long, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOTomodifiedBy;
	Map<Integer, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOTojobCat;
	Map<Long, Set<Job_applicant_professional_infoDTO> >mapOfJob_applicant_professional_infoDTOTolastModificationTime;


	static Job_applicant_professional_infoRepository instance = null;  
	private Job_applicant_professional_infoRepository(){
		mapOfJob_applicant_professional_infoDTOToiD = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOTojobApplicantId = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOTogovtServiceStartDate = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOTogazettedDate = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOToencardmentDate = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOToemployeeCadreId = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOToemployeeBatchesId = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOTodesignation = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOTodepartment = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOTolprDate = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOToisGovtJob = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOTonameOfEmployee = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOToemployeeAddress = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOToemployeeEmploymentType = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOToservingFrom = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOToservingTo = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOToisCurrentlyWorking = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOTojobResponsibility = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOToemployeeWorkAreaType = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOTokeyAcheivement = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOTototalExperience = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOToserviceType = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOTosupervisorName = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOTosupervisorMobile = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOTosupervisorEmail = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOTofilesDropzone = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOToinsertedBy = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOTojobCat = new ConcurrentHashMap<>();
		mapOfJob_applicant_professional_infoDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Job_applicant_professional_infoRepository getInstance(){
		if (instance == null){
			instance = new Job_applicant_professional_infoRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(job_applicant_professional_infoDAO == null)
		{
			return;
		}
		try {
			List<Job_applicant_professional_infoDTO> job_applicant_professional_infoDTOs = job_applicant_professional_infoDAO.getAllJob_applicant_professional_info(reloadAll);
			for(Job_applicant_professional_infoDTO job_applicant_professional_infoDTO : job_applicant_professional_infoDTOs) {
				Job_applicant_professional_infoDTO oldJob_applicant_professional_infoDTO = getJob_applicant_professional_infoDTOByID(job_applicant_professional_infoDTO.iD);
				if( oldJob_applicant_professional_infoDTO != null ) {
					mapOfJob_applicant_professional_infoDTOToiD.remove(oldJob_applicant_professional_infoDTO.iD);
				
					if(mapOfJob_applicant_professional_infoDTOTojobApplicantId.containsKey(oldJob_applicant_professional_infoDTO.jobApplicantId)) {
						mapOfJob_applicant_professional_infoDTOTojobApplicantId.get(oldJob_applicant_professional_infoDTO.jobApplicantId).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOTojobApplicantId.get(oldJob_applicant_professional_infoDTO.jobApplicantId).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOTojobApplicantId.remove(oldJob_applicant_professional_infoDTO.jobApplicantId);
					}
					
					if(mapOfJob_applicant_professional_infoDTOTogovtServiceStartDate.containsKey(oldJob_applicant_professional_infoDTO.govtServiceStartDate)) {
						mapOfJob_applicant_professional_infoDTOTogovtServiceStartDate.get(oldJob_applicant_professional_infoDTO.govtServiceStartDate).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOTogovtServiceStartDate.get(oldJob_applicant_professional_infoDTO.govtServiceStartDate).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOTogovtServiceStartDate.remove(oldJob_applicant_professional_infoDTO.govtServiceStartDate);
					}
					
					if(mapOfJob_applicant_professional_infoDTOTogazettedDate.containsKey(oldJob_applicant_professional_infoDTO.gazettedDate)) {
						mapOfJob_applicant_professional_infoDTOTogazettedDate.get(oldJob_applicant_professional_infoDTO.gazettedDate).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOTogazettedDate.get(oldJob_applicant_professional_infoDTO.gazettedDate).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOTogazettedDate.remove(oldJob_applicant_professional_infoDTO.gazettedDate);
					}
					
					if(mapOfJob_applicant_professional_infoDTOToencardmentDate.containsKey(oldJob_applicant_professional_infoDTO.encardmentDate)) {
						mapOfJob_applicant_professional_infoDTOToencardmentDate.get(oldJob_applicant_professional_infoDTO.encardmentDate).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOToencardmentDate.get(oldJob_applicant_professional_infoDTO.encardmentDate).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOToencardmentDate.remove(oldJob_applicant_professional_infoDTO.encardmentDate);
					}
					
					if(mapOfJob_applicant_professional_infoDTOToemployeeCadreId.containsKey(oldJob_applicant_professional_infoDTO.employeeCadreId)) {
						mapOfJob_applicant_professional_infoDTOToemployeeCadreId.get(oldJob_applicant_professional_infoDTO.employeeCadreId).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOToemployeeCadreId.get(oldJob_applicant_professional_infoDTO.employeeCadreId).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOToemployeeCadreId.remove(oldJob_applicant_professional_infoDTO.employeeCadreId);
					}
					
					if(mapOfJob_applicant_professional_infoDTOToemployeeBatchesId.containsKey(oldJob_applicant_professional_infoDTO.employeeBatchesId)) {
						mapOfJob_applicant_professional_infoDTOToemployeeBatchesId.get(oldJob_applicant_professional_infoDTO.employeeBatchesId).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOToemployeeBatchesId.get(oldJob_applicant_professional_infoDTO.employeeBatchesId).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOToemployeeBatchesId.remove(oldJob_applicant_professional_infoDTO.employeeBatchesId);
					}
					
					if(mapOfJob_applicant_professional_infoDTOTodesignation.containsKey(oldJob_applicant_professional_infoDTO.designation)) {
						mapOfJob_applicant_professional_infoDTOTodesignation.get(oldJob_applicant_professional_infoDTO.designation).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOTodesignation.get(oldJob_applicant_professional_infoDTO.designation).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOTodesignation.remove(oldJob_applicant_professional_infoDTO.designation);
					}
					
					if(mapOfJob_applicant_professional_infoDTOTodepartment.containsKey(oldJob_applicant_professional_infoDTO.department)) {
						mapOfJob_applicant_professional_infoDTOTodepartment.get(oldJob_applicant_professional_infoDTO.department).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOTodepartment.get(oldJob_applicant_professional_infoDTO.department).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOTodepartment.remove(oldJob_applicant_professional_infoDTO.department);
					}
					
					if(mapOfJob_applicant_professional_infoDTOTolprDate.containsKey(oldJob_applicant_professional_infoDTO.lprDate)) {
						mapOfJob_applicant_professional_infoDTOTolprDate.get(oldJob_applicant_professional_infoDTO.lprDate).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOTolprDate.get(oldJob_applicant_professional_infoDTO.lprDate).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOTolprDate.remove(oldJob_applicant_professional_infoDTO.lprDate);
					}
					
					if(mapOfJob_applicant_professional_infoDTOToisGovtJob.containsKey(oldJob_applicant_professional_infoDTO.isGovtJob)) {
						mapOfJob_applicant_professional_infoDTOToisGovtJob.get(oldJob_applicant_professional_infoDTO.isGovtJob).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOToisGovtJob.get(oldJob_applicant_professional_infoDTO.isGovtJob).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOToisGovtJob.remove(oldJob_applicant_professional_infoDTO.isGovtJob);
					}
					
					if(mapOfJob_applicant_professional_infoDTOTonameOfEmployee.containsKey(oldJob_applicant_professional_infoDTO.nameOfEmployee)) {
						mapOfJob_applicant_professional_infoDTOTonameOfEmployee.get(oldJob_applicant_professional_infoDTO.nameOfEmployee).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOTonameOfEmployee.get(oldJob_applicant_professional_infoDTO.nameOfEmployee).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOTonameOfEmployee.remove(oldJob_applicant_professional_infoDTO.nameOfEmployee);
					}
					
					if(mapOfJob_applicant_professional_infoDTOToemployeeAddress.containsKey(oldJob_applicant_professional_infoDTO.employeeAddress)) {
						mapOfJob_applicant_professional_infoDTOToemployeeAddress.get(oldJob_applicant_professional_infoDTO.employeeAddress).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOToemployeeAddress.get(oldJob_applicant_professional_infoDTO.employeeAddress).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOToemployeeAddress.remove(oldJob_applicant_professional_infoDTO.employeeAddress);
					}
					
					if(mapOfJob_applicant_professional_infoDTOToemployeeEmploymentType.containsKey(oldJob_applicant_professional_infoDTO.employeeEmploymentType)) {
						mapOfJob_applicant_professional_infoDTOToemployeeEmploymentType.get(oldJob_applicant_professional_infoDTO.employeeEmploymentType).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOToemployeeEmploymentType.get(oldJob_applicant_professional_infoDTO.employeeEmploymentType).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOToemployeeEmploymentType.remove(oldJob_applicant_professional_infoDTO.employeeEmploymentType);
					}
					
					if(mapOfJob_applicant_professional_infoDTOToservingFrom.containsKey(oldJob_applicant_professional_infoDTO.servingFrom)) {
						mapOfJob_applicant_professional_infoDTOToservingFrom.get(oldJob_applicant_professional_infoDTO.servingFrom).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOToservingFrom.get(oldJob_applicant_professional_infoDTO.servingFrom).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOToservingFrom.remove(oldJob_applicant_professional_infoDTO.servingFrom);
					}
					
					if(mapOfJob_applicant_professional_infoDTOToservingTo.containsKey(oldJob_applicant_professional_infoDTO.servingTo)) {
						mapOfJob_applicant_professional_infoDTOToservingTo.get(oldJob_applicant_professional_infoDTO.servingTo).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOToservingTo.get(oldJob_applicant_professional_infoDTO.servingTo).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOToservingTo.remove(oldJob_applicant_professional_infoDTO.servingTo);
					}
					
					if(mapOfJob_applicant_professional_infoDTOToisCurrentlyWorking.containsKey(oldJob_applicant_professional_infoDTO.isCurrentlyWorking)) {
						mapOfJob_applicant_professional_infoDTOToisCurrentlyWorking.get(oldJob_applicant_professional_infoDTO.isCurrentlyWorking).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOToisCurrentlyWorking.get(oldJob_applicant_professional_infoDTO.isCurrentlyWorking).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOToisCurrentlyWorking.remove(oldJob_applicant_professional_infoDTO.isCurrentlyWorking);
					}
					
					if(mapOfJob_applicant_professional_infoDTOTojobResponsibility.containsKey(oldJob_applicant_professional_infoDTO.jobResponsibility)) {
						mapOfJob_applicant_professional_infoDTOTojobResponsibility.get(oldJob_applicant_professional_infoDTO.jobResponsibility).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOTojobResponsibility.get(oldJob_applicant_professional_infoDTO.jobResponsibility).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOTojobResponsibility.remove(oldJob_applicant_professional_infoDTO.jobResponsibility);
					}
					
					if(mapOfJob_applicant_professional_infoDTOToemployeeWorkAreaType.containsKey(oldJob_applicant_professional_infoDTO.employeeWorkAreaType)) {
						mapOfJob_applicant_professional_infoDTOToemployeeWorkAreaType.get(oldJob_applicant_professional_infoDTO.employeeWorkAreaType).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOToemployeeWorkAreaType.get(oldJob_applicant_professional_infoDTO.employeeWorkAreaType).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOToemployeeWorkAreaType.remove(oldJob_applicant_professional_infoDTO.employeeWorkAreaType);
					}
					
					if(mapOfJob_applicant_professional_infoDTOTokeyAcheivement.containsKey(oldJob_applicant_professional_infoDTO.keyAcheivement)) {
						mapOfJob_applicant_professional_infoDTOTokeyAcheivement.get(oldJob_applicant_professional_infoDTO.keyAcheivement).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOTokeyAcheivement.get(oldJob_applicant_professional_infoDTO.keyAcheivement).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOTokeyAcheivement.remove(oldJob_applicant_professional_infoDTO.keyAcheivement);
					}
					
					if(mapOfJob_applicant_professional_infoDTOTototalExperience.containsKey(oldJob_applicant_professional_infoDTO.totalExperience)) {
						mapOfJob_applicant_professional_infoDTOTototalExperience.get(oldJob_applicant_professional_infoDTO.totalExperience).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOTototalExperience.get(oldJob_applicant_professional_infoDTO.totalExperience).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOTototalExperience.remove(oldJob_applicant_professional_infoDTO.totalExperience);
					}
					
					if(mapOfJob_applicant_professional_infoDTOToserviceType.containsKey(oldJob_applicant_professional_infoDTO.serviceType)) {
						mapOfJob_applicant_professional_infoDTOToserviceType.get(oldJob_applicant_professional_infoDTO.serviceType).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOToserviceType.get(oldJob_applicant_professional_infoDTO.serviceType).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOToserviceType.remove(oldJob_applicant_professional_infoDTO.serviceType);
					}
					
					if(mapOfJob_applicant_professional_infoDTOTosupervisorName.containsKey(oldJob_applicant_professional_infoDTO.supervisorName)) {
						mapOfJob_applicant_professional_infoDTOTosupervisorName.get(oldJob_applicant_professional_infoDTO.supervisorName).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOTosupervisorName.get(oldJob_applicant_professional_infoDTO.supervisorName).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOTosupervisorName.remove(oldJob_applicant_professional_infoDTO.supervisorName);
					}
					
					if(mapOfJob_applicant_professional_infoDTOTosupervisorMobile.containsKey(oldJob_applicant_professional_infoDTO.supervisorMobile)) {
						mapOfJob_applicant_professional_infoDTOTosupervisorMobile.get(oldJob_applicant_professional_infoDTO.supervisorMobile).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOTosupervisorMobile.get(oldJob_applicant_professional_infoDTO.supervisorMobile).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOTosupervisorMobile.remove(oldJob_applicant_professional_infoDTO.supervisorMobile);
					}
					
					if(mapOfJob_applicant_professional_infoDTOTosupervisorEmail.containsKey(oldJob_applicant_professional_infoDTO.supervisorEmail)) {
						mapOfJob_applicant_professional_infoDTOTosupervisorEmail.get(oldJob_applicant_professional_infoDTO.supervisorEmail).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOTosupervisorEmail.get(oldJob_applicant_professional_infoDTO.supervisorEmail).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOTosupervisorEmail.remove(oldJob_applicant_professional_infoDTO.supervisorEmail);
					}
					
					if(mapOfJob_applicant_professional_infoDTOTofilesDropzone.containsKey(oldJob_applicant_professional_infoDTO.filesDropzone)) {
						mapOfJob_applicant_professional_infoDTOTofilesDropzone.get(oldJob_applicant_professional_infoDTO.filesDropzone).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOTofilesDropzone.get(oldJob_applicant_professional_infoDTO.filesDropzone).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOTofilesDropzone.remove(oldJob_applicant_professional_infoDTO.filesDropzone);
					}
					
					if(mapOfJob_applicant_professional_infoDTOToinsertionDate.containsKey(oldJob_applicant_professional_infoDTO.insertionDate)) {
						mapOfJob_applicant_professional_infoDTOToinsertionDate.get(oldJob_applicant_professional_infoDTO.insertionDate).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOToinsertionDate.get(oldJob_applicant_professional_infoDTO.insertionDate).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOToinsertionDate.remove(oldJob_applicant_professional_infoDTO.insertionDate);
					}
					
					if(mapOfJob_applicant_professional_infoDTOToinsertedBy.containsKey(oldJob_applicant_professional_infoDTO.insertedBy)) {
						mapOfJob_applicant_professional_infoDTOToinsertedBy.get(oldJob_applicant_professional_infoDTO.insertedBy).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOToinsertedBy.get(oldJob_applicant_professional_infoDTO.insertedBy).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOToinsertedBy.remove(oldJob_applicant_professional_infoDTO.insertedBy);
					}
					
					if(mapOfJob_applicant_professional_infoDTOTomodifiedBy.containsKey(oldJob_applicant_professional_infoDTO.modifiedBy)) {
						mapOfJob_applicant_professional_infoDTOTomodifiedBy.get(oldJob_applicant_professional_infoDTO.modifiedBy).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOTomodifiedBy.get(oldJob_applicant_professional_infoDTO.modifiedBy).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOTomodifiedBy.remove(oldJob_applicant_professional_infoDTO.modifiedBy);
					}
					
					if(mapOfJob_applicant_professional_infoDTOTojobCat.containsKey(oldJob_applicant_professional_infoDTO.jobCat)) {
						mapOfJob_applicant_professional_infoDTOTojobCat.get(oldJob_applicant_professional_infoDTO.jobCat).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOTojobCat.get(oldJob_applicant_professional_infoDTO.jobCat).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOTojobCat.remove(oldJob_applicant_professional_infoDTO.jobCat);
					}
					
					if(mapOfJob_applicant_professional_infoDTOTolastModificationTime.containsKey(oldJob_applicant_professional_infoDTO.lastModificationTime)) {
						mapOfJob_applicant_professional_infoDTOTolastModificationTime.get(oldJob_applicant_professional_infoDTO.lastModificationTime).remove(oldJob_applicant_professional_infoDTO);
					}
					if(mapOfJob_applicant_professional_infoDTOTolastModificationTime.get(oldJob_applicant_professional_infoDTO.lastModificationTime).isEmpty()) {
						mapOfJob_applicant_professional_infoDTOTolastModificationTime.remove(oldJob_applicant_professional_infoDTO.lastModificationTime);
					}
					
					
				}
				if(job_applicant_professional_infoDTO.isDeleted == 0) 
				{
					
					mapOfJob_applicant_professional_infoDTOToiD.put(job_applicant_professional_infoDTO.iD, job_applicant_professional_infoDTO);
				
					if( ! mapOfJob_applicant_professional_infoDTOTojobApplicantId.containsKey(job_applicant_professional_infoDTO.jobApplicantId)) {
						mapOfJob_applicant_professional_infoDTOTojobApplicantId.put(job_applicant_professional_infoDTO.jobApplicantId, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOTojobApplicantId.get(job_applicant_professional_infoDTO.jobApplicantId).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOTogovtServiceStartDate.containsKey(job_applicant_professional_infoDTO.govtServiceStartDate)) {
						mapOfJob_applicant_professional_infoDTOTogovtServiceStartDate.put(job_applicant_professional_infoDTO.govtServiceStartDate, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOTogovtServiceStartDate.get(job_applicant_professional_infoDTO.govtServiceStartDate).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOTogazettedDate.containsKey(job_applicant_professional_infoDTO.gazettedDate)) {
						mapOfJob_applicant_professional_infoDTOTogazettedDate.put(job_applicant_professional_infoDTO.gazettedDate, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOTogazettedDate.get(job_applicant_professional_infoDTO.gazettedDate).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOToencardmentDate.containsKey(job_applicant_professional_infoDTO.encardmentDate)) {
						mapOfJob_applicant_professional_infoDTOToencardmentDate.put(job_applicant_professional_infoDTO.encardmentDate, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOToencardmentDate.get(job_applicant_professional_infoDTO.encardmentDate).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOToemployeeCadreId.containsKey(job_applicant_professional_infoDTO.employeeCadreId)) {
						mapOfJob_applicant_professional_infoDTOToemployeeCadreId.put(job_applicant_professional_infoDTO.employeeCadreId, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOToemployeeCadreId.get(job_applicant_professional_infoDTO.employeeCadreId).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOToemployeeBatchesId.containsKey(job_applicant_professional_infoDTO.employeeBatchesId)) {
						mapOfJob_applicant_professional_infoDTOToemployeeBatchesId.put(job_applicant_professional_infoDTO.employeeBatchesId, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOToemployeeBatchesId.get(job_applicant_professional_infoDTO.employeeBatchesId).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOTodesignation.containsKey(job_applicant_professional_infoDTO.designation)) {
						mapOfJob_applicant_professional_infoDTOTodesignation.put(job_applicant_professional_infoDTO.designation, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOTodesignation.get(job_applicant_professional_infoDTO.designation).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOTodepartment.containsKey(job_applicant_professional_infoDTO.department)) {
						mapOfJob_applicant_professional_infoDTOTodepartment.put(job_applicant_professional_infoDTO.department, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOTodepartment.get(job_applicant_professional_infoDTO.department).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOTolprDate.containsKey(job_applicant_professional_infoDTO.lprDate)) {
						mapOfJob_applicant_professional_infoDTOTolprDate.put(job_applicant_professional_infoDTO.lprDate, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOTolprDate.get(job_applicant_professional_infoDTO.lprDate).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOToisGovtJob.containsKey(job_applicant_professional_infoDTO.isGovtJob)) {
						mapOfJob_applicant_professional_infoDTOToisGovtJob.put(job_applicant_professional_infoDTO.isGovtJob, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOToisGovtJob.get(job_applicant_professional_infoDTO.isGovtJob).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOTonameOfEmployee.containsKey(job_applicant_professional_infoDTO.nameOfEmployee)) {
						mapOfJob_applicant_professional_infoDTOTonameOfEmployee.put(job_applicant_professional_infoDTO.nameOfEmployee, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOTonameOfEmployee.get(job_applicant_professional_infoDTO.nameOfEmployee).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOToemployeeAddress.containsKey(job_applicant_professional_infoDTO.employeeAddress)) {
						mapOfJob_applicant_professional_infoDTOToemployeeAddress.put(job_applicant_professional_infoDTO.employeeAddress, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOToemployeeAddress.get(job_applicant_professional_infoDTO.employeeAddress).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOToemployeeEmploymentType.containsKey(job_applicant_professional_infoDTO.employeeEmploymentType)) {
						mapOfJob_applicant_professional_infoDTOToemployeeEmploymentType.put(job_applicant_professional_infoDTO.employeeEmploymentType, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOToemployeeEmploymentType.get(job_applicant_professional_infoDTO.employeeEmploymentType).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOToservingFrom.containsKey(job_applicant_professional_infoDTO.servingFrom)) {
						mapOfJob_applicant_professional_infoDTOToservingFrom.put(job_applicant_professional_infoDTO.servingFrom, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOToservingFrom.get(job_applicant_professional_infoDTO.servingFrom).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOToservingTo.containsKey(job_applicant_professional_infoDTO.servingTo)) {
						mapOfJob_applicant_professional_infoDTOToservingTo.put(job_applicant_professional_infoDTO.servingTo, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOToservingTo.get(job_applicant_professional_infoDTO.servingTo).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOToisCurrentlyWorking.containsKey(job_applicant_professional_infoDTO.isCurrentlyWorking)) {
						mapOfJob_applicant_professional_infoDTOToisCurrentlyWorking.put(job_applicant_professional_infoDTO.isCurrentlyWorking, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOToisCurrentlyWorking.get(job_applicant_professional_infoDTO.isCurrentlyWorking).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOTojobResponsibility.containsKey(job_applicant_professional_infoDTO.jobResponsibility)) {
						mapOfJob_applicant_professional_infoDTOTojobResponsibility.put(job_applicant_professional_infoDTO.jobResponsibility, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOTojobResponsibility.get(job_applicant_professional_infoDTO.jobResponsibility).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOToemployeeWorkAreaType.containsKey(job_applicant_professional_infoDTO.employeeWorkAreaType)) {
						mapOfJob_applicant_professional_infoDTOToemployeeWorkAreaType.put(job_applicant_professional_infoDTO.employeeWorkAreaType, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOToemployeeWorkAreaType.get(job_applicant_professional_infoDTO.employeeWorkAreaType).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOTokeyAcheivement.containsKey(job_applicant_professional_infoDTO.keyAcheivement)) {
						mapOfJob_applicant_professional_infoDTOTokeyAcheivement.put(job_applicant_professional_infoDTO.keyAcheivement, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOTokeyAcheivement.get(job_applicant_professional_infoDTO.keyAcheivement).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOTototalExperience.containsKey(job_applicant_professional_infoDTO.totalExperience)) {
						mapOfJob_applicant_professional_infoDTOTototalExperience.put(job_applicant_professional_infoDTO.totalExperience, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOTototalExperience.get(job_applicant_professional_infoDTO.totalExperience).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOToserviceType.containsKey(job_applicant_professional_infoDTO.serviceType)) {
						mapOfJob_applicant_professional_infoDTOToserviceType.put(job_applicant_professional_infoDTO.serviceType, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOToserviceType.get(job_applicant_professional_infoDTO.serviceType).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOTosupervisorName.containsKey(job_applicant_professional_infoDTO.supervisorName)) {
						mapOfJob_applicant_professional_infoDTOTosupervisorName.put(job_applicant_professional_infoDTO.supervisorName, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOTosupervisorName.get(job_applicant_professional_infoDTO.supervisorName).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOTosupervisorMobile.containsKey(job_applicant_professional_infoDTO.supervisorMobile)) {
						mapOfJob_applicant_professional_infoDTOTosupervisorMobile.put(job_applicant_professional_infoDTO.supervisorMobile, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOTosupervisorMobile.get(job_applicant_professional_infoDTO.supervisorMobile).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOTosupervisorEmail.containsKey(job_applicant_professional_infoDTO.supervisorEmail)) {
						mapOfJob_applicant_professional_infoDTOTosupervisorEmail.put(job_applicant_professional_infoDTO.supervisorEmail, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOTosupervisorEmail.get(job_applicant_professional_infoDTO.supervisorEmail).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOTofilesDropzone.containsKey(job_applicant_professional_infoDTO.filesDropzone)) {
						mapOfJob_applicant_professional_infoDTOTofilesDropzone.put(job_applicant_professional_infoDTO.filesDropzone, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOTofilesDropzone.get(job_applicant_professional_infoDTO.filesDropzone).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOToinsertionDate.containsKey(job_applicant_professional_infoDTO.insertionDate)) {
						mapOfJob_applicant_professional_infoDTOToinsertionDate.put(job_applicant_professional_infoDTO.insertionDate, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOToinsertionDate.get(job_applicant_professional_infoDTO.insertionDate).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOToinsertedBy.containsKey(job_applicant_professional_infoDTO.insertedBy)) {
						mapOfJob_applicant_professional_infoDTOToinsertedBy.put(job_applicant_professional_infoDTO.insertedBy, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOToinsertedBy.get(job_applicant_professional_infoDTO.insertedBy).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOTomodifiedBy.containsKey(job_applicant_professional_infoDTO.modifiedBy)) {
						mapOfJob_applicant_professional_infoDTOTomodifiedBy.put(job_applicant_professional_infoDTO.modifiedBy, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOTomodifiedBy.get(job_applicant_professional_infoDTO.modifiedBy).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOTojobCat.containsKey(job_applicant_professional_infoDTO.jobCat)) {
						mapOfJob_applicant_professional_infoDTOTojobCat.put(job_applicant_professional_infoDTO.jobCat, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOTojobCat.get(job_applicant_professional_infoDTO.jobCat).add(job_applicant_professional_infoDTO);
					
					if( ! mapOfJob_applicant_professional_infoDTOTolastModificationTime.containsKey(job_applicant_professional_infoDTO.lastModificationTime)) {
						mapOfJob_applicant_professional_infoDTOTolastModificationTime.put(job_applicant_professional_infoDTO.lastModificationTime, new HashSet<>());
					}
					mapOfJob_applicant_professional_infoDTOTolastModificationTime.get(job_applicant_professional_infoDTO.lastModificationTime).add(job_applicant_professional_infoDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoList() {
		List <Job_applicant_professional_infoDTO> job_applicant_professional_infos = new ArrayList<Job_applicant_professional_infoDTO>(this.mapOfJob_applicant_professional_infoDTOToiD.values());
		return job_applicant_professional_infos;
	}
	
	
	public Job_applicant_professional_infoDTO getJob_applicant_professional_infoDTOByID( long ID){
		return mapOfJob_applicant_professional_infoDTOToiD.get(ID);
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOByjob_applicant_id(long job_applicant_id) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOTojobApplicantId.getOrDefault(job_applicant_id,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOBygovt_service_start_date(long govt_service_start_date) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOTogovtServiceStartDate.getOrDefault(govt_service_start_date,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOBygazetted_date(long gazetted_date) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOTogazettedDate.getOrDefault(gazetted_date,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOByencardment_date(long encardment_date) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOToencardmentDate.getOrDefault(encardment_date,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOByemployee_cadre_id(long employee_cadre_id) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOToemployeeCadreId.getOrDefault(employee_cadre_id,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOByemployee_batches_id(long employee_batches_id) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOToemployeeBatchesId.getOrDefault(employee_batches_id,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOBydesignation(String designation) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOTodesignation.getOrDefault(designation,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOBydepartment(String department) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOTodepartment.getOrDefault(department,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOBylpr_date(long lpr_date) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOTolprDate.getOrDefault(lpr_date,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOByis_govt_job(int is_govt_job) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOToisGovtJob.getOrDefault(is_govt_job,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOByname_of_employee(String name_of_employee) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOTonameOfEmployee.getOrDefault(name_of_employee,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOByemployee_address(String employee_address) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOToemployeeAddress.getOrDefault(employee_address,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOByemployee_employment_type(int employee_employment_type) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOToemployeeEmploymentType.getOrDefault(employee_employment_type,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOByserving_from(long serving_from) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOToservingFrom.getOrDefault(serving_from,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOByserving_to(long serving_to) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOToservingTo.getOrDefault(serving_to,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOByis_currently_working(int is_currently_working) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOToisCurrentlyWorking.getOrDefault(is_currently_working,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOByjob_responsibility(String job_responsibility) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOTojobResponsibility.getOrDefault(job_responsibility,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOByemployee_work_area_type(int employee_work_area_type) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOToemployeeWorkAreaType.getOrDefault(employee_work_area_type,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOBykey_acheivement(String key_acheivement) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOTokeyAcheivement.getOrDefault(key_acheivement,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOBytotal_experience(long total_experience) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOTototalExperience.getOrDefault(total_experience,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOByservice_type(int service_type) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOToserviceType.getOrDefault(service_type,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOBysupervisor_name(String supervisor_name) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOTosupervisorName.getOrDefault(supervisor_name,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOBysupervisor_mobile(String supervisor_mobile) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOTosupervisorMobile.getOrDefault(supervisor_mobile,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOBysupervisor_email(String supervisor_email) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOTosupervisorEmail.getOrDefault(supervisor_email,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOByfilesDropzone(long filesDropzone) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOTofilesDropzone.getOrDefault(filesDropzone,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOByinserted_by(long inserted_by) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOToinsertedBy.getOrDefault(inserted_by,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOBymodified_by(long modified_by) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOByjob_cat(int job_cat) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOTojobCat.getOrDefault(job_cat,new HashSet<>()));
	}
	
	
	public List<Job_applicant_professional_infoDTO> getJob_applicant_professional_infoDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfJob_applicant_professional_infoDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "job_applicant_professional_info";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


