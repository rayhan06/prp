package job_applicant_professional_info;

import util.CommonDTO;


public class Job_applicant_professional_infoSummaryDTO extends CommonDTO
{
	public long jobId = 0;
	public long jobApplicantId = 0;
	public long govtServiceStartDate = 0;
	public long gazettedDate = 0;
	public long encardmentDate = 0;
	public long employeeCadreId = 0;
	public long employeeBatchesId = 0;
    public String designation = "";
    public String department = "";
	public long lprDate = 0;
	public int isGovtJob = 0;
    public String nameOfEmployee = "";
    public String employeeAddress = "";
	public int employeeEmploymentType = 0;
	public long servingFrom = 0;
	public long servingTo = 0;
	public int isCurrentlyWorking = 0;
    public String jobResponsibility = "";
	public int employeeWorkAreaType = 0;
    public String keyAcheivement = "";
	public long totalExperience = 0;
	public int serviceType = 0;
    public String supervisorName = "";
    public String supervisorMobile = "";
    public String supervisorEmail = "";
	public long filesDropzone = 0;
	public long insertionDate = 0;
	public long insertedBy = 0;
	public long modifiedBy = 0;
	public int jobApplicantCandidateCat = 0;
	
    @Override
	public String toString() {
            return "$Job_applicant_professional_infoDTO[" +
            " iD = " + iD +
            " jobApplicantId = " + jobApplicantId +
            " govtServiceStartDate = " + govtServiceStartDate +
            " gazettedDate = " + gazettedDate +
            " encardmentDate = " + encardmentDate +
            " employeeCadreId = " + employeeCadreId +
            " employeeBatchesId = " + employeeBatchesId +
            " designation = " + designation +
            " department = " + department +
            " lprDate = " + lprDate +
            " isGovtJob = " + isGovtJob +
            " nameOfEmployee = " + nameOfEmployee +
            " employeeAddress = " + employeeAddress +
            " employeeEmploymentType = " + employeeEmploymentType +
            " servingFrom = " + servingFrom +
            " servingTo = " + servingTo +
            " isCurrentlyWorking = " + isCurrentlyWorking +
            " jobResponsibility = " + jobResponsibility +
            " employeeWorkAreaType = " + employeeWorkAreaType +
            " keyAcheivement = " + keyAcheivement +
            " totalExperience = " + totalExperience +
            " serviceType = " + serviceType +
            " supervisorName = " + supervisorName +
            " supervisorMobile = " + supervisorMobile +
            " supervisorEmail = " + supervisorEmail +
            " filesDropzone = " + filesDropzone +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " jobCat = " + jobCat +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}