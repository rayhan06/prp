package recruitment_marks_committee;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import recruitment_job_description.Recruitment_job_descriptionDTO;
import recruitment_job_description.Recruitment_job_descriptionRepository;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;
import util.UtilCharacter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Recruitment_marks_committeeDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Recruitment_marks_committeeDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Recruitment_marks_committeeMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"name",
			"recruitment_job_description_ids",
			"search_column",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Recruitment_marks_committeeDAO()
	{
		this("recruitment_marks_committee");		
	}
	
	public void setSearchColumn(Recruitment_marks_committeeDTO recruitment_marks_committeeDTO)
	{
		recruitment_marks_committeeDTO.searchColumn = "";
		recruitment_marks_committeeDTO.searchColumn += recruitment_marks_committeeDTO.name + " ";
		recruitment_marks_committeeDTO.searchColumn += recruitment_marks_committeeDTO.recruitmentJobDescriptionIds + " ";
		recruitment_marks_committeeDTO.searchColumn += recruitment_marks_committeeDTO.insertedBy + " ";
		recruitment_marks_committeeDTO.searchColumn += recruitment_marks_committeeDTO.modifiedBy + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Recruitment_marks_committeeDTO recruitment_marks_committeeDTO = (Recruitment_marks_committeeDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(recruitment_marks_committeeDTO);
		if(isInsert)
		{
			ps.setObject(index++,recruitment_marks_committeeDTO.iD);
		}
		ps.setObject(index++,recruitment_marks_committeeDTO.name);
		ps.setObject(index++,recruitment_marks_committeeDTO.recruitmentJobDescriptionIds);
		ps.setObject(index++,recruitment_marks_committeeDTO.searchColumn);
		ps.setObject(index++,recruitment_marks_committeeDTO.insertionDate);
		ps.setObject(index++,recruitment_marks_committeeDTO.insertedBy);
		ps.setObject(index++,recruitment_marks_committeeDTO.modifiedBy);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Recruitment_marks_committeeDTO recruitment_marks_committeeDTO, ResultSet rs) throws SQLException
	{

	}


	public Recruitment_marks_committeeDTO build(ResultSet rs)
	{
		try
		{
			Recruitment_marks_committeeDTO recruitment_marks_committeeDTO = new Recruitment_marks_committeeDTO();
			recruitment_marks_committeeDTO.iD = rs.getLong("ID");
			recruitment_marks_committeeDTO.name = rs.getString("name");
			recruitment_marks_committeeDTO.recruitmentJobDescriptionIds = rs.getString("recruitment_job_description_ids");
			recruitment_marks_committeeDTO.searchColumn = rs.getString("search_column");
			recruitment_marks_committeeDTO.insertionDate = rs.getLong("insertion_date");
			recruitment_marks_committeeDTO.insertedBy = rs.getString("inserted_by");
			recruitment_marks_committeeDTO.modifiedBy = rs.getString("modified_by");
			recruitment_marks_committeeDTO.isDeleted = rs.getInt("isDeleted");
			recruitment_marks_committeeDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return recruitment_marks_committeeDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
		
	

	//need another getter for repository
	public Recruitment_marks_committeeDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		return 	ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
	}
	
	
	
	
	public List<Recruitment_marks_committeeDTO> getDTOs(Collection recordIDs){
		if(recordIDs.isEmpty()){
			return new ArrayList<>();
		}
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+= " ? ";
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	
	}
	
	

	
	
	
	//add repository
	public List<Recruitment_marks_committeeDTO> getAllRecruitment_marks_committee (boolean isFirstReload)
    {
		List<Recruitment_marks_committeeDTO> recruitment_marks_committeeDTOList = new ArrayList<>();

		String sql = "SELECT * FROM recruitment_marks_committee";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by recruitment_marks_committee.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }

	

	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ? ";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name")
//						|| str.equals("recruitment_job_description_ids")
//						|| str.equals("insertion_date_start")
//						|| str.equals("insertion_date_end")
//						|| str.equals("inserted_by")
//						|| str.equals("modified_by")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name"))
					{
						AllFieldSql += "" + tableName + ".name like ? " ;
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
//					else if(str.equals("recruitment_job_description_ids"))
//					{
//						AllFieldSql += "" + tableName + ".recruitment_job_description_ids like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("insertion_date_start"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("insertion_date_end"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("inserted_by"))
//					{
//						AllFieldSql += "" + tableName + ".inserted_by like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("modified_by"))
//					{
//						AllFieldSql += "" + tableName + ".modified_by like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	


    public static String getJobDetails(Recruitment_marks_committeeDTO dto, String Language){
		String details = "";
		String Value;

		String[] jobIds = dto.recruitmentJobDescriptionIds.split(",");
		int count = 0;

		for(String x : jobIds) {
			Value = Jsoup.clean(x, Whitelist.simpleText());
			if (Value != null && !Value.equalsIgnoreCase("")) {
				Recruitment_job_descriptionDTO jobDescriptionDTO =
						Recruitment_job_descriptionRepository.getInstance().
								getRecruitment_job_descriptionDTOByID(Long.parseLong(Value));
				if(count != 0){
					details += ", ";
				}

				details += UtilCharacter.getDataByLanguage
						(Language, jobDescriptionDTO.jobTitleBn, jobDescriptionDTO.jobTitleEn);

				count++;
			}
		}

		return details;
	}
				
}
	