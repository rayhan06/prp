package recruitment_marks_committee;
import java.util.*; 
import util.*; 


public class Recruitment_marks_committeeDTO extends CommonDTO
{

    public String name = "";
    public String recruitmentJobDescriptionIds = "";
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Recruitment_marks_committeeDTO[" +
            " iD = " + iD +
            " name = " + name +
            " recruitmentJobDescriptionIds = " + recruitmentJobDescriptionIds +
            " searchColumn = " + searchColumn +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}