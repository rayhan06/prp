package recruitment_marks_committee;

import recruitment_marks_committee_members.Recruitment_marks_committee_membersDTO;
import util.CommonDTO;

import java.util.ArrayList;
import java.util.List;


public class CommitteeMemberWithStatusDTO
{

    public boolean status = false;
    public long committeeId = 0;
    public List<Recruitment_marks_committee_membersDTO> marks_committee_membersDTOS = new ArrayList<>();

    @Override
	public String toString() {
            return "CommitteeMemberWithStatusDTO[" +
            " status = " + status +
            " marks_committee_membersDTOS = " + marks_committee_membersDTOS +
            "]";
    }

}