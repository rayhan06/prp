package recruitment_marks_committee;

import com.google.gson.Gson;
import common.ApiResponse;
import employee_assign.EmployeeSearchModel;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import job_applicant_application.Job_applicant_applicationServlet;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import recruitment_job_description.Recruitment_job_descriptionDAO;
import recruitment_job_description.Recruitment_job_descriptionDTO;
import recruitment_job_description.Recruitment_job_descriptionRepository;
import recruitment_marks_committee_members.Recruitment_marks_committee_membersDAO;
import recruitment_marks_committee_members.Recruitment_marks_committee_membersDTO;
import recruitment_marks_committee_members.Recruitment_marks_committee_membersRepository;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import sms.SmsService;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Random;




/**
 * Servlet implementation class Recruitment_marks_committeeServlet
 */
@WebServlet("/Recruitment_marks_committeeServlet")
@MultipartConfig
public class Recruitment_marks_committeeServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Recruitment_marks_committeeServlet.class);

    String tableName = "recruitment_marks_committee";

	Recruitment_marks_committeeDAO recruitment_marks_committeeDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Recruitment_marks_committeeServlet()
	{
        super();
    	try
    	{
			recruitment_marks_committeeDAO = new Recruitment_marks_committeeDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(recruitment_marks_committeeDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_MARKS_COMMITTEE_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_MARKS_COMMITTEE_UPDATE))
				{
					getRecruitment_marks_committee(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_MARKS_COMMITTEE_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchRecruitment_marks_committee(request, response, isPermanentTable, filter);
						}
						else
						{
							searchRecruitment_marks_committee(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchRecruitment_marks_committee(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_MARKS_COMMITTEE_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_MARKS_COMMITTEE_ADD))
				{
					System.out.println("going to  addRecruitment_marks_committee ");
					addRecruitment_marks_committee(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addRecruitment_marks_committee ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_MARKS_COMMITTEE_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addRecruitment_marks_committee ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_MARKS_COMMITTEE_UPDATE))
				{
					addRecruitment_marks_committee(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_MARKS_COMMITTEE_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_MARKS_COMMITTEE_SEARCH))
				{
					searchRecruitment_marks_committee(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			else if(actionType.equals("sendOTP")){
				Long committeeId = Long.parseLong(request.getParameter("committeeId"));
				Recruitment_marks_committee_membersDAO memberDao = new Recruitment_marks_committee_membersDAO();
				List<Recruitment_marks_committee_membersDTO> membersDTOS = Recruitment_marks_committee_membersRepository.getInstance().
						getRecruitment_marks_committee_membersDTOByrecruitment_marks_committee_id(committeeId);
//						memberDao.
//								getAllRecruitment_marks_committee_membersByCommitteeId(committeeId);

				for(Recruitment_marks_committee_membersDTO dto: membersDTOS){
					dto.otp = OTP(6);
					dto.otpGenerationTime = System.currentTimeMillis();
					memberDao.update(dto);
					String msg = String.format("Your One-Time-Password is " + dto.otp );

					SmsService.send(dto.mobile, msg);

					String mail = "";
					Employee_recordsDTO employeeRecordsDTO =
							Employee_recordsRepository.getInstance().getById(dto.employeeRecordId);
					if(employeeRecordsDTO != null){
						mail = employeeRecordsDTO.personalEml;
					}

					new Job_applicant_applicationServlet().sendMail(mail, msg, "Parliament Recruitment");

				}
			}

			else if(actionType.equals("checkOTP")){
				int length = Integer.parseInt(request.getParameter("length"));

				boolean status = true;
				String param ;

				long currentTime = System.currentTimeMillis();

				Recruitment_marks_committee_membersDAO membersDAO =
						new Recruitment_marks_committee_membersDAO();

				for(int i = 1; i <= length; i++){
					param = "member_id_" + i;
					long memberId = Long.parseLong(request.getParameter(param));
					param = "otp_" + i;
					String receivedOTP = request.getParameter(param);
					Recruitment_marks_committee_membersDTO membersDTO = membersDAO.getDTOByID(memberId);
					long allowedTime = membersDTO.otpGenerationTime + 300000;
					if((!membersDTO.otp.equalsIgnoreCase(receivedOTP)) || currentTime > allowedTime  ){
						status = false;
						break;
					}
				}

//				System.out.println("###");
//				System.out.println(status);
//				System.out.println("###");

				CommitteeMemberWithStatusDTO statusDTO = new CommitteeMemberWithStatusDTO();
				statusDTO.status = status;

				PrintWriter out = response.getWriter();
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");

				String encoded = this.gson.toJson(statusDTO);
				System.out.println("json encoded data = " + encoded);
				out.print(encoded);
				out.flush();


			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private String OTP(int len)
	{
		System.out.println("Generating OTP using random() : ");
		System.out.print("You OTP is : ");

		// Using numeric values
		String numbers = "0123456789";

		// Using random method
		Random rndm_method = new Random();

		char[] otp = new char[len];

		for (int i = 0; i < len; i++)
		{
			// Use of charAt() method : to get character value
			// Use of nextInt() as it is scanning the value as int
			otp[i] =
					numbers.charAt(rndm_method.nextInt(numbers.length()));
		}
		return new String(otp);
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Recruitment_marks_committeeDTO recruitment_marks_committeeDTO = Recruitment_marks_committeeRepository.getInstance().
					getRecruitment_marks_committeeDTOByID(Long.parseLong(request.getParameter("ID")));
//					(Recruitment_marks_committeeDTO)recruitment_marks_committeeDAO.getDTOByID
//							(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(recruitment_marks_committeeDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addRecruitment_marks_committee(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		ApiResponse apiResponse;

		String prevJobIds = "";
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addRecruitment_marks_committee");
			String path = getServletContext().getRealPath("/img2/");
			Recruitment_marks_committeeDTO recruitment_marks_committeeDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				recruitment_marks_committeeDTO = new Recruitment_marks_committeeDTO();
			}
			else
			{
				recruitment_marks_committeeDTO = Recruitment_marks_committeeRepository.getInstance().
						getRecruitment_marks_committeeDTOByID(Long.parseLong(request.getParameter("iD")));
//						(Recruitment_marks_committeeDTO)recruitment_marks_committeeDAO.getDTOByID
//								(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("name");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("name = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				recruitment_marks_committeeDTO.name = (Value);
			}
			else
			{
				throw new Exception("Invalid name");
			}


			Value = request.getParameter("jobs");

			if (Value != null) {
				Value = Jsoup.clean(Value, Whitelist.simpleText());
			}
			System.out.println("jobs = " + Value);
			if (Value != null ) {
				prevJobIds = recruitment_marks_committeeDTO.recruitmentJobDescriptionIds;
				recruitment_marks_committeeDTO.recruitmentJobDescriptionIds = Value;
			} else {
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}



//			Value = request.getParameter("recruitmentJobDescriptionIds");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("recruitmentJobDescriptionIds = " + Value);
//			if(Value != null)
//			{
//				recruitment_marks_committeeDTO.recruitmentJobDescriptionIds = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}

			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				recruitment_marks_committeeDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				recruitment_marks_committeeDTO.insertionDate = c.getTimeInMillis();
			}


			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null)
			{
				recruitment_marks_committeeDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null)
			{
				recruitment_marks_committeeDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addRecruitment_marks_committee dto = " + recruitment_marks_committeeDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				recruitment_marks_committeeDAO.setIsDeleted(recruitment_marks_committeeDTO.iD, CommonDTO.OUTDATED);
				returnedID = recruitment_marks_committeeDAO.add(recruitment_marks_committeeDTO);
				recruitment_marks_committeeDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = recruitment_marks_committeeDAO.manageWriteOperations(recruitment_marks_committeeDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = recruitment_marks_committeeDAO.manageWriteOperations(recruitment_marks_committeeDTO, SessionConstants.UPDATE, -1, userDTO);
			}


			Value = request.getParameter("memberId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("memberId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{



				EmployeeSearchModel[] models = gson.fromJson(Value, EmployeeSearchModel[].class);
				Recruitment_marks_committee_membersDAO membersDAO = new Recruitment_marks_committee_membersDAO();
//				Appointment_letter_onulipiDAO onulipiDAO = new Appointment_letter_onulipiDAO();
//
//
//				membersDAO.getAllRecruitment_marks_committee_membersByCommitteeId(returnedID)
				Recruitment_marks_committee_membersRepository.getInstance().
						getRecruitment_marks_committee_membersDTOByrecruitment_marks_committee_id(returnedID)
						.forEach(i -> {
							try {
								membersDAO.delete(i.iD);
							} catch (Exception e) {
								e.printStackTrace();
							}
						});


				for(EmployeeSearchModel model: models){
					Recruitment_marks_committee_membersDTO membersDTO = new Recruitment_marks_committee_membersDTO();
					membersDTO.recruitmentMarksCommitteeId = returnedID;
					membersDTO.employeeRecordId = model.employeeRecordId;
					membersDTO.unitId = model.officeUnitId;
					membersDTO.postId = model.organogramId;
					membersDTO.employeeRecordName = model.employeeNameEn;
					membersDTO.employeeRecordNameBn = model.employeeNameBn;
					membersDTO.postName = model.organogramNameEn;
					membersDTO.postNameBn = model.organogramNameBn;
					membersDTO.unitName = model.officeUnitNameEn;
					membersDTO.unitNameBn = model.officeUnitNameBn;
					membersDTO.mobile = model.phoneNumber;
					membersDAO.add(membersDTO);
				}



			}
			else
			{
				throw new Exception(" Invalid committee member");
			}

			// job related data add

			Recruitment_job_descriptionDAO jobDescriptionDAO = new Recruitment_job_descriptionDAO();

			String[] prevJobIdArray = prevJobIds.split(",");
			for(String x : prevJobIdArray) {
				Value = Jsoup.clean(x, Whitelist.simpleText());
				if (Value != null && !Value.equalsIgnoreCase("")) {
					Recruitment_job_descriptionDTO jobDescriptionDTO =
							Recruitment_job_descriptionRepository.getInstance()
									.getRecruitment_job_descriptionDTOByID(Long.parseLong(Value));
					jobDescriptionDTO.marks_committee_id = 0;
					jobDescriptionDAO.update(jobDescriptionDTO);
				}
			}

			String[] jobIds = recruitment_marks_committeeDTO.
					recruitmentJobDescriptionIds.split(",");

			for(String x : jobIds) {
				Value = Jsoup.clean(x, Whitelist.simpleText());
				if (Value != null && !Value.equalsIgnoreCase("")) {
					Recruitment_job_descriptionDTO jobDescriptionDTO =
							Recruitment_job_descriptionRepository.getInstance()
									.getRecruitment_job_descriptionDTOByID(Long.parseLong(Value));
					jobDescriptionDTO.marks_committee_id = returnedID;
					jobDescriptionDAO.update(jobDescriptionDTO);
				}
			}


			apiResponse = ApiResponse.makeSuccessResponse("Recruitment_marks_committeeServlet?actionType=search");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			apiResponse = ApiResponse.makeErrorResponse(e.getMessage());
		}

		PrintWriter pw = response.getWriter();
		pw.write(apiResponse.getJSONString());
		pw.flush();
		pw.close();
	}









	private void getRecruitment_marks_committee(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getRecruitment_marks_committee");
		Recruitment_marks_committeeDTO recruitment_marks_committeeDTO = null;
		try
		{
			recruitment_marks_committeeDTO = Recruitment_marks_committeeRepository.getInstance().
					getRecruitment_marks_committeeDTOByID(id);
//					(Recruitment_marks_committeeDTO)recruitment_marks_committeeDAO.getDTOByID(id);
			request.setAttribute("ID", recruitment_marks_committeeDTO.iD);
			request.setAttribute("recruitment_marks_committeeDTO",recruitment_marks_committeeDTO);
			request.setAttribute("recruitment_marks_committeeDAO",recruitment_marks_committeeDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "recruitment_marks_committee/recruitment_marks_committeeInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "recruitment_marks_committee/recruitment_marks_committeeSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "recruitment_marks_committee/recruitment_marks_committeeEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "recruitment_marks_committee/recruitment_marks_committeeEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getRecruitment_marks_committee(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getRecruitment_marks_committee(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchRecruitment_marks_committee(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchRecruitment_marks_committee 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_RECRUITMENT_MARKS_COMMITTEE,
			request,
			recruitment_marks_committeeDAO,
			SessionConstants.VIEW_RECRUITMENT_MARKS_COMMITTEE,
			SessionConstants.SEARCH_RECRUITMENT_MARKS_COMMITTEE,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("recruitment_marks_committeeDAO",recruitment_marks_committeeDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to recruitment_marks_committee/recruitment_marks_committeeApproval.jsp");
	        	rd = request.getRequestDispatcher("recruitment_marks_committee/recruitment_marks_committeeApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to recruitment_marks_committee/recruitment_marks_committeeApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("recruitment_marks_committee/recruitment_marks_committeeApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to recruitment_marks_committee/recruitment_marks_committeeSearch.jsp");
	        	rd = request.getRequestDispatcher("recruitment_marks_committee/recruitment_marks_committeeSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to recruitment_marks_committee/recruitment_marks_committeeSearchForm.jsp");
	        	rd = request.getRequestDispatcher("recruitment_marks_committee/recruitment_marks_committeeSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

