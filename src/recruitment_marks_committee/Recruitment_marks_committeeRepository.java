package recruitment_marks_committee;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;
import vm_route.VmRouteStoppageDTO;


public class Recruitment_marks_committeeRepository implements Repository {
	Recruitment_marks_committeeDAO recruitment_marks_committeeDAO = null;
	Gson gson = new Gson();
	
	public void setDAO(Recruitment_marks_committeeDAO recruitment_marks_committeeDAO)
	{
		this.recruitment_marks_committeeDAO = recruitment_marks_committeeDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Recruitment_marks_committeeRepository.class);
	Map<Long, Recruitment_marks_committeeDTO>mapOfRecruitment_marks_committeeDTOToiD;


	static Recruitment_marks_committeeRepository instance = null;  
	private Recruitment_marks_committeeRepository(){
		mapOfRecruitment_marks_committeeDTOToiD = new ConcurrentHashMap<>();
		setDAO(new Recruitment_marks_committeeDAO());

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Recruitment_marks_committeeRepository getInstance(){
		if (instance == null){
			instance = new Recruitment_marks_committeeRepository();
		}
		return instance;
	}

	public Recruitment_marks_committeeDTO clone(Recruitment_marks_committeeDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Recruitment_marks_committeeDTO.class);
	}

	public List<Recruitment_marks_committeeDTO> clone(List<Recruitment_marks_committeeDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public void reload(boolean reloadAll){
		if(recruitment_marks_committeeDAO == null)
		{
			return;
		}
		try {
			List<Recruitment_marks_committeeDTO> recruitment_marks_committeeDTOs = recruitment_marks_committeeDAO.getAllRecruitment_marks_committee(reloadAll);
			for(Recruitment_marks_committeeDTO recruitment_marks_committeeDTO : recruitment_marks_committeeDTOs) {
				Recruitment_marks_committeeDTO oldRecruitment_marks_committeeDTO =
						getRecruitment_marks_committeeDTOByIDWithoutClone(recruitment_marks_committeeDTO.iD);
				if( oldRecruitment_marks_committeeDTO != null ) {
					mapOfRecruitment_marks_committeeDTOToiD.remove(oldRecruitment_marks_committeeDTO.iD);
					
					
				}
				if(recruitment_marks_committeeDTO.isDeleted == 0) 
				{
					
					mapOfRecruitment_marks_committeeDTOToiD.put(recruitment_marks_committeeDTO.iD, recruitment_marks_committeeDTO);

				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Recruitment_marks_committeeDTO> getRecruitment_marks_committeeList() {
		List <Recruitment_marks_committeeDTO> recruitment_marks_committees = new ArrayList<Recruitment_marks_committeeDTO>(this.mapOfRecruitment_marks_committeeDTOToiD.values());
		return clone(recruitment_marks_committees);
	}
	
	
	public Recruitment_marks_committeeDTO getRecruitment_marks_committeeDTOByIDWithoutClone( long ID){
		return mapOfRecruitment_marks_committeeDTOToiD.get(ID);
	}

	public Recruitment_marks_committeeDTO getRecruitment_marks_committeeDTOByID( long ID){
		return clone(mapOfRecruitment_marks_committeeDTOToiD.get(ID));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "recruitment_marks_committee";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


