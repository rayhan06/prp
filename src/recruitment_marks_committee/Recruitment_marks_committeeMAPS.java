package recruitment_marks_committee;
import java.util.*; 
import util.*;


public class Recruitment_marks_committeeMAPS extends CommonMaps
{	
	public Recruitment_marks_committeeMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("name".toLowerCase(), "name".toLowerCase());
		java_DTO_map.put("recruitmentJobDescriptionIds".toLowerCase(), "recruitmentJobDescriptionIds".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("name".toLowerCase(), "name".toLowerCase());
		java_SQL_map.put("recruitment_job_description_ids".toLowerCase(), "recruitmentJobDescriptionIds".toLowerCase());
		java_SQL_map.put("inserted_by".toLowerCase(), "insertedBy".toLowerCase());
		java_SQL_map.put("modified_by".toLowerCase(), "modifiedBy".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Name".toLowerCase(), "name".toLowerCase());
		java_Text_map.put("Recruitment Job Description Ids".toLowerCase(), "recruitmentJobDescriptionIds".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}