package payroll_allowance_configuration;

import com.google.gson.Gson;
import common.BaseServlet;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


@WebServlet("/Payroll_allowance_configurationServlet")
@MultipartConfig
public class Payroll_allowance_configurationServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static final long EMPLOYEE_REMUNERATION_ECONOMIC_CODE = 6L;

    public Payroll_allowance_configurationDTO buildDTO(UserInput userInput) {
        Payroll_allowance_configurationDTO allowanceConfigDTO = new Payroll_allowance_configurationDTO();
        allowanceConfigDTO.insertedBy = allowanceConfigDTO.modifiedBy = userInput.modifiedBy;
        allowanceConfigDTO.insertionTime = allowanceConfigDTO.lastModificationTime = userInput.modificationTime;
        allowanceConfigDTO.employmentCat = userInput.employmentCat;
        allowanceConfigDTO.economicSubCodeId = userInput.economicSubCodeId;
        return allowanceConfigDTO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        List<Payroll_allowance_configurationDTO> newDTOs = new ArrayList<>();
        UserInput userInput = new UserInput();
        userInput.employmentCat = Integer.parseInt(Jsoup.clean(
                request.getParameter("employmentCat"), Whitelist.simpleText())
        );
        userInput.modifiedBy = userDTO.ID;
        userInput.modificationTime = System.currentTimeMillis();
        long[] economicSubCodeIds = new Gson().fromJson(request.getParameter("economicSubCodes"), long[].class);
        for (long economicSubCodeId : economicSubCodeIds) {
            userInput.economicSubCodeId = economicSubCodeId;
            newDTOs.add(buildDTO(userInput));
        }
        Payroll_allowance_configurationDAO.getInstance().updateWithNewList(
                newDTOs, userInput.employmentCat, userInput.modifiedBy
        );
        return null;
    }

    @Override
    public String getTableName() {
        return Payroll_allowance_configurationDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Payroll_allowance_configurationServlet";
    }

    @Override
    public Payroll_allowance_configurationDAO getCommonDAOService() {
        return Payroll_allowance_configurationDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PAYROLL_ALLOWANCE_CONFIGURATION_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PAYROLL_ALLOWANCE_CONFIGURATION_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PAYROLL_ALLOWANCE_CONFIGURATION_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Payroll_allowance_configurationServlet.class;
    }

    static class UserInput {
        int employmentCat;
        long economicSubCodeId;
        long modifiedBy;
        long modificationTime;
    }
}