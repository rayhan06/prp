package payroll_allowance_configuration;

import economic_sub_code.Economic_sub_codeDTO;
import economic_sub_code.Economic_sub_codeRepository;
import pb.OptionDTO;
import util.CommonDTO;

import static util.StringUtils.convertToBanNumber;

public class Payroll_allowance_configurationDTO extends CommonDTO {
    public int employmentCat = -1;
    public long economicSubCodeId = -1;
    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;

    public OptionDTO getOptionDTO() {
        Economic_sub_codeDTO subCodeDTO = Economic_sub_codeRepository.getInstance().getDTOByID(economicSubCodeId);
        return new OptionDTO(
                subCodeDTO.code.concat(" - ").concat(subCodeDTO.descriptionEn),
                convertToBanNumber(subCodeDTO.code).concat(" - ").concat(subCodeDTO.descriptionBn),
                String.valueOf(iD)
        );
    }

    @Override
    public String toString() {
        return "$Payroll_allowance_configurationDTO[" +
                " iD = " + iD +
                " employmentCat = " + employmentCat +
                " economicSubCodeId = " + economicSubCodeId +
                " isDeleted = " + isDeleted +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }
}