package payroll_allowance_configuration;

import common.CommonDAOService;
import employee_records.Employee_recordsRepository;
import office_unit_organograms.SameDesignationGroup;
import org.apache.log4j.Logger;
import payroll_allowance_lookup.Payroll_allowance_lookupDAO;
import payroll_allowance_lookup.Payroll_allowance_lookupServlet;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class Payroll_allowance_configurationDAO implements CommonDAOService<Payroll_allowance_configurationDTO> {
    private static final Logger logger = Logger.getLogger(Payroll_allowance_configurationDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (employment_cat,economic_sub_code_id,modified_by,lastModificationTime,"
                    .concat("isDeleted,inserted_by,insertion_time,ID) VALUES(?,?,?,?,?,?,?,?)");

    private static final String updateQuery =
            "UPDATE {tableName} SET employment_cat=?,economic_sub_code_id=?,modified_by=?,lastModificationTime=? WHERE ID=?";

    private static final String getByEmploymentCat =
            "SELECT * from payroll_allowance_configuration WHERE employment_cat = ? AND isDeleted = 0";

    private final Map<String, String> searchMap = new HashMap<>();

    private Payroll_allowance_configurationDAO() {
        searchMap.put("employment_cat", " and (employment_cat = ?)");
        searchMap.put("economic_sub_code_id", " and (economic_sub_code_id = ?)");
    }

    private static class LazyLoader {
        static final Payroll_allowance_configurationDAO INSTANCE = new Payroll_allowance_configurationDAO();
    }

    public static Payroll_allowance_configurationDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Payroll_allowance_configurationDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setInt(++index, dto.employmentCat);
        ps.setLong(++index, dto.economicSubCodeId);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setInt(++index, 0 /* isDeleted */);
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Payroll_allowance_configurationDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Payroll_allowance_configurationDTO dto = new Payroll_allowance_configurationDTO();
            dto.iD = rs.getLong("ID");
            dto.employmentCat = rs.getInt("employment_cat");
            dto.economicSubCodeId = rs.getLong("economic_sub_code_id");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Payroll_allowance_configurationDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "payroll_allowance_configuration";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Payroll_allowance_configurationDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Payroll_allowance_configurationDTO) commonDTO, updateQuery, false);
    }

    public void updateWithNewList(List<Payroll_allowance_configurationDTO> newList, int employmentCat, long requestBy) throws Exception {
        if (newList == null) newList = new ArrayList<>();
        Set<Long> newSubCodes = newList.stream()
                                       .map(dto -> dto.economicSubCodeId)
                                       .collect(Collectors.toSet());
        List<Payroll_allowance_configurationDTO> oldList = getDTOs(
                getByEmploymentCat,
                Collections.singletonList(employmentCat)
        );
        deleteMissingCodesFromConfigAndLookupTable(oldList, newSubCodes, requestBy);
        addNewCodesInConfigAndLookupTable(oldList, newList, employmentCat, requestBy);
    }

    private void deleteMissingCodesFromConfigAndLookupTable(List<Payroll_allowance_configurationDTO> oldList,
                                                            Set<Long> newSubCodes, long requestBy) {
        List<Long> idsToDelete = oldList.stream()
                                        .filter(dto -> !newSubCodes.contains(dto.economicSubCodeId))
                                        .map(dto -> dto.iD)
                                        .collect(Collectors.toList());
        long requestTime = System.currentTimeMillis();
        if (!idsToDelete.isEmpty()) {
            delete(requestBy, idsToDelete);
            Payroll_allowance_lookupDAO.getInstance().deleteByAllowanceConfigId(idsToDelete, requestBy, requestTime);
        }
    }

    private void addNewCodesInConfigAndLookupTable(List<Payroll_allowance_configurationDTO> oldList,
                                                   List<Payroll_allowance_configurationDTO> newList,
                                                   int employmentCat, long requestBy) throws Exception {
        Set<Long> oldSubCodes = oldList.stream()
                                       .map(dto -> dto.economicSubCodeId)
                                       .collect(Collectors.toSet());

        Payroll_allowance_lookupServlet.UserInput lookupUserInput = new Payroll_allowance_lookupServlet.UserInput();
        lookupUserInput.lastModificationTime = System.currentTimeMillis();
        lookupUserInput.modifierId = requestBy;
        lookupUserInput.amount = 0;
        lookupUserInput.employmentCat = employmentCat;
        Set<SameDesignationGroup> designations = Employee_recordsRepository.getInstance()
                                                                           .getSameDesignationGroups(employmentCat);

        for (Payroll_allowance_configurationDTO dto : newList) {
            if (!oldSubCodes.contains(dto.economicSubCodeId)) {
                add(dto);
                for (SameDesignationGroup designation : designations) {
                    lookupUserInput.configId = dto.iD;
                    lookupUserInput.organogramId = designation.organogramId;
                    lookupUserInput.organogramKey = designation.organogramKey;
                    Payroll_allowance_lookupDAO.getInstance().addLookupDTO(lookupUserInput, true);
                }
            }
        }
    }
}