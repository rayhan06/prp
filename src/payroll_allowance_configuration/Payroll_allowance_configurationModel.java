package payroll_allowance_configuration;

import economic_sub_code.Economic_sub_codeDTO;
import economic_sub_code.Economic_sub_codeRepository;
import payroll_month_bill_allowance.Payroll_month_bill_allowanceDTO;
import pb.Utils;

public class Payroll_allowance_configurationModel {
    public String configurationId;
    public String name;
    public String code;

    public Payroll_allowance_configurationModel(Payroll_allowance_configurationDTO dto, String language) {
        configurationId = String.valueOf(dto.iD);
        Economic_sub_codeDTO codeDTO = Economic_sub_codeRepository.getInstance().getDTOByID(dto.economicSubCodeId);
        name = language.equalsIgnoreCase("ENGLISH") ? codeDTO.descriptionEn : codeDTO.descriptionBn;
        code = Utils.getDigits(codeDTO.code, language);
    }

    public Payroll_allowance_configurationModel(Payroll_month_bill_allowanceDTO dto, String language) {
        configurationId = String.valueOf(dto.payrollAllowanceConfigurationId);
        Payroll_allowance_configurationDTO payroll_allowance_configurationDTO = Payroll_allowance_configurationRepository.getInstance().getDTODeletedOrNot(dto.payrollAllowanceConfigurationId);
        Economic_sub_codeDTO codeDTO = Economic_sub_codeRepository.getInstance().getDTOByID(payroll_allowance_configurationDTO.economicSubCodeId);
        name = language.equalsIgnoreCase("ENGLISH") ? codeDTO.descriptionEn : codeDTO.descriptionBn;
        code = Utils.getDigits(codeDTO.code, language);
    }
}
