package payroll_allowance_configuration;

import economic_sub_code.EconomicSubCodeModel;
import economic_sub_code.Economic_sub_codeDTO;
import economic_sub_code.Economic_sub_codeRepository;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;
import static util.StringUtils.convertToBanNumber;


public class Payroll_allowance_configurationRepository implements Repository {
    private static final Logger logger = Logger.getLogger(Payroll_allowance_configurationRepository.class);
    private final Payroll_allowance_configurationDAO dao;
    private final Map<Long, Payroll_allowance_configurationDTO> mapById;

    private Payroll_allowance_configurationRepository() {
        dao = Payroll_allowance_configurationDAO.getInstance();
        mapById = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Payroll_allowance_configurationRepository INSTANCE = new Payroll_allowance_configurationRepository();
    }

    public static Payroll_allowance_configurationRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void reload(boolean reloadAll) {
        logger.debug("Payroll_allowance_configurationRepository reload start for, reloadAll : " + reloadAll);
        List<Payroll_allowance_configurationDTO> dtoList = dao.getAllDTOs(reloadAll);
        if (dtoList != null && dtoList.size() > 0) {
            dtoList.stream()
                    .peek(this::removeIfPresent)
                    .filter(dto -> dto.isDeleted == 0)
                    .forEach(dto -> mapById.put(dto.iD, dto));
        }
        logger.debug("Payroll_allowance_configurationRepository reload end for, reloadAll : " + reloadAll);
    }

    private void removeIfPresent(Payroll_allowance_configurationDTO newDTO) {
        if (newDTO == null) return;
        if (mapById.get(newDTO.iD) != null) {
            mapById.remove(newDTO.iD);
        }
    }

    public List<Payroll_allowance_configurationDTO> getAllDTOs() {
        return new ArrayList<>(mapById.values());
    }

    public List<Payroll_allowance_configurationDTO> getDTOsByEmploymentCat(int employmentCat) {
        return getAllDTOs().stream()
                .filter(allowanceConfig -> allowanceConfig.employmentCat == employmentCat)
                .collect(toList());
    }

    public Payroll_allowance_configurationDTO getDTO(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "PACRGDTO")) {
                if (mapById.get(id) == null) {
                    Payroll_allowance_configurationDTO dto = dao.getDTOFromID(id);
                    if (dto != null) {
                        mapById.put(dto.iD, dto);
                    }
                }
            }
        }
        return mapById.get(id);
    }

    public Payroll_allowance_configurationDTO getDTODeletedOrNot(long id) {
        if (mapById.get(id) == null) {
            return dao.getDTOFromIdDeletedOrNot(id);
        }
        return mapById.get(id);
    }

    @Override
    public String getTableName() {
        return "payroll_allowance_configuration";
    }

    public List<EconomicSubCodeModel> getEconomicSubCodeModels(int employmentCat, String language) {
        return getAllDTOs().stream()
                .filter(allowanceConfig -> allowanceConfig.employmentCat == employmentCat)
                .map(allowanceConfig -> allowanceConfig.economicSubCodeId)
                .map(subCodeId -> Economic_sub_codeRepository.getInstance().getDTOByID(subCodeId))
                .map(subCodeDTO -> new EconomicSubCodeModel(subCodeDTO, language))
                .collect(toList());
    }

    public String getAllowanceName(long id, String language) {
        Payroll_allowance_configurationDTO dto = getDTO(id);
        if (dto == null) return "";
        Economic_sub_codeDTO subCodeDTO = Economic_sub_codeRepository.getInstance().getDTOByID(dto.economicSubCodeId);
        return "english".equalsIgnoreCase(language)
                ? subCodeDTO.code.concat(" - ").concat(subCodeDTO.descriptionEn)
                : convertToBanNumber(subCodeDTO.code).concat(" - ").concat(subCodeDTO.descriptionBn);
    }

    public String buildOptions(int employmentCat, Long selectedId, String language) {
        List<OptionDTO> optionDTOList =
                getAllDTOs().stream()
                        .filter(allowanceConfig -> allowanceConfig.employmentCat == employmentCat)
                        .map(Payroll_allowance_configurationDTO::getOptionDTO)
                        .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public List<Payroll_allowance_configurationModel> getActiveModels(int employmentCat, String language) {
        return getAllDTOs().stream()
                .filter(dto -> dto.employmentCat == employmentCat)
                .map(dto -> new Payroll_allowance_configurationModel(dto, language))
                .sorted(Comparator.comparing(dto -> dto.configurationId))
                .collect(Collectors.toList());
    }

    public List<Payroll_allowance_configurationDTO> getActiveDTOs(int employmentCat) {
        return getAllDTOs().stream()
                .filter(dto -> dto.employmentCat == employmentCat)
                .collect(Collectors.toList());
    }

    public Payroll_allowance_configurationDTO getBySubCodeId(long economicSubCodeId) {
        return getAllDTOs().stream()
                .filter(dto -> dto.economicSubCodeId == economicSubCodeId)
                .findAny()
                .orElse(null);
    }
}