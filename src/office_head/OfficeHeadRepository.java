package office_head;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class OfficeHeadRepository implements Repository {
    OfficeHeadDAO employee_recordsDAO = new OfficeHeadDAO();


    static Logger logger = Logger.getLogger(OfficeHeadRepository.class);
    Map<Long, OfficeHeadDTO> mapOfEmployee_recordsDTOToiD;
    Map<String, Set<OfficeHeadDTO>> mapOfEmployee_recordsDTOTonameBng;
    Map<String, Set<OfficeHeadDTO>> mapOfEmployee_recordsDTOTonameEng;
    Map<Integer, Set<OfficeHeadDTO>> mapOfEmployee_recordsDTOToofficeUnitsType;
    Map<Integer, Set<OfficeHeadDTO>> mapOfEmployee_recordsDTOToemployeeOfficesType;


    static OfficeHeadRepository instance = null;

    private OfficeHeadRepository() {
        mapOfEmployee_recordsDTOToiD = new ConcurrentHashMap<>();
        mapOfEmployee_recordsDTOTonameBng = new ConcurrentHashMap<>();
        mapOfEmployee_recordsDTOTonameEng = new ConcurrentHashMap<>();
        mapOfEmployee_recordsDTOToofficeUnitsType = new ConcurrentHashMap<>();
        mapOfEmployee_recordsDTOToemployeeOfficesType = new ConcurrentHashMap<>();

        RepositoryManager.getInstance().addRepository(this);
    }

    public synchronized static OfficeHeadRepository getInstance() {
        if (instance == null) {
            instance = new OfficeHeadRepository();
        }
        return instance;
    }

    public void reload(boolean reloadAll) {
        try {
            List<OfficeHeadDTO> employee_recordsDTOs = employee_recordsDAO.getAllEmployee_records(reloadAll);
            for (OfficeHeadDTO employee_recordsDTO : employee_recordsDTOs) {
                OfficeHeadDTO oldEmployee_recordsDTO = getEmployee_recordsDTOByID(employee_recordsDTO.iD);
                if (oldEmployee_recordsDTO != null) {
                    mapOfEmployee_recordsDTOToiD.remove(oldEmployee_recordsDTO.iD);

                    if (mapOfEmployee_recordsDTOTonameBng.containsKey(oldEmployee_recordsDTO.nameBng)) {
                        mapOfEmployee_recordsDTOTonameBng.get(oldEmployee_recordsDTO.nameBng).remove(oldEmployee_recordsDTO);
                    }
                    if (mapOfEmployee_recordsDTOTonameBng.get(oldEmployee_recordsDTO.nameBng).isEmpty()) {
                        mapOfEmployee_recordsDTOTonameBng.remove(oldEmployee_recordsDTO.nameBng);
                    }

                    if (mapOfEmployee_recordsDTOTonameEng.containsKey(oldEmployee_recordsDTO.nameEng)) {
                        mapOfEmployee_recordsDTOTonameEng.get(oldEmployee_recordsDTO.nameEng).remove(oldEmployee_recordsDTO);
                    }
                    if (mapOfEmployee_recordsDTOTonameEng.get(oldEmployee_recordsDTO.nameEng).isEmpty()) {
                        mapOfEmployee_recordsDTOTonameEng.remove(oldEmployee_recordsDTO.nameEng);
                    }

                    if (mapOfEmployee_recordsDTOToofficeUnitsType.containsKey(oldEmployee_recordsDTO.officeUnitsType)) {
                        mapOfEmployee_recordsDTOToofficeUnitsType.get(oldEmployee_recordsDTO.officeUnitsType).remove(oldEmployee_recordsDTO);
                    }
                    if (mapOfEmployee_recordsDTOToofficeUnitsType.get(oldEmployee_recordsDTO.officeUnitsType).isEmpty()) {
                        mapOfEmployee_recordsDTOToofficeUnitsType.remove(oldEmployee_recordsDTO.officeUnitsType);
                    }

                    if (mapOfEmployee_recordsDTOToemployeeOfficesType.containsKey(oldEmployee_recordsDTO.employeeOfficesType)) {
                        mapOfEmployee_recordsDTOToemployeeOfficesType.get(oldEmployee_recordsDTO.employeeOfficesType).remove(oldEmployee_recordsDTO);
                    }
                    if (mapOfEmployee_recordsDTOToemployeeOfficesType.get(oldEmployee_recordsDTO.employeeOfficesType).isEmpty()) {
                        mapOfEmployee_recordsDTOToemployeeOfficesType.remove(oldEmployee_recordsDTO.employeeOfficesType);
                    }


                }
                if (employee_recordsDTO.isDeleted == false) {

                    mapOfEmployee_recordsDTOToiD.put(employee_recordsDTO.iD, employee_recordsDTO);

                    if (!mapOfEmployee_recordsDTOTonameBng.containsKey(employee_recordsDTO.nameBng)) {
                        mapOfEmployee_recordsDTOTonameBng.put(employee_recordsDTO.nameBng, new HashSet<>());
                    }
                    mapOfEmployee_recordsDTOTonameBng.get(employee_recordsDTO.nameBng).add(employee_recordsDTO);

                    if (!mapOfEmployee_recordsDTOTonameEng.containsKey(employee_recordsDTO.nameEng)) {
                        mapOfEmployee_recordsDTOTonameEng.put(employee_recordsDTO.nameEng, new HashSet<>());
                    }
                    mapOfEmployee_recordsDTOTonameEng.get(employee_recordsDTO.nameEng).add(employee_recordsDTO);

                    if (!mapOfEmployee_recordsDTOToofficeUnitsType.containsKey(employee_recordsDTO.officeUnitsType)) {
                        mapOfEmployee_recordsDTOToofficeUnitsType.put(employee_recordsDTO.officeUnitsType, new HashSet<>());
                    }
                    mapOfEmployee_recordsDTOToofficeUnitsType.get(employee_recordsDTO.officeUnitsType).add(employee_recordsDTO);

                    if (!mapOfEmployee_recordsDTOToemployeeOfficesType.containsKey(employee_recordsDTO.employeeOfficesType)) {
                        mapOfEmployee_recordsDTOToemployeeOfficesType.put(employee_recordsDTO.employeeOfficesType, new HashSet<>());
                    }
                    mapOfEmployee_recordsDTOToemployeeOfficesType.get(employee_recordsDTO.employeeOfficesType).add(employee_recordsDTO);

                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public List<OfficeHeadDTO> getEmployee_recordsList() {
        List<OfficeHeadDTO> employee_recordss = new ArrayList<OfficeHeadDTO>(this.mapOfEmployee_recordsDTOToiD.values());
        return employee_recordss;
    }


    public OfficeHeadDTO getEmployee_recordsDTOByID(long ID) {
        return mapOfEmployee_recordsDTOToiD.get(ID);
    }


    public List<OfficeHeadDTO> getEmployee_recordsDTOByname_bng(String name_bng) {
        return new ArrayList<>(mapOfEmployee_recordsDTOTonameBng.getOrDefault(name_bng, new HashSet<>()));
    }


    public List<OfficeHeadDTO> getEmployee_recordsDTOByname_eng(String name_eng) {
        return new ArrayList<>(mapOfEmployee_recordsDTOTonameEng.getOrDefault(name_eng, new HashSet<>()));
    }


    public List<OfficeHeadDTO> getEmployee_recordsDTOByoffice_units_type(int office_units_type) {
        return new ArrayList<>(mapOfEmployee_recordsDTOToofficeUnitsType.getOrDefault(office_units_type, new HashSet<>()));
    }


    public List<OfficeHeadDTO> getEmployee_recordsDTOByemployee_offices_type(int employee_offices_type) {
        return new ArrayList<>(mapOfEmployee_recordsDTOToemployeeOfficesType.getOrDefault(employee_offices_type, new HashSet<>()));
    }


    @Override
    public String getTableName() {
        String tableName = "";
        try {
            tableName = "office_head";
        } catch (Exception ex) {
            logger.debug("FATAL", ex);
        }
        return tableName;
    }
}


