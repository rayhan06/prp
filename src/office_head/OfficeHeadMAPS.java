package office_head;
import java.util.*; 


public class OfficeHeadMAPS
{

	public HashMap<String, String> java_allfield_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_anyfield_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	public HashMap<String, String> java_SQL_map = new HashMap<String, String>();
	public HashMap<String, String> java_Text_map = new HashMap<String, String>();
	
	private static OfficeHeadMAPS self = null;
	
	private OfficeHeadMAPS()
	{
		
		java_allfield_type_map.put("name_bng".toLowerCase(), "String");
		java_allfield_type_map.put("name_eng".toLowerCase(), "String");
		java_allfield_type_map.put("office_units_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("employee_offices_id".toLowerCase(), "Integer");

		java_anyfield_search_map.put("employee_records.name_bng".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.name_eng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_units.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("office_units.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_offices.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_offices.name_bn".toLowerCase(), "String");

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("nameBng".toLowerCase(), "nameBng".toLowerCase());
		java_DTO_map.put("nameEng".toLowerCase(), "nameEng".toLowerCase());
		java_DTO_map.put("officeUnitsType".toLowerCase(), "officeUnitsType".toLowerCase());
		java_DTO_map.put("employeeOfficesType".toLowerCase(), "employeeOfficesType".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());

		java_SQL_map.put("name_bng".toLowerCase(), "nameBng".toLowerCase());
		java_SQL_map.put("name_eng".toLowerCase(), "nameEng".toLowerCase());
		java_SQL_map.put("office_units_id".toLowerCase(), "officeUnitsType".toLowerCase());
		java_SQL_map.put("employee_offices_id".toLowerCase(), "employeeOfficesType".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Name (Bangla)".toLowerCase(), "nameBng".toLowerCase());
		java_Text_map.put("Name (English)".toLowerCase(), "nameEng".toLowerCase());
		java_Text_map.put("Office Units".toLowerCase(), "officeUnitsType".toLowerCase());
		java_Text_map.put("Employee Offices".toLowerCase(), "employeeOfficesType".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
			
	}
	
	public static OfficeHeadMAPS GetInstance()
	{
		if(self == null)
		{
			self = new OfficeHeadMAPS();
		}
		return self;
	}
	

}