package office_head;


public class OfficeHeadDTO {

    public long iD = 0;
    public String nameBng = "";
    public String nameEng = "";
    public String designation = "";
    public String unit = "";
    public int officeUnitsType = 0;
    public int officeUnitId = 0;
    public int employeeOfficesType = 0;
    public int isHead = 0;
    public int officeId = 0;
    public int employeeOfficeId = 0;
    public boolean isDeleted = false;


    @Override
    public String toString() {
        return "$OfficeHeadDTO[" +
                " id = " + iD +
                " nameBng = " + nameBng +
                " nameEng = " + nameEng +
                " officeUnitsType = " + officeUnitsType +
                " employeeOfficesType = " + employeeOfficesType +
                " employeeOfficeId = " + employeeOfficeId +
                " officeId = " + officeId +
                " isHead = " + isHead +
                " isDeleted = " + isDeleted +
                "]";
    }

}