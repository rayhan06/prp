package office_head;

import dbm.DBMR;
import dbm.DBMW;
import login.LoginDTO;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.NavigationService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public class OfficeHeadDAO implements NavigationService {

    public int ministry_id;
    public int office_layer;
    public int office_origin;
    public int office;
    public int page_number;
    public String id;

    Logger logger = Logger.getLogger(getClass());

    private void printSql(String sql) {
        System.out.println("sql: " + sql);
    }

    private void recordUpdateTime(Connection connection, long lastModificationTime) {
        recordUpdateTime(connection,"office_head",lastModificationTime);
    }

    public long addEmployee_records(OfficeHeadDTO employee_recordsDTO) throws Exception {

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();

        try {
            connection = DBMW.getInstance().getConnection();

            if (connection == null) {
                System.out.println("nullconn");
            }

            employee_recordsDTO.iD = DBMW.getInstance().getNextSequenceId("Employee_records");

            String sql = "INSERT INTO employee_records";

            sql += " (";
            sql += "ID";
            sql += ", ";
            sql += "name_bng";
            sql += ", ";
            sql += "name_eng";
            sql += ", ";
            sql += "office_units_id";
            sql += ", ";
            sql += "employee_offices_id";
            sql += ", ";
            sql += "isDeleted";
            sql += ", lastModificationTime)";


            sql += " VALUES(";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ?)";

            printSql(sql);

            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, employee_recordsDTO.iD);
            ps.setObject(index++, employee_recordsDTO.nameBng);
            ps.setObject(index++, employee_recordsDTO.nameEng);
            ps.setObject(index++, employee_recordsDTO.officeUnitsType);
            ps.setObject(index++, employee_recordsDTO.employeeOfficesType);
            ps.setObject(index++, employee_recordsDTO.isDeleted);
            ps.setObject(index++, lastModificationTime);

            System.out.println(ps);
            ps.execute();


            recordUpdateTime(connection, lastModificationTime);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return employee_recordsDTO.iD;
    }

    //need another getter for repository
    public OfficeHeadDTO getEmployee_recordsDTOByID(long ID) throws Exception {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        OfficeHeadDTO employee_recordsDTO = null;
        try {

            String sql = "SELECT * ";

            sql += " FROM employee_records";

            sql += " WHERE ID=" + ID;

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                employee_recordsDTO = new OfficeHeadDTO();

                employee_recordsDTO.iD = rs.getLong("ID");
                employee_recordsDTO.nameBng = rs.getString("name_bng");
                employee_recordsDTO.nameEng = rs.getString("name_eng");
                employee_recordsDTO.officeUnitsType = rs.getInt("office_units_id");
                employee_recordsDTO.employeeOfficesType = rs.getInt("employee_offices_id");
                employee_recordsDTO.isDeleted = rs.getBoolean("isDeleted");

            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return employee_recordsDTO;
    }

    public long updateEmployee_records(OfficeHeadDTO employee_recordsDTO) throws Exception {

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            connection = DBMW.getInstance().getConnection();

            String sql = "UPDATE employee_records";

            sql += " SET ";
            sql += "name_bng=?";
            sql += ", ";
            sql += "name_eng=?";
            sql += ", ";
            sql += "office_units_id=?";
            sql += ", ";
            sql += "employee_offices_id=?";
            sql += ", ";
            sql += "isDeleted=?";
            sql += ", lastModificationTime = " + lastModificationTime + "";
            sql += " WHERE ID = " + employee_recordsDTO.iD;

            printSql(sql);

            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, employee_recordsDTO.nameBng);
            ps.setObject(index++, employee_recordsDTO.nameEng);
            ps.setObject(index++, employee_recordsDTO.officeUnitsType);
            ps.setObject(index++, employee_recordsDTO.employeeOfficesType);
            ps.setObject(index++, employee_recordsDTO.isDeleted);
            System.out.println(ps);
            ps.executeUpdate();


            recordUpdateTime(connection, lastModificationTime);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return employee_recordsDTO.iD;
    }

    public void deleteEmployee_recordsByID(long ID) throws Exception {
        Connection connection = null;
        Statement stmt = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            String sql = "UPDATE employee_records";

            sql += " SET isDeleted=1,lastModificationTime=" + lastModificationTime + " WHERE ID = " + ID;

            printSql(sql);

            connection = DBMW.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.execute(sql);


            recordUpdateTime(connection, lastModificationTime);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
    }

    public List<OfficeHeadDTO> getDTOs(Collection recordIDs, LoginDTO loginDTO) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        OfficeHeadDTO officeHeadDTO = null;
        List<OfficeHeadDTO> officeHeadManagerDTOList = new ArrayList<>();
        if (recordIDs.isEmpty()) {
            return officeHeadManagerDTOList;
        }
        try {

            String sql1 = "SELECT * ";

            sql1 += " FROM employee_records";

            sql1 += " WHERE ID IN ( ";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql1 += ",";
                }
                sql1 += ((ArrayList) recordIDs).get(i);
            }
            sql1 += ")";

            printSql(sql1);

            String sql = "SELECT distinct employee_records.ID as ID, " +
                    " employee_records.name_eng as name_eng, " +
                    " employee_records.name_bng as name_bng, " +
                    " employee_offices.designation as designation, " +
                    " employee_offices.id as employeeOfficeId, " +
                    " employee_offices.office_id as officeId, " +
                    " employee_offices.office_head as isHead, " +
                    " office_units.unit_name_bng as unit " +
                    " FROM employee_records ";
            sql += " join employee_offices on employee_records.ID = employee_offices.employee_record_id ";
            sql += " join office_units on employee_offices.office_unit_id = office_units.id  ";
            sql += " join offices on offices.id = employee_offices.office_id ";
            sql += " where offices.office_ministry_id = " + ministry_id +
                    " and offices.office_layer_id = " + office_layer +
                    " and offices.office_origin_id = " + office_origin +
                    " and offices.id = " + office +
                    " and employee_records.ID IN (";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql += ",";
                }
                sql += ((ArrayList) recordIDs).get(i);
            }
            sql += ")  order by ID desc";

            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                officeHeadDTO = new OfficeHeadDTO();
                officeHeadDTO.iD = rs.getLong("ID");
                officeHeadDTO.nameEng = rs.getString("name_eng");
                officeHeadDTO.nameBng = rs.getString("name_bng");
                officeHeadDTO.designation = rs.getString("designation");
                officeHeadDTO.employeeOfficeId = rs.getInt("employeeOfficeId");
                officeHeadDTO.unit = rs.getString("unit");
                officeHeadDTO.officeId = rs.getInt("officeId");
                officeHeadDTO.isHead = rs.getInt("isHead");
                System.out.println("got this DTO: " + officeHeadDTO);

                officeHeadManagerDTOList.add(officeHeadDTO);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return officeHeadManagerDTOList;

    }

    public Collection getIDs(LoginDTO loginDTO) {
        Collection data = new ArrayList();
        Connection connection = null;
        Statement stmt = null;
        ResultSet resultSet = null;

        String sql = "SELECT distinct employee_records.ID as ID from employee_records";

        sql += " join employee_offices on employee_records.ID = employee_offices.employee_record_id ";
        sql += " join office_units on employee_offices.office_unit_id = office_units.id  ";
        sql += " join offices on offices.id = employee_offices.office_id ";
        sql += " where offices.office_ministry_id = " + ministry_id +
                " and offices.office_layer_id = " + office_layer +
                " and offices.office_origin_id = " + office_origin +
                " and offices.id = " + office + " order by employee_records.name_eng asc, employee_offices.designation asc, office_units.unit_level";

        printSql(sql);

        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();

            for (resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("ID"))) ;

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("DAO " + e, e);
        } finally {
            try {
                if (resultSet != null && !resultSet.isClosed()) {
                    resultSet.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception e) {
                logger.fatal("DAO finally :" + e);
            }
        }
        return data;
    }

    //add repository
    public List<OfficeHeadDTO> getAllEmployee_records(boolean isFirstReload) {
        List<OfficeHeadDTO> employee_recordsDTOList = new ArrayList<>();

        String sql = "SELECT * FROM employee_records";
        sql += " WHERE ";


        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        printSql(sql);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;


        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);


            while (rs.next()) {
                OfficeHeadDTO employee_recordsDTO = new OfficeHeadDTO();
                employee_recordsDTO.iD = rs.getLong("ID");
                employee_recordsDTO.nameBng = rs.getString("name_bng");
                employee_recordsDTO.nameEng = rs.getString("name_eng");
                employee_recordsDTO.officeUnitsType = rs.getInt("office_units_id");
                employee_recordsDTO.employeeOfficesType = rs.getInt("employee_offices_id");
                employee_recordsDTO.isDeleted = rs.getBoolean("isDeleted");
                int i = 0;
                long primaryKey = employee_recordsDTO.iD;
                while (i < employee_recordsDTOList.size() && employee_recordsDTOList.get(i).iD < primaryKey) {
                    i++;
                }
                employee_recordsDTOList.add(i, employee_recordsDTO);
                //employee_recordsDTOList.add(employee_recordsDTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return employee_recordsDTOList;
    }

    public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO) throws Exception {
        System.out.println("table: " + p_searchCriteria);
        List<Long> idList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement ps = null;

        try {

//            String sql = "SELECT distinct employee_records.ID as ID FROM employee_records ";
//            sql += " join office_units on employee_records.office_units_id = office_units.ID ";
//            sql += " join employee_offices on employee_records.employee_offices_id = employee_offices.ID ";

            String sql = "SELECT distinct employee_records.ID as ID from employee_records";

            sql += " join employee_offices on employee_records.ID = employee_offices.employee_record_id ";
            sql += " join office_units on employee_offices.office_unit_id = office_units.id  ";
            sql += " join offices on offices.id = employee_offices.office_id ";
            sql += " where offices.office_ministry_id = " + ministry_id +
                    " and offices.office_layer_id = " + office_layer +
                    " and offices.office_origin_id = " + office_origin +
                    " and offices.id = " + office +
                    " and employee_records.isDeleted = false ";

            printSql(sql);

            Enumeration names = p_searchCriteria.keys();
            String str, value;

            String AnyfieldSql = "(";

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {

                AnyfieldSql += " employee_records.name_eng like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
                AnyfieldSql += " or ";
                AnyfieldSql += " employee_records.name_bng like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
            }
            AnyfieldSql += ")";
            System.out.println("AnyfieldSql = " + AnyfieldSql);

            String AllFieldSql = "(";
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                System.out.println(str + ": " + value);
                if (OfficeHeadMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()) != null && !OfficeHeadMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
                        && !str.equalsIgnoreCase("AnyField")
                        && value != null && !value.equalsIgnoreCase("")) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql += " AND  ";
                    }
                    if (OfficeHeadMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equals("String")) {
                        AllFieldSql += "employee_records." + str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
                    } else {
                        AllFieldSql += "employee_records." + str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
                    }
                    i++;
                }
            }

            AllFieldSql += ")";

            System.out.println("AllFieldSql = " + AllFieldSql);


//            sql += " WHERE ";
//            sql += " employee_records.isDeleted = false";


            if (!AnyfieldSql.equals("()")) {
                sql += " AND " + AnyfieldSql;

            }
            if (!AllFieldSql.equals("()")) {
                sql += " AND " + AllFieldSql;
            }

            sql += " order by employee_records.name_eng asc, employee_offices.designation asc, office_units.unit_level";

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                idList.add(rs.getLong("ID"));
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return idList;
    }

    public boolean updateAsHead(int employeeOfficeId, int officeId, int officeUnitId) {
        Connection connection = null;
        Statement stmt = null;

        if (removeAsHead(employeeOfficeId, officeId, officeUnitId)) {
            try {
                String sql = "update employee_offices set office_head = 1 where id = " + employeeOfficeId + ";";
                printSql(sql);

                connection = DBMW.getInstance().getConnection();
                stmt = connection.createStatement();
                stmt.execute(sql);

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (stmt != null) {
                        stmt.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (connection != null) {
                        DBMW.getInstance().freeConnection(connection);
                    }
                } catch (Exception ex2) {
                }
            }
            return true;
        }
        return false;
    }

    private boolean removeAsHead(int officeUnitOrganogramId, int officeId, int officeUnitId) {
        Connection connection = null;
        Statement stmt = null;

        try {
            String sql = "update employee_offices set office_head = 0 where office_id = " + officeId + " AND office_head > 0;";
            printSql(sql);

            connection = DBMW.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.execute(sql);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return true;
    }
}
	