/*
package office_head;

import java.io.IOException;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import general_purpose_servlet.GeneralPurposeModel;
import general_purpose_servlet.GeneralPurposeDAO;
import org.apache.log4j.Logger;

import login.LoginDTO;
import org.json.JSONObject;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;

import util.RecordNavigationManager;

import javax.servlet.http.*;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.UUID;

*/
/**
 * Servlet implementation class OfficeHeadServlet
 *//*

@WebServlet("/OfficeHeadServlet")
@MultipartConfig
public class OfficeHeadServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(OfficeHeadServlet.class);

    */
/**
     * @see HttpServlet#HttpServlet()
     *//*

    public OfficeHeadServlet() {
        super();
    }

    */
/**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     *//*

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getAddPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_RECORDS_ADD)) {
                    getAddPage(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getEditPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_RECORDS_UPDATE)) {
                    getEmployee_records(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getURL")) {
                String URL = request.getParameter("URL");
                System.out.println("URL = " + URL);
                response.sendRedirect(URL);
            } else if (actionType.equals("search")) {
                System.out.println("search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_RECORDS_SEARCH)) {
                    searchEmployee_records(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("ID", -1L);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("office_head/OfficeHeadEdit.jsp");
        requestDispatcher.forward(request, response);
    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        System.out.println("Part Header = {0}" + partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        System.out.println("doPost");

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("add")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_RECORDS_ADD)) {
                    System.out.println("going to  addEmployee_records ");
                    addEmployee_records(request, response, true);
                } else {
                    System.out.println("Not going to  addEmployee_records ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("edit")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_RECORDS_UPDATE)) {
                    addEmployee_records(request, response, false);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("delete")) {
                deleteEmployee_records(request, response);
            } else if (actionType.equals("search")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_RECORDS_SEARCH)) {
                    searchEmployee_records(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getGeo")) {
                System.out.println("going to geoloc ");
                request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
            } else if (actionType.equals("submit")) {
                int id = Integer.parseInt(request.getParameter("id"));
                int officeId = Integer.parseInt(request.getParameter("officeId"));
                int officeUnitId = Integer.parseInt(request.getParameter("officeUnitId"));
                new OfficeHeadDAO().updateAsHead(id, officeId, officeUnitId);
                //response.sendRedirect("GenericTreeServlet?actionType=addOfficeHead");

                response.setContentType("application/json");
                HashMap a = new HashMap<String, Boolean>();
                a.put("success", true);

                String res = new JSONObject(a).toString();

                PrintWriter out = response.getWriter();

                out.print(res);
                out.flush();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void addEmployee_records(HttpServletRequest request, HttpServletResponse response, Boolean addFlag) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addEmployee_records");
            String path = getServletContext().getRealPath("/img2/");
            OfficeHeadDAO employee_recordsDAO = new OfficeHeadDAO();
            OfficeHeadDTO employee_recordsDTO;
            String FileNamePrefix;
            if (addFlag == true) {
                employee_recordsDTO = new OfficeHeadDTO();
                FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
            } else {
                employee_recordsDTO = employee_recordsDAO.getEmployee_recordsDTOByID(Long.parseLong(request.getParameter("identity")));
                FileNamePrefix = request.getParameter("identity");
            }

            String Value = "";
            Value = request.getParameter("nameBng");
            System.out.println("nameBng = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                employee_recordsDTO.nameBng = (Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("nameEng");
            System.out.println("nameEng = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                employee_recordsDTO.nameEng = (Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("officeUnitsType");
            System.out.println("officeUnitsType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                employee_recordsDTO.officeUnitsType = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("employeeOfficesType");
            System.out.println("employeeOfficesType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                employee_recordsDTO.employeeOfficesType = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("isDeleted");
            System.out.println("isDeleted = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                employee_recordsDTO.isDeleted = Boolean.parseBoolean(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }

            System.out.println("Done adding  addEmployee_records dto = " + employee_recordsDTO);

            if (addFlag == true) {
                employee_recordsDAO.addEmployee_records(employee_recordsDTO);
            } else {
                employee_recordsDAO.updateEmployee_records(employee_recordsDTO);

            }


            String inPlaceSubmit = (String) request.getParameter("inplacesubmit");

            if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                getEmployee_records(request, response);
            } else {
                response.sendRedirect("OfficeHeadServlet?actionType=search");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteEmployee_records(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            String[] IDsToDelete = request.getParameterValues("ID");
            for (int i = 0; i < IDsToDelete.length; i++) {
                long id = Long.parseLong(IDsToDelete[i]);
                System.out.println("###DELETING " + IDsToDelete[i]);
                new OfficeHeadDAO().deleteEmployee_recordsByID(id);
            }
        } catch (Exception ex) {
            logger.debug(ex);
        }
        response.sendRedirect("OfficeHeadServlet?actionType=search");
    }

    private void getEmployee_records(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("in getEmployee_records");
        OfficeHeadDTO employee_recordsDTO = null;
        try {
            employee_recordsDTO = new OfficeHeadDAO().getEmployee_recordsDTOByID(Long.parseLong(request.getParameter("ID")));
            request.setAttribute("ID", employee_recordsDTO.iD);
            request.setAttribute("employee_recordsDTO", employee_recordsDTO);

            String URL = "";

            String inPlaceEdit = (String) request.getParameter("inplaceedit");
            String inPlaceSubmit = (String) request.getParameter("inplacesubmit");
            String getBodyOnly = (String) request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "office_head/OfficeHeadInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "office_head/OfficeHeadSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "office_head/OfficeHeadEditBody.jsp?actionType=edit";
                } else {
                    URL = "office_head/OfficeHeadEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void searchEmployee_records(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("in  searchEmployee_records 1");
        OfficeHeadDAO employee_recordsDAO = new OfficeHeadDAO();

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);

        GeneralPurposeDAO commonServlet = new GeneralPurposeDAO();
        GeneralPurposeModel.OfficeModel officeModel = commonServlet.getOfficeModelByOfficeId(userDTO.officeID);
        */
/*
        employee_recordsDAO.ministry_id = officeModel.getOffice_ministry_id();//Integer.parseInt((String) request.getParameter("ministry_id") != null ? request.getParameter("ministry_id") : "0");
        employee_recordsDAO.office_layer = officeModel.getOffice_layer_id();//Integer.parseInt((String) request.getParameter("office_layer") != null ? request.getParameter("office_layer") : "0");
        employee_recordsDAO.office_origin = officeModel.getOffice_origin_id();//Integer.parseInt((String) request.getParameter("office_origin") != null ? request.getParameter("office_origin") : "0");
        employee_recordsDAO.office = officeModel.getId();// Integer.parseInt((String) request.getParameter("office") != null ? request.getParameter("office") : "0");
        employee_recordsDAO.page_number = Integer.parseInt((String) request.getParameter("page_number") != null ? request.getParameter("page_number") : "0");
        employee_recordsDAO.id = (String) request.getParameter("id") != null ? request.getParameter("id") : "0";
        *//*

        employee_recordsDAO.ministry_id = Integer.parseInt((String) request.getParameter("ministry_id") != null ? request.getParameter("ministry_id") : "0");
        employee_recordsDAO.office_layer = Integer.parseInt((String) request.getParameter("office_layer") != null ? request.getParameter("office_layer") : "0");
        employee_recordsDAO.office_origin = Integer.parseInt((String) request.getParameter("office_origin") != null ? request.getParameter("office_origin") : "0");
        employee_recordsDAO.office = Integer.parseInt((String) request.getParameter("office") != null ? request.getParameter("office") : "0");
        employee_recordsDAO.page_number = Integer.parseInt((String) request.getParameter("page_number") != null ? request.getParameter("page_number") : "0");
        employee_recordsDAO.id = (String) request.getParameter("id") != null ? request.getParameter("id") : "0";

        String ajax = (String) request.getParameter("ajax");
        boolean hasAjax = false;
        if (ajax != null && !ajax.equalsIgnoreCase("")) {
            hasAjax = true;
        }
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager rnManager = new RecordNavigationManager(SessionConstants.NAV_EMPLOYEE_RECORDS, request, employee_recordsDAO, SessionConstants.VIEW_EMPLOYEE_RECORDS, SessionConstants.SEARCH_EMPLOYEE_RECORDS);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        RequestDispatcher rd;
        if (true) {
            System.out.println("Going to office_head/OfficeHeadSearchForm.jsp");
            rd = request.getRequestDispatcher("office_head/OfficeHeadSearchBody.jsp");
        } else {
            System.out.println("Going to office_head/OfficeHeadSearchForm.jsp");
            rd = request.getRequestDispatcher("office_head/OfficeHeadSearchForm.jsp");
        }
        rd.forward(request, response);
    }

}

*/
