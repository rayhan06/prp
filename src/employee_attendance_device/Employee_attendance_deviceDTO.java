package employee_attendance_device;
import java.util.*; 
import util.*; 


public class Employee_attendance_deviceDTO extends CommonDTO
{

	public long employeeRecordsId = 0;
	public long attendanceDeviceId = 0;
	public long employeeIdInAttendenceDevice = 0;
    public String cardNumber = "";
	public long filesDropzone = 0;
	public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Employee_attendance_deviceDTO[" +
            " iD = " + iD +
            " employeeRecordsId = " + employeeRecordsId +
            " attendanceDeviceId = " + attendanceDeviceId +
            " employeeIdInAttendenceDevice = " + employeeIdInAttendenceDevice +
            " cardNumber = " + cardNumber +
            " filesDropzone = " + filesDropzone +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}