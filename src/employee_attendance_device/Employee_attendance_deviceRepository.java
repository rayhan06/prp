package employee_attendance_device;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;


public class Employee_attendance_deviceRepository implements Repository {
	Employee_attendance_deviceDAO employee_attendance_deviceDAO;

	public void setDAO(Employee_attendance_deviceDAO employee_attendance_deviceDAO)
	{
		this.employee_attendance_deviceDAO = employee_attendance_deviceDAO;
	}


	static Logger logger = Logger.getLogger(Employee_attendance_deviceRepository.class);
	Map<Long, Employee_attendance_deviceDTO >mapOfEmployee_attendance_deviceDTOToiD;
	Map<Long, Set<Employee_attendance_deviceDTO> >mapOfEmployee_attendance_deviceDTOToemployeeRecordsId;
	Map<Long, Set<Employee_attendance_deviceDTO> >mapOfEmployee_attendance_deviceDTOToattendanceDeviceId;
	Map<String, Set<Employee_attendance_deviceDTO> >mapOfEmployee_attendance_deviceDTOTocardNumber;
    List<Employee_attendance_deviceDTO> employee_attendance_deviceDTOList;

	static Employee_attendance_deviceRepository instance = null;
	private Employee_attendance_deviceRepository(){
		mapOfEmployee_attendance_deviceDTOToiD = new ConcurrentHashMap<>();
		mapOfEmployee_attendance_deviceDTOToemployeeRecordsId = new ConcurrentHashMap<>();
		mapOfEmployee_attendance_deviceDTOToattendanceDeviceId = new ConcurrentHashMap<>();
		mapOfEmployee_attendance_deviceDTOTocardNumber = new ConcurrentHashMap<>();
        employee_attendance_deviceDAO = Employee_attendance_deviceDAO.getInstance();
		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Employee_attendance_deviceRepository getInstance(){
		if (instance == null){
			instance = new Employee_attendance_deviceRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(employee_attendance_deviceDAO == null)
		{
			return;
		}
		try {
			List<Employee_attendance_deviceDTO> employee_attendance_deviceDTOs = employee_attendance_deviceDAO.getAllEmployee_attendance_device(reloadAll);
			for(Employee_attendance_deviceDTO employee_attendance_deviceDTO : employee_attendance_deviceDTOs) {
				Employee_attendance_deviceDTO oldEmployee_attendance_deviceDTO = getEmployee_attendance_deviceDTOByID(employee_attendance_deviceDTO.iD);
				if( oldEmployee_attendance_deviceDTO != null ) {
					if(mapOfEmployee_attendance_deviceDTOToiD.containsKey(oldEmployee_attendance_deviceDTO.iD)) {
						mapOfEmployee_attendance_deviceDTOToiD.remove(oldEmployee_attendance_deviceDTO);
					}
					if(mapOfEmployee_attendance_deviceDTOToiD.isEmpty()) {
						mapOfEmployee_attendance_deviceDTOToiD.remove(oldEmployee_attendance_deviceDTO.iD);
					}

					if(mapOfEmployee_attendance_deviceDTOToemployeeRecordsId.containsKey(oldEmployee_attendance_deviceDTO.employeeRecordsId)) {
						mapOfEmployee_attendance_deviceDTOToemployeeRecordsId.get(oldEmployee_attendance_deviceDTO.employeeRecordsId).remove(oldEmployee_attendance_deviceDTO);
					}
					if(mapOfEmployee_attendance_deviceDTOToemployeeRecordsId.get(oldEmployee_attendance_deviceDTO.employeeRecordsId).isEmpty()) {
						mapOfEmployee_attendance_deviceDTOToemployeeRecordsId.remove(oldEmployee_attendance_deviceDTO.employeeRecordsId);
					}

					if(mapOfEmployee_attendance_deviceDTOToattendanceDeviceId.containsKey(oldEmployee_attendance_deviceDTO.attendanceDeviceId)) {
						mapOfEmployee_attendance_deviceDTOToattendanceDeviceId.get(oldEmployee_attendance_deviceDTO.attendanceDeviceId).remove(oldEmployee_attendance_deviceDTO);
					}
					if(mapOfEmployee_attendance_deviceDTOToattendanceDeviceId.get(oldEmployee_attendance_deviceDTO.attendanceDeviceId).isEmpty()) {
						mapOfEmployee_attendance_deviceDTOToattendanceDeviceId.remove(oldEmployee_attendance_deviceDTO.attendanceDeviceId);
					}

					if(mapOfEmployee_attendance_deviceDTOTocardNumber.containsKey(oldEmployee_attendance_deviceDTO.cardNumber)) {
						mapOfEmployee_attendance_deviceDTOTocardNumber.get(oldEmployee_attendance_deviceDTO.cardNumber).remove(oldEmployee_attendance_deviceDTO);
					}
					if(mapOfEmployee_attendance_deviceDTOTocardNumber.get(oldEmployee_attendance_deviceDTO.cardNumber).isEmpty()) {
						mapOfEmployee_attendance_deviceDTOTocardNumber.remove(oldEmployee_attendance_deviceDTO.cardNumber);
					}

				}
				if(employee_attendance_deviceDTO.isDeleted == 0)
				{

                    mapOfEmployee_attendance_deviceDTOToiD.put(employee_attendance_deviceDTO.iD, employee_attendance_deviceDTO);

					if( ! mapOfEmployee_attendance_deviceDTOToemployeeRecordsId.containsKey(employee_attendance_deviceDTO.employeeRecordsId)) {
						mapOfEmployee_attendance_deviceDTOToemployeeRecordsId.put(employee_attendance_deviceDTO.employeeRecordsId, new HashSet<>());
					}
					mapOfEmployee_attendance_deviceDTOToemployeeRecordsId.get(employee_attendance_deviceDTO.employeeRecordsId).add(employee_attendance_deviceDTO);

					if( ! mapOfEmployee_attendance_deviceDTOToattendanceDeviceId.containsKey(employee_attendance_deviceDTO.attendanceDeviceId)) {
						mapOfEmployee_attendance_deviceDTOToattendanceDeviceId.put(employee_attendance_deviceDTO.attendanceDeviceId, new HashSet<>());
					}
					mapOfEmployee_attendance_deviceDTOToattendanceDeviceId.get(employee_attendance_deviceDTO.attendanceDeviceId).add(employee_attendance_deviceDTO);

					if( ! mapOfEmployee_attendance_deviceDTOTocardNumber.containsKey(employee_attendance_deviceDTO.cardNumber)) {
						mapOfEmployee_attendance_deviceDTOTocardNumber.put(employee_attendance_deviceDTO.cardNumber, new HashSet<>());
					}
					mapOfEmployee_attendance_deviceDTOTocardNumber.get(employee_attendance_deviceDTO.cardNumber).add(employee_attendance_deviceDTO);

				}
			}
            employee_attendance_deviceDTOList = new ArrayList<>(mapOfEmployee_attendance_deviceDTOToiD.values());
            employee_attendance_deviceDTOList.sort(Comparator.comparing(o->o.iD));
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public List<Employee_attendance_deviceDTO> getEmployee_attendance_deviceList() {
		List <Employee_attendance_deviceDTO> employee_attendance_devices = new ArrayList<Employee_attendance_deviceDTO>();
		return employee_attendance_devices;
	}



	public Employee_attendance_deviceDTO getEmployee_attendance_deviceDTOByID(long ID) {
		return mapOfEmployee_attendance_deviceDTOToiD.get(ID);
	}


	public List<Employee_attendance_deviceDTO> getEmployee_attendance_deviceDTOByemployee_records_id(long employee_records_id) {
		return new ArrayList<>( mapOfEmployee_attendance_deviceDTOToemployeeRecordsId.getOrDefault(employee_records_id,new HashSet<>()));
	}


	public List<Employee_attendance_deviceDTO> getEmployee_attendance_deviceDTOByattendance_device_id(long attendance_device_id) {
		return new ArrayList<>( mapOfEmployee_attendance_deviceDTOToattendanceDeviceId.getOrDefault(attendance_device_id,new HashSet<>()));
	}


	public List<Employee_attendance_deviceDTO> getEmployee_attendance_deviceDTOBycard_number(String card_number) {
		return new ArrayList<>( mapOfEmployee_attendance_deviceDTOTocardNumber.getOrDefault(card_number,new HashSet<>()));
	}

//    private String buildOptions(String language, Long selectedId, boolean withoutSelectOption){
//        List<OptionDTO> optionDTOList = null;
//        if (employee_attendance_deviceDTOList!=null && employee_attendance_deviceDTOList.size() > 0){
//            optionDTOList = employee_attendance_deviceDTOList.stream()
//                    .map(dto->new OptionDTO(dto.,dto.nameBn,String.valueOf(dto.iD)))
//                    .collect(Collectors.toList());
//        }
//        if(withoutSelectOption){
//            return Utils.buildOptionsWithoutSelectOption(optionDTOList,language);
//        }
//        return Utils.buildOptions(optionDTOList,language,selectedId == null ? null : String.valueOf(selectedId));
//    }


	@Override
	public String getTableName() {
		return "employee_attendance_device";
	}
}


