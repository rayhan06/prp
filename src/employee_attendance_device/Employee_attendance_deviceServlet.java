package employee_attendance_device;

import attendance_device.Attendance_deviceRepository;
import com.google.gson.Gson;
import files.FilesDAO;
import files.FilesDTO;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.OptionalLong;



/**
 * Servlet implementation class Employee_attendance_deviceServlet
 */
@WebServlet("/Employee_attendance_deviceServlet")
@MultipartConfig
public class Employee_attendance_deviceServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Employee_attendance_deviceServlet.class);

	Employee_attendance_deviceDAO employee_attendance_deviceDAO = Employee_attendance_deviceDAO.getInstance();
	CommonRequestHandler commonRequestHandler = new CommonRequestHandler(employee_attendance_deviceDAO);
	FilesDAO filesDAO = new FilesDAO();
    private final Gson gson = new Gson();
    

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ATTENDANCE_DEVICE_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ATTENDANCE_DEVICE_UPDATE))
				{
					getEmployee_attendance_device(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("downloadDropzoneFile"))
			{
				long id = Long.parseLong(request.getParameter("id"));

				FilesDTO filesDTO = (FilesDTO) filesDAO.getDTOByID(id);
				
				Utils.ProcessFile(request, response, filesDTO.fileTitle, filesDTO.inputStream);
				
			}
			else if(actionType.equals("DeleteFileFromDropZone"))
			{
				long id = Long.parseLong(request.getParameter("id"));
			
				
				System.out.println("In delete file");
				filesDAO.hardDeleteByID(id);
				response.getWriter().write("Deleted");
							
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ATTENDANCE_DEVICE_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchEmployee_attendance_device(request, response, isPermanentTable, filter);
						}
						else
						{
							searchEmployee_attendance_device(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchEmployee_attendance_device(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ATTENDANCE_DEVICE_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			} else if( actionType.equals("getDevice") ) {
				String language = request.getParameter("language");
				String val = request.getParameter("selectedId");
				String employeeRecordsId = request.getParameter("employeeRecordId");
				Long selectedId = null;
				if(val != null && !val.isEmpty()) {
					selectedId = Long.parseLong(val);
				}

				String options = Attendance_deviceRepository.getInstance().buildOptionsForEmployee(language, selectedId, Long.parseLong(employeeRecordsId) );
				PrintWriter out = response.getWriter();
				out.println(options);
				out.close();
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ATTENDANCE_DEVICE_ADD))
				{
					System.out.println("going to  addEmployee_attendance_device ");
					addEmployee_attendance_device(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addEmployee_attendance_device ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("UploadFilesFromDropZone"))
			{
				String Column = request.getParameter("columnName");
				long ColumnID = Long.parseLong(request.getParameter("ColumnID"));
				String pageType = request.getParameter("pageType");
				
				System.out.println("In " + pageType);				
				Utils.UploadFilesFromDropZone(request, response, userDTO.ID, ColumnID, "Employee_attendance_deviceServlet");	
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ATTENDANCE_DEVICE_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addEmployee_attendance_device ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ATTENDANCE_DEVICE_UPDATE))
				{					
					addEmployee_attendance_device(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ATTENDANCE_DEVICE_SEARCH))
				{
					searchEmployee_attendance_device(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Employee_attendance_deviceDTO employee_attendance_deviceDTO = employee_attendance_deviceDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(employee_attendance_deviceDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addEmployee_attendance_device(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addEmployee_attendance_device");
			String path = getServletContext().getRealPath("/img2/");
			Employee_attendance_deviceDTO employee_attendance_deviceDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				employee_attendance_deviceDTO = new Employee_attendance_deviceDTO();
			}
			else
			{
				employee_attendance_deviceDTO = employee_attendance_deviceDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("iD");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("iD = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				employee_attendance_deviceDTO.iD = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("employeeRecordsId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeRecordsId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				employee_attendance_deviceDTO.employeeRecordsId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("attendanceDeviceId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("attendanceDeviceId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				employee_attendance_deviceDTO.attendanceDeviceId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			employee_attendance_deviceDTO.employeeIdInAttendenceDevice = getEmployeeIdInDevice(employee_attendance_deviceDTO.employeeRecordsId, employee_attendance_deviceDTO.attendanceDeviceId);

			Value = request.getParameter("cardNumber");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("cardNumber = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				employee_attendance_deviceDTO.cardNumber = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("filesDropzone");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("filesDropzone = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				
				System.out.println("filesDropzone = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					employee_attendance_deviceDTO.filesDropzone = Long.parseLong(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}
				
				if(addFlag == false)
				{
					String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
					String[] deleteArray = filesDropzoneFilesToDelete.split(",");
					for(int i = 0; i < deleteArray.length; i ++)
					{
						System.out.println("going to delete " + deleteArray[i]);
						if(i>0)
						{
							filesDAO.delete(Long.parseLong(deleteArray[i]));
						}
					}
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				employee_attendance_deviceDTO.insertionDate = c.getTimeInMillis();
			}			


			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				employee_attendance_deviceDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				employee_attendance_deviceDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				employee_attendance_deviceDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addEmployee_attendance_device dto = " + employee_attendance_deviceDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				employee_attendance_deviceDAO.setIsDeleted(employee_attendance_deviceDTO.iD, CommonDTO.OUTDATED);
				returnedID = employee_attendance_deviceDAO.add(employee_attendance_deviceDTO);
				employee_attendance_deviceDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = employee_attendance_deviceDAO.manageWriteOperations(employee_attendance_deviceDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = employee_attendance_deviceDAO.manageWriteOperations(employee_attendance_deviceDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getEmployee_attendance_device(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Employee_attendance_deviceServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(employee_attendance_deviceDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	

    private long getEmployeeIdInDevice(long employeeRecordsId,long attendanceDeviceId) {
		List<Employee_attendance_deviceDTO> attendanceDeviceRecords = employee_attendance_deviceDAO.getDTOsByRecordsIdAndDeviceId(employeeRecordsId,attendanceDeviceId);

		if(attendanceDeviceRecords==null || attendanceDeviceRecords.size()==0) {
			attendanceDeviceRecords = employee_attendance_deviceDAO.getDTOsByDeviceId(attendanceDeviceId);
			if(attendanceDeviceRecords==null || attendanceDeviceRecords.size()==0) {
				return 1;
			} else {
				OptionalLong maxValue = attendanceDeviceRecords.stream()
						.map(v->v.employeeIdInAttendenceDevice)
						.mapToLong(v -> v)
						.max();
				return maxValue.getAsLong()+1;
			}
		} else {
			return attendanceDeviceRecords.get(0).employeeIdInAttendenceDevice;
		}
	}

	
	
	

	private void getEmployee_attendance_device(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getEmployee_attendance_device");
		Employee_attendance_deviceDTO employee_attendance_deviceDTO = null;
		try 
		{
			employee_attendance_deviceDTO = employee_attendance_deviceDAO.getDTOByID(id);
			request.setAttribute("ID", employee_attendance_deviceDTO.iD);
			request.setAttribute("employee_attendance_deviceDTO",employee_attendance_deviceDTO);
			request.setAttribute("employee_attendance_deviceDAO",employee_attendance_deviceDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "employee_attendance_device/employee_attendance_deviceInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "employee_attendance_device/employee_attendance_deviceSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "employee_attendance_device/employee_attendance_deviceEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "employee_attendance_device/employee_attendance_deviceEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getEmployee_attendance_device(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getEmployee_attendance_device(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchEmployee_attendance_device(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchEmployee_attendance_device 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_EMPLOYEE_ATTENDANCE_DEVICE,
			request,
			employee_attendance_deviceDAO,
			SessionConstants.VIEW_EMPLOYEE_ATTENDANCE_DEVICE,
			SessionConstants.SEARCH_EMPLOYEE_ATTENDANCE_DEVICE,
				employee_attendance_deviceDAO.getTableName(),
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("employee_attendance_deviceDAO",employee_attendance_deviceDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to employee_attendance_device/employee_attendance_deviceApproval.jsp");
	        	rd = request.getRequestDispatcher("employee_attendance_device/employee_attendance_deviceApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to employee_attendance_device/employee_attendance_deviceApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("employee_attendance_device/employee_attendance_deviceApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to employee_attendance_device/employee_attendance_deviceSearch.jsp");
	        	rd = request.getRequestDispatcher("employee_attendance_device/employee_attendance_deviceSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to employee_attendance_device/employee_attendance_deviceSearchForm.jsp");
	        	rd = request.getRequestDispatcher("employee_attendance_device/employee_attendance_deviceSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

