package employee_attendance_device;

import attendance_device.Attendance_deviceDAO;
import employee_records.EmployeeCommonDTOService;
import employee_records.Employee_recordsRepository;
import org.apache.log4j.Logger;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@SuppressWarnings({"unused","rawtypes","Duplicates"})
public class Employee_attendance_deviceDAO extends NavigationService4 implements EmployeeCommonDTOService<Employee_attendance_deviceDTO> {

    private static final Logger logger = Logger.getLogger(Employee_attendance_deviceDAO.class);
    private static final String addQuery = "INSERT INTO {tableName} (employee_records_id, attendance_device_id, employee_id_in_attendance_device, card_number, files_dropzone, insertion_date, " +
            "inserted_by, modified_by,search_column, lastModificationTime, isDeleted, ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

    private static final String updateQuery = "UPDATE {tableName} SET employee_records_id=?, attendance_device_id=?, employee_id_in_attendance_device=?, card_number=?, files_dropzone=?, insertion_date=?, inserted_by=?, " +
            "modified_by=?,search_column=?, lastModificationTime=? WHERE ID=?";

    private static final String tableName = "employee_attendance_device";

    private final Map<String,String > searchMap = new HashMap<>();

    private static Employee_attendance_deviceDAO INSTANCE = null;

    public static Employee_attendance_deviceDAO getInstance(){
        if(INSTANCE == null){
            synchronized (Employee_attendance_deviceDAO.class){
                if(INSTANCE == null){
                    INSTANCE = new Employee_attendance_deviceDAO();
                }
            }
        }
        return INSTANCE;
    }

    private Employee_attendance_deviceDAO() {
        super(tableName);
        joinSQL = "";
        useSafeSearch = true;
        searchMap.put("employeeRecordsId"," and (employee_records_id = ?)");
        searchMap.put("card_number", " and (card_number = ?)");
        searchMap.put("AnyField"," and (search_column like ?)");
        commonMaps = new Employee_attendance_deviceMAPS(tableName);
        columnNames = new String[]
                {
                        "ID",
                        "employee_records_id",
                        "attendance_device_id",
                        "employee_id_in_attendance_device",
                        "card_number",
                        "files_dropzone",
                        "insertion_date",
                        "inserted_by",
                        "modified_by",
                        "search_column",
                        "isDeleted",
                        "lastModificationTime"
                };
    }

    public void setSearchColumn(Employee_attendance_deviceDTO employee_attendance_deviceDTO) {

        employee_attendance_deviceDTO.searchColumn = employee_attendance_deviceDTO.cardNumber +
                Employee_recordsRepository.getInstance().getEmployeeName(employee_attendance_deviceDTO.employeeRecordsId, "English") +
                Employee_recordsRepository.getInstance().getEmployeeName(employee_attendance_deviceDTO.employeeRecordsId, "Bangla") +
                Attendance_deviceDAO.getInstance().getDeviceName(employee_attendance_deviceDTO.attendanceDeviceId, "English") +
                Attendance_deviceDAO.getInstance().getDeviceName(employee_attendance_deviceDTO.attendanceDeviceId, "Bangla");
    }
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_attendance_deviceDTO) commonDTO, addQuery, true);
    }
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_attendance_deviceDTO) commonDTO, updateQuery, false);
    }
    public void set(PreparedStatement ps, Employee_attendance_deviceDTO employee_attendance_deviceDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(employee_attendance_deviceDTO);
        ps.setObject(++index, employee_attendance_deviceDTO.employeeRecordsId);
        ps.setObject(++index, employee_attendance_deviceDTO.attendanceDeviceId);
        ps.setObject(++index, employee_attendance_deviceDTO.employeeIdInAttendenceDevice);
        ps.setObject(++index, employee_attendance_deviceDTO.cardNumber);
        ps.setObject(++index, employee_attendance_deviceDTO.filesDropzone);
        ps.setObject(++index, employee_attendance_deviceDTO.insertionDate);
        ps.setObject(++index, employee_attendance_deviceDTO.insertedBy);
        ps.setObject(++index, employee_attendance_deviceDTO.modifiedBy);
        ps.setObject(++index, employee_attendance_deviceDTO.searchColumn);
        ps.setObject(++index, lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, 0);

        }
        ps.setObject(++index, employee_attendance_deviceDTO.iD);
    }

    public Employee_attendance_deviceDTO buildObjectFromResultSet(ResultSet rs) {
        Employee_attendance_deviceDTO employee_attendance_deviceDTO = new Employee_attendance_deviceDTO();
        try {
            employee_attendance_deviceDTO.iD = rs.getLong("ID");
            employee_attendance_deviceDTO.employeeRecordsId = rs.getLong("employee_records_id");
            employee_attendance_deviceDTO.attendanceDeviceId = rs.getLong("attendance_device_id");
            employee_attendance_deviceDTO.employeeIdInAttendenceDevice = rs.getLong("employee_id_in_attendance_device");
            employee_attendance_deviceDTO.cardNumber = rs.getString("card_number");
            employee_attendance_deviceDTO.filesDropzone = rs.getLong("files_dropzone");
            employee_attendance_deviceDTO.insertionDate = rs.getLong("insertion_date");
            employee_attendance_deviceDTO.insertedBy = rs.getString("inserted_by");
            employee_attendance_deviceDTO.modifiedBy = rs.getString("modified_by");
            employee_attendance_deviceDTO.searchColumn = rs.getString("search_column");
            employee_attendance_deviceDTO.isDeleted = rs.getInt("isDeleted");
            employee_attendance_deviceDTO.lastModificationTime = rs.getLong("lastModificationTime");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return employee_attendance_deviceDTO;
    }

    @Override
    public String getTableName() {
        return tableName;
    }


    //need another getter for repository
    public Employee_attendance_deviceDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }


    public long getEmployeeIdInEmployeeAttendanceDevice(long employeeRecordsId, long deviceId) throws Exception {
        List<Employee_attendance_deviceDTO> allRecords = getDTOsByRecordsIdAndDeviceId(employeeRecordsId, deviceId);
        if(allRecords != null && allRecords.size()>0) {
            return allRecords.get(0).employeeIdInAttendenceDevice;
        } else {
            allRecords = new Employee_attendance_deviceDAO().getDTOsByDeviceId(deviceId);
            long newId;
            if(allRecords==null || allRecords.size()==0) {
                newId = 1;
            } else {
                OptionalLong maxValue = allRecords.stream()
                        .map(v->v.employeeIdInAttendenceDevice)
                        .mapToLong(v -> v)
                        .max();
                newId = maxValue.getAsLong()+1;
            }
            Employee_attendance_deviceDTO employee_attendance_deviceDTO = new Employee_attendance_deviceDTO();
            employee_attendance_deviceDTO.employeeRecordsId = employeeRecordsId;
            employee_attendance_deviceDTO.attendanceDeviceId = deviceId;
            employee_attendance_deviceDTO.employeeIdInAttendenceDevice = newId;
            this.add(employee_attendance_deviceDTO);
            return newId;
        }
    }

    public List<Employee_attendance_deviceDTO> getDTOsByRecordsIdAndDeviceId(long employeeRecordsId,long attendanceDeviceId) {
        String sql = "SELECT * from " + tableName + " WHERE employee_records_id = " + employeeRecordsId + " AND attendance_device_id = " +  attendanceDeviceId ;
        return getDTOs(sql);
    }

    public List<Employee_attendance_deviceDTO> getDTOsByDeviceId(long attendanceDeviceId) {
        String sql = "SELECT * from " + tableName + " WHERE attendance_device_id = " +  attendanceDeviceId ;
        return getDTOs(sql);
    }

    //add repository
    public List<Employee_attendance_deviceDTO> getAllEmployee_attendance_device(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }


    public List<Employee_attendance_deviceDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }


    public List<Employee_attendance_deviceDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
                                                       String filter, boolean tableHasJobCat) {
        List<Object> objectList = new ArrayList<>();
        String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat,objectList);
        return getDTOs(sql,objectList);
    }

    public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                          UserDTO userDTO, String filter, boolean tableHasJobCat,List<Object>objectList) {
        return getSearchQuery(tableName,p_searchCriteria,limit,offset,category,searchMap,objectList);
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
                                           boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat,List<Object>objectList) {
        return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat,objectList);
    }
}