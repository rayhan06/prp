package lost_card_info;
import java.util.*; 
import util.*;


public class Lost_card_infoMAPS extends CommonMaps
{	
	public Lost_card_infoMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("cardInfoId".toLowerCase(), "cardInfoId".toLowerCase());
		java_DTO_map.put("gdCopyFileDropzone".toLowerCase(), "gdCopyFileDropzone".toLowerCase());
		java_DTO_map.put("moneyReceiptFileDropzone".toLowerCase(), "moneyReceiptFileDropzone".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("insertionTime".toLowerCase(), "insertionTime".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("card_info_id".toLowerCase(), "cardInfoId".toLowerCase());
		java_SQL_map.put("gd_copy_file_dropzone".toLowerCase(), "gdCopyFileDropzone".toLowerCase());
		java_SQL_map.put("money_receipt_file_dropzone".toLowerCase(), "moneyReceiptFileDropzone".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Card Info Id".toLowerCase(), "cardInfoId".toLowerCase());
		java_Text_map.put("Gd Copy File Dropzone".toLowerCase(), "gdCopyFileDropzone".toLowerCase());
		java_Text_map.put("Money Receipt File Dropzone".toLowerCase(), "moneyReceiptFileDropzone".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Insertion Time".toLowerCase(), "insertionTime".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}