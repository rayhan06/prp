package lost_card_info;

import common.ConnectionAndStatementUtil;
import common.EmployeeCommonDAOService;
import common.RoleEnum;
import leave_reliever_mapping.Leave_reliever_mappingDAO;
import org.apache.log4j.Logger;
import user.UserDTO;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused","Duplicates"})
public class Lost_card_infoDAO implements EmployeeCommonDAOService<Lost_card_infoDTO> {
    private static final Logger logger = Logger.getLogger(Leave_reliever_mappingDAO.class);

    private static final String getByCardInfoId = "SELECT * FROM lost_card_info WHERE card_info_id=%d AND isDeleted=0";

    private static final String getByReIssueCardInfoId = "SELECT * FROM lost_card_info WHERE reissue_card_info_id = %d ORDER BY lastModificationTime DESC";

    private static final String getByReIssueCardInfoIds = "SELECT * FROM lost_card_info WHERE reissue_card_info_id in (%s)";

    private static final String resetReIssueIdSQL = "UPDATE lost_card_info SET reissue_card_info_id = 0 WHERE reissue_card_info_id = %d";

    private static Lost_card_infoDAO INSTANCE = null;

    private Lost_card_infoDAO() {}

    public static Lost_card_infoDAO getInstance(){
        if(INSTANCE == null){
            synchronized (Lost_card_infoDAO.class){
                if(INSTANCE == null){
                    INSTANCE = new Lost_card_infoDAO();
                }
            }
        }
        return INSTANCE;
    }

    public void setSearchColumn(Lost_card_infoDTO lost_card_infoDTO) {
        lost_card_infoDTO.searchColumn = "";
    }

    private static final String addQuery = "INSERT INTO {tableName} (gd_copy_file_dropzone,money_receipt_file_dropzone,"
            .concat("modified_by,lastModificationTime,card_cat,card_info_id,employee_records_id,inserted_by,insertion_time,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery = "UPDATE {tableName} SET gd_copy_file_dropzone=?,money_receipt_file_dropzone=?,"
            .concat("modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final String updateReIssueCardInfoId = "UPDATE lost_card_info SET reissue_card_info_id = ? WHERE id = ?";

    private static final String getLostCardWhichNotIssued = "SELECT * FROM lost_card_info WHERE card_info_id in (%s) and card_cat = 1 and isDeleted = 0 and reissue_card_info_id = 0";

    private static final String getLostCardWhichNotIssuedForEmployee = "SELECT * FROM lost_card_info WHERE card_info_id in (%s) and  card_cat = 1 and isDeleted = 0 " +
            " and reissue_card_info_id = 0  and (employee_records_id = {employee_records_id} OR inserted_by = {employee_records_id})";

    @Override
    public void set(PreparedStatement ps, Lost_card_infoDTO lost_card_infoDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(lost_card_infoDTO);
        ps.setObject(++index, lost_card_infoDTO.gdCopyFileDropzone);
        ps.setObject(++index, lost_card_infoDTO.moneyReceiptFileDropzone);
        ps.setObject(++index, lost_card_infoDTO.modifiedBy);
        ps.setObject(++index, lost_card_infoDTO.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, lost_card_infoDTO.cardCat);
            ps.setObject(++index, lost_card_infoDTO.cardInfoId);
            ps.setObject(++index, lost_card_infoDTO.employeeRecordsId);
            ps.setObject(++index, lost_card_infoDTO.insertedBy);
            ps.setObject(++index, lost_card_infoDTO.insertionTime);
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, lost_card_infoDTO.iD);
    }

    @Override
    public Lost_card_infoDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Lost_card_infoDTO lost_card_infoDTO = new Lost_card_infoDTO();
            lost_card_infoDTO.iD = rs.getLong("ID");
            lost_card_infoDTO.cardInfoId = rs.getLong("card_info_id");
            lost_card_infoDTO.reissueCardInfoId = rs.getLong("reissue_card_info_id");
            lost_card_infoDTO.gdCopyFileDropzone = rs.getLong("gd_copy_file_dropzone");
            lost_card_infoDTO.moneyReceiptFileDropzone = rs.getLong("money_receipt_file_dropzone");
            lost_card_infoDTO.isDeleted = rs.getInt("isDeleted");
            lost_card_infoDTO.insertedBy = rs.getLong("inserted_by");
            lost_card_infoDTO.insertionTime = rs.getLong("insertion_time");
            lost_card_infoDTO.modifiedBy = rs.getLong("modified_by");
            lost_card_infoDTO.lastModificationTime = rs.getLong("lastModificationTime");
            lost_card_infoDTO.cardCat = rs.getInt("card_cat");
            lost_card_infoDTO.employeeRecordsId = rs.getLong("employee_records_id");
            return lost_card_infoDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "lost_card_info";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return null;
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Lost_card_infoDTO) commonDTO, updateQuery, false);
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Lost_card_infoDTO) commonDTO, addQuery, true);
    }

    public Lost_card_infoDTO getByCardInfoId(long cardInfoId) {
        return ConnectionAndStatementUtil.getT(String.format(getByCardInfoId, cardInfoId),this::buildObjectFromResultSet);
    }

    public void updateReIssueCardInfoId(long id,long reIssueCardInfoId){
        ConnectionAndStatementUtil.getWritePrepareStatement(ps->{
            try{
              ps.setLong(1,reIssueCardInfoId);
              ps.setLong(2,id);
              logger.debug(ps);
              ps.executeUpdate();
            }catch (SQLException ex){
                logger.error(ex);
            }
        },updateReIssueCardInfoId);
    }

    public Lost_card_infoDTO getByReIssueCardInfoId(long reIssueCardInfoId){
        String sql = String.format(getByReIssueCardInfoId,reIssueCardInfoId);
        return ConnectionAndStatementUtil.getT(sql,this::buildObjectFromResultSet);
    }

    public List<Lost_card_infoDTO> getByReIssueCardInfoIds(List<Long> reIssueCardInfoIds){
        return ConnectionAndStatementUtil.getDTOListByNumbers(getByReIssueCardInfoIds,reIssueCardInfoIds,this::buildObjectFromResultSet);
    }

    public List<Lost_card_infoDTO> getLostCardWhichNotIssued(List<Long>cardInfoIds,UserDTO userDTO){
        String sql;
        if (userDTO.roleID == RoleEnum.EMPLOYEE_OF_PARLIAMENT.getRoleId()){
            sql = getLostCardWhichNotIssuedForEmployee.replace("{employee_records_id}",String.valueOf(userDTO.employee_record_id));
        }else{
            sql = getLostCardWhichNotIssued;
        }
        return ConnectionAndStatementUtil.getDTOListByNumbers(sql,cardInfoIds,this::buildObjectFromResultSet);
    }

    public void resetReIssueId(long reissueId){
        String sql = String.format(resetReIssueIdSQL,reissueId);
        ConnectionAndStatementUtil.getWriteStatement(st->{
            try {
                st.executeUpdate(sql);
            } catch (SQLException ex) {
                ex.printStackTrace();
                logger.error(ex);
            }
        });
    }
}