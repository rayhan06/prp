package lost_card_info;

import util.CommonEmployeeDTO;


public class Lost_card_infoDTO extends CommonEmployeeDTO {
    public long cardInfoId = 0;
    public long reissueCardInfoId = 0;
    public long gdCopyFileDropzone = 0;
    public long moneyReceiptFileDropzone = 0;
    public long insertedBy = 0;
    public long insertionTime = 0;
    public long modifiedBy = 0;
    public int cardCat = 0;

    @Override
    public String toString() {
        return "Lost_card_infoDTO{" +
                "cardInfoId=" + cardInfoId +
                ", gdCopyFileDropzone=" + gdCopyFileDropzone +
                ", moneyReceiptFileDropzone=" + moneyReceiptFileDropzone +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                ", employeeRecordId=" + employeeRecordsId +
                ", iD=" + iD +
                ", isDeleted=" + isDeleted +
                '}';
    }
}