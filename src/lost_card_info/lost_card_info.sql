CREATE TABLE lost_card_info
(
    ID                          bigint(20) primary key,
    card_info_id                bigint(20)           not null,
    gd_copy_file_dropzone       bigint(20)           null,
    money_receipt_file_dropzone bigint(20)           null,
    isDeleted                   int(11)    default 0 null,
    inserted_by                 bigint(20) default 0 null,
    insertion_time              bigint(20) default 0 null,
    modified_by                 bigint(20) default 0 null,
    lastModificationTime        bigint(20) default 0 null
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;