package lost_card_info;

import card_info.CardApprovalNotification;
import card_info.CardInfoDAO;
import card_info.CardStatusEnum;
import card_info.Card_infoDTO;
import common.BaseServlet;
import common.RoleEnum;
import files.FilesDAO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import role.PermissionRepository;
import task_type_approval_path.TaskTypeApprovalPathDTO;
import task_type_approval_path.TaskTypeApprovalPathRepository;
import task_type_level.TaskTypeLevelDTO;
import task_type_level.TaskTypeLevelRepository;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@SuppressWarnings({"unused"})
@WebServlet("/Lost_card_infoServlet")
@MultipartConfig
public class Lost_card_infoServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(Lost_card_infoServlet.class);

    private final Lost_card_infoDAO lostCardInfoDAO = Lost_card_infoDAO.getInstance();

    private final FilesDAO filesDAO = new FilesDAO();

    @Override
    public String getTableName() {
        return lostCardInfoDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Lost_card_infoServlet";
    }

    @Override
    public Lost_card_infoDAO getCommonDAOService() {
        return lostCardInfoDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        long lostCardInfoId = Long.parseLong(Jsoup.clean(request.getParameter("cardInfoId"), Whitelist.simpleText()));
        if(addFlag){
            Lost_card_infoDTO lostCardInfoDTO = lostCardInfoDAO.getByCardInfoId(lostCardInfoId);
            if(lostCardInfoDTO != null){
                throw new Exception("Card has been already reported as lost card");
            }
        }
        Card_infoDTO cardInfoDTO = CardInfoDAO.getInstance().getDTOFromID(lostCardInfoId);
        if(cardInfoDTO == null){
            throw new Exception("Card info is not found");
        }
        long currentTime = System.currentTimeMillis();
        Lost_card_infoDTO lost_card_infoDTO;
        if (addFlag) {
            lost_card_infoDTO = new Lost_card_infoDTO();
            lost_card_infoDTO.insertionTime = currentTime;
            lost_card_infoDTO.insertedBy = userDTO.employee_record_id;
            lost_card_infoDTO.employeeRecordsId = cardInfoDTO.employeeRecordsId;
            lost_card_infoDTO.cardInfoId = lostCardInfoId;
            lost_card_infoDTO.cardCat = cardInfoDTO.cardCat;
        } else {
            lost_card_infoDTO = lostCardInfoDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        lost_card_infoDTO.modifiedBy = userDTO.employee_record_id;
        lost_card_infoDTO.lastModificationTime = currentTime;

        String Value;

        Value = request.getParameter("gdCopyFileDropzone");
        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {
            lost_card_infoDTO.gdCopyFileDropzone = Long.parseLong(Value);
            if (!addFlag) {
                String gdCopyFileDropzoneFilesToDelete = request.getParameter("gdCopyFileDropzoneFilesToDelete");
                String[] deleteArray = gdCopyFileDropzoneFilesToDelete.split(",");
                for (int i = 1; i < deleteArray.length; i++) {
                    filesDAO.delete(Long.parseLong(deleteArray[i]));
                }
            }
        }

        Value = request.getParameter("moneyReceiptFileDropzone");
        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {
            lost_card_infoDTO.moneyReceiptFileDropzone = Long.parseLong(Value);
            if (!addFlag) {
                String moneyReceiptFileDropzoneFilesToDelete = request.getParameter("moneyReceiptFileDropzoneFilesToDelete");
                String[] deleteArray = moneyReceiptFileDropzoneFilesToDelete.split(",");
                for (int i = 1; i < deleteArray.length; i++) {
                    filesDAO.delete(Long.parseLong(deleteArray[i]));
                }
            }
        }
        if(addFlag){
            lostCardInfoDAO.add(lost_card_infoDTO);
        }else {
            lostCardInfoDAO.update(lost_card_infoDTO);
        }
        return lost_card_infoDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.LOST_CARD_INFO_SEARCH};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Lost_card_infoServlet.class;
    }

    @Override
    public String getEditRedirectURL(HttpServletRequest request,CommonDTO commonDTO) {
        return "Card_infoServlet?actionType=reissue_card&lostCardId=" + commonDTO.iD;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        try {
            if ("ajax_reportLostCard".equals(request.getParameter("actionType"))) {
                String resJson;
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LOST_CARD_INFO_SEARCH)) {
                    long lostCardInfoId = Long.parseLong(Jsoup.clean(request.getParameter("cardInfoId"), Whitelist.simpleText()));
                    Card_infoDTO cardInfoDTO = CardInfoDAO.getInstance().getDTOFromID(lostCardInfoId);
                    if (userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId() ||
                            userDTO.employee_record_id == cardInfoDTO.insertedBy || userDTO.employee_record_id == cardInfoDTO.employeeRecordsId) {
                        Lost_card_infoDTO lost_card_infoDTO = (Lost_card_infoDTO) addT(request, true, userDTO);
                        CardInfoDAO.getInstance().updateCardStatus(lost_card_infoDTO.cardInfoId,
                                CardStatusEnum.LOST.getValue(), lost_card_infoDTO.modifiedBy, lost_card_infoDTO.lastModificationTime);
                        resJson = "{\"success\": true,\"iD\":" + lost_card_infoDTO.iD + "}";
                        PrintWriter out = response.getWriter();
                        response.setContentType("application/json");
                        response.setCharacterEncoding("UTF-8");
                        out.print(resJson);
                        out.flush();
                        sendNotificationToLastLevel(cardInfoDTO);
                        return;
                    }
                } else {
                    logger.debug("user has no menu permission. menu constant is " + MenuConstants.LOST_CARD_INFO_SEARCH + " user role Id is : " + userDTO.roleID);
                }
            } else {
                super.doPost(request, response);
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void sendNotificationToLastLevel(Card_infoDTO card_infoDTO){
        TaskTypeLevelDTO taskTypeLevelDTO = TaskTypeLevelRepository.getInstance().getIdForMaxLevel(card_infoDTO.taskTypeId);
        if(taskTypeLevelDTO!=null){
            List<TaskTypeApprovalPathDTO> taskTypeApprovalPathDTOList = TaskTypeApprovalPathRepository.getInstance().getByTaskTypeLevelId(taskTypeLevelDTO.iD);
            if(taskTypeApprovalPathDTOList!=null && taskTypeApprovalPathDTOList.size()>0){
                List<Long> organograms = taskTypeApprovalPathDTOList.stream()
                        .map(e->e.officeUnitOrganogramId)
                        .distinct()
                        .collect(Collectors.toList());
                CardApprovalNotification.getInstance().sendLostCardNotification(organograms,card_infoDTO);
            }
        }
    }

    private boolean requestedOwnActiveCard(HttpServletRequest request, UserDTO userDTO) {
        long cardInfoId = Long.parseLong(Jsoup.clean(request.getParameter("cardInfoId"), Whitelist.simpleText()));
        long employeeRecordId = userDTO.employee_record_id;

        List<Card_infoDTO> cardInfoDTOList = CardInfoDAO.getInstance().getByEmployeeId(employeeRecordId);
        if (cardInfoDTOList == null || cardInfoDTOList.size() == 0) {
            return false;
        }

        Set<Long> activeCardInfoIds = cardInfoDTOList.stream()
                .filter(cardInfoDTO -> cardInfoDTO.cardStatusCat == CardStatusEnum.ACTIVATE.getValue())
                .map(cardInfoDTO -> cardInfoDTO.iD)
                .collect(Collectors.toSet());

        return activeCardInfoIds.contains(cardInfoId);
    }
}

