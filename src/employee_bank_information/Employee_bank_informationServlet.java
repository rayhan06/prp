package employee_bank_information;

import bank_name.Bank_nameRepository;
import common.BaseServlet;
import common.EmployeeServletService;
import employee_records.Employee_recordsRepository;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.CatRepository;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@SuppressWarnings("Duplicates")
@WebServlet("/Employee_bank_informationServlet")
@MultipartConfig
public class Employee_bank_informationServlet extends BaseServlet implements EmployeeServletService{
    private static final long serialVersionUID = 1L;

    private final Employee_bank_informationDAO employee_bank_informationDAO = Employee_bank_informationDAO.getInstance();

    @Override
    public String getTableName() {
        return employee_bank_informationDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Employee_bank_informationServlet";
    }

    @Override
    public Employee_bank_informationDAO getCommonDAOService() {
        return employee_bank_informationDAO;
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request,CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request,6,"bank_info");
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request,CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request,6,"bank_info");
    }

    @Override
    public String getDeleteRedirectURL(HttpServletRequest request) {
        return getAddOrEditOrDeleteRedirectURL(request,6,"bank_info");
    }

    @Override
    public boolean getEditPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getViewPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getEditPermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getAddPermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    @Override
    public boolean getAddPagePermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Employee_bank_informationDTO employee_bank_informationDTO;
        long currentTime = System.currentTimeMillis();
        if (addFlag) {
            employee_bank_informationDTO = new Employee_bank_informationDTO();
            long empId;
            try{
                empId = Long.parseLong(request.getParameter("empId"));
            }catch (Exception ex){
                ex.printStackTrace();
                throw new Exception(isLangEng?"empId is not found":"empId পাওয়া যায়নি");
            }
            if(Employee_recordsRepository.getInstance().getById(empId) == null){
                throw new Exception(isLangEng?"Employee information is not found":"কর্মকর্তার তথ্য পাওয়া যায়নি");
            }
            employee_bank_informationDTO.employeeRecordsId = empId;
            employee_bank_informationDTO.insertedByUserId = userDTO.ID;
            employee_bank_informationDTO.insertionDate = currentTime;
        } else {
            employee_bank_informationDTO = employee_bank_informationDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        employee_bank_informationDTO.modifiedBy = String.valueOf(userDTO.ID);
        employee_bank_informationDTO.lastModificationTime = currentTime;
        employee_bank_informationDTO.bankNameType = Long.parseLong(request.getParameter("bankNameType"));
        if (Bank_nameRepository.getInstance().getDTOByID(employee_bank_informationDTO.bankNameType) == null) {
            throw new Exception(isLangEng?"Please enter bank name":"অনুগ্রহ করে ব্যাংকের নাম দিন");
        }
        employee_bank_informationDTO.branchName = Jsoup.clean(request.getParameter("branchName"), Whitelist.simpleText()).trim();
        if (employee_bank_informationDTO.branchName.length() == 0) {
            throw new Exception(isLangEng?"Please enter branch name":"অনুগ্রহ করে ব্রাঞ্চের নাম দিন");
        }
        employee_bank_informationDTO.accountCat = Integer.parseInt(Jsoup.clean(request.getParameter("accountCat"), Whitelist.simpleText()));
        if (CatRepository.getInstance().getCategoryLanguageModel("account", employee_bank_informationDTO.accountCat) == null) {
            throw new Exception(isLangEng?"Please select valid account type":"অনুগ্রহ করে সঠিক একাউন্ট ধরন বাছাই করুন");
        }
        employee_bank_informationDTO.bankAccountNumber = Jsoup.clean(request.getParameter("bankAccountNumber"), Whitelist.simpleText()).trim();
        if (employee_bank_informationDTO.bankAccountNumber.length() == 0) {
            throw new Exception(isLangEng?"Please enter account number":"অনুগ্রহ করে একাউন্ট নাম্বার দিন");
        }

        if (addFlag) {
            employee_bank_informationDAO.add(employee_bank_informationDTO);
        } else {
            employee_bank_informationDAO.update(employee_bank_informationDTO);
        }
        return employee_bank_informationDTO;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        search(request);
        super.doGet(request, response);
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Employee_bank_informationServlet.class;
    }

    @Override
    protected void deleteT(List<Long> ids, UserDTO userDTO) throws Exception {
        employee_bank_informationDAO.delete(ids,userDTO);
    }
}