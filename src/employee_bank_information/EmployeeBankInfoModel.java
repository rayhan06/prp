package employee_bank_information;

public class EmployeeBankInfoModel {
    public Employee_bank_informationDTO dto;
    public String bankNameEng;
    public String bankNameBan;
    public String accountCatEng;
    public String accountCatBan;
    public String accountNoBan;

    @Override
    public String toString() {
        return "EmployeeBankInfoModel{" +
                "dto=" + dto +
                ", bankNameEng='" + bankNameEng + '\'' +
                ", bankNameBan='" + bankNameBan + '\'' +
                ", accountCatEng='" + accountCatEng + '\'' +
                ", accountCatBan='" + accountCatBan + '\'' +
                ", accountNoBan='" + accountNoBan + '\'' +
                '}';
    }
}
