package employee_bank_information;

import util.CommonEmployeeDTO;


public class Employee_bank_informationDTO extends CommonEmployeeDTO {
    public static final int SAVINGS_ACCOUNT = 1;

    public long bankNameType = 0;
    public String branchName = "";
    public int accountCat = 0;
    public String bankAccountNumber = "";
    public long insertedByUserId = 0;
    public long insertionDate = 0;
    public String modifiedBy = "";

    @Override
    public String toString() {
        return "$Employee_bank_informationDTO[" +
                " iD = " + iD +
                " employeeRecordsId = " + employeeRecordsId +
                " bankNameType = " + bankNameType +
                " branchName = " + branchName +
                " accountCat = " + accountCat +
                " bankAccountNumber = " + bankAccountNumber +
                " insertedByUserId = " + insertedByUserId +
                " insertionDate = " + insertionDate +
                " modifiedBy = " + modifiedBy +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}