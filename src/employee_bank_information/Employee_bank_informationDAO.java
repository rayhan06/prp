package employee_bank_information;

import bank_name.Bank_nameDAO;
import common.ConnectionAndStatementUtil;
import common.EmployeeCommonDAOService;
import common.NameDTO;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import pb.Utils;
import user.UserDTO;
import util.CommonDTO;
import util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"unused", "Duplicates"})
public class Employee_bank_informationDAO implements EmployeeCommonDAOService<Employee_bank_informationDTO> {

    private final Logger logger = Logger.getLogger(Employee_bank_informationDAO.class);

    private final String addQuery = "INSERT INTO {tableName} (employee_records_id,bank_name_type,branch_name,account_cat,bank_account_number,"
            .concat(" inserted_by_user_id,insertion_date,modified_by,search_column,lastModificationTime,isDeleted,ID)")
            .concat(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");

    private final String deleteQuery = "UPDATE employee_bank_information SET deleted_bank_account_number  = bank_account_number, " +
            " bank_account_number = null,isDeleted=1, lastModificationTime = %d, modified_by = %d WHERE id in (%s)";

    private static final String updateQuery = "UPDATE {tableName} SET employee_records_id=?,bank_name_type=?,branch_name=?,account_cat=?,bank_account_number=?,"
            .concat(" inserted_by_user_id=?,insertion_date=?,modified_by=?,search_column=?,lastModificationTime = ? WHERE ID = ?");

    private static final String getByEmployeeId = "SELECT * FROM employee_bank_information WHERE employee_records_id = ? AND isDeleted = 0";

    private final Map<String, String> searchMap = new HashMap<>();

    private Employee_bank_informationDAO() {
        searchMap.put("bank_name_type", " and (bank_name_type = ?)");
        searchMap.put("branch_name", " and (branch_name = ?)");
        searchMap.put("account_cat", " and (account_cat = ?)");
        searchMap.put("bank_account_number", " and (bank_account_number = ?)");
        searchMap.put("employee_records_id_internal", " and (employee_records_id = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private static class LazyLoader {
        static final Employee_bank_informationDAO INSTANCE = new Employee_bank_informationDAO();
    }

    public static Employee_bank_informationDAO getInstance() {
        return Employee_bank_informationDAO.LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Employee_bank_informationDTO employee_bank_informationDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(employee_bank_informationDTO);
        ps.setObject(++index, employee_bank_informationDTO.employeeRecordsId);
        ps.setObject(++index, employee_bank_informationDTO.bankNameType);
        ps.setObject(++index, employee_bank_informationDTO.branchName);
        ps.setObject(++index, employee_bank_informationDTO.accountCat);
        ps.setObject(++index, employee_bank_informationDTO.bankAccountNumber);
        ps.setObject(++index, employee_bank_informationDTO.insertedByUserId);
        ps.setObject(++index, employee_bank_informationDTO.insertionDate);
        ps.setObject(++index, employee_bank_informationDTO.modifiedBy);
        ps.setObject(++index, employee_bank_informationDTO.searchColumn);
        ps.setObject(++index, System.currentTimeMillis());
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, employee_bank_informationDTO.iD);
    }

    public void setSearchColumn(Employee_bank_informationDTO employee_bank_informationDTO) {
        employee_bank_informationDTO.searchColumn = "";
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employee_bank_informationDTO.employeeRecordsId);
        if (employeeRecordsDTO != null) {
            if (employeeRecordsDTO.nameEng != null && employeeRecordsDTO.nameEng.trim().length() > 0) {
                employee_bank_informationDTO.searchColumn += employeeRecordsDTO.nameEng.trim();
            }
            if (employeeRecordsDTO.nameBng != null && employeeRecordsDTO.nameBng.trim().length() > 0) {
                employee_bank_informationDTO.searchColumn += employeeRecordsDTO.nameBng.trim();
            }
        }
        NameDTO bankNameDTO = Bank_nameDAO.getInstance().getDTOFromID(employee_bank_informationDTO.bankNameType);
        employee_bank_informationDTO.searchColumn += bankNameDTO.nameEn;
        employee_bank_informationDTO.searchColumn += bankNameDTO.nameBn;
        CategoryLanguageModel languageModel = CatRepository.getInstance().getCategoryLanguageModel("account", employee_bank_informationDTO.accountCat);
        if (languageModel != null) {
            employee_bank_informationDTO.searchColumn += languageModel.englishText;
            employee_bank_informationDTO.searchColumn += languageModel.banglaText;
        }
        employee_bank_informationDTO.searchColumn += employee_bank_informationDTO.branchName;
    }

    @Override
    public Employee_bank_informationDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Employee_bank_informationDTO employee_bank_informationDTO = new Employee_bank_informationDTO();
            employee_bank_informationDTO.iD = rs.getLong("ID");
            employee_bank_informationDTO.employeeRecordsId = rs.getLong("employee_records_id");
            employee_bank_informationDTO.bankNameType = rs.getLong("bank_name_type");
            employee_bank_informationDTO.branchName = rs.getString("branch_name");
            employee_bank_informationDTO.accountCat = rs.getInt("account_cat");
            employee_bank_informationDTO.bankAccountNumber = rs.getString("bank_account_number");
            employee_bank_informationDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
            employee_bank_informationDTO.insertionDate = rs.getLong("insertion_date");
            employee_bank_informationDTO.modifiedBy = rs.getString("modified_by");
            employee_bank_informationDTO.searchColumn = rs.getString("search_column");
            employee_bank_informationDTO.isDeleted = rs.getInt("isDeleted");
            employee_bank_informationDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return employee_bank_informationDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "employee_bank_information";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_bank_informationDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_bank_informationDTO) commonDTO, updateQuery, false);
    }

    public List<EmployeeBankInfoModel> getEmployeeBankInfoModelListByEmployeeId(long employeeId) {
        return getByEmployeeId(employeeId)
                .stream()
                .map(this::buildEmployeeBankInfoModel)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public Employee_bank_informationDTO getEmployeeSavingsAccountInfoByEmployeeId(long employeeId) {
        return getByEmployeeId(employeeId)
                .stream()
                .filter(Objects::nonNull)
                .filter(dto -> dto.accountCat == Employee_bank_informationDTO.SAVINGS_ACCOUNT)
                .findAny()
                .orElse(null);
    }

    private EmployeeBankInfoModel buildEmployeeBankInfoModel(Employee_bank_informationDTO dto) {
        CategoryLanguageModel languageModel = CatRepository.getInstance().getCategoryLanguageModel("account", dto.accountCat);
        if (languageModel == null) {
            return null;
        }
        EmployeeBankInfoModel model = new EmployeeBankInfoModel();
        model.dto = dto;
        NameDTO bankNameDTO = Bank_nameDAO.getInstance().getDTOFromID(dto.bankNameType);
        model.bankNameEng = bankNameDTO.nameEn;
        model.bankNameBan = bankNameDTO.nameBn;
        model.accountCatEng = languageModel.englishText;
        model.accountCatBan = languageModel.banglaText;
        model.accountNoBan = StringUtils.convertToBanNumber(dto.bankAccountNumber);
        return model;
    }

    public void delete(List<Long> ids, UserDTO userDTO) throws Exception {
        if (ids == null || ids.isEmpty()) {
            return;
        }

        String str = ids.stream()
                .map(String::valueOf)
                .collect(Collectors.joining(","));

        Utils.wrapWithAtomicReference(ar -> ConnectionAndStatementUtil.getWriteStatement(st -> {
            String sql = String.format(deleteQuery, Calendar.getInstance().getTimeInMillis(), userDTO.ID, str);
            logger.info("sql : " + sql);
            try {
                st.executeUpdate(sql);
            } catch (SQLException e) {
                logger.error(e);
                ar.set(e);
            }
        }));
    }
}