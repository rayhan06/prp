package cum_report;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;
import login.LoginDTO;
import medical_inventory_lot.MedicalInventoryInDTO;
import pbReport.ReportService;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.ReportRequestHandler;
import org.apache.commons.lang3.tuple.ImmutablePair;

@WebServlet("/Cum_report_Servlet")
public class Cum_report_Servlet   extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
		{
			{"criteria","","transaction_date",">=","","long","","",1 + "","startDate", LC.HM_START_DATE + ""},		
			{"criteria","","transaction_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},
			{"criteria","medical_transaction","employee_record_id","=","AND","long","","",SessionConstants.OUTSIDER_PATIENT_ERID + "","other",  ""},
			{"criteria","","transaction_type","in","AND","String","","", MedicalInventoryInDTO.TR_RECEIVE_MEDICINE + ", " +  MedicalInventoryInDTO.TR_RETURN,"none", ""},
			{"criteria","","medical_item_cat","=","AND","int","","","0","none", ""}
			
		};
		
		String[][] Display =
		{
			{"display","","other_office_id","other_office",""},
			{"display","","COUNT(DISTINCT prescription_details_id)","int",""},
			{"display","","sum(quantity * unit_price)","double",""}
		};
		
		String GroupBy = " other_office_id ";
		String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Cum_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");
		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "medical_transaction left join prescription_details on medical_transaction.prescription_details_id = prescription_details.id";

		Display[0][4] = LM.getText(LC.HM_OFFICE, loginDTO);
		Display[1][4] = language.equalsIgnoreCase("english")?"Prescriptions Used":"জমাকৃত প্রেসক্রিপশন";
		Display[2][4] = LM.getText(LC.HM_COST, loginDTO);


		
		String reportName = language.equalsIgnoreCase("english")?"Common User Medical Report":"সাধারণ জনগণের মেডিকেল রিপোর্ট";

		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(1, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(2, ReportRequestHandler.RIGHT_ALIGN_FLOAT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "cum_report",
				MenuConstants.CUMREPORT, language, reportName, "cum_report");
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}

}
