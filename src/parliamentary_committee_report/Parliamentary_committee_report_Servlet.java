package parliamentary_committee_report;


import language.LC;
import language.LM;
import login.LoginDTO;
import pbReport.PBReportUtils;
import pbReport.ReportCommonService;
import permission.MenuConstants;
import role.PermissionRepository;
import user.UserDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@WebServlet("/Parliamentary_committee_report_Servlet")
public class Parliamentary_committee_report_Servlet extends HttpServlet implements ReportCommonService {
    private static final long serialVersionUID = 1L;

    private static final Set<String> searchParam = new HashSet<>(Arrays.asList(
            "electionDetailsId", "committeesId", "electionConstituencyId", "nameEng", "nameBng"
    ));

    private final String[][] Display =
            {
                    {"display", "cm", "committees_id", "parliamentary_committee", ""},
                    {"display", "cm", "employee_records_id", "employee_records_id", ""},
                    {"display", "cm", "election_details_id", "parliament_number", ""},
                    {"display", "ewm", "election_constituency_id", "election_constituency", ""},
                    {"display", "cm", "committees_role_cat", "cat", ""},
                    {"display", "cm", "start_date", "date", ""},
                    {"display", "cm", "end_date", "date", ""}
            };

    private String[][] Criteria;

    private final Map<String, String[]> stringMap = new HashMap<>();

    public Parliamentary_committee_report_Servlet() {
        stringMap.put("electionDetailsId", new String[]{"criteria", "cm", "election_details_id", "=", "AND", "long", "", "", ""
                , "electionDetailsId", "electionDetailsId", String.valueOf(LC.PARLIAMENTARY_COMMITTEE_REPORT_WHERE_ELECTIONDETAILSID), "election_details", null, "true", "1"});
        stringMap.put("electionConstituencyId", new String[]{"criteria", "ewm", "election_constituency_id", "=", "AND", "long", "", "", "any"
                , "electionConstituencyId", "electionConstituencyId", String.valueOf(LC.PARLIAMENTARY_COMMITTEE_REPORT_WHERE_ELECTIONCONSTITUENCYID), "election_constituency", null, "true", "2"});
        stringMap.put("committeesId", new String[]{"criteria", "cm", "committees_id", "=", "AND", "long", "", "", "any",
                "committeesId", String.valueOf(LC.PARLIAMENTARY_COMMITTEE_REPORT_WHERE_COMMITTEESID), "text", null, "true", "3"});
        stringMap.put("nameEng", new String[]{"criteria", "er", "name_eng", "Like", "AND", "String", "", "", "any", "nameEng",
                "nameEng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEENG), "text", null, "true", "4"});
        stringMap.put("nameBng", new String[]{"criteria", "er", "name_bng", "Like", "AND", "String", "", "", "any", "nameBng",
                "nameBng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG), "text", null, "true", "5"});
        stringMap.put("isDeleted", new String[]{"criteria", "cm", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted", null, null, null, null, null, null});
    }

    private static final String sql = " committees_mapping cm JOIN election_wise_mp ewm ON cm.election_wise_mp_id = ewm.ID  left join employee_records er on ewm.employee_records_id = er.id";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;

        Display[0][4] = LM.getText(LC.PARLIAMENTARY_COMMITTEE_REPORT_SELECT_COMMITTEESID, loginDTO);
        Display[1][4] = LM.getText(LC.PARLIAMENTARY_COMMITTEE_REPORT_SELECT_EMPLOYEERECORDSID, loginDTO);
        Display[2][4] = LM.getText(LC.PARLIAMENTARY_COMMITTEE_REPORT_SELECT_ELECTIONDETAILSID, loginDTO);
        Display[3][4] = LM.getText(LC.PARLIAMENTARY_COMMITTEE_REPORT_SELECT_ELECTIONCONSTITUENCYID, loginDTO);
        Display[4][4] = LM.getText(LC.PARLIAMENTARY_COMMITTEE_REPORT_SELECT_COMMITTEESROLECAT, loginDTO);
        Display[5][4] = LM.getText(LC.PARLIAMENTARY_COMMITTEE_REPORT_SELECT_STARTDATE, loginDTO);
        Display[6][4] = LM.getText(LC.PARLIAMENTARY_COMMITTEE_REPORT_SELECT_ENDDATE, loginDTO);

        Set<String> inputs = PBReportUtils.prepareInputSets(searchParam, request);
        inputs.add("isDeleted");

        Criteria = PBReportUtils.prepareCriteria(inputs, stringMap);

        if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENTARY_COMMITTEE_REPORT_DETAILS)) {
            reportGenerate(request, response);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    @Override
    public String[][] getCriteria() {
        return Criteria;
    }

    @Override
    public String[][] getDisplay() {
        return Display;
    }

    @Override
    public String getSQL() {
        return sql;
    }

    @Override
    public int getLCForReportName() {
        return LC.PARLIAMENTARY_COMMITTEE_REPORT_OTHER_PARLIAMENTARY_COMMITTEE_REPORT;
    }

    @Override
    public String getFileName() {
        return "parliamentary_committee_report";
    }

    @Override
    public String getTableName() {
        return "parliamentary_committee_report";
    }
}