package payroll_daily_configuration;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Payroll_daily_configurationDAO implements CommonDAOService<Payroll_daily_configurationDTO> {
    private static final Logger logger = Logger.getLogger(Payroll_daily_configurationDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (organogram_id,organogram_key,daily_amount,tax_deduction_amount_type,tax_deduction_amount,modified_by,lastModificationTime,"
                    .concat("inserted_by,insertion_time,isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery =
            "UPDATE {tableName} SET organogram_id=?,organogram_key=?,daily_amount=?,tax_deduction_amount_type=?,tax_deduction_amount=?,"
                    .concat("modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final String getByOrganogramKey =
            "SELECT * FROM payroll_daily_configuration WHERE organogram_key='%s' AND isDeleted=0";

    private final Map<String, String> searchMap = new HashMap<>();

    private Payroll_daily_configurationDAO() {
        searchMap.put("organogram_key", " and (organogram_key = ?)");
    }

    private static class LazyLoader {
        static final Payroll_daily_configurationDAO INSTANCE = new Payroll_daily_configurationDAO();
    }

    public static Payroll_daily_configurationDAO getInstance() {
        return Payroll_daily_configurationDAO.LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Payroll_daily_configurationDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.organogramId);
        ps.setString(++index, dto.organogramKey);
        ps.setInt(++index, dto.dailyAmount);
        ps.setInt(++index, dto.taxDeductionAmountType);
        ps.setInt(++index, dto.taxDeductionAmount);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Payroll_daily_configurationDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Payroll_daily_configurationDTO dto = new Payroll_daily_configurationDTO();
            dto.iD = rs.getLong("ID");
            dto.organogramKey = rs.getString("organogram_key");
            dto.organogramId = rs.getLong("organogram_id");
            dto.dailyAmount = rs.getInt("daily_amount");
            dto.taxDeductionAmountType = rs.getInt("tax_deduction_amount_type");
            dto.taxDeductionAmount = rs.getInt("tax_deduction_amount");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Payroll_daily_configurationDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "payroll_daily_configuration";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Payroll_daily_configurationDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Payroll_daily_configurationDTO) commonDTO, updateQuery, false);
    }

    public List<Payroll_daily_configurationDTO> getByOrganogramKey(String organogramKey) {
        return getDTOs(String.format(getByOrganogramKey, organogramKey.trim()));
    }
}