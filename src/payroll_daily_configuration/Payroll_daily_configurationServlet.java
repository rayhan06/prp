package payroll_daily_configuration;

import common.BaseServlet;
import office_unit_organograms.SameDesignationGroup;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;


@WebServlet("/Payroll_daily_configurationServlet")
@MultipartConfig
@SuppressWarnings({"Duplicates"})
public class Payroll_daily_configurationServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Payroll_daily_configurationServlet.class);
    private final Payroll_daily_configurationDAO payroll_daily_configurationDAO = Payroll_daily_configurationDAO.getInstance();

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Payroll_daily_configurationDTO dailyConfigDTO;
        long curTime = System.currentTimeMillis();
        if (addFlag) {
            dailyConfigDTO = new Payroll_daily_configurationDTO();
            dailyConfigDTO.insertedBy = userDTO.ID;
            dailyConfigDTO.insertionTime = curTime;
        } else {
            dailyConfigDTO = Payroll_daily_configurationDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        dailyConfigDTO.organogramId = Long.parseLong(Jsoup.clean(request.getParameter("organogramId"), Whitelist.simpleText()));
        dailyConfigDTO.organogramKey = new SameDesignationGroup(dailyConfigDTO.organogramId).organogramKey;
        dailyConfigDTO.dailyAmount = Integer.parseInt(Jsoup.clean(request.getParameter("dailyAmount"), Whitelist.simpleText()));
        dailyConfigDTO.taxDeductionAmountType = Integer.parseInt(Jsoup.clean(request.getParameter("taxDeductionAmountType"), Whitelist.simpleText()));
        dailyConfigDTO.taxDeductionAmount = Integer.parseInt(Jsoup.clean(request.getParameter("taxDeductionAmount"), Whitelist.simpleText()));
        dailyConfigDTO.modifiedBy = userDTO.ID;
        dailyConfigDTO.lastModificationTime = curTime;
        boolean existConfigure = false;
        Payroll_daily_configurationDTO tmpDTO = Payroll_daily_configurationRepository.getInstance().getByOrganogramKey(dailyConfigDTO.organogramKey);
        if (tmpDTO != null) {
            dailyConfigDTO.iD = tmpDTO.iD;
            existConfigure = true;
        }
        if (addFlag && !existConfigure) {
            payroll_daily_configurationDAO.add(dailyConfigDTO);
        } else {
            payroll_daily_configurationDAO.update(dailyConfigDTO);
        }
        return dailyConfigDTO;
    }

    @Override
    public String getTableName() {
        return Payroll_daily_configurationDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Payroll_daily_configurationServlet";
    }

    @Override
    public Payroll_daily_configurationDAO getCommonDAOService() {
        return payroll_daily_configurationDAO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PAYROLL_DAILY_CONFIGURATION_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PAYROLL_DAILY_CONFIGURATION_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PAYROLL_DAILY_CONFIGURATION_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Payroll_daily_configurationServlet.class;
    }
}

