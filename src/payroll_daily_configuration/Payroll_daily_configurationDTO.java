package payroll_daily_configuration;

import util.CommonDTO;


public class Payroll_daily_configurationDTO extends CommonDTO {
    public String organogramKey = "";
    public long organogramId = 0;
    public int dailyAmount = -0;
    public int taxDeductionAmountType = 0;
    public int taxDeductionAmount = 0;
    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;

    @Override
    public String toString() {
        return "$Payroll_daily_configurationDTO[" +
                " iD = " + iD +
                " organogramKey = " + organogramKey +
                " dailyAmount = " + dailyAmount +
                " taxDeductionAmountType = " + taxDeductionAmountType +
                " taxDeductionAmount = " + taxDeductionAmount +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " isDeleted = " + isDeleted +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }
}