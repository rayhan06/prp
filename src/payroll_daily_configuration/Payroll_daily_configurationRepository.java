package payroll_daily_configuration;

import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class Payroll_daily_configurationRepository implements Repository {
    private final Payroll_daily_configurationDAO payroll_daily_configurationDAO;
    private final Map<Long, Payroll_daily_configurationDTO> mapById;
    private final Map<String, Payroll_daily_configurationDTO> mapByOrganogramKey;

    private Payroll_daily_configurationRepository() {
        payroll_daily_configurationDAO = Payroll_daily_configurationDAO.getInstance();
        mapById = new ConcurrentHashMap<>();
        mapByOrganogramKey = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Payroll_daily_configurationRepository INSTANCE = new Payroll_daily_configurationRepository();
    }

    public static Payroll_daily_configurationRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        List<Payroll_daily_configurationDTO> list = payroll_daily_configurationDAO.getAllDTOs(true);
        if (list != null && list.size() > 0) {
            list.forEach(dto -> {
                mapById.put(dto.iD, dto);
                mapByOrganogramKey.put(dto.organogramKey, dto);
            });
        }
    }

    public List<Payroll_daily_configurationDTO> getPayroll_daily_configurationList() {
        return new ArrayList<>(this.mapById.values());
    }

    public Payroll_daily_configurationDTO getById(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "PDCR")) {
                if (mapById.get(id) == null) {
                    Payroll_daily_configurationDTO dto = payroll_daily_configurationDAO.getDTOFromIdDeletedOrNot(id);
                    if (dto != null) {
                        mapById.put(dto.iD, dto);
                        if (dto.isDeleted == 0) {
                            mapByOrganogramKey.put(dto.organogramKey, dto);
                        }
                    }
                }
            }

        }
        return mapById.get(id);
    }

    public Payroll_daily_configurationDTO getByOrganogramKey(String organogramKey) {
        String organogramKeyTrim = organogramKey.trim();
        synchronized (LockManager.getLock(organogramKeyTrim + "PDCR")) {
            if (mapByOrganogramKey.get(organogramKeyTrim) == null) {
                List<Payroll_daily_configurationDTO> list = payroll_daily_configurationDAO.getByOrganogramKey(organogramKeyTrim);
                if (list != null && list.size() > 0) {
                    Payroll_daily_configurationDTO dto = list.get(0);
                    mapByOrganogramKey.put(dto.organogramKey, dto);
                    mapById.put(dto.iD, dto);
                }
            }
        }
        return mapByOrganogramKey.get(organogramKeyTrim);
    }

    @Override
    public String getTableName() {
        return "payroll_daily_configuration";
    }
}