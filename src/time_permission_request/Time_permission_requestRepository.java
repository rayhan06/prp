package time_permission_request;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Time_permission_requestRepository implements Repository {
	Time_permission_requestDAO time_permission_requestDAO = null;
	
	public void setDAO(Time_permission_requestDAO time_permission_requestDAO)
	{
		this.time_permission_requestDAO = time_permission_requestDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Time_permission_requestRepository.class);
	Map<Long, Time_permission_requestDTO>mapOfTime_permission_requestDTOToiD;
	Map<Long, Set<Time_permission_requestDTO> >mapOfTime_permission_requestDTOToemployeeRecordsId;
	Map<Integer, Set<Time_permission_requestDTO> >mapOfTime_permission_requestDTOTotimeRequestCat;
	Map<Integer, Set<Time_permission_requestDTO> >mapOfTime_permission_requestDTOToreasonTypeCat;
	Map<Integer, Set<Time_permission_requestDTO> >mapOfTime_permission_requestDTOToisApproved;
	Map<Long, Set<Time_permission_requestDTO> >mapOfTime_permission_requestDTOToapprovalDate;
	Map<Long, Set<Time_permission_requestDTO> >mapOfTime_permission_requestDTOToinsertedBy;
	Map<Long, Set<Time_permission_requestDTO> >mapOfTime_permission_requestDTOTomodifiedBy;
	Map<Integer, Set<Time_permission_requestDTO> >mapOfTime_permission_requestDTOTojobCat;
	Map<Long, Set<Time_permission_requestDTO> >mapOfTime_permission_requestDTOTolastModificationTime;


	static Time_permission_requestRepository instance = null;  
	private Time_permission_requestRepository(){
		mapOfTime_permission_requestDTOToiD = new ConcurrentHashMap<>();
		mapOfTime_permission_requestDTOToemployeeRecordsId = new ConcurrentHashMap<>();
		mapOfTime_permission_requestDTOTotimeRequestCat = new ConcurrentHashMap<>();
		mapOfTime_permission_requestDTOToreasonTypeCat = new ConcurrentHashMap<>();
		mapOfTime_permission_requestDTOToisApproved = new ConcurrentHashMap<>();
		mapOfTime_permission_requestDTOToapprovalDate = new ConcurrentHashMap<>();
		mapOfTime_permission_requestDTOToinsertedBy = new ConcurrentHashMap<>();
		mapOfTime_permission_requestDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfTime_permission_requestDTOTojobCat = new ConcurrentHashMap<>();
		mapOfTime_permission_requestDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Time_permission_requestRepository getInstance(){
		if (instance == null){
			instance = new Time_permission_requestRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(time_permission_requestDAO == null)
		{
			return;
		}
		try {
			List<Time_permission_requestDTO> time_permission_requestDTOs = time_permission_requestDAO.getAllTime_permission_request(reloadAll);
			for(Time_permission_requestDTO time_permission_requestDTO : time_permission_requestDTOs) {
				Time_permission_requestDTO oldTime_permission_requestDTO = getTime_permission_requestDTOByID(time_permission_requestDTO.iD);
				if( oldTime_permission_requestDTO != null ) {
					mapOfTime_permission_requestDTOToiD.remove(oldTime_permission_requestDTO.iD);
				
					if(mapOfTime_permission_requestDTOToemployeeRecordsId.containsKey(oldTime_permission_requestDTO.employeeRecordsId)) {
						mapOfTime_permission_requestDTOToemployeeRecordsId.get(oldTime_permission_requestDTO.employeeRecordsId).remove(oldTime_permission_requestDTO);
					}
					if(mapOfTime_permission_requestDTOToemployeeRecordsId.get(oldTime_permission_requestDTO.employeeRecordsId).isEmpty()) {
						mapOfTime_permission_requestDTOToemployeeRecordsId.remove(oldTime_permission_requestDTO.employeeRecordsId);
					}
					
					if(mapOfTime_permission_requestDTOTotimeRequestCat.containsKey(oldTime_permission_requestDTO.timeRequestCat)) {
						mapOfTime_permission_requestDTOTotimeRequestCat.get(oldTime_permission_requestDTO.timeRequestCat).remove(oldTime_permission_requestDTO);
					}
					if(mapOfTime_permission_requestDTOTotimeRequestCat.get(oldTime_permission_requestDTO.timeRequestCat).isEmpty()) {
						mapOfTime_permission_requestDTOTotimeRequestCat.remove(oldTime_permission_requestDTO.timeRequestCat);
					}
					
					if(mapOfTime_permission_requestDTOToreasonTypeCat.containsKey(oldTime_permission_requestDTO.reasonTypeCat)) {
						mapOfTime_permission_requestDTOToreasonTypeCat.get(oldTime_permission_requestDTO.reasonTypeCat).remove(oldTime_permission_requestDTO);
					}
					if(mapOfTime_permission_requestDTOToreasonTypeCat.get(oldTime_permission_requestDTO.reasonTypeCat).isEmpty()) {
						mapOfTime_permission_requestDTOToreasonTypeCat.remove(oldTime_permission_requestDTO.reasonTypeCat);
					}
					
					if(mapOfTime_permission_requestDTOToisApproved.containsKey(oldTime_permission_requestDTO.isApproved)) {
						mapOfTime_permission_requestDTOToisApproved.get(oldTime_permission_requestDTO.isApproved).remove(oldTime_permission_requestDTO);
					}
					if(mapOfTime_permission_requestDTOToisApproved.get(oldTime_permission_requestDTO.isApproved).isEmpty()) {
						mapOfTime_permission_requestDTOToisApproved.remove(oldTime_permission_requestDTO.isApproved);
					}
					
					if(mapOfTime_permission_requestDTOToapprovalDate.containsKey(oldTime_permission_requestDTO.approvalDate)) {
						mapOfTime_permission_requestDTOToapprovalDate.get(oldTime_permission_requestDTO.approvalDate).remove(oldTime_permission_requestDTO);
					}
					if(mapOfTime_permission_requestDTOToapprovalDate.get(oldTime_permission_requestDTO.approvalDate).isEmpty()) {
						mapOfTime_permission_requestDTOToapprovalDate.remove(oldTime_permission_requestDTO.approvalDate);
					}
					
					if(mapOfTime_permission_requestDTOToinsertedBy.containsKey(oldTime_permission_requestDTO.insertedBy)) {
						mapOfTime_permission_requestDTOToinsertedBy.get(oldTime_permission_requestDTO.insertedBy).remove(oldTime_permission_requestDTO);
					}
					if(mapOfTime_permission_requestDTOToinsertedBy.get(oldTime_permission_requestDTO.insertedBy).isEmpty()) {
						mapOfTime_permission_requestDTOToinsertedBy.remove(oldTime_permission_requestDTO.insertedBy);
					}
					
					if(mapOfTime_permission_requestDTOTomodifiedBy.containsKey(oldTime_permission_requestDTO.modifiedBy)) {
						mapOfTime_permission_requestDTOTomodifiedBy.get(oldTime_permission_requestDTO.modifiedBy).remove(oldTime_permission_requestDTO);
					}
					if(mapOfTime_permission_requestDTOTomodifiedBy.get(oldTime_permission_requestDTO.modifiedBy).isEmpty()) {
						mapOfTime_permission_requestDTOTomodifiedBy.remove(oldTime_permission_requestDTO.modifiedBy);
					}
					
					if(mapOfTime_permission_requestDTOTojobCat.containsKey(oldTime_permission_requestDTO.jobCat)) {
						mapOfTime_permission_requestDTOTojobCat.get(oldTime_permission_requestDTO.jobCat).remove(oldTime_permission_requestDTO);
					}
					if(mapOfTime_permission_requestDTOTojobCat.get(oldTime_permission_requestDTO.jobCat).isEmpty()) {
						mapOfTime_permission_requestDTOTojobCat.remove(oldTime_permission_requestDTO.jobCat);
					}
					
					if(mapOfTime_permission_requestDTOTolastModificationTime.containsKey(oldTime_permission_requestDTO.lastModificationTime)) {
						mapOfTime_permission_requestDTOTolastModificationTime.get(oldTime_permission_requestDTO.lastModificationTime).remove(oldTime_permission_requestDTO);
					}
					if(mapOfTime_permission_requestDTOTolastModificationTime.get(oldTime_permission_requestDTO.lastModificationTime).isEmpty()) {
						mapOfTime_permission_requestDTOTolastModificationTime.remove(oldTime_permission_requestDTO.lastModificationTime);
					}
					
					
				}
				if(time_permission_requestDTO.isDeleted == 0) 
				{
					
					mapOfTime_permission_requestDTOToiD.put(time_permission_requestDTO.iD, time_permission_requestDTO);
				
					if( ! mapOfTime_permission_requestDTOToemployeeRecordsId.containsKey(time_permission_requestDTO.employeeRecordsId)) {
						mapOfTime_permission_requestDTOToemployeeRecordsId.put(time_permission_requestDTO.employeeRecordsId, new HashSet<>());
					}
					mapOfTime_permission_requestDTOToemployeeRecordsId.get(time_permission_requestDTO.employeeRecordsId).add(time_permission_requestDTO);
					
					if( ! mapOfTime_permission_requestDTOTotimeRequestCat.containsKey(time_permission_requestDTO.timeRequestCat)) {
						mapOfTime_permission_requestDTOTotimeRequestCat.put(time_permission_requestDTO.timeRequestCat, new HashSet<>());
					}
					mapOfTime_permission_requestDTOTotimeRequestCat.get(time_permission_requestDTO.timeRequestCat).add(time_permission_requestDTO);
					
					if( ! mapOfTime_permission_requestDTOToreasonTypeCat.containsKey(time_permission_requestDTO.reasonTypeCat)) {
						mapOfTime_permission_requestDTOToreasonTypeCat.put(time_permission_requestDTO.reasonTypeCat, new HashSet<>());
					}
					mapOfTime_permission_requestDTOToreasonTypeCat.get(time_permission_requestDTO.reasonTypeCat).add(time_permission_requestDTO);
					
					if( ! mapOfTime_permission_requestDTOToisApproved.containsKey(time_permission_requestDTO.isApproved)) {
						mapOfTime_permission_requestDTOToisApproved.put(time_permission_requestDTO.isApproved, new HashSet<>());
					}
					mapOfTime_permission_requestDTOToisApproved.get(time_permission_requestDTO.isApproved).add(time_permission_requestDTO);
					
					if( ! mapOfTime_permission_requestDTOToapprovalDate.containsKey(time_permission_requestDTO.approvalDate)) {
						mapOfTime_permission_requestDTOToapprovalDate.put(time_permission_requestDTO.approvalDate, new HashSet<>());
					}
					mapOfTime_permission_requestDTOToapprovalDate.get(time_permission_requestDTO.approvalDate).add(time_permission_requestDTO);
					
					if( ! mapOfTime_permission_requestDTOToinsertedBy.containsKey(time_permission_requestDTO.insertedBy)) {
						mapOfTime_permission_requestDTOToinsertedBy.put(time_permission_requestDTO.insertedBy, new HashSet<>());
					}
					mapOfTime_permission_requestDTOToinsertedBy.get(time_permission_requestDTO.insertedBy).add(time_permission_requestDTO);
					
					if( ! mapOfTime_permission_requestDTOTomodifiedBy.containsKey(time_permission_requestDTO.modifiedBy)) {
						mapOfTime_permission_requestDTOTomodifiedBy.put(time_permission_requestDTO.modifiedBy, new HashSet<>());
					}
					mapOfTime_permission_requestDTOTomodifiedBy.get(time_permission_requestDTO.modifiedBy).add(time_permission_requestDTO);
					
					if( ! mapOfTime_permission_requestDTOTojobCat.containsKey(time_permission_requestDTO.jobCat)) {
						mapOfTime_permission_requestDTOTojobCat.put(time_permission_requestDTO.jobCat, new HashSet<>());
					}
					mapOfTime_permission_requestDTOTojobCat.get(time_permission_requestDTO.jobCat).add(time_permission_requestDTO);
					
					if( ! mapOfTime_permission_requestDTOTolastModificationTime.containsKey(time_permission_requestDTO.lastModificationTime)) {
						mapOfTime_permission_requestDTOTolastModificationTime.put(time_permission_requestDTO.lastModificationTime, new HashSet<>());
					}
					mapOfTime_permission_requestDTOTolastModificationTime.get(time_permission_requestDTO.lastModificationTime).add(time_permission_requestDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Time_permission_requestDTO> getTime_permission_requestList() {
		List <Time_permission_requestDTO> time_permission_requests = new ArrayList<Time_permission_requestDTO>(this.mapOfTime_permission_requestDTOToiD.values());
		return time_permission_requests;
	}
	
	
	public Time_permission_requestDTO getTime_permission_requestDTOByID( long ID){
		return mapOfTime_permission_requestDTOToiD.get(ID);
	}
	
	
	public List<Time_permission_requestDTO> getTime_permission_requestDTOByemployee_records_id(long employee_records_id) {
		return new ArrayList<>( mapOfTime_permission_requestDTOToemployeeRecordsId.getOrDefault(employee_records_id,new HashSet<>()));
	}
	
	
	public List<Time_permission_requestDTO> getTime_permission_requestDTOBytime_request_cat(int time_request_cat) {
		return new ArrayList<>( mapOfTime_permission_requestDTOTotimeRequestCat.getOrDefault(time_request_cat,new HashSet<>()));
	}
	
	
	public List<Time_permission_requestDTO> getTime_permission_requestDTOByreason_type_cat(int reason_type_cat) {
		return new ArrayList<>( mapOfTime_permission_requestDTOToreasonTypeCat.getOrDefault(reason_type_cat,new HashSet<>()));
	}
	
	
	public List<Time_permission_requestDTO> getTime_permission_requestDTOByis_approved(int is_approved) {
		return new ArrayList<>( mapOfTime_permission_requestDTOToisApproved.getOrDefault(is_approved,new HashSet<>()));
	}
	
	
	public List<Time_permission_requestDTO> getTime_permission_requestDTOByapproval_date(long approval_date) {
		return new ArrayList<>( mapOfTime_permission_requestDTOToapprovalDate.getOrDefault(approval_date,new HashSet<>()));
	}
	
	
	public List<Time_permission_requestDTO> getTime_permission_requestDTOByinserted_by(long inserted_by) {
		return new ArrayList<>( mapOfTime_permission_requestDTOToinsertedBy.getOrDefault(inserted_by,new HashSet<>()));
	}
	
	
	public List<Time_permission_requestDTO> getTime_permission_requestDTOBymodified_by(long modified_by) {
		return new ArrayList<>( mapOfTime_permission_requestDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Time_permission_requestDTO> getTime_permission_requestDTOByjob_cat(int job_cat) {
		return new ArrayList<>( mapOfTime_permission_requestDTOTojobCat.getOrDefault(job_cat,new HashSet<>()));
	}
	
	
	public List<Time_permission_requestDTO> getTime_permission_requestDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfTime_permission_requestDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "time_permission_request";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


