package time_permission_request;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Time_permission_requestDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	public CommonMaps approvalMaps = new Time_permission_requestApprovalMAPS("time_permission_request");
	
	public Time_permission_requestDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";

		joinSQL += " join category on (";
		joinSQL += " (" + tableName + ".time_request_cat = category.value  and category.domain_name = 'time_request')";
		joinSQL += " or "; 
		joinSQL += " (" + tableName + ".reason_type_cat = category.value  and category.domain_name = 'reason_type')";
		joinSQL += " )";
		joinSQL += " join language_text on category.language_id = language_text.id";		
		commonMaps = new Time_permission_requestMAPS(tableName);
	}
	
	public Time_permission_requestDAO()
	{
		this("time_permission_request");		
	}
	
	public void set(PreparedStatement ps, Time_permission_requestDTO time_permission_requestDTO, boolean isInsert) throws SQLException
	{
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		if(isInsert)
		{
			ps.setObject(index++,time_permission_requestDTO.iD);
		}
		ps.setObject(index++,time_permission_requestDTO.employeeRecordsId);
		ps.setObject(index++,time_permission_requestDTO.timeRequestCat);
		ps.setObject(index++,time_permission_requestDTO.reasonTypeCat);
		ps.setObject(index++,time_permission_requestDTO.isApproved);
		ps.setObject(index++,time_permission_requestDTO.approvalDate);
		ps.setObject(index++,time_permission_requestDTO.insertedBy);
		ps.setObject(index++,time_permission_requestDTO.modifiedBy);
		ps.setObject(index++,time_permission_requestDTO.jobCat);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Time_permission_requestDTO time_permission_requestDTO, ResultSet rs) throws SQLException
	{
		time_permission_requestDTO.iD = rs.getLong("ID");
		time_permission_requestDTO.employeeRecordsId = rs.getLong("employee_records_id");
		time_permission_requestDTO.timeRequestCat = rs.getInt("time_request_cat");
		time_permission_requestDTO.reasonTypeCat = rs.getInt("reason_type_cat");
		time_permission_requestDTO.isApproved = rs.getInt("is_approved");
		time_permission_requestDTO.approvalDate = rs.getLong("approval_date");
		time_permission_requestDTO.insertedBy = rs.getLong("inserted_by");
		time_permission_requestDTO.modifiedBy = rs.getLong("modified_by");
		time_permission_requestDTO.jobCat = rs.getInt("job_cat");
		time_permission_requestDTO.isDeleted = rs.getInt("isDeleted");
		time_permission_requestDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}
	
	
	
	public long add(CommonDTO commonDTO) throws Exception
	{
		
		Time_permission_requestDTO time_permission_requestDTO = (Time_permission_requestDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			time_permission_requestDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "employee_records_id";
			sql += ", ";
			sql += "time_request_cat";
			sql += ", ";
			sql += "reason_type_cat";
			sql += ", ";
			sql += "is_approved";
			sql += ", ";
			sql += "approval_date";
			sql += ", ";
			sql += "inserted_by";
			sql += ", ";
			sql += "modified_by";
			sql += ", ";
			sql += "job_cat";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
		
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
			sql += ")";
				
			ps = connection.prepareStatement(sql);
			set(ps, time_permission_requestDTO, true);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return time_permission_requestDTO.iD;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Time_permission_requestDTO time_permission_requestDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				time_permission_requestDTO = new Time_permission_requestDTO();

				get(time_permission_requestDTO, rs);

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return time_permission_requestDTO;
	}
	
	public long update(CommonDTO commonDTO) throws Exception
	{		
		Time_permission_requestDTO time_permission_requestDTO = (Time_permission_requestDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "employee_records_id=?";
			sql += ", ";
			sql += "time_request_cat=?";
			sql += ", ";
			sql += "reason_type_cat=?";
			sql += ", ";
			sql += "is_approved=?";
			sql += ", ";
			sql += "approval_date=?";
			sql += ", ";
			sql += "inserted_by=?";
			sql += ", ";
			sql += "modified_by=?";
			sql += ", ";
			sql += "job_cat=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + time_permission_requestDTO.iD;
				

			ps = connection.prepareStatement(sql);
			set(ps, time_permission_requestDTO, false);
			ps.executeUpdate();
			

			recordUpdateTime(connection, lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return time_permission_requestDTO.iD;
	}
	
	
	public List<Time_permission_requestDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Time_permission_requestDTO time_permission_requestDTO = null;
		List<Time_permission_requestDTO> time_permission_requestDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return time_permission_requestDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				time_permission_requestDTO = new Time_permission_requestDTO();
				get(time_permission_requestDTO, rs);
				System.out.println("got this DTO: " + time_permission_requestDTO);
				
				time_permission_requestDTOList.add(time_permission_requestDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return time_permission_requestDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Time_permission_requestDTO> getAllTime_permission_request (boolean isFirstReload)
    {
		List<Time_permission_requestDTO> time_permission_requestDTOList = new ArrayList<>();

		String sql = "SELECT * FROM time_permission_request";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by time_permission_request.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Time_permission_requestDTO time_permission_requestDTO = new Time_permission_requestDTO();
				get(time_permission_requestDTO, rs);
				
				time_permission_requestDTOList.add(time_permission_requestDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return time_permission_requestDTOList;
    }

	
	public List<Time_permission_requestDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Time_permission_requestDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Time_permission_requestDTO> time_permission_requestDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Time_permission_requestDTO time_permission_requestDTO = new Time_permission_requestDTO();
				get(time_permission_requestDTO, rs);
				
				time_permission_requestDTOList.add(time_permission_requestDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return time_permission_requestDTOList;
	
	}
	public String getSqlWithSearchCriteriaForApprovalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
	{
		
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";

		
		sql += " join approval_execution_table as aet on ("
				+ tableName + ".id = aet.updated_row_id "
				+ ")";
		
		sql += " join approval_summary on ("
				+ "aet.previous_row_id = approval_summary.table_id and  approval_summary.table_name = '" + tableName + "'"
				+ ")";
		
		if(!viewAll)
		{
																	
			sql += " left join approval_path_details on ("
					+ "aet.approval_path_id = approval_path_details.approval_path_id "
					+ "and aet.approval_path_order = approval_path_details.approval_order "
					+ ")";
		}
									
		
	

		

		String AllFieldSql = "";
		
	
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
		        if(approvalMaps.java_allfield_type_map.get(str.toLowerCase()) != null &&  !approvalMaps.java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
		        		&& !str.equalsIgnoreCase("AnyField")
		        		&&
						( ( value != null && !value.equalsIgnoreCase("") ) || commonMaps.rangeMap.get( str.toLowerCase() ) != null ) )
		        {
					if(p_searchCriteria.get(str).equals("any"))
		        	{
		        		continue;
		        	}
					
		        	if( i > 0)
		        	{
		        		AllFieldSql+= " AND  ";
		        	}

		        	String dataType = approvalMaps.java_allfield_type_map.get(str.toLowerCase()); 
		        	
		        	String fromTable = tableName;
		        	if(approvalMaps.java_table_map.get(str) != null)
		    		{
		    			fromTable = approvalMaps.java_table_map.get(str);
		    		}
		    		
		        	if(str.equalsIgnoreCase("starting_date") || str.equalsIgnoreCase("ending_date"))
		        	{
		        		String string_date = (String) p_searchCriteria.get(str);
		        		long milliseconds = 0;

		        		SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
		        		try {
		        		    Date d = f.parse(string_date);
		        		    milliseconds = d.getTime();
		        		} catch (Exception e) {
		        		    e.printStackTrace();
		        		}
		        		if(str.equalsIgnoreCase("starting_date"))
		        		{
		        			AllFieldSql += "approval_summary.date_of_initiation >= " +  milliseconds;
		        		}
		        		else
		        		{
		        			AllFieldSql += "approval_summary.date_of_initiation <= " +  milliseconds;
		        		}
		        	}
		        	else
		        	{
		        		if( dataType.equals("String") )
			        	{
			        		AllFieldSql += "" + fromTable + "." + str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
			        	}
			        	else if( dataType.equals("Integer") || dataType.equals("Long"))
			        	{
			        		AllFieldSql += "" + fromTable + "." + str.toLowerCase() + " = " + p_searchCriteria.get(str) ;
			        	}
		        	}
		        	
		        	
		        	
		        	
		        	i ++;
		        }
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		sql += " WHERE ";
		
		if(viewAll)
		{
			sql += " (" + tableName + ".isDeleted != " + CommonDTO.DELETED + " and " + tableName + ".isDeleted != " + CommonDTO.OUTDATED
					+ ")";
		}
		else
		{
			sql += " (" + tableName + ".isDeleted != " + CommonDTO.DELETED + " and " + tableName + ".isDeleted != " + CommonDTO.OUTDATED
				+ " and (approval_path_details.organogram_id = " + userDTO.organogramID 
						+ " or " +  userDTO.organogramID + " in ("
						+ " select organogram_id from approval_execution_table where previous_row_id = aet.previous_row_id"
						+ "))"
				+ ")";
		}
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}

		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += "  order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
	}
	
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				int i = 0;
				Iterator it = (commonMaps).java_anyfield_search_map.entrySet().iterator();
				while(it.hasNext())
				{
					if( i > 0)
		        	{
						AnyfieldSql+= " OR  ";
		        	}
					Entry pair = (Entry)it.next();
					AnyfieldSql+= pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
					i ++;
				}						
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if((commonMaps).java_allfield_type_map.get(str.toLowerCase()) != null &&  !(commonMaps).java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
						&& !str.equalsIgnoreCase("AnyField")
						&& value != null && !value.equalsIgnoreCase(""))
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					if((commonMaps).java_allfield_type_map.get(str.toLowerCase()).equals("String"))
					{
						AllFieldSql += "" + tableName + "." + str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
					}
					else
					{
						AllFieldSql += "" + tableName + "." + str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
					}
					i ++;
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		if(isPermanentTable)
		{
			return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
		}
		else
		{
			return getSqlWithSearchCriteriaForApprovalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
		}
				
    }
				
}
	