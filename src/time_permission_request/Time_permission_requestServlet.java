package time_permission_request;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import workflow.WorkflowController;
import approval_execution_table.*;
import pb_notifications.Pb_notificationsDAO;



/**
 * Servlet implementation class Time_permission_requestServlet
 */
@WebServlet("/Time_permission_requestServlet")
@MultipartConfig
public class Time_permission_requestServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Time_permission_requestServlet.class);

    String tableName = "time_permission_request";

	Time_permission_requestDAO time_permission_requestDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Time_permission_requestServlet() 
	{
        super();
    	try
    	{
			time_permission_requestDAO = new Time_permission_requestDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(time_permission_requestDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TIME_PERMISSION_REQUEST_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TIME_PERMISSION_REQUEST_UPDATE))
				{
					getTime_permission_request(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TIME_PERMISSION_REQUEST_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchTime_permission_request(request, response, isPermanentTable, filter);
						}
						else
						{
							searchTime_permission_request(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchTime_permission_request(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("getApprovalPage"))
			{
				System.out.println("Time_permission_request getApprovalPage requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TIME_PERMISSION_REQUEST_APPROVE))
				{
					searchTime_permission_request(request, response, false, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("viewApprovalNotification"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_TEST_SEARCH))
				{
					commonRequestHandler.viewApprovalNotification(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TIME_PERMISSION_REQUEST_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TIME_PERMISSION_REQUEST_ADD))
				{
					System.out.println("going to  addTime_permission_request ");
					addTime_permission_request(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addTime_permission_request ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("SendToApprovalPath"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TIME_PERMISSION_REQUEST_APPROVE))
				{
					commonRequestHandler.sendToApprovalPath(request, response, userDTO);
				}
				else
				{
					System.out.println("Not going to SendToApprovalPath ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("approve"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TIME_PERMISSION_REQUEST_ADD))
				{					
					commonRequestHandler.approve(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addTime_permission_request ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("reject"))
			{
				System.out.println("trying to approve");
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TIME_PERMISSION_REQUEST_APPROVE))
				{					
					commonRequestHandler.approve(request, response, true, userDTO, false);
				}
				else
				{
					System.out.println("Not going to  addTime_permission_request ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("terminate"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TIME_PERMISSION_REQUEST_ADD))
				{
					commonRequestHandler.terminate(request, response, userDTO);
				}
				else
				{
					System.out.println("Not going to  addTime_permission_request ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("skipStep"))
			{
				
				System.out.println("skipStep");
				commonRequestHandler.skipStep(request, response, userDTO);									
			}
			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TIME_PERMISSION_REQUEST_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addTime_permission_request ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TIME_PERMISSION_REQUEST_UPDATE))
				{					
					addTime_permission_request(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{								
				deleteTime_permission_request(request, response, userDTO);				
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TIME_PERMISSION_REQUEST_SEARCH))
				{
					searchTime_permission_request(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Time_permission_requestDTO time_permission_requestDTO = (Time_permission_requestDTO)time_permission_requestDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(time_permission_requestDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addTime_permission_request(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addTime_permission_request");
			String path = getServletContext().getRealPath("/img2/");
			Time_permission_requestDTO time_permission_requestDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				time_permission_requestDTO = new Time_permission_requestDTO();
			}
			else
			{
				time_permission_requestDTO = (Time_permission_requestDTO)time_permission_requestDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("employeeRecordsId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeRecordsId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				time_permission_requestDTO.employeeRecordsId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("timeRequestCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("timeRequestCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				time_permission_requestDTO.timeRequestCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("reasonTypeCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("reasonTypeCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				time_permission_requestDTO.reasonTypeCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("isApproved");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("isApproved = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				time_permission_requestDTO.isApproved = 1;
			}
			else
			{
				time_permission_requestDTO.isApproved = 0;
			}

			Value = request.getParameter("approvalDate");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approvalDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				try 
				{
					Date d = f.parse(Value);
					time_permission_requestDTO.approvalDate = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				time_permission_requestDTO.insertedBy = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				time_permission_requestDTO.modifiedBy = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("jobCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("jobCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				time_permission_requestDTO.jobCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addTime_permission_request dto = " + time_permission_requestDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				time_permission_requestDAO.setIsDeleted(time_permission_requestDTO.iD, CommonDTO.OUTDATED);
				returnedID = time_permission_requestDAO.add(time_permission_requestDTO);
				time_permission_requestDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = time_permission_requestDAO.manageWriteOperations(time_permission_requestDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = time_permission_requestDAO.manageWriteOperations(time_permission_requestDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getTime_permission_request(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Time_permission_requestServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(time_permission_requestDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void deleteTime_permission_request(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				
				Time_permission_requestDTO time_permission_requestDTO = (Time_permission_requestDTO)time_permission_requestDAO.getDTOByID(id);
				time_permission_requestDAO.manageWriteOperations(time_permission_requestDTO, SessionConstants.DELETE, id, userDTO);
				response.sendRedirect("Time_permission_requestServlet?actionType=search");
				
			}			
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		
	}

	private void getTime_permission_request(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getTime_permission_request");
		Time_permission_requestDTO time_permission_requestDTO = null;
		try 
		{
			time_permission_requestDTO = (Time_permission_requestDTO)time_permission_requestDAO.getDTOByID(id);
			boolean isPermanentTable = true;
			if(request.getParameter("isPermanentTable") != null)
			{
				isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
			}
			request.setAttribute("ID", time_permission_requestDTO.iD);
			request.setAttribute("time_permission_requestDTO",time_permission_requestDTO);
			request.setAttribute("time_permission_requestDAO",time_permission_requestDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "time_permission_request/time_permission_requestInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "time_permission_request/time_permission_requestSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "time_permission_request/time_permission_requestEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "time_permission_request/time_permission_requestEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getTime_permission_request(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getTime_permission_request(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchTime_permission_request(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchTime_permission_request 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_TIME_PERMISSION_REQUEST,
			request,
			time_permission_requestDAO,
			SessionConstants.VIEW_TIME_PERMISSION_REQUEST,
			SessionConstants.SEARCH_TIME_PERMISSION_REQUEST,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("time_permission_requestDAO",time_permission_requestDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to time_permission_request/time_permission_requestApproval.jsp");
	        	rd = request.getRequestDispatcher("time_permission_request/time_permission_requestApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to time_permission_request/time_permission_requestApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("time_permission_request/time_permission_requestApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to time_permission_request/time_permission_requestSearch.jsp");
	        	rd = request.getRequestDispatcher("time_permission_request/time_permission_requestSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to time_permission_request/time_permission_requestSearchForm.jsp");
	        	rd = request.getRequestDispatcher("time_permission_request/time_permission_requestSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

