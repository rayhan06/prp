package time_permission_request;
import java.util.*; 
import util.*; 


public class Time_permission_requestDTO extends CommonDTO
{

	public long employeeRecordsId = 0;
	public int timeRequestCat = 0;
	public int reasonTypeCat = 0;
	public int isApproved = 0;
	public long approvalDate = 0;
	public long insertedBy = 0;
	public long modifiedBy = 0;
	
	
    @Override
	public String toString() {
            return "$Time_permission_requestDTO[" +
            " iD = " + iD +
            " employeeRecordsId = " + employeeRecordsId +
            " timeRequestCat = " + timeRequestCat +
            " reasonTypeCat = " + reasonTypeCat +
            " isApproved = " + isApproved +
            " approvalDate = " + approvalDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " jobCat = " + jobCat +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}