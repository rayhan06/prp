package time_permission_request;
import util.*;

public class Time_permission_requestApprovalMAPS extends CommonMaps
{	
	public Time_permission_requestApprovalMAPS(String tableName)
	{
		
		java_allfield_type_map.put("employee_records_id".toLowerCase(), "Long");
		java_allfield_type_map.put("time_request_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("reason_type_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("is_approved".toLowerCase(), "Integer");
		java_allfield_type_map.put("approval_date".toLowerCase(), "Long");
		java_allfield_type_map.put("inserted_by".toLowerCase(), "Long");
		java_allfield_type_map.put("modified_by".toLowerCase(), "Long");

		java_allfield_type_map.put("job_cat", "Integer");
		java_allfield_type_map.put("approval_status_cat", "Integer");
		java_allfield_type_map.put("initiator", "Long");
		java_allfield_type_map.put("assigned_to", "Long");
		java_allfield_type_map.put("starting_date", "Long");
		java_allfield_type_map.put("ending_date", "Long");
		
		java_table_map.put("approval_status_cat", "approval_summary");
		java_table_map.put("initiator", "approval_summary");
		java_table_map.put("assigned_to", "approval_summary");
	}

}