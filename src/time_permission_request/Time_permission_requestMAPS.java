package time_permission_request;
import java.util.*; 
import util.*;


public class Time_permission_requestMAPS extends CommonMaps
{	
	public Time_permission_requestMAPS(String tableName)
	{
		
		java_allfield_type_map.put("employee_records_id".toLowerCase(), "Long");
		java_allfield_type_map.put("time_request_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("reason_type_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("is_approved".toLowerCase(), "Integer");
		java_allfield_type_map.put("approval_date".toLowerCase(), "Long");
		java_allfield_type_map.put("inserted_by".toLowerCase(), "Long");
		java_allfield_type_map.put("modified_by".toLowerCase(), "Long");

		java_anyfield_search_map.put("language_text.languageTextEnglish", "String");
		java_anyfield_search_map.put("language_text.languageTextBangla", "String");

		java_anyfield_search_map.put(tableName + ".employee_records_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".is_approved".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".approval_date".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".inserted_by".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".modified_by".toLowerCase(), "Long");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("employeeRecordsId".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_DTO_map.put("timeRequestCat".toLowerCase(), "timeRequestCat".toLowerCase());
		java_DTO_map.put("reasonTypeCat".toLowerCase(), "reasonTypeCat".toLowerCase());
		java_DTO_map.put("isApproved".toLowerCase(), "isApproved".toLowerCase());
		java_DTO_map.put("approvalDate".toLowerCase(), "approvalDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("jobCat".toLowerCase(), "jobCat".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("employee_records_id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_SQL_map.put("time_request_cat".toLowerCase(), "timeRequestCat".toLowerCase());
		java_SQL_map.put("reason_type_cat".toLowerCase(), "reasonTypeCat".toLowerCase());
		java_SQL_map.put("is_approved".toLowerCase(), "isApproved".toLowerCase());
		java_SQL_map.put("approval_date".toLowerCase(), "approvalDate".toLowerCase());
		java_SQL_map.put("inserted_by".toLowerCase(), "insertedBy".toLowerCase());
		java_SQL_map.put("modified_by".toLowerCase(), "modifiedBy".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Employee Records Id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_Text_map.put("Time Request".toLowerCase(), "timeRequestCat".toLowerCase());
		java_Text_map.put("Reason Type".toLowerCase(), "reasonTypeCat".toLowerCase());
		java_Text_map.put("Is Approved".toLowerCase(), "isApproved".toLowerCase());
		java_Text_map.put("Approval Date".toLowerCase(), "approvalDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Job Cat".toLowerCase(), "jobCat".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}