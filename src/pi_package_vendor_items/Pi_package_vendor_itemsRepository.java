package pi_package_vendor_items;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Pi_package_vendor_itemsRepository implements Repository {
	Pi_package_vendor_itemsDAO pi_package_vendor_itemsDAO = null;
	
	static Logger logger = Logger.getLogger(Pi_package_vendor_itemsRepository.class);
	Map<Long, Pi_package_vendor_itemsDTO>mapOfPi_package_vendor_itemsDTOToiD;
	Map<Long, Set<Pi_package_vendor_itemsDTO> >mapOfPi_package_vendor_itemsDTOTopiPackageVendorId;
	Map<Long, Set<Pi_package_vendor_itemsDTO> >mapOfPi_package_vendor_itemsDTOTopiPackageVendorChildrenId;
	Map<Long, Set<Pi_package_vendor_itemsDTO> >mapOfPi_package_vendor_itemsDTOTopackageId;
	Map<Long, Set<Pi_package_vendor_itemsDTO> >mapOfPi_package_vendor_itemsDTOTofiscalYearId;
	Map<Long, Set<Pi_package_vendor_itemsDTO> >mapOfPi_package_vendor_itemsDTOToproductId;
	Gson gson;

  
	private Pi_package_vendor_itemsRepository(){
		pi_package_vendor_itemsDAO = Pi_package_vendor_itemsDAO.getInstance();
		mapOfPi_package_vendor_itemsDTOToiD = new ConcurrentHashMap<>();
		mapOfPi_package_vendor_itemsDTOTopiPackageVendorId = new ConcurrentHashMap<>();
		mapOfPi_package_vendor_itemsDTOTopiPackageVendorChildrenId = new ConcurrentHashMap<>();
		mapOfPi_package_vendor_itemsDTOTopackageId = new ConcurrentHashMap<>();
		mapOfPi_package_vendor_itemsDTOTofiscalYearId = new ConcurrentHashMap<>();
		mapOfPi_package_vendor_itemsDTOToproductId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Pi_package_vendor_itemsRepository INSTANCE = new Pi_package_vendor_itemsRepository();
    }

    public static Pi_package_vendor_itemsRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Pi_package_vendor_itemsDTO> pi_package_vendor_itemsDTOs = pi_package_vendor_itemsDAO.getAllDTOs(reloadAll);
			for(Pi_package_vendor_itemsDTO pi_package_vendor_itemsDTO : pi_package_vendor_itemsDTOs) {
				Pi_package_vendor_itemsDTO oldPi_package_vendor_itemsDTO = getPi_package_vendor_itemsDTOByiD(pi_package_vendor_itemsDTO.iD);
				if( oldPi_package_vendor_itemsDTO != null ) {
					mapOfPi_package_vendor_itemsDTOToiD.remove(oldPi_package_vendor_itemsDTO.iD);
				
					if(mapOfPi_package_vendor_itemsDTOTopiPackageVendorId.containsKey(oldPi_package_vendor_itemsDTO.piPackageVendorId)) {
						mapOfPi_package_vendor_itemsDTOTopiPackageVendorId.get(oldPi_package_vendor_itemsDTO.piPackageVendorId).remove(oldPi_package_vendor_itemsDTO);
					}
					if(mapOfPi_package_vendor_itemsDTOTopiPackageVendorId.get(oldPi_package_vendor_itemsDTO.piPackageVendorId).isEmpty()) {
						mapOfPi_package_vendor_itemsDTOTopiPackageVendorId.remove(oldPi_package_vendor_itemsDTO.piPackageVendorId);
					}
					
					if(mapOfPi_package_vendor_itemsDTOTopiPackageVendorChildrenId.containsKey(oldPi_package_vendor_itemsDTO.piPackageVendorChildrenId)) {
						mapOfPi_package_vendor_itemsDTOTopiPackageVendorChildrenId.get(oldPi_package_vendor_itemsDTO.piPackageVendorChildrenId).remove(oldPi_package_vendor_itemsDTO);
					}
					if(mapOfPi_package_vendor_itemsDTOTopiPackageVendorChildrenId.get(oldPi_package_vendor_itemsDTO.piPackageVendorChildrenId).isEmpty()) {
						mapOfPi_package_vendor_itemsDTOTopiPackageVendorChildrenId.remove(oldPi_package_vendor_itemsDTO.piPackageVendorChildrenId);
					}
					
					if(mapOfPi_package_vendor_itemsDTOTopackageId.containsKey(oldPi_package_vendor_itemsDTO.packageId)) {
						mapOfPi_package_vendor_itemsDTOTopackageId.get(oldPi_package_vendor_itemsDTO.packageId).remove(oldPi_package_vendor_itemsDTO);
					}
					if(mapOfPi_package_vendor_itemsDTOTopackageId.get(oldPi_package_vendor_itemsDTO.packageId).isEmpty()) {
						mapOfPi_package_vendor_itemsDTOTopackageId.remove(oldPi_package_vendor_itemsDTO.packageId);
					}
					
					if(mapOfPi_package_vendor_itemsDTOTofiscalYearId.containsKey(oldPi_package_vendor_itemsDTO.fiscalYearId)) {
						mapOfPi_package_vendor_itemsDTOTofiscalYearId.get(oldPi_package_vendor_itemsDTO.fiscalYearId).remove(oldPi_package_vendor_itemsDTO);
					}
					if(mapOfPi_package_vendor_itemsDTOTofiscalYearId.get(oldPi_package_vendor_itemsDTO.fiscalYearId).isEmpty()) {
						mapOfPi_package_vendor_itemsDTOTofiscalYearId.remove(oldPi_package_vendor_itemsDTO.fiscalYearId);
					}
					
					if(mapOfPi_package_vendor_itemsDTOToproductId.containsKey(oldPi_package_vendor_itemsDTO.productId)) {
						mapOfPi_package_vendor_itemsDTOToproductId.get(oldPi_package_vendor_itemsDTO.productId).remove(oldPi_package_vendor_itemsDTO);
					}
					if(mapOfPi_package_vendor_itemsDTOToproductId.get(oldPi_package_vendor_itemsDTO.productId).isEmpty()) {
						mapOfPi_package_vendor_itemsDTOToproductId.remove(oldPi_package_vendor_itemsDTO.productId);
					}
					
					
				}
				if(pi_package_vendor_itemsDTO.isDeleted == 0) 
				{
					
					mapOfPi_package_vendor_itemsDTOToiD.put(pi_package_vendor_itemsDTO.iD, pi_package_vendor_itemsDTO);
				
					if( ! mapOfPi_package_vendor_itemsDTOTopiPackageVendorId.containsKey(pi_package_vendor_itemsDTO.piPackageVendorId)) {
						mapOfPi_package_vendor_itemsDTOTopiPackageVendorId.put(pi_package_vendor_itemsDTO.piPackageVendorId, new HashSet<>());
					}
					mapOfPi_package_vendor_itemsDTOTopiPackageVendorId.get(pi_package_vendor_itemsDTO.piPackageVendorId).add(pi_package_vendor_itemsDTO);
					
					if( ! mapOfPi_package_vendor_itemsDTOTopiPackageVendorChildrenId.containsKey(pi_package_vendor_itemsDTO.piPackageVendorChildrenId)) {
						mapOfPi_package_vendor_itemsDTOTopiPackageVendorChildrenId.put(pi_package_vendor_itemsDTO.piPackageVendorChildrenId, new HashSet<>());
					}
					mapOfPi_package_vendor_itemsDTOTopiPackageVendorChildrenId.get(pi_package_vendor_itemsDTO.piPackageVendorChildrenId).add(pi_package_vendor_itemsDTO);
					
					if( ! mapOfPi_package_vendor_itemsDTOTopackageId.containsKey(pi_package_vendor_itemsDTO.packageId)) {
						mapOfPi_package_vendor_itemsDTOTopackageId.put(pi_package_vendor_itemsDTO.packageId, new HashSet<>());
					}
					mapOfPi_package_vendor_itemsDTOTopackageId.get(pi_package_vendor_itemsDTO.packageId).add(pi_package_vendor_itemsDTO);
					
					if( ! mapOfPi_package_vendor_itemsDTOTofiscalYearId.containsKey(pi_package_vendor_itemsDTO.fiscalYearId)) {
						mapOfPi_package_vendor_itemsDTOTofiscalYearId.put(pi_package_vendor_itemsDTO.fiscalYearId, new HashSet<>());
					}
					mapOfPi_package_vendor_itemsDTOTofiscalYearId.get(pi_package_vendor_itemsDTO.fiscalYearId).add(pi_package_vendor_itemsDTO);
					
					if( ! mapOfPi_package_vendor_itemsDTOToproductId.containsKey(pi_package_vendor_itemsDTO.productId)) {
						mapOfPi_package_vendor_itemsDTOToproductId.put(pi_package_vendor_itemsDTO.productId, new HashSet<>());
					}
					mapOfPi_package_vendor_itemsDTOToproductId.get(pi_package_vendor_itemsDTO.productId).add(pi_package_vendor_itemsDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Pi_package_vendor_itemsDTO clone(Pi_package_vendor_itemsDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Pi_package_vendor_itemsDTO.class);
	}
	
	
	public List<Pi_package_vendor_itemsDTO> getPi_package_vendor_itemsList() {
		List <Pi_package_vendor_itemsDTO> pi_package_vendor_itemss = new ArrayList<Pi_package_vendor_itemsDTO>(this.mapOfPi_package_vendor_itemsDTOToiD.values());
		return pi_package_vendor_itemss;
	}
	
	public List<Pi_package_vendor_itemsDTO> copyPi_package_vendor_itemsList() {
		List <Pi_package_vendor_itemsDTO> pi_package_vendor_itemss = getPi_package_vendor_itemsList();
		return pi_package_vendor_itemss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Pi_package_vendor_itemsDTO getPi_package_vendor_itemsDTOByiD( long iD){
		return mapOfPi_package_vendor_itemsDTOToiD.get(iD);
	}
	
	public Pi_package_vendor_itemsDTO copyPi_package_vendor_itemsDTOByiD( long iD){
		return clone(mapOfPi_package_vendor_itemsDTOToiD.get(iD));
	}
	
	
	public List<Pi_package_vendor_itemsDTO> getPi_package_vendor_itemsDTOBypiPackageVendorId(long piPackageVendorId) {
		return new ArrayList<>( mapOfPi_package_vendor_itemsDTOTopiPackageVendorId.getOrDefault(piPackageVendorId,new HashSet<>()));
	}
	
	public List<Pi_package_vendor_itemsDTO> copyPi_package_vendor_itemsDTOBypiPackageVendorId(long piPackageVendorId)
	{
		List <Pi_package_vendor_itemsDTO> pi_package_vendor_itemss = getPi_package_vendor_itemsDTOBypiPackageVendorId(piPackageVendorId);
		return pi_package_vendor_itemss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<Pi_package_vendor_itemsDTO> getPi_package_vendor_itemsDTOBypiPackageVendorChildrenId(long piPackageVendorChildrenId) {
		return new ArrayList<>( mapOfPi_package_vendor_itemsDTOTopiPackageVendorChildrenId.getOrDefault(piPackageVendorChildrenId,new HashSet<>()));
	}
	
	public List<Pi_package_vendor_itemsDTO> copyPi_package_vendor_itemsDTOBypiPackageVendorChildrenId(long piPackageVendorChildrenId)
	{
		List <Pi_package_vendor_itemsDTO> pi_package_vendor_itemss = getPi_package_vendor_itemsDTOBypiPackageVendorChildrenId(piPackageVendorChildrenId);
		return pi_package_vendor_itemss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<Pi_package_vendor_itemsDTO> getPi_package_vendor_itemsDTOBypackageId(long packageId) {
		return new ArrayList<>( mapOfPi_package_vendor_itemsDTOTopackageId.getOrDefault(packageId,new HashSet<>()));
	}
	
	public List<Pi_package_vendor_itemsDTO> copyPi_package_vendor_itemsDTOBypackageId(long packageId)
	{
		List <Pi_package_vendor_itemsDTO> pi_package_vendor_itemss = getPi_package_vendor_itemsDTOBypackageId(packageId);
		return pi_package_vendor_itemss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<Pi_package_vendor_itemsDTO> getPi_package_vendor_itemsDTOByfiscalYearId(long fiscalYearId) {
		return new ArrayList<>( mapOfPi_package_vendor_itemsDTOTofiscalYearId.getOrDefault(fiscalYearId,new HashSet<>()));
	}
	
	public List<Pi_package_vendor_itemsDTO> copyPi_package_vendor_itemsDTOByfiscalYearId(long fiscalYearId)
	{
		List <Pi_package_vendor_itemsDTO> pi_package_vendor_itemss = getPi_package_vendor_itemsDTOByfiscalYearId(fiscalYearId);
		return pi_package_vendor_itemss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<Pi_package_vendor_itemsDTO> getPi_package_vendor_itemsDTOByproductId(long productId) {
		return new ArrayList<>( mapOfPi_package_vendor_itemsDTOToproductId.getOrDefault(productId,new HashSet<>()));
	}
	
	public List<Pi_package_vendor_itemsDTO> copyPi_package_vendor_itemsDTOByproductId(long productId)
	{
		List <Pi_package_vendor_itemsDTO> pi_package_vendor_itemss = getPi_package_vendor_itemsDTOByproductId(productId);
		return pi_package_vendor_itemss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return pi_package_vendor_itemsDAO.getTableName();
	}
}


