package pi_package_vendor_items;

import util.CommonDTO;


public class Pi_package_vendor_itemsDTO extends CommonDTO {
    public long piPackageVendorId = -1;
    public long piPackageVendorChildrenId = -1;
    public long packageId = -1;
    public long lotId = -1;
    public long fiscalYearId = -1;
    public long productId = -1;
    public double price = -1;
    public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
    public long officeUnitId = -1;
    public long actualVendorId = -1;
    public double appApprovedQuantity = -1;
    public double totalBill = -1;
    public boolean isWinner = false;

    @Override
    public String toString() {
        return "$Pi_package_vendor_itemsDTO[" +
                " iD = " + iD +
                " piPackageVendorId = " + piPackageVendorId +
                " piPackageVendorChildrenId = " + piPackageVendorChildrenId +
                " packageId = " + packageId +
                " fiscalYearId = " + fiscalYearId +
                " productId = " + productId +
                " price = " + price +
                " insertionDate = " + insertionDate +
                " insertedBy = " + insertedBy +
                " modifiedBy = " + modifiedBy +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                " searchColumn = " + searchColumn +
                " officeUnitId = " + officeUnitId +
                " actualVendorId = " + actualVendorId +
                " appApprovedQuantity = " + appApprovedQuantity +
                " totalBill = " + totalBill +
                " isWinner = " + isWinner +
                "]";
    }
}