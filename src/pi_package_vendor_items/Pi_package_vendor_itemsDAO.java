package pi_package_vendor_items;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pi_package_vendor.Pi_package_vendorDTO;
import user.UserDTO;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

public class Pi_package_vendor_itemsDAO implements CommonDAOService<Pi_package_vendor_itemsDTO> {
    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private String[] columnNames = null;

    private static final String getByVendorAndPackageAndFiscalYearIdAndOfficeUnitId =
            "SELECT * FROM pi_package_vendor_items WHERE pi_package_vendor_id = %d AND package_id = %d AND" +
                    " fiscal_year_id = %d AND office_unit_id = %d AND isDeleted = 0 ORDER BY lastModificationTime DESC";

    private static final String deletePiPackageVendorChildrenItems =
            "UPDATE pi_package_vendor_items SET isDeleted = %d,modified_by = %d,lastModificationTime = %d " +
                    " WHERE pi_package_vendor_id = %d AND package_id = %d and fiscal_year_id = %d ";

    private static final String getPriceByFiscalYearIdAndOfficeUnitIdAndPackageIdAndVendorId =
            "SELECT * FROM pi_package_vendor_items WHERE  fiscal_year_id = %d AND" +
                    " office_unit_id = %d AND package_id = %d AND actual_vendor_id = %d AND isDeleted = 0 AND is_winner = 1 ORDER BY lastModificationTime DESC";

    private static final String getPriceByFiscalYearIdAndOfficeUnitIdAndVendorId =
            "SELECT * FROM pi_package_vendor_items WHERE  fiscal_year_id = %d AND" +
                    " office_unit_id = %d AND actual_vendor_id = %d AND isDeleted = 0 AND is_winner = 1 ORDER BY lastModificationTime DESC";

    private static final String getPriceByFiscalYearIdAndVendorIdAndPackageIdAndLotId =
            "SELECT * FROM pi_package_vendor_items WHERE  fiscal_year_id = %d AND" +
                    " package_id = %d AND lot_id = %d AND actual_vendor_id = %d AND isDeleted = 0 AND is_winner = 1 " +
                    "ORDER BY lastModificationTime DESC";

    private static final String getPriceByFiscalYearIdAndOfficeUnitIdAndPackageIdAndVendorIdAndProductId =
            "SELECT * FROM pi_package_vendor_items WHERE  fiscal_year_id = %d AND" +
                    " office_unit_id = %d AND package_id = %d AND actual_vendor_id = %d AND product_id = %d AND isDeleted = 0 ORDER BY lastModificationTime DESC";

    private static final String getPriceByOfficeUnitIdAndProductId =
            "SELECT * FROM pi_package_vendor_items WHERE " +
                    " office_unit_id = %d AND product_id = %d AND isDeleted = 0 ORDER BY lastModificationTime DESC";

    private static final String getProductIdByPackageAndFiscalYearAndOfficeUnitId =
            "SELECT * FROM pi_package_vendor_items WHERE  fiscal_year_id = %d AND " +
                    " package_id = %d AND office_unit_id = %d  AND isDeleted = 0 ORDER BY lastModificationTime DESC";

    private static final String getProductIdByPackageAndFiscalYearAndOfficeUnitIdAndNotVendorId =
            "SELECT * FROM pi_package_vendor_items WHERE  fiscal_year_id = %d AND " +
                    " package_id = %d AND office_unit_id = %d AND pi_package_vendor_id != %d  AND " +
                    " isDeleted = 0 ORDER BY lastModificationTime DESC";

    private static final String getProductIdByPackageAndLotAndFiscalYearAndNotVendorId =
            "SELECT * FROM pi_package_vendor_items WHERE  fiscal_year_id = %d AND " +
                    " package_id = %d AND lot_id = %d AND pi_package_vendor_id != %d  AND " +
                    " isDeleted = 0 ORDER BY lastModificationTime DESC";

    private static final String getProductIdByPackageAndLotAndFiscalYear =
            "SELECT * FROM pi_package_vendor_items WHERE  fiscal_year_id = %d AND " +
                    " package_id = %d AND lot_id = %d AND " +
                    " isDeleted = 0 ORDER BY lastModificationTime DESC";


    private Pi_package_vendor_itemsDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "pi_package_vendor_id",
                        "pi_package_vendor_children_id",
                        "package_id",
                        "lot_id",
                        "fiscal_year_id",
                        "product_id",
                        "price",
                        "insertion_date",
                        "inserted_by",
                        "modified_by",
                        "search_column",
                        "office_unit_id",
                        "actual_vendor_id",
                        "app_approved_quantity",
                        "total_bill",
                        "is_winner",
                        "isDeleted",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("insertion_date_start", " and (insertion_date >= ?)");
        searchMap.put("insertion_date_end", " and (insertion_date <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Pi_package_vendor_itemsDAO INSTANCE = new Pi_package_vendor_itemsDAO();
    }

    public static Pi_package_vendor_itemsDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Pi_package_vendor_itemsDTO pi_package_vendor_itemsDTO) {
        pi_package_vendor_itemsDTO.searchColumn = "";
    }

    @Override
    public void set(PreparedStatement ps, Pi_package_vendor_itemsDTO pi_package_vendor_itemsDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(pi_package_vendor_itemsDTO);
        if (isInsert) {
            ps.setObject(++index, pi_package_vendor_itemsDTO.iD);
        }
        ps.setObject(++index, pi_package_vendor_itemsDTO.piPackageVendorId);
        ps.setObject(++index, pi_package_vendor_itemsDTO.piPackageVendorChildrenId);
        ps.setObject(++index, pi_package_vendor_itemsDTO.packageId);
        ps.setObject(++index, pi_package_vendor_itemsDTO.lotId);
        ps.setObject(++index, pi_package_vendor_itemsDTO.fiscalYearId);
        ps.setObject(++index, pi_package_vendor_itemsDTO.productId);
        ps.setObject(++index, pi_package_vendor_itemsDTO.price);
        ps.setObject(++index, pi_package_vendor_itemsDTO.insertionDate);
        ps.setObject(++index, pi_package_vendor_itemsDTO.insertedBy);
        ps.setObject(++index, pi_package_vendor_itemsDTO.modifiedBy);
        ps.setObject(++index, pi_package_vendor_itemsDTO.searchColumn);
        ps.setObject(++index, pi_package_vendor_itemsDTO.officeUnitId);
        ps.setObject(++index, pi_package_vendor_itemsDTO.actualVendorId);
        ps.setObject(++index, pi_package_vendor_itemsDTO.appApprovedQuantity);
        ps.setObject(++index, pi_package_vendor_itemsDTO.totalBill);
        ps.setObject(++index, pi_package_vendor_itemsDTO.isWinner);
        if (isInsert) {
            ps.setObject(++index, pi_package_vendor_itemsDTO.isDeleted);
        }
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, pi_package_vendor_itemsDTO.iD);
        }
    }

    @Override
    public Pi_package_vendor_itemsDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Pi_package_vendor_itemsDTO pi_package_vendor_itemsDTO = new Pi_package_vendor_itemsDTO();
            int i = 0;
            pi_package_vendor_itemsDTO.iD = rs.getLong(columnNames[i++]);
            pi_package_vendor_itemsDTO.piPackageVendorId = rs.getLong(columnNames[i++]);
            pi_package_vendor_itemsDTO.piPackageVendorChildrenId = rs.getLong(columnNames[i++]);
            pi_package_vendor_itemsDTO.packageId = rs.getLong(columnNames[i++]);
            pi_package_vendor_itemsDTO.lotId = rs.getLong(columnNames[i++]);
            pi_package_vendor_itemsDTO.fiscalYearId = rs.getLong(columnNames[i++]);
            pi_package_vendor_itemsDTO.productId = rs.getLong(columnNames[i++]);
            pi_package_vendor_itemsDTO.price = rs.getDouble(columnNames[i++]);
            pi_package_vendor_itemsDTO.insertionDate = rs.getLong(columnNames[i++]);
            pi_package_vendor_itemsDTO.insertedBy = rs.getString(columnNames[i++]);
            pi_package_vendor_itemsDTO.modifiedBy = rs.getString(columnNames[i++]);
            pi_package_vendor_itemsDTO.searchColumn = rs.getString(columnNames[i++]);
            pi_package_vendor_itemsDTO.officeUnitId = rs.getLong(columnNames[i++]);
            pi_package_vendor_itemsDTO.actualVendorId = rs.getLong(columnNames[i++]);
            pi_package_vendor_itemsDTO.appApprovedQuantity = rs.getDouble(columnNames[i++]);
            pi_package_vendor_itemsDTO.totalBill = rs.getDouble(columnNames[i++]);
            pi_package_vendor_itemsDTO.isWinner = rs.getBoolean(columnNames[i++]);
            pi_package_vendor_itemsDTO.isDeleted = rs.getInt(columnNames[i++]);
            pi_package_vendor_itemsDTO.lastModificationTime = rs.getLong(columnNames[i++]);

            return pi_package_vendor_itemsDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Pi_package_vendor_itemsDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "pi_package_vendor_items";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_package_vendor_itemsDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_package_vendor_itemsDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

    public List<Pi_package_vendor_itemsDTO> getAllDTOByVendorAndPackageAndFiscalYearIdAndOfficeUnitId(long piPackageVendorId, long packageId, long fiscalYearId, long officeUnitId) {
        String sql = String.format(getByVendorAndPackageAndFiscalYearIdAndOfficeUnitId, piPackageVendorId, packageId, fiscalYearId, officeUnitId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public Pi_package_vendor_itemsDTO getPiPackageItemsDtoByProductIdAndChildrenId(List<Pi_package_vendor_itemsDTO> pi_package_vendor_itemsDTOS, long productId,
                                                                                   long piPackageChildrenId) {

        return pi_package_vendor_itemsDTOS.stream()
                .filter(dto -> dto.productId == productId && dto.piPackageVendorChildrenId == piPackageChildrenId)
                .findFirst()
                .orElse(null);

    }

    public List<Pi_package_vendor_itemsDTO> getPiPackageItemsDtoByProductId(List<Pi_package_vendor_itemsDTO> pi_package_vendor_itemsDTOS, long productId) {
        return pi_package_vendor_itemsDTOS.stream()
                .filter(dto -> dto.productId == productId)
                .collect(toList());

    }

    public void deletePiPackageVendorChildrenItems(Pi_package_vendorDTO pi_package_vendorDTO, UserDTO userDTO) {
        String sql = String.format(
                deletePiPackageVendorChildrenItems, 1,
                userDTO.employee_record_id, System.currentTimeMillis(), pi_package_vendorDTO.iD, pi_package_vendorDTO.packageId
                , pi_package_vendorDTO.fiscalYearId
        );

        boolean res = (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
            try {
                st.executeUpdate(sql);
                return true;
            } catch (SQLException ex) {
                logger.error(ex);
                return false;
            }
        });
    }

    public List<Pi_package_vendor_itemsDTO> getPriceByFiscalYearIdAndOfficeUnitIdAndPackageIdAndVendorId(long fiscalYearId, long officeUnitId, long packageId, long vendorId) {
        String sql = String.format(getPriceByFiscalYearIdAndOfficeUnitIdAndPackageIdAndVendorId, fiscalYearId, officeUnitId, packageId, vendorId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public List<Pi_package_vendor_itemsDTO> getPriceByFiscalYearIdAndOfficeUnitIdAndVendorId(long fiscalYearId, long officeUnitId, long vendorId) {
        String sql = String.format(getPriceByFiscalYearIdAndOfficeUnitIdAndVendorId, fiscalYearId, officeUnitId, vendorId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }
    public List<Pi_package_vendor_itemsDTO> getPriceByFiscalYearIdAndVendorIdAndPackageIdAndLotId(long fiscalYearId, long packageId,long lotId ,long vendorId) {
        String sql = String.format(getPriceByFiscalYearIdAndVendorIdAndPackageIdAndLotId, fiscalYearId, packageId,lotId, vendorId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }



    public List<Pi_package_vendor_itemsDTO> getPriceByFiscalYearIdAndOfficeUnitIdAndPackageIdAndVendorIdAndProductId(long fiscalYearId, long officeUnitId, long packageId, long vendorId, long productId) {
        String sql = String.format(getPriceByFiscalYearIdAndOfficeUnitIdAndPackageIdAndVendorIdAndProductId, fiscalYearId, officeUnitId, packageId, vendorId, productId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public List<Pi_package_vendor_itemsDTO> getPriceByOfficeUnitIdAndProductId(long officeUnitId, long productId) {
        String sql = String.format(getPriceByOfficeUnitIdAndProductId, officeUnitId, productId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public List<Pi_package_vendor_itemsDTO> getProductIdByPackageAndFiscalYearAndOfficeUnitId(long fiscalYearId, long packageId, long officeUnitId) {
        String sql = String.format(getProductIdByPackageAndFiscalYearAndOfficeUnitId, fiscalYearId, packageId, officeUnitId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public List<Pi_package_vendor_itemsDTO> getProductIdByPackageAndFiscalYearAndOfficeUnitIdAndNotVendorId(long fiscalYearId,
                                                                                                            long packageId, long officeUnitId, long vendorId) {
        String sql = String.format(getProductIdByPackageAndFiscalYearAndOfficeUnitIdAndNotVendorId, fiscalYearId, packageId, officeUnitId, vendorId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public List<Pi_package_vendor_itemsDTO> getProductIdByPackageAndLotAndFiscalYearAndNotVendorId(long fiscalYearId,
                                                                                                            long packageId, long lotId, long vendorId) {
        String sql = String.format(getProductIdByPackageAndLotAndFiscalYearAndNotVendorId, fiscalYearId, packageId, lotId, vendorId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public List<Pi_package_vendor_itemsDTO> getProductIdByPackageAndLotAndFiscalYear(long fiscalYearId,
                                                                                                   long packageId, long lotId) {
        String sql = String.format(getProductIdByPackageAndLotAndFiscalYear, fiscalYearId, packageId, lotId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public List<Pi_package_vendor_itemsDTO> getWinningTopFiveItem(List<Long> ids) {
        if (!ids.isEmpty()) {
            StringBuilder text = new StringBuilder("select * from ")
                    .append(getTableName())
                    .append(" where isDeleted = 0 and is_winner = 1 and pi_package_vendor_id in (");
            for (int i = 0; i < ids.size(); i++) {
                if (i != 0) {
                    text.append(", ");
                }
                text.append(ids.get(i));
            }

            text.append(" ) order by total_bill desc limit 5");
            return ConnectionAndStatementUtil.getListOfT(text.toString(), this::buildObjectFromResultSet);
        } else {
            return new ArrayList<>();
        }
    }

    public boolean productExistInThisVendor(List<Pi_package_vendor_itemsDTO> pi_package_vendor_itemsNotDTOS, long productId) {
        boolean isPresent = false;
        Pi_package_vendor_itemsDTO pi_package_vendor_itemsDTO = pi_package_vendor_itemsNotDTOS.stream()
                .filter(dto -> dto.productId == productId)
                .findAny()
                .orElse(null);
        if (pi_package_vendor_itemsDTO != null) {
            isPresent = true;
        }
        return isPresent;
    }
}
	