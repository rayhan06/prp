package pi_package_vendor_items;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;

import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;
import javax.servlet.http.*;
import java.util.*;


import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;
import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Pi_package_vendor_itemsServlet
 */
@WebServlet("/Pi_package_vendor_itemsServlet")
@MultipartConfig
public class Pi_package_vendor_itemsServlet extends BaseServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_package_vendor_itemsServlet.class);

    @Override
    public String getTableName() {
        return Pi_package_vendor_itemsDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_package_vendor_itemsServlet";
    }

    @Override
    public Pi_package_vendor_itemsDAO getCommonDAOService() {
        return Pi_package_vendor_itemsDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.PI_PACKAGE_VENDOR_ITEMS_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.PI_PACKAGE_VENDOR_ITEMS_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.PI_PACKAGE_VENDOR_ITEMS_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_package_vendor_itemsServlet.class;
    }
    private final Gson gson = new Gson();


	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addPi_package_vendor_items");
			String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
			Pi_package_vendor_itemsDTO pi_package_vendor_itemsDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				pi_package_vendor_itemsDTO = new Pi_package_vendor_itemsDTO();
			}
			else
			{
				pi_package_vendor_itemsDTO = Pi_package_vendor_itemsDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("piPackageVendorId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("piPackageVendorId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				pi_package_vendor_itemsDTO.piPackageVendorId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("piPackageVendorChildrenId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("piPackageVendorChildrenId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				pi_package_vendor_itemsDTO.piPackageVendorChildrenId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("packageId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("packageId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				pi_package_vendor_itemsDTO.packageId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("fiscalYearId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("fiscalYearId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				pi_package_vendor_itemsDTO.fiscalYearId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("productId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("productId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				pi_package_vendor_itemsDTO.productId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("price");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("price = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				pi_package_vendor_itemsDTO.price = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				pi_package_vendor_itemsDTO.insertionDate = TimeConverter.getToday();
			}


			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null)
			{
				pi_package_vendor_itemsDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null)
			{
				pi_package_vendor_itemsDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				pi_package_vendor_itemsDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addPi_package_vendor_items dto = " + pi_package_vendor_itemsDTO);
			long returnedID = -1;


			if(addFlag == true)
			{
				returnedID = Pi_package_vendor_itemsDAO.getInstance().add(pi_package_vendor_itemsDTO);
			}
			else
			{
				returnedID = Pi_package_vendor_itemsDAO.getInstance().update(pi_package_vendor_itemsDTO);
			}


			return pi_package_vendor_itemsDTO;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
}

