create table food_allowance
(
    ID                         bigint primary key,
    food_bill_id               bigint,
    budget_office_id           bigint,
    budget_register_id         bigint,
    bill_register_id           bigint,
    allowance_employee_info_id bigint,
    serial_no                  int,
    employee_records_id        bigint,
    office_unit_id             bigint,
    organogram_id              bigint,
    daily_rate                 bigint,
    days                       bigint,
    sorted_bill_date_ot_types  varchar(4096),
    revenue_stamp_deduction    int,
    modified_by                bigint,
    lastModificationTime       bigint,
    inserted_by                bigint,
    insertion_date             bigint,
    isDeleted                  bigint default 0
)
    charset = utf8mb4;

insert into vb_sequencer (table_name, next_id, table_LastModificationTime) value ('food_allowance', 1, 0);


