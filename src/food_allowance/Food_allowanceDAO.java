package food_allowance;

import common.ConnectionAndStatementUtil;
import common.EmployeeCommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Food_allowanceDAO implements EmployeeCommonDAOService<Food_allowanceDTO> {
    private static final Logger logger = Logger.getLogger(Food_allowanceDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (food_bill_id,budget_office_id,budget_register_id,bill_register_id,"
                    .concat("allowance_employee_info_id,serial_no,employee_records_id,office_unit_id,organogram_id,designation_prefix,")
                    .concat("daily_rate,days,sorted_bill_date_ot_types,revenue_stamp_deduction,")
                    .concat("modified_by,lastModificationTime,inserted_by,insertion_date,isDeleted,ID) ")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET food_bill_id=?,budget_office_id=?,budget_register_id=?,bill_register_id=?,"
                    .concat("allowance_employee_info_id=?,serial_no=?,employee_records_id=?,office_unit_id=?,organogram_id=?,designation_prefix=?,")
                    .concat("daily_rate=?,days=?,sorted_bill_date_ot_types=?,revenue_stamp_deduction=?,")
                    .concat("modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private Food_allowanceDAO() {
    }


    private static class LazyLoader {
        static final Food_allowanceDAO INSTANCE = new Food_allowanceDAO();
    }

    public static Food_allowanceDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Food_allowanceDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.foodBillId);
        ps.setLong(++index, dto.budgetOfficeId);
        ps.setLong(++index, dto.budgetRegisterId);
        ps.setLong(++index, dto.billRegisterId);
        ps.setLong(++index, dto.allowanceEmployeeInfoId);
        ps.setInt(++index, dto.serialNo);
        ps.setLong(++index, dto.employeeRecordsId);
        ps.setLong(++index, dto.officeUnitId);
        ps.setLong(++index, dto.organogramId);
        ps.setString(++index, dto.designationPrefix);
        ps.setLong(++index, dto.dailyRate);
        ps.setLong(++index, dto.days);
        ps.setString(++index, dto.getSortedBillDateOtTypeListString());
        ps.setInt(++index, dto.revenueStampDeduction);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionDate);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Food_allowanceDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Food_allowanceDTO dto = new Food_allowanceDTO();
            dto.iD = rs.getLong("ID");
            dto.foodBillId = rs.getLong("food_bill_id");
            dto.budgetOfficeId = rs.getLong("budget_office_id");
            dto.budgetRegisterId = rs.getLong("budget_register_id");
            dto.billRegisterId = rs.getLong("bill_register_id");
            dto.allowanceEmployeeInfoId = rs.getLong("allowance_employee_info_id");
            dto.serialNo = rs.getInt("serial_no");
            dto.employeeRecordsId = rs.getLong("employee_records_id");
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.organogramId = rs.getLong("organogram_id");
            dto.designationPrefix = rs.getString("designation_prefix");
            dto.dailyRate = rs.getLong("daily_rate");
            dto.days = rs.getLong("days");
            dto.calculateAndSetTotalAmount();
            dto.setSortedBillDateOtTypeListFromString(rs.getString("sorted_bill_date_ot_types"));
            dto.calculateAndSetTotalAmount();
            dto.revenueStampDeduction = rs.getInt("revenue_stamp_deduction");
            dto.netAmount = dto.totalAmount - dto.revenueStampDeduction;
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionDate = rs.getLong("insertion_date");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "food_allowance";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Food_allowanceDTO) commonDTO, addSqlQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Food_allowanceDTO) commonDTO, updateSqlQuery, false);
    }

    private static final String findByFoodBillIdSql =
            "SELECT * FROM food_allowance WHERE food_bill_id=? AND isDeleted=0";

    public List<Food_allowanceDTO> findByFoodBillId(long foodBillId) {
        return getDTOs(
                findByFoodBillIdSql,
                Collections.singletonList(foodBillId)
        );
    }

    private static final String findByFoodBillIdAndEmployeeIdSql =
            "SELECT * FROM food_allowance WHERE food_bill_id=%d AND employee_records_id =%d AND isDeleted=0";

    public Food_allowanceDTO findByFoodBillIdAndEmployeeId(long foodBillId, long employeeRecordsId) {
        return ConnectionAndStatementUtil.getT(
                String.format(findByFoodBillIdAndEmployeeIdSql, foodBillId, employeeRecordsId),
                this::buildObjectFromResultSet
        );
    }

    private Food_allowanceEmployeeIdModel buildEmployeeIdModelFromResultSet(ResultSet rs) {
        try {
            Food_allowanceEmployeeIdModel model = new Food_allowanceEmployeeIdModel();
            model.foodAllowanceId = rs.getLong("fa.id");
            model.employeeRecordsId = rs.getLong("fa.employee_records_id");
            model.officeUnitId = rs.getLong("fa.office_unit_id");
            model.organogramId = rs.getLong("fa.organogram_id");
            model.isInPreviewStage = rs.getBoolean("fb.is_in_preview_stage");
            return model;
        } catch (Exception exception) {
            logger.error(exception.getMessage());
        }
        return null;
    }

    private static final String findAddedEmployeeIdModelSql =
            "select fa.id, " +
            "       fa.employee_records_id, " +
            "       fa.office_unit_id, " +
            "       fa.organogram_id, " +
            "       fb.is_in_preview_stage " +
            "from food_bill fb left join food_allowance fa on fb.ID = fa.food_bill_id " +
            "where fb.isDeleted = 0 " +
            "  and fa.isDeleted = 0 " +
            "  and fb.office_unit_id = %d " +
            "  and fb.food_bill_submission_config_id = %d ";

    public List<Food_allowanceEmployeeIdModel> getAddedEmployeeIdModel(long officeUnitsId,
                                                                       long otBillSubmissionConfigId) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(findAddedEmployeeIdModelSql, officeUnitsId, otBillSubmissionConfigId),
                this::buildEmployeeIdModelFromResultSet
        );
    }

    private static final String deleteByFoodBillIdSql =
            "UPDATE food_allowance SET isDeleted = 1,modified_by = %d,lastModificationTime = %d WHERE food_bill_id = %d";

    public void deleteByFoodBillId(long foodBillId, long modifiedBy, long modificationTime) {
        String sql = String.format(deleteByFoodBillIdSql, modifiedBy, modificationTime, foodBillId);
        logger.debug(sql);
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            try {
                model.getStatement().executeUpdate(sql);
                recordUpdateTime(model.getConnection(), getTableName(), modificationTime);
                return true;
            } catch (SQLException ex) {
                ex.printStackTrace();
                return false;
            }
        });
    }
}
