package food_allowance;

import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import overtime_bill.OvertimeInfo;
import overtime_bill.OvertimeType;
import pbReport.DateUtils;
import util.HttpRequestUtils;
import util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class Food_allowanceModel {
    public Long employeeRecordsId = -1L;
    public Long foodAllowanceId = -1L;
    public Integer serialNo = 0;
    public String name = "";
    public String nid = "";
    public String officeName = "";
    public String organogramName = "";
    public String mobileNumber = "";
    public String savingAccountNumber = "";
    public long dailyRate = 0;
    public int revenueStampDeduction = 0;
    public long days = 0;
    public long totalAmount = 0;
    public long netAmount = 0;
    public List<Integer> sortedBillDateOtTypeList;

    public static void fixOrdering(List<Food_allowanceModel> allowanceModels) {
        if (allowanceModels != null) {
            allowanceModels.sort(Comparator.comparingInt(allowanceModel -> allowanceModel.serialNo));
        }
    }

    public Food_allowanceModel() {
    }

    public Food_allowanceModel(AllowanceEmployeeInfoDTO employeeInfoDTO, String language) {
        this();
        setEmployeeInfo(this, employeeInfoDTO, language);
        String designationPrefix = employeeInfoDTO.getDesignationPrefixWithEmploymentCat();
        if (!designationPrefix.isEmpty()) {
            organogramName = designationPrefix + "-" + organogramName;
        }
    }

    public Food_allowanceModel(Food_allowanceDTO allowanceDTO, String language) {
        this();
        AllowanceEmployeeInfoDTO allowanceEmployeeInfoDTO =
                AllowanceEmployeeInfoRepository.getInstance().getById(allowanceDTO.allowanceEmployeeInfoId);
        setEmployeeInfo(this, allowanceEmployeeInfoDTO, language);
        setAllowanceInfo(this, allowanceDTO);
    }

    private static void setEmployeeInfo(Food_allowanceModel model, AllowanceEmployeeInfoDTO employeeInfoDTO, String language) {
        boolean isLangBn = !"English".equalsIgnoreCase(language);
        model.employeeRecordsId = employeeInfoDTO.employeeRecordId;
        model.name = isLangBn ? employeeInfoDTO.nameBn : employeeInfoDTO.nameEn;
        model.officeName = isLangBn ? employeeInfoDTO.officeNameBn : employeeInfoDTO.officeNameEn;
        model.organogramName = isLangBn ? employeeInfoDTO.organogramNameBn : employeeInfoDTO.organogramNameEn;

        model.nid = employeeInfoDTO.nid;
        model.mobileNumber = employeeInfoDTO.mobileNumber;
        model.savingAccountNumber = employeeInfoDTO.savingAccountNumber;
        if (isLangBn) {
            model.nid = StringUtils.convertToBanNumber(model.nid);
            model.mobileNumber = StringUtils.convertToBanNumber(model.mobileNumber);
            model.savingAccountNumber = StringUtils.convertToBanNumber(model.savingAccountNumber);
        }
    }

    private static void setAllowanceInfo(Food_allowanceModel model, Food_allowanceDTO allowanceDTO) {
        model.serialNo = allowanceDTO.serialNo;
        model.foodAllowanceId = allowanceDTO.iD;
        model.dailyRate = allowanceDTO.dailyRate;
        model.days = allowanceDTO.days;
        model.totalAmount = Math.max(0, allowanceDTO.totalAmount);
        model.revenueStampDeduction = Math.max(0, allowanceDTO.revenueStampDeduction);
        model.netAmount = Math.max(0, allowanceDTO.netAmount);
        if (model.totalAmount == 0) {
            model.revenueStampDeduction = 0;
            model.netAmount = 0;
        }
        model.sortedBillDateOtTypeList =
                allowanceDTO.sortedBillDateOtTypeList
                        .stream()
                        .map(OvertimeType::getIntValue)
                        .collect(Collectors.toList());
        if (!allowanceDTO.designationPrefix.isEmpty()) {
            model.organogramName = allowanceDTO.designationPrefix + "-" + model.organogramName;
        }
    }

    public void calculateAndSetDays() {
        if (sortedBillDateOtTypeList == null || sortedBillDateOtTypeList.isEmpty()) {
            days = 0;
            return;
        }
        days = sortedBillDateOtTypeList
                .stream()
                .filter(otType -> otType == OvertimeType.KA.getIntValue())
                .mapToInt(Integer::intValue)
                .count();
    }

    public TreeMap<Long, List<OvertimeInfo>> getOvertimeInfoListBySortedMonth(List<Long> sortedBillDateList) {
        if (sortedBillDateList == null || sortedBillDateList.isEmpty()) {
            return new TreeMap<>();
        }
        TreeMap<Long, List<OvertimeInfo>> overtimeInfoListBySortedMonth = new TreeMap<>();
        for (int index = 0; index < sortedBillDateList.size(); ++index) {
            Long date = sortedBillDateList.get(index);
            long month = DateUtils.get1stDayOfMonth(date);
            if (!overtimeInfoListBySortedMonth.containsKey(month)) {
                overtimeInfoListBySortedMonth.put(month, new ArrayList<>());
            }
            List<OvertimeInfo> overtimeInfoList = overtimeInfoListBySortedMonth.get(month);
            OvertimeType overtimeType;
            if (index < sortedBillDateOtTypeList.size()) {
                overtimeType = OvertimeType.getByIntValue(sortedBillDateOtTypeList.get(index));
            } else {
                overtimeType = OvertimeType.NONE;
            }
            overtimeInfoList.add(new OvertimeInfo(date, overtimeType));
        }
        return overtimeInfoListBySortedMonth;
    }

    public static String getPresentDate(OvertimeInfo overtimeInfo, boolean isLangEng) {
        if (overtimeInfo == null) {
            return "";
        }
        String dayOfTheMonth = String.format("%d", DateUtils.getDayOfTheMonth(overtimeInfo.date));
        return isLangEng ? dayOfTheMonth : StringUtils.convertToBanNumber(dayOfTheMonth);
    }

    public static String getPresentDatesString(List<OvertimeInfo> overtimeInfoList, boolean isLangEng) {
        if (overtimeInfoList == null) {
            return "";
        }
        return overtimeInfoList
                .stream()
                .filter(overtimeInfo -> overtimeInfo.overtimeType == OvertimeType.KA)
                .map(overtimeInfo -> getPresentDate(overtimeInfo, isLangEng))
                .collect(Collectors.joining(","));
    }

    public static Food_allowanceModel mergeSameEmployeeIdModels(List<Food_allowanceModel> modelList) {
        if (modelList == null || modelList.isEmpty()) {
            boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
            throw new IllegalArgumentException(isLangEn ? "Invalid Data In database" : "ডাটাবেজে তথ্য সঠিক নেই");
        }
        Food_allowanceModel mergedModel = modelList.get(0);
        int mergedSortedBillDateOtTypeListSize = mergedModel.sortedBillDateOtTypeList.size();
        for (int i = 0; i < mergedSortedBillDateOtTypeListSize; ++i) {
            for (Food_allowanceModel model : modelList) {
                boolean isNonNoneValue = i < model.sortedBillDateOtTypeList.size()
                                         && OvertimeType.isNonNoneValue(model.sortedBillDateOtTypeList.get(i));
                if (isNonNoneValue) {
                    mergedModel.sortedBillDateOtTypeList.set(i, model.sortedBillDateOtTypeList.get(i));
                    break;
                }
            }
        }
        return mergedModel;
    }

    public static final List<Object> excelTitleRow = Arrays.asList(
            "ক্র. নং", "নাম", "পদবী", "মোবাইল নম্বর", "সঞ্চয়ী হিসাব নম্বর", "টাকার পরিমাণ", "রাজস্ব স্ট্যাম্প বাবদ কর্তন", "নীট দাবী"
    );

    public List<Object> getExcelRow(int index) {
        List<Object> excelRow = new ArrayList<>();
        excelRow.add(StringUtils.convertToBanNumber(String.format("%d", index + 1)));
        excelRow.add(name);
        excelRow.add(organogramName);
        excelRow.add(StringUtils.convertToEngNumber(mobileNumber));
        excelRow.add(StringUtils.convertToEngNumber(savingAccountNumber));
        excelRow.add(totalAmount);
        excelRow.add(revenueStampDeduction);
        excelRow.add(netAmount);
        return excelRow;
    }
}
