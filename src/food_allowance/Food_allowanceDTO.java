package food_allowance;

import com.google.gson.Gson;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import overtime_bill.OvertimeType;
import util.CommonDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Food_allowanceDTO extends CommonDTO {
    private static final Gson GSON = new Gson();
    public long foodBillId = -1;
    public long budgetRegisterId = -1;
    public long billRegisterId = -1;
    public long budgetOfficeId = -1;
    public long allowanceEmployeeInfoId = -1;
    public int serialNo = 0;
    public long employeeRecordsId = -1;
    public long officeUnitId = -1;
    public long organogramId = -1;
    public String designationPrefix = "";
    public long dailyRate = 0;
    public long days = 0;
    public List<OvertimeType> sortedBillDateOtTypeList = new ArrayList<>();
    public long totalAmount = 0;
    public int revenueStampDeduction = 0;
    public long netAmount = -1;
    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionDate = -1;

    public static Comparator<Food_allowanceDTO> compareOfficeUnitThenOrganogramOrderValue
            = Comparator.comparingLong((Food_allowanceDTO dto) -> dto.officeUnitId)
                        .thenComparing(dto -> OfficeUnitOrganogramsRepository.getInstance().getOrderValueById(dto.organogramId));

    public static Comparator<Food_allowanceDTO> orderingComparator = Comparator.comparingInt(dto -> dto.serialNo);

    public String getSortedBillDateOtTypeListString() {
        if (sortedBillDateOtTypeList == null || sortedBillDateOtTypeList.isEmpty()) {
            return "[]";
        }
        return GSON.toJson(
                sortedBillDateOtTypeList.stream()
                                        .map(OvertimeType::getIntValue)
                                        .collect(Collectors.toList())
        );
    }

    public boolean matchSortedBillDateSizedOrThrow(List<Long> sortedBillDateList, boolean isLangEn) {
        boolean isOtTypeListArrayInValid
                = sortedBillDateList == null
                  || sortedBillDateList.isEmpty()
                  || sortedBillDateOtTypeList == null
                  || sortedBillDateOtTypeList.isEmpty()
                  || sortedBillDateList.size() != sortedBillDateOtTypeList.size();
        if (isOtTypeListArrayInValid) {
            throw new IllegalArgumentException(
                    isLangEn ? "Food Bill Dates date has been changed. Try again!"
                             : "খাবার বিলের তারিখ পরিবর্তিত হয়েছে। আবার চেষ্টা করুন!"
            );
        }
        return true;
    }

    public void setSortedBillDateOtTypeListFromString(String sortedBillDateOtTypeListString) {
        sortedBillDateOtTypeList =
                Arrays.stream(GSON.fromJson(sortedBillDateOtTypeListString, int[].class))
                      .mapToObj(OvertimeType::getByIntValue)
                      .collect(Collectors.toList());
    }

    public void setSortedBillDateOtTypeListFromInts(List<Integer> sortedBillDateOtTypeListFromInts) {
        if (sortedBillDateOtTypeListFromInts == null) {
            sortedBillDateOtTypeList = new ArrayList<>();
            return;
        }
        sortedBillDateOtTypeList =
                sortedBillDateOtTypeListFromInts
                        .stream()
                        .map(OvertimeType::getByIntValue)
                        .collect(Collectors.toList());
    }

    public void calculateAndSetTotalAmount() {
        totalAmount = dailyRate * days;
    }
}
