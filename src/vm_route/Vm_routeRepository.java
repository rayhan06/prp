package vm_route;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class Vm_routeRepository implements Repository {
	Vm_routeDAO vm_routeDAO = new Vm_routeDAO();
	
	public void setDAO(Vm_routeDAO vm_routeDAO)
	{
		this.vm_routeDAO = vm_routeDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Vm_routeRepository.class);
	Map<Long, Vm_routeDTO>mapOfVm_routeDTOToiD;
	Gson gson;


	static Vm_routeRepository instance = null;  
	private Vm_routeRepository(){
		mapOfVm_routeDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vm_routeRepository getInstance(){
		if (instance == null){
			instance = new Vm_routeRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(vm_routeDAO == null)
		{
			return;
		}
		try {
			List<Vm_routeDTO> vm_routeDTOs = (List<Vm_routeDTO>) vm_routeDAO.getAll(reloadAll);
			for(Vm_routeDTO vm_routeDTO : vm_routeDTOs) {
				Vm_routeDTO oldVm_routeDTO = getVm_routeDTOByIDWithoutClone(vm_routeDTO.iD);
				if( oldVm_routeDTO != null ) {
					mapOfVm_routeDTOToiD.remove(oldVm_routeDTO.iD);
					
					
				}
				if(vm_routeDTO.isDeleted == 0) 
				{
					
					mapOfVm_routeDTOToiD.put(vm_routeDTO.iD, vm_routeDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Vm_routeDTO> getVm_routeList() {
		List <Vm_routeDTO> vm_routes = new ArrayList<Vm_routeDTO>(this.mapOfVm_routeDTOToiD.values());
		return clone(vm_routes);
	}
	
	
	public Vm_routeDTO getVm_routeDTOByIDWithoutClone( long ID){
		return mapOfVm_routeDTOToiD.get(ID);
	}

	public Vm_routeDTO getVm_routeDTOByID( long ID){
		return clone(mapOfVm_routeDTOToiD.get(ID));
	}

	public Vm_routeDTO clone(Vm_routeDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Vm_routeDTO.class);
	}

	public List<Vm_routeDTO> clone(List<Vm_routeDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_route";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


