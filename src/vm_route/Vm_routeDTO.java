package vm_route;
import java.util.*; 
import util.*; 


public class Vm_routeDTO extends CommonDTO
{

	public long vehicleId = -1;
	public long vehicleDriverAssignmentId = -1;
	public int vehicleTypeCat = -1;
    public String nameEn = "";
    public String nameBn = "";
//    public String remarks = "";
	public int status = -1;
	public int occupied_seats = 0;
	public long filesDropzone = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public int count = 0;
	
	public List<VmRouteStoppageDTO> vmRouteStoppageDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Vm_routeDTO[" +
            " iD = " + iD +
            " vehicleId = " + vehicleId +
            " vehicleDriverAssignmentId = " + vehicleDriverAssignmentId +
            " vehicleTypeCat = " + vehicleTypeCat +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " remarks = " + remarks +
            " status = " + status +
            " filesDropzone = " + filesDropzone +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            "]";
    }

}