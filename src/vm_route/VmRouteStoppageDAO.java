package vm_route;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class VmRouteStoppageDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public VmRouteStoppageDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new VmRouteStoppageMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"vm_route_id",
			"stoppage_name",
			"rent",
			"start_date",
			"end_date",
			"self_id",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public VmRouteStoppageDAO()
	{
		this("vm_route_stoppage");		
	}
	
	public void setSearchColumn(VmRouteStoppageDTO vmroutestoppageDTO)
	{
		vmroutestoppageDTO.searchColumn = "";
		vmroutestoppageDTO.searchColumn += vmroutestoppageDTO.stoppageName + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		VmRouteStoppageDTO vmroutestoppageDTO = (VmRouteStoppageDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(vmroutestoppageDTO);
		if(isInsert)
		{
			ps.setObject(index++,vmroutestoppageDTO.iD);
		}
		ps.setObject(index++,vmroutestoppageDTO.vmRouteId);
		ps.setObject(index++,vmroutestoppageDTO.stoppageName);
		ps.setObject(index++,vmroutestoppageDTO.rent);
		ps.setObject(index++,vmroutestoppageDTO.start_date);
		ps.setObject(index++,vmroutestoppageDTO.end_date);
		ps.setObject(index++,vmroutestoppageDTO.self_id);
		ps.setObject(index++,vmroutestoppageDTO.insertedByUserId);
		ps.setObject(index++,vmroutestoppageDTO.insertedByOrganogramId);
		ps.setObject(index++,vmroutestoppageDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}

	public VmRouteStoppageDTO build(ResultSet rs)
	{
		try
		{
			VmRouteStoppageDTO vmroutestoppageDTO = new VmRouteStoppageDTO();
			vmroutestoppageDTO.iD = rs.getLong("ID");
			vmroutestoppageDTO.self_id = rs.getLong("self_id");
			vmroutestoppageDTO.start_date = rs.getLong("start_date");
			vmroutestoppageDTO.end_date = rs.getLong("end_date");
			vmroutestoppageDTO.vmRouteId = rs.getLong("vm_route_id");
			vmroutestoppageDTO.stoppageName = rs.getString("stoppage_name");
			vmroutestoppageDTO.rent = rs.getDouble("rent");
			vmroutestoppageDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			vmroutestoppageDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			vmroutestoppageDTO.insertionDate = rs.getLong("insertion_date");
			vmroutestoppageDTO.isDeleted = rs.getInt("isDeleted");
			vmroutestoppageDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return vmroutestoppageDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	
	
	
		
	
	public void deleteVmRouteStoppageByVmRouteID(long vmRouteID) throws Exception{

		String sql = "UPDATE vm_route_stoppage SET isDeleted=0 WHERE vm_route_id="+vmRouteID;

		ConnectionAndStatementUtil.getWriteConnectionAndStatement(model->{

			try {
				model.getStatement().executeUpdate(sql);
//				recordUpdateTime(model.getConnection(),"vm_route_stoppage",System.currentTimeMillis());
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		});
	}		
   
	public List<VmRouteStoppageDTO> getVmRouteStoppageDTOListByVmRouteID(long vmRouteID) throws Exception{


		String sql = "SELECT * FROM vm_route_stoppage where isDeleted=0 and vm_route_id= ?  order by vm_route_stoppage.lastModificationTime";
		printSql(sql);
		return ConnectionAndStatementUtil.
				getListOfT(sql, Arrays.asList(vmRouteID),this::build);
	}

	public List<VmRouteStoppageDTO> getVmRouteStoppageDTOListBySelfId(long selfId) throws Exception{

		String sql = "SELECT * FROM vm_route_stoppage where isDeleted=0 and self_id= ?  order by vm_route_stoppage.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.
				getListOfT(sql, Arrays.asList(selfId),this::build);
	}

	public List<VmRouteStoppageDTO> getAllVmRouteStoppageDTOList() throws Exception{


		return VmStoppageRepository.getInstance()
				.getVmRouteStoppageDTOs()
				.stream()
				.filter(i -> i.self_id == -1)
				.collect(Collectors.toList());

//		String sql = "SELECT * FROM vm_route_stoppage where isDeleted=0 and self_id= -1 order by vm_route_stoppage.lastModificationTime desc";
//		printSql(sql);
//		return ConnectionAndStatementUtil.getListOfT(sql, this::build);
	}

	//need another getter for repository
	public VmRouteStoppageDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		VmRouteStoppageDTO vmRouteStoppageDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
		return vmRouteStoppageDTO;
	}
	
	
	
	
	public List<VmRouteStoppageDTO> getDTOs(Collection recordIDs){
		if(recordIDs.isEmpty()){
			return new ArrayList<>();
		}
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+= " ? ";
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);

	
	}
	


	
	public List<VmRouteStoppageDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<VmRouteStoppageDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{

		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	
	}

	public String getOptionList(String Language, long iD) throws Exception {

		String selected = "";
		String sOptions = "";
//		if (Language.equals("English")) {
//			sOptions = "<option value = '-1' "+selected+">Select</option>";
//		} else {
//			sOptions = "<option value = '-1' "+selected+">বাছাই করুন</option>";
//		}

		List<VmRouteStoppageDTO> dtos = getAllVmRouteStoppageDTOList();

		for(int i = 0; i < dtos.size(); i++){
			selected = dtos.get(i).iD == iD?"selected":"";
			sOptions += "<option value = '" + dtos.get(i).iD + "' "+selected+" >" + dtos.get(i).stoppageName + "</option>";
		}

		return sOptions;
	}


	public double getPriceByIDAndDate (long ID, long date) throws Exception {
		double price = 0.0;
		VmRouteStoppageDTO dto = VmStoppageRepository.getInstance().getVmRouteStoppageDTOByID(ID);
//				getDTOByID(ID);
		if(dto != null){
			if(date >= dto.start_date){
				price = dto.rent;
			} else {
				List<VmRouteStoppageDTO> stoppageDTOS = VmStoppageRepository.getInstance().
						getVmRouteStoppageDTOBySelfId(dto.iD);
//						getVmRouteStoppageDTOListBySelfId(dto.iD);
				for(VmRouteStoppageDTO stoppageDTO: stoppageDTOS){
					if(stoppageDTO.start_date <= date && stoppageDTO.end_date >= date){
						price = stoppageDTO.rent;
						break;
					}
				}
			}
		}

		return price;

	}



				
}
	