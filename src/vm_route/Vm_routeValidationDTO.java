package vm_route;

public class Vm_routeValidationDTO
{

	public boolean flag = false;
	public String errMsg = "";
	
    @Override
	public String toString() {
            return "Vm_routeValidationDTO[" +
            " flag = " + flag +
            " errMsg = " + errMsg +
            "]";
    }

}