package vm_route;
import java.util.*; 
import util.*; 


public class VmRouteStoppageDTO extends CommonDTO
{

	public long vmRouteId = -1;
	public long self_id = -1;
	public long start_date = -1;
	public long end_date = -1;
    public String stoppageName = "";
	public double rent = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	
	public List<VmRouteStoppageDTO> vmRouteStoppageDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$VmRouteStoppageDTO[" +
            " iD = " + iD +
            " vmRouteId = " + vmRouteId +
            " stoppageName = " + stoppageName +
            " rent = " + rent +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}