package vm_route;
import java.util.*; 
import util.*;


public class VmRouteStoppageMAPS extends CommonMaps
{	
	public VmRouteStoppageMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("vmRouteId".toLowerCase(), "vmRouteId".toLowerCase());
		java_DTO_map.put("stoppageName".toLowerCase(), "stoppageName".toLowerCase());
		java_DTO_map.put("rent".toLowerCase(), "rent".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("vm_route_id".toLowerCase(), "vmRouteId".toLowerCase());
		java_SQL_map.put("stoppage_name".toLowerCase(), "stoppageName".toLowerCase());
		java_SQL_map.put("rent".toLowerCase(), "rent".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Vm Route Id".toLowerCase(), "vmRouteId".toLowerCase());
		java_Text_map.put("Stoppage Name".toLowerCase(), "stoppageName".toLowerCase());
		java_Text_map.put("Rent".toLowerCase(), "rent".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}