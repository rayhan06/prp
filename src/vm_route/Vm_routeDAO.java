package vm_route;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pb.CatDAO;
import pb.CatRepository;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;
import util.UtilCharacter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;

public class Vm_routeDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public static HashMap<Integer, String[]> statusMap = new HashMap() {{
		put(1, new String[]{"Active", "সক্রিয়"});
		put(2, new String[]{"In Active", "নিষ্ক্রিয় "});
	}};


	public static String getStatus(int value, String language){
		String[] s = statusMap.get(value);
		if(s != null){
			return UtilCharacter.getDataByLanguage(language, s[1], s[0]);

		} else {
			return UtilCharacter.getDataByLanguage(language, "পাওয়া যায় নি ", "Not Available");
		}

	}

	public static String getStatusList(String Language, int value){

		String selected = "";
		String sOptions = "";
		if (Language.equals("English")) {
			sOptions = "<option value = '-1' "+selected+">Select</option>";
		} else {
			sOptions = "<option value = '-1' "+selected+">বাছাই করুন</option>";
		}

		for(int i = 1; i <= statusMap.size(); i++){
			selected = i == value?"selected":"";
			if (Language.equals("English")) {
				sOptions += "<option value = '" + i+ "' "+selected+" >" + statusMap.get(i)[0] + "</option>";
			} else {
				sOptions += "<option value = '" + i + "' "+ selected +">" + statusMap.get(i)[1] + "</option>";
			}
		}

		return sOptions;
	}
	

	
	public Vm_routeDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Vm_routeMAPS(tableName);
		columnNames = new String[]
		{
			"ID",
			"vehicle_id",
			"vehicle_driver_assignment_id",
			"vehicle_type_cat",
			"name_en",
			"name_bn",
			"remarks",
			"status",
			"occupied_seats",
			"files_dropzone",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Vm_routeDAO()
	{
		this("vm_route");
	}
	
	public void setSearchColumn(Vm_routeDTO vm_routeDTO)
	{
		vm_routeDTO.searchColumn = "";
		vm_routeDTO.searchColumn += CatRepository.getName("English", "vehicle_type", vm_routeDTO.vehicleTypeCat) + " " + CatRepository.getName("Bangla", "vehicle_type", vm_routeDTO.vehicleTypeCat) + " ";
		vm_routeDTO.searchColumn += vm_routeDTO.nameEn + " ";
		vm_routeDTO.searchColumn += vm_routeDTO.nameBn + " ";
		vm_routeDTO.searchColumn += vm_routeDTO.remarks + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Vm_routeDTO vm_routeDTO = (Vm_routeDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();
		setSearchColumn(vm_routeDTO);
		if(isInsert)
		{
			ps.setObject(index++,vm_routeDTO.iD);
		}
		ps.setObject(index++,vm_routeDTO.vehicleId);
		ps.setObject(index++,vm_routeDTO.vehicleDriverAssignmentId);
		ps.setObject(index++,vm_routeDTO.vehicleTypeCat);
		ps.setObject(index++,vm_routeDTO.nameEn);
		ps.setObject(index++,vm_routeDTO.nameBn);
		ps.setObject(index++,vm_routeDTO.remarks);
		ps.setObject(index++,vm_routeDTO.status);
		ps.setObject(index++,vm_routeDTO.occupied_seats);
		ps.setObject(index++,vm_routeDTO.filesDropzone);
		ps.setObject(index++,vm_routeDTO.insertedByUserId);
		ps.setObject(index++,vm_routeDTO.insertedByOrganogramId);
		ps.setObject(index++,vm_routeDTO.insertionDate);
		ps.setObject(index++,vm_routeDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}


	public Vm_routeDTO build(ResultSet rs)
	{
		try
		{
			Vm_routeDTO vm_routeDTO = new Vm_routeDTO();
			vm_routeDTO.iD = rs.getLong("ID");
			vm_routeDTO.vehicleId = rs.getLong("vehicle_id");
			vm_routeDTO.vehicleDriverAssignmentId = rs.getLong("vehicle_driver_assignment_id");
			vm_routeDTO.vehicleTypeCat = rs.getInt("vehicle_type_cat");
			vm_routeDTO.nameEn = rs.getString("name_en");
			vm_routeDTO.nameBn = rs.getString("name_bn");
			vm_routeDTO.remarks = rs.getString("remarks");
			vm_routeDTO.status = rs.getInt("status");
			vm_routeDTO.occupied_seats = rs.getInt("occupied_seats");
			vm_routeDTO.filesDropzone = rs.getLong("files_dropzone");
			vm_routeDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			vm_routeDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			vm_routeDTO.insertionDate = rs.getLong("insertion_date");
			vm_routeDTO.isDeleted = rs.getInt("isDeleted");
			vm_routeDTO.lastModificationTime = rs.getLong("lastModificationTime");
			vm_routeDTO.searchColumn = rs.getString("search_column");
			return vm_routeDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	public Vm_routeDTO buildForCount(ResultSet rs)
	{
		try
		{
			Vm_routeDTO vm_routeDTO = new Vm_routeDTO();
			vm_routeDTO.count = rs.getInt("count");
			return vm_routeDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}


	public Vm_routeDTO getDTOByID (long ID) throws Exception {

		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		Vm_routeDTO vm_routeDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);

		if(vm_routeDTO != null){
			VmRouteStoppageDAO vmRouteStoppageDAO = new VmRouteStoppageDAO("vm_route_stoppage");
			List<VmRouteStoppageDTO> vmRouteStoppageDTOList =
					vmRouteStoppageDAO.getVmRouteStoppageDTOListByVmRouteID(vm_routeDTO.iD);
			vm_routeDTO.vmRouteStoppageDTOList = vmRouteStoppageDTOList;
		}

		return vm_routeDTO;
	}
	
		
	

	//need another getter for repository
	
	

	
	
	
	//add repository


	public List<Vm_routeDTO> getAllVm_routeByVehicleId (long vehicleId)
	{

		String sql = "SELECT * FROM vm_route";
		sql += " WHERE ";
		sql+=" isDeleted =  0 and vehicle_id = ? ";

		sql += " order by vm_route.lastModificationTime desc";
		printSql(sql);

		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(vehicleId),this::build);
	}


	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ? ";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("vehicle_type_cat")
						|| str.equals("name_en")
						|| str.equals("name_bn")
						|| str.equals("start_year")
						|| str.equals("end_year")
//						|| str.equals("remarks")
//						|| str.equals("insertion_date_start")
//						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("vehicle_type_cat"))
					{
						AllFieldSql += "" + tableName + ".vehicle_type_cat = ? " ;
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".name_en like  ? ";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("name_bn"))
					{
						AllFieldSql += "" + tableName + ".name_bn like ? ";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("start_year"))
                     {
                         AllFieldSql += "" + tableName + ".insertion_date >= ? ";
						 long epoch;
						 try {
							  epoch = new java.text.SimpleDateFormat("MM/dd/yyyy")
									 .parse("01/01/" + Long.parseLong((String) p_searchCriteria.get(str))).getTime();
						 }  catch (ParseException e) {
							 epoch =3661L; // epoch timestamp of year 1970
						 }
						 objectList.add(epoch);
                         i ++;
                     }
					 else if(str.equals("end_year"))
					 {
						 AllFieldSql += "" + tableName + ".insertion_date <= ? ";
						 long epoch;
						 try {
							 epoch = new java.text.SimpleDateFormat("MM/dd/yyyy")
									 .parse("12/31/" + Long.parseLong((String) p_searchCriteria.get(str))).getTime();
						 }  catch (ParseException e) {
							 epoch =4102448461L; // epoch timestamp of year 2100
						 }
						 objectList.add(epoch);
						 i ++;
					 }
//					else if(str.equals("remarks"))
//					{
//						AllFieldSql += "" + tableName + ".remarks like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("insertion_date_start"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("insertion_date_end"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
//						i ++;
//					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";

		sql += " (" + tableName + ".isDeleted = 0 ";
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }


	public int getCountOfVehicleId (long vehicleId)
	{

		return (int) Vm_routeRepository.getInstance().
				getVm_routeList().stream().filter(i -> i.vehicleId == vehicleId).count();


//		String sql = "SELECT COUNT(*) AS count FROM vm_route ";
//		sql+=" WHERE isDeleted = 0 AND vehicle_id = ? " ;
//		printSql(sql);
//
//		int count = 0;
//
//		Vm_routeDTO dto = ConnectionAndStatementUtil.
//				getT(sql, Arrays.asList(vehicleId),this::buildForCount);
//		if(dto != null){
//			count = dto.count;
//		}
//		return count;
	}
				
}
	