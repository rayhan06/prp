package vm_route;

import com.google.gson.Gson;
import common.ApiResponse;
import files.FilesDAO;
import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;
import vm_route_travel.Vm_route_responseDTO;
import vm_vehicle.Vm_vehicleDAO;
import vm_vehicle.Vm_vehicleDTO;
import vm_vehicle.Vm_vehicleRepository;
import vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentDAO;
import vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentDTO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Servlet implementation class Vm_routeServlet
 */
@WebServlet("/Vm_routeServlet")
@MultipartConfig
public class Vm_routeServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Vm_routeServlet.class);

    String tableName = "vm_route";

	Vm_routeDAO vm_routeDAO;
	CommonRequestHandler commonRequestHandler;
	FilesDAO filesDAO = new FilesDAO();
	VmRouteStoppageDAO vmRouteStoppageDAO;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vm_routeServlet()
	{
        super();
    	try
    	{
			vm_routeDAO = new Vm_routeDAO(tableName);
			vmRouteStoppageDAO = new VmRouteStoppageDAO("vm_route_stoppage");
			commonRequestHandler = new CommonRequestHandler(vm_routeDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_UPDATE))
				{
					getVm_route(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getStoppagesByRootId"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_UPDATE))
				{
					getStoppagesByRootId(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getStoppagesInfoById"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_UPDATE))
				{
					getStoppagesInfoById(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("downloadDropzoneFile"))
			{
				commonRequestHandler.downloadDropzoneFile(request, response, filesDAO);
			}
			else if(actionType.equals("DeleteFileFromDropZone"))
			{
				commonRequestHandler.deleteFileFromDropZone(request, response, filesDAO);
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("vehicleValidation"))
			{
				Vm_routeValidationDTO validationDTO = new Vm_routeValidationDTO();
				long vehicleId = Long.parseLong(request.getParameter("vehicleId"));
				String type = request.getParameter("type");
				int count = vm_routeDAO.getCountOfVehicleId(vehicleId);
				if(type.equalsIgnoreCase("add")){
					if(count == 0){
						validationDTO.flag = true;
					}


				}else {
					Vm_routeDTO dto = Vm_routeRepository.getInstance().getVm_routeDTOByID
							(Long.parseLong(request.getParameter("iD")));

					if(dto.vehicleId == vehicleId && count == 1){
						validationDTO.flag = true;
					} else if(dto.vehicleId != vehicleId && count == 0){
						validationDTO.flag = true;
					}

				}

				PrintWriter out = response.getWriter();
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");

				String encoded = this.gson.toJson(validationDTO);
				System.out.println("json encoded data = " + encoded);
				out.print(encoded);
				out.flush();

			}

			else if(actionType.equals("driverValidation"))
			{
				Vm_routeValidationDTO validationDTO = new Vm_routeValidationDTO();
				long vehicleId = Long.parseLong(request.getParameter("vehicleId"));
				int countOfAssignment = new Vm_vehicle_driver_assignmentDAO().
						getCountByVehicleId(vehicleId);
				if(countOfAssignment > 0){
					validationDTO.flag = true;
				}

				if(!validationDTO.flag){
					validationDTO.errMsg = "No Driver Assigned to this vehicle";
				}

				PrintWriter out = response.getWriter();
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");

				String encoded = this.gson.toJson(validationDTO);
				System.out.println("json encoded data = " + encoded);
				out.print(encoded);
				out.flush();

			}

			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchVm_route(request, response, isPermanentTable, filter);
						}
						else
						{
							searchVm_route(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchVm_route(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_ADD))
				{
					System.out.println("going to  addVm_route ");
					addVm_route(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addVm_route ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("UploadFilesFromDropZone"))
			{
				commonRequestHandler.UploadFilesFromDropZone(request, response, userDTO);
			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addVm_route ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_UPDATE))
				{
					addVm_route(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_SEARCH))
				{
					searchVm_route(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Vm_routeDTO vm_routeDTO = Vm_routeRepository.getInstance().getVm_routeDTOByID
					(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(vm_routeDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addVm_route(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		ApiResponse apiResponse;
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addVm_route");
			String path = getServletContext().getRealPath("/img2/");
			Vm_routeDTO vm_routeDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				vm_routeDTO = new Vm_routeDTO();
			}
			else
			{
				vm_routeDTO = Vm_routeRepository.getInstance().getVm_routeDTOByID((Long.parseLong(request.getParameter("iD"))));
			}

			String Value = "";

			Value = request.getParameter("vehicleId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("vehicleId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_routeDTO.vehicleId = Long.parseLong(Value);
			}
			else
			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				throw new Exception(" Invalid vehicle");
			}

//			Value = request.getParameter("vehicleDriverAssignmentId");
//
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("vehicleDriverAssignmentId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				vm_routeDTO.vehicleDriverAssignmentId = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}

			Value = request.getParameter("vehicleTypeCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("vehicleTypeCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_routeDTO.vehicleTypeCat = Integer.parseInt(Value);
				if(vm_routeDTO.vehicleTypeCat == -1){
					throw new Exception(" Select vehicle type");
				}
			}
			else
			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				throw new Exception(" Invalid vehicle type");
			}

			Value = request.getParameter("nameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEn = " + Value);
			if(Value != null && Value.length() > 0)
			{
				vm_routeDTO.nameEn = (Value);
			}
			else
			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				throw new Exception(" Invalid name En");
			}

			Value = request.getParameter("nameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameBn = " + Value);
			if(Value != null && Value.length() > 0)
			{
				vm_routeDTO.nameBn = (Value);
			}
			else
			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				throw new Exception(" Invalid name Bn");
			}

			Value = request.getParameter("remarks");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("remarks = " + Value);
			if(Value != null)
			{
				vm_routeDTO.remarks = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("status");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("status = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_routeDTO.status = Integer.parseInt(Value);
				if(vm_routeDTO.status == -1){
					throw new Exception(" Select status");
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				throw new Exception(" Invalid status");
			}

			Value = request.getParameter("filesDropzone");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("filesDropzone = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				System.out.println("filesDropzone = " + Value);

				vm_routeDTO.filesDropzone = Long.parseLong(Value);


				if(addFlag == false)
				{
					String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
					String[] deleteArray = filesDropzoneFilesToDelete.split(",");
					for(int i = 0; i < deleteArray.length; i ++)
					{
						System.out.println("going to delete " + deleteArray[i]);
						if(i>0)
						{
							filesDAO.delete(Long.parseLong(deleteArray[i]));
						}
					}
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				vm_routeDTO.insertedByUserId = userDTO.ID;
				vm_routeDTO.insertedByOrganogramId = userDTO.organogramID;
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				vm_routeDTO.insertionDate = c.getTimeInMillis();
			}

			System.out.println("Done adding  addVm_route dto = " + vm_routeDTO);


			Vm_vehicle_driver_assignmentDTO driver_assignmentDTO = new Vm_vehicle_driver_assignmentDAO().
					getAllVm_vehicle_driver_assignmentByVehicleId(vm_routeDTO.vehicleId);

			if(driver_assignmentDTO != null){
				vm_routeDTO.vehicleDriverAssignmentId = driver_assignmentDTO.iD;
			} else {
				throw new Exception(" No driver assigned in this vehicle");
			}

			childValidation(request);




			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				vm_routeDAO.setIsDeleted(vm_routeDTO.iD, CommonDTO.OUTDATED);
				returnedID = vm_routeDAO.add(vm_routeDTO);
				vm_routeDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = vm_routeDAO.manageWriteOperations(vm_routeDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = vm_routeDAO.manageWriteOperations(vm_routeDTO, SessionConstants.UPDATE, -1, userDTO);
			}

			long finalReturnedID = returnedID;
			List<Vm_routeDTO> dtos = vm_routeDAO.
					getAllVm_routeByVehicleId(vm_routeDTO.vehicleId).
					stream().filter(i -> i.iD != finalReturnedID).collect(Collectors.toList());

//			System.out.println("#####");
//
//			System.out.println(dtos);
//
//			System.out.println("#####");


			for(Vm_routeDTO dto: dtos){
				dto.vehicleId = -1;
				dto.vehicleDriverAssignmentId = -1;
				dto.status = 2;
				vm_routeDAO.update(dto);
			}













			List<VmRouteStoppageDTO> vmRouteStoppageDTOList = createVmRouteStoppageDTOListByRequest(request);
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(vmRouteStoppageDTOList != null)
				{
					for(VmRouteStoppageDTO vmRouteStoppageDTO: vmRouteStoppageDTOList)
					{
						vmRouteStoppageDTO.vmRouteId = vm_routeDTO.iD;
						vmRouteStoppageDAO.add(vmRouteStoppageDTO);
					}
				}

			}
			else
			{
				List<Long> childIdsFromRequest = vmRouteStoppageDAO.getChildIdsFromRequest(request, "vmRouteStoppage");
				//delete the removed children
				vmRouteStoppageDAO.deleteChildrenNotInList("vm_route", "vm_route_stoppage", vm_routeDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = vmRouteStoppageDAO.getChilIds("vm_route", "vm_route_stoppage", vm_routeDTO.iD);


				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							VmRouteStoppageDTO vmRouteStoppageDTO =  createVmRouteStoppageDTOByRequestAndIndex(request, false, i);
							vmRouteStoppageDTO.vmRouteId = vm_routeDTO.iD;

							VmRouteStoppageDTO existingStoppageDTO = VmStoppageRepository.getInstance().
									getVmRouteStoppageDTOByID(childIDFromRequest);
//									vmRouteStoppageDAO.getDTOByID(childIDFromRequest);

							if(vmRouteStoppageDTO.rent != existingStoppageDTO.rent){
								VmRouteStoppageDTO newDTO = new VmRouteStoppageDTO();
								newDTO.self_id = childIDFromRequest;
								newDTO.rent = existingStoppageDTO.rent;
								newDTO.start_date = existingStoppageDTO.start_date;
								newDTO.end_date = vmRouteStoppageDTO.start_date - 86400000;
								vmRouteStoppageDAO.add(newDTO);
							}
							vmRouteStoppageDAO.update(vmRouteStoppageDTO);
						}
						else
						{
							VmRouteStoppageDTO vmRouteStoppageDTO =  createVmRouteStoppageDTOByRequestAndIndex(request, true, i);
							vmRouteStoppageDTO.vmRouteId = vm_routeDTO.iD;
							vmRouteStoppageDAO.add(vmRouteStoppageDTO);
						}
					}
				}
				else
				{
					vmRouteStoppageDAO.deleteVmRouteStoppageByVmRouteID(vm_routeDTO.iD);
				}

			}


			apiResponse = ApiResponse.makeSuccessResponse("Vm_routeServlet?actionType=search");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			apiResponse = ApiResponse.makeErrorResponse(e.getMessage());
		}

		PrintWriter pw = response.getWriter();
		pw.write(apiResponse.getJSONString());
		pw.flush();
		pw.close();
	}





	private List<VmRouteStoppageDTO> createVmRouteStoppageDTOListByRequest(HttpServletRequest request) throws Exception{
		List<VmRouteStoppageDTO> vmRouteStoppageDTOList = new ArrayList<VmRouteStoppageDTO>();
		if(request.getParameterValues("vmRouteStoppage.iD") != null)
		{
			int vmRouteStoppageItemNo = request.getParameterValues("vmRouteStoppage.iD").length;


			for(int index=0;index<vmRouteStoppageItemNo;index++){
				VmRouteStoppageDTO vmRouteStoppageDTO = createVmRouteStoppageDTOByRequestAndIndex(request,true,index);
				vmRouteStoppageDTOList.add(vmRouteStoppageDTO);
			}

			return vmRouteStoppageDTOList;
		}
		return null;
	}


	private VmRouteStoppageDTO createVmRouteStoppageDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{

		VmRouteStoppageDTO vmRouteStoppageDTO;
		if(addFlag == true )
		{
			vmRouteStoppageDTO = new VmRouteStoppageDTO();
		}
		else
		{
			vmRouteStoppageDTO = VmStoppageRepository.getInstance().getVmRouteStoppageDTOByID(
					Long.parseLong(request.getParameterValues("vmRouteStoppage.iD")[index]));
//					(VmRouteStoppageDTO)vmRouteStoppageDAO.getDTOByID(Long.parseLong(request.getParameterValues("vmRouteStoppage.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");



		String Value = "";
		Value = request.getParameterValues("vmRouteStoppage.vmRouteId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmRouteStoppageDTO.vmRouteId = Long.parseLong(Value);
		Value = request.getParameterValues("vmRouteStoppage.stoppageName")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmRouteStoppageDTO.stoppageName = (Value);
		Value = request.getParameterValues("vmRouteStoppage.rent")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmRouteStoppageDTO.rent = Double.parseDouble(Value);


		Value = request.getParameterValues("vmRouteStoppage.startDate")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		try
		{
			Date d = f.parse(Value);
			vmRouteStoppageDTO.start_date = d.getTime();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

//		Value = request.getParameterValues("vmRouteStoppage.endDate")[index];
//
//		if(Value != null)
//		{
//			Value = Jsoup.clean(Value,Whitelist.simpleText());
//		}
//
//		try
//		{
//			Date d = f.parse(Value);
//			vmRouteStoppageDTO.end_date = d.getTime();
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//		}



		Value = request.getParameterValues("vmRouteStoppage.insertedByUserId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmRouteStoppageDTO.insertedByUserId = Long.parseLong(Value);
		Value = request.getParameterValues("vmRouteStoppage.insertedByOrganogramId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmRouteStoppageDTO.insertedByOrganogramId = Long.parseLong(Value);
		Value = request.getParameterValues("vmRouteStoppage.insertionDate")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmRouteStoppageDTO.insertionDate = Long.parseLong(Value);
		return vmRouteStoppageDTO;

	}

	private void childValidation(HttpServletRequest request) throws Exception {
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		if(request.getParameterValues("vmRouteStoppage.iD") != null)
		{
			int vmRouteStoppageItemNo = request.getParameterValues("vmRouteStoppage.iD").length;
			String Value = "";
			for(int index=0;index<vmRouteStoppageItemNo;index++){
				Value = request.getParameterValues("vmRouteStoppage.stoppageName")[index];

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText()).trim();
					if(Value.length() == 0){
						throw new Exception("Invalid Stoppage name");
					}
				} else {
					throw new Exception("Invalid Stoppage name");
				}

				Value = request.getParameterValues("vmRouteStoppage.rent")[index];

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText()).trim();
					if(Value.length() == 0){
						throw new Exception("Invalid rent");
					}
					double v = Double.parseDouble(Value);
				} else {
					throw new Exception("Invalid rent");
				}

				Value = request.getParameterValues("vmRouteStoppage.startDate")[index];

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText()).trim();
					if(Value.length() == 0){
						throw new Exception("Invalid start date");
					}
					Date d = f.parse(Value);
					long v = d.getTime();
				} else {
					throw new Exception("Invalid start date");
				}


			}
		}
	}



	private void getVm_route(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getVm_route");
		Vm_routeDTO vm_routeDTO = null;
		try
		{
			vm_routeDTO = Vm_routeRepository.getInstance().getVm_routeDTOByID(id);
			vm_routeDTO.vmRouteStoppageDTOList = VmStoppageRepository.getInstance().
					getVmRouteStoppageDTOByRouteId(id).stream()
			        .sorted((f1, f2) -> Long.compare(f2.lastModificationTime, f1.lastModificationTime))
					.collect(Collectors.toList());
//					(Vm_routeDTO) vm_routeDAO.getDTOByID(id);
			request.setAttribute("ID", vm_routeDTO.iD);
			request.setAttribute("vm_routeDTO",vm_routeDTO);
			request.setAttribute("vm_routeDAO",vm_routeDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "vm_route/vm_routeInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "vm_route/vm_routeSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "vm_route/vm_routeEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "vm_route/vm_routeEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getVm_route(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getVm_route(request, response, Long.parseLong(request.getParameter("ID")));
	}


	private void getStoppagesByRootId(HttpServletRequest request, HttpServletResponse response, int id) throws Exception {
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);

		VmRouteStoppageDAO vm_requisitionDAO = new VmRouteStoppageDAO();
		List<VmRouteStoppageDTO> vmRouteStoppageDTOS = vm_requisitionDAO.getVmRouteStoppageDTOListByVmRouteID(id);

		String tempString = "";

		for (VmRouteStoppageDTO vmRouteStoppageDTO: vmRouteStoppageDTOS) {

			String option = vmRouteStoppageDTO.stoppageName ;
			tempString += "<option value='" + vmRouteStoppageDTO.iD + "'>" + Utils.getDigits(option, Language) + "</option>";
		}

		PrintWriter out = response.getWriter();
		out.println(tempString);
		out.close();
	}

	private void getStoppagesByRootId(HttpServletRequest request, HttpServletResponse response) throws Exception {
		getStoppagesByRootId(request, response, Integer.parseInt(request.getParameter("ID")));
	}

	private void getStoppagesInfoById(HttpServletRequest request, HttpServletResponse response, int id) throws Exception {
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);

		VmRouteStoppageDAO vmRouteStoppageDAO = new VmRouteStoppageDAO();

		VmRouteStoppageDTO vmRouteStoppageDTO = VmStoppageRepository.getInstance().
				getVmRouteStoppageDTOByID(id);
//				vmRouteStoppageDAO.getDTOByID(id);
		Vm_routeDTO vm_routeDTO = Vm_routeRepository.getInstance().getVm_routeDTOByID(vmRouteStoppageDTO.vmRouteId);
		Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().
				getVm_vehicleDTOByID(vm_routeDTO.vehicleId);

		Vm_route_responseDTO responseDTO = new Vm_route_responseDTO();
		responseDTO.rent = Utils.getDigits(vmRouteStoppageDAO.getPriceByIDAndDate(vmRouteStoppageDTO.iD, new Date().getTime()), Language);
		responseDTO.totalSeat = Utils.getDigits(vm_vehicleDTO.numberOfSeats, Language);
		responseDTO.remainingSeat = Utils.getDigits(vm_vehicleDTO.numberOfSeats-vm_routeDTO.occupied_seats, Language);

		PrintWriter out = response.getWriter();
		out.println(new Gson().toJson(responseDTO));
		out.close();
	}

	private void getStoppagesInfoById(HttpServletRequest request, HttpServletResponse response) throws Exception {
		getStoppagesInfoById(request, response, Integer.parseInt(request.getParameter("ID")));
	}


	private void searchVm_route(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchVm_route 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_VM_ROUTE,
			request,
			vm_routeDAO,
			SessionConstants.VIEW_VM_ROUTE,
			SessionConstants.SEARCH_VM_ROUTE,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("vm_routeDAO",vm_routeDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_route/vm_routeApproval.jsp");
	        	rd = request.getRequestDispatcher("vm_route/vm_routeApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_route/vm_routeApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("vm_route/vm_routeApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_route/vm_routeSearch.jsp");
	        	rd = request.getRequestDispatcher("vm_route/vm_routeSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_route/vm_routeSearchForm.jsp");
	        	rd = request.getRequestDispatcher("vm_route/vm_routeSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

