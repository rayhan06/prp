package vm_route;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class VmStoppageRepository implements Repository {
    VmRouteStoppageDAO vmRouteStoppageDAO = null;

    public void setDAO(VmRouteStoppageDAO vmRouteStoppageDAO)
    {
        this.vmRouteStoppageDAO = vmRouteStoppageDAO;
    }


    static Logger logger = Logger.getLogger(VmStoppageRepository.class);

    Map<Long, VmRouteStoppageDTO> mapOfVmRouteStoppageDTOToiD;
    Map<Long, Set<VmRouteStoppageDTO>>mapOfVmRouteStoppageDTOToRouteID;
    Map<Long, Set<VmRouteStoppageDTO>>mapOfVmRouteStoppageDTOToSelfID;
    Gson gson = new Gson();



    static VmStoppageRepository instance = null;
    private VmStoppageRepository(){
        mapOfVmRouteStoppageDTOToiD = new ConcurrentHashMap<>();
        mapOfVmRouteStoppageDTOToRouteID =  new ConcurrentHashMap<>();
        mapOfVmRouteStoppageDTOToSelfID =  new ConcurrentHashMap<>();

        setDAO(new VmRouteStoppageDAO());
        RepositoryManager.getInstance().addRepository(this);
    }

    public synchronized static VmStoppageRepository getInstance(){
        if (instance == null){
            instance = new VmStoppageRepository();
        }
        return instance;
    }

    public void reload(boolean reloadAll){
        if(vmRouteStoppageDAO == null)
        {
            return;
        }
        try {
            List<VmRouteStoppageDTO> dtos =(List<VmRouteStoppageDTO>) vmRouteStoppageDAO.getAll(reloadAll);
            for(VmRouteStoppageDTO dto : dtos) {
                VmRouteStoppageDTO oldDTO = getVmRouteStoppageDTOByIDWithoutClone(dto.iD);
                if( oldDTO != null ) {
                    mapOfVmRouteStoppageDTOToiD.remove(oldDTO.iD);

                    if(mapOfVmRouteStoppageDTOToRouteID.containsKey(oldDTO.vmRouteId)) {
                        mapOfVmRouteStoppageDTOToRouteID.get(oldDTO.vmRouteId).remove(oldDTO);
                    }
                    if(mapOfVmRouteStoppageDTOToRouteID.get(oldDTO.vmRouteId).isEmpty()) {
                        mapOfVmRouteStoppageDTOToRouteID.remove(oldDTO.vmRouteId);
                    }

                    if(mapOfVmRouteStoppageDTOToSelfID.containsKey(oldDTO.self_id)) {
                        mapOfVmRouteStoppageDTOToSelfID.get(oldDTO.self_id).remove(oldDTO);
                    }
                    if(mapOfVmRouteStoppageDTOToSelfID.get(oldDTO.self_id).isEmpty()) {
                        mapOfVmRouteStoppageDTOToSelfID.remove(oldDTO.self_id);
                    }
                }
                if(dto.isDeleted == 0)
                {

                    mapOfVmRouteStoppageDTOToiD.put(dto.iD, dto);

                    if( ! mapOfVmRouteStoppageDTOToRouteID.containsKey(dto.vmRouteId)) {
                        mapOfVmRouteStoppageDTOToRouteID.put(dto.vmRouteId, new HashSet<>());
                    }
                    mapOfVmRouteStoppageDTOToRouteID.get(dto.vmRouteId).add(dto);


                    if( ! mapOfVmRouteStoppageDTOToSelfID.containsKey(dto.self_id)) {
                        mapOfVmRouteStoppageDTOToSelfID.put(dto.self_id, new HashSet<>());
                    }
                    mapOfVmRouteStoppageDTOToSelfID.get(dto.self_id).add(dto);

                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public List<VmRouteStoppageDTO> getVmRouteStoppageDTOByRouteId(long routeId) {
        return clone(new ArrayList<>( mapOfVmRouteStoppageDTOToRouteID.getOrDefault(routeId,new HashSet<>())));
    }

    public List<VmRouteStoppageDTO> getVmRouteStoppageDTOBySelfId(long selfId) {
        return clone(new ArrayList<>( mapOfVmRouteStoppageDTOToSelfID.getOrDefault(selfId,new HashSet<>())));
    }

    public List<VmRouteStoppageDTO> getVmRouteStoppageDTOs() {
        List <VmRouteStoppageDTO> vmRouteStoppageDTOS = new ArrayList<>(this.mapOfVmRouteStoppageDTOToiD.values());
        return clone(vmRouteStoppageDTOS);
    }

    public VmRouteStoppageDTO clone(VmRouteStoppageDTO dto) {
        String raw = gson.toJson(dto);
        return gson.fromJson(raw, VmRouteStoppageDTO.class);
    }

    public List<VmRouteStoppageDTO> clone(List<VmRouteStoppageDTO> dtoList) {
        return dtoList
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }

    public VmRouteStoppageDTO getVmRouteStoppageDTOByID( long ID){
        return clone(mapOfVmRouteStoppageDTOToiD.get(ID));
    }

    public VmRouteStoppageDTO getVmRouteStoppageDTOByIDWithoutClone( long ID){
        return mapOfVmRouteStoppageDTOToiD.get(ID);
    }

    @Override
    public String getTableName() {
        String tableName = "";
        try{
            tableName = "vm_route_stoppage";
        }catch(Exception ex){
            logger.debug("FATAL",ex);
        }
        return tableName;
    }


}
