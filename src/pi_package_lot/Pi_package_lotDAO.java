package pi_package_lot;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Pi_package_lotDAO implements CommonDAOService<Pi_package_lotDTO> {
    Logger logger = Logger.getLogger(getClass());
    private static String addQuery = "";
    private static String updateQuery = "";
    private String[] columnNames = null;
    private final Map<String, String> searchMap = new HashMap<>();

    private Pi_package_lotDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "pi_package_new_id",
                        "office_unit_id",
                        "lot_number_en",
                        "lot_number_bn",
                        "lot_name_en",
                        "lot_name_bn",
                        "search_column",
                        "inserted_by",
                        "modified_by",
                        "insertion_date",
                        "isDeleted",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("office_unit_id", " and (office_unit_id = ?)");
        searchMap.put("lot_number_en", " and (lot_number_en like ?)");
        searchMap.put("lot_number_bn", " and (lot_number_bn like ?)");
        searchMap.put("lot_name_en", " and (lot_name_en like ?)");
        searchMap.put("lot_name_bn", " and (lot_name_bn like ?)");
        searchMap.put("insertion_date_start", " and (insertion_date >= ?)");
        searchMap.put("insertion_date_end", " and (insertion_date <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private static class LazyLoader {
        static final Pi_package_lotDAO INSTANCE = new Pi_package_lotDAO();
    }

    public static Pi_package_lotDAO getInstance() {
        return Pi_package_lotDAO.LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Pi_package_lotDTO pi_package_lotDTO) {
        pi_package_lotDTO.searchColumn = "";
        pi_package_lotDTO.searchColumn += pi_package_lotDTO.lotNameEn + " ";
        pi_package_lotDTO.searchColumn += pi_package_lotDTO.lotNameBn + " ";
        pi_package_lotDTO.searchColumn += pi_package_lotDTO.lotNumberEn + " ";
        pi_package_lotDTO.searchColumn += pi_package_lotDTO.lotNumberBn + " ";
    }

    @Override
    public void set(PreparedStatement ps, Pi_package_lotDTO pi_package_lotDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(pi_package_lotDTO);
        if (isInsert) {
            ps.setObject(++index, pi_package_lotDTO.iD);
        }
        ps.setObject(++index, pi_package_lotDTO.piPackageNewId);
        ps.setObject(++index, pi_package_lotDTO.officeUnitId);
        ps.setObject(++index, pi_package_lotDTO.lotNumberEn);
        ps.setObject(++index, pi_package_lotDTO.lotNumberBn);
        ps.setObject(++index, pi_package_lotDTO.lotNameEn);
        ps.setObject(++index, pi_package_lotDTO.lotNameBn);
        ps.setObject(++index, pi_package_lotDTO.searchColumn);
        ps.setObject(++index, pi_package_lotDTO.insertedBy);
        ps.setObject(++index, pi_package_lotDTO.modifiedBy);
        ps.setObject(++index, pi_package_lotDTO.insertionDate);
        if (isInsert) {
            ps.setObject(++index, pi_package_lotDTO.isDeleted);
        }
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, pi_package_lotDTO.iD);
        }
    }

    @Override
    public Pi_package_lotDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Pi_package_lotDTO pi_package_lotDTO = new Pi_package_lotDTO();
            int i = 0;
            pi_package_lotDTO.iD = rs.getLong(columnNames[i++]);
            pi_package_lotDTO.piPackageNewId = rs.getLong(columnNames[i++]);
            pi_package_lotDTO.officeUnitId = rs.getLong(columnNames[i++]);
            pi_package_lotDTO.lotNumberEn = rs.getString(columnNames[i++]);
            pi_package_lotDTO.lotNumberBn = rs.getString(columnNames[i++]);
            pi_package_lotDTO.lotNameEn = rs.getString(columnNames[i++]);
            pi_package_lotDTO.lotNameBn = rs.getString(columnNames[i++]);
            pi_package_lotDTO.searchColumn = rs.getString(columnNames[i++]);
            pi_package_lotDTO.insertedBy = rs.getString(columnNames[i++]);
            pi_package_lotDTO.modifiedBy = rs.getString(columnNames[i++]);
            pi_package_lotDTO.insertionDate = rs.getLong(columnNames[i++]);
            pi_package_lotDTO.isDeleted = rs.getInt(columnNames[i++]);
            pi_package_lotDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            return pi_package_lotDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Pi_package_lotDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "pi_package_lot";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_package_lotDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_package_lotDTO) commonDTO, updateQuery, false);
    }

    public String getText(String Language, Long id) {
        Pi_package_lotDTO dto = Pi_package_lotDAO.getInstance().getDTOByID(id);
        return dto == null ? "" : Language.equalsIgnoreCase("English") ? dto.lotNumberEn : dto.lotNumberBn;
    }

    public List<Pi_package_lotDTO> getLotsByPackageId(long packageId){
        return (List<Pi_package_lotDTO>) Pi_package_lotDAO.getInstance()
                .getDTOsByParent("pi_package_new_id", packageId);
    }
}
