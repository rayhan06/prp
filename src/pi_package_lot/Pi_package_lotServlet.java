package pi_package_lot;

import com.google.gson.Gson;
import common.ApiResponse;
import common.BaseServlet;
import org.apache.log4j.Logger;
import pb.Utils;
import permission.MenuConstants;
import pi_annual_demand.Pi_annual_demandServlet;
import user.UserDTO;
import util.CommonDTO;
import util.CommonLoginData;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/Pi_package_lotServlet")
@MultipartConfig
public class Pi_package_lotServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_annual_demandServlet.class);

    @Override
    public String getTableName() {
        return Pi_package_lotDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_package_lotServlet";
    }

    @Override
    public Pi_package_lotDAO getCommonDAOService() {
        return Pi_package_lotDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PI_ANNUAL_DEMAND_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PI_ANNUAL_DEMAND_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PI_ANNUAL_DEMAND_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_annual_demandServlet.class;
    }

    Pi_package_lotDAO pi_package_lotDAO = Pi_package_lotDAO.getInstance();
    private final Gson gson = new Gson();

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        try {
            switch (request.getParameter("actionType")) {
                case "getLotOptionsByPackage":
                    if (Utils.checkPermission(commonLoginData.userDTO, getSearchMenuConstants()) && getAddPagePermission(request)) {
                        String packageId = request.getParameter("packageId");
                        String lotId = request.getParameter("piLotId");
                        if (packageId == null || packageId.equals("")) {
                            ApiResponse.sendSuccessResponse(response, "");
                            return;
                        }
                        List<Pi_package_lotDTO> lotDTOs = (List<Pi_package_lotDTO>) Pi_package_lotDAO.getInstance().getDTOsByParent("pi_package_new_id", Long.parseLong(packageId));
                        boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
                        String lotOptions = Utils.buildSelectOption(isLanguageEnglish);
                        StringBuilder option = new StringBuilder();
                        for (Pi_package_lotDTO dto : lotDTOs) {
                            if (lotId != null && !lotId.equals("") && Long.parseLong(lotId) == dto.iD) {
                                option.append("<option value = '").append(dto.iD).append("' selected>");
                            } else {
                                option.append("<option value = '").append(dto.iD).append("'>");
                            }
                            option.append(isLanguageEnglish ? dto.lotNumberEn : dto.lotNumberBn).append("</option>");
                        }
                        lotOptions += option.toString();
                        ApiResponse.sendSuccessResponse(response, lotOptions);
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        super.doGet(request, response);
    }
}

