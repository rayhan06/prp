package pi_package_lot;

import util.CommonDTO;

public class Pi_package_lotDTO extends CommonDTO {
    public long piPackageNewId = -1;
    public long officeUnitId = -1;
    public String lotNumberEn = "";
    public String lotNumberBn = "";
    public String lotNameEn = "";
    public String lotNameBn = "";
    public String insertedBy = "";
    public String modifiedBy = "";
    public long insertionDate = -1;


    @Override
    public String toString() {
        return "$Pi_package_newDTO[" +
                " iD = " + iD +
                " piPackageNewId=" + piPackageNewId +
                " officeUnitId=" + officeUnitId +
                " lotNumberEn='" + lotNumberEn +
                " lotNumberBn='" + lotNumberBn +
                " lotNameEn='" + lotNameEn +
                " lotNameBn='" + lotNameBn +
                " searchColumn = " + searchColumn +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }
}
