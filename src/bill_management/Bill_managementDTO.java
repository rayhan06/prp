package bill_management;

import sessionmanager.SessionConstants;
import util.CommonDTO;

import static util.StringUtils.convertToBanNumber;

public class Bill_managementDTO extends CommonDTO {
    public long budgetSelectionInfoId = -1;
    public long budgetInstitutionalGroupId = -1;
    public long budgetOfficeId = -1;
    public long budgetMappingId = -1;
    public long economicGroupId = -1;
    public long economicCodeId = -1;
    public long economicSubCodeId = -1;
    public String officeLetterNumber = "";
    public long budgetRegisterId = -1;
    public long letterDate = SessionConstants.MIN_DATE;
    public long billAmount = -1;
    public double vatPercent = -1;
    public double taxPercent = -1;
    public double serviceChargePercent = -1;
    public long allocatedBudget = -1;
    public long uptoLastBillTotal = -1;
    public long dueBillAmount = -1;
    public String dueBillDescription = "";
    public long dueVatAmount = -1;
    public String dueVatDescription = "";
    public long dueTaxAmount = -1;
    public String dueTaxDescription = "";
    public long dueServiceChargeAmount = -1;
    public String dueServiceChargeDescription = "";
    public long supplierInstitutionId = -1;
    public long supplierTotalBill = 0;
    public String suppliedItemDescription = "";
    public long fileDropzone = -1;
    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = -1;

    public boolean hasVat() {
        return vatPercent > .0;
    }

    public long getVatAmount() {
        if (!hasVat()) return 0;
        return Bill_managementDTO.getPercentageOfAmount(billAmount, vatPercent);
    }

    public boolean hasTax() {
        return taxPercent > .0;
    }

    public long getTaxAmount() {
        if (!hasTax()) return 0;
        return Bill_managementDTO.getPercentageOfAmount(billAmount, taxPercent);
    }

    public boolean hasServiceCharge() {
        return serviceChargePercent > .0;
    }

    public long getServiceChargeAmount() {
        if (!hasServiceCharge()) return 0;
        return Bill_managementDTO.getPercentageOfAmount(billAmount, serviceChargePercent);
    }

    public static String getDueAmountBanglaText(long amount, long dueAmount, String description) {
        if (dueAmount <= 0) return "";
        String dueAmountStr = convertToBanNumber(String.format("%d", dueAmount));
        String amountStr = convertToBanNumber(String.format("%d", amount));
        String totalStr = convertToBanNumber(String.format("%d", amount + dueAmount));
        return description + " বাবদ " + dueAmountStr + "/- টাকা সহ সর্বমোট=("
               + amountStr + "+" + dueAmountStr + ")=" + totalStr + "/- টাকা অত্র বিল হতে কর্তন করা হলো";
    }

    public static long getRemainingBudget(long allocatedBudget, long uptoLastBillTotal) {
        return (allocatedBudget < uptoLastBillTotal) ? 0 : allocatedBudget - uptoLastBillTotal;
    }

    public static long getPercentageOfAmount(long amount, double percentage) {
        return (long) Math.ceil(amount * (percentage * 0.01d));
    }

    @Override
    public String toString() {
        return "Bill_managementDTO{" +
               "budgetSelectionInfoId=" + budgetSelectionInfoId +
               ", budgetInstitutionalGroupId=" + budgetInstitutionalGroupId +
               ", budgetOfficeId=" + budgetOfficeId +
               ", budgetMappingId=" + budgetMappingId +
               ", economicGroupId=" + economicGroupId +
               ", economicCodeId=" + economicCodeId +
               ", economicSubCodeId=" + economicSubCodeId +
               ", officeLetterNumber='" + officeLetterNumber + '\'' +
               ", letterDate=" + letterDate +
               ", billAmount=" + billAmount +
               ", vatPercent=" + vatPercent +
               ", taxPercent=" + taxPercent +
               ", serviceChargePercent=" + serviceChargePercent +
               ", allocatedBudget=" + allocatedBudget +
               ", dueBillAmount=" + dueBillAmount +
               ", dueBillDescription='" + dueBillDescription + '\'' +
               ", suppliedItemDescription='" + suppliedItemDescription + '\'' +
               ", fileDropzone=" + fileDropzone +
               ", modifiedBy=" + modifiedBy +
               ", insertedBy=" + insertedBy +
               ", insertionTime=" + insertionTime +
               '}';
    }
}