package bill_management;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Bill_managementDAO implements CommonDAOService<Bill_managementDTO> {
    private static final Logger logger = Logger.getLogger(Bill_managementDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (budget_register_id,budget_selection_info_id,budget_institutional_group_id,budget_office_id,budget_mapping_id,"
                    .concat("economic_group_id,economic_code_id,economic_sub_code_id,office_letter_number,letter_date,")
                    .concat("bill_amount,vat_percent,tax_percent,service_charge_percent,allocated_budget,")
                    .concat("upto_last_bill_total,due_bill_amount,due_bill_description,due_vat_amount,due_vat_description,")
                    .concat("due_tax_amount,due_tax_description,due_service_charge_amount,due_service_charge_description,supplier_institution_id,supplier_total_bill,supplied_item_description,")
                    .concat("file_dropzone,modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET budget_register_id=?,budget_selection_info_id=?,budget_institutional_group_id=?,budget_office_id=?,budget_mapping_id=?,"
                    .concat("economic_group_id=?,economic_code_id=?,economic_sub_code_id=?,office_letter_number=?,letter_date=?,")
                    .concat("bill_amount=?,vat_percent=?,tax_percent=?,service_charge_percent=?,")
                    .concat("allocated_budget=?,upto_last_bill_total=?,due_bill_amount=?,due_bill_description=?,")
                    .concat("due_vat_amount=?,due_vat_description=?,due_tax_amount=?,due_tax_description=?,due_service_charge_amount=?,due_service_charge_description=?,")
                    .concat("supplier_institution_id=?,supplier_total_bill=?,supplied_item_description=?,file_dropzone=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private Bill_managementDAO() {
        searchMap.put("budget_selection_info_id", " AND (budget_selection_info_id = ?)");
        searchMap.put("budget_mapping_id", " AND (budget_mapping_id = ?)");
        searchMap.put("budget_office_id", " AND (budget_office_id = ?)");
        searchMap.put("office_letter_number", " AND (office_letter_number LIKE ?)");
        searchMap.put("voucher_number", " AND (budget_register_id LIKE ?)");
        searchMap.put("supplierInstitutionId", " AND (supplier_institution_id = ?)");
    }

    private static class LazyLoader {
        static final Bill_managementDAO INSTANCE = new Bill_managementDAO();
    }

    public static Bill_managementDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Bill_managementDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.budgetRegisterId);
        ps.setLong(++index, dto.budgetSelectionInfoId);
        ps.setLong(++index, dto.budgetInstitutionalGroupId);
        ps.setLong(++index, dto.budgetOfficeId);
        ps.setLong(++index, dto.budgetMappingId);
        ps.setLong(++index, dto.economicGroupId);
        ps.setLong(++index, dto.economicCodeId);
        ps.setLong(++index, dto.economicSubCodeId);
        ps.setString(++index, dto.officeLetterNumber);
        ps.setLong(++index, dto.letterDate);
        ps.setLong(++index, dto.billAmount);
        ps.setDouble(++index, dto.vatPercent);
        ps.setDouble(++index, dto.taxPercent);
        ps.setDouble(++index, dto.serviceChargePercent);
        ps.setLong(++index, dto.allocatedBudget);
        ps.setLong(++index, dto.uptoLastBillTotal);
        ps.setLong(++index, dto.dueBillAmount);
        ps.setString(++index, dto.dueBillDescription);
        ps.setLong(++index, dto.dueVatAmount);
        ps.setString(++index, dto.dueVatDescription);
        ps.setLong(++index, dto.dueTaxAmount);
        ps.setString(++index, dto.dueTaxDescription);
        ps.setLong(++index, dto.dueServiceChargeAmount);
        ps.setString(++index, dto.dueServiceChargeDescription);
        ps.setLong(++index, dto.supplierInstitutionId);
        ps.setLong(++index, dto.supplierTotalBill);
        ps.setString(++index, dto.suppliedItemDescription);
        ps.setLong(++index, dto.fileDropzone);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Bill_managementDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Bill_managementDTO dto = new Bill_managementDTO();
            dto.iD = rs.getLong("ID");
            dto.budgetRegisterId = rs.getLong("budget_register_id");
            dto.budgetSelectionInfoId = rs.getLong("budget_selection_info_id");
            dto.budgetInstitutionalGroupId = rs.getLong("budget_institutional_group_id");
            dto.budgetOfficeId = rs.getLong("budget_office_id");
            dto.budgetMappingId = rs.getLong("budget_mapping_id");
            dto.economicGroupId = rs.getLong("economic_group_id");
            dto.economicCodeId = rs.getLong("economic_code_id");
            dto.economicSubCodeId = rs.getLong("economic_sub_code_id");
            dto.officeLetterNumber = rs.getString("office_letter_number");
            dto.letterDate = rs.getLong("letter_date");
            dto.billAmount = rs.getLong("bill_amount");
            dto.vatPercent = rs.getDouble("vat_percent");
            dto.taxPercent = rs.getDouble("tax_percent");
            dto.serviceChargePercent = rs.getDouble("service_charge_percent");
            dto.allocatedBudget = rs.getLong("allocated_budget");
            dto.uptoLastBillTotal = rs.getLong("upto_last_bill_total");
            dto.dueBillAmount = rs.getLong("due_bill_amount");
            dto.dueBillDescription = rs.getString("due_bill_description");
            dto.dueVatAmount = rs.getLong("due_vat_amount");
            dto.dueVatDescription = rs.getString("due_vat_description");
            dto.dueTaxAmount = rs.getLong("due_tax_amount");
            dto.dueTaxDescription = rs.getString("due_tax_description");
            dto.dueServiceChargeAmount = rs.getLong("due_service_charge_amount");
            dto.dueServiceChargeDescription = rs.getString("due_service_charge_description");
            dto.supplierInstitutionId = rs.getLong("supplier_institution_id");
            dto.supplierTotalBill = rs.getLong("supplier_total_bill");
            dto.suppliedItemDescription = rs.getString("supplied_item_description");
            dto.fileDropzone = rs.getLong("file_dropzone");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "bill_management";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Bill_managementDTO) commonDTO, addSqlQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Bill_managementDTO) commonDTO, updateSqlQuery, false);
    }
}