package bill_management;

import bill_register.Bill_registerServlet;
import budget.BudgetUtils;
import budget_register.Budget_registerDAO;
import budget_register.Budget_registerDTO;
import budget_selection_info.BudgetSelectionInfoRepository;
import com.google.gson.Gson;
import common.BaseServlet;
import economic_operation_mapping.Economic_operation_mappingRepository;
import finance.CashTypeEnum;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import supplier_institution.Supplier_institutionRepository;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@WebServlet("/Bill_managementServlet")
@MultipartConfig
@SuppressWarnings({"Duplicates"})
public class Bill_managementServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static final Set<Long> selectedEconomicGroups = new HashSet<>();
    public static final CashTypeEnum CASH_TYPE = CashTypeEnum.OTHERS;
    private static final Logger logger = Logger.getLogger(Bill_managementServlet.class);

    static {
        selectedEconomicGroups.add(32L);
        selectedEconomicGroups.add(38L);
        selectedEconomicGroups.add(41L);
    }

    @Override
    public String getTableName() {
        return Bill_managementDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Bill_managementServlet";
    }

    @Override
    public Bill_managementDAO getCommonDAOService() {
        return Bill_managementDAO.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "ajax_getDataForSubCode": {
                    Budget_registerDTO budgetRegisterDTO = new Budget_registerDTO();
                    budgetRegisterDTO.iD = Long.parseLong(request.getParameter("budgetRegisterId"));
                    budgetRegisterDTO.budgetMappingId = Long.parseLong(request.getParameter("budgetMappingId"));
                    budgetRegisterDTO.economicSubCodeId = Long.parseLong(request.getParameter("economicSubCodeId"));
                    budgetRegisterDTO.issueDate = Utils.parseMandatoryDate(request.getParameter("letterDate"), null);
                    Long selectionInfoId = BudgetSelectionInfoRepository.getInstance().getId(
                            BudgetUtils.getEconomicYear(budgetRegisterDTO.issueDate)
                    );
                    if (selectionInfoId != null) {
                        budgetRegisterDTO.budgetSelectionInfoId = selectionInfoId;
                    }
                    budgetRegisterDTO.performBillAndBudgetCalculations();

                    Map<String, Object> res = new HashMap<>();
                    res.put("allocatedBudget", budgetRegisterDTO.allocatedBudget);
                    res.put("uptoLastBillTotal", budgetRegisterDTO.uptoLastBillTotal);
                    res.put("remainingBudget", budgetRegisterDTO.remainingBudget);

                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().println(new Gson().toJson(res));
                    return;
                }
                case "ajax_buildEconomicGroup": {
                    long budgetOperationId = Long.parseLong(request.getParameter("budget_operation_id"));
                    String language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
                    String options = Economic_operation_mappingRepository.getInstance().buildEconomicGroupOption(
                            language,
                            budgetOperationId,
                            0L,
                            selectedEconomicGroups
                    );
                    logger.debug(options);
                    response.setContentType("text/html; charset=UTF-8");
                    response.getWriter().println(options);
                    return;
                }
                case "downloadBill": {
                    long id = Long.parseLong(request.getParameter("ID"));
                    Bill_managementDTO billManagementDTO = Bill_managementDAO.getInstance().getDTOFromID(id);
                    if (billManagementDTO == null) throw new FileNotFoundException("no DTO found");
                    request.setAttribute(BaseServlet.DTO_FOR_JSP, billManagementDTO);
                    request.getRequestDispatcher("bill_management/bill_managementBillTemplate.jsp").forward(request, response);
                    return;
                }
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        String url = getServletName() + "?actionType=";

        String source = request.getParameter("source");
        if ("preview".equalsIgnoreCase(source)) {
            url += "view&ID=" + commonDTO.iD;
        } else {
            url += "search";
        }

        return url;
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAjaxAddRedirectURL(request, commonDTO);
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        long currentTime = System.currentTimeMillis();
        Bill_managementDTO billManagementDTO;
        if (addFlag) {
            billManagementDTO = new Bill_managementDTO();
            billManagementDTO.insertedBy = userDTO.ID;
            billManagementDTO.insertionTime = currentTime;
        } else {
            billManagementDTO = Bill_managementDAO.getInstance().getDTOFromID(
                    Long.parseLong(request.getParameter("iD"))
            );
        }
        billManagementDTO.modifiedBy = userDTO.ID;
        billManagementDTO.lastModificationTime = currentTime;

        billManagementDTO.officeLetterNumber = Jsoup.clean(
                request.getParameter("officeLetterNumber"), Whitelist.simpleText()
        );

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        billManagementDTO.letterDate = dateFormat.parse(request.getParameter("letterDate")).getTime();

        billManagementDTO.budgetSelectionInfoId = BudgetSelectionInfoRepository.getInstance().getId(
                BudgetUtils.getEconomicYear(billManagementDTO.letterDate)
        );
        billManagementDTO.budgetInstitutionalGroupId = Long.parseLong(Jsoup.clean(
                request.getParameter("budgetInstitutionalGroupId"), Whitelist.simpleText())
        );
        billManagementDTO.budgetOfficeId = Long.parseLong(Jsoup.clean(
                request.getParameter("budgetOfficeId"), Whitelist.simpleText())
        );
        billManagementDTO.budgetMappingId = Long.parseLong(Jsoup.clean(
                request.getParameter("budgetMappingId"), Whitelist.simpleText())
        );
        billManagementDTO.economicGroupId = Long.parseLong(Jsoup.clean(
                request.getParameter("economicGroupId"), Whitelist.simpleText())
        );
        billManagementDTO.economicCodeId = Long.parseLong(Jsoup.clean(
                request.getParameter("economicCodeId"), Whitelist.simpleText()
        ));
        billManagementDTO.economicSubCodeId = Long.parseLong(Jsoup.clean(
                request.getParameter("economicSubCodeId"), Whitelist.simpleText()
        ));
        billManagementDTO.billAmount = Long.parseLong(Jsoup.clean(
                request.getParameter("billAmount"), Whitelist.simpleText()
        ));
        billManagementDTO.supplierInstitutionId = Long.parseLong(request.getParameter("supplierInstitutionId"));
        billManagementDTO.suppliedItemDescription = Utils.cleanAndGetOptionalString(request.getParameter("suppliedItemDescription"));

        billManagementDTO.vatPercent = Double.parseDouble(request.getParameter("vatPercent"));
        billManagementDTO.taxPercent = Utils.parseOptionalDouble(
                request.getParameter("taxPercent"),
                -1.0,
                null
        );
        billManagementDTO.serviceChargePercent = Utils.parseOptionalDouble(
                request.getParameter("serviceChargePercent"),
                -1.0,
                null
        );

        billManagementDTO.dueBillAmount = Utils.parseOptionalLong(
                request.getParameter("dueBillAmount"),
                -1L,
                null
        );
        billManagementDTO.dueBillDescription = Utils.cleanAndGetOptionalString(request.getParameter("dueBillDescription"));
        billManagementDTO.dueVatAmount = Utils.parseOptionalLong(
                request.getParameter("dueVatAmount"),
                -1L,
                null
        );
        billManagementDTO.dueVatDescription = Utils.cleanAndGetOptionalString(request.getParameter("dueVatDescription"));
        billManagementDTO.dueTaxAmount = Utils.parseOptionalLong(
                request.getParameter("dueTaxAmount"),
                -1L,
                null
        );
        billManagementDTO.dueTaxDescription = Utils.cleanAndGetOptionalString(request.getParameter("dueTaxDescription"));
        billManagementDTO.dueServiceChargeAmount = Utils.parseOptionalLong(
                request.getParameter("dueServiceChargeAmount"),
                -1L,
                null
        );
        billManagementDTO.dueServiceChargeDescription = Utils.cleanAndGetOptionalString(request.getParameter("dueServiceChargeDescription"));

        String Value;
        Value = request.getParameter("fileDropzone");
        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {
            billManagementDTO.fileDropzone = Long.parseLong(Value);
            if (!addFlag) {
                String fileDropzoneFilesToDelete = request.getParameter("fileDropzoneFilesToDelete");
                String[] deleteArray = fileDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    logger.debug("going to delete " + deleteArray[i]);
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        }

        Budget_registerDTO budgetRegisterDTO = addBudgetRegisterDTO(billManagementDTO, addFlag);
        billManagementDTO.budgetRegisterId = budgetRegisterDTO.iD;
        Bill_registerServlet.addBillRegister(budgetRegisterDTO, CASH_TYPE, addFlag);

        billManagementDTO.uptoLastBillTotal = budgetRegisterDTO.uptoLastBillTotal;
        billManagementDTO.allocatedBudget = budgetRegisterDTO.allocatedBudget;
        billManagementDTO.supplierTotalBill = budgetRegisterDTO.calculateSupplierTotalBill();
        if (addFlag) {
            Bill_managementDAO.getInstance().add(billManagementDTO);
        } else {
            Bill_managementDAO.getInstance().update(billManagementDTO);
        }
        return billManagementDTO;
    }

    private Budget_registerDTO addBudgetRegisterDTO(Bill_managementDTO billManagementDTO, boolean addFlag) throws Exception {
        Budget_registerDTO budgetRegisterDTO;
        if (addFlag) {
            budgetRegisterDTO = new Budget_registerDTO();
            budgetRegisterDTO.insertedBy = billManagementDTO.insertedBy;
            budgetRegisterDTO.insertionTime = billManagementDTO.insertionTime;
        } else {
            budgetRegisterDTO = Budget_registerDAO.getInstance().getDTOFromID(billManagementDTO.budgetRegisterId);
        }
        budgetRegisterDTO.modifiedBy = billManagementDTO.modifiedBy;
        budgetRegisterDTO.lastModificationTime = billManagementDTO.lastModificationTime;

        budgetRegisterDTO.supplierInstitutionId = billManagementDTO.supplierInstitutionId;
        budgetRegisterDTO.recipientName =
                Supplier_institutionRepository.getInstance()
                                              .getIssuedToText(billManagementDTO.supplierInstitutionId, "Bangla");
        budgetRegisterDTO.issueNumber = billManagementDTO.officeLetterNumber;
        budgetRegisterDTO.issueDate = billManagementDTO.letterDate;
        budgetRegisterDTO.description = billManagementDTO.suppliedItemDescription;
        budgetRegisterDTO.billAmount = billManagementDTO.billAmount;
        budgetRegisterDTO.budgetSelectionInfoId = billManagementDTO.budgetSelectionInfoId;
        budgetRegisterDTO.budgetOfficeId = billManagementDTO.budgetOfficeId;
        budgetRegisterDTO.budgetMappingId = billManagementDTO.budgetMappingId;
        budgetRegisterDTO.economicSubCodeId = billManagementDTO.economicSubCodeId;

        if (addFlag) {
            Budget_registerDAO.getInstance().add(budgetRegisterDTO);
        } else {
            Budget_registerDAO.getInstance().update(budgetRegisterDTO);
        }
        return budgetRegisterDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.BILL_MANAGEMENT_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.BILL_MANAGEMENT_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.BILL_MANAGEMENT_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Bill_managementServlet.class;
    }
}