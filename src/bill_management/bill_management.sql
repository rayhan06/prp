CREATE TABLE bill_management
(
    ID                            BIGINT(20) PRIMARY KEY,
    budget_selection_info_id      BIGINT(20),
    budget_institutional_group_id BIGINT(20),
    budget_office_id              BIGINT(20),
    budget_mapping_id             BIGINT(20),
    economic_group_id             BIGINT(20),
    economic_code_id              BIGINT(20),
    economic_sub_code_id          BIGINT(20),
    office_letter_number          VARCHAR(50),
    letter_date                   BIGINT(20),
    bill_amount                   BIGINT(20),
    bill_amount_in_word           VARCHAR(255),
    vat_percent                   DECIMAL(5, 3),
    tax_percent                   DECIMAL(5, 3),
    service_charge_percent        DECIMAL(5, 3),
    allocated_budget              BIGINT(20),
    upto_last_bill_total          BIGINT(20),
    due_bill_amount               BIGINT(20),
    due_bill_description          VARCHAR(1024),
    issued_to_name                VARCHAR(255),
    supplied_item_description     VARCHAR(1024),
    file_dropzone                 BIGINT(20),
    modified_by                   BIGINT(20),
    lastModificationTime          BIGINT(20) DEFAULT -62135791200000,
    inserted_by                   BIGINT(20),
    insertion_time                BIGINT(20) DEFAULT -62135791200000,
    isDeleted                     INT(11) DEFAULT 0
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;