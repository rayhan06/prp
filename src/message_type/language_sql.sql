DELETE FROM language_text WHERE menuID = 169101;
DELETE FROM language_text WHERE menuID = 169102;
DELETE FROM language_text WHERE menuID = 169103;
DELETE FROM language_text WHERE languageConstantPrefix = 'MESSAGE_TYPE_ADD';
DELETE FROM language_text WHERE languageConstantPrefix = 'MESSAGE_TYPE_EDIT';
DELETE FROM language_text WHERE languageConstantPrefix = 'MESSAGE_TYPE_SEARCH';
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72400','169101','ID','????','MESSAGE_TYPE_ADD','ID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72401','169101','Description','?????','MESSAGE_TYPE_ADD','DESCRIPTION');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72402','169101','IsDeleted','????? ??? ?????','MESSAGE_TYPE_ADD','ISDELETED');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72403','169101','LastModificationTime','?????','MESSAGE_TYPE_ADD','LASTMODIFICATIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72404','169102','ID','????','MESSAGE_TYPE_EDIT','ID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72405','169102','Description','?????','MESSAGE_TYPE_EDIT','DESCRIPTION');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72406','169102','IsDeleted','????? ??? ?????','MESSAGE_TYPE_EDIT','ISDELETED');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72407','169102','LastModificationTime','?????','MESSAGE_TYPE_EDIT','LASTMODIFICATIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72408','169103','ID','????','MESSAGE_TYPE_SEARCH','ID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72409','169103','Description','?????','MESSAGE_TYPE_SEARCH','DESCRIPTION');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72410','169103','IsDeleted','????? ??? ?????','MESSAGE_TYPE_SEARCH','ISDELETED');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72411','169103','LastModificationTime','?????','MESSAGE_TYPE_SEARCH','LASTMODIFICATIONTIME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72412','169101','MESSAGE TYPE ADD','?????? ???? ??? ??? ????','MESSAGE_TYPE_ADD','MESSAGE_TYPE_ADD_FORMNAME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72413','169101','ADD','??? ????','MESSAGE_TYPE_ADD','MESSAGE_TYPE_ADD_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72414','169101','SUBMIT','?????? ????','MESSAGE_TYPE_ADD','MESSAGE_TYPE_SUBMIT_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72415','169101','CANCEL','????? ????','MESSAGE_TYPE_ADD','MESSAGE_TYPE_CANCEL_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72416','169102','MESSAGE TYPE EDIT','?????? ???? ??? ???????? ????','MESSAGE_TYPE_EDIT','MESSAGE_TYPE_EDIT_FORMNAME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72417','169102','SUBMIT','?????? ????','MESSAGE_TYPE_EDIT','MESSAGE_TYPE_SUBMIT_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72418','169102','CANCEL','????? ????','MESSAGE_TYPE_EDIT','MESSAGE_TYPE_CANCEL_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72419','169102','English','Bangla','MESSAGE_TYPE_EDIT','LANGUAGE');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72420','169103','MESSAGE TYPE SEARCH','?????? ???? ??? ??????','MESSAGE_TYPE_SEARCH','MESSAGE_TYPE_SEARCH_FORMNAME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72421','169103','SEARCH','??????','MESSAGE_TYPE_SEARCH','MESSAGE_TYPE_SEARCH_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72422','169103','DELETE','????? ????','MESSAGE_TYPE_SEARCH','MESSAGE_TYPE_DELETE_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72423','169103','EDIT','???????? ????','MESSAGE_TYPE_SEARCH','MESSAGE_TYPE_EDIT_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72424','169103','CANCEL','????? ????','MESSAGE_TYPE_SEARCH','MESSAGE_TYPE_CANCEL_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('72425','169103','MESSAGE TYPE','?????? ???? ???','MESSAGE_TYPE_SEARCH','ANYFIELD');
