package message_type;
import java.util.*; 
import util.*; 


public class Message_typeDTO extends CommonDTO
{

    public String description = "";
	
	
    @Override
	public String toString() {
            return "$Message_typeDTO[" +
            " iD = " + iD +
            " description = " + description +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}