package message_type;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Message_typeRepository implements Repository {
	Message_typeDAO message_typeDAO = null;
	
	public void setDAO(Message_typeDAO message_typeDAO)
	{
		this.message_typeDAO = message_typeDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Message_typeRepository.class);
	Map<Integer, Message_typeDTO>mapOfMessage_typeDTOToiD;
	Map<String, Set<Message_typeDTO> >mapOfMessage_typeDTOTodescription;
	Map<Long, Set<Message_typeDTO> >mapOfMessage_typeDTOTolastModificationTime;


	static Message_typeRepository instance = null;  
	private Message_typeRepository(){
		mapOfMessage_typeDTOToiD = new ConcurrentHashMap<>();
		mapOfMessage_typeDTOTodescription = new ConcurrentHashMap<>();
		mapOfMessage_typeDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Message_typeRepository getInstance(){
		if (instance == null){
			instance = new Message_typeRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(message_typeDAO == null)
		{
			return;
		}
		try {
			List<Message_typeDTO> message_typeDTOs = message_typeDAO.getAllMessage_type(reloadAll);
			for(Message_typeDTO message_typeDTO : message_typeDTOs) {
				Message_typeDTO oldMessage_typeDTO = getMessage_typeDTOByID(message_typeDTO.iD);
				if( oldMessage_typeDTO != null ) {
					mapOfMessage_typeDTOToiD.remove(oldMessage_typeDTO.iD);
				
					if(mapOfMessage_typeDTOTodescription.containsKey(oldMessage_typeDTO.description)) {
						mapOfMessage_typeDTOTodescription.get(oldMessage_typeDTO.description).remove(oldMessage_typeDTO);
					}
					if(mapOfMessage_typeDTOTodescription.get(oldMessage_typeDTO.description).isEmpty()) {
						mapOfMessage_typeDTOTodescription.remove(oldMessage_typeDTO.description);
					}
					
					if(mapOfMessage_typeDTOTolastModificationTime.containsKey(oldMessage_typeDTO.lastModificationTime)) {
						mapOfMessage_typeDTOTolastModificationTime.get(oldMessage_typeDTO.lastModificationTime).remove(oldMessage_typeDTO);
					}
					if(mapOfMessage_typeDTOTolastModificationTime.get(oldMessage_typeDTO.lastModificationTime).isEmpty()) {
						mapOfMessage_typeDTOTolastModificationTime.remove(oldMessage_typeDTO.lastModificationTime);
					}
					
					
				}
				if(message_typeDTO.isDeleted == 0) 
				{
					
					mapOfMessage_typeDTOToiD.put((int)message_typeDTO.iD, message_typeDTO);
				
					if( ! mapOfMessage_typeDTOTodescription.containsKey(message_typeDTO.description)) {
						mapOfMessage_typeDTOTodescription.put(message_typeDTO.description, new HashSet<>());
					}
					mapOfMessage_typeDTOTodescription.get(message_typeDTO.description).add(message_typeDTO);
					
					if( ! mapOfMessage_typeDTOTolastModificationTime.containsKey(message_typeDTO.lastModificationTime)) {
						mapOfMessage_typeDTOTolastModificationTime.put(message_typeDTO.lastModificationTime, new HashSet<>());
					}
					mapOfMessage_typeDTOTolastModificationTime.get(message_typeDTO.lastModificationTime).add(message_typeDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Message_typeDTO> getMessage_typeList() {
		List <Message_typeDTO> message_types = new ArrayList<Message_typeDTO>(this.mapOfMessage_typeDTOToiD.values());
		return message_types;
	}
	
	
	public Message_typeDTO getMessage_typeDTOByID( long ID){
		return mapOfMessage_typeDTOToiD.get(ID);
	}
	
	
	public List<Message_typeDTO> getMessage_typeDTOBydescription(String description) {
		return new ArrayList<>( mapOfMessage_typeDTOTodescription.getOrDefault(description,new HashSet<>()));
	}
	
	
	public List<Message_typeDTO> getMessage_typeDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfMessage_typeDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "message_type";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


