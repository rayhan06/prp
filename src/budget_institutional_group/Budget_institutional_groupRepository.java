package budget_institutional_group;

import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import util.LockManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static util.StringUtils.convertBanglaIfLanguageIsBangla;
import static util.UtilCharacter.getDataByLanguage;

public class Budget_institutional_groupRepository {
    private final Logger logger = Logger.getLogger(Budget_institutional_groupRepository.class);
    private final Budget_institutional_groupDAO budget_institutional_groupDAO = Budget_institutional_groupDAO.getInstance();
    private Map<Long, Budget_institutional_groupDTO> mapById;
    public List<Budget_institutional_groupDTO> allBudgetInstitutionalGroupDTO;

    private Budget_institutional_groupRepository() {
        reload();
    }

    private static class Budget_institutional_groupRepositoryLazyLoader {
        static final Budget_institutional_groupRepository INSTANCE = new Budget_institutional_groupRepository();
    }

    public static Budget_institutional_groupRepository getInstance() {
        return Budget_institutional_groupRepository.Budget_institutional_groupRepositoryLazyLoader.INSTANCE;
    }

    private void reload() {
        logger.debug("Budget_institutional_groupRepository reload start");
        List<Budget_institutional_groupDTO> list = budget_institutional_groupDAO.getAllDTOs(true);
        if (list != null && list.size() > 0) {
            allBudgetInstitutionalGroupDTO = list.stream()
                    .filter(e -> e.isDeleted == 0)
                    .collect(Collectors.toList());
            mapById = allBudgetInstitutionalGroupDTO.stream()
                    .collect(Collectors.toMap(e -> e.iD, e -> e, (e1, e2) -> e1));
        } else {
            allBudgetInstitutionalGroupDTO = new ArrayList<>();
            mapById = new HashMap<>();
        }
        logger.debug("Budget_institutional_groupRepository reload end");
    }

    public Budget_institutional_groupDTO getById(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "BIGR")) {

                Budget_institutional_groupDTO dto = budget_institutional_groupDAO.getDTOFromIdDeletedOrNot(id);
                if (dto != null) {
                    mapById.put(dto.iD, dto);
                    if (dto.isDeleted == 0) {
                        allBudgetInstitutionalGroupDTO.add(dto);
                    }
                }
            }

        }
        return mapById.get(id);
    }

    public String getCode(long id, String language) {
        Budget_institutional_groupDTO dto = getById(id);
        if (dto == null) return "";
        return convertBanglaIfLanguageIsBangla(language, dto.code);
    }

    public String getText(long id, boolean withCode, String language) {
        Budget_institutional_groupDTO dto = getById(id);
        if (dto == null) return "";

        String text = withCode ? convertBanglaIfLanguageIsBangla(language, dto.code) + " - " : "";
        return text.concat(getDataByLanguage(language, dto.nameBn, dto.nameEn));
    }

    public String buildOptions(String language, Long selectedId, boolean withCode) {
        List<OptionDTO> optionDTOList = null;
        if (allBudgetInstitutionalGroupDTO != null && allBudgetInstitutionalGroupDTO.size() > 0) {
            optionDTOList = allBudgetInstitutionalGroupDTO
                    .stream()
                    .map(dto -> dto.getOptionDTO(withCode))
                    .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }
}