package budget_institutional_group;

import pb.OptionDTO;
import util.CommonDTO;
import util.StringUtils;

public class Budget_institutional_groupDTO extends CommonDTO {
    public String code = "";
    public String nameEn = "";
    public String nameBn = "";


    public long insertedBy = 0;
    public long insertionTime = 0;
    public long modifiedBy = 0;

    public OptionDTO getOptionDTO(boolean withCode){
        return new OptionDTO(
                (withCode ? code.concat(" - ") : "").concat(nameEn),
                (withCode ? StringUtils.convertToBanNumber(code).concat(" - ") : "").concat(nameBn),
                String.valueOf(iD)
        );
    }

    @Override
    public String toString() {
        return "Budget_institutional_groupDTO{" +
                "code='" + code + '\'' +
                ", nameEn='" + nameEn + '\'' +
                ", nameBn='" + nameBn + '\'' +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                '}';
    }
}
