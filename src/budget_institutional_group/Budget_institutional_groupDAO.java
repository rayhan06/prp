package budget_institutional_group;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

@SuppressWarnings("Duplicates")
public class Budget_institutional_groupDAO implements CommonDAOService<Budget_institutional_groupDTO> {
    private static final Logger logger = Logger.getLogger(Budget_institutional_groupDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (code, name_eng, name_bng, "
            .concat("modified_by, lastModificationTime, inserted_by, insertion_time, isDeleted, ID) ")
            .concat("VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?)");

    private static final String updateQuery = "UPDATE {tableName} SET code = ?, name_eng = ?, name_bng = ?, "
            .concat(" modified_by = ?, lastModificationTime = ? WHERE ID = ?");

    private static Budget_institutional_groupDAO INSTANCE = null;

    public static Budget_institutional_groupDAO getInstance() {
        if (INSTANCE == null) {
            synchronized (Budget_institutional_groupDAO.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Budget_institutional_groupDAO();
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Budget_institutional_groupDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setString(++index, dto.code);
        ps.setString(++index, dto.nameEn);
        ps.setString(++index, dto.nameBn);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);

        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* dto.isDeleted */);
        }

        ps.setLong(++index, dto.iD);
    }

    @Override
    public Budget_institutional_groupDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Budget_institutional_groupDTO dto = new Budget_institutional_groupDTO();
            dto.iD = rs.getLong("ID");
            dto.code = rs.getString("code");
            dto.nameEn = rs.getString("name_eng");
            dto.nameBn = rs.getString("name_bng");

            dto.isDeleted = rs.getInt("isDeleted");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");

            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "budget_institutional_group";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return null;
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Budget_institutional_groupDTO) commonDTO, updateQuery, false);
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Budget_institutional_groupDTO) commonDTO, addQuery, true);
    }
}