package drug_delivery_report;


import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;

import login.LoginDTO;
import medical_inventory_lot.MedicalInventoryInDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Drug_delivery_report_Servlet")
public class Drug_delivery_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","medical_item_cat","=","","int","","","0","none",  ""},		
		{"criteria","","transaction_type","in","AND","String","","", MedicalInventoryInDTO.TR_RECEIVE_MEDICINE + ", " +  MedicalInventoryInDTO.TR_RETURN ,"none", ""},		
		{"criteria","","transaction_date",">=","AND","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","transaction_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},		
		{"criteria","","drug_information_id","=","AND","any","","","any","drugInformationId", LC.HM_DRUGS + ""}		
	};
	
	String[][] Display =
	{
		{"display","","CONCAT(drug_information.name_en, ', ', strength)","basic",""},		
		{"display","","SUM(quantity)","int",""},		
		{"display","","sum(medical_transaction.quantity * medical_transaction.unit_price)","double",""},
		{"display","","COUNT(distinct user_name)","int",""},
		{"display","","drug_information.current_stock","int",""}	
	};
	
	String GroupBy = "drug_information_id";
	String OrderBY = "drug_information.name_en asc";
	
	ReportRequestHandler reportRequestHandler;
	
	public Drug_delivery_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
		boolean isLangEng = language.equalsIgnoreCase("english");		
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "medical_transaction join drug_information on drug_information.id = drug_information_id";

		Display[0][4] = LM.getText(LC.HM_DRUGS, loginDTO);
		Display[1][4] = isLangEng?"Medicine Count":"ঔষধের পরিমাণ";
		Display[2][4] = isLangEng?"Medicine Cost":"ঔষধের মূল্য";
		Display[3][4] = isLangEng?"Patient Count":"রোগীর সংখ্যা";
		Display[4][4] = isLangEng?"Current Stock":"বর্তমান স্টক";

		
		String reportName = LM.getText(LC.DRUG_DELIVERY_REPORT_OTHER_DRUG_DELIVERY_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(1, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(2, ReportRequestHandler.RIGHT_ALIGN_FLOAT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(3, ReportRequestHandler.RIGHT_ALIGN_INT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "drug_delivery_report",
				MenuConstants.DRUG_DELIVERY_REPORT_DETAILS, language, reportName, "drug_delivery_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
