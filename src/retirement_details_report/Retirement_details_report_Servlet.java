package retirement_details_report;


import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pbReport.PBReportUtils;
import pbReport.ReportCommonService;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"unused"})
@WebServlet("/Retirement_details_report_Servlet")
public class Retirement_details_report_Servlet extends HttpServlet implements ReportCommonService {
    private static final Logger logger = Logger.getLogger(Retirement_details_report_Servlet.class);

    private static final long serialVersionUID = 1L;

    private static final Set<String> searchParam = new HashSet<>(Arrays.asList("age_end", "age_start", "officeUnitIds", "officeUnitOrganogramId", "employmentCat", "startDate", "endDate",
            "lprDateStart", "lprDateEnd", "home_district", "jobGradeTypeCat", "nameEng", "nameBng"));

    private final Map<String, String[]> stringMap = new HashMap<>();

    private static final String sql = "employee_offices eo INNER JOIN employee_records er ON eo.employee_record_id = er.id";

    private String[][] Criteria;

    private final String[][] Display = {
            {"display", "eo", "employee_record_id", "employee_records_id", ""},
            {"display", "eo", "employee_record_id", "user_name", ""},
            {"display", "er", "personal_mobile", "number", ""},
            {"display", "er", "personal_email", "plain", ""},
            {"display", "er", "home_district", "location", ""},
            {"display", "eo", "office_unit_id", "office_unit", ""},
            {"display", "eo", "id", "designation", ""},
            {"display", "er", "employment_cat", "category", ""},
            {"display", "er", "lpr_date", "date", ""},
            {"display", "er", "retirement_date", "date", ""},
            {"display", "er", "date_of_birth", "complete_age", ""},
            {"display", "eo", "employee_record_id", "id_to_present_address", ""},
            {"display", "eo", "employee_record_id", "id_to_permanent_address", ""},
            {"display", "eo", "job_grade_type_cat", "category", ""}
    };

    public Retirement_details_report_Servlet() {
        stringMap.put("officeUnitIds", new String[]{"criteria", "eo", "office_unit_id", "IN", "AND", "String", "", "", ""
                , "officeUnitIdList", "officeUnitIds", String.valueOf(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID), "office_units", null, "true", "1"});
        stringMap.put("officeUnitOrganogramId", new String[]{"criteria", "eo", "office_unit_organogram_id", "=", "AND", "long", "", "", "", "officeUnitOrganogramId",
                "officeUnitOrganogramId", String.valueOf(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITORGANOGRAMID), "office_unit_organogram", null, "true", "2"});
        stringMap.put("employmentCat", new String[]{"criteria", "er", "employment_cat", "=", "AND", "int", "", "", "any", "employmentCat"
                , "employmentCat", String.valueOf(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY), "category", "employment", "true", "3"});
        stringMap.put("age_start", new String[]{"criteria", "er", "date_of_birth", "<=", "AND", "long", "", "", "", "ageStart"
                , "age_start", String.valueOf(LC.HR_REPORT_AGE_RANGE_START), "number", null, "true", "4"});
        stringMap.put("age_end", new String[]{"criteria", "er", "date_of_birth", ">=", "AND", "long", "", "", "", "ageEnd"
                , "age_end", String.valueOf(LC.HR_REPORT_AGE_RANGE_TO), "number", null, "true", "5"});
        stringMap.put("startDate", new String[]{"criteria", "er", "retirement_date", ">=", "AND", "long", "", "", ""
                , "startDate", "startDate", String.valueOf(LC.RETIREMENT_DETAILS_REPORT_WHERE_RETIREMENTDATE), "date", null, "true", "6"});
        stringMap.put("endDate", new String[]{"criteria", "er", "retirement_date", "<=", "AND", "long", "", "", "",
                "endDate", "endDate", String.valueOf(LC.RETIREMENT_DETAILS_REPORT_WHERE_RETIREMENTDATE_4), "date", null, "true", "7"});
        stringMap.put("lprDateStart", new String[]{"criteria", "er", "lpr_date", ">=", "AND", "long", "", "", Long.MIN_VALUE + ""
                , "lprDateStart", "lprDateStart", String.valueOf(LC.EMPLOYEE_INFO_REPORT_WHERE_LPRDATE), "date", null, "true", "8"});
        stringMap.put("lprDateEnd", new String[]{"criteria", "er", "lpr_date", "<=", "AND", "long", "", "", Long.MAX_VALUE + ""
                , "lprDateEnd", "lprDateEnd", String.valueOf(LC.EMPLOYEE_INFO_REPORT_WHERE_LPRDATE_12), "date", null, "true", "9"});
        stringMap.put("home_district", new String[]{"criteria", "er", "home_district", "=", "AND", "long", "", "", "", "home_district"
                , "home_district", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT), "location", null, "true", "10"});
        stringMap.put("jobGradeTypeCat", new String[]{"criteria", "eo", "job_grade_type_cat", "=", "AND", "int", "", "", "any", "jobGradeTypeCat",
                "jobGradeTypeCat", String.valueOf(LC.DESIGNATION_STATUS_REPORT_WHERE_JOBGRADETYPECAT), "category", "job_grade_type", "true", "11"});
        stringMap.put("nameEng", new String[]{"criteria", "er", "name_eng", "Like", "AND", "String", "", "", "any", "nameEng",
                "nameEng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEENG), "text", null, "true", "12"});
        stringMap.put("nameBng", new String[]{"criteria", "er", "name_bng", "Like", "AND", "String", "", "", "any", "nameBng",
                "nameBng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG), "text", null, "true", "13"});
        stringMap.put("office_id", new String[]{"criteria", "eo", "office_id", "=", "AND", "long", "", "", "2294", "office_id", null, null, null, null, null, null});
        stringMap.put("status", new String[]{"criteria", "eo", "status", "=", "AND", "long", "", "", "1", "status", null, null, null, null, null, null});
        stringMap.put("isDeleted_1", new String[]{"criteria", "er", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted_1", null, null, null, null, null, null});
        stringMap.put("isDeleted", new String[]{"criteria", "eo", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted", null, null, null, null, null, null});
        stringMap.put("endDateDefault", new String[]{"criteria", "er", "retirement_date", ">", "AND", "long", "", "", String.valueOf(SessionConstants.MIN_DATE), "endDateDefault", null, null, null, null, null, null});
        stringMap.put("lprDateEndDefault", new String[]{"criteria", "er", "lpr_date", ">", "AND", "long", "", "", String.valueOf(SessionConstants.MIN_DATE), "lprDateEndDefault", null, null, null, null, null, null});
        stringMap.put("isDeleted", new String[]{"criteria", "eo", "is_default_role", "=", "AND", "long", "", "", "1", "is_default_role", null, null, null, null, null, null});
        
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        Display[0][4] = LM.getText(LC.RETIREMENT_DETAILS_REPORT_SELECT_EMPLOYEERECORDID, loginDTO);
        Display[1][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_USER_ID, loginDTO);
        Display[2][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_MOBILE_NUMBER, loginDTO);
        Display[3][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_EMAIL, loginDTO);
        Display[4][4] = LM.getText(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT, loginDTO);
        Display[5][4] = LM.getText(LC.RETIREMENT_DETAILS_REPORT_SELECT_OFFICEUNITID, loginDTO);
        Display[6][4] = LM.getText(LC.RETIREMENT_DETAILS_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO);
        Display[7][4] = LM.getText(LC.RETIREMENT_DETAILS_REPORT_SELECT_EMPLOYMENTCAT, loginDTO);
        Display[8][4] = LM.getText(LC.RETIREMENT_DETAILS_REPORT_SELECT_LPRDATEASLPRDATE, loginDTO);
        Display[9][4] = LM.getText(LC.RETIREMENT_DETAILS_REPORT_SELECT_RETIREMENTDATEASRETIREMENTDATE, loginDTO);
        Display[10][4] = LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_AGE, loginDTO);
        Display[11][4] = LM.getText(LC.EMPLOYEE_RECORDS_ADD_PRESENTADDRESS, loginDTO);
        Display[12][4] = LM.getText(LC.EMPLOYEE_RECORDS_ADD_PERMANENTADDRESS, loginDTO);
        Display[13][4] = LM.getText(LC.DESIGNATION_STATUS_REPORT_SELECT_JOBGRADETYPECAT, loginDTO);


        Set<String> inputs = PBReportUtils.prepareInputSets(searchParam, request);
        if (inputs.contains("endDate")) {
            inputs.add("endDateDefault");
        }
        if (inputs.contains("lprDateEnd")) {
            inputs.add("lprDateEndDefault");
        }
        inputs.add("office_id");
        inputs.add("isDeleted");
        inputs.add("status");
        inputs.add("isDeleted_1");
        Criteria = PBReportUtils.prepareCriteria(inputs, stringMap);
        if (inputs.contains("officeUnitIds") || inputs.contains("age_start") || inputs.contains("age_end") || inputs.contains("jobGradeTypeCat")) {
            for (String[] arr : Criteria) {
                switch (arr[9]) {
                    case "officeUnitIdList":
                        arr[8] = getOfficeIdsFromOfficeUnitIds(request).stream()
                                .map(String::valueOf)
                                .collect(Collectors.joining(","));
                        break;
                    case "ageStart":
                        long ageStart = Long.parseLong(request.getParameter("age_start"));
                        arr[8] = String.valueOf(calculateLongDateValue(ageStart));
                        break;
                    case "ageEnd":
                        long ageEnd = Long.parseLong(request.getParameter("age_end"));
                        arr[8] = String.valueOf(calculateLongDateValue(ageEnd));
                        break;
                    case "jobGradeTypeCat":
                        int jobGradeCat = Integer.parseInt(request.getParameter("jobGradeTypeCat"));
                        String sign = "=";
                        int jobGradeEqual = Integer.parseInt(request.getParameter("jobGradeEqual"));
                        int jobGradeLess = Integer.parseInt(request.getParameter("jobGradeLess"));
                        int jobGradeMore = Integer.parseInt(request.getParameter("jobGradeMore"));
                        if (jobGradeEqual == 0 && jobGradeLess == 0 && jobGradeMore == 0)
                            break;
                        if (jobGradeEqual == 0)
                            sign = "";
                        if (jobGradeLess == 1) {
                            if (jobGradeCat <= 20) {
                                sign = ">" + sign;
                            }
                        }
                        if (jobGradeMore == 1) {
                            if (jobGradeCat <= 20) {
                                sign = "<" + sign;
                            }
                        }
                        arr[3] = sign;
                        break;
                }
            }
        }

        request.setAttribute("dontUseOutDatedModal", true);
        if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RETIREMENT_DETAILS_REPORT_DETAILS)) {
            reportGenerate(request, response);
        }
    }

    private long calculateLongDateValue(long ageYears) {
        LocalDate ld = LocalDate.now();
        ld = ld.minusYears(ageYears);
        return Date.from(ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()).getTime();
    }

    @Override
    public String[][] getCriteria() {
        return Criteria;
    }

    @Override
    public String[][] getDisplay() {
        return Display;
    }

    @Override
    public String getSQL() {
        return sql;
    }

    @Override
    public int getLCForReportName() {
        return LC.RETIREMENT_DETAILS_REPORT_OTHER_RETIREMENT_DETAILS_REPORT;
    }

    @Override
    public String getFileName() {
        return "retirement_details_report";
    }

    @Override
    public String getTableName() {
        return "retirement_details_report";
    }
}
