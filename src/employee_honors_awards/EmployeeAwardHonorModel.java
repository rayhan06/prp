package employee_honors_awards;

public class EmployeeAwardHonorModel {
    public Employee_honors_awardsDTO dto;
    public String awardEng;
    public String awardBan;
    public String titlesOfAward = "";
    public String awardingInstitution = "";
    public String ground = "";
    public String awardDateEng = "";
    public String awardDateBan = "";
    public String briefDescription = "";
}
