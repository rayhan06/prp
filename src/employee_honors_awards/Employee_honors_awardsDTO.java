package employee_honors_awards;

import sessionmanager.SessionConstants;
import util.CommonDTO;
import util.CommonEmployeeDTO;


public class Employee_honors_awardsDTO extends CommonEmployeeDTO {
    public int awardTypeCat = 0;
    public String titlesOfAward = "";
    public String awardingInstitution = "";
    public String ground = "";
    public long awardDate = SessionConstants.MIN_DATE;
    public String briefDescription = "";
    public long filesDropzone = 0;
    public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";


    @Override
    public String toString() {
        return "$Employee_honors_awardsDTO[" +
                " iD = " + iD +
                " employeeRecordsId = " + employeeRecordsId +
                " awardTypeCat = " + awardTypeCat +
                " titlesOfAward = " + titlesOfAward +
                " awardingInstitution = " + awardingInstitution +
                " ground = " + ground +
                " awardDate = " + awardDate +
                " briefDescription = " + briefDescription +
                " filesDropzone = " + filesDropzone +
                " insertionDate = " + insertionDate +
                " insertedBy = " + insertedBy +
                " modifiedBy = " + modifiedBy +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}