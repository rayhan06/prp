package employee_honors_awards;

import common.BaseServlet;
import common.EmployeeServletService;
import employee_records.Employee_recordsRepository;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.CatRepository;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


@SuppressWarnings({"Duplicates"})
@WebServlet("/Employee_honors_awardsServlet")
@MultipartConfig
public class Employee_honors_awardsServlet extends BaseServlet implements EmployeeServletService {
    private static final long serialVersionUID = 1L;

    private final Employee_honors_awardsDAO employee_honors_awardsDAO = Employee_honors_awardsDAO.getInstance();

    @Override
    public String getTableName() {
        return employee_honors_awardsDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Employee_honors_awardsServlet";
    }

    @Override
    public Employee_honors_awardsDAO getCommonDAOService() {
        return employee_honors_awardsDAO;
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request, 5, "award");
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request, 5, "award");
    }

    @Override
    public String getDeleteRedirectURL(HttpServletRequest request) {
        return getAddOrEditOrDeleteRedirectURL(request, 5, "award");
    }

    @Override
    public boolean getEditPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getViewPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getEditPermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getAddPermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    @Override
    public boolean getAddPagePermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Employee_honors_awardsDTO employee_honors_awardsDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        if (addFlag) {
            employee_honors_awardsDTO = new Employee_honors_awardsDTO();
            long empId;
            try {
                empId = Long.parseLong(request.getParameter("empId"));
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new Exception(isLangEng ? "empId is not found" : "empId পাওয়া যায়নি");
            }
            if (Employee_recordsRepository.getInstance().getById(empId) == null) {
                throw new Exception(isLangEng ? "Employee information is not found" : "কর্মকর্তার তথ্য পাওয়া যায়নি");
            }
            employee_honors_awardsDTO.employeeRecordsId = empId;
            employee_honors_awardsDTO.insertedBy = String.valueOf(userDTO.ID);
            employee_honors_awardsDTO.insertionDate = new Date().getTime();
        } else {
            employee_honors_awardsDTO = employee_honors_awardsDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }

        employee_honors_awardsDTO.modifiedBy = String.valueOf(userDTO.ID);
        employee_honors_awardsDTO.lastModificationTime = new Date().getTime();
        employee_honors_awardsDTO.awardTypeCat = Integer.parseInt(Jsoup.clean(request.getParameter("awardTypeCat"), Whitelist.simpleText()));
        if (CatRepository.getInstance().getCategoryLanguageModel("award_type", employee_honors_awardsDTO.awardTypeCat) == null) {
            throw new Exception("Award Type not valid");
        }
        employee_honors_awardsDTO.titlesOfAward = Jsoup.clean(request.getParameter("titlesOfAward"), Whitelist.simpleText()).trim();
        employee_honors_awardsDTO.awardingInstitution = Jsoup.clean(request.getParameter("awardingInstitution"), Whitelist.simpleText()).trim();
        employee_honors_awardsDTO.ground = Jsoup.clean(request.getParameter("ground"), Whitelist.simpleText()).trim();
        try {
            Date d = f.parse(Jsoup.clean(request.getParameter("awardDate"), Whitelist.simpleText()));
            employee_honors_awardsDTO.awardDate = d.getTime();
        } catch (Exception e) {
            logger.error(e);
            employee_honors_awardsDTO.awardDate = SessionConstants.MIN_DATE;
        }
        employee_honors_awardsDTO.briefDescription = Jsoup.clean(request.getParameter("briefDescription"), Whitelist.simpleText()).trim();
        employee_honors_awardsDTO.filesDropzone = Long.parseLong(request.getParameter("filesDropzone"));

        if (!addFlag) {
            String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
            String[] deleteArray = filesDropzoneFilesToDelete.split(",");
            for (int i = 0; i < deleteArray.length; i++) {
                if (i > 0) {
                    filesDAO.delete(Long.parseLong(deleteArray[i]));
                }
            }
        }

        if (addFlag) {
            employee_honors_awardsDAO.add(employee_honors_awardsDTO);
        } else {
            employee_honors_awardsDAO.update(employee_honors_awardsDTO);
        }
        return employee_honors_awardsDTO;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        search(request);
        super.doGet(request, response);
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Employee_honors_awardsServlet.class;
    }
}