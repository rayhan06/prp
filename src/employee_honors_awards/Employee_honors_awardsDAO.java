package employee_honors_awards;

import common.ConnectionAndStatementUtil;
import common.EmployeeCommonDAOService;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import sessionmanager.SessionConstants;
import util.CommonDTO;
import util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"unused", "Duplicates"})
public class Employee_honors_awardsDAO implements EmployeeCommonDAOService<Employee_honors_awardsDTO> {

    private static final Logger logger = Logger.getLogger(Employee_honors_awardsDAO.class);
    private static final String addSqlQuery = "INSERT INTO {tableName} (employee_records_id,award_type_cat,titles_of_award,awarding_institution,ground,award_date,"
            .concat(" brief_description,files_dropzone,insertion_date,inserted_by,modified_by,search_column,lastModificationTime,isDeleted,ID)")
            .concat(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    private static final String updateSqlQuery = "UPDATE {tableName} SET employee_records_id=?,award_type_cat=?,titles_of_award=?,"
            .concat("awarding_institution=?,ground=?,award_date=?,brief_description=?,files_dropzone=?,")
            .concat("insertion_date=?,inserted_by=?,modified_by=?,search_column=?,lastModificationTime = ? WHERE ID =  ?");
    String getGetByEmployeeIdAndId = "SELECT * FROM employee_honors_awards WHERE employee_records_id = %d AND ID = %d  AND isDeleted = 0";

    private static final String sqlQueryByEpmloyeeId = "SELECT * FROM {tableName} where employee_records_id = ? order by award_date desc";
    private static final Map<String, String> searchMap = new HashMap<>();

    private Employee_honors_awardsDAO() {
        searchMap.put("award_type_cat", " and (award_type_cat = ?)");
        searchMap.put("titles_of_award", " and (titles_of_award = ?)");
        searchMap.put("awarding_institution", " and (awarding_institution = ?)");
        searchMap.put("award_date", " and (award_date = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
        searchMap.put("employee_records_id_internal"," and (employee_records_id = ?)");
    }

    private static class LazyLoader {
        static final Employee_honors_awardsDAO INSTANCE = new Employee_honors_awardsDAO();
    }

    public static Employee_honors_awardsDAO getInstance() {
        return Employee_honors_awardsDAO.LazyLoader.INSTANCE;
    }

    public void set(PreparedStatement ps, Employee_honors_awardsDTO employee_honors_awardsDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(employee_honors_awardsDTO);
        ps.setObject(++index, employee_honors_awardsDTO.employeeRecordsId);
        ps.setObject(++index, employee_honors_awardsDTO.awardTypeCat);
        ps.setObject(++index, employee_honors_awardsDTO.titlesOfAward);
        ps.setObject(++index, employee_honors_awardsDTO.awardingInstitution);
        ps.setObject(++index, employee_honors_awardsDTO.ground);
        ps.setObject(++index, employee_honors_awardsDTO.awardDate);
        ps.setObject(++index, employee_honors_awardsDTO.briefDescription);
        ps.setObject(++index, employee_honors_awardsDTO.filesDropzone);
        ps.setObject(++index, employee_honors_awardsDTO.insertionDate);
        ps.setObject(++index, employee_honors_awardsDTO.insertedBy);
        ps.setObject(++index, employee_honors_awardsDTO.modifiedBy);
        ps.setObject(++index, employee_honors_awardsDTO.searchColumn);
        ps.setObject(++index, System.currentTimeMillis());
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, employee_honors_awardsDTO.iD);
    }

    public void setSearchColumn(Employee_honors_awardsDTO employee_honors_awardsDTO) {
        List<String> list = new ArrayList<>();
        try {
            Employee_recordsDTO employeeRecordsDTO = new Employee_recordsDAO().getEmployee_recordsDTOByID(employee_honors_awardsDTO.employeeRecordsId);
            if (employeeRecordsDTO != null) {
                if (employeeRecordsDTO.nameEng != null && employeeRecordsDTO.nameEng.trim().length() > 0) {
                    list.add(employeeRecordsDTO.nameEng.trim());
                }
                if (employeeRecordsDTO.nameBng != null && employeeRecordsDTO.nameBng.trim().length() > 0) {
                    list.add(employeeRecordsDTO.nameBng.trim());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        CategoryLanguageModel languageModel = CatRepository.getInstance().getCategoryLanguageModel("award_type", employee_honors_awardsDTO.awardTypeCat);
        if (languageModel != null) {
            list.add(languageModel.banglaText);
            list.add(languageModel.englishText);
        }


        list.add(employee_honors_awardsDTO.awardingInstitution);
        list.add(employee_honors_awardsDTO.titlesOfAward);
        list.add(employee_honors_awardsDTO.briefDescription);
        list.add(employee_honors_awardsDTO.ground);

        employee_honors_awardsDTO.searchColumn = String.join(" ", list);
    }

    @Override
    public Employee_honors_awardsDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Employee_honors_awardsDTO employee_honors_awardsDTO = new Employee_honors_awardsDTO();
            employee_honors_awardsDTO.iD = rs.getLong("ID");
            employee_honors_awardsDTO.employeeRecordsId = rs.getLong("employee_records_id");
            employee_honors_awardsDTO.awardTypeCat = rs.getInt("award_type_cat");
            employee_honors_awardsDTO.titlesOfAward = rs.getString("titles_of_award");
            employee_honors_awardsDTO.awardingInstitution = rs.getString("awarding_institution");
            employee_honors_awardsDTO.ground = rs.getString("ground");
            employee_honors_awardsDTO.awardDate = rs.getLong("award_date");
            employee_honors_awardsDTO.briefDescription = rs.getString("brief_description");
            employee_honors_awardsDTO.filesDropzone = rs.getLong("files_dropzone");
            employee_honors_awardsDTO.insertionDate = rs.getLong("insertion_date");
            employee_honors_awardsDTO.insertedBy = rs.getString("inserted_by");
            employee_honors_awardsDTO.modifiedBy = rs.getString("modified_by");
            employee_honors_awardsDTO.searchColumn = rs.getString("search_column");
            employee_honors_awardsDTO.isDeleted = rs.getInt("isDeleted");
            employee_honors_awardsDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return employee_honors_awardsDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "employee_honors_awards";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_honors_awardsDTO) commonDTO, addSqlQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_honors_awardsDTO) commonDTO, updateSqlQuery, false);
    }

    public List<Employee_honors_awardsDTO> getAllEmployee_honors_awards(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }

    public String getHonorAwardCategory(int catValue) {
        String sql = "SELECT * FROM language_text where id = (SELECT language_id FROM category where domain_name = 'award_type' and value =?)";
        logger.debug("catValue : " + catValue);
        Object obj = ConnectionAndStatementUtil.getReadPrepareStatement(prepareStatement -> {
            String value = null;
            try {
                prepareStatement.setObject(1, catValue);
                ResultSet rs = prepareStatement.executeQuery();
                if (rs.next()) {
                    value = rs.getString("languageTextEnglish");
                }
            } catch (Exception e) {
                logger.error(e);
            }
            return value;
        }, sql);
        return obj == null ? null : (String) obj;
    }

    public List<Employee_honors_awardsDTO> getHonorAwardModelByEmployeeId(long employeeId) {
        String sql = sqlQueryByEpmloyeeId.replace("{tableName}", getTableName());
        return getDTOs(sql, ps -> {
            try {
                ps.setLong(1, employeeId);
            } catch (SQLException e) {
                logger.error(e);
            }
        });
    }

    public List<EmployeeAwardHonorModel> getEmployeeAwardHonorModelByEmployeeId(long employeeId) {
        return getHonorAwardModelByEmployeeId(employeeId)
                .stream()
                .filter(dto -> dto.isDeleted == 0)
                .map(this::buildEmployeeAwardHonorModel)
                .collect(Collectors.toList());
    }

    private EmployeeAwardHonorModel buildEmployeeAwardHonorModel(Employee_honors_awardsDTO dto) {
        EmployeeAwardHonorModel model = new EmployeeAwardHonorModel();
        model.dto = dto;
        CategoryLanguageModel languageModel = CatRepository.getInstance().getCategoryLanguageModel("award_type", dto.awardTypeCat);
        if (languageModel == null) {
            return null;
        }
        model.awardBan = languageModel.banglaText;
        model.awardEng = languageModel.englishText;
        if (dto.awardDate > SessionConstants.MIN_DATE) {
            model.awardDateEng = new SimpleDateFormat("dd-MM-yyyy").format(new Date(dto.awardDate));
            model.awardDateBan = StringUtils.convertToBanNumber(model.awardDateEng);
        }
        model.awardingInstitution = dto.awardingInstitution;
        model.titlesOfAward = dto.titlesOfAward;
        model.briefDescription = dto.briefDescription;
        model.ground = dto.ground;
        return model;
    }
}
