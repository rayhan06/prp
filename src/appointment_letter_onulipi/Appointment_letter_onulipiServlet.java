package appointment_letter_onulipi;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Appointment_letter_onulipiServlet
 */
@WebServlet("/Appointment_letter_onulipiServlet")
@MultipartConfig
public class Appointment_letter_onulipiServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Appointment_letter_onulipiServlet.class);

    String tableName = "appointment_letter_onulipi";

	Appointment_letter_onulipiDAO appointment_letter_onulipiDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Appointment_letter_onulipiServlet()
	{
        super();
    	try
    	{
			appointment_letter_onulipiDAO = new Appointment_letter_onulipiDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(appointment_letter_onulipiDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_LETTER_ONULIPI_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_LETTER_ONULIPI_UPDATE))
				{
					getAppointment_letter_onulipi(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_LETTER_ONULIPI_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchAppointment_letter_onulipi(request, response, isPermanentTable, filter);
						}
						else
						{
							searchAppointment_letter_onulipi(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchAppointment_letter_onulipi(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_LETTER_ONULIPI_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_LETTER_ONULIPI_ADD))
				{
					System.out.println("going to  addAppointment_letter_onulipi ");
					addAppointment_letter_onulipi(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addAppointment_letter_onulipi ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_LETTER_ONULIPI_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addAppointment_letter_onulipi ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_LETTER_ONULIPI_UPDATE))
				{
					addAppointment_letter_onulipi(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_LETTER_ONULIPI_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_LETTER_ONULIPI_SEARCH))
				{
					searchAppointment_letter_onulipi(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Appointment_letter_onulipiDTO appointment_letter_onulipiDTO = appointment_letter_onulipiDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(appointment_letter_onulipiDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addAppointment_letter_onulipi(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addAppointment_letter_onulipi");
			String path = getServletContext().getRealPath("/img2/");
			Appointment_letter_onulipiDTO appointment_letter_onulipiDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				appointment_letter_onulipiDTO = new Appointment_letter_onulipiDTO();
			}
			else
			{
				appointment_letter_onulipiDTO = appointment_letter_onulipiDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("appointmentLetterId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("appointmentLetterId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				appointment_letter_onulipiDTO.appointmentLetterId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("employeeRecordId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeRecordId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				appointment_letter_onulipiDTO.employeeRecordId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("unitId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("unitId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				appointment_letter_onulipiDTO.unitId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("postId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("postId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				appointment_letter_onulipiDTO.postId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("employeeRecordName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeRecordName = " + Value);
			if(Value != null)
			{
				appointment_letter_onulipiDTO.employeeRecordName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("employeeRecordNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeRecordNameBn = " + Value);
			if(Value != null)
			{
				appointment_letter_onulipiDTO.employeeRecordNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("unitName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("unitName = " + Value);
			if(Value != null)
			{
				appointment_letter_onulipiDTO.unitName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("unitNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("unitNameBn = " + Value);
			if(Value != null)
			{
				appointment_letter_onulipiDTO.unitNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("postName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("postName = " + Value);
			if(Value != null)
			{
				appointment_letter_onulipiDTO.postName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("postNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("postNameBn = " + Value);
			if(Value != null)
			{
				appointment_letter_onulipiDTO.postNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				appointment_letter_onulipiDTO.insertionDate = c.getTimeInMillis();
			}


			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null)
			{
				appointment_letter_onulipiDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null)
			{
				appointment_letter_onulipiDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addAppointment_letter_onulipi dto = " + appointment_letter_onulipiDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				appointment_letter_onulipiDAO.setIsDeleted(appointment_letter_onulipiDTO.iD, CommonDTO.OUTDATED);
				returnedID = appointment_letter_onulipiDAO.add(appointment_letter_onulipiDTO);
				appointment_letter_onulipiDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = appointment_letter_onulipiDAO.manageWriteOperations(appointment_letter_onulipiDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = appointment_letter_onulipiDAO.manageWriteOperations(appointment_letter_onulipiDTO, SessionConstants.UPDATE, -1, userDTO);
			}









			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getAppointment_letter_onulipi(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Appointment_letter_onulipiServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(appointment_letter_onulipiDAO.getDTOByID(returnedID), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}









	private void getAppointment_letter_onulipi(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getAppointment_letter_onulipi");
		Appointment_letter_onulipiDTO appointment_letter_onulipiDTO = null;
		try
		{
			appointment_letter_onulipiDTO = appointment_letter_onulipiDAO.getDTOByID(id);
			request.setAttribute("ID", appointment_letter_onulipiDTO.iD);
			request.setAttribute("appointment_letter_onulipiDTO",appointment_letter_onulipiDTO);
			request.setAttribute("appointment_letter_onulipiDAO",appointment_letter_onulipiDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "appointment_letter_onulipi/appointment_letter_onulipiInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "appointment_letter_onulipi/appointment_letter_onulipiSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "appointment_letter_onulipi/appointment_letter_onulipiEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "appointment_letter_onulipi/appointment_letter_onulipiEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getAppointment_letter_onulipi(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getAppointment_letter_onulipi(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchAppointment_letter_onulipi(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchAppointment_letter_onulipi 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_APPOINTMENT_LETTER_ONULIPI,
			request,
			appointment_letter_onulipiDAO,
			SessionConstants.VIEW_APPOINTMENT_LETTER_ONULIPI,
			SessionConstants.SEARCH_APPOINTMENT_LETTER_ONULIPI,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("appointment_letter_onulipiDAO",appointment_letter_onulipiDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to appointment_letter_onulipi/appointment_letter_onulipiApproval.jsp");
	        	rd = request.getRequestDispatcher("appointment_letter_onulipi/appointment_letter_onulipiApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to appointment_letter_onulipi/appointment_letter_onulipiApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("appointment_letter_onulipi/appointment_letter_onulipiApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to appointment_letter_onulipi/appointment_letter_onulipiSearch.jsp");
	        	rd = request.getRequestDispatcher("appointment_letter_onulipi/appointment_letter_onulipiSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to appointment_letter_onulipi/appointment_letter_onulipiSearchForm.jsp");
	        	rd = request.getRequestDispatcher("appointment_letter_onulipi/appointment_letter_onulipiSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

