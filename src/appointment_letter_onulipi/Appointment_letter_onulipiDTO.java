package appointment_letter_onulipi;
import java.util.*; 
import util.*; 


public class Appointment_letter_onulipiDTO extends CommonDTO
{

	public long appointmentLetterId = -1;
	public long employeeRecordId = -1;
	public long unitId = -1;
	public long postId = -1;
    public String employeeRecordName = "";
    public String employeeRecordNameBn = "";
    public String unitName = "";
    public String unitNameBn = "";
    public String postName = "";
    public String postNameBn = "";
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Appointment_letter_onulipiDTO[" +
            " iD = " + iD +
            " appointmentLetterId = " + appointmentLetterId +
            " employeeRecordId = " + employeeRecordId +
            " unitId = " + unitId +
            " postId = " + postId +
            " employeeRecordName = " + employeeRecordName +
            " employeeRecordNameBn = " + employeeRecordNameBn +
            " unitName = " + unitName +
            " unitNameBn = " + unitNameBn +
            " postName = " + postName +
            " postNameBn = " + postNameBn +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}