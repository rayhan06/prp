package appointment_letter_onulipi;
import java.util.*; 
import util.*;


public class Appointment_letter_onulipiMAPS extends CommonMaps
{	
	public Appointment_letter_onulipiMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("appointmentLetterId".toLowerCase(), "appointmentLetterId".toLowerCase());
		java_DTO_map.put("employeeRecordId".toLowerCase(), "employeeRecordId".toLowerCase());
		java_DTO_map.put("unitId".toLowerCase(), "unitId".toLowerCase());
		java_DTO_map.put("postId".toLowerCase(), "postId".toLowerCase());
		java_DTO_map.put("employeeRecordName".toLowerCase(), "employeeRecordName".toLowerCase());
		java_DTO_map.put("employeeRecordNameBn".toLowerCase(), "employeeRecordNameBn".toLowerCase());
		java_DTO_map.put("unitName".toLowerCase(), "unitName".toLowerCase());
		java_DTO_map.put("unitNameBn".toLowerCase(), "unitNameBn".toLowerCase());
		java_DTO_map.put("postName".toLowerCase(), "postName".toLowerCase());
		java_DTO_map.put("postNameBn".toLowerCase(), "postNameBn".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("appointment_letter_id".toLowerCase(), "appointmentLetterId".toLowerCase());
		java_SQL_map.put("employee_record_id".toLowerCase(), "employeeRecordId".toLowerCase());
		java_SQL_map.put("unit_id".toLowerCase(), "unitId".toLowerCase());
		java_SQL_map.put("post_id".toLowerCase(), "postId".toLowerCase());
		java_SQL_map.put("employee_record_name".toLowerCase(), "employeeRecordName".toLowerCase());
		java_SQL_map.put("employee_record_name_bn".toLowerCase(), "employeeRecordNameBn".toLowerCase());
		java_SQL_map.put("unit_name".toLowerCase(), "unitName".toLowerCase());
		java_SQL_map.put("unit_name_bn".toLowerCase(), "unitNameBn".toLowerCase());
		java_SQL_map.put("post_name".toLowerCase(), "postName".toLowerCase());
		java_SQL_map.put("post_name_bn".toLowerCase(), "postNameBn".toLowerCase());
		java_SQL_map.put("inserted_by".toLowerCase(), "insertedBy".toLowerCase());
		java_SQL_map.put("modified_by".toLowerCase(), "modifiedBy".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Appointment Letter Id".toLowerCase(), "appointmentLetterId".toLowerCase());
		java_Text_map.put("Employee Record Id".toLowerCase(), "employeeRecordId".toLowerCase());
		java_Text_map.put("Unit Id".toLowerCase(), "unitId".toLowerCase());
		java_Text_map.put("Post Id".toLowerCase(), "postId".toLowerCase());
		java_Text_map.put("Employee Record Name".toLowerCase(), "employeeRecordName".toLowerCase());
		java_Text_map.put("Employee Record Name Bn".toLowerCase(), "employeeRecordNameBn".toLowerCase());
		java_Text_map.put("Unit Name".toLowerCase(), "unitName".toLowerCase());
		java_Text_map.put("Unit Name Bn".toLowerCase(), "unitNameBn".toLowerCase());
		java_Text_map.put("Post Name".toLowerCase(), "postName".toLowerCase());
		java_Text_map.put("Post Name Bn".toLowerCase(), "postNameBn".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}