package appointment_letter_onulipi;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;
import vm_route.VmRouteStoppageDTO;


public class Appointment_letter_onulipiRepository implements Repository {
	Appointment_letter_onulipiDAO appointment_letter_onulipiDAO = null;
	Gson gson = new Gson();
	
	public void setDAO(Appointment_letter_onulipiDAO appointment_letter_onulipiDAO)
	{
		this.appointment_letter_onulipiDAO = appointment_letter_onulipiDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Appointment_letter_onulipiRepository.class);
	Map<Long, Appointment_letter_onulipiDTO>mapOfAppointment_letter_onulipiDTOToiD;
	Map<Long, Set<Appointment_letter_onulipiDTO> >mapOfAppointment_letter_onulipiDTOToappointmentLetterId;
//	Map<Long, Set<Appointment_letter_onulipiDTO> >mapOfAppointment_letter_onulipiDTOToemployeeRecordId;
//	Map<Long, Set<Appointment_letter_onulipiDTO> >mapOfAppointment_letter_onulipiDTOTounitId;
//	Map<Long, Set<Appointment_letter_onulipiDTO> >mapOfAppointment_letter_onulipiDTOTopostId;
//	Map<String, Set<Appointment_letter_onulipiDTO> >mapOfAppointment_letter_onulipiDTOToemployeeRecordName;
//	Map<String, Set<Appointment_letter_onulipiDTO> >mapOfAppointment_letter_onulipiDTOToemployeeRecordNameBn;
//	Map<String, Set<Appointment_letter_onulipiDTO> >mapOfAppointment_letter_onulipiDTOTounitName;
//	Map<String, Set<Appointment_letter_onulipiDTO> >mapOfAppointment_letter_onulipiDTOTounitNameBn;
//	Map<String, Set<Appointment_letter_onulipiDTO> >mapOfAppointment_letter_onulipiDTOTopostName;
//	Map<String, Set<Appointment_letter_onulipiDTO> >mapOfAppointment_letter_onulipiDTOTopostNameBn;
//	Map<Long, Set<Appointment_letter_onulipiDTO> >mapOfAppointment_letter_onulipiDTOToinsertionDate;
//	Map<String, Set<Appointment_letter_onulipiDTO> >mapOfAppointment_letter_onulipiDTOToinsertedBy;
//	Map<String, Set<Appointment_letter_onulipiDTO> >mapOfAppointment_letter_onulipiDTOTomodifiedBy;
//	Map<Long, Set<Appointment_letter_onulipiDTO> >mapOfAppointment_letter_onulipiDTOTolastModificationTime;


	static Appointment_letter_onulipiRepository instance = null;  
	private Appointment_letter_onulipiRepository(){
		mapOfAppointment_letter_onulipiDTOToiD = new ConcurrentHashMap<>();
		mapOfAppointment_letter_onulipiDTOToappointmentLetterId = new ConcurrentHashMap<>();
//		mapOfAppointment_letter_onulipiDTOToemployeeRecordId = new ConcurrentHashMap<>();
//		mapOfAppointment_letter_onulipiDTOTounitId = new ConcurrentHashMap<>();
//		mapOfAppointment_letter_onulipiDTOTopostId = new ConcurrentHashMap<>();
//		mapOfAppointment_letter_onulipiDTOToemployeeRecordName = new ConcurrentHashMap<>();
//		mapOfAppointment_letter_onulipiDTOToemployeeRecordNameBn = new ConcurrentHashMap<>();
//		mapOfAppointment_letter_onulipiDTOTounitName = new ConcurrentHashMap<>();
//		mapOfAppointment_letter_onulipiDTOTounitNameBn = new ConcurrentHashMap<>();
//		mapOfAppointment_letter_onulipiDTOTopostName = new ConcurrentHashMap<>();
//		mapOfAppointment_letter_onulipiDTOTopostNameBn = new ConcurrentHashMap<>();
//		mapOfAppointment_letter_onulipiDTOToinsertionDate = new ConcurrentHashMap<>();
//		mapOfAppointment_letter_onulipiDTOToinsertedBy = new ConcurrentHashMap<>();
//		mapOfAppointment_letter_onulipiDTOTomodifiedBy = new ConcurrentHashMap<>();
//		mapOfAppointment_letter_onulipiDTOTolastModificationTime = new ConcurrentHashMap<>();

		setDAO(new Appointment_letter_onulipiDAO());
		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Appointment_letter_onulipiRepository getInstance(){
		if (instance == null){
			instance = new Appointment_letter_onulipiRepository();
		}
		return instance;
	}

	public Appointment_letter_onulipiDTO clone(Appointment_letter_onulipiDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Appointment_letter_onulipiDTO.class);
	}

	public List<Appointment_letter_onulipiDTO> clone(List<Appointment_letter_onulipiDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public void reload(boolean reloadAll){
		if(appointment_letter_onulipiDAO == null)
		{
			return;
		}
		try {
			List<Appointment_letter_onulipiDTO> appointment_letter_onulipiDTOs = appointment_letter_onulipiDAO.getAllAppointment_letter_onulipi(reloadAll);
			for(Appointment_letter_onulipiDTO appointment_letter_onulipiDTO : appointment_letter_onulipiDTOs) {
				Appointment_letter_onulipiDTO oldAppointment_letter_onulipiDTO =
						getAppointment_letter_onulipiDTOByIDWithoutClone(appointment_letter_onulipiDTO.iD);
				if( oldAppointment_letter_onulipiDTO != null ) {
					mapOfAppointment_letter_onulipiDTOToiD.remove(oldAppointment_letter_onulipiDTO.iD);
				
					if(mapOfAppointment_letter_onulipiDTOToappointmentLetterId.containsKey(oldAppointment_letter_onulipiDTO.appointmentLetterId)) {
						mapOfAppointment_letter_onulipiDTOToappointmentLetterId.get(oldAppointment_letter_onulipiDTO.appointmentLetterId).remove(oldAppointment_letter_onulipiDTO);
					}
					if(mapOfAppointment_letter_onulipiDTOToappointmentLetterId.get(oldAppointment_letter_onulipiDTO.appointmentLetterId).isEmpty()) {
						mapOfAppointment_letter_onulipiDTOToappointmentLetterId.remove(oldAppointment_letter_onulipiDTO.appointmentLetterId);
					}
					
//					if(mapOfAppointment_letter_onulipiDTOToemployeeRecordId.containsKey(oldAppointment_letter_onulipiDTO.employeeRecordId)) {
//						mapOfAppointment_letter_onulipiDTOToemployeeRecordId.get(oldAppointment_letter_onulipiDTO.employeeRecordId).remove(oldAppointment_letter_onulipiDTO);
//					}
//					if(mapOfAppointment_letter_onulipiDTOToemployeeRecordId.get(oldAppointment_letter_onulipiDTO.employeeRecordId).isEmpty()) {
//						mapOfAppointment_letter_onulipiDTOToemployeeRecordId.remove(oldAppointment_letter_onulipiDTO.employeeRecordId);
//					}
//
//					if(mapOfAppointment_letter_onulipiDTOTounitId.containsKey(oldAppointment_letter_onulipiDTO.unitId)) {
//						mapOfAppointment_letter_onulipiDTOTounitId.get(oldAppointment_letter_onulipiDTO.unitId).remove(oldAppointment_letter_onulipiDTO);
//					}
//					if(mapOfAppointment_letter_onulipiDTOTounitId.get(oldAppointment_letter_onulipiDTO.unitId).isEmpty()) {
//						mapOfAppointment_letter_onulipiDTOTounitId.remove(oldAppointment_letter_onulipiDTO.unitId);
//					}
//
//					if(mapOfAppointment_letter_onulipiDTOTopostId.containsKey(oldAppointment_letter_onulipiDTO.postId)) {
//						mapOfAppointment_letter_onulipiDTOTopostId.get(oldAppointment_letter_onulipiDTO.postId).remove(oldAppointment_letter_onulipiDTO);
//					}
//					if(mapOfAppointment_letter_onulipiDTOTopostId.get(oldAppointment_letter_onulipiDTO.postId).isEmpty()) {
//						mapOfAppointment_letter_onulipiDTOTopostId.remove(oldAppointment_letter_onulipiDTO.postId);
//					}
//
//					if(mapOfAppointment_letter_onulipiDTOToemployeeRecordName.containsKey(oldAppointment_letter_onulipiDTO.employeeRecordName)) {
//						mapOfAppointment_letter_onulipiDTOToemployeeRecordName.get(oldAppointment_letter_onulipiDTO.employeeRecordName).remove(oldAppointment_letter_onulipiDTO);
//					}
//					if(mapOfAppointment_letter_onulipiDTOToemployeeRecordName.get(oldAppointment_letter_onulipiDTO.employeeRecordName).isEmpty()) {
//						mapOfAppointment_letter_onulipiDTOToemployeeRecordName.remove(oldAppointment_letter_onulipiDTO.employeeRecordName);
//					}
//
//					if(mapOfAppointment_letter_onulipiDTOToemployeeRecordNameBn.containsKey(oldAppointment_letter_onulipiDTO.employeeRecordNameBn)) {
//						mapOfAppointment_letter_onulipiDTOToemployeeRecordNameBn.get(oldAppointment_letter_onulipiDTO.employeeRecordNameBn).remove(oldAppointment_letter_onulipiDTO);
//					}
//					if(mapOfAppointment_letter_onulipiDTOToemployeeRecordNameBn.get(oldAppointment_letter_onulipiDTO.employeeRecordNameBn).isEmpty()) {
//						mapOfAppointment_letter_onulipiDTOToemployeeRecordNameBn.remove(oldAppointment_letter_onulipiDTO.employeeRecordNameBn);
//					}
//
//					if(mapOfAppointment_letter_onulipiDTOTounitName.containsKey(oldAppointment_letter_onulipiDTO.unitName)) {
//						mapOfAppointment_letter_onulipiDTOTounitName.get(oldAppointment_letter_onulipiDTO.unitName).remove(oldAppointment_letter_onulipiDTO);
//					}
//					if(mapOfAppointment_letter_onulipiDTOTounitName.get(oldAppointment_letter_onulipiDTO.unitName).isEmpty()) {
//						mapOfAppointment_letter_onulipiDTOTounitName.remove(oldAppointment_letter_onulipiDTO.unitName);
//					}
//
//					if(mapOfAppointment_letter_onulipiDTOTounitNameBn.containsKey(oldAppointment_letter_onulipiDTO.unitNameBn)) {
//						mapOfAppointment_letter_onulipiDTOTounitNameBn.get(oldAppointment_letter_onulipiDTO.unitNameBn).remove(oldAppointment_letter_onulipiDTO);
//					}
//					if(mapOfAppointment_letter_onulipiDTOTounitNameBn.get(oldAppointment_letter_onulipiDTO.unitNameBn).isEmpty()) {
//						mapOfAppointment_letter_onulipiDTOTounitNameBn.remove(oldAppointment_letter_onulipiDTO.unitNameBn);
//					}
//
//					if(mapOfAppointment_letter_onulipiDTOTopostName.containsKey(oldAppointment_letter_onulipiDTO.postName)) {
//						mapOfAppointment_letter_onulipiDTOTopostName.get(oldAppointment_letter_onulipiDTO.postName).remove(oldAppointment_letter_onulipiDTO);
//					}
//					if(mapOfAppointment_letter_onulipiDTOTopostName.get(oldAppointment_letter_onulipiDTO.postName).isEmpty()) {
//						mapOfAppointment_letter_onulipiDTOTopostName.remove(oldAppointment_letter_onulipiDTO.postName);
//					}
//
//					if(mapOfAppointment_letter_onulipiDTOTopostNameBn.containsKey(oldAppointment_letter_onulipiDTO.postNameBn)) {
//						mapOfAppointment_letter_onulipiDTOTopostNameBn.get(oldAppointment_letter_onulipiDTO.postNameBn).remove(oldAppointment_letter_onulipiDTO);
//					}
//					if(mapOfAppointment_letter_onulipiDTOTopostNameBn.get(oldAppointment_letter_onulipiDTO.postNameBn).isEmpty()) {
//						mapOfAppointment_letter_onulipiDTOTopostNameBn.remove(oldAppointment_letter_onulipiDTO.postNameBn);
//					}
//
//					if(mapOfAppointment_letter_onulipiDTOToinsertionDate.containsKey(oldAppointment_letter_onulipiDTO.insertionDate)) {
//						mapOfAppointment_letter_onulipiDTOToinsertionDate.get(oldAppointment_letter_onulipiDTO.insertionDate).remove(oldAppointment_letter_onulipiDTO);
//					}
//					if(mapOfAppointment_letter_onulipiDTOToinsertionDate.get(oldAppointment_letter_onulipiDTO.insertionDate).isEmpty()) {
//						mapOfAppointment_letter_onulipiDTOToinsertionDate.remove(oldAppointment_letter_onulipiDTO.insertionDate);
//					}
//
//					if(mapOfAppointment_letter_onulipiDTOToinsertedBy.containsKey(oldAppointment_letter_onulipiDTO.insertedBy)) {
//						mapOfAppointment_letter_onulipiDTOToinsertedBy.get(oldAppointment_letter_onulipiDTO.insertedBy).remove(oldAppointment_letter_onulipiDTO);
//					}
//					if(mapOfAppointment_letter_onulipiDTOToinsertedBy.get(oldAppointment_letter_onulipiDTO.insertedBy).isEmpty()) {
//						mapOfAppointment_letter_onulipiDTOToinsertedBy.remove(oldAppointment_letter_onulipiDTO.insertedBy);
//					}
//
//					if(mapOfAppointment_letter_onulipiDTOTomodifiedBy.containsKey(oldAppointment_letter_onulipiDTO.modifiedBy)) {
//						mapOfAppointment_letter_onulipiDTOTomodifiedBy.get(oldAppointment_letter_onulipiDTO.modifiedBy).remove(oldAppointment_letter_onulipiDTO);
//					}
//					if(mapOfAppointment_letter_onulipiDTOTomodifiedBy.get(oldAppointment_letter_onulipiDTO.modifiedBy).isEmpty()) {
//						mapOfAppointment_letter_onulipiDTOTomodifiedBy.remove(oldAppointment_letter_onulipiDTO.modifiedBy);
//					}
//
//					if(mapOfAppointment_letter_onulipiDTOTolastModificationTime.containsKey(oldAppointment_letter_onulipiDTO.lastModificationTime)) {
//						mapOfAppointment_letter_onulipiDTOTolastModificationTime.get(oldAppointment_letter_onulipiDTO.lastModificationTime).remove(oldAppointment_letter_onulipiDTO);
//					}
//					if(mapOfAppointment_letter_onulipiDTOTolastModificationTime.get(oldAppointment_letter_onulipiDTO.lastModificationTime).isEmpty()) {
//						mapOfAppointment_letter_onulipiDTOTolastModificationTime.remove(oldAppointment_letter_onulipiDTO.lastModificationTime);
//					}
					
					
				}
				if(appointment_letter_onulipiDTO.isDeleted == 0) 
				{
					
					mapOfAppointment_letter_onulipiDTOToiD.put(appointment_letter_onulipiDTO.iD, appointment_letter_onulipiDTO);
				
					if( ! mapOfAppointment_letter_onulipiDTOToappointmentLetterId.containsKey(appointment_letter_onulipiDTO.appointmentLetterId)) {
						mapOfAppointment_letter_onulipiDTOToappointmentLetterId.put(appointment_letter_onulipiDTO.appointmentLetterId, new HashSet<>());
					}
					mapOfAppointment_letter_onulipiDTOToappointmentLetterId.get(appointment_letter_onulipiDTO.appointmentLetterId).add(appointment_letter_onulipiDTO);
					
//					if( ! mapOfAppointment_letter_onulipiDTOToemployeeRecordId.containsKey(appointment_letter_onulipiDTO.employeeRecordId)) {
//						mapOfAppointment_letter_onulipiDTOToemployeeRecordId.put(appointment_letter_onulipiDTO.employeeRecordId, new HashSet<>());
//					}
//					mapOfAppointment_letter_onulipiDTOToemployeeRecordId.get(appointment_letter_onulipiDTO.employeeRecordId).add(appointment_letter_onulipiDTO);
//
//					if( ! mapOfAppointment_letter_onulipiDTOTounitId.containsKey(appointment_letter_onulipiDTO.unitId)) {
//						mapOfAppointment_letter_onulipiDTOTounitId.put(appointment_letter_onulipiDTO.unitId, new HashSet<>());
//					}
//					mapOfAppointment_letter_onulipiDTOTounitId.get(appointment_letter_onulipiDTO.unitId).add(appointment_letter_onulipiDTO);
//
//					if( ! mapOfAppointment_letter_onulipiDTOTopostId.containsKey(appointment_letter_onulipiDTO.postId)) {
//						mapOfAppointment_letter_onulipiDTOTopostId.put(appointment_letter_onulipiDTO.postId, new HashSet<>());
//					}
//					mapOfAppointment_letter_onulipiDTOTopostId.get(appointment_letter_onulipiDTO.postId).add(appointment_letter_onulipiDTO);
//
//					if( ! mapOfAppointment_letter_onulipiDTOToemployeeRecordName.containsKey(appointment_letter_onulipiDTO.employeeRecordName)) {
//						mapOfAppointment_letter_onulipiDTOToemployeeRecordName.put(appointment_letter_onulipiDTO.employeeRecordName, new HashSet<>());
//					}
//					mapOfAppointment_letter_onulipiDTOToemployeeRecordName.get(appointment_letter_onulipiDTO.employeeRecordName).add(appointment_letter_onulipiDTO);
//
//					if( ! mapOfAppointment_letter_onulipiDTOToemployeeRecordNameBn.containsKey(appointment_letter_onulipiDTO.employeeRecordNameBn)) {
//						mapOfAppointment_letter_onulipiDTOToemployeeRecordNameBn.put(appointment_letter_onulipiDTO.employeeRecordNameBn, new HashSet<>());
//					}
//					mapOfAppointment_letter_onulipiDTOToemployeeRecordNameBn.get(appointment_letter_onulipiDTO.employeeRecordNameBn).add(appointment_letter_onulipiDTO);
//
//					if( ! mapOfAppointment_letter_onulipiDTOTounitName.containsKey(appointment_letter_onulipiDTO.unitName)) {
//						mapOfAppointment_letter_onulipiDTOTounitName.put(appointment_letter_onulipiDTO.unitName, new HashSet<>());
//					}
//					mapOfAppointment_letter_onulipiDTOTounitName.get(appointment_letter_onulipiDTO.unitName).add(appointment_letter_onulipiDTO);
//
//					if( ! mapOfAppointment_letter_onulipiDTOTounitNameBn.containsKey(appointment_letter_onulipiDTO.unitNameBn)) {
//						mapOfAppointment_letter_onulipiDTOTounitNameBn.put(appointment_letter_onulipiDTO.unitNameBn, new HashSet<>());
//					}
//					mapOfAppointment_letter_onulipiDTOTounitNameBn.get(appointment_letter_onulipiDTO.unitNameBn).add(appointment_letter_onulipiDTO);
//
//					if( ! mapOfAppointment_letter_onulipiDTOTopostName.containsKey(appointment_letter_onulipiDTO.postName)) {
//						mapOfAppointment_letter_onulipiDTOTopostName.put(appointment_letter_onulipiDTO.postName, new HashSet<>());
//					}
//					mapOfAppointment_letter_onulipiDTOTopostName.get(appointment_letter_onulipiDTO.postName).add(appointment_letter_onulipiDTO);
//
//					if( ! mapOfAppointment_letter_onulipiDTOTopostNameBn.containsKey(appointment_letter_onulipiDTO.postNameBn)) {
//						mapOfAppointment_letter_onulipiDTOTopostNameBn.put(appointment_letter_onulipiDTO.postNameBn, new HashSet<>());
//					}
//					mapOfAppointment_letter_onulipiDTOTopostNameBn.get(appointment_letter_onulipiDTO.postNameBn).add(appointment_letter_onulipiDTO);
//
//					if( ! mapOfAppointment_letter_onulipiDTOToinsertionDate.containsKey(appointment_letter_onulipiDTO.insertionDate)) {
//						mapOfAppointment_letter_onulipiDTOToinsertionDate.put(appointment_letter_onulipiDTO.insertionDate, new HashSet<>());
//					}
//					mapOfAppointment_letter_onulipiDTOToinsertionDate.get(appointment_letter_onulipiDTO.insertionDate).add(appointment_letter_onulipiDTO);
//
//					if( ! mapOfAppointment_letter_onulipiDTOToinsertedBy.containsKey(appointment_letter_onulipiDTO.insertedBy)) {
//						mapOfAppointment_letter_onulipiDTOToinsertedBy.put(appointment_letter_onulipiDTO.insertedBy, new HashSet<>());
//					}
//					mapOfAppointment_letter_onulipiDTOToinsertedBy.get(appointment_letter_onulipiDTO.insertedBy).add(appointment_letter_onulipiDTO);
//
//					if( ! mapOfAppointment_letter_onulipiDTOTomodifiedBy.containsKey(appointment_letter_onulipiDTO.modifiedBy)) {
//						mapOfAppointment_letter_onulipiDTOTomodifiedBy.put(appointment_letter_onulipiDTO.modifiedBy, new HashSet<>());
//					}
//					mapOfAppointment_letter_onulipiDTOTomodifiedBy.get(appointment_letter_onulipiDTO.modifiedBy).add(appointment_letter_onulipiDTO);
//
//					if( ! mapOfAppointment_letter_onulipiDTOTolastModificationTime.containsKey(appointment_letter_onulipiDTO.lastModificationTime)) {
//						mapOfAppointment_letter_onulipiDTOTolastModificationTime.put(appointment_letter_onulipiDTO.lastModificationTime, new HashSet<>());
//					}
//					mapOfAppointment_letter_onulipiDTOTolastModificationTime.get(appointment_letter_onulipiDTO.lastModificationTime).add(appointment_letter_onulipiDTO);
//
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Appointment_letter_onulipiDTO> getAppointment_letter_onulipiList() {
		List <Appointment_letter_onulipiDTO> appointment_letter_onulipis = new ArrayList<Appointment_letter_onulipiDTO>(this.mapOfAppointment_letter_onulipiDTOToiD.values());
		return clone(appointment_letter_onulipis);
	}
	
	
	public Appointment_letter_onulipiDTO getAppointment_letter_onulipiDTOByIDWithoutClone( long ID){
		return mapOfAppointment_letter_onulipiDTOToiD.get(ID);
	}

	public Appointment_letter_onulipiDTO getAppointment_letter_onulipiDTOByID( long ID){
		return clone(mapOfAppointment_letter_onulipiDTOToiD.get(ID));
	}
	
	
	public List<Appointment_letter_onulipiDTO> getAppointment_letter_onulipiDTOByappointment_letter_id(long appointment_letter_id) {
		return clone(new ArrayList<>( mapOfAppointment_letter_onulipiDTOToappointmentLetterId.
				getOrDefault(appointment_letter_id,new HashSet<>())));
	}
	
	
//	public List<Appointment_letter_onulipiDTO> getAppointment_letter_onulipiDTOByemployee_record_id(long employee_record_id) {
//		return new ArrayList<>( mapOfAppointment_letter_onulipiDTOToemployeeRecordId.getOrDefault(employee_record_id,new HashSet<>()));
//	}
//
//
//	public List<Appointment_letter_onulipiDTO> getAppointment_letter_onulipiDTOByunit_id(long unit_id) {
//		return new ArrayList<>( mapOfAppointment_letter_onulipiDTOTounitId.getOrDefault(unit_id,new HashSet<>()));
//	}
//
//
//	public List<Appointment_letter_onulipiDTO> getAppointment_letter_onulipiDTOBypost_id(long post_id) {
//		return new ArrayList<>( mapOfAppointment_letter_onulipiDTOTopostId.getOrDefault(post_id,new HashSet<>()));
//	}
//
//
//	public List<Appointment_letter_onulipiDTO> getAppointment_letter_onulipiDTOByemployee_record_name(String employee_record_name) {
//		return new ArrayList<>( mapOfAppointment_letter_onulipiDTOToemployeeRecordName.getOrDefault(employee_record_name,new HashSet<>()));
//	}
//
//
//	public List<Appointment_letter_onulipiDTO> getAppointment_letter_onulipiDTOByemployee_record_name_bn(String employee_record_name_bn) {
//		return new ArrayList<>( mapOfAppointment_letter_onulipiDTOToemployeeRecordNameBn.getOrDefault(employee_record_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Appointment_letter_onulipiDTO> getAppointment_letter_onulipiDTOByunit_name(String unit_name) {
//		return new ArrayList<>( mapOfAppointment_letter_onulipiDTOTounitName.getOrDefault(unit_name,new HashSet<>()));
//	}
//
//
//	public List<Appointment_letter_onulipiDTO> getAppointment_letter_onulipiDTOByunit_name_bn(String unit_name_bn) {
//		return new ArrayList<>( mapOfAppointment_letter_onulipiDTOTounitNameBn.getOrDefault(unit_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Appointment_letter_onulipiDTO> getAppointment_letter_onulipiDTOBypost_name(String post_name) {
//		return new ArrayList<>( mapOfAppointment_letter_onulipiDTOTopostName.getOrDefault(post_name,new HashSet<>()));
//	}
//
//
//	public List<Appointment_letter_onulipiDTO> getAppointment_letter_onulipiDTOBypost_name_bn(String post_name_bn) {
//		return new ArrayList<>( mapOfAppointment_letter_onulipiDTOTopostNameBn.getOrDefault(post_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Appointment_letter_onulipiDTO> getAppointment_letter_onulipiDTOByinsertion_date(long insertion_date) {
//		return new ArrayList<>( mapOfAppointment_letter_onulipiDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
//	}
//
//
//	public List<Appointment_letter_onulipiDTO> getAppointment_letter_onulipiDTOByinserted_by(String inserted_by) {
//		return new ArrayList<>( mapOfAppointment_letter_onulipiDTOToinsertedBy.getOrDefault(inserted_by,new HashSet<>()));
//	}
//
//
//	public List<Appointment_letter_onulipiDTO> getAppointment_letter_onulipiDTOBymodified_by(String modified_by) {
//		return new ArrayList<>( mapOfAppointment_letter_onulipiDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
//	}
//
//
//	public List<Appointment_letter_onulipiDTO> getAppointment_letter_onulipiDTOBylastModificationTime(long lastModificationTime) {
//		return new ArrayList<>( mapOfAppointment_letter_onulipiDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
//	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "appointment_letter_onulipi";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


