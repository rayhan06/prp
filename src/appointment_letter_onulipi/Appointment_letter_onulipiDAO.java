package appointment_letter_onulipi;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Appointment_letter_onulipiDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Appointment_letter_onulipiDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Appointment_letter_onulipiMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"appointment_letter_id",
			"employee_record_id",
			"unit_id",
			"post_id",
			"employee_record_name",
			"employee_record_name_bn",
			"unit_name",
			"unit_name_bn",
			"post_name",
			"post_name_bn",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Appointment_letter_onulipiDAO()
	{
		this("appointment_letter_onulipi");		
	}
	
	public void setSearchColumn(Appointment_letter_onulipiDTO appointment_letter_onulipiDTO)
	{
		appointment_letter_onulipiDTO.searchColumn = "";
		appointment_letter_onulipiDTO.searchColumn += appointment_letter_onulipiDTO.employeeRecordName + " ";
		appointment_letter_onulipiDTO.searchColumn += appointment_letter_onulipiDTO.employeeRecordNameBn + " ";
		appointment_letter_onulipiDTO.searchColumn += appointment_letter_onulipiDTO.unitName + " ";
		appointment_letter_onulipiDTO.searchColumn += appointment_letter_onulipiDTO.unitNameBn + " ";
		appointment_letter_onulipiDTO.searchColumn += appointment_letter_onulipiDTO.postName + " ";
		appointment_letter_onulipiDTO.searchColumn += appointment_letter_onulipiDTO.postNameBn + " ";
		appointment_letter_onulipiDTO.searchColumn += appointment_letter_onulipiDTO.insertedBy + " ";
		appointment_letter_onulipiDTO.searchColumn += appointment_letter_onulipiDTO.modifiedBy + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Appointment_letter_onulipiDTO appointment_letter_onulipiDTO = (Appointment_letter_onulipiDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(appointment_letter_onulipiDTO);
		if(isInsert)
		{
			ps.setObject(index++,appointment_letter_onulipiDTO.iD);
		}
		ps.setObject(index++,appointment_letter_onulipiDTO.appointmentLetterId);
		ps.setObject(index++,appointment_letter_onulipiDTO.employeeRecordId);
		ps.setObject(index++,appointment_letter_onulipiDTO.unitId);
		ps.setObject(index++,appointment_letter_onulipiDTO.postId);
		ps.setObject(index++,appointment_letter_onulipiDTO.employeeRecordName);
		ps.setObject(index++,appointment_letter_onulipiDTO.employeeRecordNameBn);
		ps.setObject(index++,appointment_letter_onulipiDTO.unitName);
		ps.setObject(index++,appointment_letter_onulipiDTO.unitNameBn);
		ps.setObject(index++,appointment_letter_onulipiDTO.postName);
		ps.setObject(index++,appointment_letter_onulipiDTO.postNameBn);
		ps.setObject(index++,appointment_letter_onulipiDTO.insertionDate);
		ps.setObject(index++,appointment_letter_onulipiDTO.insertedBy);
		ps.setObject(index++,appointment_letter_onulipiDTO.modifiedBy);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}

	public Appointment_letter_onulipiDTO build(ResultSet rs)
	{
		try
		{
			Appointment_letter_onulipiDTO appointment_letter_onulipiDTO = new Appointment_letter_onulipiDTO();
			appointment_letter_onulipiDTO.iD = rs.getLong("ID");
			appointment_letter_onulipiDTO.appointmentLetterId = rs.getLong("appointment_letter_id");
			appointment_letter_onulipiDTO.employeeRecordId = rs.getLong("employee_record_id");
			appointment_letter_onulipiDTO.unitId = rs.getLong("unit_id");
			appointment_letter_onulipiDTO.postId = rs.getLong("post_id");
			appointment_letter_onulipiDTO.employeeRecordName = rs.getString("employee_record_name");
			appointment_letter_onulipiDTO.employeeRecordNameBn = rs.getString("employee_record_name_bn");
			appointment_letter_onulipiDTO.unitName = rs.getString("unit_name");
			appointment_letter_onulipiDTO.unitNameBn = rs.getString("unit_name_bn");
			appointment_letter_onulipiDTO.postName = rs.getString("post_name");
			appointment_letter_onulipiDTO.postNameBn = rs.getString("post_name_bn");
			appointment_letter_onulipiDTO.insertionDate = rs.getLong("insertion_date");
			appointment_letter_onulipiDTO.insertedBy = rs.getString("inserted_by");
			appointment_letter_onulipiDTO.modifiedBy = rs.getString("modified_by");
			appointment_letter_onulipiDTO.isDeleted = rs.getInt("isDeleted");
			appointment_letter_onulipiDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return appointment_letter_onulipiDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	
	
	
		
	

	//need another getter for repository
	public Appointment_letter_onulipiDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		return 	ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
	}
	
	
	
	
	public List<Appointment_letter_onulipiDTO> getDTOs(Collection recordIDs){
		if(recordIDs.isEmpty()){
			return new ArrayList<>();
		}
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+= " ? ";
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	
	}
	
	

	
	
	
	//add repository
	public List<Appointment_letter_onulipiDTO> getAllAppointment_letter_onulipi (boolean isFirstReload)
    {

		String sql = "SELECT * FROM appointment_letter_onulipi";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by appointment_letter_onulipi.lastModificationTime desc";
		printSql(sql);

		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }


	public List<Appointment_letter_onulipiDTO> getAllAppointment_letter_onulipiByAppointmentLetterId (long appointmentLetterId)
	{

		String sql = "SELECT * FROM appointment_letter_onulipi";
		sql += " WHERE ";
		sql+=" isDeleted =  0 and appointment_letter_id = ? " ;

		sql += " order by appointment_letter_onulipi.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(appointmentLetterId), this::build);
	}

	
	public List<Appointment_letter_onulipiDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Appointment_letter_onulipiDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{

		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("employee_record_name")
						|| str.equals("employee_record_name_bn")
						|| str.equals("unit_name")
						|| str.equals("unit_name_bn")
						|| str.equals("post_name")
						|| str.equals("post_name_bn")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
						|| str.equals("inserted_by")
						|| str.equals("modified_by")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("employee_record_name"))
					{
						AllFieldSql += "" + tableName + ".employee_record_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("employee_record_name_bn"))
					{
						AllFieldSql += "" + tableName + ".employee_record_name_bn like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("unit_name"))
					{
						AllFieldSql += "" + tableName + ".unit_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".unit_name_bn like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("post_name"))
					{
						AllFieldSql += "" + tableName + ".post_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("post_name_bn"))
					{
						AllFieldSql += "" + tableName + ".post_name_bn like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("inserted_by"))
					{
						AllFieldSql += "" + tableName + ".inserted_by like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("modified_by"))
					{
						AllFieldSql += "" + tableName + ".modified_by like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	