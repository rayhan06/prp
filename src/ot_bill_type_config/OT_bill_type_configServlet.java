package ot_bill_type_config;

import org.apache.log4j.Logger;
import pb.Utils;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/OT_bill_type_configServlet")
@MultipartConfig
public class OT_bill_type_configServlet extends HttpServlet {
    private final Logger logger = Logger.getLogger(OT_bill_type_configServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

        String actionType = request.getParameter("actionType");
        try {
            if ("ajax_getEconomicSubCodeDropDown".equals(actionType)) {
                Long selectedEconomicSubCodeId = Utils.parseOptionalLong(
                        request.getParameter("selectedEconomicSubCodeId"),
                        null,
                        null
                );
                Long budgetMappingId = Utils.parseMandatoryLong(
                        request.getParameter("budgetMappingId"),
                        "Select budgetMappingId"
                );
                String economicSubCodeDropDown =
                        OT_bill_type_configRepository.getInstance().buildEconomicSubCodeDropDown(
                                language,
                                selectedEconomicSubCodeId,
                                budgetMappingId
                        );
                response.setCharacterEncoding("UTF-8");
                response.getWriter().println(economicSubCodeDropDown);
                return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

}
