package ot_bill_type_config;

import sessionmanager.SessionConstants;
import util.CommonDTO;

public class OT_bill_type_configDTO extends CommonDTO {
    public long budgetOfficeId;
    public long otEmployeeTypeId;
    public long budgetMappingId;
    public long economicSubCodeId;
    public long taskTypeId;
    public String financeBillViewTemplate;

    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;
}
