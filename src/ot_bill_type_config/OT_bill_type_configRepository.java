package ot_bill_type_config;

import budget_mapping.Budget_mappingRepository;
import economic_sub_code.Economic_sub_codeDTO;
import economic_sub_code.Economic_sub_codeRepository;
import org.apache.log4j.Logger;
import ot_employee_type.OT_employee_typeRepository;
import repository.Repository;
import repository.RepositoryManager;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class OT_bill_type_configRepository implements Repository {
    private static final Logger logger = Logger.getLogger(OT_employee_typeRepository.class);

    private final Map<Long, OT_bill_type_configDTO> mapById;

    private OT_bill_type_configRepository() {
        mapById = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static OT_bill_type_configRepository INSTANCE = new OT_bill_type_configRepository();
    }

    public static OT_bill_type_configRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public synchronized void reload(boolean reloadAll) {
        logger.debug("OT_bill_type_configRepository loading start for reloadAll: " + reloadAll);
        List<OT_bill_type_configDTO> dtoList = OT_bill_type_configDAO.getInstance().getAllDTOs(reloadAll);
        if (dtoList != null && dtoList.size() > 0) {
            dtoList.stream()
                   .peek(this::removeIfPresent)
                   .filter(dto -> dto.isDeleted == 0)
                   .forEach(dto -> mapById.put(dto.iD, dto));
        }
        logger.debug("OT_bill_type_configRepository loading end for reloadAll: " + reloadAll);
    }

    private void removeIfPresent(OT_bill_type_configDTO dto) {
        if (mapById.get(dto.iD) != null) {
            mapById.remove(dto.iD);
        }
    }

    public OT_bill_type_configDTO getById(long id) {
        return mapById.getOrDefault(id, null);
    }

    public OT_bill_type_configDTO findByBudgetOfficeAndEmployeeType(long budgetOfficeId, long otEmployeeTypeId) {
        return mapById.values()
                      .stream()
                      .filter(dto -> dto.budgetOfficeId == budgetOfficeId)
                      .filter(dto -> dto.otEmployeeTypeId == otEmployeeTypeId)
                      .findAny()
                      .orElse(null);
    }

    public OT_bill_type_configDTO findByBudgetMappingAndEconomicCode(long budgetMappingId, long economicSubCodeId) {
        return mapById.values()
                      .stream()
                      .filter(dto -> dto.budgetMappingId == budgetMappingId)
                      .filter(dto -> dto.economicSubCodeId == economicSubCodeId)
                      .findAny()
                      .orElse(null);
    }

    public List<OT_bill_type_configDTO> findByBudgetOffice(Long budgetOfficeId) {
        return mapById.values()
                      .stream()
                      .filter(dto -> dto.budgetOfficeId == budgetOfficeId)
                      .collect(Collectors.toList());
    }

    public String buildBudgetMappingDropDown(String language, Long selectedBudgetMappingId, boolean withCode) {
        Set<Long> budgetMappingIds = mapById.values()
                                            .stream()
                                            .map(ot_bill_type_configDTO -> ot_bill_type_configDTO.budgetMappingId)
                                            .collect(Collectors.toSet());
        return Budget_mappingRepository.getInstance().buildDropDown(
                language,
                selectedBudgetMappingId,
                withCode,
                budget_mappingDTO -> budgetMappingIds.contains(budget_mappingDTO.iD)
        );
    }

    public String buildEconomicSubCodeDropDown(String language, Long selectedEconomicSubCodeId, Long budgetMappingId) {
        List<Economic_sub_codeDTO> subCodeDTOs =
                mapById.values()
                       .stream()
                       .filter(ot_bill_type_configDTO -> ot_bill_type_configDTO.budgetMappingId == budgetMappingId)
                       .map(ot_bill_type_configDTO -> ot_bill_type_configDTO.economicSubCodeId)
                       .distinct()
                       .map(economicSubCodeId -> Economic_sub_codeRepository.getInstance().getDTOByID(economicSubCodeId))
                       .collect(Collectors.toList());
        return Economic_sub_codeRepository.getInstance().buildOptionFromDTOs(
                subCodeDTOs,
                language,
                selectedEconomicSubCodeId
        );
    }

    @Override
    public String getTableName() {
        return "ot_bill_type_config";
    }
}
