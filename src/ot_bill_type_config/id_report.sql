select id as budget_mapping_id,
       (
           select code
           from budget_operation
           where id = budget_mapping.budget_operation_id
       )  as budget_code,
       (
           select description_bng
           from budget_operation
           where id = budget_mapping.budget_operation_id
       )  as budget_office_name,
       (
           select name_en
           from category
           where domain_name = 'budget'
             and value = budget_mapping.budget_cat
       )  as budget_type
from budget_mapping
where budget_operation_id in (
    select id
    from budget_operation
    where code in (
                   1020101100004,
                   1020102100005,
                   1020201100006,
                   1020202100008,
                   1020203100010,
                   1020204100012,
                   1020205100013,
                   1020206100015
        )
)
order by budget_code;

# budget_mapping_id,budget_code,budget_office_name,budget_type
# 1,1020101100004,"সচিবালয়,জাতীয় সংসদ",OPERATIONAL
# 3,1020101100004,"সচিবালয়,জাতীয় সংসদ",REVENUE
# 2,1020102100005,সংসদ সাস্থ্য কেন্দ্র,OPERATIONAL
# 4,1020201100006,স্পীকারের কার্যালয়,OPERATIONAL
# 5,1020202100008,সংসদ নেতা কার্যালয়,OPERATIONAL
# 6,1020203100010,বিরোধী দলীয় নেতার কার্যালয়,OPERATIONAL
# 7,1020204100012,সংসদীয় স্থায়ী কমিটির কার্যালয়,OPERATIONAL
# 8,1020205100013,চীফ হুইফের কার্যালয়,OPERATIONAL
# 9,1020206100015,সংসদ সদস্যগণের কার্যালয়,OPERATIONAL

select id as economic_sub_code_id,
       code,
       description_en,
       description_bn
from economic_sub_code
where code in (
               '3111327',
               '3211104',
               '3211109'
    )
order by code;

# economic_sub_code_id,code,description_en,description_bn
# 602,3111327,Overtime Allowance,অধিকাল ভাতা
# 301,3211104,Ancillary Employee/Institutions (Administrative Expenses),আনুষঙ্গিক কর্মচারী/প্রতিষ্ঠান (প্রশাসনিক ব্যয়)
# 304,3211109,Gross Salary (Excluding Government Employees),সাকুল্য বেতন(সরকারি কর্মচারী ব্যতিত)

