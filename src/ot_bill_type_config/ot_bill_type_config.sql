CREATE TABLE ot_bill_type_config
(
    ID                         BIGINT PRIMARY KEY,

    budget_office_id           BIGINT,
    ot_employee_type_id        BIGINT,
    budget_mapping_id          BIGINT,
    economic_sub_code_id       BIGINT,
    task_type_id               BIGINT,
    finance_bill_view_template VARCHAR(1024),

    modified_by                BIGINT,
    lastModificationTime       BIGINT DEFAULT -62135791200000,
    inserted_by                BIGINT,
    insertion_time             BIGINT DEFAULT -62135791200000,
    isDeleted                  INT    DEFAULT 0,

    -- (budget_office_id, ot_employee_type_id) uniquely identifies each Overtime Bill
    CONSTRAINT unique_budget_office_emp_type UNIQUE (budget_office_id, ot_employee_type_id)
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

INSERT INTO vb_sequencer (table_name, next_id, table_LastModificationTime)
VALUES ('ot_bill_type_config', 1, 0);

INSERT INTO task_type (ID, name_en, name_bn, isDeleted, inserted_by, insertion_time, modified_by, lastModificationTime)
VALUES (06, 'OT Bill Section Head -> Branch/Wing Head', 'অধিকাল ভাতা শাখা -> অধিশাখা/উইং', 0, -1, 0, -1, 0),
       (07, 'OT Bill Finance-1 Head', 'অধিকাল ভাতা অর্থ শাখা-১ প্রধান', 0, -1, 0, -1, 0),
       (08, 'OT Bill Director(Medical) -> Finance-1 Head', 'অধিকাল ভাতা ডিরেক্টর(মেডিকেল) -> অর্থ শাখা-১ প্রধান', 0, -1, 0, -1, 0),
       (09, 'OT Bill APS/PS', 'অধিকাল ভাতা এ.পি.এস/পি.এস', 0, -1, 0, -1, 0),
       (10, 'OT Bill PS -> Committee Secretary(Virtual)', 'অধিকাল ভাতা পি.এস -> কমিটি সেক্রেটারি(ভার্চুয়াল)', 0, -1, 0, -1, 0),
       (11, 'OT Bill MP', 'অধিকাল ভাতা এম.পি', 0, -1, 0, -1, 0),
       (12, 'OT Speaker & Deputy Speaker', 'অধিকাল ভাতা স্পিকার ও ডেপুটি স্পিকার', 0, -1, 0, -1, 0),
       (13, 'OT Chief Whip & Whip', 'অধিকাল ভাতা হুইপ ও চীফ হুইপ', 0, -1, 0, -1, 0),
       (14, 'Driver Ot Bill', 'ড্রাইভারদের অধিকাল ভাতা', 0, -1, 0, -1, 0);

INSERT INTO ot_bill_type_config (ID, budget_office_id, ot_employee_type_id, budget_mapping_id,
                                 economic_sub_code_id, task_type_id, finance_bill_view_template, modified_by,
                                 lastModificationTime, inserted_by, insertion_time, isDeleted)
VALUES -- Parliament Secretariat
       (1, 1, 6, 1, 602, 6, 'overtime_allowance/overtime_allowancePreview.jsp', -1, 0, -1, 0, 0),
       (2, 1, 5, 1, 602, 7, 'overtime_allowance/overtime_allowancePreview.jsp', -1, 0, -1, 0, 0),
       (3, 1, 4, 1, 301, 6, 'overtime_allowance/overtime_allowancePreviewMasterRoll.jsp', -1, 0, -1, 0, 0),
       -- Medical
       (4, 2, 2, 2, 602, 8, 'overtime_allowance/overtime_allowancePreview.jsp', -1, 0, -1, 0, 0),
       (5, 2, 4, 1, 301, 8, 'overtime_allowance/overtime_allowancePreviewMasterRoll.jsp', -1, 0, -1, 0, 0),
       (6, 2, 1, 1, 602, 8, 'overtime_allowance/overtime_allowancePreview.jsp', -1, 0, -1, 0, 0),
       -- Speaker & Deputy Speaker
       (7, 3, 2, 4, 602, 12, 'overtime_allowance/overtime_allowancePreview.jsp', -1, 0, -1, 0, 0),
       (8, 3, 4, 1, 301, 12, 'overtime_allowance/overtime_allowancePreviewMasterRoll.jsp', -1, 0, -1, 0, 0),
       (9, 3, 1, 1, 602, 12, 'overtime_allowance/overtime_allowancePreview.jsp', -1, 0, -1, 0, 0),
       (10, 3, 3, 4, 304, 12, 'overtime_allowance/overtime_allowancePreview.jsp', -1, 0, -1, 0, 0),
       -- Leader & Deputy Leader
       (11, 4, 2, 5, 602, 9, 'overtime_allowance/overtime_allowancePreview.jsp', -1, 0, -1, 0, 0),
       (12, 4, 4, 1, 301, 9, 'overtime_allowance/overtime_allowancePreviewMasterRoll.jsp', -1, 0, -1, 0, 0),
       (13, 4, 1, 1, 602, 9, 'overtime_allowance/overtime_allowancePreview.jsp', -1, 0, -1, 0, 0),
       (14, 4, 3, 5, 304, 9, 'overtime_allowance/overtime_allowancePreview.jsp', -1, 0, -1, 0, 0),
       -- Leader of the Opposition
       (15, 5, 2, 6, 602, 9, 'overtime_allowance/overtime_allowancePreview.jsp', -1, 0, -1, 0, 0),
       (16, 5, 4, 1, 301, 9, 'overtime_allowance/overtime_allowancePreviewMasterRoll.jsp', -1, 0, -1, 0, 0),
       (17, 5, 1, 1, 602, 9, 'overtime_allowance/overtime_allowancePreview.jsp', -1, 0, -1, 0, 0),
       (18, 5, 3, 6, 304, 9, 'overtime_allowance/overtime_allowancePreview.jsp', -1, 0, -1, 0, 0),
       -- Standing Committee
       (19, 6, 2, 7, 602, 10, 'overtime_allowance/overtime_allowancePreview.jsp', -1, 0, -1, 0, 0),
       (20, 6, 3, 7, 304, 10, 'overtime_allowance/overtime_allowancePreview.jsp', -1, 0, -1, 0, 0),
       (26, 6, 7, 7, 602, 10, 'overtime_allowance/overtime_allowancePreview.jsp', -1, 0, -1, 0, 0),
       -- Chief Whip & Whip
       (21, 7, 2, 8, 602, 13, 'overtime_allowance/overtime_allowancePreview.jsp', -1, 0, -1, 0, 0),
       (22, 7, 4, 1, 301, 13, 'overtime_allowance/overtime_allowancePreviewMasterRoll.jsp', -1, 0, -1, 0, 0),
       (23, 7, 1, 1, 602, 13, 'overtime_allowance/overtime_allowancePreview.jsp', -1, 0, -1, 0, 0),
       (24, 7, 3, 8, 304, 13, 'overtime_allowance/overtime_allowancePreview.jsp', -1, 0, -1, 0, 0),
       -- Member of the Parliament
       (25, 8, 3, 9, 304, 11, 'overtime_allowance/overtime_allowancePreview.jsp', -1, 0, -1, 0, 0);