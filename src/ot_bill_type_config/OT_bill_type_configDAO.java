package ot_bill_type_config;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class OT_bill_type_configDAO implements CommonDAOService<OT_bill_type_configDTO> {
    private static final Logger logger = Logger.getLogger(OT_bill_type_configDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (budget_office_id,ot_employee_type_id,budget_mapping_id,economic_sub_code_id,task_type_id,"
                    .concat("finance_bill_view_template,modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET budget_office_id=?,ot_employee_type_id=?,budget_mapping_id=?,economic_sub_code_id=?,task_type_id=?,"
                    .concat("finance_bill_view_template=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private OT_bill_type_configDAO() {

    }

    private static class LazyLoader {
        static final OT_bill_type_configDAO INSTANCE = new OT_bill_type_configDAO();
    }

    public static OT_bill_type_configDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public String getTableName() {
        return "ot_bill_type_config";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public void set(PreparedStatement ps, OT_bill_type_configDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.budgetOfficeId);
        ps.setLong(++index, dto.otEmployeeTypeId);
        ps.setLong(++index, dto.budgetMappingId);
        ps.setLong(++index, dto.economicSubCodeId);
        ps.setLong(++index, dto.taskTypeId);
        ps.setString(++index, dto.financeBillViewTemplate);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public OT_bill_type_configDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            OT_bill_type_configDTO dto = new OT_bill_type_configDTO();
            dto.iD = rs.getLong("ID");
            dto.budgetOfficeId = rs.getLong("budget_office_id");
            dto.otEmployeeTypeId = rs.getLong("ot_employee_type_id");
            dto.budgetMappingId = rs.getLong("budget_mapping_id");
            dto.economicSubCodeId = rs.getLong("economic_sub_code_id");
            dto.taskTypeId = rs.getLong("task_type_id");
            dto.financeBillViewTemplate = rs.getString("finance_bill_view_template");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((OT_bill_type_configDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((OT_bill_type_configDTO) commonDTO, updateSqlQuery, false);
    }
}
