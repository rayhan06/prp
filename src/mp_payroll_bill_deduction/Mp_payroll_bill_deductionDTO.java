package mp_payroll_bill_deduction;

import sessionmanager.SessionConstants;
import util.CommonDTO;

public class Mp_payroll_bill_deductionDTO extends CommonDTO {
    public long mpPayrollBillId = -1;
    public long mpPayrollDeductionConfigurationId = -1;
    public int amount = 0;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;
    public long modifiedBy = -1;

    public Mp_payroll_bill_deductionDTO() {
    }

    public Mp_payroll_bill_deductionDTO(long configurationId, int amount) {
        this.mpPayrollDeductionConfigurationId = configurationId;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Mp_payroll_bill_deductionDTO{" +
                "mpPayrollBillId=" + mpPayrollBillId +
                ", mpPayrollDeductionConfigurationId=" + mpPayrollDeductionConfigurationId +
                ", amount=" + amount +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                '}';
    }
}
