package event_calendar;
import java.util.*; 
import util.*; 


public class Event_calendarDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
	public int eventCat = -1;
    public String eventLocation = "";
	public long eventDate = System.currentTimeMillis();
    public String eventStartTime = "";
    public String eventEndTime = "";
    public String eventDescription = "";
	public long filesDropzone = -1;
	public boolean isRecurring = false;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	public long eventActualTime = -1;
	
	public List<EventCalenderNotificationDTO> eventCalenderNotificationDTOList = new ArrayList<>();
	public List<EventMembersDTO> eventMembersDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Event_calendarDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " eventCat = " + eventCat +
            " eventLocation = " + eventLocation +
            " eventDate = " + eventDate +
            " eventStartTime = " + eventStartTime +
            " eventEndTime = " + eventEndTime +
            " eventDescription = " + eventDescription +
            " filesDropzone = " + filesDropzone +
            " isRecurring = " + isRecurring +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " eventActualTime = " + eventActualTime +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}