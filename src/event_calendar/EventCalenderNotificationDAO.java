package event_calendar;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class EventCalenderNotificationDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public EventCalenderNotificationDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new EventCalenderNotificationMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"event_calendar_id",
			"notification_cat",
			"occurrence_cat",
			"occurrence",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"noti_date_time",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public EventCalenderNotificationDAO()
	{
		this("event_calender_notification");		
	}
	
	public void setSearchColumn(EventCalenderNotificationDTO eventcalendernotificationDTO)
	{
		eventcalendernotificationDTO.searchColumn = "";
		eventcalendernotificationDTO.searchColumn += CatDAO.getName("English", "notification", eventcalendernotificationDTO.notificationCat) + " " + CatDAO.getName("Bangla", "notification", eventcalendernotificationDTO.notificationCat) + " ";
		eventcalendernotificationDTO.searchColumn += CatDAO.getName("English", "occurrence", eventcalendernotificationDTO.occurrenceCat) + " " + CatDAO.getName("Bangla", "occurrence", eventcalendernotificationDTO.occurrenceCat) + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		EventCalenderNotificationDTO eventcalendernotificationDTO = (EventCalenderNotificationDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(eventcalendernotificationDTO);
		if(isInsert)
		{
			ps.setObject(index++,eventcalendernotificationDTO.iD);
		}
		ps.setObject(index++,eventcalendernotificationDTO.eventCalendarId);
		ps.setObject(index++,eventcalendernotificationDTO.notificationCat);
		ps.setObject(index++,eventcalendernotificationDTO.occurrenceCat);
		ps.setObject(index++,eventcalendernotificationDTO.occurrence);
		ps.setObject(index++,eventcalendernotificationDTO.insertedByUserId);
		ps.setObject(index++,eventcalendernotificationDTO.insertedByOrganogramId);
		ps.setObject(index++,eventcalendernotificationDTO.insertionDate);
		ps.setObject(index++,eventcalendernotificationDTO.lastModifierUser);
		ps.setObject(index++,eventcalendernotificationDTO.notiDateTime);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(EventCalenderNotificationDTO eventcalendernotificationDTO, ResultSet rs) throws SQLException
	{
		eventcalendernotificationDTO.iD = rs.getLong("ID");
		eventcalendernotificationDTO.eventCalendarId = rs.getLong("event_calendar_id");
		eventcalendernotificationDTO.notificationCat = rs.getInt("notification_cat");
		eventcalendernotificationDTO.occurrenceCat = rs.getInt("occurrence_cat");
		eventcalendernotificationDTO.occurrence = rs.getInt("occurrence");
		eventcalendernotificationDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		eventcalendernotificationDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
		eventcalendernotificationDTO.insertionDate = rs.getLong("insertion_date");
		eventcalendernotificationDTO.lastModifierUser = rs.getString("last_modifier_user");
		eventcalendernotificationDTO.notiDateTime = rs.getLong("noti_date_time");
		eventcalendernotificationDTO.isDeleted = rs.getInt("isDeleted");
		eventcalendernotificationDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}
	
	
	
	
		
	
	public void deleteEventCalenderNotificationByEventCalendarID(long eventCalendarID) throws Exception{
		
		
		Connection connection = null;
		Statement stmt = null;
		try{
			
			String sql = "UPDATE event_calender_notification SET isDeleted=0 WHERE event_calendar_id="+eventCalendarID;			
			logger.debug("sql " + sql);
			connection = DBMW.getInstance().getConnection();
			stmt = connection.createStatement();
			stmt.execute(sql);
		}catch(Exception ex){
			logger.debug("fatal",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMW.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
	}		
   
	public List<EventCalenderNotificationDTO> getEventCalenderNotificationDTOListByEventCalendarID(long eventCalendarID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		EventCalenderNotificationDTO eventcalendernotificationDTO = null;
		List<EventCalenderNotificationDTO> eventcalendernotificationDTOList = new ArrayList<>();
		
		try{
			
			String sql = "SELECT * FROM event_calender_notification where isDeleted=0 and event_calendar_id="+eventCalendarID+" order by event_calender_notification.lastModificationTime";
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);

			while(rs.next()){
				eventcalendernotificationDTO = new EventCalenderNotificationDTO();
				get(eventcalendernotificationDTO, rs);
				eventcalendernotificationDTOList.add(eventcalendernotificationDTO);

			}			
			
		}catch(Exception ex){
			logger.debug("fatal",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return eventcalendernotificationDTOList;
	}

	//need another getter for repository
	public EventCalenderNotificationDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		EventCalenderNotificationDTO eventcalendernotificationDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				eventcalendernotificationDTO = new EventCalenderNotificationDTO();

				get(eventcalendernotificationDTO, rs);

			}			
			
			
			
			EventCalenderNotificationDAO eventCalenderNotificationDAO = new EventCalenderNotificationDAO("event_calender_notification");			
			List<EventCalenderNotificationDTO> eventCalenderNotificationDTOList = eventCalenderNotificationDAO.getEventCalenderNotificationDTOListByEventCalendarID(eventcalendernotificationDTO.iD);
			eventcalendernotificationDTO.eventCalenderNotificationDTOList = eventCalenderNotificationDTOList;
			
			EventMembersDAO eventMembersDAO = new EventMembersDAO("event_members");			
			List<EventMembersDTO> eventMembersDTOList = eventMembersDAO.getEventMembersDTOListByEventCalendarID(eventcalendernotificationDTO.iD);
			eventcalendernotificationDTO.eventMembersDTOList = eventMembersDTOList;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return eventcalendernotificationDTO;
	}
	
	
	
	
	public List<EventCalenderNotificationDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		EventCalenderNotificationDTO eventcalendernotificationDTO = null;
		List<EventCalenderNotificationDTO> eventcalendernotificationDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return eventcalendernotificationDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				eventcalendernotificationDTO = new EventCalenderNotificationDTO();
				get(eventcalendernotificationDTO, rs);
				System.out.println("got this DTO: " + eventcalendernotificationDTO);
				
				eventcalendernotificationDTOList.add(eventcalendernotificationDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return eventcalendernotificationDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<EventCalenderNotificationDTO> getAllEventCalenderNotification (boolean isFirstReload)
    {
		List<EventCalenderNotificationDTO> eventcalendernotificationDTOList = new ArrayList<>();

		String sql = "SELECT * FROM event_calender_notification";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by eventcalendernotification.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				EventCalenderNotificationDTO eventcalendernotificationDTO = new EventCalenderNotificationDTO();
				get(eventcalendernotificationDTO, rs);
				
				eventcalendernotificationDTOList.add(eventcalendernotificationDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return eventcalendernotificationDTOList;
    }

	
	public List<EventCalenderNotificationDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<EventCalenderNotificationDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<EventCalenderNotificationDTO> eventcalendernotificationDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				EventCalenderNotificationDTO eventcalendernotificationDTO = new EventCalenderNotificationDTO();
				get(eventcalendernotificationDTO, rs);
				
				eventcalendernotificationDTOList.add(eventcalendernotificationDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return eventcalendernotificationDTOList;
	
	}
				
}
	