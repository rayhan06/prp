package event_calendar;
import java.util.*; 
import util.*; 


public class EventMembersDTO extends CommonDTO
{

	public long eventCalendarId = -1;
	public long organogramId = -1;
    public String userName = "";
	public long userId = -1;
    public String nameEn = "";
    public String nameBn = "";
    public String phone = "";
    public String email = "";
	
	public List<EventCalenderNotificationDTO> eventCalenderNotificationDTOList = new ArrayList<>();
	public List<EventMembersDTO> eventMembersDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$EventMembersDTO[" +
            " iD = " + iD +
            " eventCalendarId = " + eventCalendarId +
            " organogramId = " + organogramId +
            " userName = " + userName +
            " userId = " + userId +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " phone = " + phone +
            " email = " + email +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}