package event_calendar;
import java.util.*; 
import util.*;


public class Event_calendarMAPS extends CommonMaps
{	
	public Event_calendarMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("nameEn".toLowerCase(), "nameEn".toLowerCase());
		java_DTO_map.put("nameBn".toLowerCase(), "nameBn".toLowerCase());
		java_DTO_map.put("eventCat".toLowerCase(), "eventCat".toLowerCase());
		java_DTO_map.put("eventLocation".toLowerCase(), "eventLocation".toLowerCase());
		java_DTO_map.put("eventDate".toLowerCase(), "eventDate".toLowerCase());
		java_DTO_map.put("eventStartTime".toLowerCase(), "eventStartTime".toLowerCase());
		java_DTO_map.put("eventEndTime".toLowerCase(), "eventEndTime".toLowerCase());
		java_DTO_map.put("eventDescription".toLowerCase(), "eventDescription".toLowerCase());
		java_DTO_map.put("filesDropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_DTO_map.put("isRecurring".toLowerCase(), "isRecurring".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("lastModifierUser".toLowerCase(), "lastModifierUser".toLowerCase());
		java_DTO_map.put("eventActualTime".toLowerCase(), "eventActualTime".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("name_en".toLowerCase(), "nameEn".toLowerCase());
		java_SQL_map.put("name_bn".toLowerCase(), "nameBn".toLowerCase());
		java_SQL_map.put("event_cat".toLowerCase(), "eventCat".toLowerCase());
		java_SQL_map.put("event_location".toLowerCase(), "eventLocation".toLowerCase());
		java_SQL_map.put("event_date".toLowerCase(), "eventDate".toLowerCase());
		java_SQL_map.put("event_start_time".toLowerCase(), "eventStartTime".toLowerCase());
		java_SQL_map.put("event_end_time".toLowerCase(), "eventEndTime".toLowerCase());
		java_SQL_map.put("event_description".toLowerCase(), "eventDescription".toLowerCase());
		java_SQL_map.put("files_dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_SQL_map.put("is_recurring".toLowerCase(), "isRecurring".toLowerCase());
		java_SQL_map.put("event_actual_time".toLowerCase(), "eventActualTime".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("English Name".toLowerCase(), "nameEn".toLowerCase());
		java_Text_map.put("Bangla Name".toLowerCase(), "nameBn".toLowerCase());
		java_Text_map.put("Event".toLowerCase(), "eventCat".toLowerCase());
		java_Text_map.put("Event Location".toLowerCase(), "eventLocation".toLowerCase());
		java_Text_map.put("Event Date".toLowerCase(), "eventDate".toLowerCase());
		java_Text_map.put("Event Start Time".toLowerCase(), "eventStartTime".toLowerCase());
		java_Text_map.put("Event End Time".toLowerCase(), "eventEndTime".toLowerCase());
		java_Text_map.put("Event Description".toLowerCase(), "eventDescription".toLowerCase());
		java_Text_map.put("Files Dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_Text_map.put("Is Recurring".toLowerCase(), "isRecurring".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Last Modifier User".toLowerCase(), "lastModifierUser".toLowerCase());
		java_Text_map.put("Event Actual Time".toLowerCase(), "eventActualTime".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}