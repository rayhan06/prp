package event_calendar;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;
import util.TimeFormat;
import workflow.WorkflowController;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import files.*;



/**
 * Servlet implementation class Event_calendarServlet
 */
@WebServlet("/Event_calendarServlet")
@MultipartConfig
public class Event_calendarServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Event_calendarServlet.class);

    String tableName = "event_calendar";

	Event_calendarDAO event_calendarDAO;
	CommonRequestHandler commonRequestHandler;
	FilesDAO filesDAO = new FilesDAO();
	EventCalenderNotificationDAO eventCalenderNotificationDAO;
	EventMembersDAO eventMembersDAO;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Event_calendarServlet() 
	{
        super();
    	try
    	{
			event_calendarDAO = new Event_calendarDAO(tableName);
			eventCalenderNotificationDAO = new EventCalenderNotificationDAO("event_calender_notification");
			eventMembersDAO = new EventMembersDAO("event_members");
			commonRequestHandler = new CommonRequestHandler(event_calendarDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EVENT_CALENDAR_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EVENT_CALENDAR_UPDATE))
				{
					getEvent_calendar(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("downloadDropzoneFile"))
			{
				commonRequestHandler.downloadDropzoneFile(request, response, filesDAO);					
			}
			else if(actionType.equals("DeleteFileFromDropZone"))
			{
				commonRequestHandler.deleteFileFromDropZone(request, response, filesDAO);
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EVENT_CALENDAR_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchEvent_calendar(request, response, isPermanentTable, filter);
						}
						else
						{
							searchEvent_calendar(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchEvent_calendar(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EVENT_CALENDAR_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EVENT_CALENDAR_ADD))
				{
					System.out.println("going to  addEvent_calendar ");
					addEvent_calendar(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addEvent_calendar ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("UploadFilesFromDropZone"))
			{
				commonRequestHandler.UploadFilesFromDropZone(request, response, userDTO);
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EVENT_CALENDAR_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addEvent_calendar ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EVENT_CALENDAR_UPDATE))
				{					
					addEvent_calendar(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EVENT_CALENDAR_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EVENT_CALENDAR_SEARCH))
				{
					searchEvent_calendar(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Event_calendarDTO event_calendarDTO = event_calendarDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(event_calendarDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addEvent_calendar(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addEvent_calendar");
			String path = getServletContext().getRealPath("/img2/");
			Event_calendarDTO event_calendarDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				event_calendarDTO = new Event_calendarDTO();
			}
			else
			{
				event_calendarDTO = event_calendarDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("nameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEn = " + Value);
			if(Value != null)
			{
				event_calendarDTO.nameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameBn = " + Value);
			if(Value != null)
			{
				event_calendarDTO.nameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("eventCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("eventCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				event_calendarDTO.eventCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("eventLocation");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("eventLocation = " + Value);
			if(Value != null)
			{
				event_calendarDTO.eventLocation = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("eventDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("eventDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try 
				{
					Date d = f.parse(Value);
					event_calendarDTO.eventDate = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("eventStartTime");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("eventStartTime = " + Value);
			if(Value != null)
			{
				event_calendarDTO.eventStartTime = (Value);
				event_calendarDTO.eventActualTime = event_calendarDTO.eventDate + TimeFormat.getMillisFromHourMiunteAMPM(event_calendarDTO.eventStartTime);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("eventEndTime");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("eventEndTime = " + Value);
			if(Value != null)
			{
				event_calendarDTO.eventEndTime = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("eventDescription");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("eventDescription = " + Value);
			if(Value != null)
			{
				event_calendarDTO.eventDescription = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("filesDropzone");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("filesDropzone = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				System.out.println("filesDropzone = " + Value);
				
				event_calendarDTO.filesDropzone = Long.parseLong(Value);
				
				
				if(addFlag == false)
				{
					String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
					String[] deleteArray = filesDropzoneFilesToDelete.split(",");
					for(int i = 0; i < deleteArray.length; i ++)
					{
						System.out.println("going to delete " + deleteArray[i]);
						if(i>0)
						{
							filesDAO.delete(Long.parseLong(deleteArray[i]));
						}
					}
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("isRecurring");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("isRecurring = " + Value);
            event_calendarDTO.isRecurring = Value != null && !Value.equalsIgnoreCase("");

			if(addFlag)
			{
				event_calendarDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				event_calendarDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				event_calendarDTO.insertionDate = c.getTimeInMillis();
			}			


			event_calendarDTO.lastModifierUser = userDTO.userName;

			
			System.out.println("Done adding  addEvent_calendar dto = " + event_calendarDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				event_calendarDAO.setIsDeleted(event_calendarDTO.iD, CommonDTO.OUTDATED);
				returnedID = event_calendarDAO.add(event_calendarDTO);
				event_calendarDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = event_calendarDAO.manageWriteOperations(event_calendarDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = event_calendarDAO.manageWriteOperations(event_calendarDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			List<EventCalenderNotificationDTO> eventCalenderNotificationDTOList = createEventCalenderNotificationDTOListByRequest(request, event_calendarDTO);			
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(eventCalenderNotificationDTOList != null)
				{				
					for(EventCalenderNotificationDTO eventCalenderNotificationDTO: eventCalenderNotificationDTOList)
					{
						eventCalenderNotificationDTO.eventCalendarId = event_calendarDTO.iD; 
						eventCalenderNotificationDAO.add(eventCalenderNotificationDTO);
					}
				}
			
			}
			else
			{
				List<Long> childIdsFromRequest = eventCalenderNotificationDAO.getChildIdsFromRequest(request, "eventCalenderNotification");
				//delete the removed children
				eventCalenderNotificationDAO.deleteChildrenNotInList("event_calendar", "event_calender_notification", event_calendarDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = eventCalenderNotificationDAO.getChilIds("event_calendar", "event_calender_notification", event_calendarDTO.iD);
				
				
				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							EventCalenderNotificationDTO eventCalenderNotificationDTO =  createEventCalenderNotificationDTOByRequestAndIndex(request, false, i, event_calendarDTO);
							eventCalenderNotificationDTO.eventCalendarId = event_calendarDTO.iD; 
							eventCalenderNotificationDAO.update(eventCalenderNotificationDTO);
						}
						else
						{
							EventCalenderNotificationDTO eventCalenderNotificationDTO =  createEventCalenderNotificationDTOByRequestAndIndex(request, true, i, event_calendarDTO);
							eventCalenderNotificationDTO.eventCalendarId = event_calendarDTO.iD; 
							eventCalenderNotificationDAO.add(eventCalenderNotificationDTO);
						}
					}
				}
				else
				{
					eventCalenderNotificationDAO.deleteEventCalenderNotificationByEventCalendarID(event_calendarDTO.iD);
				}
				
			}
			
			List<EventMembersDTO> eventMembersDTOList = createEventMembersDTOListByRequest(request);			
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(eventMembersDTOList != null)
				{				
					for(EventMembersDTO eventMembersDTO: eventMembersDTOList)
					{
						eventMembersDTO.eventCalendarId = event_calendarDTO.iD; 
						eventMembersDAO.add(eventMembersDTO);
					}
				}
			
			}
			else
			{
				List<Long> childIdsFromRequest = eventMembersDAO.getChildIdsFromRequest(request, "eventMembers");
				//delete the removed children
				eventMembersDAO.deleteChildrenNotInList("event_calendar", "event_members", event_calendarDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = eventMembersDAO.getChilIds("event_calendar", "event_members", event_calendarDTO.iD);
				
				
				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							EventMembersDTO eventMembersDTO =  createEventMembersDTOByRequestAndIndex(request, false, i);
							eventMembersDTO.eventCalendarId = event_calendarDTO.iD; 
							eventMembersDAO.update(eventMembersDTO);
						}
						else
						{
							EventMembersDTO eventMembersDTO =  createEventMembersDTOByRequestAndIndex(request, true, i);
							eventMembersDTO.eventCalendarId = event_calendarDTO.iD; 
							eventMembersDAO.add(eventMembersDTO);
						}
					}
				}
				else
				{
					eventMembersDAO.deleteEventMembersByEventCalendarID(event_calendarDTO.iD);
				}
				
			}
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getEvent_calendar(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Event_calendarServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(event_calendarDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	private List<EventCalenderNotificationDTO> createEventCalenderNotificationDTOListByRequest(HttpServletRequest request, Event_calendarDTO event_calendarDTO) throws Exception{ 
		List<EventCalenderNotificationDTO> eventCalenderNotificationDTOList = new ArrayList<EventCalenderNotificationDTO>();
		if(request.getParameterValues("eventCalenderNotification.iD") != null) 
		{
			int eventCalenderNotificationItemNo = request.getParameterValues("eventCalenderNotification.iD").length;
			
			
			for(int index=0;index<eventCalenderNotificationItemNo;index++){
				EventCalenderNotificationDTO eventCalenderNotificationDTO = createEventCalenderNotificationDTOByRequestAndIndex(request,true,index, event_calendarDTO);
				eventCalenderNotificationDTOList.add(eventCalenderNotificationDTO);
			}
			
			return eventCalenderNotificationDTOList;
		}
		return null;
	}
	private List<EventMembersDTO> createEventMembersDTOListByRequest(HttpServletRequest request) throws Exception{ 
		List<EventMembersDTO> eventMembersDTOList = new ArrayList<EventMembersDTO>();
		if(request.getParameterValues("eventMembers.iD") != null) 
		{
			int eventMembersItemNo = request.getParameterValues("eventMembers.iD").length;
			
			
			for(int index=0;index<eventMembersItemNo;index++){
				EventMembersDTO eventMembersDTO = createEventMembersDTOByRequestAndIndex(request,true,index);
				eventMembersDTOList.add(eventMembersDTO);
			}
			
			return eventMembersDTOList;
		}
		return null;
	}
	
	
	private EventCalenderNotificationDTO createEventCalenderNotificationDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index, Event_calendarDTO event_calendarDTO) throws Exception{
	
		EventCalenderNotificationDTO eventCalenderNotificationDTO;
		if(addFlag == true )
		{
			eventCalenderNotificationDTO = new EventCalenderNotificationDTO();
		}
		else
		{
			eventCalenderNotificationDTO = eventCalenderNotificationDAO.getDTOByID(Long.parseLong(request.getParameterValues("eventCalenderNotification.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		
				
		String Value = "";
		Value = request.getParameterValues("eventCalenderNotification.eventCalendarId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		eventCalenderNotificationDTO.eventCalendarId = Long.parseLong(Value);
		Value = request.getParameterValues("eventCalenderNotification.notificationCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		eventCalenderNotificationDTO.notificationCat = Integer.parseInt(Value);
		Value = request.getParameterValues("eventCalenderNotification.occurrenceCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		eventCalenderNotificationDTO.occurrenceCat = Integer.parseInt(Value);
		Value = request.getParameterValues("eventCalenderNotification.occurrence")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		eventCalenderNotificationDTO.occurrence = Integer.parseInt(Value);
		eventCalenderNotificationDTO.notiDateTime = getNotiDateTime(eventCalenderNotificationDTO.occurrenceCat, eventCalenderNotificationDTO.occurrence, event_calendarDTO.eventActualTime);
		Value = request.getParameterValues("eventCalenderNotification.insertedByUserId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		eventCalenderNotificationDTO.insertedByUserId = Long.parseLong(Value);
		Value = request.getParameterValues("eventCalenderNotification.insertedByOrganogramId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		eventCalenderNotificationDTO.insertedByOrganogramId = Long.parseLong(Value);
		Value = request.getParameterValues("eventCalenderNotification.insertionDate")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		eventCalenderNotificationDTO.insertionDate = Long.parseLong(Value);
		Value = request.getParameterValues("eventCalenderNotification.lastModifierUser")[index];


		
		eventCalenderNotificationDTO.eventCalendarId = event_calendarDTO.fileID;
		return eventCalenderNotificationDTO;
	
	}
	
	
	private long getNotiDateTime(int occurrenceCat, int occurrence, long eventActualTime) {
		
		long hourMultiplier = 1;
		if(occurrenceCat == SessionConstants.OCCURRENCE_DAY)
		{
			hourMultiplier= 24;
		}
		
		long millisBefore = hourMultiplier * occurrence * 60000;
		long notiTime = eventActualTime - millisBefore;
		if(notiTime < 0)
		{
			return -1;
		}
		// TODO Auto-generated method stub
		return notiTime;
	}

	private EventMembersDTO createEventMembersDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{
	
		EventMembersDTO eventMembersDTO;
		if(addFlag == true )
		{
			eventMembersDTO = new EventMembersDTO();
		}
		else
		{
			eventMembersDTO = eventMembersDAO.getDTOByID(Long.parseLong(request.getParameterValues("eventMembers.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		
				
		String Value = "";
		Value = request.getParameterValues("eventMembers.eventCalendarId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		eventMembersDTO.eventCalendarId = Long.parseLong(Value);
		Value = request.getParameterValues("eventMembers.organogramId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			eventMembersDTO.organogramId = Long.parseLong(Value);
			UserDTO employeeDTO = UserRepository.getUserDTOByOrganogramID(eventMembersDTO.organogramId);
			if(employeeDTO != null)
			{
				eventMembersDTO.userId = employeeDTO.ID;
			}
			
			eventMembersDTO.nameEn = WorkflowController.getNameFromOrganogramId(eventMembersDTO.organogramId, "English");
			eventMembersDTO.nameBn = WorkflowController.getNameFromOrganogramId(eventMembersDTO.organogramId, "Bangla");
			eventMembersDTO.phone = WorkflowController.getPhoneNumberFromOrganogramId(eventMembersDTO.organogramId);
			eventMembersDTO.email = WorkflowController.getEmailFromOrganogramId(eventMembersDTO.organogramId);
		}

		
		return eventMembersDTO;
	
	}
	
	
	

	private void getEvent_calendar(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getEvent_calendar");
		Event_calendarDTO event_calendarDTO = null;
		try 
		{
			event_calendarDTO = event_calendarDAO.getDTOByID(id);
			request.setAttribute("ID", event_calendarDTO.iD);
			request.setAttribute("event_calendarDTO",event_calendarDTO);
			request.setAttribute("event_calendarDAO",event_calendarDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "event_calendar/event_calendarInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "event_calendar/event_calendarSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "event_calendar/event_calendarEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "event_calendar/event_calendarEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getEvent_calendar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getEvent_calendar(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchEvent_calendar(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchEvent_calendar 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_EVENT_CALENDAR,
			request,
			event_calendarDAO,
			SessionConstants.VIEW_EVENT_CALENDAR,
			SessionConstants.SEARCH_EVENT_CALENDAR,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("event_calendarDAO",event_calendarDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to event_calendar/event_calendarApproval.jsp");
	        	rd = request.getRequestDispatcher("event_calendar/event_calendarApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to event_calendar/event_calendarApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("event_calendar/event_calendarApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to event_calendar/event_calendarSearch.jsp");
	        	rd = request.getRequestDispatcher("event_calendar/event_calendarSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to event_calendar/event_calendarSearchForm.jsp");
	        	rd = request.getRequestDispatcher("event_calendar/event_calendarSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

