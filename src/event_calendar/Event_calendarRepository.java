package event_calendar;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Event_calendarRepository implements Repository {
	Event_calendarDAO event_calendarDAO = null;
	
	public void setDAO(Event_calendarDAO event_calendarDAO)
	{
		this.event_calendarDAO = event_calendarDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Event_calendarRepository.class);
	Map<Long, Event_calendarDTO>mapOfEvent_calendarDTOToiD;
	Map<String, Set<Event_calendarDTO> >mapOfEvent_calendarDTOTonameEn;
	Map<String, Set<Event_calendarDTO> >mapOfEvent_calendarDTOTonameBn;
	Map<Integer, Set<Event_calendarDTO> >mapOfEvent_calendarDTOToeventCat;
	Map<String, Set<Event_calendarDTO> >mapOfEvent_calendarDTOToeventLocation;
	Map<Long, Set<Event_calendarDTO> >mapOfEvent_calendarDTOToeventDate;
	Map<String, Set<Event_calendarDTO> >mapOfEvent_calendarDTOToeventStartTime;
	Map<String, Set<Event_calendarDTO> >mapOfEvent_calendarDTOToeventEndTime;
	Map<String, Set<Event_calendarDTO> >mapOfEvent_calendarDTOToeventDescription;
	Map<Long, Set<Event_calendarDTO> >mapOfEvent_calendarDTOTofilesDropzone;
	Map<Boolean, Set<Event_calendarDTO> >mapOfEvent_calendarDTOToisRecurring;
	Map<Long, Set<Event_calendarDTO> >mapOfEvent_calendarDTOToinsertedByUserId;
	Map<Long, Set<Event_calendarDTO> >mapOfEvent_calendarDTOToinsertedByOrganogramId;
	Map<Long, Set<Event_calendarDTO> >mapOfEvent_calendarDTOToinsertionDate;
	Map<String, Set<Event_calendarDTO> >mapOfEvent_calendarDTOTolastModifierUser;
	Map<Long, Set<Event_calendarDTO> >mapOfEvent_calendarDTOToeventActualTime;
	Map<String, Set<Event_calendarDTO> >mapOfEvent_calendarDTOTosearchColumn;
	Map<Long, Set<Event_calendarDTO> >mapOfEvent_calendarDTOTolastModificationTime;


	static Event_calendarRepository instance = null;  
	private Event_calendarRepository(){
		mapOfEvent_calendarDTOToiD = new ConcurrentHashMap<>();
		mapOfEvent_calendarDTOTonameEn = new ConcurrentHashMap<>();
		mapOfEvent_calendarDTOTonameBn = new ConcurrentHashMap<>();
		mapOfEvent_calendarDTOToeventCat = new ConcurrentHashMap<>();
		mapOfEvent_calendarDTOToeventLocation = new ConcurrentHashMap<>();
		mapOfEvent_calendarDTOToeventDate = new ConcurrentHashMap<>();
		mapOfEvent_calendarDTOToeventStartTime = new ConcurrentHashMap<>();
		mapOfEvent_calendarDTOToeventEndTime = new ConcurrentHashMap<>();
		mapOfEvent_calendarDTOToeventDescription = new ConcurrentHashMap<>();
		mapOfEvent_calendarDTOTofilesDropzone = new ConcurrentHashMap<>();
		mapOfEvent_calendarDTOToisRecurring = new ConcurrentHashMap<>();
		mapOfEvent_calendarDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfEvent_calendarDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfEvent_calendarDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfEvent_calendarDTOTolastModifierUser = new ConcurrentHashMap<>();
		mapOfEvent_calendarDTOToeventActualTime = new ConcurrentHashMap<>();
		mapOfEvent_calendarDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfEvent_calendarDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Event_calendarRepository getInstance(){
		if (instance == null){
			instance = new Event_calendarRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(event_calendarDAO == null)
		{
			return;
		}
		try {
			List<Event_calendarDTO> event_calendarDTOs = event_calendarDAO.getAllEvent_calendar(reloadAll);
			for(Event_calendarDTO event_calendarDTO : event_calendarDTOs) {
				Event_calendarDTO oldEvent_calendarDTO = getEvent_calendarDTOByID(event_calendarDTO.iD);
				if( oldEvent_calendarDTO != null ) {
					mapOfEvent_calendarDTOToiD.remove(oldEvent_calendarDTO.iD);
				
					if(mapOfEvent_calendarDTOTonameEn.containsKey(oldEvent_calendarDTO.nameEn)) {
						mapOfEvent_calendarDTOTonameEn.get(oldEvent_calendarDTO.nameEn).remove(oldEvent_calendarDTO);
					}
					if(mapOfEvent_calendarDTOTonameEn.get(oldEvent_calendarDTO.nameEn).isEmpty()) {
						mapOfEvent_calendarDTOTonameEn.remove(oldEvent_calendarDTO.nameEn);
					}
					
					if(mapOfEvent_calendarDTOTonameBn.containsKey(oldEvent_calendarDTO.nameBn)) {
						mapOfEvent_calendarDTOTonameBn.get(oldEvent_calendarDTO.nameBn).remove(oldEvent_calendarDTO);
					}
					if(mapOfEvent_calendarDTOTonameBn.get(oldEvent_calendarDTO.nameBn).isEmpty()) {
						mapOfEvent_calendarDTOTonameBn.remove(oldEvent_calendarDTO.nameBn);
					}
					
					if(mapOfEvent_calendarDTOToeventCat.containsKey(oldEvent_calendarDTO.eventCat)) {
						mapOfEvent_calendarDTOToeventCat.get(oldEvent_calendarDTO.eventCat).remove(oldEvent_calendarDTO);
					}
					if(mapOfEvent_calendarDTOToeventCat.get(oldEvent_calendarDTO.eventCat).isEmpty()) {
						mapOfEvent_calendarDTOToeventCat.remove(oldEvent_calendarDTO.eventCat);
					}
					
					if(mapOfEvent_calendarDTOToeventLocation.containsKey(oldEvent_calendarDTO.eventLocation)) {
						mapOfEvent_calendarDTOToeventLocation.get(oldEvent_calendarDTO.eventLocation).remove(oldEvent_calendarDTO);
					}
					if(mapOfEvent_calendarDTOToeventLocation.get(oldEvent_calendarDTO.eventLocation).isEmpty()) {
						mapOfEvent_calendarDTOToeventLocation.remove(oldEvent_calendarDTO.eventLocation);
					}
					
					if(mapOfEvent_calendarDTOToeventDate.containsKey(oldEvent_calendarDTO.eventDate)) {
						mapOfEvent_calendarDTOToeventDate.get(oldEvent_calendarDTO.eventDate).remove(oldEvent_calendarDTO);
					}
					if(mapOfEvent_calendarDTOToeventDate.get(oldEvent_calendarDTO.eventDate).isEmpty()) {
						mapOfEvent_calendarDTOToeventDate.remove(oldEvent_calendarDTO.eventDate);
					}
					
					if(mapOfEvent_calendarDTOToeventStartTime.containsKey(oldEvent_calendarDTO.eventStartTime)) {
						mapOfEvent_calendarDTOToeventStartTime.get(oldEvent_calendarDTO.eventStartTime).remove(oldEvent_calendarDTO);
					}
					if(mapOfEvent_calendarDTOToeventStartTime.get(oldEvent_calendarDTO.eventStartTime).isEmpty()) {
						mapOfEvent_calendarDTOToeventStartTime.remove(oldEvent_calendarDTO.eventStartTime);
					}
					
					if(mapOfEvent_calendarDTOToeventEndTime.containsKey(oldEvent_calendarDTO.eventEndTime)) {
						mapOfEvent_calendarDTOToeventEndTime.get(oldEvent_calendarDTO.eventEndTime).remove(oldEvent_calendarDTO);
					}
					if(mapOfEvent_calendarDTOToeventEndTime.get(oldEvent_calendarDTO.eventEndTime).isEmpty()) {
						mapOfEvent_calendarDTOToeventEndTime.remove(oldEvent_calendarDTO.eventEndTime);
					}
					
					if(mapOfEvent_calendarDTOToeventDescription.containsKey(oldEvent_calendarDTO.eventDescription)) {
						mapOfEvent_calendarDTOToeventDescription.get(oldEvent_calendarDTO.eventDescription).remove(oldEvent_calendarDTO);
					}
					if(mapOfEvent_calendarDTOToeventDescription.get(oldEvent_calendarDTO.eventDescription).isEmpty()) {
						mapOfEvent_calendarDTOToeventDescription.remove(oldEvent_calendarDTO.eventDescription);
					}
					
					if(mapOfEvent_calendarDTOTofilesDropzone.containsKey(oldEvent_calendarDTO.filesDropzone)) {
						mapOfEvent_calendarDTOTofilesDropzone.get(oldEvent_calendarDTO.filesDropzone).remove(oldEvent_calendarDTO);
					}
					if(mapOfEvent_calendarDTOTofilesDropzone.get(oldEvent_calendarDTO.filesDropzone).isEmpty()) {
						mapOfEvent_calendarDTOTofilesDropzone.remove(oldEvent_calendarDTO.filesDropzone);
					}
					
					if(mapOfEvent_calendarDTOToisRecurring.containsKey(oldEvent_calendarDTO.isRecurring)) {
						mapOfEvent_calendarDTOToisRecurring.get(oldEvent_calendarDTO.isRecurring).remove(oldEvent_calendarDTO);
					}
					if(mapOfEvent_calendarDTOToisRecurring.get(oldEvent_calendarDTO.isRecurring).isEmpty()) {
						mapOfEvent_calendarDTOToisRecurring.remove(oldEvent_calendarDTO.isRecurring);
					}
					
					if(mapOfEvent_calendarDTOToinsertedByUserId.containsKey(oldEvent_calendarDTO.insertedByUserId)) {
						mapOfEvent_calendarDTOToinsertedByUserId.get(oldEvent_calendarDTO.insertedByUserId).remove(oldEvent_calendarDTO);
					}
					if(mapOfEvent_calendarDTOToinsertedByUserId.get(oldEvent_calendarDTO.insertedByUserId).isEmpty()) {
						mapOfEvent_calendarDTOToinsertedByUserId.remove(oldEvent_calendarDTO.insertedByUserId);
					}
					
					if(mapOfEvent_calendarDTOToinsertedByOrganogramId.containsKey(oldEvent_calendarDTO.insertedByOrganogramId)) {
						mapOfEvent_calendarDTOToinsertedByOrganogramId.get(oldEvent_calendarDTO.insertedByOrganogramId).remove(oldEvent_calendarDTO);
					}
					if(mapOfEvent_calendarDTOToinsertedByOrganogramId.get(oldEvent_calendarDTO.insertedByOrganogramId).isEmpty()) {
						mapOfEvent_calendarDTOToinsertedByOrganogramId.remove(oldEvent_calendarDTO.insertedByOrganogramId);
					}
					
					if(mapOfEvent_calendarDTOToinsertionDate.containsKey(oldEvent_calendarDTO.insertionDate)) {
						mapOfEvent_calendarDTOToinsertionDate.get(oldEvent_calendarDTO.insertionDate).remove(oldEvent_calendarDTO);
					}
					if(mapOfEvent_calendarDTOToinsertionDate.get(oldEvent_calendarDTO.insertionDate).isEmpty()) {
						mapOfEvent_calendarDTOToinsertionDate.remove(oldEvent_calendarDTO.insertionDate);
					}
					
					if(mapOfEvent_calendarDTOTolastModifierUser.containsKey(oldEvent_calendarDTO.lastModifierUser)) {
						mapOfEvent_calendarDTOTolastModifierUser.get(oldEvent_calendarDTO.lastModifierUser).remove(oldEvent_calendarDTO);
					}
					if(mapOfEvent_calendarDTOTolastModifierUser.get(oldEvent_calendarDTO.lastModifierUser).isEmpty()) {
						mapOfEvent_calendarDTOTolastModifierUser.remove(oldEvent_calendarDTO.lastModifierUser);
					}
					
					if(mapOfEvent_calendarDTOToeventActualTime.containsKey(oldEvent_calendarDTO.eventActualTime)) {
						mapOfEvent_calendarDTOToeventActualTime.get(oldEvent_calendarDTO.eventActualTime).remove(oldEvent_calendarDTO);
					}
					if(mapOfEvent_calendarDTOToeventActualTime.get(oldEvent_calendarDTO.eventActualTime).isEmpty()) {
						mapOfEvent_calendarDTOToeventActualTime.remove(oldEvent_calendarDTO.eventActualTime);
					}
					
					if(mapOfEvent_calendarDTOTosearchColumn.containsKey(oldEvent_calendarDTO.searchColumn)) {
						mapOfEvent_calendarDTOTosearchColumn.get(oldEvent_calendarDTO.searchColumn).remove(oldEvent_calendarDTO);
					}
					if(mapOfEvent_calendarDTOTosearchColumn.get(oldEvent_calendarDTO.searchColumn).isEmpty()) {
						mapOfEvent_calendarDTOTosearchColumn.remove(oldEvent_calendarDTO.searchColumn);
					}
					
					if(mapOfEvent_calendarDTOTolastModificationTime.containsKey(oldEvent_calendarDTO.lastModificationTime)) {
						mapOfEvent_calendarDTOTolastModificationTime.get(oldEvent_calendarDTO.lastModificationTime).remove(oldEvent_calendarDTO);
					}
					if(mapOfEvent_calendarDTOTolastModificationTime.get(oldEvent_calendarDTO.lastModificationTime).isEmpty()) {
						mapOfEvent_calendarDTOTolastModificationTime.remove(oldEvent_calendarDTO.lastModificationTime);
					}
					
					
				}
				if(event_calendarDTO.isDeleted == 0) 
				{
					
					mapOfEvent_calendarDTOToiD.put(event_calendarDTO.iD, event_calendarDTO);
				
					if( ! mapOfEvent_calendarDTOTonameEn.containsKey(event_calendarDTO.nameEn)) {
						mapOfEvent_calendarDTOTonameEn.put(event_calendarDTO.nameEn, new HashSet<>());
					}
					mapOfEvent_calendarDTOTonameEn.get(event_calendarDTO.nameEn).add(event_calendarDTO);
					
					if( ! mapOfEvent_calendarDTOTonameBn.containsKey(event_calendarDTO.nameBn)) {
						mapOfEvent_calendarDTOTonameBn.put(event_calendarDTO.nameBn, new HashSet<>());
					}
					mapOfEvent_calendarDTOTonameBn.get(event_calendarDTO.nameBn).add(event_calendarDTO);
					
					if( ! mapOfEvent_calendarDTOToeventCat.containsKey(event_calendarDTO.eventCat)) {
						mapOfEvent_calendarDTOToeventCat.put(event_calendarDTO.eventCat, new HashSet<>());
					}
					mapOfEvent_calendarDTOToeventCat.get(event_calendarDTO.eventCat).add(event_calendarDTO);
					
					if( ! mapOfEvent_calendarDTOToeventLocation.containsKey(event_calendarDTO.eventLocation)) {
						mapOfEvent_calendarDTOToeventLocation.put(event_calendarDTO.eventLocation, new HashSet<>());
					}
					mapOfEvent_calendarDTOToeventLocation.get(event_calendarDTO.eventLocation).add(event_calendarDTO);
					
					if( ! mapOfEvent_calendarDTOToeventDate.containsKey(event_calendarDTO.eventDate)) {
						mapOfEvent_calendarDTOToeventDate.put(event_calendarDTO.eventDate, new HashSet<>());
					}
					mapOfEvent_calendarDTOToeventDate.get(event_calendarDTO.eventDate).add(event_calendarDTO);
					
					if( ! mapOfEvent_calendarDTOToeventStartTime.containsKey(event_calendarDTO.eventStartTime)) {
						mapOfEvent_calendarDTOToeventStartTime.put(event_calendarDTO.eventStartTime, new HashSet<>());
					}
					mapOfEvent_calendarDTOToeventStartTime.get(event_calendarDTO.eventStartTime).add(event_calendarDTO);
					
					if( ! mapOfEvent_calendarDTOToeventEndTime.containsKey(event_calendarDTO.eventEndTime)) {
						mapOfEvent_calendarDTOToeventEndTime.put(event_calendarDTO.eventEndTime, new HashSet<>());
					}
					mapOfEvent_calendarDTOToeventEndTime.get(event_calendarDTO.eventEndTime).add(event_calendarDTO);
					
					if( ! mapOfEvent_calendarDTOToeventDescription.containsKey(event_calendarDTO.eventDescription)) {
						mapOfEvent_calendarDTOToeventDescription.put(event_calendarDTO.eventDescription, new HashSet<>());
					}
					mapOfEvent_calendarDTOToeventDescription.get(event_calendarDTO.eventDescription).add(event_calendarDTO);
					
					if( ! mapOfEvent_calendarDTOTofilesDropzone.containsKey(event_calendarDTO.filesDropzone)) {
						mapOfEvent_calendarDTOTofilesDropzone.put(event_calendarDTO.filesDropzone, new HashSet<>());
					}
					mapOfEvent_calendarDTOTofilesDropzone.get(event_calendarDTO.filesDropzone).add(event_calendarDTO);
					
					if( ! mapOfEvent_calendarDTOToisRecurring.containsKey(event_calendarDTO.isRecurring)) {
						mapOfEvent_calendarDTOToisRecurring.put(event_calendarDTO.isRecurring, new HashSet<>());
					}
					mapOfEvent_calendarDTOToisRecurring.get(event_calendarDTO.isRecurring).add(event_calendarDTO);
					
					if( ! mapOfEvent_calendarDTOToinsertedByUserId.containsKey(event_calendarDTO.insertedByUserId)) {
						mapOfEvent_calendarDTOToinsertedByUserId.put(event_calendarDTO.insertedByUserId, new HashSet<>());
					}
					mapOfEvent_calendarDTOToinsertedByUserId.get(event_calendarDTO.insertedByUserId).add(event_calendarDTO);
					
					if( ! mapOfEvent_calendarDTOToinsertedByOrganogramId.containsKey(event_calendarDTO.insertedByOrganogramId)) {
						mapOfEvent_calendarDTOToinsertedByOrganogramId.put(event_calendarDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfEvent_calendarDTOToinsertedByOrganogramId.get(event_calendarDTO.insertedByOrganogramId).add(event_calendarDTO);
					
					if( ! mapOfEvent_calendarDTOToinsertionDate.containsKey(event_calendarDTO.insertionDate)) {
						mapOfEvent_calendarDTOToinsertionDate.put(event_calendarDTO.insertionDate, new HashSet<>());
					}
					mapOfEvent_calendarDTOToinsertionDate.get(event_calendarDTO.insertionDate).add(event_calendarDTO);
					
					if( ! mapOfEvent_calendarDTOTolastModifierUser.containsKey(event_calendarDTO.lastModifierUser)) {
						mapOfEvent_calendarDTOTolastModifierUser.put(event_calendarDTO.lastModifierUser, new HashSet<>());
					}
					mapOfEvent_calendarDTOTolastModifierUser.get(event_calendarDTO.lastModifierUser).add(event_calendarDTO);
					
					if( ! mapOfEvent_calendarDTOToeventActualTime.containsKey(event_calendarDTO.eventActualTime)) {
						mapOfEvent_calendarDTOToeventActualTime.put(event_calendarDTO.eventActualTime, new HashSet<>());
					}
					mapOfEvent_calendarDTOToeventActualTime.get(event_calendarDTO.eventActualTime).add(event_calendarDTO);
					
					if( ! mapOfEvent_calendarDTOTosearchColumn.containsKey(event_calendarDTO.searchColumn)) {
						mapOfEvent_calendarDTOTosearchColumn.put(event_calendarDTO.searchColumn, new HashSet<>());
					}
					mapOfEvent_calendarDTOTosearchColumn.get(event_calendarDTO.searchColumn).add(event_calendarDTO);
					
					if( ! mapOfEvent_calendarDTOTolastModificationTime.containsKey(event_calendarDTO.lastModificationTime)) {
						mapOfEvent_calendarDTOTolastModificationTime.put(event_calendarDTO.lastModificationTime, new HashSet<>());
					}
					mapOfEvent_calendarDTOTolastModificationTime.get(event_calendarDTO.lastModificationTime).add(event_calendarDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Event_calendarDTO> getEvent_calendarList() {
		List <Event_calendarDTO> event_calendars = new ArrayList<Event_calendarDTO>(this.mapOfEvent_calendarDTOToiD.values());
		return event_calendars;
	}
	
	
	public Event_calendarDTO getEvent_calendarDTOByID( long ID){
		return mapOfEvent_calendarDTOToiD.get(ID);
	}
	
	
	public List<Event_calendarDTO> getEvent_calendarDTOByname_en(String name_en) {
		return new ArrayList<>( mapOfEvent_calendarDTOTonameEn.getOrDefault(name_en,new HashSet<>()));
	}
	
	
	public List<Event_calendarDTO> getEvent_calendarDTOByname_bn(String name_bn) {
		return new ArrayList<>( mapOfEvent_calendarDTOTonameBn.getOrDefault(name_bn,new HashSet<>()));
	}
	
	
	public List<Event_calendarDTO> getEvent_calendarDTOByevent_cat(int event_cat) {
		return new ArrayList<>( mapOfEvent_calendarDTOToeventCat.getOrDefault(event_cat,new HashSet<>()));
	}
	
	
	public List<Event_calendarDTO> getEvent_calendarDTOByevent_location(String event_location) {
		return new ArrayList<>( mapOfEvent_calendarDTOToeventLocation.getOrDefault(event_location,new HashSet<>()));
	}
	
	
	public List<Event_calendarDTO> getEvent_calendarDTOByevent_date(long event_date) {
		return new ArrayList<>( mapOfEvent_calendarDTOToeventDate.getOrDefault(event_date,new HashSet<>()));
	}
	
	
	public List<Event_calendarDTO> getEvent_calendarDTOByevent_start_time(String event_start_time) {
		return new ArrayList<>( mapOfEvent_calendarDTOToeventStartTime.getOrDefault(event_start_time,new HashSet<>()));
	}
	
	
	public List<Event_calendarDTO> getEvent_calendarDTOByevent_end_time(String event_end_time) {
		return new ArrayList<>( mapOfEvent_calendarDTOToeventEndTime.getOrDefault(event_end_time,new HashSet<>()));
	}
	
	
	public List<Event_calendarDTO> getEvent_calendarDTOByevent_description(String event_description) {
		return new ArrayList<>( mapOfEvent_calendarDTOToeventDescription.getOrDefault(event_description,new HashSet<>()));
	}
	
	
	public List<Event_calendarDTO> getEvent_calendarDTOByfiles_dropzone(long files_dropzone) {
		return new ArrayList<>( mapOfEvent_calendarDTOTofilesDropzone.getOrDefault(files_dropzone,new HashSet<>()));
	}
	
	
	public List<Event_calendarDTO> getEvent_calendarDTOByis_recurring(boolean is_recurring) {
		return new ArrayList<>( mapOfEvent_calendarDTOToisRecurring.getOrDefault(is_recurring,new HashSet<>()));
	}
	
	
	public List<Event_calendarDTO> getEvent_calendarDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfEvent_calendarDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Event_calendarDTO> getEvent_calendarDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfEvent_calendarDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Event_calendarDTO> getEvent_calendarDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfEvent_calendarDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Event_calendarDTO> getEvent_calendarDTOBylast_modifier_user(String last_modifier_user) {
		return new ArrayList<>( mapOfEvent_calendarDTOTolastModifierUser.getOrDefault(last_modifier_user,new HashSet<>()));
	}
	
	
	public List<Event_calendarDTO> getEvent_calendarDTOByevent_actual_time(long event_actual_time) {
		return new ArrayList<>( mapOfEvent_calendarDTOToeventActualTime.getOrDefault(event_actual_time,new HashSet<>()));
	}
	
	
	public List<Event_calendarDTO> getEvent_calendarDTOBysearch_column(String search_column) {
		return new ArrayList<>( mapOfEvent_calendarDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
	}
	
	
	public List<Event_calendarDTO> getEvent_calendarDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfEvent_calendarDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "event_calendar";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


