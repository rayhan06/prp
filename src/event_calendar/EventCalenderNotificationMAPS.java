package event_calendar;
import java.util.*; 
import util.*;


public class EventCalenderNotificationMAPS extends CommonMaps
{	
	public EventCalenderNotificationMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("eventCalendarId".toLowerCase(), "eventCalendarId".toLowerCase());
		java_DTO_map.put("notificationCat".toLowerCase(), "notificationCat".toLowerCase());
		java_DTO_map.put("occurrenceCat".toLowerCase(), "occurrenceCat".toLowerCase());
		java_DTO_map.put("occurrence".toLowerCase(), "occurrence".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("lastModifierUser".toLowerCase(), "lastModifierUser".toLowerCase());
		java_DTO_map.put("notiDateTime".toLowerCase(), "notiDateTime".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("event_calendar_id".toLowerCase(), "eventCalendarId".toLowerCase());
		java_SQL_map.put("notification_cat".toLowerCase(), "notificationCat".toLowerCase());
		java_SQL_map.put("occurrence_cat".toLowerCase(), "occurrenceCat".toLowerCase());
		java_SQL_map.put("occurrence".toLowerCase(), "occurrence".toLowerCase());
		java_SQL_map.put("noti_date_time".toLowerCase(), "notiDateTime".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Event Calendar Id".toLowerCase(), "eventCalendarId".toLowerCase());
		java_Text_map.put("Notification".toLowerCase(), "notificationCat".toLowerCase());
		java_Text_map.put("Occurrence".toLowerCase(), "occurrenceCat".toLowerCase());
		java_Text_map.put("Occurrence".toLowerCase(), "occurrence".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Last Modifier User".toLowerCase(), "lastModifierUser".toLowerCase());
		java_Text_map.put("Noti Date Time".toLowerCase(), "notiDateTime".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}