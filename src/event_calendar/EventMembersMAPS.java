package event_calendar;
import java.util.*; 
import util.*;


public class EventMembersMAPS extends CommonMaps
{	
	public EventMembersMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("eventCalendarId".toLowerCase(), "eventCalendarId".toLowerCase());
		java_DTO_map.put("organogramId".toLowerCase(), "organogramId".toLowerCase());
		java_DTO_map.put("userName".toLowerCase(), "userName".toLowerCase());
		java_DTO_map.put("userId".toLowerCase(), "userId".toLowerCase());
		java_DTO_map.put("nameEn".toLowerCase(), "nameEn".toLowerCase());
		java_DTO_map.put("nameBn".toLowerCase(), "nameBn".toLowerCase());
		java_DTO_map.put("phone".toLowerCase(), "phone".toLowerCase());
		java_DTO_map.put("email".toLowerCase(), "email".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("event_calendar_id".toLowerCase(), "eventCalendarId".toLowerCase());
		java_SQL_map.put("organogram_id".toLowerCase(), "organogramId".toLowerCase());
		java_SQL_map.put("user_name".toLowerCase(), "userName".toLowerCase());
		java_SQL_map.put("user_id".toLowerCase(), "userId".toLowerCase());
		java_SQL_map.put("name_en".toLowerCase(), "nameEn".toLowerCase());
		java_SQL_map.put("name_bn".toLowerCase(), "nameBn".toLowerCase());
		java_SQL_map.put("phone".toLowerCase(), "phone".toLowerCase());
		java_SQL_map.put("email".toLowerCase(), "email".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Event Calendar Id".toLowerCase(), "eventCalendarId".toLowerCase());
		java_Text_map.put("Organogram Id".toLowerCase(), "organogramId".toLowerCase());
		java_Text_map.put("User Name".toLowerCase(), "userName".toLowerCase());
		java_Text_map.put("User Id".toLowerCase(), "userId".toLowerCase());
		java_Text_map.put("English Name".toLowerCase(), "nameEn".toLowerCase());
		java_Text_map.put("Bangla Name".toLowerCase(), "nameBn".toLowerCase());
		java_Text_map.put("Phone".toLowerCase(), "phone".toLowerCase());
		java_Text_map.put("Email".toLowerCase(), "email".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}