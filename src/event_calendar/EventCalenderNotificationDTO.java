package event_calendar;
import java.util.*; 
import util.*; 


public class EventCalenderNotificationDTO extends CommonDTO
{

	public long eventCalendarId = -1;
	public int notificationCat = -1;
	public int occurrenceCat = -1;
	public int occurrence = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	public long notiDateTime = -1;
	
	public List<EventCalenderNotificationDTO> eventCalenderNotificationDTOList = new ArrayList<>();
	public List<EventMembersDTO> eventMembersDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$EventCalenderNotificationDTO[" +
            " iD = " + iD +
            " eventCalendarId = " + eventCalendarId +
            " notificationCat = " + notificationCat +
            " occurrenceCat = " + occurrenceCat +
            " occurrence = " + occurrence +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " notiDateTime = " + notiDateTime +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}