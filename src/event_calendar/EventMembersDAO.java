package event_calendar;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class EventMembersDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public EventMembersDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new EventMembersMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"event_calendar_id",
			"organogram_id",
			"user_name",
			"user_id",
			"name_en",
			"name_bn",
			"phone",
			"email",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public EventMembersDAO()
	{
		this("event_members");		
	}
	
	public void setSearchColumn(EventMembersDTO eventmembersDTO)
	{
		eventmembersDTO.searchColumn = "";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		EventMembersDTO eventmembersDTO = (EventMembersDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(eventmembersDTO);
		if(isInsert)
		{
			ps.setObject(index++,eventmembersDTO.iD);
		}
		ps.setObject(index++,eventmembersDTO.eventCalendarId);
		ps.setObject(index++,eventmembersDTO.organogramId);
		ps.setObject(index++,eventmembersDTO.userName);
		ps.setObject(index++,eventmembersDTO.userId);
		ps.setObject(index++,eventmembersDTO.nameEn);
		ps.setObject(index++,eventmembersDTO.nameBn);
		ps.setObject(index++,eventmembersDTO.phone);
		ps.setObject(index++,eventmembersDTO.email);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(EventMembersDTO eventmembersDTO, ResultSet rs) throws SQLException
	{
		eventmembersDTO.iD = rs.getLong("ID");
		eventmembersDTO.eventCalendarId = rs.getLong("event_calendar_id");
		eventmembersDTO.organogramId = rs.getLong("organogram_id");
		eventmembersDTO.userName = rs.getString("user_name");
		eventmembersDTO.userId = rs.getLong("user_id");
		eventmembersDTO.nameEn = rs.getString("name_en");
		eventmembersDTO.nameBn = rs.getString("name_bn");
		eventmembersDTO.phone = rs.getString("phone");
		eventmembersDTO.email = rs.getString("email");
		eventmembersDTO.isDeleted = rs.getInt("isDeleted");
		eventmembersDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}
	
	
	
	
		
	
	public void deleteEventMembersByEventCalendarID(long eventCalendarID) throws Exception{
		
		
		Connection connection = null;
		Statement stmt = null;
		try{
			
			String sql = "UPDATE event_members SET isDeleted=0 WHERE event_calendar_id="+eventCalendarID;			
			logger.debug("sql " + sql);
			connection = DBMW.getInstance().getConnection();
			stmt = connection.createStatement();
			stmt.execute(sql);
		}catch(Exception ex){
			logger.debug("fatal",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMW.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
	}		
   
	public List<EventMembersDTO> getEventMembersDTOListByEventCalendarID(long eventCalendarID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		EventMembersDTO eventmembersDTO = null;
		List<EventMembersDTO> eventmembersDTOList = new ArrayList<>();
		
		try{
			
			String sql = "SELECT * FROM event_members where isDeleted=0 and event_calendar_id="+eventCalendarID+" order by event_members.lastModificationTime";
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);

			while(rs.next()){
				eventmembersDTO = new EventMembersDTO();
				get(eventmembersDTO, rs);
				eventmembersDTOList.add(eventmembersDTO);

			}			
			
		}catch(Exception ex){
			logger.debug("fatal",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return eventmembersDTOList;
	}

	//need another getter for repository
	public EventMembersDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		EventMembersDTO eventmembersDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				eventmembersDTO = new EventMembersDTO();

				get(eventmembersDTO, rs);

			}			
			
			
			
			EventCalenderNotificationDAO eventCalenderNotificationDAO = new EventCalenderNotificationDAO("event_calender_notification");			
			List<EventCalenderNotificationDTO> eventCalenderNotificationDTOList = eventCalenderNotificationDAO.getEventCalenderNotificationDTOListByEventCalendarID(eventmembersDTO.iD);
			eventmembersDTO.eventCalenderNotificationDTOList = eventCalenderNotificationDTOList;
			
			EventMembersDAO eventMembersDAO = new EventMembersDAO("event_members");			
			List<EventMembersDTO> eventMembersDTOList = eventMembersDAO.getEventMembersDTOListByEventCalendarID(eventmembersDTO.iD);
			eventmembersDTO.eventMembersDTOList = eventMembersDTOList;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return eventmembersDTO;
	}
	
	
	
	
	public List<EventMembersDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		EventMembersDTO eventmembersDTO = null;
		List<EventMembersDTO> eventmembersDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return eventmembersDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				eventmembersDTO = new EventMembersDTO();
				get(eventmembersDTO, rs);
				System.out.println("got this DTO: " + eventmembersDTO);
				
				eventmembersDTOList.add(eventmembersDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return eventmembersDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<EventMembersDTO> getAllEventMembers (boolean isFirstReload)
    {
		List<EventMembersDTO> eventmembersDTOList = new ArrayList<>();

		String sql = "SELECT * FROM event_members";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by eventmembers.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				EventMembersDTO eventmembersDTO = new EventMembersDTO();
				get(eventmembersDTO, rs);
				
				eventmembersDTOList.add(eventmembersDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return eventmembersDTOList;
    }

	
	public List<EventMembersDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<EventMembersDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<EventMembersDTO> eventmembersDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				EventMembersDTO eventmembersDTO = new EventMembersDTO();
				get(eventmembersDTO, rs);
				
				eventmembersDTOList.add(eventmembersDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return eventmembersDTOList;
	
	}
				
}
	