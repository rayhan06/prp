package event_calendar;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Event_calendarDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Event_calendarDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Event_calendarMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"event_cat",
			"event_location",
			"event_date",
			"event_start_time",
			"event_end_time",
			"event_description",
			"files_dropzone",
			"is_recurring",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"event_actual_time",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Event_calendarDAO()
	{
		this("event_calendar");		
	}
	
	public void setSearchColumn(Event_calendarDTO event_calendarDTO)
	{
		event_calendarDTO.searchColumn = "";
		event_calendarDTO.searchColumn += event_calendarDTO.nameEn + " ";
		event_calendarDTO.searchColumn += event_calendarDTO.nameBn + " ";
		event_calendarDTO.searchColumn += CatDAO.getName("English", "event", event_calendarDTO.eventCat) + " " + CatDAO.getName("Bangla", "event", event_calendarDTO.eventCat) + " ";
		event_calendarDTO.searchColumn += event_calendarDTO.eventLocation + " ";
		event_calendarDTO.searchColumn += event_calendarDTO.eventDescription + " ";
	
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Event_calendarDTO event_calendarDTO = (Event_calendarDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(event_calendarDTO);
		if(isInsert)
		{
			ps.setObject(index++,event_calendarDTO.iD);
		}
		ps.setObject(index++,event_calendarDTO.nameEn);
		ps.setObject(index++,event_calendarDTO.nameBn);
		ps.setObject(index++,event_calendarDTO.eventCat);
		ps.setObject(index++,event_calendarDTO.eventLocation);
		ps.setObject(index++,event_calendarDTO.eventDate);
		ps.setObject(index++,event_calendarDTO.eventStartTime);
		ps.setObject(index++,event_calendarDTO.eventEndTime);
		ps.setObject(index++,event_calendarDTO.eventDescription);
		ps.setObject(index++,event_calendarDTO.filesDropzone);
		ps.setObject(index++,event_calendarDTO.isRecurring);
		ps.setObject(index++,event_calendarDTO.insertedByUserId);
		ps.setObject(index++,event_calendarDTO.insertedByOrganogramId);
		ps.setObject(index++,event_calendarDTO.insertionDate);
		ps.setObject(index++,event_calendarDTO.lastModifierUser);
		ps.setObject(index++,event_calendarDTO.eventActualTime);
		ps.setObject(index++,event_calendarDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Event_calendarDTO event_calendarDTO, ResultSet rs) throws SQLException
	{
		event_calendarDTO.iD = rs.getLong("ID");
		event_calendarDTO.nameEn = rs.getString("name_en");
		event_calendarDTO.nameBn = rs.getString("name_bn");
		event_calendarDTO.eventCat = rs.getInt("event_cat");
		event_calendarDTO.eventLocation = rs.getString("event_location");
		event_calendarDTO.eventDate = rs.getLong("event_date");
		event_calendarDTO.eventStartTime = rs.getString("event_start_time");
		event_calendarDTO.eventEndTime = rs.getString("event_end_time");
		event_calendarDTO.eventDescription = rs.getString("event_description");
		event_calendarDTO.filesDropzone = rs.getLong("files_dropzone");
		event_calendarDTO.isRecurring = rs.getBoolean("is_recurring");
		event_calendarDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		event_calendarDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
		event_calendarDTO.insertionDate = rs.getLong("insertion_date");
		event_calendarDTO.lastModifierUser = rs.getString("last_modifier_user");
		event_calendarDTO.eventActualTime = rs.getLong("event_actual_time");
		event_calendarDTO.searchColumn = rs.getString("search_column");
		event_calendarDTO.isDeleted = rs.getInt("isDeleted");
		event_calendarDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}
	
	
	
	
		
	

	//need another getter for repository
	public Event_calendarDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Event_calendarDTO event_calendarDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				event_calendarDTO = new Event_calendarDTO();

				get(event_calendarDTO, rs);

			}			
			
			
			
			EventCalenderNotificationDAO eventCalenderNotificationDAO = new EventCalenderNotificationDAO("event_calender_notification");			
			List<EventCalenderNotificationDTO> eventCalenderNotificationDTOList = eventCalenderNotificationDAO.getEventCalenderNotificationDTOListByEventCalendarID(event_calendarDTO.iD);
			event_calendarDTO.eventCalenderNotificationDTOList = eventCalenderNotificationDTOList;
			
			EventMembersDAO eventMembersDAO = new EventMembersDAO("event_members");			
			List<EventMembersDTO> eventMembersDTOList = eventMembersDAO.getEventMembersDTOListByEventCalendarID(event_calendarDTO.iD);
			event_calendarDTO.eventMembersDTOList = eventMembersDTOList;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return event_calendarDTO;
	}
	
	
	
	
	public List<Event_calendarDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Event_calendarDTO event_calendarDTO = null;
		List<Event_calendarDTO> event_calendarDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return event_calendarDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				event_calendarDTO = new Event_calendarDTO();
				get(event_calendarDTO, rs);
				System.out.println("got this DTO: " + event_calendarDTO);
				
				event_calendarDTOList.add(event_calendarDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return event_calendarDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Event_calendarDTO> getAllEvent_calendar (boolean isFirstReload)
    {
		List<Event_calendarDTO> event_calendarDTOList = new ArrayList<>();

		String sql = "SELECT * FROM event_calendar";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by event_calendar.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Event_calendarDTO event_calendarDTO = new Event_calendarDTO();
				get(event_calendarDTO, rs);
				
				event_calendarDTOList.add(event_calendarDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return event_calendarDTOList;
    }

	
	public List<Event_calendarDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Event_calendarDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Event_calendarDTO> event_calendarDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Event_calendarDTO event_calendarDTO = new Event_calendarDTO();
				get(event_calendarDTO, rs);
				
				event_calendarDTOList.add(event_calendarDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return event_calendarDTOList;
	
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name_en")
						|| str.equals("name_bn")
						|| str.equals("event_cat")
						|| str.equals("event_location")
						|| str.equals("event_date_start")
						|| str.equals("event_date_end")
						|| str.equals("event_description")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".name_en like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("name_bn"))
					{
						AllFieldSql += "" + tableName + ".name_bn like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("event_cat"))
					{
						AllFieldSql += "" + tableName + ".event_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("event_location"))
					{
						AllFieldSql += "" + tableName + ".event_location like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("event_date_start"))
					{
						AllFieldSql += "" + tableName + ".event_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("event_date_end"))
					{
						AllFieldSql += "" + tableName + ".event_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("event_description"))
					{
						AllFieldSql += "" + tableName + ".event_description like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
		if(userDTO.roleID != SessionConstants.ADMIN_ROLE && userDTO.roleID != SessionConstants.TICKET_ADMIN_ROLE)
		{
			sql += " AND event_calendar.inserted_by_organogram_id = " +   userDTO.organogramID;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	