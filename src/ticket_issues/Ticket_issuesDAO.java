package ticket_issues;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Ticket_issuesDAO implements CommonDAOService<Ticket_issuesDTO> {

    private final Logger logger = Logger.getLogger(Ticket_issuesDAO.class);

    private final Map<String,String> searchMap = new HashMap<>();

    private final String addQuery;

    private final String updateQuery;

    private static class Holder{
        static final Ticket_issuesDAO INSTANCE = new Ticket_issuesDAO();
    }

    public static Ticket_issuesDAO getInstance(){
        return Holder.INSTANCE;
    }

    private Ticket_issuesDAO() {
        searchMap.put("admin_organogram_id"," and (admin_organogram_id = ?)");
        searchMap.put("name_en", " and (name_en like ?)");
        searchMap.put("name_bn", " and (name_bn like ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
        String[] columnNames = new String[]
                {
                        "name_en",
                        "name_bn",
                        "search_column",
                        "admin_user_name",
                        "admin_organogram_id",
                        "lastModificationTime",
                        "isDeleted",
                        "ID"
                };
        addQuery = getAddQuery2(columnNames);
        updateQuery = getUpdateQuery2(columnNames);
    }

    public Ticket_issuesDTO build(ResultSet rs) {
        try {
            Ticket_issuesDTO ticket_issuesDTO = new Ticket_issuesDTO();
            ticket_issuesDTO.iD = rs.getLong("ID");
            ticket_issuesDTO.nameEn = rs.getString("name_en");
            ticket_issuesDTO.nameBn = rs.getString("name_bn");
            ticket_issuesDTO.searchColumn = rs.getString("search_column");
            ticket_issuesDTO.adminUserName = rs.getString("admin_user_name");
            ticket_issuesDTO.adminOrganogramId = rs.getLong("admin_organogram_id");
            ticket_issuesDTO.isDeleted = rs.getInt("isDeleted");
            ticket_issuesDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return ticket_issuesDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public void set(PreparedStatement ps, Ticket_issuesDTO ticket_issuesDTO, boolean isInsert) throws SQLException {
        if(ticket_issuesDTO.lastModificationTime == 0){
            ticket_issuesDTO.lastModificationTime = System.currentTimeMillis();
        }
        ticket_issuesDTO.searchColumn = ticket_issuesDTO.nameEn + " "+ticket_issuesDTO.nameBn;
        int index = 0;
        ps.setObject(++index, ticket_issuesDTO.nameEn);
        ps.setObject(++index, ticket_issuesDTO.nameBn);
        ps.setObject(++index, ticket_issuesDTO.searchColumn);
        ps.setObject(++index, ticket_issuesDTO.adminUserName);
        ps.setObject(++index, ticket_issuesDTO.adminOrganogramId);
        ps.setObject(++index, ticket_issuesDTO.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, ticket_issuesDTO.iD);
    }

    @Override
    public Ticket_issuesDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Ticket_issuesDTO ticket_issuesDTO = new Ticket_issuesDTO();
            ticket_issuesDTO.iD = rs.getLong("ID");
            ticket_issuesDTO.nameEn = rs.getString("name_en");
            ticket_issuesDTO.nameBn = rs.getString("name_bn");
            ticket_issuesDTO.searchColumn = rs.getString("search_column");
            ticket_issuesDTO.adminUserName = rs.getString("admin_user_name");
            ticket_issuesDTO.adminOrganogramId = rs.getLong("admin_organogram_id");
            ticket_issuesDTO.isDeleted = rs.getInt("isDeleted");
            ticket_issuesDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return ticket_issuesDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "ticket_issues";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Ticket_issuesDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Ticket_issuesDTO) commonDTO, updateQuery, false);
    }
}
	