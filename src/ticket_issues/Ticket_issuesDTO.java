package ticket_issues;

import annotation.NotEmpty;
import util.CommonDTO;

import java.util.ArrayList;
import java.util.List;


public class Ticket_issuesDTO extends CommonDTO {
    @NotEmpty(engError = "Please Insert English Name",bngError = "অনুগ্রহকরে ইংরেজীতে নাম লিখুন")
    public String nameEn = "";

    @NotEmpty(engError = "Please Insert Bangla Name",bngError = "অনুগ্রহকরে বাংলায় নাম লিখুন")
    public String nameBn = "";
    public String adminUserName = "";

    public long adminOrganogramId = -1;
    
    public static final int NOC_ISSUE = 1000;
    public static final int ESERVICE = 900;

    public List<TicketIssuesSubtypeDTO> ticketIssuesSubtypeDTOList = new ArrayList<>();

    @Override
    public String toString() {
        return "$Ticket_issuesDTO[" +
                " iD = " + iD +
                " nameEn = " + nameEn +
                " nameBn = " + nameBn +
                " searchColumn = " + searchColumn +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}