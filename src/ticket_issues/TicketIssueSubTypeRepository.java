package ticket_issues;

/*
 * @author Md. Erfan Hossain
 * @created 15/12/2021 - 2:01 PM
 * @project parliament
 */


import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class TicketIssueSubTypeRepository implements Repository {
    private final Logger logger = Logger.getLogger(TicketIssueSubTypeRepository.class);
    private final TicketIssuesSubtypeDAO dao = TicketIssuesSubtypeDAO.getInstance();
    private final Map<Long,TicketIssuesSubtypeDTO> mapById = new ConcurrentHashMap<>();
    private final List<TicketIssuesSubtypeDTO> allDTOList = new ArrayList<>();

    private static class Holder{
        static final TicketIssueSubTypeRepository INSTANCE = new TicketIssueSubTypeRepository();
    }

    public static TicketIssueSubTypeRepository getInstance(){
        return Holder.INSTANCE;
    }

    private TicketIssueSubTypeRepository(){
        RepositoryManager.getInstance().addRepository(this);
    }

    @Override
    public void reload(boolean reloadAll) {
        logger.debug("Start reloading TicketIssueSubTypeRepository reloadAll : "+reloadAll);
        List<TicketIssuesSubtypeDTO> list = dao.getAllDTOs(reloadAll);
        list.forEach(e->{
            TicketIssuesSubtypeDTO dto = mapById.get(e.iD);
            if(dto != null){
                mapById.remove(e.iD);
                allDTOList.remove(dto);
            }
            if(e.isDeleted == 0){
                mapById.put(e.iD,e);
                allDTOList.add(e);
            }
        });
        logger.debug("End reloading TicketIssueSubTypeRepository reloadAll : "+reloadAll);
    }

    public TicketIssuesSubtypeDTO getById(long id){
        if(id <=0){
            return null;
        }
        if(mapById.get(id) == null){
            synchronized (LockManager.getLock("TIST_"+id)){
                if(mapById.get(id) == null){
                    TicketIssuesSubtypeDTO dto = dao.getDTOFromID(id);
                    if(dto!=null){
                        mapById.put(dto.iD,dto);
                        allDTOList.add(dto);
                    }
                }
            }
        }
        return mapById.get(id);
    }

    @Override
    public String getTableName() {
        return dao.getTableName();
    }

    public List<TicketIssuesSubtypeDTO> getByTicketIssuesId(long ticketIssuesId){
        return allDTOList.stream()
                .filter(e->e.ticketIssuesId == ticketIssuesId)
                .collect(Collectors.toList());
    }

    public String getText(long id,boolean isLangEng){
        TicketIssuesSubtypeDTO dto = getById(id);
        return dto == null ? "": (isLangEng?dto.nameEn : dto.nameBn);
    }

    public String buildOptions(String language,long ticketIssuesId, long selectedId) {
        List<OptionDTO> optionDTOList = null;
        List<TicketIssuesSubtypeDTO> list = getByTicketIssuesId(ticketIssuesId);
        if (list.size() > 0) {
            optionDTOList = list.stream()
                    .map(e -> new OptionDTO(e.nameEn, e.nameBn, String.valueOf(e.iD)))
                    .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, String.valueOf(selectedId));
    }
}
