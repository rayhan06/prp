package ticket_issues;

import java.util.*;

import util.*;


public class TicketIssuesSubtypeDTO extends CommonDTO {

    public long ticketIssuesId = -1;
    public String nameEn = "";
    public String nameBn = "";
    public String eserviceCats = "";
    public List<Long> eserviceCatsArray = null;


    public List<TicketIssuesSubtypeDTO> ticketIssuesSubtypeDTOList = new ArrayList<>();

    @Override
    public String toString() {
        return "$TicketIssuesSubtypeDTO[" +
                " iD = " + iD +
                " ticketIssuesId = " + ticketIssuesId +
                " nameEn = " + nameEn +
                " nameBn = " + nameBn +

                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}