package ticket_issues;

import org.apache.log4j.Logger;
import pb.CategoryLanguageModel;
import pb.OptionDTO;
import pb.OrderByEnum;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@SuppressWarnings({"unused"})
public class Ticket_issuesRepository implements Repository {
	private final Ticket_issuesDAO ticket_issuesDAO = Ticket_issuesDAO.getInstance();
	private final Logger logger = Logger.getLogger(Ticket_issuesRepository.class);
	private final Map<Long, Ticket_issuesDTO>mapById;
	private final List<Ticket_issuesDTO> allDTOList = new ArrayList<>();

	private Ticket_issuesRepository(){
		mapById = new ConcurrentHashMap<>();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class Holder{
		final static Ticket_issuesRepository INSTANCE = new Ticket_issuesRepository();
	}
	public static Ticket_issuesRepository getInstance(){
		return Holder.INSTANCE;
	}

	public void reload(boolean reloadAll){
		logger.debug("Start reloading Ticket_issuesRepository reloadAll : "+reloadAll);
		List<Ticket_issuesDTO> list = ticket_issuesDAO.getAllDTOs(reloadAll);
		list.forEach(e->{
			Ticket_issuesDTO dto = mapById.get(e.iD);
			if(dto != null){
				allDTOList.remove(dto);
				mapById.remove(e.iD);
			}
			if(e.isDeleted == 0){
				mapById.put(e.iD,e);
				allDTOList.add(e);
			}
		});
		logger.debug("Start reloading Ticket_issuesRepository reloadAll : "+reloadAll);
	}
	
	public List<Ticket_issuesDTO> getTicket_issuesList() {
		return allDTOList;
	}

	public Ticket_issuesDTO getById( long id){
		if(mapById.get(id) == null){
			synchronized (LockManager.getLock("TISS_"+id)){
				if(mapById.get(id) == null){
					Ticket_issuesDTO dto = ticket_issuesDAO.getDTOFromID(id);
					if(dto!=null){
						allDTOList.add(dto);
						mapById.put(dto.iD,dto);
					}
				}
			}
		}
		return mapById.get(id);
	}

	public String getText(long id,boolean isLangEng){
		Ticket_issuesDTO dto = getById(id);
		return dto == null ? "" : (isLangEng ? dto.nameEn : dto.nameBn);
	}
	
	@Override
	public String getTableName() {
		return ticket_issuesDAO.getTableName();
	}

	public String buildOptions(String language, long selectedId) {
		List<OptionDTO> optionDTOList = null;
		if (allDTOList.size() > 0) {
			optionDTOList = allDTOList.stream()
					.map(e -> new OptionDTO(e.nameEn, e.nameBn, String.valueOf(e.iD)))
					.sorted((o1, o2) -> {
						long i1 = Long.parseLong(o1.value);
						long i2 = Long.parseLong(o2.value);
						return Long.compare(i1, i2);
					})
					.collect(Collectors.toList());
		}
		return Utils.buildOptions(optionDTOList, language, String.valueOf(selectedId));
	}
}