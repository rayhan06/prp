package ticket_issues;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import pb.Utils;

import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TicketIssuesSubtypeDAO implements CommonDAOService<TicketIssuesSubtypeDTO> {

    private final Logger logger = Logger.getLogger(TicketIssuesSubtypeDAO.class);
    private final Map<String,String> searchMap = new HashMap<>();
    private final String addQuery;
    private final String updateQuery;

    private static class Holder{
        final static TicketIssuesSubtypeDAO INSTANCE = new TicketIssuesSubtypeDAO();
    }

    public static TicketIssuesSubtypeDAO getInstance(){
        return Holder.INSTANCE;
    }

    private TicketIssuesSubtypeDAO() {
        String []columnNames = new String[]
                {
                        "ticket_issues_id",
                        "name_en",
                        "name_bn",
                        "eservice_cats",
                        "lastModificationTime",
                        "isDeleted",
                        "ID"
                };
        addQuery = getAddQuery2(columnNames);
        updateQuery = getUpdateQueryWithDeletedUpdate(columnNames);
    }

    @Override
    public void set(PreparedStatement ps, TicketIssuesSubtypeDTO ticketIssuesSubtypeDTO, boolean isInsert) throws SQLException {
        int index = 0;
        if(ticketIssuesSubtypeDTO.lastModificationTime == 0){
            ticketIssuesSubtypeDTO.lastModificationTime = System.currentTimeMillis();
        }
        ps.setObject(++index, ticketIssuesSubtypeDTO.ticketIssuesId);
        ps.setObject(++index, ticketIssuesSubtypeDTO.nameEn);
        ps.setObject(++index, ticketIssuesSubtypeDTO.nameBn);
        ps.setObject(++index, ticketIssuesSubtypeDTO.eserviceCats);
        ps.setObject(++index, ticketIssuesSubtypeDTO.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, 0);
        }else{
            ps.setObject(++index,ticketIssuesSubtypeDTO.isDeleted);
        }
        ps.setObject(++index, ticketIssuesSubtypeDTO.iD);
    }

    @Override
    public TicketIssuesSubtypeDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            TicketIssuesSubtypeDTO ticketissuessubtypeDTO = new TicketIssuesSubtypeDTO();
            ticketissuessubtypeDTO.iD = rs.getLong("ID");
            ticketissuessubtypeDTO.ticketIssuesId = rs.getLong("ticket_issues_id");
            ticketissuessubtypeDTO.nameEn = rs.getString("name_en");
            ticketissuessubtypeDTO.nameBn = rs.getString("name_bn");
            ticketissuessubtypeDTO.eserviceCats = rs.getString("eservice_cats"); 
            ticketissuessubtypeDTO.isDeleted = rs.getInt("isDeleted");
            ticketissuessubtypeDTO.lastModificationTime = rs.getLong("lastModificationTime");
            
            ticketissuessubtypeDTO.eserviceCatsArray = Utils.StringToArrayList(ticketissuessubtypeDTO.eserviceCats);
            return ticketissuessubtypeDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "ticket_issues_subtype";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((TicketIssuesSubtypeDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((TicketIssuesSubtypeDTO) commonDTO, updateQuery, false);
    }
}
	