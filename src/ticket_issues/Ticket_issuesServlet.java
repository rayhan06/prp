package ticket_issues;

import common.BaseServlet;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import common.ConnectionType;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Servlet implementation class Ticket_issuesServlet
 */
@WebServlet("/Ticket_issuesServlet")
@MultipartConfig
public class Ticket_issuesServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Ticket_issuesServlet.class);
    private final Ticket_issuesDAO ticket_issuesDAO = Ticket_issuesDAO.getInstance();
    private final TicketIssuesSubtypeDAO ticketIssuesSubtypeDAO = TicketIssuesSubtypeDAO.getInstance();

    @Override
    public String getTableName() {
        return ticket_issuesDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Ticket_issuesServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return ticket_issuesDAO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.TICKET_ISSUES_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.TICKET_ISSUES_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.TICKET_ISSUES_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Ticket_issuesServlet.class;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
    	
    	logger.debug("addT called");
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Ticket_issuesDTO ticket_issuesDTO;
        long lastModificationTime = System.currentTimeMillis() + 20000;
        if(addFlag){
            ticket_issuesDTO = new Ticket_issuesDTO();
        }else {
            ticket_issuesDTO = ticket_issuesDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        ticket_issuesDTO.lastModificationTime = lastModificationTime;
        validateAndSet(ticket_issuesDTO, request, isLangEng);
        ticket_issuesDTO.adminOrganogramId = Long.parseLong(request.getParameter("adminOrganogramId"));
        UserDTO adminOrganogramUserDTO = UserRepository.getUserDTOByOrganogramID(ticket_issuesDTO.adminOrganogramId);
        if(adminOrganogramUserDTO == null){
            throw new Exception(isLangEng?"Please select correct assign to user":"অনুগ্রহ করে সঠিক অ্যাসাইন কর্মকর্তা বাছাই করুন");
        }
        ticket_issuesDTO.adminUserName = adminOrganogramUserDTO.userName;
        Connection connection = null;
        try{
            connection = ConnectionAndStatementUtil.getConnection(ConnectionType.WRITE);
            connection.setAutoCommit(false);
            CommonDAOService.CONNECTION_THREAD_LOCAL.set(connection);
            List<TicketIssuesSubtypeDTO> ticketIssuesSubtypeDTOList = createTicketIssuesSubtypeDTOListByRequest(request,isLangEng);
            if(addFlag){
                ticket_issuesDAO.add(ticket_issuesDTO);
            }else {
                List<TicketIssuesSubtypeDTO> dtoList = TicketIssueSubTypeRepository.getInstance().getByTicketIssuesId(ticket_issuesDTO.iD);
                if (request.getParameterValues("ticketIssuesSubtype.iD") != null){
                    Set<Long> ids = Stream.of(request.getParameterValues("ticketIssuesSubtype.iD"))
                            .map(Long::parseLong)
                            .filter(e->e>0)
                            .collect(Collectors.toSet());
                    List<TicketIssuesSubtypeDTO> deletedList = dtoList.stream()
                            .filter(e->!ids.contains(e.iD))
                            .peek(dto-> dto.isDeleted=1)
                            .collect(Collectors.toList());
                    if(deletedList.size()>0){
                        if(ticketIssuesSubtypeDTOList == null){
                            ticketIssuesSubtypeDTOList = deletedList;
                        }else{
                            ticketIssuesSubtypeDTOList.addAll(deletedList);
                        }
                    }
                }
                ticket_issuesDAO.update(ticket_issuesDTO);
            }
            if (ticketIssuesSubtypeDTOList != null) {
                for (TicketIssuesSubtypeDTO dto : ticketIssuesSubtypeDTOList) {
                    dto.ticketIssuesId = ticket_issuesDTO.iD;
                    dto.lastModificationTime = lastModificationTime;
                    if(dto.iD == -1){
                        ticketIssuesSubtypeDAO.add(dto);
                    }else{
                        ticketIssuesSubtypeDAO.update(dto);
                    }
                }
            }
            connection.commit();
            connection.setAutoCommit(true);
        }catch (Exception ex){
            if(connection!=null){
                connection.rollback();
                connection.setAutoCommit(true);
            }
            ex.printStackTrace();
            throw ex;
        }finally {
            CommonDAOService.CONNECTION_THREAD_LOCAL.remove();
            if(connection!=null){
                connection.close();
            }
        }
        
        Ticket_issuesRepository.getInstance().reload(false);
        TicketIssueSubTypeRepository.getInstance().reload(false);

        return null;
    }

    private List<TicketIssuesSubtypeDTO> createTicketIssuesSubtypeDTOListByRequest(HttpServletRequest request, boolean isLangEng) throws Exception {
    	logger.debug("createTicketIssuesSubtypeDTOListByRequest");
        List<TicketIssuesSubtypeDTO> dtoList = new ArrayList<>();
        if (request.getParameterValues("ticketIssuesSubtype.iD") != null) {
            int degreeExamItemNo = request.getParameterValues("ticketIssuesSubtype.iD").length;
            for (int index = 0; index < degreeExamItemNo; index++) {
                TicketIssuesSubtypeDTO dto = createTicketIssuesSubtypeDTOByRequestAndIndex(request, index,isLangEng);
                if(dto!=null){
                    dtoList.add(dto);
                }
            }
            return dtoList;
        }
        return null;
    }

    private TicketIssuesSubtypeDTO createTicketIssuesSubtypeDTOByRequestAndIndex(HttpServletRequest request, int index,boolean isLangEng) throws Exception {
    	logger.debug("createTicketIssuesSubtypeDTOByRequestAndIndex");
        TicketIssuesSubtypeDTO dto;
        long id = Long.parseLong(request.getParameterValues("ticketIssuesSubtype.iD")[index]);
        String nameBn = Jsoup.clean(request.getParameterValues("ticketIssuesSubtype.nameBn")[index], Whitelist.simpleText());
        if(nameBn == null || nameBn.trim().length() == 0){
            throw new Exception(isLangEng ? "Bangla name for ticket issue subtype is missing" : "টিকেট ইস্যুর সাবটাইপের বাংলা নাম পাওয়া যায়নি");
        }
        String nameEn = Jsoup.clean(request.getParameterValues("ticketIssuesSubtype.nameEn")[index], Whitelist.simpleText());
        if(nameEn == null || nameEn.trim().length() == 0){
            throw new Exception(isLangEng ? "English name for ticket issue subtype is missing" : "টিকেট ইস্যুর সাবটাইপের ইংরেজীর নাম পাওয়া যায়নি");
        }
        if(id == -1){
            dto = new TicketIssuesSubtypeDTO();
        }else{
            dto = TicketIssueSubTypeRepository.getInstance().getById(id);
            if(dto == null){
                throw new Exception(isLangEng ? "Ticket issue subtype is not found" : "টিকেট ইস্যুর সাবটাইপ পাওয়া যায়নি");
            }
            
        }
        dto.nameBn = nameBn;
        dto.nameEn = nameEn;
        if(request.getParameterValues("ticketIssuesSubtype.eserviceCats") != null)
        {
        	dto.eserviceCats = Jsoup.clean(request.getParameterValues("ticketIssuesSubtype.eserviceCats")[index], Whitelist.simpleText());
        	logger.debug("dto.eserviceCats = " + dto.eserviceCats);
        }
        else
        {
        	logger.debug("dto.eserviceCats is not found");
        }
        
        return dto;
    }
}

