package office_building;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;

import building.BuildingBlockDTO;
import building.BuildingBlockRepository;
import building.BuildingDTO;
import building.BuildingRepository;
import util.*;
import workflow.WorkflowController;
import pb.*;

public class Office_buildingDAO  implements CommonDAOService<Office_buildingDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Office_buildingDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"office_units_id",
			"building_type",
			"building_block_type",
			"building_level",
			"room_no",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("building_type"," and (building_type = ?)");
		searchMap.put("building_block_type"," and (building_block_type = ?)");
		searchMap.put("room_no"," and (room_no like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Office_buildingDAO INSTANCE = new Office_buildingDAO();
	}

	public static Office_buildingDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Office_buildingDTO office_buildingDTO)
	{
		office_buildingDTO.searchColumn = "";
		office_buildingDTO.searchColumn += CommonDAO.getName("English", "building", office_buildingDTO.buildingType) + " " + CommonDAO.getName("Bangla", "building", office_buildingDTO.buildingType) + " ";
		office_buildingDTO.searchColumn += CommonDAO.getName("English", "building_block", office_buildingDTO.buildingBlockType) + " " + CommonDAO.getName("Bangla", "building_block", office_buildingDTO.buildingBlockType) + " ";
		office_buildingDTO.searchColumn += office_buildingDTO.roomNo + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Office_buildingDTO office_buildingDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		if(office_buildingDTO.lastModificationTime<=0){
			office_buildingDTO.lastModificationTime =  System.currentTimeMillis();
		}
		setSearchColumn(office_buildingDTO);
		if(isInsert)
		{
			ps.setObject(++index,office_buildingDTO.iD);
		}
		ps.setObject(++index,office_buildingDTO.officeUnitsId);
		ps.setObject(++index,office_buildingDTO.buildingType);
		ps.setObject(++index,office_buildingDTO.buildingBlockType);
		ps.setObject(++index,office_buildingDTO.buildingLevel);
		ps.setObject(++index,office_buildingDTO.roomNo);
		ps.setObject(++index,office_buildingDTO.insertedByUserId);
		ps.setObject(++index,office_buildingDTO.insertedByOrganogramId);
		ps.setObject(++index,office_buildingDTO.insertionDate);
		ps.setObject(++index,office_buildingDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(++index,office_buildingDTO.isDeleted);
		}
		ps.setObject(++index, office_buildingDTO.lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,office_buildingDTO.iD);
		}
	}
	
	@Override
	public Office_buildingDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Office_buildingDTO office_buildingDTO = new Office_buildingDTO();
			int i = 0;
			office_buildingDTO.iD = rs.getLong(columnNames[i++]);
			office_buildingDTO.officeUnitsId = rs.getLong(columnNames[i++]);
			office_buildingDTO.buildingType = rs.getLong(columnNames[i++]);
			office_buildingDTO.buildingBlockType = rs.getLong(columnNames[i++]);
			office_buildingDTO.buildingLevel = rs.getInt(columnNames[i++]);
			office_buildingDTO.roomNo = rs.getString(columnNames[i++]);
			office_buildingDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			office_buildingDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			office_buildingDTO.insertionDate = rs.getLong(columnNames[i++]);
			office_buildingDTO.lastModifierUser = rs.getString(columnNames[i++]);
			office_buildingDTO.isDeleted = rs.getInt(columnNames[i++]);
			office_buildingDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			
			BuildingDTO buildingDTO = BuildingRepository.getInstance().getBuildingDTOByiD(office_buildingDTO.buildingType);
			if(buildingDTO != null)
			{
				office_buildingDTO.nameEn = buildingDTO.nameEn + ", Level-" +  office_buildingDTO.buildingLevel;
				office_buildingDTO.nameBn = buildingDTO.nameBn + ", লেভেল-" +  
						Utils.getDigits(office_buildingDTO.buildingLevel, "bangla");
				
				BuildingBlockDTO buildingBlockDTO = BuildingBlockRepository.getInstance().getBuildingBlockDTOByiD(office_buildingDTO.buildingBlockType);
				if(buildingBlockDTO != null)
				{
					office_buildingDTO.nameEn +=  ", " + buildingBlockDTO.nameEn ;
					office_buildingDTO.nameBn +=  ", " + buildingBlockDTO.nameBn ;
				}
				
				if(!office_buildingDTO.roomNo.equalsIgnoreCase(""))
				{
					office_buildingDTO.nameEn +=  ", room-" + Utils.getDigits(office_buildingDTO.roomNo, "english");
					office_buildingDTO.nameBn +=  ", কক্ষ-" + Utils.getDigits(office_buildingDTO.roomNo, "bangla");
				}
				
			}
			return office_buildingDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	public List<Office_buildingDTO> getByOfficeId (long officeUnitsId)
	{
		
		String sql = "select * from office_building where isDeleted = 0 "
				+ " and office_units_id = " + officeUnitsId;
		return ConnectionAndStatementUtil.getListOfT(sql,this::buildObjectFromResultSet);	
	}
		
	public Office_buildingDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "office_building";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Office_buildingDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Office_buildingDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
				
}
	