package office_building;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;


public class Office_buildingRepository implements Repository {
    Office_buildingDAO office_buildingDAO = null;

    static Logger logger = Logger.getLogger(Office_buildingRepository.class);
    Map<Long, Office_buildingDTO> mapOfOffice_buildingDTOToiD;
    Map<Long, Set<Office_buildingDTO>> mapOfOffice_buildingDTOToofficeUnitsId;
    Gson gson;


    private Office_buildingRepository() {
        office_buildingDAO = Office_buildingDAO.getInstance();
        mapOfOffice_buildingDTOToiD = new ConcurrentHashMap<>();
        mapOfOffice_buildingDTOToofficeUnitsId = new ConcurrentHashMap<>();
        gson = new Gson();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Office_buildingRepository INSTANCE = new Office_buildingRepository();
    }

    public static Office_buildingRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    long ourOfficeId = 2101;

    public void reload(boolean reloadAll) {
        try {
            List<Office_buildingDTO> office_buildingDTOs;
            if (reloadAll) {
                mapOfOffice_buildingDTOToiD = new ConcurrentHashMap<>();
                mapOfOffice_buildingDTOToofficeUnitsId = new ConcurrentHashMap<>();
                office_buildingDTOs = office_buildingDAO.getAllDTOs(reloadAll);
            } else {
                office_buildingDTOs = office_buildingDAO.getAllDTOsByLastModificationTimeGreaterThan(System.currentTimeMillis() - 60 * 1000);
            }
            reloadCache(office_buildingDTOs, reloadAll);
        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }
	
	public String getName(long id, boolean isLangEng)
	{
		Office_buildingDTO office_buildingDTO = getOffice_buildingDTOByiD(id);
		if(office_buildingDTO== null)
		{
			return "";
		}
		return isLangEng?office_buildingDTO.nameEn:office_buildingDTO.nameBn;
	}
	
	public String getOptions(long officeUnitsId, boolean isLangEng)
	{
		String options = "";
		List<Office_buildingDTO> office_buildingDTOs = getOffice_buildingDTOByofficeUnitsId(officeUnitsId);
		if(office_buildingDTOs.isEmpty())
		{
			return "<option value = '-1'></option>";
		}

		for(Office_buildingDTO office_buildingDTO: office_buildingDTOs)
		{
			options += "<option value ='" + office_buildingDTO.iD + "'>"
					+ (isLangEng? office_buildingDTO.nameEn: office_buildingDTO.nameBn)
					+ "</option>";

		}
		return options;
	}

    @Override
    public void reloadWithExactModificationTime(long time) {
        List<Office_buildingDTO> office_buildingDTOs = office_buildingDAO.getAllDTOsExactLastModificationTime(time);
        reloadCache(office_buildingDTOs, false);
    }

    private void reloadCache(List<Office_buildingDTO> office_buildingDTOs, boolean reloadAll) {
        logger.debug("office_buildingDTOssize = " + office_buildingDTOs.size());
        int ourOfficeCount = 0;
        int ourOfficeCountNotDeleted = 0;

        for (Office_buildingDTO office_buildingDTO : office_buildingDTOs) {
            if (office_buildingDTO.officeUnitsId == ourOfficeId) {
                ourOfficeCount++;
                if (office_buildingDTO.isDeleted == 0) {
                    ourOfficeCountNotDeleted++;
                }
            }
            Office_buildingDTO oldOffice_buildingDTO = getOffice_buildingDTOByiD(office_buildingDTO.iD);
            if (oldOffice_buildingDTO != null) {
                mapOfOffice_buildingDTOToiD.remove(oldOffice_buildingDTO.iD);

                if (mapOfOffice_buildingDTOToofficeUnitsId.containsKey(oldOffice_buildingDTO.officeUnitsId)) {
                    mapOfOffice_buildingDTOToofficeUnitsId.get(oldOffice_buildingDTO.officeUnitsId).remove(oldOffice_buildingDTO);
                }
                if (mapOfOffice_buildingDTOToofficeUnitsId.get(oldOffice_buildingDTO.officeUnitsId).isEmpty()) {
                    mapOfOffice_buildingDTOToofficeUnitsId.remove(oldOffice_buildingDTO.officeUnitsId);
                }

            }
            if (office_buildingDTO.isDeleted == 0) {

                mapOfOffice_buildingDTOToiD.put(office_buildingDTO.iD, office_buildingDTO);

                if (!mapOfOffice_buildingDTOToofficeUnitsId.containsKey(office_buildingDTO.officeUnitsId)) {
                    mapOfOffice_buildingDTOToofficeUnitsId.put(office_buildingDTO.officeUnitsId, new HashSet<>());
                }
                mapOfOffice_buildingDTOToofficeUnitsId.get(office_buildingDTO.officeUnitsId).add(office_buildingDTO);

            }
        }
        logger.debug("just after reload:" + reloadAll + " ourOfficeCount = " + ourOfficeCount
                + " ourOfficeCountNotDeleted = " + ourOfficeCountNotDeleted
                + " hashTable entry count = "
                + (mapOfOffice_buildingDTOToofficeUnitsId.get(ourOfficeId) == null ? "null" : mapOfOffice_buildingDTOToofficeUnitsId.get(ourOfficeId).size()));
    }

    public Office_buildingDTO clone(Office_buildingDTO dto) {
        String raw = gson.toJson(dto);
        return gson.fromJson(raw, Office_buildingDTO.class);
    }


    public List<Office_buildingDTO> getOffice_buildingList() {
        List<Office_buildingDTO> office_buildings = new ArrayList<Office_buildingDTO>(this.mapOfOffice_buildingDTOToiD.values());
        return office_buildings;
    }

    public List<Office_buildingDTO> copyOffice_buildingList() {
        List<Office_buildingDTO> office_buildings = getOffice_buildingList();
        return office_buildings
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }


    public Office_buildingDTO getOffice_buildingDTOByiD(long iD) {
        return mapOfOffice_buildingDTOToiD.get(iD);
    }

    public Office_buildingDTO copyOffice_buildingDTOByiD(long iD) {
        return clone(mapOfOffice_buildingDTOToiD.get(iD));
    }


    public List<Office_buildingDTO> getOffice_buildingDTOByofficeUnitsId(long officeUnitsId) {
        return new ArrayList<>(mapOfOffice_buildingDTOToofficeUnitsId.getOrDefault(officeUnitsId, new HashSet<>()));
    }

    public String getNames(long officeUnitsId, boolean isLangEng) {
        String name = "";
        List<Office_buildingDTO> office_buildingDTOs = getOffice_buildingDTOByofficeUnitsId(officeUnitsId);
        int i = 1;
        for (Office_buildingDTO office_buildingDTO : office_buildingDTOs) {
            if (isLangEng) {
                name += "<b>" + i + "</b>. " + office_buildingDTO.nameEn + "<br>";
            } else {
                name += "<b>" + Utils.getDigits(i, "bangla") + "</b>. "
                        + office_buildingDTO.nameBn + "<br>";
            }
            i++;
        }
        return name;
    }

    public List<Office_buildingDTO> copyOffice_buildingDTOByofficeUnitsId(long officeUnitsId) {
        List<Office_buildingDTO> office_buildings = getOffice_buildingDTOByofficeUnitsId(officeUnitsId);
        return office_buildings
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }


    @Override
    public String getTableName() {
        return office_buildingDAO.getTableName();
    }
}


