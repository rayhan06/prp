package office_building;
import util.*; 


public class Office_buildingDTO extends CommonDTO
{

	public long officeUnitsId = -1;
	public long buildingType = -1;
	public long buildingBlockType = -1;
	public int buildingLevel = -1;
    public String roomNo = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
    
    public String nameEn = "";
    public String nameBn = "";
	
	
    @Override
	public String toString() {
            return "$Office_buildingDTO[" +
            " iD = " + iD +
            " officeUnitsId = " + officeUnitsId +
            " buildingType = " + buildingType +
            " buildingBlockType = " + buildingBlockType +
            " buildingLevel = " + buildingLevel +
            " roomNo = " + roomNo +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}