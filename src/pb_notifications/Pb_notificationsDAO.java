package pb_notifications;

import common.CommonDTOService;
import common.ConnectionAndStatementUtil;
import files.FilesDAO;
import files.FilesDTO;
import login.LoginDTO;
import mail.EmailAttachment;
import mail.EmailService;
import mail.SendEmailDTO;
import org.apache.log4j.Logger;
import org.eclipse.jetty.util.log.Log;

import pbReport.DateUtils;
import sessionmanager.SessionConstants;
import sms.SmsSender;
import sms.SmsService;
import sms_log.Sms_logDAO;
import sms_log.Sms_logDTO;
import util.NavigationService;
import workflow.WorkflowController;

import java.sql.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings({"unchecked", "unused", "rawtypes"})
public class Pb_notificationsDAO implements NavigationService, CommonDTOService<Pb_notificationsDTO> {

    private static final Logger logger = Logger.getLogger(Pb_notificationsDAO.class);
    private static final String[] addColumns = {"is_seen", "is_hidden", "source", "destination", "from_id", "to_id", "text", "url", "entry_date", "seen_date",
            "showing_date", "send_alarm", "send_sms", "send_mail", "send_push", "mail_sent", "sms_sent", "push_sent",
            "to_role", "to_user_name", "isDeleted", "lastModificationTime", "ID"};
    private static final String getByToUserIDSql = "SELECT * FROM pb_notifications WHERE showing_date <= %d and to_id= %d and destination = %d order by id des";
    
    private static final String toSql = "(to_user_name= %d and destination = %d) "
    		+ "or "
    		+ "(to_role= %d and destination = %d) "
    		+ "or "
    		+ "(to_id= %d and destination = %d)" 
    		+ "or "
    		+ "(to_id= %d and to_role= %d and destination = %d)";
    
    private static final String getByUserIdRoleIdOrganogramId = "SELECT * FROM pb_notifications WHERE showing_date <= %d and ( " +
    		toSql +
            ") order by id desc";
    private static final String getTotalUnseenCountSQL = "SELECT count(*) FROM pb_notifications WHERE showing_date <= %d and ( " +
    		toSql +
            ") and is_seen = 0";
    private static final String initialFetchNotificationSQL = "SELECT * FROM pb_notifications WHERE showing_date <= %d and ( " +
    		toSql +
            ") order by id desc limit " + SessionConstants.INITIAL_FETCH_NOTIFICATION_COUNT;
    private static final String nextFetchNotificationSQL = "SELECT * FROM pb_notifications WHERE id < %d and ( " +
    		toSql +
            ") order by id desc limit " + SessionConstants.NEXT_FETCH_NOTIFICATION_COUNT;
    private static final String topUnFetchNotificationSQL = "SELECT * FROM pb_notifications WHERE id > %d and ( " +
    		toSql +
            ") order by id desc";
    private static final String updateSeenSql = "UPDATE pb_notifications SET is_seen = true , seen_date = %d where ID = %d";
    private static final String getAllIdSql = "SELECT ID FROM pb_notifications WHERE isDeleted = 0  order by ID desc";
    private static final String addQuery;
    private static final String updateQuery;
    private static Pb_notificationsDAO INSTANCE = null;
    
    public static String urlPrefix = "https://prp.parliament.gov.bd";

    private Pb_notificationsDAO(){
    }

    public static Pb_notificationsDAO getInstance(){
        if(INSTANCE == null){
            synchronized (Pb_notificationsDAO.class){
                if(INSTANCE == null){
                    INSTANCE = new Pb_notificationsDAO();
                }
            }
        }
        return INSTANCE;
    }

    static {
        String firstPart = Stream.of(addColumns)
                .collect(Collectors.joining(",", "INSERT INTO {tableName} (", ")"));
        String[] questionMarks = new String[addColumns.length];
        Arrays.fill(questionMarks, "?");
        String secondPart = Stream.of(questionMarks)
                .collect(Collectors.joining(",", "VALUES (", ")"));
        addQuery = firstPart + secondPart;

        String[] updateColumns = Arrays.copyOfRange(addColumns, 0, addColumns.length - 1);
        String updateClause = Stream.of(updateColumns)
                .map(e -> e + "=?")
                .collect(Collectors.joining(","));

        updateQuery = "UPDATE pb_notifications SET " + updateClause + "WHERE ID = ?";
    }

    public void set(PreparedStatement ps, Pb_notificationsDTO pb_notificationsDTO, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setObject(++index, pb_notificationsDTO.isSeen);
        ps.setObject(++index, pb_notificationsDTO.isHidden);
        ps.setObject(++index, pb_notificationsDTO.source);
        ps.setObject(++index, pb_notificationsDTO.destination);
        ps.setObject(++index, pb_notificationsDTO.fromId);
        ps.setObject(++index, pb_notificationsDTO.toId);
        ps.setObject(++index, pb_notificationsDTO.text);
        ps.setObject(++index, pb_notificationsDTO.url);
        ps.setObject(++index, pb_notificationsDTO.entryDate);
        ps.setObject(++index, pb_notificationsDTO.seenDate);
        ps.setObject(++index, pb_notificationsDTO.showingDate);
        ps.setObject(++index, pb_notificationsDTO.sendAlarm);
        ps.setObject(++index, pb_notificationsDTO.sendSms);
        ps.setObject(++index, pb_notificationsDTO.sendMail);
        ps.setObject(++index, pb_notificationsDTO.sendPush);
        ps.setObject(++index, pb_notificationsDTO.mailSent);
        ps.setObject(++index, pb_notificationsDTO.smsSent);
        ps.setObject(++index, pb_notificationsDTO.pushSent);
        ps.setObject(++index, pb_notificationsDTO.toRole);
        ps.setObject(++index, pb_notificationsDTO.toUserName);
        ps.setObject(++index, pb_notificationsDTO.isDeleted);
        ps.setObject(++index, System.currentTimeMillis());
        ps.setObject(++index, pb_notificationsDTO.iD);
    }

    @Override
    public Pb_notificationsDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Pb_notificationsDTO pb_notificationsDTO = new Pb_notificationsDTO();
            pb_notificationsDTO.iD = rs.getLong("ID");
            pb_notificationsDTO.isSeen = rs.getBoolean("is_seen");
            pb_notificationsDTO.isHidden = rs.getBoolean("is_hidden");
            pb_notificationsDTO.source = rs.getInt("source");
            pb_notificationsDTO.destination = rs.getInt("destination");
            pb_notificationsDTO.fromId = rs.getLong("from_id");
            pb_notificationsDTO.toId = rs.getLong("to_id");
            pb_notificationsDTO.text = rs.getString("text");
            pb_notificationsDTO.url = rs.getString("url");
            pb_notificationsDTO.entryDate = rs.getLong("entry_date");
            pb_notificationsDTO.seenDate = rs.getLong("seen_date");
            pb_notificationsDTO.showingDate = rs.getLong("showing_date");
            pb_notificationsDTO.sendAlarm = rs.getBoolean("send_alarm");
            pb_notificationsDTO.sendSms = rs.getBoolean("send_sms");
            pb_notificationsDTO.sendMail = rs.getBoolean("send_mail");
            pb_notificationsDTO.sendPush = rs.getBoolean("send_push");
            pb_notificationsDTO.mailSent = rs.getBoolean("mail_sent");
            pb_notificationsDTO.smsSent = rs.getBoolean("sms_sent");
            pb_notificationsDTO.pushSent = rs.getBoolean("push_sent");
            pb_notificationsDTO.toRole = rs.getLong("to_role");
            pb_notificationsDTO.toUserName = rs.getLong("to_user_name");
            pb_notificationsDTO.isDeleted = rs.getInt("isDeleted");
            return pb_notificationsDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "pb_notifications";
    }

    private void recordUpdateTime(Connection connection, long lastModificationTime) {
        recordUpdateTime(connection, "pb_notifications", lastModificationTime);
    }

    public long addPb_notifications(Pb_notificationsDTO pb_notificationsDTO) {
        try {
            return executeAddOrUpdateQuery(pb_notificationsDTO, addQuery, true);
        } catch (Exception e) {
            logger.error(e);
            return -1;
        }
    }

    public long addPb_notifications(Pb_notificationsDTO pb_notificationsDTO, boolean doAudit) {
        try {
            return executeAddOrUpdateQuery(pb_notificationsDTO, addQuery, true, doAudit);
        } catch (Exception e) {
            logger.error(e);
            return -1;
        }
    }

    public long addPb_notifications(long userID, long toID, long toRole, int destination, long showing_date, String URL, String text,
                                    boolean sendMail, boolean sendSms, boolean sendPush) {
        return addPb_notifications(userID, toID, toRole, destination, showing_date, URL, text,
               sendMail, sendSms, sendPush, false);
    }

    public long addPb_notifications(long userID, long toID, long toRole, int destination, long showing_date, String URL, String text,
                                    boolean sendMail, boolean sendSms, boolean sendPush, boolean doAudit) {
        Pb_notificationsDTO pb_notificationsDTO = new Pb_notificationsDTO();
        pb_notificationsDTO.fromId = userID;
        pb_notificationsDTO.toId = toID;
        pb_notificationsDTO.toRole = toRole;
        pb_notificationsDTO.destination = destination;
        pb_notificationsDTO.url = URL;
        pb_notificationsDTO.text = text;
        pb_notificationsDTO.entryDate = DateUtils.getToday12AM();
        pb_notificationsDTO.showingDate = showing_date;
        pb_notificationsDTO.sendMail = sendMail;
        pb_notificationsDTO.sendSms = sendSms;
        pb_notificationsDTO.sendPush = sendPush;
        return addPb_notifications(pb_notificationsDTO, doAudit);
    }

    public void sendSms(long toID, String text) {
        ExecutorService executorService = Executors.newSingleThreadExecutor(r -> {
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            return thread;
        });
        executorService.execute(() -> {
            try {
                SmsService.send(WorkflowController.getPhoneNumberFromOrganogramId(toID), text);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
    
    public void sendSms(String phoneNumber, String text) {
        ExecutorService executorService = Executors.newSingleThreadExecutor(r -> {
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            return thread;
        });
        executorService.execute(() -> {
            try {
                SmsService.send(phoneNumber, text);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void sendMail(long organogramId, String subject, String text, long filesDropzone, byte[] template) {
        String[] mailIds = new String[1];
        try {
            mailIds[0] = WorkflowController.getEmailFromOrganogramId(organogramId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        sendMail(mailIds, subject, text, filesDropzone, template);
    }

    public void sendMail(long[] organogramIds, String subject, String text, long filesDropzone, byte[] template) {
        String[] mailIds = new String[organogramIds.length];

        int i = 0;
        for (long organogramId : organogramIds) {
            try {
                mailIds[i] = WorkflowController.getEmailFromOrganogramId(organogramId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            i++;
        }
        sendMail(mailIds, subject, text, filesDropzone, template);
    }

    public void sendMail(String[] to, String subject, String text, long filesDropzone, byte[] template) {
        ExecutorService executorService = Executors.newSingleThreadExecutor(r -> {
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            return thread;
        });
        executorService.execute(() -> {
            List<EmailAttachment> emailAttachments = null;
            if (filesDropzone != -1) {
                try {
                    emailAttachments = new FilesDAO().getDTOsByFileID(filesDropzone)
                            .stream()
                            .map(this::buildEmailAttachment)
                            .collect(Collectors.toList());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (template != null) {
                if (emailAttachments == null) {
                    emailAttachments = new ArrayList<>();
                }
                EmailAttachment emailAttachment = new EmailAttachment();
                emailAttachment.setName("template.pdf");
                emailAttachment.setData(template);
                emailAttachment.setContentType("application/pdf");
                emailAttachments.add(0, emailAttachment);
            } else {
                logger.debug("Null template");
            }
            SendEmailDTO sendEmailDTO = new SendEmailDTO();
            sendEmailDTO.setFrom("edms@pbrlp.gov.bd");
            sendEmailDTO.setTo(to);
            sendEmailDTO.setSubject(subject);
            sendEmailDTO.setText(text);
            sendEmailDTO.setAttachments(emailAttachments);
            if (to != null && to.length != 0 && !to[0].equalsIgnoreCase("")) {
                //new EmailService().sendMail(sendEmailDTO); //No email sending until a valid username password is provided
            }

        });
    }

    private EmailAttachment buildEmailAttachment(FilesDTO filesDTO) {
        EmailAttachment emailAttachment = new EmailAttachment();
        emailAttachment.setName(filesDTO.fileTitle);
        emailAttachment.setData(filesDTO.fileBlob);
        emailAttachment.setContentType(filesDTO.fileTypes);
        return emailAttachment;
    }
    
    public void addPb_notificationsAndSendMailSMS(long toID, long showing_date, String URL,
            String englishText, String banglaText, String mailSubject, String phone)
    {
    	addPb_notificationsAndSendMailSMS(toID, showing_date, URL,
                englishText, banglaText, mailSubject, phone, -1);
    }
    
    public void addPb_notificationsAndSendMailSMS(long toID, long showing_date, String URL,
            String englishText, String banglaText, String mailSubject, String phone, long toRole, boolean isEnglishSMS, boolean sendSMS)
    {
    	if (toID == -1) {
            return;
        }
        String notiText = englishText + "$" + banglaText;
        if(toRole == -1)
        {
        	addPb_notifications(toID, showing_date, URL, notiText);
        }
        else
        {
        	addPb_notifications(toID, toRole, showing_date, URL, notiText);
        }
        
        ExecutorService executorService = Executors.newSingleThreadExecutor(r -> {
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            return thread;
        });
        executorService.execute(() -> {
        	if(sendSMS)
        	{
        		if(isEnglishSMS)
            	{
            		 if (phone == null || phone.equals("")) {
                         sendSms(toID, englishText + ".");
                     } else {
                         sendSms(phone, englishText + ".");
                     }
            	}
            	else
            	{
            		 if (phone == null || phone.equals("")) {
                         sendSms(toID, banglaText );
                     } else {
                         sendSms(phone, banglaText );
                     }
            	}
        	}
        	
           

            try {
                String mailBody = "Dear " + WorkflowController.getNameFromOrganogramId(toID, "English") + ",<br>";
                mailBody += englishText + ".<br>";
                mailBody += "Click <a href = '" + urlPrefix  + "/" + URL + "'>here</a> for details";
                logger.debug("mail url: " + urlPrefix + "/" + URL);
                sendMail(toID, mailSubject, mailBody, -1, null);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
        });
    }
 

    public void addPb_notificationsAndSendMailSMS(long toID, long showing_date, String URL,
                                                  String englishText, String banglaText, String mailSubject, String phone, long toRole) {
    	addPb_notificationsAndSendMailSMS(toID, showing_date, URL,
                englishText, banglaText, mailSubject, phone, toRole, true, false);
    }

    public void addPb_notificationsAndSendMailSMS(long toID, long showing_date, String URL,
                                                  String englishText, String banglaText, String mailSubject) {
        addPb_notificationsAndSendMailSMS(toID, showing_date, URL,
                englishText, banglaText, mailSubject, "");
    }
    
    public void addPb_notificationsAndSendMailSMS(long toID, long showing_date, String URL,
            String englishText, String banglaText, String mailSubject, long toRole) {
			addPb_notificationsAndSendMailSMS(toID, showing_date, URL,
			englishText, banglaText, mailSubject, "", toRole);
	}
    
    public long addPb_notifications(long toID, long showing_date, String URL, String text)
    {
    	return addPb_notifications(-1, toID, -1, SessionConstants.NOTIFICATION_DESTINATION_ORGANOGRAM, showing_date, URL, text,
                true, true, true);
    }
    
    public long addPb_notifications(long toID, long toRole, long showing_date, String URL, String text)
    {
    	return addPb_notifications(-1, toID, toRole, SessionConstants.NOTIFICATION_DESTINATION_ORGANOGRAM_AND_ROLE, showing_date, URL, text,
                true, true, true);
    }

 

    public long addPb_notifications(long toID, long showing_date, String URL, String text, boolean doAudit) {
        return addPb_notifications(-1, toID, -1, SessionConstants.NOTIFICATION_DESTINATION_ORGANOGRAM, showing_date, URL, text,
                true, true, true, doAudit);
    }

    public Pb_notificationsDTO getPb_notificationsDTOByID(long ID) {
        return getDTOFromIdDeletedOrNot(ID);
    }

    public List<Pb_notificationsDTO> getPb_notificationsDTOsBytoUserID(long toID, int destination) {
        String sql = String.format(getByToUserIDSql, DateUtils.getToday12AM(), toID, destination);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public List<Pb_notificationsDTO> getPb_notificationsDTOsBytoUserID(long toUserID, long toRoleID, long toOrganogramID) {
        String sql = String.format(getByUserIdRoleIdOrganogramId, System.currentTimeMillis(), toUserID, SessionConstants.NOTIFICATION_DESTINATION_USER,
                toRoleID, SessionConstants.NOTIFICATION_DESTINATION_ROLE, toOrganogramID, SessionConstants.NOTIFICATION_DESTINATION_ORGANOGRAM,
                toOrganogramID, toRoleID, SessionConstants.NOTIFICATION_DESTINATION_ORGANOGRAM_AND_ROLE);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public List<Pb_notificationsDTO> initialFetchNotification(long toUserID, long toRoleID, long toOrganogramID) {
        String sql = String.format(initialFetchNotificationSQL, System.currentTimeMillis(), toUserID, SessionConstants.NOTIFICATION_DESTINATION_USER,
                toRoleID, SessionConstants.NOTIFICATION_DESTINATION_ROLE, toOrganogramID, SessionConstants.NOTIFICATION_DESTINATION_ORGANOGRAM,
                toOrganogramID, toRoleID, SessionConstants.NOTIFICATION_DESTINATION_ORGANOGRAM_AND_ROLE);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public List<Pb_notificationsDTO> nextFetchNotification(long lastId, long toUserID, long toRoleID, long toOrganogramID) {
        String sql = String.format(nextFetchNotificationSQL, lastId, toUserID, SessionConstants.NOTIFICATION_DESTINATION_USER,
                toRoleID, SessionConstants.NOTIFICATION_DESTINATION_ROLE, toOrganogramID, SessionConstants.NOTIFICATION_DESTINATION_ORGANOGRAM,
                toOrganogramID, toRoleID, SessionConstants.NOTIFICATION_DESTINATION_ORGANOGRAM_AND_ROLE);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public List<Pb_notificationsDTO> topUnFetchNotificationSQL(long firstId, long toUserID, long toRoleID, long toOrganogramID) {
        String sql = String.format(topUnFetchNotificationSQL, firstId, toUserID, SessionConstants.NOTIFICATION_DESTINATION_USER,
                toRoleID, SessionConstants.NOTIFICATION_DESTINATION_ROLE, toOrganogramID, SessionConstants.NOTIFICATION_DESTINATION_ORGANOGRAM,
                toOrganogramID, toRoleID, SessionConstants.NOTIFICATION_DESTINATION_ORGANOGRAM_AND_ROLE);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public long getTotalUnseenCount(long toUserID, long toRoleID, long toOrganogramID) {
        String sql = String.format(getTotalUnseenCountSQL, System.currentTimeMillis(), toUserID, SessionConstants.NOTIFICATION_DESTINATION_USER,
                toRoleID, SessionConstants.NOTIFICATION_DESTINATION_ROLE, toOrganogramID, SessionConstants.NOTIFICATION_DESTINATION_ORGANOGRAM,
                toOrganogramID, toRoleID, SessionConstants.NOTIFICATION_DESTINATION_ORGANOGRAM_AND_ROLE);
        Long count = ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getLong(1);
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            }
        });
        return count == null ? 0L : count;
    }

    public void seePb_notification(long id) {
        String sql = String.format(updateSeenSql, DateUtils.getToday12AM(), id);
        Thread thread = new Thread(() -> ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            try {
                Statement st = model.getStatement();
                st.executeUpdate(sql);
                recordUpdateTime(model.getConnection(), System.currentTimeMillis());
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }));
        thread.setDaemon(true);
        thread.start();
    }

    public void updatePb_notifications(Pb_notificationsDTO pb_notificationsDTO) {
        try {
            executeAddOrUpdateQuery(pb_notificationsDTO, updateQuery, false);
        } catch (Exception e) {
            logger.error(e);
        }
    }

    public void deletePb_notificationsByID(long ID) {
        deleteById2(ID, System.currentTimeMillis());
    }

    public List<Pb_notificationsDTO> getDTOs(Collection recordIDs, LoginDTO loginDTO) {
        return getDTOs((List<Long>) recordIDs);
    }

    public Collection getIDs(LoginDTO loginDTO) {
        return ConnectionAndStatementUtil.getListOfT(getAllIdSql, rs -> {
            try {
                return rs.getString("ID");
            } catch (SQLException ex) {
                logger.error(ex);
                return null;
            }
        });
    }

    public List<Pb_notificationsDTO> getAllPb_notifications(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }

    public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO) throws Exception {
        String sql = "SELECT distinct pb_notifications.ID as ID FROM pb_notifications ";

        Enumeration names = p_searchCriteria.keys();
        String str, value;

        StringBuilder AnyfieldSql = new StringBuilder("(");

        if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
            int i = 0;
            for (Map.Entry<String, String> stringStringEntry : Pb_notificationsMAPS.GetInstance().java_anyfield_search_map.entrySet()) {
                if (i > 0) {
                    AnyfieldSql.append(" OR  ");
                }
                AnyfieldSql.append(((Map.Entry) stringStringEntry).getKey()).append(" like '%").append(p_searchCriteria.get("AnyField").toString()).append("%'");
                i++;
            }
        }
        AnyfieldSql.append(")");
        logger.debug("AnyfieldSql = " + AnyfieldSql);

        StringBuilder AllFieldSql = new StringBuilder("(");
        int i = 0;
        while (names.hasMoreElements()) {
            str = (String) names.nextElement();
            value = (String) p_searchCriteria.get(str);
            logger.debug(str + ": " + value);
            if (Pb_notificationsMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()) != null && !Pb_notificationsMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
                    && !str.equalsIgnoreCase("AnyField")
                    && value != null && !value.equalsIgnoreCase("")) {
                if (p_searchCriteria.get(str).equals("any")) {
                    continue;
                }

                if (i > 0) {
                    AllFieldSql.append(" AND  ");
                }
                if (Pb_notificationsMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equals("String")) {
                    AllFieldSql.append("pb_notifications.").append(str.toLowerCase()).append(" like '%").append(p_searchCriteria.get(str)).append("%'");
                } else {
                    AllFieldSql.append("pb_notifications.").append(str.toLowerCase()).append(" = '").append(p_searchCriteria.get(str)).append("'");
                }
                i++;
            }
        }

        AllFieldSql.append(")");

        logger.debug("AllFieldSql = " + AllFieldSql);


        sql += " WHERE ";
        sql += " pb_notifications.isDeleted = 0";


        if (!AnyfieldSql.toString().equals("()")) {
            sql += " AND " + AnyfieldSql;

        }
        if (!AllFieldSql.toString().equals("()")) {
            sql += " AND " + AllFieldSql;
        }

        sql += " order by pb_notifications.ID desc ";
        return ConnectionAndStatementUtil.getListOfT(sql, rs -> {
            try {
                return rs.getLong("ID");
            } catch (SQLException ex) {
                logger.error(ex);
                return null;
            }
        });
    }
}