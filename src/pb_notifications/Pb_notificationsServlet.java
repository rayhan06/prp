package pb_notifications;

import com.google.gson.Gson;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pb.Utils;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.RecordNavigationManager;
import util.StringUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Servlet implementation class Pb_notificationsServlet
 */
@WebServlet("/Pb_notificationsServlet")
@MultipartConfig
public class Pb_notificationsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Pb_notificationsServlet.class);

    public Pb_notificationsServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getAddPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PB_NOTIFICATIONS_ADD)) {
                        getAddPage(request, response);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "getEditPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PB_NOTIFICATIONS_UPDATE)) {
                        getPb_notifications(request, response);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "getURL":
                    String URL = request.getParameter("URL");
                    logger.debug("URL = " + URL);
                    response.sendRedirect(URL);
                    break;
                case "search":
                    logger.debug("search requested");
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PB_NOTIFICATIONS_SEARCH)) {
                        searchPb_notifications(request, response);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "nextFetchNotification":
                    String language = request.getParameter("language");
                    List<Pb_notificationsDTO> dtoList = (List<Pb_notificationsDTO>) request.getSession(true).getAttribute(SessionConstants.USER_NOTIFICATION);
                    if(dtoList!=null && dtoList.size()>0){
                        List<Pb_notificationsDTO> list = Pb_notificationsDAO.getInstance().nextFetchNotification(dtoList.get(dtoList.size()-1).iD,loginDTO.userID,userDTO.roleID, userDTO.organogramID);
                        if(list.size()>0){
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
                            list.forEach(e->{
                                e.dateStr = Utils.getDigits(sdf.format(new Date(e.showingDate)),language);
                            });
                            List<Pb_notificationsDTO> newList = Stream.concat(dtoList.stream(),list.stream()).collect(Collectors.toList());
                            request.getSession(true).setAttribute(SessionConstants.USER_NOTIFICATION,newList);
                            response.setContentType("application/json");
                            response.setCharacterEncoding("UTF-8");
                            PrintWriter out = response.getWriter();
                            out.println(new Gson().toJson(list));
                            out.close();
                        }
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("ID", -1L);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("pb_notifications/pb_notificationsEdit.jsp");
        requestDispatcher.forward(request, response);
    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        logger.debug("Part Header = {0}" + partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            logger.debug("actionType = " + actionType);
            switch (actionType) {
                case "add":

                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PB_NOTIFICATIONS_ADD)) {
                        logger.debug("going to  addPb_notifications ");
                        addPb_notifications(request, response, true);
                    } else {
                        logger.debug("Not going to  addPb_notifications ");
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }

                    break;
                case "edit":

                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PB_NOTIFICATIONS_UPDATE)) {
                        addPb_notifications(request, response, false);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "makeseen": {
                    long seenID = Long.parseLong(request.getParameter("id"));
                    int index = Integer.parseInt(request.getParameter("index"));
                    logger.debug("seeing notification Id : " + seenID);
                    List<Pb_notificationsDTO> list = (List<Pb_notificationsDTO>) request.getSession(true).getAttribute(SessionConstants.USER_NOTIFICATION);
                    Pb_notificationsDTO dto = list.get(index);
                    if(!dto.isSeen){
                       dto.isSeen = true;
                        Pb_notificationsDAO.getInstance().seePb_notification(seenID);
                        long count = (long)request.getSession(true).getAttribute(SessionConstants.USER_NOTIFICATION_COUNT);
                        count = count -1;
                        request.getSession(true).setAttribute(SessionConstants.USER_NOTIFICATION_COUNT,count);
                    }
                }

                break;
                case "delete":
                    deletePb_notifications(request, response);
                    break;
                case "search":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PB_NOTIFICATIONS_SEARCH)) {
                        searchPb_notifications(request, response);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "getGeo":
                    logger.debug("going to geoloc ");
                    request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
                    break;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void addPb_notifications(HttpServletRequest request, HttpServletResponse response, Boolean addFlag) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            logger.debug("%%%% addPb_notifications");
            Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();
            Pb_notificationsDTO pb_notificationsDTO;
            String FileNamePrefix;
            if (addFlag == true) {
                pb_notificationsDTO = new Pb_notificationsDTO();
                FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
            } else {
                pb_notificationsDTO = pb_notificationsDAO.getPb_notificationsDTOByID(Long.parseLong(request.getParameter("identity")));
                FileNamePrefix = request.getParameter("identity");
            }

            String Value = "";
            Value = request.getParameter("iD");
            logger.debug("iD = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                pb_notificationsDTO.iD = Long.parseLong(Value);
            } else {
                logger.debug("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("isSeen");
            logger.debug("isSeen = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                pb_notificationsDTO.isSeen = Boolean.parseBoolean(Value);
            } else {
                logger.debug("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("isHidden");
            logger.debug("isHidden = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                pb_notificationsDTO.isHidden = Boolean.parseBoolean(Value);
            } else {
                logger.debug("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("source");
            logger.debug("source = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                pb_notificationsDTO.source = Integer.parseInt(Value);
            } else {
                logger.debug("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("destination");
            logger.debug("destination = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                pb_notificationsDTO.destination = Integer.parseInt(Value);
            } else {
                logger.debug("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("fromId");
            logger.debug("fromId = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                pb_notificationsDTO.fromId = Long.parseLong(Value);
            } else {
                logger.debug("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("toId");
            logger.debug("toId = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                pb_notificationsDTO.toId = Long.parseLong(Value);
            } else {
                logger.debug("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("text");
            logger.debug("text = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                pb_notificationsDTO.text = (Value);
            } else {
                logger.debug("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("url");
            logger.debug("url = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                pb_notificationsDTO.url = (Value);
            } else {
                logger.debug("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("entryDate");
            logger.debug("entryDate = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                pb_notificationsDTO.entryDate = Long.parseLong(Value);
            } else {
                logger.debug("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("seenDate");
            logger.debug("seenDate = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                pb_notificationsDTO.seenDate = Long.parseLong(Value);
            } else {
                logger.debug("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("showingDate");
            logger.debug("showingDate = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                pb_notificationsDTO.showingDate = Long.parseLong(Value);
            } else {
                logger.debug("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("sendAlarm");
            logger.debug("sendAlarm = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                pb_notificationsDTO.sendAlarm = Boolean.parseBoolean(Value);
            } else {
                logger.debug("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("sendSms");
            logger.debug("sendSms = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                pb_notificationsDTO.sendSms = Boolean.parseBoolean(Value);
            } else {
                logger.debug("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("sendMail");
            logger.debug("sendMail = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                pb_notificationsDTO.sendMail = Boolean.parseBoolean(Value);
            } else {
                logger.debug("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("sendPush");
            logger.debug("sendPush = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                pb_notificationsDTO.sendPush = Boolean.parseBoolean(Value);
            } else {
                logger.debug("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("mailSent");
            logger.debug("mailSent = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                pb_notificationsDTO.mailSent = Boolean.parseBoolean(Value);
            } else {
                logger.debug("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("smsSent");
            logger.debug("smsSent = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                pb_notificationsDTO.smsSent = Boolean.parseBoolean(Value);
            } else {
                logger.debug("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("pushSent");
            logger.debug("pushSent = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                pb_notificationsDTO.pushSent = Boolean.parseBoolean(Value);
            } else {
                logger.debug("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("isDeleted");
            logger.debug("isDeleted = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                pb_notificationsDTO.isDeleted = Integer.parseInt(Value);
            } else {
                logger.debug("FieldName has a null value, not updating" + " = " + Value);
            }

            logger.debug("Done adding  addPb_notifications dto = " + pb_notificationsDTO);

            if (addFlag == true) {
                pb_notificationsDAO.addPb_notifications(pb_notificationsDTO);
            } else {
                pb_notificationsDAO.updatePb_notifications(pb_notificationsDTO);
            }

            String inPlaceSubmit = request.getParameter("inplacesubmit");

            if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                getPb_notifications(request, response);
            } else {
                response.sendRedirect("Pb_notificationsServlet?actionType=search");
            }

        } catch (Exception e) {
            logger.debug(e);
        }
    }

    private void deletePb_notifications(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            String[] IDsToDelete = request.getParameterValues("ID");
            for (int i = 0; i < IDsToDelete.length; i++) {
                long id = Long.parseLong(IDsToDelete[i]);
                logger.debug("###DELETING " + IDsToDelete[i]);
                Pb_notificationsDAO.getInstance().deletePb_notificationsByID(id);
            }
        } catch (Exception ex) {
            logger.debug(ex);
        }
        response.sendRedirect("Pb_notificationsServlet?actionType=search");
    }

    private void getPb_notifications(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("in getPb_notifications");
        Pb_notificationsDTO pb_notificationsDTO = null;
        try {
            pb_notificationsDTO = Pb_notificationsDAO.getInstance().getPb_notificationsDTOByID(Long.parseLong(request.getParameter("ID")));
            request.setAttribute("ID", pb_notificationsDTO.iD);
            request.setAttribute("pb_notificationsDTO", pb_notificationsDTO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "pb_notifications/pb_notificationsInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "pb_notifications/pb_notificationsSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                URL = "pb_notifications/pb_notificationsEdit.jsp?actionType=edit";
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void searchPb_notifications(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("in  searchPb_notifications 1");
        Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        logger.debug("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager rnManager = new RecordNavigationManager(SessionConstants.NAV_PB_NOTIFICATIONS, request, pb_notificationsDAO, SessionConstants.VIEW_PB_NOTIFICATIONS, SessionConstants.SEARCH_PB_NOTIFICATIONS);
        try {
            logger.debug("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            logger.debug("failed to dojob" + e);
        }

        RequestDispatcher rd;
        if (hasAjax == false) {
            logger.debug("Going to pb_notifications/pb_notificationsSearch.jsp");
            rd = request.getRequestDispatcher("pb_notifications/pb_notificationsSearch.jsp");
        } else {
            logger.debug("Going to pb_notifications/pb_notificationsSearchForm.jsp");
            rd = request.getRequestDispatcher("pb_notifications/pb_notificationsSearchForm.jsp");
        }
        rd.forward(request, response);
    }

}

