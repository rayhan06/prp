package gate_pass_sub_type;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import gate_pass_type.Gate_pass_typeDAO;
import gate_pass_type.Gate_pass_typeDTO;
import org.apache.log4j.Logger;

import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;


public class Gate_pass_sub_typeRepository implements Repository {
	Gate_pass_sub_typeDAO gate_pass_sub_typeDAO = new Gate_pass_sub_typeDAO();
	
	public void setDAO(Gate_pass_sub_typeDAO gate_pass_sub_typeDAO)
	{
		this.gate_pass_sub_typeDAO = gate_pass_sub_typeDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Gate_pass_sub_typeRepository.class);
	Map<Long, Gate_pass_sub_typeDTO>mapOfGate_pass_sub_typeDTOToid;
	Map<Long, Set<Gate_pass_sub_typeDTO> >mapOfGate_pass_sub_typeDTOTogatePassTypeId;
	Map<String, Set<Gate_pass_sub_typeDTO> >mapOfGate_pass_sub_typeDTOTonameEng;
	Map<String, Set<Gate_pass_sub_typeDTO> >mapOfGate_pass_sub_typeDTOTonameBng;
	Map<String, Set<Gate_pass_sub_typeDTO> >mapOfGate_pass_sub_typeDTOTodescriptionEng;
	Map<String, Set<Gate_pass_sub_typeDTO> >mapOfGate_pass_sub_typeDTOTodescriptionBng;
	Map<Long, Set<Gate_pass_sub_typeDTO> >mapOfGate_pass_sub_typeDTOToinsertionDate;
	Map<String, Set<Gate_pass_sub_typeDTO> >mapOfGate_pass_sub_typeDTOToinsertedBy;
	Map<String, Set<Gate_pass_sub_typeDTO> >mapOfGate_pass_sub_typeDTOTomodifiedBy;
	Map<Long, Set<Gate_pass_sub_typeDTO> >mapOfGate_pass_sub_typeDTOTolastModificationTime;


	static Gate_pass_sub_typeRepository instance = null;  
	private Gate_pass_sub_typeRepository(){
		mapOfGate_pass_sub_typeDTOToid = new ConcurrentHashMap<>();
		mapOfGate_pass_sub_typeDTOTogatePassTypeId = new ConcurrentHashMap<>();
		mapOfGate_pass_sub_typeDTOTonameEng = new ConcurrentHashMap<>();
		mapOfGate_pass_sub_typeDTOTonameBng = new ConcurrentHashMap<>();
		mapOfGate_pass_sub_typeDTOTodescriptionEng = new ConcurrentHashMap<>();
		mapOfGate_pass_sub_typeDTOTodescriptionBng = new ConcurrentHashMap<>();
		mapOfGate_pass_sub_typeDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfGate_pass_sub_typeDTOToinsertedBy = new ConcurrentHashMap<>();
		mapOfGate_pass_sub_typeDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfGate_pass_sub_typeDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Gate_pass_sub_typeRepository getInstance(){
		if (instance == null){
			instance = new Gate_pass_sub_typeRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(gate_pass_sub_typeDAO == null)
		{
			return;
		}
		try {
			List<Gate_pass_sub_typeDTO> gate_pass_sub_typeDTOs = gate_pass_sub_typeDAO.getAllGate_pass_sub_type(reloadAll);
			for(Gate_pass_sub_typeDTO gate_pass_sub_typeDTO : gate_pass_sub_typeDTOs) {
				Gate_pass_sub_typeDTO oldGate_pass_sub_typeDTO = getGate_pass_sub_typeDTOByid(gate_pass_sub_typeDTO.iD);
				if( oldGate_pass_sub_typeDTO != null ) {
					mapOfGate_pass_sub_typeDTOToid.remove(oldGate_pass_sub_typeDTO.iD);
				
					if(mapOfGate_pass_sub_typeDTOTogatePassTypeId.containsKey(oldGate_pass_sub_typeDTO.gatePassTypeId)) {
						mapOfGate_pass_sub_typeDTOTogatePassTypeId.get(oldGate_pass_sub_typeDTO.gatePassTypeId).remove(oldGate_pass_sub_typeDTO);
					}
					if(mapOfGate_pass_sub_typeDTOTogatePassTypeId.get(oldGate_pass_sub_typeDTO.gatePassTypeId).isEmpty()) {
						mapOfGate_pass_sub_typeDTOTogatePassTypeId.remove(oldGate_pass_sub_typeDTO.gatePassTypeId);
					}
					
					if(mapOfGate_pass_sub_typeDTOTonameEng.containsKey(oldGate_pass_sub_typeDTO.nameEng)) {
						mapOfGate_pass_sub_typeDTOTonameEng.get(oldGate_pass_sub_typeDTO.nameEng).remove(oldGate_pass_sub_typeDTO);
					}
					if(mapOfGate_pass_sub_typeDTOTonameEng.get(oldGate_pass_sub_typeDTO.nameEng).isEmpty()) {
						mapOfGate_pass_sub_typeDTOTonameEng.remove(oldGate_pass_sub_typeDTO.nameEng);
					}
					
					if(mapOfGate_pass_sub_typeDTOTonameBng.containsKey(oldGate_pass_sub_typeDTO.nameBng)) {
						mapOfGate_pass_sub_typeDTOTonameBng.get(oldGate_pass_sub_typeDTO.nameBng).remove(oldGate_pass_sub_typeDTO);
					}
					if(mapOfGate_pass_sub_typeDTOTonameBng.get(oldGate_pass_sub_typeDTO.nameBng).isEmpty()) {
						mapOfGate_pass_sub_typeDTOTonameBng.remove(oldGate_pass_sub_typeDTO.nameBng);
					}
					
					if(mapOfGate_pass_sub_typeDTOTodescriptionEng.containsKey(oldGate_pass_sub_typeDTO.descriptionEng)) {
						mapOfGate_pass_sub_typeDTOTodescriptionEng.get(oldGate_pass_sub_typeDTO.descriptionEng).remove(oldGate_pass_sub_typeDTO);
					}
					if(mapOfGate_pass_sub_typeDTOTodescriptionEng.get(oldGate_pass_sub_typeDTO.descriptionEng).isEmpty()) {
						mapOfGate_pass_sub_typeDTOTodescriptionEng.remove(oldGate_pass_sub_typeDTO.descriptionEng);
					}
					
					if(mapOfGate_pass_sub_typeDTOTodescriptionBng.containsKey(oldGate_pass_sub_typeDTO.descriptionBng)) {
						mapOfGate_pass_sub_typeDTOTodescriptionBng.get(oldGate_pass_sub_typeDTO.descriptionBng).remove(oldGate_pass_sub_typeDTO);
					}
					if(mapOfGate_pass_sub_typeDTOTodescriptionBng.get(oldGate_pass_sub_typeDTO.descriptionBng).isEmpty()) {
						mapOfGate_pass_sub_typeDTOTodescriptionBng.remove(oldGate_pass_sub_typeDTO.descriptionBng);
					}
					
					if(mapOfGate_pass_sub_typeDTOToinsertionDate.containsKey(oldGate_pass_sub_typeDTO.insertionDate)) {
						mapOfGate_pass_sub_typeDTOToinsertionDate.get(oldGate_pass_sub_typeDTO.insertionDate).remove(oldGate_pass_sub_typeDTO);
					}
					if(mapOfGate_pass_sub_typeDTOToinsertionDate.get(oldGate_pass_sub_typeDTO.insertionDate).isEmpty()) {
						mapOfGate_pass_sub_typeDTOToinsertionDate.remove(oldGate_pass_sub_typeDTO.insertionDate);
					}
					
					if(mapOfGate_pass_sub_typeDTOToinsertedBy.containsKey(oldGate_pass_sub_typeDTO.insertedBy)) {
						mapOfGate_pass_sub_typeDTOToinsertedBy.get(oldGate_pass_sub_typeDTO.insertedBy).remove(oldGate_pass_sub_typeDTO);
					}
					if(mapOfGate_pass_sub_typeDTOToinsertedBy.get(oldGate_pass_sub_typeDTO.insertedBy).isEmpty()) {
						mapOfGate_pass_sub_typeDTOToinsertedBy.remove(oldGate_pass_sub_typeDTO.insertedBy);
					}
					
					if(mapOfGate_pass_sub_typeDTOTomodifiedBy.containsKey(oldGate_pass_sub_typeDTO.modifiedBy)) {
						mapOfGate_pass_sub_typeDTOTomodifiedBy.get(oldGate_pass_sub_typeDTO.modifiedBy).remove(oldGate_pass_sub_typeDTO);
					}
					if(mapOfGate_pass_sub_typeDTOTomodifiedBy.get(oldGate_pass_sub_typeDTO.modifiedBy).isEmpty()) {
						mapOfGate_pass_sub_typeDTOTomodifiedBy.remove(oldGate_pass_sub_typeDTO.modifiedBy);
					}
					
					if(mapOfGate_pass_sub_typeDTOTolastModificationTime.containsKey(oldGate_pass_sub_typeDTO.lastModificationTime)) {
						mapOfGate_pass_sub_typeDTOTolastModificationTime.get(oldGate_pass_sub_typeDTO.lastModificationTime).remove(oldGate_pass_sub_typeDTO);
					}
					if(mapOfGate_pass_sub_typeDTOTolastModificationTime.get(oldGate_pass_sub_typeDTO.lastModificationTime).isEmpty()) {
						mapOfGate_pass_sub_typeDTOTolastModificationTime.remove(oldGate_pass_sub_typeDTO.lastModificationTime);
					}
					
					
				}
				if(gate_pass_sub_typeDTO.isDeleted == 0) 
				{
					
					mapOfGate_pass_sub_typeDTOToid.put(gate_pass_sub_typeDTO.iD, gate_pass_sub_typeDTO);
				
					if( ! mapOfGate_pass_sub_typeDTOTogatePassTypeId.containsKey(gate_pass_sub_typeDTO.gatePassTypeId)) {
						mapOfGate_pass_sub_typeDTOTogatePassTypeId.put(gate_pass_sub_typeDTO.gatePassTypeId, new HashSet<>());
					}
					mapOfGate_pass_sub_typeDTOTogatePassTypeId.get(gate_pass_sub_typeDTO.gatePassTypeId).add(gate_pass_sub_typeDTO);
					
					if( ! mapOfGate_pass_sub_typeDTOTonameEng.containsKey(gate_pass_sub_typeDTO.nameEng)) {
						mapOfGate_pass_sub_typeDTOTonameEng.put(gate_pass_sub_typeDTO.nameEng, new HashSet<>());
					}
					mapOfGate_pass_sub_typeDTOTonameEng.get(gate_pass_sub_typeDTO.nameEng).add(gate_pass_sub_typeDTO);
					
					if( ! mapOfGate_pass_sub_typeDTOTonameBng.containsKey(gate_pass_sub_typeDTO.nameBng)) {
						mapOfGate_pass_sub_typeDTOTonameBng.put(gate_pass_sub_typeDTO.nameBng, new HashSet<>());
					}
					mapOfGate_pass_sub_typeDTOTonameBng.get(gate_pass_sub_typeDTO.nameBng).add(gate_pass_sub_typeDTO);
					
					if( ! mapOfGate_pass_sub_typeDTOTodescriptionEng.containsKey(gate_pass_sub_typeDTO.descriptionEng)) {
						mapOfGate_pass_sub_typeDTOTodescriptionEng.put(gate_pass_sub_typeDTO.descriptionEng, new HashSet<>());
					}
					mapOfGate_pass_sub_typeDTOTodescriptionEng.get(gate_pass_sub_typeDTO.descriptionEng).add(gate_pass_sub_typeDTO);
					
					if( ! mapOfGate_pass_sub_typeDTOTodescriptionBng.containsKey(gate_pass_sub_typeDTO.descriptionBng)) {
						mapOfGate_pass_sub_typeDTOTodescriptionBng.put(gate_pass_sub_typeDTO.descriptionBng, new HashSet<>());
					}
					mapOfGate_pass_sub_typeDTOTodescriptionBng.get(gate_pass_sub_typeDTO.descriptionBng).add(gate_pass_sub_typeDTO);
					
					if( ! mapOfGate_pass_sub_typeDTOToinsertionDate.containsKey(gate_pass_sub_typeDTO.insertionDate)) {
						mapOfGate_pass_sub_typeDTOToinsertionDate.put(gate_pass_sub_typeDTO.insertionDate, new HashSet<>());
					}
					mapOfGate_pass_sub_typeDTOToinsertionDate.get(gate_pass_sub_typeDTO.insertionDate).add(gate_pass_sub_typeDTO);
					
					if( ! mapOfGate_pass_sub_typeDTOToinsertedBy.containsKey(gate_pass_sub_typeDTO.insertedBy)) {
						mapOfGate_pass_sub_typeDTOToinsertedBy.put(gate_pass_sub_typeDTO.insertedBy, new HashSet<>());
					}
					mapOfGate_pass_sub_typeDTOToinsertedBy.get(gate_pass_sub_typeDTO.insertedBy).add(gate_pass_sub_typeDTO);
					
					if( ! mapOfGate_pass_sub_typeDTOTomodifiedBy.containsKey(gate_pass_sub_typeDTO.modifiedBy)) {
						mapOfGate_pass_sub_typeDTOTomodifiedBy.put(gate_pass_sub_typeDTO.modifiedBy, new HashSet<>());
					}
					mapOfGate_pass_sub_typeDTOTomodifiedBy.get(gate_pass_sub_typeDTO.modifiedBy).add(gate_pass_sub_typeDTO);
					
					if( ! mapOfGate_pass_sub_typeDTOTolastModificationTime.containsKey(gate_pass_sub_typeDTO.lastModificationTime)) {
						mapOfGate_pass_sub_typeDTOTolastModificationTime.put(gate_pass_sub_typeDTO.lastModificationTime, new HashSet<>());
					}
					mapOfGate_pass_sub_typeDTOTolastModificationTime.get(gate_pass_sub_typeDTO.lastModificationTime).add(gate_pass_sub_typeDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Gate_pass_sub_typeDTO> getGate_pass_sub_typeList() {
		List <Gate_pass_sub_typeDTO> gate_pass_sub_types = new ArrayList<Gate_pass_sub_typeDTO>(this.mapOfGate_pass_sub_typeDTOToid.values());
		return gate_pass_sub_types;
	}
	
	
	public Gate_pass_sub_typeDTO getGate_pass_sub_typeDTOByid( long id){
		return mapOfGate_pass_sub_typeDTOToid.get(id);
	}
	
	
	public List<Gate_pass_sub_typeDTO> getGate_pass_sub_typeDTOBygate_pass_type_id(long gate_pass_type_id) {
		return new ArrayList<>( mapOfGate_pass_sub_typeDTOTogatePassTypeId.getOrDefault(gate_pass_type_id,new HashSet<>()));
	}
	
	
	public List<Gate_pass_sub_typeDTO> getGate_pass_sub_typeDTOByname_eng(String name_eng) {
		return new ArrayList<>( mapOfGate_pass_sub_typeDTOTonameEng.getOrDefault(name_eng,new HashSet<>()));
	}
	
	
	public List<Gate_pass_sub_typeDTO> getGate_pass_sub_typeDTOByname_bng(String name_bng) {
		return new ArrayList<>( mapOfGate_pass_sub_typeDTOTonameBng.getOrDefault(name_bng,new HashSet<>()));
	}
	
	
	public List<Gate_pass_sub_typeDTO> getGate_pass_sub_typeDTOBydescription_eng(String description_eng) {
		return new ArrayList<>( mapOfGate_pass_sub_typeDTOTodescriptionEng.getOrDefault(description_eng,new HashSet<>()));
	}
	
	
	public List<Gate_pass_sub_typeDTO> getGate_pass_sub_typeDTOBydescription_bng(String description_bng) {
		return new ArrayList<>( mapOfGate_pass_sub_typeDTOTodescriptionBng.getOrDefault(description_bng,new HashSet<>()));
	}
	
	
	public List<Gate_pass_sub_typeDTO> getGate_pass_sub_typeDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfGate_pass_sub_typeDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Gate_pass_sub_typeDTO> getGate_pass_sub_typeDTOByinserted_by(String inserted_by) {
		return new ArrayList<>( mapOfGate_pass_sub_typeDTOToinsertedBy.getOrDefault(inserted_by,new HashSet<>()));
	}
	
	
	public List<Gate_pass_sub_typeDTO> getGate_pass_sub_typeDTOBymodified_by(String modified_by) {
		return new ArrayList<>( mapOfGate_pass_sub_typeDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Gate_pass_sub_typeDTO> getGate_pass_sub_typeDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfGate_pass_sub_typeDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	public String buildOptions(String language, long selectedId) {
		List<OptionDTO> optionDTOList = null;
		List<Gate_pass_sub_typeDTO> gate_pass_sub_typeDTOS = new Gate_pass_sub_typeDAO().getAllGate_pass_sub_type(true);
		if (gate_pass_sub_typeDTOS != null && gate_pass_sub_typeDTOS.size() > 0) {
			optionDTOList = gate_pass_sub_typeDTOS.stream()
					.map(dto -> new OptionDTO(dto.nameEng, dto.nameBng, String.valueOf(dto.iD)))
					.collect(Collectors.toList());
		}
		return Utils.buildOptions(optionDTOList, language, String.valueOf(selectedId));
	}

	public String buildJSON(String language){
		String str = "[";
		List<Gate_pass_sub_typeDTO> gate_pass_sub_typeDTOS = new Gate_pass_sub_typeDAO().getAllGate_pass_sub_type(true);
		Gate_pass_sub_typeDTO dto;
		for (int i = 0; i < gate_pass_sub_typeDTOS.size(); i++) {
			dto = gate_pass_sub_typeDTOS.get(i);
			str += "{\"id\":"+ "\""+dto.iD+"\"";
			str += ",\"gatePassTypeId\":"+ "\""+dto.gatePassTypeId+"\"";
			str += ",\"nameEng\":"+ "\""+dto.nameEng+"\"";
			str += ",\"nameBng\":"+ "\""+dto.nameBng+"\"";
			str += "}";
			if(i != gate_pass_sub_typeDTOS.size()-1) str += ",";
		}
		str += "]";
		return str;
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "gate_pass_sub_type";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


