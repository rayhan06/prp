package gate_pass_sub_type;
import java.util.*; 
import util.*;


public class Gate_pass_sub_typeMAPS extends CommonMaps
{	
	public Gate_pass_sub_typeMAPS(String tableName)
	{
		

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("gatePassTypeId".toLowerCase(), "gatePassTypeId".toLowerCase());
		java_DTO_map.put("nameEng".toLowerCase(), "nameEng".toLowerCase());
		java_DTO_map.put("nameBng".toLowerCase(), "nameBng".toLowerCase());
		java_DTO_map.put("descriptionEng".toLowerCase(), "descriptionEng".toLowerCase());
		java_DTO_map.put("descriptionBng".toLowerCase(), "descriptionBng".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("gate_pass_type_id".toLowerCase(), "gatePassTypeId".toLowerCase());
		java_SQL_map.put("name_eng".toLowerCase(), "nameEng".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Gate Pass Type Id".toLowerCase(), "gatePassTypeId".toLowerCase());
		java_Text_map.put("Name Eng".toLowerCase(), "nameEng".toLowerCase());
		java_Text_map.put("Name Bng".toLowerCase(), "nameBng".toLowerCase());
		java_Text_map.put("Description Eng".toLowerCase(), "descriptionEng".toLowerCase());
		java_Text_map.put("Description Bng".toLowerCase(), "descriptionBng".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}