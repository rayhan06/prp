package gate_pass_sub_type;
import java.util.*; 
import util.*; 


public class Gate_pass_sub_typeDTO extends CommonDTO
{

	public long gatePassTypeId = -1;
    public String nameEng = "";
    public String nameBng = "";
    public String descriptionEng = "";
    public String descriptionBng = "";
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Gate_pass_sub_typeDTO[" +
            " id = " + iD +
            " gatePassTypeId = " + gatePassTypeId +
            " nameEng = " + nameEng +
            " nameBng = " + nameBng +
            " descriptionEng = " + descriptionEng +
            " descriptionBng = " + descriptionBng +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}