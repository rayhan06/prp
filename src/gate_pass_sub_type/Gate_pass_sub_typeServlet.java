package gate_pass_sub_type;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Gate_pass_sub_typeServlet
 */
@WebServlet("/Gate_pass_sub_typeServlet")
@MultipartConfig
public class Gate_pass_sub_typeServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Gate_pass_sub_typeServlet.class);

    String tableName = "gate_pass_sub_type";

	Gate_pass_sub_typeDAO gate_pass_sub_typeDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Gate_pass_sub_typeServlet() 
	{
        super();
    	try
    	{
			gate_pass_sub_typeDAO = new Gate_pass_sub_typeDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(gate_pass_sub_typeDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_SUB_TYPE_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_SUB_TYPE_UPDATE))
				{
					getGate_pass_sub_type(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_SUB_TYPE_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchGate_pass_sub_type(request, response, isPermanentTable, filter);
						}
						else
						{
							searchGate_pass_sub_type(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchGate_pass_sub_type(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_SUB_TYPE_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_SUB_TYPE_ADD))
				{
					System.out.println("going to  addGate_pass_sub_type ");
					addGate_pass_sub_type(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addGate_pass_sub_type ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_SUB_TYPE_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addGate_pass_sub_type ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_SUB_TYPE_UPDATE))
				{					
					addGate_pass_sub_type(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_SUB_TYPE_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_SUB_TYPE_SEARCH))
				{
					searchGate_pass_sub_type(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Gate_pass_sub_typeDTO gate_pass_sub_typeDTO = gate_pass_sub_typeDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(gate_pass_sub_typeDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addGate_pass_sub_type(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addGate_pass_sub_type");
			String path = getServletContext().getRealPath("/img2/");
			Gate_pass_sub_typeDTO gate_pass_sub_typeDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				gate_pass_sub_typeDTO = new Gate_pass_sub_typeDTO();
			}
			else
			{
				gate_pass_sub_typeDTO = gate_pass_sub_typeDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			}
			
			String Value = "";

			Value = request.getParameter("gatePassTypeId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("gatePassTypeId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				gate_pass_sub_typeDTO.gatePassTypeId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nameEng");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEng = " + Value);
			if(Value != null)
			{
				gate_pass_sub_typeDTO.nameEng = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nameBng");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameBng = " + Value);
			if(Value != null)
			{
				gate_pass_sub_typeDTO.nameBng = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("descriptionEng");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("descriptionEng = " + Value);
			if(Value != null)
			{
				gate_pass_sub_typeDTO.descriptionEng = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("descriptionBng");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("descriptionBng = " + Value);
			if(Value != null)
			{
				gate_pass_sub_typeDTO.descriptionBng = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				gate_pass_sub_typeDTO.insertionDate = c.getTimeInMillis();
			}			


			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null)
			{
				gate_pass_sub_typeDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null)
			{
				gate_pass_sub_typeDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addGate_pass_sub_type dto = " + gate_pass_sub_typeDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				gate_pass_sub_typeDAO.setIsDeleted(gate_pass_sub_typeDTO.iD, CommonDTO.OUTDATED);
				returnedID = gate_pass_sub_typeDAO.add(gate_pass_sub_typeDTO);
				gate_pass_sub_typeDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = gate_pass_sub_typeDAO.manageWriteOperations(gate_pass_sub_typeDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = gate_pass_sub_typeDAO.manageWriteOperations(gate_pass_sub_typeDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getGate_pass_sub_type(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Gate_pass_sub_typeServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(gate_pass_sub_typeDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void getGate_pass_sub_type(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getGate_pass_sub_type");
		Gate_pass_sub_typeDTO gate_pass_sub_typeDTO = null;
		try 
		{
			gate_pass_sub_typeDTO = gate_pass_sub_typeDAO.getDTOByID(id);
			request.setAttribute("ID", gate_pass_sub_typeDTO.iD);
			request.setAttribute("gate_pass_sub_typeDTO",gate_pass_sub_typeDTO);
			request.setAttribute("gate_pass_sub_typeDAO",gate_pass_sub_typeDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "gate_pass_sub_type/gate_pass_sub_typeInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "gate_pass_sub_type/gate_pass_sub_typeSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "gate_pass_sub_type/gate_pass_sub_typeEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "gate_pass_sub_type/gate_pass_sub_typeEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getGate_pass_sub_type(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getGate_pass_sub_type(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchGate_pass_sub_type(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchGate_pass_sub_type 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_GATE_PASS_SUB_TYPE,
			request,
			gate_pass_sub_typeDAO,
			SessionConstants.VIEW_GATE_PASS_SUB_TYPE,
			SessionConstants.SEARCH_GATE_PASS_SUB_TYPE,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("gate_pass_sub_typeDAO",gate_pass_sub_typeDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to gate_pass_sub_type/gate_pass_sub_typeApproval.jsp");
	        	rd = request.getRequestDispatcher("gate_pass_sub_type/gate_pass_sub_typeApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to gate_pass_sub_type/gate_pass_sub_typeApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("gate_pass_sub_type/gate_pass_sub_typeApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to gate_pass_sub_type/gate_pass_sub_typeSearch.jsp");
	        	rd = request.getRequestDispatcher("gate_pass_sub_type/gate_pass_sub_typeSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to gate_pass_sub_type/gate_pass_sub_typeSearchForm.jsp");
	        	rd = request.getRequestDispatcher("gate_pass_sub_type/gate_pass_sub_typeSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

