package gate_pass_sub_type;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Gate_pass_sub_typeDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Gate_pass_sub_typeDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Gate_pass_sub_typeMAPS(tableName);
		columnNames = new String[] 
		{
			"id",
			"gate_pass_type_id",
			"name_eng",
			"name_bng",
			"description_eng",
			"description_bng",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Gate_pass_sub_typeDAO()
	{
		this("gate_pass_sub_type");		
	}
	
	public void setSearchColumn(Gate_pass_sub_typeDTO gate_pass_sub_typeDTO)
	{
		gate_pass_sub_typeDTO.searchColumn = "";
		gate_pass_sub_typeDTO.searchColumn += gate_pass_sub_typeDTO.nameEng + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Gate_pass_sub_typeDTO gate_pass_sub_typeDTO = (Gate_pass_sub_typeDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(gate_pass_sub_typeDTO);
		if(isInsert)
		{
			ps.setObject(index++,gate_pass_sub_typeDTO.iD);
		}
		ps.setObject(index++,gate_pass_sub_typeDTO.gatePassTypeId);
		ps.setObject(index++,gate_pass_sub_typeDTO.nameEng);
		ps.setObject(index++,gate_pass_sub_typeDTO.nameBng);
		ps.setObject(index++,gate_pass_sub_typeDTO.descriptionEng);
		ps.setObject(index++,gate_pass_sub_typeDTO.descriptionBng);
		ps.setObject(index++,gate_pass_sub_typeDTO.insertionDate);
		ps.setObject(index++,gate_pass_sub_typeDTO.insertedBy);
		ps.setObject(index++,gate_pass_sub_typeDTO.modifiedBy);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Gate_pass_sub_typeDTO gate_pass_sub_typeDTO, ResultSet rs) throws SQLException
	{
		gate_pass_sub_typeDTO.iD = rs.getLong("id");
		gate_pass_sub_typeDTO.gatePassTypeId = rs.getLong("gate_pass_type_id");
		gate_pass_sub_typeDTO.nameEng = rs.getString("name_eng");
		gate_pass_sub_typeDTO.nameBng = rs.getString("name_bng");
		gate_pass_sub_typeDTO.descriptionEng = rs.getString("description_eng");
		gate_pass_sub_typeDTO.descriptionBng = rs.getString("description_bng");
		gate_pass_sub_typeDTO.insertionDate = rs.getLong("insertion_date");
		gate_pass_sub_typeDTO.insertedBy = rs.getString("inserted_by");
		gate_pass_sub_typeDTO.modifiedBy = rs.getString("modified_by");
		gate_pass_sub_typeDTO.isDeleted = rs.getInt("isDeleted");
		gate_pass_sub_typeDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}
	
	
	
	
		
	

	//need another getter for repository
	public Gate_pass_sub_typeDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Gate_pass_sub_typeDTO gate_pass_sub_typeDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE id=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				gate_pass_sub_typeDTO = new Gate_pass_sub_typeDTO();

				get(gate_pass_sub_typeDTO, rs);

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return gate_pass_sub_typeDTO;
	}
	
	
	
	
	public List<Gate_pass_sub_typeDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Gate_pass_sub_typeDTO gate_pass_sub_typeDTO = null;
		List<Gate_pass_sub_typeDTO> gate_pass_sub_typeDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return gate_pass_sub_typeDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE id IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				gate_pass_sub_typeDTO = new Gate_pass_sub_typeDTO();
				get(gate_pass_sub_typeDTO, rs);
				System.out.println("got this DTO: " + gate_pass_sub_typeDTO);
				
				gate_pass_sub_typeDTOList.add(gate_pass_sub_typeDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return gate_pass_sub_typeDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Gate_pass_sub_typeDTO> getAllGate_pass_sub_type (boolean isFirstReload)
    {
		List<Gate_pass_sub_typeDTO> gate_pass_sub_typeDTOList = new ArrayList<>();

		String sql = "SELECT * FROM gate_pass_sub_type";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by gate_pass_sub_type.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Gate_pass_sub_typeDTO gate_pass_sub_typeDTO = new Gate_pass_sub_typeDTO();
				get(gate_pass_sub_typeDTO, rs);
				
				gate_pass_sub_typeDTOList.add(gate_pass_sub_typeDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return gate_pass_sub_typeDTOList;
    }

	
	public List<Gate_pass_sub_typeDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Gate_pass_sub_typeDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Gate_pass_sub_typeDTO> gate_pass_sub_typeDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Gate_pass_sub_typeDTO gate_pass_sub_typeDTO = new Gate_pass_sub_typeDTO();
				get(gate_pass_sub_typeDTO, rs);
				
				gate_pass_sub_typeDTOList.add(gate_pass_sub_typeDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return gate_pass_sub_typeDTOList;
	
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name_eng")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name_eng"))
					{
						AllFieldSql += "" + tableName + ".name_eng like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	