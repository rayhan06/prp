package mp_management;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import dbm.DBMW;
import election_constituency.Election_constituencyDTO;
import election_constituency.Election_constituencyRepository;
import election_wise_mp.Election_wise_mpRepository;
import employee_management.CreateUserService;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import employee_records.MpStatusEnum;
import org.apache.log4j.Logger;
import pb.Utils;
import political_party.Political_partyDTO;
import political_party.Political_partyRepository;
import user.UserRepository;
import util.CommonDTO;
import util.RecordNavigator;
import util.StringUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

@SuppressWarnings("Duplicates")
public class Mp_managementDAO implements CommonDAOService<Mp_managementDTO>, CreateUserService {

    private final Logger logger = Logger.getLogger(Mp_managementDAO.class);

    private static final String addQuery = "INSERT INTO employee_records (id,name_eng,name_bng,personal_mobile,created_by," +
            "modified_by,created,lastModificationTime,isDeleted,personal_email,joining_date," +
            "employee_number,is_mp,date_of_birth,election_wise_mp_id,search_column,mp_elected_count,nid)" +
            "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private static final String addMpQuery = "INSERT INTO election_wise_mp (id,election_details_id,election_constituency_id,constituency_number," +
            "political_party_id,employee_records_id,mp_status_cat,username,date_of_birth,inserted_by,modified_by," +
            "insertion_date,lastModificationTime,search_column,isDeleted)" +
            "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private Mp_managementDAO() {
        searchMap.put("name_eng", " AND (name_eng like ?)");
        searchMap.put("name_bng", " AND (name_bng like ?)");
        searchMap.put("mobile_number", " AND (personal_mobile = ?)");
        searchMap.put("email", " AND (personal_email = ?)");
        searchMap.put("userName", " AND (employee_number = ?)");
        searchMap.put("AnyField", " AND (employee_records.search_column like ?)");
        searchMap.put("isMp", " AND (is_mp = ? )");
        searchMap.put("electionDetailsId", " AND (election_details_id = ? )");
        searchMap.put("electionConstituencyId", " AND (election_constituency_id = ? )");
        searchMap.put("politicalPartyId", " AND (political_party_id = ? )");
        searchMap.put("AnyField2", " OR (election_wise_mp.search_column like ?)");
        searchMap.put("mpStatus", " AND (mp_status_cat = ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Mp_managementDAO INSTANCE = new Mp_managementDAO();
    }

    public static Mp_managementDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Mp_managementDTO mp_managementDTO) {
        mp_managementDTO.searchColumn = "";
        mp_managementDTO.searchColumn += mp_managementDTO.nameEng + " ";
        mp_managementDTO.searchColumn += mp_managementDTO.nameBng + " ";
        mp_managementDTO.searchColumn += mp_managementDTO.mobileNumber + " ";
        mp_managementDTO.searchColumn += mp_managementDTO.personalEml + " ";
        mp_managementDTO.searchColumn += mp_managementDTO.userName + " "+ StringUtils.convertToBanNumber(mp_managementDTO.userName)+" ";
    }

    @Override
    public void set(PreparedStatement ps, Mp_managementDTO mp_managementDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(mp_managementDTO);
        ps.setObject(++index, mp_managementDTO.iD);
        ps.setObject(++index, mp_managementDTO.nameEng);
        ps.setObject(++index, mp_managementDTO.nameBng);
        ps.setObject(++index, mp_managementDTO.mobileNumber);
        ps.setObject(++index, mp_managementDTO.insertedBy);
        ps.setObject(++index, mp_managementDTO.modifiedBy);
        ps.setObject(++index, mp_managementDTO.insertionDate);
        ps.setObject(++index, mp_managementDTO.lastModificationTime);
        ps.setObject(++index, mp_managementDTO.isDeleted);
        ps.setObject(++index, mp_managementDTO.personalEml);
        ps.setObject(++index, mp_managementDTO.joiningDate);
        ps.setObject(++index, mp_managementDTO.userName);
        ps.setObject(++index, 1);
        ps.setObject(++index, mp_managementDTO.dob);
        ps.setObject(++index,mp_managementDTO.electionWiseMpId);
        ps.setObject(++index, mp_managementDTO.searchColumn);
        ps.setObject(++index, mp_managementDTO.mpElectedCount);
        ps.setObject(++index, mp_managementDTO.nid);
    }

    public String getMPSearchColumn(Mp_managementDTO dto) {
        StringBuilder column = new StringBuilder();
        Election_constituencyDTO constituencyDTO = Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(dto.electionConstituencyId);
        column.append(constituencyDTO.constituencyNameEn).append(" ").append(constituencyDTO.constituencyNameBn).append(" ");
        column.append(constituencyDTO.constituencyNumber).append(" ").append(StringUtils.convertToBanNumber(String.valueOf(constituencyDTO.constituencyNumber))).append(" ");
        Employee_recordsDTO recordDTO = Employee_recordsRepository.getInstance().getById(dto.iD);
        if (recordDTO != null) {
            column.append(recordDTO.nameEng).append(" ").append(recordDTO.nameBng).append(" ");
        }
        Political_partyDTO politicalPartyDTO = Political_partyRepository.getInstance().getPolitical_partyDTOByID(dto.politicalPartyId);
        column.append(politicalPartyDTO.nameEn).append(" ").append(politicalPartyDTO.nameBn).append(" ");
        return column.toString();
    }

    @Override
    public Mp_managementDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Mp_managementDTO mp_managementDTO = new Mp_managementDTO();
            mp_managementDTO.iD = rs.getLong("id");
            mp_managementDTO.joiningDate = rs.getLong("joining_date");
            mp_managementDTO.nameEng = rs.getString("name_eng");
            mp_managementDTO.nameBng = rs.getString("name_bng");
            mp_managementDTO.mobileNumber = rs.getString("personal_mobile");
            mp_managementDTO.personalEml = rs.getString("personal_email");
            mp_managementDTO.status = rs.getBoolean("status");
            mp_managementDTO.userName = rs.getString("username");
            mp_managementDTO.electionDetailsId = rs.getLong("election_details_id");
            mp_managementDTO.electionConstituencyId = rs.getLong("election_constituency_id");
            mp_managementDTO.politicalPartyId = rs.getLong("political_party_id");
            mp_managementDTO.mpStatus = rs.getInt("mp_status_cat");
            mp_managementDTO.nid = rs.getString("nid");
            mp_managementDTO.photo = Utils.getByteArrayFromInputStream(rs.getBinaryStream("photo"));
            return mp_managementDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            ex.printStackTrace();
            return null;
        }
    }

    public Mp_managementDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "employee_records";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        AtomicReference<Exception> exception = new AtomicReference<>();
        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            Mp_managementDTO dto = (Mp_managementDTO) commonDTO;
            Connection connection = model.getConnection();
            try {
                connection.setAutoCommit(false);
                PreparedStatement ps = (PreparedStatement) model.getStatement();
                dto.iD = DBMW.getInstance().getNextSequenceId("employee_records");
                dto.lastModificationTime = System.currentTimeMillis() + 120000; // Add 2mins caching delay from current time
                createMP(dto,connection);
                set(ps,dto ,true);
                logger.debug(ps);
                ps.execute();
                createUser(dto,connection);
                connection.commit();
                connection.setAutoCommit(true);
            } catch (Exception ex) {
                ex.printStackTrace();
                try {
                    connection.rollback();
                    connection.setAutoCommit(true);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                exception.set(ex);
            }
        },addQuery);
        if(exception.get()!=null){
            throw exception.get();
        }
        return commonDTO.iD;
    }

    private void createMP(Mp_managementDTO dto,Connection connection) throws Exception{
        AtomicReference<Exception> exception = new AtomicReference<>();
        ConnectionAndStatementUtil.getWritePrepareStatement(ps->{
            int index = 0;
            try {
                dto.electionWiseMpId = DBMW.getInstance().getNextSequenceId("election_wise_mp");
                ps.setObject(++index,dto.electionWiseMpId);
                ps.setObject(++index,dto.electionDetailsId);
                ps.setObject(++index,dto.electionConstituencyId);
                ps.setObject(++index,dto.constituencyNumber);
                ps.setObject(++index,dto.politicalPartyId);
                ps.setObject(++index,dto.iD);
                ps.setObject(++index, MpStatusEnum.ACTIVE.getValue());
                ps.setObject(++index,dto.userName);
                ps.setObject(++index,dto.dob);
                ps.setObject(++index,dto.insertedBy);
                ps.setObject(++index,dto.modifiedBy);
                ps.setObject(++index,dto.insertionDate);
                ps.setObject(++index,dto.lastModificationTime);
                ps.setObject(++index,getMPSearchColumn(dto));
                ps.setObject(++index,0);
                logger.debug(ps);
                ps.execute();
                recordUpdateTime(connection,"election_wise_mp",dto.lastModificationTime);
            } catch (Exception ex) {
                ex.printStackTrace();
                exception.set(ex);
            }
        },connection,addMpQuery);
        if(exception.get()!=null){
            throw exception.get();
        }
    }

    public long update(CommonDTO commonDTO) throws Exception {
        throw new Exception("Updating is not supported");
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

    @Override
    public RecordNavigator getRecordNavigator(Map<String, String> params, String tableJoinClause, String whereClause, String sortingClause) {
        tableJoinClause = " inner join election_wise_mp on employee_records.id = election_wise_mp.employee_records_id inner join election_constituency on election_wise_mp.election_constituency_id = election_constituency.ID ";
        if(whereClause == null){
            whereClause = "";
        }
        whereClause += " AND (election_wise_mp.isDeleted = 0) ";
        RecordNavigator rn = CommonDAOService.super.getRecordNavigator(params, tableJoinClause, whereClause, sortingClause);
        rn.m_tableName = "mp_management";
        return rn;
    }
}