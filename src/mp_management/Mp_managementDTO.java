package mp_management;

import annotation.ElectionConstituencyValidation;
import annotation.ParliamentNumberValidation;
import annotation.PoliticalPartyAnnotation;
import employee_management.EmployeeMPModel;

public class Mp_managementDTO extends EmployeeMPModel{
    @ParliamentNumberValidation
    public long electionDetailsId = -1;

    @ElectionConstituencyValidation
    public long electionConstituencyId = -1;

    @PoliticalPartyAnnotation
    public long politicalPartyId = -1;

    public int mpElectedCount = 0;
    public long electionWiseMpId = 0;
    public int constituencyNumber = 0;
    public byte[] photo = null;

    @Override
    public String toString() {
        return "$Mp_managementDTO[" +
                " iD = " + iD +
                " nameEng = " + nameEng +
                " nameBng = " + nameBng +
                " dob = " + dob +
                " mobileNumber = " + mobileNumber +
                " email = " + personalEml +
                " joiningDate = " + joiningDate +
                " electionDetailsId = " + electionDetailsId +
                " electionConstituencyId = " + electionConstituencyId +
                " politicalPartyId = " + politicalPartyId +
                " insertionDate = " + insertionDate +
                " insertedBy = " + insertedBy +
                " modifiedBy = " + modifiedBy +
                " searchColumn = " + searchColumn +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }
}