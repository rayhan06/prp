package mp_management;

import common.BaseServlet;
import common.CommonDAOService;
import election_constituency.Election_constituencyDAO;
import election_constituency.Election_constituencyDTO;
import election_constituency.Election_constituencyRepository;
import election_details.Election_detailsDTO;
import election_details.Election_detailsRepository;
import employee_management.EmployeeMPModel;
import employee_management.EmployeeMPService;
import employee_records.Employee_recordsRepository;
import pb.Utils;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("Duplicates")
@WebServlet("/Mp_managementServlet")
@MultipartConfig
public class Mp_managementServlet extends BaseServlet implements EmployeeMPService {
    private static final long serialVersionUID = 1L;

    private final Mp_managementDAO mpManagementDAO = Mp_managementDAO.getInstance();

    @Override
    public String getTableName() {
        return mpManagementDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Mp_managementServlet";
    }

    @Override
    public Mp_managementDAO getCommonDAOService() {
        return mpManagementDAO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.MP_MANAGEMENT_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.MP_MANAGEMENT_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.MP_MANAGEMENT_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Mp_managementServlet.class;
    }

    public String commonPartOfDispatchURL() {
        return "mp_management/mp_management";
    }

    @Override
    public String getSortClause(HttpServletRequest request) {
        return " election_wise_mp.election_details_id DESC, election_constituency.constituency_number ";
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if ("search".equals(request.getParameter("actionType"))) {
            Map<String, String> map = new HashMap<>();
            map.put("isMp", "1");
            if(request.getParameter("AnyField")!=null && request.getParameter("AnyField").trim().length()>0){
                map.put("AnyField2",request.getParameter("AnyField"));
            }
            request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, map);
        }
        super.doGet(request, response);
    }

    @Override
    public synchronized CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Mp_managementDTO mpManagementDTO = new Mp_managementDTO();
        validateAndSet(mpManagementDTO,request,addFlag);
        mpManagementDTO.constituencyNumber = Election_constituencyRepository.getInstance()
                .getElection_constituencyDTOByID(mpManagementDTO.electionConstituencyId).constituencyNumber;
        setEmployeeMPModel(mpManagementDTO, request, addFlag, isLangEng);

        try{
            mpManagementDTO.mpElectedCount = Integer.parseInt(Utils.doJsoupCleanOrReturnDefault(request.getParameter("electedCount"), "0"));
        }catch (Exception ex){
            logger.debug("Not a mandatory field, No need to throw message!");
        }

        mpManagementDTO.userName = EmployeeMPModel.iDGenerationOfMP(getPrefixIfActiveMPNotPresent(mpManagementDTO.electionDetailsId, mpManagementDTO.electionConstituencyId));

        mpManagementDAO.add(mpManagementDTO);
        Election_detailsDTO electionDetailsDTO = Election_detailsRepository.getInstance().getElectionDetailsDTOByID(mpManagementDTO.electionDetailsId);

        // Make isVacant field 0 as MP is assigned to a constituency
        Election_constituencyDTO election_constituencyDTO = Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(mpManagementDTO.electionConstituencyId);
        election_constituencyDTO.isVacant = 0;
        Election_constituencyDAO.getInstance().update(election_constituencyDTO);
        // Make isVacant field 0 as MP is assigned to a constituency
        if(electionDetailsDTO.parliamentStatusCat == 1){
            Utils.runIOTaskInSingleThread(()->()-> sendSmsAndMailForNewUser(mpManagementDTO));
        }
        return mpManagementDTO;
    }

    private String getPrefixIfActiveMPNotPresent(long parliamentNumberOfMP, long electionConstituencyOfMP) throws Exception {
        String prefix = EmployeeMPModel.generateUserNamePrefixForMP(parliamentNumberOfMP, electionConstituencyOfMP);
        int count = Employee_recordsRepository.getInstance().getActiveCountByEmployeeNumberPrefix(prefix);
        if (count > 0) {
            Election_detailsDTO election_detailsDTO = Election_detailsRepository.getInstance().getElectionDetailsDTOByID(parliamentNumberOfMP);
            Election_constituencyDTO election_constituencyDTO = Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(electionConstituencyOfMP);
            if(election_constituencyDTO.isVacant != 1) {
                String msg;
                if ("English".equalsIgnoreCase(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language)) {
                    msg = "A active MP is already found for " + election_constituencyDTO.constituencyNameEn + " parliament number-" + election_detailsDTO.parliamentNumber;
                } else
                    msg = "নির্বাচন-" + StringUtils.convertToBanNumber(String.valueOf(election_detailsDTO.parliamentNumber)) + " অধীনে নির্বাচনী এলাকা "
                            + election_constituencyDTO.constituencyNameBn + " এ সক্রিয় সংসদ সদস্য ইতিমধ্যে রয়েছেন";
                throw new Exception(msg);
            }
        }
        return prefix;
    }



    @Override
    public void init(HttpServletRequest request) throws ServletException {
        request.setAttribute("navName", "../mp_management/mp_managementNav.jsp");
        request.setAttribute("formName", "../mp_management/mp_managementSearchForm.jsp");
        request.setAttribute("servletName", "Mp_managementServlet");
    }
}