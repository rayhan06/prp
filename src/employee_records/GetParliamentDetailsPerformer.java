package employee_records;

import common.ApiResponse;
import election_constituency.Election_constituencyRepository;
import election_details.Election_detailsDTO;
import election_details.Election_detailsRepository;
import election_wise_mp.Election_wise_mpRepository;
import org.apache.log4j.Logger;
import political_party.Political_partyRepository;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class GetParliamentDetailsPerformer implements EmployeeRecordsApiPerformer{
    public final Logger logger = Logger.getLogger(GetMpListByParliamentNumberPerformer.class);
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    @Override
    public ApiResponse perform(HttpServletRequest request) {
        long parliament_no;

        try {
            parliament_no = Long.parseLong(request.getParameter("parliament_number"));
        } catch (NumberFormatException nfe) {
            return ApiResponse.makeResponseToBadRequest("Kindly check the parliament_number parameter provided!");
        }

        ApiResponse apiResponse;

        ParliamentDetailsModel model;

        try {
            Election_detailsDTO election_detailsDTO = null;

            for (Election_detailsDTO dto: Election_detailsRepository.getInstance().getElectionDetailsList()) {
                if (dto.parliamentNumber==parliament_no) {
                    election_detailsDTO = dto;
                }
            }

            model = election_detailsDTO != null ? new ParliamentDetailsModel(election_detailsDTO) : null;

            apiResponse = model==null ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                    ApiResponse.makeSuccessResponse(model, "Successfully found the data.");


        } catch (Exception e) {
            e.printStackTrace();
            return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
        }

        return apiResponse;
    }
}
