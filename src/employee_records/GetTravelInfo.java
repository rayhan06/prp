package employee_records;

import common.ApiResponse;
import emp_travel_details.Emp_travel_detailsDAO;
import org.apache.log4j.Logger;
import user.UserRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

public class GetTravelInfo implements EmployeeRecordsApiPerformer{
    public final Logger logger = Logger.getLogger(GetMpListByParliamentNumberPerformer.class);

    @Override
    public ApiResponse perform(HttpServletRequest request) {
        String userName = request.getParameter("employee_record_id");
        long employeeRecordId;

        try {
            employeeRecordId = Employee_recordsRepository.getInstance().getEmployeeRecordIdByUsername(userName);
            if (employeeRecordId==0)
                return ApiResponse.makeResponseToResourceNotFound("No Record found with employee_record_id: "+userName+"!");
        } catch (Exception ex) {
            return ApiResponse.makeResponseToResourceNotFound("No Record found with employee_record_id: "+userName+"!");
        }

        try {
            try {
                List<EmployeeTravelModel> employeeTravelModels = Emp_travel_detailsDAO.getInstance()
                        .getByEmployeeId(employeeRecordId)
                        .stream()
                        .map(EmployeeTravelModel::new)
                        .collect(Collectors.toList());
                return employeeTravelModels.size()==0 ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                        ApiResponse.makeSuccessResponse(employeeTravelModels, "Successfully found the data.");
            } catch (Exception e) {
                e.printStackTrace();
                return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
            }
        } catch (NumberFormatException nfe) {
            return ApiResponse.makeResponseToBadRequest("Kindly check the employee_record_id parameter provided!");
        }
    }
}