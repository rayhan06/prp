package employee_records;

/*
 * @author Md. Erfan Hossain
 * @created 16/03/2021 - 6:21 AM
 * @project parliament
 */

public class EmpOfficeModel {
    public long rowId;
    public long employeeRecordId;
    public long officeUnitId;
    public long organogramId;
    public long officeUnitOrganogramId;

    @Override
    public String toString() {
        return "EmpOfficeModel{" +
                "rowId=" + rowId +
                ", employeeRecordId=" + employeeRecordId +
                ", officeUnitId=" + officeUnitId +
                ", organogramId=" + organogramId +
                ", officeUnitOrganogramId=" + officeUnitOrganogramId +
                '}';
    }
}
