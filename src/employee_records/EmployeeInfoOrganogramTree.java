package employee_records;

import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import user.UserRepository;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EmployeeInfoOrganogramTree {
    public long organogramId = 0;
    public String empRecordId = "-1";
    public String nameEng = "";
    public String fatherNameEng = "";
    public String dateOfBirth = "";
    public String designation = "";
    public String inchargeLebel = "";



    public EmployeeInfoOrganogramTree(EmployeeOfficeDTO employeeOfficeDTO) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        Employee_recordsDTO dto = Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId);

        try {
            this.empRecordId = UserRepository.getInstance().getUserDtoByEmployeeRecordId(dto.iD).userName;
        } catch (Exception ex) {
            this.empRecordId = "-1";
        }

        this.nameEng = dto.nameEng;
        this.fatherNameEng = dto.fatherNameEng;
        this.dateOfBirth = dateFormat.format(new Date(dto.dateOfBirth));
        this.designation = employeeOfficeDTO.designation;
        this.inchargeLebel = employeeOfficeDTO.inchargeLabel;
        this.organogramId = employeeOfficeDTO.officeUnitOrganogramId;
    }
}
