package employee_records;

/*
 * @author Md. Erfan Hossain
 * @created 04/08/2021 - 10:40 PM
 * @project parliament
 */

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum MpStatusEnum {
    ACTIVE(1),
    DIED(2),
    RESIGNED(3),
    CANCELLED(4),
    COMPLETED(5);

    private final int value;

    MpStatusEnum(int value) {
        this.value = value;
    }

    private static volatile Map<Integer,MpStatusEnum> mapByValue = null;

    private static void reload(){
        mapByValue = Stream.of(MpStatusEnum.values())
                .collect(Collectors.toMap(e->e.value,e->e));
    }

    public int getValue() {
        return value;
    }

    public static MpStatusEnum getByValue(int value){
        if(mapByValue == null){
            synchronized (MpStatusEnum.class){
                if(mapByValue == null){
                    reload();
                }
            }
        }
        return mapByValue.get(value);
    }
}
