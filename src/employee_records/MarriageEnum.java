package employee_records;

public enum MarriageEnum {
    SINGLE(1),
    MARRIED(2),
    DIVORCED(3),
    WIDOWED(4);

    private final int value;

    MarriageEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
