package employee_records;

import common.ApiResponse;
import election_constituency.Election_constituencyRepository;
import election_details.Election_detailsDTO;
import election_details.Election_detailsRepository;
import election_wise_mp.Election_wise_mpRepository;
import org.apache.log4j.Logger;
import parliament_session.Parliament_sessionDTO;
import parliament_session.Parliament_sessionRepository;
import political_party.Political_partyRepository;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class GetParliamentSessionPerformer implements EmployeeRecordsApiPerformer{
    public final Logger logger = Logger.getLogger(GetMpListByParliamentNumberPerformer.class);
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    @Override
    public ApiResponse perform(HttpServletRequest request) {
        long parliament_no;

        try {
            parliament_no = Long.parseLong(request.getParameter("parliament_number"));
        } catch (NumberFormatException nfe) {
            return ApiResponse.makeResponseToBadRequest("Kindly check the parliament_number parameter provided!");
        }

        ApiResponse apiResponse;

        List<ParliamentSessionModel> parliamentSessionModels = new ArrayList<>();

        try {
            Election_detailsDTO election_detailsDTO = null;
            for (Election_detailsDTO election_dto: Election_detailsRepository.getInstance().getElectionDetailsList()) {
                if (election_dto.parliamentNumber==parliament_no) {
                    election_detailsDTO = election_dto;
                    break;
                }
            }

            if (election_detailsDTO !=null) {
                for (Parliament_sessionDTO session_dto: Parliament_sessionRepository.getInstance().getParliament_sessionList()) {
                    if (session_dto.electionDetailsId==election_detailsDTO.iD) {
                        parliamentSessionModels.add(new ParliamentSessionModel(session_dto));
                    }
                }
            }

            apiResponse = parliamentSessionModels.size()==0 ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                    ApiResponse.makeSuccessResponse(parliamentSessionModels, "Successfully found the data.");


        } catch (Exception e) {
            e.printStackTrace();
            return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
        }

        return apiResponse;
    }
}
