package employee_records;

import api_authentication_and_log.APIAgentDAO;
import api_authentication_and_log.APIAgentDTO;
import api_authentication_and_log.APILogDAO;
import api_authentication_and_log.APILogDTO;
import common.ApiResponse;
import org.apache.log4j.Logger;
import pb.Utils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ext/employee-records-api")
public class EmployeeRecordsApiServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(EmployeeRecordsApiServlet.class);

    private final APILogDAO apiLogDAO = APILogDAO.getInstance();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        long requestTime = System.currentTimeMillis();

        if (request.getParameter("action").equals(EmployeeRecordsApiPerformerFactory.GET_AUTHENTICATED)) {
            successOnSecurity(request, response, request.getHeader("alias"), getFullURL(request), EmployeeRecordsApiPerformerFactory.GET_AUTHENTICATED, requestTime, getIpAddress(request));
        } else {
            String alias = request.getHeader("alias");
            String token = request.getHeader("token");
            int authenticationStatus = APIAgentDAO.getInstance().getAuthenticated(alias, token);

            if (authenticationStatus == APIAgentDTO.AUTHENTICATED) {
                successOnSecurity(request, response, alias, getFullURL(request), request.getParameter("action"), requestTime, getIpAddress(request));
            } else {
                failureOnSecurity(response, authenticationStatus, alias, getFullURL(request), request.getParameter("action"), requestTime, getIpAddress(request));
            }

        }
    }

    private void failureOnSecurity(HttpServletResponse response, int authenticationStatus, String alias, String url, String apiMethod, long requestTime, String ipAddress) throws IOException {
        ApiResponse api_response;
        String responseMessage = "";

        if (authenticationStatus==APIAgentDTO.TOKEN_EXPIRED)
            responseMessage = "Token Expired!";
        else if (authenticationStatus==APIAgentDTO.ALIAS_TOKEN_NULL_VALUE)
            responseMessage = "Kindly check the alias and token value, one of the parameters certainly contains null value!";
        else if(authenticationStatus==APIAgentDTO.NO_TOKEN_GENERATED_SO_FAR)
            responseMessage = "No token generated so far, kindly call the getAuthenticated method first!";
        else if (authenticationStatus==APIAgentDTO.TOKEN_MISMATCHED)
            responseMessage = "Token mismatched!";

        api_response = ApiResponse.makeResponseToUnauthorizedRequest(responseMessage);
        writeResponse(response, api_response);

        // Logging the API hit
        Utils.runIOTaskInSingleThread(()->()->{
            APILogDTO apiLogDTO = new APILogDTO(alias, url, apiMethod, requestTime, ipAddress, false);
            apiLogDAO.insertApiLog(apiLogDTO);
        });
    }

    private void successOnSecurity(HttpServletRequest request, HttpServletResponse response, String alias, String url, String apiMethod, long requestTime, String ipAddress) throws IOException {
        ApiResponse api_response;
        String actionType = request.getParameter("action");
        logger.debug("Employee Records API actionType = " + actionType);

        EmployeeRecordsApiPerformer performer = EmployeeRecordsApiPerformerFactory.getPerformer(actionType);

        if(performer == null) {
            api_response = ApiResponse.makeResponseToBadRequest("Action type is invalid!");
        }else {
            api_response = performer.perform(request);
        }

        writeResponse(response, api_response);

        // Logging the API hit
        Utils.runIOTaskInSingleThread(()->()->{
            APILogDTO apiLogDTO = new APILogDTO(alias, url, apiMethod, requestTime, ipAddress, true);
            apiLogDAO.insertApiLog(apiLogDTO);
        });
    }

    private void writeResponse(HttpServletResponse response, ApiResponse apiResponse) throws IOException {
        response.getWriter().write(apiResponse.getJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }

    public static String getFullURL(HttpServletRequest request) {
        StringBuilder requestURL = new StringBuilder(request.getRequestURL().toString());
        String queryString = request.getQueryString();

        if (queryString == null) {
            return requestURL.toString();
        } else {
            return requestURL.append('?').append(queryString).toString();
        }
    }

    public static String getIpAddress(HttpServletRequest request) {
        String ipAddress;
        ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }

        return ipAddress;
    }

}
