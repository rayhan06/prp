package employee_records;

/*
 * @author Md. Erfan Hossain
 * @created 15/03/2021 - 7:10 PM
 * @project parliament
 */

import common.ConnectionAndStatementUtil;
import dbm.DBMW;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

/*
    Client's responsibility to manage foreign_key
    Because foreign_key's data type varies from client to client
 */

@SuppressWarnings({"unused"})
public interface EmployeeFlatInfoDAO<T extends EmployeeFlatInfoDTO> {

    Logger logger = Logger.getLogger(EmployeeFlatInfoDAO.class);

    String addSQLQuery = "INSERT INTO %s (%s employee_records_id,employee_name_en,employee_name_bn," +
            " employee_user_name,office_unit_id,office_name_en,office_name_bn," +
            " organogram_id,organogram_name_en,organogram_name_bn," +
            " modified_by,lastmodificationtime,insert_by,insertion_date,isDeleted,id)" +
            " VALUES (%s ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    String updateSQLQuery = "UPDATE %s SET %s employee_records_id = ?,employee_name_en = ?, employee_name_bn = ?," +
            " employee_user_name = ?,office_unit_id = ?,office_name_en = ?,office_name_bn=?," +
            " organogram_id = ?, organogram_name_en = ?, organogram_name_bn = ?," +
            " modified_by = ?, lastmodificationtime = ? WHERE id = ?";

    String getByForeignKeySQLQuery = "SELECT * FROM %s WHERE %s = '%s' AND isDeleted = 0";

    String deleteByIds = "UPDATE %s SET isDeleted = 1,modified_by= '%s',lastmodificationtime = %d WHERE id IN (%s)";

    String getById = "SELECT * FROM %s WHERE id = %d";

    List<Long> deletedOrganograms = new ArrayList<>();

    /*
     ** getTableName method must return table name
     */
    String getTableName();

    /*
     ** getExtraColumnListForAddSQLQueryClause method must returns List a which contains at least foreign_key String
     * 1st index value is foreign_key
     */
    List<String> getExtraColumnListForAddSQLQueryClause();

    /*
     ** getExtraColumnListUpdateSQLQueryClause method must returns List a which contains at least foreign_key String
     */
    List<String> getExtraColumnListUpdateSQLQueryClause();

    /*
     ** setExtraPreparedStatementParams method must set at least foreign_key value
     */
    void setExtraPreparedStatementParams(PreparedStatement ps, T dto, boolean isInsert) throws Exception;

    /*
     ** Must return a object which contain foreignKey property
     */
    T buildTFromResultSet(ResultSet rs) throws SQLException;

    default void set(PreparedStatement ps, T dto, boolean isInsert) throws Exception {
        int index;
        if (isInsert) {
            index = getExtraColumnListForAddSQLQueryClause().size();
        } else {
            index = getExtraColumnListUpdateSQLQueryClause().size();
        }
        setExtraPreparedStatementParams(ps, dto, isInsert);
        ps.setObject(++index, dto.employeeRecordsId);
        ps.setObject(++index, dto.employeeNameEn);
        ps.setObject(++index, dto.employeeNameBn);
        ps.setObject(++index, dto.employeeUserName);
        ps.setObject(++index, dto.officeUnitId);
        ps.setObject(++index, dto.officeNameEn);
        ps.setObject(++index, dto.officeNameBn);
        ps.setObject(++index, dto.organogramId);
        ps.setObject(++index, dto.organogramNameEn);
        ps.setObject(++index, dto.organogramNameBn);
        ps.setObject(++index, dto.modified_by);
        ps.setObject(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, dto.insertBy);
            ps.setObject(++index, dto.insertionDate);
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, dto.iD);
    }

    default void setTFromRS(ResultSet rs, T t) throws SQLException {
        t.iD = rs.getLong("id");

        t.employeeRecordsId = rs.getLong("employee_records_id");
        t.employeeNameEn = rs.getString("employee_name_en");
        t.employeeNameBn = rs.getString("employee_name_bn");
        t.employeeUserName = rs.getString("employee_user_name");

        t.officeUnitId = rs.getLong("office_unit_id");
        t.officeNameEn = rs.getString("office_name_en");
        t.officeNameBn = rs.getString("office_name_bn");

        t.organogramId = rs.getLong("organogram_id");
        t.organogramNameEn = rs.getString("organogram_name_en");
        t.organogramNameBn = rs.getString("organogram_name_bn");

        t.modified_by = rs.getString("modified_by");
        t.lastModificationTime = rs.getLong("lastmodificationtime");

        t.insertBy = rs.getString("insert_by");
        t.insertionDate = rs.getLong("insertion_date");

        t.isDeleted = rs.getInt("isDeleted");
    }

    default EmployeeFlatInfoDTO buildEmployeeFlatInfoDto(ResultSet rs) {
        try {
            EmployeeFlatInfoDTO dto = new EmployeeFlatInfoDTO();
            dto.iD = rs.getLong("id");
            dto.employeeRecordsId = rs.getLong("employee_records_id");
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.organogramId = rs.getLong("organogram_id");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    default T buildTFromRS(ResultSet rs) {
        try {
            T t = buildTFromResultSet(rs);
            setTFromRS(rs, t);
            return t;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    default void addEmployeeFlatInfoDTO(T dto) throws Exception {
        List<String> extraParamNames = getExtraColumnListForAddSQLQueryClause();

        StringBuilder extraParamBuilder = new StringBuilder();
        StringBuilder extraQuestionMarksBuilder = new StringBuilder();
        for (String param : extraParamNames) {
            extraParamBuilder.append(param).append(",");
            extraQuestionMarksBuilder.append("?").append(",");
        }
        String extraParam = extraParamBuilder.toString();
        String extraQuestionMarks = extraQuestionMarksBuilder.toString();

        String sql = String.format(addSQLQuery, getTableName(), extraParam, extraQuestionMarks);
        dto.iD = DBMW.getInstance().getNextSequenceId(getTableName());
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                setMetaData(dto);
                set(ps, dto, true);
                ps.execute();
            } catch (Exception e) {
                logger.error(e);
            }
        }, sql);
    }

    default void updateEmployeeFlatInfoDTO(T dto) {
        List<String> extraParamNames = getExtraColumnListUpdateSQLQueryClause();

        StringBuilder extraParamBuilder = new StringBuilder();
        for (String param : extraParamNames) {
            extraParamBuilder.append(param).append(" = ?, ");
        }
        String extraParam = extraParamBuilder.toString();

        String sql = String.format(updateSQLQuery, getTableName(), extraParam);
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                setMetaData(dto);
                set(ps, dto, false);
                ps.executeUpdate();
            } catch (Exception e) {
                logger.error(e);
            }
        }, sql);
    }

    default void setMetaData(T dto) {
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(dto.employeeRecordsId);
        dto.employeeNameEn = employeeRecordsDTO.nameEng;
        dto.employeeNameBn = employeeRecordsDTO.nameBng;
        dto.employeeUserName = employeeRecordsDTO.employeeNumber;

        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(dto.officeUnitId);
        if (officeUnitsDTO != null) {
            dto.officeNameEn = officeUnitsDTO.unitNameEng;
            dto.officeNameBn = officeUnitsDTO.unitNameBng;
        }


        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(dto.organogramId);
        if (officeUnitOrganograms != null) {
            dto.organogramNameEn = officeUnitOrganograms.designation_eng;
            dto.organogramNameBn = officeUnitOrganograms.designation_bng;
        }

    }

    default List<T> getByForeignKey(String foreignKey) {
        String sql = String.format(getByForeignKeySQLQuery, getTableName(), getExtraColumnListForAddSQLQueryClause().get(0), foreignKey);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildTFromRS);
    }

    default void deleteByIds(List<Long> idList, String requestBy, long requestTime) {
        String ids = idList.stream().map(String::valueOf).collect(Collectors.joining(","));
        String sql = String.format(deleteByIds, getTableName(), requestBy, requestTime, ids);
        logger.debug("sql : " + sql);
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            try {
                st.executeUpdate(sql);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
    }

    default void setDeletedOrganograms(List<Long> deletedOrganogramIds) {
        deletedOrganograms.clear();
        deletedOrganogramIds.forEach(e -> {
            deletedOrganograms.add(e);
        });
    }

    default List<Long> getDeletedOrganograms() {
        return deletedOrganograms;
    }
    /*
     *****
     * Delete those items which are not present in latest list
     * Return a boolean map of list
     * key : true, list-> already added items list
     * key: false, list-> new items list.
     *****
     */


    default Map<Boolean, List<EmpOfficeModel>> deleteDeletedInfoAndReturnAMap(List<EmpOfficeModel> modelList, String foreignKey, String requestBy, long requestTime, Boolean setDeletedOrganograms) {
        String sql = String.format(getByForeignKeySQLQuery, getTableName(), getExtraColumnListForAddSQLQueryClause().get(0), foreignKey);
        List<EmployeeFlatInfoDTO> oldDTOList = ConnectionAndStatementUtil.getListOfT(sql, this::buildEmployeeFlatInfoDto);
        if (modelList == null || modelList.size() == 0) {
            if (oldDTOList != null && oldDTOList.size() > 0) {
                List<Long> ids = oldDTOList.stream()
                        .map(e -> e.iD)
                        .collect(Collectors.toList());
                if (setDeletedOrganograms) {
                    List<Long> deletedOrganograms = oldDTOList.stream()
                            .map(e -> e.organogramId)
                            .collect(Collectors.toList());
                    setDeletedOrganograms(deletedOrganograms);
                }
                deleteByIds(ids, requestBy, requestTime);
            }
            return new HashMap<>();
        } else if (oldDTOList == null || oldDTOList.size() == 0) {
            Map<Boolean, List<EmpOfficeModel>> booleanListMap = new HashMap<>();
            booleanListMap.put(false, modelList);
            if (setDeletedOrganograms) {
                List<Long> deletedOrganograms = new ArrayList<>();
                setDeletedOrganograms(deletedOrganograms);
            }
            return booleanListMap;
        } else {
            Map<String, Long> map = oldDTOList.stream()
                    .collect(Collectors.toMap(dto -> dto.employeeRecordsId + "," + dto.officeUnitId + "," + dto.organogramId, dto -> dto.iD));
            Set<String> oldSet = map.keySet();

            Set<String> latestSet = modelList.stream()
                    .map(model -> model.employeeRecordId + "," + model.officeUnitId + "," + model.organogramId)
                    .collect(Collectors.toSet());

            List<Long> deletedIds = oldDTOList.stream()
                    .filter(dto -> !latestSet.contains(dto.employeeRecordsId + "," + dto.officeUnitId + "," + dto.organogramId))
                    .map(dto -> map.get(dto.employeeRecordsId + "," + dto.officeUnitId + "," + dto.organogramId))
                    .collect(Collectors.toList());
            if (setDeletedOrganograms) {
                List<Long> deletedOrganograms = oldDTOList.stream()
                        .filter(dto -> !latestSet.contains(dto.employeeRecordsId + "," + dto.officeUnitId + "," + dto.organogramId))
                        .map(dto -> dto.organogramId)
                        .collect(Collectors.toList());
                setDeletedOrganograms(deletedOrganograms);
            }
            deleteByIds(deletedIds, requestBy, requestTime);

            return latestSet.stream()
                    .collect(Collectors.partitioningBy(oldSet::contains,
                            Collectors.mapping(e -> {
                                String[] tokens = e.split(",");
                                EmpOfficeModel model = new EmpOfficeModel();
                                if (map.containsKey(e)) {
                                    model.rowId = map.get(e);
                                }
                                model.employeeRecordId = Long.parseLong(tokens[0]);
                                model.officeUnitId = Long.parseLong(tokens[1]);
                                model.organogramId = Long.parseLong(tokens[2]);
                                return model;
                            }, Collectors.toList())));
        }
    }

    default Map<Boolean, List<EmpOfficeModel>> deleteDeletedInfoAndReturnAMap(List<EmpOfficeModel> modelList, String foreignKey, String requestBy, long requestTime) {
        return deleteDeletedInfoAndReturnAMap(modelList, foreignKey, requestBy, requestTime, false);
    }

    default T getById(long id) {
        String sql = String.format(getById, getTableName(), id);
        return ConnectionAndStatementUtil.getT(sql, this::buildTFromRS);
    }


}
