package employee_records;

import committees_mapping.Committees_mappingDTO;
import pb.CatRepository;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommitteesInfoModel {
    public long committeesNameId;
    public String designation;
    public String startDate;
    public String endDate;


    public CommitteesInfoModel(Committees_mappingDTO dto) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

        committeesNameId = dto.committeesId;
        designation = CatRepository.getName("English", "committees_role", dto.committeesRoleCat);
        startDate = simpleDateFormat.format(new Date(dto.startDate));
        endDate = simpleDateFormat.format(new Date(dto.endDate));
    }

    public CommitteesInfoModel() {
        System.out.println("CommitteesInfoModel Called!");
    }
}
