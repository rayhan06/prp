package employee_records;

import election_details.Election_detailsDAO;
import election_details.Election_detailsDTO;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ParliamentDetailsModel {
    int parliamentNumber;
    String electionDate;
    String oathDate;
    String gazetteDate;
    String parliamentLastDate;

    public ParliamentDetailsModel(Election_detailsDTO dto) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        this.parliamentNumber = dto.parliamentNumber;
        this.electionDate = dateFormat.format(new Date(dto.electionDate));
        this.oathDate = dateFormat.format(new Date(dto.generralOathDate));
        this.gazetteDate = dateFormat.format(new Date(dto.gazetteDate));
        this.parliamentLastDate = dateFormat.format(new Date(dto.parliamentLastDate));
    }
}
