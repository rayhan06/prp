package employee_records;

import common.ApiResponse;
import employee_family_info.Employee_family_infoDAO;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import user.UserRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

public class GetGenderTable implements EmployeeRecordsApiPerformer{
    @Override
    public ApiResponse perform(HttpServletRequest request) {
        try {
            List<CategoryLanguageModel> genderList = CatRepository.getInstance().getCategoryLanguageModelList("gender");
            List<GenderModel> genderTable = genderList.stream().map(dto -> new GenderModel(dto.categoryValue, dto.englishText, dto.banglaText))
                    .collect(Collectors.toList());
            return genderTable.size()==0 ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                    ApiResponse.makeSuccessResponse(genderTable, "Successfully found the data.");
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
        }

    }
}