package employee_records;

import common.ApiResponse;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class GetEmployeeRecordPageByPagePerformer implements EmployeeRecordsApiPerformer {
    public final Logger logger = Logger.getLogger(GetEmployeeRecordPageByPagePerformer.class);
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    @Override
    public ApiResponse perform(HttpServletRequest request) {

        ApiResponse apiResponse;

        Employee_recordsDAO employee_recordsDAO = new Employee_recordsDAO();
        List<EmployeeMinimumInfoModel> employeeModels = null;

        try {

//            perPageModels.employeeMinimumInfoModelList = employee_recordsDAO.getEmployeeRecordsViaPageNumber(pageNumber, recordPerPage).stream().
//                    map(dto -> new EmployeeMinimumInfoModel(dto.iD, dto.nameEng, dto.nameBng, dto.fatherNameEng, dto.fatherNameBng,
//                            dateFormat.format(new Date(dto.dateOfBirth)), dto.photo)).collect(Collectors.toList());
//
//
            employeeModels = Employee_recordsRepository.getInstance().getEmployee_recordsList().stream().
                    map(dto -> new EmployeeMinimumInfoModel(dto.employeeNumber, dto.nameEng, dto.nameBng, dto.fatherNameEng, dto.fatherNameBng,
                    dateFormat.format(new Date(dto.dateOfBirth)), dto.photo)).collect(Collectors.toList());

            apiResponse = ApiResponse.makeSuccessResponse(employeeModels, "Successfully found the entries.");



        } catch (Exception e) {
            e.printStackTrace();
            apiResponse = ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
        }

        return apiResponse;
    }
}
