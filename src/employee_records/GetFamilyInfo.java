package employee_records;

import common.ApiResponse;
import employee_family_info.Employee_family_infoDAO;
import user.UserRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

public class GetFamilyInfo implements EmployeeRecordsApiPerformer{
    @Override
    public ApiResponse perform(HttpServletRequest request) {
        String userName = request.getParameter("employee_record_id");
        long employeeRecordId;

        try {
            employeeRecordId = Employee_recordsRepository.getInstance().getEmployeeRecordIdByUsername(userName);
            if (employeeRecordId==0)
                return ApiResponse.makeResponseToResourceNotFound("No Record found with employee_record_id: "+userName+"!");
        } catch (Exception ex) {
            return ApiResponse.makeResponseToResourceNotFound("No Record found with employee_record_id: "+userName+"!");
        }

        try {
            try {
                List<EmployeeFamilyModel> employeeFamilyModels = Employee_family_infoDAO.getInstance().getByEmployeeId(employeeRecordId)
                        .stream()
                        .map(EmployeeFamilyModel::new)
                        .collect(Collectors.toList());
                return employeeFamilyModels.size()==0 ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                        ApiResponse.makeSuccessResponse(employeeFamilyModels, "Successfully found the data.");
            } catch (Exception e) {
                e.printStackTrace();
                return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
            }
        } catch (NumberFormatException nfe) {
            return ApiResponse.makeResponseToBadRequest("Kindly check the employee_record_id parameter provided!");
        }
    }
}