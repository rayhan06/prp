package employee_records;

import common.ApiResponse;
import election_details.Election_detailsDTO;
import election_details.Election_detailsRepository;
import org.apache.log4j.Logger;
import parliament_session.Parliament_sessionDTO;
import parliament_session.Parliament_sessionRepository;
import political_party.Political_partyDTO;
import political_party.Political_partyRepository;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class GetPoliticalPartyDetailsPerformer implements EmployeeRecordsApiPerformer {
    public final Logger logger = Logger.getLogger(GetMpListByParliamentNumberPerformer.class);
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    @Override
    public ApiResponse perform(HttpServletRequest request) {

        ApiResponse apiResponse;

        List<PoliticalPartyDetailsModel> politicalPartyDetailsModels = new ArrayList<>();

        try {

            for (Political_partyDTO dto: Political_partyRepository.getInstance().getPolitical_partyList()) {
                politicalPartyDetailsModels.add(new PoliticalPartyDetailsModel(dto));
            }

            apiResponse = politicalPartyDetailsModels.size()==0 ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                    ApiResponse.makeSuccessResponse(politicalPartyDetailsModels, "Successfully found the data.");


        } catch (Exception e) {
            e.printStackTrace();
            return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
        }

        return apiResponse;
    }
}
