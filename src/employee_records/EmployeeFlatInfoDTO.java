package employee_records;

import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import util.CommonDTO;

public class EmployeeFlatInfoDTO extends CommonDTO {
    public long employeeRecordsId = 0;
    public String employeeNameEn = "";
    public String employeeNameBn = "";
    public String employeeUserName = "";
    public String nid = "";

    public long officeUnitId = 0;
    public String officeNameEn = "";
    public String officeNameBn = "";


    public long organogramId = 0;
    public String organogramNameEn = "";
    public String organogramNameBn = "";

    public long insertionDate = 0;
    public String insertBy;
    public String modified_by;

    public String getCommaFormattedInfo(String language){
        if(language.equalsIgnoreCase("English")){
            return "<b>".concat(this.employeeNameEn).concat("</b><br>")
                                      .concat(this.organogramNameEn).concat("<br>")
                                      .concat(this.officeNameEn);
        }else{
            return "<b>".concat(this.employeeNameBn).concat("</b><br>")
                                      .concat(this.organogramNameBn).concat("<br>")
                                      .concat(this.officeNameBn);
        }
    }

    @Override
    public String toString() {
        return "EmployeeFlatInfoDTO{" +
                "employeeRecordsId=" + employeeRecordsId +
                ", employeeNameEn='" + employeeNameEn + '\'' +
                ", employeeNameBn='" + employeeNameBn + '\'' +
                ", employeeUserName='" + employeeUserName + '\'' +
                ", officeUnitId=" + officeUnitId +
                ", officeNameEn='" + officeNameEn + '\'' +
                ", officeNameBn='" + officeNameBn + '\'' +
                ", organogramId=" + organogramId +
                ", organogramNameEn='" + organogramNameEn + '\'' +
                ", organogramNameBn='" + organogramNameBn + '\'' +
                ", insertionDate=" + insertionDate +
                ", insertBy='" + insertBy + '\'' +
                ", modified_by='" + modified_by + '\'' +
                '}';
    }

    public String getIdsStrInJsonString(){
        return String.format("{employeeRecordId: '%s', officeUnitId: '%s', organogramId: '%s'}",
                employeeRecordsId,officeUnitId,organogramId);
    }

    public static EmployeeFlatInfoDTO getFlatInfoOfDefaultOffice(long employeeRecordId, String errorMessageLanguage) throws Exception{
        boolean isEng = "English".equalsIgnoreCase(errorMessageLanguage);
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employeeRecordId);
        if(employeeOfficeDTO == null){
            throw new Exception(
                    isEng ? "No routine responsibility found!" : "কোন নিয়মিত দায়িত্ব পাওয়া যায়নি"
            );
        }

        Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId);
        if(employee_recordsDTO == null){
            throw new Exception(
                    isEng ? "Employee Record not found!" : "কর্মকর্তা তথ্য পাওয়া যায়নি"
            );
        }
        EmployeeFlatInfoDTO employeeFlatInfoDTO =  new EmployeeFlatInfoDTO();
        employeeFlatInfoDTO.employeeRecordsId = employee_recordsDTO.iD;
        employeeFlatInfoDTO.employeeNameEn = employee_recordsDTO.nameEng;
        employeeFlatInfoDTO.employeeNameBn = employee_recordsDTO.nameBng;
        employeeFlatInfoDTO.employeeUserName = employee_recordsDTO.employeeNumber;
        employeeFlatInfoDTO.nid = employee_recordsDTO.nid;


        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
        if(officeUnitsDTO == null){
            throw new Exception(
                    isEng ? "Employee Office Information not found!" : "কর্মকর্তা অফিসের তথ্য পাওয়া যায়নি"
            );
        }
        employeeFlatInfoDTO.officeUnitId = officeUnitsDTO.iD;
        employeeFlatInfoDTO.officeNameEn = officeUnitsDTO.unitNameEng;
        employeeFlatInfoDTO.officeNameBn = officeUnitsDTO.unitNameBng;

        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        if(officeUnitOrganograms == null){
            throw new Exception(
                    isEng ? "Employee Designation Information not found!" : "কর্মকর্তা পদবীর তথ্য পাওয়া যায়নি"
            );
        }
        employeeFlatInfoDTO.organogramId = officeUnitOrganograms.id;
        employeeFlatInfoDTO.organogramNameEn = officeUnitOrganograms.designation_eng;
        employeeFlatInfoDTO.organogramNameBn = officeUnitOrganograms.designation_bng;

        return employeeFlatInfoDTO;
    }
}
