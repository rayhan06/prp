package employee_records;

import common.ApiResponse;
import common.NameDTO;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import religion.ReligionRepository;
import user.UserDAO;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GetEmpidUsernameList implements EmployeeRecordsApiPerformer{
    public final Logger logger = Logger.getLogger(GetEmpidUsernameList.class);

    @Override
    public ApiResponse perform(HttpServletRequest request) {
        try {

            int isMP;

            try {
                isMP = Integer.parseInt(request.getParameter("isMP"));
            } catch (NumberFormatException nfe) {
                return ApiResponse.makeResponseToBadRequest("Kindly check the isMP parameter provided!");
            }

            List<EmpidUsernameModel> empidUsernameModels = new ArrayList<>();
            UserDAO userDAO = new UserDAO();
            try {
//                empidUsernameModels = userDAO.getAllUsers(false).stream().
//                        map(dto -> new EmpidUsernameModel(dto)).collect(Collectors.toList());
                List<UserDTO> userDTOList = userDAO.getALlUsersForAPI();

                for (UserDTO dto: userDTOList) {
                    if (Employee_recordsRepository.getInstance().getById(dto.employee_record_id).isMP==isMP) {
                        empidUsernameModels.add(new EmpidUsernameModel(dto));
                    }
                }
            } catch (Exception ex) {
                logger.debug("got exception while fetching data from employee_records and users table!");
            }


            return empidUsernameModels.size()==0 ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                    ApiResponse.makeSuccessResponse(empidUsernameModels, "Successfully found the data.");
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
        }

    }
}

