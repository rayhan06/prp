package employee_records;

import common.ApiResponse;
import employee_education_info.GetDegreePerformer;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class DeleteEmployeeRecordByIdPerformer implements EmployeeRecordsApiPerformer {
    Logger logger = Logger.getLogger(DeleteEmployeeRecordByIdPerformer.class);
    @Override
    public ApiResponse perform(HttpServletRequest request) {
        long empId;

        try {
            empId = Long.parseLong(request.getParameter("empId"));
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            return ApiResponse.makeErrorResponse("Please, check the parameters provided!");
        }

        ApiResponse apiResponse;

        Employee_recordsDAO employee_recordsDAO = new Employee_recordsDAO();

        try {

            Employee_recordsDTO employee_recordsDTO = employee_recordsDAO.getEmployee_recordsDTOByID(empId);

            if (employee_recordsDTO == null || employee_recordsDTO.isDeleted) {
                apiResponse = ApiResponse.makeErrorResponse("No Record found with empID: "+empId+"!");
            } else {
                employee_recordsDAO.deleteEmployee_recordsByID(empId);
                apiResponse = ApiResponse.makeSuccessResponse("Successfully deleted the Employee.");
            }

        } catch (Exception e) {
            e.printStackTrace();
            apiResponse = ApiResponse.makeErrorResponse("Exception is Occurred!");
        }

        return apiResponse;
    }
}
