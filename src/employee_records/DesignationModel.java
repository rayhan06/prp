package employee_records;

import office_unit_organograms.OfficeUnitOrganograms;

public class DesignationModel {
    public long designationId = -1;
    public String designation_eng = "";
    public String designation_bng = "";

    public DesignationModel(OfficeUnitOrganograms dto) {
        designationId = dto.id;
        designation_eng = dto.designation_eng;
        designation_bng = dto.designation_bng;
    }
}
