package employee_records;

import election_details.Election_detailsDTO;
import parliament_session.Parliament_sessionDTO;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ParliamentSessionModel {
    int sessionNumber;
    String startDate;
    String endDate;

    public ParliamentSessionModel(Parliament_sessionDTO dto) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        this.sessionNumber = dto.sessionNumber;
        this.startDate = dateFormat.format(new Date(dto.startDate));
        this.endDate = dateFormat.format(new Date(dto.endDate));
    }
}
