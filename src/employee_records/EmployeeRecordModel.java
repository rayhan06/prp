package employee_records;

import election_constituency.Election_constituencyRepository;
import election_details.Election_detailsRepository;
import election_wise_mp.Election_wise_mpDTO;
import election_wise_mp.Election_wise_mpRepository;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import geolocation.GeoDistrictRepository;
import geolocation.GeoLocationRepository;
import geolocation.GeoLocationUtils;
import grade_wise_pay_scale.Grade_wise_pay_scaleRepository;
import nationality.NationalityRepository;
import org.apache.commons.codec.binary.Base64;
import pb.CatRepository;
import pb.CommonDAO;
import political_party.Political_partyRepository;
import religion.ReligionRepository;
import util.StringUtils;

public class EmployeeRecordModel {
    public long iD = 0;
    public String nameEng = "";
    public String nameBng = "";
    public String fatherNameEng = "";
    public String fatherNameBng = "";
    public String motherNameEng = "";
    public String motherNameBng = "";
    public String nid = "";
    public String homeDistrict = "";
    public String dateOfBirth = "";
    public String birthDistrict = "";
    public String identificationMark = "";
    public String maritalStatus = "";
    public String passportNo = "";
    public String nationality = "";
    public String bcn = "";
    public String gender = "";
    public String presentAddressEng = "";
    public String presentAddressBng = "";
    public String permanentAddressEng = "";
    public String permanentAddressBng = "";
    public String personalMobile = "";
    public String officePhoneNumber = "";
    public String personalEml = "";
    public String officialEml = "";
    public String faxNumber = "";
    public String employeeCatagory = "";
    public String employeeClass = "";
    public String height = "";
    public String passportIssueDate = "";
    public String passportExpiryDate = "";
    public String marriageDate = "";
    public String bloodGroup = "";
    public String religion = "";
    public String alternativeMobile = "";
    public String officePhoneExtension = "";
    public String freedomFighterInfo = "";

    public String joiningDate = "";
    public String lprDate = "";
    public String retirementDate = "";
    public String govtJobJoiningDate = "";

    public String electionConstituency = "";
    public String officerType = "";

    public int isMp = 0;
    public String photo = "";
    public String signature = "";
    public String parliamentNumber = "";
    public String politicalParty = "";
    public String nidFrontPhoto = "";
    public String nidBackPhoto = "";

    public String professionOfMP = "";
    public String addressOfMP = "";
    public String alternateImageOfMP = "";
    public String provisionPeriod = "";
    public String provisionEndDate = "";

    public String currentOffice = "";
    public String deptAndDesignation = "";
    public String payScale = "";

}

class ModelUtil {
    public static EmployeeRecordModel fromDTO(Employee_recordsDTO employee_recordsDTO, String language) {
        EmployeeRecordModel model = new EmployeeRecordModel();
        boolean isLanguageEnglish = language.equalsIgnoreCase("English");

        model.iD = employee_recordsDTO.iD;
        model.isMp = employee_recordsDTO.isMP;
        model.nameEng = employee_recordsDTO.nameEng;
        model.nameBng = employee_recordsDTO.nameBng;

        model.fatherNameEng = employee_recordsDTO.fatherNameEng;
        model.fatherNameBng = employee_recordsDTO.fatherNameBng;

        model.motherNameEng = employee_recordsDTO.motherNameEng;
        model.motherNameBng = employee_recordsDTO.motherNameBng;

        model.nid = isLanguageEnglish ? employee_recordsDTO.nid : StringUtils.convertToBanNumber(employee_recordsDTO.nid);
        model.homeDistrict = GeoLocationRepository.getInstance().getText(language, employee_recordsDTO.homeDistrict);
        model.dateOfBirth = StringUtils.getFormattedDate(language, employee_recordsDTO.dateOfBirth);
        model.birthDistrict = GeoDistrictRepository.getInstance().getText(language, employee_recordsDTO.birthDistrict);
        model.identificationMark = employee_recordsDTO.identificationMark;
        model.maritalStatus = CatRepository.getInstance().getText(language, "marital_status", employee_recordsDTO.maritalStatus);
        model.passportNo = isLanguageEnglish ? employee_recordsDTO.passportNo : StringUtils.convertToBanNumber(employee_recordsDTO.passportNo);
        model.nationality = NationalityRepository.getInstance().getText(language, employee_recordsDTO.nationalityType);
        model.bcn = isLanguageEnglish ? employee_recordsDTO.bcn : StringUtils.convertToBanNumber(employee_recordsDTO.bcn);
        model.gender = CatRepository.getInstance().getText(language, "gender", employee_recordsDTO.gender);
        model.presentAddressEng = GeoLocationUtils.getGeoLocationString(employee_recordsDTO.presentAddress, "English");
        model.presentAddressBng = GeoLocationUtils.getGeoLocationString(employee_recordsDTO.presentAddressBng, "Bangla");

        EmployeeOfficeDTO employeeOfficeDTO =
                EmployeeOfficeRepository.getInstance()
                                        .getByEmployeeRecordIdIsDefault(employee_recordsDTO.iD);
        if (employeeOfficeDTO != null) {
            model.payScale = Grade_wise_pay_scaleRepository.getInstance().getText(language, employeeOfficeDTO.gradeTypeLevel);
        }

        model.currentOffice = CommonDAO.getName(language, "other_office", employee_recordsDTO.currentOffice);
        model.deptAndDesignation = employee_recordsDTO.currentDesignation;
        String dept = CommonDAO.getName(language, "other_office_department", employee_recordsDTO.otherOfficeDeptType);
        if (!dept.equalsIgnoreCase("")) {
            model.deptAndDesignation += ", " + dept;
        }

        if (employee_recordsDTO.personalMobile != null && employee_recordsDTO.personalMobile.length() >= 2) {
            model.personalMobile = isLanguageEnglish ?
                                   employee_recordsDTO.personalMobile.substring(2)
                                                     : StringUtils.convertToBanNumber(employee_recordsDTO.personalMobile).substring(2);
        }

        model.officePhoneNumber = isLanguageEnglish ?
                                  employee_recordsDTO.officePhoneNumber
                                                    : StringUtils.convertToBanNumber(employee_recordsDTO.officePhoneNumber);

        model.faxNumber = isLanguageEnglish ?
                          employee_recordsDTO.faxNumber
                                            : StringUtils.convertToBanNumber(employee_recordsDTO.faxNumber);

        model.personalEml = employee_recordsDTO.personalEml;
        model.officialEml = employee_recordsDTO.officialEml;

        if (employee_recordsDTO.height != 0.0) {
            model.height = StringUtils.convertBanglaIfLanguageIsBangla(language, String.format("%.2f", employee_recordsDTO.height));
        }

        model.marriageDate = StringUtils.getFormattedDate(language, employee_recordsDTO.marriageDate);
        model.passportIssueDate = StringUtils.getFormattedDate(language, employee_recordsDTO.passportIssueDate);
        model.passportExpiryDate = StringUtils.getFormattedDate(language, employee_recordsDTO.passportExpiryDate);
        model.bloodGroup = CatRepository.getInstance().getText(language, "blood_group", employee_recordsDTO.bloodGroup);
        model.religion = ReligionRepository.getInstance().getText(language, employee_recordsDTO.religion);

        model.permanentAddressEng = GeoLocationUtils.getGeoLocationString(employee_recordsDTO.permanentAddress, "English");
        model.permanentAddressBng = GeoLocationUtils.getGeoLocationString(employee_recordsDTO.permanentAddressBng, "Bangla");

        if (employee_recordsDTO.alternativeMobile.length() > 2) {
            model.alternativeMobile =
                    isLanguageEnglish ? employee_recordsDTO.alternativeMobile.substring(2)
                                      : StringUtils.convertToBanNumber(employee_recordsDTO.alternativeMobile).substring(2);
        }

        model.officePhoneExtension = isLanguageEnglish ? employee_recordsDTO.officePhoneExtension : StringUtils.convertToBanNumber(employee_recordsDTO.officePhoneExtension);
        model.freedomFighterInfo = CatRepository.getInstance().commaSeparateFreedomFighter(employee_recordsDTO.freedomFighterInfo, language);

        model.joiningDate = StringUtils.getFormattedDate(language, employee_recordsDTO.joiningDate);
        model.lprDate = StringUtils.getFormattedDate(language, employee_recordsDTO.lprDate);
        model.retirementDate = StringUtils.getFormattedDate(language, employee_recordsDTO.retirementDate);
        model.govtJobJoiningDate = StringUtils.getFormattedDate(language, employee_recordsDTO.govtJobJoiningDate);

        byte[] encodeBase64Photo = Base64.encodeBase64(employee_recordsDTO.photo);
        model.photo = "data:image/jpg;base64," + (encodeBase64Photo != null ? new String(encodeBase64Photo) : "");

        encodeBase64Photo = Base64.encodeBase64(employee_recordsDTO.signature);
        model.signature = "data:image/jpg;base64," + (encodeBase64Photo != null ? new String(encodeBase64Photo) : "");

        encodeBase64Photo = Base64.encodeBase64(employee_recordsDTO.nidFrontPhoto);
        model.nidFrontPhoto = "data:image/jpg;base64," + (encodeBase64Photo != null ? new String(encodeBase64Photo) : "");

        encodeBase64Photo = Base64.encodeBase64(employee_recordsDTO.nidBackPhoto);
        model.nidBackPhoto = "data:image/jpg;base64," + (encodeBase64Photo != null ? new String(encodeBase64Photo) : "");

        if (employee_recordsDTO.isMP == 1) {
            Election_wise_mpDTO electionWiseMpDTO = Election_wise_mpRepository.getInstance().getDTOByID(employee_recordsDTO.electionWiseMpId);
            if (electionWiseMpDTO != null) {
                model.parliamentNumber = Election_detailsRepository.getInstance().getText(electionWiseMpDTO.electionDetailsId, isLanguageEnglish);
                model.politicalParty = Political_partyRepository.getInstance().getText(electionWiseMpDTO.politicalPartyId, isLanguageEnglish);
                model.electionConstituency = Election_constituencyRepository.getInstance().getText(electionWiseMpDTO.electionConstituencyId, isLanguageEnglish);
                model.professionOfMP = electionWiseMpDTO.professionOfMP;
                model.addressOfMP = electionWiseMpDTO.addressOfMP;
                encodeBase64Photo = Base64.encodeBase64(electionWiseMpDTO.alternateImageOfMP);
                model.alternateImageOfMP = "data:image/jpg;base64," + (encodeBase64Photo != null ? new String(encodeBase64Photo) : "");
            }

        } else if (employee_recordsDTO.isMP == 2) {
            model.employeeCatagory = CatRepository.getInstance().getText(language, "employment", employee_recordsDTO.employmentType);
            model.employeeClass = CatRepository.getInstance().getText(language, "employee_class", employee_recordsDTO.employeeClass);
            model.officerType = CatRepository.getInstance().getText(language, "emp_officer", employee_recordsDTO.officerTypeCat);
            model.provisionPeriod = employee_recordsDTO.provisionPeriod > 0 ?
                                    isLanguageEnglish ? String.valueOf(employee_recordsDTO.provisionPeriod) :
                                    StringUtils.convertToBanNumber(String.valueOf(employee_recordsDTO.provisionPeriod)) : "";
            model.provisionEndDate = employee_recordsDTO.provisionPeriod > 0 ?
                                     StringUtils.getFormattedDate(language, employee_recordsDTO.provisionEndDate) : "";
        }
        return model;
    }
}
