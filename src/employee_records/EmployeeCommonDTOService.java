package employee_records;

import common.CommonDTOService;
import common.ConnectionAndStatementUtil;
import util.CommonDTO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Deprecated
public interface EmployeeCommonDTOService<T extends CommonDTO> extends CommonDTOService<T> {
    String getByEmployeeId = "SELECT * FROM %s WHERE employee_records_id = %d AND isDeleted = 0 ORDER BY lastModificationTime DESC";

    String getGetByEmployeeIdAndId = "SELECT * FROM %s WHERE employee_records_id = %d AND ID = %d  AND isDeleted = 0";

    String getIdsByEmployeeIdAndIds = "SELECT * FROM {tableName} WHERE employee_records_id = {empId} AND ID IN (%s) AND isDeleted = 0";

    default List<T> getByEmployeeId(long employeeId) {
        return getDTOs(String.format(getByEmployeeId,getTableName(),employeeId));
    }

    default T getByEmployeeIdAndId(long employeeId, long id){
        String sql = String.format(getGetByEmployeeIdAndId,getTableName(),employeeId,id);
        return ConnectionAndStatementUtil.getT(sql,this::buildObjectFromResultSet);
    }

    default <R> List<R> getModelList(long employeeId, Function<T, R> transform) {
        return getByEmployeeId(employeeId)
                .stream()
                .map(transform)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    default List<Long> getIdsByEmployeeIdAndIds(long employeeId,String[]idArrays){
        if(idArrays == null || idArrays.length == 0){
            return new ArrayList<>();
        }
        String sql = getIdsByEmployeeIdAndIds.replace("{tableName}",getTableName()).replace("{empId}",String.valueOf(employeeId));
        List<String> idList = Stream.of(idArrays)
                .collect(Collectors.toList());
        return ConnectionAndStatementUtil.getDTOList(sql,idList,rs->{
            try {
                return rs.getLong("ID");
            } catch (SQLException ex) {
                logger.error(ex);
                return null;
            }
        });
    }

    default List<Long> getIdsByEmployeeIdAndIds(long employeeId,List<? extends Number>idList){
        if(idList == null || idList.size() == 0){
            return new ArrayList<>();
        }
        String sql = getIdsByEmployeeIdAndIds.replace("{tableName}",getTableName()).replace("{empId}",String.valueOf(employeeId));
        return ConnectionAndStatementUtil.getDTOListByNumbers(sql,idList,rs->{
            try {
                return rs.getLong("ID");
            } catch (SQLException ex) {
                logger.error(ex);
                return null;
            }
        });
    }
}