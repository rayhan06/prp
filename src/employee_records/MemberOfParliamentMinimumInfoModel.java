package employee_records;

import committees_mapping.Committees_mappingDAO;
import committees_mapping.Committees_mappingDTO;
import election_constituency.Election_constituencyRepository;
import ministry_office.Ministry_officeDAO;
import ministry_office_mapping.Ministry_office_mappingDAO;
import ministry_office_mapping.Ministry_office_mappingDTO;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import pb.CatRepository;
import user.UserRepository;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class MemberOfParliamentMinimumInfoModel {
    public String empRecordId = "-1";
    public String nameEng = "";
    public String nameBng = "";
    public String fatherNameEng = "";
    public String fatherNameBng = "";
    public String dateOfBirth = "";
    public int parliamentNumber = 0;
    public long constituencyNumber = 0;
    public String constituencyNameEng = "";
    public String constituencyNameBng = "";
    public String politicalPartyNameEng = "";
    public String politicalPartyNameBng = "";
    public String startDate = "";
    public String endDate = "";
    public String mpStatus = "";
    public byte[] photo = null;

    //    New field for MP
    public String additionalMobile = "";
    public String additionalEmail = "";  // will implement later
//    boolean isVacant = false;
    public int term = 1;
    public String mpType = "";
    boolean isMinister = false;
//    String parliamentDesignationEng = "";  // will implement later
//    String parliamentDesignationBng = "";  // will implement later
    List<CommitteesInfoModel> committeesInfoModels;
    List<MinistryInfoModel> ministryInfoModels;


    public MemberOfParliamentMinimumInfoModel(long empRecordId, int parliamentNumber, long constituencyNumber, String constituencyNameEng, String constituencyNameBng,
                                              String politicalPartyNameEng, String politicalPartyNameBng, long startDate, long endDate, List<Committees_mappingDTO> committees_mappingDTOS, List<Ministry_office_mappingDTO> ministry_office_mappingDTOS, int mpStatus) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().getById(empRecordId);

        this.empRecordId = employee_recordsDTO.employeeNumber;
        this.nameEng = employee_recordsDTO.nameEng == null ? "" : employee_recordsDTO.nameEng;
        this.nameBng = employee_recordsDTO.nameBng == null ? "" : employee_recordsDTO.nameBng;
        this.fatherNameEng = employee_recordsDTO.fatherNameEng == null ? "" : employee_recordsDTO.fatherNameEng;
        this.fatherNameBng = employee_recordsDTO.fatherNameBng == null ? "" : employee_recordsDTO.fatherNameBng;
        this.dateOfBirth = dateFormat.format(new Date(employee_recordsDTO.dateOfBirth));
        this.photo = Base64.encodeBase64(employee_recordsDTO.photo);
//        this.photo = employee_recordsDTO.photo;
        this.parliamentNumber = parliamentNumber;
        this.constituencyNumber = constituencyNumber;
        this.constituencyNameEng = constituencyNameEng;
        this.constituencyNameBng = constituencyNameBng;
        this.politicalPartyNameEng = politicalPartyNameEng;
        this.politicalPartyNameBng = politicalPartyNameBng;
        this.startDate = dateFormat.format(new Date(startDate));
        this.endDate = dateFormat.format(new Date(endDate));
        this.mpStatus = CatRepository.getName("English", "mp_status", mpStatus);

        this.additionalMobile = employee_recordsDTO.alternativeMobile;
//        this.isVacant = Election_constituencyRepository.getInstance().getElection_constituencyDTOByConstituencyNumber(parliamentNumber, (int) constituencyNumber).isVacant == 1;
        this.mpType = Election_constituencyRepository.getInstance().getElection_constituencyDTOByConstituencyNumber(parliamentNumber, (int) constituencyNumber).isReserved == 1 ? "reserved" : "elected";
        this.term = employee_recordsDTO.mpElectedCount;

        this.committeesInfoModels = getCommitteesInfoModels(committees_mappingDTOS, empRecordId, parliamentNumber);
        this.ministryInfoModels = getMinistryInfoModels(ministry_office_mappingDTOS, empRecordId, parliamentNumber);
        this.isMinister = this.ministryInfoModels.size() != 0;
    }


    private List<CommitteesInfoModel> getCommitteesInfoModels(List<Committees_mappingDTO> dtos, long empRecId, long parliamentId) {
        return dtos.stream().filter(dto -> dto.employeeRecordsId==empRecId && dto.electionDetailsId==parliamentId).map(CommitteesInfoModel::new).collect(Collectors.toList());

    }

    private List<MinistryInfoModel> getMinistryInfoModels(List<Ministry_office_mappingDTO> dtos, long empRecId, long parliamentId) {
        return dtos.stream().filter(dto -> dto.employeeRecordsId==empRecId && dto.electionDetailsId==parliamentId).map(MinistryInfoModel::new).collect(Collectors.toList());
    }
}
