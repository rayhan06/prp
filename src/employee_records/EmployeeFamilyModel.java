package employee_records;

import employee_family_info.Employee_family_infoDTO;
import nationality.NationalityRepository;
import pb.CatRepository;
import profession.ProfessionRepository;
import relation.RelationRepository;
import religion.ReligionRepository;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EmployeeFamilyModel {
    String nameEng;
    String nameBng;
    String relationType;
    String mobileNumber;
    String alternativeMobileNumber;
    String email;
    String dateOfBirth;
    int gender;
    long religion;
    String profession;
    int bloodGroup;
    String nationality;
    String permanentAddress;
    String presentAddress;
    String nid;
    String passportNo;
    String passportIssueDate;
    String passportExpiryDate;



    public EmployeeFamilyModel(Employee_family_infoDTO dto) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        this.nameEng = dto.firstNameEn;
        this.nameBng = dto.firstNameBn;
        this.relationType = RelationRepository.getInstance().getRelationNameById(dto.relationType, true);
        this.mobileNumber = dto.mobileNumber1;
        this.alternativeMobileNumber = dto.mobileNumber2;
        this.email = dto.email;
        this.dateOfBirth = dateFormat.format(new Date(dto.dob));
        this.gender = dto.genderCat;
        this.profession = ProfessionRepository.getInstance().getText("English", dto.professionType);
        this.bloodGroup = dto.bloodGroupCat;
        this.nationality = NationalityRepository.getInstance().getText("English", dto.nationalityType);
        try {
            this.permanentAddress = dto.permanentAddress.split(":")[1]==null || dto.permanentAddress.split(":")[1].isEmpty() ? "" : dto.permanentAddress.split(":")[1];
        } catch (Exception e) {
            this.permanentAddress = "";
        }
        try {
            this.presentAddress = dto.presentAddress.split(":")[1]==null || dto.presentAddress.split(":")[1].isEmpty() ? "" : dto.presentAddress.split(":")[1];
        } catch (Exception e) {
            this.presentAddress = "";
        }
        this.nid = dto.nationalIdNo;
        this.passportNo = dto.passportNo;
        this.passportIssueDate = dateFormat.format(new Date(dto.passportIssueDate));
        this.passportExpiryDate = dateFormat.format(new Date(dto.passportExpiryDate));
        this.religion = dto.religionType;
    }
}
