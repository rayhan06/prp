package employee_records;

import common.ApiResponse;
import ministry_office.Ministry_officeRepository;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GetMinistryOfficeName implements EmployeeRecordsApiPerformer{
    public final Logger logger = Logger.getLogger(GetMinistryOfficeName.class);

    @Override
    public ApiResponse perform(HttpServletRequest request) {
        try {
            List<MinistryOfficeNameModel> ministryOfficeNameModels = new ArrayList<>();

            ministryOfficeNameModels = Ministry_officeRepository.getInstance().getNameDTOList().stream().
                    map(MinistryOfficeNameModel::new).collect(Collectors.toList());


            return ministryOfficeNameModels.size()==0 ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                    ApiResponse.makeSuccessResponse(ministryOfficeNameModels, "Successfully found the data.");
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
        }

    }
}
