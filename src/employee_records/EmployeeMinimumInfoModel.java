package employee_records;

import org.apache.log4j.Logger;
import user.UserDAO;
import user.UserRepository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class EmployeeMinimumInfoModel {
    public String empRecordId;
    public String nameEng = "";
    public String nameBng = "";
    public String fatherNameEng = "";
    public String fatherNameBng = "";
    public String dateOfBirth = "";
    public byte[] photo = null;



    public EmployeeMinimumInfoModel(String useName, String nameEng, String nameBng, String fatherNameEng, String fatherNameBng,
                                    String dateOfBirth, byte[] photo) {
        this.empRecordId = useName;
        this.nameEng = nameEng;
        this.nameBng = nameBng;
        this.fatherNameEng = fatherNameEng;
        this.fatherNameBng = fatherNameBng;
        this.dateOfBirth = dateOfBirth;
        this.photo = photo;
    }
}
