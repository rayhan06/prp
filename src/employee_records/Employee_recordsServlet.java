package employee_records;

import com.google.gson.Gson;

import appointment.YearMonthCount;
import common.ApiResponse;
import common.NameDTO;
import common.RoleEnum;
import dbm.DBMW;
import disciplinary_details.DisciplinaryDetailsModelWithLogModel;
import disciplinary_details.Disciplinary_detailsDAO;
import election_constituency.Election_constituencyDAO;
import election_constituency.Election_constituencyDTO;
import election_constituency.Election_constituencyRepository;
import election_details.Election_detailsDTO;
import election_details.Election_detailsRepository;
import election_wise_mp.Election_wise_mpDAO;
import election_wise_mp.Election_wise_mpDTO;
import emp_travel_details.Emp_travel_detailsDAO;
import emp_travel_details.Emp_travel_detailsDtoWithValue;
import employee_acr.Employee_acrDAO;
import employee_acr.Employee_acrDTO;
import employee_attachment.Employee_attachmentDAO;
import employee_attachment.Employee_attachmentDTO;
import employee_bank_information.EmployeeBankInfoModel;
import employee_bank_information.Employee_bank_informationDAO;
import employee_certification.Employee_certificationDAO;
import employee_certification.Employee_certificationDTO;
import employee_education_info.EmployeeEducationInfoDTOWithValue;
import employee_education_info.Employee_education_infoDAO;
import employee_family_info.Employee_family_infoDAO;
import employee_family_info.Employee_family_infoDtoWithValue;
import employee_honors_awards.EmployeeAwardHonorModel;
import employee_honors_awards.Employee_honors_awardsDAO;
import employee_language_proficiency.EmployeeLanguageProficiencyModel;
import employee_language_proficiency.Employee_language_proficiencyDAO;
import employee_leave_details.EmpLeaveDetails;
import employee_leave_details.Employee_leave_detailsDAO;
import employee_management.EmployeeMPModel;
import employee_nominee.EmployeeNomineeModel;
import employee_nominee.Employee_nomineeDAO;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_offices.EmployeeOfficesDAO;
import employee_posting.Employee_local_posting_model;
import employee_posting.Employee_postingDAO;
import employee_posting.Employee_postingDTO;
import employee_publication.Employee_publicationDAO;
import employee_publication.Employee_publicationDetails;
import employee_service_history.EmployeeServiceModel;
import employee_service_history.Employee_service_historyDAO;
import employee_service_history.Employee_service_historyDTO;
import employee_vaccination_info.EmployeeVaccineModel;
import employee_vaccination_info.Employee_vaccination_infoDAO;
import files.FilesDAO;
import files.FilesDTO;
import geolocation.GeoLocationDTO;
import geolocation.GeoLocationRepository;
import language.LC;
import language.LM;
import login.LoginDTO;
import mail.EmailService;
import mail.SendEmailDTO;
import nationality.NationalityRepository;
import office_unit_organogram.Office_unit_organogramDAO;
import office_unit_organogram.Office_unit_organogramDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import pb.CatDTO;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import pb.Utils;
import permission.MenuConstants;
import political_party.Political_partyDTO;
import political_party.Political_partyRepository;
import promotion_history.Promotion_historyDAO;
import promotion_history.Promotion_historyDTOWithValues;
import religion.ReligionRepository;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import sms.SmsService;
import sms_log.Sms_logDTO;
import training_calendar_details.TrainingCalendarDetailsShortInfo;
import training_calendar_details.Training_calendar_detailsDAO;
import user.UserDTO;
import user.UserRepository;
import util.HttpRequestUtils;
import util.RecordNavigationManager2;
import util.StringUtils;

import javax.imageio.ImageIO;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;


@WebServlet("/Employee_recordsServlet")
@MultipartConfig
public class Employee_recordsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static final int DEFAULT_IMAGE_WIDTH = 250;
    public static final int DEFAULT_IMAGE_HEIGHT = 250;
    public static final int PROFILE_IMAGE_WIDTH = 300;
    public static final int PROFILE_IMAGE_HEIGHT = 300;
    public static final int SIGNATURE_IMAGE_WIDTH = 300;
    public static final int SIGNATURE_IMAGE_HEIGHT = 80;
    public static final int nid_IMAGE_WIDTH = 600;
    public static final int nid_IMAGE_HEIGHT = 400;
    public static final int MP_GAZETTE_IMAGE_WIDTH = 500;
    public static final int MP_GAZETTE_IMAGE_HEIGHT = 1000;
    public static final int EMPLOYEE_JOB_DURATION = 59;
    public static final int FREEDOM_FIGHTER_EMPLOYEE_JOB_DURATION = 61;
    public static final int RETIREMENT_DURATION = 1;
    
    public static final int STATUS_RELEASED = 6;
    public static final int STATUS_PRL = 7;
    public static final int STATUS_DISMISSAL = 8;
    
    public static final int DEPUTATION_OFFICER = 1;
    public static final int PARLIAMENT_OWN_OFFICER = 2;
    public static final int OTHER_OFFICER = 3;
    
    public static final int JOINING = 1;
    public static final int LPR = 2;
    
    
    public static Logger logger = Logger.getLogger(Employee_recordsServlet.class);
    private final Employee_family_infoDAO employee_family_infoDAO = Employee_family_infoDAO.getInstance();
    private final Employee_recordsDAO employeeRecordsDAO = new Employee_recordsDAO();
    //private static final String smsBody = "Dear %s,\n" + "Your user name is : %s\n" + "Password: %s";
    private static final String smsBodyForUserNameAndPasswordUpdate = "Dear %s,\n" + "Your new user name is : %s\n" + "Updated Password is : %s";
    private static final String smsBodyForUserNameUpdate = "Dear %s,\n" + "Your new user name is : %s\n" + "Password is your current password";
    private static final String smsBodyForPasswordUpdate = "Dear %s,\n" + "Your user name is : %s\n" + "Updated Password is : %s";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

    private boolean isValid(HttpServletRequest request) {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        return userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId() || userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.DATA_ENTRY.getRoleId()
                || Long.parseLong(request.getParameter("ID")) == userDTO.employee_record_id;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO != null) {
            try {
                String actionType = request.getParameter("actionType");
                switch (actionType) {
                    case "addMultiForm":
                        if (Utils.checkPermission(request.getParameter("ID"), userDTO, MenuConstants.EMPLOYEE_RECORDS_ADD)) {
                            getMultiPageAdd(request, response);
                            return;
                        }
                        break;
                    case "getStatusOptions":
                    	{
                    		int type = Integer.parseInt(request.getParameter("type"));
                    		int status = Integer.parseInt(request.getParameter("status"));
                    		logger.debug("getStatusOptions, type = " + type + " status = " + status);
                    		List<CatDTO> statuseDTOs = CatRepository.getDTOs("employees_employment_status");
                    		String options = "<option value='-1'> " + LM.getText(LC.HM_SELECT, loginDTO) + "</option>";
                    		List<CatDTO> realStatuseDTOs = new ArrayList<CatDTO> ();
                    		for(CatDTO catDTO: statuseDTOs)
                    		{
                    			if(catDTO.value != STATUS_RELEASED && catDTO.value != STATUS_PRL && catDTO.value != STATUS_DISMISSAL)
                    			{
                    				realStatuseDTOs.add(catDTO);
                    			}
                    			else
                    			{
                    				if(type == DEPUTATION_OFFICER && catDTO.value == STATUS_RELEASED)
                    				{
                    					realStatuseDTOs.add(catDTO);
                    				}
                    				else if(type == PARLIAMENT_OWN_OFFICER && (catDTO.value == STATUS_PRL ||  catDTO.value == STATUS_DISMISSAL))
                    				{
                    					realStatuseDTOs.add(catDTO);
                    				}
                    			}
                    		}
                    		String Language = LM.getLanguage(loginDTO);
                    		boolean isLangEng = Language.equalsIgnoreCase("english");
                    		for(CatDTO catDTO: realStatuseDTOs)
                    		{
                    			options += "<option value = '" + catDTO.value + "'";
                    			if(catDTO.value == status)
                    			{
                    				options += " selected ";
                    			}
                    			options += ">";
                    			if(isLangEng)
                    			{
                    				options += catDTO.nameEn;
                    			}
                    			else
                    			{
                    				options += catDTO.nameBn;
                    			}
                    			options += "</option>";
                    		}
                    		logger.debug("options = " + options);
                    		PrintWriter out = response.getWriter();
                            out.print(options);
                            out.flush();
                            return;
                    	}
                    case "editMultiForm": {
                        long id = Long.parseLong(request.getParameter("ID"));
                        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(id);
                        if (employeeRecordsDTO != null && isValid(request)) {
                            getMultiPageEdit(request, response, userDTO);
                            return;
                        }
                    }
                    break;
                    case "viewMyProfile":
                        if (userDTO.roleID != RoleEnum.ADMIN.getRoleId()) {
                            getMultiPageView(request, response);
                            return;
                        }
                        break;
                    case "viewMultiForm":
                        if (userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId() || userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.DATA_ENTRY.getRoleId()) {
                            getMultiPageView(request, response);
                            return;
                        }
                        break;
                    case "ajax_getFamilyMembers":
                        if (isValid(request)) {
                            sendFamilyMembersInfoHTML(request, response);
                            return;
                        }
                        break;
                    case "ajax_getVaccineInfo":
                        if (isValid(request)) {
                            sendVaccineInfoHTML(request, response);
                            return;
                        }
                        break;
                    case "ajax_getNomineeInfo":
                        if (isValid(request)) {
                            sendNomineeInfoHTML(request, response);
                            return;
                        }
                        break;
                    case "ajax_getBankInfo":
                        if (isValid(request)) {
                            sendBankInfoHTML(request, response);
                            return;
                        }
                        break;
                    case "ajax_getLeaveDetails":
                        if (isValid(request)) {
                            sendLeaveDetailsHTML(request, response);
                            return;
                        }
                        break;
                    case "ajax_getEmployeeRecord":
                        if (isValid(request)) {
                            sendEmployeeRecordJSON(request, response);
                            return;
                        }
                        break;
                    case "ajax_getLanguageRecord":
                        if (isValid(request)) {
                            setTab2LanguageData(request, response);
                            return;
                        }
                        break;
                    case "ajax_getEducationRecord":
                        if (isValid(request)) {
                            setTab2EducationData(request, response);
                            return;
                        }
                        break;
                    case "ajax_getLocalPostingRecord":
                        if (isValid(request)) {
                            setTab4LocalPostingData(request, response);
                            return;
                        }
                        break;
                    case "ajax_getForeignPostingRecord":
                        if (isValid(request)) {
                            setTab4ForeignPostingData(request, response);
                            return;
                        }
                        break;
                    case "ajax_getEmployeePromotionHistory":
                        if (isValid(request)) {
                            setTab3PromotionHistory(request, response);
                            return;
                        }
                        break;
                    case "ajax_getEmployeeACR":
                        if (isValid(request)) {
                            setTab3ACR(request, response);
                            return;
                        }
                        break;
                    case "ajax_getTrainingInfo":
                        if (isValid(request)) {
                            setTab4TrainingInfo(request, response);
                            return;
                        }
                        break;
                    case "ajax_getServiceHistory":
                        if (isValid(request)) {
                            setTab3ServiceHistory(request, response);
                            return;
                        }
                        break;
                    case "ajax_getParliamentServiceHistory":
                        if (isValid(request)) {
                            setTab3ParliamentServiceHistory(request, response);
                            return;
                        }
                        break;
                    case "ajax_getTravelDetails":
                        if (isValid(request)) {
                            setTab5TravelDetails(request, response);
                            return;
                        }
                        break;
                    case "ajax_getCertificateDetails":
                        if (isValid(request)) {
                            setTab5CertificateDetails(request, response);
                            return;
                        }
                        break;
                    case "ajax_getAwardDetails":
                        if (isValid(request)) {
                            setTab5AwardDetails(request, response);
                            return;
                        }
                        break;
                    case "ajax_getPublicationDetails":
                        if (isValid(request)) {
                            setTab6PublicationDetails(request, response);
                            return;
                        }
                        break;
                    case "ajax_getDisciplinaryDetails":
                        if (isValid(request)) {
                            setTab6DisplinaryDetails(request, response);
                            return;
                        }
                        break;
                    case "viewSummary":
                    case "pds": {
                        long id;
                        if (request.getParameter("ID") == null) {
                            id = userDTO.employee_record_id;
                        } else {
                            id = Long.parseLong(request.getParameter("ID"));
                        }
                        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(id);
                        if (employeeRecordsDTO != null && (userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId() || userDTO.roleID == RoleEnum.DATA_ENTRY.getRoleId() ||
                                userDTO.roleID == RoleEnum.ADMIN.getRoleId() ||
                                id == userDTO.employee_record_id)) {
                            getSummary(request, response, id);
                            return;
                        }
                    }
                    break;
                    case "getEditPage":
                        if (isValid(request)) {
                            getEmployee_records(request, response);
                            return;
                        }
                        break;
                    case "getURL":
                        String URL = request.getParameter("URL");
                        logger.debug("URL = " + URL);
                        response.sendRedirect(URL);
                        return;
                    case "search":
                        logger.debug("search requested");
                        if (Utils.checkPermission(userDTO, MenuConstants.EMPLOYEE_RECORDS_ADD)) {
                            searchEmployee_records(request, response, loginDTO);
                            return;
                        }
                        break;
                    case "pension":
                        if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PENSION_MANAGEMENT)) {
                            searchEmployee_pension(request, response, loginDTO);
                        } else {
                            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                        }
                        return;
                    case "getCadre": {
                        logger.debug("getCadre");
                        ArrayList<Map<Object, Object>> r = employeeRecordsDAO.getCadre();
                        response.setContentType("application/json");
                        String res = new Gson().toJson(r);
                        PrintWriter out = response.getWriter();
                        out.print(res);
                        out.flush();
                        return;
                    }
                    case "getBatch": {
                        logger.debug("getBatch");
                        ArrayList<Map<Object, Object>> r = employeeRecordsDAO.getBatch();
                        response.setContentType("application/json");
                        String res = new Gson().toJson(r);
                        PrintWriter out = response.getWriter();
                        out.print(res);
                        out.flush();
                        break;
                    }
                    case "buildMP":
                        long electionId = Long.parseLong(request.getParameter("election_id"));
                        String language = request.getParameter("language");
                        String options = employeeRecordsDAO.mpBuildOptionByElection(language, electionId);
                        PrintWriter out = response.getWriter();
                        out.println(options);
                        out.close();
                        return;
                    case "downloadDropzoneFile": {
                        long id = Long.parseLong(request.getParameter("id"));
                        FilesDTO filesDTO = (FilesDTO) new FilesDAO().getDTOByID(id);
                        Utils.ProcessFile(request, response, filesDTO.fileTitle, filesDTO.inputStream);
                        return;
                    }
                    case "DeleteFileFromDropZone": {
                        long id = Long.parseLong(request.getParameter("id"));
                        logger.debug("In delete file");
                        new FilesDAO().hardDeleteByID(id);
                        response.getWriter().write("Deleted");
                        return;
                    }
                    case "ajax_emp_type_count":
                    case "ajax_emp_class_count":
                        getCount(actionType, request, response);
                        return;
                    case "ajax_get_6moth_joining":
                        get6MonthCount(actionType, request, response, JOINING);
                        return;
                    case "ajax_get_6moth_lpr":
                        get6MonthCount(actionType, request, response, LPR);
                        return;
                    case "demoTree": {
                        request.getRequestDispatcher("/employee_records/demoTree.jsp").forward(request, response);
                    }
                }
            } catch (Exception ex) {
                logger.error("",ex);
            }
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }
    
    private void get6MonthCount(String actionType, HttpServletRequest request, HttpServletResponse response, int type) {
		// TODO Auto-generated method stub
		List<YearMonthCount> counts;
		String Language = request.getParameter("Language");
		boolean isLangEng = Language.equalsIgnoreCase("english");
		if(type == JOINING)
		{
			counts = employeeRecordsDAO.getLast6Month(isLangEng, "joining_date");
		}
		else
		{
			counts = employeeRecordsDAO.getLast6Month(isLangEng, "lpr_date");
		}
		String data = new Gson().toJson(counts);
		response.setContentType("application/json");
		PrintWriter out;
		try {
			out = response.getWriter();
			out.print(data);
			out.flush();
			out.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
            logger.error("",e);
		}
	
	}

	private void activate(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
    	long id = Long.parseLong(request.getParameter("ID"));
    	Employee_recordsDTO employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(id);
    	if(employee_recordsDTO != null)
    	{
    		employee_recordsDTO.status = Boolean.parseBoolean(request.getParameter("activate"));
    		employee_recordsDTO.deactivationReason = request.getParameter("deactivationReason");
    		employeeRecordsDAO.activate(employee_recordsDTO,true);
    		Employee_recordsRepository.getInstance().reload(false);
    	}
    	
    	response.sendRedirect("Employee_recordsServlet?actionType=search");
		
	}

    private void getCount(String action, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String language = request.getParameter("language");
        long officeUnitId = Long.parseLong(request.getParameter("officeUnitId"));
        Set<Long> officeUnitsIdSet = Office_unitsRepository.getInstance().getAllChildOfficeDTOWithParent(officeUnitId)
                .stream()
                .map(e -> e.iD)
                .collect(Collectors.toSet());
        Set<Long> employeeRecordIdSet = EmployeeOfficeRepository.getInstance()
                .getEmployeeOfficeDTOs(officeUnitsIdSet)
                .stream()
                .map(e -> e.employeeRecordId)
                .collect(toSet());
        List<Employee_recordsDTO> employeeRecordsDTOList = Employee_recordsRepository.getInstance().getByIds(employeeRecordIdSet);
        Map<String, Object> result;
        if (action.equals("ajax_emp_type_count")) {
            result = Employee_recordsRepository.getInstance().getCountByType(language, employeeRecordsDTOList, officeUnitId);
        } else {
            result = Employee_recordsRepository.getInstance().getCountByClass(language, employeeRecordsDTOList, officeUnitId);
        }
        String data = new Gson().toJson(result);
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        out.print(data);
        out.flush();
        out.close();
    }

    private void getSummary(HttpServletRequest request, HttpServletResponse response, Long id) throws Exception {
        setTab1Data(request, id);
        setTab2Data(request, id);
        setTab3Data(request, id);
        setTab4Data(request, id);
        setTab5Data(request, id);
        setTab6Data(request, id);
        RequestDispatcher rd = request.getRequestDispatcher("employee_records/employee_record_view_summary.jsp");
        rd.forward(request, response);
    }

    private void getMultiPageAdd(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Employee_recordsDTO employee_recordsDTO = new Employee_recordsDTO();
        request.setAttribute("ID", employee_recordsDTO.iD);
        request.setAttribute("employee_recordsDTO", employee_recordsDTO);
        request.setAttribute("tab", 1);
        RequestDispatcher rd = request.getRequestDispatcher("employee_records/multiform/tab1/tab1.jsp");
        rd.forward(request, response);
    }

    private void getMultiPageEdit(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception {
        String id = request.getParameter("ID");
        Employee_recordsDTO employee_recordsDTO;
        if (id == null) {
            employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(userDTO.employee_record_id);
        } else {
            employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(Long.parseLong(id));
        }
        request.setAttribute("ID", employee_recordsDTO.iD);
        request.setAttribute("employee_recordsDTO", employee_recordsDTO);
        request.setAttribute("tab", 1);
        request.getRequestDispatcher("employee_records/multiform/tab1/tab1.jsp").forward(request, response);
    }

    private void getMultiPageView(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String tabStr = request.getParameter("tab");
        tabStr = tabStr == null ? "1" : tabStr;
        request.setAttribute("tab", Integer.parseInt(tabStr));
        request.getRequestDispatcher("employee_records/multiform/tab" + tabStr + "/tab" + tabStr + ".jsp").forward(request, response);
    }

    private void sendEmployeeRecordJSON(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Employee_recordsDTO dto = employeeRecordsDAO.getEmployee_recordsDTOByID(Long.parseLong(request.getParameter("ID")));
        EmployeeRecordModel employeeRecord = ModelUtil.fromDTO(dto, request.getParameter("language"));
        String employeeRecordJSON = new Gson().toJson(employeeRecord);
        response.setContentType("application/json");
        response.getWriter().println(employeeRecordJSON);
    }

    private void sendFamilyMembersInfoHTML(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long id = Long.parseLong(request.getParameter("ID"));
        List<Employee_family_infoDtoWithValue> employee_family_info = employee_family_infoDAO.getDtoWithValueByEmployeeRecordsId(id);
        request.setAttribute("employee_family_info", employee_family_info);

        logger.debug("Getting Family Member");
        request.getRequestDispatcher("employee_records/multiform/tab1/section2_existing_family_members_info.jsp")
                .forward(request, response);
    }

    private void sendVaccineInfoHTML(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long employeeId = Long.parseLong(request.getParameter("ID"));

        List<EmployeeVaccineModel> savedVaccineModelList = Employee_vaccination_infoDAO.getInstance().getEmployeeVaccineModelListByEmployeeId(employeeId, null);
        request.setAttribute("savedVaccineModelList", savedVaccineModelList);

        logger.debug("Getting Vaccine Info HTML");
        request.getRequestDispatcher("employee_records/multiform/tab6/tab6_vaccine_info.jsp")
                .forward(request, response);
    }

    private void sendNomineeInfoHTML(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long employeeId = Long.parseLong(request.getParameter("ID"));

        List<EmployeeNomineeModel> savedNomineeModelList = Employee_nomineeDAO.getInstance().getEmployeeNomineeModelListByEmployeeId(employeeId, null);
        request.setAttribute("savedNomineeModelList", savedNomineeModelList);

        logger.debug("Getting Nominee Info HTML");
        request.getRequestDispatcher("employee_records/multiform/tab6/tab6_nominee_info.jsp")
                .forward(request, response);
    }

    private void sendBankInfoHTML(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long employeeId = Long.parseLong(request.getParameter("ID"));

        List<EmployeeBankInfoModel> savedBankInfoModelList = Employee_bank_informationDAO.getInstance().getEmployeeBankInfoModelListByEmployeeId(employeeId);
        request.setAttribute("savedBankInfoModelList", savedBankInfoModelList);

        logger.debug("Getting Bank Info HTML");
        request.getRequestDispatcher("employee_records/multiform/tab6/bank_info_section.jsp")
                .forward(request, response);
    }

    private void sendLeaveDetailsHTML(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long employeeId = Long.parseLong(request.getParameter("ID"));

        List<EmpLeaveDetails> savedEmpLeaveDetailsList = Employee_leave_detailsDAO.getInstance().getEmpLeaveDetailsByEmployeeId(employeeId);
        request.setAttribute("savedEmpLeaveDetailsList", savedEmpLeaveDetailsList);

        logger.debug("Getting Leave Details HTML");
        request.getRequestDispatcher("employee_records/multiform/tab6/leave_details_section.jsp")
                .forward(request, response);
    }

    private void setTab1Data(HttpServletRequest request, Long id) {
        Employee_recordsDTO employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(id);
        List<Employee_family_infoDtoWithValue> employee_family_info = employee_family_infoDAO.getDtoWithValueByEmployeeRecordsId(employee_recordsDTO.iD);
        request.setAttribute("ID", employee_recordsDTO.iD);
        request.setAttribute("employee_recordsDTO", employee_recordsDTO);
        request.setAttribute("employee_family_info", employee_family_info);
    }

    private void setTab2Data(HttpServletRequest request, Long employeeId) {
        Employee_recordsDTO employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(employeeId);
        request.setAttribute("ID", employee_recordsDTO.iD);
        request.setAttribute("employee_recordsDTO", employee_recordsDTO);

        List<EmployeeEducationInfoDTOWithValue> savedEductionInfoList = Employee_education_infoDAO.getInstance().getEmployeeEducationInfosByEmployeeId(employee_recordsDTO.iD);
        request.setAttribute("savedEductionInfoList", savedEductionInfoList);

        List<EmployeeLanguageProficiencyModel> languageProficiencyModelList = Employee_language_proficiencyDAO.getInstance().getEmployeeLanguageProficiencyModels(employeeId);
        request.setAttribute("savedLanguageProficiencyList", languageProficiencyModelList);
    }

    private void setTab2EducationData(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long employeeId = Long.parseLong(request.getParameter("ID"));
        Employee_recordsDTO employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(employeeId);
        request.setAttribute("ID", employee_recordsDTO.iD);
        request.setAttribute("employee_recordsDTO", employee_recordsDTO);

        List<EmployeeEducationInfoDTOWithValue> savedEductionInfoList = Employee_education_infoDAO.getInstance().getByEmpIdSortByEduLevel(employee_recordsDTO.iD);
        request.setAttribute("savedEductionInfoList", savedEductionInfoList);
        logger.debug("Inside set Education Data");
        RequestDispatcher rd = request.getRequestDispatcher("employee_records/multiform/tab2/" + "section3_education_info.jsp");
        rd.forward(request, response);
    }

    private void setTab2LanguageData(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long employeeId = Long.parseLong(request.getParameter("ID"));
        Employee_recordsDTO employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(employeeId);
        request.setAttribute("ID", employee_recordsDTO.iD);
        request.setAttribute("employee_recordsDTO", employee_recordsDTO);

        List<EmployeeLanguageProficiencyModel> languageProficiencyModelList = Employee_language_proficiencyDAO.getInstance().getEmployeeLanguageProficiencyModels(employeeId);
        request.setAttribute("savedLanguageProficiencyList", languageProficiencyModelList);
        logger.debug("Inside set Language Data");
        RequestDispatcher rd = request.getRequestDispatcher("employee_records/multiform/tab2/" + "section_language.jsp");
        rd.forward(request, response);
    }

    private void setTab3Data(HttpServletRequest request, long employeeId) throws Exception {
        Employee_recordsDTO employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(employeeId);
        List<Employee_acrDTO> employeeAcrDtos = Employee_acrDAO.getInstance().getByEmployeeId(employeeId);
        List<Promotion_historyDTOWithValues> promotionHistoryDetails = Promotion_historyDAO.getInstance().getAllPromotionDetailsByEmployeeRecordsId(employee_recordsDTO.iD);
        List<Employee_local_posting_model> savedLocalPostingModels = Employee_postingDAO.getInstance().getAllLocalPostingModelByEmployeeId(employeeId);
        List<Employee_postingDTO> employeeForeignPostingDTOs = Employee_postingDAO.getInstance().getForeignPostingDTOsByEmployeeId(employeeId);
        List<EmployeeOfficeDTO> employeeOfficeDTOList = EmployeeOfficesDAO.getInstance().getByEmployeeRecordIdIgnoringStatus(employeeId);
        List<Employee_attachmentDTO> employeeAttachmentDTOList = Employee_attachmentDAO.getInstance().getByEmployeeId(employeeId);
        request.setAttribute("employeeOfficeDTOList", employeeOfficeDTOList);
        request.setAttribute("employeeAttachmentDTOList", employeeAttachmentDTOList);
        request.setAttribute("ID", employee_recordsDTO.iD);
        request.setAttribute("employee_recordsDTO", employee_recordsDTO);
        request.setAttribute("promotionHistoryDetails", promotionHistoryDetails);
        request.setAttribute("savedLocalPostingModels", savedLocalPostingModels);
        request.setAttribute("savedForeignPostingList", employeeForeignPostingDTOs);
        request.setAttribute("employeeAcrDtos", employeeAcrDtos);
    }

    private void setTab4LocalPostingData(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long employeeId = Long.parseLong(request.getParameter("ID"));

        Employee_recordsDTO employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(employeeId);
        List<Employee_local_posting_model> savedLocalPostingModels = Employee_postingDAO.getInstance().getAllLocalPostingModelByEmployeeId(employeeId);

        request.setAttribute("ID", employee_recordsDTO.iD);
        request.setAttribute("employee_recordsDTO", employee_recordsDTO);
        request.setAttribute("savedLocalPostingModels", savedLocalPostingModels);
        logger.debug("Inside set Local Posting Data");
        RequestDispatcher rd = request.getRequestDispatcher("employee_records/multiform/tab4/" + "section_local_posting.jsp");
        rd.forward(request, response);
    }

    private void setTab4ForeignPostingData(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long employeeId = Long.parseLong(request.getParameter("ID"));

        Employee_recordsDTO employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(employeeId);
        List<Employee_postingDTO> employeeForeignPostingDTOs = Employee_postingDAO.getInstance().getForeignPostingDTOsByEmployeeId(employeeId);

        request.setAttribute("ID", employee_recordsDTO.iD);
        request.setAttribute("employee_recordsDTO", employee_recordsDTO);
        request.setAttribute("savedForeignPostingList", employeeForeignPostingDTOs);
        logger.debug("Inside set Local Posting Data");
        RequestDispatcher rd = request.getRequestDispatcher("employee_records/multiform/tab4/" + "section_foreign_posting.jsp");
        rd.forward(request, response);
    }

    private void setTab3PromotionHistory(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long employeeId = Long.parseLong(request.getParameter("ID"));

        Employee_recordsDTO employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(employeeId);
        List<Promotion_historyDTOWithValues> promotionHistoryDetails = Promotion_historyDAO.getInstance().getAllPromotionDetailsByEmployeeRecordsId(employee_recordsDTO.iD);
        request.setAttribute("ID", employee_recordsDTO.iD);
        request.setAttribute("employee_recordsDTO", employee_recordsDTO);
        request.setAttribute("promotionHistoryDetails", promotionHistoryDetails);
        logger.debug("Inside set Promotion History Data");
        RequestDispatcher rd = request.getRequestDispatcher("employee_records/multiform/tab3/" + "promotion_history_details.jsp");
        rd.forward(request, response);
    }

    private void setTab3ACR(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long employeeId = Long.parseLong(request.getParameter("ID"));

        Employee_recordsDTO employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(employeeId);
        List<Employee_acrDTO> employeeAcrDtos = Employee_acrDAO.getInstance().getByEmployeeId(employeeId);
        if(employeeAcrDtos!=null){
            employeeAcrDtos = employeeAcrDtos.stream().sorted(Comparator.comparing(dto->dto.year)).collect(Collectors.toList());
        }
        request.setAttribute("ID", employee_recordsDTO.iD);
        request.setAttribute("employee_recordsDTO", employee_recordsDTO);
        request.setAttribute("employeeAcrDtos", employeeAcrDtos);
        logger.debug("Inside set Tab3 ACR Data");
        RequestDispatcher rd = request.getRequestDispatcher("employee_records/multiform/tab3/" + "employee_acr_details.jsp");
        rd.forward(request, response);
    }

    private void setTab4Data(HttpServletRequest request, long employeeId) {
        Employee_recordsDTO employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(employeeId);
        request.setAttribute("ID", employee_recordsDTO.iD);
        request.setAttribute("employee_recordsDTO", employee_recordsDTO);

        List<TrainingCalendarDetailsShortInfo> savedTrainingList = new Training_calendar_detailsDAO().getTrainingCalendarDetailsShortInfoList(employeeId, "English");
        request.setAttribute("savedTrainingList", savedTrainingList);

        List<EmployeeServiceModel> savedServiceModelList = Employee_service_historyDAO.getInstance().getEmployeeServiceModelList(employeeId);
        request.setAttribute("savedServiceModelList", savedServiceModelList);
    }

    private void setTab4TrainingInfo(HttpServletRequest request, HttpServletResponse response) throws Exception {
        RequestDispatcher rd = request.getRequestDispatcher("employee_records/multiform/tab4/" + "training_info.jsp");
        rd.forward(request, response);
    }

    private void setTab3ServiceHistory(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long employeeId = Long.parseLong(request.getParameter("ID"));
        Employee_recordsDTO employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(employeeId);
        request.setAttribute("ID", employee_recordsDTO.iD);
        request.setAttribute("employee_recordsDTO", employee_recordsDTO);
        logger.debug("Inside set Tab4 Service History Data");
        List<Employee_service_historyDTO> savedServiceModelList = Employee_service_historyDAO.getInstance().getByEmployeeId(employeeId);
        request.setAttribute("savedServiceModelList", savedServiceModelList);
        RequestDispatcher rd = request.getRequestDispatcher("employee_records/multiform/tab3/" + "service_history.jsp");
        rd.forward(request, response);
    }

    private void setTab3ParliamentServiceHistory(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long employeeRecordId = Long.parseLong(request.getParameter("ID"));
        List<EmployeeOfficeDTO> employeeOfficeDTOList = EmployeeOfficesDAO.getInstance().getByEmployeeRecordIdIgnoringStatus(employeeRecordId);
        request.setAttribute("employeeOfficeDTOList", employeeOfficeDTOList);
        List<Employee_attachmentDTO> employeeAttachmentDTOList = Employee_attachmentDAO.getInstance().getByEmployeeId(employeeRecordId);
        request.setAttribute("employeeAttachmentDTOList", employeeAttachmentDTOList);
        request.getRequestDispatcher("employee_records/multiform/tab3/" + "parliament_service_history.jsp").forward(request, response);
    }

    private void setTab5Data(HttpServletRequest request, long employeeId) {
        Employee_recordsDTO employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(employeeId);
        request.setAttribute("employee_recordsDTO", employee_recordsDTO);
        List<Emp_travel_detailsDtoWithValue> emp_travel_details = Emp_travel_detailsDAO.getInstance().getAllEmp_travel_detailsByEmployeeRecordsId(employee_recordsDTO.iD);
        request.setAttribute("emp_travel_details", emp_travel_details);
        List<Employee_certificationDTO> employee_certificationDtos = Employee_certificationDAO.getInstance().getByEmployeeId(employee_recordsDTO.iD);
        request.setAttribute("employee_certificationDtos", employee_certificationDtos);
    }

    private void setTab5TravelDetails(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Employee_recordsDTO employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(Long.parseLong(request.getParameter("ID")));
        request.setAttribute("employee_recordsDTO", employee_recordsDTO);
        List<Emp_travel_detailsDtoWithValue> emp_travel_details = Emp_travel_detailsDAO.getInstance().getAllEmp_travel_detailsByEmployeeRecordsId(employee_recordsDTO.iD);
        request.setAttribute("emp_travel_details", emp_travel_details);
        logger.debug("Inside set Tab5 Travel Details Data");
        RequestDispatcher rd = request.getRequestDispatcher("employee_records/multiform/tab5/" + "existing-travel-details.jsp");
        rd.forward(request, response);
    }

    private void setTab5CertificateDetails(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Employee_recordsDTO employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(Long.parseLong(request.getParameter("ID")));
        request.setAttribute("employee_recordsDTO", employee_recordsDTO);
        List<Employee_certificationDTO> employee_certificationDtos = Employee_certificationDAO.getInstance().getByEmployeeId(employee_recordsDTO.iD);
        request.setAttribute("employee_certificationDtos", employee_certificationDtos);
        logger.debug("Inside set Tab5 Certificate Details Data");
        RequestDispatcher rd = request.getRequestDispatcher("employee_records/multiform/tab5/" + "employee-certification-details.jsp");
        rd.forward(request, response);
    }

    private void setTab6Data(HttpServletRequest request, long employeeId) {
        Employee_recordsDTO employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(employeeId);
        request.setAttribute("ID", employee_recordsDTO.iD);
        request.setAttribute("employee_recordsDTO", employee_recordsDTO);

        List<EmployeeAwardHonorModel> savedAwardModelList = Employee_honors_awardsDAO.getInstance().getEmployeeAwardHonorModelByEmployeeId(employeeId);
        request.setAttribute("savedAwardModelList", savedAwardModelList);

        List<EmployeeBankInfoModel> savedBankInfoModelList = Employee_bank_informationDAO.getInstance().getEmployeeBankInfoModelListByEmployeeId(employeeId);
        request.setAttribute("savedBankInfoModelList", savedBankInfoModelList);

        List<EmployeeNomineeModel> savedNomineeModelList = Employee_nomineeDAO.getInstance().getEmployeeNomineeModelListByEmployeeId(employeeId, null);
        request.setAttribute("savedNomineeModelList", savedNomineeModelList);

        List<EmployeeVaccineModel> savedVaccineModelList = Employee_vaccination_infoDAO.getInstance().getEmployeeVaccineModelListByEmployeeId(employeeId, null);
        request.setAttribute("savedVaccineModelList", savedVaccineModelList);

        List<DisciplinaryDetailsModelWithLogModel> savedDisciplinaryList = new Disciplinary_detailsDAO().getDisciplinaryListModel(employeeId);
        request.setAttribute("savedDisciplinaryList", savedDisciplinaryList);

        List<EmpLeaveDetails> savedEmpLeaveDetailsList = Employee_leave_detailsDAO.getInstance().getEmpLeaveDetailsByEmployeeId(employeeId);
        request.setAttribute("savedEmpLeaveDetailsList", savedEmpLeaveDetailsList);

        List<Employee_publicationDetails> savedEmpPublicationDetailsList = Employee_publicationDAO.getInstance().getEmployeePublicationDetailsByEmployeeRecordsId(employeeId);
        request.setAttribute("savedEmpPublicationDetailsList", savedEmpPublicationDetailsList);
    }

    private void setTab5AwardDetails(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long employeeId = Long.parseLong(request.getParameter("ID"));
        Employee_recordsDTO employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(employeeId);
        request.setAttribute("ID", employee_recordsDTO.iD);
        request.setAttribute("employee_recordsDTO", employee_recordsDTO);

        List<EmployeeAwardHonorModel> savedAwardModelList = Employee_honors_awardsDAO.getInstance().getEmployeeAwardHonorModelByEmployeeId(employeeId);
        request.setAttribute("savedAwardModelList", savedAwardModelList);
        logger.debug("Inside set Tab5 Award Details Data");
        RequestDispatcher rd = request.getRequestDispatcher("employee_records/multiform/tab5/" + "award_section.jsp");
        rd.forward(request, response);
    }

    private void setTab6PublicationDetails(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long employeeId = Long.parseLong(request.getParameter("ID"));
        Employee_recordsDTO employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(employeeId);
        request.setAttribute("ID", employee_recordsDTO.iD);
        request.setAttribute("employee_recordsDTO", employee_recordsDTO);

        List<Employee_publicationDetails> savedEmpPublicationDetailsList = Employee_publicationDAO.getInstance().getEmployeePublicationDetailsByEmployeeRecordsId(employeeId);
        request.setAttribute("savedEmpPublicationDetailsList", savedEmpPublicationDetailsList);
        logger.debug("Inside set Tab6 Publication Data");
        RequestDispatcher rd = request.getRequestDispatcher("employee_records/multiform/tab6/" + "publication_section.jsp");
        rd.forward(request, response);
    }

    private void setTab6DisplinaryDetails(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long employeeId = Long.parseLong(request.getParameter("ID"));
        Employee_recordsDTO employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(employeeId);
        request.setAttribute("ID", employee_recordsDTO.iD);
        request.setAttribute("employee_recordsDTO", employee_recordsDTO);
        List<DisciplinaryDetailsModelWithLogModel> savedDisciplinaryList = new Disciplinary_detailsDAO().getDisciplinaryListModel(employeeId);
        request.setAttribute("savedDisciplinaryList", savedDisciplinaryList);
        logger.debug("Inside set Tab6 Disciplinary Data");
        RequestDispatcher rd = request.getRequestDispatcher("employee_records/multiform/tab6/" + "disciplinary_section.jsp");
        rd.forward(request, response);
    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        logger.debug("Part Header = {0}" + partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        try {
            String actionType = request.getParameter("actionType");
            logger.debug("actionType = " + actionType);
            switch (actionType) {
                case "edit":
                    if (isValid(request)) {
                        edit(request, response);
                        return;
                    }
                    break;
                case "activate":
                    if (isValid(request)) {
                    	activate(request, response);
                        return;
                    }
                    break;
                case "personalInfoEdit":
                    if (isValid(request)) {
                        personalInfoEdit(request, response);
                        return;
                    }
                    break;
                case "addressInfoEdit":
                    if (isValid(request)) {
                        addressInfoEdit(request, response);
                        return;
                    }
                    break;
                case "confidentialInfoEdit":
                    if (isValid(request)) {
                        confidentialInfoEdit(request, response);
                        return;
                    }
                    break;
                case "employmentInfoEdit":
                    if (isValid(request)) {
                        employmentInfoEdit(request, response);
                        return;
                    }
                    break;
                case "LPRInfoEdit":
                    if (isValid(request)) {
                        LPRInfoEdit(request, response);
                        return;
                    }
                    break;
                case "contactInfoEdit":
                    if (isValid(request)) {
                        contactInfoEdit(request, response);
                        return;
                    }
                    break;
                case "miscellaneousInfoEdit":
                    if (isValid(request)) {
                        miscellaneousInfoEdit(request, response);
                        return;
                    }
                    break;
                case "nidInfoEdit":
                    if (isValid(request)) {
                        nidInfoEdit(request, response);
                        return;
                    }
                    break;
                case "imageInfoEdit":
                    if (isValid(request)) {
                        imageInfoEdit(request, response);
                        return;
                    }
                    break;
                case "passwordInfoEdit":
                    if (isValid(request)) {
                        passwordInfoEdit(request, response);
                        return;
                    }
                    break;
                case "delete":
                    if (Utils.checkPermission(request.getParameter("ID"), userDTO, MenuConstants.EMPLOYEE_RECORDS_UPDATE)) {
                        deleteEmployee_records(request, response);
                        return;
                    }
                    break;
                case "search":
                    if (Utils.checkPermission(request.getParameter("ID"), userDTO, MenuConstants.EMPLOYEE_RECORDS_SEARCH)) {
                        searchEmployee_records(request, response, loginDTO);
                        return;
                    }
                    break;
                case "getGeo":
                    request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
                    return;
                case "validate":
                    validatePhotoAndSignature(request, response);
                    return;
                case "updateRemarks":
                    String pensionRemarks = request.getParameter("remarks");
                    String employeeRecordsId = request.getParameter("id");
                    employeeRecordsDAO.updateRemarks(Long.parseLong(employeeRecordsId), pensionRemarks);
                case "UploadFilesFromDropZone":
                    long ColumnID = Long.parseLong(request.getParameter("ColumnID"));
                    Utils.UploadFilesFromDropZone(request, response, userDTO.ID, ColumnID, getServletName());
                    return;
            }
        } catch (Exception ex) {
            logger.debug(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void validatePhotoAndSignature(HttpServletRequest request, HttpServletResponse response) {
        long empId = Long.parseLong(request.getParameter("ID"));
        Employee_recordsDTO employeeRecordsDTO = null;
        try {
            employeeRecordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(empId);
        } catch (Exception e) {
            logger.error("",e);
        }
        String validationMessage = null;
        try {
            String photoPartImage = getFileName(request.getPart("photo"));
            if (photoPartImage == null || photoPartImage.trim().length() == 0) {
                if (!(employeeRecordsDTO != null && employeeRecordsDTO.photo != null && employeeRecordsDTO.photo.length > 0)) {
                    sendMessageToServer("Invalid : Photo is not found", response);
                    return;
                }
            } else {
                validationMessage = validatePhoto(request.getPart("photo"), "Photo", 300, PROFILE_IMAGE_WIDTH, 100);
            }
            if (validationMessage != null) {
                sendMessageToServer(validationMessage, response);
                return;
            }
            photoPartImage = getFileName(request.getPart("signature"));
            if (photoPartImage == null || photoPartImage.trim().length() == 0) {
                if (!(employeeRecordsDTO != null && employeeRecordsDTO.photo != null && employeeRecordsDTO.photo.length > 0)) {
                    sendMessageToServer("Invalid : Signature is not found", response);
                    return;
                }
            } else {
                validationMessage = validatePhoto(request.getPart("signature"), "Signature", 80, PROFILE_IMAGE_WIDTH, 60);
            }
            if (validationMessage == null) {
                validationMessage = "Photo and Signature are valid";
            }
            PrintWriter out = response.getWriter();
            out.println(validationMessage);
            out.close();
        } catch (Exception e) {
            logger.error("",e);
        }
    }

    private void sendMessageToServer(String validationMessage, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            out.println(validationMessage);
            out.close();
        } catch (IOException e) {
            logger.error("",e);
        }
    }

    private String validatePhoto(Part photoPart, String fileType, int height, int width, long size) throws IOException {
        String photoPartImage = getFileName(photoPart);
        photoPartImage = Jsoup.clean(photoPartImage, Whitelist.simpleText());
        if (photoPartImage != null && !photoPartImage.equalsIgnoreCase("")) {
            if (photoPartImage.toLowerCase().endsWith(".jpg") || photoPartImage.toLowerCase().endsWith(".png")
                    || photoPartImage.toLowerCase().endsWith(".jpeg")) {
                byte[] photoByteArray = Utils.uploadFileToByteAray(photoPart);
                if (photoByteArray == null || photoByteArray.length == 0) {
                    return "Invalid : " + fileType + " is not found";
                }
                InputStream in = new ByteArrayInputStream(photoByteArray);
                BufferedImage buf = ImageIO.read(in);
                int photoHeight = buf.getHeight();
                int photoWidth = buf.getWidth();
                long fileSize = photoByteArray.length;
                in.close();
                if (photoHeight != height || photoWidth != width || fileSize > 1024 * size) {
                    return "Invalid : " + fileType + " size is invalid (width =" + photoWidth + ", height = " + photoHeight + ", and size = " + (double) (fileSize / 1024) + "Kb)";
                }
            } else {
                return "Invalid: " + fileType + " format is invalid ";
            }
        } else {
            return "Invalid : " + fileType + " is not found";
        }
        return null;
    }

    private void setCommonDataForMp(HttpServletRequest request, Election_wise_mpDTO electionWiseMpDTO1, Employee_recordsDTO employee_recordsDTO, boolean isLangEng) throws Exception {
        try {
            electionWiseMpDTO1.electionDetailsId = Long.parseLong(request.getParameter("parliamentNumber").trim());
        } catch (Exception ex) {
            throw new Exception(isLangEng ? "Please select parliament number" : "অনুগ্রহ করে সংসদ নাম্বার বাছাই করুন");
        }
        Election_detailsDTO electionDetailsDTO = Election_detailsRepository.getInstance().getElectionDetailsDTOByID(electionWiseMpDTO1.electionDetailsId);
        if (electionDetailsDTO == null) {
            throw new Exception(isLangEng ? "Please select correct parliament number" : "অনুগ্রহ করে সঠিক সংসদ নাম্বার বাছাই করুন");
        }

        if (electionDetailsDTO.parliamentStatusCat == 0) {
            throw new Exception(isLangEng ? "Parliament election - " + electionDetailsDTO.parliamentNumber + " is activated yet"
                    : "জাতীয় সংসদ নির্বাচন - " + StringUtils.convertToBanNumber(String.valueOf(electionDetailsDTO.parliamentNumber)) + " এখন পর্যন্ত সক্রিয় করা হয় নি");
        }

        /*if (electionDetailsDTO.parliamentStatusCat == 2) {
            throw new Exception(isLangEng ? "Already parliament election - " + electionDetailsDTO.parliamentNumber + " was being breakdown"
                    : "ইতিমধ্যে জাতীয় সংসদ নির্বাচন - " + StringUtils.convertToBanNumber(String.valueOf(electionDetailsDTO.parliamentNumber)) + " ভেঙ্গে ফেলা হয়েছে");
        }*/

        try {
            electionWiseMpDTO1.electionConstituencyId = Long.parseLong(request.getParameter("electionConstituencyOfMp").trim());
        } catch (Exception ex) {
            throw new Exception(isLangEng ? "Please select election constituency" : "অনুগ্রহ করে নির্বাচনী এলাকা বাছাই করুন");
        }
        Election_constituencyDTO electionConstituencyDTO = Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(electionWiseMpDTO1.electionConstituencyId);
        if (electionConstituencyDTO == null) {
            throw new Exception(isLangEng ? "Please select correct election constituency" : "অনুগ্রহ করে সঠিক নির্বাচনী এলাকা বাছাই করুন");
        }
        electionWiseMpDTO1.constituencyNumber = electionConstituencyDTO.constituencyNumber;

        try {
            electionWiseMpDTO1.politicalPartyId = Long.parseLong(request.getParameter("politicalPartyId").trim());
        } catch (Exception ex) {
            throw new Exception(isLangEng ? "Please select political party" : "অনুগ্রহ করে রাজনৈতিক দল বাছাই করুন");
        }
        Political_partyDTO politicalPartyDTO = Political_partyRepository.getInstance().getPolitical_partyDTOByID(electionWiseMpDTO1.politicalPartyId);
        if (politicalPartyDTO == null) {
            throw new Exception(isLangEng ? "Please select correct political party" : "অনুগ্রহ করে সঠিক রাজনৈতিক দল বাছাই করুন");
        }

        Election_wise_mpDTO electionWiseMpDTO = Election_wise_mpDAO.getInstance().getActiveMPByElectionDetailsIdAndElectionConstituencyId(electionWiseMpDTO1.electionDetailsId, electionWiseMpDTO1.electionConstituencyId);
        if (electionWiseMpDTO != null && electionWiseMpDTO.employeeRecordsId != employee_recordsDTO.iD) {
            throw new Exception(isLangEng ? "Already a MP is assigned for election : " + electionDetailsDTO.parliamentNumber + " and constituency : " + electionConstituencyDTO.constituencyNameEn
                    : "ইতিমধ্যেই একজন এমপি নিযুক্ত করা আছে, নির্বাচন : " + StringUtils.convertToBanNumber(String.valueOf(electionDetailsDTO.parliamentNumber)) + " এবং নির্বাচনী এলাকা  : " + electionConstituencyDTO.constituencyNameBn + " জন্য");
        }

        electionWiseMpDTO1.mpRemarks = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("mp_remark"));
        electionWiseMpDTO1.mpOrganization = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("mp_organization"));

        if (request.getParameter("electedCount") != null && request.getParameter("electedCount").trim().length() > 0) {
            employee_recordsDTO.mpElectedCount = Integer.parseInt(request.getParameter("electedCount").trim());
        } else {
            employee_recordsDTO.mpElectedCount = 0;
        }

        electionWiseMpDTO1.professionOfMP = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("ProfessionOfMP"));

        electionWiseMpDTO1.addressOfMP = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("ParliamentAddress"));

        byte[] altSign = convertByteArray(request.getPart("alternateImage"), "signature");
        if (altSign != null) {
            electionWiseMpDTO1.alternateImageOfMP = altSign;
        }

        electionWiseMpDTO1.startDate = employee_recordsDTO.joiningDate;
        electionWiseMpDTO1.dateOfBirth = employee_recordsDTO.dateOfBirth;
    }

    private void personalInfoEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
            Employee_recordsDTO employeeRecordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(Long.parseLong(request.getParameter("identity")));
            if (employeeRecordsDTO == null) {
                throw new Exception(isLangEng ? "Employee is not found" : "কর্মকর্তা পাওয়া যায়নি");
            }
            setPersonalData(request, employeeRecordsDTO, isLangEng);
            Election_wise_mpDTO electionWiseMpDTO = null;
            if (employeeRecordsDTO.isMP == 1) {
                electionWiseMpDTO = Election_wise_mpDAO.getInstance().getDTOFromID(employeeRecordsDTO.electionWiseMpId);
                if (electionWiseMpDTO == null) {
                    throw new Exception(isLangEng ? "MP info is not found" : "এমপির তথ্য পাওয়া যায়নি");
                }
            }
            employeeRecordsDAO.updatePersonalInformation(employeeRecordsDTO, electionWiseMpDTO);
            ApiResponse.sendSuccessResponse(response, isLangEng ? "Personal information submitted successfully" : "ব্যক্তিগত তথ্য সঠিকভাবে সাবমিট হয়েছে");
        } catch (Exception ex) {
            ApiResponse.sendErrorResponse(response, ex.getMessage());
        }
    }

    private void addressInfoEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
            Employee_recordsDTO employeeRecordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(Long.parseLong(request.getParameter("identity")));
            if (employeeRecordsDTO == null) {
                throw new Exception(isLangEng ? "Employee is not found" : "কর্মকর্তা পাওয়া যায়নি");
            }
            setAddressData(request, employeeRecordsDTO, isLangEng);
            Election_wise_mpDTO electionWiseMpDTO = null;
            if (employeeRecordsDTO.isMP == 1) {
                electionWiseMpDTO = Election_wise_mpDAO.getInstance().getDTOFromID(employeeRecordsDTO.electionWiseMpId);
                if (electionWiseMpDTO == null) {
                    throw new Exception(isLangEng ? "MP info is not found" : "এমপির তথ্য পাওয়া যায়নি");
                }
            }
            employeeRecordsDAO.updateAddressInformation(employeeRecordsDTO, electionWiseMpDTO);
            ApiResponse.sendSuccessResponse(response, isLangEng ? "Address information submitted successfully" : "ঠিকানা সঠিকভাবে সাবমিট হয়েছে");
        } catch (Exception ex) {
            ApiResponse.sendErrorResponse(response, ex.getMessage());
        }
    }

    private void LPRInfoEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
            Employee_recordsDTO employeeRecordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(Long.parseLong(request.getParameter("identity")));
            if (employeeRecordsDTO == null) {
                throw new Exception(isLangEng ? "Employee is not found" : "কর্মকর্তা পাওয়া যায়নি");
            }
            String oldEmployeeNumber = employeeRecordsDTO.employeeNumber;
            setLPRAndUseName(request, employeeRecordsDTO, oldEmployeeNumber);
            employeeRecordsDAO.updateLPRInformation(employeeRecordsDTO);
            ApiResponse.sendSuccessResponse(response, isLangEng ? "LPR information submitted successfully" : "এলপিআর সংক্রান্ত তথ্য সঠিকভাবে সাবমিট হয়েছে");
        } catch (Exception ex) {
            ApiResponse.sendErrorResponse(response, ex.getMessage());
        }
    }

    private void confidentialInfoEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
            Employee_recordsDTO employeeRecordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(Long.parseLong(request.getParameter("identity")));
            if (employeeRecordsDTO == null) {
                throw new Exception(isLangEng ? "Employee is not found" : "কর্মকর্তা পাওয়া যায়নি");
            }
            setConfidentialData(request, employeeRecordsDTO);
            employeeRecordsDAO.updateConfidentialInformation(employeeRecordsDTO);

            ApiResponse.sendSuccessResponse(response, isLangEng ? "Confidential information submitted successfully" : "গোপনীয় তথ্য সঠিকভাবে সাবমিট হয়েছে");
        } catch (Exception ex) {
            ApiResponse.sendErrorResponse(response, ex.getMessage());
        }
    }


    private void contactInfoEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
            Employee_recordsDTO employeeRecordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(Long.parseLong(request.getParameter("identity")));
            if (employeeRecordsDTO == null) {
                throw new Exception(isLangEng ? "Employee is not found" : "কর্মকর্তা পাওয়া যায়নি");
            }
            setContactData(request, employeeRecordsDTO, isLangEng);
            employeeRecordsDAO.updateContactInformation(employeeRecordsDTO);
            ApiResponse.sendSuccessResponse(response, isLangEng ? "Contact information submitted successfully" : "প্রয়োজনে যোগাযোগের তথ্য সঠিকভাবে সাবমিট হয়েছে");
        } catch (Exception ex) {
            ApiResponse.sendErrorResponse(response, ex.getMessage());
        }
    }


    private void miscellaneousInfoEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
            Employee_recordsDTO employeeRecordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(Long.parseLong(request.getParameter("identity")));
            if (employeeRecordsDTO == null) {
                throw new Exception(isLangEng ? "Employee is not found" : "কর্মকর্তা পাওয়া যায়নি");
            }
            setMiscellaneousData(request, employeeRecordsDTO, isLangEng);
            employeeRecordsDAO.updateMiscellaneousInformation(employeeRecordsDTO);
            ApiResponse.sendSuccessResponse(response, isLangEng ? "Miscellaneous information submitted successfully" : "বিবিধ তথ্য সঠিকভাবে সাবমিট হয়েছে");
        } catch (Exception ex) {
            ApiResponse.sendErrorResponse(response, ex.getMessage());
        }
    }

    private void nidInfoEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
            Employee_recordsDTO employeeRecordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(Long.parseLong(request.getParameter("identity")));
            if (employeeRecordsDTO == null) {
                throw new Exception(isLangEng ? "Employee is not found" : "কর্মকর্তা পাওয়া যায়নি");
            }
            setNIDData(request, employeeRecordsDTO);
            employeeRecordsDAO.updateNIDInformation(employeeRecordsDTO);
            ApiResponse.sendSuccessResponse(response, isLangEng ? "National ID Card information submitted successfully" : "জাতীয় পরিচয়পত্রের তথ্য সঠিকভাবে সাবমিট হয়েছে");
        } catch (Exception ex) {
            ApiResponse.sendErrorResponse(response, ex.getMessage());
        }
    }

    private void imageInfoEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
            Employee_recordsDTO employeeRecordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(Long.parseLong(request.getParameter("identity")));
            if (employeeRecordsDTO == null) {
                throw new Exception(isLangEng ? "Employee is not found" : "কর্মকর্তা পাওয়া যায়নি");
            }
            setImageData(request, employeeRecordsDTO);
            employeeRecordsDAO.updateImageInformation(employeeRecordsDTO);
            ApiResponse.sendSuccessResponse(response, isLangEng ? "Photo & Signature information submitted successfully" : "ছবি ও স্বাক্ষরের তথ্য সঠিকভাবে সাবমিট হয়েছে");
        } catch (Exception ex) {
            ApiResponse.sendErrorResponse(response, ex.getMessage());
        }
    }

    private void passwordInfoEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
            Employee_recordsDTO employeeRecordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(Long.parseLong(request.getParameter("identity")));
            if (employeeRecordsDTO == null) {
                throw new Exception(isLangEng ? "Employee is not found" : "কর্মকর্তা পাওয়া যায়নি");
            }
            boolean updatePassword = passwordData(request, employeeRecordsDTO, isLangEng);
            if (!employeeRecordsDTO.password.isEmpty()) {
                updatePassword = true;
                employeeRecordsDAO.updatePasswordInformation(employeeRecordsDTO);
            }

            Election_wise_mpDTO electionWiseMpDTO;
            if (employeeRecordsDTO.isMP == 1) {
                electionWiseMpDTO = Election_wise_mpDAO.getInstance().getDTOFromID(employeeRecordsDTO.electionWiseMpId);
                if (electionWiseMpDTO == null) {
                    throw new Exception(isLangEng ? "MP info is not found" : "এমপির তথ্য পাওয়া যায়নি");
                }
                Election_detailsDTO electionDetailsDTO = Election_detailsRepository.getInstance().getElectionDetailsDTOByID(electionWiseMpDTO.electionDetailsId);
                if (electionDetailsDTO.parliamentStatusCat == 1) {
                    sendNotificationForUpdate(updatePassword, false, employeeRecordsDTO);
                }
            } else {
                sendNotificationForUpdate(updatePassword, false, employeeRecordsDTO);

            }

            ApiResponse.sendSuccessResponse(response, isLangEng ? "Password information submitted successfully" : "পাসওয়ার্ডের তথ্য সঠিকভাবে সাবমিট হয়েছে");
        } catch (Exception ex) {
            ApiResponse.sendErrorResponse(response, ex.getMessage());
        }
    }

    private void employmentInfoEdit(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
            UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
            boolean loginUserIsAdmin = userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId() || userDTO.roleID == RoleEnum.DATA_ENTRY.getRoleId();
            boolean usernameChanged = false;
            Employee_recordsDTO employeeRecordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(Long.parseLong(request.getParameter("identity")));
            if (employeeRecordsDTO == null) {
                throw new Exception(isLangEng ? "Employee is not found" : "কর্মকর্তা পাওয়া যায়নি");
            }
            try {
                employeeRecordsDTO.joiningDate = simpleDateFormat.parse(request.getParameter("joiningDate")).getTime();
            } catch (Exception ex) {
                throw new Exception(isLangEng ? "Please enter joining date" : "অনুগ্রহ করে যোগদান তারিখ দিন");
            }

            Election_wise_mpDTO electionWiseMpDTO = null;
            String oldEmployeeNumber = employeeRecordsDTO.employeeNumber;
            if (employeeRecordsDTO.isMP == 1) {
                electionWiseMpDTO = setDataForMP(request, employeeRecordsDTO, isLangEng);
                if (loginUserIsAdmin) {
                    setCommonDataForMp(request, electionWiseMpDTO, employeeRecordsDTO, isLangEng);
                    usernameChanged = validateAndSetEditDataForMp(request, employeeRecordsDTO, electionWiseMpDTO, isLangEng);
                }
            } else {
                if (loginUserIsAdmin) {
                    setCommonDataForEmployee(request, employeeRecordsDTO, isLangEng);
                    usernameChanged = setLPRAndUseName(request, employeeRecordsDTO, oldEmployeeNumber);
                }
            }
            employeeRecordsDTO.password = "";
            employeeRecordsDAO.updateEmploymentInformation(employeeRecordsDTO, electionWiseMpDTO);
            boolean updatePassword = !employeeRecordsDTO.password.isEmpty();
            if (employeeRecordsDTO.isMP == 1 && electionWiseMpDTO != null) {
                Election_detailsDTO electionDetailsDTO = Election_detailsRepository.getInstance().getElectionDetailsDTOByID(electionWiseMpDTO.electionDetailsId);
                if (electionDetailsDTO != null && electionDetailsDTO.parliamentStatusCat == 1) {
                    sendNotificationForUpdate(updatePassword, usernameChanged, employeeRecordsDTO);
                }
            } else {
                sendNotificationForUpdate(updatePassword, usernameChanged, employeeRecordsDTO);

            }
            responseForSave(request, response, employeeRecordsDTO, userDTO);
        } catch (Exception ex) {
            ApiResponse.sendErrorResponse(response, ex.getMessage());
        }
    }

    private void responseForSave(HttpServletRequest request, HttpServletResponse response, Employee_recordsDTO employeeRecordsDTO, UserDTO userDTO) throws IOException {
        if (employeeRecordsDTO.iD == userDTO.employee_record_id) {
            ApiResponse.sendSuccessResponse(response, "Employee_recordsServlet?actionType=viewMyProfile&tab=1&ID=" + employeeRecordsDTO.iD + "&userId=" + request.getParameter("userId"));
        } else {
            ApiResponse.sendSuccessResponse(response, "Employee_recordsServlet?actionType=viewMultiForm&tab=1&ID=" + employeeRecordsDTO.iD + "&userId=" + request.getParameter("userId"));
        }
    }

    private void checkPassword(String password, String confirmPassword, boolean isLangEng) throws Exception {
        if (password == null || password.trim().length() == 0) {
            throw new Exception(isLangEng ? "Please enter password" : "পাসওয়ার্ড দিন");
        }
        if (confirmPassword == null || confirmPassword.trim().length() == 0) {
            throw new Exception(isLangEng ? "Please enter confirm password" : "পাসওয়ার্ড নিশ্চিত দিন");
        }
        if (!password.equals(confirmPassword)) {
            throw new Exception(isLangEng ? "Confirm password doesn't matched with password" : "নিশ্চিত পাসওয়ার্ড  পাসওয়ার্ডের সাথে মিলে নাই");
        }
    }

    private boolean passwordData(HttpServletRequest request, Employee_recordsDTO employee_recordsDTO, boolean isLangEng) throws Exception {
        boolean updatePassword = false;
        String value = request.getParameter("changePassword");
        if (value != null && !value.equalsIgnoreCase("")) {
            updatePassword = value.equalsIgnoreCase("on");
        }

        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("confirmPassword");
        if (updatePassword) {
            checkPassword(password, confirmPassword, isLangEng);
            employee_recordsDTO.password = password;
        } else {
            employee_recordsDTO.password = "";
        }
        return updatePassword;
    }

    private void edit(HttpServletRequest request, HttpServletResponse response) throws IOException {
        boolean updatePassword, usernameChanged = false;
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        boolean loginUserIsAdmin = userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId() || userDTO.roleID == RoleEnum.DATA_ENTRY.getRoleId();
        try {
            Employee_recordsDTO employeeRecordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(Long.parseLong(request.getParameter("identity")));
            if (employeeRecordsDTO == null) {
                throw new Exception(isLangEng ? "Employee is not found" : "কর্মকর্তা পাওয়া যায়নি");
            }

            try {
                employeeRecordsDTO.joiningDate = simpleDateFormat.parse(request.getParameter("joiningDate")).getTime();
            } catch (Exception ex) {
                throw new Exception(isLangEng ? "Please enter joining date" : "অনুগ্রহ করে যোগদান তারিখ দিন");
            }

            updatePassword = passwordData(request, employeeRecordsDTO, isLangEng);
            setCommonData(request, employeeRecordsDTO, isLangEng);
            String oldEmployeeNumber = employeeRecordsDTO.employeeNumber;
            Election_wise_mpDTO electionWiseMpDTO = null;
            if (employeeRecordsDTO.isMP == 1) {
                electionWiseMpDTO = setDataForMP(request, employeeRecordsDTO, isLangEng);
                if (loginUserIsAdmin) {
                    setCommonDataForMp(request, electionWiseMpDTO, employeeRecordsDTO, isLangEng);
                    usernameChanged = validateAndSetEditDataForMp(request, employeeRecordsDTO, electionWiseMpDTO, isLangEng);
                }
            } else {
                if (loginUserIsAdmin) {
                    setCommonDataForEmployee(request, employeeRecordsDTO, isLangEng);
                    usernameChanged = setLPRAndUseName(request, employeeRecordsDTO, oldEmployeeNumber);
                }
            }
            employeeRecordsDAO.updateEmployee_records(employeeRecordsDTO, electionWiseMpDTO);
            if (!employeeRecordsDTO.password.isEmpty()) {
                updatePassword = true;
            }
            if (employeeRecordsDTO.isMP == 1 && electionWiseMpDTO != null) {
                Election_detailsDTO electionDetailsDTO = Election_detailsRepository.getInstance().getElectionDetailsDTOByID(electionWiseMpDTO.electionDetailsId);
                if (electionDetailsDTO != null && electionDetailsDTO.parliamentStatusCat == 1) {
                    sendNotificationForUpdate(updatePassword, usernameChanged, employeeRecordsDTO);
                }
            } else {
                sendNotificationForUpdate(updatePassword, usernameChanged, employeeRecordsDTO);
            }
            if (employeeRecordsDTO.iD == userDTO.employee_record_id) {
                ApiResponse.sendSuccessResponse(response, "Employee_recordsServlet?actionType=viewMyProfile&tab=1&ID=" + employeeRecordsDTO.iD + "&userId=" + request.getParameter("userId"));
            } else {
                ApiResponse.sendSuccessResponse(response, "Employee_recordsServlet?actionType=viewMultiForm&tab=1&ID=" + employeeRecordsDTO.iD + "&userId=" + request.getParameter("userId"));
            }
        } catch (Exception ex) {
            ApiResponse.sendErrorResponse(response, ex.getMessage());
        }
    }

    private Election_wise_mpDTO setDataForMP(HttpServletRequest request, Employee_recordsDTO employeeRecordsDTO, boolean isLangEng) throws Exception {
        String value;
        Election_wise_mpDTO electionWiseMpDTO = Election_wise_mpDAO.getInstance().getDTOFromID(employeeRecordsDTO.electionWiseMpId);
        if (electionWiseMpDTO == null) {
            throw new Exception(isLangEng ? "MP info is not found" : "এমপির তথ্য পাওয়া যায়নি");
        }
        value = request.getParameter("mp_hobbies");
        value = value != null ? Jsoup.clean(value, Whitelist.simpleText()) : "";
        electionWiseMpDTO.mpHobbies = value;

        value = request.getParameter("parliamentary_designation");
        value = value != null ? Jsoup.clean(value, Whitelist.simpleText()) : "";
        electionWiseMpDTO.parliamentaryDesignation = value;

        value = request.getParameter("mpGazetteDate");
        if (value != null && !value.equalsIgnoreCase("") && !value.equalsIgnoreCase("NaN")) {
            try {
                electionWiseMpDTO.mpGazetteDate = simpleDateFormat.parse(value).getTime();
            } catch (Exception ex) {
                logger.error("",ex);
                electionWiseMpDTO.mpGazetteDate = SessionConstants.MIN_DATE;
            }
        } else {
            electionWiseMpDTO.mpGazetteDate = SessionConstants.MIN_DATE;
        }

        value = request.getParameter("oathDate");
        if (value != null && !value.equalsIgnoreCase("") && !value.equalsIgnoreCase("NaN")) {
            try {
                electionWiseMpDTO.oathDate = simpleDateFormat.parse(value).getTime();
            } catch (Exception ex) {
                logger.error("",ex);
                electionWiseMpDTO.oathDate = SessionConstants.MIN_DATE;
            }
        } else {
            electionWiseMpDTO.oathDate = SessionConstants.MIN_DATE;
        }

        value = request.getParameter("electionDate");
        if (value != null && !value.equalsIgnoreCase("") && !value.equalsIgnoreCase("NaN")) {
            try {
                electionWiseMpDTO.electionDate = simpleDateFormat.parse(value).getTime();
            } catch (Exception ex) {
                logger.error("",ex);
                electionWiseMpDTO.electionDate = SessionConstants.MIN_DATE;
            }
        } else {
            electionWiseMpDTO.electionDate = SessionConstants.MIN_DATE;
        }

        value = request.getParameter("mpStatusChangeDate");
        if (value != null && !value.equalsIgnoreCase("") && !value.equalsIgnoreCase("NaN")) {
            try {
                electionWiseMpDTO.mpStatusChangeDate = simpleDateFormat.parse(value).getTime();
            } catch (Exception ex) {
                logger.error("",ex);
                electionWiseMpDTO.mpStatusChangeDate = SessionConstants.MIN_DATE;
            }
        } else {
            electionWiseMpDTO.mpStatusChangeDate = SessionConstants.MIN_DATE;
        }

        byte[] mpGazetteDocumentArray = convertPdfToByteArray(request.getPart("mp_gazette_doc"));
        if (mpGazetteDocumentArray != null) {
            electionWiseMpDTO.mpGazetteDocument = mpGazetteDocumentArray;
        }
        return electionWiseMpDTO;
    }

    private boolean validateAndSetEditDataForMp(HttpServletRequest request, Employee_recordsDTO employeeRecordsDTO, Election_wise_mpDTO electionWiseMpDTO, boolean isLangEng) throws Exception {
        MpStatusEnum mpStatusEnum = MpStatusEnum.getByValue(electionWiseMpDTO.mpStatus);
        String prefix = generateUserNamePrefixForMP(electionWiseMpDTO.electionDetailsId, electionWiseMpDTO.electionConstituencyId);
        boolean usernameChanged = !employeeRecordsDTO.employeeNumber.startsWith(prefix);
        if (usernameChanged) {
            if (mpStatusEnum == MpStatusEnum.ACTIVE) {
                int parliamentNumber = Integer.parseInt(employeeRecordsDTO.employeeNumber.substring(1, 3));
                Election_detailsDTO electionDetailsDTO = Election_detailsRepository.getInstance().getElection_detailsDTOByParliamentNumber(parliamentNumber);
                int constituencyNumber = Integer.parseInt(employeeRecordsDTO.employeeNumber.substring(3, 6));
                Election_constituencyDTO electionConstituencyDTO = Election_constituencyRepository.getInstance().getElection_constituencyDTOByConstituencyNumber(electionDetailsDTO.iD, constituencyNumber);
                throw new Exception(isLangEng ? "Already assigned as MP in " + electionConstituencyDTO.constituencyNameEn + " area"
                        : "ইতিমধ্যে " + electionConstituencyDTO.constituencyNameBn + " নির্বাচনী এলাকার জন্য এমপি হিসেবে নিযুক্ত আছেন");
            }
        }

        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        if (!usernameChanged) {
            MpStatusEnum newMpStatusEnum = mpStatusEnum;
            if(request.getParameter("mpStatus") != null){
                try {
                    int mpStatus = Integer.parseInt(request.getParameter("mpStatus").trim());
                    newMpStatusEnum = MpStatusEnum.getByValue(mpStatus);
                    if (newMpStatusEnum == null) {
                        throw new Exception("");
                    }
                } catch (Exception ex) {
                    logger.error("Illegal MP status : " + request.getParameter("mpStatus"));
                    throw new Exception(isLangEng ? "Please select correct MP status" : "অনুগ্রহ করে সঠিক এমপি অবস্থা বাছাই করুন");
                }
            }
            electionWiseMpDTO.endDate = electionWiseMpDTO.mpStatusChangeDate;
            if (newMpStatusEnum != mpStatusEnum) {
                if (mpStatusEnum != MpStatusEnum.ACTIVE) {
                    throw new Exception(isLangEng ? "Can't change MP status" : "এমপি অবস্থা পরিবর্তন যোগ্য নয়");
                }
                electionWiseMpDTO.mpStatus = newMpStatusEnum.getValue();
//                electionWiseMpDTO.mpStatusChangeDate = System.currentTimeMillis();
                employeeRecordsDTO.status = false;

                // Now constituency will be vacant and need to update election_constituency
                Election_constituencyDTO election_constituencyDTO = Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(electionWiseMpDTO.electionConstituencyId);
                election_constituencyDTO.isVacant = 1;
                Election_constituencyDAO.getInstance().update(election_constituencyDTO);
            }
        } else {
            electionWiseMpDTO.isAdd = true;
            electionWiseMpDTO.iD = DBMW.getInstance().getNextSequenceId("election_wise_mp");
            employeeRecordsDTO.electionWiseMpId = electionWiseMpDTO.iD;
            employeeRecordsDTO.status = true;
            employeeRecordsDTO.employeeNumber = iDGenerationOfMP(prefix);
            electionWiseMpDTO.userName = employeeRecordsDTO.employeeNumber;
            electionWiseMpDTO.mpStatus = MpStatusEnum.ACTIVE.getValue();
            electionWiseMpDTO.insertedBy = String.valueOf(userDTO.employee_record_id);
            electionWiseMpDTO.insertionDate = System.currentTimeMillis();
        }
        electionWiseMpDTO.modifiedBy = String.valueOf(userDTO.employee_record_id);
        return usernameChanged;
    }

    private boolean setLPRAndUseName(HttpServletRequest request, Employee_recordsDTO employeeRecordsDTO, String oldEmployeeNumber) {
        boolean usernameChanged = false;
        String extendPRL = request.getParameter("extendPRL");
        if ("1".equals(extendPRL)) {
            try {
                employeeRecordsDTO.lprDate = simpleDateFormat.parse(request.getParameter("lprDate").trim()).getTime();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date(employeeRecordsDTO.lprDate));
                calendar.add(Calendar.YEAR, RETIREMENT_DURATION);
                employeeRecordsDTO.retirementDate = calendar.getTime().getTime();
                employeeRecordsDTO.employmentStatus = EmploymentStatusEnum.ACTIVE.getValue();
                employeeRecordsDTO.deactivationReason= "Extension";
                employeeRecordsDTO.status = true;
            } catch (Exception ex) {
                logger.error("",ex);
                logger.error(ex.getMessage());
            }
        }
        String prefix = EmployeeMPModel.generatePrefixOfSecretary(employeeRecordsDTO.employmentType, employeeRecordsDTO.officerTypeCat, employeeRecordsDTO.employeeClass);
        if (oldEmployeeNumber.length() < 3 || !oldEmployeeNumber.substring(0, 4).equals(prefix)) {
            employeeRecordsDTO.employeeNumber = EmployeeMPModel.iDGenerationOfSecretary(prefix);
            employeeRecordsDTO.employeeNumberBng = Utils.getDigits(employeeRecordsDTO.employeeNumber, false);
            usernameChanged = true;
        }
        return usernameChanged;
    }

    private void sendNotificationForUpdate(boolean updatePassword, boolean usernameChanged, Employee_recordsDTO employee_recordsDTO) {
        if (usernameChanged || updatePassword) {
            Utils.runIOTaskInASeparateThread(() -> () -> {
                Sms_logDTO smsLogDTO = new Sms_logDTO();
                smsLogDTO.insertionTime = Calendar.getInstance().getTimeInMillis();
                smsLogDTO.receiver = employee_recordsDTO.personalMobile;
                String mailSubject;
                if (usernameChanged && updatePassword) {
                    smsLogDTO.message = String.format(smsBodyForUserNameAndPasswordUpdate, employee_recordsDTO.nameEng, employee_recordsDTO.employeeNumber, employee_recordsDTO.password);
                    mailSubject = "User name and password are updated";
                } else if (usernameChanged) {
                    smsLogDTO.message = String.format(smsBodyForUserNameUpdate, employee_recordsDTO.nameEng, employee_recordsDTO.employeeNumber);
                    mailSubject = "User name is updated";
                } else {
                    smsLogDTO.message = String.format(smsBodyForPasswordUpdate, employee_recordsDTO.nameEng, employee_recordsDTO.employeeNumber, employee_recordsDTO.password);
                    mailSubject = "Password is updated";
                }
                try {
                    SmsService.send(smsLogDTO.receiver, smsLogDTO.message);
                } catch (IOException e) {
                    logger.error("",e);
                }
                sendMail(employee_recordsDTO.personalEml, smsLogDTO.message, mailSubject);
            });
        }
    }

    private void setCommonDataForEmployee(HttpServletRequest request, Employee_recordsDTO employee_recordsDTO, boolean isLangEng) throws Exception {
        CategoryLanguageModel model;

        try {
            employee_recordsDTO.officerTypeCat = Integer.parseInt(request.getParameter("officerType"));
        } catch (Exception ex) {
            throw new Exception(isLangEng ? "Please select officer type" : "অনুগ্রহ করে অফিসারের ধরন বাছাই করুন");
        }
        model = CatRepository.getInstance().getCategoryLanguageModel("emp_officer", employee_recordsDTO.officerTypeCat);
        if (model == null) {
            throw new Exception(isLangEng ? "Please select correct officer type" : "অনুগ্রহ করে সঠিক অফিসারের ধরন বাছাই করুন");
        }
        
        try {
            employee_recordsDTO.payScale = Integer.parseInt(request.getParameter("payScale"));
        } catch (Exception ex) {
            throw new Exception(isLangEng ? "Please select Pay Scale" : "অনুগ্রহ করে পে স্কেলবাছাই করুন");
        }

        try {
            employee_recordsDTO.employeeClass = Integer.parseInt(request.getParameter("employeeClassType"));
        } catch (Exception ex) {
            throw new Exception(isLangEng ? "Please select employee class" : "অনুগ্রহ করে কর্মকর্তার শ্রেণী বাছাই করুন");
        }
        model = CatRepository.getInstance().getCategoryLanguageModel("employee_class", employee_recordsDTO.employeeClass);
        if (model == null) {
            throw new Exception(isLangEng ? "Please select correct employee class" : "অনুগ্রহ করে সঠিক কর্মকর্তার শ্রেণী বাছাই করুন");
        }
        
        
        try {
            employee_recordsDTO.employmentType = Integer.parseInt(request.getParameter("employmentType"));
        } catch (Exception ex) {
            throw new Exception(isLangEng ? "Please select employment category" : "অনুগ্রহ করে চাকুরির ধরন বাছাই করুন");
        }
        model = CatRepository.getInstance().getCategoryLanguageModel("employment", employee_recordsDTO.employmentType);
        if (model == null) {
            throw new Exception(isLangEng ? "Please select correct employment category" : "অনুগ্রহ করে সঠিক চাকুরির ধরন বাছাই করুন");
        }

        String value = request.getParameter("provision_period");
        if (value != null && !value.equalsIgnoreCase("")) {
            employee_recordsDTO.provisionPeriod = Long.parseLong(value);
            Calendar c = Calendar.getInstance();
            c.setTime(new Date(employee_recordsDTO.joiningDate));
            c.add(Calendar.MONTH, (int) employee_recordsDTO.provisionPeriod);
            c.add(Calendar.DATE, -1);
            employee_recordsDTO.provisionEndDate = c.getTimeInMillis();
        } else {
            employee_recordsDTO.provisionPeriod = 0;
        }

        value = request.getParameter("employmentStatus");
        if (value != null && !value.equalsIgnoreCase("")) {
            employee_recordsDTO.employmentStatus = Integer.parseInt(value);
        }

        value = request.getParameter("employmentStatusChangeDate");
        if (value != null && !value.equalsIgnoreCase("") && employee_recordsDTO.employmentStatus != 1) {
            employee_recordsDTO.employmentStatusChangeDate = simpleDateFormat.parse(value).getTime();
        }
        
        value = request.getParameter("currentOffice");
        if (value != null && !value.equalsIgnoreCase("")) {
            employee_recordsDTO.currentOffice = Long.parseLong(value);
        }
        
        value = request.getParameter("currentDesignation");
        if (value != null && !value.equalsIgnoreCase("")) {
            employee_recordsDTO.currentDesignation = (value);
        }
        
        value = request.getParameter("otherOfficeDeptType");
        if (value != null && !value.equalsIgnoreCase("")) {
            employee_recordsDTO.otherOfficeDeptType = Long.parseLong(value);
        }
    }

    private void setPersonalData(HttpServletRequest request, Employee_recordsDTO employee_recordsDTO, boolean isLangEng) throws Exception {
        String value;
        NameDTO nameDTO;
        CategoryLanguageModel model;
        value = request.getParameter("nameEng") == null ? null : Jsoup.clean(request.getParameter("nameEng"), Whitelist.simpleText());
        if (value == null || value.trim().length() == 0) {
            throw new Exception(isLangEng ? "English name is missing" : "ইংরেজীতে নাম পাওয়া যায়নি");
        }
        employee_recordsDTO.nameEng = value;

        value = request.getParameter("nameBng") == null ? null : Jsoup.clean(request.getParameter("nameBng"), Whitelist.simpleText());
        if (value == null || value.trim().length() == 0) {
            throw new Exception(isLangEng ? "Bangla name is missing" : "বাংলায় নাম পাওয়া যায়নি");
        }
        employee_recordsDTO.nameBng = value;

        value = request.getParameter("fatherNameEng");
        value = value != null ? Jsoup.clean(value, Whitelist.simpleText()) : "";
        employee_recordsDTO.fatherNameEng = value;

        value = request.getParameter("fatherNameBng");
        value = value != null ? Jsoup.clean(value, Whitelist.simpleText()) : "";
        employee_recordsDTO.fatherNameBng = value;

        value = request.getParameter("motherNameEng");
        value = value != null ? Jsoup.clean(value, Whitelist.simpleText()) : "";
        employee_recordsDTO.motherNameEng = value;

        value = request.getParameter("motherNameBng");
        value = value != null ? Jsoup.clean(value, Whitelist.simpleText()) : "";
        employee_recordsDTO.motherNameBng = value;

        try {
            employee_recordsDTO.dateOfBirth = simpleDateFormat.parse(request.getParameter("dateOfBirth").trim()).getTime();
        } catch (Exception ex) {
            throw new Exception(isLangEng ? "Please enter correct date of birth" : "অনুগ্রহ করে সঠিক জন্মতারিখ দিন");
        }


        try {
            employee_recordsDTO.gender = Integer.parseInt(request.getParameter("gender").trim());
            model = CatRepository.getInstance().getCategoryLanguageModel("gender", employee_recordsDTO.gender);
            if (model == null) {
                throw new Exception(isLangEng ? "Please select correct gender" : "অনুগ্রহ করে সঠিকভাবে লিঙ্গ বাছাই করুন");
            }
        } catch (Exception ex) {
            logger.error("Invalid gender input : " + value);
            throw new Exception(isLangEng ? "Please select correct gender" : "অনুগ্রহ করে সঠিকভাবে লিঙ্গ বাছাই করুন");
        }

        value = request.getParameter("religion");
        if (value != null && !value.trim().equals("") && !value.trim().equals("0")) {
            try {
                employee_recordsDTO.religion = Long.parseLong(value.trim());
                nameDTO = ReligionRepository.getInstance().getDTOByID(employee_recordsDTO.religion);
                if (nameDTO == null) {
                    throw new Exception(isLangEng ? "Please select correct religion" : "অনুগ্রহ করে সঠিকভাবে ধর্ম বাছাই করুন");
                }
            } catch (Exception ex) {
                throw new Exception(isLangEng ? "Please select correct religion" : "অনুগ্রহ করে সঠিকভাবে ধর্ম বাছাই করুন");
            }
        } else {
            employee_recordsDTO.religion = 0;
        }
        try {
            employee_recordsDTO.nationalityType = Long.parseLong(request.getParameter("nationalityType"));
        } catch (Exception ex) {
            throw new Exception(isLangEng ? "Please select nationality" : "অনুগ্রহ করে জাতীয়তা বাছাই করুন");
        }

        nameDTO = NationalityRepository.getInstance().getDTOByID(employee_recordsDTO.nationalityType);
        if (nameDTO == null) {
            throw new Exception(isLangEng ? "Please select correct nationality" : "অনুগ্রহ করে সঠিক জাতীয়তা বাছাই করুন");
        }

    }

    private void setContactData(HttpServletRequest request, Employee_recordsDTO employee_recordsDTO, boolean isLangEng) throws Exception {
        String value;
        value = request.getParameter("personalEml");

        employee_recordsDTO.personalEml = Jsoup.clean(value, Whitelist.simpleText());


        value = request.getParameter("personalMobile");
        if (value != null && Utils.isMobileNumberValid("88" + value)) {
            employee_recordsDTO.personalMobile = "88" + value;
        } else {
            throw new Exception(isLangEng ? "Please enter valid personal mobile number(01XXXXXXXXX)" : "অনুগ্রহ করে সঠিক ব্যক্তিগত মোবাইল নাম্বার দিন (01XXXXXXXXX)");
        }

        value = request.getParameter("alternativeMobile");
        value = value != null ? value.trim() : null;
        if (value != null && value.length() > 0) {
            if (Utils.isMobileNumberValid("88" + value)) {
                employee_recordsDTO.alternativeMobile = "88" + value;
            } else {
                throw new Exception(isLangEng ? "Please enter valid alternative mobile number(01XXXXXXXXX)" : "অনুগ্রহ করে সঠিক বিকল্প মোবাইল নাম্বার দিন (01XXXXXXXXX)");
            }
        } else {
            employee_recordsDTO.alternativeMobile = "";
        }

        value = request.getParameter("officePhoneNumber");
        value = value != null ? value.trim() : null;
        if (value != null) {
            if (Utils.digitOnlyOrEmptyValid(value)) {
                employee_recordsDTO.officePhoneNumber = value;
            } else {
                throw new Exception(isLangEng ? "Please enter valid office phone number(digit only)" : "অনুগ্রহ করে সঠিক ফোন নাম্বার (অফিস) দিন (শুধুমাত্র সংখ্যা)");
            }
        }

        value = request.getParameter("officePhoneExtension");
        value = value != null ? value.trim() : null;
        if (value != null) {
            if (Utils.digitOnlyOrEmptyValid(value)) {
                employee_recordsDTO.officePhoneExtension = value;
            } else {
                throw new Exception(isLangEng ? "Please enter valid office phone extension(digit only)" : "অনুগ্রহ করে সঠিক অফিস ফোন এক্সটেনশন দিন (শুধুমাত্র সংখ্যা)");
            }
        }

        value = request.getParameter("faxNumber");
        employee_recordsDTO.faxNumber = value != null ? Jsoup.clean(value, Whitelist.simpleText()) : "";

        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        if(userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId() || userDTO.roleID == RoleEnum.DATA_ENTRY.getRoleId()){
            value = request.getParameter("officialEml");
            employee_recordsDTO.officialEml = Jsoup.clean(value, Whitelist.simpleText());
        }

    }

    private void setAddressData(HttpServletRequest request, Employee_recordsDTO employee_recordsDTO, boolean isLangEng) throws Exception {
        String value;
        int x;

        value = request.getParameter("permanentAddressEng") == null ? null : Jsoup.clean(request.getParameter("permanentAddressEng"), Whitelist.simpleText());
        if (value == null) {
            throw new Exception(isLangEng ? "Please enter permanent address in english" : "অনুগ্রহ করে স্থায়ী ঠিকানা ইংরেজীতে দিন");
        }
        String[] address = value.split("@@");
        if (address.length < 5) {
            throw new Exception(isLangEng ? "Please enter permanent address in english correctly" : "অনুগ্রহ করে সঠিকভাবে স্থায়ী ঠিকানা ইংরেজীতে দিন");
        }
        employee_recordsDTO.permanentDivisionId = Long.parseLong(address[0]);
        GeoLocationDTO geoLocationDTO = GeoLocationRepository.getInstance().getById((int) employee_recordsDTO.permanentDivisionId);
        if (geoLocationDTO == null) {
            throw new Exception(isLangEng ? "Please select correct permanent division in english" : "অনুগ্রহ করে সঠিকভাবে স্থায়ী বিভাগ ইংরেজীতে বাছাই করুন");
        }
        employee_recordsDTO.permanentDistrictId = Long.parseLong(address[1]);
        GeoLocationDTO geoLocationDTO1 = GeoLocationRepository.getInstance().getById((int) employee_recordsDTO.permanentDistrictId);
        if (geoLocationDTO1 == null || geoLocationDTO1.parentID != geoLocationDTO.id) {
            throw new Exception(isLangEng ? "Please select correct permanent district in english" : "অনুগ্রহ করে সঠিকভাবে স্থায়ী জেলা ইংরেজীতে বাছাই করুন");
        }
        geoLocationDTO = geoLocationDTO1;
        geoLocationDTO1 = GeoLocationRepository.getInstance().getById(Integer.parseInt(address[2]));
        if (geoLocationDTO1 == null || geoLocationDTO1.parentID != geoLocationDTO.id) {
            throw new Exception(isLangEng ? "Please select correct permanent upazila in english" : "অনুগ্রহ করে সঠিকভাবে স্থায়ী উপজেলা ইংরেজীতে বাছাই করুন");
        }
        geoLocationDTO = geoLocationDTO1;
        x = Integer.parseInt(address[3]);
        if (x != 0) {
            geoLocationDTO1 = GeoLocationRepository.getInstance().getById(x);
            if (geoLocationDTO1 == null || geoLocationDTO1.parentID != geoLocationDTO.id) {
                throw new Exception(isLangEng ? "Please select correct permanent paurashava in english" : "অনুগ্রহ করে সঠিকভাবে স্থায়ী পৌরসভা ইংরেজীতে বাছাই করুন");
            }
        }
        employee_recordsDTO.permanentAddress = value.trim();

        value = request.getParameter("permanentAddressBng") == null ? null : Jsoup.clean(request.getParameter("permanentAddressBng"), Whitelist.simpleText());
        if (value == null) {
            throw new Exception(isLangEng ? "Please enter permanent address in bangla" : "অনুগ্রহ করে বর্ত্মান ঠিকানা বাংলা দিন");
        }
        String[] addressBng = value.split("@@");
        if (addressBng.length < 5) {
            throw new Exception(isLangEng ? "Please enter permanent address in bangla correctly" : "অনুগ্রহ করে সঠিকভাবে বর্ত্মান ঠিকানা বাংলা দিন");
        }
        if (!addressBng[0].equals(address[0]) || !addressBng[1].equals(address[1]) || !addressBng[2].equals(address[2]) || !addressBng[3].equals(address[3])) {
            throw new Exception(isLangEng ? "Permanent address in english and bangla are not matched" : "ইংরেজি এবং বাংলায় বর্তমান ঠিকানা একই হবে");
        }
        employee_recordsDTO.permanentAddressBng = value.trim();

        try {
            employee_recordsDTO.homeDistrict = Integer.parseInt(request.getParameter("homeDistrict").trim());
        } catch (Exception ex) {
            throw new Exception(isLangEng ? "Home district is missing" : "স্থায়ী জেলা পাওয়া যায়নি");
        }
        if (employee_recordsDTO.homeDistrict != employee_recordsDTO.permanentDistrictId) {
            throw new Exception(isLangEng ? "Home district and permanent district should be same district" :
                    "স্থায়ী জেলা এবং স্থায়ী ঠিকানার জেলা একই হবে");
        }

        value = request.getParameter("birthDistrict");
        if (value != null && !value.equalsIgnoreCase("")) {
            try {
                employee_recordsDTO.birthDistrict = Long.parseLong(value);
            } catch (Exception ex) {
                throw new Exception(isLangEng ? "Please select correct birth district" : "অনুগ্রহ করে সঠিক জন্মস্থান বাছাই করুন");
            }
        } else {
            employee_recordsDTO.birthDistrict = 0;
        }

        if(request.getParameter("presentAddressSameAsPermanent")!=null){
            if(request.getParameter("presentAddressSameAsPermanent").equals("true")){
                employee_recordsDTO.sameAsPermanent = true;
                employee_recordsDTO.presentDivisionId = employee_recordsDTO.permanentDivisionId;
                employee_recordsDTO.presentDistrictId = employee_recordsDTO.permanentDistrictId;
                employee_recordsDTO.presentAddress = employee_recordsDTO.permanentAddress;
                employee_recordsDTO.presentAddressBng = employee_recordsDTO.permanentAddressBng;
                return;
            }
        }
        employee_recordsDTO.sameAsPermanent = false;

        value = request.getParameter("presentAddressEng") == null ? null : Jsoup.clean(request.getParameter("presentAddressEng"), Whitelist.simpleText());
        if (value == null) {
            throw new Exception(isLangEng ? "Please enter present address in english" : "অনুগ্রহ করে বর্তমান ঠিকানা ইংরেজীতে দিন");
        }
        address = value.split("@@");
        if (address.length < 5) {
            throw new Exception(isLangEng ? "Please enter present address in english correctly" : "অনুগ্রহ করে সঠিকভাবে বর্তমান ঠিকানা ইংরেজীতে দিন");
        }
        employee_recordsDTO.presentDivisionId = Long.parseLong(address[0]);
        geoLocationDTO = GeoLocationRepository.getInstance().getById((int) employee_recordsDTO.presentDivisionId);
        if (geoLocationDTO == null) {
            throw new Exception(isLangEng ? "Please select correct present division in english" : "অনুগ্রহ করে সঠিকভাবে বর্তমান বিভাগ ইংরেজীতে বাছাই করুন");
        }
        employee_recordsDTO.presentDistrictId = Long.parseLong(address[1]);
        geoLocationDTO1 = GeoLocationRepository.getInstance().getById((int) employee_recordsDTO.presentDistrictId);
        if (geoLocationDTO1 == null || geoLocationDTO1.parentID != geoLocationDTO.id) {
            throw new Exception(isLangEng ? "Please select correct present district in english" : "অনুগ্রহ করে সঠিকভাবে বর্তমান জেলা ইংরেজীতে বাছাই করুন");
        }
        geoLocationDTO = geoLocationDTO1;
        geoLocationDTO1 = GeoLocationRepository.getInstance().getById(Integer.parseInt(address[2]));
        if (geoLocationDTO1 == null || geoLocationDTO1.parentID != geoLocationDTO.id) {
            throw new Exception(isLangEng ? "Please select correct present upazila in english" : "অনুগ্রহ করে সঠিকভাবে বর্তমান উপজেলা ইংরেজীতে বাছাই করুন");
        }
        geoLocationDTO = geoLocationDTO1;
        x = Integer.parseInt(address[3]);
        if (x != 0) {
            geoLocationDTO1 = GeoLocationRepository.getInstance().getById(x);
            if (geoLocationDTO1 == null || geoLocationDTO1.parentID != geoLocationDTO.id) {
                throw new Exception(isLangEng ? "Please select correct present paurashava in english" : "অনুগ্রহ করে সঠিকভাবে বর্তমান পৌরসভা ইংরেজীতে বাছাই করুন");
            }
        }
        employee_recordsDTO.presentAddress = value.trim();

        value = request.getParameter("presentAddressBng") == null ? null : Jsoup.clean(request.getParameter("presentAddressBng"), Whitelist.simpleText());
        if (value == null) {
            throw new Exception(isLangEng ? "Please enter present address in bangla" : "অনুগ্রহ করে বর্তমান ঠিকানা বাংলা দিন");
        }
        addressBng = value.split("@@");
        if (addressBng.length < 5) {
            throw new Exception(isLangEng ? "Please enter present address in bangla correctly" : "অনুগ্রহ করে সঠিকভাবে বর্তমান ঠিকানা বাংলা দিন");
        }
        if (!addressBng[0].equals(address[0]) || !addressBng[1].equals(address[1]) || !addressBng[2].equals(address[2]) || !addressBng[3].equals(address[3])) {
            throw new Exception(isLangEng ? "Present address in english and bangla are not matched" : "ইংরেজি এবং বাংলায় বর্তমান ঠিকানা একই হবে");
        }
        employee_recordsDTO.presentAddressBng = value.trim();
    }

    private void setConfidentialData(HttpServletRequest request, Employee_recordsDTO employee_recordsDTO) {
        String value;

        value = request.getParameter("bcn");
        value = value != null ? Jsoup.clean(value, Whitelist.simpleText()) : "";
        employee_recordsDTO.bcn = value;

        value = request.getParameter("passportNo");
        employee_recordsDTO.passportNo = value != null ? Jsoup.clean(value, Whitelist.simpleText()) : "";

        value = request.getParameter("passportIssueDate");
        if (value != null && !value.equalsIgnoreCase("") && !value.equalsIgnoreCase("NaN")) {
            try {
                employee_recordsDTO.passportIssueDate = simpleDateFormat.parse(value).getTime();
            } catch (Exception ex) {
                logger.error(ex.getMessage());
                employee_recordsDTO.passportIssueDate = SessionConstants.MIN_DATE;
            }
        } else {
            employee_recordsDTO.passportIssueDate = SessionConstants.MIN_DATE;
        }

        value = request.getParameter("passportExpiryDate");
        if (value != null && !value.equalsIgnoreCase("") && !value.equalsIgnoreCase("NaN")) {
            try {
                employee_recordsDTO.passportExpiryDate = simpleDateFormat.parse(value).getTime();
            } catch (Exception ex) {
                logger.error(ex.getMessage());
                employee_recordsDTO.passportExpiryDate = SessionConstants.MIN_DATE;
            }
        } else {
            employee_recordsDTO.passportExpiryDate = SessionConstants.MIN_DATE;
        }

    }

    private void setMiscellaneousData(HttpServletRequest request, Employee_recordsDTO employee_recordsDTO, boolean isLangEng) throws Exception {
        String value;
        CategoryLanguageModel model;

        value = request.getParameter("bloodGroup");
        if (value != null && !value.trim().equals("") && !value.trim().equals("0")) {
            try {
                employee_recordsDTO.bloodGroup = Integer.parseInt(value.trim());
                model = CatRepository.getInstance().getCategoryLanguageModel("blood_group", employee_recordsDTO.bloodGroup);
                if (model == null) {
                    throw new Exception(isLangEng ? "Please select correct blood group" : "অনুগ্রহ করে সঠিকভাবে রক্তের গ্রুপ বাছাই করুন");
                }
            } catch (Exception ex) {
                logger.error("Invalid blood input : " + value);
                throw new Exception(isLangEng ? "Please select correct blood group" : "অনুগ্রহ করে সঠিকভাবে রক্তের গ্রুপ বাছাই করুন");
            }
        } else {
            employee_recordsDTO.bloodGroup = 0;
        }


        value = request.getParameter("identificationMark");
        employee_recordsDTO.identificationMark = value != null ? Jsoup.clean(value, Whitelist.simpleText()) : "";


        value = request.getParameter("maritalStatus");
        if (value != null && !value.equalsIgnoreCase("") && !value.trim().equals("0")) {
            try {
                employee_recordsDTO.maritalStatus = Integer.parseInt(value.trim());
            } catch (Exception ex) {
                throw new Exception(isLangEng ? "Please select correct marital status" : "অনুগ্রহ করে সঠিক বৈবাহিক অবস্থা বাছাই করুন");
            }
            model = CatRepository.getInstance().getCategoryLanguageModel("marital_status", employee_recordsDTO.maritalStatus);
            if (model == null) {
                throw new Exception(isLangEng ? "Please select correct marital status" : "অনুগ্রহ করে সঠিক বৈবাহিক অবস্থা বাছাই করুন");
            }
        } else {
            employee_recordsDTO.maritalStatus = 0;
        }

        value = request.getParameter("marriageDate");
        if (value != null && !value.equalsIgnoreCase("") && !value.equalsIgnoreCase("NaN")) {
            try {
                employee_recordsDTO.marriageDate = simpleDateFormat.parse(value).getTime();
            } catch (Exception ex) {
                logger.error(ex.getMessage());
                employee_recordsDTO.marriageDate = SessionConstants.MIN_DATE;
            }
        } else {
            employee_recordsDTO.marriageDate = SessionConstants.MIN_DATE;
        }


        value = request.getParameter("height");
        if (value != null && !value.equalsIgnoreCase("")) {
            try {
                employee_recordsDTO.height = Double.parseDouble(value.trim());
            } catch (Exception ex) {
                logger.error(ex.getMessage());
                employee_recordsDTO.height = 0.0;
            }
        } else {
            employee_recordsDTO.height = 0.0;
        }
        employee_recordsDTO.freedomFighterInfo = "";
        value = request.getParameter("freedomFighter");
        if (value != null && value.trim().length() > 0) {
            value = value.trim();
            Set<CategoryLanguageModel> freedomFighterInfos = Stream.of(value.split(","))
                    .filter(option -> option.length() > 0)
                    .map(Utils::convertToInteger)
                    .filter(Objects::nonNull)
                    .map(val -> CatRepository.getInstance().getCategoryLanguageModel("freedom_fighter_info", val))
                    .collect(Collectors.toSet());
            if (freedomFighterInfos.contains(null)) {
                throw new Exception(isLangEng ? "Please select correct freedom fighter info" : "অনুগ্রহ করে সঠিক মুক্তিযুদ্ধা তথ্য বাছাই করুন");
            }
            if (freedomFighterInfos.size() > 0) {
                employee_recordsDTO.freedomFighterInfo = freedomFighterInfos.stream().map(e -> String.valueOf(e.categoryValue)).collect(Collectors.joining(","));
            }
        }

        value = request.getParameter("jobQuota");
        if (value != null && !value.trim().equals("") && !value.trim().equals("0")) {
            try {
                employee_recordsDTO.jobQuota = Integer.parseInt(value.trim());
                model = CatRepository.getInstance().getCategoryLanguageModel("job_quota", employee_recordsDTO.jobQuota);
                if (model == null) {
                    throw new Exception(isLangEng ? "Please select correct job quota" : "অনুগ্রহ করে সঠিকভাবে  চাকরির কোটা বাছাই করুন");
                }
            } catch (Exception ex) {
                logger.error("Invalid quota input : " + value);
                throw new Exception(isLangEng ? "Please select correct job quota" : "অনুগ্রহ করে সঠিকভাবে  চাকরির কোটা বাছাই করুন");
            }
        } else {
            employee_recordsDTO.jobQuota = 0;
        }

        value = request.getParameter("quota_file");

        if (value != null && !value.equalsIgnoreCase("")) {
            value = Jsoup.clean(value, Whitelist.simpleText());
        }
        if (value != null && !value.equalsIgnoreCase("")) {

            System.out.println("quota_file = " + value);

            employee_recordsDTO.quotaFile = Long.parseLong(value);
            String fileDropzoneFilesToDelete = request.getParameter("quota_fileFilesToDelete");
            String[] deleteArray = fileDropzoneFilesToDelete.split(",");
            for (int i = 0; i < deleteArray.length; i++) {
                System.out.println("going to delete " + deleteArray[i]);
                if (i > 0) {
                    new FilesDAO().delete(Long.parseLong(deleteArray[i]));
                }
            }

        }
    }

    private void setNIDData(HttpServletRequest request, Employee_recordsDTO employee_recordsDTO) throws Exception {
        String value;
        value = request.getParameter("nid");
        value = value != null ? Jsoup.clean(value, Whitelist.simpleText()) : "";
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        if (value == null || value.trim().length() == 0) {
            throw new Exception(isLangEng ? "Please enter nid number" : "ইংরেজীতে নাম পাওয়া যায়নি");
        }
        employee_recordsDTO.nid = value;
        System.out.println(request.getParameter("nid_front_photo"));
        byte[] nidFrontPhotoArray = convertByteArray(request.getPart("nid_front_photo"), "nid");
        if (nidFrontPhotoArray != null) {
            employee_recordsDTO.nidFrontPhoto = nidFrontPhotoArray;
        }
        byte[] nidBackPhotoArray = convertByteArray(request.getPart("nid_back_photo"), "nid");
        if (nidBackPhotoArray != null) {
            employee_recordsDTO.nidBackPhoto = nidBackPhotoArray;
        }
    }

    private void setImageData(HttpServletRequest request, Employee_recordsDTO employee_recordsDTO) throws Exception {
        byte[] photoArray = convertByteArray(request.getPart("photo"), "profile");
        if (photoArray != null) {
            employee_recordsDTO.photo = photoArray;
        }

        byte[] signatureArray = convertByteArray(request.getPart("signature"), "signature");
        if (signatureArray != null) {
            employee_recordsDTO.signature = signatureArray;
        }

    }

    private void setCommonData(HttpServletRequest request, Employee_recordsDTO employee_recordsDTO, boolean isLangEng) throws Exception {
        setPersonalData(request, employee_recordsDTO, isLangEng);
        setAddressData(request, employee_recordsDTO, isLangEng);
        setConfidentialData(request, employee_recordsDTO);
        setContactData(request, employee_recordsDTO, isLangEng);
        setMiscellaneousData(request, employee_recordsDTO, isLangEng);
        setNIDData(request, employee_recordsDTO);
        setImageData(request, employee_recordsDTO);
    }


    private void sendMail(String mailId, String message, String subject) {
        if (mailId != null && mailId.trim().length() > 0) {
            SendEmailDTO sendEmailDTO = new SendEmailDTO();
            sendEmailDTO.setTo(new String[]{mailId});
            sendEmailDTO.setText(message);
            sendEmailDTO.setSubject(subject);
            sendEmailDTO.setFrom("edms@pbrlp.gov.bd");
            try {
                new EmailService().sendMail(sendEmailDTO);
            } catch (Exception ex) {
                logger.error("",ex);
            }
        } else {
            logger.debug("<<<<mail id is null or empty, mailId : " + mailId);
        }
    }

    private String iDGenerationOfMP(String prefix) {
        int maxNumber = employeeRecordsDAO.getCountOfMpByPrefix(prefix);
        return prefix.concat(String.valueOf(maxNumber + 1));
    }

    private String generateUserNamePrefixForMP(long parliamentNumberId, long electionConstituencyId) {
        StringBuilder prefixBuilder = new StringBuilder("0");
        Election_detailsDTO electionDetailsDto = Election_detailsRepository.getInstance().getElectionDetailsDTOByID(parliamentNumberId);
        prefixBuilder.append(String.format("%02d", electionDetailsDto.parliamentNumber));
        Election_constituencyDTO electionConstituencyDto = Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(electionConstituencyId);
        prefixBuilder.append(String.format("%04d", electionConstituencyDto.constituencyNumber));
        return prefixBuilder.toString();
    }

    private void deleteEmployee_records(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String[] IDsToDelete = request.getParameterValues("ID");
        try {
            Arrays.stream(IDsToDelete)
                    .map(Long::parseLong)
                    .peek(id -> logger.debug("###DELETING " + id))
                    .forEach(id -> {
                        try {
                            employeeRecordsDAO.deleteEmployee_recordsByID(id);
                        } catch (Exception ex) {
                            logger.debug(ex);
                        }
                    });
        } catch (Exception ex) {
            logger.debug(ex);
            ApiResponse.sendErrorResponse(response, ex.getMessage());
        }
        ApiResponse.sendSuccessResponse(response, "option");
        //         searchEmployee_records(request,response,loginDTO);
        // response.sendRedirect("GenericTreeServlet?actionType=searchEmployeeRecord&success=true");
    }

    private void getEmployee_records(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("in getEmployee_records");
        Employee_recordsDTO employee_recordsDTO;
        try {
            employee_recordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(Long.parseLong(request.getParameter("ID")));
            request.setAttribute("ID", employee_recordsDTO.iD);
            request.setAttribute("employee_recordsDTO", employee_recordsDTO);
            String URL = "";
            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (Exception e) {
            logger.error("",e);
        }
    }

    private void searchEmployee_records(HttpServletRequest request, HttpServletResponse response, LoginDTO loginDTO) throws Exception {
        logger.debug("in  searchEmployee_records 1");
        Employee_recordsDAO employee_recordsDAO = new Employee_recordsDAO(loginDTO);

        String value = request.getParameter("ministry_id");
        if (value != null && !value.equalsIgnoreCase("")) {
            employee_recordsDAO.ministry_id = Integer.parseInt(value);
        }
        value = request.getParameter("layer_id");
        if (value != null && !value.equalsIgnoreCase("")) {
            employee_recordsDAO.layer_id = Integer.parseInt(value);
        }
        value = request.getParameter("origin_id");
        if (value != null && !value.equalsIgnoreCase("")) {
            employee_recordsDAO.origin_id = Integer.parseInt(value);
        }
        value = request.getParameter("office_id");
        if (value != null && !value.equalsIgnoreCase("")) {
            employee_recordsDAO.office_id = Integer.parseInt(value);
        }

        value = request.getParameter("name_en");
        if (value != null && !value.equalsIgnoreCase("")) {
            employee_recordsDAO.name_en = value;
        }
        value = request.getParameter("name_bn");
        if (value != null && !value.equalsIgnoreCase("")) {
            employee_recordsDAO.name_bn = value;
        }
        value = request.getParameter("nid");
        if (value != null && !value.equalsIgnoreCase("")) {
            employee_recordsDAO.nid = StringUtils.convertToEngNumber(value);
        }
        value = request.getParameter("email");
        if (value != null && !value.equalsIgnoreCase("")) {
            employee_recordsDAO.email = value;
        }
        value = request.getParameter("phone0");
        if (value != null && !value.equalsIgnoreCase("")) {
            employee_recordsDAO.phone0 = StringUtils.convertToEngNumber(value);
        }
        value = request.getParameter("user_name");
        if (value != null && !value.equalsIgnoreCase("")) {
            employee_recordsDAO.user_name = StringUtils.convertToEngNumber(value);
        }

        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        logger.debug("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager2 rnManager = new RecordNavigationManager2(
                SessionConstants.NAV_EMPLOYEE_RECORDS,
                request,
                employee_recordsDAO,
                SessionConstants.VIEW_EMPLOYEE_RECORDS,
                SessionConstants.SEARCH_EMPLOYEE_RECORDS,
                "employee_records");
        rnManager.doJob(loginDTO);
        logger.debug("Going to employee_records/employee_recordsSearchForm.jsp");
        request.getRequestDispatcher("employee_records/employee_recordsSearchForm.jsp").forward(request, response);
    }

    private byte[] resizeImage(byte[] image, String fileType, String imageType) throws IOException {

        int imageWidth = DEFAULT_IMAGE_WIDTH;
        int imageHeight = DEFAULT_IMAGE_HEIGHT;

        if (imageType.equalsIgnoreCase("profile")) {
            imageWidth = PROFILE_IMAGE_WIDTH;
            imageHeight = PROFILE_IMAGE_HEIGHT;
        } else if (imageType.equalsIgnoreCase("signature")) {
            imageWidth = SIGNATURE_IMAGE_WIDTH;
            imageHeight = SIGNATURE_IMAGE_HEIGHT;
        } else if (imageType.equalsIgnoreCase("nid")) {
            imageWidth = nid_IMAGE_WIDTH;
            imageHeight = nid_IMAGE_HEIGHT;
        } else if (imageType.equalsIgnoreCase("mp_gazette")) {
            imageWidth = MP_GAZETTE_IMAGE_WIDTH;
            imageHeight = MP_GAZETTE_IMAGE_HEIGHT;
        }

        InputStream inputStream = new ByteArrayInputStream(image);
        BufferedImage bufferedImage = ImageIO.read(inputStream);
        BufferedImage resultBufferedImage = Utils.resize(bufferedImage, imageWidth, imageHeight);
        return Utils.toByteArrayAutoClosable(resultBufferedImage, fileType);
    }

    private byte[] convertPdfToByteArray(Part filePart) throws IOException {
        if (filePart == null) {
            return null;
        }
        String alternateDoc = getFileName(filePart);
        byte[] tempByteDoc;
        if (alternateDoc != null) {
            alternateDoc = Jsoup.clean(alternateDoc, Whitelist.simpleText());
        }
        logger.debug("alternateImage = " + alternateDoc);
        if (alternateDoc != null && !alternateDoc.equalsIgnoreCase("")) {

            if (alternateDoc.toLowerCase().endsWith(".pdf") || alternateDoc.toLowerCase().endsWith(".jpg") || alternateDoc.toLowerCase().endsWith(".png")
                    || alternateDoc.toLowerCase().endsWith(".jpeg")) {

                tempByteDoc = Utils.uploadFileToByteAray(filePart);

                if (alternateDoc.toLowerCase().endsWith(".jpg")) {
                    return resizeImage(tempByteDoc, "jpg", "mp_gazette");
                } else if (alternateDoc.toLowerCase().endsWith(".png")) {
                    return resizeImage(tempByteDoc, "png", "mp_gazette");
                } else if (alternateDoc.toLowerCase().endsWith(".jpeg")) {
                    return resizeImage(tempByteDoc, "jpeg", "mp_gazette");
                } else {
                    return tempByteDoc;
                }
            }
        }
        return null;
    }

    private byte[] convertByteArray(Part photoPart, String imageType) throws IOException {
        if (photoPart == null) {
            return null;
        }
        String alternateImage = getFileName(photoPart);
        byte[] tempByteImage;
        if (alternateImage != null) {
            alternateImage = Jsoup.clean(alternateImage, Whitelist.simpleText());
        }
        logger.debug("alternateImage = " + alternateImage);
        if (alternateImage != null && !alternateImage.equalsIgnoreCase("")) {

            if (alternateImage.toLowerCase().endsWith(".jpg") || alternateImage.toLowerCase().endsWith(".png")
                    || alternateImage.toLowerCase().endsWith(".jpeg")) {

                tempByteImage = Utils.uploadFileToByteAray(photoPart);

                if (alternateImage.toLowerCase().endsWith(".jpg")) {
                    return resizeImage(tempByteImage, "jpg", imageType);
                } else if (alternateImage.toLowerCase().endsWith(".png")) {
                    return resizeImage(tempByteImage, "png", imageType);
                } else {
                    return resizeImage(tempByteImage, "jpeg", imageType);
                }

            }
        }
        return null;
    }

    private void searchEmployee_pension(HttpServletRequest request, HttpServletResponse response, LoginDTO loginDTO) throws Exception {
        String fiscal_year;
        boolean firstReload = true;

        String value = request.getParameter("fiscal_year");
        if (value != null && !value.equalsIgnoreCase("")) {
            fiscal_year = value;
        } else {
            fiscal_year = "-1";
        }

        value = request.getParameter("firstReload");
        if (value != null && !value.equalsIgnoreCase("")) {
            firstReload = Boolean.parseBoolean(value);
        }


        Employee_recordsDAO employee_recordsDAO = new Employee_recordsDAO(loginDTO, Integer.parseInt(fiscal_year));
        RecordNavigationManager2 rnManager = new RecordNavigationManager2(
                SessionConstants.NAV_EMPLOYEE_PENSION,
                request,
                employee_recordsDAO,
                SessionConstants.VIEW_EMPLOYEE_PENSION,
                SessionConstants.SEARCH_EMPLOYEE_PENSION,
                "employee_records");
        rnManager.doJob(loginDTO);

        RequestDispatcher rd;

        if (firstReload) {
            logger.debug("Going to gate_pass/gate_passSearch.jsp");
            rd = request.getRequestDispatcher("employee_pension/employee_pensionSearch.jsp");
        } else {
            rd = request.getRequestDispatcher("employee_pension/employee_pensionSearchForm.jsp");
        }
        rd.forward(request, response);
    }
}