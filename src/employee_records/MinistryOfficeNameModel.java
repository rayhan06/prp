package employee_records;

import common.NameDTO;

public class MinistryOfficeNameModel {
    long id;
    String nameEng;
    String nameBng;

    MinistryOfficeNameModel(NameDTO nameDTO) {
        id = nameDTO.iD;
        nameEng = nameDTO.nameEn;
        nameBng = nameDTO.nameBn;
    }
}
