package employee_records;

import common.ApiResponse;
import employee_language_proficiency.Employee_language_proficiencyDAO;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

public class GetLanguageInfo implements EmployeeRecordsApiPerformer {
    public final Logger logger = Logger.getLogger(GetMpListByParliamentNumberPerformer.class);

    @Override
    public ApiResponse perform(HttpServletRequest request) {

        String userName = request.getParameter("employee_record_id");
        long employeeRecordId;

        try {
            employeeRecordId = Employee_recordsRepository.getInstance().getEmployeeRecordIdByUsername(userName);
            if (employeeRecordId==0)
                return ApiResponse.makeResponseToResourceNotFound("No Record found with employee_record_id: "+userName+"!");
        } catch (Exception ex) {
            return ApiResponse.makeResponseToResourceNotFound("No Record found with employee_record_id: "+userName+"!");
        }

        try {

            List<EmployeeLanguageModel> employeeLanguageModels = Employee_language_proficiencyDAO.getInstance()
                    .getByEmployeeId(employeeRecordId)
                    .stream()
                    .map(EmployeeLanguageModel::new)
                    .collect(Collectors.toList());
            return employeeLanguageModels.size() == 0 ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                    ApiResponse.makeSuccessResponse(employeeLanguageModels, "Successfully found the data.");
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            return ApiResponse.makeResponseToBadRequest("Kindly check the employee_record_id parameter provided!");
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
        }
    }
}