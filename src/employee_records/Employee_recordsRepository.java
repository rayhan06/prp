package employee_records;

import employee_assign.EmployeeAssignDTO;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_service_history.Employee_service_historyDAO;
import employee_service_history.Employee_service_historyDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_unit_organograms.SameDesignationGroup;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;

import appointment.KeyCountDTO;
import appointment.YearMonthCount;
import common.ConnectionAndStatementUtil;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.IntegerWrapper;
import util.TimeConverter;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

import java.sql.ResultSet;
import java.sql.SQLException;

@SuppressWarnings({"Duplicates", "unused"})
public class Employee_recordsRepository implements Repository {
    private final Employee_recordsDAO employeeRecordsDAO;

    private static final Logger logger = Logger.getLogger(Employee_recordsRepository.class);
    private final Map<Long, Employee_recordsDTO> mapById;
    private List<Employee_recordsDTO> employeeRecordsDTOList;
    private static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private static final ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
    private static final ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();

    private Employee_recordsRepository() {
        employeeRecordsDAO = new Employee_recordsDAO();
        mapById = new HashMap<>();
        employeeRecordsDTOList = new ArrayList<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Employee_recordsRepository INSTANCE = new Employee_recordsRepository();
    }

    public static Employee_recordsRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        long lastModifyTime = RepositoryManager.lastModifyTime;
        logger.debug("Employee_recordsRepository reload start, reloadAll = " + reloadAll);
        List<Employee_recordsDTO> employee_recordsDTOs = employeeRecordsDAO.getAllEmployee_records(reloadAll, lastModifyTime);
        logger.debug("Data loaded from DB, loaded size : " + employee_recordsDTOs.size());
        if (employee_recordsDTOs.size() > 0) {
            if (reloadAll) {
                employeeRecordsDTOList = employee_recordsDTOs.stream()
                        .filter(e -> !e.isDeleted)
                        .peek(e -> mapById.put(e.iD, e))
                        .collect(toList());
            } else {
                updateCache(employee_recordsDTOs);
            }
        }
        /*if(reloadAll)
        {
        	try {
				employeeRecordsDAO.massUpdate();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }*/
        logger.debug("Employee_recordsRepository reload end, reloadAll = " + reloadAll);
    }

    private void updateCache(List<Employee_recordsDTO> dtos) {
        if (dtos == null || dtos.isEmpty()) {
            return;
        }
        try {
            writeLock.lock();
            for (Employee_recordsDTO dto : dtos) {
                Employee_recordsDTO oldDTO = mapById.get(dto.iD);
                if (oldDTO == null || dto.lastModificationTime > oldDTO.lastModificationTime) {
                    if (oldDTO != null) {
                        employeeRecordsDTOList.remove(oldDTO);
                        mapById.remove(dto.iD);
                    }
                    if (!dto.isDeleted) {
                        employeeRecordsDTOList.add(dto);
                        mapById.put(dto.iD, dto);
                    }
                }
            }
        } finally {
            writeLock.unlock();
        }
    }

    public List<Employee_recordsDTO> getEmployee_recordsList() {
        List<Employee_recordsDTO> recordsDTOList;
        try {
            readLock.lock();
            recordsDTOList = new ArrayList<>(employeeRecordsDTOList);
        } finally {
            readLock.unlock();
        }
        return recordsDTOList;
    }
    
    public Employee_recordsDTO getByIdRepoOnly(long id) {
        if (id == 0) {
            return null;
        }
        return mapById.getOrDefault(id, null);

    }

    public Employee_recordsDTO getById(long id) {
        if (id == 0) {
            return null;
        }
        Employee_recordsDTO employeeRecordsDTO;
        try {
            readLock.lock();
            employeeRecordsDTO = mapById.get(id);

            if (employeeRecordsDTO == null) {

                employeeRecordsDTO = employeeRecordsDAO.getEmployee_recordsDTOByID(id);
                if (employeeRecordsDTO != null) {
                    ExecutorService executorService = Executors.newSingleThreadExecutor((r) -> {
                        Thread thread = new Thread(r);
                        thread.setDaemon(true);
                        return thread;
                    });

                    Employee_recordsDTO finalEmployeeRecordsDTO = employeeRecordsDTO;
                    executorService.execute(
                            () -> updateCache(Collections.singletonList(finalEmployeeRecordsDTO))
                    );
                    executorService.shutdown();
                }
            }
        } finally {
            readLock.unlock();
        }
        return employeeRecordsDTO;

    }

    public String getEmployeeName(long id, String language) {
        return getEmployeeName(id,language.equalsIgnoreCase("English"));
    }

    public String getEmployeeName(long id, boolean isLangEng) {
        Employee_recordsDTO dto = getById(id);
        return dto == null ? "" : isLangEng ? dto.nameEng : dto.nameBng;
    }

    @Override
    public String getTableName() {
        return "employee_records";
    }

    public Map<String, Object> getCountByType(String language, List<Employee_recordsDTO> employeeRecordsDTOList, long officeUnitId) {
        return getCountByType("English".equalsIgnoreCase(language), employeeRecordsDTOList, officeUnitId);
    }

    public Map<String, Object> getCountByType(boolean isLangEng, List<Employee_recordsDTO> employeeRecordsDTOList, long officeUnitId) {
        Map<Integer, CategoryLanguageModel> typeModelMap = CatRepository.getInstance()
                .getCategoryLanguageModelList("employment")
                .stream()
                .collect(Collectors.toMap(e -> e.categoryValue, e -> e));
        IntegerWrapper ai = new IntegerWrapper(0);
        return employeeRecordsDTOList
                .stream()
                .filter(dto -> typeModelMap.get(dto.employeeClass) != null)
                .collect(groupingBy(dto -> dto.employmentType, collectingAndThen(counting(), Long::intValue)))
                .entrySet()
                .stream()
                .collect(toMap(
                        e -> String.valueOf(ai.incrementAndGet()),
                        e -> getTypeCountAndLink(isLangEng ? typeModelMap.get(e.getKey()).englishText : typeModelMap.get(e.getKey()).banglaText,
                                e.getValue(), e.getKey(), officeUnitId)
                ));
    }

    private Map<String, Object> getTypeCountAndLink(String title, Integer count, Integer employmentCat, long officeUnitId) {
        Map<String, Object> countLinkMap = new HashMap<>();
        countLinkMap.put("title", title);
        countLinkMap.put("count", count);
        countLinkMap.put("link", "Employee_office_report_Servlet?actionType=reportPage&employmentCat=" + employmentCat + "&officeUnitId=" + officeUnitId);
        return countLinkMap;
    }

    public Map<String, Object> getCountByClass(String language, List<Employee_recordsDTO> employeeRecordsDTOList, long officeUnitId) {
        return getCountByClass("English".equalsIgnoreCase(language), employeeRecordsDTOList, officeUnitId);
    }

    public Map<String, Object> getCountByClass(boolean isLangEng, List<Employee_recordsDTO> employeeRecordsDTOList, long officeUnitId) {
        Map<Integer, CategoryLanguageModel> classModelMap = CatRepository.getInstance()
                .getCategoryLanguageModelList("employee_class")
                .stream()
                .collect(Collectors.toMap(e -> e.categoryValue, e -> e));
        IntegerWrapper ai = new IntegerWrapper(0);
        return employeeRecordsDTOList
                .stream()
                .filter(dto -> classModelMap.get(dto.employeeClass) != null)
                .collect(groupingBy(dto -> dto.employeeClass, collectingAndThen(counting(), Long::intValue)))
                .entrySet()
                .stream()
                .collect(toMap(
                        e -> String.valueOf(ai.incrementAndGet()),
                        e -> getClassCountAndLink(isLangEng ? classModelMap.get(e.getKey()).englishText : classModelMap.get(e.getKey()).banglaText,
                                e.getValue(), e.getKey(), officeUnitId)
                ));
    }

    private Map<String, Object> getClassCountAndLink(String title, Integer count, Integer employeeClass, long officeUnitId) {
        Map<String, Object> countLinkMap = new HashMap<>();
        countLinkMap.put("title", title);
        countLinkMap.put("count", count);
        countLinkMap.put("link", "Employee_office_report_Servlet?actionType=reportPage&employeeClass=" + employeeClass + "&officeUnitId=" + officeUnitId);
        return countLinkMap;
    }

    public List<Employee_recordsDTO> getByDesignationId(List<Long> designationId) {
        Set<Long> empIds = EmployeeOfficeRepository.getInstance().getEmpIdByOrganogramId(new HashSet<>(designationId));
        return empIds
                .parallelStream()
                .map(this::getById)
                .collect(toList());
    }

    public Employee_recordsDTO getDTOByOrganogramId(long organogramId) {
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(organogramId);
        if (employeeOfficeDTO == null) return null;
        return getById(employeeOfficeDTO.employeeRecordId);
    }

    public List<Employee_recordsDTO> getByIds(Collection<Long> ids) {
        return ids.stream()
                .map(this::getById)
                .filter(Objects::nonNull)
                .collect(toList());
    }

    public List<EmployeeAssignDTO> getEmployeeAssignDTOByOfficeUnitId(long officeUnitId) {
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitId);
        List<EmployeeOfficeDTO> employeeOfficeDTOList = EmployeeOfficeRepository.getInstance().getEmployeeOfficeDTOListByOfficeUnitId(officeUnitId);
        return employeeOfficeDTOList.stream()
                .map(dto -> buildEmployeeAssignDTO(dto, officeUnitsDTO))
                .collect(toList());
    }

    public List<EmployeeAssignDTO> getActiveEmployeeAssignDTOByOfficeUnitId(long officeUnitId) {
        logger.debug("getActiveEmployeeAssignDTOByOfficeUnitId = " + officeUnitId);
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitId);
        List<EmployeeOfficeDTO> employeeOfficeDTOList = EmployeeOfficeRepository.getInstance().getEmployeeOfficeDTOListByOfficeUnitId(officeUnitId);
        List<EmployeeAssignDTO> employeeAssignDTOs = new ArrayList<EmployeeAssignDTO>();
        for (EmployeeOfficeDTO employeeOfficeDTO : employeeOfficeDTOList) {
            UserDTO userDTO = UserRepository.getInstance().getUserDtoByEmployeeRecordId(employeeOfficeDTO.employeeRecordId);
            if (userDTO != null && userDTO.active) {
                employeeAssignDTOs.add(buildEmployeeAssignDTO(employeeOfficeDTO, officeUnitsDTO));
            }
        }
        return employeeAssignDTOs;
    }


    public List<EmployeeAssignDTO> getDriverEmployeeAssignDTO() {
        Office_unitsDTO officeUnitsDTOTransport = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(45);
        List<EmployeeOfficeDTO> employeeOfficeDTOListTransport = EmployeeOfficeRepository.getInstance().getEmployeeOfficeDTOListByOfficeUnitId(45);

        Office_unitsDTO officeUnitsDTOReceiveAndIssue = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(46);
        List<EmployeeOfficeDTO> employeeOfficeDTOListReceiveAndIssue = EmployeeOfficeRepository.getInstance().getEmployeeOfficeDTOListByOfficeUnitId(46);

        return Stream.concat(
                employeeOfficeDTOListTransport.stream()
                        .map(dto -> buildEmployeeAssignDTO(dto, officeUnitsDTOTransport)),
                employeeOfficeDTOListReceiveAndIssue.stream()
                        .filter(dto -> dto.designationEn.contains("Despatch Rider"))
                        .map(dto -> buildEmployeeAssignDTO(dto, officeUnitsDTOReceiveAndIssue))
        )
                .collect(toList());

    }


    private EmployeeAssignDTO buildEmployeeAssignDTO(EmployeeOfficeDTO employeeOfficeDTO, Office_unitsDTO officeUnitsDTO) {
        EmployeeAssignDTO dto = new EmployeeAssignDTO();
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        Employee_recordsDTO employeeRecordsDTO = getById(employeeOfficeDTO.employeeRecordId);
        dto.employee_office_id = employeeOfficeDTO.iD;
        dto.employee_id = employeeOfficeDTO.employeeRecordId;
        dto.employee_name_eng = employeeRecordsDTO != null ? employeeRecordsDTO.nameEng : "";
        dto.employee_name_bng = employeeRecordsDTO != null ? employeeRecordsDTO.nameBng:"";
        dto.officeId = officeUnitsDTO.officeId;
        dto.office_unit_id = officeUnitsDTO.iD;
        dto.unit_name_eng = officeUnitsDTO.unitNameEng;
        dto.unit_name_bng = officeUnitsDTO.unitNameBng;
        dto.username = employeeRecordsDTO != null ? employeeRecordsDTO.employeeNumber:"";
        dto.organogram_id = officeUnitOrganograms.id;
        dto.organogram_name_eng = officeUnitOrganograms.designation_eng;
        dto.organogram_name_bng = officeUnitOrganograms.designation_bng;
        dto.designation_id = officeUnitOrganograms.id;
        dto.designation_level = employeeOfficeDTO.designationLevel;
        dto.designation_sequence = employeeOfficeDTO.designationSequence;
        dto.joining_date = employeeOfficeDTO.joiningDate;
        dto.last_office_date = employeeOfficeDTO.lastOfficeDate;
        return dto;
    }

    public int getActiveCountByEmployeeNumberPrefix(String prefix) {
        return employeeRecordsDTOList.parallelStream()
                .filter(dto -> dto.employeeNumber != null && dto.employeeNumber.startsWith(prefix) && dto.status)
                .collect(collectingAndThen(counting(), Long::intValue));
    }

    public Set<SameDesignationGroup> getSameDesignationGroups(int employmentCat) {
        return Employee_recordsRepository.getInstance().getEmployee_recordsList()
                .stream()
                .filter(recordsDTO -> recordsDTO.employmentType == employmentCat)
                .map(recordsDTO -> EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(recordsDTO.iD))
                .filter(Objects::nonNull)
                .map(officeDTO -> new SameDesignationGroup(officeDTO.officeUnitOrganogramId))
                .collect(toSet());
    }

    private Set<SameDesignationGroup> getSameDesignationGroupsAll() {
        return OfficeUnitOrganogramsRepository.getInstance().getOffice_unit_organogramsList()
                .stream()
                .filter(Objects::nonNull)
                .map(SameDesignationGroup::new)
                .collect(toSet());
    }

    public String buildDesignationOption(int employmentCat, String selectedOrganogramKey, String language) {
        String selectedId = null;
        List<OptionDTO> optionDTOs = new ArrayList<>();
        for (SameDesignationGroup designationGroup : getSameDesignationGroups(employmentCat)) {
            // this search is necessary because I cant guarantee that the same organogram id is selected every time
            if (designationGroup.organogramKey.equals(selectedOrganogramKey)) {
                selectedId = String.valueOf(designationGroup.organogramId);
            }
            optionDTOs.add(designationGroup.getOptionDTO());
        }
        return Utils.buildOptions(optionDTOs, language, selectedId);
    }

    public String buildOrganogramKeyOption(int employmentCat, String selectedOrganogramKey, String language) {
        String selectedId = null;
        List<OptionDTO> optionDTOs = new ArrayList<>();
        for (SameDesignationGroup designationGroup : getSameDesignationGroups(employmentCat)) {
            // this search is necessary because I cant guarantee that the same organogram id is selected every time
            if (designationGroup.organogramKey.equals(selectedOrganogramKey)) {
                selectedId = String.valueOf(designationGroup.organogramId);
            }
            optionDTOs.add(designationGroup.getOptionDTOofOrganogramKey());
        }
        return Utils.buildOptions(optionDTOs, language, selectedId);
    }

    public String buildOrganogramKeyOptionAll(String selectedOrganograms, String language) {
        List<OptionDTO> optionDTOs = new ArrayList<>();
        for (SameDesignationGroup designationGroup : getSameDesignationGroupsAll()) {
            if (designationGroup.organogramKey != null && designationGroup.organogramKey.length() > 0)
                optionDTOs.add(designationGroup.getOptionDTOofOrganogramKeyAll());
        }
        return Utils.buildOptionsMultipleSelection(optionDTOs, language, selectedOrganograms);
    }

    public String buildAllDesignationOption(String language, String selectedOrganogramKey) {
        String selectedId = null;
        List<OptionDTO> optionDTOs = new ArrayList<>();
        for (SameDesignationGroup designationGroup : getSameDesignationGroupsAll()) {
            if (designationGroup.organogramKey != null && designationGroup.organogramKey.length() > 0) {
                // this search is necessary because I cant guarantee that the same organogram id is selected every time
                if (designationGroup.organogramKey.equals(selectedOrganogramKey)) {
                    selectedId = String.valueOf(designationGroup.organogramId);
                }
                optionDTOs.add(designationGroup.getOptionDTOofOrganogramKey());
            }
        }
        return Utils.buildOptions(optionDTOs, language, selectedId);
    }

    public String getByUserName(String userName, String language) {
        if (userName == null) {
            return "";
        }
        return employeeRecordsDTOList.parallelStream()
                .filter(e -> e.employeeNumber != null && e.employeeNumber.equals(userName))
                .findAny()
                .map(e -> "English".equalsIgnoreCase(language) ? e.nameEng : e.nameBng)
                .orElse("");
    }
    
    public long getIdByUserName(String userName) {
        if (userName == null) {
            return -1L;
        }
        return employeeRecordsDTOList.parallelStream()
                .filter(e -> e.employeeNumber != null && e.employeeNumber.equals(userName))
                .findAny()
                .map(e -> e.iD)
                .orElse(-1L);
    }

    public String getPhoneByUserName(String userName) {
        if (userName == null) {
            return "";
        }
        return employeeRecordsDTOList.parallelStream()
                .filter(e -> e.employeeNumber != null && e.employeeNumber.equals(userName))
                .findAny()
                .map(e -> e.personalMobile)
                .orElse("");

    }

    public String getUserNameByPhone(String phone) {
        if (phone == null || phone.equalsIgnoreCase("")) {
            return "";
        }
        return employeeRecordsDTOList.parallelStream()
                .filter(e -> e.personalMobile != null && e.personalMobile.equals(phone))
                .findAny()
                .map(e -> e.employeeNumber)
                .orElse("");

    }

    public Employee_recordsDTO getByUserNameOrPhone(String userName, String phone) {
        return employeeRecordsDTOList.parallelStream()
                .filter(e -> phone.equals("") || ((e.personalMobile != null && e.personalMobile.equals(phone)) || (e.alternativeMobile != null && e.alternativeMobile.equals(phone))))
                .filter(e -> userName.equals("") || (e.employeeNumber != null && e.employeeNumber.equals(userName)))
                .findAny().orElse(null);
    }

    public long getEmployeeRecordIdByUsername(String username) {
        return employeeRecordsDTOList.parallelStream()
                .filter(e -> e.employeeNumber.equals(username))
                .findAny()
                .map(e -> e.iD)
                .orElse(0L);
    }

    public int getGradeByEmployeeRecordId(long employeeRecordId) {
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employeeRecordId);
        OfficeUnitOrganograms officeUnitOrganograms = employeeOfficeDTO == null ? null : OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        return officeUnitOrganograms != null ? officeUnitOrganograms.job_grade_type_cat : 0;
    }

    public int getGovtOfficeDaysByEmployeeRecordId(long employeeRecordId) {
        List<Employee_service_historyDTO> employee_service_historyDTOList = Employee_service_historyDAO.getInstance().getDTOSgovJobbyEmployeeId(employeeRecordId);
        int days = 0;
        Employee_recordsDTO employeeRecordsDTO = getById(employeeRecordId);
        LocalDate start_date, end_date;
        if (employeeRecordsDTO != null && employeeRecordsDTO.joiningDate != SessionConstants.MIN_DATE) {
            start_date = Instant.ofEpochMilli(employeeRecordsDTO.joiningDate)
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();

            end_date = Instant.ofEpochMilli(System.currentTimeMillis())
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();
            days += ChronoUnit.DAYS.between(start_date, end_date);

        }
        for (Employee_service_historyDTO employee_service_historyDTO : employee_service_historyDTOList) {
            if (employee_service_historyDTO.isGovtJob && !employee_service_historyDTO.isCurrentlyWorking) {

                start_date = Instant.ofEpochMilli(employee_service_historyDTO.servingFrom)
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate();

                end_date = Instant.ofEpochMilli(employee_service_historyDTO.servingTo)
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate();
                days += ChronoUnit.DAYS.between(start_date, end_date);

            }
        }
        return days;
    }

    public void updateCache(Employee_recordsDTO dto) {
        if (dto != null) {
            try {
                writeLock.lock();
                Employee_recordsDTO oldDTO = mapById.get(dto.iD);
                if (oldDTO != null) {
                    employeeRecordsDTOList.remove(oldDTO);
                }
                if (dto.lastModificationTime > System.currentTimeMillis()) {
                    dto.lastModificationTime = System.currentTimeMillis();
                }
                mapById.put(dto.iD, dto);
                employeeRecordsDTOList.add(dto);
            } finally {
                writeLock.unlock();
            }
        }
    }

    public void updateCacheByIds(List<Long> idList) {
        if (idList == null || idList.isEmpty()) {
            return;
        }
        List<Employee_recordsDTO> dtoList = employeeRecordsDAO.getDTOs(idList);
        updateCache(dtoList);
    }

}