package employee_records;

import board.BoardRepository;
import education_level.DegreeExamRepository;
import education_level.Education_levelRepository;
import employee_education_info.Employee_education_infoDTO;
import pb.CatRepository;

public class EducationInfoModel {
    String institutionName;
    String educationLevel;
    String degreeExam;
    String major;
    String gradePoint;
    String cgpa;
    String passingYear;
    String board;
    String durationYears;

    public EducationInfoModel(Employee_education_infoDTO dto) {
        this.institutionName = dto.institutionName;
        this.educationLevel = Education_levelRepository.getInstance().getText("English",dto.educationLevelType);
        this.degreeExam = DegreeExamRepository.getInstance().getText("English",dto.degreeExamType,dto.educationLevelType);
        this.major = dto.major;
        this.gradePoint = CatRepository.getInstance().getText("English", "grade_point", dto.gradePointCat);
        this.cgpa = String.valueOf(dto.cgpaNumber);
        this.passingYear = String.valueOf(dto.yearOfPassingNumber);
        this.board = BoardRepository.getInstance().getText("English",dto.boardType);
        this.durationYears = String.valueOf(dto.durationYears);
    }
}
