package employee_records;

import pb.CatRepository;
import training_calendar_details.Training_calendar_detailsDTO;
import training_calender.Training_calenderDAO;
import training_calender.Training_calenderDTO;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EmployeeTrainingInfo {
    public String trainingNameEng;
    public String trainingNameBng;
    public String trainingType;
    public String instituteName;
    public String startDate;
    public String endDate;
    public Integer arrangedBy;
    public String venue;
    public String trainingMode;
    public int maxNumberOfParticipant;
    public String isEnrollmentApplicable;
    public String lastEnrollmentDate;
    public String prerequisite;
    public String objective;


    public EmployeeTrainingInfo(Training_calendar_detailsDTO dto) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Training_calenderDTO training_calenderDTO = Training_calenderDAO.getInstance().getDTOFromID(dto.trainingCalendarId);
        try {
            this.trainingNameEng = training_calenderDTO.nameEn;
            this.trainingNameBng = training_calenderDTO.nameBn;
            this.trainingType = CatRepository.getInstance().getText("English", "training_type", training_calenderDTO.trainingTypeCat);
            this.instituteName = training_calenderDTO.instituteName;
            this.startDate = dateFormat.format(new Date(training_calenderDTO.startDate));
            this.endDate = dateFormat.format(new Date(training_calenderDTO.endDate));
            this.arrangedBy = training_calenderDTO.arrangedBy;
            this.venue = training_calenderDTO.venue;
            this.trainingMode = CatRepository.getInstance().getText("English", "training_mode", training_calenderDTO.trainingModeCat);
            this.maxNumberOfParticipant = training_calenderDTO.maxNumberOfParticipants;
            this.isEnrollmentApplicable = training_calenderDTO.isEnrollmentApplicale ? "Yes" : "No";
            this.lastEnrollmentDate = dateFormat.format(new Date(training_calenderDTO.lastEnrollmentDate));
            this.prerequisite = training_calenderDTO.trainingPrerequisite;
            this.objective = training_calenderDTO.trainingObjective;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
