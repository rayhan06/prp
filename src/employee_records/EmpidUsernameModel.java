package employee_records;

import user.UserDTO;

public class EmpidUsernameModel {
    long empId;
    String userName;

    public EmpidUsernameModel(UserDTO dto) {
        this.empId = dto.employee_record_id;
        this.userName = dto.userName;
    }
}
