package employee_records;

/*
 * @author Md. Erfan Hossain
 * @created 19/04/2021 - 2:30 PM
 * @project parliament
 */

import common.ConnectionAndStatementUtil;
import sessionmanager.SessionConstants;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class LprDateAndRetirementDateUpdate {
    private static final String updateQuery = "UPDATE employee_records SET lpr_date = %d, retirement_date = %d WHERE ID = %d";
    public static void main(String[] args) {
        update();
        print();
    }

    private static void print(){
        List<Employee_recordsDTO> list = Employee_recordsRepository.getInstance().getEmployee_recordsList();
        list.parallelStream()
                .filter(e->e.isMP!=1)
                .forEach(dto->{
                    LocalDate dobLD = new Date(dto.dateOfBirth).toInstant()
                            .atZone(ZoneId.systemDefault())
                            .toLocalDate();
                    LocalDate prlLD = new Date(dto.lprDate).toInstant()
                            .atZone(ZoneId.systemDefault())
                            .toLocalDate();
                    LocalDate retirementLD = new Date(dto.retirementDate).toInstant()
                            .atZone(ZoneId.systemDefault())
                            .toLocalDate();
                    System.out.println(dto.iD+"\t"+dto.freedomFighterInfo+"\t"+dobLD+"\t"+prlLD+"\t"+retirementLD);
        });
    }

    private static void update(){
        List<Employee_recordsDTO> list = Employee_recordsRepository.getInstance().getEmployee_recordsList();
        list = list.parallelStream()
                .filter(e->e.isMP!=1)
                .collect(Collectors.toList());
        list.forEach(dto->{
            boolean flag = setDate(dto);
            if(flag){
                String sql = String.format(updateQuery,dto.lprDate,dto.retirementDate,dto.iD);
                System.out.println(sql);
                ConnectionAndStatementUtil.getWriteStatement(st->{
                    try {
                        st.execute(sql);
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                });
            }else{
                System.out.println("-- -- > "+dto);
            }
        });
    }

    private static boolean setDate(Employee_recordsDTO dto){
        if(dto.isMP != 1 && dto.dateOfBirth!= SessionConstants.MIN_DATE){
            LocalDate dobLD = new Date(dto.dateOfBirth).toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();
            LocalDate lprLD;
            if(dto.freedomFighterInfo != null && (dto.freedomFighterInfo.contains("1") || dto.freedomFighterInfo.contains("2"))){
                lprLD = dobLD.plusYears(61);
            }else{
                lprLD = dobLD.plusYears(59);
            }
            lprLD = lprLD.minusDays(1);

            LocalDate retirementLD = lprLD.plusYears(1);

            dto.lprDate = Date.from(lprLD.atStartOfDay()
                    .atZone(ZoneId.systemDefault())
                    .toInstant()).getTime();

            dto.retirementDate = Date.from(retirementLD.atStartOfDay()
                    .atZone(ZoneId.systemDefault())
                    .toInstant()).getTime();
            return true;
        }
        return false;
    }
}
