package employee_records;
import java.util.*; 


public class Employee_recordsMAPS 
{

	public HashMap<String, String> java_allfield_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_anyfield_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	public HashMap<String, String> java_SQL_map = new HashMap<String, String>();
	public HashMap<String, String> java_Text_map = new HashMap<String, String>();
	
	private static Employee_recordsMAPS self = null;
	
	private Employee_recordsMAPS()
	{
		
		java_allfield_type_map.put("name_eng".toLowerCase(), "String");
		java_allfield_type_map.put("name_bng".toLowerCase(), "String");
		java_allfield_type_map.put("nid".toLowerCase(), "String");
		java_allfield_type_map.put("personal_email".toLowerCase(), "String");
		java_allfield_type_map.put("personal_mobile".toLowerCase(), "String");
		java_allfield_type_map.put("alternative_mobile".toLowerCase(), "String");

		java_anyfield_search_map.put("employee_records.name_eng".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.name_bng".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.father_name_eng".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.father_name_bng".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.mother_name_eng".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.mother_name_bng".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.date_of_birth".toLowerCase(), "Long");
		java_anyfield_search_map.put("employee_records.place_of_birth".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.nationality".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.present_address".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.permanent_address".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.occupation".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.nid".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.nid_valid".toLowerCase(), "Boolean");
		java_anyfield_search_map.put("employee_records.bcn".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.ppn".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.gender".toLowerCase(), "String");
		java_anyfield_search_map.put("Islam, Hindu.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("Islam, Hindu.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.blood_group".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.marital_status".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.spouse_name_eng".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.spouse_name_bng".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.personal_email".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.personal_mobile".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.alternative_mobile".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.is_cadre".toLowerCase(), "Integer");
		java_anyfield_search_map.put("employee_records.employee_cadre_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("employee_records.employee_batch_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("employee_records.identity_no".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.appointment_memo_no".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.joining_date".toLowerCase(), "Long");
		java_anyfield_search_map.put("employee_records.service_rank_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("employee_records.service_grade_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("employee_records.service_ministry_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("employee_records.service_office_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("employee_records.current_office_ministry_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("employee_records.current_office_layer_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("employee_records.current_office_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("employee_records.current_office_unit_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("employee_records.current_office_joining_date".toLowerCase(), "Long");
		java_anyfield_search_map.put("employee_records.current_office_designation_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("employee_records.current_office_address".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.e_sign".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.d_sign".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.image_file_name".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.status".toLowerCase(), "Boolean");
		java_anyfield_search_map.put("employee_records.created_by".toLowerCase(), "Integer");
		java_anyfield_search_map.put("employee_records.modified_by".toLowerCase(), "Integer");
		java_anyfield_search_map.put("employee_records.created".toLowerCase(), "Long");
		java_anyfield_search_map.put("employee_records.modified".toLowerCase(), "Long");

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("nameEng".toLowerCase(), "nameEng".toLowerCase());
		java_DTO_map.put("nameBng".toLowerCase(), "nameBng".toLowerCase());
		java_DTO_map.put("fatherNameEng".toLowerCase(), "fatherNameEng".toLowerCase());
		java_DTO_map.put("fatherNameBng".toLowerCase(), "fatherNameBng".toLowerCase());
		java_DTO_map.put("motherNameEng".toLowerCase(), "motherNameEng".toLowerCase());
		java_DTO_map.put("motherNameBng".toLowerCase(), "motherNameBng".toLowerCase());
		java_DTO_map.put("dateOfBirth".toLowerCase(), "dateOfBirth".toLowerCase());
		java_DTO_map.put("placeOfBirth".toLowerCase(), "placeOfBirth".toLowerCase());
		java_DTO_map.put("nationality".toLowerCase(), "nationality".toLowerCase());
		java_DTO_map.put("presentAddress".toLowerCase(), "presentAddress".toLowerCase());
		java_DTO_map.put("permanentAddress".toLowerCase(), "permanentAddress".toLowerCase());
		java_DTO_map.put("occupation".toLowerCase(), "occupation".toLowerCase());
		java_DTO_map.put("nid".toLowerCase(), "nid".toLowerCase());
		java_DTO_map.put("nidValid".toLowerCase(), "nidValid".toLowerCase());
		java_DTO_map.put("bcn".toLowerCase(), "bcn".toLowerCase());
		java_DTO_map.put("ppn".toLowerCase(), "ppn".toLowerCase());
		java_DTO_map.put("gender".toLowerCase(), "gender".toLowerCase());
		java_DTO_map.put("religion".toLowerCase(), "religion".toLowerCase());
		java_DTO_map.put("bloodGroup".toLowerCase(), "bloodGroup".toLowerCase());
		java_DTO_map.put("maritalStatus".toLowerCase(), "maritalStatus".toLowerCase());
		java_DTO_map.put("spouseNameEng".toLowerCase(), "spouseNameEng".toLowerCase());
		java_DTO_map.put("spouseNameBng".toLowerCase(), "spouseNameBng".toLowerCase());
		java_DTO_map.put("personalEmail".toLowerCase(), "personalEmail".toLowerCase());
		java_DTO_map.put("personalMobile".toLowerCase(), "personalMobile".toLowerCase());
		java_DTO_map.put("alternativeMobile".toLowerCase(), "alternativeMobile".toLowerCase());
		java_DTO_map.put("isCadre".toLowerCase(), "isCadre".toLowerCase());
		java_DTO_map.put("employeeCadreId".toLowerCase(), "employeeCadreId".toLowerCase());
		java_DTO_map.put("employeeBatchId".toLowerCase(), "employeeBatchId".toLowerCase());
		java_DTO_map.put("identityNo".toLowerCase(), "identityNo".toLowerCase());
		java_DTO_map.put("appointmentMemoNo".toLowerCase(), "appointmentMemoNo".toLowerCase());
		java_DTO_map.put("joiningDate".toLowerCase(), "joiningDate".toLowerCase());
		java_DTO_map.put("serviceRankId".toLowerCase(), "serviceRankId".toLowerCase());
		java_DTO_map.put("serviceGradeId".toLowerCase(), "serviceGradeId".toLowerCase());
		java_DTO_map.put("serviceMinistryId".toLowerCase(), "serviceMinistryId".toLowerCase());
		java_DTO_map.put("serviceOfficeId".toLowerCase(), "serviceOfficeId".toLowerCase());
		java_DTO_map.put("currentOfficeMinistryId".toLowerCase(), "currentOfficeMinistryId".toLowerCase());
		java_DTO_map.put("currentOfficeLayerId".toLowerCase(), "currentOfficeLayerId".toLowerCase());
		java_DTO_map.put("currentOfficeId".toLowerCase(), "currentOfficeId".toLowerCase());
		java_DTO_map.put("currentOfficeUnitId".toLowerCase(), "currentOfficeUnitId".toLowerCase());
		java_DTO_map.put("currentOfficeJoiningDate".toLowerCase(), "currentOfficeJoiningDate".toLowerCase());
		java_DTO_map.put("currentOfficeDesignationId".toLowerCase(), "currentOfficeDesignationId".toLowerCase());
		java_DTO_map.put("currentOfficeAddress".toLowerCase(), "currentOfficeAddress".toLowerCase());
		java_DTO_map.put("eSign".toLowerCase(), "eSign".toLowerCase());
		java_DTO_map.put("dSign".toLowerCase(), "dSign".toLowerCase());
		java_DTO_map.put("imageFileName".toLowerCase(), "imageFileName".toLowerCase());
		java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
		java_DTO_map.put("createdBy".toLowerCase(), "createdBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("created".toLowerCase(), "created".toLowerCase());
		java_DTO_map.put("modified".toLowerCase(), "modified".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("name_eng".toLowerCase(), "nameEng".toLowerCase());
		java_SQL_map.put("name_bng".toLowerCase(), "nameBng".toLowerCase());
		java_SQL_map.put("father_name_eng".toLowerCase(), "fatherNameEng".toLowerCase());
		java_SQL_map.put("father_name_bng".toLowerCase(), "fatherNameBng".toLowerCase());
		java_SQL_map.put("mother_name_eng".toLowerCase(), "motherNameEng".toLowerCase());
		java_SQL_map.put("mother_name_bng".toLowerCase(), "motherNameBng".toLowerCase());
		java_SQL_map.put("date_of_birth".toLowerCase(), "dateOfBirth".toLowerCase());
		java_SQL_map.put("nid".toLowerCase(), "nid".toLowerCase());
		java_SQL_map.put("bcn".toLowerCase(), "bcn".toLowerCase());
		java_SQL_map.put("ppn".toLowerCase(), "ppn".toLowerCase());
		java_SQL_map.put("gender".toLowerCase(), "gender".toLowerCase());
		java_SQL_map.put("religion".toLowerCase(), "religion".toLowerCase());
		java_SQL_map.put("blood_group".toLowerCase(), "bloodGroup".toLowerCase());
		java_SQL_map.put("marital_status".toLowerCase(), "maritalStatus".toLowerCase());
		java_SQL_map.put("personal_email".toLowerCase(), "personalEmail".toLowerCase());
		java_SQL_map.put("personal_mobile".toLowerCase(), "personalMobile".toLowerCase());
		java_SQL_map.put("alternative_mobile".toLowerCase(), "alternativeMobile".toLowerCase());
		java_SQL_map.put("status".toLowerCase(), "status".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Name (English)".toLowerCase(), "nameEng".toLowerCase());
		java_Text_map.put("Name (Bangla)".toLowerCase(), "nameBng".toLowerCase());
		java_Text_map.put("Father Name (English)".toLowerCase(), "fatherNameEng".toLowerCase());
		java_Text_map.put("Father Name (Bangla)".toLowerCase(), "fatherNameBng".toLowerCase());
		java_Text_map.put("Mother Name (English)".toLowerCase(), "motherNameEng".toLowerCase());
		java_Text_map.put("Mother Name (Bangla)".toLowerCase(), "motherNameBng".toLowerCase());
		java_Text_map.put("Date Of Birth".toLowerCase(), "dateOfBirth".toLowerCase());
		java_Text_map.put("Place Of Birth".toLowerCase(), "placeOfBirth".toLowerCase());
		java_Text_map.put("Nationality".toLowerCase(), "nationality".toLowerCase());
		java_Text_map.put("Present Address".toLowerCase(), "presentAddress".toLowerCase());
		java_Text_map.put("Permanent Address".toLowerCase(), "permanentAddress".toLowerCase());
		java_Text_map.put("Occupation".toLowerCase(), "occupation".toLowerCase());
		java_Text_map.put("Nid".toLowerCase(), "nid".toLowerCase());
		java_Text_map.put("Nid Valid".toLowerCase(), "nidValid".toLowerCase());
		java_Text_map.put("Birth Reg. No".toLowerCase(), "bcn".toLowerCase());
		java_Text_map.put("Passport No".toLowerCase(), "ppn".toLowerCase());
		java_Text_map.put("Gender".toLowerCase(), "gender".toLowerCase());
		java_Text_map.put("Religion".toLowerCase(), "religion".toLowerCase());
		java_Text_map.put("Blood Group".toLowerCase(), "bloodGroup".toLowerCase());
		java_Text_map.put("Marital Status".toLowerCase(), "maritalStatus".toLowerCase());
		java_Text_map.put("Spouse Name (English)".toLowerCase(), "spouseNameEng".toLowerCase());
		java_Text_map.put("Spouse Name (Bngla)".toLowerCase(), "spouseNameBng".toLowerCase());
		java_Text_map.put("Personal Email".toLowerCase(), "personalEmail".toLowerCase());
		java_Text_map.put("Personal Mobile".toLowerCase(), "personalMobile".toLowerCase());
		java_Text_map.put("Alternative Mobile".toLowerCase(), "alternativeMobile".toLowerCase());
		java_Text_map.put("Is Cadre".toLowerCase(), "isCadre".toLowerCase());
		java_Text_map.put("Employee Cadre Id".toLowerCase(), "employeeCadreId".toLowerCase());
		java_Text_map.put("Employee Batch Id".toLowerCase(), "employeeBatchId".toLowerCase());
		java_Text_map.put("Identity No".toLowerCase(), "identityNo".toLowerCase());
		java_Text_map.put("Appointment Memo No".toLowerCase(), "appointmentMemoNo".toLowerCase());
		java_Text_map.put("Joining Date".toLowerCase(), "joiningDate".toLowerCase());
		java_Text_map.put("Service Rank Id".toLowerCase(), "serviceRankId".toLowerCase());
		java_Text_map.put("Service Grade Id".toLowerCase(), "serviceGradeId".toLowerCase());
		java_Text_map.put("Service Ministry Id".toLowerCase(), "serviceMinistryId".toLowerCase());
		java_Text_map.put("Service Office Id".toLowerCase(), "serviceOfficeId".toLowerCase());
		java_Text_map.put("Current Office Ministry Id".toLowerCase(), "currentOfficeMinistryId".toLowerCase());
		java_Text_map.put("Current Office Layer Id".toLowerCase(), "currentOfficeLayerId".toLowerCase());
		java_Text_map.put("Current Office Id".toLowerCase(), "currentOfficeId".toLowerCase());
		java_Text_map.put("Current Office Unit Id".toLowerCase(), "currentOfficeUnitId".toLowerCase());
		java_Text_map.put("Current Office Joining Date".toLowerCase(), "currentOfficeJoiningDate".toLowerCase());
		java_Text_map.put("Current Office Designation Id".toLowerCase(), "currentOfficeDesignationId".toLowerCase());
		java_Text_map.put("Current Office Address".toLowerCase(), "currentOfficeAddress".toLowerCase());
		java_Text_map.put("E Sign".toLowerCase(), "eSign".toLowerCase());
		java_Text_map.put("D Sign".toLowerCase(), "dSign".toLowerCase());
		java_Text_map.put("Image File Name".toLowerCase(), "imageFileName".toLowerCase());
		java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
		java_Text_map.put("Created By".toLowerCase(), "createdBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Created".toLowerCase(), "created".toLowerCase());
		java_Text_map.put("Modified".toLowerCase(), "modified".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}
	
	public static Employee_recordsMAPS GetInstance()
	{
		if(self == null)
		{
			self = new Employee_recordsMAPS ();
		}
		return self;
	}
	

}