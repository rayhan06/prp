package employee_records;

import political_party.Political_partyDTO;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PoliticalPartyDetailsModel {
    long id;
    String englishName;
    String banglaName;
    String abbreviation;
    String currentPresident;
    String currentGeneralSecretary;
    String inaugurationDate;
    String officialAddress;
    String websiteAddress;
    String emailAddress;

    public PoliticalPartyDetailsModel(Political_partyDTO dto) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        this.id = dto.iD;
        this.englishName = dto.nameEn;
        this.banglaName = dto.nameBn;
        this.abbreviation = dto.abbreviation;
        this.currentPresident = dto.presidentName;
        this.currentGeneralSecretary = dto.gsName;
        this.inaugurationDate = dateFormat.format(new Date(dto.foundationDate));
        this.officialAddress = dto.officeAddress;
        this.websiteAddress = dto.website;
        this.emailAddress = dto.email;
    }
}
