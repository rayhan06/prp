package employee_records;

import common.ApiResponse;
import common.NameDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import religion.ReligionRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GetOfficeUnitOrganograms implements EmployeeRecordsApiPerformer{
    @Override
    public ApiResponse perform(HttpServletRequest request) {
        try {

            List<DesignationModel> designationModels;
            List<OfficeUnitOrganograms> officeUnitOrganogramsList = OfficeUnitOrganogramsRepository.getInstance().getOffice_unit_organogramsList();

            designationModels = officeUnitOrganogramsList.stream().map(DesignationModel::new).collect(Collectors.toList());


            return designationModels.size()==0 ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                    ApiResponse.makeSuccessResponse(designationModels, "Successfully found the data.");
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
        }

    }
}