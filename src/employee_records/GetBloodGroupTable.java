package employee_records;

import common.ApiResponse;
import employee_family_info.Employee_family_infoDAO;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import user.UserRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

public class GetBloodGroupTable implements EmployeeRecordsApiPerformer{
    @Override
    public ApiResponse perform(HttpServletRequest request) {
        try {
            List<CategoryLanguageModel> bloodGroupList = CatRepository.getInstance().getCategoryLanguageModelList("blood_group");
            List<BloodGroupModel> bloodGroupTable = bloodGroupList.stream().map(dto -> new BloodGroupModel(dto.categoryValue, dto.englishText, dto.banglaText))
                    .collect(Collectors.toList());
            return bloodGroupTable.size()==0 ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                    ApiResponse.makeSuccessResponse(bloodGroupTable, "Successfully found the data.");
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
        }

    }
}
