package employee_records;

import committees_mapping.Committees_mappingDAO;
import committees_mapping.Committees_mappingDTO;
import common.ApiResponse;
import election_constituency.Election_constituencyDTO;
import election_constituency.Election_constituencyRepository;
import election_details.Election_detailsRepository;
import election_wise_mp.Election_wise_mpDTO;
import election_wise_mp.Election_wise_mpRepository;
import ministry_office_mapping.Ministry_office_mappingDAO;
import ministry_office_mapping.Ministry_office_mappingDTO;
import org.apache.log4j.Logger;
import political_party.Political_partyRepository;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class GetMpListByConstituencyNumberPerformer implements EmployeeRecordsApiPerformer{
    public final Logger logger = Logger.getLogger(GetMpListByConstituencyNumberPerformer.class);
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    @Override
    public ApiResponse perform(HttpServletRequest request) {
        long constituency_number, pageNo, pageSize, offset;

        try {
            constituency_number = Long.parseLong(request.getParameter("constituency_number"));
        } catch (NumberFormatException nfe) {
            return ApiResponse.makeResponseToBadRequest("Kindly check the constituency_number parameter provided!");
        }

        try {
            pageNo = Long.parseLong(request.getParameter("pageNo"));
        } catch (NumberFormatException nfe) {
            return ApiResponse.makeResponseToBadRequest("Kindly check the pageNo parameter provided!");
        }

        try {
            pageSize = Long.parseLong(request.getParameter("pageSize"));
        } catch (NumberFormatException nfe) {
            return ApiResponse.makeResponseToBadRequest("Kindly check the pageSize parameter provided!");
        }

        offset = (pageNo - 1) * pageSize;

        ApiResponse apiResponse;

        Employee_recordsDAO employee_recordsDAO = new Employee_recordsDAO();
        ConstituencyWiseMpModel constituencyWiseMpModel = new ConstituencyWiseMpModel();

        try {
//            Assuming that constituency number is unique... but it is not.... so, needed later implementation and made the current comment out!!!!

//            Optional<Election_constituencyDTO> election_constituencyDTO = Election_constituencyRepository.getInstance().getElection_constituencyList().stream().
//                    filter(dto -> dto.constituencyNumber==constituency_number).findFirst();
//
//            List<MemberOfParliamentMinimumInfoModel> memberOfParliamentMinimumInfoModels = Election_wise_mpRepository.getInstance().getAll().stream().
//                    filter(dto -> dto.electionConstituencyId==election_constituencyDTO.get().iD).map(dto -> new MemberOfParliamentMinimumInfoModel(dto.employeeRecordsId, Election_detailsRepository.getInstance().getElectionDetailsDTOByID(dto.electionDetailsId).parliamentNumber,
//                    election_constituencyDTO.get().constituencyNumber, election_constituencyDTO.get().constituencyNameEn, election_constituencyDTO.get().constituencyNameBn, Political_partyRepository.getInstance().getPolitical_partyDTOByID(dto.politicalPartyId).nameEn,
//                    Political_partyRepository.getInstance().getPolitical_partyDTOByID(dto.politicalPartyId).nameBn, dto.startDate, dto.endDate)).collect(Collectors.toList());


            List<MemberOfParliamentMinimumInfoModel> memberOfParliamentMinimumInfoModels = new ArrayList<MemberOfParliamentMinimumInfoModel>();
            List<Election_constituencyDTO> election_constituencyDTOs = Election_constituencyRepository.getInstance().getElection_constituencyList().stream().
                    filter(dto -> dto.constituencyNumber==constituency_number).collect(Collectors.toList());

            List<Committees_mappingDTO> committees_mappingDTOS = Committees_mappingDAO.getInstance().getAllDTOs(true);
            List<Ministry_office_mappingDTO> ministry_office_mappingDTOS = Ministry_office_mappingDAO.getInstance().getAllDTOs(true);

            for (Election_constituencyDTO election_constituencyDTO: election_constituencyDTOs) {
                memberOfParliamentMinimumInfoModels.addAll(Election_wise_mpRepository.getInstance().getAll().stream().
                        filter(dto -> dto.electionConstituencyId==election_constituencyDTO.iD).map(dto -> new MemberOfParliamentMinimumInfoModel(dto.employeeRecordsId, Election_detailsRepository.getInstance().getElectionDetailsDTOByID(dto.electionDetailsId).parliamentNumber,
                        election_constituencyDTO.constituencyNumber, election_constituencyDTO.constituencyNameEn, election_constituencyDTO.constituencyNameBn, Political_partyRepository.getInstance().getPolitical_partyDTOByID(dto.politicalPartyId).nameEn,
                        Political_partyRepository.getInstance().getPolitical_partyDTOByID(dto.politicalPartyId).nameBn, dto.startDate, dto.endDate, committees_mappingDTOS, ministry_office_mappingDTOS, dto.mpStatus)).collect(Collectors.toList()));
            }

            constituencyWiseMpModel.memberOfParliamentMinimumInfoModels = memberOfParliamentMinimumInfoModels.stream().skip(offset).limit(pageSize).collect(Collectors.toList());
            constituencyWiseMpModel.constituencyNumber = constituency_number;
            constituencyWiseMpModel.pageNo = pageNo;
            constituencyWiseMpModel.pageSize = pageSize;
            constituencyWiseMpModel.totalCount = memberOfParliamentMinimumInfoModels.stream().count();
            constituencyWiseMpModel.totalPageSize = (long) Math.ceil((double)constituencyWiseMpModel.totalCount / pageSize);

            apiResponse = (constituencyWiseMpModel.memberOfParliamentMinimumInfoModels.isEmpty()) ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                    ApiResponse.makeSuccessResponse(constituencyWiseMpModel, "Successfully found the entries.");


        } catch (Exception e) {
            e.printStackTrace();
            return ApiResponse.makeResponseToInternalServerError("Internal Server Errors!");
        }

        return apiResponse;
    }
}
