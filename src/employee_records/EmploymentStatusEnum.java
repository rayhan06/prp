package employee_records;

/*
 * @author Md. Erfan Hossain
 * @created 06/10/2021 - 11:53 PM
 * @project parliament
 */

public enum EmploymentStatusEnum {
    ACTIVE(1),
    DIED(2),
    RESIGNED(3),
    FIRED(4),
    RETIRED(5),
    RELEASED(6),
    LPR(7),
    DISMISSAL(7)
    ;

    private final int value;

    EmploymentStatusEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
