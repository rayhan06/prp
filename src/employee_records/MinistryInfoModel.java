package employee_records;

import ministry_office_mapping.Ministry_office_mappingDTO;
import pb.CatRepository;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MinistryInfoModel {
    public long ministryNameId;
    public String designation;
    public String startDate;
    public String endDate;

    public MinistryInfoModel(Ministry_office_mappingDTO dto) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

        ministryNameId = dto.ministryOfficeId;
        designation = CatRepository.getName("English", "ministers", dto.ministersCat);
        startDate = simpleDateFormat.format(new Date(dto.startDate));
        endDate = simpleDateFormat.format(new Date(dto.endDate));
    }

    public MinistryInfoModel() {
        System.out.println("MinistryInfoModel Called!");
    }
}
