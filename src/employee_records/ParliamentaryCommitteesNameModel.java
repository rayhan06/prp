package employee_records;

import common.NameDTO;

public class ParliamentaryCommitteesNameModel {
    long id;
    String nameEng;
    String nameBng;

    ParliamentaryCommitteesNameModel(NameDTO nameDTO) {
        id = nameDTO.iD;
        nameEng = nameDTO.nameEn;
        nameBng = nameDTO.nameBn;
    }
}
