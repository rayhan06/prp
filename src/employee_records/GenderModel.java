package employee_records;

public class GenderModel {
    public int id = 0;
    public String name_eng = "";
    public String name_bng = "";

    public GenderModel(int id, String name_eng, String name_bng) {
        this.id = id;
        this.name_eng = name_eng;
        this.name_bng = name_bng;
    }
}
