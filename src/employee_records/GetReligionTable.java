package employee_records;

import common.ApiResponse;
import common.NameDTO;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import religion.ReligionRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GetReligionTable implements EmployeeRecordsApiPerformer{
    @Override
    public ApiResponse perform(HttpServletRequest request) {
        try {

            List<ReligionModel> religionTable = new ArrayList<>();
            Map<Long, NameDTO> NameDTOtoId =  ReligionRepository.getInstance().getMapOfNameDTOToiD();

            for (Map.Entry<Long, NameDTO> entry : NameDTOtoId.entrySet()) {
                religionTable.add(new ReligionModel(entry.getKey(), entry.getValue().nameEn, entry.getValue().nameBn));
            }


            return religionTable.size()==0 ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                    ApiResponse.makeSuccessResponse(religionTable, "Successfully found the data.");
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
        }

    }
}
