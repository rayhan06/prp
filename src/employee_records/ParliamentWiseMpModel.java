package employee_records;

import java.util.ArrayList;
import java.util.List;

public class ParliamentWiseMpModel {
    long parliamentNumber;
    List<MemberOfParliamentMinimumInfoModel> memberOfParliamentMinimumInfoModels = new ArrayList<MemberOfParliamentMinimumInfoModel>();
    long pageNo;
    long pageSize;
    long totalPageSize;
    long totalCount;
}
