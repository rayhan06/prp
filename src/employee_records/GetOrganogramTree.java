package employee_records;

import common.ApiResponse;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class GetOrganogramTree implements EmployeeRecordsApiPerformer{
    public final Logger logger = Logger.getLogger(GetMpListByParliamentNumberPerformer.class);
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    @Override
    public ApiResponse perform(HttpServletRequest request) {
        long officeId = -1;

        try {
            // Optional Parameter
            officeId = Long.parseLong(request.getParameter("office_id"));
        } catch (NumberFormatException nfe) {
            System.out.println(nfe);
        }

        ApiResponse apiResponse;

        List<EmployeeInfoOrganogramTree> employeeMinimalInfoModels = new ArrayList<>();
        List<EmployeeInfoOrganogramTreeWithOfficeID> parliamentOrganogram = new ArrayList<>();

        try {


            if (officeId != -1) {

                List<OfficeUnitOrganograms> organogramDTOS = OfficeUnitOrganogramsRepository.getInstance().getByOfficeUnitId(officeId);

                for (OfficeUnitOrganograms organogramDTO : organogramDTOS) {
                    EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(organogramDTO.id);
                    if (employeeOfficeDTO != null) {
                        employeeMinimalInfoModels.add(new EmployeeInfoOrganogramTree(employeeOfficeDTO));
                    }
                }

                apiResponse = employeeMinimalInfoModels.size() == 0 ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                        ApiResponse.makeSuccessResponse(employeeMinimalInfoModels, "Successfully found the data.");
            } else {

                List<Office_unitsDTO> officeList = Office_unitsRepository.getInstance().getOffice_unitsList();

                for (Office_unitsDTO office_unitsDTO: officeList) {
                    List<OfficeUnitOrganograms> organogramDTOS = OfficeUnitOrganogramsRepository.getInstance().getByOfficeUnitId(office_unitsDTO.iD);

                    List<EmployeeInfoOrganogramTree> minimalEmployeeInfo = new ArrayList<>();
                    for (OfficeUnitOrganograms organogramDTO : organogramDTOS) {
                        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(organogramDTO.id);
                        if (employeeOfficeDTO != null) {
                            employeeMinimalInfoModels.add(new EmployeeInfoOrganogramTree(employeeOfficeDTO));
                        }
                    }

                    if (employeeMinimalInfoModels.size() != 0) {
                        EmployeeInfoOrganogramTreeWithOfficeID employeeInfoOrganogramTreeWithOfficeID = new EmployeeInfoOrganogramTreeWithOfficeID();
                        employeeInfoOrganogramTreeWithOfficeID.officeID = office_unitsDTO.iD;
                        employeeInfoOrganogramTreeWithOfficeID.organograms = employeeMinimalInfoModels;
                        parliamentOrganogram.add(employeeInfoOrganogramTreeWithOfficeID);
                    }
                }

                apiResponse = parliamentOrganogram.size() == 0 ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                        ApiResponse.makeSuccessResponse(parliamentOrganogram, "Successfully found the data.");

            }



        } catch (Exception e) {
            e.printStackTrace();
            return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
        }

        return apiResponse;
    }
}
