package employee_records;

import common.ApiResponse;
import employee_education_info.Employee_education_infoDAO;
import org.apache.log4j.Logger;
import user.UserRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

public class GetEducationInfo implements EmployeeRecordsApiPerformer{
    public final Logger logger = Logger.getLogger(GetMpListByParliamentNumberPerformer.class);

    @Override
    public ApiResponse perform(HttpServletRequest request) {
        String userName = request.getParameter("employee_record_id");
        long employeeRecordId;

        try {
            employeeRecordId = Employee_recordsRepository.getInstance().getEmployeeRecordIdByUsername(userName);
            if (employeeRecordId==0)
                return ApiResponse.makeResponseToResourceNotFound("No Record found with employee_record_id: "+userName+"!");
        } catch (Exception ex) {
            return ApiResponse.makeResponseToResourceNotFound("No Record found with employee_record_id: "+userName+"!");
        }

        try {
            try {
                List<EducationInfoModel> educationInfoModels = Employee_education_infoDAO.getInstance()
                        .getByEmployeeId(employeeRecordId)
                        .stream().map(EducationInfoModel::new)
                        .collect(Collectors.toList());
                return educationInfoModels.size()==0 ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                        ApiResponse.makeSuccessResponse(educationInfoModels, "Successfully found the data.");
            } catch (Exception e) {
                e.printStackTrace();
                return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
            }
        } catch (NumberFormatException nfe) {
            return ApiResponse.makeResponseToBadRequest("Kindly check the employee_record_id parameter provided!");
        }
    }
}