package employee_records;

import employee_language_proficiency.Employee_language_proficiencyDTO;
import pb.CatRepository;

public class EmployeeLanguageModel {
    String language = "";
    String readingSkill = "";
    String writingSkill = "";
    String speakingSkill = "";


    public EmployeeLanguageModel(Employee_language_proficiencyDTO dto) {
        this.language = CatRepository.getInstance().getText("English", "language", dto.languageCat);
        this.readingSkill = CatRepository.getInstance().getText("English", "read_skill", dto.languageCat);
        this.writingSkill = CatRepository.getInstance().getText("English", "write_skill", dto.languageCat);
        this.speakingSkill = CatRepository.getInstance().getText("English", "speak_skill", dto.languageCat);
    }
}
