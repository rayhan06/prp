package employee_records;

import common.ApiResponse;

import javax.servlet.http.HttpServletRequest;

public interface EmployeeRecordsApiPerformer {
    ApiResponse perform(HttpServletRequest request);
}
