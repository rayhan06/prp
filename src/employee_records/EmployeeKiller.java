package employee_records;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import office_unit_organograms.OfficeUnitOrganogramsRepository;
import org.apache.log4j.Logger;

import common.ConnectionAndStatementUtil;

import employee_offices.*;
import office_unit_organogram.Office_unit_organogramDAO;
import repository.RepositoryManager;
import user.UserRepository;
import util.TimeConverter;

public class EmployeeKiller {
    public static Logger logger = Logger.getLogger(RepositoryManager.class);

    Employee_recordsDAO employee_recordsDAO = new Employee_recordsDAO();
    EmployeeOfficesDAO employeeOfficesDAO = new EmployeeOfficesDAO();
    Office_unit_organogramDAO office_unit_organogramDAO = Office_unit_organogramDAO.getInstance();

    private EmployeeKiller() {
        logger.debug("<<<<EmployeeKiller is constructed");
        start();
    }

    private static class LazyLoader {
        static final EmployeeKiller INSTANCE = new EmployeeKiller();
    }

    public static EmployeeKiller getInstance() {
        return LazyLoader.INSTANCE;
    }

    private void start() {
        update(false);
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor(r -> {
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            return thread;
        });

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, 1);
        TimeConverter.clearTimes(cal);
        long delay = cal.getTimeInMillis() - System.currentTimeMillis();
        delay = delay / 1000;
        logger.info("delay : "+delay+" SECONDS");
        long hour = delay / 3600;
        long rem = delay % 3600;
        long min = rem / 60;
        long sec = rem % 60;
        logger.info(String.format("Scheduler will start after (hh:mm:ss) %02d:%02d:%02d", hour, min, sec));
        executorService.scheduleAtFixedRate(() -> update(true), delay, 24*3600, TimeUnit.SECONDS);
    }

    private void update(boolean updateCache) {
        ConnectionAndStatementUtil.getReadStatement(st -> {
            logger.debug("Killer is starting");
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_YEAR, -1);
            TimeConverter.clearTimes(cal);

            List<Employee_recordsDTO> retireList = employee_recordsDAO.getAllRetireEmployees(cal.getTimeInMillis());
            logger.info("RETIREMENT LIST : " + retireList.stream().map(e->e.iD).collect(Collectors.toList()));
            retireList.forEach(erDTO -> update(erDTO, "Retired",false, EmploymentStatusEnum.RETIRED));

            List<Employee_recordsDTO> lprList = employee_recordsDAO.getAllLPREmployees(cal.getTimeInMillis());
            logger.info("LPR LIST : " + lprList.stream().map(e->e.iD).collect(Collectors.toList()));
            lprList.forEach(erDTO -> update(erDTO, "LPR over",true, EmploymentStatusEnum.LPR));

            if (updateCache && (retireList.size() > 0 || lprList.size() > 0)) {
                logger.info("reload caches....");
                Employee_recordsRepository.getInstance().reload(true);
                EmployeeOfficeRepository.getInstance().reload(true);
                OfficeUnitOrganogramsRepository.getInstance().reload(true);
                UserRepository.getInstance().reload(true);
            }
        });
    }

    private void update(Employee_recordsDTO erDTO, String reason,boolean status, EmploymentStatusEnum employmentStatus) {
        employeeOfficesDAO.updateLastOfficeAll(erDTO.iD, erDTO.lprDate);
        office_unit_organogramDAO.updateVacancyALl(erDTO.iD);
        erDTO.status = status;
        erDTO.deactivationReason = reason;
        erDTO.employmentStatus = employmentStatus.getValue();
        try {
            employee_recordsDAO.activate(erDTO, false);
        } catch (Exception e) {
            logger.error(e);
        }
    }
}