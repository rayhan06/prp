package employee_records;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum EmploymentEnum {
    NONE(-1),
    REGULAR(1),
    DEPUTATION(2),
    PRIVILEGED(3),
    MASTER_ROLL(4),
    DAILY_BASIS(5),
    CONTACT_BASIS(6),
    OTHERS(9),
    ;

    private final static Map<Integer, EmploymentEnum> mapByValue;

    static {
        mapByValue = Arrays.stream(values())
                           .collect(Collectors.toMap(
                                   employeeTypeEnum -> employeeTypeEnum.value,
                                   Function.identity()
                           ));
    }

    public static EmploymentEnum getFromValue(int value) {
        return mapByValue.getOrDefault(value, NONE);
    }

    private final int value;

    EmploymentEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
