package employee_records;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum EmployeeOfficerType {
    NONE(-1),
    DEPUTATION(1),
    PARLIAMENT_OWN(2),
    OTHER(3),
    ;

    private final static Map<Integer, EmployeeOfficerType> mapByValue;

    static {
        mapByValue = Arrays.stream(values())
                           .collect(Collectors.toMap(
                                   officerType -> officerType.value,
                                   Function.identity()
                           ));
    }

    public static EmployeeOfficerType getFromValue(int value) {
        return mapByValue.getOrDefault(value, NONE);
    }

    private final int value;

    EmployeeOfficerType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
