package employee_records;

import common.ApiResponse;
import election_wise_mp.ElectionWiseMpModelForApi;
import election_wise_mp.Election_wise_mpDAO;
import election_wise_mp.Election_wise_mpDTO;
import election_wise_mp.Election_wise_mpRepository;
import geolocation.GeoLocationDAO2;
import geolocation.GeoLocationUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import pb.CatRepository;
import util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class GetEmployeeRecordByIdPerformer implements EmployeeRecordsApiPerformer {
    Logger logger = Logger.getLogger(GetEmployeeRecordByIdPerformer.class);
    @Override
    public ApiResponse perform(HttpServletRequest request) {
        String userId;
        ApiResponse apiResponse;

        try {
            userId = request.getParameter("empId");
        } catch (NumberFormatException nfe) {
            return ApiResponse.makeResponseToBadRequest("Kindly check the empId parameter provided!");
        }

        try {
            EmployeeRecordModelForApi employeeRecordModelForApi = new EmployeeRecordModelForApi();
            Employee_recordsDTO employee_recordsDTO =new Employee_recordsDAO().getByEmployeeNumber(userId);
            if(employee_recordsDTO == null){
                return ApiResponse.makeResponseToResourceNotFound("No Record ID found with empID: "+userId+"!");
            }
            Election_wise_mpDAO election_wise_mpDAO = Election_wise_mpDAO.getInstance();
            List<ElectionWiseMpModelForApi> electionDetailModels = election_wise_mpDAO.getAllElectionInfoByEmpId(employee_recordsDTO.iD);

            if (employee_recordsDTO.isDeleted) {
                apiResponse = ApiResponse.makeResponseToResourceNotFound("This employee was deleted");
            } else {
                employeeRecordModelForApi.employeeBasicInformationModel = getEmployeeBasicInfoFromEmployeeRecordDTO(employee_recordsDTO);
                employeeRecordModelForApi.electionDetailModels = electionDetailModels;

                apiResponse = ApiResponse.makeSuccessResponse(employeeRecordModelForApi, "Successfully retrieved the Employee Record.");
            }

        } catch (Exception e) {
            e.printStackTrace();
            apiResponse = ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
        }

        return apiResponse;
    }


    private EmployeeBasicInformationModelForApi getEmployeeBasicInfoFromEmployeeRecordDTO(Employee_recordsDTO dto) {
        EmployeeBasicInformationModelForApi model = new EmployeeBasicInformationModelForApi();
        model.nameEng = dto.nameEng;
        model.nameBng = dto.nameBng;
        model.fatherNameEng = dto.fatherNameEng;
        model.fatherNameBng = dto.fatherNameBng;
        model.motherNameEng = dto.motherNameEng;
        model.motherNameBng = dto.motherNameBng;
        model.dateOfBirth = StringUtils.getFormattedDate("English",dto.dateOfBirth);
        model.presentAddressEng = GeoLocationDAO2.parseAddress(dto.presentAddress);
        model.presentAddressBng = GeoLocationDAO2.parseAddress(dto.presentAddressBng);
        model.permanentAddressEng = GeoLocationDAO2.parseAddress(dto.permanentAddress);
        model.permanentAddressBng = GeoLocationDAO2.parseAddress(dto.permanentAddressBng);

        model.presentAddressEng = GeoLocationUtils.getGeoLocationString(dto.presentAddress, "English");
        model.presentAddressBng = GeoLocationUtils.getGeoLocationString(dto.presentAddressBng, "Bangla");
        model.permanentAddressEng = GeoLocationUtils.getGeoLocationString(dto.permanentAddress, "English");
        model.permanentAddressBng = GeoLocationUtils.getGeoLocationString(dto.permanentAddressBng, "Bangla");

        model.nidNumber = dto.nid;
        model.birthCertificateNumber = dto.bcn;
        model.passportNumber = dto.passportNo;
        model.passportIssueDate = StringUtils.getFormattedDate("English",dto.passportIssueDate);
        model.passportExpireDate = StringUtils.getFormattedDate("English",dto.passportExpiryDate);
        model.gender = String.valueOf(dto.gender);
        model.bloodGroup = String.valueOf(dto.bloodGroup);
        model.religion = String.valueOf(dto.religion);
        model.height = dto.height;
        model.identificationMark = dto.identificationMark;
        model.personalMobile = dto.personalMobile;
        model.alternativeMobile = dto.alternativeMobile;
        model.email = dto.personalEml;
        model.employmentCategory = CatRepository.getInstance().getText("English","employment",dto.employmentType);
        model.freedomFighterInfo = CatRepository.getInstance().commaSeparateFreedomFighter(dto.freedomFighterInfo, "English");
        model.officePhoneNumber = dto.officePhoneNumber;
        model.officePhoneExtension = dto.officePhoneExtension;
        model.faxNumber = dto.faxNumber;
        model.isMP = dto.isMP;


        model.photo = Base64.encodeBase64(dto.photo);
//        model.photo = dto.photo;
        model.signature = Base64.encodeBase64(dto.signature);
//        model.signature = dto.signature;


        if(dto.isMP == 1){
            Election_wise_mpDTO electionWiseMpDTO = Election_wise_mpRepository.getInstance().getDTOByID(dto.electionWiseMpId);
            try {
                model.alternateImageOfMP = Base64.encodeBase64(electionWiseMpDTO.alternateImageOfMP);
//                model.alternateImageOfMP = electionWiseMpDTO.alternateImageOfMP;
            } catch (Exception ex) {
                model.alternateImageOfMP = null;
            }
            model.addressOfMP = dto.addressOfMP;
            model.professionOfMP = dto.professionOfMP;
        }

        return model;
    }
}



