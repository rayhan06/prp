package employee_records;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import common.ConnectionType;
import dbm.DBMW;
import election_constituency.Election_constituencyDTO;
import election_constituency.Election_constituencyRepository;
import election_details.Election_detailsDTO;
import election_details.Election_detailsRepository;
import election_wise_mp.Election_wise_mpDAO;
import election_wise_mp.Election_wise_mpDTO;
import election_wise_mp.Election_wise_mpRepository;
import employee_family_info.Employee_family_infoDAO;
import employee_family_info.Employee_family_infoDTO;
import employee_nominee.Employee_nomineeDAO;
import employee_nominee.Employee_nomineeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_offices.EmployeeOfficesDAO;
import login.LoginDTO;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.log4j.Logger;

import appointment.YearMonthCount;
import pb.CatDAO;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import pb.OptionDTO;
import pb.Utils;
import repository.RepositoryManager;
import sessionmanager.SessionConstants;
import user.UserDAO;
import user.UserDTO;
import user.UserRepository;
import util.LongWrapper;
import util.NavigationService2;
import util.PasswordUtil;
import util.StringUtils;
import util.TimeConverter;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@SuppressWarnings({"rawtypes", "unchecked", "Duplicates", "unused"})
public class Employee_recordsDAO extends NavigationService2 {

    public int ministry_id;
    public int layer_id;
    public int origin_id;
    public int office_id;

    public String name_en;
    public String name_bn;
    public String nid;
    public String email;
    public String officialEmail;
    public String phone0;
    public String user_name;
    public LoginDTO loginDTO;

    private final int pensionStartYear;

    public Employee_recordsDAO(LoginDTO loginDTO) {
        this.loginDTO = loginDTO;
        this.pensionStartYear = 0;
    }

    public Employee_recordsDAO(LoginDTO loginDTO, int pensionStartYear) {
        this.loginDTO = loginDTO;
        this.pensionStartYear = pensionStartYear;
    }

    public Employee_recordsDAO() {
        this.pensionStartYear = 0;
    }

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    private static final Logger logger = Logger.getLogger(Employee_recordsDAO.class);

    private static final String getBatchSqlQuery = "select id, batch_no from employee_batches";

    private static final String getCadresSqlQuery = "select id, cadre_name_eng, cadre_name_bng from employee_cadres";

    private static final String getById = "SELECT * FROM employee_records  WHERE ID= ?";

    private static final String getByFirstLoadCondition = "SELECT * FROM employee_records  WHERE {firstLoadCondition} order by lastModificationTime desc";

    private static final String getByIds = "SELECT * FROM employee_records WHERE ID IN ( {ids} ) order by lastModificationTime desc";

    private static final String getByIdsNotSort = "SELECT * FROM employee_records WHERE ID IN (%s) ";

    private static final String getByMatchingName = "SELECT id from employee_records where name_eng like '%{search}%' or name_bng like '%{search}%'";

    private static final String addQuery = "INSERT INTO employee_records ( name_eng, name_bng, father_name_eng, father_name_bng, " //4
            .concat("mother_name_eng, mother_name_bng, date_of_birth, present_address, permanent_address, present_address_bng, ")//6
            .concat("permanent_address_bng, nid, bcn, gender_cat, religion, blood_group, marital_status, ")//7
            .concat("personal_email, personal_mobile, alternative_mobile,office_phone_number,office_phone_extension,fax_number, birth_district,")//7
            .concat("home_district, passport_no, passport_issue_date, passport_expiry_date, marriage_date, nationality_type, identification_mark,")//7
            .concat("height, status, created_by, modified_by, created, filesDropzone, ")//7
            .concat(" lastModificationTime, is_mp,employee_number,")//7
            .concat("freedom_fighter_info,photo,signature,nid_front_photo,nid_back_photo,employment_cat,employee_class_cat,emp_officer_cat,")//7
            .concat("joining_date, lpr_date,retirement_date,present_division,present_district,")//5
            .concat("permanent_division,permanent_district,provision_period,provision_end_date,pension_remarks,official_email,job_quota,quota_file,")//8
            .concat("mp_elected_count,election_wise_mp_id,employment_status,employment_status_change_date, other_office_department_type, search_column,same_as_permanent,isDeleted, ID) VALUES (")//5
            .concat("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
            .concat("?,?,?,?,?,?,?,?)");

    private static final String updateQuery = "UPDATE employee_records SET name_eng=?, name_bng=?, father_name_eng=?, father_name_bng=?, "
            .concat("mother_name_eng=?, mother_name_bng=?, date_of_birth=?, present_address=?, permanent_address=?, present_address_bng=?,")
            .concat("permanent_address_bng=?, nid=?, bcn=?, gender_cat=?, religion=?, blood_group=?, marital_status=?,")
            .concat("personal_email=?, personal_mobile=?, alternative_mobile=?,office_phone_number=?,office_phone_extension=?,fax_number=?,")
            .concat("birth_district=?, home_district=?, passport_no=?, passport_issue_date=?, passport_expiry_date=?, marriage_date=?, ")
            .concat("nationality_type=?, identification_mark=?, height=?,status=?, created_by=?, modified_by=?, created=?,")
            .concat("filesDropzone=?, lastModificationTime=?, is_mp=?,")
            .concat("employee_number=?,freedom_fighter_info=?,photo=?,signature=?,nid_front_photo=?,nid_back_photo=?, employment_cat = ?,")
            .concat("employee_class_cat = ?,emp_officer_cat=?, joining_date = ?, lpr_date = ?,retirement_date=?,")
            .concat("present_division=?,present_district=?,permanent_division=?,permanent_district=?,provision_period=?,provision_end_date=?,")
            .concat("pension_remarks=?,official_email=?,job_quota=?,quota_file=?,mp_elected_count=?,election_wise_mp_id=?,employment_status=?,")
            .concat("employment_status_change_date=?, other_office_department_type = ?, search_column=?,same_as_permanent=?,pay_scale=? WHERE ID = ?");

    private static final String updatePersonalInfoQuery = "UPDATE employee_records SET name_eng=?, name_bng=?, father_name_eng=?, father_name_bng=?, "
            .concat("mother_name_eng=?, mother_name_bng=?, date_of_birth=?,")
            .concat("gender_cat=?, religion=?,nationality_type=?,lastModificationTime=?,search_column=? WHERE ID = ?  ");

    private static final String updateAddressInfoQuery = "UPDATE employee_records SET present_division=?, present_district=?, present_address=?, present_address_bng=?, "
            .concat("permanent_division=?, permanent_district=?, permanent_address=?,permanent_address_bng=?,")
            .concat("home_district=?, birth_district=?,lastModificationTime=?,same_as_permanent=? WHERE ID = ?  ");

    private static final String updateConfidentialInfoQuery = "UPDATE employee_records SET bcn=?, passport_no=?, passport_issue_date=?, passport_expiry_date=?, "
            .concat("lastModificationTime=? WHERE ID = ?  ");


    private static final String updateContactInfoQuery = "UPDATE employee_records SET personal_email=?, personal_mobile=?, alternative_mobile=?,office_phone_number=?,office_phone_extension=?,fax_number=?, official_email=?,"
            .concat("lastModificationTime=?,search_column=? WHERE ID = ?  ");


    private static final String updateSearchColumnAndEmpBn = "UPDATE employee_records SET employee_number_bng=?,"
            .concat("lastModificationTime=?,search_column=? WHERE ID = ?  ");

    private static final String updateMiscellaneousInfoQuery = "UPDATE employee_records SET marital_status=?, marriage_date=?, identification_mark=?,height=?,blood_group=?,freedom_fighter_info=?,job_quota=?,quota_file=?, "
            .concat("lastModificationTime=? WHERE ID = ?  ");

    private static final String updateNIDInfoQuery = "UPDATE employee_records SET nid=?, nid_front_photo=?, nid_back_photo=?, "
            .concat("lastModificationTime=?,search_column=? WHERE ID = ?  ");

    private static final String updateLPRInfoQuery = "UPDATE employee_records SET lpr_date=?, retirement_date=?, "
            .concat("lastModificationTime=? WHERE ID = ?  ");

    private static final String updateImageInfoQuery = "UPDATE employee_records SET photo=?, signature=?,  "
            .concat("lastModificationTime=? WHERE ID = ?  ");

    private static final String updateEmploymentInfoQuery = "UPDATE employee_records SET joining_date=?, employment_cat = ?,employee_class_cat = ?,emp_officer_cat=?, lpr_date = ?,retirement_date=?, "
            .concat("provision_period=?,provision_end_date=?,employment_status=?,employment_status_change_date=?,employee_number=?,employee_number_bng=?,mp_elected_count=?,")
            .concat("current_office=?,current_designation=?,other_office_department_type = ?,pay_scale = ?,")
            .concat("lastModificationTime=?,search_column=? WHERE ID = ?  ");

    private static final String addUserQuery = " INSERT INTO users (id, username, password, user_alias, employee_record_id, user_role_id, user_status, roleID, "
            .concat("languageID, userType, fullName, phoneNo, lastModificationTime, isDeleted, username_old) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

    private static final String updatePasswordQuery = " UPDATE users SET password = ?,lastModificationTime= ? WHERE username = ?";

    private static final String deleteQuery = "UPDATE employee_records SET isDeleted=1,status=0,lastModificationTime= ? WHERE ID = ?";

    private static final String countByEmployeeNumberPrefix = "SELECT count(id) as countID from employee_records where employee_number like '{}%'";

    private static final String employeeNumberByPrefix = "SELECT employee_number from employee_records where employee_number like '{}%'";

    private static final String updatePensionRemarks = "update employee_records set pension_remarks=? where id=?";

    private static final String activeCountByEmployeeNumberPrefix = "SELECT count(*) from employee_records where employee_number like '{}%' and status = 1";

    private static final String employeeIdByReligion = "SELECT ID from employee_records where religion=%d and isDeleted=0";

    private static final String deleteMpQuery = "UPDATE election_wise_mp SET isDeleted=1,lastModificationTime= ? WHERE ID = ?";

    private static final String deActiveUser = "UPDATE users SET active = 0,lastModificationTime = ? WHERE username = ?";

    private static final String getByEmployeeNumber = "SELECT * FROM employee_records WHERE employee_number = '%s'";

    private void set(Employee_recordsDTO employee_recordsDTO, PreparedStatement ps, boolean isInsert, long lastModificationTime) throws SQLException {
        int index = 0;
        ps.setObject(++index, employee_recordsDTO.nameEng);
        ps.setObject(++index, employee_recordsDTO.nameBng);
        ps.setObject(++index, employee_recordsDTO.fatherNameEng);
        ps.setObject(++index, employee_recordsDTO.fatherNameBng);
        ps.setObject(++index, employee_recordsDTO.motherNameEng);
        ps.setObject(++index, employee_recordsDTO.motherNameBng);
        ps.setObject(++index, employee_recordsDTO.dateOfBirth);
        ps.setObject(++index, employee_recordsDTO.presentAddress);
        ps.setObject(++index, employee_recordsDTO.permanentAddress);
        ps.setObject(++index, employee_recordsDTO.presentAddressBng);
        ps.setObject(++index, employee_recordsDTO.permanentAddressBng);
        ps.setObject(++index, employee_recordsDTO.nid);
        ps.setObject(++index, employee_recordsDTO.bcn);
        ps.setObject(++index, employee_recordsDTO.gender);
        ps.setObject(++index, employee_recordsDTO.religion);
        ps.setObject(++index, employee_recordsDTO.bloodGroup);
        ps.setObject(++index, employee_recordsDTO.maritalStatus);
        ps.setObject(++index, employee_recordsDTO.personalEml);
        ps.setObject(++index, employee_recordsDTO.personalMobile);
        ps.setObject(++index, employee_recordsDTO.alternativeMobile);
        ps.setObject(++index, employee_recordsDTO.officePhoneNumber);
        ps.setObject(++index, employee_recordsDTO.officePhoneExtension);
        ps.setObject(++index, employee_recordsDTO.faxNumber);
        ps.setObject(++index, employee_recordsDTO.birthDistrict);
        ps.setObject(++index, employee_recordsDTO.homeDistrict);
        ps.setObject(++index, employee_recordsDTO.passportNo);
        ps.setObject(++index, employee_recordsDTO.passportIssueDate);
        ps.setObject(++index, employee_recordsDTO.passportExpiryDate);
        ps.setObject(++index, employee_recordsDTO.marriageDate);
        ps.setObject(++index, employee_recordsDTO.nationalityType);
        ps.setObject(++index, employee_recordsDTO.identificationMark);
        ps.setObject(++index, employee_recordsDTO.height);
        ps.setObject(++index, employee_recordsDTO.status);
        ps.setObject(++index, employee_recordsDTO.createdBy);
        ps.setObject(++index, employee_recordsDTO.modifiedBy);
        ps.setObject(++index, employee_recordsDTO.created);
        ps.setObject(++index, employee_recordsDTO.filesDropzone);
        ps.setObject(++index, lastModificationTime);
        ps.setObject(++index, employee_recordsDTO.isMP);
        ps.setObject(++index, employee_recordsDTO.employeeNumber);
        ps.setObject(++index, employee_recordsDTO.freedomFighterInfo);
        ps.setBytes(++index, employee_recordsDTO.photo);
        ps.setBytes(++index, employee_recordsDTO.signature);
        ps.setBytes(++index, employee_recordsDTO.nidFrontPhoto);
        ps.setBytes(++index, employee_recordsDTO.nidBackPhoto);
        ps.setObject(++index, employee_recordsDTO.employmentType);
        ps.setObject(++index, employee_recordsDTO.employeeClass);
        ps.setObject(++index, employee_recordsDTO.officerTypeCat);
        ps.setObject(++index, employee_recordsDTO.joiningDate);
        ps.setObject(++index, employee_recordsDTO.lprDate);
        ps.setObject(++index, employee_recordsDTO.retirementDate);
        ps.setObject(++index, employee_recordsDTO.presentDivisionId);
        ps.setObject(++index, employee_recordsDTO.presentDistrictId);
        ps.setObject(++index, employee_recordsDTO.permanentDivisionId);
        ps.setObject(++index, employee_recordsDTO.permanentDistrictId);
        ps.setObject(++index, employee_recordsDTO.provisionPeriod);
        ps.setObject(++index, employee_recordsDTO.provisionEndDate);
        ps.setObject(++index, employee_recordsDTO.pensionRemarks);
        ps.setObject(++index, employee_recordsDTO.officialEml);
        ps.setObject(++index, employee_recordsDTO.jobQuota);
        ps.setObject(++index, employee_recordsDTO.quotaFile);
        ps.setObject(++index, employee_recordsDTO.mpElectedCount);
        ps.setObject(++index, employee_recordsDTO.electionWiseMpId);
        ps.setObject(++index, employee_recordsDTO.employmentStatus);
        ps.setObject(++index, employee_recordsDTO.employmentStatusChangeDate);
        ps.setObject(++index, employee_recordsDTO.otherOfficeDeptType);
        ps.setObject(++index, buildSearchColumn(employee_recordsDTO));
        ps.setObject(++index, employee_recordsDTO.sameAsPermanent);
        ps.setObject(++index, employee_recordsDTO.payScale);
        if (isInsert) {
            ps.setObject(++index, employee_recordsDTO.isDeleted);
        }
        ps.setObject(++index, employee_recordsDTO.iD);
    }


    private void setForSearchColumnAndEmpBn(Employee_recordsDTO employee_recordsDTO, PreparedStatement ps, long lastModificationTime) throws SQLException {
        int index = 0;
        ps.setObject(++index, employee_recordsDTO.employeeNumberBng);
        ps.setObject(++index, lastModificationTime);
        ps.setObject(++index, buildSearchColumn(employee_recordsDTO));
        ps.setObject(++index, employee_recordsDTO.iD);
    }

    private String buildSearchColumn(Employee_recordsDTO employee_recordsDTO) {
        StringBuilder searchColumnBuilder = new StringBuilder();
        appendIfNotFound(employee_recordsDTO.nameEng, searchColumnBuilder)
                .appendIfNotFound(employee_recordsDTO.nameBng, searchColumnBuilder)
                .appendIfNotFound(employee_recordsDTO.personalMobile, searchColumnBuilder)
                .appendIfNotFound(StringUtils.convertToBanNumber(employee_recordsDTO.personalMobile), searchColumnBuilder)
                .appendIfNotFound(employee_recordsDTO.alternativeMobile, searchColumnBuilder)
                .appendIfNotFound(StringUtils.convertToBanNumber(employee_recordsDTO.alternativeMobile), searchColumnBuilder)
                .appendIfNotFound(employee_recordsDTO.personalEml, searchColumnBuilder)
                .appendIfNotFound(employee_recordsDTO.employeeNumber, searchColumnBuilder)
                .appendIfNotFound(StringUtils.convertToBanNumber(employee_recordsDTO.employeeNumber), searchColumnBuilder)
                .appendIfNotFound(employee_recordsDTO.nid, searchColumnBuilder)
                .appendIfNotFound(StringUtils.convertToBanNumber(employee_recordsDTO.nid), searchColumnBuilder)
        ;
        CategoryLanguageModel model = CatRepository.getInstance().getCategoryLanguageModel("emp_officer", employee_recordsDTO.officerTypeCat);
        if (model != null) {
            searchColumnBuilder.append(model.englishText).append(" ").append(model.banglaText).append(" ");
        }
        model = CatRepository.getInstance().getCategoryLanguageModel("employee_class", employee_recordsDTO.employeeClass);
        if (model != null) {
            searchColumnBuilder.append(model.englishText).append(" ").append(model.banglaText).append(" ");
        }
        model = CatRepository.getInstance().getCategoryLanguageModel("employment", employee_recordsDTO.employmentType);
        if (model != null) {
            searchColumnBuilder.append(model.englishText).append(" ").append(model.banglaText).append(" ");
        }
        return searchColumnBuilder.toString();
    }

    private Employee_recordsDAO appendIfNotFound(String appendStr, StringBuilder sb) {
        if (appendStr != null && appendStr.length() > 0 && sb.indexOf(appendStr) == -1) {
            sb.append(appendStr).append(" ");
        }
        return this;
    }


    private void setPersonalInfo(Employee_recordsDTO employee_recordsDTO, PreparedStatement ps, long lastModificationTime) throws SQLException {
        int index = 0;
        ps.setObject(++index, employee_recordsDTO.nameEng);
        ps.setObject(++index, employee_recordsDTO.nameBng);
        ps.setObject(++index, employee_recordsDTO.fatherNameEng);
        ps.setObject(++index, employee_recordsDTO.fatherNameBng);
        ps.setObject(++index, employee_recordsDTO.motherNameEng);
        ps.setObject(++index, employee_recordsDTO.motherNameBng);
        ps.setObject(++index, employee_recordsDTO.dateOfBirth);
        ps.setObject(++index, employee_recordsDTO.gender);
        ps.setObject(++index, employee_recordsDTO.religion);
        ps.setObject(++index, employee_recordsDTO.nationalityType);
        ps.setObject(++index, lastModificationTime);
        ps.setObject(++index, buildSearchColumn(employee_recordsDTO));
        ps.setObject(++index, employee_recordsDTO.iD);
    }

    private void setAddressInfo(Employee_recordsDTO employee_recordsDTO, PreparedStatement ps, long lastModificationTime) throws SQLException {
        int index = 0;
        ps.setObject(++index, employee_recordsDTO.presentDivisionId);
        ps.setObject(++index, employee_recordsDTO.presentDistrictId);
        ps.setObject(++index, employee_recordsDTO.presentAddress);
        ps.setObject(++index, employee_recordsDTO.presentAddressBng);

        ps.setObject(++index, employee_recordsDTO.permanentDivisionId);
        ps.setObject(++index, employee_recordsDTO.permanentDistrictId);

        ps.setObject(++index, employee_recordsDTO.permanentAddress);
        ps.setObject(++index, employee_recordsDTO.permanentAddressBng);
        ps.setObject(++index, employee_recordsDTO.homeDistrict);

        ps.setObject(++index, employee_recordsDTO.birthDistrict);
        ps.setObject(++index, lastModificationTime);
        ps.setObject(++index, employee_recordsDTO.sameAsPermanent);
        ps.setObject(++index, employee_recordsDTO.iD);
    }

    private void setConfidentialInfo(Employee_recordsDTO employee_recordsDTO, PreparedStatement ps, long lastModificationTime) throws SQLException {
        int index = 0;
        ps.setObject(++index, employee_recordsDTO.bcn);
        ps.setObject(++index, employee_recordsDTO.passportNo);
        ps.setObject(++index, employee_recordsDTO.passportIssueDate);
        ps.setObject(++index, employee_recordsDTO.passportExpiryDate);

        ps.setObject(++index, lastModificationTime);
        ps.setObject(++index, employee_recordsDTO.iD);

    }

    private static final String activateDeactivateEmployee = "UPDATE employee_records SET status=?, deactivation_reason = ?,"
            .concat("lastModificationTime=?,employment_status=? WHERE ID = ?  ");
    private void activate(Employee_recordsDTO employee_recordsDTO, PreparedStatement ps, long lastModificationTime) throws SQLException {
        int index = 0;
        ps.setObject(++index, employee_recordsDTO.status);
        ps.setObject(++index, employee_recordsDTO.deactivationReason);

        ps.setObject(++index, lastModificationTime);
        ps.setObject(++index, employee_recordsDTO.employmentStatus);
        ps.setObject(++index, employee_recordsDTO.iD);

    }

    private void setContactInfo(Employee_recordsDTO employee_recordsDTO, PreparedStatement ps, long lastModificationTime) throws SQLException {
        int index = 0;
        ps.setObject(++index, employee_recordsDTO.personalEml);
        ps.setObject(++index, employee_recordsDTO.personalMobile);
        ps.setObject(++index, employee_recordsDTO.alternativeMobile);
        ps.setObject(++index, employee_recordsDTO.officePhoneNumber);
        ps.setObject(++index, employee_recordsDTO.officePhoneExtension);
        ps.setObject(++index, employee_recordsDTO.faxNumber);
        ps.setObject(++index, employee_recordsDTO.officialEml);

        ps.setObject(++index, lastModificationTime);
        ps.setObject(++index, buildSearchColumn(employee_recordsDTO));
        ps.setObject(++index, employee_recordsDTO.iD);

    }

    private void setMiscellaneousInfo(Employee_recordsDTO employee_recordsDTO, PreparedStatement ps, long lastModificationTime) throws SQLException {
        int index = 0;
        ps.setObject(++index, employee_recordsDTO.maritalStatus);
        ps.setObject(++index, employee_recordsDTO.marriageDate);
        ps.setObject(++index, employee_recordsDTO.identificationMark);
        ps.setObject(++index, employee_recordsDTO.height);
        ps.setObject(++index, employee_recordsDTO.bloodGroup);
        ps.setObject(++index, employee_recordsDTO.freedomFighterInfo);
        ps.setObject(++index, employee_recordsDTO.jobQuota);
        ps.setObject(++index, employee_recordsDTO.quotaFile);

        ps.setObject(++index, lastModificationTime);
        ps.setObject(++index, employee_recordsDTO.iD);

    }

    private void setNIDInfo(Employee_recordsDTO employee_recordsDTO, PreparedStatement ps, long lastModificationTime) throws SQLException {
        int index = 0;
        ps.setObject(++index, employee_recordsDTO.nid);
        ps.setObject(++index, employee_recordsDTO.nidFrontPhoto);
        ps.setObject(++index, employee_recordsDTO.nidBackPhoto);
        ps.setObject(++index, lastModificationTime);
        ps.setObject(++index, buildSearchColumn(employee_recordsDTO));
        ps.setObject(++index, employee_recordsDTO.iD);
    }

    private void setLPRInfo(Employee_recordsDTO employee_recordsDTO, PreparedStatement ps, long lastModificationTime) throws SQLException {
        int index = 0;
        ps.setObject(++index, employee_recordsDTO.lprDate);
        ps.setObject(++index, employee_recordsDTO.retirementDate);
        ps.setObject(++index, lastModificationTime);
        ps.setObject(++index, employee_recordsDTO.iD);

    }

    private void setImageInfo(Employee_recordsDTO employee_recordsDTO, PreparedStatement ps, long lastModificationTime) throws SQLException {
        int index = 0;
        ps.setObject(++index, employee_recordsDTO.photo);
        ps.setObject(++index, employee_recordsDTO.signature);
        ps.setObject(++index, lastModificationTime);
        ps.setObject(++index, employee_recordsDTO.iD);

    }

    private void setEmploymentInfo(Employee_recordsDTO employee_recordsDTO, PreparedStatement ps, long lastModificationTime) throws SQLException {

        int index = 0;
        ps.setObject(++index, employee_recordsDTO.joiningDate);
        ps.setObject(++index, employee_recordsDTO.employmentType);
        ps.setObject(++index, employee_recordsDTO.employeeClass);
        ps.setObject(++index, employee_recordsDTO.officerTypeCat);
        ps.setObject(++index, employee_recordsDTO.lprDate);
        ps.setObject(++index, employee_recordsDTO.retirementDate);
        ps.setObject(++index, employee_recordsDTO.provisionPeriod);
        ps.setObject(++index, employee_recordsDTO.provisionEndDate);
        ps.setObject(++index, employee_recordsDTO.employmentStatus);
        ps.setObject(++index, employee_recordsDTO.employmentStatusChangeDate);
        ps.setObject(++index, employee_recordsDTO.employeeNumber);
        ps.setObject(++index, employee_recordsDTO.employeeNumberBng);
        ps.setObject(++index, employee_recordsDTO.mpElectedCount);

        ps.setObject(++index, employee_recordsDTO.currentOffice);
        ps.setObject(++index, employee_recordsDTO.currentDesignation);
        ps.setObject(++index, employee_recordsDTO.otherOfficeDeptType);
        ps.setObject(++index, employee_recordsDTO.payScale);

        ps.setObject(++index, lastModificationTime);
        ps.setObject(++index, buildSearchColumn(employee_recordsDTO));
        ps.setObject(++index, employee_recordsDTO.iD);
    }

    private void deActiveUser(String userName, Connection connection, long lastModificationTime) throws Exception {
        AtomicReference<Exception> atomicReference = new AtomicReference();
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                ps.setObject(1, lastModificationTime);
                ps.setObject(2, userName);
                logger.debug(ps);
                ps.executeUpdate();
                recordUpdateTime(connection, "users", lastModificationTime);
            } catch (SQLException ex) {
                ex.printStackTrace();
                atomicReference.set(ex);
            }
        }, connection, deActiveUser);
        if (atomicReference.get() != null) {
            throw atomicReference.get();
        }
    }

    public void massUpdate() throws Exception {
        logger.debug("-----------------Started mass updating er--------------------");
        List<Employee_recordsDTO> erDTOs = getAllEmployee_records(true);
        for (Employee_recordsDTO employee_recordsDTO : erDTOs) {

            employee_recordsDTO.employeeNumberBng = StringUtils.convertToBanNumber(employee_recordsDTO.employeeNumber);

            long lastModificationTime = System.currentTimeMillis() + 180000; // add 3 minutes delay

            Connection connection = ConnectionAndStatementUtil.getConnection(ConnectionType.WRITE);
            connection.setAutoCommit(false);

            ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
                try {
                    setForSearchColumnAndEmpBn(employee_recordsDTO, ps, lastModificationTime);
                    ps.executeUpdate();
                    recordUpdateTime(connection, "employee_records", lastModificationTime);
                    connection.commit();
                    connection.setAutoCommit(true);

                } catch (Exception ex) {
                    ex.printStackTrace();
                    try {
                        connection.rollback();
                        connection.setAutoCommit(true);
                    } catch (SQLException ex2) {
                        ex2.printStackTrace();
                    }
                }
                buildSearchColumn(employee_recordsDTO);
            }, connection, updateSearchColumnAndEmpBn);

            ConnectionAndStatementUtil.closeConnection(connection, ConnectionType.WRITE);
        }
    }

    private long createUser(Employee_recordsDTO employee_recordsDTO, long lastModificationTime, Connection connection, String userName, String hashPassword, String oldUserName) throws Exception {
        AtomicReference<Exception> atomicReference = new AtomicReference();
        Long userId = (Long) ConnectionAndStatementUtil.getWritePrepareStatement(ps2 -> {
            int k = 0;
            try {
                long id = DBMW.getInstance().getNextSequenceId("users");
                ps2.setLong(++k, id);
                ps2.setString(++k, userName);
                ps2.setString(++k, hashPassword);
                ps2.setString(++k, userName);
                ps2.setLong(++k, employee_recordsDTO.iD);
                ps2.setInt(++k, 6);
                ps2.setInt(++k, 1);
                ps2.setInt(++k, 9601);
                ps2.setInt(++k, 2);
                ps2.setInt(++k, 2);
                ps2.setString(++k, employee_recordsDTO.nameEng);
                ps2.setString(++k, employee_recordsDTO.personalMobile);
                ps2.setLong(++k, lastModificationTime);
                ps2.setInt(++k, 0);
                ps2.setString(++k, oldUserName);
                logger.debug(ps2);
                ps2.execute();
                if (oldUserName == null) { //need to update during only new Employee creation
                    recordUpdateTime(connection, "users", lastModificationTime);
                }
                return id;
            } catch (Exception ex) {
                ex.printStackTrace();
                atomicReference.set(ex);
                return -1L;
            }
        }, connection, addUserQuery);
        if (atomicReference.get() != null) {
            throw atomicReference.get();
        }
        return userId;
    }

    public void updateEmployee_records(Employee_recordsDTO employee_recordsDTO, Election_wise_mpDTO electionWiseMpDTO) throws Exception {
        Employee_recordsDTO oldEmployeeRecordsDTO = new Employee_recordsDAO().getEmployee_recordsDTOByID(employee_recordsDTO.iD);
        long lastModificationTime = System.currentTimeMillis() + 180000; // add 3 minutes delay
        Connection connection = ConnectionAndStatementUtil.getConnection(ConnectionType.WRITE);
        employee_recordsDTO.lastModificationTime = lastModificationTime;
        connection.setAutoCommit(false);
        if (employee_recordsDTO.employmentStatus != EmploymentStatusEnum.ACTIVE.getValue()) {
            try {
                EmployeeOfficesDAO.getInstance().deactivateStatus(employee_recordsDTO.iD, employee_recordsDTO.employmentStatusChangeDate, lastModificationTime, connection);
                employee_recordsDTO.status = false;
            } catch (Exception ex) {
                ex.printStackTrace();
                connection.rollback();
                connection.setAutoCommit(true);
                ConnectionAndStatementUtil.closeConnection(connection, ConnectionType.WRITE);
                throw ex;
            }
        }
        LongWrapper electionWiseMpId = new LongWrapper(-1L);
        LongWrapper userId1 = new LongWrapper(-1L);
        LongWrapper userId2 = new LongWrapper(-1L);
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                set(employee_recordsDTO, ps, false, lastModificationTime);
                ps.executeUpdate();
                recordUpdateTime(connection, "employee_records", lastModificationTime);
                if (electionWiseMpDTO != null) {
                    electionWiseMpDTO.lastModificationTime = lastModificationTime;
                    if (electionWiseMpDTO.isAdd) {
                        Election_wise_mpDAO.getInstance().add(electionWiseMpDTO, connection);
                    } else {
                        Election_wise_mpDAO.getInstance().update(electionWiseMpDTO, connection);
                    }
                    electionWiseMpId.setValue(electionWiseMpDTO.iD);
                }
                String hashPassword;
                if (!oldEmployeeRecordsDTO.employeeNumber.equals(employee_recordsDTO.employeeNumber)) {
                    UserDTO userDTO = UserRepository.getInstance().getUserDtoByEmployeeUserName(oldEmployeeRecordsDTO.employeeNumber);
                    if (userDTO == null) {
                        employee_recordsDTO.password = Utils.randomNumberGenerator(8);
                        hashPassword = PasswordUtil.getInstance().encrypt(employee_recordsDTO.password);
                        long userId = createUser(employee_recordsDTO, lastModificationTime, connection, employee_recordsDTO.employeeNumber, hashPassword, null);
                        userId2.setValue(userId);
                    } else {
                        if (!employee_recordsDTO.password.isEmpty()) {
                            hashPassword = PasswordUtil.getInstance().encrypt(employee_recordsDTO.password);
                        } else {
                            hashPassword = userDTO.password;
                        }
                        long userId = createUser(employee_recordsDTO, lastModificationTime, connection, employee_recordsDTO.employeeNumber, hashPassword, userDTO.userName);
                        userId2.setValue(userId);
                        deActiveUser(userDTO.userName, connection, lastModificationTime);
                        userId1.setValue(userDTO.ID);
                    }
                } else {
                    if (employee_recordsDTO.password != null && !employee_recordsDTO.password.isEmpty()) {
                        updateUserPassword(employee_recordsDTO.employeeNumber, employee_recordsDTO.password, connection);
                    }
                    UserDTO userDTO = UserRepository.getInstance().getUserDtoByEmployeeUserName(employee_recordsDTO.employeeNumber);
                    userDTO.phoneNo = employee_recordsDTO.personalMobile;
                    userDTO.mailAddress = employee_recordsDTO.personalEml;
                    new UserDAO().updateContactInfo(userDTO, lastModificationTime);
                    userId1.setValue(userDTO.ID);
                }
                connection.commit();
                connection.setAutoCommit(true);
            } catch (Exception ex) {
                ex.printStackTrace();
                try {
                    connection.rollback();
                    connection.setAutoCommit(true);
                } catch (SQLException ex2) {
                    ex2.printStackTrace();
                }
            }
        }, connection, updateQuery);
        ConnectionAndStatementUtil.closeConnection(connection, ConnectionType.WRITE);
        Employee_recordsRepository.getInstance().updateCache(employee_recordsDTO);
        if (userId1.isValueModified()) {
            UserRepository.getInstance().updateCache(userId1.getValue());
        }
        if (userId2.isValueModified()) {
            UserRepository.getInstance().updateCache(userId2.getValue());
        }
        if (electionWiseMpId.isValueModified()) {
            Election_wise_mpRepository.getInstance().updateCache(electionWiseMpDTO.iD);
        }

        if (!employee_recordsDTO.status) {
            EmployeeOfficeRepository.getInstance().removedFormCacheByEmpIdInSeparateThread(employee_recordsDTO.iD);
        }
    }

    public void updatePersonalInformation(Employee_recordsDTO employee_recordsDTO, Election_wise_mpDTO electionWiseMpDTO) throws Exception {
        long lastModificationTime = System.currentTimeMillis() + 180000; // add 3 minutes delay
        employee_recordsDTO.lastModificationTime = lastModificationTime;
        Connection connection = ConnectionAndStatementUtil.getConnection(ConnectionType.WRITE);
        connection.setAutoCommit(false);
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                setPersonalInfo(employee_recordsDTO, ps, lastModificationTime);
                ps.executeUpdate();
                recordUpdateTime(connection, "employee_records", lastModificationTime);
                UserDTO userDTO = new UserDAO().getUserDTOByEmployeeRecordId(employee_recordsDTO.iD);
                if (userDTO != null) {
                    userDTO.fullName = employee_recordsDTO.nameEng;
                    userDTO.dateOfBirth = employee_recordsDTO.dateOfBirth;
                    userDTO.lastModificationTime = lastModificationTime;
                    new UserDAO().updatePersonalInfo(userDTO, lastModificationTime);
                }
                if (electionWiseMpDTO != null) {
                    electionWiseMpDTO.dateOfBirth = employee_recordsDTO.dateOfBirth;
                    electionWiseMpDTO.lastModificationTime = lastModificationTime;
                    Election_wise_mpDAO.getInstance().updatePersonalInfoMP(electionWiseMpDTO);
                }
                connection.commit();
                connection.setAutoCommit(true);
                if (userDTO != null) {
                    UserRepository.updateCache(userDTO);
                }
                Employee_recordsRepository.getInstance().updateCache(employee_recordsDTO);
                if (electionWiseMpDTO != null) {
                    Election_wise_mpRepository.getInstance().updateCache(electionWiseMpDTO);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                try {
                    connection.rollback();
                    connection.setAutoCommit(true);
                } catch (SQLException ex2) {
                    ex2.printStackTrace();
                }
            }
        }, connection, updatePersonalInfoQuery);

        ConnectionAndStatementUtil.closeConnection(connection, ConnectionType.WRITE);
    }

    public void updateAddressInformation(Employee_recordsDTO employee_recordsDTO, Election_wise_mpDTO electionWiseMpDTO) throws Exception {
        long lastModificationTime = System.currentTimeMillis() + 180000; // add 3 minutes delay
        employee_recordsDTO.lastModificationTime = lastModificationTime;
        Connection connection = ConnectionAndStatementUtil.getConnection(ConnectionType.WRITE);
        connection.setAutoCommit(false);
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                setAddressInfo(employee_recordsDTO, ps, lastModificationTime);
                ps.executeUpdate();
                recordUpdateTime(connection, "employee_records", lastModificationTime);
                if (electionWiseMpDTO != null) {
                    electionWiseMpDTO.presentAddress = employee_recordsDTO.presentAddress;
                    electionWiseMpDTO.permanentAddress = employee_recordsDTO.permanentAddress;
                    electionWiseMpDTO.lastModificationTime = lastModificationTime;
                    Election_wise_mpDAO.getInstance().updateAddressInfoMP(electionWiseMpDTO);
                }
                connection.commit();
                connection.setAutoCommit(true);
                Employee_recordsRepository.getInstance().updateCache(employee_recordsDTO);
                if (electionWiseMpDTO != null) {
                    Election_wise_mpRepository.getInstance().updateCache(electionWiseMpDTO);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                try {
                    connection.rollback();
                    connection.setAutoCommit(true);
                } catch (SQLException ex2) {
                    ex2.printStackTrace();
                }
            }
        }, connection, updateAddressInfoQuery);

        ConnectionAndStatementUtil.closeConnection(connection, ConnectionType.WRITE);
    }


    public void activate(Employee_recordsDTO employee_recordsDTO, boolean updateCache) throws Exception {
        long lastModificationTime = System.currentTimeMillis() + 180000; // add 3 minutes delay
        employee_recordsDTO.lastModificationTime = lastModificationTime;

        Connection connection = ConnectionAndStatementUtil.getConnection(ConnectionType.WRITE);
        connection.setAutoCommit(false);
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {

                UserDTO userDTO = new UserDAO().getUserDTOByEmployeeRecordIdActiveOrNot(employee_recordsDTO.iD, employee_recordsDTO.employeeNumber);
                if (userDTO != null) {
                    activate(employee_recordsDTO, ps, lastModificationTime);
                    logger.info(ps);
                    ps.executeUpdate();
                    recordUpdateTime(connection, "employee_records", lastModificationTime);
                    userDTO.active = employee_recordsDTO.status;
                    userDTO.lastModificationTime = lastModificationTime;
                    new UserDAO().activate(userDTO, lastModificationTime);
                    connection.commit();
                    connection.setAutoCommit(true);
                    if (updateCache) {
                        UserRepository.updateCache(userDTO);
                        Employee_recordsRepository.getInstance().updateCache(employee_recordsDTO);
                    }
                } else {
                    logger.debug("activate error: userDTO not found");
                }
            } catch (Exception ex) {
                logger.error(ex);
                try {
                    connection.rollback();
                    connection.setAutoCommit(true);
                } catch (SQLException ex2) {
                    logger.error(ex2);
                }
            }
        }, connection, activateDeactivateEmployee);

        ConnectionAndStatementUtil.closeConnection(connection, ConnectionType.WRITE);
    }

    public void updateConfidentialInformation(Employee_recordsDTO employee_recordsDTO) throws Exception {
        long lastModificationTime = System.currentTimeMillis() + 180000; // add 3 minutes delay
        employee_recordsDTO.lastModificationTime = lastModificationTime;
        Connection connection = ConnectionAndStatementUtil.getConnection(ConnectionType.WRITE);
        connection.setAutoCommit(false);
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                setConfidentialInfo(employee_recordsDTO, ps, lastModificationTime);
                ps.executeUpdate();
                recordUpdateTime(connection, "employee_records", lastModificationTime);
                UserDTO userDTO = new UserDAO().getUserDTOByEmployeeRecordId(employee_recordsDTO.iD);
                if (userDTO != null) {
                    userDTO.birthRegistrationNo = employee_recordsDTO.bcn;
                    userDTO.lastModificationTime = lastModificationTime;
                    new UserDAO().updateBCNInfo(userDTO, lastModificationTime);
                }
                connection.commit();
                connection.setAutoCommit(true);
                if (userDTO != null) {
                    UserRepository.updateCache(userDTO);
                }
                Employee_recordsRepository.getInstance().updateCache(employee_recordsDTO);
            } catch (Exception ex) {
                ex.printStackTrace();
                try {
                    connection.rollback();
                    connection.setAutoCommit(true);
                } catch (SQLException ex2) {
                    ex2.printStackTrace();
                }
            }
        }, connection, updateConfidentialInfoQuery);

        ConnectionAndStatementUtil.closeConnection(connection, ConnectionType.WRITE);
    }

    public void updateContactInformation(Employee_recordsDTO employee_recordsDTO) throws Exception {
        long lastModificationTime = System.currentTimeMillis() + 180000; // add 3 minutes delay
        employee_recordsDTO.lastModificationTime = lastModificationTime;
        Connection connection = ConnectionAndStatementUtil.getConnection(ConnectionType.WRITE);
        connection.setAutoCommit(false);

        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                setContactInfo(employee_recordsDTO, ps, lastModificationTime);
                ps.executeUpdate();
                recordUpdateTime(connection, "employee_records", lastModificationTime);
                UserDTO userDTO = new UserDAO().getUserDTOByEmployeeRecordId(employee_recordsDTO.iD);
                if (userDTO != null) {
                    userDTO.phoneNo = employee_recordsDTO.personalMobile;
                    userDTO.mailAddress = employee_recordsDTO.personalEml;
                    userDTO.lastModificationTime = lastModificationTime;
                    new UserDAO().updateContactInfo(userDTO, lastModificationTime);
                }
                connection.commit();
                connection.setAutoCommit(true);
                if (userDTO != null) {
                    UserRepository.updateCache(userDTO);
                }
                Employee_recordsRepository.getInstance().updateCache(employee_recordsDTO);
            } catch (Exception ex) {
                ex.printStackTrace();
                try {
                    connection.rollback();
                    connection.setAutoCommit(true);
                } catch (SQLException ex2) {
                    ex2.printStackTrace();
                }
            }
            buildSearchColumn(employee_recordsDTO);
        }, connection, updateContactInfoQuery);

        ConnectionAndStatementUtil.closeConnection(connection, ConnectionType.WRITE);
    }

    static long SPOUSEID = 301;

    void fixFamilyandNominee(Employee_recordsDTO employee_recordsDTO) throws Exception {
        if (employee_recordsDTO.maritalStatus == MarriageEnum.DIVORCED.getValue() || employee_recordsDTO.maritalStatus == MarriageEnum.WIDOWED.getValue()) {
            Set<Long> deletedFamilyInfoId = new HashSet<>();
            List<Employee_family_infoDTO> employee_family_infoDTOList = Employee_family_infoDAO.getInstance().getByEmployeeId(employee_recordsDTO.iD);
            for (Employee_family_infoDTO employee_family_infoDTO : employee_family_infoDTOList) {
                if (employee_family_infoDTO.relationType == SPOUSEID) {
                    employee_family_infoDTO.isDeleted = 2;
                    Employee_family_infoDAO.getInstance().deleteBySpouse(employee_family_infoDTO.iD);
                    deletedFamilyInfoId.add(employee_family_infoDTO.iD);
                }
            }

            List<Employee_nomineeDTO> employee_nomineeDTOList = Employee_nomineeDAO.getInstance().getByEmployeeId(employee_recordsDTO.iD);
            for (Employee_nomineeDTO employee_nomineeDTO : employee_nomineeDTOList) {
                if (deletedFamilyInfoId.contains(employee_nomineeDTO.employeeFamilyInfoId)) {
                    Employee_nomineeDAO.getInstance().deleteNomineeByID(employee_nomineeDTO.iD);
                }
            }
        }
    }


    public void updateMiscellaneousInformation(Employee_recordsDTO employee_recordsDTO) throws Exception {
        long lastModificationTime = System.currentTimeMillis() + 180000; // add 3 minutes delay
        employee_recordsDTO.lastModificationTime = lastModificationTime;
        Utils.handleTransaction(() -> {
            AtomicReference<Exception> atomicReference = new AtomicReference<>();
            Connection connection = CommonDAOService.CONNECTION_THREAD_LOCAL.get();
            ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
                try {
                    setMiscellaneousInfo(employee_recordsDTO, ps, lastModificationTime);
                    ps.executeUpdate();
                    recordUpdateTime(connection, "employee_records", lastModificationTime);
                    Employee_recordsRepository.getInstance().updateCache(employee_recordsDTO);
                    fixFamilyandNominee(employee_recordsDTO);
                } catch (Exception e) {
                    e.printStackTrace();
                    atomicReference.set(e);
                }
            }, connection, updateMiscellaneousInfoQuery);
            if (atomicReference.get() != null) {
                throw atomicReference.get();
            }
        });
    }

    public void updateNIDInformation(Employee_recordsDTO employee_recordsDTO) throws Exception {
        long lastModificationTime = System.currentTimeMillis() + 180000; // add 3 minutes delay
        employee_recordsDTO.lastModificationTime = lastModificationTime;
        Connection connection = ConnectionAndStatementUtil.getConnection(ConnectionType.WRITE);
        connection.setAutoCommit(false);
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                setNIDInfo(employee_recordsDTO, ps, lastModificationTime);
                ps.executeUpdate();
                recordUpdateTime(connection, "employee_records", lastModificationTime);
                UserDTO userDTO = new UserDAO().getUserDTOByEmployeeRecordId(employee_recordsDTO.iD);
                if (userDTO != null) {
                    userDTO.nid = employee_recordsDTO.nid;
                    userDTO.lastModificationTime = lastModificationTime;
                    new UserDAO().updateNIDInfo(userDTO, lastModificationTime);
                }
                connection.commit();
                connection.setAutoCommit(true);
                if (userDTO != null) {
                    UserRepository.updateCache(userDTO);
                }
                Employee_recordsRepository.getInstance().updateCache(employee_recordsDTO);
            } catch (Exception ex) {
                ex.printStackTrace();
                try {
                    connection.rollback();
                    connection.setAutoCommit(true);
                } catch (SQLException ex2) {
                    ex2.printStackTrace();
                }
            }
            buildSearchColumn(employee_recordsDTO);
        }, connection, updateNIDInfoQuery);

        ConnectionAndStatementUtil.closeConnection(connection, ConnectionType.WRITE);
    }

    public void updateLPRInformation(Employee_recordsDTO employee_recordsDTO) throws Exception {
        long lastModificationTime = System.currentTimeMillis() + 180000; // add 3 minutes delay
        employee_recordsDTO.lastModificationTime = lastModificationTime;
        Connection connection = ConnectionAndStatementUtil.getConnection(ConnectionType.WRITE);
        connection.setAutoCommit(false);
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                setLPRInfo(employee_recordsDTO, ps, lastModificationTime);
                ps.executeUpdate();
                recordUpdateTime(connection, "employee_records", lastModificationTime);
                connection.commit();
                connection.setAutoCommit(true);
                Employee_recordsRepository.getInstance().updateCache(employee_recordsDTO);
            } catch (Exception ex) {
                ex.printStackTrace();
                try {
                    connection.rollback();
                    connection.setAutoCommit(true);
                } catch (SQLException ex2) {
                    ex2.printStackTrace();
                }
            }
        }, connection, updateLPRInfoQuery);

        ConnectionAndStatementUtil.closeConnection(connection, ConnectionType.WRITE);
    }

    public void updateImageInformation(Employee_recordsDTO employee_recordsDTO) throws Exception {
        long lastModificationTime = System.currentTimeMillis() + 180000; // add 3 minutes delay
        employee_recordsDTO.lastModificationTime = lastModificationTime;
        Connection connection = ConnectionAndStatementUtil.getConnection(ConnectionType.WRITE);
        connection.setAutoCommit(false);
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                setImageInfo(employee_recordsDTO, ps, lastModificationTime);
                ps.executeUpdate();
                recordUpdateTime(connection, "employee_records", lastModificationTime);
                connection.commit();
                connection.setAutoCommit(true);
                Employee_recordsRepository.getInstance().updateCache(employee_recordsDTO);
            } catch (Exception ex) {
                ex.printStackTrace();
                try {
                    connection.rollback();
                    connection.setAutoCommit(true);
                } catch (SQLException ex2) {
                    ex2.printStackTrace();
                }
            }
        }, connection, updateImageInfoQuery);

        ConnectionAndStatementUtil.closeConnection(connection, ConnectionType.WRITE);
    }

    public void updatePasswordInformation(Employee_recordsDTO employee_recordsDTO) throws Exception {
        long lastModificationTime = System.currentTimeMillis() + 180000; // add 3 minutes delay
        Connection connection = ConnectionAndStatementUtil.getConnection(ConnectionType.WRITE);
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                String encryptedPassword = PasswordUtil.getInstance().encrypt(employee_recordsDTO.password);
                ps.setObject(1, encryptedPassword);
                ps.setObject(2, lastModificationTime);
                ps.setObject(3, employee_recordsDTO.employeeNumber);
                logger.debug(ps);
                connection.setAutoCommit(false);
                ps.executeUpdate();
                recordUpdateTime(connection, "users", lastModificationTime);
                connection.commit();
                connection.setAutoCommit(true);
                UserRepository.cacheUpdateByUsername(employee_recordsDTO.employeeNumber);
            } catch (Exception ex) {
                ex.printStackTrace();
                try {
                    connection.rollback();
                    connection.setAutoCommit(true);
                } catch (SQLException ex2) {
                    ex2.printStackTrace();
                }
            }
        }, connection, updatePasswordQuery);

        ConnectionAndStatementUtil.closeConnection(connection, ConnectionType.WRITE);
    }

    public void updateEmploymentInformation(Employee_recordsDTO employee_recordsDTO, Election_wise_mpDTO electionWiseMpDTO) throws Exception {

        Employee_recordsDTO oldEmployeeRecordsDTO = new Employee_recordsDAO().getEmployee_recordsDTOByID(employee_recordsDTO.iD);
        long lastModificationTime = System.currentTimeMillis() + 180000; // add 3 minutes delay
        employee_recordsDTO.lastModificationTime = lastModificationTime;
        Connection connection = ConnectionAndStatementUtil.getConnection(ConnectionType.WRITE);
        connection.setAutoCommit(false);
        if (employee_recordsDTO.employmentStatus != EmploymentStatusEnum.ACTIVE.getValue()) {
            try {
                EmployeeOfficesDAO.getInstance().deactivateStatus(employee_recordsDTO.iD, employee_recordsDTO.employmentStatusChangeDate, lastModificationTime, connection);
                employee_recordsDTO.status = false;
            } catch (Exception ex) {
                ex.printStackTrace();
                connection.rollback();
                connection.setAutoCommit(true);
                ConnectionAndStatementUtil.closeConnection(connection, ConnectionType.WRITE);
                throw ex;
            }
        }
        LongWrapper user1 = new LongWrapper(-1L);
        LongWrapper user2 = new LongWrapper(-1L);
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                setEmploymentInfo(employee_recordsDTO, ps, lastModificationTime);
                ps.executeUpdate();
                recordUpdateTime(connection, "employee_records", lastModificationTime);
                String hashPassword;
                if (!oldEmployeeRecordsDTO.employeeNumber.equals(employee_recordsDTO.employeeNumber)) {
                    UserDTO userDTO = UserRepository.getUserDTOByUserNameFromUsersTable(oldEmployeeRecordsDTO.employeeNumber);
                    if (userDTO == null) {
                        employee_recordsDTO.password = Utils.randomNumberGenerator(8);
                        hashPassword = PasswordUtil.getInstance().encrypt(employee_recordsDTO.password);
                        long userId = createUser(employee_recordsDTO, lastModificationTime, connection, employee_recordsDTO.employeeNumber, hashPassword, null);
                        user2.setValue(userId);
                    } else {
                        if (!employee_recordsDTO.password.isEmpty()) {
                            hashPassword = PasswordUtil.getInstance().encrypt(employee_recordsDTO.password);
                        } else {
                            hashPassword = userDTO.password;
                        }
                        long userId = createUser(employee_recordsDTO, lastModificationTime, connection, employee_recordsDTO.employeeNumber, hashPassword, userDTO.userName);
                        user2.setValue(userId);
                        deActiveUser(userDTO.userName, connection, lastModificationTime);
                        user1.setValue(userDTO.ID);
                    }
                }

                if (electionWiseMpDTO != null) {
                    electionWiseMpDTO.lastModificationTime = lastModificationTime;
                    Election_wise_mpDAO.getInstance().update(electionWiseMpDTO);
                }
                connection.commit();
                connection.setAutoCommit(true);
            } catch (Exception ex) {
                ex.printStackTrace();
                try {
                    connection.rollback();
                    connection.setAutoCommit(true);
                } catch (SQLException ex2) {
                    ex2.printStackTrace();
                }
            }
            buildSearchColumn(employee_recordsDTO);
        }, connection, updateEmploymentInfoQuery);
        ConnectionAndStatementUtil.closeConnection(connection, ConnectionType.WRITE);
        Employee_recordsRepository.getInstance().updateCache(employee_recordsDTO);
        if (electionWiseMpDTO != null) {
            Election_wise_mpRepository.getInstance().updateCache(electionWiseMpDTO);
        }
        if (user1.isValueModified()) {
            UserRepository.getInstance().updateCache(user1.getValue());
        }
        if (user2.isValueModified()) {
            UserRepository.getInstance().updateCache(user2.getValue());
        }

        if (!employee_recordsDTO.status) {
            EmployeeOfficeRepository.getInstance().removedFormCacheByEmpIdInSeparateThread(employee_recordsDTO.iD);
        }
    }

    private Long buildIdList(ResultSet rs) {
        try {
            return rs.getLong("id");
        } catch (SQLException ex) {
            logger.error(ex);
            ex.printStackTrace();
        }
        return null;
    }

    public Employee_recordsDTO buildEmployeeRecordsDTO(ResultSet rs) {
        try {
            Employee_recordsDTO employee_recordsDTO = new Employee_recordsDTO();
            employee_recordsDTO.iD = rs.getLong("ID");
            employee_recordsDTO.nameEng = rs.getString("name_eng");
            employee_recordsDTO.nameBng = rs.getString("name_bng");
            employee_recordsDTO.fatherNameEng = rs.getString("father_name_eng");
            employee_recordsDTO.fatherNameBng = rs.getString("father_name_bng");
            employee_recordsDTO.motherNameEng = rs.getString("mother_name_eng");
            employee_recordsDTO.motherNameBng = rs.getString("mother_name_bng");
            employee_recordsDTO.dateOfBirth = rs.getLong("date_of_birth");
            employee_recordsDTO.presentAddress = rs.getString("present_address");
            employee_recordsDTO.permanentAddress = rs.getString("permanent_address");
            employee_recordsDTO.presentAddressBng = rs.getString("present_address_bng");
            employee_recordsDTO.permanentAddressBng = rs.getString("permanent_address_bng");
            employee_recordsDTO.nid = rs.getString("nid");
            employee_recordsDTO.bcn = rs.getString("bcn");
            employee_recordsDTO.gender = rs.getInt("gender_cat");
            employee_recordsDTO.religion = rs.getLong("religion");
            employee_recordsDTO.bloodGroup = rs.getInt("blood_group");
            employee_recordsDTO.maritalStatus = rs.getInt("marital_status");
            employee_recordsDTO.personalEml = rs.getString("personal_email");
            employee_recordsDTO.personalMobile = rs.getString("personal_mobile");
            employee_recordsDTO.alternativeMobile = rs.getString("alternative_mobile");
            employee_recordsDTO.officePhoneNumber = rs.getString("office_phone_number");
            employee_recordsDTO.officePhoneExtension = rs.getString("office_phone_extension");
            employee_recordsDTO.faxNumber = rs.getString("fax_number");
            employee_recordsDTO.birthDistrict = rs.getLong("birth_district");
            employee_recordsDTO.homeDistrict = rs.getInt("home_district");
            employee_recordsDTO.passportNo = rs.getString("passport_no");
            employee_recordsDTO.passportIssueDate = rs.getLong("passport_issue_date");
            employee_recordsDTO.passportExpiryDate = rs.getLong("passport_expiry_date");
            employee_recordsDTO.marriageDate = rs.getLong("marriage_date");
            employee_recordsDTO.nationalityType = rs.getLong("nationality_type");
            employee_recordsDTO.identificationMark = rs.getString("identification_mark");
            employee_recordsDTO.height = rs.getDouble("height");
            employee_recordsDTO.joiningDate = rs.getLong("joining_date");
            employee_recordsDTO.status = rs.getBoolean("status");
            employee_recordsDTO.createdBy = rs.getInt("created_by");
            employee_recordsDTO.modifiedBy = rs.getInt("modified_by");
            employee_recordsDTO.created = rs.getLong("created");
            employee_recordsDTO.filesDropzone = rs.getLong("filesDropzone");
            employee_recordsDTO.isDeleted = rs.getBoolean("isDeleted");
            employee_recordsDTO.lastModificationTime = rs.getLong("lastModificationTime");
            employee_recordsDTO.isMP = rs.getInt("is_mp");
            employee_recordsDTO.employeeNumber = rs.getString("employee_number");
            employee_recordsDTO.freedomFighterInfo = rs.getString("freedom_fighter_info");
            employee_recordsDTO.photo = Utils.getByteArrayFromInputStream(rs.getBinaryStream("photo"));
            employee_recordsDTO.signature = Utils.getByteArrayFromInputStream(rs.getBinaryStream("signature"));
            employee_recordsDTO.nidFrontPhoto = Utils.getByteArrayFromInputStream(rs.getBinaryStream("nid_front_photo"));
            employee_recordsDTO.nidBackPhoto = Utils.getByteArrayFromInputStream(rs.getBinaryStream("nid_back_photo"));
            employee_recordsDTO.employmentType = rs.getInt("employment_cat");
            employee_recordsDTO.employeeClass = rs.getInt("employee_class_cat");
            employee_recordsDTO.officerTypeCat = rs.getInt("emp_officer_cat");
            employee_recordsDTO.currentOffice = rs.getLong("current_office");
            employee_recordsDTO.currentDesignation = rs.getString("current_designation");
            employee_recordsDTO.otherOfficeDeptType = rs.getLong("other_office_department_type");
            employee_recordsDTO.lprDate = rs.getLong("lpr_date");
            employee_recordsDTO.retirementDate = rs.getLong("retirement_date");
            employee_recordsDTO.presentDivisionId = rs.getLong("present_division");
            employee_recordsDTO.presentDistrictId = rs.getLong("present_district");
            employee_recordsDTO.permanentDivisionId = rs.getLong("permanent_division");
            employee_recordsDTO.permanentDistrictId = rs.getLong("permanent_district");
            employee_recordsDTO.provisionPeriod = rs.getLong("provision_period");
            employee_recordsDTO.provisionEndDate = rs.getLong("provision_end_date");
            employee_recordsDTO.pensionRemarks = rs.getString("pension_remarks");
            employee_recordsDTO.officialEml = rs.getString("official_email");
            employee_recordsDTO.mpElectedCount = rs.getInt("mp_elected_count");
            employee_recordsDTO.electionWiseMpId = rs.getLong("election_wise_mp_id");
            employee_recordsDTO.employmentStatus = rs.getInt("employment_status");
            employee_recordsDTO.employmentStatusChangeDate = rs.getLong("employment_status_change_date");
            employee_recordsDTO.jobQuota = rs.getInt("job_quota");
            employee_recordsDTO.quotaFile = rs.getInt("quota_file");
            employee_recordsDTO.payScale = rs.getLong("pay_scale");
            employee_recordsDTO.sameAsPermanent = rs.getBoolean("same_as_permanent");
            if (employee_recordsDTO.isMP == 1 && employee_recordsDTO.employeeNumber.length() > 5) {
                int parliament = Integer.parseInt(employee_recordsDTO.employeeNumber.substring(1, 3));
                int constituency = Integer.parseInt(employee_recordsDTO.employeeNumber.substring(3, 6));
                Election_detailsDTO electionDetailsDTO = Election_detailsRepository.getInstance().getElection_detailsDTOByParliamentNumber(parliament);
                if (electionDetailsDTO != null) {
                    Election_constituencyDTO electionConstituencyDTO = Election_constituencyRepository.getInstance().getElection_constituencyDTOByConstituencyNumber(electionDetailsDTO.iD, constituency);
                    if (electionConstituencyDTO != null) {
                        Election_wise_mpDTO electionWiseMpDTO = Election_wise_mpRepository.getInstance().getByElectionAndConstituency(electionDetailsDTO.iD, electionConstituencyDTO.iD);
                        if (electionWiseMpDTO != null) {
                            employee_recordsDTO.parliamentNumberId = electionWiseMpDTO.electionDetailsId;
                            employee_recordsDTO.electionConstituencyId = electionWiseMpDTO.electionConstituencyId;
                            employee_recordsDTO.politicalPartyId = electionWiseMpDTO.politicalPartyId;
                            employee_recordsDTO.professionOfMP = electionWiseMpDTO.professionOfMP;
                            employee_recordsDTO.addressOfMP = electionWiseMpDTO.addressOfMP;
                        }
                    }
                }
            }
            return employee_recordsDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    private byte[] getByteArrayFromInputStream(InputStream is) {
        if (is != null) {
            try {
                return IOUtils.toByteArray(is);
            } catch (IOException e) {
                logger.error(e);
                e.printStackTrace();
            }
        }
        return null;
    }

    public Employee_recordsDTO getEmployee_recordsDTOByID(long ID) {
        return ConnectionAndStatementUtil.getT(getById, ps -> {
            try {
                ps.setLong(1, ID);
            } catch (SQLException ex) {
                logger.error(ex);
                ex.printStackTrace();
            }
        }, this::buildEmployeeRecordsDTO);
    }

    private void updateUserPassword(String userName, String password, Connection connection) throws Exception {
        AtomicReference<Exception> atomicReference = new AtomicReference();
        String hashPassword = PasswordUtil.getInstance().encrypt(password);
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                long currentTime = System.currentTimeMillis();
                ps.setString(1, hashPassword);
                ps.setLong(2, currentTime);
                ps.setString(3, userName);
                logger.debug(ps);
                ps.executeUpdate();
                recordUpdateTime(connection, "users", currentTime);
                UserRepository.cacheUpdateByUsername(userName);
            } catch (SQLException ex) {
                ex.printStackTrace();
                atomicReference.set(ex);
            }
        }, connection, updatePasswordQuery);
        if (atomicReference.get() != null) {
            throw atomicReference.get();
        }
    }

    public void deleteEmployee_recordsByID(long ID) {
        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            Connection connection = model.getConnection();
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            long lastModificationTime = System.currentTimeMillis();
            Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(ID);
            try {
                if (employeeRecordsDTO.isMP == 1) {
                    connection.setAutoCommit(false);
                }
                ps.setLong(1, lastModificationTime);
                ps.setLong(2, ID);
                logger.debug(ps);
                ps.executeUpdate();
                if (employeeRecordsDTO.isMP == 1) {
                    AtomicBoolean occurException = new AtomicBoolean();
                    occurException.set(false);
                    ConnectionAndStatementUtil.getWritePrepareStatement(ps2 -> {
                        try {
                            ps2.setLong(1, employeeRecordsDTO.electionWiseMpId);
                            ps2.setLong(2, lastModificationTime);
                            logger.debug(ps2);
                            ps2.executeUpdate();
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                            occurException.set(true);
                        }
                    }, connection, deleteMpQuery);
                    if (occurException.get()) {
                        throw new Exception("Exception is occurred");
                    }
                }
                recordUpdateTime(connection, lastModificationTime);
                if (employeeRecordsDTO.isMP == 1) {
                    recordUpdateTime(connection, "election_wise_mp", lastModificationTime);
                }

                connection.setAutoCommit(true);
            } catch (Exception ex) {
                logger.error(ex);
                ex.printStackTrace();
                if (employeeRecordsDTO.isMP == 1) {
                    try {
                        connection.rollback();
                        connection.setAutoCommit(true);
                    } catch (SQLException ex2) {
                        ex2.printStackTrace();
                    }
                }
            }
        }, deleteQuery);
    }

    public List<Employee_recordsDTO> getDTOs(Set<Long> recordIDs) {
        String ids = recordIDs.stream()
                .map(String::valueOf)
                .collect(Collectors.joining(","));
        String sql = getByIds.replace("{ids}", ids);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildEmployeeRecordsDTO);
    }

    public List<Employee_recordsDTO> getDTOs(List<Long> ids) {
        return ConnectionAndStatementUtil.getDTOListByNumbers(getByIdsNotSort, ids, this::buildEmployeeRecordsDTO);
    }

    public List<Long> getIdsByName(String searchText) {

        String sql = getByMatchingName.replace("{search}", searchText);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildIdList);
    }

    public List<Employee_recordsDTO> getAllEmployee_records(boolean isFirstReload) {
        String sql;
        if (isFirstReload) {
            sql = getByFirstLoadCondition.replace("{firstLoadCondition}", " isDeleted =  0");
        } else {
            sql = getByFirstLoadCondition.replace("{firstLoadCondition}", " lastModificationTime >= " + RepositoryManager.lastModifyTime);
        }
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildEmployeeRecordsDTO);
    }

    public List<Employee_recordsDTO> getAllEmployee_records(boolean isFirstReload, long lastModifyTime) {
        String sql;
        if (isFirstReload) {
            sql = getByFirstLoadCondition.replace("{firstLoadCondition}", " isDeleted =  0");
        } else {
            sql = getByFirstLoadCondition.replace("{firstLoadCondition}", " lastModificationTime >= " + lastModifyTime);
        }
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildEmployeeRecordsDTO);
    }

    public List<Employee_recordsDTO> getAllLPREmployees(long lprTime) {
        String sql = "select * from employee_records where isDeleted = 0 and"
                + " lpr_date > " + SessionConstants.MIN_DATE
                + " and lpr_date <= " + lprTime
                + " and retirement_date > " + lprTime
                + " and status = 1 and employment_status = 1";
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildEmployeeRecordsDTO);
    }

    public List<Employee_recordsDTO> getAllRetireEmployees(long retirement_date) {
        String sql = "select * from employee_records where isDeleted = 0 and"
                + " retirement_date > " + SessionConstants.MIN_DATE
                + " and retirement_date <= " + retirement_date
                + " and status = 1"
                + " and employment_status in (1,7)";
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildEmployeeRecordsDTO);
    }


    public List<Employee_recordsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset) {
        String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildEmployeeRecordsDTO);
    }

    public Collection getIDs() {
        String sql = getSqlWithSearchCriteria(null, -1, -1, GETIDS);
        return ConnectionAndStatementUtil.getListOfT(sql, rs -> {
            try {
                return rs.getString("ID");
            } catch (SQLException ex) {
                logger.error(ex);
                ex.printStackTrace();
                return null;
            }
        });
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category) {
        if (this.pensionStartYear != 0) {
            return generateQueryForPension(limit, offset, category);
        } else {
            return generateQueryForId(limit, offset, category);
        }
    }

    public String generateQueryForId(int limit, int offset, int category) {
        StringBuilder stringBuilder = new StringBuilder("SELECT ");

        switch (category) {
            case GETIDS:
                stringBuilder.append(" employee_records.id as ID ");
                break;
            case GETCOUNT:
                stringBuilder.append(" count(employee_records.id) as countID ");
                break;
            case GETDTOS:
                stringBuilder.append(" employee_records.* ");
                break;
        }

        if (ministry_id > 0 || layer_id > 0 || origin_id > 0 || office_id > 0) {
            stringBuilder.append(" FROM employee_records, employee_offices, offices ");
            stringBuilder.append(" WHERE employee_records.id = employee_offices.employee_record_id ");
            stringBuilder.append(" and offices.id = employee_offices.office_id ");
            stringBuilder.append(" and employee_records.isDeleted = 0 ");

            if (ministry_id > 0) stringBuilder.append(" and offices.office_ministry_id=").append(ministry_id);
            if (layer_id > 0) stringBuilder.append(" and offices.office_layer_id=").append(layer_id);
            if (origin_id > 0) stringBuilder.append(" and offices.office_origin_id=").append(origin_id);
            if (office_id > 0) stringBuilder.append(" and offices.id=").append(office_id);

        } else {
            stringBuilder.append(" FROM employee_records WHERE employee_records.isDeleted = 0 ");
        }

        if (name_en != null && name_en.length() > 0)
            stringBuilder.append(" and employee_records.name_eng like '%").append(name_en).append("%' ");
        if (name_bn != null && name_bn.length() > 0)
            stringBuilder.append(" and employee_records.name_bng like '%").append(name_bn).append("%' ");
        if (nid != null && nid.length() > 0)
            stringBuilder.append(" and employee_records.nid like '%").append(nid).append("%' ");
        if (email != null && email.length() > 0)
            stringBuilder.append(" and employee_records.personal_email like '%").append(email).append("%' ");
        if (phone0 != null && phone0.length() > 0)
            stringBuilder.append(" and employee_records.personal_mobile like '%").append(phone0).append("%' ");
        if (user_name != null && user_name.length() > 0)
            stringBuilder.append(" and employee_records.employee_number like '%").append(user_name).append("%' ");

        stringBuilder.append(" order by employee_records.id desc ");

        if (limit >= 0) {
            stringBuilder.append(" limit ").append(limit);
        }
        if (offset >= 0) {
            stringBuilder.append(" offset ").append(offset);
        }
        return stringBuilder.toString();
    }

    public String generateQueryForPension(int limit, int offset, int category) {

        StringBuilder stringBuilder = new StringBuilder("SELECT ");

        switch (category) {
            case GETIDS:
                stringBuilder.append(" employee_records.id as ID ");
                break;
            case GETCOUNT:
                stringBuilder.append(" count(employee_records.id) as countID ");
                break;
            case GETDTOS:
                stringBuilder.append(" employee_records.* ");
                break;
        }

        long fiscalYearStartDate, fiscalYearEndDate;
        Calendar calendar = Calendar.getInstance();
        stringBuilder.append(" FROM employee_records WHERE employee_records.isDeleted = 0 ");


        if (this.pensionStartYear == -1) {
            if (calendar.get(Calendar.MONTH) > Calendar.JUNE) {
                calendar.set(calendar.get(Calendar.YEAR), Calendar.JULY, 1, 0, 0);
            } else {
                calendar.set(calendar.get(Calendar.YEAR) - 1, Calendar.JULY, 1, 0, 0);
            }
        } else {
            calendar.set(this.pensionStartYear, Calendar.JULY, 1, 0, 0);
        }

        fiscalYearStartDate = calendar.getTimeInMillis();
        calendar.add(Calendar.YEAR, 1);
        fiscalYearEndDate = calendar.getTimeInMillis();

        stringBuilder.append(" and employee_records.retirement_date > ").append(fiscalYearStartDate).append(" and employee_records.retirement_date < ").append(fiscalYearEndDate);

        stringBuilder.append(" order by employee_records.retirement_date asc ");

        if (limit >= 0) {
            stringBuilder.append(" limit ").append(limit);
        }
        if (offset >= 0) {
            stringBuilder.append(" offset ").append(offset);
        }

        return stringBuilder.toString();
    }

    public ArrayList<Map<Object, Object>> getCadre() {
        return (ArrayList<Map<Object, Object>>) ConnectionAndStatementUtil.getReadStatement(st -> {
            ArrayList<Map<Object, Object>> result = new ArrayList<>();
            try {
                logger.debug(getCadresSqlQuery);
                ResultSet rs = st.executeQuery(getCadresSqlQuery);
                while (rs.next()) {
                    Map<Object, Object> a = new HashMap<>();
                    a.put("id", rs.getInt("id"));
                    a.put("cadre_name_eng", rs.getString("cadre_name_eng"));
                    a.put("cadre_name_bng", rs.getString("cadre_name_bng"));
                    result.add(a);
                }
            } catch (SQLException e) {
                logger.error(e);
                e.printStackTrace();
            }
            return result;
        });
    }

    public ArrayList<Map<Object, Object>> getBatch() {
        return (ArrayList<Map<Object, Object>>) ConnectionAndStatementUtil.getReadStatement(st -> {
            ArrayList<Map<Object, Object>> result = new ArrayList<>();
            try {
                logger.debug(getBatchSqlQuery);
                ResultSet rs = st.executeQuery(getBatchSqlQuery);
                while (rs.next()) {
                    Map<Object, Object> a = new HashMap<>();
                    a.put("id", rs.getInt("id"));
                    a.put("batch_no", rs.getString("batch_no"));
                    result.add(a);
                }
            } catch (SQLException e) {
                logger.error(e);
                e.printStackTrace();
            }
            return result;
        });
    }

    public static String getEmployeeDOBbyId(long id, String language) {
        Employee_recordsDTO dto = Employee_recordsRepository.getInstance().getById(id);
        return dto == null ? "" : dto.dateOfBirth <= SessionConstants.MIN_DATE ? "" : language.equals("English") ? simpleDateFormat.format(new Date(dto.dateOfBirth)) : StringUtils.convertToBanNumber(simpleDateFormat.format(new Date(dto.dateOfBirth)));
    }

    public static String getEmployeeEngName(long id) {
        Employee_recordsDTO dto = Employee_recordsRepository.getInstance().getById(id);
        return dto == null ? "" : dto.nameEng;
    }

    public static String getEmployeeName(long id, String language) {
        Employee_recordsDTO dto = Employee_recordsRepository.getInstance().getById(id);
        return dto == null ? "" : (language.equals("English") ? dto.nameEng : dto.nameBng);
    }

    public static String getOptions(long id, long selectedId, String language) {
        List<Employee_recordsDTO> records = Employee_recordsRepository.getInstance().getEmployee_recordsList();
        List<OptionDTO> optionDTOList = null;
        if (records != null && records.size() > 0) {
            optionDTOList = records.parallelStream()
                    .filter(e -> e.iD != id)
                    .map(dto -> new OptionDTO(dto.nameEng, dto.nameBng, String.valueOf(dto.iD)))
                    .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, String.valueOf(selectedId));
    }

    public String mpMemberBuildOption(String language, long selectedId) {
        String sql = "SELECT * FROM employee_records where is_mp=1 and isDeleted =  0";
        List<Employee_recordsDTO> list = ConnectionAndStatementUtil.getListOfT(sql, this::buildEmployeeRecordsDTO);
        List<OptionDTO> optionDTOList = null;
        if (list != null && list.size() > 0) {
            optionDTOList = list.stream()
                    .map(dto -> new OptionDTO(dto.nameEng, dto.nameBng, String.valueOf(dto.iD)))
                    .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, String.valueOf(selectedId));
    }

    public String mpBuildOptionByElection(String language, long electionDetailsId) {
        List<Election_wise_mpDTO> mpDTOS = Election_wise_mpRepository.getInstance().getElection_wise_mpDTOByElectionDetailsId(electionDetailsId);
        List<OptionDTO> optionDTOList;

        optionDTOList = mpDTOS.stream()
                .map(dto -> new OptionDTO(getEmployeeName(dto.employeeRecordsId, "English"), getEmployeeName(dto.employeeRecordsId, "Bangla"), String.valueOf(dto.employeeRecordsId)))
                .collect(Collectors.toList());

        return Utils.buildOptions(optionDTOList, language, null);
    }

    public List<String> getAllEmployeeNumbersByPrefix(String idPrefix) {
        String sql = employeeNumberByPrefix.replace("{}", idPrefix);
        logger.debug("sql : " + sql);
        return ConnectionAndStatementUtil.getListOfT(sql, rs -> {
            try {
                return rs.getString("employee_number");
            } catch (SQLException ex) {
                logger.error(ex);
                ex.printStackTrace();
                return null;
            }
        });
    }

    public int getCountOfMpByPrefix(String idPrefix) {
        return getCountByPrefixSQL(countByEmployeeNumberPrefix.replace("{}", idPrefix));
    }

    public int getActiveCountByEmployeeNumberPrefix(String idPrefix) {
        return getCountByPrefixSQL(activeCountByEmployeeNumberPrefix.replace("{}", idPrefix));
    }

    private int getCountByPrefixSQL(String sql) {
        logger.debug("sql : " + sql);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public void updateRemarks(long employeeRecordId, String remarks) {
        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            Connection connection = model.getConnection();
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            long lastModificationTime = System.currentTimeMillis();
            try {
                ps.setString(1, remarks);
                ps.setLong(2, employeeRecordId);
                ps.executeUpdate();
                recordUpdateTime(connection, lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
                ex.printStackTrace();
            }
        }, updatePensionRemarks);
    }

    public List<Long> getAllEmployeeIdByReligion(long religion) {
        String sql = String.format(employeeIdByReligion, religion);
        logger.debug("sql : " + sql);
        return ConnectionAndStatementUtil.getListOfT(sql, rs -> {
            try {
                return rs.getLong("ID");
            } catch (SQLException ex) {
                logger.error(ex);
                ex.printStackTrace();
                return null;
            }
        });
    }

    public Employee_recordsDTO getByEmployeeNumber(String employeeNumber) {
        return ConnectionAndStatementUtil.getT(String.format(getByEmployeeNumber, employeeNumber), this::buildEmployeeRecordsDTO, null);
    }

    private static final Map<String, String> employeeSearchMap = new HashMap<>();

    static {
        employeeSearchMap.put("employee_number", "AND employee_number ={}");
        employeeSearchMap.put("name_eng", "AND name_eng like '%{}%'");
        employeeSearchMap.put("name_bng", "AND name_bng like '%{}%'");
        employeeSearchMap.put("personal_mobile", "AND personal_mobile = '{}'");
    }

    private String getSearchQueryForActiveEmployees(Map<String, String> searchCriteria) {
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("SELECT * FROM employee_records WHERE isDeleted = 0 AND employment_status = ")
                .append(EmploymentStatusEnum.ACTIVE.getValue()).append(" ");
        if (searchCriteria != null) {
            String searchString =
                    searchCriteria.entrySet()
                            .stream()
                            .filter(entry -> entry.getValue() != null && entry.getValue().trim().length() > 0 && Employee_recordsDAO.employeeSearchMap.get(entry.getKey()) != null)
                            .map(entry -> Employee_recordsDAO.employeeSearchMap.get(entry.getKey()).replace("{}", entry.getValue()))
                            .collect(Collectors.joining(" "));
            if (searchString.trim().length() > 0) {
                sqlBuilder.append(searchString);
            }
        }
        return sqlBuilder.toString();
    }

    public List<Employee_recordsDTO> searchActiveEmployees(Map<String, String> searchCriteria) {
        return ConnectionAndStatementUtil.getListOfT(
                getSearchQueryForActiveEmployees(searchCriteria),
                this::buildEmployeeRecordsDTO
        );
    }


    public List<YearMonthCount> getLast6Month(boolean isLangEng, String dc) {
        String sql = "";
        if (dc.equalsIgnoreCase("joining_date")) {
            sql =
                    "SELECT \r\n" +
                            "    DATE_FORMAT(FROM_UNIXTIME(er." + dc + " / 1000),\r\n" +
                            "            '%Y-%m') AS ym,\r\n" +
                            " employment_cat, " +
                            "    COUNT(*)\r\n" +
                            "FROM\r\n" +
                            "    employee_offices eo\r\n" +
                            "        INNER JOIN\r\n" +
                            "    employee_records er ON eo.employee_record_id = er.id\r\n" +
                            "WHERE\r\n" +
                            "    eo.office_id = 2294\r\n" +
                            "        AND er.joining_date >= " + TimeConverter.get1stDayOfNthtMonth(-6) + "\r\n" +
                            "        AND er.joining_date <= " + TimeConverter.get1stDayOfNthtMonth(0) + "\r\n" +
                            "        AND eo.isDeleted = 0\r\n" +
                            "        AND er.is_mp = 2\r\n" +
                            "        AND er.employment_cat = 1\r\n" +
                            "        AND eo.incharge_label IN ('Routine responsibility' , 'Additional responsibility',\r\n" +
                            "        'Attachment responsibility')\r\n" +
                            "        AND er.isDeleted = 0\r\n" +
                            "        AND eo.is_default_role = 1\r\n" +
                            "        AND eo.joining_date > - 62135791200000\r\n" +
                            "        AND eo.status = 1 " +
                            "GROUP BY ym, employment_cat\r\n" +
                            " order by ym asc, employment_cat asc "
            ;

        } else {
            sql =
                    "SELECT \r\n" +
                            "    DATE_FORMAT(FROM_UNIXTIME(`" + dc + "` / 1000),\r\n" +
                            "            '%Y-%m') AS ym,\r\n" +
                            " 0 as employment_cat, " +
                            "    COUNT(*)\r\n" +
                            "FROM\r\n" +
                            "    employee_offices eo\r\n" +
                            "        INNER JOIN\r\n" +
                            "    employee_records er ON eo.employee_record_id = er.id\r\n" +
                            "WHERE\r\n" +
                            "  eo.office_id = 2294\r\n" +
                            "        AND eo.isDeleted = 0\r\n" +
                            "        AND er.isDeleted = 0\r\n" +
                            "        AND er.lpr_date >= " + TimeConverter.get1stDayOfNthtMonth(0) + "\r\n" +
                            "        AND er.lpr_date <= " + TimeConverter.get1stDayOfNthtMonth(5) + "\r\n" +
                            "        AND eo.status = 1\r\n" +
                            "GROUP BY ym\r\n" +
                            " order by ym asc "
            ;

        }


        //System.out.println(sql);
        List<YearMonthCount> counts = ConnectionAndStatementUtil.getListOfT(sql, this::getYmCount);
        String Language = isLangEng ? "english" : "bangla";
        for (YearMonthCount yearMonthCount : counts) {
            yearMonthCount.keyStr2 = CatDAO.getName(Language, "employment", yearMonthCount.key);
            yearMonthCount.monthName = Utils.getMonth(yearMonthCount.month, isLangEng);
        }
        return counts;
    }


    public YearMonthCount getYmCount(ResultSet rs) {
        try {
            YearMonthCount ymCount = new YearMonthCount();
            String ym = rs.getString("ym");
            ymCount.keyStr = ym;
            ymCount.year = Integer.parseInt(ym.split("-")[0]);
            ymCount.month = Integer.parseInt(ym.split("-")[1]);
            ymCount.key = rs.getLong("employment_cat");
            ymCount.count = rs.getInt("count(*)");
            ymCount.startDate = TimeConverter.get1stDayOfYeartMonth(ymCount.year, ymCount.month);
            ymCount.endDate = TimeConverter.getLastDayOfYeartMonth(ymCount.year, ymCount.month);

            //System.out.println("year = " + ymCount.year + " month = " + ymCount.month + " count = " + ymCount.count);

            return ymCount;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}