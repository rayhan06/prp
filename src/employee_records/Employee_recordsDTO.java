package employee_records;


import sessionmanager.SessionConstants;

public class Employee_recordsDTO {
    public long iD = 0;
    public String nameEng = "";
    public String nameBng = "";
    public String fatherNameEng = "";
    public String fatherNameBng = "";
    public String motherNameEng = "";
    public String motherNameBng = "";
    public long dateOfBirth = SessionConstants.MIN_DATE;
    public String nationality = "";
    public String presentAddress = "";
    public String permanentAddress = "";
    public String presentAddressBng = "";
    public String permanentAddressBng = "";
    public String nid = "";
    public String bcn = "";
    public int gender = 0;
    public long religion = -1;
    public int bloodGroup = 0;
    public int employmentType = 0;
    public int maritalStatus = 0;
    public String spouseNameEng = "";
    public String spouseNameBng = "";
    public String personalEml = "";
    public String officialEml = "";
    public String personalMobile = "";
    public String alternativeMobile = "";
    public String officePhoneNumber = "";
    public String officePhoneExtension = "";
    public String faxNumber = "";
    public long joiningDate = SessionConstants.MIN_DATE;
    public boolean status = false;
    public int createdBy = 0;
    public int modifiedBy = 0;
    public long created = 0;
    public long modified = 0;
    public boolean isDeleted = false;
    public long lastModificationTime = 0;
    public long birthDistrict = 0;
    public int homeDistrict = 0;
    public String passportNo = "";
    public String identificationMark = "";
    public double height = 0.0;
    public long passportIssueDate = SessionConstants.MIN_DATE;
    public long marriageDate = SessionConstants.MIN_DATE;
    public long passportExpiryDate = SessionConstants.MIN_DATE;
    public long nationalityType = 0;
    public String password;
    public String confirmPassword;
    public long filesDropzone = -1L;
    public String professionOfMP = "";
    public String addressOfMP = "";
    public String freedomFighterInfo = "";
    public byte[] photo = null;
    public byte[] signature = null;
    public int isMP = 0;
    public String employeeNumber = "";
    public String employeeNumberBng = "";
    public int employeeClass = 0;
    public int officerTypeCat = 0;
    public long currentOffice = -1;
    public String currentDesignation = "";
    public long politicalPartyId = 0;
    public long parliamentNumberId = 0;
    public long electionConstituencyId = 0;
    public long lprDate = SessionConstants.MIN_DATE;
    public long govtJobJoiningDate = SessionConstants.MIN_DATE;
    public long retirementDate = SessionConstants.MIN_DATE;
    public byte[] nidFrontPhoto = null;
    public byte[] nidBackPhoto = null;
    public long presentDivisionId = 0;
    public long presentDistrictId = 0;
    public long permanentDivisionId = 0;
    public long permanentDistrictId = 0;
    public long provisionPeriod = 0;
    public long provisionEndDate = SessionConstants.MIN_DATE;
    public int mpElectedCount = 0;
    public String mpRemarks = "";
    public int mpStatus = 0;
    public long mpStatusChangeDate = SessionConstants.MIN_DATE;
    public String pensionRemarks = "";
    public long electionWiseMpId = 0;
    public int employmentStatus = 1;
    public long employmentStatusChangeDate = SessionConstants.MIN_DATE;
    public int jobQuota = 0;
    public long quotaFile = 0;
    
    public long payScale = -1;
    
    public String deactivationReason = "";
    
    public long otherOfficeDeptType = -1;

    public boolean sameAsPermanent = false;

   
}