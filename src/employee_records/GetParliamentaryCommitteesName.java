package employee_records;

import committees.CommitteesRepository;
import common.ApiResponse;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GetParliamentaryCommitteesName implements EmployeeRecordsApiPerformer{
    public final Logger logger = Logger.getLogger(GetParliamentaryCommitteesName.class);

    @Override
    public ApiResponse perform(HttpServletRequest request) {
        try {
            List<ParliamentaryCommitteesNameModel> parliamentaryCommittees;

            parliamentaryCommittees = CommitteesRepository.getInstance().getNameDTOList().stream().
                    map(ParliamentaryCommitteesNameModel::new).collect(Collectors.toList());


            return parliamentaryCommittees.size()==0 ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                    ApiResponse.makeSuccessResponse(parliamentaryCommittees, "Successfully found the data.");
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
        }

    }
}
