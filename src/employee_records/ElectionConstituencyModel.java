package employee_records;

import election_constituency.Election_constituencyDTO;
import election_details.Election_detailsRepository;

public class ElectionConstituencyModel {
    int constituencyNumber;
    String constituencyNameEng;
    String constituencyNameBng;
    long districtId;
    long divisionId;
    boolean isVacant;
    boolean isReserved;


    public ElectionConstituencyModel(Election_constituencyDTO dto) {
        this.constituencyNumber = dto.constituencyNumber;
        this.constituencyNameEng = dto.constituencyNameEn;
        this.constituencyNameBng = dto.constituencyNameBn;
        this.districtId = dto.geoDistrictsType;
        this.divisionId = dto.geoDivisionsType;
        this.isVacant = dto.isVacant==1;
        this.isReserved = dto.isReserved==1;
    }
}
