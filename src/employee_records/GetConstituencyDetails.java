package employee_records;

import common.ApiResponse;
import common.NameDTO;
import election_constituency.Election_constituencyDTO;
import election_constituency.Election_constituencyRepository;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import religion.ReligionRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

public class GetConstituencyDetails implements EmployeeRecordsApiPerformer{
    @Override
    public ApiResponse perform(HttpServletRequest request) {
        long parliament_number;

        try {
            parliament_number = Long.parseLong(request.getParameter("parliament_number"));
        } catch (NumberFormatException nfe) {
            return ApiResponse.makeResponseToBadRequest("Kindly check the parliament_number parameter provided!");
        }

        List<ElectionConstituencyModel> electionConstituencyModels;
        List<Election_constituencyDTO> electionConstituencyDTOList;

        try {
            electionConstituencyDTOList = Election_constituencyRepository.getInstance().getElection_constituencyList().stream().filter(dto -> dto.electionDetailsType==parliament_number).collect(Collectors.toList());

            try {
                electionConstituencyModels = electionConstituencyDTOList.stream().map(ElectionConstituencyModel::new).collect(Collectors.toList());
            } catch (Exception ex) {
                electionConstituencyModels = new ArrayList<>();
            }

            electionConstituencyModels.sort(Comparator.comparingInt(model -> model.constituencyNumber));

            return electionConstituencyModels.size()==0 ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                    ApiResponse.makeSuccessResponse(electionConstituencyModels, "Successfully found the data.");
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
        }

    }
}

