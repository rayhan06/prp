package employee_records;

import committees_mapping.Committees_mappingDAO;
import committees_mapping.Committees_mappingDTO;
import common.ApiResponse;
import election_constituency.Election_constituencyRepository;
import election_details.Election_detailsDTO;
import election_details.Election_detailsRepository;
import election_wise_mp.Election_wise_mpDTO;
import election_wise_mp.Election_wise_mpRepository;
import ministry_office_mapping.Ministry_office_mappingDAO;
import ministry_office_mapping.Ministry_office_mappingDTO;
import org.apache.log4j.Logger;
import political_party.Political_partyRepository;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class GetMpListByParliamentNumberPerformer implements EmployeeRecordsApiPerformer{
    public final Logger logger = Logger.getLogger(GetMpListByParliamentNumberPerformer.class);

    @Override
    public ApiResponse perform(HttpServletRequest request) {
        long parliament_number;

        try {
            parliament_number = Long.parseLong(request.getParameter("parliament_number"));
        } catch (NumberFormatException nfe) {
            return ApiResponse.makeResponseToBadRequest("Kindly check the parliament_number parameter provided!");
        }

        ApiResponse apiResponse;

        List<MemberOfParliamentMinimumInfoModel> mpModels;

        try {


           Optional<Election_detailsDTO> election_detailsDTO = Election_detailsRepository.getInstance().getElectionDetailsList().stream().
                   filter(dto -> dto.parliamentNumber==parliament_number).findFirst();


            List<Committees_mappingDTO> committees_mappingDTOS = Committees_mappingDAO.getInstance().getAllDTOs(true);
            List<Ministry_office_mappingDTO> ministry_office_mappingDTOS = Ministry_office_mappingDAO.getInstance().getAllDTOs(true);

           mpModels = Election_wise_mpRepository.getInstance().getElection_wise_mpDTOByElectionDetailsId(election_detailsDTO.get().iD).stream().
                   map(dto -> new MemberOfParliamentMinimumInfoModel(dto.employeeRecordsId, election_detailsDTO.get().parliamentNumber, Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(dto.electionConstituencyId).constituencyNumber,
                           Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(dto.electionConstituencyId).constituencyNameEn, Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(dto.electionConstituencyId).constituencyNameBn,
                           Political_partyRepository.getInstance().getPolitical_partyDTOByID(dto.politicalPartyId).nameEn, Political_partyRepository.getInstance().getPolitical_partyDTOByID(dto.politicalPartyId).nameBn, dto.startDate, dto.endDate, committees_mappingDTOS, ministry_office_mappingDTOS, dto.mpStatus)).collect(Collectors.toList());

            apiResponse = mpModels==null ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                    ApiResponse.makeSuccessResponse(mpModels, "Successfully found the entries.");


        } catch (Exception e) {
            e.printStackTrace();
            return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
        }

        return apiResponse;
    }
}
