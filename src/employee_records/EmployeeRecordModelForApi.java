package employee_records;

import election_wise_mp.ElectionWiseMpModelForApi;

import java.util.List;

public class EmployeeRecordModelForApi {
    public  EmployeeBasicInformationModelForApi employeeBasicInformationModel;
    public List<ElectionWiseMpModelForApi> electionDetailModels;

    @Override
    public String toString() {
        return "EmployeeRecordModelForApi{" +
                "dto=" + employeeBasicInformationModel +
                ", electionWiseDeatils='" + electionDetailModels + '\'' +
                '}';
    }
}
