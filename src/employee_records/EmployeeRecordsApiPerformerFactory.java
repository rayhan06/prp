package employee_records;

import api_authentication_and_log.GetAuthenticated;
import geolocation.GetDistrictTable;
import geolocation.GetDivisionTable;

public class EmployeeRecordsApiPerformerFactory {
    public static final String GET_AUTHENTICATED = "getAuthenticated";
    private static final String GET_EMPLOYEE_RECORD_BY_ID_REQUEST = "getEmployeeRecordById";
    private static final String DELETE_EMPLOYEE_RECORD_BY_ID_REQUEST = "deleteEmployeeRecordById";
    private static final String GET_ALL_EMPLOYEE_RECORD_PAGE_BY_PAGE = "getEmployeeList";
    private static final String GET_MP_LIST_BY_CONSTITUENCY_NUMBER = "getMpListByConstituencyNumber";
    private static final String GET_MP_LIST_BY_PARLIAMENT_NUMBER = "getMpList";
    private static final String GET_PARLIAMENT_DETAILS = "getParliamentDetails";
    private static final String GET_POLITICAL_PARTY_DETAILS = "getPoliticalPartyDetails";
    private static final String GET_PARLIAMENT_SESSION = "getParliamentSession";
    private static final String GET_FAMILY_INFO = "getFamilyInfo";
    private static final String GET_EDUCATION_INFO = "getEducationInfo";
    private static final String GET_ORAGANOGRAM_TREE = "getOrganogramTree";
    private static final String GET_LANGUAGE_INFO = "getLanguageInfo";
    private static final String GET_TRAVEL_INFO = "getTravelInfo";
    private static final String GET_TRAINING_INFO = "getTrainingInfo";
    private static final String GET_RELIGION_TABLE = "getReligionTable";
    private static final String GET_GENDER_TABLE = "getGenderTable";
    private static final String GET_BLOOD_GROUP_TABLE = "getBloodGroupTable";
    private static final String GET_CONSTITUENCY_DETAILS = "getConstituencyDetails";
    private static final String GET_EMPID_USERNAME_LIST = "getUniqueIdDropdownList";
    private static final String GET_PARLIAMENTARY_COMMITTEES_NAME = "getParliamentaryCommitteesName";
    private static final String GET_MINISTRY_OFFICE_NAME = "getMinistryOfficeName";
    private static final String GET_DISTRICT_TABLE = "getDistrictTable";
    private static final String GET_DIVISION_TABLE = "getDivisionTable";
    private static final String GET_OFFICE_UNIT_ORGANOGRAMS = "getOfficeUnitOrganograms";


    public static EmployeeRecordsApiPerformer getPerformer(String actionType) {
        if (actionType != null) {
            switch (actionType) {
                case GET_EMPLOYEE_RECORD_BY_ID_REQUEST:
                    return new GetEmployeeRecordByIdPerformer();

                case DELETE_EMPLOYEE_RECORD_BY_ID_REQUEST:
                    return new DeleteEmployeeRecordByIdPerformer();

                case GET_ALL_EMPLOYEE_RECORD_PAGE_BY_PAGE:
                    return new GetEmployeeRecordPageByPagePerformer();

                case GET_MP_LIST_BY_CONSTITUENCY_NUMBER:
                    return new GetMpListByConstituencyNumberPerformer();

                case GET_MP_LIST_BY_PARLIAMENT_NUMBER:
                    return new GetMpListByParliamentNumberPerformer();

                case GET_PARLIAMENT_DETAILS:
                    return new GetParliamentDetailsPerformer();

                case GET_POLITICAL_PARTY_DETAILS:
                    return new GetPoliticalPartyDetailsPerformer();

                case GET_PARLIAMENT_SESSION:
                    return new GetParliamentSessionPerformer();

                case GET_FAMILY_INFO:
                    return new GetFamilyInfo();

                case GET_EDUCATION_INFO:
                    return new GetEducationInfo();

                case GET_ORAGANOGRAM_TREE:
                    return new GetOrganogramTree();

                case GET_LANGUAGE_INFO:
                    return new GetLanguageInfo();

                case GET_TRAVEL_INFO:
                    return new GetTravelInfo();

                case GET_TRAINING_INFO:
                    return new GetTrainingInfo();

                case GET_GENDER_TABLE:
                    return new GetGenderTable();

                case GET_BLOOD_GROUP_TABLE:
                    return new GetBloodGroupTable();

                case GET_RELIGION_TABLE:
                    return new GetReligionTable();

                case GET_CONSTITUENCY_DETAILS:
                    return new GetConstituencyDetails();

                case GET_EMPID_USERNAME_LIST:
                    return new GetEmpidUsernameList();

                case GET_PARLIAMENTARY_COMMITTEES_NAME:
                    return new GetParliamentaryCommitteesName();

                case GET_MINISTRY_OFFICE_NAME:
                    return new GetMinistryOfficeName();

                case GET_DISTRICT_TABLE:
                    return new GetDistrictTable();

                case GET_DIVISION_TABLE:
                    return new GetDivisionTable();

                case GET_OFFICE_UNIT_ORGANOGRAMS:
                    return new GetOfficeUnitOrganograms();

                case GET_AUTHENTICATED:
                    return new GetAuthenticated();
            }
        }
        return null;
    }
}
