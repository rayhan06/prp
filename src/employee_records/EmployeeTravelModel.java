package employee_records;

import emp_travel_details.Emp_travel_detailsDTO;
import geolocation.GeoCountryRepository;
import pb.CatRepository;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EmployeeTravelModel {
    public String travelPlanNumber;
    public String travelType;
    public String modelOfTravel;
    public String travelPurpose;
    public String travelDestination;
    public String plannedCities;
    public String transportFacilities;
    public String departureDate;
    public String returnDate;
    public int numberOfWorkingDays;
    public int estimatedExpenseBDT;
    public String isAdvanceRequire;
    public int requestedAmount;
    public int numberOfPassengers;

    public EmployeeTravelModel(Emp_travel_detailsDTO dto) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        this.travelPlanNumber = dto.travelPlanNumber;
        this.travelType = CatRepository.getInstance().getText("English", "travel_type", dto.travelTypeCat);
        this.modelOfTravel = CatRepository.getInstance().getText("English", "model_of_travel", dto.modelOfTravelCat);
        this.travelPurpose = CatRepository.getInstance().getText("English", "travel_purpose", dto.travelPurposeCat);
        this.travelDestination = GeoCountryRepository.getInstance().getTexts("English", dto.travelDestination);
        this.plannedCities = dto.plannedCities;
        this.transportFacilities = CatRepository.getInstance().getText("English", "transport_facility", dto.transportFacilityCat);
        this.departureDate = dateFormat.format(new Date(dto.departureDate));
        this.returnDate = dateFormat.format(new Date(dto.returnDate));
        this.numberOfWorkingDays = dto.numberOfWorkingDays;
        this.estimatedExpenseBDT = dto.plannedCostBdt;
        this.isAdvanceRequire = dto.isAdvanceRequired ? "Yes" : "No";
        this.requestedAmount = dto.requestedAdvanceAmount;
        this.numberOfPassengers = dto.numberOfPassengers;
    }

}
