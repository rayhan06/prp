package employee_records;

import common.ApiResponse;
import org.apache.log4j.Logger;
import training_calendar_details.TrainingCalendarDetailsShortInfo;
import training_calendar_details.Training_calendar_detailsDAO;
import user.UserRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class GetTrainingInfo implements EmployeeRecordsApiPerformer {
    public final Logger logger = Logger.getLogger(GetMpListByParliamentNumberPerformer.class);

    @Override
    public ApiResponse perform(HttpServletRequest request) {
        String userName = request.getParameter("employee_record_id");
        long employeeRecordId;

        try {
            employeeRecordId = Employee_recordsRepository.getInstance().getEmployeeRecordIdByUsername(userName);
            if (employeeRecordId==0)
                return ApiResponse.makeResponseToResourceNotFound("No Record found with employee_record_id: "+userName+"!");
        } catch (Exception ex) {
            return ApiResponse.makeResponseToResourceNotFound("No Record found with employee_record_id: "+userName+"!");
        }

        try {
            try {
                List<TrainingCalendarDetailsShortInfo> employeeTrainingModel = new Training_calendar_detailsDAO()
                        .getTrainingCalendarDetailsShortInfoList(employeeRecordId, "English");

                return employeeTrainingModel.size() == 0 ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                        ApiResponse.makeSuccessResponse(employeeTrainingModel, "Successfully found the data.");
            } catch (Exception e) {
                e.printStackTrace();
                return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
            }
        } catch (NumberFormatException nfe) {
            return ApiResponse.makeResponseToBadRequest("Kindly check the employee_record_id parameter provided!");
        }
    }
}