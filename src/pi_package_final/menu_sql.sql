select menuid from menu where constantName = 'PI_PACKAGE_FINAL';
select menuid from menu where constantName = 'PI_PACKAGE_FINAL_ADD';
select menuid from menu where constantName = 'PI_PACKAGE_FINAL_UPDATE';
select menuid from menu where constantName = 'PI_PACKAGE_FINAL_SEARCH';
select menuid from menu where constantName = 'PI_PACKAGE_FINAL_UPLOAD';
select menuid from menu where constantName = 'PI_PACKAGE_FINAL_APPROVE';
INSERT INTO `menu` (`menuID`, `parentMenuID`, `menuName`, `menuNameBangla`, `languageTextID`, `orderIndex`, `selectedMenuID`, `isVisible`, `requestMethodType`, `hyperLink`, `icon`, `isAPI`, `constantName`)  VALUES ('1559440','1257704','FINAL PACKAGE','চূড়ান্ত প্যাকেজ','-1','1','-1','1','1','','fa fa-arrows','0','PI_PACKAGE_FINAL');
INSERT INTO prp_live.menu (menuID, parentMenuID, menuName, menuNameBangla, languageTextID, orderIndex, selectedMenuID, isVisible, requestMethodType, hyperLink, icon, isAPI, constantName, isDeleted, lastModificationTime) VALUES (1559441, 1559440, 'ADD', 'যোগ করুন', -1, 2, -1, 1, 1, 'Pi_package_finalServlet?actionType=getAddPage', 'fa fa-arrows', 0, 'PI_PACKAGE_FINAL_ADD', 0, 0);
INSERT INTO prp_live.menu (menuID, parentMenuID, menuName, menuNameBangla, languageTextID, orderIndex, selectedMenuID, isVisible, requestMethodType, hyperLink, icon, isAPI, constantName, isDeleted, lastModificationTime) VALUES (1559442, 1559440, 'EDIT PI PACKAGE FINAL', 'এডিট করুন অজানা প্যাকেজ চূড়ান্ত', -1, 3, -1, -1, 1, 'Pi_package_finalServlet?actionType=edit', 'fa fa-arrows', 0, 'PI_PACKAGE_FINAL_UPDATE', 0, 0);
INSERT INTO prp_live.menu (menuID, parentMenuID, menuName, menuNameBangla, languageTextID, orderIndex, selectedMenuID, isVisible, requestMethodType, hyperLink, icon, isAPI, constantName, isDeleted, lastModificationTime) VALUES (1559443, 1559440, 'SEARCH', 'খুঁজুন', -1, 4, -1, 1, 1, 'Pi_package_finalServlet?actionType=search', 'fa fa-arrows', 0, 'PI_PACKAGE_FINAL_SEARCH', 0, 0);

INSERT INTO menu_permission VALUES(1, 1559440);
INSERT INTO menu_permission VALUES(1, 1559441);
INSERT INTO menu_permission VALUES(1, 1559442);
INSERT INTO menu_permission VALUES(1, 1559443);
