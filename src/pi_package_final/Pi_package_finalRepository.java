package pi_package_final;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import pb.OptionDTO;
import pb.Utils;
import pi_package_new.Pi_package_newDTO;
import repository.Repository;
import repository.RepositoryManager;
import util.UtilCharacter;


public class Pi_package_finalRepository implements Repository {
    Pi_package_finalDAO pi_package_finalDAO = null;

    static Logger logger = Logger.getLogger(Pi_package_finalRepository.class);
    Map<Long, Pi_package_finalDTO> mapOfPi_package_finalDTOToiD;
    Map<Long, Set<Pi_package_finalDTO>> mapOfPi_package_finalDTOToofficeUnitId;
    Gson gson;

    private List<Pi_package_finalDTO> pi_package_finalDTOS;


    private Pi_package_finalRepository() {
        pi_package_finalDAO = Pi_package_finalDAO.getInstance();
        mapOfPi_package_finalDTOToiD = new ConcurrentHashMap<>();
        mapOfPi_package_finalDTOToofficeUnitId = new ConcurrentHashMap<>();
        gson = new Gson();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Pi_package_finalRepository INSTANCE = new Pi_package_finalRepository();
    }

    public static Pi_package_finalRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        try {
            List<Pi_package_finalDTO> pi_package_finalDTOs = pi_package_finalDAO.getAllDTOs(reloadAll);
            for (Pi_package_finalDTO pi_package_finalDTO : pi_package_finalDTOs) {
                Pi_package_finalDTO oldPi_package_finalDTO = getPi_package_finalDTOByiD(pi_package_finalDTO.iD);
                if (oldPi_package_finalDTO != null) {
                    mapOfPi_package_finalDTOToiD.remove(oldPi_package_finalDTO.iD);

                    if (mapOfPi_package_finalDTOToofficeUnitId.containsKey(oldPi_package_finalDTO.officeUnitId)) {
                        mapOfPi_package_finalDTOToofficeUnitId.get(oldPi_package_finalDTO.officeUnitId).remove(oldPi_package_finalDTO);
                    }
                    if (mapOfPi_package_finalDTOToofficeUnitId.get(oldPi_package_finalDTO.officeUnitId).isEmpty()) {
                        mapOfPi_package_finalDTOToofficeUnitId.remove(oldPi_package_finalDTO.officeUnitId);
                    }


                }
                if (pi_package_finalDTO.isDeleted == 0) {

                    mapOfPi_package_finalDTOToiD.put(pi_package_finalDTO.iD, pi_package_finalDTO);

                    if (!mapOfPi_package_finalDTOToofficeUnitId.containsKey(pi_package_finalDTO.officeUnitId)) {
                        mapOfPi_package_finalDTOToofficeUnitId.put(pi_package_finalDTO.officeUnitId, new HashSet<>());
                    }
                    mapOfPi_package_finalDTOToofficeUnitId.get(pi_package_finalDTO.officeUnitId).add(pi_package_finalDTO);

                }
            }

            pi_package_finalDTOS = new ArrayList<>(mapOfPi_package_finalDTOToiD.values());
        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public Pi_package_finalDTO clone(Pi_package_finalDTO dto) {
        String raw = gson.toJson(dto);
        return gson.fromJson(raw, Pi_package_finalDTO.class);
    }


    public List<Pi_package_finalDTO> getPi_package_finalList() {
        List<Pi_package_finalDTO> pi_package_finals = new ArrayList<Pi_package_finalDTO>(this.mapOfPi_package_finalDTOToiD.values());
        return pi_package_finals;
    }

    public List<Pi_package_finalDTO> copyPi_package_finalList() {
        List<Pi_package_finalDTO> pi_package_finals = getPi_package_finalList();
        return pi_package_finals
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }


    public Pi_package_finalDTO getPi_package_finalDTOByiD(long iD) {
        return mapOfPi_package_finalDTOToiD.get(iD);
    }

    public Pi_package_finalDTO copyPi_package_finalDTOByiD(long iD) {
        return clone(mapOfPi_package_finalDTOToiD.get(iD));
    }


    public List<Pi_package_finalDTO> getPi_package_finalDTOByofficeUnitId(long officeUnitId) {
        return new ArrayList<>(mapOfPi_package_finalDTOToofficeUnitId.getOrDefault(officeUnitId, new HashSet<>()));
    }

    public List<Pi_package_finalDTO> copyPi_package_finalDTOByofficeUnitId(long officeUnitId) {
        List<Pi_package_finalDTO> pi_package_finals = getPi_package_finalDTOByofficeUnitId(officeUnitId);
        return pi_package_finals
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }

    public String buildOptions(String language, Long selectedId) {
        List<OptionDTO> optionDTOList = null;
        if (pi_package_finalDTOS != null && pi_package_finalDTOS.size() > 0) {
            optionDTOList = pi_package_finalDTOS.stream()
                    .map(Pi_package_finalDTO::getOptionDTO)
                    .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }


    @Override
    public String getTableName() {
        return pi_package_finalDAO.getTableName();
    }

    public String getPackageNumberTextById(String Language, long packageFinalId) {
        Pi_package_finalDTO dto = Pi_package_finalRepository.getInstance().getPi_package_finalDTOByiD(packageFinalId);
        return dto == null ? "" : UtilCharacter.getDataByLanguage(Language, dto.packageNumberBn, dto.packageNumberEn);
    }

    public List<Pi_package_finalDTO> getDTOsByIds(List<Long> ids){
        List<Pi_package_finalDTO> dtos = new ArrayList<>();
        ids.forEach(i -> {
            Pi_package_finalDTO dto = getPi_package_finalDTOByiD(i);
            if(dto != null){
                dtos.add(dto);
            }
        });
        return dtos;
    }

    public String getName(long packageId, String Language){
        Pi_package_finalDTO dto = Pi_package_finalRepository.getInstance().getPi_package_finalDTOByiD(packageId);
        return dto == null ? "" : UtilCharacter.getDataByLanguage(Language, dto.packageNumberBn, dto.packageNumberEn);
    }
}


