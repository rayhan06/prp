package pi_package_final;

import com.google.gson.Gson;
import common.ApiResponse;
import common.BaseServlet;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import pi_package_item_map.PiPackageItemMapChildDAO;
import pi_package_item_map.PiPackageItemMapChildDTO;
import pi_package_item_map.Pi_package_item_mapDAO;
import pi_package_item_map.Pi_package_item_mapDTO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;
import util.CommonLoginData;
import util.HttpRequestUtils;
import util.UtilCharacter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/Pi_package_finalServlet")
@MultipartConfig
public class Pi_package_finalServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_package_finalServlet.class);

    @Override
    public String getTableName() {
        return Pi_package_finalDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_package_finalServlet";
    }

    @Override
    public Pi_package_finalDAO getCommonDAOService() {
        return Pi_package_finalDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PI_PACKAGE_FINAL_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PI_PACKAGE_FINAL_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PI_PACKAGE_FINAL_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_package_finalServlet.class;
    }

    PiPackageLotFinalDAO piPackageLotFinalDAO = PiPackageLotFinalDAO.getInstance();
    private final Gson gson = new Gson();


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        try {
            switch (request.getParameter("actionType")) {
                case "getLotOptionsByPackage":
                    if (Utils.checkPermission(commonLoginData.userDTO, getSearchMenuConstants()) && getAddPagePermission(request)) {
                        String packageId = request.getParameter("packageId");
                        String lotId = request.getParameter("piLotId");
                        if (packageId == null || packageId.equals("")) {
                            ApiResponse.sendSuccessResponse(response, "");
                            return;
                        }
                        List<PiPackageLotFinalDTO> lotDTOs = (List<PiPackageLotFinalDTO>) PiPackageLotFinalDAO.getInstance().getDTOsByParent("pi_package_final_id", Long.parseLong(packageId));
                        boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
                        String lotOptions = Utils.buildSelectOption(isLanguageEnglish);
                        StringBuilder option = new StringBuilder();
                        for (PiPackageLotFinalDTO dto : lotDTOs) {
                            if (lotId != null && !lotId.equals("") && Long.parseLong(lotId) == dto.iD) {
                                option.append("<option value = '").append(dto.iD).append("' selected>");
                            } else {
                                option.append("<option value = '").append(dto.iD).append("'>");
                            }
                            option.append(isLanguageEnglish ? dto.lotNumberEn : dto.lotNumberBn).append("</option>");
                        }
                        lotOptions += option.toString();
                        ApiResponse.sendSuccessResponse(response, lotOptions);
                    }
                    break;
                case "deletePackageWithValidation":
                    if (getDeletePermission(request)) {
                        String packageIdStr = request.getParameter("packageId");
                        if (packageIdStr == null) {
                            UtilCharacter.throwException("ডিলিট ব্যর্থ হয়েছে!", "Delete Failed!");
                        }
                        Long packageId = Long.parseLong(packageIdStr);
                        List<Pi_package_item_mapDTO> DTOs = (List<Pi_package_item_mapDTO>) Pi_package_item_mapDAO.getInstance()
                                .getDTOsByParent("pi_package_final_id", packageId);
                        if (DTOs != null && DTOs.size() == 0) {
                            List<PiPackageLotFinalDTO> lots = (List<PiPackageLotFinalDTO>) PiPackageLotFinalDAO.getInstance()
                                    .getDTOsByParent("pi_package_final_id", packageId);
                            if (lots != null) {
                                for (PiPackageLotFinalDTO lot : lots) {
                                    PiPackageLotFinalDAO.getInstance().delete(commonLoginData.userDTO.employee_record_id, lot.iD);
                                }
                            }
                            Pi_package_finalDAO.getInstance().delete(commonLoginData.userDTO.employee_record_id, packageId);
                            String successMessage = UtilCharacter.getDataByLanguage(Language, "ডিলিট সফল হয়েছে", "Delete Successful");
                            ApiResponse.sendSuccessResponse(response, successMessage);
                        } else {
                            Pi_package_finalDTO packageModel = Pi_package_finalDAO.getInstance().getDTOByID(packageId);
                            String errorMessage = UtilCharacter.getDataByLanguage(Language, packageModel.packageNumberBn + " ডিলিট করা যাবে না। কারণ এই এটা প্যাকেজ আইটেম ম্যাপে ব্যবহার করা হচ্ছে!",
                                    packageModel.packageNumberEn + " can not be deleted. Because this package is used in package item map!");
                            ApiResponse.sendErrorResponse(response, errorMessage);
                        }
                    }
                    break;
                case "deleteLotWithValidation":
                    if (getDeletePermission(request)) {
                        String lotStr = request.getParameter("lotId");
                        if (lotStr == null) {
                            String errorMessage = UtilCharacter.getDataByLanguage(Language, "লট ডিলিট ব্যর্থ হয়েছে!", "Lot Delete Failed!");
                            ApiResponse.sendErrorResponse(response, errorMessage);
                        }
                        Long lotId = Long.parseLong(lotStr);
                        List<Pi_package_item_mapDTO> DTOs = (List<Pi_package_item_mapDTO>) Pi_package_item_mapDAO.getInstance()
                                .getDTOsByParent("pi_lot_final_id", lotId);
                        if (DTOs != null && DTOs.size() == 0) {
                            PiPackageLotFinalDAO.getInstance().delete(commonLoginData.userDTO.employee_record_id, lotId);
                            String successMessage = UtilCharacter.getDataByLanguage(Language, "লট ডিলিট সফল হয়েছে", "Lot Delete Successful");
                            ApiResponse.sendSuccessResponse(response, successMessage);
                        } else {
                            PiPackageLotFinalDTO lotModel = PiPackageLotFinalDAO.getInstance().getDTOByID(lotId);
                            String errorMessage = UtilCharacter.getDataByLanguage(Language,
                                    lotModel.lotNumberBn + " ডিলিট করা যাবে না। কারণ এটা প্যাকেজ আইটেম ম্যাপে ব্যবহার হচ্ছে!",
                                    lotModel.lotNumberEn + "  can not be deleted. Because it is used in package item map!");
                            ApiResponse.sendErrorResponse(response, errorMessage);
                        }
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        super.doGet(request, response);
    }


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        request.setAttribute("failureMessage", "");
        logger.debug("%%%% addPi_package_new");
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        Pi_package_finalDTO pi_package_finalDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLanEng = Language.equalsIgnoreCase("English");

        if (addFlag) {
            pi_package_finalDTO = new Pi_package_finalDTO();
            pi_package_finalDTO.insertionDate = pi_package_finalDTO.lastModificationTime = System.currentTimeMillis();
            pi_package_finalDTO.insertedBy = pi_package_finalDTO.modifiedBy = String.valueOf(userDTO.ID);
        } else {
            pi_package_finalDTO = Pi_package_finalDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
            if (pi_package_finalDTO == null) {
                throw new Exception(isLanEng ? "Package information is not found" : "প্যাকেজ তথ্য খুঁজে পাওয়া যায়নি");
            }
            pi_package_finalDTO.lastModificationTime = System.currentTimeMillis();
            pi_package_finalDTO.modifiedBy = String.valueOf(userDTO.ID);
        }

        String Value = "";

        Value = request.getParameter("officeUnitId");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please Select any Office" : "অনুগ্রহপূর্বক একটি দপ্তর নির্বাচন করুন");
        }
        pi_package_finalDTO.officeUnitId = Long.parseLong(Value);


        Value = request.getParameter("hasLot");
        pi_package_finalDTO.hasLot = Value != null && !Value.equals("") && !Value.equals("-1");


        Value = request.getParameter("packageNumberEn");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide package number english" : "অনুগ্রহপূর্বক প্যাকেজের ইংরেজি নাম্বার দিন");
        }
        Value = Jsoup.clean(Value, Whitelist.simpleText());

        boolean hasNonEnglish =
                Value.chars()
                        .anyMatch(ch -> !(ch >= ' ' && ch <= '~'));
        if (!hasNonEnglish) {
            if (addFlag && Pi_package_finalDAO.getInstance().isPackageNumberExists(Value)) {
                throw new Exception(isLanEng ? "Package number already exist" : "প্যাকেজের নাম্বার ইতিমধ্যে বিদ্যমান");
            } else if (!addFlag && Pi_package_finalDAO.getInstance().isPackageNumberExists(Value, pi_package_finalDTO.iD)) {
                throw new Exception(isLanEng ? "Package number already exist" : "প্যাকেজের নাম্বার ইতিমধ্যে বিদ্যমান");
            } else {
                pi_package_finalDTO.packageNumberEn = (Value);
            }
        } else {
            throw new Exception(isLanEng ? "Please provide package number in english" : "অনুগ্রহপূর্বক প্যাকেজের ইংরেজি নাম্বার দিন");
        }


        Value = request.getParameter("packageNumberBn");

        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide package number bangla" : "অনুগ্রহপূর্বক প্যাকেজের বাংলা নাম্বার দিন");
        }
        Value = Jsoup.clean(Value, Whitelist.simpleText());

        boolean hasAllBangla = Value.chars().allMatch(Utils::isValidBanglaCharacter);
        if (!hasAllBangla) {
            throw new Exception(isLanEng ? "Please provide package number in bangla" : "অনুগ্রহপূর্বক প্যাকেজের বাংলা নাম্বার দিন");
        }
        if (addFlag && Pi_package_finalDAO.getInstance().isPackageNumberExists(Value)) {
            throw new Exception(isLanEng ? "Package number already exist" : "প্যাকেজের নাম্বার ইতিমধ্যে বিদ্যমান");
        } else if (!addFlag && Pi_package_finalDAO.getInstance().isPackageNumberExists(Value, pi_package_finalDTO.iD)) {
            throw new Exception(isLanEng ? "Package number already exist" : "প্যাকেজের নাম্বার ইতিমধ্যে বিদ্যমান");
        } else {
            pi_package_finalDTO.packageNumberBn = (Value);
        }


        Value = request.getParameter("packageNameEn");
        if ((Value == null || Value.equals("") || Value.equals("-1")) && !pi_package_finalDTO.hasLot) {
            throw new Exception(isLanEng ? "Please provide package english name" : "অনুগ্রহপূর্বক প্যাকেজের ইংরেজি নাম দিন");
        }
        Value = Jsoup.clean(Value, Whitelist.simpleText());

        hasNonEnglish =
                Value.chars()
                        .anyMatch(ch -> !(ch >= ' ' && ch <= '~'));
        if (!hasNonEnglish) {
            if (addFlag && Pi_package_finalDAO.getInstance().isPackageNameExists(Value) && !pi_package_finalDTO.hasLot) {
                throw new Exception(isLanEng ? "Package name already exist" : "প্যাকেজের নাম ইতিমধ্যে বিদ্যমান");
            } else if (!addFlag && Pi_package_finalDAO.getInstance().isPackageNameExists(Value, pi_package_finalDTO.iD) && !pi_package_finalDTO.hasLot) {
                throw new Exception(isLanEng ? "Package name already exist" : "প্যাকেজের নাম ইতিমধ্যে বিদ্যমান");
            } else {
                pi_package_finalDTO.packageNameEn = (Value);
            }
        } else {
            throw new Exception(isLanEng ? "Please provide package english name" : "অনুগ্রহপূর্বক প্যাকেজের ইংরেজি নাম দিন");
        }


        Value = request.getParameter("packageNameBn");
        if ((Value == null || Value.equals("") || Value.equals("-1")) && !pi_package_finalDTO.hasLot) {
            throw new Exception(isLanEng ? "Please provide package bangla name" : "অনুগ্রহপূর্বক প্যাকেজের বাংলা নাম দিন");
        }
        Value = Jsoup.clean(Value, Whitelist.simpleText());

        hasAllBangla = Value.chars().allMatch(Utils::isValidBanglaCharacter);
        if (!hasAllBangla) {
            throw new Exception(isLanEng ? "Please provide package bangla name" : "অনুগ্রহপূর্বক প্যাকেজের বাংলা নাম দিন");
        }
        if (addFlag && Pi_package_finalDAO.getInstance().isPackageNameExists(Value) && !pi_package_finalDTO.hasLot) {
            throw new Exception(isLanEng ? "Package name already exist" : "প্যাকেজের নাম ইতিমধ্যে বিদ্যমান");
        } else if (!addFlag && Pi_package_finalDAO.getInstance().isPackageNameExists(Value, pi_package_finalDTO.iD) && !pi_package_finalDTO.hasLot) {
            throw new Exception(isLanEng ? "Package name already exist" : "প্যাকেজের নাম ইতিমধ্যে বিদ্যমান");
        } else {
            pi_package_finalDTO.packageNameBn = (Value);
        }

        validateLotCount(request);

        long returnedID = -1;

        if (addFlag) {
            returnedID = Pi_package_finalDAO.getInstance().add(pi_package_finalDTO);
            pi_package_finalDTO.iD = returnedID;
        } else {
            // IF THERE IS LOT, USER CAN NOT GIVE PACKAGE NAME AND NUMBER
            List<PiPackageLotFinalDTO> lotDTOs = (List<PiPackageLotFinalDTO>) PiPackageLotFinalDAO.getInstance()
                    .getDTOsByParent("pi_package_final_id", pi_package_finalDTO.iD);
            if(lotDTOs != null && lotDTOs.size() > 0){
                pi_package_finalDTO.hasLot = true;
                pi_package_finalDTO.packageNameEn = "";
                pi_package_finalDTO.packageNameBn = "";
            }
            returnedID = Pi_package_finalDAO.getInstance().update(pi_package_finalDTO);
        }

        List<PiPackageLotFinalDTO> newLotDTOs = createPiPackageLotDTOListByRequest(request, language, pi_package_finalDTO);
        List<PiPackageLotFinalDTO> oldLotDTOs = (List<PiPackageLotFinalDTO>) PiPackageLotFinalDAO.getInstance()
                .getDTOsByParent("pi_package_final_id", pi_package_finalDTO.iD);
        // IDS WITH -1 ARE FOR ADD
        List<PiPackageLotFinalDTO> lotDTOsAdd = newLotDTOs.stream().filter(dto -> dto.iD == -1).collect(Collectors.toList());
        // COMMON DTOS ARE FOR EDIT
        List<PiPackageLotFinalDTO> lotDTOsEdit = newLotDTOs.stream().filter(dto -> fieldContains(oldLotDTOs, dto.iD)).collect(Collectors.toList());
        // IDS NOT COMMON AND NOT -1 ARE FOR DELETE
        List<PiPackageLotFinalDTO> lotDTOsDelete = oldLotDTOs.stream().filter(dto -> !fieldContains(lotDTOsEdit, dto.iD)).collect(Collectors.toList());

        for (PiPackageLotFinalDTO lotDTO : lotDTOsAdd) {
            lotDTO.insertedBy = lotDTO.modifiedBy = String.valueOf(userDTO.employee_record_id);
            lotDTO.insertionDate = lotDTO.lastModificationTime = System.currentTimeMillis();
            PiPackageLotFinalDAO.getInstance().add(lotDTO);
        }
        for (PiPackageLotFinalDTO lotDTO : lotDTOsEdit) {
            lotDTO.modifiedBy = String.valueOf(userDTO.employee_record_id);
            lotDTO.lastModificationTime = System.currentTimeMillis();
            PiPackageLotFinalDAO.getInstance().update(lotDTO);
        }
        for (PiPackageLotFinalDTO lotDTO : lotDTOsDelete) {
            // FILTER OUT LOT IF USED IN PACKAGE ITEM MAP
            List<PiPackageItemMapChildDTO> dto = (List<PiPackageItemMapChildDTO>) PiPackageItemMapChildDAO.getInstance()
                    .getDTOsByParent("pi_lot_final_id", lotDTO.iD);
            if (dto != null && dto.size() > 0)
                UtilCharacter.throwException(lotDTO.lotNumberBn + " ডিলিট করা যাবে না। কারণ এটা প্যাকেজ আইটেম ম্যাপে ব্যবহার হচ্ছে!",
                        lotDTO.lotNumberEn + "  can not be deleted. Because it is used in package item map!");
            PiPackageLotFinalDAO.getInstance().delete(userDTO.employee_record_id, lotDTO.iD, System.currentTimeMillis());
        }

        return pi_package_finalDTO;
    }

    private boolean fieldContains(List<PiPackageLotFinalDTO> lotDTOs, Long id) {
        List<PiPackageLotFinalDTO> dto = lotDTOs.stream().filter(lotDTO -> lotDTO.iD == id).collect(Collectors.toList());
        return dto.size() > 0;
    }

    private void validateLotCount(HttpServletRequest request) throws Exception {
        if (getLotCount(request) > 5) {
            UtilCharacter.throwException("৫ টার বেশি লট তৈরি করা যাবে না", "You can not create more than 5 lot");
        }
    }

    private int getLotCount(HttpServletRequest request) {
        if (request.getParameterValues("lot.iD") != null) {
            int lotCount = request.getParameterValues("lot.iD").length;
            return lotCount;
        }
        return 0;
    }


    private List<PiPackageLotFinalDTO> createPiPackageLotDTOListByRequest(HttpServletRequest request, String language,
                                                                          Pi_package_finalDTO pi_package_finalDTO) throws Exception {
        List<PiPackageLotFinalDTO> pi_package_lotDTOList = new ArrayList<>();

        int lotCount = getLotCount(request);

        for (int index = 0; index < lotCount; index++) {
            PiPackageLotFinalDTO lotDTO = createPiPackageLotDTOByRequestAndIndex(request, index, language);
            lotDTO.officeUnitId = pi_package_finalDTO.officeUnitId;
            lotDTO.piPackageFinalId = pi_package_finalDTO.iD;
            pi_package_lotDTOList.add(lotDTO);
        }
        return pi_package_lotDTOList;
    }

    private PiPackageLotFinalDTO createPiPackageLotDTOByRequestAndIndex(HttpServletRequest request, int index, String language) throws Exception {
        PiPackageLotFinalDTO lotDTO;
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLanEng = Language.equalsIgnoreCase("English");

        lotDTO = new PiPackageLotFinalDTO();

        String Value = "";

        Value = request.getParameterValues("lot.iD")[index];
        if (Value == null || Value.equals("")) {
            lotDTO.iD = -1;
        } else {
            lotDTO.iD = Long.parseLong(Value);
        }

        Value = request.getParameterValues("insertionDate")[index];
        if (Value != null || !Value.equalsIgnoreCase("")) {
            lotDTO.insertionDate = Long.parseLong(Value);
        }
        Value = request.getParameterValues("insertedBy")[index];
        if (Value != null || !Value.equalsIgnoreCase("")) {
            lotDTO.insertedBy = Value;
        }

        Value = request.getParameterValues("lotNumberEn")[index];
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(isLanEng ? "Please write lot number english" : "অনুগ্রহপূর্বক লটের ইংরেজি নাম্বার দিন");
        }
        boolean hasNonEnglish = Value.chars().anyMatch(ch -> !(ch >= ' ' && ch <= '~'));
        if (hasNonEnglish) {
            throw new Exception(isLanEng ? "Please write lot number in english language" : "অনুগ্রহপূর্বক লটের ইংরেজি নাম্বার দিন");
        } else {
            lotDTO.lotNumberEn = Value;
        }


        Value = request.getParameterValues("lotNumberBn")[index];
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(isLanEng ? "Please write lot number bangla" : "অনুগ্রহপূর্বক লটের বাংলা নাম্বার দিন");
        }
        boolean hasAllBangla = Value.chars().allMatch(Utils::isValidBanglaCharacter);
        if (!hasAllBangla) {
            throw new Exception(isLanEng ? "Please write lot number in bangla" : "অনুগ্রহপূর্বক লটের বাংলা নাম্বার দিন");
        } else {
            lotDTO.lotNumberBn = Value;
        }

        Value = request.getParameterValues("lotNameEn")[index];
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(isLanEng ? "Please write lot name english" : "অনুগ্রহপূর্বক লটের ইংরেজি নাম দিন");
        }
        hasNonEnglish = Value.chars().anyMatch(ch -> !(ch >= ' ' && ch <= '~'));
        if (hasNonEnglish) {
            throw new Exception(isLanEng ? "Please write lot name in english language" : "অনুগ্রহপূর্বক লটের ইংরেজি নাম দিন");
        } else {
            lotDTO.lotNameEn = Value;
        }


        Value = request.getParameterValues("lotNameBn")[index];
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(isLanEng ? "Please write lot name bangla" : "অনুগ্রহপূর্বক লটের বাংলা নাম দিন");
        }
        hasAllBangla = Value.chars().allMatch(Utils::isValidBanglaCharacter);
        if (!hasAllBangla) {
            throw new Exception(isLanEng ? "Please write lot name in bangla" : "অনুগ্রহপূর্বক লটের বাংলা নাম দিন");
        } else {
            lotDTO.lotNameBn = Value;
        }

        return lotDTO;
    }
}

