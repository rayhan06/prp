package pi_package_final;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PiPackageLotFinalDAO implements CommonDAOService<PiPackageLotFinalDTO> {
    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private String[] columnNames = null;

    private PiPackageLotFinalDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "pi_package_final_id",
                        "office_unit_id",
                        "lot_number_en",
                        "lot_number_bn",
                        "lot_name_en",
                        "lot_name_bn",
                        "search_column",
                        "inserted_by",
                        "modified_by",
                        "insertion_date",
                        "isDeleted",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("lot_number_en", " and (lot_number_en like ?)");
        searchMap.put("lot_number_bn", " and (lot_number_bn like ?)");
        searchMap.put("lot_name_en", " and (lot_name_en like ?)");
        searchMap.put("lot_name_bn", " and (lot_name_bn like ?)");
        searchMap.put("inserted_by", " and (inserted_by like ?)");
        searchMap.put("modified_by", " and (modified_by like ?)");
        searchMap.put("insertion_date_start", " and (insertion_date >= ?)");
        searchMap.put("insertion_date_end", " and (insertion_date <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final PiPackageLotFinalDAO INSTANCE = new PiPackageLotFinalDAO();
    }

    public static PiPackageLotFinalDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(PiPackageLotFinalDTO pipackagelotfinalDTO) {
        pipackagelotfinalDTO.searchColumn = "";
        pipackagelotfinalDTO.searchColumn += pipackagelotfinalDTO.lotNumberEn + " ";
        pipackagelotfinalDTO.searchColumn += pipackagelotfinalDTO.lotNumberBn + " ";
        pipackagelotfinalDTO.searchColumn += pipackagelotfinalDTO.lotNameEn + " ";
        pipackagelotfinalDTO.searchColumn += pipackagelotfinalDTO.lotNameBn + " ";
        pipackagelotfinalDTO.searchColumn += pipackagelotfinalDTO.insertedBy + " ";
        pipackagelotfinalDTO.searchColumn += pipackagelotfinalDTO.modifiedBy + " ";
    }

    @Override
    public void set(PreparedStatement ps, PiPackageLotFinalDTO pipackagelotfinalDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(pipackagelotfinalDTO);
        if (isInsert) {
            ps.setObject(++index, pipackagelotfinalDTO.iD);
        }
        ps.setObject(++index, pipackagelotfinalDTO.piPackageFinalId);
        ps.setObject(++index, pipackagelotfinalDTO.officeUnitId);
        ps.setObject(++index, pipackagelotfinalDTO.lotNumberEn);
        ps.setObject(++index, pipackagelotfinalDTO.lotNumberBn);
        ps.setObject(++index, pipackagelotfinalDTO.lotNameEn);
        ps.setObject(++index, pipackagelotfinalDTO.lotNameBn);
        ps.setObject(++index, pipackagelotfinalDTO.searchColumn);
        ps.setObject(++index, pipackagelotfinalDTO.insertedBy);
        ps.setObject(++index, pipackagelotfinalDTO.modifiedBy);
        ps.setObject(++index, pipackagelotfinalDTO.insertionDate);
        if (isInsert) {
            ps.setObject(++index, pipackagelotfinalDTO.isDeleted);
        }
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, pipackagelotfinalDTO.iD);
        }
    }

    @Override
    public PiPackageLotFinalDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            PiPackageLotFinalDTO pipackagelotfinalDTO = new PiPackageLotFinalDTO();
            int i = 0;
            pipackagelotfinalDTO.iD = rs.getLong(columnNames[i++]);
            pipackagelotfinalDTO.piPackageFinalId = rs.getLong(columnNames[i++]);
            pipackagelotfinalDTO.officeUnitId = rs.getLong(columnNames[i++]);
            pipackagelotfinalDTO.lotNumberEn = rs.getString(columnNames[i++]);
            pipackagelotfinalDTO.lotNumberBn = rs.getString(columnNames[i++]);
            pipackagelotfinalDTO.lotNameEn = rs.getString(columnNames[i++]);
            pipackagelotfinalDTO.lotNameBn = rs.getString(columnNames[i++]);
            pipackagelotfinalDTO.searchColumn = rs.getString(columnNames[i++]);
            pipackagelotfinalDTO.insertedBy = rs.getString(columnNames[i++]);
            pipackagelotfinalDTO.modifiedBy = rs.getString(columnNames[i++]);
            pipackagelotfinalDTO.insertionDate = rs.getLong(columnNames[i++]);
            pipackagelotfinalDTO.isDeleted = rs.getInt(columnNames[i++]);
            pipackagelotfinalDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            return pipackagelotfinalDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public PiPackageLotFinalDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "pi_package_lot_final";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((PiPackageLotFinalDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((PiPackageLotFinalDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

    public List<PiPackageLotFinalDTO> getLotsByPackageId(long packageId){
        return (List<PiPackageLotFinalDTO>) PiPackageLotFinalDAO.getInstance()
                .getDTOsByParent("pi_package_final_id", packageId);
    }
}
	