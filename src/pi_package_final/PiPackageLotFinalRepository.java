package pi_package_final;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.UtilCharacter;


public class PiPackageLotFinalRepository implements Repository {
	PiPackageLotFinalDAO pipackagelotfinalDAO = null;
	
	static Logger logger = Logger.getLogger(PiPackageLotFinalRepository.class);
	Map<Long, PiPackageLotFinalDTO>mapOfPiPackageLotFinalDTOToiD;
	Map<Long, Set<PiPackageLotFinalDTO> >mapOfPiPackageLotFinalDTOTopiPackageNewId;
	Map<Long, Set<PiPackageLotFinalDTO> >mapOfPiPackageLotFinalDTOToofficeUnitId;
	Gson gson;

  
	private PiPackageLotFinalRepository(){
		pipackagelotfinalDAO = PiPackageLotFinalDAO.getInstance();
		mapOfPiPackageLotFinalDTOToiD = new ConcurrentHashMap<>();
		mapOfPiPackageLotFinalDTOTopiPackageNewId = new ConcurrentHashMap<>();
		mapOfPiPackageLotFinalDTOToofficeUnitId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static PiPackageLotFinalRepository INSTANCE = new PiPackageLotFinalRepository();
    }

    public static PiPackageLotFinalRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<PiPackageLotFinalDTO> pipackagelotfinalDTOs = pipackagelotfinalDAO.getAllDTOs(reloadAll);
			for(PiPackageLotFinalDTO pipackagelotfinalDTO : pipackagelotfinalDTOs) {
				PiPackageLotFinalDTO oldPiPackageLotFinalDTO = getPiPackageLotFinalDTOByiD(pipackagelotfinalDTO.iD);
				if( oldPiPackageLotFinalDTO != null ) {
					mapOfPiPackageLotFinalDTOToiD.remove(oldPiPackageLotFinalDTO.iD);
				
					if(mapOfPiPackageLotFinalDTOTopiPackageNewId.containsKey(oldPiPackageLotFinalDTO.piPackageFinalId)) {
						mapOfPiPackageLotFinalDTOTopiPackageNewId.get(oldPiPackageLotFinalDTO.piPackageFinalId).remove(oldPiPackageLotFinalDTO);
					}
					if(mapOfPiPackageLotFinalDTOTopiPackageNewId.get(oldPiPackageLotFinalDTO.piPackageFinalId).isEmpty()) {
						mapOfPiPackageLotFinalDTOTopiPackageNewId.remove(oldPiPackageLotFinalDTO.piPackageFinalId);
					}
					
					if(mapOfPiPackageLotFinalDTOToofficeUnitId.containsKey(oldPiPackageLotFinalDTO.officeUnitId)) {
						mapOfPiPackageLotFinalDTOToofficeUnitId.get(oldPiPackageLotFinalDTO.officeUnitId).remove(oldPiPackageLotFinalDTO);
					}
					if(mapOfPiPackageLotFinalDTOToofficeUnitId.get(oldPiPackageLotFinalDTO.officeUnitId).isEmpty()) {
						mapOfPiPackageLotFinalDTOToofficeUnitId.remove(oldPiPackageLotFinalDTO.officeUnitId);
					}
					
					
				}
				if(pipackagelotfinalDTO.isDeleted == 0) 
				{
					
					mapOfPiPackageLotFinalDTOToiD.put(pipackagelotfinalDTO.iD, pipackagelotfinalDTO);
				
					if( ! mapOfPiPackageLotFinalDTOTopiPackageNewId.containsKey(pipackagelotfinalDTO.piPackageFinalId)) {
						mapOfPiPackageLotFinalDTOTopiPackageNewId.put(pipackagelotfinalDTO.piPackageFinalId, new HashSet<>());
					}
					mapOfPiPackageLotFinalDTOTopiPackageNewId.get(pipackagelotfinalDTO.piPackageFinalId).add(pipackagelotfinalDTO);
					
					if( ! mapOfPiPackageLotFinalDTOToofficeUnitId.containsKey(pipackagelotfinalDTO.officeUnitId)) {
						mapOfPiPackageLotFinalDTOToofficeUnitId.put(pipackagelotfinalDTO.officeUnitId, new HashSet<>());
					}
					mapOfPiPackageLotFinalDTOToofficeUnitId.get(pipackagelotfinalDTO.officeUnitId).add(pipackagelotfinalDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public PiPackageLotFinalDTO clone(PiPackageLotFinalDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, PiPackageLotFinalDTO.class);
	}
	
	
	public List<PiPackageLotFinalDTO> getPiPackageLotFinalList() {
		List <PiPackageLotFinalDTO> pipackagelotfinals = new ArrayList<PiPackageLotFinalDTO>(this.mapOfPiPackageLotFinalDTOToiD.values());
		return pipackagelotfinals;
	}
	
	public List<PiPackageLotFinalDTO> copyPiPackageLotFinalList() {
		List <PiPackageLotFinalDTO> pipackagelotfinals = getPiPackageLotFinalList();
		return pipackagelotfinals
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public PiPackageLotFinalDTO getPiPackageLotFinalDTOByiD( long iD){
		return mapOfPiPackageLotFinalDTOToiD.get(iD);
	}
	
	public PiPackageLotFinalDTO copyPiPackageLotFinalDTOByiD( long iD){
		return clone(mapOfPiPackageLotFinalDTOToiD.get(iD));
	}
	
	
	public List<PiPackageLotFinalDTO> getPiPackageLotFinalDTOBypiPackageNewId(long piPackageNewId) {
		return new ArrayList<>( mapOfPiPackageLotFinalDTOTopiPackageNewId.getOrDefault(piPackageNewId,new HashSet<>()));
	}
	
	public List<PiPackageLotFinalDTO> copyPiPackageLotFinalDTOBypiPackageNewId(long piPackageNewId)
	{
		List <PiPackageLotFinalDTO> pipackagelotfinals = getPiPackageLotFinalDTOBypiPackageNewId(piPackageNewId);
		return pipackagelotfinals
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<PiPackageLotFinalDTO> getPiPackageLotFinalDTOByofficeUnitId(long officeUnitId) {
		return new ArrayList<>( mapOfPiPackageLotFinalDTOToofficeUnitId.getOrDefault(officeUnitId,new HashSet<>()));
	}
	
	public List<PiPackageLotFinalDTO> copyPiPackageLotFinalDTOByofficeUnitId(long officeUnitId)
	{
		List <PiPackageLotFinalDTO> pipackagelotfinals = getPiPackageLotFinalDTOByofficeUnitId(officeUnitId);
		return pipackagelotfinals
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return pipackagelotfinalDAO.getTableName();
	}

	public String getLotNumberTextById(String Language, long lotFinalId) {
		PiPackageLotFinalDTO dto = PiPackageLotFinalRepository.getInstance().getPiPackageLotFinalDTOByiD(lotFinalId);
		return dto == null ? "" : UtilCharacter.getDataByLanguage(Language, dto.lotNumberBn, dto.lotNumberEn);
	}

	public String buildOptions(String language, Long selectedId) {
		List<OptionDTO> optionDTOList = null;
		List<PiPackageLotFinalDTO> lotDTOs = PiPackageLotFinalDAO.getInstance().getAllDTOs();
		if ( lotDTOs!= null && lotDTOs.size() > 0) {
			optionDTOList = lotDTOs.stream()
					.map(PiPackageLotFinalDTO::getOptionDTO)
					.collect(Collectors.toList());
		}
		return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
	}

	public List<PiPackageLotFinalDTO> getDTOsByIds(List<Long> ids){
		List<PiPackageLotFinalDTO> dtos = new ArrayList<>();
		ids.forEach(i -> {
			PiPackageLotFinalDTO dto = getPiPackageLotFinalDTOByiD(i);
			if(dto != null){
				dtos.add(dto);
			}
		});
		return dtos;
	}

	public String getName(long lotId, String language){
		PiPackageLotFinalDTO dto = PiPackageLotFinalRepository.getInstance().getPiPackageLotFinalDTOByiD(lotId);
		return dto == null ? "" : UtilCharacter.getDataByLanguage(language, dto.lotNumberBn, dto.lotNumberEn);
	}
}


