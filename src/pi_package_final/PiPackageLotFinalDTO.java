package pi_package_final;

import java.util.*;

import pb.OptionDTO;
import util.*;


public class PiPackageLotFinalDTO extends CommonDTO {
    public long piPackageFinalId = -1;
    public long officeUnitId = -1;
    public String lotNumberEn = "";
    public String lotNumberBn = "";
    public String lotNameEn = "";
    public String lotNameBn = "";
    public String insertedBy = "";
    public String modifiedBy = "";
    public long insertionDate = -1;

    public List<PiPackageLotFinalDTO> piPackageLotFinalDTOList = new ArrayList<>();

    public OptionDTO getOptionDTO() {
        return new OptionDTO(
                lotNumberEn,
                lotNumberBn,
                String.format("%d", iD)
        );
    }

    @Override
    public String toString() {
        return "$PiPackageLotFinalDTO[" +
                " iD = " + iD +
                " piPackageFinalId = " + piPackageFinalId +
                " officeUnitId = " + officeUnitId +
                " lotNumberEn = " + lotNumberEn +
                " lotNumberBn = " + lotNumberBn +
                " lotNameEn = " + lotNameEn +
                " lotNameBn = " + lotNameBn +
                " searchColumn = " + searchColumn +
                " insertedBy = " + insertedBy +
                " modifiedBy = " + modifiedBy +
                " insertionDate = " + insertionDate +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}