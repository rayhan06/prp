package pi_package_final;

import java.util.*;

import pb.OptionDTO;
import util.*;


public class Pi_package_finalDTO extends CommonDTO {

    public long officeUnitId = -1;
    public String packageNumberEn = "";
    public String packageNumberBn = "";
    public String packageNameEn = "";
    public String packageNameBn = "";
    public boolean hasLot = false;
    public String insertedBy = "";
    public String modifiedBy = "";
    public long insertionDate = -1;

    public List<PiPackageLotFinalDTO> piPackageLotFinalDTOList = new ArrayList<>();

    public OptionDTO getOptionDTO() {
        return new OptionDTO(
                packageNumberEn,
                packageNumberBn,
                String.format("%d", iD)
        );
    }

    @Override
    public String toString() {
        return "$Pi_package_finalDTO[" +
                " iD = " + iD +
                " officeUnitId = " + officeUnitId +
                " packageNumberEn = " + packageNumberEn +
                " packageNumberBn = " + packageNumberBn +
                " packageNameEn = " + packageNameEn +
                " packageNameBn = " + packageNameBn +
                " hasLot = " + hasLot +
                " searchColumn = " + searchColumn +
                " insertedBy = " + insertedBy +
                " modifiedBy = " + modifiedBy +
                " insertionDate = " + insertionDate +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}