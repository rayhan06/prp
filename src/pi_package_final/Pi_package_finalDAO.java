package pi_package_final;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import pi_package_new.Pi_package_newDTO;
import pi_package_new.Pi_package_newRepository;
import util.*;
import pb.*;

public class Pi_package_finalDAO implements CommonDAOService<Pi_package_finalDTO> {

    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private String[] columnNames = null;

    private Pi_package_finalDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "office_unit_id",
                        "package_number_en",
                        "package_number_bn",
                        "package_name_en",
                        "package_name_bn",
                        "has_lot",
                        "search_column",
                        "inserted_by",
                        "modified_by",
                        "insertion_date",
                        "isDeleted",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("package_number_en", " and (package_number_en like ?)");
        searchMap.put("package_number_bn", " and (package_number_bn like ?)");
        searchMap.put("package_name_en", " and (package_name_en like ?)");
        searchMap.put("package_name_bn", " and (package_name_bn like ?)");
        searchMap.put("inserted_by", " and (inserted_by like ?)");
        searchMap.put("modified_by", " and (modified_by like ?)");
        searchMap.put("insertion_date_start", " and (insertion_date >= ?)");
        searchMap.put("insertion_date_end", " and (insertion_date <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Pi_package_finalDAO INSTANCE = new Pi_package_finalDAO();
    }

    public static Pi_package_finalDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Pi_package_finalDTO pi_package_finalDTO) {
        pi_package_finalDTO.searchColumn = "";
        pi_package_finalDTO.searchColumn += pi_package_finalDTO.packageNumberEn + " ";
        pi_package_finalDTO.searchColumn += pi_package_finalDTO.packageNumberBn + " ";
        pi_package_finalDTO.searchColumn += pi_package_finalDTO.packageNameEn + " ";
        pi_package_finalDTO.searchColumn += pi_package_finalDTO.packageNameBn + " ";
        pi_package_finalDTO.searchColumn += pi_package_finalDTO.insertedBy + " ";
        pi_package_finalDTO.searchColumn += pi_package_finalDTO.modifiedBy + " ";
    }

    @Override
    public void set(PreparedStatement ps, Pi_package_finalDTO pi_package_finalDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(pi_package_finalDTO);
        if (isInsert) {
            ps.setObject(++index, pi_package_finalDTO.iD);
        }
        ps.setObject(++index, pi_package_finalDTO.officeUnitId);
        ps.setObject(++index, pi_package_finalDTO.packageNumberEn);
        ps.setObject(++index, pi_package_finalDTO.packageNumberBn);
        ps.setObject(++index, pi_package_finalDTO.packageNameEn);
        ps.setObject(++index, pi_package_finalDTO.packageNameBn);
        ps.setObject(++index, pi_package_finalDTO.hasLot);
        ps.setObject(++index, pi_package_finalDTO.searchColumn);
        ps.setObject(++index, pi_package_finalDTO.insertedBy);
        ps.setObject(++index, pi_package_finalDTO.modifiedBy);
        ps.setObject(++index, pi_package_finalDTO.insertionDate);
        if (isInsert) {
            ps.setObject(++index, pi_package_finalDTO.isDeleted);
        }
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, pi_package_finalDTO.iD);
        }
    }

    @Override
    public Pi_package_finalDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Pi_package_finalDTO pi_package_finalDTO = new Pi_package_finalDTO();
            int i = 0;
            pi_package_finalDTO.iD = rs.getLong(columnNames[i++]);
            pi_package_finalDTO.officeUnitId = rs.getLong(columnNames[i++]);
            pi_package_finalDTO.packageNumberEn = rs.getString(columnNames[i++]);
            pi_package_finalDTO.packageNumberBn = rs.getString(columnNames[i++]);
            pi_package_finalDTO.packageNameEn = rs.getString(columnNames[i++]);
            pi_package_finalDTO.packageNameBn = rs.getString(columnNames[i++]);
            pi_package_finalDTO.hasLot = rs.getBoolean(columnNames[i++]);
            pi_package_finalDTO.searchColumn = rs.getString(columnNames[i++]);
            pi_package_finalDTO.insertedBy = rs.getString(columnNames[i++]);
            pi_package_finalDTO.modifiedBy = rs.getString(columnNames[i++]);
            pi_package_finalDTO.insertionDate = rs.getLong(columnNames[i++]);
            pi_package_finalDTO.isDeleted = rs.getInt(columnNames[i++]);
            pi_package_finalDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            return pi_package_finalDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Pi_package_finalDTO getDTOByID(long id) {
        Pi_package_finalDTO pi_package_finalDTO = null;
        try {
            pi_package_finalDTO = getDTOFromID(id);
            if (pi_package_finalDTO != null) {
                PiPackageLotFinalDAO piPackageLotFinalDAO = PiPackageLotFinalDAO.getInstance();
                List<PiPackageLotFinalDTO> piPackageLotFinalDTOList = (List<PiPackageLotFinalDTO>) piPackageLotFinalDAO.getDTOsByParent("pi_package_final_id", pi_package_finalDTO.iD);
                pi_package_finalDTO.piPackageLotFinalDTOList = piPackageLotFinalDTOList;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return pi_package_finalDTO;
    }

    @Override
    public String getTableName() {
        return "pi_package_final";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_package_finalDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_package_finalDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

    public boolean isPackageNumberExists(String name) {
        List<Pi_package_finalDTO> pi_package_finalList = Pi_package_finalRepository.getInstance().getPi_package_finalList();
        return pi_package_finalList.stream().anyMatch(e -> (e.packageNumberBn.equals(name)) ||
                (e.packageNumberEn.equals(name)));
    }

    // THIS FUNCTION CHECK FOR DUPLICATE EXCLUDING THE CURRENT PACKAGE.
    public boolean isPackageNumberExists(String name, long currentId) {
        List<Pi_package_finalDTO> pi_package_finalList = Pi_package_finalRepository.getInstance().getPi_package_finalList();
        return pi_package_finalList.stream().filter(dto -> dto.iD != currentId).anyMatch(e -> (e.packageNumberBn.equals(name)) ||
                (e.packageNumberEn.equals(name)));
    }

    public boolean isPackageNameExists(String name) {
        List<Pi_package_finalDTO> pi_package_finalList = Pi_package_finalRepository.getInstance().getPi_package_finalList();
        return pi_package_finalList.stream().anyMatch(e -> (e.packageNameBn.equals(name)) ||
                (e.packageNameEn.equals(name)));
    }

    // THIS FUNCTION CHECK FOR DUPLICATE EXCLUDING THE CURRENT PACKAGE.
    public boolean isPackageNameExists(String name, long currentId) {
        List<Pi_package_finalDTO> pi_package_finalList = Pi_package_finalRepository.getInstance().getPi_package_finalList();
        return pi_package_finalList.stream().filter(dto -> dto.iD != currentId).anyMatch(e -> (e.packageNameBn.equals(name)) ||
                (e.packageNameEn.equals(name)));
    }

    public String getName(String Language, long packageId){
        Pi_package_finalDTO dto = Pi_package_finalDAO.getInstance().getDTOByID(packageId);
        return dto == null ? "" : UtilCharacter.getDataByLanguage(Language, dto.packageNumberBn, dto.packageNumberEn);
    }
}
	