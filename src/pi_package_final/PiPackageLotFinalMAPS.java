package pi_package_final;
import java.util.*; 
import util.*;


public class PiPackageLotFinalMAPS extends CommonMaps
{	
	public PiPackageLotFinalMAPS(String tableName)
	{
		


		java_SQL_map.put("pi_package_new_id".toLowerCase(), "piPackageNewId".toLowerCase());
		java_SQL_map.put("office_unit_id".toLowerCase(), "officeUnitId".toLowerCase());
		java_SQL_map.put("lot_number_en".toLowerCase(), "lotNumberEn".toLowerCase());
		java_SQL_map.put("lot_number_bn".toLowerCase(), "lotNumberBn".toLowerCase());
		java_SQL_map.put("lot_name_en".toLowerCase(), "lotNameEn".toLowerCase());
		java_SQL_map.put("lot_name_bn".toLowerCase(), "lotNameBn".toLowerCase());
		java_SQL_map.put("inserted_by".toLowerCase(), "insertedBy".toLowerCase());
		java_SQL_map.put("modified_by".toLowerCase(), "modifiedBy".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Pi Package New Id".toLowerCase(), "piPackageNewId".toLowerCase());
		java_Text_map.put("Office Unit Id".toLowerCase(), "officeUnitId".toLowerCase());
		java_Text_map.put("Lot Number En".toLowerCase(), "lotNumberEn".toLowerCase());
		java_Text_map.put("Lot Number Bn".toLowerCase(), "lotNumberBn".toLowerCase());
		java_Text_map.put("Lot Name En".toLowerCase(), "lotNameEn".toLowerCase());
		java_Text_map.put("Lot Name Bn".toLowerCase(), "lotNameBn".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}