package board;

import common.BaseServlet;
import common.NameDao;
import common.NameInterface;
import common.NameRepository;
import language.LC;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

@SuppressWarnings({"rawtypes"})
@WebServlet("/BoardServlet")
@MultipartConfig
public class BoardServlet extends BaseServlet implements NameInterface {

    private final BoardDAO boardDAO = BoardDAO.getInstance();

    @Override
    public String getTableName() {
        return boardDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "BoardServlet";
    }

    @Override
    public NameDao getCommonDAOService() {
        return boardDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return addT(request,addFlag,userDTO,boardDAO);
    }

    public String commonPartOfDispatchURL(){
        return  "common/name";
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.BOARD_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.BOARD_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.BOARD_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return BoardServlet.class;
    }

    @Override
    public int getSearchTitleValue() {
        return LC.BOARD_SEARCH_BOARD_SEARCH_FORMNAME;
    }

    @Override
    public String get_p_navigatorName() {
        return SessionConstants.NAV_BOARD;
    }

    @Override
    public String get_p_dtoCollectionName() {
        return SessionConstants.VIEW_BOARD;
    }

    @Override
    public NameRepository getNameRepository() {
        return BoardRepository.getInstance();
    }

    @Override
    public int getAddTitleValue() {
        return LC.BOARD_ADD_BOARD_ADD_FORMNAME;
    }

    @Override
    public int getEditTitleValue() {
        return LC.BOARD_EDIT_BOARD_EDIT_FORMNAME;
    }

    @Override
    public void init(HttpServletRequest request) throws ServletException {
        setCommonAttributes(request,getServletName(),getCommonDAOService());
    }
}