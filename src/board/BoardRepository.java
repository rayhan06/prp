package board;

import common.NameRepository;

public class BoardRepository extends NameRepository {

    private BoardRepository() {
        super(BoardDAO.getInstance(),BoardRepository.class);
    }

    private static class BoardRepositoryLoader {
        static BoardRepository INSTANCE = new BoardRepository();
    }

    public static BoardRepository getInstance() {
        return BoardRepositoryLoader.INSTANCE;
    }
}
