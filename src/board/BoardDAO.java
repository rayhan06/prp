package board;

import common.NameDao;

public class BoardDAO extends NameDao {
	private BoardDAO() {
		super("board");
	}

	private static class LazyLoader{
		static final BoardDAO INSTANCE = new BoardDAO();
	}

	public static BoardDAO getInstance(){
		return LazyLoader.INSTANCE;
	}
}
