package pi_requisition;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class Pi_requisitionRepository implements Repository {
    Pi_requisitionDAO pi_requisitionDAO;

    static Logger logger = Logger.getLogger(Pi_requisitionRepository.class);
    Map<Long, Pi_requisitionDTO> mapOfPi_requisitionDTOToiD;
    Gson gson;


    private Pi_requisitionRepository() {
        pi_requisitionDAO = Pi_requisitionDAO.getInstance();
        mapOfPi_requisitionDTOToiD = new ConcurrentHashMap<>();
        gson = new Gson();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Pi_requisitionRepository INSTANCE = new Pi_requisitionRepository();
    }

    public static Pi_requisitionRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        try {
            List<Pi_requisitionDTO> pi_requisitionDTOs = pi_requisitionDAO.getAllDTOs(reloadAll);
            for (Pi_requisitionDTO pi_requisitionDTO : pi_requisitionDTOs) {
                Pi_requisitionDTO oldPi_requisitionDTO = getPi_requisitionDTOByiD(pi_requisitionDTO.iD);
                if (oldPi_requisitionDTO != null) {
                    mapOfPi_requisitionDTOToiD.remove(oldPi_requisitionDTO.iD);


                }
                if (pi_requisitionDTO.isDeleted == 0) {

                    mapOfPi_requisitionDTOToiD.put(pi_requisitionDTO.iD, pi_requisitionDTO);

                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public Pi_requisitionDTO clone(Pi_requisitionDTO dto) {
        String raw = gson.toJson(dto);
        return gson.fromJson(raw, Pi_requisitionDTO.class);
    }


    public List<Pi_requisitionDTO> getPi_requisitionList() {
        return new ArrayList<>(this.mapOfPi_requisitionDTOToiD.values());
    }

    public List<Pi_requisitionDTO> copyPi_requisitionList() {
        List<Pi_requisitionDTO> pi_requisitions = getPi_requisitionList();
        return pi_requisitions
                .stream()
                .map(this::clone)
                .collect(Collectors.toList());
    }


    public Pi_requisitionDTO getPi_requisitionDTOByiD(long iD) {
        return mapOfPi_requisitionDTOToiD.get(iD);
    }

    public Pi_requisitionDTO copyPi_requisitionDTOByiD(long iD) {
        return clone(mapOfPi_requisitionDTOToiD.get(iD));
    }


    @Override
    public String getTableName() {
        return pi_requisitionDAO.getTableName();
    }
}


