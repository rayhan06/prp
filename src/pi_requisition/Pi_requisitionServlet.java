package pi_requisition;

import common.ApiResponse;
import common.BaseServlet;
import common.CommonDAOService;
import common.RoleEnum;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import fiscal_year.Fiscal_yearDAO;
import login.LoginDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import pi_office_unit_store_keeper_mapping.Pi_office_unit_store_keeper_mappingDTO;
import pi_office_unit_store_keeper_mapping.Pi_office_unit_store_keeper_mappingRepository;
import pi_unique_item.*;
import procurement_goods.Procurement_goodsDTO;
import procurement_goods.Procurement_goodsRepository;
import procurement_goods.Procurement_goodsResponseDTO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.StringUtils;
import util.TimeUtils;
import util.UtilCharacter;
import vm_requisition.CommonApprovalStatus;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Servlet implementation class Pi_requisitionServlet
 */
@WebServlet("/Pi_requisitionServlet")
@MultipartConfig
public class Pi_requisitionServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_requisitionServlet.class);

    @Override
    public String getTableName() {
        return Pi_requisitionDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_requisitionServlet";
    }

    @Override
    public Pi_requisitionDAO getCommonDAOService() {
        return Pi_requisitionDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PI_REQUISITION_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PI_REQUISITION_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PI_REQUISITION_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_requisitionServlet.class;
    }


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLanguageEnglish = userDTO.languageID == SessionConstants.ENGLISH;
        Pi_requisitionDTO pi_requisitionDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy hh:mm aaa");

        if (addFlag) {
            pi_requisitionDTO = new Pi_requisitionDTO();
            pi_requisitionDTO.insertedBy = userDTO.employee_record_id;
            pi_requisitionDTO.insertionTime = new Date().getTime();
        } else {
            pi_requisitionDTO = Pi_requisitionDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        pi_requisitionDTO.modifiedBy = userDTO.employee_record_id;
        pi_requisitionDTO.lastModificationTime = new Date().getTime();

        pi_requisitionDTO.organogramId = Long.parseLong(Jsoup.clean(request.getParameter("organogramId"), Whitelist.simpleText()));

        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(pi_requisitionDTO.organogramId);
        if (officeUnitOrganograms == null) {
            throw new Exception(isLanguageEnglish ? "Organogram not Found!" : "পদবি পাওয়া যায় নি!");
        }
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(pi_requisitionDTO.organogramId);
        if (employeeOfficeDTO == null) {
            throw new Exception(isLanguageEnglish ? "Office information not Found!" : "অফিসের তথ্য পাওয়া যায় নি!");
        }
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
        if (officeUnitsDTO == null) {
            throw new Exception(isLanguageEnglish ? "Employee office unit not Found!" : "অফিস ইউনিটের তথ্য পাওয়া যায় নি!");
        }
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId);
        if (employeeRecordsDTO == null) {
            throw new Exception(isLanguageEnglish ? "Employee record not Found!" : "কর্মকর্তা/কর্মচারীর তথ্য পাওয়া যায় নি!");
        }
        pi_requisitionDTO.employeeRecordId = employeeRecordsDTO.iD;
        pi_requisitionDTO.requesterNameEng = employeeRecordsDTO.nameEng;
        pi_requisitionDTO.requesterNameBng = employeeRecordsDTO.nameBng;
        pi_requisitionDTO.officeUnitId = officeUnitsDTO.iD;
        pi_requisitionDTO.officeUnitEng = officeUnitsDTO.unitNameEng;
        pi_requisitionDTO.officeUnitBng = officeUnitsDTO.unitNameBng;
        pi_requisitionDTO.organogramId = officeUnitOrganograms.id;
        pi_requisitionDTO.organogramEng = officeUnitOrganograms.designation_eng;
        pi_requisitionDTO.organogramBng = officeUnitOrganograms.designation_bng;

        /*is personal start*/

        if (!StringUtils.isValidString(request.getParameter("is_personal"))) {
            throw new Exception(isLanguageEnglish ? "Please select requisition type" : "অনুগ্রহপূর্বক সরবরাহের ধরন বাছাই করুন");
        }
        pi_requisitionDTO.isPersonal = Integer.parseInt(request.getParameter("is_personal"));

        /*is personal end*/


        /*store keeper added in request start*/

        if (!StringUtils.isValidString(request.getParameter("officeUnitId"))) {
            throw new Exception(isLanguageEnglish ? "No office selected as office" : "স্টোর পাওয়া যায় নি");
        }
        long storeOfficeId = Long.parseLong(request.getParameter("officeUnitId"));
        Pi_office_unit_store_keeper_mappingDTO storeKeeperMapping = Pi_office_unit_store_keeper_mappingRepository.getInstance()
                .copyPi_office_unit_store_keeper_mappingDTOByOfficeUnitId(storeOfficeId);
        if (storeKeeperMapping == null) {
            throw new Exception(isLanguageEnglish ? "No store keeper found for this office" : "স্টোর কীপার নেই");
        }
        EmployeeOfficeDTO storeKeeperOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(storeKeeperMapping.organogramId);

        if (storeKeeperOfficeDTO == null) {
            throw new Exception(isLanguageEnglish ? "Store keeper's office record not Found!" : "স্টোর কিপারের অফিসের তথ্য পাওয়া যায় নি!");
        }
        OfficeUnitOrganograms storeKeeperOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(storeKeeperOfficeDTO.officeUnitOrganogramId);
        if (storeKeeperOrganograms == null) {
            throw new Exception(isLanguageEnglish ? "Store keeper's organogram not Found!" : "স্টোর কিপারের পদবি পাওয়া যায় নি!");
        }
        pi_requisitionDTO.storeKeeperId = storeKeeperOfficeDTO.employeeRecordId;
        pi_requisitionDTO.storeKeeperOfficeUnitId = storeKeeperOfficeDTO.officeUnitId;
        pi_requisitionDTO.storeKeeperOrganogramId = storeKeeperOrganograms.id;
        pi_requisitionDTO.storeKeeperRganogramEng = storeKeeperOrganograms.designation_eng;
        pi_requisitionDTO.storeKeeperOrganogramBng = storeKeeperOrganograms.designation_bng;

        /*store keeper added in request end*/


        OfficeUnitOrganograms superiorOrganograms = OfficeUnitOrganogramsRepository.getInstance().
                getImmediateSuperiorToOrganogramIdWithActiveEmpStatusAndConsiderSubstitute(pi_requisitionDTO.organogramId);
        if (superiorOrganograms == null) {
            throw new Exception(isLanguageEnglish ? "Employee's superior organogram not Found!" : "উপরস্থ কর্মকর্তার তথ্য পাওয়া যায় নি!");
        }
        EmployeeOfficeDTO superiorOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(superiorOrganograms.id);
        if (superiorOfficeDTO == null) {
            throw new Exception(isLanguageEnglish ? "Employee's superior office record not Found!" : "উপরস্থ কর্মকর্তার অফিসের তথ্য পাওয়া যায় নি!");
        }
        pi_requisitionDTO.approvalOneId = superiorOfficeDTO.employeeRecordId;
        pi_requisitionDTO.approvalOneOrganogramId = superiorOrganograms.id;
        pi_requisitionDTO.approvalOneOrganogramEng = superiorOrganograms.designation_eng;
        pi_requisitionDTO.approvalOneOrganogramBng = superiorOrganograms.designation_bng;
//        long secondApproverUnitId = pi_requisitionDTO.storeKeeperOfficeUnitId;
//        if (pi_requisitionDTO.officeUnitId < 10) {
//            secondApproverUnitId = 47; // for employees under speaker
//        }
//        OfficeUnitOrganograms secondApproverOrganogram = OfficeUnitOrganogramsRepository.getInstance().
//                getOfficeUnitHeadWithActiveEmpStatusAndNotConsiderSubstitute(secondApproverUnitId);
//        if (secondApproverOrganogram == null) {
//            throw new Exception(isLanguageEnglish ? "Admin approver's organogram not Found!" : "এডমিন অনুমোদনকারীর পদবি পাওয়া যায় নি!");
//        }
//        EmployeeOfficeDTO secondApproverOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(secondApproverOrganogram.id);
//        if (secondApproverOfficeDTO == null) {
//            throw new Exception(isLanguageEnglish ? "Admin approver's office record not Found!" : "এডমিন অনুমোদনকারীর অফিসের তথ্য পাওয়া যায় নি!");
//        }
//        pi_requisitionDTO.approvalTwoId = secondApproverOfficeDTO.employeeRecordId;
//        pi_requisitionDTO.approvalTwoOrganogramId = secondApproverOrganogram.id;
//        pi_requisitionDTO.approvalTwoOrganogramEng = secondApproverOrganogram.designation_eng;
//        pi_requisitionDTO.approvalTwoOrganogramBng = secondApproverOrganogram.designation_bng;

        List<Long> adminOneChildOffice = Arrays.asList(43L, 45L, 47L);
        long adminOneOfficeId = 42;
        long adminTwoOfficeId = 49;
        long selectedAdminOfficeIdForApproval = -1;
        if (adminOneChildOffice.contains(storeOfficeId)) {
            selectedAdminOfficeIdForApproval = adminOneOfficeId;
        } else {
            selectedAdminOfficeIdForApproval = adminTwoOfficeId;
        }
        OfficeUnitOrganograms secondApproverOrganogram = OfficeUnitOrganogramsRepository.getInstance().
                getOfficeUnitHeadWithActiveEmpStatusAndNotConsiderSubstitute(selectedAdminOfficeIdForApproval);
        if (secondApproverOrganogram == null) {
            throw new Exception(isLanguageEnglish ? "Admin approver's organogram not Found!" : "এডমিন অনুমোদনকারীর পদবি পাওয়া যায় নি!");
        }
        EmployeeOfficeDTO secondApproverOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(secondApproverOrganogram.id);
        pi_requisitionDTO.approvalTwoId = secondApproverOfficeDTO.employeeRecordId;
        pi_requisitionDTO.approvalTwoOrganogramId = secondApproverOrganogram.id;
        pi_requisitionDTO.approvalTwoOrganogramEng = secondApproverOrganogram.designation_eng;
        pi_requisitionDTO.approvalTwoOrganogramBng = secondApproverOrganogram.designation_bng;

        Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();


        /*String applyTime = Jsoup.clean(request.getParameter("applyTime"), Whitelist.simpleText());
        if (applyTime == null || applyTime.trim().length() == 0) {
            applyTime = TimeUtils.DATE_FORMAT_TIME_AM_PM.format(new Date());
        }*/

        String applyTime = TimeUtils.DATE_FORMAT_TIME_AM_PM.format(new Date());
        pi_requisitionDTO.applyTime = applyTime;
        String applyDate = Jsoup.clean(request.getParameter("applyDate"), Whitelist.simpleText()) + " " + applyTime;
        Date d = f.parse(applyDate);
        pi_requisitionDTO.applyDate = d.getTime();

        pi_requisitionDTO.comment = (Jsoup.clean(request.getParameter("remarks"), Whitelist.simpleText()));
        pi_requisitionDTO.fiscalYear = fiscal_yearDAO.getFiscalYearBYDateLong(pi_requisitionDTO.applyDate).id;

        //pi_requisitionDTO.status = CommonApprovalStatus.REQUISITION_NOT_APPROVED.getValue();
        pi_requisitionDTO.status = CommonApprovalStatus.REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue();

        OfficeUnitOrganograms requisitionOrganogram = OfficeUnitOrganogramsRepository.getInstance().getById(pi_requisitionDTO.organogramId);

        if (superiorOfficeDTO.officeUnitId == 1
                || ((Office_unitsRepository.getInstance().isSAsJsDsSas(pi_requisitionDTO.officeUnitId))
                && requisitionOrganogram.orgTree.endsWith("|"))
                || pi_requisitionDTO.officeUnitId < 10
                || Employee_recordsRepository.getInstance().getById(pi_requisitionDTO.employeeRecordId).employeeClass == 1
        ) {
            pi_requisitionDTO.status = CommonApprovalStatus.REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue(); // For employee under speaker
//            pi_requisitionDTO.status = CommonApprovalStatus.REQUISITION_APPROVED_BY_SECOND_APPROVER.getValue(); // For employee under speaker
        }


        System.out.println("Done adding  addPi_requisition dto = " + pi_requisitionDTO);
        String Value = request.getParameter("productData");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        Procurement_goodsResponseDTO[] items = gson.fromJson(Value, Procurement_goodsResponseDTO[].class);

        if (items.length < 1) {
            throw new Exception(isLanguageEnglish ? "Please at least one item" : "অন্তত একটি আইটেম বাছাই করুন");
        }

        boolean gotOneItemQuantity = true;
        for (Procurement_goodsResponseDTO item : items) {
            gotOneItemQuantity &= item.quantity > 0;
        }

        if (!gotOneItemQuantity) {
            throw new Exception(isLanguageEnglish ? "Please at least one item" : "অন্তত একটি আইটেম বাছাই করুন");
        }

        Utils.handleTransaction(() -> {
            if (addFlag) {
                Pi_requisitionDAO.getInstance().add(pi_requisitionDTO);
            } else {
                Pi_requisitionDAO.getInstance().update(pi_requisitionDTO);
            }
            if (addFlag) {
                for (Procurement_goodsResponseDTO item : items) {
                    Pi_requisition_itemDTO pi_requisition_itemDTO = buildRequisitionItem(item, pi_requisitionDTO, userDTO);
//                int stock_amount = PiUniqueItemAssignmentDAO.getInstance().getInitialStockAtDate(pi_requisitionDTO.officeUnitId, pi_requisition_itemDTO.fiscalYear, pi_requisition_itemDTO.itemId, pi_requisition_itemDTO.apply_date);
//                if (pi_requisition_itemDTO.requestedQuantity > stock_amount) {
//                    throw new Exception(isLanguageEnglish ? "Not enough in stock" : "পর্যাপ্ত মজুদে নেই");
//                }
                    Pi_requisition_itemDAO.getInstance().add(pi_requisition_itemDTO);
                }
            } else {
                List<Pi_requisition_itemDTO> Old_requisition_itemDTOList = Pi_requisition_itemDAO.getInstance().getDTOsByRequisitionId(pi_requisitionDTO.iD);
                Set<Long> ids = new HashSet<>();
                for (Procurement_goodsResponseDTO item : items) {
                    Pi_requisition_itemDTO pi_requisition_itemDTO = Pi_requisition_itemDAO.getInstance().getDTOsByRequisitionIdAndItemId(pi_requisitionDTO.iD, item.iD);
                    if (pi_requisition_itemDTO == null) {
                        pi_requisition_itemDTO = buildRequisitionItem(item, pi_requisitionDTO, userDTO);
//                    int stock_amount = PiUniqueItemAssignmentDAO.getInstance().getInitialStockAtDate(pi_requisitionDTO.officeUnitId, pi_requisition_itemDTO.fiscalYear, pi_requisition_itemDTO.itemId, pi_requisition_itemDTO.apply_date);
//                    if (pi_requisition_itemDTO.requestedQuantity > stock_amount) {
//                        throw new Exception(isLanguageEnglish ? "Not enough in stock" : "পর্যাপ্ত মজুদে নেই");
//                    }
                        Pi_requisition_itemDAO.getInstance().add(pi_requisition_itemDTO);
                    } else {
                        ids.add(pi_requisition_itemDTO.iD);
                        pi_requisition_itemDTO.requestedQuantity = (int) item.quantity;
                        pi_requisition_itemDTO.modifiedBy = userDTO.employee_record_id;
                        pi_requisition_itemDTO.lastModificationTime = new Date().getTime();
//                    int stock_amount = PiUniqueItemAssignmentDAO.getInstance().getInitialStockAtDate(pi_requisitionDTO.officeUnitId, pi_requisition_itemDTO.fiscalYear, pi_requisition_itemDTO.itemId, pi_requisition_itemDTO.apply_date);
//                    if (pi_requisition_itemDTO.requestedQuantity > stock_amount) {
//                        throw new Exception(isLanguageEnglish ? "Not enough in stock" : "পর্যাপ্ত মজুদে নেই");
//                    }
                        Pi_requisition_itemDAO.getInstance().update(pi_requisition_itemDTO);
                    }
                }

                List<Pi_requisition_itemDTO> deletedList = Old_requisition_itemDTOList.stream()
                        .filter(e -> !ids.contains(e.iD))
                        .peek(dto -> dto.isDeleted = 1)
                        .collect(Collectors.toList());
                for (Pi_requisition_itemDTO pi_requisition_itemDTO : deletedList) {
                    Pi_requisition_itemDAO.getInstance().delete(userDTO.employee_record_id, pi_requisition_itemDTO.iD);
                }
            }
            Pi_requisition_notification.getInstance().sendRequesterNotification(pi_requisitionDTO);
//            Pi_requisition_notification.getInstance().sendApproverNotification(pi_requisitionDTO, "getSupplyPage");
            Pi_requisition_notification.getInstance().sendApproverNotification(pi_requisitionDTO, "getSecondApproverPage");

            /*if (pi_requisitionDTO.status == CommonApprovalStatus.REQUISITION_NOT_APPROVED.getValue()) {
                Pi_requisition_notification.getInstance().sendApproverNotification(pi_requisitionDTO, "getFirstApproverPage");
            } else {
                Pi_requisition_notification.getInstance().sendApproverNotification(pi_requisitionDTO, "getSecondApproverPage");
            }*/
        });


        return pi_requisitionDTO;
    }

    private void filterAddForDashboard(HttpServletRequest request, String filter) {
        Map<String, String> extraCriteriaMap;
        String STATUS = "status_in";
        if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) == null) {
            extraCriteriaMap = new HashMap<>();
        } else {
            extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
        }
        if (filter.equalsIgnoreCase("supplied")) {
            extraCriteriaMap.put(STATUS,
                    CommonApprovalStatus.REQUISITION_SUPPLIED_BY_STORE_KEEPER.getValue() + "");
        } else if (filter.equalsIgnoreCase("pending")) {
            extraCriteriaMap.put(STATUS,
                    CommonApprovalStatus.REQUISITION_NOT_APPROVED.getValue() + "");
        } else if (filter.equalsIgnoreCase("second_pending")) {
            extraCriteriaMap.put(STATUS,
                    CommonApprovalStatus.REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue() + "");
        } else if (filter.equalsIgnoreCase("rejected")) {
            extraCriteriaMap.put(STATUS,
                    CommonApprovalStatus.REQUISITION_REJECTED_BY_FIRST_APPROVER.getValue() + ", "
                            + CommonApprovalStatus.REQUISITION_REJECTED_BY_SECOND_APPROVER.getValue());
        } else if (filter.equalsIgnoreCase("approved")) {
            extraCriteriaMap.put(STATUS,
                    CommonApprovalStatus.REQUISITION_APPROVED_BY_SECOND_APPROVER.getValue() + "");
        }

        request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            long roleId = userDTO.roleID;
            switch (actionType) {
                case "search":
                    if (roleId != SessionConstants.INVENTORY_ADMIN_ROLE && roleId != SessionConstants.ADMIN_ROLE) {
                        Map<String, String> extraCriteriaMap;
                        if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) == null) {
                            extraCriteriaMap = new HashMap<>();
                        } else {
                            extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
                        }
                        extraCriteriaMap.put("employee_records_id_internal", String.valueOf(userDTO.employee_record_id));
                        request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                    }

                    String filter = request.getParameter("filter");
                    if (filter != null) {
                        filterAddForDashboard(request, filter);
                    }

                    String officeUnitId = request.getParameter("officeUnitId");
                    if (officeUnitId != null) {
                        long unitId = Long.parseLong(officeUnitId);
                        Map<String, String> extraCriteriaMap;
                        if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) == null) {
                            extraCriteriaMap = new HashMap<>();
                        } else {
                            extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
                        }
                        extraCriteriaMap.put("office_unit_id", String.valueOf(unitId));
                        request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                    }

                    super.doGet(request, response);
                    return;
                case "getFirstApproverPage":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_REQUISITION_SEARCH_FIRST_APPROVER)) {

                        Pi_requisitionDTO requisitionDTO = getCommonDAOService().getDTOFromID(getId(request));
                        if (requisitionDTO == null || requisitionDTO.approvalOneId != userDTO.employee_record_id) {
                            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                            return;
                        }

                        request.setAttribute(BaseServlet.DTO_FOR_JSP, requisitionDTO);
                        finalize(request);
                        request.getRequestDispatcher(commonPartOfDispatchURL() + "FirstApprover.jsp?actionType=edit").forward(request, response);

                        return;
                    }
                    break;
                case "getFirstApproverList":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_REQUISITION_SEARCH_FIRST_APPROVER)) {
                        Map<String, String> extraCriteriaMap = new HashMap<>();
                        extraCriteriaMap.put("approval_one_id", String.valueOf(userDTO.employee_record_id));
                        request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                        search(request, response);
                        return;
                    }
                    break;
                case "getSecondApproverPage":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_REQUISITION_SEARCH_SECOND_APPROVER)) {
                        Pi_requisitionDTO requisitionDTO = getCommonDAOService().getDTOFromID(getId(request));
                        if (requisitionDTO == null || requisitionDTO.approvalTwoId != userDTO.employee_record_id) {
                            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                            return;
                        }
                        request.setAttribute(BaseServlet.DTO_FOR_JSP, requisitionDTO);
                        finalize(request);
                        request.getRequestDispatcher(commonPartOfDispatchURL() + "SecondApprover.jsp?actionType=edit").forward(request, response);

                        return;
                    }
                    break;
                case "getSecondApproverList":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_REQUISITION_SEARCH_SECOND_APPROVER)) {

                        Map<String, String> extraCriteriaMap = new HashMap<>();
                        extraCriteriaMap.put("approval_two_id", String.valueOf(userDTO.employee_record_id));
                        request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                        search(request, response);

                        return;
                    }
                    break;
                case "getSupplyPage":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_REQUISITION_SEARCH_SUPPLY)) {
                        long storeKeeperOrgId = Pi_office_unit_store_keeper_mappingRepository
                                .getInstance()
                                .copyPi_office_unit_store_keeper_mappingDTOByOfficeUnitId(userDTO.unitID)
                                .organogramId;
                        if (userDTO.organogramID == storeKeeperOrgId || roleId == SessionConstants.INVENTORY_ADMIN_ROLE || roleId == SessionConstants.ADMIN_ROLE) {
                            Pi_requisitionDTO requisitionDTO = getCommonDAOService().getDTOFromID(getId(request));
//                            || requisitionDTO.storeKeeperId != userDTO.employee_record_id
                            if (requisitionDTO == null) {
                                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                                return;
                            }
                            request.setAttribute(BaseServlet.DTO_FOR_JSP, requisitionDTO);
                            request.getRequestDispatcher(commonPartOfDispatchURL() + "Supply.jsp?actionType=edit").forward(request, response);
                        } else {
                            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                        }
                        return;
                    }
                    break;
                case "getSupplyList":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_REQUISITION_SEARCH_SUPPLY)) {
                        roleId = userDTO.roleID;
                        if (roleId == SessionConstants.INVENTORY_ADMIN_ROLE || roleId == SessionConstants.ADMIN_ROLE) {
                            search(request, response);
                            return;
                        }
                        Map<String, String> extraCriteriaMap = new HashMap<>();
                        extraCriteriaMap.put("store_keeper_id", String.valueOf(userDTO.employee_record_id));
                        request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                        search(request, response);
                        return;
                    }
                    break;
                case "getPreviousRequisitionItem":
                    if (Utils.checkPermission(userDTO, getAddPageMenuConstants()) && getAddPagePermission(request)) {
                        long itemId = Long.parseLong(request.getParameter("itemId"));
                        long organogramId = Long.parseLong(Jsoup.clean(request.getParameter("organogramId"), Whitelist.simpleText()));
                        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(organogramId);
                        Pi_requisition_itemDTO pi_requisition_itemDTO = Pi_requisition_itemDAO.getInstance().getDTOsByItemId(itemId, employeeOfficeDTO.employeeRecordId, System.currentTimeMillis());
                        int totalInStock = PiUniqueItemAssignmentDAO.getInstance().getInitialStockAtDate(employeeOfficeDTO.officeUnitId, new Fiscal_yearDAO().getFiscalYearBYDateLong(System.currentTimeMillis()).id, itemId, System.currentTimeMillis());

                        PrintWriter out = response.getWriter();
                        if (pi_requisition_itemDTO == null) {
                            pi_requisition_itemDTO = new Pi_requisition_itemDTO();
                        }
                        Map<String, Object> res = new HashMap<>();
                        res.put("pi_requisition_itemDTO", pi_requisition_itemDTO);
                        res.put("totalInStock", totalInStock);
                        response.setContentType("application/json");
                        response.setCharacterEncoding("UTF-8");
                        out.println(gson.toJson(res));
                        out.close();
                        return;
                    }
                    break;
                case "getDTOsByRequisitionId":
                    if (Utils.checkPermission(userDTO, getAddPageMenuConstants()) && getAddPagePermission(request)) {
                        long ID = -1;
                        long officeUnitIdLong = -1;
                        if (request.getParameter("ID") != null) {
                            ID = Long.parseLong(request.getParameter("ID"));
                            officeUnitIdLong = Long.parseLong(request.getParameter("officeUnitId"));
                        }
                        System.out.println("ID = " + ID);
                        List<Pi_requisition_itemDTO> pi_requisition_itemDTOList = Pi_requisition_itemDAO.getInstance().getDTOsByRequisitionId(ID);
                        for (Pi_requisition_itemDTO pi_requisition_itemDTO : pi_requisition_itemDTOList) {
                            pi_requisition_itemDTO.stockAmount = PiUniqueItemAssignmentDAO.getInstance().getTotalItemCountWithStage(officeUnitIdLong, pi_requisition_itemDTO.itemId, PiStageEnum.IN_STOCK.getValue(), pi_requisition_itemDTO.apply_date);
                        }
                        Map<String, Object> res = new HashMap<>();
                        res.put("pi_requisition_itemDTOList", pi_requisition_itemDTOList);
                        response.setContentType("application/json");
                        response.setCharacterEncoding("UTF-8");
                        PrintWriter out = response.getWriter();
                        out.println(gson.toJson(pi_requisition_itemDTOList));
                        out.close();
                        return;
                    }
                    break;
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        boolean isLanguageEnglish = userDTO.languageID == SessionConstants.ENGLISH;
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "ajax_approved_first_approver":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_REQUISITION_SEARCH_FIRST_APPROVER)) {
                        try {
                            Utils.handleTransaction(() -> {
                                Pi_requisitionDTO requisitionDTO = getCommonDAOService().getDTOFromID(getId(request));
                                if (requisitionDTO == null || requisitionDTO.approvalOneId != userDTO.employee_record_id) {
                                    throw new Exception(isLanguageEnglish ? "not for approval" : "অনুমোদনের জন্য নয়");
                                }
                                if (requisitionDTO.status == CommonApprovalStatus.REQUISITION_NOT_APPROVED.getValue()) {
                                    requisitionDTO.status = CommonApprovalStatus.REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue();
                                    requisitionDTO.approvalOneDate = System.currentTimeMillis();
                                    updateForFirstApproval(request, userDTO, requisitionDTO);
                                    Pi_requisitionDAO.getInstance().update(requisitionDTO);
                                    Pi_requisition_notification.getInstance().sendApprovedNotification(requisitionDTO, " First Approver", "প্রথম অনুমোদনকারী");
                                    Pi_requisition_notification.getInstance().sendApproverNotification(requisitionDTO, "getSecondApproverPage");
                                    ApiResponse.sendSuccessResponse(response, "Pi_requisitionServlet?actionType=getFirstApproverList");
                                } else {
                                    throw new Exception(isLanguageEnglish ? "not for approval" : "অনুমোদনের জন্য নয়");
                                }
                            });
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }

                        return;
                    }
                    break;
                case "ajax_reject_first_approver":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_REQUISITION_SEARCH_FIRST_APPROVER)) {
                        if (userDTO.roleID == RoleEnum.EMPLOYEE_OF_PARLIAMENT.getRoleId()) {
                            try {
                                Utils.handleTransaction(() -> {
                                    Pi_requisitionDTO requisitionDTO = getCommonDAOService().getDTOFromID(getId(request));
                                    if (requisitionDTO == null || requisitionDTO.approvalOneId != userDTO.employee_record_id) {
                                        throw new Exception(isLanguageEnglish ? "not for rejection" : "বাতিলের জন্য নয়");
                                    }
                                    if (requisitionDTO.status == CommonApprovalStatus.REQUISITION_NOT_APPROVED.getValue()) {
                                        requisitionDTO.status = CommonApprovalStatus.REQUISITION_REJECTED_BY_FIRST_APPROVER.getValue();
                                        changeStatus(userDTO, requisitionDTO);
                                        Pi_requisitionDAO.getInstance().update(requisitionDTO);
                                        Pi_requisition_notification.getInstance().sendRejectNotification(requisitionDTO);
                                        ApiResponse.sendSuccessResponse(response, "Pi_requisitionServlet?actionType=getFirstApproverList");
                                    } else {
                                        throw new Exception("not for rejection");
                                    }
                                });
                            } catch (Exception ex) {
                                logger.error(ex);
                                ApiResponse.sendErrorResponse(response, ex.getMessage());
                            }
                        } else {
                            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                        }
                        return;
                    }
                    break;
                case "ajax_approved_second_approver":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_REQUISITION_SEARCH_SECOND_APPROVER)) {
                        try {
                            Utils.handleTransaction(() -> {
                                Pi_requisitionDTO requisitionDTO = getCommonDAOService().getDTOFromID(getId(request));
                                if (requisitionDTO == null || requisitionDTO.approvalTwoId != userDTO.employee_record_id) {
                                    throw new Exception(isLanguageEnglish ? "not for approval" : "অনুমোদনের জন্য নয়");
                                }
                                if (requisitionDTO.status == CommonApprovalStatus.REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue()) {
                                    requisitionDTO.status = CommonApprovalStatus.REQUISITION_APPROVED_BY_SECOND_APPROVER.getValue();
                                    requisitionDTO.approvalTwoDate = System.currentTimeMillis();
                                    updateForSecondApproval(request, userDTO, requisitionDTO);
                                    Pi_requisitionDAO.getInstance().update(requisitionDTO);
//                                    Pi_requisition_notification.getInstance().sendSecondApproverApprovedNotification(requisitionDTO, " Second Approver", "দ্বিতীয় অনুমোদনকারী");
                                    Pi_requisition_notification.getInstance().sendApproverNotification(requisitionDTO, "getSupplyPage");
                                    Pi_requisition_notification.getInstance().sendApprovedNotification(requisitionDTO, "Approver", "অনুমোদনকারী");
                                    ApiResponse.sendSuccessResponse(response, "Pi_requisitionServlet?actionType=getSecondApproverList");
                                } else {
                                    throw new Exception(isLanguageEnglish ? "not for approval" : "অনুমোদনের জন্য নয়");
                                }
                            });
                        } catch (Exception ex) {
                            logger.error(ex);
                            ex.printStackTrace();
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }

                        return;
                    }
                    break;
                case "ajax_reject_second_approver":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_REQUISITION_SEARCH_SECOND_APPROVER)) {
                        try {
                            Utils.handleTransaction(() -> {
                                Pi_requisitionDTO requisitionDTO = getCommonDAOService().getDTOFromID(getId(request));
                                if (requisitionDTO == null || requisitionDTO.approvalTwoId != userDTO.employee_record_id) {
                                    throw new Exception(isLanguageEnglish ? "not for rejection" : "বাতিলের জন্য নয়");
                                }
                                if (requisitionDTO.status == CommonApprovalStatus.REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue()) {
                                    requisitionDTO.status = CommonApprovalStatus.REQUISITION_REJECTED_BY_SECOND_APPROVER.getValue();
                                    changeStatus(userDTO, requisitionDTO);

                                    Pi_requisitionDAO.getInstance().update(requisitionDTO);
                                    Pi_requisition_notification.getInstance().sendRejectNotification(requisitionDTO);
                                    ApiResponse.sendSuccessResponse(response, "Pi_requisitionServlet?actionType=getSecondApproverList");
                                } else {
                                    throw new Exception(isLanguageEnglish ? "not for rejection" : "বাতিলের জন্য নয়");
                                }
                            });
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }

                        return;
                    }
                    break;
                case "ajax_supply":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_REQUISITION_SEARCH_SUPPLY)) {
                        try {
                            Utils.handleTransaction(() -> {
                                Pi_requisitionDTO requisitionDTO = getCommonDAOService().getDTOFromID(getId(request));
                                String value = "";
                                value = request.getParameter("disburseComment");
                                if (StringUtils.isValidString(value)) {
                                    value = Jsoup.clean(value, Whitelist.simpleText());
                                    requisitionDTO.disburseComment = value;
                                }
                                if (requisitionDTO == null || requisitionDTO.storeKeeperId != userDTO.employee_record_id) {
                                    throw new Exception(isLanguageEnglish ? "not for supply" : "সরবরাহের জন্য নয়");
                                }
                                if (requisitionDTO.status == CommonApprovalStatus.REQUISITION_APPROVED_BY_SECOND_APPROVER.getValue()) {
                                    requisitionDTO.status = CommonApprovalStatus.REQUISITION_SUPPLIED_BY_STORE_KEEPER.getValue();
                                    requisitionDTO.storeKeeperDate = System.currentTimeMillis();
                                    changeStatus(userDTO, requisitionDTO);
                                    Pi_unique_item_transactionDAO.getInstance().recordItemsTransactionAfterRequisition(requisitionDTO, userDTO);
                                    sendNotificationOnLessStock(userDTO, requisitionDTO);
                                    Pi_requisitionDAO.getInstance().update(requisitionDTO);
                                    Pi_requisition_notification.getInstance().sendSupplyNotification(requisitionDTO);
                                    ApiResponse.sendSuccessResponse(response, "Pi_requisitionServlet?actionType=getSupplyList");
                                } else {
                                    throw new Exception(isLanguageEnglish ? "not for supply" : "সরবরাহের জন্য নয়");
                                }
                            });
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }

                        return;
                    }
                    break;
                default:
                    super.doPost(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    Pi_requisition_itemDTO buildRequisitionItem(Procurement_goodsResponseDTO item, Pi_requisitionDTO pi_requisitionDTO, UserDTO userDTO) {
        Pi_requisition_itemDTO pi_requisition_itemDTO = new Pi_requisition_itemDTO();
        pi_requisition_itemDTO.piRequisitionId = pi_requisitionDTO.iD;
        pi_requisition_itemDTO.requesterEmployeeRecordId = pi_requisitionDTO.employeeRecordId;
        pi_requisition_itemDTO.itemId = item.iD;
        pi_requisition_itemDTO.itemNameEn = item.nameEn;
        pi_requisition_itemDTO.itemNameBn = item.nameBn;
        pi_requisition_itemDTO.itemTypeId = item.procurementGoodsTypeId;
        pi_requisition_itemDTO.itemTypeNameEn = item.procurementGoodsTypeNameEn;
        pi_requisition_itemDTO.itemTypeNameBn = item.procurementGoodsTypeNameBn;
        pi_requisition_itemDTO.requestedQuantity = (int) item.quantity;
//        pi_requisition_itemDTO.approvalOneQuantity = (int) item.quantity;
//        pi_requisition_itemDTO.approvalTwoQuantity = (int) item.quantity;
        pi_requisition_itemDTO.fiscalYear = new Fiscal_yearDAO().getFiscalYearBYDateLong(pi_requisitionDTO.applyDate).id;
        pi_requisition_itemDTO.apply_date = pi_requisitionDTO.applyDate;
        pi_requisition_itemDTO.status = pi_requisitionDTO.status;
        pi_requisition_itemDTO.isPersonal = pi_requisitionDTO.isPersonal;
        pi_requisition_itemDTO.insertedBy = userDTO.employee_record_id;
        pi_requisition_itemDTO.insertionTime = new Date().getTime();
        pi_requisition_itemDTO.modifiedBy = userDTO.employee_record_id;
        pi_requisition_itemDTO.lastModificationTime = new Date().getTime();

        return pi_requisition_itemDTO;
    }

    void changeStatus(UserDTO userDTO, Pi_requisitionDTO pi_requisitionDTO) throws Exception {
        List<Pi_requisition_itemDTO> pi_requisition_itemDTOList = Pi_requisition_itemDAO.getInstance().getDTOsByRequisitionId(pi_requisitionDTO.iD);
        for (Pi_requisition_itemDTO pi_requisition_itemDTO : pi_requisition_itemDTOList) {
            pi_requisition_itemDTO.status = pi_requisitionDTO.status;
            pi_requisition_itemDTO.modifiedBy = userDTO.employee_record_id;
            pi_requisition_itemDTO.lastModificationTime = new Date().getTime();
            Pi_requisition_itemDAO.getInstance().update(pi_requisition_itemDTO);

            List<PiUniqueItemAssignmentDTO> piUniqueItemAssignmentDTOList = PiUniqueItemAssignmentDAO.getInstance().
                    getInStockDTOs(pi_requisitionDTO.storeKeeperOfficeUnitId, pi_requisition_itemDTO.itemId, pi_requisition_itemDTO.approvalTwoQuantity);
            validateItemCount(piUniqueItemAssignmentDTOList, pi_requisition_itemDTO.approvalTwoQuantity);
            for (PiUniqueItemAssignmentDTO piUniqueItemAssignmentDTO : piUniqueItemAssignmentDTOList) {
                piUniqueItemAssignmentDTO.stage = PiStageEnum.OUT_STOCK.getValue();
                //piUniqueItemAssignmentDTO.actionType = PiActionTypeEnum.DELIVERY.getValue();
                PiUniqueItemAssignmentDAO.getInstance().update(piUniqueItemAssignmentDTO);

                PiUniqueItemAssignmentDTO requisitionItem = new PiUniqueItemAssignmentDTO();
                requisitionItem.piPurchaseId = piUniqueItemAssignmentDTO.piPurchaseId;
                requisitionItem.piReceivedId = piUniqueItemAssignmentDTO.piReceivedId;
                requisitionItem.piUniqueItemId = piUniqueItemAssignmentDTO.piUniqueItemId;
                requisitionItem.officeUnitId = piUniqueItemAssignmentDTO.officeUnitId;
                requisitionItem.fiscalYearId = piUniqueItemAssignmentDTO.fiscalYearId;
                requisitionItem.fromDate = pi_requisitionDTO.storeKeeperDate;
                requisitionItem.purchaseDate = piUniqueItemAssignmentDTO.purchaseDate;
                requisitionItem.receiveDate = piUniqueItemAssignmentDTO.receiveDate;
                requisitionItem.piRequisitionId = pi_requisition_itemDTO.iD;
                requisitionItem.orgId = pi_requisitionDTO.organogramId;
                requisitionItem.itemId = piUniqueItemAssignmentDTO.itemId;
                requisitionItem.actionType = PiActionTypeEnum.DELIVERY.getValue();
                requisitionItem.stage = PiStageEnum.OUT_STOCK.getValue();
                requisitionItem.insertedBy = userDTO.employee_record_id;
                requisitionItem.insertionTime = new Date().getTime();
                requisitionItem.modifiedBy = userDTO.employee_record_id;
                requisitionItem.lastModificationTime = new Date().getTime();
                requisitionItem.requisitionOfficeUnitId = pi_requisitionDTO.officeUnitId;
                requisitionItem.returnable = piUniqueItemAssignmentDTO.returnable;
                PiUniqueItemAssignmentDAO.getInstance().add(requisitionItem);
            }


        }
    }

    private void validateItemCount(List<PiUniqueItemAssignmentDTO> piUniqueItemAssignmentDTOList, int approvalTwoQuantity) throws Exception {
        if (piUniqueItemAssignmentDTOList == null || piUniqueItemAssignmentDTOList.size() < approvalTwoQuantity) {
            UtilCharacter.throwException("পর্যাপ্ত মালামাল পাওয়া যায়নি!", "Enough Item Not Found!");
        }
    }

    void sendNotificationOnLessStock(UserDTO userDTO, Pi_requisitionDTO pi_requisitionDTO) {
        List<Pi_requisition_itemDTO> list = Pi_requisition_itemDAO.getInstance()
                .getRemainingStock(pi_requisitionDTO.iD, PiStageEnum.IN_STOCK.getValue(), userDTO.officeID);
        int itemCount = list == null ? 0 : list.size();
        if (itemCount < 10) {
            Pi_requisition_notification.getInstance().sendStockLimitationNotification(pi_requisitionDTO);
        }
    }

    void updateForFirstApproval(HttpServletRequest request, UserDTO userDTO, Pi_requisitionDTO pi_requisitionDTO) throws Exception {
        boolean isLanguageEnglish = userDTO.languageID == SessionConstants.ENGLISH;
        List<Pi_requisition_itemDTO> pi_requisition_itemDTOList = Pi_requisition_itemDAO.getInstance().getDTOsByRequisitionId(pi_requisitionDTO.iD);
        for (Pi_requisition_itemDTO pi_requisition_itemDTO : pi_requisition_itemDTOList) {
            String name = isLanguageEnglish ? pi_requisition_itemDTO.itemNameEn : pi_requisition_itemDTO.itemNameBn;
            pi_requisition_itemDTO.approvalOneQuantity = Integer.parseInt(Jsoup.clean(request.getParameter("approved_one_quantity_" + pi_requisition_itemDTO.iD), Whitelist.simpleText()));
            if (pi_requisition_itemDTO.approvalOneQuantity < 0) {
                throw new Exception(isLanguageEnglish ? "amount needed(Greater than or equal zero): " + name : "পরিমান আবশ্যক(শূন্য বা ততোধিক): " + name);
            }
            pi_requisition_itemDTO.approvalTwoQuantity = pi_requisition_itemDTO.approvalOneQuantity;
            int stock_amount = PiUniqueItemAssignmentDAO.getInstance().getTotalItemCountWithStageOverall(pi_requisition_itemDTO.itemId, PiStageEnum.IN_STOCK.getValue(), pi_requisition_itemDTO.apply_date);
            if (pi_requisition_itemDTO.approvalOneQuantity > stock_amount) {
                throw new Exception(isLanguageEnglish ? "Not enough in stock: " + name : "পর্যাপ্ত মজুদে নেই: " + name);
            }
            pi_requisition_itemDTO.status = pi_requisitionDTO.status;
            pi_requisition_itemDTO.modifiedBy = userDTO.employee_record_id;
            pi_requisition_itemDTO.lastModificationTime = new Date().getTime();
        }
        for (Pi_requisition_itemDTO pi_requisition_itemDTO : pi_requisition_itemDTOList) {
            Pi_requisition_itemDAO.getInstance().update(pi_requisition_itemDTO);
        }
    }

    void updateForSecondApproval(HttpServletRequest request, UserDTO userDTO, Pi_requisitionDTO pi_requisitionDTO) throws Exception {
        boolean isLanguageEnglish = userDTO.languageID == SessionConstants.ENGLISH;
//        if (!StringUtils.isValidString(request.getParameter("officeUnitId"))) {
//            throw new Exception(isLanguageEnglish ? "No office selected as office" : "স্টোর পাওয়া যায় নি");
//        }
//        long storeOfficeId = Long.parseLong(request.getParameter("officeUnitId"));
        Pi_office_unit_store_keeper_mappingDTO storeKeeperMapping = Pi_office_unit_store_keeper_mappingRepository.getInstance()
                .copyPi_office_unit_store_keeper_mappingDTOByOfficeUnitId(pi_requisitionDTO.storeKeeperOfficeUnitId);
        if (storeKeeperMapping == null) {
            throw new Exception(isLanguageEnglish ? "No store keeper found for this office" : "স্টোর কীপার নেই");
        }
//        EmployeeOfficeDTO storeKeeperOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(storeKeeperMapping.organogramId);
//
//        if (storeKeeperOfficeDTO == null) {
//            throw new Exception(isLanguageEnglish ? "Store keeper's office record not Found!" : "স্টোর কিপারের অফিসের তথ্য পাওয়া যায় নি!");
//        }
//        OfficeUnitOrganograms storeKeeperOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(storeKeeperOfficeDTO.officeUnitOrganogramId);
//        if (storeKeeperOrganograms == null) {
//            throw new Exception(isLanguageEnglish ? "Store keeper's organogram not Found!" : "স্টোর কিপারের পদবি পাওয়া যায় নি!");
//        }
//        pi_requisitionDTO.storeKeeperId = storeKeeperOfficeDTO.employeeRecordId;
//        pi_requisitionDTO.storeKeeperOrganogramId = storeKeeperOrganograms.id;
//        pi_requisitionDTO.storeKeeperRganogramEng = storeKeeperOrganograms.designation_eng;
//        pi_requisitionDTO.storeKeeperOrganogramBng = storeKeeperOrganograms.designation_bng;


        List<Pi_requisition_itemDTO> pi_requisition_itemDTOList = Pi_requisition_itemDAO.getInstance().getDTOsByRequisitionId(pi_requisitionDTO.iD);
        for (Pi_requisition_itemDTO pi_requisition_itemDTO : pi_requisition_itemDTOList) {
            String name = isLanguageEnglish ? pi_requisition_itemDTO.itemNameEn : pi_requisition_itemDTO.itemNameBn;
            String value = "";
            value = request.getParameter("approved_two_quantity_" + pi_requisition_itemDTO.iD);
            if (StringUtils.isValidString(value)) {
                value = Jsoup.clean(value, Whitelist.simpleText());
                pi_requisition_itemDTO.approvalTwoQuantity = Integer.parseInt(value);
            } else {
                pi_requisition_itemDTO.approvalTwoQuantity = 0;
            }
            if (pi_requisition_itemDTO.approvalTwoQuantity < 0) {
                throw new Exception(isLanguageEnglish ? "amount needed(Greater than or equal zero): " + name : "পরিমান আবশ্যক(শূন্য বা ততোধিক): " + name);
            }
            pi_requisition_itemDTO.status = pi_requisitionDTO.status;
            pi_requisition_itemDTO.modifiedBy = userDTO.employee_record_id;
            pi_requisition_itemDTO.lastModificationTime = new Date().getTime();
            int stock_amount = PiUniqueItemAssignmentDAO.getInstance().getTotalItemCountWithStage(storeKeeperMapping.officeUnitId, pi_requisition_itemDTO.itemId, PiStageEnum.IN_STOCK.getValue(), pi_requisition_itemDTO.apply_date);
            if (pi_requisition_itemDTO.approvalTwoQuantity > stock_amount) {
                throw new Exception(isLanguageEnglish ? "Not enough in stock: " + name : "পর্যাপ্ত মজুদে নেই: " + name);
            }
        }
        for (Pi_requisition_itemDTO pi_requisition_itemDTO : pi_requisition_itemDTOList) {
            Pi_requisition_itemDAO.getInstance().update(pi_requisition_itemDTO);
        }
    }
}

