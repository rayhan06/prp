package pi_requisition;

import pb.Utils;
import pb_notifications.Pb_notificationsDAO;
import util.StringUtils;
import vm_requisition.CommonApprovalStatus;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Pi_requisition_notification {
    private final Pb_notificationsDAO pb_notificationsDAO;

    private Pi_requisition_notification() {
        pb_notificationsDAO = Pb_notificationsDAO.getInstance();
    }

    private static class Pi_requisition_notificationLazyLoader {
        static final Pi_requisition_notification INSTANCE = new Pi_requisition_notification();
    }

    public static Pi_requisition_notification getInstance() {
        return Pi_requisition_notification.Pi_requisition_notificationLazyLoader.INSTANCE;
    }

    public void sendRequesterNotification(Pi_requisitionDTO pi_requisitionDTO) {
        String notificationEngText = "You have been submitted a requisition request on " +
                StringUtils.getFormattedDate("ENGLISH", pi_requisitionDTO.applyDate);
        String notificationBngText = "আপনি কিছু পন্য সরবরাহের অনুরোধ করেছেন " +
                StringUtils.getFormattedDate("BANGLA", pi_requisitionDTO.applyDate) + " তারিখে";
        String notificationMessage = notificationEngText + "$" + notificationBngText;
        String url = "Pi_requisitionServlet?actionType=view&ID=" + pi_requisitionDTO.iD;
        sendNotification(Collections.singletonList(pi_requisitionDTO.organogramId), notificationMessage, url);
    }

    public void sendSecondApproverApprovedNotification(Pi_requisitionDTO pi_requisitionDTO, String english, String Bangla) {
        int requesterTotalAmount = 0;
        int secondApproverTotalAmount = 0;
        List<Pi_requisition_itemDTO> pi_requisition_itemDTOS = Pi_requisition_itemDAO.getInstance().getDTOsByRequisitionId(pi_requisitionDTO.iD);
        for (Pi_requisition_itemDTO pi_requisition_itemDTO : pi_requisition_itemDTOS) {
            requesterTotalAmount += pi_requisition_itemDTO.requestedQuantity;
            secondApproverTotalAmount += pi_requisition_itemDTO.approvalTwoQuantity;
        }

        String notificationEngText = "";
        String notificationBngText = "";

        if (requesterTotalAmount > 0 && secondApproverTotalAmount > 0) {
            notificationEngText = "Your requisition request has been approved by " + english +
                    " You have requested " + requesterTotalAmount + " items and approved " + secondApproverTotalAmount + " items";
            notificationBngText = "আপনার সরবরাহের অনুরোধ " + Bangla + " কর্তৃক অনুমোদিত হয়েছে। আপনি " +
                    Utils.getDigitBanglaFromEnglish(String.valueOf(requesterTotalAmount)) + " টি আইটেমের অনুরোধ করেছেন এবং " +
                    Utils.getDigitBanglaFromEnglish(String.valueOf(secondApproverTotalAmount)) + " টি আইটেম অনুমোদিত হয়েছে।";
        } else {

            notificationEngText = "Your requisition request has been approved by " + english;
            notificationBngText = "আপনার সরবরাহের অনুরোধ " + Bangla + " কর্তৃক অনুমোদিত হয়েছে।";
        }


        String notificationMessage = notificationEngText + "$" + notificationBngText;
        String url = "Pi_requisitionServlet?actionType=view&ID=" + pi_requisitionDTO.iD;
        sendNotification(Collections.singletonList(pi_requisitionDTO.organogramId), notificationMessage, url);
    }

    public void sendApprovedNotification(Pi_requisitionDTO pi_requisitionDTO, String english, String Bangla) {
        String notificationEngText = "Your requisition request has been approved by " + english;
        String notificationBngText = "আপনার সরবরাহের অনুরোধ " + Bangla + " কর্তৃক অনুমোদিত হয়েছে।";
        String notificationMessage = notificationEngText + "$" + notificationBngText;
        String url = "Pi_requisitionServlet?actionType=view&ID=" + pi_requisitionDTO.iD;
        sendNotification(Collections.singletonList(pi_requisitionDTO.organogramId), notificationMessage, url);
    }

    public void sendRejectNotification(Pi_requisitionDTO pi_requisitionDTO) {
        String notificationEngText = "Your requisition request has been rejected.";
        String notificationBngText = "আপনার সরবরাহের অনুরোধ বাতিল হয়েছে।";
        String notificationMessage = notificationEngText + "$" + notificationBngText;
        String url = "Pi_requisitionServlet?actionType=view&ID=" + pi_requisitionDTO.iD;
        sendNotification(Collections.singletonList(pi_requisitionDTO.organogramId), notificationMessage, url);
    }

    public void sendSupplyNotification(Pi_requisitionDTO pi_requisitionDTO) {
        String notificationEngText = "Your requisition request has been supplied.";
        String notificationBngText = "আপনার সরবরাহের অনুরোধ সরবরাহ হয়েছে।";
        String notificationMessage = notificationEngText + "$" + notificationBngText;
        String url = "Pi_requisitionServlet?actionType=view&ID=" + pi_requisitionDTO.iD;
        sendNotification(Collections.singletonList(pi_requisitionDTO.organogramId), notificationMessage, url);
    }

    public void sendStockLimitationNotification(Pi_requisitionDTO pi_requisitionDTO) {
        String notificationEngText = "Stock of this item is less than 10";
        String notificationBngText = "এই আইটেমটির স্টক 10 এর কম।";
        String notificationMessage = notificationEngText + "$" + notificationBngText;
        String url = "Pi_stockServlet?actionType=view&ID=" + pi_requisitionDTO.iD;
        sendNotification(Arrays.asList(pi_requisitionDTO.storeKeeperId, pi_requisitionDTO.approvalOneId,
                pi_requisitionDTO.approvalTwoId), notificationMessage, url);
    }

    public void sendApproverNotification(Pi_requisitionDTO pi_requisitionDTO, String path) {
        String notificationEngText = "A requisition has been request on " +
                StringUtils.getFormattedDate("ENGLISH", pi_requisitionDTO.applyDate) + " by " + pi_requisitionDTO.requesterNameEng;
        String notificationBngText = pi_requisitionDTO.requesterNameBng + " কিছু পন্য সরবরাহের অনুরোধ করেছেন " +
                StringUtils.getFormattedDate("BANGLA", pi_requisitionDTO.applyDate) + " তারিখে";
        String notificationMessage = notificationEngText + "$" + notificationBngText;
        String url = "Pi_requisitionServlet?actionType=" + path + "&ID=" + pi_requisitionDTO.iD;
        long organogramId = -1;
        if (pi_requisitionDTO.status == CommonApprovalStatus.REQUISITION_NOT_APPROVED.getValue()) {
            organogramId = pi_requisitionDTO.approvalOneOrganogramId;
        } else if (pi_requisitionDTO.status == CommonApprovalStatus.REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue()) {
            organogramId = pi_requisitionDTO.approvalTwoOrganogramId;
        } else if (pi_requisitionDTO.status == CommonApprovalStatus.REQUISITION_APPROVED_BY_SECOND_APPROVER.getValue()) {
            organogramId = pi_requisitionDTO.storeKeeperOrganogramId;
        }
        sendNotification(Collections.singletonList(organogramId), notificationMessage, url);
    }

    private void sendNotification(List<Long> organogramIds, String notificationMessage, String url) {
        if (organogramIds == null || organogramIds.size() == 0) {
            return;
        }
        Thread thread = new Thread(() -> {
            long currentTime = System.currentTimeMillis();
            organogramIds.forEach(id -> pb_notificationsDAO.addPb_notifications(id, currentTime, url, notificationMessage, false));
        });
        thread.setDaemon(true);
        thread.start();
    }
}
