package pi_requisition;

import util.CommonDTO;


public class Pi_requisition_itemDTO extends CommonDTO {

    public long piRequisitionId = -1;
    public long requesterEmployeeRecordId = -1;
    public long itemId = -1;
    public String itemNameEn = "";
    public String itemNameBn = "";
    public long itemTypeId = -1;
    public String itemTypeNameEn = "";
    public String itemTypeNameBn = "";
    public int requestedQuantity = 0;
    public int approvalOneQuantity = 0;
    public int approvalTwoQuantity = 0;
    public int stockAmount = 0;
    public long fiscalYear = -1;
    public long apply_date = System.currentTimeMillis();
    public int status = -1;
    public long insertedBy = -1;
    public long insertionTime = System.currentTimeMillis();
    public long modifiedBy = -1;
    public int isPersonal = -1;

    @Override
    public String toString() {
        return "Pi_requisition_itemDTO{" +
                "piRequisitionId=" + piRequisitionId +
                ", itemId=" + itemId +
                ", itemNameEn='" + itemNameEn + '\'' +
                ", itemNameBn='" + itemNameBn + '\'' +
                ", itemTypeId=" + itemTypeId +
                ", itemTypeNameEn='" + itemTypeNameEn + '\'' +
                ", itemTypeNameBn='" + itemTypeNameBn + '\'' +
                ", requestedQuantity=" + requestedQuantity +
                ", approvalOneQuantity=" + approvalOneQuantity +
                ", approvalTwoQuantity=" + approvalTwoQuantity +
                ", fiscalYear=" + fiscalYear +
                ", apply_date=" + apply_date +
                ", status=" + status +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                '}';
    }

}