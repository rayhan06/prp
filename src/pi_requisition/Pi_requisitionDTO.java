package pi_requisition;

import util.CommonDTO;
import util.TimeUtils;
import vm_requisition.CommonApprovalStatus;

import java.util.Date;


public class Pi_requisitionDTO extends CommonDTO {

    public long employeeRecordId = -1;
    public String requesterNameEng = "";
    public String requesterNameBng = "";
    public long officeUnitId = -1;
    public String officeUnitEng = "";
    public String officeUnitBng = "";
    public long organogramId = -1;
    public String organogramEng = "";
    public String organogramBng = "";
    public long fiscalYear = -1;
    public long applyDate = System.currentTimeMillis();
    public String applyTime = TimeUtils.DATE_FORMAT_TIME_AM_PM.format(new Date());
    public String comment = "";
    public String disburseComment = "";
    public int status = CommonApprovalStatus.REQUISITION_NOT_APPROVED.getValue();
    public long approvalOneId = -1;
    public long approvalOneOrganogramId = -1;
    public String approvalOneOrganogramEng = "";
    public String approvalOneOrganogramBng = "";
    public long approvalOneDate = System.currentTimeMillis();
    public long approvalTwoId = -1;
    public long approvalTwoOrganogramId = -1;
    public String approvalTwoOrganogramEng = "";
    public String approvalTwoOrganogramBng = "";
    public long approvalTwoDate = System.currentTimeMillis();
    public long storeKeeperId = -1;
    public long storeKeeperOrganogramId = -1;
    public long storeKeeperOfficeUnitId = -1;
    public String storeKeeperRganogramEng = "";
    public String storeKeeperOrganogramBng = "";
    public long storeKeeperDate = System.currentTimeMillis();
    public long insertedBy = -1;
    public long insertionTime = System.currentTimeMillis();
    public long modifiedBy = -1;
    public int count = 0;
    public int isPersonal = -1;


    @Override
    public String toString() {
        return "Pi_requisitionDTO{" +
                "employeeRecordId=" + employeeRecordId +
                ", requesterNameEng='" + requesterNameEng + '\'' +
                ", requesterNameBng='" + requesterNameBng + '\'' +
                ", officeUnitId=" + officeUnitId +
                ", officeUnitEng='" + officeUnitEng + '\'' +
                ", officeUnitBng='" + officeUnitBng + '\'' +
                ", organogramId=" + organogramId +
                ", organogramEng='" + organogramEng + '\'' +
                ", organogramBng='" + organogramBng + '\'' +
                ", fiscalYear=" + fiscalYear +
                ", applyDate=" + applyDate +
                ", comment='" + comment + '\'' +
                ", status=" + status +
                ", approvalOneId=" + approvalOneId +
                ", approvalOneOrganogramId=" + approvalOneOrganogramId +
                ", approvalOneOrganogramEng='" + approvalOneOrganogramEng + '\'' +
                ", approvalOneOrganogramBng='" + approvalOneOrganogramBng + '\'' +
                ", approvalOneDate=" + approvalOneDate +
                ", approvalTwoId=" + approvalTwoId +
                ", approvalTwoOrganogramId=" + approvalTwoOrganogramId +
                ", approvalTwoOrganogramEng='" + approvalTwoOrganogramEng + '\'' +
                ", approvalTwoOrganogramBng='" + approvalTwoOrganogramBng + '\'' +
                ", approvalTwoDate=" + approvalTwoDate +
                ", storeKeeperId=" + storeKeeperId +
                ", storeKeeperOrganogramId=" + storeKeeperOrganogramId +
                ", storeKeeperRganogramEng='" + storeKeeperRganogramEng + '\'' +
                ", storeKeeperOrganogramBng='" + storeKeeperOrganogramBng + '\'' +
                ", storeKeeperDate=" + storeKeeperDate +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                '}';
    }

}