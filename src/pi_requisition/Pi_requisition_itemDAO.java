package pi_requisition;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;
import vm_requisition.CommonApprovalStatus;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Pi_requisition_itemDAO implements CommonDAOService<Pi_requisition_itemDTO> {

    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";

    private final String[] columnNames;

    public Pi_requisition_itemDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "pi_requisition_id",
                        "requester_employee_record_id",
                        "item_id",
                        "item_nameEn",
                        "item_nameBn",
                        "item_type_id",
                        "item_type_nameEn",
                        "item_type_nameBn",
                        "requested_quantity",
                        "approval_one_quantity",
                        "approval_two_quantity",
                        "fiscal_year",
                        "apply_date",
                        "status",
                        "is_personal",
                        "inserted_by",
                        "insertion_time",
                        "isDeleted",
                        "modified_by",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("item_nameEn", " and (item_nameEn like ?)");
        searchMap.put("item_nameBn", " and (item_nameBn like ?)");
        searchMap.put("item_type_nameEn", " and (item_type_nameEn like ?)");
        searchMap.put("item_type_nameBn", " and (item_type_nameBn like ?)");
        searchMap.put("inserted_by", " and (inserted_by like ?)");
        searchMap.put("modified_by", " and (modified_by like ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    public List<Pi_requisition_itemDTO> getRemainingStock(long itemId, int inStockId, long officeID) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM pi_unique_item_assignment WHERE ")
                .append("item_id=").append(itemId).append(" AND ")
                .append("stage=").append(inStockId).append(" AND ")
                .append("office_unit_id=").append(officeID);
        List<Pi_requisition_itemDTO> list = getDTOs(sql.toString());
        return list.size() == 0 ? null : list;
    }

    private static class LazyLoader {
        static final Pi_requisition_itemDAO INSTANCE = new Pi_requisition_itemDAO();
    }

    public static Pi_requisition_itemDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Pi_requisition_itemDTO pi_requisition_itemDTO) {
        pi_requisition_itemDTO.searchColumn = "";
        pi_requisition_itemDTO.searchColumn += pi_requisition_itemDTO.itemNameEn + " ";
        pi_requisition_itemDTO.searchColumn += pi_requisition_itemDTO.itemNameBn + " ";
        pi_requisition_itemDTO.searchColumn += pi_requisition_itemDTO.itemTypeNameEn + " ";
        pi_requisition_itemDTO.searchColumn += pi_requisition_itemDTO.itemTypeNameBn + " ";
    }

    @Override
    public void set(PreparedStatement ps, Pi_requisition_itemDTO pi_requisition_itemDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        //setSearchColumn(pi_requisition_itemDTO);
        if (isInsert) {
            ps.setObject(++index, pi_requisition_itemDTO.iD);
        }
        ps.setObject(++index, pi_requisition_itemDTO.piRequisitionId);
        ps.setObject(++index, pi_requisition_itemDTO.requesterEmployeeRecordId);
        ps.setObject(++index, pi_requisition_itemDTO.itemId);
        ps.setObject(++index, pi_requisition_itemDTO.itemNameEn);
        ps.setObject(++index, pi_requisition_itemDTO.itemNameBn);
        ps.setObject(++index, pi_requisition_itemDTO.itemTypeId);
        ps.setObject(++index, pi_requisition_itemDTO.itemTypeNameEn);
        ps.setObject(++index, pi_requisition_itemDTO.itemTypeNameBn);
        ps.setObject(++index, pi_requisition_itemDTO.requestedQuantity);
        ps.setObject(++index, pi_requisition_itemDTO.approvalOneQuantity);
        ps.setObject(++index, pi_requisition_itemDTO.approvalTwoQuantity);
        ps.setObject(++index, pi_requisition_itemDTO.fiscalYear);
        ps.setObject(++index, pi_requisition_itemDTO.apply_date);
        ps.setObject(++index, pi_requisition_itemDTO.status);
        ps.setObject(++index, pi_requisition_itemDTO.isPersonal);
        ps.setObject(++index, pi_requisition_itemDTO.insertedBy);
        ps.setObject(++index, pi_requisition_itemDTO.insertionTime);
        if (isInsert) {
            ps.setObject(++index, pi_requisition_itemDTO.isDeleted);
        }
        ps.setObject(++index, pi_requisition_itemDTO.modifiedBy);
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, pi_requisition_itemDTO.iD);
        }
    }

    @Override
    public Pi_requisition_itemDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Pi_requisition_itemDTO pi_requisition_itemDTO = new Pi_requisition_itemDTO();
            int i = 0;
            pi_requisition_itemDTO.iD = rs.getLong(columnNames[i++]);
            pi_requisition_itemDTO.piRequisitionId = rs.getLong(columnNames[i++]);
            pi_requisition_itemDTO.requesterEmployeeRecordId = rs.getLong(columnNames[i++]);
            pi_requisition_itemDTO.itemId = rs.getLong(columnNames[i++]);
            pi_requisition_itemDTO.itemNameEn = rs.getString(columnNames[i++]);
            pi_requisition_itemDTO.itemNameBn = rs.getString(columnNames[i++]);
            pi_requisition_itemDTO.itemTypeId = rs.getLong(columnNames[i++]);
            pi_requisition_itemDTO.itemTypeNameEn = rs.getString(columnNames[i++]);
            pi_requisition_itemDTO.itemTypeNameBn = rs.getString(columnNames[i++]);
            pi_requisition_itemDTO.requestedQuantity = rs.getInt(columnNames[i++]);
            pi_requisition_itemDTO.approvalOneQuantity = rs.getInt(columnNames[i++]);
            pi_requisition_itemDTO.approvalTwoQuantity = rs.getInt(columnNames[i++]);
            pi_requisition_itemDTO.fiscalYear = rs.getLong(columnNames[i++]);
            pi_requisition_itemDTO.apply_date = rs.getLong(columnNames[i++]);
            pi_requisition_itemDTO.status = rs.getInt(columnNames[i++]);
            pi_requisition_itemDTO.isPersonal = rs.getInt(columnNames[i++]);
            pi_requisition_itemDTO.insertedBy = rs.getLong(columnNames[i++]);
            pi_requisition_itemDTO.insertionTime = rs.getLong(columnNames[i++]);
            pi_requisition_itemDTO.isDeleted = rs.getInt(columnNames[i++]);
            pi_requisition_itemDTO.modifiedBy = rs.getLong(columnNames[i++]);
            pi_requisition_itemDTO.lastModificationTime = rs.getLong(columnNames[i]);
            return pi_requisition_itemDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Pi_requisition_itemDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "pi_requisition_item";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_requisition_itemDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_requisition_itemDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

    public List<Pi_requisition_itemDTO> getDTOsByRequisitionId(long piRequisitionId) {
        String getDTOsByRequisitionIdQuery = "SELECT * FROM pi_requisition_item WHERE pi_requisition_id=%d AND  isDeleted = 0";
        return getDTOs(String.format(getDTOsByRequisitionIdQuery, piRequisitionId));
    }

    public Pi_requisition_itemDTO getDTOsByRequisitionIdAndItemId(long piRequisitionId, long itemId) {
        String getDTOsByRequisitionAndItemIdIdQuery = "SELECT * FROM pi_requisition_item WHERE pi_requisition_id=%d AND item_id=%d AND isDeleted = 0";
        List<Pi_requisition_itemDTO> pi_requisition_itemDTOList = getDTOs(String.format(getDTOsByRequisitionAndItemIdIdQuery, piRequisitionId, itemId));
        if (pi_requisition_itemDTOList == null || pi_requisition_itemDTOList.size() == 0) {
            return null;
        }
        return pi_requisition_itemDTOList.get(0);
    }


    public Pi_requisition_itemDTO getDTOsByItemId(long itemId, long employeeRecordId, long applyDate) {
        String getDTOsByItemIdQuery = "SELECT * FROM pi_requisition_item WHERE item_id=%d AND status=%d AND apply_date<%d AND requester_employee_record_id=%d AND isDeleted = 0 ";
        List<Pi_requisition_itemDTO> pi_requisition_itemDTOList = getDTOs(String.format(getDTOsByItemIdQuery, itemId, CommonApprovalStatus.REQUISITION_SUPPLIED_BY_STORE_KEEPER.getValue(), applyDate,employeeRecordId));
        if (pi_requisition_itemDTOList == null || pi_requisition_itemDTOList.size() == 0) {
            return null;
        }

        return pi_requisition_itemDTOList.stream()
                .max(Comparator.comparing(dto -> dto.iD))
                .orElse(null);
    }
}
	