package pi_requisition;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Pi_requisitionDAO implements CommonDAOService<Pi_requisitionDTO> {

    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private final String[] columnNames;

    private Pi_requisitionDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "employee_record_id",
                        "requester_name_eng",
                        "requester_name_bng",
                        "office_unit_id",
                        "office_unit_eng",
                        "office_unit_bng",
                        "organogram_id",
                        "organogram_eng",
                        "organogram_bng",
                        "fiscal_year",
                        "apply_date",
                        "apply_time",
                        "comment",
                        "disburse_comment",
                        "status",
                        "search_column",
                        "approval_one_id",
                        "approval_one_organogram_id",
                        "approval_one_organogram_eng",
                        "approval_one_organogram_bng",
                        "approval_one_date",
                        "approval_two_id",
                        "approval_two_organogram_id",
                        "approval_two_organogram_eng",
                        "approval_two_organogram_bng",
                        "approval_two_date",
                        "store_keeper_id",
                        "store_keeper_office_unit_id",
                        "store_keeper_organogram_id",
                        "store_keeper_organogram_eng",
                        "store_keeper_organogram_bng",
                        "store_keeper_date",
                        "is_personal",
                        "inserted_by",
                        "insertion_time",
                        "isDeleted",
                        "modified_by",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
        searchMap.put("employee_records_id_internal", " and (employee_record_id = ?)");
        searchMap.put("status_in", " and (status in ?)");
        searchMap.put("office_unit_id", " and (office_unit_id = ?)");
        searchMap.put("apply_date_start", " and (apply_date >= ?)");
        searchMap.put("apply_date_end", " and (apply_date <= ?)");
        searchMap.put("approval_one_date_start", " and (approval_one_date >= ?)");
        searchMap.put("approval_one_date_end", " and (approval_one_date <= ?)");
        searchMap.put("approval_two_date_start", " and (approval_two_date >= ?)");
        searchMap.put("approval_two_date_end", " and (approval_two_date <= ?)");
        searchMap.put("store_keeper_date_start", " and (store_keeper_date >= ?)");
        searchMap.put("store_keeper_date_end", " and (store_keeper_date <= ?)");
        searchMap.put("approval_one_id", " and (approval_one_id = ?)");
        searchMap.put("approval_two_id", " and (approval_two_id = ?)");
        searchMap.put("store_keeper_id", " and (store_keeper_id = ?)");
        searchMap.put("status", " and (status = ?)");
        searchMap.put("office_units_id", " and (office_unit_id = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Pi_requisitionDAO INSTANCE = new Pi_requisitionDAO();
    }

    public static Pi_requisitionDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Pi_requisitionDTO pi_requisitionDTO) {
        pi_requisitionDTO.searchColumn = "";
        pi_requisitionDTO.searchColumn += pi_requisitionDTO.requesterNameEng + " ";
        pi_requisitionDTO.searchColumn += pi_requisitionDTO.requesterNameBng + " ";
        pi_requisitionDTO.searchColumn += pi_requisitionDTO.officeUnitEng + " ";
        pi_requisitionDTO.searchColumn += pi_requisitionDTO.officeUnitBng + " ";
        pi_requisitionDTO.searchColumn += pi_requisitionDTO.organogramEng + " ";
        pi_requisitionDTO.searchColumn += pi_requisitionDTO.organogramBng + " ";
    }

    @Override
    public void set(PreparedStatement ps, Pi_requisitionDTO pi_requisitionDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(pi_requisitionDTO);
        if (isInsert) {
            ps.setObject(++index, pi_requisitionDTO.iD);
        }
        ps.setObject(++index, pi_requisitionDTO.employeeRecordId);
        ps.setObject(++index, pi_requisitionDTO.requesterNameEng);
        ps.setObject(++index, pi_requisitionDTO.requesterNameBng);
        ps.setObject(++index, pi_requisitionDTO.officeUnitId);
        ps.setObject(++index, pi_requisitionDTO.officeUnitEng);
        ps.setObject(++index, pi_requisitionDTO.officeUnitBng);
        ps.setObject(++index, pi_requisitionDTO.organogramId);
        ps.setObject(++index, pi_requisitionDTO.organogramEng);
        ps.setObject(++index, pi_requisitionDTO.organogramBng);
        ps.setObject(++index, pi_requisitionDTO.fiscalYear);
        ps.setObject(++index, pi_requisitionDTO.applyDate);
        ps.setObject(++index, pi_requisitionDTO.applyTime);
        ps.setObject(++index, pi_requisitionDTO.comment);
        ps.setObject(++index, pi_requisitionDTO.disburseComment);
        ps.setObject(++index, pi_requisitionDTO.status);
        ps.setObject(++index, pi_requisitionDTO.searchColumn);
        ps.setObject(++index, pi_requisitionDTO.approvalOneId);
        ps.setObject(++index, pi_requisitionDTO.approvalOneOrganogramId);
        ps.setObject(++index, pi_requisitionDTO.approvalOneOrganogramEng);
        ps.setObject(++index, pi_requisitionDTO.approvalOneOrganogramBng);
        ps.setObject(++index, pi_requisitionDTO.approvalOneDate);
        ps.setObject(++index, pi_requisitionDTO.approvalTwoId);
        ps.setObject(++index, pi_requisitionDTO.approvalTwoOrganogramId);
        ps.setObject(++index, pi_requisitionDTO.approvalTwoOrganogramEng);
        ps.setObject(++index, pi_requisitionDTO.approvalTwoOrganogramBng);
        ps.setObject(++index, pi_requisitionDTO.approvalTwoDate);
        ps.setObject(++index, pi_requisitionDTO.storeKeeperId);
        ps.setObject(++index, pi_requisitionDTO.storeKeeperOfficeUnitId);
        ps.setObject(++index, pi_requisitionDTO.storeKeeperOrganogramId);
        ps.setObject(++index, pi_requisitionDTO.storeKeeperRganogramEng);
        ps.setObject(++index, pi_requisitionDTO.storeKeeperOrganogramBng);
        ps.setObject(++index, pi_requisitionDTO.storeKeeperDate);
        ps.setObject(++index, pi_requisitionDTO.isPersonal);
        ps.setObject(++index, pi_requisitionDTO.insertedBy);
        ps.setObject(++index, pi_requisitionDTO.insertionTime);
        if (isInsert) {
            ps.setObject(++index, pi_requisitionDTO.isDeleted);
        }
        ps.setObject(++index, pi_requisitionDTO.modifiedBy);
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, pi_requisitionDTO.iD);
        }
    }

    @Override
    public Pi_requisitionDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Pi_requisitionDTO pi_requisitionDTO = new Pi_requisitionDTO();
            int i = 0;
            pi_requisitionDTO.iD = rs.getLong(columnNames[i++]);
            pi_requisitionDTO.employeeRecordId = rs.getLong(columnNames[i++]);
            pi_requisitionDTO.requesterNameEng = rs.getString(columnNames[i++]);
            pi_requisitionDTO.requesterNameBng = rs.getString(columnNames[i++]);
            pi_requisitionDTO.officeUnitId = rs.getLong(columnNames[i++]);
            pi_requisitionDTO.officeUnitEng = rs.getString(columnNames[i++]);
            pi_requisitionDTO.officeUnitBng = rs.getString(columnNames[i++]);
            pi_requisitionDTO.organogramId = rs.getLong(columnNames[i++]);
            pi_requisitionDTO.organogramEng = rs.getString(columnNames[i++]);
            pi_requisitionDTO.organogramBng = rs.getString(columnNames[i++]);
            pi_requisitionDTO.fiscalYear = rs.getLong(columnNames[i++]);
            pi_requisitionDTO.applyDate = rs.getLong(columnNames[i++]);
            pi_requisitionDTO.applyTime = rs.getString(columnNames[i++]);
            pi_requisitionDTO.comment = rs.getString(columnNames[i++]);
            pi_requisitionDTO.disburseComment = rs.getString(columnNames[i++]);
            pi_requisitionDTO.status = rs.getInt(columnNames[i++]);
            pi_requisitionDTO.searchColumn = rs.getString(columnNames[i++]);
            pi_requisitionDTO.approvalOneId = rs.getLong(columnNames[i++]);
            pi_requisitionDTO.approvalOneOrganogramId = rs.getLong(columnNames[i++]);
            pi_requisitionDTO.approvalOneOrganogramEng = rs.getString(columnNames[i++]);
            pi_requisitionDTO.approvalOneOrganogramBng = rs.getString(columnNames[i++]);
            pi_requisitionDTO.approvalOneDate = rs.getLong(columnNames[i++]);
            pi_requisitionDTO.approvalTwoId = rs.getLong(columnNames[i++]);
            pi_requisitionDTO.approvalTwoOrganogramId = rs.getLong(columnNames[i++]);
            pi_requisitionDTO.approvalTwoOrganogramEng = rs.getString(columnNames[i++]);
            pi_requisitionDTO.approvalTwoOrganogramBng = rs.getString(columnNames[i++]);
            pi_requisitionDTO.approvalTwoDate = rs.getLong(columnNames[i++]);
            pi_requisitionDTO.storeKeeperId = rs.getLong(columnNames[i++]);
            pi_requisitionDTO.storeKeeperOfficeUnitId = rs.getLong(columnNames[i++]);
            pi_requisitionDTO.storeKeeperOrganogramId = rs.getLong(columnNames[i++]);
            pi_requisitionDTO.storeKeeperRganogramEng = rs.getString(columnNames[i++]);
            pi_requisitionDTO.storeKeeperOrganogramBng = rs.getString(columnNames[i++]);
            pi_requisitionDTO.storeKeeperDate = rs.getLong(columnNames[i++]);
            pi_requisitionDTO.isPersonal = rs.getInt(columnNames[i++]);
            pi_requisitionDTO.insertedBy = rs.getLong(columnNames[i++]);
            pi_requisitionDTO.insertionTime = rs.getLong(columnNames[i++]);
            pi_requisitionDTO.isDeleted = rs.getInt(columnNames[i++]);
            pi_requisitionDTO.modifiedBy = rs.getLong(columnNames[i++]);
            pi_requisitionDTO.lastModificationTime = rs.getLong(columnNames[i]);
            return pi_requisitionDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Pi_requisitionDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "pi_requisition";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_requisitionDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_requisitionDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

    public Pi_requisitionDTO getCountBuild(ResultSet rs)
    {
        try
        {
            Pi_requisitionDTO countDTO = new Pi_requisitionDTO();
            countDTO.status = rs.getInt("status");
            countDTO.count = rs.getInt("count");
            return countDTO;
        }
        catch (SQLException ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    public List<Pi_requisitionDTO> getCountByStatus(long unitId){
        String countQuery = "SELECT status, COUNT(*) AS count FROM "
                + getTableName() + " where isDeleted = 0 AND office_unit_id = ?  GROUP BY status  ";
        return ConnectionAndStatementUtil.getListOfT(countQuery, Arrays.asList(unitId), this::getCountBuild);
    }

}
	