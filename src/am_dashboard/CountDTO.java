package am_dashboard;

public class CountDTO {
    public String type1 = "";
    public String type2 = "";
    public String type3 = "";
    public int type4 = 0;
    public int type5 = 0;

    @Override
    public String toString() {
        return "CountDTO[" +
                " type1 = " + type1 +
                " type2 = " + type2 +
                " type3 = " + type3 +
                "]";
    }
}
