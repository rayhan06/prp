package am_dashboard;

import am_house.Am_houseRepository;
import am_house_allocation_request.Am_house_allocation_requestDAO;
import am_minister_hostel_level.Am_minister_hostel_levelRepository;
import am_office_assignment_request.Am_office_assignment_requestDAO;
import am_parliament_building_room.Am_parliament_building_roomRepository;
import com.google.gson.Gson;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pb.CatDTO;
import pb.CatRepository;
import util.HttpRequestUtils;
import util.UtilCharacter;
import vm_requisition.CommonApprovalStatus;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import language.LC;
import language.LM;



@WebServlet("/AssetDashboardServlet")
@MultipartConfig
public class AssetDashboardServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(AssetDashboardServlet.class);
    public AssetDashboardServlet()
	{
        super();
    }

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		logger.debug("In do get request = " + request);
		try
		{
			String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
			LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;

			String actionType = request.getParameter("actionType");
			if(actionType.equals("house_count")){

				List<CatDTO> catDTOS = CatRepository.getDTOs("am_house_class");
				List<CountDTO> countDTOS = new ArrayList<>();

				CountDTO dto = new CountDTO();
				dto.type1 = LM.getText(LC.AM_HOUSE_ADD_AMHOUSECLASSCAT, loginDTO);
				dto.type2 = UtilCharacter.getDataByLanguage(Language, "ব্যবহারযোগ্য", "Available" );
				dto.type3 = UtilCharacter.getDataByLanguage(Language, "ব্যবহৃত", "Used");
				countDTOS.add(dto);


				for(CatDTO catDTO: catDTOS){
					CountDTO countDTO = new CountDTO();
					countDTO.type1 = UtilCharacter.getDataByLanguage(Language, catDTO.languageTextBangla,
							catDTO.languageTextEnglish);
					countDTO.type2 = Am_houseRepository.getInstance().getAm_houseDTOByClassId(catDTO.value)
							.stream().filter(i -> i.status == CommonApprovalStatus.AVAILABLE.getValue()).count() + "";
					countDTO.type3 = Am_houseRepository.getInstance().getAm_houseDTOByClassId(catDTO.value)
							.stream().filter(i -> i.status == CommonApprovalStatus.USED.getValue()).count() + "";
					countDTOS.add(countDTO);
				}

				String data = "";
				data = new Gson().toJson(countDTOS);
				response.setContentType("application/json");
				PrintWriter out = response.getWriter();
				out.print(data);
				out.flush();
				out.close();
			}
			else if(actionType.equals("office_count")){
				List<CountDTO> countDTOS = new ArrayList<>();

				CountDTO dto = new CountDTO();
				dto.type1 = UtilCharacter.getDataByLanguage(Language, "কক্ষ", "Room");
				dto.type2 = UtilCharacter.getDataByLanguage(Language, "ব্যবহারযোগ্য", "Available" );
				dto.type3 = UtilCharacter.getDataByLanguage(Language, "ব্যবহৃত", "Used");
				countDTOS.add(dto);

				dto = new CountDTO();
				dto.type1 = CatRepository.getName(Language, "building_type", 1);
				dto.type2 = Am_parliament_building_roomRepository.getInstance().getAm_parliament_building_roomList()
						.stream().filter(i -> i.status == CommonApprovalStatus.AVAILABLE.getValue()).count() + "";
				dto.type3 = Am_parliament_building_roomRepository.getInstance().getAm_parliament_building_roomList()
						.stream().filter(i -> i.status == CommonApprovalStatus.USED.getValue()).count() + "";
				countDTOS.add(dto);

				dto = new CountDTO();
				dto.type1 = CatRepository.getName(Language, "building_type", 2);
				dto.type2 = Am_minister_hostel_levelRepository.getInstance().getAm_minister_hostel_levelList()
						.stream().filter(i -> i.status == CommonApprovalStatus.AVAILABLE.getValue()).count() + "";
				dto.type3 = Am_minister_hostel_levelRepository.getInstance().getAm_minister_hostel_levelList()
						.stream().filter(i -> i.status == CommonApprovalStatus.USED.getValue()).count() + "";
				countDTOS.add(dto);

				String data = "";
				data = new Gson().toJson(countDTOS);
				response.setContentType("application/json");
				PrintWriter out = response.getWriter();
				out.print(data);
				out.flush();
				out.close();
			}
			else if(actionType.equals("house_request")){

				List<CountDTO> countDTOS = Am_house_allocation_requestDAO.getInstance().getCountByStatus();
				Map<String, Integer> result =  new HashMap<>();
				for(CountDTO countDTO: countDTOS){
					String key = CatRepository.getInstance().getText(
							Language, "am_house_allocation_status", countDTO.type4);
					result.put(key, countDTO.type5);
				}
				String data = new Gson().toJson(result);
				response.setContentType("application/json");
				PrintWriter out = response.getWriter();
				out.print(data);
				out.flush();
				out.close();
			}
			else if(actionType.equals("office_request")){

				List<CountDTO> countDTOS = Am_office_assignment_requestDAO.getInstance().getCountByStatus();
				Map<String, Integer> result =  new HashMap<>();
				for(CountDTO countDTO: countDTOS){
					String key = CatRepository.getInstance().getText(
							Language, "am_office_assignment_request_status", countDTO.type4);
					result.put(key, countDTO.type5);
				}
				String data = new Gson().toJson(result);
				response.setContentType("application/json");
				PrintWriter out = response.getWriter();
				out.print(data);
				out.flush();
				out.close();
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

	}






}

