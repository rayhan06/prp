package vm_vehicle_driver_assignment_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Vm_vehicle_driver_assignment_report_Servlet")
public class Vm_vehicle_driver_assignment_report_Servlet  extends HttpServlet {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","tr","start_time",">=","","String","","","any","startTime", LC.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_WHERE_STARTTIME + ""},
		{"criteria","tr","start_time","<=","AND","String","","","any","startTime_1", LC.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_WHERE_STARTTIME_1 + ""},
		{"criteria","tr","end_time",">=","AND","String","","","any","endTime", LC.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_WHERE_ENDTIME + ""},
		{"criteria","tr","end_time","<=","AND","String","","","any","endTime_3", LC.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_WHERE_ENDTIME_3 + ""},
		{"criteria","tr","vehicle_type_cat_multiple","=","AND","String","","","any","vehicleTypeCat", LC.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_WHERE_VEHICLETYPECAT + ""},
		{"criteria","tr","driver_id","=","AND","String","","","any","driverId", LC.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_WHERE_DRIVERID + ""},
		{"criteria","tr","isDeleted","=","AND","String","","","0","isDeleted", LC.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_WHERE_ISDELETED + ""}
	};

	String[][] Display =
	{
		{"display","tr","driver_id","vehicle_driver_assignment_id_check",""},
		{"display","tr","vehicle_type_cat_multiple","cat_multiple",""},
		{"display","tr","vehicle_id_multiple","reg_no",""},
		{"display","tr","post_name_bn","text",""},
		{"display","tr","start_time","date",""},
		{"display","tr","end_time","date",""}
	};

	String GroupBy = "";
	String OrderBY = "";

	ReportRequestHandler reportRequestHandler;

	public Vm_vehicle_driver_assignment_report_Servlet(){

	}

	private final ReportService reportService = new ReportService();

	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}

		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);

		sql = "vm_vehicle_driver_assignment tr";

		Display[0][4] = LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_SELECT_DRIVERID, loginDTO);
		Display[1][4] = LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_SELECT_VEHICLETYPECAT, loginDTO);
		Display[2][4] = LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_SELECT_VEHICLEID, loginDTO);
		Display[3][4] = LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_SELECT_POSTNAMEBN, loginDTO);
		Display[4][4] = LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_SELECT_STARTTIME, loginDTO);
		Display[5][4] = LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_SELECT_ENDTIME, loginDTO);


		String reportName = LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_OTHER_VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT, loginDTO);

		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);


		reportRequestHandler.handleReportGet(request, response, userDTO, "vm_vehicle_driver_assignment_report",
				MenuConstants.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_DETAILS, language, reportName, "vm_vehicle_driver_assignment_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doGet(request, response);
	}
}
