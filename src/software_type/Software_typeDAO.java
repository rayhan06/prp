package software_type;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.util.stream.Collectors;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Software_typeDAO  implements CommonDAOService<Software_typeDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Software_typeDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"software_cat",
			"software_sub_cat",
			"lot",
			"expiry_date",
			"purchase_date",
			"po_number",
			"asset_model_id",
			
			"quantity",
			"has_license",
			"can_be_assigned",
			
			
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("software_cat"," and (software_cat = ?)");
		searchMap.put("software_sub_cat"," and (software_sub_cat = ?)");
		searchMap.put("lot"," and (lot like ?)");
		searchMap.put("expiry_date_start"," and (expiry_date >= ?)");
		searchMap.put("expiry_date_end"," and (expiry_date <= ?)");
		searchMap.put("purchase_date_start"," and (purchase_date >= ?)");
		searchMap.put("purchase_date_end"," and (purchase_date <= ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Software_typeDAO INSTANCE = new Software_typeDAO();
	}

	public static Software_typeDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Software_typeDTO software_typeDTO)
	{
		software_typeDTO.searchColumn = "";
		software_typeDTO.searchColumn += CatDAO.getName("English", "software", software_typeDTO.softwareCat) + " " + CatDAO.getName("Bangla", "software", software_typeDTO.softwareCat) + " ";
		software_typeDTO.searchColumn += CatDAO.getName("English", "software_sub", software_typeDTO.softwareSubCat) + " " + CatDAO.getName("Bangla", "software_sub", software_typeDTO.softwareSubCat) + " ";
		software_typeDTO.searchColumn += software_typeDTO.lot + " ";
	}
	@Override
	public boolean deletePb(long modifiedBy, List<Long> ids, long deleteTime) {
    	
        String idStr = ids.stream()
                .distinct()
                .map(String::valueOf)
                .collect(Collectors.joining(","));
        try {
			SoftwareSubtypeDAO.getInstance().deleteChildrenByParent(idStr, "software_type_id");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        String sql = "UPDATE " + getTableName() + " SET isDeleted = 1, lastModificationTime = " + deleteTime;
        if (!getLastModifierUser().equalsIgnoreCase("")) {
            sql += ", " + getLastModifierUser() + " = " + modifiedBy;
        }
        sql += " WHERE can_be_assigned = 0 and id IN (" + idStr + ")";

        final String deleteSql = sql;
        loggerCommonDAOService.debug(sql);
        return (boolean) ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            try {
                model.getStatement().executeUpdate(deleteSql);
                recordUpdateTime(model.getConnection(), getTableName(), deleteTime);
                return true;
            } catch (SQLException ex) {
                ex.printStackTrace();
                return false;
            }
        });
    }
	
	@Override
	public void set(PreparedStatement ps, Software_typeDTO software_typeDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(software_typeDTO);
		if(isInsert)
		{
			ps.setObject(++index,software_typeDTO.iD);
		}
		ps.setObject(++index,software_typeDTO.softwareCat);
		ps.setObject(++index,software_typeDTO.softwareSubCat);
		ps.setObject(++index,software_typeDTO.lot);
		ps.setObject(++index,software_typeDTO.expiryDate);
		ps.setObject(++index,software_typeDTO.purchaseDate);
		ps.setObject(++index,software_typeDTO.poNumber);
		ps.setObject(++index,software_typeDTO.assetModelId);
		
		ps.setObject(++index,software_typeDTO.quantity);
		ps.setObject(++index,software_typeDTO.hasLicense);
		ps.setObject(++index,software_typeDTO.canBeAssigned);
		
		
		ps.setObject(++index,software_typeDTO.insertedByUserId);
		ps.setObject(++index,software_typeDTO.insertedByOrganogramId);
		ps.setObject(++index,software_typeDTO.insertionDate);
		ps.setObject(++index,software_typeDTO.lastModifierUser);
		ps.setObject(++index,software_typeDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(++index,software_typeDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,software_typeDTO.iD);
		}
	}
	
	@Override
	public Software_typeDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Software_typeDTO software_typeDTO = new Software_typeDTO();
			int i = 0;
			software_typeDTO.iD = rs.getLong(columnNames[i++]);
			software_typeDTO.softwareCat = rs.getInt(columnNames[i++]);
			software_typeDTO.softwareSubCat = rs.getInt(columnNames[i++]);
			software_typeDTO.lot = rs.getString(columnNames[i++]);
			software_typeDTO.expiryDate = rs.getLong(columnNames[i++]);
			software_typeDTO.purchaseDate = rs.getLong(columnNames[i++]);
			software_typeDTO.poNumber = rs.getString(columnNames[i++]);
			software_typeDTO.assetModelId = rs.getLong(columnNames[i++]);
			
			software_typeDTO.quantity = rs.getInt(columnNames[i++]);
			software_typeDTO.hasLicense = rs.getBoolean(columnNames[i++]);
			software_typeDTO.canBeAssigned = rs.getBoolean(columnNames[i++]);
			
			
			software_typeDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			software_typeDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			software_typeDTO.insertionDate = rs.getLong(columnNames[i++]);
			software_typeDTO.lastModifierUser = rs.getString(columnNames[i++]);
			software_typeDTO.searchColumn = rs.getString(columnNames[i++]);
			software_typeDTO.isDeleted = rs.getInt(columnNames[i++]);
			software_typeDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return software_typeDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	public String getDomainName(int cat)
	{
		String domainName = CatDAO.getName("English", "software", cat);
		domainName = domainName.toLowerCase().replaceAll(" ", "_").concat("_version");
		return domainName;
	}
	
	public String getName(int cat, int subCat)
	{
		String domainName = getDomainName(cat);
		return CatRepository.getName("English", domainName, subCat);
	}
	
		
	public Software_typeDTO getDTOByID (long id)
	{
		Software_typeDTO software_typeDTO = null;
		try 
		{
			software_typeDTO = getDTOFromID(id);
			if(software_typeDTO != null)
			{
				SoftwareSubtypeDAO softwareSubtypeDAO = SoftwareSubtypeDAO.getInstance();				
				List<SoftwareSubtypeDTO> softwareSubtypeDTOList = (List<SoftwareSubtypeDTO>)softwareSubtypeDAO.getDTOsByParent("software_type_id", software_typeDTO.iD);
				software_typeDTO.softwareSubtypeDTOList = softwareSubtypeDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return software_typeDTO;
	}
	
	public void finalize(long id)
	{
		Software_typeDTO software_typeDTO = getDTOByID (id);
		if(software_typeDTO != null)
		{
			software_typeDTO.canBeAssigned = true;
			try {
				update(software_typeDTO);
				SoftwareSubtypeDAO.getInstance().finalize(id);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public String getTableName() {
		return "software_type";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Software_typeDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Software_typeDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
				
}
	