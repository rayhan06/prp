package software_type;
import java.util.*; 
import util.*;


public class SoftwareSubtypeMAPS extends CommonMaps
{	
	public SoftwareSubtypeMAPS(String tableName)
	{
		


		java_SQL_map.put("software_type_id".toLowerCase(), "softwareTypeId".toLowerCase());
		java_SQL_map.put("license_key".toLowerCase(), "licenseKey".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Software Type Id".toLowerCase(), "softwareTypeId".toLowerCase());
		java_Text_map.put("License Key".toLowerCase(), "licenseKey".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}