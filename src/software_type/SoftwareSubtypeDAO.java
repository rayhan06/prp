package software_type;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.util.stream.Collectors;
import java.sql.SQLException;
import java.sql.Statement;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import user.UserDTO;
import user.UserRepository;

import org.apache.log4j.Logger;

import appointment.KeyCountDTO;
import asset_category.Asset_categoryDTO;
import util.*;


public class SoftwareSubtypeDAO  implements CommonDAOService<SoftwareSubtypeDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private SoftwareSubtypeDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"software_type_id",
			"software_cat",
			"software_sub_cat",
			"license_key",
			"name",
			"assigned_to_user_name",
			"assigned_to_org_id",
			"asset_assignee_id",
			"asset_model_id",
			"assignement_date",
			"expiry_date",
			"can_be_assigned",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("license_key"," and (license_key like ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final SoftwareSubtypeDAO INSTANCE = new SoftwareSubtypeDAO();
	}

	public static SoftwareSubtypeDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(SoftwareSubtypeDTO softwaresubtypeDTO)
	{
		softwaresubtypeDTO.searchColumn = "";
		softwaresubtypeDTO.searchColumn += softwaresubtypeDTO.licenseKey + " ";
	}
	
	public  long deleteChildrenByParent(String parentIds, String parentColName) throws Exception {
        long lastModificationTime = System.currentTimeMillis();
        StringBuilder sqlBuilder = new StringBuilder("UPDATE ")
                .append(getTableName())
                .append(" SET isDeleted=1,lastModificationTime=")
                .append(lastModificationTime).append(" WHERE can_be_assigned = 0 and ")
                .append(parentColName).append(" in ( ")
                .append(parentIds).append(")");
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            String sql = sqlBuilder.toString();
            Connection connection = model.getConnection();
            Statement stmt = model.getStatement();
            try {
                loggerCommonDAOService.debug(sql);
                stmt.execute(sql);
                recordUpdateTime(model.getConnection(), getTableName(), lastModificationTime);
            } catch (SQLException ex) {
                loggerCommonDAOService.error(ex);
            }
        });
        return 1L;
    }
	
	@Override
	public void set(PreparedStatement ps, SoftwareSubtypeDTO softwaresubtypeDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(softwaresubtypeDTO);
		if(isInsert)
		{
			ps.setObject(++index,softwaresubtypeDTO.iD);
		}
		ps.setObject(++index,softwaresubtypeDTO.softwareTypeId);
		ps.setObject(++index,softwaresubtypeDTO.softwareCat);
		ps.setObject(++index,softwaresubtypeDTO.softwareSubCat);
		ps.setObject(++index,softwaresubtypeDTO.licenseKey);
		ps.setObject(++index,softwaresubtypeDTO.name);
		ps.setObject(++index,softwaresubtypeDTO.assignedToUserName);
		ps.setObject(++index,softwaresubtypeDTO.assignedToOrgId);
		ps.setObject(++index,softwaresubtypeDTO.assetAssigneeId);
		ps.setObject(++index,softwaresubtypeDTO.assetModelId);
		
		ps.setObject(++index,softwaresubtypeDTO.assignmentDate);
		ps.setObject(++index,softwaresubtypeDTO.expiryDate);
		ps.setObject(++index,softwaresubtypeDTO.canBeAssigned);
		if(isInsert)
		{
			ps.setObject(++index,softwaresubtypeDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,softwaresubtypeDTO.iD);
		}
	}
	
	@Override
	public SoftwareSubtypeDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			SoftwareSubtypeDTO softwaresubtypeDTO = new SoftwareSubtypeDTO();
			int i = 0;
			softwaresubtypeDTO.iD = rs.getLong(columnNames[i++]);
			softwaresubtypeDTO.softwareTypeId = rs.getLong(columnNames[i++]);
			softwaresubtypeDTO.softwareCat = rs.getInt(columnNames[i++]);
			softwaresubtypeDTO.softwareSubCat = rs.getInt(columnNames[i++]);
			softwaresubtypeDTO.licenseKey = rs.getString(columnNames[i++]);
			softwaresubtypeDTO.name = rs.getString(columnNames[i++]);
			softwaresubtypeDTO.assignedToUserName = rs.getString(columnNames[i++]);
			softwaresubtypeDTO.assignedToOrgId = rs.getLong(columnNames[i++]);
			softwaresubtypeDTO.assetAssigneeId = rs.getLong(columnNames[i++]);
			softwaresubtypeDTO.assetModelId = rs.getLong(columnNames[i++]);
			
			softwaresubtypeDTO.assignmentDate = rs.getLong(columnNames[i++]);
			softwaresubtypeDTO.expiryDate = rs.getLong(columnNames[i++]);
			
			softwaresubtypeDTO.canBeAssigned = rs.getBoolean(columnNames[i++]);
			
			softwaresubtypeDTO.isDeleted = rs.getInt(columnNames[i++]);
			softwaresubtypeDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return softwaresubtypeDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	 public long finalize(long softwareTypeId) throws Exception 
	 {
        long lastModificationTime = System.currentTimeMillis();	
		String sql = "UPDATE software_sub_type " ;
		
		sql += " SET can_be_assigned = 1,";

		
		sql += " lastModificationTime = "	+ lastModificationTime + "";
        sql += " WHERE software_type_id = " + softwareTypeId;
        return (long) ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            Connection connection = model.getConnection();
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                ps.executeUpdate();
                recordUpdateTime(connection, "software_sub_type");
                return softwareTypeId;
            } catch (SQLException ex) {
                logger.error(ex);
                return -1L;
            }
        }, sql);
	}
	
	public List<SoftwareSubtypeDTO> getUnassigned(int cat, int subCat, String notIn) throws Exception {
        String sql = "select * from software_subtype WHERE software_cat=" + cat + " and software_sub_cat = " + subCat
        		+ " and assigned_to_org_id = -1 and isDeleted = 0 and "
        		+ "(select expiry_date from software_type where id = software_subtype.software_type_id) > " + System.currentTimeMillis();
        if(notIn != null & ! notIn.equalsIgnoreCase(""))
        {
        	sql += " and id not in (" + notIn + ")";
        }
        return ConnectionAndStatementUtil.getListOfT(sql,this::buildObjectFromResultSet);
    }
	
	public List<KeyCountDTO> getKeyedSoftStatus()
    {
		String sql = "SELECT \r\n" + 
				"    software_cat,\r\n" + 
				"    (CASE\r\n" + 
				"        WHEN (assigned_to_org_id = - 1) THEN 0\r\n" + 
				"        ELSE 1\r\n" + 
				"    END) AS assignment_status,\r\n" + 
				"    COUNT(id)\r\n" + 
				"FROM\r\n" + 
				"    software_subtype\r\n" + 
				"WHERE\r\n" + 
				"    license_key != '' and isDeleted = 0\r\n" + 
				"GROUP BY software_cat , assignment_status";
		return ConnectionAndStatementUtil.getListOfT(sql,this::getKeyCount);
    }
	
	public KeyCountDTO getKeyCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();			
			keyCountDTO.key = rs.getLong("software_cat");
			keyCountDTO.key2 = rs.getLong("assignment_status");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public void unassign(long id)
	{
		SoftwareSubtypeDTO sDTO = SoftwareSubtypeDAO.getInstance().getDTOByID(id);
		if(sDTO != null)
		{
			sDTO.assignedToOrgId = -1;
			sDTO.assignmentDate = TimeConverter.getToday();
			try {
				getInstance().update(sDTO);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void assign(long id, long userOrgId)
	{
		SoftwareSubtypeDTO sDTO = SoftwareSubtypeDAO.getInstance().getDTOByID(id);
		UserDTO userDTO = UserRepository.getUserDTOByOrganogramID(userOrgId);
		if(sDTO != null && userDTO!= null && sDTO.assignedToOrgId == -1)
		{
			sDTO.assignedToOrgId = userOrgId;
			sDTO.assignedToUserName = userDTO.userName;
			sDTO.assignmentDate = TimeConverter.getToday();
			try {
				getInstance().update(sDTO);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
		
	public SoftwareSubtypeDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "software_subtype";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((SoftwareSubtypeDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((SoftwareSubtypeDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	