package software_type;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class SoftwareSubtypeRepository implements Repository {
	SoftwareSubtypeDAO softwaresubtypeDAO = null;
	
	static Logger logger = Logger.getLogger(SoftwareSubtypeRepository.class);
	Map<Long, SoftwareSubtypeDTO>mapOfSoftwareSubtypeDTOToiD;
	Map<Long, Set<SoftwareSubtypeDTO> >mapOfSoftwareSubtypeDTOTosoftwareTypeId;
	Gson gson;

  
	private SoftwareSubtypeRepository(){
		softwaresubtypeDAO = SoftwareSubtypeDAO.getInstance();
		mapOfSoftwareSubtypeDTOToiD = new ConcurrentHashMap<>();
		mapOfSoftwareSubtypeDTOTosoftwareTypeId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static SoftwareSubtypeRepository INSTANCE = new SoftwareSubtypeRepository();
    }

    public static SoftwareSubtypeRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<SoftwareSubtypeDTO> softwaresubtypeDTOs = softwaresubtypeDAO.getAllDTOs(reloadAll);
			for(SoftwareSubtypeDTO softwaresubtypeDTO : softwaresubtypeDTOs) {
				SoftwareSubtypeDTO oldSoftwareSubtypeDTO = getSoftwareSubtypeDTOByiD(softwaresubtypeDTO.iD);
				if( oldSoftwareSubtypeDTO != null ) {
					mapOfSoftwareSubtypeDTOToiD.remove(oldSoftwareSubtypeDTO.iD);
				
					if(mapOfSoftwareSubtypeDTOTosoftwareTypeId.containsKey(oldSoftwareSubtypeDTO.softwareTypeId)) {
						mapOfSoftwareSubtypeDTOTosoftwareTypeId.get(oldSoftwareSubtypeDTO.softwareTypeId).remove(oldSoftwareSubtypeDTO);
					}
					if(mapOfSoftwareSubtypeDTOTosoftwareTypeId.get(oldSoftwareSubtypeDTO.softwareTypeId).isEmpty()) {
						mapOfSoftwareSubtypeDTOTosoftwareTypeId.remove(oldSoftwareSubtypeDTO.softwareTypeId);
					}
					
					
				}
				if(softwaresubtypeDTO.isDeleted == 0) 
				{
					
					mapOfSoftwareSubtypeDTOToiD.put(softwaresubtypeDTO.iD, softwaresubtypeDTO);
				
					if( ! mapOfSoftwareSubtypeDTOTosoftwareTypeId.containsKey(softwaresubtypeDTO.softwareTypeId)) {
						mapOfSoftwareSubtypeDTOTosoftwareTypeId.put(softwaresubtypeDTO.softwareTypeId, new HashSet<>());
					}
					mapOfSoftwareSubtypeDTOTosoftwareTypeId.get(softwaresubtypeDTO.softwareTypeId).add(softwaresubtypeDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public SoftwareSubtypeDTO clone(SoftwareSubtypeDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, SoftwareSubtypeDTO.class);
	}
	
	
	public List<SoftwareSubtypeDTO> getSoftwareSubtypeList() {
		List <SoftwareSubtypeDTO> softwaresubtypes = new ArrayList<SoftwareSubtypeDTO>(this.mapOfSoftwareSubtypeDTOToiD.values());
		return softwaresubtypes;
	}
	
	public List<SoftwareSubtypeDTO> copySoftwareSubtypeList() {
		List <SoftwareSubtypeDTO> softwaresubtypes = getSoftwareSubtypeList();
		return softwaresubtypes
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public SoftwareSubtypeDTO getSoftwareSubtypeDTOByiD( long iD){
		return mapOfSoftwareSubtypeDTOToiD.get(iD);
	}
	
	public SoftwareSubtypeDTO copySoftwareSubtypeDTOByiD( long iD){
		return clone(mapOfSoftwareSubtypeDTOToiD.get(iD));
	}
	
	public String getName( long iD){
		SoftwareSubtypeDTO softwareSubtypeDTO = getSoftwareSubtypeDTOByiD(iD);
		if(softwareSubtypeDTO != null)
		{
			return softwareSubtypeDTO.name;
		}
		return "";
	}
	
	public String getKey( long iD){
		SoftwareSubtypeDTO softwareSubtypeDTO = getSoftwareSubtypeDTOByiD(iD);
		if(softwareSubtypeDTO != null)
		{
			return softwareSubtypeDTO.licenseKey;
		}
		return "";
	}
	
	public long getExpiryDate( long iD){
		SoftwareSubtypeDTO softwareSubtypeDTO = getSoftwareSubtypeDTOByiD(iD);
		if(softwareSubtypeDTO == null)
		{
			return -1;
		}
		Software_typeDTO software_typeDTO = Software_typeRepository.getInstance().getSoftware_typeDTOByiD(softwareSubtypeDTO.softwareTypeId);
		if(software_typeDTO == null)
		{
			return -1;
		}
		return software_typeDTO.expiryDate;
	}
	
	public String getExpiryDateStr( long iD)
	{
		if(getExpiryDate(iD) == -1)
		{
			return "";
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		return simpleDateFormat.format(new Date(iD));
	}
	
	
	public List<SoftwareSubtypeDTO> getSoftwareSubtypeDTOBysoftwareTypeId(long softwareTypeId) {
		return new ArrayList<>( mapOfSoftwareSubtypeDTOTosoftwareTypeId.getOrDefault(softwareTypeId,new HashSet<>()));
	}
	
	public List<SoftwareSubtypeDTO> copySoftwareSubtypeDTOBysoftwareTypeId(long softwareTypeId)
	{
		List <SoftwareSubtypeDTO> softwaresubtypes = getSoftwareSubtypeDTOBysoftwareTypeId(softwareTypeId);
		return softwaresubtypes
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return softwaresubtypeDAO.getTableName();
	}
}


