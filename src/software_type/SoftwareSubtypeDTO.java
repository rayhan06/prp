package software_type;
import java.util.*;

import pb.CatDAO;
import util.*; 


public class SoftwareSubtypeDTO extends CommonDTO
{

	public long softwareTypeId = -1;
    public String licenseKey = "";
    
    public long assignedToOrgId = -1;
    public long assignmentDate = 0;
    public String assignedToUserName = "";
    
    public int softwareCat = -1;
	public int softwareSubCat = -1;
	
	public long assetAssigneeId = -1;
	public long assetModelId = -1;
	public long expiryDate = -1;
	
	public boolean canBeAssigned = false;
	
	public String name = "";
	
	public List<SoftwareSubtypeDTO> softwareSubtypeDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$SoftwareSubtypeDTO[" +
            " iD = " + iD +
            " softwareTypeId = " + softwareTypeId +
            " licenseKey = " + licenseKey +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }
    
    public SoftwareSubtypeDTO()
    {
    	
    }
    
    public SoftwareSubtypeDTO(Software_typeDTO software_typeDTO)
    {
    	softwareTypeId = software_typeDTO.iD;
    	softwareCat = software_typeDTO.softwareCat;
    	softwareSubCat = software_typeDTO.softwareSubCat;
    	String domainName = "";
		if(softwareSubCat == Software_typeDTO.OS)
		{
			domainName = "os_version";
		}
		else if(softwareSubCat == Software_typeDTO.OFFICE)
		{
			domainName = "office_version";
		}
		else if(softwareSubCat == Software_typeDTO.AV)
		{
			domainName = "antivirus_version";
		}
		expiryDate = software_typeDTO.expiryDate;
		name = CatDAO.getName("English", domainName, softwareSubCat);
    }

}