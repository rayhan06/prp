package software_type;
import java.util.*; 
import util.*; 


public class Software_typeDTO extends CommonDTO
{

	public int softwareCat = -1;
	public int softwareSubCat = -1;
    public String lot = "";
	public long expiryDate = System.currentTimeMillis();
	public long purchaseDate = System.currentTimeMillis();
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
    public String poNumber = "";
    
    public long assetModelId = -1;
    
    public int quantity = 1;
    public boolean hasLicense = true;
    public boolean canBeAssigned = false;
    
    public static final int OS = 0;
    public static final int OFFICE = 1;
    public static final int AV = 2;
	
	public List<SoftwareSubtypeDTO> softwareSubtypeDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Software_typeDTO[" +
            " iD = " + iD +
            " softwareCat = " + softwareCat +
            " softwareSubCat = " + softwareSubCat +
            " lot = " + lot +
            " expiryDate = " + expiryDate +
            " purchaseDate = " + purchaseDate +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}