package software_type;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Software_typeRepository implements Repository {
	Software_typeDAO software_typeDAO = null;
	
	static Logger logger = Logger.getLogger(Software_typeRepository.class);
	Map<Long, Software_typeDTO>mapOfSoftware_typeDTOToiD;
	Gson gson;

  
	private Software_typeRepository(){
		software_typeDAO = Software_typeDAO.getInstance();
		mapOfSoftware_typeDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Software_typeRepository INSTANCE = new Software_typeRepository();
    }

    public static Software_typeRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Software_typeDTO> software_typeDTOs = software_typeDAO.getAllDTOs(reloadAll);
			for(Software_typeDTO software_typeDTO : software_typeDTOs) {
				Software_typeDTO oldSoftware_typeDTO = getSoftware_typeDTOByiD(software_typeDTO.iD);
				if( oldSoftware_typeDTO != null ) {
					mapOfSoftware_typeDTOToiD.remove(oldSoftware_typeDTO.iD);
				
					
				}
				if(software_typeDTO.isDeleted == 0) 
				{
					
					mapOfSoftware_typeDTOToiD.put(software_typeDTO.iD, software_typeDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Software_typeDTO clone(Software_typeDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Software_typeDTO.class);
	}
	
	
	public List<Software_typeDTO> getSoftware_typeList() {
		List <Software_typeDTO> software_types = new ArrayList<Software_typeDTO>(this.mapOfSoftware_typeDTOToiD.values());
		return software_types;
	}
	
	public List<Software_typeDTO> copySoftware_typeList() {
		List <Software_typeDTO> software_types = getSoftware_typeList();
		return software_types
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Software_typeDTO getSoftware_typeDTOByiD( long iD){
		return mapOfSoftware_typeDTOToiD.get(iD);
	}
	
	public Software_typeDTO copySoftware_typeDTOByiD( long iD){
		return clone(mapOfSoftware_typeDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return software_typeDAO.getTableName();
	}
}


