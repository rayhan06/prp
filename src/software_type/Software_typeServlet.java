package software_type;


import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;


import permission.MenuConstants;

import user.UserDTO;
import util.*;
import javax.servlet.http.*;
import java.util.*;


import language.LC;
import language.LM;
import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;


import asset_model.AssetAssigneeDAO;
import asset_model.AssetAssigneeDTO;
import asset_model.Asset_modelDAO;
import asset_model.Asset_modelDTO;
import asset_transaction.Asset_transactionDAO;
import asset_transaction.Asset_transactionDTO;
import category.CategoryDAO;
import category.CategoryDTO;
import pb.*;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Software_typeServlet
 */
@WebServlet("/Software_typeServlet")
@MultipartConfig
public class Software_typeServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Software_typeServlet.class);
    Asset_modelDAO asset_modelDAO = new Asset_modelDAO();
    AssetAssigneeDAO assetAssigneeDAO = new AssetAssigneeDAO();
    Asset_transactionDAO asset_transactionDAO = new Asset_transactionDAO();

    @Override
    public String getTableName() {
        return Software_typeDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Software_typeServlet";
    }

    @Override
    public Software_typeDAO getCommonDAOService() {
        return Software_typeDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.SOFTWARE_TYPE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.SOFTWARE_TYPE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.SOFTWARE_TYPE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Software_typeServlet.class;
    }
	SoftwareSubtypeDAO softwareSubtypeDAO = SoftwareSubtypeDAO.getInstance();
    private final Gson gson = new Gson();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        init(request);
        String actionType = request.getParameter("actionType");
		if ("getSubType".equals(actionType)) {
			try {
				int cat = Integer.parseInt(request.getParameter("cat"));
				int subCat = Integer.parseInt(request.getParameter("subCat"));
				String domainName = Software_typeDAO.getInstance().getDomainName(cat);
			
				String options = CatDAO.getOptions("English", domainName, subCat);
				response.getWriter().write(options);
				
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else if ("getSubTypeForReport".equals(actionType)) {
			try {
				int cat = Integer.parseInt(request.getParameter("cat"));

				String domainName = Software_typeDAO.getInstance().getDomainName(cat);
			
				String options = CatDAO.getOptions("English", domainName, -2);
				response.getWriter().write(options);
				
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else if ("finalize".equals(actionType)) {
			try {
				long id = Integer.parseInt(request.getParameter("id"));
				Software_typeDAO.getInstance().finalize(id);
				
				SoftwareSubtypeRepository.getInstance().reload(true);
				Software_typeRepository.getInstance().reload(true);
				
				Software_typeDTO software_typeDTO = Software_typeDAO.getInstance().getDTOByID(id);
				if(software_typeDTO != null)
				{
					Asset_modelDTO asset_modelDTO = new Asset_modelDTO(software_typeDTO);
					
					asset_modelDAO.add(asset_modelDTO);
					
					if(software_typeDTO.softwareSubtypeDTOList != null)
					{
						for(SoftwareSubtypeDTO softwareSubtypeDTO: software_typeDTO.softwareSubtypeDTOList)
						{
							AssetAssigneeDTO assetAssigneeDTO = new AssetAssigneeDTO(asset_modelDTO, softwareSubtypeDTO);
							assetAssigneeDAO.add(assetAssigneeDTO);
							Asset_transactionDTO asset_transactionDTO = new Asset_transactionDTO(asset_modelDTO, assetAssigneeDTO, Asset_transactionDTO.STOCK_IN);
							asset_transactionDAO.add(asset_transactionDTO);
						}
					}
				}
				response.sendRedirect(getDeleteRedirectURL(request));
				
				
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else if ("getUnassignedKeys".equals(actionType)) {
			try {
				int cat = Integer.parseInt(request.getParameter("cat"));
				int subCat = Integer.parseInt(request.getParameter("subCat"));
				long keyId = Long.parseLong(request.getParameter("keyId"));
				String notIn = request.getParameter("notIn");
				if(notIn.endsWith(","))
				{
					notIn = notIn.substring(0, notIn.length() - 1);
				}
				List<SoftwareSubtypeDTO> softwareSubtypeDTOs = softwareSubtypeDAO.getUnassigned(cat, subCat, notIn);
				String options = "";
				if(keyId == -1)
				{
					options = "<option value = '-1'>" + LM.getText(LC.HM_SELECT, commonLoginData.loginDTO) + "</option>";
				}
				else
				{
					options += "<option value = '" + keyId + "'>" + SoftwareSubtypeRepository.getInstance().getKey(keyId) + "</option>";
					options += "<option value = '-1'>" + LM.getText(LC.HM_NONE, commonLoginData.loginDTO) + "</option>";
				}
				
				
				if(softwareSubtypeDTOs != null)
				{
					for(SoftwareSubtypeDTO sub: softwareSubtypeDTOs)
					{
						options += "<option value = '" + sub.iD + "'>" + sub.licenseKey + "</option>";
					}
				}
				response.getWriter().write(options);
				
			} catch (Exception ex) {
				ex.printStackTrace();
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else
		{
			super.doGet(request,response);
		}
    }

 	

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addSoftware_type");
		String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		Software_typeDTO software_typeDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
		if(addFlag)
		{
			software_typeDTO = new Software_typeDTO();
		}
		else
		{
			software_typeDTO = Software_typeDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
			if(software_typeDTO.canBeAssigned)
			{
				return null;
			}
		}
		
		String Value = "";

		Value = request.getParameter("softwareCat");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("softwareCat = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			software_typeDTO.softwareCat = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		Value = request.getParameter("otherSoftware");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("otherSoftware = " + Value);
		if(Value != null && !Value.equalsIgnoreCase("") && software_typeDTO.softwareCat == -1)
		{
			CategoryDTO categoryDTO = new CategoryDTO("software", Value, Value);
			CategoryDAO.getInstance().add(categoryDTO);
			software_typeDTO.softwareCat = categoryDTO.value;
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("softwareSubCat");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("softwareSubCat = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			software_typeDTO.softwareSubCat = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		Value = request.getParameter("otherSoftwareSub");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("otherSoftwareSub = " + Value);
		if(Value != null && !Value.equalsIgnoreCase("") && software_typeDTO.softwareSubCat == -1)
		{
			CatDTO softDTO = CatRepository.getDTO("software", software_typeDTO.softwareCat);
			CategoryDTO categoryDTO = new CategoryDTO(softDTO.languageTextEnglish.toLowerCase() + "_version", Value, Value);
			CategoryDAO.getInstance().add(categoryDTO);
			software_typeDTO.softwareSubCat = categoryDTO.value;
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("lot");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("lot = " + Value);
		if(Value != null)
		{
			software_typeDTO.lot = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		Value = request.getParameter("poNumber");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("poNumber = " + Value);
		if(Value != null)
		{
			software_typeDTO.poNumber = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("expiryDate");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("expiryDate = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			try 
			{
				Date d = f.parse(Value);
				software_typeDTO.expiryDate = d.getTime();
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				throw new Exception(LM.getText(LC.SOFTWARE_TYPE_ADD_EXPIRYDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("purchaseDate");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("purchaseDate = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			try 
			{
				Date d = f.parse(Value);
				software_typeDTO.purchaseDate = d.getTime();
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				throw new Exception(LM.getText(LC.SOFTWARE_TYPE_ADD_PURCHASEDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		if(addFlag)
		{
			software_typeDTO.insertedByUserId = userDTO.ID;
		}


		if(addFlag)
		{
			software_typeDTO.insertedByOrganogramId = userDTO.organogramID;
		}


		if(addFlag)
		{				
			software_typeDTO.insertionDate = TimeConverter.getToday();
		}			


		software_typeDTO.lastModifierUser = userDTO.userName;
		software_typeDTO.canBeAssigned = false;
		
		Value = request.getParameter("hasLicense");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("hasLicense = " + Value);
        software_typeDTO.hasLicense = Value != null;

		System.out.println("Done adding  addSoftware_type dto = " + software_typeDTO);

		if(addFlag == true)
		{
			Software_typeDAO.getInstance().add(software_typeDTO);
		}
		else
		{				
			Software_typeDAO.getInstance().update(software_typeDTO);										
		}
		
		if(software_typeDTO.hasLicense)
		{
			List<SoftwareSubtypeDTO> softwareSubtypeDTOList = createSoftwareSubtypeDTOListByRequest(request, language, software_typeDTO, userDTO);
			software_typeDTO.quantity = softwareSubtypeDTOList.size();
			Software_typeDAO.getInstance().update(software_typeDTO);
	
			if(addFlag == true) //add or validate
			{
				if(softwareSubtypeDTOList != null)
				{				
					for(SoftwareSubtypeDTO softwareSubtypeDTO: softwareSubtypeDTOList)
					{
						softwareSubtypeDAO.add(softwareSubtypeDTO);
					}
				}
			
			}			
			else
			{
				List<Long> childIdsFromRequest = softwareSubtypeDAO.getChildIdsFromRequest(request, "softwareSubtype");
				//delete the removed children
				softwareSubtypeDAO.deleteChildrenNotInList("software_type", "software_subtype", software_typeDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = softwareSubtypeDAO.getChilIds("software_type", "software_subtype", software_typeDTO.iD);
				
				
				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							SoftwareSubtypeDTO softwareSubtypeDTO =  createSoftwareSubtypeDTOByRequestAndIndex(request, false, i, language, software_typeDTO, userDTO);
							softwareSubtypeDAO.update(softwareSubtypeDTO);
						}
						else
						{
							SoftwareSubtypeDTO softwareSubtypeDTO =  createSoftwareSubtypeDTOByRequestAndIndex(request, true, i, language, software_typeDTO, userDTO);
							softwareSubtypeDAO.add(softwareSubtypeDTO);
						}
					}
				}
				else
				{
					softwareSubtypeDAO.deleteChildrenByParent(software_typeDTO.iD, "software_type_id");
				}
				
				
			}
		}
		else
		{
			Value = request.getParameter("quantity");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("quantity = " + Value);
			if(Value != null)
			{
				software_typeDTO.quantity = Integer.parseInt(Value);
				if(software_typeDTO.quantity <= 0)
				{
					software_typeDTO.quantity = 1;
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Software_typeDAO.getInstance().update(software_typeDTO);
			softwareSubtypeDAO.deleteChildrenByParent(software_typeDTO.iD, "software_type_id");
			
			for(int i = 0; i < software_typeDTO.quantity; i ++)
			{
				SoftwareSubtypeDTO softwareSubtypeDTO = new SoftwareSubtypeDTO(software_typeDTO);
				softwareSubtypeDAO.add(softwareSubtypeDTO);
			}
		}
		SoftwareSubtypeRepository.getInstance().reload(true);
		Software_typeRepository.getInstance().reload(true);
		return software_typeDTO;

	}
	private List<SoftwareSubtypeDTO> createSoftwareSubtypeDTOListByRequest(HttpServletRequest request, String language, Software_typeDTO software_typeDTO, UserDTO userDTO) throws Exception{ 
		List<SoftwareSubtypeDTO> softwareSubtypeDTOList = new ArrayList<SoftwareSubtypeDTO>();
		if(request.getParameterValues("softwareSubtype.iD") != null) 
		{
			int softwareSubtypeItemNo = request.getParameterValues("softwareSubtype.iD").length;
			
			
			for(int index=0;index<softwareSubtypeItemNo;index++){
				SoftwareSubtypeDTO softwareSubtypeDTO = createSoftwareSubtypeDTOByRequestAndIndex(request,true,index, language, software_typeDTO, userDTO);
				softwareSubtypeDTOList.add(softwareSubtypeDTO);
			}
			
			return softwareSubtypeDTOList;
		}
		return null;
	}
	
	private SoftwareSubtypeDTO createSoftwareSubtypeDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, String language, Software_typeDTO software_typeDTO, UserDTO userDTO) throws Exception{
	
		SoftwareSubtypeDTO softwareSubtypeDTO;
		if(addFlag == true )
		{
			softwareSubtypeDTO = new SoftwareSubtypeDTO(software_typeDTO);
		}
		else
		{
			softwareSubtypeDTO = softwareSubtypeDAO.getDTOByID(Long.parseLong(request.getParameterValues("softwareSubtype.iD")[index]));
		}

		
		String Value = "";

		Value = request.getParameterValues("softwareSubtype.licenseKey")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("licenseKey = " + Value);
		if(Value != null)
		{
			softwareSubtypeDTO.licenseKey = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		return softwareSubtypeDTO;
	
	}	
}

