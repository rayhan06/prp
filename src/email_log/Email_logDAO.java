package email_log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Email_logDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public Email_logDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";

		commonMaps = new Email_logMAPS(tableName);
	}
	
	public Email_logDAO()
	{
		this("email_log");		
	}
	
	
	
	public long add(CommonDTO commonDTO) throws Exception
	{
		
		Email_logDTO email_logDTO = (Email_logDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			email_logDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "from_addr";
			sql += ", ";
			sql += "to_addr";
			sql += ", ";
			sql += "to_addr_book";
			sql += ", ";
			sql += "cc";
			sql += ", ";
			sql += "cc_addr_book";
			sql += ", ";
			sql += "bcc";
			sql += ", ";
			sql += "subject";
			sql += ", ";
			sql += "email_body";
			sql += ", ";
			sql += "files_dropzone";
			sql += ", ";
			sql += "is_draft";
			sql += ", ";
			sql += "sending_time";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
		
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
			sql += ")";
				

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,email_logDTO.iD);
			ps.setObject(index++,email_logDTO.fromAddr);
			ps.setObject(index++,email_logDTO.toAddr);
			ps.setObject(index++,email_logDTO.toAddrBook);
			ps.setObject(index++,email_logDTO.cc);
			ps.setObject(index++,email_logDTO.ccAddrBook);
			ps.setObject(index++,email_logDTO.bcc);
			ps.setObject(index++,email_logDTO.subject);
			ps.setObject(index++,email_logDTO.emailBody);
			ps.setObject(index++,email_logDTO.filesDropzone);
			ps.setObject(index++,email_logDTO.isDraft);
			ps.setObject(index++,email_logDTO.sendingTime);
			ps.setObject(index++,email_logDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return email_logDTO.iD;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Email_logDTO email_logDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				email_logDTO = new Email_logDTO();

				email_logDTO.iD = rs.getLong("ID");
				email_logDTO.fromAddr = rs.getString("from_addr");
				email_logDTO.toAddr = rs.getString("to_addr");
				email_logDTO.toAddrBook = rs.getLong("to_addr_book");
				email_logDTO.cc = rs.getString("cc");
				email_logDTO.ccAddrBook = rs.getLong("cc_addr_book");
				email_logDTO.bcc = rs.getString("bcc");
				email_logDTO.subject = rs.getString("subject");
				email_logDTO.emailBody = rs.getString("email_body");
				email_logDTO.filesDropzone = rs.getLong("files_dropzone");
				email_logDTO.isDraft = rs.getInt("is_draft");
				email_logDTO.sendingTime = rs.getLong("sending_time");
				email_logDTO.isDeleted = rs.getInt("isDeleted");
				email_logDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return email_logDTO;
	}
	
	public long update(CommonDTO commonDTO) throws Exception
	{		
		Email_logDTO email_logDTO = (Email_logDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "from_addr=?";
			sql += ", ";
			sql += "to_addr=?";
			sql += ", ";
			sql += "to_addr_book=?";
			sql += ", ";
			sql += "cc=?";
			sql += ", ";
			sql += "cc_addr_book=?";
			sql += ", ";
			sql += "bcc=?";
			sql += ", ";
			sql += "subject=?";
			sql += ", ";
			sql += "email_body=?";
			sql += ", ";
			sql += "files_dropzone=?";
			sql += ", ";
			sql += "is_draft=?";
			sql += ", ";
			sql += "sending_time=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + email_logDTO.iD;
				

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,email_logDTO.fromAddr);
			ps.setObject(index++,email_logDTO.toAddr);
			ps.setObject(index++,email_logDTO.toAddrBook);
			ps.setObject(index++,email_logDTO.cc);
			ps.setObject(index++,email_logDTO.ccAddrBook);
			ps.setObject(index++,email_logDTO.bcc);
			ps.setObject(index++,email_logDTO.subject);
			ps.setObject(index++,email_logDTO.emailBody);
			ps.setObject(index++,email_logDTO.filesDropzone);
			ps.setObject(index++,email_logDTO.isDraft);
			ps.setObject(index++,email_logDTO.sendingTime);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection,lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return email_logDTO.iD;
	}
	
	
	public List<Email_logDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Email_logDTO email_logDTO = null;
		List<Email_logDTO> email_logDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return email_logDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				email_logDTO = new Email_logDTO();
				email_logDTO.iD = rs.getLong("ID");
				email_logDTO.fromAddr = rs.getString("from_addr");
				email_logDTO.toAddr = rs.getString("to_addr");
				email_logDTO.toAddrBook = rs.getLong("to_addr_book");
				email_logDTO.cc = rs.getString("cc");
				email_logDTO.ccAddrBook = rs.getLong("cc_addr_book");
				email_logDTO.bcc = rs.getString("bcc");
				email_logDTO.subject = rs.getString("subject");
				email_logDTO.emailBody = rs.getString("email_body");
				email_logDTO.filesDropzone = rs.getLong("files_dropzone");
				email_logDTO.isDraft = rs.getInt("is_draft");
				email_logDTO.sendingTime = rs.getLong("sending_time");
				email_logDTO.isDeleted = rs.getInt("isDeleted");
				email_logDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + email_logDTO);
				
				email_logDTOList.add(email_logDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return email_logDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Email_logDTO> getAllEmail_log (boolean isFirstReload)
    {
		List<Email_logDTO> email_logDTOList = new ArrayList<>();

		String sql = "SELECT * FROM email_log";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by email_log.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Email_logDTO email_logDTO = new Email_logDTO();
				email_logDTO.iD = rs.getLong("ID");
				email_logDTO.fromAddr = rs.getString("from_addr");
				email_logDTO.toAddr = rs.getString("to_addr");
				email_logDTO.toAddrBook = rs.getLong("to_addr_book");
				email_logDTO.cc = rs.getString("cc");
				email_logDTO.ccAddrBook = rs.getLong("cc_addr_book");
				email_logDTO.bcc = rs.getString("bcc");
				email_logDTO.subject = rs.getString("subject");
				email_logDTO.emailBody = rs.getString("email_body");
				email_logDTO.filesDropzone = rs.getLong("files_dropzone");
				email_logDTO.isDraft = rs.getInt("is_draft");
				email_logDTO.sendingTime = rs.getLong("sending_time");
				email_logDTO.isDeleted = rs.getInt("isDeleted");
				email_logDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				email_logDTOList.add(email_logDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return email_logDTOList;
    }

	
	public List<Email_logDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Email_logDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Email_logDTO> email_logDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Email_logDTO email_logDTO = new Email_logDTO();
				email_logDTO.iD = rs.getLong("ID");
				email_logDTO.fromAddr = rs.getString("from_addr");
				email_logDTO.toAddr = rs.getString("to_addr");
				email_logDTO.toAddrBook = rs.getLong("to_addr_book");
				email_logDTO.cc = rs.getString("cc");
				email_logDTO.ccAddrBook = rs.getLong("cc_addr_book");
				email_logDTO.bcc = rs.getString("bcc");
				email_logDTO.subject = rs.getString("subject");
				email_logDTO.emailBody = rs.getString("email_body");
				email_logDTO.filesDropzone = rs.getLong("files_dropzone");
				email_logDTO.isDraft = rs.getInt("is_draft");
				email_logDTO.sendingTime = rs.getLong("sending_time");
				email_logDTO.isDeleted = rs.getInt("isDeleted");
				email_logDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				email_logDTOList.add(email_logDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return email_logDTOList;
	
	}
				
}
	