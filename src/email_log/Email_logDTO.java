package email_log;
import java.util.*; 
import util.*; 


public class Email_logDTO extends CommonDTO
{

    public String fromAddr = "";
    public String toAddr = "";
    public Long toAddrBook;
    public String cc = "";
    public Long ccAddrBook;
    public String bcc = "";
    public String subject = "";
    public String emailBody = "";
	public long filesDropzone = 0;
	public int isDraft = 0;
	public long sendingTime = 0;
	
	
    @Override
	public String toString() {
            return "$Email_logDTO[" +
            " iD = " + iD +
            " fromAddr = " + fromAddr +
            " toAddr = " + toAddr +
            " cc = " + cc +
            " subject = " + subject +
            " emailBody = " + emailBody +
            " filesDropzone = " + filesDropzone +
            " isDraft = " + isDraft +
            " sendingTime = " + sendingTime +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}