package email_log;
import java.util.*; 
import util.*;


public class Email_logMAPS extends CommonMaps
{	
	public Email_logMAPS(String tableName)
	{
		
		java_allfield_type_map.put("from_addr".toLowerCase(), "String");
		java_allfield_type_map.put("to_addr".toLowerCase(), "String");
		java_allfield_type_map.put("cc".toLowerCase(), "String");
		java_allfield_type_map.put("subject".toLowerCase(), "String");
		java_allfield_type_map.put("email_body".toLowerCase(), "String");
		java_allfield_type_map.put("is_draft".toLowerCase(), "Integer");
		java_allfield_type_map.put("sending_time".toLowerCase(), "Long");


		java_anyfield_search_map.put(tableName + ".from_addr".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".to_addr".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".cc".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".subject".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".email_body".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".is_draft".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".sending_time".toLowerCase(), "Long");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("fromAddr".toLowerCase(), "fromAddr".toLowerCase());
		java_DTO_map.put("toAddr".toLowerCase(), "toAddr".toLowerCase());
		java_DTO_map.put("cc".toLowerCase(), "cc".toLowerCase());
		java_DTO_map.put("subject".toLowerCase(), "subject".toLowerCase());
		java_DTO_map.put("emailBody".toLowerCase(), "emailBody".toLowerCase());
		java_DTO_map.put("filesDropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_DTO_map.put("isDraft".toLowerCase(), "isDraft".toLowerCase());
		java_DTO_map.put("sendingTime".toLowerCase(), "sendingTime".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("from_addr".toLowerCase(), "fromAddr".toLowerCase());
		java_SQL_map.put("to_addr".toLowerCase(), "toAddr".toLowerCase());
		java_SQL_map.put("cc".toLowerCase(), "cc".toLowerCase());
		java_SQL_map.put("subject".toLowerCase(), "subject".toLowerCase());
		java_SQL_map.put("email_body".toLowerCase(), "emailBody".toLowerCase());
		java_SQL_map.put("files_dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_SQL_map.put("is_draft".toLowerCase(), "isDraft".toLowerCase());
		java_SQL_map.put("sending_time".toLowerCase(), "sendingTime".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("From Addr".toLowerCase(), "fromAddr".toLowerCase());
		java_Text_map.put("To Addr".toLowerCase(), "toAddr".toLowerCase());
		java_Text_map.put("Cc".toLowerCase(), "cc".toLowerCase());
		java_Text_map.put("Subject".toLowerCase(), "subject".toLowerCase());
		java_Text_map.put("Email Body".toLowerCase(), "emailBody".toLowerCase());
		java_Text_map.put("Files Dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_Text_map.put("Is Draft".toLowerCase(), "isDraft".toLowerCase());
		java_Text_map.put("Sending Time".toLowerCase(), "sendingTime".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}