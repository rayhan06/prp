package email_log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Email_logRepository implements Repository {
	Email_logDAO email_logDAO = null;
	
	public void setDAO(Email_logDAO email_logDAO)
	{
		this.email_logDAO = email_logDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Email_logRepository.class);
	Map<Long, Email_logDTO>mapOfEmail_logDTOToiD;
	Map<String, Set<Email_logDTO> >mapOfEmail_logDTOTofromAddr;
	Map<String, Set<Email_logDTO> >mapOfEmail_logDTOTotoAddr;
	Map<String, Set<Email_logDTO> >mapOfEmail_logDTOTocc;
	Map<String, Set<Email_logDTO> >mapOfEmail_logDTOTosubject;
	Map<String, Set<Email_logDTO> >mapOfEmail_logDTOToemailBody;
	Map<Long, Set<Email_logDTO> >mapOfEmail_logDTOTofilesDropzone;
	Map<Integer, Set<Email_logDTO> >mapOfEmail_logDTOToisDraft;
	Map<Long, Set<Email_logDTO> >mapOfEmail_logDTOTosendingTime;
	Map<Long, Set<Email_logDTO> >mapOfEmail_logDTOTolastModificationTime;


	static Email_logRepository instance = null;  
	private Email_logRepository(){
		mapOfEmail_logDTOToiD = new ConcurrentHashMap<>();
		mapOfEmail_logDTOTofromAddr = new ConcurrentHashMap<>();
		mapOfEmail_logDTOTotoAddr = new ConcurrentHashMap<>();
		mapOfEmail_logDTOTocc = new ConcurrentHashMap<>();
		mapOfEmail_logDTOTosubject = new ConcurrentHashMap<>();
		mapOfEmail_logDTOToemailBody = new ConcurrentHashMap<>();
		mapOfEmail_logDTOTofilesDropzone = new ConcurrentHashMap<>();
		mapOfEmail_logDTOToisDraft = new ConcurrentHashMap<>();
		mapOfEmail_logDTOTosendingTime = new ConcurrentHashMap<>();
		mapOfEmail_logDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Email_logRepository getInstance(){
		if (instance == null){
			instance = new Email_logRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(email_logDAO == null)
		{
			return;
		}
		try {
			List<Email_logDTO> email_logDTOs = email_logDAO.getAllEmail_log(reloadAll);
			for(Email_logDTO email_logDTO : email_logDTOs) {
				Email_logDTO oldEmail_logDTO = getEmail_logDTOByID(email_logDTO.iD);
				if( oldEmail_logDTO != null ) {
					mapOfEmail_logDTOToiD.remove(oldEmail_logDTO.iD);
				
					if(mapOfEmail_logDTOTofromAddr.containsKey(oldEmail_logDTO.fromAddr)) {
						mapOfEmail_logDTOTofromAddr.get(oldEmail_logDTO.fromAddr).remove(oldEmail_logDTO);
					}
					if(mapOfEmail_logDTOTofromAddr.get(oldEmail_logDTO.fromAddr).isEmpty()) {
						mapOfEmail_logDTOTofromAddr.remove(oldEmail_logDTO.fromAddr);
					}
					
					if(mapOfEmail_logDTOTotoAddr.containsKey(oldEmail_logDTO.toAddr)) {
						mapOfEmail_logDTOTotoAddr.get(oldEmail_logDTO.toAddr).remove(oldEmail_logDTO);
					}
					if(mapOfEmail_logDTOTotoAddr.get(oldEmail_logDTO.toAddr).isEmpty()) {
						mapOfEmail_logDTOTotoAddr.remove(oldEmail_logDTO.toAddr);
					}
					
					if(mapOfEmail_logDTOTocc.containsKey(oldEmail_logDTO.cc)) {
						mapOfEmail_logDTOTocc.get(oldEmail_logDTO.cc).remove(oldEmail_logDTO);
					}
					if(mapOfEmail_logDTOTocc.get(oldEmail_logDTO.cc).isEmpty()) {
						mapOfEmail_logDTOTocc.remove(oldEmail_logDTO.cc);
					}
					
					if(mapOfEmail_logDTOTosubject.containsKey(oldEmail_logDTO.subject)) {
						mapOfEmail_logDTOTosubject.get(oldEmail_logDTO.subject).remove(oldEmail_logDTO);
					}
					if(mapOfEmail_logDTOTosubject.get(oldEmail_logDTO.subject).isEmpty()) {
						mapOfEmail_logDTOTosubject.remove(oldEmail_logDTO.subject);
					}
					
					if(mapOfEmail_logDTOToemailBody.containsKey(oldEmail_logDTO.emailBody)) {
						mapOfEmail_logDTOToemailBody.get(oldEmail_logDTO.emailBody).remove(oldEmail_logDTO);
					}
					if(mapOfEmail_logDTOToemailBody.get(oldEmail_logDTO.emailBody).isEmpty()) {
						mapOfEmail_logDTOToemailBody.remove(oldEmail_logDTO.emailBody);
					}
					
					if(mapOfEmail_logDTOTofilesDropzone.containsKey(oldEmail_logDTO.filesDropzone)) {
						mapOfEmail_logDTOTofilesDropzone.get(oldEmail_logDTO.filesDropzone).remove(oldEmail_logDTO);
					}
					if(mapOfEmail_logDTOTofilesDropzone.get(oldEmail_logDTO.filesDropzone).isEmpty()) {
						mapOfEmail_logDTOTofilesDropzone.remove(oldEmail_logDTO.filesDropzone);
					}
					
					if(mapOfEmail_logDTOToisDraft.containsKey(oldEmail_logDTO.isDraft)) {
						mapOfEmail_logDTOToisDraft.get(oldEmail_logDTO.isDraft).remove(oldEmail_logDTO);
					}
					if(mapOfEmail_logDTOToisDraft.get(oldEmail_logDTO.isDraft).isEmpty()) {
						mapOfEmail_logDTOToisDraft.remove(oldEmail_logDTO.isDraft);
					}
					
					if(mapOfEmail_logDTOTosendingTime.containsKey(oldEmail_logDTO.sendingTime)) {
						mapOfEmail_logDTOTosendingTime.get(oldEmail_logDTO.sendingTime).remove(oldEmail_logDTO);
					}
					if(mapOfEmail_logDTOTosendingTime.get(oldEmail_logDTO.sendingTime).isEmpty()) {
						mapOfEmail_logDTOTosendingTime.remove(oldEmail_logDTO.sendingTime);
					}
					
					if(mapOfEmail_logDTOTolastModificationTime.containsKey(oldEmail_logDTO.lastModificationTime)) {
						mapOfEmail_logDTOTolastModificationTime.get(oldEmail_logDTO.lastModificationTime).remove(oldEmail_logDTO);
					}
					if(mapOfEmail_logDTOTolastModificationTime.get(oldEmail_logDTO.lastModificationTime).isEmpty()) {
						mapOfEmail_logDTOTolastModificationTime.remove(oldEmail_logDTO.lastModificationTime);
					}
					
					
				}
				if(email_logDTO.isDeleted == 0) 
				{
					
					mapOfEmail_logDTOToiD.put(email_logDTO.iD, email_logDTO);
				
					if( ! mapOfEmail_logDTOTofromAddr.containsKey(email_logDTO.fromAddr)) {
						mapOfEmail_logDTOTofromAddr.put(email_logDTO.fromAddr, new HashSet<>());
					}
					mapOfEmail_logDTOTofromAddr.get(email_logDTO.fromAddr).add(email_logDTO);
					
					if( ! mapOfEmail_logDTOTotoAddr.containsKey(email_logDTO.toAddr)) {
						mapOfEmail_logDTOTotoAddr.put(email_logDTO.toAddr, new HashSet<>());
					}
					mapOfEmail_logDTOTotoAddr.get(email_logDTO.toAddr).add(email_logDTO);
					
					if( ! mapOfEmail_logDTOTocc.containsKey(email_logDTO.cc)) {
						mapOfEmail_logDTOTocc.put(email_logDTO.cc, new HashSet<>());
					}
					mapOfEmail_logDTOTocc.get(email_logDTO.cc).add(email_logDTO);
					
					if( ! mapOfEmail_logDTOTosubject.containsKey(email_logDTO.subject)) {
						mapOfEmail_logDTOTosubject.put(email_logDTO.subject, new HashSet<>());
					}
					mapOfEmail_logDTOTosubject.get(email_logDTO.subject).add(email_logDTO);
					
					if( ! mapOfEmail_logDTOToemailBody.containsKey(email_logDTO.emailBody)) {
						mapOfEmail_logDTOToemailBody.put(email_logDTO.emailBody, new HashSet<>());
					}
					mapOfEmail_logDTOToemailBody.get(email_logDTO.emailBody).add(email_logDTO);
					
					if( ! mapOfEmail_logDTOTofilesDropzone.containsKey(email_logDTO.filesDropzone)) {
						mapOfEmail_logDTOTofilesDropzone.put(email_logDTO.filesDropzone, new HashSet<>());
					}
					mapOfEmail_logDTOTofilesDropzone.get(email_logDTO.filesDropzone).add(email_logDTO);
					
					if( ! mapOfEmail_logDTOToisDraft.containsKey(email_logDTO.isDraft)) {
						mapOfEmail_logDTOToisDraft.put(email_logDTO.isDraft, new HashSet<>());
					}
					mapOfEmail_logDTOToisDraft.get(email_logDTO.isDraft).add(email_logDTO);
					
					if( ! mapOfEmail_logDTOTosendingTime.containsKey(email_logDTO.sendingTime)) {
						mapOfEmail_logDTOTosendingTime.put(email_logDTO.sendingTime, new HashSet<>());
					}
					mapOfEmail_logDTOTosendingTime.get(email_logDTO.sendingTime).add(email_logDTO);
					
					if( ! mapOfEmail_logDTOTolastModificationTime.containsKey(email_logDTO.lastModificationTime)) {
						mapOfEmail_logDTOTolastModificationTime.put(email_logDTO.lastModificationTime, new HashSet<>());
					}
					mapOfEmail_logDTOTolastModificationTime.get(email_logDTO.lastModificationTime).add(email_logDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Email_logDTO> getEmail_logList() {
		List <Email_logDTO> email_logs = new ArrayList<Email_logDTO>(this.mapOfEmail_logDTOToiD.values());
		return email_logs;
	}
	
	
	public Email_logDTO getEmail_logDTOByID( long ID){
		return mapOfEmail_logDTOToiD.get(ID);
	}
	
	
	public List<Email_logDTO> getEmail_logDTOByfrom_addr(String from_addr) {
		return new ArrayList<>( mapOfEmail_logDTOTofromAddr.getOrDefault(from_addr,new HashSet<>()));
	}
	
	
	public List<Email_logDTO> getEmail_logDTOByto_addr(String to_addr) {
		return new ArrayList<>( mapOfEmail_logDTOTotoAddr.getOrDefault(to_addr,new HashSet<>()));
	}
	
	
	public List<Email_logDTO> getEmail_logDTOBycc(String cc) {
		return new ArrayList<>( mapOfEmail_logDTOTocc.getOrDefault(cc,new HashSet<>()));
	}
	
	
	public List<Email_logDTO> getEmail_logDTOBysubject(String subject) {
		return new ArrayList<>( mapOfEmail_logDTOTosubject.getOrDefault(subject,new HashSet<>()));
	}
	
	
	public List<Email_logDTO> getEmail_logDTOByemail_body(String email_body) {
		return new ArrayList<>( mapOfEmail_logDTOToemailBody.getOrDefault(email_body,new HashSet<>()));
	}
	
	
	public List<Email_logDTO> getEmail_logDTOByfiles_dropzone(long files_dropzone) {
		return new ArrayList<>( mapOfEmail_logDTOTofilesDropzone.getOrDefault(files_dropzone,new HashSet<>()));
	}
	
	
	public List<Email_logDTO> getEmail_logDTOByis_draft(int is_draft) {
		return new ArrayList<>( mapOfEmail_logDTOToisDraft.getOrDefault(is_draft,new HashSet<>()));
	}
	
	
	public List<Email_logDTO> getEmail_logDTOBysending_time(long sending_time) {
		return new ArrayList<>( mapOfEmail_logDTOTosendingTime.getOrDefault(sending_time,new HashSet<>()));
	}
	
	
	public List<Email_logDTO> getEmail_logDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfEmail_logDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "email_log";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


