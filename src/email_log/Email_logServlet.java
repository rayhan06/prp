package email_log;

import java.io.IOException;
import java.io.*;


import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMultipart;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import address_book.AddressBookDetailDAO;
import address_book.AddressBookDetailDTO;
import edms_documents.Edms_documentsDAO;
import edms_documents.Edms_documentsDTO;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import mail.EmailAttachment;
import mail.EmailService;
import mail.SendEmailDTO;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDAO;
import user.UserDTO;
import user.UserNameEmailDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;

import email_log.Constants;


import geolocation.GeoLocationDAO2;

import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import files.*;


/**
 * Servlet implementation class Email_logServlet
 */
@WebServlet("/Email_logServlet")
@MultipartConfig
public class Email_logServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Email_logServlet.class);

    String tableName = "email_log";

    Email_logDAO email_logDAO;
    EmailService emailService;
    CommonRequestHandler commonRequestHandler;
    UserDAO userDAO;
    Employee_recordsDAO employee_recordsDAO;
    FilesDAO filesDAO = new FilesDAO();
    private final Gson gson = new Gson();
    private AddressBookDetailDAO addressBookDAO;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Email_logServlet() {
        super();
        try {
            email_logDAO = new Email_logDAO(tableName);
            commonRequestHandler = new CommonRequestHandler(email_logDAO);
            emailService = new EmailService();
            userDAO = new UserDAO();
            employee_recordsDAO = new Employee_recordsDAO();
            addressBookDAO = new AddressBookDetailDAO();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getAddPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMAIL_LOG_ADD)) {
                    String[] docIds = request.getParameterValues( "docId" );
                    if(docIds != null) {
                        List<Long> docIdList = new ArrayList<>();
                        for (String docId : docIds) {
                            docIdList.add(Long.parseLong(docId));
                        }
                        List<Edms_documentsDTO> edms_documentsDTOList = new Edms_documentsDAO().getDTOs(docIdList);
                        request.setAttribute("EDMS_DOCUMENTS_LIST", edms_documentsDTOList);
                    }
                    List<UserDTO> userDTOS = UserRepository.getUserList();
                    List<UserNameEmailDTO> emailDTOS = new ArrayList<>();
                    for (UserDTO dto : userDTOS){
                        emailDTOS.add(UserRepository.convertUserDTOIntoNameEmailDTO(dto));
                    }
                    request.setAttribute("userList", emailDTOS);
                    commonRequestHandler.getAddPage(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getEditPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMAIL_LOG_UPDATE)) {
                    getEmail_log(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("downloadAttachment")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMAIL_LOG_UPDATE)) {
                    downloadAttachment(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getEmailViewPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMAIL_LOG_UPDATE)) {
                    getEmailViewById(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("downloadDropzoneFile")) {
                long id = Long.parseLong(request.getParameter("id"));

                FilesDTO filesDTO = (FilesDTO) filesDAO.getDTOByID(id);

                Utils.ProcessFile(request, response, filesDTO.fileTitle, filesDTO.fileBlob);

            } else if (actionType.equals("DeleteFileFromDropZone")) {
                long id = Long.parseLong(request.getParameter("id"));


                System.out.println("In delete file");
                filesDAO.hardDeleteByID(id);
                response.getWriter().write("Deleted");

            } else if (actionType.equals("getURL")) {
                String URL = request.getParameter("URL");
                System.out.println("URL = " + URL);
                response.sendRedirect(URL);
            } else if (actionType.equals("setPass")) {
                String password = request.getParameter("password");
                String redAction = request.getParameter("redirectAction");
                request.getSession().setAttribute("userPass", password);
                String redirectUrl = "Email_logServlet?actionType=" + redAction;
                response.sendRedirect(redirectUrl);
            } else if (actionType.equals("search")) {
                System.out.println("search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMAIL_LOG_SEARCH)) {

                    if (isPermanentTable) {
                        searchEmail_log(request, response, isPermanentTable);
                    } else {
                        //searchEmail_log(request, response, tempTableName, isPermanentTable);
                    }
                }
            } else if (actionType.equals("inbox")) {
                getInbox(request, response);
            } else if (actionType.equals("view")) {
                System.out.println("view requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMAIL_LOG_SEARCH)) {
                    commonRequestHandler.view(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else {
                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void downloadAttachment(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long id = Long.parseLong(request.getParameter("emailId"));
        int attachmentId = Integer.parseInt(request.getParameter("attachmentId"));
        /*String protocol = "imap";
        String host = "mail.pbrlp.gov.bd";
        String port = "993";


        String userName = "edms@pbrlp.gov.bd";
        String password = "ZYy9nGozC87#";*/
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = userDAO.getUserDTOByUserID(loginDTO.userID);
        Employee_recordsDTO employee_recordsDTO = employee_recordsDAO.getEmployee_recordsDTOByID(userDTO.employee_record_id);
        Message message = emailService.getMessageByUID(id, employee_recordsDTO.personalEml, "");
        EmailInboxDTO inboxDTO = convertMessageToInboxDTO(message, id);
        EmailAttachment emailAttachment = inboxDTO.getAttachments().get(attachmentId);
        /*int size = new DataInputStream(emailAttachment.getInputStream()).read(new byte[1024]);
        Utils.ProcessFile(request, response, emailAttachment.getName(), emailAttachment.getInputStream());*/

        response.setContentType(emailAttachment.getContentType());
        response.setContentLength(emailAttachment.getData().length);
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", emailAttachment.getName() );
        response.setHeader(headerKey, headerValue);

        try {
            // writes the file to the client
            OutputStream outStream = response.getOutputStream();
            outStream.write(emailAttachment.getData());
            outStream.close();
        }
        catch ( Exception e ){
             e.printStackTrace();
        }
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        System.out.println("doPost");
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("add")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMAIL_LOG_ADD)) {
                    System.out.println("going to  addEmail_log ");
                    addEmail_log(request, response, true, userDTO, true);
                } else {
                    System.out.println("Not going to  addEmail_log ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("UploadFilesFromDropZone")) {
                String Column = request.getParameter("columnName");
                long ColumnID = Long.parseLong(request.getParameter("ColumnID"));
                String pageType = request.getParameter("pageType");

                System.out.println("In " + pageType);
                Utils.UploadFilesFromDropZone(request, response, userDTO.ID, ColumnID, "Email_logServlet");
            } else if (actionType.equals("getDTO")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMAIL_LOG_ADD)) {
                    getDTO(request, response);
                } else {
                    System.out.println("Not going to  addEmail_log ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("edit")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMAIL_LOG_UPDATE)) {
                    addEmail_log(request, response, false, userDTO, isPermanentTable);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("delete")) {
                deleteEmail_log(request, response, userDTO);
            } else if (actionType.equals("search")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMAIL_LOG_SEARCH)) {
                    searchEmail_log(request, response, true);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getGeo")) {
                System.out.println("going to geoloc ");
                request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getDTO(HttpServletRequest request, HttpServletResponse response) {
        try {
            System.out.println("In getDTO");
            Email_logDTO email_logDTO = (Email_logDTO) email_logDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            String encoded = this.gson.toJson(email_logDTO);
            System.out.println("json encoded data = " + encoded);
            out.print(encoded);
            out.flush();
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void addEmail_log(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addEmail_log");
            String path = getServletContext().getRealPath("/img2/");
            Email_logDTO email_logDTO;

            if (addFlag == true) {
                email_logDTO = new Email_logDTO();
            } else {
                email_logDTO = (Email_logDTO) email_logDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
            }

            String Value = "";

            email_logDTO.fromAddr = userDTO.userName;

            Value = request.getParameter("toAddr");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("toAddr = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                email_logDTO.toAddr = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            if( email_logDTO.toAddr == null || email_logDTO.toAddr.trim().length() == 0 )
                throw new Exception( "No reciever email address given" );

            Value = request.getParameter("address_book_select_to");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("address_book_select_to = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                Long addressBookId = Long.parseLong(Value);
                email_logDTO.toAddrBook = addressBookId;
                List<AddressBookDetailDTO> addressBookDetailDTOS = addressBookDAO.getAddressBookDetailDTOListByAddressBookID(addressBookId);
                String addresses = "";
                for (AddressBookDetailDTO detailDTO : addressBookDetailDTOS){
                    addresses += detailDTO.employeeEmail + "; ";
                }
                if (email_logDTO.toAddr == null){

                    email_logDTO.toAddr = addresses;
                } else {

                    email_logDTO.toAddr += addresses;
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            if( email_logDTO.toAddr == null || email_logDTO.toAddr.trim().length() == 0 )
                throw new Exception( "No reciever email address given" );

            Value = request.getParameter("cc");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("cc = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                email_logDTO.cc = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("address_book_select_cc");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("address_book_select_cc = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                Long addressBookId = Long.parseLong(Value);
                email_logDTO.ccAddrBook = addressBookId;
                List<AddressBookDetailDTO> addressBookDetailDTOS = addressBookDAO.getAddressBookDetailDTOListByAddressBookID(addressBookId);
                String addresses = "";
                for (AddressBookDetailDTO detailDTO : addressBookDetailDTOS){
                    addresses += detailDTO.employeeEmail + "; ";
                }
                if (email_logDTO.cc == null){
                    email_logDTO.cc = addresses;
                } else {
                    email_logDTO.cc += addresses;
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("bcc");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("bcc = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                email_logDTO.bcc = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("subject");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("subject = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                email_logDTO.subject = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("emailBody");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("emailBody = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                email_logDTO.emailBody = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("filesDropzone");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("filesDropzone = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {


                System.out.println("filesDropzone = " + Value);
                if (Value != null && !Value.equalsIgnoreCase("")) {
                    email_logDTO.filesDropzone = Long.parseLong(Value);
                } else {
                    System.out.println("FieldName has a null Value, not updating" + " = " + Value);
                }

                if (addFlag == false) {
                    String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                    String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                    for (int i = 0; i < deleteArray.length; i++) {
                        System.out.println("going to delete " + deleteArray[i]);
                        if (i > 0) {
                            filesDAO.delete(Long.parseLong(deleteArray[i]));
                        }
                    }
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("isDraft");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("isDraft = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                email_logDTO.isDraft = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("sendingTime");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("sendingTime = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                email_logDTO.sendingTime = Calendar.getInstance().getTimeInMillis();
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            FilesDAO filesDAO = new FilesDAO();
            System.out.println("Done adding  addEmail_log dto = " + email_logDTO);
            long returnedID = -1;
            List<FilesDTO> filesDTOS = filesDAO.getDTOsByFileID( email_logDTO.filesDropzone );

            String[] edmsDocIds = request.getParameterValues( "edmsDocId" );
            List<Long> edmsDocIdList = new ArrayList<>();

            if( edmsDocIds != null && edmsDocIds.length > 0 ){

                for( String edmsDocId: edmsDocIds ){

                    edmsDocIdList.add( Long.parseLong( edmsDocId ) );
                }

                List<Edms_documentsDTO> edms_documentsDTOList = new Edms_documentsDAO().getDTOs( edmsDocIdList );
                for( Edms_documentsDTO edms_documentsDTO: edms_documentsDTOList ){

                    List<FilesDTO> filesDTOList = filesDAO.getDTOsByFileID( edms_documentsDTO.filesDropzone );
                    filesDTOS.addAll( filesDTOList );
                }
            }

            List<EmailAttachment> attachments = new ArrayList<>();
            if( filesDTOS != null ) {
                for (FilesDTO dto : filesDTOS) {
                    EmailAttachment emailAttachment = new EmailAttachment();
                    emailAttachment.setName(dto.fileTitle);
                    emailAttachment.setContentType(dto.fileTypes);
                    emailAttachment.setInputStream(dto.inputStream);
                    attachments.add(emailAttachment);
                }
            }

            if (email_logDTO.isDraft != 1){
                SendEmailDTO sendEmailDTO = new SendEmailDTO();
                sendEmailDTO.setFrom(email_logDTO.fromAddr);
                sendEmailDTO.setSubject(email_logDTO.subject);
                sendEmailDTO.setText(email_logDTO.emailBody);
                sendEmailDTO.setTo(email_logDTO.toAddr.trim().split(";"));
                sendEmailDTO.setBcc(email_logDTO.bcc != null ? email_logDTO.bcc.trim().split(";") : null);
                sendEmailDTO.setCc(email_logDTO.cc != null ? email_logDTO.cc.trim().split(";") : null);
                sendEmailDTO.setAttachments(attachments);
                emailService.sendMail(sendEmailDTO);
            }


            if (isPermanentTable == false) //add new row for validation and make the old row outdated
            {
                email_logDAO.setIsDeleted(email_logDTO.iD, CommonDTO.OUTDATED);
                returnedID = email_logDAO.add(email_logDTO);
                email_logDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
            } else if (addFlag == true) {
                returnedID = email_logDAO.manageWriteOperations(email_logDTO, SessionConstants.INSERT, -1, userDTO);
            } else {
                returnedID = email_logDAO.manageWriteOperations(email_logDTO, SessionConstants.UPDATE, -1, userDTO);
            }


            if (isPermanentTable) {
                String inPlaceSubmit = request.getParameter("inplacesubmit");

                if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                    getEmail_log(request, response, returnedID);
                } else {
                    response.sendRedirect("Email_logServlet?actionType=search");
                }
            } else {
                commonRequestHandler.validate(email_logDAO.getDTOByID(returnedID), request, response, userDTO);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void deleteEmail_log(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException {
        try {
            String[] IDsToDelete = request.getParameterValues("ID");
            for (int i = 0; i < IDsToDelete.length; i++) {
                long id = Long.parseLong(IDsToDelete[i]);
                System.out.println("------ DELETING " + IDsToDelete[i]);


                Email_logDTO email_logDTO = (Email_logDTO) email_logDAO.getDTOByID(id);
                email_logDAO.manageWriteOperations(email_logDTO, SessionConstants.DELETE, id, userDTO);
                response.sendRedirect("Email_logServlet?actionType=search");

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }




    private void getEmailViewById(HttpServletRequest request, HttpServletResponse response) throws Exception {
        /*String protocol = "imap";
        String host = "mail.pbrlp.gov.bd";
        String port = "993";


        String userName = "edms@pbrlp.gov.bd";
        String password = "ZYy9nGozC87#";*/

        long id = Long.parseLong(request.getParameter("ID"));
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = userDAO.getUserDTOByUserID(loginDTO.userID);
        Employee_recordsDTO employee_recordsDTO = employee_recordsDAO.getEmployee_recordsDTOByID(userDTO.employee_record_id);
        Message message = emailService.getMessageByUID(id, employee_recordsDTO.personalEml, "");
        EmailInboxDTO inboxDTO = convertMessageToInboxDTO(message, id);
        request.setAttribute("ID", inboxDTO.iD);
        request.setAttribute("inboxDTO", inboxDTO);
        String URL = "email_log/emailview.jsp";
        RequestDispatcher rd = request.getRequestDispatcher(URL);
        rd.forward(request, response);

    }

    private EmailInboxDTO convertMessageToInboxDTO(Message message, Long uid) {
        EmailInboxDTO inboxDTO = new EmailInboxDTO();
        try {
            Address[] addresses = message.getAllRecipients();
            inboxDTO.setFrom(addresses[0].toString());
            inboxDTO.subject = message.getSubject();
            inboxDTO.setTo(emailService.parseAddresses(message.getRecipients(Message.RecipientType.TO)));
            inboxDTO.setCc(emailService.parseAddresses(message.getRecipients(Message.RecipientType.CC)));
            inboxDTO.setSentDate(message.getSentDate() == null ? 0 : message.getSentDate().getTime());
            inboxDTO.contentType = message.getContentType();
            inboxDTO.setText(emailService.getBodyText((MimeMultipart) message.getContent()));
            inboxDTO.setUid(uid);
            inboxDTO.setAttachments(emailService.getAttachments((MimeMultipart) message.getContent()));
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return inboxDTO;
    }


    private void getEmail_log(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {
        System.out.println("in getEmail_log");
        Email_logDTO email_logDTO = null;
        try {
            email_logDTO = (Email_logDTO) email_logDAO.getDTOByID(id);
            request.setAttribute("ID", email_logDTO.iD);
            request.setAttribute("email_logDTO", email_logDTO);
            request.setAttribute("email_logDAO", email_logDAO);
            List<UserDTO> userDTOS = UserRepository.getUserList();
            List<UserNameEmailDTO> emailDTOS = new ArrayList<>();
            for (UserDTO dto : userDTOS){
                emailDTOS.add(UserRepository.convertUserDTOIntoNameEmailDTO(dto));
            }
            request.setAttribute("userList", emailDTOS);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "email_log/email_logInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "email_log/email_logSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "email_log/email_logEditBody.jsp?actionType=edit";
                } else {
                    URL = "email_log/email_logEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getEmail_log(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getEmail_log(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void getInbox(HttpServletRequest request, HttpServletResponse response) throws Exception {
        /*String protocol = "imap";
        String host = "mail.pbrlp.gov.bd";
        String port = "993";


        String userName = "edms@pbrlp.gov.bd";
        String password = "ZYy9nGozC87#";*/

        EmailService receiver = emailService;
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = userDAO.getUserDTOByUserID(loginDTO.userID);
        Employee_recordsDTO employee_recordsDTO = employee_recordsDAO.getEmployee_recordsDTOByID(userDTO.employee_record_id);
        List<EmailInboxDTO> emailInboxDTOS = receiver.downloadEmails( employee_recordsDTO.personalEml, (String) request.getSession().getAttribute("userPass"));
        request.setAttribute("inboxDTOS", emailInboxDTOS);
        RequestDispatcher rd = request.getRequestDispatcher("email_log/emailInboxSearch.jsp");
        rd.forward(request, response);
    }

    private void searchEmail_log(HttpServletRequest request, HttpServletResponse response, boolean isPermanent) throws ServletException, IOException {
        System.out.println("in  searchEmail_log 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_EMAIL_LOG,
                request,
                email_logDAO,
                SessionConstants.VIEW_EMAIL_LOG,
                SessionConstants.SEARCH_EMAIL_LOG,
                tableName,
                isPermanent,
                userDTO,
                "",
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("email_logDAO", email_logDAO);
        RequestDispatcher rd;
        if (!isPermanent) {
            if (hasAjax == false) {
                System.out.println("Going to email_log/email_logApproval.jsp");
                rd = request.getRequestDispatcher("email_log/email_logApproval.jsp");
            } else {
                System.out.println("Going to email_log/email_logApprovalForm.jsp");
                rd = request.getRequestDispatcher("email_log/email_logApprovalForm.jsp");
            }
        } else {
            if (hasAjax == false) {
                System.out.println("Going to email_log/email_logSearch.jsp");
                rd = request.getRequestDispatcher("email_log/email_logSearch.jsp");
            } else {
                System.out.println("Going to email_log/email_logSearchForm.jsp");
                rd = request.getRequestDispatcher("email_log/email_logSearchForm.jsp");
            }
        }
        rd.forward(request, response);
    }

}

