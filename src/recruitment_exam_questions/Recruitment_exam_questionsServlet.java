package recruitment_exam_questions;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import recruitment_seat_plan.Recruitment_seat_planDAO;
import recruitment_seat_plan.Recruitment_seat_planDTO;
import role.PermissionRepository;

import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;

import javax.servlet.http.*;
import java.util.*;


import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;
import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import files.*;


/**
 * Servlet implementation class Recruitment_exam_questionsServlet
 */
@WebServlet("/Recruitment_exam_questionsServlet")
@MultipartConfig
public class Recruitment_exam_questionsServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Recruitment_exam_questionsServlet.class);

    @Override
    public String getTableName() {
        return Recruitment_exam_questionsDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Recruitment_exam_questionsServlet";
    }

    @Override
    public Recruitment_exam_questionsDAO getCommonDAOService() {
        return Recruitment_exam_questionsDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.RECRUITMENT_EXAM_QUESTIONS_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.RECRUITMENT_EXAM_QUESTIONS_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.RECRUITMENT_EXAM_QUESTIONS_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Recruitment_exam_questionsServlet.class;
    }

    FilesDAO filesDAO = new FilesDAO();
    private final Gson gson = new Gson();


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {

        request.setAttribute("failureMessage", "");
        System.out.println("%%%% addRecruitment_exam_questions");
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        Recruitment_exam_questionsDTO recruitment_exam_questionsDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLanEng = Language.equalsIgnoreCase("English");

        if (addFlag) {
            recruitment_exam_questionsDTO = new Recruitment_exam_questionsDTO();
            recruitment_exam_questionsDTO.insertionDate = recruitment_exam_questionsDTO.lastModificationTime = System.currentTimeMillis();
            recruitment_exam_questionsDTO.insertedBy = recruitment_exam_questionsDTO.modifiedBy = String.valueOf(userDTO.ID);
        } else {
            recruitment_exam_questionsDTO = Recruitment_exam_questionsDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
            if (recruitment_exam_questionsDTO == null) {
                throw new Exception(isLanEng ? "Recruitment exam question information is not found" : "প্রশ্নপত্রের তথ্য খুঁজে পাওয়া যায়নি");
            }
            recruitment_exam_questionsDTO.lastModificationTime = System.currentTimeMillis();
            recruitment_exam_questionsDTO.modifiedBy = String.valueOf(userDTO.ID);
        }

        String Value = "";

        Value = request.getParameter("recruitmentTestNameId");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {
            recruitment_exam_questionsDTO.recruitmentTestNameId = Long.parseLong(Value);
        } else {
            throw new Exception(isLanEng ? "Please provide test name" : "অনুগ্রহপূর্বক নিয়োগ পরীক্ষার নাম বাছাই করুন");
        }

        Value = request.getParameter("recruitmentJobDescriptionId");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {
            recruitment_exam_questionsDTO.recruitmentJobDescriptionId = Long.parseLong(Value);
        } else {
            throw new Exception(isLanEng ? "Please provide job title" : "অনুগ্রহপূর্বক পদের নাম বাছাই করুন");
        }

        Value = request.getParameter("recruitmentJobSpecificExamTypeId");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {
            recruitment_exam_questionsDTO.recruitmentJobSpecificExamTypeId = Long.parseLong(Value);
        } else {
            throw new Exception(isLanEng ? "Please provide exam level" : "অনুগ্রহপূর্বক পরীক্ষার স্তর বাছাই করুন");
        }

        Value = request.getParameter("filesDropzone");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        System.out.println("filesDropzone = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {

            System.out.println("filesDropzone = " + Value);

            recruitment_exam_questionsDTO.filesDropzone = Long.parseLong(Value);


            if (addFlag == false) {
                String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    System.out.println("going to delete " + deleteArray[i]);
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        } else {
            throw new Exception(isLanEng ? "Please provide question paper" : "অনুগ্রহপূর্বক প্রশ্নপত্র আপলোড করুন");
        }


        logger.debug("Done adding  addRecruitment_exam_questions dto = " + recruitment_exam_questionsDTO);

        if (addFlag == true) {
            Recruitment_exam_questionsDAO.getInstance().add(recruitment_exam_questionsDTO);
        } else {
            Recruitment_exam_questionsDAO.getInstance().update(recruitment_exam_questionsDTO);
        }

        return recruitment_exam_questionsDTO;
    }
}

