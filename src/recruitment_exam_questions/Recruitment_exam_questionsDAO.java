package recruitment_exam_questions;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Recruitment_exam_questionsDAO  implements CommonDAOService<Recruitment_exam_questionsDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Recruitment_exam_questionsDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"recruitment_test_name_id",
			"recruitment_job_description_id",
			"recruitment_job_specific_exam_type_id",
			"files_dropzone",
			"search_column",
			"inserted_by",
			"modified_by",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Recruitment_exam_questionsDAO INSTANCE = new Recruitment_exam_questionsDAO();
	}

	public static Recruitment_exam_questionsDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Recruitment_exam_questionsDTO recruitment_exam_questionsDTO)
	{
		recruitment_exam_questionsDTO.searchColumn = "";
	}
	
	@Override
	public void set(PreparedStatement ps, Recruitment_exam_questionsDTO recruitment_exam_questionsDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(recruitment_exam_questionsDTO);
		if(isInsert)
		{
			ps.setObject(++index,recruitment_exam_questionsDTO.iD);
		}
		ps.setObject(++index,recruitment_exam_questionsDTO.recruitmentTestNameId);
		ps.setObject(++index,recruitment_exam_questionsDTO.recruitmentJobDescriptionId);
		ps.setObject(++index,recruitment_exam_questionsDTO.recruitmentJobSpecificExamTypeId);
		ps.setObject(++index,recruitment_exam_questionsDTO.filesDropzone);
		ps.setObject(++index,recruitment_exam_questionsDTO.searchColumn);
		ps.setObject(++index,recruitment_exam_questionsDTO.insertedBy);
		ps.setObject(++index,recruitment_exam_questionsDTO.modifiedBy);
		ps.setObject(++index,recruitment_exam_questionsDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(++index,recruitment_exam_questionsDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,recruitment_exam_questionsDTO.iD);
		}
	}
	
	@Override
	public Recruitment_exam_questionsDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Recruitment_exam_questionsDTO recruitment_exam_questionsDTO = new Recruitment_exam_questionsDTO();
			int i = 0;
			recruitment_exam_questionsDTO.iD = rs.getLong(columnNames[i++]);
			recruitment_exam_questionsDTO.recruitmentTestNameId = rs.getLong(columnNames[i++]);
			recruitment_exam_questionsDTO.recruitmentJobDescriptionId = rs.getLong(columnNames[i++]);
			recruitment_exam_questionsDTO.recruitmentJobSpecificExamTypeId = rs.getLong(columnNames[i++]);
			recruitment_exam_questionsDTO.filesDropzone = rs.getLong(columnNames[i++]);
			recruitment_exam_questionsDTO.searchColumn = rs.getString(columnNames[i++]);
			recruitment_exam_questionsDTO.insertedBy = rs.getString(columnNames[i++]);
			recruitment_exam_questionsDTO.modifiedBy = rs.getString(columnNames[i++]);
			recruitment_exam_questionsDTO.insertionDate = rs.getLong(columnNames[i++]);
			recruitment_exam_questionsDTO.isDeleted = rs.getInt(columnNames[i++]);
			recruitment_exam_questionsDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return recruitment_exam_questionsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Recruitment_exam_questionsDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "recruitment_exam_questions";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Recruitment_exam_questionsDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Recruitment_exam_questionsDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }
				
}
	