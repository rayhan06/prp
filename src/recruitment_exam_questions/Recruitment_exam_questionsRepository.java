package recruitment_exam_questions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Recruitment_exam_questionsRepository implements Repository {
	Recruitment_exam_questionsDAO recruitment_exam_questionsDAO = null;
	
	static Logger logger = Logger.getLogger(Recruitment_exam_questionsRepository.class);
	Map<Long, Recruitment_exam_questionsDTO>mapOfRecruitment_exam_questionsDTOToiD;
	Map<Long, Set<Recruitment_exam_questionsDTO> >mapOfRecruitment_exam_questionsDTOTorecruitmentTestNameId;
	Map<Long, Set<Recruitment_exam_questionsDTO> >mapOfRecruitment_exam_questionsDTOTorecruitmentJobDescriptionId;
	Map<Long, Set<Recruitment_exam_questionsDTO> >mapOfRecruitment_exam_questionsDTOTorecruitmentJobSpecificExamTypeId;
	Gson gson;

  
	private Recruitment_exam_questionsRepository(){
		recruitment_exam_questionsDAO = Recruitment_exam_questionsDAO.getInstance();
		mapOfRecruitment_exam_questionsDTOToiD = new ConcurrentHashMap<>();
		mapOfRecruitment_exam_questionsDTOTorecruitmentTestNameId = new ConcurrentHashMap<>();
		mapOfRecruitment_exam_questionsDTOTorecruitmentJobDescriptionId = new ConcurrentHashMap<>();
		mapOfRecruitment_exam_questionsDTOTorecruitmentJobSpecificExamTypeId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Recruitment_exam_questionsRepository INSTANCE = new Recruitment_exam_questionsRepository();
    }

    public static Recruitment_exam_questionsRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Recruitment_exam_questionsDTO> recruitment_exam_questionsDTOs = recruitment_exam_questionsDAO.getAllDTOs(reloadAll);
			for(Recruitment_exam_questionsDTO recruitment_exam_questionsDTO : recruitment_exam_questionsDTOs) {
				Recruitment_exam_questionsDTO oldRecruitment_exam_questionsDTO = getRecruitment_exam_questionsDTOByiD(recruitment_exam_questionsDTO.iD);
				if( oldRecruitment_exam_questionsDTO != null ) {
					mapOfRecruitment_exam_questionsDTOToiD.remove(oldRecruitment_exam_questionsDTO.iD);
				
					if(mapOfRecruitment_exam_questionsDTOTorecruitmentTestNameId.containsKey(oldRecruitment_exam_questionsDTO.recruitmentTestNameId)) {
						mapOfRecruitment_exam_questionsDTOTorecruitmentTestNameId.get(oldRecruitment_exam_questionsDTO.recruitmentTestNameId).remove(oldRecruitment_exam_questionsDTO);
					}
					if(mapOfRecruitment_exam_questionsDTOTorecruitmentTestNameId.get(oldRecruitment_exam_questionsDTO.recruitmentTestNameId).isEmpty()) {
						mapOfRecruitment_exam_questionsDTOTorecruitmentTestNameId.remove(oldRecruitment_exam_questionsDTO.recruitmentTestNameId);
					}
					
					if(mapOfRecruitment_exam_questionsDTOTorecruitmentJobDescriptionId.containsKey(oldRecruitment_exam_questionsDTO.recruitmentJobDescriptionId)) {
						mapOfRecruitment_exam_questionsDTOTorecruitmentJobDescriptionId.get(oldRecruitment_exam_questionsDTO.recruitmentJobDescriptionId).remove(oldRecruitment_exam_questionsDTO);
					}
					if(mapOfRecruitment_exam_questionsDTOTorecruitmentJobDescriptionId.get(oldRecruitment_exam_questionsDTO.recruitmentJobDescriptionId).isEmpty()) {
						mapOfRecruitment_exam_questionsDTOTorecruitmentJobDescriptionId.remove(oldRecruitment_exam_questionsDTO.recruitmentJobDescriptionId);
					}
					
					if(mapOfRecruitment_exam_questionsDTOTorecruitmentJobSpecificExamTypeId.containsKey(oldRecruitment_exam_questionsDTO.recruitmentJobSpecificExamTypeId)) {
						mapOfRecruitment_exam_questionsDTOTorecruitmentJobSpecificExamTypeId.get(oldRecruitment_exam_questionsDTO.recruitmentJobSpecificExamTypeId).remove(oldRecruitment_exam_questionsDTO);
					}
					if(mapOfRecruitment_exam_questionsDTOTorecruitmentJobSpecificExamTypeId.get(oldRecruitment_exam_questionsDTO.recruitmentJobSpecificExamTypeId).isEmpty()) {
						mapOfRecruitment_exam_questionsDTOTorecruitmentJobSpecificExamTypeId.remove(oldRecruitment_exam_questionsDTO.recruitmentJobSpecificExamTypeId);
					}
					
					
				}
				if(recruitment_exam_questionsDTO.isDeleted == 0) 
				{
					
					mapOfRecruitment_exam_questionsDTOToiD.put(recruitment_exam_questionsDTO.iD, recruitment_exam_questionsDTO);
				
					if( ! mapOfRecruitment_exam_questionsDTOTorecruitmentTestNameId.containsKey(recruitment_exam_questionsDTO.recruitmentTestNameId)) {
						mapOfRecruitment_exam_questionsDTOTorecruitmentTestNameId.put(recruitment_exam_questionsDTO.recruitmentTestNameId, new HashSet<>());
					}
					mapOfRecruitment_exam_questionsDTOTorecruitmentTestNameId.get(recruitment_exam_questionsDTO.recruitmentTestNameId).add(recruitment_exam_questionsDTO);
					
					if( ! mapOfRecruitment_exam_questionsDTOTorecruitmentJobDescriptionId.containsKey(recruitment_exam_questionsDTO.recruitmentJobDescriptionId)) {
						mapOfRecruitment_exam_questionsDTOTorecruitmentJobDescriptionId.put(recruitment_exam_questionsDTO.recruitmentJobDescriptionId, new HashSet<>());
					}
					mapOfRecruitment_exam_questionsDTOTorecruitmentJobDescriptionId.get(recruitment_exam_questionsDTO.recruitmentJobDescriptionId).add(recruitment_exam_questionsDTO);
					
					if( ! mapOfRecruitment_exam_questionsDTOTorecruitmentJobSpecificExamTypeId.containsKey(recruitment_exam_questionsDTO.recruitmentJobSpecificExamTypeId)) {
						mapOfRecruitment_exam_questionsDTOTorecruitmentJobSpecificExamTypeId.put(recruitment_exam_questionsDTO.recruitmentJobSpecificExamTypeId, new HashSet<>());
					}
					mapOfRecruitment_exam_questionsDTOTorecruitmentJobSpecificExamTypeId.get(recruitment_exam_questionsDTO.recruitmentJobSpecificExamTypeId).add(recruitment_exam_questionsDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Recruitment_exam_questionsDTO clone(Recruitment_exam_questionsDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Recruitment_exam_questionsDTO.class);
	}
	
	
	public List<Recruitment_exam_questionsDTO> getRecruitment_exam_questionsList() {
		List <Recruitment_exam_questionsDTO> recruitment_exam_questionss = new ArrayList<Recruitment_exam_questionsDTO>(this.mapOfRecruitment_exam_questionsDTOToiD.values());
		return recruitment_exam_questionss;
	}
	
	public List<Recruitment_exam_questionsDTO> copyRecruitment_exam_questionsList() {
		List <Recruitment_exam_questionsDTO> recruitment_exam_questionss = getRecruitment_exam_questionsList();
		return recruitment_exam_questionss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Recruitment_exam_questionsDTO getRecruitment_exam_questionsDTOByiD( long iD){
		return mapOfRecruitment_exam_questionsDTOToiD.get(iD);
	}
	
	public Recruitment_exam_questionsDTO copyRecruitment_exam_questionsDTOByiD( long iD){
		return clone(mapOfRecruitment_exam_questionsDTOToiD.get(iD));
	}
	
	
	public List<Recruitment_exam_questionsDTO> getRecruitment_exam_questionsDTOByrecruitmentTestNameId(long recruitmentTestNameId) {
		return new ArrayList<>( mapOfRecruitment_exam_questionsDTOTorecruitmentTestNameId.getOrDefault(recruitmentTestNameId,new HashSet<>()));
	}
	
	public List<Recruitment_exam_questionsDTO> copyRecruitment_exam_questionsDTOByrecruitmentTestNameId(long recruitmentTestNameId)
	{
		List <Recruitment_exam_questionsDTO> recruitment_exam_questionss = getRecruitment_exam_questionsDTOByrecruitmentTestNameId(recruitmentTestNameId);
		return recruitment_exam_questionss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<Recruitment_exam_questionsDTO> getRecruitment_exam_questionsDTOByrecruitmentJobDescriptionId(long recruitmentJobDescriptionId) {
		return new ArrayList<>( mapOfRecruitment_exam_questionsDTOTorecruitmentJobDescriptionId.getOrDefault(recruitmentJobDescriptionId,new HashSet<>()));
	}
	
	public List<Recruitment_exam_questionsDTO> copyRecruitment_exam_questionsDTOByrecruitmentJobDescriptionId(long recruitmentJobDescriptionId)
	{
		List <Recruitment_exam_questionsDTO> recruitment_exam_questionss = getRecruitment_exam_questionsDTOByrecruitmentJobDescriptionId(recruitmentJobDescriptionId);
		return recruitment_exam_questionss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<Recruitment_exam_questionsDTO> getRecruitment_exam_questionsDTOByrecruitmentJobSpecificExamTypeId(long recruitmentJobSpecificExamTypeId) {
		return new ArrayList<>( mapOfRecruitment_exam_questionsDTOTorecruitmentJobSpecificExamTypeId.getOrDefault(recruitmentJobSpecificExamTypeId,new HashSet<>()));
	}
	
	public List<Recruitment_exam_questionsDTO> copyRecruitment_exam_questionsDTOByrecruitmentJobSpecificExamTypeId(long recruitmentJobSpecificExamTypeId)
	{
		List <Recruitment_exam_questionsDTO> recruitment_exam_questionss = getRecruitment_exam_questionsDTOByrecruitmentJobSpecificExamTypeId(recruitmentJobSpecificExamTypeId);
		return recruitment_exam_questionss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return recruitment_exam_questionsDAO.getTableName();
	}
}


