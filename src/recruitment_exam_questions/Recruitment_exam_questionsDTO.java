package recruitment_exam_questions;
import java.util.*; 
import util.*; 


public class Recruitment_exam_questionsDTO extends CommonDTO
{

	public long recruitmentTestNameId = -1;
	public long recruitmentJobDescriptionId = -1;
	public long recruitmentJobSpecificExamTypeId = -1;
	public long filesDropzone = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
	public long insertionDate = -1;
	
	
    @Override
	public String toString() {
            return "$Recruitment_exam_questionsDTO[" +
            " iD = " + iD +
            " recruitmentTestNameId = " + recruitmentTestNameId +
            " recruitmentJobDescriptionId = " + recruitmentJobDescriptionId +
            " recruitmentJobSpecificExamTypeId = " + recruitmentJobSpecificExamTypeId +
            " filesDropzone = " + filesDropzone +
            " searchColumn = " + searchColumn +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}