package parliament_item;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import gate_pass_type.Gate_pass_typeDAO;
import gate_pass_type.Gate_pass_typeDTO;
import org.apache.log4j.Logger;

import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;


public class Parliament_itemRepository implements Repository {
	Parliament_itemDAO parliament_itemDAO = new Parliament_itemDAO();
	
	public void setDAO(Parliament_itemDAO parliament_itemDAO)
	{
		this.parliament_itemDAO = parliament_itemDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Parliament_itemRepository.class);
	Map<Long, Parliament_itemDTO>mapOfParliament_itemDTOToid;
	Map<String, Set<Parliament_itemDTO> >mapOfParliament_itemDTOTonameEng;
	Map<String, Set<Parliament_itemDTO> >mapOfParliament_itemDTOTonameBng;
	Map<String, Set<Parliament_itemDTO> >mapOfParliament_itemDTOTodescription;
	Map<Long, Set<Parliament_itemDTO> >mapOfParliament_itemDTOToinsertionDate;
	Map<String, Set<Parliament_itemDTO> >mapOfParliament_itemDTOToinsertedBy;
	Map<String, Set<Parliament_itemDTO> >mapOfParliament_itemDTOTomodifiedBy;
	Map<Long, Set<Parliament_itemDTO> >mapOfParliament_itemDTOTolastModificationTime;


	static Parliament_itemRepository instance = null;  
	private Parliament_itemRepository(){
		mapOfParliament_itemDTOToid = new ConcurrentHashMap<>();
		mapOfParliament_itemDTOTonameEng = new ConcurrentHashMap<>();
		mapOfParliament_itemDTOTonameBng = new ConcurrentHashMap<>();
		mapOfParliament_itemDTOTodescription = new ConcurrentHashMap<>();
		mapOfParliament_itemDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfParliament_itemDTOToinsertedBy = new ConcurrentHashMap<>();
		mapOfParliament_itemDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfParliament_itemDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Parliament_itemRepository getInstance(){
		if (instance == null){
			instance = new Parliament_itemRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(parliament_itemDAO == null)
		{
			return;
		}
		try {
			List<Parliament_itemDTO> parliament_itemDTOs = parliament_itemDAO.getAllParliament_item(reloadAll);
			for(Parliament_itemDTO parliament_itemDTO : parliament_itemDTOs) {
				Parliament_itemDTO oldParliament_itemDTO = getParliament_itemDTOByid(parliament_itemDTO.iD);
				if( oldParliament_itemDTO != null ) {
					mapOfParliament_itemDTOToid.remove(oldParliament_itemDTO.iD);
				
					if(mapOfParliament_itemDTOTonameEng.containsKey(oldParliament_itemDTO.nameEng)) {
						mapOfParliament_itemDTOTonameEng.get(oldParliament_itemDTO.nameEng).remove(oldParliament_itemDTO);
					}
					if(mapOfParliament_itemDTOTonameEng.get(oldParliament_itemDTO.nameEng).isEmpty()) {
						mapOfParliament_itemDTOTonameEng.remove(oldParliament_itemDTO.nameEng);
					}
					
					if(mapOfParliament_itemDTOTonameBng.containsKey(oldParliament_itemDTO.nameBng)) {
						mapOfParliament_itemDTOTonameBng.get(oldParliament_itemDTO.nameBng).remove(oldParliament_itemDTO);
					}
					if(mapOfParliament_itemDTOTonameBng.get(oldParliament_itemDTO.nameBng).isEmpty()) {
						mapOfParliament_itemDTOTonameBng.remove(oldParliament_itemDTO.nameBng);
					}
					
					if(mapOfParliament_itemDTOTodescription.containsKey(oldParliament_itemDTO.description)) {
						mapOfParliament_itemDTOTodescription.get(oldParliament_itemDTO.description).remove(oldParliament_itemDTO);
					}
					if(mapOfParliament_itemDTOTodescription.get(oldParliament_itemDTO.description).isEmpty()) {
						mapOfParliament_itemDTOTodescription.remove(oldParliament_itemDTO.description);
					}
					
					if(mapOfParliament_itemDTOToinsertionDate.containsKey(oldParliament_itemDTO.insertionDate)) {
						mapOfParliament_itemDTOToinsertionDate.get(oldParliament_itemDTO.insertionDate).remove(oldParliament_itemDTO);
					}
					if(mapOfParliament_itemDTOToinsertionDate.get(oldParliament_itemDTO.insertionDate).isEmpty()) {
						mapOfParliament_itemDTOToinsertionDate.remove(oldParliament_itemDTO.insertionDate);
					}
					
					if(mapOfParliament_itemDTOToinsertedBy.containsKey(oldParliament_itemDTO.insertedBy)) {
						mapOfParliament_itemDTOToinsertedBy.get(oldParliament_itemDTO.insertedBy).remove(oldParliament_itemDTO);
					}
					if(mapOfParliament_itemDTOToinsertedBy.get(oldParliament_itemDTO.insertedBy).isEmpty()) {
						mapOfParliament_itemDTOToinsertedBy.remove(oldParliament_itemDTO.insertedBy);
					}
					
					if(mapOfParliament_itemDTOTomodifiedBy.containsKey(oldParliament_itemDTO.modifiedBy)) {
						mapOfParliament_itemDTOTomodifiedBy.get(oldParliament_itemDTO.modifiedBy).remove(oldParliament_itemDTO);
					}
					if(mapOfParliament_itemDTOTomodifiedBy.get(oldParliament_itemDTO.modifiedBy).isEmpty()) {
						mapOfParliament_itemDTOTomodifiedBy.remove(oldParliament_itemDTO.modifiedBy);
					}
					
					if(mapOfParliament_itemDTOTolastModificationTime.containsKey(oldParliament_itemDTO.lastModificationTime)) {
						mapOfParliament_itemDTOTolastModificationTime.get(oldParliament_itemDTO.lastModificationTime).remove(oldParliament_itemDTO);
					}
					if(mapOfParliament_itemDTOTolastModificationTime.get(oldParliament_itemDTO.lastModificationTime).isEmpty()) {
						mapOfParliament_itemDTOTolastModificationTime.remove(oldParliament_itemDTO.lastModificationTime);
					}
					
					
				}
				if(parliament_itemDTO.isDeleted == 0) 
				{
					
					mapOfParliament_itemDTOToid.put(parliament_itemDTO.iD, parliament_itemDTO);
				
					if( ! mapOfParliament_itemDTOTonameEng.containsKey(parliament_itemDTO.nameEng)) {
						mapOfParliament_itemDTOTonameEng.put(parliament_itemDTO.nameEng, new HashSet<>());
					}
					mapOfParliament_itemDTOTonameEng.get(parliament_itemDTO.nameEng).add(parliament_itemDTO);
					
					if( ! mapOfParliament_itemDTOTonameBng.containsKey(parliament_itemDTO.nameBng)) {
						mapOfParliament_itemDTOTonameBng.put(parliament_itemDTO.nameBng, new HashSet<>());
					}
					mapOfParliament_itemDTOTonameBng.get(parliament_itemDTO.nameBng).add(parliament_itemDTO);
					
					if( ! mapOfParliament_itemDTOTodescription.containsKey(parliament_itemDTO.description)) {
						mapOfParliament_itemDTOTodescription.put(parliament_itemDTO.description, new HashSet<>());
					}
					mapOfParliament_itemDTOTodescription.get(parliament_itemDTO.description).add(parliament_itemDTO);
					
					if( ! mapOfParliament_itemDTOToinsertionDate.containsKey(parliament_itemDTO.insertionDate)) {
						mapOfParliament_itemDTOToinsertionDate.put(parliament_itemDTO.insertionDate, new HashSet<>());
					}
					mapOfParliament_itemDTOToinsertionDate.get(parliament_itemDTO.insertionDate).add(parliament_itemDTO);
					
					if( ! mapOfParliament_itemDTOToinsertedBy.containsKey(parliament_itemDTO.insertedBy)) {
						mapOfParliament_itemDTOToinsertedBy.put(parliament_itemDTO.insertedBy, new HashSet<>());
					}
					mapOfParliament_itemDTOToinsertedBy.get(parliament_itemDTO.insertedBy).add(parliament_itemDTO);
					
					if( ! mapOfParliament_itemDTOTomodifiedBy.containsKey(parliament_itemDTO.modifiedBy)) {
						mapOfParliament_itemDTOTomodifiedBy.put(parliament_itemDTO.modifiedBy, new HashSet<>());
					}
					mapOfParliament_itemDTOTomodifiedBy.get(parliament_itemDTO.modifiedBy).add(parliament_itemDTO);
					
					if( ! mapOfParliament_itemDTOTolastModificationTime.containsKey(parliament_itemDTO.lastModificationTime)) {
						mapOfParliament_itemDTOTolastModificationTime.put(parliament_itemDTO.lastModificationTime, new HashSet<>());
					}
					mapOfParliament_itemDTOTolastModificationTime.get(parliament_itemDTO.lastModificationTime).add(parliament_itemDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Parliament_itemDTO> getParliament_itemList() {
		List <Parliament_itemDTO> parliament_items = new ArrayList<Parliament_itemDTO>(this.mapOfParliament_itemDTOToid.values());
		return parliament_items;
	}
	
	
	public Parliament_itemDTO getParliament_itemDTOByid( long id){
		return mapOfParliament_itemDTOToid.get(id);
	}
	
	
	public List<Parliament_itemDTO> getParliament_itemDTOByname_eng(String name_eng) {
		return new ArrayList<>( mapOfParliament_itemDTOTonameEng.getOrDefault(name_eng,new HashSet<>()));
	}
	
	
	public List<Parliament_itemDTO> getParliament_itemDTOByname_bng(String name_bng) {
		return new ArrayList<>( mapOfParliament_itemDTOTonameBng.getOrDefault(name_bng,new HashSet<>()));
	}
	
	
	public List<Parliament_itemDTO> getParliament_itemDTOBydescription(String description) {
		return new ArrayList<>( mapOfParliament_itemDTOTodescription.getOrDefault(description,new HashSet<>()));
	}
	
	
	public List<Parliament_itemDTO> getParliament_itemDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfParliament_itemDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Parliament_itemDTO> getParliament_itemDTOByinserted_by(String inserted_by) {
		return new ArrayList<>( mapOfParliament_itemDTOToinsertedBy.getOrDefault(inserted_by,new HashSet<>()));
	}
	
	
	public List<Parliament_itemDTO> getParliament_itemDTOBymodified_by(String modified_by) {
		return new ArrayList<>( mapOfParliament_itemDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Parliament_itemDTO> getParliament_itemDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfParliament_itemDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	public String buildOptions(String language, long selectedId) {
		List<OptionDTO> optionDTOList = null;
		List<Parliament_itemDTO> parliament_itemDTOS = new Parliament_itemDAO().getAllParliament_item(true);
		if (parliament_itemDTOS != null && !parliament_itemDTOS.isEmpty()) {
			optionDTOList = parliament_itemDTOS.stream()
					.map(dto -> new OptionDTO(dto.nameEng, dto.nameBng, String.valueOf(dto.iD)))
					.collect(Collectors.toList());
		}
		return Utils.buildOptions(optionDTOList, language, String.valueOf(selectedId));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "parliament_item";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


