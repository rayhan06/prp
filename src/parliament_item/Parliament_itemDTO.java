package parliament_item;
import java.nio.charset.StandardCharsets;
import java.util.*;
import util.*; 


public class Parliament_itemDTO extends CommonDTO
{

    public String nameEng = "";
    public String nameBng = "";
    public String description = "";
	public byte[] image = "dummy".getBytes();
	public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Parliament_itemDTO[" +
            " id = " + iD +
            " nameEng = " + nameEng +
            " nameBng = " + nameBng +
            " description = " + description +
            " image = " + image +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}