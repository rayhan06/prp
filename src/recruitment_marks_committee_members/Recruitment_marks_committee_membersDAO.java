package recruitment_marks_committee_members;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Recruitment_marks_committee_membersDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Recruitment_marks_committee_membersDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Recruitment_marks_committee_membersMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"recruitment_marks_committee_id",
			"mobile",
			"otp",
			"otpGenerationTime",
			"employee_record_id",
			"unit_id",
			"post_id",
			"employee_record_name",
			"employee_record_name_bn",
			"unit_name",
			"unit_name_bn",
			"post_name",
			"post_name_bn",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Recruitment_marks_committee_membersDAO()
	{
		this("recruitment_marks_committee_members");		
	}
	
	public void setSearchColumn(Recruitment_marks_committee_membersDTO recruitment_marks_committee_membersDTO)
	{
		recruitment_marks_committee_membersDTO.searchColumn = "";
		recruitment_marks_committee_membersDTO.searchColumn += recruitment_marks_committee_membersDTO.mobile + " ";
		recruitment_marks_committee_membersDTO.searchColumn += recruitment_marks_committee_membersDTO.otp + " ";
		recruitment_marks_committee_membersDTO.searchColumn += recruitment_marks_committee_membersDTO.employeeRecordName + " ";
		recruitment_marks_committee_membersDTO.searchColumn += recruitment_marks_committee_membersDTO.employeeRecordNameBn + " ";
		recruitment_marks_committee_membersDTO.searchColumn += recruitment_marks_committee_membersDTO.unitName + " ";
		recruitment_marks_committee_membersDTO.searchColumn += recruitment_marks_committee_membersDTO.unitNameBn + " ";
		recruitment_marks_committee_membersDTO.searchColumn += recruitment_marks_committee_membersDTO.postName + " ";
		recruitment_marks_committee_membersDTO.searchColumn += recruitment_marks_committee_membersDTO.postNameBn + " ";
		recruitment_marks_committee_membersDTO.searchColumn += recruitment_marks_committee_membersDTO.insertedBy + " ";
		recruitment_marks_committee_membersDTO.searchColumn += recruitment_marks_committee_membersDTO.modifiedBy + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Recruitment_marks_committee_membersDTO recruitment_marks_committee_membersDTO = (Recruitment_marks_committee_membersDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(recruitment_marks_committee_membersDTO);
		if(isInsert)
		{
			ps.setObject(index++,recruitment_marks_committee_membersDTO.iD);
		}
		ps.setObject(index++,recruitment_marks_committee_membersDTO.recruitmentMarksCommitteeId);
		ps.setObject(index++,recruitment_marks_committee_membersDTO.mobile);
		ps.setObject(index++,recruitment_marks_committee_membersDTO.otp);
		ps.setObject(index++,recruitment_marks_committee_membersDTO.otpGenerationTime);
		ps.setObject(index++,recruitment_marks_committee_membersDTO.employeeRecordId);
		ps.setObject(index++,recruitment_marks_committee_membersDTO.unitId);
		ps.setObject(index++,recruitment_marks_committee_membersDTO.postId);
		ps.setObject(index++,recruitment_marks_committee_membersDTO.employeeRecordName);
		ps.setObject(index++,recruitment_marks_committee_membersDTO.employeeRecordNameBn);
		ps.setObject(index++,recruitment_marks_committee_membersDTO.unitName);
		ps.setObject(index++,recruitment_marks_committee_membersDTO.unitNameBn);
		ps.setObject(index++,recruitment_marks_committee_membersDTO.postName);
		ps.setObject(index++,recruitment_marks_committee_membersDTO.postNameBn);
		ps.setObject(index++,recruitment_marks_committee_membersDTO.insertionDate);
		ps.setObject(index++,recruitment_marks_committee_membersDTO.insertedBy);
		ps.setObject(index++,recruitment_marks_committee_membersDTO.modifiedBy);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}


	public Recruitment_marks_committee_membersDTO build(ResultSet rs)
	{
		try
		{
			Recruitment_marks_committee_membersDTO recruitment_marks_committee_membersDTO = new Recruitment_marks_committee_membersDTO();
			recruitment_marks_committee_membersDTO.iD = rs.getLong("ID");
			recruitment_marks_committee_membersDTO.otpGenerationTime = rs.getLong("otpGenerationTime");

			recruitment_marks_committee_membersDTO.recruitmentMarksCommitteeId = rs.getLong("recruitment_marks_committee_id");
			recruitment_marks_committee_membersDTO.mobile = rs.getString("mobile");
			recruitment_marks_committee_membersDTO.otp = rs.getString("otp");
			recruitment_marks_committee_membersDTO.employeeRecordId = rs.getLong("employee_record_id");
			recruitment_marks_committee_membersDTO.unitId = rs.getLong("unit_id");
			recruitment_marks_committee_membersDTO.postId = rs.getLong("post_id");
			recruitment_marks_committee_membersDTO.employeeRecordName = rs.getString("employee_record_name");
			recruitment_marks_committee_membersDTO.employeeRecordNameBn = rs.getString("employee_record_name_bn");
			recruitment_marks_committee_membersDTO.unitName = rs.getString("unit_name");
			recruitment_marks_committee_membersDTO.unitNameBn = rs.getString("unit_name_bn");
			recruitment_marks_committee_membersDTO.postName = rs.getString("post_name");
			recruitment_marks_committee_membersDTO.postNameBn = rs.getString("post_name_bn");
			recruitment_marks_committee_membersDTO.insertionDate = rs.getLong("insertion_date");
			recruitment_marks_committee_membersDTO.insertedBy = rs.getString("inserted_by");
			recruitment_marks_committee_membersDTO.modifiedBy = rs.getString("modified_by");
			recruitment_marks_committee_membersDTO.isDeleted = rs.getInt("isDeleted");
			recruitment_marks_committee_membersDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return recruitment_marks_committee_membersDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	
	
	
		
	

	//need another getter for repository
	public Recruitment_marks_committee_membersDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		Recruitment_marks_committee_membersDTO dto =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
		return dto;
	}
	
	
	
	
	public List<Recruitment_marks_committee_membersDTO> getDTOs(Collection recordIDs){
		if(recordIDs.isEmpty()){
			return new ArrayList<>();
		}
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+= " ? ";
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	
	}
	
	

	
	
	
	//add repository
	public List<Recruitment_marks_committee_membersDTO> getAllRecruitment_marks_committee_members (boolean isFirstReload)
    {

		String sql = "SELECT * FROM recruitment_marks_committee_members";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by recruitment_marks_committee_members.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }

	public List<Recruitment_marks_committee_membersDTO> getAllRecruitment_marks_committee_membersByCommitteeId (long committeeId)
	{

		String sql = "SELECT * FROM recruitment_marks_committee_members ";
		sql += " WHERE ";
		sql+=" isDeleted =  0 and recruitment_marks_committee_id = ? ";

		sql += " order by recruitment_marks_committee_members.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(committeeId), this::build);

	}

	
	public List<Recruitment_marks_committee_membersDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Recruitment_marks_committee_membersDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{

		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);

		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,  this::build);
	
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("mobile")
						|| str.equals("otp")
						|| str.equals("employee_record_name")
						|| str.equals("employee_record_name_bn")
						|| str.equals("unit_name")
						|| str.equals("unit_name_bn")
						|| str.equals("post_name")
						|| str.equals("post_name_bn")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
						|| str.equals("inserted_by")
						|| str.equals("modified_by")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("mobile"))
					{
						AllFieldSql += "" + tableName + ".mobile like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("otp"))
					{
						AllFieldSql += "" + tableName + ".otp like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("employee_record_name"))
					{
						AllFieldSql += "" + tableName + ".employee_record_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("employee_record_name_bn"))
					{
						AllFieldSql += "" + tableName + ".employee_record_name_bn like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("unit_name"))
					{
						AllFieldSql += "" + tableName + ".unit_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".unit_name_bn like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("post_name"))
					{
						AllFieldSql += "" + tableName + ".post_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("post_name_bn"))
					{
						AllFieldSql += "" + tableName + ".post_name_bn like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("inserted_by"))
					{
						AllFieldSql += "" + tableName + ".inserted_by like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("modified_by"))
					{
						AllFieldSql += "" + tableName + ".modified_by like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	