package recruitment_marks_committee_members;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;
import vm_route.VmRouteStoppageDTO;


public class Recruitment_marks_committee_membersRepository implements Repository {
	Recruitment_marks_committee_membersDAO recruitment_marks_committee_membersDAO = null;
	Gson gson = new Gson();
	
	public void setDAO(Recruitment_marks_committee_membersDAO recruitment_marks_committee_membersDAO)
	{
		this.recruitment_marks_committee_membersDAO = recruitment_marks_committee_membersDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Recruitment_marks_committee_membersRepository.class);
	Map<Long, Recruitment_marks_committee_membersDTO>mapOfRecruitment_marks_committee_membersDTOToiD;
	Map<Long, Set<Recruitment_marks_committee_membersDTO> >mapOfRecruitment_marks_committee_membersDTOTorecruitmentMarksCommitteeId;
//	Map<String, Set<Recruitment_marks_committee_membersDTO> >mapOfRecruitment_marks_committee_membersDTOTomobile;
//	Map<String, Set<Recruitment_marks_committee_membersDTO> >mapOfRecruitment_marks_committee_membersDTOTootp;
//	Map<Long, Set<Recruitment_marks_committee_membersDTO> >mapOfRecruitment_marks_committee_membersDTOToemployeeRecordId;
//	Map<Long, Set<Recruitment_marks_committee_membersDTO> >mapOfRecruitment_marks_committee_membersDTOTounitId;
//	Map<Long, Set<Recruitment_marks_committee_membersDTO> >mapOfRecruitment_marks_committee_membersDTOTopostId;
//	Map<String, Set<Recruitment_marks_committee_membersDTO> >mapOfRecruitment_marks_committee_membersDTOToemployeeRecordName;
//	Map<String, Set<Recruitment_marks_committee_membersDTO> >mapOfRecruitment_marks_committee_membersDTOToemployeeRecordNameBn;
//	Map<String, Set<Recruitment_marks_committee_membersDTO> >mapOfRecruitment_marks_committee_membersDTOTounitName;
//	Map<String, Set<Recruitment_marks_committee_membersDTO> >mapOfRecruitment_marks_committee_membersDTOTounitNameBn;
//	Map<String, Set<Recruitment_marks_committee_membersDTO> >mapOfRecruitment_marks_committee_membersDTOTopostName;
//	Map<String, Set<Recruitment_marks_committee_membersDTO> >mapOfRecruitment_marks_committee_membersDTOTopostNameBn;
//	Map<Long, Set<Recruitment_marks_committee_membersDTO> >mapOfRecruitment_marks_committee_membersDTOToinsertionDate;
//	Map<String, Set<Recruitment_marks_committee_membersDTO> >mapOfRecruitment_marks_committee_membersDTOToinsertedBy;
//	Map<String, Set<Recruitment_marks_committee_membersDTO> >mapOfRecruitment_marks_committee_membersDTOTomodifiedBy;
//	Map<Long, Set<Recruitment_marks_committee_membersDTO> >mapOfRecruitment_marks_committee_membersDTOTolastModificationTime;


	static Recruitment_marks_committee_membersRepository instance = null;  
	private Recruitment_marks_committee_membersRepository(){
		mapOfRecruitment_marks_committee_membersDTOToiD = new ConcurrentHashMap<>();
		mapOfRecruitment_marks_committee_membersDTOTorecruitmentMarksCommitteeId = new ConcurrentHashMap<>();
//		mapOfRecruitment_marks_committee_membersDTOTomobile = new ConcurrentHashMap<>();
//		mapOfRecruitment_marks_committee_membersDTOTootp = new ConcurrentHashMap<>();
//		mapOfRecruitment_marks_committee_membersDTOToemployeeRecordId = new ConcurrentHashMap<>();
//		mapOfRecruitment_marks_committee_membersDTOTounitId = new ConcurrentHashMap<>();
//		mapOfRecruitment_marks_committee_membersDTOTopostId = new ConcurrentHashMap<>();
//		mapOfRecruitment_marks_committee_membersDTOToemployeeRecordName = new ConcurrentHashMap<>();
//		mapOfRecruitment_marks_committee_membersDTOToemployeeRecordNameBn = new ConcurrentHashMap<>();
//		mapOfRecruitment_marks_committee_membersDTOTounitName = new ConcurrentHashMap<>();
//		mapOfRecruitment_marks_committee_membersDTOTounitNameBn = new ConcurrentHashMap<>();
//		mapOfRecruitment_marks_committee_membersDTOTopostName = new ConcurrentHashMap<>();
//		mapOfRecruitment_marks_committee_membersDTOTopostNameBn = new ConcurrentHashMap<>();
//		mapOfRecruitment_marks_committee_membersDTOToinsertionDate = new ConcurrentHashMap<>();
//		mapOfRecruitment_marks_committee_membersDTOToinsertedBy = new ConcurrentHashMap<>();
//		mapOfRecruitment_marks_committee_membersDTOTomodifiedBy = new ConcurrentHashMap<>();
//		mapOfRecruitment_marks_committee_membersDTOTolastModificationTime = new ConcurrentHashMap<>();

		setDAO(new Recruitment_marks_committee_membersDAO());

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Recruitment_marks_committee_membersRepository getInstance(){
		if (instance == null){
			instance = new Recruitment_marks_committee_membersRepository();
		}
		return instance;
	}

	public Recruitment_marks_committee_membersDTO clone(Recruitment_marks_committee_membersDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Recruitment_marks_committee_membersDTO.class);
	}

	public List<Recruitment_marks_committee_membersDTO> clone(List<Recruitment_marks_committee_membersDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public void reload(boolean reloadAll){
		if(recruitment_marks_committee_membersDAO == null)
		{
			return;
		}
		try {
			List<Recruitment_marks_committee_membersDTO> recruitment_marks_committee_membersDTOs = recruitment_marks_committee_membersDAO.getAllRecruitment_marks_committee_members(reloadAll);
			for(Recruitment_marks_committee_membersDTO recruitment_marks_committee_membersDTO : recruitment_marks_committee_membersDTOs) {
				Recruitment_marks_committee_membersDTO oldRecruitment_marks_committee_membersDTO =
						getRecruitment_marks_committee_membersDTOByIDWithoutClone(recruitment_marks_committee_membersDTO.iD);
				if( oldRecruitment_marks_committee_membersDTO != null ) {
					mapOfRecruitment_marks_committee_membersDTOToiD.remove(oldRecruitment_marks_committee_membersDTO.iD);
				
					if(mapOfRecruitment_marks_committee_membersDTOTorecruitmentMarksCommitteeId.containsKey(oldRecruitment_marks_committee_membersDTO.recruitmentMarksCommitteeId)) {
						mapOfRecruitment_marks_committee_membersDTOTorecruitmentMarksCommitteeId.get(oldRecruitment_marks_committee_membersDTO.recruitmentMarksCommitteeId).remove(oldRecruitment_marks_committee_membersDTO);
					}
					if(mapOfRecruitment_marks_committee_membersDTOTorecruitmentMarksCommitteeId.get(oldRecruitment_marks_committee_membersDTO.recruitmentMarksCommitteeId).isEmpty()) {
						mapOfRecruitment_marks_committee_membersDTOTorecruitmentMarksCommitteeId.remove(oldRecruitment_marks_committee_membersDTO.recruitmentMarksCommitteeId);
					}
					
//					if(mapOfRecruitment_marks_committee_membersDTOTomobile.containsKey(oldRecruitment_marks_committee_membersDTO.mobile)) {
//						mapOfRecruitment_marks_committee_membersDTOTomobile.get(oldRecruitment_marks_committee_membersDTO.mobile).remove(oldRecruitment_marks_committee_membersDTO);
//					}
//					if(mapOfRecruitment_marks_committee_membersDTOTomobile.get(oldRecruitment_marks_committee_membersDTO.mobile).isEmpty()) {
//						mapOfRecruitment_marks_committee_membersDTOTomobile.remove(oldRecruitment_marks_committee_membersDTO.mobile);
//					}
//
//					if(mapOfRecruitment_marks_committee_membersDTOTootp.containsKey(oldRecruitment_marks_committee_membersDTO.otp)) {
//						mapOfRecruitment_marks_committee_membersDTOTootp.get(oldRecruitment_marks_committee_membersDTO.otp).remove(oldRecruitment_marks_committee_membersDTO);
//					}
//					if(mapOfRecruitment_marks_committee_membersDTOTootp.get(oldRecruitment_marks_committee_membersDTO.otp).isEmpty()) {
//						mapOfRecruitment_marks_committee_membersDTOTootp.remove(oldRecruitment_marks_committee_membersDTO.otp);
//					}
//
//					if(mapOfRecruitment_marks_committee_membersDTOToemployeeRecordId.containsKey(oldRecruitment_marks_committee_membersDTO.employeeRecordId)) {
//						mapOfRecruitment_marks_committee_membersDTOToemployeeRecordId.get(oldRecruitment_marks_committee_membersDTO.employeeRecordId).remove(oldRecruitment_marks_committee_membersDTO);
//					}
//					if(mapOfRecruitment_marks_committee_membersDTOToemployeeRecordId.get(oldRecruitment_marks_committee_membersDTO.employeeRecordId).isEmpty()) {
//						mapOfRecruitment_marks_committee_membersDTOToemployeeRecordId.remove(oldRecruitment_marks_committee_membersDTO.employeeRecordId);
//					}
//
//					if(mapOfRecruitment_marks_committee_membersDTOTounitId.containsKey(oldRecruitment_marks_committee_membersDTO.unitId)) {
//						mapOfRecruitment_marks_committee_membersDTOTounitId.get(oldRecruitment_marks_committee_membersDTO.unitId).remove(oldRecruitment_marks_committee_membersDTO);
//					}
//					if(mapOfRecruitment_marks_committee_membersDTOTounitId.get(oldRecruitment_marks_committee_membersDTO.unitId).isEmpty()) {
//						mapOfRecruitment_marks_committee_membersDTOTounitId.remove(oldRecruitment_marks_committee_membersDTO.unitId);
//					}
//
//					if(mapOfRecruitment_marks_committee_membersDTOTopostId.containsKey(oldRecruitment_marks_committee_membersDTO.postId)) {
//						mapOfRecruitment_marks_committee_membersDTOTopostId.get(oldRecruitment_marks_committee_membersDTO.postId).remove(oldRecruitment_marks_committee_membersDTO);
//					}
//					if(mapOfRecruitment_marks_committee_membersDTOTopostId.get(oldRecruitment_marks_committee_membersDTO.postId).isEmpty()) {
//						mapOfRecruitment_marks_committee_membersDTOTopostId.remove(oldRecruitment_marks_committee_membersDTO.postId);
//					}
//
//					if(mapOfRecruitment_marks_committee_membersDTOToemployeeRecordName.containsKey(oldRecruitment_marks_committee_membersDTO.employeeRecordName)) {
//						mapOfRecruitment_marks_committee_membersDTOToemployeeRecordName.get(oldRecruitment_marks_committee_membersDTO.employeeRecordName).remove(oldRecruitment_marks_committee_membersDTO);
//					}
//					if(mapOfRecruitment_marks_committee_membersDTOToemployeeRecordName.get(oldRecruitment_marks_committee_membersDTO.employeeRecordName).isEmpty()) {
//						mapOfRecruitment_marks_committee_membersDTOToemployeeRecordName.remove(oldRecruitment_marks_committee_membersDTO.employeeRecordName);
//					}
//
//					if(mapOfRecruitment_marks_committee_membersDTOToemployeeRecordNameBn.containsKey(oldRecruitment_marks_committee_membersDTO.employeeRecordNameBn)) {
//						mapOfRecruitment_marks_committee_membersDTOToemployeeRecordNameBn.get(oldRecruitment_marks_committee_membersDTO.employeeRecordNameBn).remove(oldRecruitment_marks_committee_membersDTO);
//					}
//					if(mapOfRecruitment_marks_committee_membersDTOToemployeeRecordNameBn.get(oldRecruitment_marks_committee_membersDTO.employeeRecordNameBn).isEmpty()) {
//						mapOfRecruitment_marks_committee_membersDTOToemployeeRecordNameBn.remove(oldRecruitment_marks_committee_membersDTO.employeeRecordNameBn);
//					}
//
//					if(mapOfRecruitment_marks_committee_membersDTOTounitName.containsKey(oldRecruitment_marks_committee_membersDTO.unitName)) {
//						mapOfRecruitment_marks_committee_membersDTOTounitName.get(oldRecruitment_marks_committee_membersDTO.unitName).remove(oldRecruitment_marks_committee_membersDTO);
//					}
//					if(mapOfRecruitment_marks_committee_membersDTOTounitName.get(oldRecruitment_marks_committee_membersDTO.unitName).isEmpty()) {
//						mapOfRecruitment_marks_committee_membersDTOTounitName.remove(oldRecruitment_marks_committee_membersDTO.unitName);
//					}
//
//					if(mapOfRecruitment_marks_committee_membersDTOTounitNameBn.containsKey(oldRecruitment_marks_committee_membersDTO.unitNameBn)) {
//						mapOfRecruitment_marks_committee_membersDTOTounitNameBn.get(oldRecruitment_marks_committee_membersDTO.unitNameBn).remove(oldRecruitment_marks_committee_membersDTO);
//					}
//					if(mapOfRecruitment_marks_committee_membersDTOTounitNameBn.get(oldRecruitment_marks_committee_membersDTO.unitNameBn).isEmpty()) {
//						mapOfRecruitment_marks_committee_membersDTOTounitNameBn.remove(oldRecruitment_marks_committee_membersDTO.unitNameBn);
//					}
//
//					if(mapOfRecruitment_marks_committee_membersDTOTopostName.containsKey(oldRecruitment_marks_committee_membersDTO.postName)) {
//						mapOfRecruitment_marks_committee_membersDTOTopostName.get(oldRecruitment_marks_committee_membersDTO.postName).remove(oldRecruitment_marks_committee_membersDTO);
//					}
//					if(mapOfRecruitment_marks_committee_membersDTOTopostName.get(oldRecruitment_marks_committee_membersDTO.postName).isEmpty()) {
//						mapOfRecruitment_marks_committee_membersDTOTopostName.remove(oldRecruitment_marks_committee_membersDTO.postName);
//					}
//
//					if(mapOfRecruitment_marks_committee_membersDTOTopostNameBn.containsKey(oldRecruitment_marks_committee_membersDTO.postNameBn)) {
//						mapOfRecruitment_marks_committee_membersDTOTopostNameBn.get(oldRecruitment_marks_committee_membersDTO.postNameBn).remove(oldRecruitment_marks_committee_membersDTO);
//					}
//					if(mapOfRecruitment_marks_committee_membersDTOTopostNameBn.get(oldRecruitment_marks_committee_membersDTO.postNameBn).isEmpty()) {
//						mapOfRecruitment_marks_committee_membersDTOTopostNameBn.remove(oldRecruitment_marks_committee_membersDTO.postNameBn);
//					}
//
//					if(mapOfRecruitment_marks_committee_membersDTOToinsertionDate.containsKey(oldRecruitment_marks_committee_membersDTO.insertionDate)) {
//						mapOfRecruitment_marks_committee_membersDTOToinsertionDate.get(oldRecruitment_marks_committee_membersDTO.insertionDate).remove(oldRecruitment_marks_committee_membersDTO);
//					}
//					if(mapOfRecruitment_marks_committee_membersDTOToinsertionDate.get(oldRecruitment_marks_committee_membersDTO.insertionDate).isEmpty()) {
//						mapOfRecruitment_marks_committee_membersDTOToinsertionDate.remove(oldRecruitment_marks_committee_membersDTO.insertionDate);
//					}
//
//					if(mapOfRecruitment_marks_committee_membersDTOToinsertedBy.containsKey(oldRecruitment_marks_committee_membersDTO.insertedBy)) {
//						mapOfRecruitment_marks_committee_membersDTOToinsertedBy.get(oldRecruitment_marks_committee_membersDTO.insertedBy).remove(oldRecruitment_marks_committee_membersDTO);
//					}
//					if(mapOfRecruitment_marks_committee_membersDTOToinsertedBy.get(oldRecruitment_marks_committee_membersDTO.insertedBy).isEmpty()) {
//						mapOfRecruitment_marks_committee_membersDTOToinsertedBy.remove(oldRecruitment_marks_committee_membersDTO.insertedBy);
//					}
//
//					if(mapOfRecruitment_marks_committee_membersDTOTomodifiedBy.containsKey(oldRecruitment_marks_committee_membersDTO.modifiedBy)) {
//						mapOfRecruitment_marks_committee_membersDTOTomodifiedBy.get(oldRecruitment_marks_committee_membersDTO.modifiedBy).remove(oldRecruitment_marks_committee_membersDTO);
//					}
//					if(mapOfRecruitment_marks_committee_membersDTOTomodifiedBy.get(oldRecruitment_marks_committee_membersDTO.modifiedBy).isEmpty()) {
//						mapOfRecruitment_marks_committee_membersDTOTomodifiedBy.remove(oldRecruitment_marks_committee_membersDTO.modifiedBy);
//					}
//
//					if(mapOfRecruitment_marks_committee_membersDTOTolastModificationTime.containsKey(oldRecruitment_marks_committee_membersDTO.lastModificationTime)) {
//						mapOfRecruitment_marks_committee_membersDTOTolastModificationTime.get(oldRecruitment_marks_committee_membersDTO.lastModificationTime).remove(oldRecruitment_marks_committee_membersDTO);
//					}
//					if(mapOfRecruitment_marks_committee_membersDTOTolastModificationTime.get(oldRecruitment_marks_committee_membersDTO.lastModificationTime).isEmpty()) {
//						mapOfRecruitment_marks_committee_membersDTOTolastModificationTime.remove(oldRecruitment_marks_committee_membersDTO.lastModificationTime);
//					}
					
					
				}
				if(recruitment_marks_committee_membersDTO.isDeleted == 0) 
				{
					
					mapOfRecruitment_marks_committee_membersDTOToiD.put(recruitment_marks_committee_membersDTO.iD, recruitment_marks_committee_membersDTO);
				
					if( ! mapOfRecruitment_marks_committee_membersDTOTorecruitmentMarksCommitteeId.containsKey(recruitment_marks_committee_membersDTO.recruitmentMarksCommitteeId)) {
						mapOfRecruitment_marks_committee_membersDTOTorecruitmentMarksCommitteeId.put(recruitment_marks_committee_membersDTO.recruitmentMarksCommitteeId, new HashSet<>());
					}
					mapOfRecruitment_marks_committee_membersDTOTorecruitmentMarksCommitteeId.get(recruitment_marks_committee_membersDTO.recruitmentMarksCommitteeId).add(recruitment_marks_committee_membersDTO);
					
//					if( ! mapOfRecruitment_marks_committee_membersDTOTomobile.containsKey(recruitment_marks_committee_membersDTO.mobile)) {
//						mapOfRecruitment_marks_committee_membersDTOTomobile.put(recruitment_marks_committee_membersDTO.mobile, new HashSet<>());
//					}
//					mapOfRecruitment_marks_committee_membersDTOTomobile.get(recruitment_marks_committee_membersDTO.mobile).add(recruitment_marks_committee_membersDTO);
//
//					if( ! mapOfRecruitment_marks_committee_membersDTOTootp.containsKey(recruitment_marks_committee_membersDTO.otp)) {
//						mapOfRecruitment_marks_committee_membersDTOTootp.put(recruitment_marks_committee_membersDTO.otp, new HashSet<>());
//					}
//					mapOfRecruitment_marks_committee_membersDTOTootp.get(recruitment_marks_committee_membersDTO.otp).add(recruitment_marks_committee_membersDTO);
//
//					if( ! mapOfRecruitment_marks_committee_membersDTOToemployeeRecordId.containsKey(recruitment_marks_committee_membersDTO.employeeRecordId)) {
//						mapOfRecruitment_marks_committee_membersDTOToemployeeRecordId.put(recruitment_marks_committee_membersDTO.employeeRecordId, new HashSet<>());
//					}
//					mapOfRecruitment_marks_committee_membersDTOToemployeeRecordId.get(recruitment_marks_committee_membersDTO.employeeRecordId).add(recruitment_marks_committee_membersDTO);
//
//					if( ! mapOfRecruitment_marks_committee_membersDTOTounitId.containsKey(recruitment_marks_committee_membersDTO.unitId)) {
//						mapOfRecruitment_marks_committee_membersDTOTounitId.put(recruitment_marks_committee_membersDTO.unitId, new HashSet<>());
//					}
//					mapOfRecruitment_marks_committee_membersDTOTounitId.get(recruitment_marks_committee_membersDTO.unitId).add(recruitment_marks_committee_membersDTO);
//
//					if( ! mapOfRecruitment_marks_committee_membersDTOTopostId.containsKey(recruitment_marks_committee_membersDTO.postId)) {
//						mapOfRecruitment_marks_committee_membersDTOTopostId.put(recruitment_marks_committee_membersDTO.postId, new HashSet<>());
//					}
//					mapOfRecruitment_marks_committee_membersDTOTopostId.get(recruitment_marks_committee_membersDTO.postId).add(recruitment_marks_committee_membersDTO);
//
//					if( ! mapOfRecruitment_marks_committee_membersDTOToemployeeRecordName.containsKey(recruitment_marks_committee_membersDTO.employeeRecordName)) {
//						mapOfRecruitment_marks_committee_membersDTOToemployeeRecordName.put(recruitment_marks_committee_membersDTO.employeeRecordName, new HashSet<>());
//					}
//					mapOfRecruitment_marks_committee_membersDTOToemployeeRecordName.get(recruitment_marks_committee_membersDTO.employeeRecordName).add(recruitment_marks_committee_membersDTO);
//
//					if( ! mapOfRecruitment_marks_committee_membersDTOToemployeeRecordNameBn.containsKey(recruitment_marks_committee_membersDTO.employeeRecordNameBn)) {
//						mapOfRecruitment_marks_committee_membersDTOToemployeeRecordNameBn.put(recruitment_marks_committee_membersDTO.employeeRecordNameBn, new HashSet<>());
//					}
//					mapOfRecruitment_marks_committee_membersDTOToemployeeRecordNameBn.get(recruitment_marks_committee_membersDTO.employeeRecordNameBn).add(recruitment_marks_committee_membersDTO);
//
//					if( ! mapOfRecruitment_marks_committee_membersDTOTounitName.containsKey(recruitment_marks_committee_membersDTO.unitName)) {
//						mapOfRecruitment_marks_committee_membersDTOTounitName.put(recruitment_marks_committee_membersDTO.unitName, new HashSet<>());
//					}
//					mapOfRecruitment_marks_committee_membersDTOTounitName.get(recruitment_marks_committee_membersDTO.unitName).add(recruitment_marks_committee_membersDTO);
//
//					if( ! mapOfRecruitment_marks_committee_membersDTOTounitNameBn.containsKey(recruitment_marks_committee_membersDTO.unitNameBn)) {
//						mapOfRecruitment_marks_committee_membersDTOTounitNameBn.put(recruitment_marks_committee_membersDTO.unitNameBn, new HashSet<>());
//					}
//					mapOfRecruitment_marks_committee_membersDTOTounitNameBn.get(recruitment_marks_committee_membersDTO.unitNameBn).add(recruitment_marks_committee_membersDTO);
//
//					if( ! mapOfRecruitment_marks_committee_membersDTOTopostName.containsKey(recruitment_marks_committee_membersDTO.postName)) {
//						mapOfRecruitment_marks_committee_membersDTOTopostName.put(recruitment_marks_committee_membersDTO.postName, new HashSet<>());
//					}
//					mapOfRecruitment_marks_committee_membersDTOTopostName.get(recruitment_marks_committee_membersDTO.postName).add(recruitment_marks_committee_membersDTO);
//
//					if( ! mapOfRecruitment_marks_committee_membersDTOTopostNameBn.containsKey(recruitment_marks_committee_membersDTO.postNameBn)) {
//						mapOfRecruitment_marks_committee_membersDTOTopostNameBn.put(recruitment_marks_committee_membersDTO.postNameBn, new HashSet<>());
//					}
//					mapOfRecruitment_marks_committee_membersDTOTopostNameBn.get(recruitment_marks_committee_membersDTO.postNameBn).add(recruitment_marks_committee_membersDTO);
//
//					if( ! mapOfRecruitment_marks_committee_membersDTOToinsertionDate.containsKey(recruitment_marks_committee_membersDTO.insertionDate)) {
//						mapOfRecruitment_marks_committee_membersDTOToinsertionDate.put(recruitment_marks_committee_membersDTO.insertionDate, new HashSet<>());
//					}
//					mapOfRecruitment_marks_committee_membersDTOToinsertionDate.get(recruitment_marks_committee_membersDTO.insertionDate).add(recruitment_marks_committee_membersDTO);
//
//					if( ! mapOfRecruitment_marks_committee_membersDTOToinsertedBy.containsKey(recruitment_marks_committee_membersDTO.insertedBy)) {
//						mapOfRecruitment_marks_committee_membersDTOToinsertedBy.put(recruitment_marks_committee_membersDTO.insertedBy, new HashSet<>());
//					}
//					mapOfRecruitment_marks_committee_membersDTOToinsertedBy.get(recruitment_marks_committee_membersDTO.insertedBy).add(recruitment_marks_committee_membersDTO);
//
//					if( ! mapOfRecruitment_marks_committee_membersDTOTomodifiedBy.containsKey(recruitment_marks_committee_membersDTO.modifiedBy)) {
//						mapOfRecruitment_marks_committee_membersDTOTomodifiedBy.put(recruitment_marks_committee_membersDTO.modifiedBy, new HashSet<>());
//					}
//					mapOfRecruitment_marks_committee_membersDTOTomodifiedBy.get(recruitment_marks_committee_membersDTO.modifiedBy).add(recruitment_marks_committee_membersDTO);
//
//					if( ! mapOfRecruitment_marks_committee_membersDTOTolastModificationTime.containsKey(recruitment_marks_committee_membersDTO.lastModificationTime)) {
//						mapOfRecruitment_marks_committee_membersDTOTolastModificationTime.put(recruitment_marks_committee_membersDTO.lastModificationTime, new HashSet<>());
//					}
//					mapOfRecruitment_marks_committee_membersDTOTolastModificationTime.get(recruitment_marks_committee_membersDTO.lastModificationTime).add(recruitment_marks_committee_membersDTO);
//
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Recruitment_marks_committee_membersDTO> getRecruitment_marks_committee_membersList() {
		List <Recruitment_marks_committee_membersDTO> recruitment_marks_committee_memberss = new ArrayList<Recruitment_marks_committee_membersDTO>(this.mapOfRecruitment_marks_committee_membersDTOToiD.values());
		return clone(recruitment_marks_committee_memberss);
	}
	
	
	public Recruitment_marks_committee_membersDTO getRecruitment_marks_committee_membersDTOByIDWithoutClone( long ID){
		return mapOfRecruitment_marks_committee_membersDTOToiD.get(ID);
	}

	public Recruitment_marks_committee_membersDTO getRecruitment_marks_committee_membersDTOByID( long ID){
		return clone(mapOfRecruitment_marks_committee_membersDTOToiD.get(ID));
	}
	
	
	public List<Recruitment_marks_committee_membersDTO> getRecruitment_marks_committee_membersDTOByrecruitment_marks_committee_id(long recruitment_marks_committee_id) {
		return clone(new ArrayList<>( mapOfRecruitment_marks_committee_membersDTOTorecruitmentMarksCommitteeId.
				getOrDefault(recruitment_marks_committee_id,new HashSet<>())));
	}
	
	
//	public List<Recruitment_marks_committee_membersDTO> getRecruitment_marks_committee_membersDTOBymobile(String mobile) {
//		return new ArrayList<>( mapOfRecruitment_marks_committee_membersDTOTomobile.getOrDefault(mobile,new HashSet<>()));
//	}
//
//
//	public List<Recruitment_marks_committee_membersDTO> getRecruitment_marks_committee_membersDTOByotp(String otp) {
//		return new ArrayList<>( mapOfRecruitment_marks_committee_membersDTOTootp.getOrDefault(otp,new HashSet<>()));
//	}
//
//
//	public List<Recruitment_marks_committee_membersDTO> getRecruitment_marks_committee_membersDTOByemployee_record_id(long employee_record_id) {
//		return new ArrayList<>( mapOfRecruitment_marks_committee_membersDTOToemployeeRecordId.getOrDefault(employee_record_id,new HashSet<>()));
//	}
//
//
//	public List<Recruitment_marks_committee_membersDTO> getRecruitment_marks_committee_membersDTOByunit_id(long unit_id) {
//		return new ArrayList<>( mapOfRecruitment_marks_committee_membersDTOTounitId.getOrDefault(unit_id,new HashSet<>()));
//	}
//
//
//	public List<Recruitment_marks_committee_membersDTO> getRecruitment_marks_committee_membersDTOBypost_id(long post_id) {
//		return new ArrayList<>( mapOfRecruitment_marks_committee_membersDTOTopostId.getOrDefault(post_id,new HashSet<>()));
//	}
//
//
//	public List<Recruitment_marks_committee_membersDTO> getRecruitment_marks_committee_membersDTOByemployee_record_name(String employee_record_name) {
//		return new ArrayList<>( mapOfRecruitment_marks_committee_membersDTOToemployeeRecordName.getOrDefault(employee_record_name,new HashSet<>()));
//	}
//
//
//	public List<Recruitment_marks_committee_membersDTO> getRecruitment_marks_committee_membersDTOByemployee_record_name_bn(String employee_record_name_bn) {
//		return new ArrayList<>( mapOfRecruitment_marks_committee_membersDTOToemployeeRecordNameBn.getOrDefault(employee_record_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Recruitment_marks_committee_membersDTO> getRecruitment_marks_committee_membersDTOByunit_name(String unit_name) {
//		return new ArrayList<>( mapOfRecruitment_marks_committee_membersDTOTounitName.getOrDefault(unit_name,new HashSet<>()));
//	}
//
//
//	public List<Recruitment_marks_committee_membersDTO> getRecruitment_marks_committee_membersDTOByunit_name_bn(String unit_name_bn) {
//		return new ArrayList<>( mapOfRecruitment_marks_committee_membersDTOTounitNameBn.getOrDefault(unit_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Recruitment_marks_committee_membersDTO> getRecruitment_marks_committee_membersDTOBypost_name(String post_name) {
//		return new ArrayList<>( mapOfRecruitment_marks_committee_membersDTOTopostName.getOrDefault(post_name,new HashSet<>()));
//	}
//
//
//	public List<Recruitment_marks_committee_membersDTO> getRecruitment_marks_committee_membersDTOBypost_name_bn(String post_name_bn) {
//		return new ArrayList<>( mapOfRecruitment_marks_committee_membersDTOTopostNameBn.getOrDefault(post_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Recruitment_marks_committee_membersDTO> getRecruitment_marks_committee_membersDTOByinsertion_date(long insertion_date) {
//		return new ArrayList<>( mapOfRecruitment_marks_committee_membersDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
//	}
//
//
//	public List<Recruitment_marks_committee_membersDTO> getRecruitment_marks_committee_membersDTOByinserted_by(String inserted_by) {
//		return new ArrayList<>( mapOfRecruitment_marks_committee_membersDTOToinsertedBy.getOrDefault(inserted_by,new HashSet<>()));
//	}
//
//
//	public List<Recruitment_marks_committee_membersDTO> getRecruitment_marks_committee_membersDTOBymodified_by(String modified_by) {
//		return new ArrayList<>( mapOfRecruitment_marks_committee_membersDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
//	}
//
//
//	public List<Recruitment_marks_committee_membersDTO> getRecruitment_marks_committee_membersDTOBylastModificationTime(long lastModificationTime) {
//		return new ArrayList<>( mapOfRecruitment_marks_committee_membersDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
//	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "recruitment_marks_committee_members";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


