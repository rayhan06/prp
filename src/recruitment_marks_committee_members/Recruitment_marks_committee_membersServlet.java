package recruitment_marks_committee_members;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Recruitment_marks_committee_membersServlet
 */
@WebServlet("/Recruitment_marks_committee_membersServlet")
@MultipartConfig
public class Recruitment_marks_committee_membersServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Recruitment_marks_committee_membersServlet.class);

    String tableName = "recruitment_marks_committee_members";

	Recruitment_marks_committee_membersDAO recruitment_marks_committee_membersDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Recruitment_marks_committee_membersServlet()
	{
        super();
    	try
    	{
			recruitment_marks_committee_membersDAO = new Recruitment_marks_committee_membersDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(recruitment_marks_committee_membersDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_UPDATE))
				{
					getRecruitment_marks_committee_members(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchRecruitment_marks_committee_members(request, response, isPermanentTable, filter);
						}
						else
						{
							searchRecruitment_marks_committee_members(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchRecruitment_marks_committee_members(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD))
				{
					System.out.println("going to  addRecruitment_marks_committee_members ");
					addRecruitment_marks_committee_members(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addRecruitment_marks_committee_members ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addRecruitment_marks_committee_members ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_UPDATE))
				{
					addRecruitment_marks_committee_members(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH))
				{
					searchRecruitment_marks_committee_members(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Recruitment_marks_committee_membersDTO recruitment_marks_committee_membersDTO = recruitment_marks_committee_membersDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(recruitment_marks_committee_membersDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addRecruitment_marks_committee_members(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addRecruitment_marks_committee_members");
			String path = getServletContext().getRealPath("/img2/");
			Recruitment_marks_committee_membersDTO recruitment_marks_committee_membersDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				recruitment_marks_committee_membersDTO = new Recruitment_marks_committee_membersDTO();
			}
			else
			{
				recruitment_marks_committee_membersDTO = recruitment_marks_committee_membersDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("recruitmentMarksCommitteeId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("recruitmentMarksCommitteeId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				recruitment_marks_committee_membersDTO.recruitmentMarksCommitteeId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("mobile");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("mobile = " + Value);
			if(Value != null)
			{
				recruitment_marks_committee_membersDTO.mobile = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("otp");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("otp = " + Value);
			if(Value != null)
			{
				recruitment_marks_committee_membersDTO.otp = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("employeeRecordId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeRecordId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				recruitment_marks_committee_membersDTO.employeeRecordId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("unitId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("unitId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				recruitment_marks_committee_membersDTO.unitId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("postId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("postId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				recruitment_marks_committee_membersDTO.postId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("employeeRecordName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeRecordName = " + Value);
			if(Value != null)
			{
				recruitment_marks_committee_membersDTO.employeeRecordName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("employeeRecordNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeRecordNameBn = " + Value);
			if(Value != null)
			{
				recruitment_marks_committee_membersDTO.employeeRecordNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("unitName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("unitName = " + Value);
			if(Value != null)
			{
				recruitment_marks_committee_membersDTO.unitName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("unitNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("unitNameBn = " + Value);
			if(Value != null)
			{
				recruitment_marks_committee_membersDTO.unitNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("postName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("postName = " + Value);
			if(Value != null)
			{
				recruitment_marks_committee_membersDTO.postName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("postNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("postNameBn = " + Value);
			if(Value != null)
			{
				recruitment_marks_committee_membersDTO.postNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				recruitment_marks_committee_membersDTO.insertionDate = c.getTimeInMillis();
			}


			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null)
			{
				recruitment_marks_committee_membersDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null)
			{
				recruitment_marks_committee_membersDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addRecruitment_marks_committee_members dto = " + recruitment_marks_committee_membersDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				recruitment_marks_committee_membersDAO.setIsDeleted(recruitment_marks_committee_membersDTO.iD, CommonDTO.OUTDATED);
				returnedID = recruitment_marks_committee_membersDAO.add(recruitment_marks_committee_membersDTO);
				recruitment_marks_committee_membersDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = recruitment_marks_committee_membersDAO.manageWriteOperations(recruitment_marks_committee_membersDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = recruitment_marks_committee_membersDAO.manageWriteOperations(recruitment_marks_committee_membersDTO, SessionConstants.UPDATE, -1, userDTO);
			}









			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getRecruitment_marks_committee_members(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Recruitment_marks_committee_membersServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(recruitment_marks_committee_membersDAO.getDTOByID(returnedID), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}









	private void getRecruitment_marks_committee_members(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getRecruitment_marks_committee_members");
		Recruitment_marks_committee_membersDTO recruitment_marks_committee_membersDTO = null;
		try
		{
			recruitment_marks_committee_membersDTO = recruitment_marks_committee_membersDAO.getDTOByID(id);
			request.setAttribute("ID", recruitment_marks_committee_membersDTO.iD);
			request.setAttribute("recruitment_marks_committee_membersDTO",recruitment_marks_committee_membersDTO);
			request.setAttribute("recruitment_marks_committee_membersDAO",recruitment_marks_committee_membersDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "recruitment_marks_committee_members/recruitment_marks_committee_membersInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "recruitment_marks_committee_members/recruitment_marks_committee_membersSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "recruitment_marks_committee_members/recruitment_marks_committee_membersEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "recruitment_marks_committee_members/recruitment_marks_committee_membersEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getRecruitment_marks_committee_members(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getRecruitment_marks_committee_members(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchRecruitment_marks_committee_members(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchRecruitment_marks_committee_members 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_RECRUITMENT_MARKS_COMMITTEE_MEMBERS,
			request,
			recruitment_marks_committee_membersDAO,
			SessionConstants.VIEW_RECRUITMENT_MARKS_COMMITTEE_MEMBERS,
			SessionConstants.SEARCH_RECRUITMENT_MARKS_COMMITTEE_MEMBERS,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("recruitment_marks_committee_membersDAO",recruitment_marks_committee_membersDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to recruitment_marks_committee_members/recruitment_marks_committee_membersApproval.jsp");
	        	rd = request.getRequestDispatcher("recruitment_marks_committee_members/recruitment_marks_committee_membersApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to recruitment_marks_committee_members/recruitment_marks_committee_membersApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("recruitment_marks_committee_members/recruitment_marks_committee_membersApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to recruitment_marks_committee_members/recruitment_marks_committee_membersSearch.jsp");
	        	rd = request.getRequestDispatcher("recruitment_marks_committee_members/recruitment_marks_committee_membersSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to recruitment_marks_committee_members/recruitment_marks_committee_membersSearchForm.jsp");
	        	rd = request.getRequestDispatcher("recruitment_marks_committee_members/recruitment_marks_committee_membersSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

