package recruitment_marks_committee_members;
import java.util.*; 
import util.*; 


public class Recruitment_marks_committee_membersDTO extends CommonDTO
{

	public long recruitmentMarksCommitteeId = -1;
    public long otpGenerationTime = 0;
    public String mobile = "";
    public String otp = "";
	public long employeeRecordId = -1;
	public long unitId = -1;
	public long postId = -1;
    public String employeeRecordName = "";
    public String employeeRecordNameBn = "";
    public String unitName = "";
    public String unitNameBn = "";
    public String postName = "";
    public String postNameBn = "";
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Recruitment_marks_committee_membersDTO[" +
            " iD = " + iD +
            " recruitmentMarksCommitteeId = " + recruitmentMarksCommitteeId +
            " mobile = " + mobile +
            " otp = " + otp +
            " employeeRecordId = " + employeeRecordId +
            " unitId = " + unitId +
            " postId = " + postId +
            " employeeRecordName = " + employeeRecordName +
            " employeeRecordNameBn = " + employeeRecordNameBn +
            " unitName = " + unitName +
            " unitNameBn = " + unitNameBn +
            " postName = " + postName +
            " postNameBn = " + postNameBn +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}