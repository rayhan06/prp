-- designation_status_report
SELECT ouo.id,
       ouo.office_unit_id,
       ouo.job_grade_type_cat,
       eo.status,
       eo.joining_date,
       eo.last_office_date
FROM office_unit_organograms ouo
         LEFT JOIN employee_offices eo ON ouo.id = eo.office_unit_organogram_id
WHERE ouo.isDeleted = 0
    AND ouo.job_grade_type_cat = 'any'
    AND ouo.office_unit_id = 'any'
    AND ouo.id = 'any'
    AND eo.office_unit_organogram_id = NULL -- IS NULL gets exception in PB. change in servlet
   OR  eo.isDeleted = 0
    AND eo.status = 'any'
    AND eo.joining_date >= 'any'
    AND eo.joining_date <= 'any'
    AND eo.last_office_date >= 'any'
    AND eo.last_office_date <= 'any'