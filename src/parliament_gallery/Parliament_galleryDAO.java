package parliament_gallery;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import parliament_session.Parliament_sessionDTO;
import pb.OptionDTO;
import pb.Utils;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;
import workflow.WorkflowController;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"rawtypes","unused","Duplicates"})
public class Parliament_galleryDAO extends NavigationService4 {

    private static final Logger logger = Logger.getLogger(Parliament_galleryDAO.class);
    
    public Parliament_galleryDAO(String tableName) {
        super(tableName);
        joinSQL = "";
        commonMaps = new Parliament_galleryMAPS(tableName);
        columnNames = new String[]
                {
                        "ID",
                        "gallery_no",
                        "name_eng",
                        "name_bng",
                        "total_seat",
                        "location_eng",
                        "location_bng",
                        "isActive",
                        "inserted_by_user_id",
                        "inserted_by_organogram_id",
                        "insertion_date",
                        "last_modifier_user",
                        "search_column",
                        "isDeleted",
                        "lastModificationTime"
                };
    }

    public Parliament_galleryDAO() {
        this("parliament_gallery");
    }

    public void setSearchColumn(Parliament_galleryDTO parliament_galleryDTO) {
        parliament_galleryDTO.searchColumn = "";
        parliament_galleryDTO.searchColumn += parliament_galleryDTO.nameEng + " ";
        parliament_galleryDTO.searchColumn += parliament_galleryDTO.nameBng + " ";

    }

    public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException {
        Parliament_galleryDTO parliament_galleryDTO = (Parliament_galleryDTO) commonDTO;
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(parliament_galleryDTO);
        if (isInsert) {
            ps.setObject(++index, parliament_galleryDTO.iD);
        }
        ps.setObject(++index, parliament_galleryDTO.galleryNo);
        ps.setObject(++index, parliament_galleryDTO.nameEng);
        ps.setObject(++index, parliament_galleryDTO.nameBng);
        ps.setObject(++index, parliament_galleryDTO.totalSeat);
        ps.setObject(++index, parliament_galleryDTO.locationEng);
        ps.setObject(++index, parliament_galleryDTO.locationBng);
        ps.setObject(++index, parliament_galleryDTO.isActive);
        ps.setObject(++index, parliament_galleryDTO.insertedByUserId);
        ps.setObject(++index, parliament_galleryDTO.insertedByOrganogramId);
        ps.setObject(++index, parliament_galleryDTO.insertionDate);
        ps.setObject(++index, parliament_galleryDTO.lastModifierUser);
        ps.setObject(++index, parliament_galleryDTO.searchColumn);
        if (isInsert) {
            ps.setObject(++index, 0);
            ps.setObject(++index, lastModificationTime);
        }
    }

    public Parliament_galleryDTO buildParliamentGalleryDTO(ResultSet rs) {
        try {
            Parliament_galleryDTO dto = new Parliament_galleryDTO();
            dto.iD = rs.getLong("ID");
            dto.galleryNo = rs.getInt("gallery_no");
            dto.nameEng = rs.getString("name_eng");
            dto.nameBng = rs.getString("name_bng");
            dto.totalSeat = rs.getInt("total_seat");
            dto.locationEng = rs.getString("location_eng");
            dto.locationBng = rs.getString("location_bng");
            dto.isActive = rs.getInt("isActive");
            dto.insertedByUserId = rs.getLong("inserted_by_user_id");
            dto.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
            dto.insertionDate = rs.getLong("insertion_date");
            dto.lastModifierUser = rs.getString("last_modifier_user");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.searchColumn = rs.getString("search_column");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public Parliament_galleryDTO getDTOByID(long ID) {
        return ConnectionAndStatementUtil.getT(String.format("SELECT * FROM %s WHERE ID = %d", tableName, ID),
                this::buildParliamentGalleryDTO);
    }

    public List<Parliament_galleryDTO> getDTOs(List<Long> recordIDs) {
        if (recordIDs.isEmpty()) {
            return new ArrayList<>();
        }
        String sql = recordIDs.stream()
                .map(String::valueOf)
                .collect(Collectors.joining(",",
                        "SELECT * FROM %s WHERE ID IN ( ",
                        " ) order by lastModificationTime desc"));
        sql = String.format(sql, tableName);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildParliamentGalleryDTO);
    }

    //add repository
    public List<Parliament_galleryDTO> getAllParliament_gallery(boolean isFirstReload) {
        
        String sql = "SELECT * FROM parliament_gallery WHERE "+
                (isFirstReload?" isDeleted =  0":" lastModificationTime >= " + RepositoryManager.lastModifyTime)
                + " order by lastModificationTime desc";
                
        return ConnectionAndStatementUtil.getListOfT(sql,this::buildParliamentGalleryDTO);
    }

    public List<Parliament_galleryDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }

    public List<Parliament_galleryDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
                                               String filter, boolean tableHasJobCat) {
        String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
        return ConnectionAndStatementUtil.getListOfT(sql,this::buildParliamentGalleryDTO);
    }

    public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                          UserDTO userDTO, String filter, boolean tableHasJobCat) {
        boolean viewAll = false;

        if (p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null) {
            logger.debug("ViewAll = " + p_searchCriteria.get("ViewAll"));
        } else {
            logger.debug("ViewAll is null ");
        }

        String sql = "SELECT ";
        if (category == GETIDS) {
            sql += " " + tableName + ".ID as ID ";
        } else if (category == GETCOUNT) {
            sql += " count( " + tableName + ".ID) as countID ";
        } else if (category == GETDTOS) {
            sql += " distinct " + tableName + ".* ";
        }
        sql += "FROM " + tableName + " ";
        sql += joinSQL;


        String AnyfieldSql = "";
        StringBuilder AllFieldSql = new StringBuilder();

        if (p_searchCriteria != null) {


            Enumeration names = p_searchCriteria.keys();
            String str, value;

            AnyfieldSql = "(";

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                AnyfieldSql += tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
            }
            AnyfieldSql += ")";
            logger.debug("AnyfieldSql = " + AnyfieldSql);

            AllFieldSql = new StringBuilder("(");
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                logger.debug(str + ": " + value);
                if (value != null && !value.equalsIgnoreCase("") && (
                        str.equals("name_eng")
                                || str.equals("name_bng")
                                || str.equals("insertion_date_start")
                                || str.equals("insertion_date_end")
                )

                ) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql.append(" AND  ");
                    }

                    switch (str) {
                        case "name_eng":
                            AllFieldSql.append(tableName).append(".name_eng like '%").append(p_searchCriteria.get(str)).append("%'");
                            i++;
                            break;
                        case "name_bng":
                            AllFieldSql.append(tableName).append(".name_bng like '%").append(p_searchCriteria.get(str)).append("%'");
                            i++;
                            break;
                        case "insertion_date_start":
                            AllFieldSql.append(tableName).append(".insertion_date >= ").append(p_searchCriteria.get(str));
                            i++;
                            break;
                        case "insertion_date_end":
                            AllFieldSql.append(tableName).append(".insertion_date <= ").append(p_searchCriteria.get(str));
                            i++;
                            break;
                    }
                }
            }

            AllFieldSql.append(")");
            logger.debug("AllFieldSql = " + AllFieldSql);

        }

        sql += " WHERE ";

        sql += " (" + tableName + ".isDeleted = 0 ";
        sql += ")";


        if (!filter.equalsIgnoreCase("")) {
            sql += " and " + filter + " ";
        }

        if (!AnyfieldSql.equals("()") && !AnyfieldSql.equals("")) {
            sql += " AND " + AnyfieldSql;

        }
        if (!AllFieldSql.toString().equals("()") && !AllFieldSql.toString().equals("")) {
            sql += " AND " + AllFieldSql;
        }

        sql += " order by " + tableName + ".lastModificationTime desc ";

        printSql(sql);

        if (limit >= 0) {
            sql += " limit " + limit;
        }
        if (offset >= 0) {
            sql += " offset " + offset;
        }

        logger.debug("-------------- sql = " + sql);

        return sql;
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
                                           boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat) {
        return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
    }


    private Gate_pass_galleryDTO buildGatePassGalleryDTO(ResultSet rs) {
        try {
            Gate_pass_galleryDTO gatePassGalleryDTO = new Gate_pass_galleryDTO();
            gatePassGalleryDTO.id = rs.getLong("ID");
            gatePassGalleryDTO.parliamentGalleryId = rs.getLong("parliament_gallery_id");
            gatePassGalleryDTO.parliamentSessionId = rs.getLong("parliament_session_id");
            gatePassGalleryDTO.availableSeat = rs.getInt("available_seat");
            gatePassGalleryDTO.totalSeat = rs.getInt("total_seat");
            gatePassGalleryDTO.individualAssemblyDate = rs.getLong("individual_assembly_date");
            gatePassGalleryDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
            gatePassGalleryDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
            gatePassGalleryDTO.insertionDate = rs.getLong("insertion_date");
            gatePassGalleryDTO.lastModifierUser = rs.getString("last_modifier_user");
            gatePassGalleryDTO.isDeleted = rs.getInt("isDeleted");
            gatePassGalleryDTO.lastModificationTime = rs.getLong("lastModificationTime");

            return gatePassGalleryDTO;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<Gate_pass_galleryDTO> getGatePassGalleryTOByVisitingDate(long visitingDate) {
        String sql = String.format("select * from gate_pass_gallery where individual_assembly_date=%d and isDeleted=0", visitingDate);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildGatePassGalleryDTO);
    }

    private void addGatePassGalleryDTOs(List<Gate_pass_galleryDTO> dtos) {
        if (dtos == null || dtos.size() == 0) {
            return;
        }

        ConnectionAndStatementUtil.getWriteStatement(stmt -> {
            for (Gate_pass_galleryDTO dto : dtos) {
                String sql = "insert into gate_pass_gallery (parliament_gallery_id, parliament_session_id, individual_assembly_date, total_seat, available_seat, inserted_by_user_id, " +
                        "inserted_by_organogram_id, insertion_date, last_modifier_user, isDeleted, lastModificationTime) values (%d,%d,%d,%d,%d,%d,%d,%d,'%s',%d,%d)";

                sql = String.format(sql, dto.parliamentGalleryId, dto.parliamentSessionId, dto.individualAssemblyDate, dto.totalSeat, dto.availableSeat, dto.insertedByUserId,
                        dto.insertedByOrganogramId, dto.insertionDate, dto.lastModifierUser, dto.isDeleted, dto.lastModificationTime);
                logger.debug(sql);
                try {
                    stmt.executeUpdate(sql);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private List<Gate_pass_galleryDTO> buildGalleryDTOsFromParliamentSession(Parliament_sessionDTO parliamentSessionDTO, List<Parliament_galleryDTO> parliament_galleryDTOS, UserDTO userDTO) {
        List<Gate_pass_galleryDTO> dtos = new ArrayList<>();
        Date startDate, endDate;
        Calendar start, end;

        startDate = new Date(parliamentSessionDTO.startDate);
        endDate = new Date(parliamentSessionDTO.endDate);
        start = Calendar.getInstance();
        end = Calendar.getInstance();
        start.setTime(startDate);
        start.set(Calendar.HOUR_OF_DAY, 0);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);
        start.set(Calendar.MILLISECOND, 0);
        end.setTime(endDate);
        end.set(Calendar.HOUR_OF_DAY, 0);
        end.set(Calendar.MINUTE, 0);
        end.set(Calendar.SECOND, 0);
        end.set(Calendar.MILLISECOND, 0);

        for (Date date = start.getTime(); !start.after(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
            for (Parliament_galleryDTO parliamentGalleryDTO : parliament_galleryDTOS) {
                Gate_pass_galleryDTO dto = new Gate_pass_galleryDTO();
                dto.parliamentSessionId = parliamentSessionDTO.iD;
                dto.parliamentGalleryId = parliamentGalleryDTO.iD;
                dto.individualAssemblyDate = date.getTime();
                dto.totalSeat = parliamentGalleryDTO.totalSeat;
                dto.availableSeat = dto.totalSeat;
                dto.lastModifierUser = String.valueOf(userDTO.ID);
                dto.insertedByUserId = userDTO.ID;
                try {
                    dto.insertedByOrganogramId = WorkflowController.getOrganogramIDFromUserID(userDTO.ID);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dto.lastModificationTime = System.currentTimeMillis();
                dto.insertionDate = Calendar.getInstance().getTimeInMillis();


                dtos.add(dto);
            }
        }

        return dtos;
    }


    public void populateGatePassGalleryByParliamentSessionDTO(Parliament_sessionDTO parliament_sessionDTO, UserDTO userDTO) {
        List<Parliament_galleryDTO> parliament_galleryDTOS = Parliament_galleryRepository.getInstance().getParliament_galleryList();
        List<Gate_pass_galleryDTO> gate_pass_galleryDTOS = buildGalleryDTOsFromParliamentSession(parliament_sessionDTO, parliament_galleryDTOS, userDTO);
        addGatePassGalleryDTOs(gate_pass_galleryDTOS);
    }

    public GatePassGalleryModel getGatePassGalleryModelByVisitingDate(long visitingDate, String language) {
        GatePassGalleryModel model = new GatePassGalleryModel();
        List<OptionDTO> optionDTOList = new ArrayList<>();
        List<Long> galleryIds = new ArrayList<>();
        List<Integer> totalSeatAllGallery = new ArrayList<>();
        List<Integer> availableSeatAllGallery = new ArrayList<>();

        List<Gate_pass_galleryDTO> dtos = getGatePassGalleryTOByVisitingDate(visitingDate);

        for (Gate_pass_galleryDTO dto : dtos) {
            Parliament_galleryDTO parliamentGalleryDTO = Parliament_galleryRepository.getInstance().getParliament_galleryDTOByID(dto.parliamentGalleryId);
            OptionDTO optionDTO = new OptionDTO(parliamentGalleryDTO.nameEng, parliamentGalleryDTO.nameBng, String.valueOf(dto.id));
            optionDTOList.add(optionDTO);
            galleryIds.add(dto.id);
            totalSeatAllGallery.add(dto.totalSeat);
            availableSeatAllGallery.add(dto.availableSeat);
        }

        model.options = Utils.buildOptions(optionDTOList, language, null);
        model.galleryIds = galleryIds.stream().mapToLong(l -> l).toArray();
        model.totalSeatAllGallery = totalSeatAllGallery.stream().mapToInt(i -> i).toArray();
        model.availableSeatAllGallery = availableSeatAllGallery.stream().mapToInt(i -> i).toArray();
        return model;
    }

    public void calculateAvailableSeatByGatePassGalleryId(long gatePassGalleryId, String method) {
        String sql = method.equalsIgnoreCase("add") ?
                String.format("UPDATE gate_pass_gallery SET available_seat = available_seat + 1 WHERE ID = %d", gatePassGalleryId) :
                String.format("UPDATE gate_pass_gallery SET available_seat = available_seat - 1 WHERE ID = %d", gatePassGalleryId);
        logger.debug("sql : " + sql);
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            try {
                st.executeUpdate(sql);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public String getGalleryNameFromGatePassGalleryId(long gatePassGalleryId, String language) {
        String sql = "select parliament_gallery_id from gate_pass_gallery where ID = %d";
        sql = String.format(sql, gatePassGalleryId);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                long parliamentGalleryId = rs.getLong("parliament_gallery_id");
                Parliament_galleryDTO parliamentGalleryDTO = Parliament_galleryRepository.getInstance().getParliament_galleryDTOByID(parliamentGalleryId);
                return language.equalsIgnoreCase("English") ? parliamentGalleryDTO.nameEng : parliamentGalleryDTO.nameBng;
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        });
    }
}