package parliament_gallery;
import java.util.*; 
import util.*;


public class Parliament_galleryMAPS extends CommonMaps
{	
	public Parliament_galleryMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("galleryNo".toLowerCase(), "galleryNo".toLowerCase());
		java_DTO_map.put("nameEng".toLowerCase(), "nameEng".toLowerCase());
		java_DTO_map.put("nameBng".toLowerCase(), "nameBng".toLowerCase());
		java_DTO_map.put("totalSeat".toLowerCase(), "totalSeat".toLowerCase());
		java_DTO_map.put("locationEng".toLowerCase(), "locationEng".toLowerCase());
		java_DTO_map.put("locationBng".toLowerCase(), "locationBng".toLowerCase());
		java_DTO_map.put("isActive".toLowerCase(), "isActive".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("lastModifierUser".toLowerCase(), "lastModifierUser".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("name_eng".toLowerCase(), "nameEng".toLowerCase());
		java_SQL_map.put("name_bng".toLowerCase(), "nameBng".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Gallery No".toLowerCase(), "galleryNo".toLowerCase());
		java_Text_map.put("Name Engish".toLowerCase(), "nameEng".toLowerCase());
		java_Text_map.put("Name Bangla".toLowerCase(), "nameBng".toLowerCase());
		java_Text_map.put("Total Seat".toLowerCase(), "totalSeat".toLowerCase());
		java_Text_map.put("Location English".toLowerCase(), "locationEng".toLowerCase());
		java_Text_map.put("Location Bangla".toLowerCase(), "locationBng".toLowerCase());
		java_Text_map.put("IsActive".toLowerCase(), "isActive".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Last Modifier User".toLowerCase(), "lastModifierUser".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}