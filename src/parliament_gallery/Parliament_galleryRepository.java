package parliament_gallery;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Parliament_galleryRepository implements Repository {
	Parliament_galleryDAO parliament_galleryDAO = new Parliament_galleryDAO();
	
	public void setDAO(Parliament_galleryDAO parliament_galleryDAO)
	{
		this.parliament_galleryDAO = parliament_galleryDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Parliament_galleryRepository.class);
	Map<Long, Parliament_galleryDTO>mapOfParliament_galleryDTOToiD;
	Map<Integer, Set<Parliament_galleryDTO> >mapOfParliament_galleryDTOTogalleryNo;
	Map<String, Set<Parliament_galleryDTO> >mapOfParliament_galleryDTOTonameEng;
	Map<String, Set<Parliament_galleryDTO> >mapOfParliament_galleryDTOTonameBng;
	Map<Integer, Set<Parliament_galleryDTO> >mapOfParliament_galleryDTOTototalSeat;
	Map<String, Set<Parliament_galleryDTO> >mapOfParliament_galleryDTOTolocationEng;
	Map<String, Set<Parliament_galleryDTO> >mapOfParliament_galleryDTOTolocationBng;
	Map<Integer, Set<Parliament_galleryDTO> >mapOfParliament_galleryDTOToisActive;
	Map<Long, Set<Parliament_galleryDTO> >mapOfParliament_galleryDTOToinsertedByUserId;
	Map<Long, Set<Parliament_galleryDTO> >mapOfParliament_galleryDTOToinsertedByOrganogramId;
	Map<Long, Set<Parliament_galleryDTO> >mapOfParliament_galleryDTOToinsertionDate;
	Map<String, Set<Parliament_galleryDTO> >mapOfParliament_galleryDTOTolastModifierUser;
	Map<String, Set<Parliament_galleryDTO> >mapOfParliament_galleryDTOTosearchColumn;
	Map<Long, Set<Parliament_galleryDTO> >mapOfParliament_galleryDTOTolastModificationTime;


	static Parliament_galleryRepository instance = null;  
	private Parliament_galleryRepository(){
		mapOfParliament_galleryDTOToiD = new ConcurrentHashMap<>();
		mapOfParliament_galleryDTOTogalleryNo = new ConcurrentHashMap<>();
		mapOfParliament_galleryDTOTonameEng = new ConcurrentHashMap<>();
		mapOfParliament_galleryDTOTonameBng = new ConcurrentHashMap<>();
		mapOfParliament_galleryDTOTototalSeat = new ConcurrentHashMap<>();
		mapOfParliament_galleryDTOTolocationEng = new ConcurrentHashMap<>();
		mapOfParliament_galleryDTOTolocationBng = new ConcurrentHashMap<>();
		mapOfParliament_galleryDTOToisActive = new ConcurrentHashMap<>();
		mapOfParliament_galleryDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfParliament_galleryDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfParliament_galleryDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfParliament_galleryDTOTolastModifierUser = new ConcurrentHashMap<>();
		mapOfParliament_galleryDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfParliament_galleryDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Parliament_galleryRepository getInstance(){
		if (instance == null){
			instance = new Parliament_galleryRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(parliament_galleryDAO == null)
		{
			return;
		}
		try {
			List<Parliament_galleryDTO> parliament_galleryDTOs = parliament_galleryDAO.getAllParliament_gallery(reloadAll);
			for(Parliament_galleryDTO parliament_galleryDTO : parliament_galleryDTOs) {
				Parliament_galleryDTO oldParliament_galleryDTO = getParliament_galleryDTOByID(parliament_galleryDTO.iD);
				if( oldParliament_galleryDTO != null ) {
					mapOfParliament_galleryDTOToiD.remove(oldParliament_galleryDTO.iD);
				
					if(mapOfParliament_galleryDTOTogalleryNo.containsKey(oldParliament_galleryDTO.galleryNo)) {
						mapOfParliament_galleryDTOTogalleryNo.get(oldParliament_galleryDTO.galleryNo).remove(oldParliament_galleryDTO);
					}
					if(mapOfParliament_galleryDTOTogalleryNo.get(oldParliament_galleryDTO.galleryNo).isEmpty()) {
						mapOfParliament_galleryDTOTogalleryNo.remove(oldParliament_galleryDTO.galleryNo);
					}
					
					if(mapOfParliament_galleryDTOTonameEng.containsKey(oldParliament_galleryDTO.nameEng)) {
						mapOfParliament_galleryDTOTonameEng.get(oldParliament_galleryDTO.nameEng).remove(oldParliament_galleryDTO);
					}
					if(mapOfParliament_galleryDTOTonameEng.get(oldParliament_galleryDTO.nameEng).isEmpty()) {
						mapOfParliament_galleryDTOTonameEng.remove(oldParliament_galleryDTO.nameEng);
					}
					
					if(mapOfParliament_galleryDTOTonameBng.containsKey(oldParliament_galleryDTO.nameBng)) {
						mapOfParliament_galleryDTOTonameBng.get(oldParliament_galleryDTO.nameBng).remove(oldParliament_galleryDTO);
					}
					if(mapOfParliament_galleryDTOTonameBng.get(oldParliament_galleryDTO.nameBng).isEmpty()) {
						mapOfParliament_galleryDTOTonameBng.remove(oldParliament_galleryDTO.nameBng);
					}
					
					if(mapOfParliament_galleryDTOTototalSeat.containsKey(oldParliament_galleryDTO.totalSeat)) {
						mapOfParliament_galleryDTOTototalSeat.get(oldParliament_galleryDTO.totalSeat).remove(oldParliament_galleryDTO);
					}
					if(mapOfParliament_galleryDTOTototalSeat.get(oldParliament_galleryDTO.totalSeat).isEmpty()) {
						mapOfParliament_galleryDTOTototalSeat.remove(oldParliament_galleryDTO.totalSeat);
					}
					
					if(mapOfParliament_galleryDTOTolocationEng.containsKey(oldParliament_galleryDTO.locationEng)) {
						mapOfParliament_galleryDTOTolocationEng.get(oldParliament_galleryDTO.locationEng).remove(oldParliament_galleryDTO);
					}
					if(mapOfParliament_galleryDTOTolocationEng.get(oldParliament_galleryDTO.locationEng).isEmpty()) {
						mapOfParliament_galleryDTOTolocationEng.remove(oldParliament_galleryDTO.locationEng);
					}
					
					if(mapOfParliament_galleryDTOTolocationBng.containsKey(oldParliament_galleryDTO.locationBng)) {
						mapOfParliament_galleryDTOTolocationBng.get(oldParliament_galleryDTO.locationBng).remove(oldParliament_galleryDTO);
					}
					if(mapOfParliament_galleryDTOTolocationBng.get(oldParliament_galleryDTO.locationBng).isEmpty()) {
						mapOfParliament_galleryDTOTolocationBng.remove(oldParliament_galleryDTO.locationBng);
					}
					
					if(mapOfParliament_galleryDTOToisActive.containsKey(oldParliament_galleryDTO.isActive)) {
						mapOfParliament_galleryDTOToisActive.get(oldParliament_galleryDTO.isActive).remove(oldParliament_galleryDTO);
					}
					if(mapOfParliament_galleryDTOToisActive.get(oldParliament_galleryDTO.isActive).isEmpty()) {
						mapOfParliament_galleryDTOToisActive.remove(oldParliament_galleryDTO.isActive);
					}
					
					if(mapOfParliament_galleryDTOToinsertedByUserId.containsKey(oldParliament_galleryDTO.insertedByUserId)) {
						mapOfParliament_galleryDTOToinsertedByUserId.get(oldParliament_galleryDTO.insertedByUserId).remove(oldParliament_galleryDTO);
					}
					if(mapOfParliament_galleryDTOToinsertedByUserId.get(oldParliament_galleryDTO.insertedByUserId).isEmpty()) {
						mapOfParliament_galleryDTOToinsertedByUserId.remove(oldParliament_galleryDTO.insertedByUserId);
					}
					
					if(mapOfParliament_galleryDTOToinsertedByOrganogramId.containsKey(oldParliament_galleryDTO.insertedByOrganogramId)) {
						mapOfParliament_galleryDTOToinsertedByOrganogramId.get(oldParliament_galleryDTO.insertedByOrganogramId).remove(oldParliament_galleryDTO);
					}
					if(mapOfParliament_galleryDTOToinsertedByOrganogramId.get(oldParliament_galleryDTO.insertedByOrganogramId).isEmpty()) {
						mapOfParliament_galleryDTOToinsertedByOrganogramId.remove(oldParliament_galleryDTO.insertedByOrganogramId);
					}
					
					if(mapOfParliament_galleryDTOToinsertionDate.containsKey(oldParliament_galleryDTO.insertionDate)) {
						mapOfParliament_galleryDTOToinsertionDate.get(oldParliament_galleryDTO.insertionDate).remove(oldParliament_galleryDTO);
					}
					if(mapOfParliament_galleryDTOToinsertionDate.get(oldParliament_galleryDTO.insertionDate).isEmpty()) {
						mapOfParliament_galleryDTOToinsertionDate.remove(oldParliament_galleryDTO.insertionDate);
					}
					
					if(mapOfParliament_galleryDTOTolastModifierUser.containsKey(oldParliament_galleryDTO.lastModifierUser)) {
						mapOfParliament_galleryDTOTolastModifierUser.get(oldParliament_galleryDTO.lastModifierUser).remove(oldParliament_galleryDTO);
					}
					if(mapOfParliament_galleryDTOTolastModifierUser.get(oldParliament_galleryDTO.lastModifierUser).isEmpty()) {
						mapOfParliament_galleryDTOTolastModifierUser.remove(oldParliament_galleryDTO.lastModifierUser);
					}
					
					if(mapOfParliament_galleryDTOTosearchColumn.containsKey(oldParliament_galleryDTO.searchColumn)) {
						mapOfParliament_galleryDTOTosearchColumn.get(oldParliament_galleryDTO.searchColumn).remove(oldParliament_galleryDTO);
					}
					if(mapOfParliament_galleryDTOTosearchColumn.get(oldParliament_galleryDTO.searchColumn).isEmpty()) {
						mapOfParliament_galleryDTOTosearchColumn.remove(oldParliament_galleryDTO.searchColumn);
					}
					
					if(mapOfParliament_galleryDTOTolastModificationTime.containsKey(oldParliament_galleryDTO.lastModificationTime)) {
						mapOfParliament_galleryDTOTolastModificationTime.get(oldParliament_galleryDTO.lastModificationTime).remove(oldParliament_galleryDTO);
					}
					if(mapOfParliament_galleryDTOTolastModificationTime.get(oldParliament_galleryDTO.lastModificationTime).isEmpty()) {
						mapOfParliament_galleryDTOTolastModificationTime.remove(oldParliament_galleryDTO.lastModificationTime);
					}
					
					
				}
				if(parliament_galleryDTO.isDeleted == 0) 
				{
					
					mapOfParliament_galleryDTOToiD.put(parliament_galleryDTO.iD, parliament_galleryDTO);
				
					if( ! mapOfParliament_galleryDTOTogalleryNo.containsKey(parliament_galleryDTO.galleryNo)) {
						mapOfParliament_galleryDTOTogalleryNo.put(parliament_galleryDTO.galleryNo, new HashSet<>());
					}
					mapOfParliament_galleryDTOTogalleryNo.get(parliament_galleryDTO.galleryNo).add(parliament_galleryDTO);
					
					if( ! mapOfParliament_galleryDTOTonameEng.containsKey(parliament_galleryDTO.nameEng)) {
						mapOfParliament_galleryDTOTonameEng.put(parliament_galleryDTO.nameEng, new HashSet<>());
					}
					mapOfParliament_galleryDTOTonameEng.get(parliament_galleryDTO.nameEng).add(parliament_galleryDTO);
					
					if( ! mapOfParliament_galleryDTOTonameBng.containsKey(parliament_galleryDTO.nameBng)) {
						mapOfParliament_galleryDTOTonameBng.put(parliament_galleryDTO.nameBng, new HashSet<>());
					}
					mapOfParliament_galleryDTOTonameBng.get(parliament_galleryDTO.nameBng).add(parliament_galleryDTO);
					
					if( ! mapOfParliament_galleryDTOTototalSeat.containsKey(parliament_galleryDTO.totalSeat)) {
						mapOfParliament_galleryDTOTototalSeat.put(parliament_galleryDTO.totalSeat, new HashSet<>());
					}
					mapOfParliament_galleryDTOTototalSeat.get(parliament_galleryDTO.totalSeat).add(parliament_galleryDTO);
					
					if( ! mapOfParliament_galleryDTOTolocationEng.containsKey(parliament_galleryDTO.locationEng)) {
						mapOfParliament_galleryDTOTolocationEng.put(parliament_galleryDTO.locationEng, new HashSet<>());
					}
					mapOfParliament_galleryDTOTolocationEng.get(parliament_galleryDTO.locationEng).add(parliament_galleryDTO);
					
					if( ! mapOfParliament_galleryDTOTolocationBng.containsKey(parliament_galleryDTO.locationBng)) {
						mapOfParliament_galleryDTOTolocationBng.put(parliament_galleryDTO.locationBng, new HashSet<>());
					}
					mapOfParliament_galleryDTOTolocationBng.get(parliament_galleryDTO.locationBng).add(parliament_galleryDTO);
					
					if( ! mapOfParliament_galleryDTOToisActive.containsKey(parliament_galleryDTO.isActive)) {
						mapOfParliament_galleryDTOToisActive.put(parliament_galleryDTO.isActive, new HashSet<>());
					}
					mapOfParliament_galleryDTOToisActive.get(parliament_galleryDTO.isActive).add(parliament_galleryDTO);
					
					if( ! mapOfParliament_galleryDTOToinsertedByUserId.containsKey(parliament_galleryDTO.insertedByUserId)) {
						mapOfParliament_galleryDTOToinsertedByUserId.put(parliament_galleryDTO.insertedByUserId, new HashSet<>());
					}
					mapOfParliament_galleryDTOToinsertedByUserId.get(parliament_galleryDTO.insertedByUserId).add(parliament_galleryDTO);
					
					if( ! mapOfParliament_galleryDTOToinsertedByOrganogramId.containsKey(parliament_galleryDTO.insertedByOrganogramId)) {
						mapOfParliament_galleryDTOToinsertedByOrganogramId.put(parliament_galleryDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfParliament_galleryDTOToinsertedByOrganogramId.get(parliament_galleryDTO.insertedByOrganogramId).add(parliament_galleryDTO);
					
					if( ! mapOfParliament_galleryDTOToinsertionDate.containsKey(parliament_galleryDTO.insertionDate)) {
						mapOfParliament_galleryDTOToinsertionDate.put(parliament_galleryDTO.insertionDate, new HashSet<>());
					}
					mapOfParliament_galleryDTOToinsertionDate.get(parliament_galleryDTO.insertionDate).add(parliament_galleryDTO);
					
					if( ! mapOfParliament_galleryDTOTolastModifierUser.containsKey(parliament_galleryDTO.lastModifierUser)) {
						mapOfParliament_galleryDTOTolastModifierUser.put(parliament_galleryDTO.lastModifierUser, new HashSet<>());
					}
					mapOfParliament_galleryDTOTolastModifierUser.get(parliament_galleryDTO.lastModifierUser).add(parliament_galleryDTO);
					
					if( ! mapOfParliament_galleryDTOTosearchColumn.containsKey(parliament_galleryDTO.searchColumn)) {
						mapOfParliament_galleryDTOTosearchColumn.put(parliament_galleryDTO.searchColumn, new HashSet<>());
					}
					mapOfParliament_galleryDTOTosearchColumn.get(parliament_galleryDTO.searchColumn).add(parliament_galleryDTO);
					
					if( ! mapOfParliament_galleryDTOTolastModificationTime.containsKey(parliament_galleryDTO.lastModificationTime)) {
						mapOfParliament_galleryDTOTolastModificationTime.put(parliament_galleryDTO.lastModificationTime, new HashSet<>());
					}
					mapOfParliament_galleryDTOTolastModificationTime.get(parliament_galleryDTO.lastModificationTime).add(parliament_galleryDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Parliament_galleryDTO> getParliament_galleryList() {
		List <Parliament_galleryDTO> parliament_gallerys = new ArrayList<Parliament_galleryDTO>(this.mapOfParliament_galleryDTOToiD.values());
		return parliament_gallerys;
	}
	
	
	public Parliament_galleryDTO getParliament_galleryDTOByID( long ID){
		return mapOfParliament_galleryDTOToiD.get(ID);
	}
	
	
	public List<Parliament_galleryDTO> getParliament_galleryDTOBygallery_no(int gallery_no) {
		return new ArrayList<>( mapOfParliament_galleryDTOTogalleryNo.getOrDefault(gallery_no,new HashSet<>()));
	}
	
	
	public List<Parliament_galleryDTO> getParliament_galleryDTOByname_eng(String name_eng) {
		return new ArrayList<>( mapOfParliament_galleryDTOTonameEng.getOrDefault(name_eng,new HashSet<>()));
	}
	
	
	public List<Parliament_galleryDTO> getParliament_galleryDTOByname_bng(String name_bng) {
		return new ArrayList<>( mapOfParliament_galleryDTOTonameBng.getOrDefault(name_bng,new HashSet<>()));
	}
	
	
	public List<Parliament_galleryDTO> getParliament_galleryDTOBytotal_seat(int total_seat) {
		return new ArrayList<>( mapOfParliament_galleryDTOTototalSeat.getOrDefault(total_seat,new HashSet<>()));
	}
	
	
	public List<Parliament_galleryDTO> getParliament_galleryDTOBylocation_eng(String location_eng) {
		return new ArrayList<>( mapOfParliament_galleryDTOTolocationEng.getOrDefault(location_eng,new HashSet<>()));
	}
	
	
	public List<Parliament_galleryDTO> getParliament_galleryDTOBylocation_bng(String location_bng) {
		return new ArrayList<>( mapOfParliament_galleryDTOTolocationBng.getOrDefault(location_bng,new HashSet<>()));
	}
	
	
	public List<Parliament_galleryDTO> getParliament_galleryDTOByisActive(int isActive) {
		return new ArrayList<>( mapOfParliament_galleryDTOToisActive.getOrDefault(isActive,new HashSet<>()));
	}
	
	
	public List<Parliament_galleryDTO> getParliament_galleryDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfParliament_galleryDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Parliament_galleryDTO> getParliament_galleryDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfParliament_galleryDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Parliament_galleryDTO> getParliament_galleryDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfParliament_galleryDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Parliament_galleryDTO> getParliament_galleryDTOBylast_modifier_user(String last_modifier_user) {
		return new ArrayList<>( mapOfParliament_galleryDTOTolastModifierUser.getOrDefault(last_modifier_user,new HashSet<>()));
	}
	
	
	public List<Parliament_galleryDTO> getParliament_galleryDTOBysearch_column(String search_column) {
		return new ArrayList<>( mapOfParliament_galleryDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
	}
	
	
	public List<Parliament_galleryDTO> getParliament_galleryDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfParliament_galleryDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "parliament_gallery";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


