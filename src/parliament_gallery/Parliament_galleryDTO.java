package parliament_gallery;
import java.util.*; 
import util.*; 


public class Parliament_galleryDTO extends CommonDTO
{

	public int galleryNo = 0;
    public String nameEng = "";
    public String nameBng = "";
	public int totalSeat = 0;
    public String locationEng = "";
    public String locationBng = "";
	public int isActive = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	
	
    @Override
	public String toString() {
            return "$Parliament_galleryDTO[" +
            " iD = " + iD +
            " galleryNo = " + galleryNo +
            " nameEng = " + nameEng +
            " nameBng = " + nameBng +
            " totalSeat = " + totalSeat +
            " locationEng = " + locationEng +
            " locationBng = " + locationBng +
            " isActive = " + isActive +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " searchColumn = " + searchColumn +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}