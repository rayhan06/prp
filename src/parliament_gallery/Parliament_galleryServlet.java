package parliament_gallery;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import gate_pass_visitor.Gate_pass_visitorDTO;
import org.apache.log4j.Logger;

import login.LoginDTO;
import org.json.JSONObject;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Parliament_galleryServlet
 */
@WebServlet("/Parliament_galleryServlet")
@MultipartConfig
public class Parliament_galleryServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Parliament_galleryServlet.class);

    String tableName = "parliament_gallery";

	Parliament_galleryDAO parliament_galleryDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Parliament_galleryServlet() 
	{
        super();
    	try
    	{
			parliament_galleryDAO = new Parliament_galleryDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(parliament_galleryDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_GALLERY_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_GALLERY_UPDATE))
				{
					getParliament_gallery(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_GALLERY_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchParliament_gallery(request, response, isPermanentTable, filter);
						}
						else
						{
							searchParliament_gallery(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchParliament_gallery(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_GALLERY_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("getGatePassGalleryModel"))
			{
				String visitingDate = request.getParameter("visitingDate");
				String language = request.getParameter("language");

				Parliament_galleryDAO parliament_galleryDAO = new Parliament_galleryDAO();

				GatePassGalleryModel model = parliament_galleryDAO.getGatePassGalleryModelByVisitingDate(Long.parseLong(visitingDate), language);

				String jsonInString = new Gson().toJson(model);
				JSONObject mJSONObject = new JSONObject(jsonInString);

				PrintWriter out = response.getWriter();
				out.println(mJSONObject);
				out.close();
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_GALLERY_ADD))
				{
					System.out.println("going to  addParliament_gallery ");
					addParliament_gallery(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addParliament_gallery ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_GALLERY_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addParliament_gallery ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_GALLERY_UPDATE))
				{					
					addParliament_gallery(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_GALLERY_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_GALLERY_SEARCH))
				{
					searchParliament_gallery(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Parliament_galleryDTO parliament_galleryDTO = parliament_galleryDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(parliament_galleryDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addParliament_gallery(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addParliament_gallery");
			String path = getServletContext().getRealPath("/img2/");
			Parliament_galleryDTO parliament_galleryDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				parliament_galleryDTO = new Parliament_galleryDTO();
			}
			else
			{
				parliament_galleryDTO = parliament_galleryDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			}
			
			String Value = "";

			Value = request.getParameter("galleryNo");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("galleryNo = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				parliament_galleryDTO.galleryNo = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nameEng");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEng = " + Value);
			if(Value != null)
			{
				parliament_galleryDTO.nameEng = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nameBng");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameBng = " + Value);
			if(Value != null)
			{
				parliament_galleryDTO.nameBng = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("totalSeat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("totalSeat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				parliament_galleryDTO.totalSeat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("locationEng");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("locationEng = " + Value);
			if(Value != null)
			{
				parliament_galleryDTO.locationEng = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("locationBng");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("locationBng = " + Value);
			if(Value != null)
			{
				parliament_galleryDTO.locationBng = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("isActive");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("isActive = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				parliament_galleryDTO.isActive = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				parliament_galleryDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				parliament_galleryDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				parliament_galleryDTO.insertionDate = c.getTimeInMillis();
			}			


			parliament_galleryDTO.lastModifierUser = userDTO.userName;


			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				parliament_galleryDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addParliament_gallery dto = " + parliament_galleryDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				parliament_galleryDAO.setIsDeleted(parliament_galleryDTO.iD, CommonDTO.OUTDATED);
				returnedID = parliament_galleryDAO.add(parliament_galleryDTO);
				parliament_galleryDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = parliament_galleryDAO.manageWriteOperations(parliament_galleryDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = parliament_galleryDAO.manageWriteOperations(parliament_galleryDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getParliament_gallery(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Parliament_galleryServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(parliament_galleryDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void getParliament_gallery(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getParliament_gallery");
		Parliament_galleryDTO parliament_galleryDTO = null;
		try 
		{
			parliament_galleryDTO = parliament_galleryDAO.getDTOByID(id);
			request.setAttribute("ID", parliament_galleryDTO.iD);
			request.setAttribute("parliament_galleryDTO",parliament_galleryDTO);
			request.setAttribute("parliament_galleryDAO",parliament_galleryDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "parliament_gallery/parliament_galleryInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "parliament_gallery/parliament_gallerySearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "parliament_gallery/parliament_galleryEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "parliament_gallery/parliament_galleryEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getParliament_gallery(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getParliament_gallery(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchParliament_gallery(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchParliament_gallery 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_PARLIAMENT_GALLERY,
			request,
			parliament_galleryDAO,
			SessionConstants.VIEW_PARLIAMENT_GALLERY,
			SessionConstants.SEARCH_PARLIAMENT_GALLERY,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("parliament_galleryDAO",parliament_galleryDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to parliament_gallery/parliament_galleryApproval.jsp");
	        	rd = request.getRequestDispatcher("parliament_gallery/parliament_galleryApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to parliament_gallery/parliament_galleryApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("parliament_gallery/parliament_galleryApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to parliament_gallery/parliament_gallerySearch.jsp");
	        	rd = request.getRequestDispatcher("parliament_gallery/parliament_gallerySearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to parliament_gallery/parliament_gallerySearchForm.jsp");
	        	rd = request.getRequestDispatcher("parliament_gallery/parliament_gallerySearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

