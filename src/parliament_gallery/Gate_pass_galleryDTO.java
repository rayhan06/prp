package parliament_gallery;

public class Gate_pass_galleryDTO {
    public long id = 0;
    public long parliamentGalleryId = 0;
    public long parliamentSessionId = 0;
    public int totalSeat = 0;
    public int availableSeat = 0;
    public long individualAssemblyDate = 0;
    public long insertionDate = 0;
    public long insertedByUserId = 0;
    public long insertedByOrganogramId = 0;
    public String lastModifierUser = "";
    public int isDeleted = 0;
    public long lastModificationTime = 0;


    @Override
    public String toString() {
        return "$Gate_pass_additional_itemDTO[" +
                " id = " + id +
                " parliamentGalleryId = " + parliamentGalleryId +
                " parliamentSessionId = " + parliamentSessionId +
                " insertionDate = " + insertionDate +
                " totalSeat = " + totalSeat +
                " insertedByUserId = " + insertedByUserId +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }
}
