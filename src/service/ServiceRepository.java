package service;

import common.NameRepository;

public class ServiceRepository extends NameRepository {

    private ServiceRepository() {
        super(ServiceDAO.getInstance(), ServiceRepository.class);
    }

    private static class LazyRepositoryLoader {
        static ServiceRepository INSTANCE = new ServiceRepository();
    }

    public static ServiceRepository getInstance() {
        return ServiceRepository.LazyRepositoryLoader.INSTANCE;
    }
}
