package service;

import common.NameDao;

public class ServiceDAO extends NameDao {

    private ServiceDAO() {
        super("service");
    }

    private static class LazyLoader{
        static final ServiceDAO INSTANCE = new ServiceDAO();
    }

    public static ServiceDAO getInstance(){
        return LazyLoader.INSTANCE;
    }
}
