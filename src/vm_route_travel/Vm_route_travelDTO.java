package vm_route_travel;
import java.util.*; 
import util.*; 


public class Vm_route_travelDTO extends CommonDTO
{

	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	public long requestedStoppageId = -1;
    public String requestedStoppageName = "";
//    public String remarks = "";
	public long requestedStartDate = System.currentTimeMillis();
	public long requesterOrgId = -1;
	public long requesterOfficeId = -1;
	public long requesterOfficeUnitId = -1;
	public long requesterEmpId = -1;
    public String requesterPhoneNum = "";
    public String requesterNameEn = "";
    public String requesterNameBn = "";
    public String requesterOfficeNameEn = "";
    public String requesterOfficeNameBn = "";
    public String requesterOfficeUnitNameEn = "";
    public String requesterOfficeUnitNameBn = "";
    public String requesterOfficeUnitOrgNameEn = "";
    public String requesterOfficeUnitOrgNameBn = "";
	public int status = -1;
	public long routeId = -1;
	public long stoppageId = -1;
	public long fiscalYearId = -1;
	public long approvedStartDate = System.currentTimeMillis();
    public String decisionRemarks = "";
	public long filesDropzone = -1;
	public long approvedDate = System.currentTimeMillis();
	public long withdrawalDate = System.currentTimeMillis();
	public long approverOrgId = -1;
	public long approverOfficeId = -1;
	public long approverOfficeUnitId = -1;
	public long approverEmpId = -1;
    public String approverPhoneNum = "";
    public String approverNameEn = "";
    public String approverNameBn = "";
    public String approverOfficeNameEn = "";
    public String approverOfficeNameBn = "";
    public String approverOfficeUnitNameEn = "";
    public String approverOfficeUnitNameBn = "";
    public String approverOfficeUnitOrgNameEn = "";
    public String approverOfficeUnitOrgNameBn = "";
	public long withdrawerOrgId = -1;
	public long withdrawerOfficeId = -1;
	public long withdrawerOfficeUnitId = -1;
	public long withdrawerEmpId = -1;
    public String withdrawerPhoneNum = "";
    public String withdrawerNameEn = "";
    public String withdrawerNameBn = "";
    public String withdrawerOfficeNameEn = "";
    public String withdrawerOfficeNameBn = "";
    public String withdrawerOfficeUnitNameEn = "";
    public String withdrawerOfficeUnitNameBn = "";
    public String withdrawerOfficeUnitOrgNameEn = "";
    public String withdrawerOfficeUnitOrgNameBn = "";
	
	
    @Override
	public String toString() {
            return "$Vm_route_travelDTO[" +
            " iD = " + iD +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            " requestedStoppageId = " + requestedStoppageId +
            " requestedStoppageName = " + requestedStoppageName +
            " remarks = " + remarks +
            " requestedStartDate = " + requestedStartDate +
            " requesterOrgId = " + requesterOrgId +
            " requesterOfficeId = " + requesterOfficeId +
            " requesterOfficeUnitId = " + requesterOfficeUnitId +
            " requesterEmpId = " + requesterEmpId +
            " requesterPhoneNum = " + requesterPhoneNum +
            " requesterNameEn = " + requesterNameEn +
            " requesterNameBn = " + requesterNameBn +
            " requesterOfficeNameEn = " + requesterOfficeNameEn +
            " requesterOfficeNameBn = " + requesterOfficeNameBn +
            " requesterOfficeUnitNameEn = " + requesterOfficeUnitNameEn +
            " requesterOfficeUnitNameBn = " + requesterOfficeUnitNameBn +
            " requesterOfficeUnitOrgNameEn = " + requesterOfficeUnitOrgNameEn +
            " requesterOfficeUnitOrgNameBn = " + requesterOfficeUnitOrgNameBn +
            " status = " + status +
            " routeId = " + routeId +
            " stoppageId = " + stoppageId +
            " fiscalYearId = " + fiscalYearId +
            " approvedStartDate = " + approvedStartDate +
            " decisionRemarks = " + decisionRemarks +
            " filesDropzone = " + filesDropzone +
            " approvedDate = " + approvedDate +
            " withdrawalDate = " + withdrawalDate +
            " approverOrgId = " + approverOrgId +
            " approverOfficeId = " + approverOfficeId +
            " approverOfficeUnitId = " + approverOfficeUnitId +
            " approverEmpId = " + approverEmpId +
            " approverPhoneNum = " + approverPhoneNum +
            " approverNameEn = " + approverNameEn +
            " approverNameBn = " + approverNameBn +
            " approverOfficeNameEn = " + approverOfficeNameEn +
            " approverOfficeNameBn = " + approverOfficeNameBn +
            " approverOfficeUnitNameEn = " + approverOfficeUnitNameEn +
            " approverOfficeUnitNameBn = " + approverOfficeUnitNameBn +
            " approverOfficeUnitOrgNameEn = " + approverOfficeUnitOrgNameEn +
            " approverOfficeUnitOrgNameBn = " + approverOfficeUnitOrgNameBn +
            " withdrawerOrgId = " + withdrawerOrgId +
            " withdrawerOfficeId = " + withdrawerOfficeId +
            " withdrawerOfficeUnitId = " + withdrawerOfficeUnitId +
            " withdrawerEmpId = " + withdrawerEmpId +
            " withdrawerPhoneNum = " + withdrawerPhoneNum +
            " withdrawerNameEn = " + withdrawerNameEn +
            " withdrawerNameBn = " + withdrawerNameBn +
            " withdrawerOfficeNameEn = " + withdrawerOfficeNameEn +
            " withdrawerOfficeNameBn = " + withdrawerOfficeNameBn +
            " withdrawerOfficeUnitNameEn = " + withdrawerOfficeUnitNameEn +
            " withdrawerOfficeUnitNameBn = " + withdrawerOfficeUnitNameBn +
            " withdrawerOfficeUnitOrgNameEn = " + withdrawerOfficeUnitOrgNameEn +
            " withdrawerOfficeUnitOrgNameBn = " + withdrawerOfficeUnitOrgNameBn +
            "]";
    }

}