package vm_route_travel;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import employee_assign.EmployeeSearchModalUtil;
import employee_assign.EmployeeSearchModel;
import office_units.Office_unitsRepository;
import offices.OfficesDTO;
import offices.OfficesRepository;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import role.RoleDTO;
import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import files.*;
import vm_fuel_request.InvalidDataException;
import vm_requisition.CommonApprovalStatus;
import vm_route_travel_approver.Vm_route_travel_approverDAO;
import vm_route_travel_approver.Vm_route_travel_approverDTO;

/**
 * Servlet implementation class Vm_route_travelServlet
 */
@WebServlet("/Vm_route_travelServlet")
@MultipartConfig
public class Vm_route_travelServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Vm_route_travelServlet.class);

    String tableName = "vm_route_travel";

	Vm_route_travelDAO vm_route_travelDAO;
	CommonRequestHandler commonRequestHandler;
	FilesDAO filesDAO = new FilesDAO();
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vm_route_travelServlet()
	{
        super();
    	try
    	{
			vm_route_travelDAO = new Vm_route_travelDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(vm_route_travelDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");

			if (!actionType.equals("search") && !actionType.equals("add")) {
				if (request.getParameter("ID") != null) {
					RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
					boolean isAdmin = role.ID == SessionConstants.VEHICLE_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;

					String filterOwn = " (requester_org_id = " + userDTO.organogramID + " OR inserted_by_organogram_id = " + userDTO.organogramID;
					filterOwn += isAdmin ? " OR true)" : ")";
					filterOwn += " AND ";
					filterOwn += " ID = " + Long.parseLong(request.getParameter("ID")) + " ";
					int pendingRequest = vm_route_travelDAO.getCount(null, 1, 0, true, userDTO, filterOwn, false);
					if (pendingRequest != 1) {
						throw new card_info.InvalidDataException("This employee has no request for id : "+ request.getParameter("ID"));
					}
				}
			}

			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_UPDATE))
				{
					getVm_route_travel(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("downloadDropzoneFile"))
			{
				commonRequestHandler.downloadDropzoneFile(request, response, filesDAO);
			}
			else if(actionType.equals("DeleteFileFromDropZone"))
			{
				commonRequestHandler.deleteFileFromDropZone(request, response, filesDAO);
			}
			else if(actionType.equals("cancel"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_UPDATE))
				{
					cancel(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchVm_route_travel(request, response, isPermanentTable, filter);
						}
						else
						{
							searchVm_route_travel(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchVm_route_travel(request, response, tempTableName, isPermanentTable);
					}
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);

			if (!actionType.equals("search") && !actionType.equals("add")) {
				if (request.getParameter("iD") != null) {
					RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
					boolean isAdmin = role.ID == SessionConstants.VEHICLE_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;

					String filterOwn = " (requester_org_id = " + userDTO.organogramID + " OR inserted_by_organogram_id = " + userDTO.organogramID;
					filterOwn += isAdmin ? " OR true)" : ")";
					filterOwn += " AND ";
					filterOwn += " ID = " + Long.parseLong(request.getParameter("iD")) + " ";
					int pendingRequest = vm_route_travelDAO.getCount(null, 1, 0, true, userDTO, filterOwn, false);
					if (pendingRequest != 1) {
						throw new card_info.InvalidDataException("This employee has no request for id : "+ request.getParameter("iD"));
					}
				}
			}

			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_ADD))
				{
					System.out.println("going to  addVm_route_travel ");
					addVm_route_travel(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addVm_route_travel ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("UploadFilesFromDropZone"))
			{
				commonRequestHandler.UploadFilesFromDropZone(request, response, userDTO);
			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addVm_route_travel ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_UPDATE))
				{
					addVm_route_travel(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_SEARCH))
				{
					searchVm_route_travel(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Vm_route_travelDTO vm_route_travelDTO = Vm_route_travelRepository.getInstance().getVm_route_travelDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(vm_route_travelDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addVm_route_travel(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addVm_route_travel");
			String path = getServletContext().getRealPath("/img2/");
			Vm_route_travelDTO vm_route_travelDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
			boolean valid = true;

			if(addFlag == true)
			{
				vm_route_travelDTO = new Vm_route_travelDTO();
			}
			else
			{
				vm_route_travelDTO = Vm_route_travelRepository.getInstance().getVm_route_travelDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			if(addFlag)
			{
				vm_route_travelDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				vm_route_travelDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				vm_route_travelDTO.insertionDate = c.getTimeInMillis();
			}


			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requestedStoppageId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requestedStoppageId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travelDTO.requestedStoppageId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requestedStoppageName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requestedStoppageName = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.requestedStoppageName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("remarks");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("remarks = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.remarks = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requestedStartDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requestedStartDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					vm_route_travelDTO.requestedStartDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOrgId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOrgId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travelDTO.requesterOrgId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travelDTO.requesterOfficeId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeUnitId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeUnitId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travelDTO.requesterOfficeUnitId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterEmpId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterEmpId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travelDTO.requesterEmpId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterPhoneNum");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterPhoneNum = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.requesterPhoneNum = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterNameEn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.requesterNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterNameBn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.requesterNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeNameEn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.requesterOfficeNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeNameBn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.requesterOfficeNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeUnitNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeUnitNameEn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.requesterOfficeUnitNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeUnitNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeUnitNameBn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.requesterOfficeUnitNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeUnitOrgNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeUnitOrgNameEn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.requesterOfficeUnitOrgNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeUnitOrgNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeUnitOrgNameBn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.requesterOfficeUnitOrgNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("status");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("status = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travelDTO.status = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			vm_route_travelDTO.status = CommonApprovalStatus.PENDING.getValue();

			Value = request.getParameter("routeId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("routeId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travelDTO.routeId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("stoppageId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("stoppageId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travelDTO.stoppageId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("fiscalYearId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("fiscalYearId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travelDTO.fiscalYearId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approvedStartDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approvedStartDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					vm_route_travelDTO.approvedStartDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("decisionRemarks");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("decisionRemarks = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.decisionRemarks = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("filesDropzone");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("filesDropzone = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				System.out.println("filesDropzone = " + Value);

				vm_route_travelDTO.filesDropzone = Long.parseLong(Value);


				if(addFlag == false)
				{
					String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
					String[] deleteArray = filesDropzoneFilesToDelete.split(",");
					for(int i = 0; i < deleteArray.length; i ++)
					{
						System.out.println("going to delete " + deleteArray[i]);
						if(i>0)
						{
							filesDAO.delete(Long.parseLong(deleteArray[i]));
						}
					}
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approvedDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approvedDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					vm_route_travelDTO.approvedDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("withdrawalDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("withdrawalDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					vm_route_travelDTO.withdrawalDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOrgId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOrgId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travelDTO.approverOrgId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travelDTO.approverOfficeId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeUnitId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeUnitId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travelDTO.approverOfficeUnitId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverEmpId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverEmpId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travelDTO.approverEmpId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverPhoneNum");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverPhoneNum = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.approverPhoneNum = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverNameEn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.approverNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverNameBn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.approverNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeNameEn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.approverOfficeNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeNameBn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.approverOfficeNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeUnitNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeUnitNameEn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.approverOfficeUnitNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeUnitNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeUnitNameBn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.approverOfficeUnitNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeUnitOrgNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeUnitOrgNameEn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.approverOfficeUnitOrgNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeUnitOrgNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeUnitOrgNameBn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.approverOfficeUnitOrgNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("withdrawerOrgId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("withdrawerOrgId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travelDTO.withdrawerOrgId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("withdrawerOfficeId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("withdrawerOfficeId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travelDTO.withdrawerOfficeId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("withdrawerOfficeUnitId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("withdrawerOfficeUnitId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travelDTO.withdrawerOfficeUnitId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("withdrawerEmpId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("withdrawerEmpId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travelDTO.withdrawerEmpId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("withdrawerPhoneNum");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("withdrawerPhoneNum = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.withdrawerPhoneNum = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("withdrawerNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("withdrawerNameEn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.withdrawerNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("withdrawerNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("withdrawerNameBn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.withdrawerNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("withdrawerOfficeNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("withdrawerOfficeNameEn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.withdrawerOfficeNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("withdrawerOfficeNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("withdrawerOfficeNameBn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.withdrawerOfficeNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("withdrawerOfficeUnitNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("withdrawerOfficeUnitNameEn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.withdrawerOfficeUnitNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("withdrawerOfficeUnitNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("withdrawerOfficeUnitNameBn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.withdrawerOfficeUnitNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("withdrawerOfficeUnitOrgNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("withdrawerOfficeUnitOrgNameEn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.withdrawerOfficeUnitOrgNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("withdrawerOfficeUnitOrgNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("withdrawerOfficeUnitOrgNameBn = " + Value);
			if(Value != null)
			{
				vm_route_travelDTO.withdrawerOfficeUnitOrgNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}


			EmployeeSearchModel model = (gson.fromJson
					(EmployeeSearchModalUtil.getEmployeeSearchModelJson
							(userDTO.employee_record_id,
									userDTO.unitID,
									userDTO.organogramID),
							EmployeeSearchModel.class)
			);

			vm_route_travelDTO.requesterEmpId = model.employeeRecordId;
			vm_route_travelDTO.requesterOfficeUnitId = model.officeUnitId;
			vm_route_travelDTO.requesterOrgId = model.organogramId;

			vm_route_travelDTO.requesterNameEn = model.employeeNameEn;
			vm_route_travelDTO.requesterNameBn = model.employeeNameBn;
			vm_route_travelDTO.requesterOfficeUnitNameEn = model.officeUnitNameEn;
			vm_route_travelDTO.requesterOfficeUnitNameBn = model.officeUnitNameBn;
			vm_route_travelDTO.requesterOfficeUnitOrgNameEn = model.organogramNameEn;
			vm_route_travelDTO.requesterOfficeUnitOrgNameBn = model.organogramNameBn;

			vm_route_travelDTO.requesterPhoneNum = model.phoneNumber;

			OfficesDTO requesterOffice = OfficesRepository.getInstance().getOfficesDTOByID(vm_route_travelDTO.requesterOfficeId);

			if (requesterOffice != null) {
				vm_route_travelDTO.requesterOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(model.officeUnitId).officeId;

				vm_route_travelDTO.requesterOfficeNameEn = requesterOffice.officeNameEng;
				vm_route_travelDTO.requesterOfficeNameBn = requesterOffice.officeNameBng;
			}

			valid = vm_route_travelDTO.requestedStoppageId != -1 || !vm_route_travelDTO.requestedStoppageName.isEmpty();

			if (valid) {
				Utils.handleTransactionForNavigationService4(()->{
					System.out.println("Done adding  addVm_route_travel dto = " + vm_route_travelDTO);
					long returnedID = -1;

					if(isPermanentTable == false) //add new row for validation and make the old row outdated
					{
						vm_route_travelDAO.setIsDeleted(vm_route_travelDTO.iD, CommonDTO.OUTDATED);
						returnedID = vm_route_travelDAO.add(vm_route_travelDTO);
						vm_route_travelDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
					}
					else if(addFlag == true)
					{
						returnedID = vm_route_travelDAO.manageWriteOperations(vm_route_travelDTO, SessionConstants.INSERT, -1, userDTO);
					}
					else
					{
						returnedID = vm_route_travelDAO.manageWriteOperations(vm_route_travelDTO, SessionConstants.UPDATE, -1, userDTO);
					}

				});

				PrintWriter out = response.getWriter();
				out.println(new Gson().toJson("Success"));
				out.close();
			}
			else {
				PrintWriter out = response.getWriter();
				out.println(new Gson().toJson("Invalid Input"));
				out.close();
			}









//			if(isPermanentTable)
//			{
//				String inPlaceSubmit = (String)request.getParameter("inplacesubmit");
//
//				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
//				{
//					getVm_route_travel(request, response, returnedID);
//				}
//				else
//				{
//					response.sendRedirect("Vm_route_travelServlet?actionType=search");
//				}
//			}
//			else
//			{
//				commonRequestHandler.validate(vm_route_travelDAO.getInstance().getVm_route_travelDTOByID(returnedID), request, response, userDTO);
//			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}




	private void cancel(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% cancel vm requisition");
			String path = getServletContext().getRealPath("/img2/");
			Vm_route_travelDTO vm_requisitionDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			System.out.println("in  searchVm_requisition 1");
			LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
			UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

			vm_requisitionDTO = Vm_route_travelRepository.getInstance().getVm_route_travelDTOByID(Long.parseLong(request.getParameter("ID")));
			vm_requisitionDTO.status = CommonApprovalStatus.CANCELLED.getValue();

			System.out.println("Done adding  addVm_requisition dto = " + vm_requisitionDTO);
			long returnedID = -1;

			returnedID = vm_route_travelDAO.manageWriteOperations(vm_requisitionDTO, SessionConstants.UPDATE, -1, userDTO);


			if(true)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getVm_route_travel(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Vm_route_travelServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(Vm_route_travelRepository.getInstance().getVm_route_travelDTOByID(returnedID), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}




	private void getVm_route_travel(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getVm_route_travel");
		Vm_route_travelDTO vm_route_travelDTO = null;
		try
		{
			vm_route_travelDTO = Vm_route_travelRepository.getInstance().getVm_route_travelDTOByID(id);
			request.setAttribute("ID", vm_route_travelDTO.iD);
			request.setAttribute("vm_route_travelDTO",vm_route_travelDTO);
			request.setAttribute("vm_route_travelDAO",vm_route_travelDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "vm_route_travel/vm_route_travelInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "vm_route_travel/vm_route_travelSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "vm_route_travel/vm_route_travelEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "vm_route_travel/vm_route_travelEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getVm_route_travel(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getVm_route_travel(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchVm_route_travel(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchVm_route_travel 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

		RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
		boolean isAdmin = role.ID == SessionConstants.VEHICLE_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;

		String filterOwn = " (requester_org_id = " + userDTO.organogramID + " OR inserted_by_organogram_id = " + userDTO.organogramID;
		filterOwn += isAdmin ? " OR true) " : ") ";
		if (filter != null && !filter.isEmpty()) filter += " AND ";
		filter += filterOwn;

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_VM_ROUTE_TRAVEL,
			request,
			vm_route_travelDAO,
			SessionConstants.VIEW_VM_ROUTE_TRAVEL,
			SessionConstants.SEARCH_VM_ROUTE_TRAVEL,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("vm_route_travelDAO",vm_route_travelDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_route_travel/vm_route_travelApproval.jsp");
	        	rd = request.getRequestDispatcher("vm_route_travel/vm_route_travelApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_route_travel/vm_route_travelApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("vm_route_travel/vm_route_travelApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_route_travel/vm_route_travelSearch.jsp");
	        	rd = request.getRequestDispatcher("vm_route_travel/vm_route_travelSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_route_travel/vm_route_travelSearchForm.jsp");
	        	rd = request.getRequestDispatcher("vm_route_travel/vm_route_travelSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

