package vm_route_travel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;
import vm_requisition.CommonApprovalStatus;


public class Vm_route_travelRepository implements Repository {
	Vm_route_travelDAO vm_route_travelDAO = null;
	Gson gson;

	public void setDAO(Vm_route_travelDAO vm_route_travelDAO)
	{
		this.vm_route_travelDAO = vm_route_travelDAO;
		gson = new Gson();

	}
	
	
	static Logger logger = Logger.getLogger(Vm_route_travelRepository.class);
	Map<Long, Vm_route_travelDTO>mapOfVm_route_travelDTOToiD;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOToinsertedByUserId;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOToinsertedByOrganogramId;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOToinsertionDate;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTolastModificationTime;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTosearchColumn;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTorequestedStoppageId;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTorequestedStoppageName;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOToremarks;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTorequestedStartDate;
	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTorequesterOrgId;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTorequesterOfficeId;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTorequesterOfficeUnitId;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTorequesterEmpId;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTorequesterPhoneNum;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTorequesterNameEn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTorequesterNameBn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTorequesterOfficeNameEn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTorequesterOfficeNameBn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTorequesterOfficeUnitNameEn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTorequesterOfficeUnitNameBn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTorequesterOfficeUnitOrgNameEn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTorequesterOfficeUnitOrgNameBn;
//	Map<Integer, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTostatus;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTorouteId;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTostoppageId;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTofiscalYearId;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOToapprovedStartDate;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTodecisionRemarks;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTofilesDropzone;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOToapprovedDate;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTowithdrawalDate;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOToapproverOrgId;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOToapproverOfficeId;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOToapproverOfficeUnitId;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOToapproverEmpId;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOToapproverPhoneNum;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOToapproverNameEn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOToapproverNameBn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOToapproverOfficeNameEn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOToapproverOfficeNameBn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOToapproverOfficeUnitNameEn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOToapproverOfficeUnitNameBn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOToapproverOfficeUnitOrgNameEn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOToapproverOfficeUnitOrgNameBn;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTowithdrawerOrgId;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTowithdrawerOfficeId;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTowithdrawerOfficeUnitId;
//	Map<Long, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTowithdrawerEmpId;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTowithdrawerPhoneNum;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTowithdrawerNameEn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTowithdrawerNameBn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTowithdrawerOfficeNameEn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTowithdrawerOfficeNameBn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTowithdrawerOfficeUnitNameEn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTowithdrawerOfficeUnitNameBn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTowithdrawerOfficeUnitOrgNameEn;
//	Map<String, Set<Vm_route_travelDTO> >mapOfVm_route_travelDTOTowithdrawerOfficeUnitOrgNameBn;


	static Vm_route_travelRepository instance = null;  
	private Vm_route_travelRepository(){
		mapOfVm_route_travelDTOToiD = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOToinsertedByUserId = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOToinsertionDate = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTolastModificationTime = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTosearchColumn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTorequestedStoppageId = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTorequestedStoppageName = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOToremarks = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTorequestedStartDate = new ConcurrentHashMap<>();
		mapOfVm_route_travelDTOTorequesterOrgId = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTorequesterOfficeId = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTorequesterOfficeUnitId = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTorequesterEmpId = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTorequesterPhoneNum = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTorequesterNameEn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTorequesterNameBn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTorequesterOfficeNameEn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTorequesterOfficeNameBn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTorequesterOfficeUnitNameEn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTorequesterOfficeUnitNameBn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTorequesterOfficeUnitOrgNameEn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTorequesterOfficeUnitOrgNameBn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTostatus = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTorouteId = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTostoppageId = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTofiscalYearId = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOToapprovedStartDate = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTodecisionRemarks = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTofilesDropzone = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOToapprovedDate = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTowithdrawalDate = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOToapproverOrgId = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOToapproverOfficeId = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOToapproverOfficeUnitId = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOToapproverEmpId = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOToapproverPhoneNum = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOToapproverNameEn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOToapproverNameBn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOToapproverOfficeNameEn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOToapproverOfficeNameBn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOToapproverOfficeUnitNameEn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOToapproverOfficeUnitNameBn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOToapproverOfficeUnitOrgNameEn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOToapproverOfficeUnitOrgNameBn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTowithdrawerOrgId = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTowithdrawerOfficeId = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTowithdrawerOfficeUnitId = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTowithdrawerEmpId = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTowithdrawerPhoneNum = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTowithdrawerNameEn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTowithdrawerNameBn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTowithdrawerOfficeNameEn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTowithdrawerOfficeNameBn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTowithdrawerOfficeUnitNameEn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTowithdrawerOfficeUnitNameBn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTowithdrawerOfficeUnitOrgNameEn = new ConcurrentHashMap<>();
//		mapOfVm_route_travelDTOTowithdrawerOfficeUnitOrgNameBn = new ConcurrentHashMap<>();

		setDAO(new Vm_route_travelDAO());
		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vm_route_travelRepository getInstance(){
		if (instance == null){
			instance = new Vm_route_travelRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(vm_route_travelDAO == null)
		{
			return;
		}
		try {
			List<Vm_route_travelDTO> vm_route_travelDTOs = vm_route_travelDAO.getAllVm_route_travel(reloadAll);
			for(Vm_route_travelDTO vm_route_travelDTO : vm_route_travelDTOs) {
				Vm_route_travelDTO oldVm_route_travelDTO = getVm_route_travelDTOByIDWithoutClone(vm_route_travelDTO.iD);
				if( oldVm_route_travelDTO != null ) {
					mapOfVm_route_travelDTOToiD.remove(oldVm_route_travelDTO.iD);
				
//					if(mapOfVm_route_travelDTOToinsertedByUserId.containsKey(oldVm_route_travelDTO.insertedByUserId)) {
//						mapOfVm_route_travelDTOToinsertedByUserId.get(oldVm_route_travelDTO.insertedByUserId).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOToinsertedByUserId.get(oldVm_route_travelDTO.insertedByUserId).isEmpty()) {
//						mapOfVm_route_travelDTOToinsertedByUserId.remove(oldVm_route_travelDTO.insertedByUserId);
//					}
//
//					if(mapOfVm_route_travelDTOToinsertedByOrganogramId.containsKey(oldVm_route_travelDTO.insertedByOrganogramId)) {
//						mapOfVm_route_travelDTOToinsertedByOrganogramId.get(oldVm_route_travelDTO.insertedByOrganogramId).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOToinsertedByOrganogramId.get(oldVm_route_travelDTO.insertedByOrganogramId).isEmpty()) {
//						mapOfVm_route_travelDTOToinsertedByOrganogramId.remove(oldVm_route_travelDTO.insertedByOrganogramId);
//					}
//
//					if(mapOfVm_route_travelDTOToinsertionDate.containsKey(oldVm_route_travelDTO.insertionDate)) {
//						mapOfVm_route_travelDTOToinsertionDate.get(oldVm_route_travelDTO.insertionDate).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOToinsertionDate.get(oldVm_route_travelDTO.insertionDate).isEmpty()) {
//						mapOfVm_route_travelDTOToinsertionDate.remove(oldVm_route_travelDTO.insertionDate);
//					}
//
//					if(mapOfVm_route_travelDTOTolastModificationTime.containsKey(oldVm_route_travelDTO.lastModificationTime)) {
//						mapOfVm_route_travelDTOTolastModificationTime.get(oldVm_route_travelDTO.lastModificationTime).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTolastModificationTime.get(oldVm_route_travelDTO.lastModificationTime).isEmpty()) {
//						mapOfVm_route_travelDTOTolastModificationTime.remove(oldVm_route_travelDTO.lastModificationTime);
//					}
//
//					if(mapOfVm_route_travelDTOTosearchColumn.containsKey(oldVm_route_travelDTO.searchColumn)) {
//						mapOfVm_route_travelDTOTosearchColumn.get(oldVm_route_travelDTO.searchColumn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTosearchColumn.get(oldVm_route_travelDTO.searchColumn).isEmpty()) {
//						mapOfVm_route_travelDTOTosearchColumn.remove(oldVm_route_travelDTO.searchColumn);
//					}
//
//					if(mapOfVm_route_travelDTOTorequestedStoppageId.containsKey(oldVm_route_travelDTO.requestedStoppageId)) {
//						mapOfVm_route_travelDTOTorequestedStoppageId.get(oldVm_route_travelDTO.requestedStoppageId).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTorequestedStoppageId.get(oldVm_route_travelDTO.requestedStoppageId).isEmpty()) {
//						mapOfVm_route_travelDTOTorequestedStoppageId.remove(oldVm_route_travelDTO.requestedStoppageId);
//					}
//
//					if(mapOfVm_route_travelDTOTorequestedStoppageName.containsKey(oldVm_route_travelDTO.requestedStoppageName)) {
//						mapOfVm_route_travelDTOTorequestedStoppageName.get(oldVm_route_travelDTO.requestedStoppageName).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTorequestedStoppageName.get(oldVm_route_travelDTO.requestedStoppageName).isEmpty()) {
//						mapOfVm_route_travelDTOTorequestedStoppageName.remove(oldVm_route_travelDTO.requestedStoppageName);
//					}
//
//					if(mapOfVm_route_travelDTOToremarks.containsKey(oldVm_route_travelDTO.remarks)) {
//						mapOfVm_route_travelDTOToremarks.get(oldVm_route_travelDTO.remarks).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOToremarks.get(oldVm_route_travelDTO.remarks).isEmpty()) {
//						mapOfVm_route_travelDTOToremarks.remove(oldVm_route_travelDTO.remarks);
//					}
//
//					if(mapOfVm_route_travelDTOTorequestedStartDate.containsKey(oldVm_route_travelDTO.requestedStartDate)) {
//						mapOfVm_route_travelDTOTorequestedStartDate.get(oldVm_route_travelDTO.requestedStartDate).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTorequestedStartDate.get(oldVm_route_travelDTO.requestedStartDate).isEmpty()) {
//						mapOfVm_route_travelDTOTorequestedStartDate.remove(oldVm_route_travelDTO.requestedStartDate);
//					}
//
					if(mapOfVm_route_travelDTOTorequesterOrgId.containsKey(oldVm_route_travelDTO.requesterOrgId)) {
						mapOfVm_route_travelDTOTorequesterOrgId.get(oldVm_route_travelDTO.requesterOrgId).remove(oldVm_route_travelDTO);
					}
					if(mapOfVm_route_travelDTOTorequesterOrgId.get(oldVm_route_travelDTO.requesterOrgId).isEmpty()) {
						mapOfVm_route_travelDTOTorequesterOrgId.remove(oldVm_route_travelDTO.requesterOrgId);
					}
//
//					if(mapOfVm_route_travelDTOTorequesterOfficeId.containsKey(oldVm_route_travelDTO.requesterOfficeId)) {
//						mapOfVm_route_travelDTOTorequesterOfficeId.get(oldVm_route_travelDTO.requesterOfficeId).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTorequesterOfficeId.get(oldVm_route_travelDTO.requesterOfficeId).isEmpty()) {
//						mapOfVm_route_travelDTOTorequesterOfficeId.remove(oldVm_route_travelDTO.requesterOfficeId);
//					}
//
//					if(mapOfVm_route_travelDTOTorequesterOfficeUnitId.containsKey(oldVm_route_travelDTO.requesterOfficeUnitId)) {
//						mapOfVm_route_travelDTOTorequesterOfficeUnitId.get(oldVm_route_travelDTO.requesterOfficeUnitId).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTorequesterOfficeUnitId.get(oldVm_route_travelDTO.requesterOfficeUnitId).isEmpty()) {
//						mapOfVm_route_travelDTOTorequesterOfficeUnitId.remove(oldVm_route_travelDTO.requesterOfficeUnitId);
//					}
//
//					if(mapOfVm_route_travelDTOTorequesterEmpId.containsKey(oldVm_route_travelDTO.requesterEmpId)) {
//						mapOfVm_route_travelDTOTorequesterEmpId.get(oldVm_route_travelDTO.requesterEmpId).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTorequesterEmpId.get(oldVm_route_travelDTO.requesterEmpId).isEmpty()) {
//						mapOfVm_route_travelDTOTorequesterEmpId.remove(oldVm_route_travelDTO.requesterEmpId);
//					}
//
//					if(mapOfVm_route_travelDTOTorequesterPhoneNum.containsKey(oldVm_route_travelDTO.requesterPhoneNum)) {
//						mapOfVm_route_travelDTOTorequesterPhoneNum.get(oldVm_route_travelDTO.requesterPhoneNum).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTorequesterPhoneNum.get(oldVm_route_travelDTO.requesterPhoneNum).isEmpty()) {
//						mapOfVm_route_travelDTOTorequesterPhoneNum.remove(oldVm_route_travelDTO.requesterPhoneNum);
//					}
//
//					if(mapOfVm_route_travelDTOTorequesterNameEn.containsKey(oldVm_route_travelDTO.requesterNameEn)) {
//						mapOfVm_route_travelDTOTorequesterNameEn.get(oldVm_route_travelDTO.requesterNameEn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTorequesterNameEn.get(oldVm_route_travelDTO.requesterNameEn).isEmpty()) {
//						mapOfVm_route_travelDTOTorequesterNameEn.remove(oldVm_route_travelDTO.requesterNameEn);
//					}
//
//					if(mapOfVm_route_travelDTOTorequesterNameBn.containsKey(oldVm_route_travelDTO.requesterNameBn)) {
//						mapOfVm_route_travelDTOTorequesterNameBn.get(oldVm_route_travelDTO.requesterNameBn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTorequesterNameBn.get(oldVm_route_travelDTO.requesterNameBn).isEmpty()) {
//						mapOfVm_route_travelDTOTorequesterNameBn.remove(oldVm_route_travelDTO.requesterNameBn);
//					}
//
//					if(mapOfVm_route_travelDTOTorequesterOfficeNameEn.containsKey(oldVm_route_travelDTO.requesterOfficeNameEn)) {
//						mapOfVm_route_travelDTOTorequesterOfficeNameEn.get(oldVm_route_travelDTO.requesterOfficeNameEn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTorequesterOfficeNameEn.get(oldVm_route_travelDTO.requesterOfficeNameEn).isEmpty()) {
//						mapOfVm_route_travelDTOTorequesterOfficeNameEn.remove(oldVm_route_travelDTO.requesterOfficeNameEn);
//					}
//
//					if(mapOfVm_route_travelDTOTorequesterOfficeNameBn.containsKey(oldVm_route_travelDTO.requesterOfficeNameBn)) {
//						mapOfVm_route_travelDTOTorequesterOfficeNameBn.get(oldVm_route_travelDTO.requesterOfficeNameBn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTorequesterOfficeNameBn.get(oldVm_route_travelDTO.requesterOfficeNameBn).isEmpty()) {
//						mapOfVm_route_travelDTOTorequesterOfficeNameBn.remove(oldVm_route_travelDTO.requesterOfficeNameBn);
//					}
//
//					if(mapOfVm_route_travelDTOTorequesterOfficeUnitNameEn.containsKey(oldVm_route_travelDTO.requesterOfficeUnitNameEn)) {
//						mapOfVm_route_travelDTOTorequesterOfficeUnitNameEn.get(oldVm_route_travelDTO.requesterOfficeUnitNameEn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTorequesterOfficeUnitNameEn.get(oldVm_route_travelDTO.requesterOfficeUnitNameEn).isEmpty()) {
//						mapOfVm_route_travelDTOTorequesterOfficeUnitNameEn.remove(oldVm_route_travelDTO.requesterOfficeUnitNameEn);
//					}
//
//					if(mapOfVm_route_travelDTOTorequesterOfficeUnitNameBn.containsKey(oldVm_route_travelDTO.requesterOfficeUnitNameBn)) {
//						mapOfVm_route_travelDTOTorequesterOfficeUnitNameBn.get(oldVm_route_travelDTO.requesterOfficeUnitNameBn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTorequesterOfficeUnitNameBn.get(oldVm_route_travelDTO.requesterOfficeUnitNameBn).isEmpty()) {
//						mapOfVm_route_travelDTOTorequesterOfficeUnitNameBn.remove(oldVm_route_travelDTO.requesterOfficeUnitNameBn);
//					}
//
//					if(mapOfVm_route_travelDTOTorequesterOfficeUnitOrgNameEn.containsKey(oldVm_route_travelDTO.requesterOfficeUnitOrgNameEn)) {
//						mapOfVm_route_travelDTOTorequesterOfficeUnitOrgNameEn.get(oldVm_route_travelDTO.requesterOfficeUnitOrgNameEn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTorequesterOfficeUnitOrgNameEn.get(oldVm_route_travelDTO.requesterOfficeUnitOrgNameEn).isEmpty()) {
//						mapOfVm_route_travelDTOTorequesterOfficeUnitOrgNameEn.remove(oldVm_route_travelDTO.requesterOfficeUnitOrgNameEn);
//					}
//
//					if(mapOfVm_route_travelDTOTorequesterOfficeUnitOrgNameBn.containsKey(oldVm_route_travelDTO.requesterOfficeUnitOrgNameBn)) {
//						mapOfVm_route_travelDTOTorequesterOfficeUnitOrgNameBn.get(oldVm_route_travelDTO.requesterOfficeUnitOrgNameBn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTorequesterOfficeUnitOrgNameBn.get(oldVm_route_travelDTO.requesterOfficeUnitOrgNameBn).isEmpty()) {
//						mapOfVm_route_travelDTOTorequesterOfficeUnitOrgNameBn.remove(oldVm_route_travelDTO.requesterOfficeUnitOrgNameBn);
//					}
//
//					if(mapOfVm_route_travelDTOTostatus.containsKey(oldVm_route_travelDTO.status)) {
//						mapOfVm_route_travelDTOTostatus.get(oldVm_route_travelDTO.status).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTostatus.get(oldVm_route_travelDTO.status).isEmpty()) {
//						mapOfVm_route_travelDTOTostatus.remove(oldVm_route_travelDTO.status);
//					}
//
//					if(mapOfVm_route_travelDTOTorouteId.containsKey(oldVm_route_travelDTO.routeId)) {
//						mapOfVm_route_travelDTOTorouteId.get(oldVm_route_travelDTO.routeId).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTorouteId.get(oldVm_route_travelDTO.routeId).isEmpty()) {
//						mapOfVm_route_travelDTOTorouteId.remove(oldVm_route_travelDTO.routeId);
//					}
//
//					if(mapOfVm_route_travelDTOTostoppageId.containsKey(oldVm_route_travelDTO.stoppageId)) {
//						mapOfVm_route_travelDTOTostoppageId.get(oldVm_route_travelDTO.stoppageId).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTostoppageId.get(oldVm_route_travelDTO.stoppageId).isEmpty()) {
//						mapOfVm_route_travelDTOTostoppageId.remove(oldVm_route_travelDTO.stoppageId);
//					}
//
//					if(mapOfVm_route_travelDTOTofiscalYearId.containsKey(oldVm_route_travelDTO.fiscalYearId)) {
//						mapOfVm_route_travelDTOTofiscalYearId.get(oldVm_route_travelDTO.fiscalYearId).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTofiscalYearId.get(oldVm_route_travelDTO.fiscalYearId).isEmpty()) {
//						mapOfVm_route_travelDTOTofiscalYearId.remove(oldVm_route_travelDTO.fiscalYearId);
//					}
//
//					if(mapOfVm_route_travelDTOToapprovedStartDate.containsKey(oldVm_route_travelDTO.approvedStartDate)) {
//						mapOfVm_route_travelDTOToapprovedStartDate.get(oldVm_route_travelDTO.approvedStartDate).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOToapprovedStartDate.get(oldVm_route_travelDTO.approvedStartDate).isEmpty()) {
//						mapOfVm_route_travelDTOToapprovedStartDate.remove(oldVm_route_travelDTO.approvedStartDate);
//					}
//
//					if(mapOfVm_route_travelDTOTodecisionRemarks.containsKey(oldVm_route_travelDTO.decisionRemarks)) {
//						mapOfVm_route_travelDTOTodecisionRemarks.get(oldVm_route_travelDTO.decisionRemarks).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTodecisionRemarks.get(oldVm_route_travelDTO.decisionRemarks).isEmpty()) {
//						mapOfVm_route_travelDTOTodecisionRemarks.remove(oldVm_route_travelDTO.decisionRemarks);
//					}
//
//					if(mapOfVm_route_travelDTOTofilesDropzone.containsKey(oldVm_route_travelDTO.filesDropzone)) {
//						mapOfVm_route_travelDTOTofilesDropzone.get(oldVm_route_travelDTO.filesDropzone).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTofilesDropzone.get(oldVm_route_travelDTO.filesDropzone).isEmpty()) {
//						mapOfVm_route_travelDTOTofilesDropzone.remove(oldVm_route_travelDTO.filesDropzone);
//					}
//
//					if(mapOfVm_route_travelDTOToapprovedDate.containsKey(oldVm_route_travelDTO.approvedDate)) {
//						mapOfVm_route_travelDTOToapprovedDate.get(oldVm_route_travelDTO.approvedDate).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOToapprovedDate.get(oldVm_route_travelDTO.approvedDate).isEmpty()) {
//						mapOfVm_route_travelDTOToapprovedDate.remove(oldVm_route_travelDTO.approvedDate);
//					}
//
//					if(mapOfVm_route_travelDTOTowithdrawalDate.containsKey(oldVm_route_travelDTO.withdrawalDate)) {
//						mapOfVm_route_travelDTOTowithdrawalDate.get(oldVm_route_travelDTO.withdrawalDate).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTowithdrawalDate.get(oldVm_route_travelDTO.withdrawalDate).isEmpty()) {
//						mapOfVm_route_travelDTOTowithdrawalDate.remove(oldVm_route_travelDTO.withdrawalDate);
//					}
//
//					if(mapOfVm_route_travelDTOToapproverOrgId.containsKey(oldVm_route_travelDTO.approverOrgId)) {
//						mapOfVm_route_travelDTOToapproverOrgId.get(oldVm_route_travelDTO.approverOrgId).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOToapproverOrgId.get(oldVm_route_travelDTO.approverOrgId).isEmpty()) {
//						mapOfVm_route_travelDTOToapproverOrgId.remove(oldVm_route_travelDTO.approverOrgId);
//					}
//
//					if(mapOfVm_route_travelDTOToapproverOfficeId.containsKey(oldVm_route_travelDTO.approverOfficeId)) {
//						mapOfVm_route_travelDTOToapproverOfficeId.get(oldVm_route_travelDTO.approverOfficeId).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOToapproverOfficeId.get(oldVm_route_travelDTO.approverOfficeId).isEmpty()) {
//						mapOfVm_route_travelDTOToapproverOfficeId.remove(oldVm_route_travelDTO.approverOfficeId);
//					}
//
//					if(mapOfVm_route_travelDTOToapproverOfficeUnitId.containsKey(oldVm_route_travelDTO.approverOfficeUnitId)) {
//						mapOfVm_route_travelDTOToapproverOfficeUnitId.get(oldVm_route_travelDTO.approverOfficeUnitId).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOToapproverOfficeUnitId.get(oldVm_route_travelDTO.approverOfficeUnitId).isEmpty()) {
//						mapOfVm_route_travelDTOToapproverOfficeUnitId.remove(oldVm_route_travelDTO.approverOfficeUnitId);
//					}
//
//					if(mapOfVm_route_travelDTOToapproverEmpId.containsKey(oldVm_route_travelDTO.approverEmpId)) {
//						mapOfVm_route_travelDTOToapproverEmpId.get(oldVm_route_travelDTO.approverEmpId).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOToapproverEmpId.get(oldVm_route_travelDTO.approverEmpId).isEmpty()) {
//						mapOfVm_route_travelDTOToapproverEmpId.remove(oldVm_route_travelDTO.approverEmpId);
//					}
//
//					if(mapOfVm_route_travelDTOToapproverPhoneNum.containsKey(oldVm_route_travelDTO.approverPhoneNum)) {
//						mapOfVm_route_travelDTOToapproverPhoneNum.get(oldVm_route_travelDTO.approverPhoneNum).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOToapproverPhoneNum.get(oldVm_route_travelDTO.approverPhoneNum).isEmpty()) {
//						mapOfVm_route_travelDTOToapproverPhoneNum.remove(oldVm_route_travelDTO.approverPhoneNum);
//					}
//
//					if(mapOfVm_route_travelDTOToapproverNameEn.containsKey(oldVm_route_travelDTO.approverNameEn)) {
//						mapOfVm_route_travelDTOToapproverNameEn.get(oldVm_route_travelDTO.approverNameEn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOToapproverNameEn.get(oldVm_route_travelDTO.approverNameEn).isEmpty()) {
//						mapOfVm_route_travelDTOToapproverNameEn.remove(oldVm_route_travelDTO.approverNameEn);
//					}
//
//					if(mapOfVm_route_travelDTOToapproverNameBn.containsKey(oldVm_route_travelDTO.approverNameBn)) {
//						mapOfVm_route_travelDTOToapproverNameBn.get(oldVm_route_travelDTO.approverNameBn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOToapproverNameBn.get(oldVm_route_travelDTO.approverNameBn).isEmpty()) {
//						mapOfVm_route_travelDTOToapproverNameBn.remove(oldVm_route_travelDTO.approverNameBn);
//					}
//
//					if(mapOfVm_route_travelDTOToapproverOfficeNameEn.containsKey(oldVm_route_travelDTO.approverOfficeNameEn)) {
//						mapOfVm_route_travelDTOToapproverOfficeNameEn.get(oldVm_route_travelDTO.approverOfficeNameEn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOToapproverOfficeNameEn.get(oldVm_route_travelDTO.approverOfficeNameEn).isEmpty()) {
//						mapOfVm_route_travelDTOToapproverOfficeNameEn.remove(oldVm_route_travelDTO.approverOfficeNameEn);
//					}
//
//					if(mapOfVm_route_travelDTOToapproverOfficeNameBn.containsKey(oldVm_route_travelDTO.approverOfficeNameBn)) {
//						mapOfVm_route_travelDTOToapproverOfficeNameBn.get(oldVm_route_travelDTO.approverOfficeNameBn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOToapproverOfficeNameBn.get(oldVm_route_travelDTO.approverOfficeNameBn).isEmpty()) {
//						mapOfVm_route_travelDTOToapproverOfficeNameBn.remove(oldVm_route_travelDTO.approverOfficeNameBn);
//					}
//
//					if(mapOfVm_route_travelDTOToapproverOfficeUnitNameEn.containsKey(oldVm_route_travelDTO.approverOfficeUnitNameEn)) {
//						mapOfVm_route_travelDTOToapproverOfficeUnitNameEn.get(oldVm_route_travelDTO.approverOfficeUnitNameEn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOToapproverOfficeUnitNameEn.get(oldVm_route_travelDTO.approverOfficeUnitNameEn).isEmpty()) {
//						mapOfVm_route_travelDTOToapproverOfficeUnitNameEn.remove(oldVm_route_travelDTO.approverOfficeUnitNameEn);
//					}
//
//					if(mapOfVm_route_travelDTOToapproverOfficeUnitNameBn.containsKey(oldVm_route_travelDTO.approverOfficeUnitNameBn)) {
//						mapOfVm_route_travelDTOToapproverOfficeUnitNameBn.get(oldVm_route_travelDTO.approverOfficeUnitNameBn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOToapproverOfficeUnitNameBn.get(oldVm_route_travelDTO.approverOfficeUnitNameBn).isEmpty()) {
//						mapOfVm_route_travelDTOToapproverOfficeUnitNameBn.remove(oldVm_route_travelDTO.approverOfficeUnitNameBn);
//					}
//
//					if(mapOfVm_route_travelDTOToapproverOfficeUnitOrgNameEn.containsKey(oldVm_route_travelDTO.approverOfficeUnitOrgNameEn)) {
//						mapOfVm_route_travelDTOToapproverOfficeUnitOrgNameEn.get(oldVm_route_travelDTO.approverOfficeUnitOrgNameEn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOToapproverOfficeUnitOrgNameEn.get(oldVm_route_travelDTO.approverOfficeUnitOrgNameEn).isEmpty()) {
//						mapOfVm_route_travelDTOToapproverOfficeUnitOrgNameEn.remove(oldVm_route_travelDTO.approverOfficeUnitOrgNameEn);
//					}
//
//					if(mapOfVm_route_travelDTOToapproverOfficeUnitOrgNameBn.containsKey(oldVm_route_travelDTO.approverOfficeUnitOrgNameBn)) {
//						mapOfVm_route_travelDTOToapproverOfficeUnitOrgNameBn.get(oldVm_route_travelDTO.approverOfficeUnitOrgNameBn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOToapproverOfficeUnitOrgNameBn.get(oldVm_route_travelDTO.approverOfficeUnitOrgNameBn).isEmpty()) {
//						mapOfVm_route_travelDTOToapproverOfficeUnitOrgNameBn.remove(oldVm_route_travelDTO.approverOfficeUnitOrgNameBn);
//					}
//
//					if(mapOfVm_route_travelDTOTowithdrawerOrgId.containsKey(oldVm_route_travelDTO.withdrawerOrgId)) {
//						mapOfVm_route_travelDTOTowithdrawerOrgId.get(oldVm_route_travelDTO.withdrawerOrgId).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTowithdrawerOrgId.get(oldVm_route_travelDTO.withdrawerOrgId).isEmpty()) {
//						mapOfVm_route_travelDTOTowithdrawerOrgId.remove(oldVm_route_travelDTO.withdrawerOrgId);
//					}
//
//					if(mapOfVm_route_travelDTOTowithdrawerOfficeId.containsKey(oldVm_route_travelDTO.withdrawerOfficeId)) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeId.get(oldVm_route_travelDTO.withdrawerOfficeId).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTowithdrawerOfficeId.get(oldVm_route_travelDTO.withdrawerOfficeId).isEmpty()) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeId.remove(oldVm_route_travelDTO.withdrawerOfficeId);
//					}
//
//					if(mapOfVm_route_travelDTOTowithdrawerOfficeUnitId.containsKey(oldVm_route_travelDTO.withdrawerOfficeUnitId)) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeUnitId.get(oldVm_route_travelDTO.withdrawerOfficeUnitId).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTowithdrawerOfficeUnitId.get(oldVm_route_travelDTO.withdrawerOfficeUnitId).isEmpty()) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeUnitId.remove(oldVm_route_travelDTO.withdrawerOfficeUnitId);
//					}
//
//					if(mapOfVm_route_travelDTOTowithdrawerEmpId.containsKey(oldVm_route_travelDTO.withdrawerEmpId)) {
//						mapOfVm_route_travelDTOTowithdrawerEmpId.get(oldVm_route_travelDTO.withdrawerEmpId).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTowithdrawerEmpId.get(oldVm_route_travelDTO.withdrawerEmpId).isEmpty()) {
//						mapOfVm_route_travelDTOTowithdrawerEmpId.remove(oldVm_route_travelDTO.withdrawerEmpId);
//					}
//
//					if(mapOfVm_route_travelDTOTowithdrawerPhoneNum.containsKey(oldVm_route_travelDTO.withdrawerPhoneNum)) {
//						mapOfVm_route_travelDTOTowithdrawerPhoneNum.get(oldVm_route_travelDTO.withdrawerPhoneNum).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTowithdrawerPhoneNum.get(oldVm_route_travelDTO.withdrawerPhoneNum).isEmpty()) {
//						mapOfVm_route_travelDTOTowithdrawerPhoneNum.remove(oldVm_route_travelDTO.withdrawerPhoneNum);
//					}
//
//					if(mapOfVm_route_travelDTOTowithdrawerNameEn.containsKey(oldVm_route_travelDTO.withdrawerNameEn)) {
//						mapOfVm_route_travelDTOTowithdrawerNameEn.get(oldVm_route_travelDTO.withdrawerNameEn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTowithdrawerNameEn.get(oldVm_route_travelDTO.withdrawerNameEn).isEmpty()) {
//						mapOfVm_route_travelDTOTowithdrawerNameEn.remove(oldVm_route_travelDTO.withdrawerNameEn);
//					}
//
//					if(mapOfVm_route_travelDTOTowithdrawerNameBn.containsKey(oldVm_route_travelDTO.withdrawerNameBn)) {
//						mapOfVm_route_travelDTOTowithdrawerNameBn.get(oldVm_route_travelDTO.withdrawerNameBn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTowithdrawerNameBn.get(oldVm_route_travelDTO.withdrawerNameBn).isEmpty()) {
//						mapOfVm_route_travelDTOTowithdrawerNameBn.remove(oldVm_route_travelDTO.withdrawerNameBn);
//					}
//
//					if(mapOfVm_route_travelDTOTowithdrawerOfficeNameEn.containsKey(oldVm_route_travelDTO.withdrawerOfficeNameEn)) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeNameEn.get(oldVm_route_travelDTO.withdrawerOfficeNameEn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTowithdrawerOfficeNameEn.get(oldVm_route_travelDTO.withdrawerOfficeNameEn).isEmpty()) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeNameEn.remove(oldVm_route_travelDTO.withdrawerOfficeNameEn);
//					}
//
//					if(mapOfVm_route_travelDTOTowithdrawerOfficeNameBn.containsKey(oldVm_route_travelDTO.withdrawerOfficeNameBn)) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeNameBn.get(oldVm_route_travelDTO.withdrawerOfficeNameBn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTowithdrawerOfficeNameBn.get(oldVm_route_travelDTO.withdrawerOfficeNameBn).isEmpty()) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeNameBn.remove(oldVm_route_travelDTO.withdrawerOfficeNameBn);
//					}
//
//					if(mapOfVm_route_travelDTOTowithdrawerOfficeUnitNameEn.containsKey(oldVm_route_travelDTO.withdrawerOfficeUnitNameEn)) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeUnitNameEn.get(oldVm_route_travelDTO.withdrawerOfficeUnitNameEn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTowithdrawerOfficeUnitNameEn.get(oldVm_route_travelDTO.withdrawerOfficeUnitNameEn).isEmpty()) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeUnitNameEn.remove(oldVm_route_travelDTO.withdrawerOfficeUnitNameEn);
//					}
//
//					if(mapOfVm_route_travelDTOTowithdrawerOfficeUnitNameBn.containsKey(oldVm_route_travelDTO.withdrawerOfficeUnitNameBn)) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeUnitNameBn.get(oldVm_route_travelDTO.withdrawerOfficeUnitNameBn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTowithdrawerOfficeUnitNameBn.get(oldVm_route_travelDTO.withdrawerOfficeUnitNameBn).isEmpty()) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeUnitNameBn.remove(oldVm_route_travelDTO.withdrawerOfficeUnitNameBn);
//					}
//
//					if(mapOfVm_route_travelDTOTowithdrawerOfficeUnitOrgNameEn.containsKey(oldVm_route_travelDTO.withdrawerOfficeUnitOrgNameEn)) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeUnitOrgNameEn.get(oldVm_route_travelDTO.withdrawerOfficeUnitOrgNameEn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTowithdrawerOfficeUnitOrgNameEn.get(oldVm_route_travelDTO.withdrawerOfficeUnitOrgNameEn).isEmpty()) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeUnitOrgNameEn.remove(oldVm_route_travelDTO.withdrawerOfficeUnitOrgNameEn);
//					}
//
//					if(mapOfVm_route_travelDTOTowithdrawerOfficeUnitOrgNameBn.containsKey(oldVm_route_travelDTO.withdrawerOfficeUnitOrgNameBn)) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeUnitOrgNameBn.get(oldVm_route_travelDTO.withdrawerOfficeUnitOrgNameBn).remove(oldVm_route_travelDTO);
//					}
//					if(mapOfVm_route_travelDTOTowithdrawerOfficeUnitOrgNameBn.get(oldVm_route_travelDTO.withdrawerOfficeUnitOrgNameBn).isEmpty()) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeUnitOrgNameBn.remove(oldVm_route_travelDTO.withdrawerOfficeUnitOrgNameBn);
//					}
					
					
				}
				if(vm_route_travelDTO.isDeleted == 0) 
				{
					
					mapOfVm_route_travelDTOToiD.put(vm_route_travelDTO.iD, vm_route_travelDTO);
				
//					if( ! mapOfVm_route_travelDTOToinsertedByUserId.containsKey(vm_route_travelDTO.insertedByUserId)) {
//						mapOfVm_route_travelDTOToinsertedByUserId.put(vm_route_travelDTO.insertedByUserId, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOToinsertedByUserId.get(vm_route_travelDTO.insertedByUserId).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOToinsertedByOrganogramId.containsKey(vm_route_travelDTO.insertedByOrganogramId)) {
//						mapOfVm_route_travelDTOToinsertedByOrganogramId.put(vm_route_travelDTO.insertedByOrganogramId, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOToinsertedByOrganogramId.get(vm_route_travelDTO.insertedByOrganogramId).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOToinsertionDate.containsKey(vm_route_travelDTO.insertionDate)) {
//						mapOfVm_route_travelDTOToinsertionDate.put(vm_route_travelDTO.insertionDate, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOToinsertionDate.get(vm_route_travelDTO.insertionDate).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTolastModificationTime.containsKey(vm_route_travelDTO.lastModificationTime)) {
//						mapOfVm_route_travelDTOTolastModificationTime.put(vm_route_travelDTO.lastModificationTime, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTolastModificationTime.get(vm_route_travelDTO.lastModificationTime).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTosearchColumn.containsKey(vm_route_travelDTO.searchColumn)) {
//						mapOfVm_route_travelDTOTosearchColumn.put(vm_route_travelDTO.searchColumn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTosearchColumn.get(vm_route_travelDTO.searchColumn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTorequestedStoppageId.containsKey(vm_route_travelDTO.requestedStoppageId)) {
//						mapOfVm_route_travelDTOTorequestedStoppageId.put(vm_route_travelDTO.requestedStoppageId, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTorequestedStoppageId.get(vm_route_travelDTO.requestedStoppageId).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTorequestedStoppageName.containsKey(vm_route_travelDTO.requestedStoppageName)) {
//						mapOfVm_route_travelDTOTorequestedStoppageName.put(vm_route_travelDTO.requestedStoppageName, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTorequestedStoppageName.get(vm_route_travelDTO.requestedStoppageName).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOToremarks.containsKey(vm_route_travelDTO.remarks)) {
//						mapOfVm_route_travelDTOToremarks.put(vm_route_travelDTO.remarks, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOToremarks.get(vm_route_travelDTO.remarks).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTorequestedStartDate.containsKey(vm_route_travelDTO.requestedStartDate)) {
//						mapOfVm_route_travelDTOTorequestedStartDate.put(vm_route_travelDTO.requestedStartDate, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTorequestedStartDate.get(vm_route_travelDTO.requestedStartDate).add(vm_route_travelDTO);
//
					if( ! mapOfVm_route_travelDTOTorequesterOrgId.containsKey(vm_route_travelDTO.requesterOrgId)) {
						mapOfVm_route_travelDTOTorequesterOrgId.put(vm_route_travelDTO.requesterOrgId, new HashSet<>());
					}
					mapOfVm_route_travelDTOTorequesterOrgId.get(vm_route_travelDTO.requesterOrgId).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTorequesterOfficeId.containsKey(vm_route_travelDTO.requesterOfficeId)) {
//						mapOfVm_route_travelDTOTorequesterOfficeId.put(vm_route_travelDTO.requesterOfficeId, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTorequesterOfficeId.get(vm_route_travelDTO.requesterOfficeId).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTorequesterOfficeUnitId.containsKey(vm_route_travelDTO.requesterOfficeUnitId)) {
//						mapOfVm_route_travelDTOTorequesterOfficeUnitId.put(vm_route_travelDTO.requesterOfficeUnitId, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTorequesterOfficeUnitId.get(vm_route_travelDTO.requesterOfficeUnitId).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTorequesterEmpId.containsKey(vm_route_travelDTO.requesterEmpId)) {
//						mapOfVm_route_travelDTOTorequesterEmpId.put(vm_route_travelDTO.requesterEmpId, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTorequesterEmpId.get(vm_route_travelDTO.requesterEmpId).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTorequesterPhoneNum.containsKey(vm_route_travelDTO.requesterPhoneNum)) {
//						mapOfVm_route_travelDTOTorequesterPhoneNum.put(vm_route_travelDTO.requesterPhoneNum, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTorequesterPhoneNum.get(vm_route_travelDTO.requesterPhoneNum).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTorequesterNameEn.containsKey(vm_route_travelDTO.requesterNameEn)) {
//						mapOfVm_route_travelDTOTorequesterNameEn.put(vm_route_travelDTO.requesterNameEn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTorequesterNameEn.get(vm_route_travelDTO.requesterNameEn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTorequesterNameBn.containsKey(vm_route_travelDTO.requesterNameBn)) {
//						mapOfVm_route_travelDTOTorequesterNameBn.put(vm_route_travelDTO.requesterNameBn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTorequesterNameBn.get(vm_route_travelDTO.requesterNameBn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTorequesterOfficeNameEn.containsKey(vm_route_travelDTO.requesterOfficeNameEn)) {
//						mapOfVm_route_travelDTOTorequesterOfficeNameEn.put(vm_route_travelDTO.requesterOfficeNameEn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTorequesterOfficeNameEn.get(vm_route_travelDTO.requesterOfficeNameEn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTorequesterOfficeNameBn.containsKey(vm_route_travelDTO.requesterOfficeNameBn)) {
//						mapOfVm_route_travelDTOTorequesterOfficeNameBn.put(vm_route_travelDTO.requesterOfficeNameBn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTorequesterOfficeNameBn.get(vm_route_travelDTO.requesterOfficeNameBn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTorequesterOfficeUnitNameEn.containsKey(vm_route_travelDTO.requesterOfficeUnitNameEn)) {
//						mapOfVm_route_travelDTOTorequesterOfficeUnitNameEn.put(vm_route_travelDTO.requesterOfficeUnitNameEn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTorequesterOfficeUnitNameEn.get(vm_route_travelDTO.requesterOfficeUnitNameEn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTorequesterOfficeUnitNameBn.containsKey(vm_route_travelDTO.requesterOfficeUnitNameBn)) {
//						mapOfVm_route_travelDTOTorequesterOfficeUnitNameBn.put(vm_route_travelDTO.requesterOfficeUnitNameBn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTorequesterOfficeUnitNameBn.get(vm_route_travelDTO.requesterOfficeUnitNameBn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTorequesterOfficeUnitOrgNameEn.containsKey(vm_route_travelDTO.requesterOfficeUnitOrgNameEn)) {
//						mapOfVm_route_travelDTOTorequesterOfficeUnitOrgNameEn.put(vm_route_travelDTO.requesterOfficeUnitOrgNameEn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTorequesterOfficeUnitOrgNameEn.get(vm_route_travelDTO.requesterOfficeUnitOrgNameEn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTorequesterOfficeUnitOrgNameBn.containsKey(vm_route_travelDTO.requesterOfficeUnitOrgNameBn)) {
//						mapOfVm_route_travelDTOTorequesterOfficeUnitOrgNameBn.put(vm_route_travelDTO.requesterOfficeUnitOrgNameBn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTorequesterOfficeUnitOrgNameBn.get(vm_route_travelDTO.requesterOfficeUnitOrgNameBn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTostatus.containsKey(vm_route_travelDTO.status)) {
//						mapOfVm_route_travelDTOTostatus.put(vm_route_travelDTO.status, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTostatus.get(vm_route_travelDTO.status).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTorouteId.containsKey(vm_route_travelDTO.routeId)) {
//						mapOfVm_route_travelDTOTorouteId.put(vm_route_travelDTO.routeId, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTorouteId.get(vm_route_travelDTO.routeId).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTostoppageId.containsKey(vm_route_travelDTO.stoppageId)) {
//						mapOfVm_route_travelDTOTostoppageId.put(vm_route_travelDTO.stoppageId, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTostoppageId.get(vm_route_travelDTO.stoppageId).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTofiscalYearId.containsKey(vm_route_travelDTO.fiscalYearId)) {
//						mapOfVm_route_travelDTOTofiscalYearId.put(vm_route_travelDTO.fiscalYearId, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTofiscalYearId.get(vm_route_travelDTO.fiscalYearId).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOToapprovedStartDate.containsKey(vm_route_travelDTO.approvedStartDate)) {
//						mapOfVm_route_travelDTOToapprovedStartDate.put(vm_route_travelDTO.approvedStartDate, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOToapprovedStartDate.get(vm_route_travelDTO.approvedStartDate).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTodecisionRemarks.containsKey(vm_route_travelDTO.decisionRemarks)) {
//						mapOfVm_route_travelDTOTodecisionRemarks.put(vm_route_travelDTO.decisionRemarks, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTodecisionRemarks.get(vm_route_travelDTO.decisionRemarks).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTofilesDropzone.containsKey(vm_route_travelDTO.filesDropzone)) {
//						mapOfVm_route_travelDTOTofilesDropzone.put(vm_route_travelDTO.filesDropzone, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTofilesDropzone.get(vm_route_travelDTO.filesDropzone).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOToapprovedDate.containsKey(vm_route_travelDTO.approvedDate)) {
//						mapOfVm_route_travelDTOToapprovedDate.put(vm_route_travelDTO.approvedDate, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOToapprovedDate.get(vm_route_travelDTO.approvedDate).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTowithdrawalDate.containsKey(vm_route_travelDTO.withdrawalDate)) {
//						mapOfVm_route_travelDTOTowithdrawalDate.put(vm_route_travelDTO.withdrawalDate, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTowithdrawalDate.get(vm_route_travelDTO.withdrawalDate).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOToapproverOrgId.containsKey(vm_route_travelDTO.approverOrgId)) {
//						mapOfVm_route_travelDTOToapproverOrgId.put(vm_route_travelDTO.approverOrgId, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOToapproverOrgId.get(vm_route_travelDTO.approverOrgId).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOToapproverOfficeId.containsKey(vm_route_travelDTO.approverOfficeId)) {
//						mapOfVm_route_travelDTOToapproverOfficeId.put(vm_route_travelDTO.approverOfficeId, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOToapproverOfficeId.get(vm_route_travelDTO.approverOfficeId).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOToapproverOfficeUnitId.containsKey(vm_route_travelDTO.approverOfficeUnitId)) {
//						mapOfVm_route_travelDTOToapproverOfficeUnitId.put(vm_route_travelDTO.approverOfficeUnitId, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOToapproverOfficeUnitId.get(vm_route_travelDTO.approverOfficeUnitId).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOToapproverEmpId.containsKey(vm_route_travelDTO.approverEmpId)) {
//						mapOfVm_route_travelDTOToapproverEmpId.put(vm_route_travelDTO.approverEmpId, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOToapproverEmpId.get(vm_route_travelDTO.approverEmpId).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOToapproverPhoneNum.containsKey(vm_route_travelDTO.approverPhoneNum)) {
//						mapOfVm_route_travelDTOToapproverPhoneNum.put(vm_route_travelDTO.approverPhoneNum, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOToapproverPhoneNum.get(vm_route_travelDTO.approverPhoneNum).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOToapproverNameEn.containsKey(vm_route_travelDTO.approverNameEn)) {
//						mapOfVm_route_travelDTOToapproverNameEn.put(vm_route_travelDTO.approverNameEn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOToapproverNameEn.get(vm_route_travelDTO.approverNameEn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOToapproverNameBn.containsKey(vm_route_travelDTO.approverNameBn)) {
//						mapOfVm_route_travelDTOToapproverNameBn.put(vm_route_travelDTO.approverNameBn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOToapproverNameBn.get(vm_route_travelDTO.approverNameBn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOToapproverOfficeNameEn.containsKey(vm_route_travelDTO.approverOfficeNameEn)) {
//						mapOfVm_route_travelDTOToapproverOfficeNameEn.put(vm_route_travelDTO.approverOfficeNameEn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOToapproverOfficeNameEn.get(vm_route_travelDTO.approverOfficeNameEn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOToapproverOfficeNameBn.containsKey(vm_route_travelDTO.approverOfficeNameBn)) {
//						mapOfVm_route_travelDTOToapproverOfficeNameBn.put(vm_route_travelDTO.approverOfficeNameBn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOToapproverOfficeNameBn.get(vm_route_travelDTO.approverOfficeNameBn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOToapproverOfficeUnitNameEn.containsKey(vm_route_travelDTO.approverOfficeUnitNameEn)) {
//						mapOfVm_route_travelDTOToapproverOfficeUnitNameEn.put(vm_route_travelDTO.approverOfficeUnitNameEn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOToapproverOfficeUnitNameEn.get(vm_route_travelDTO.approverOfficeUnitNameEn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOToapproverOfficeUnitNameBn.containsKey(vm_route_travelDTO.approverOfficeUnitNameBn)) {
//						mapOfVm_route_travelDTOToapproverOfficeUnitNameBn.put(vm_route_travelDTO.approverOfficeUnitNameBn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOToapproverOfficeUnitNameBn.get(vm_route_travelDTO.approverOfficeUnitNameBn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOToapproverOfficeUnitOrgNameEn.containsKey(vm_route_travelDTO.approverOfficeUnitOrgNameEn)) {
//						mapOfVm_route_travelDTOToapproverOfficeUnitOrgNameEn.put(vm_route_travelDTO.approverOfficeUnitOrgNameEn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOToapproverOfficeUnitOrgNameEn.get(vm_route_travelDTO.approverOfficeUnitOrgNameEn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOToapproverOfficeUnitOrgNameBn.containsKey(vm_route_travelDTO.approverOfficeUnitOrgNameBn)) {
//						mapOfVm_route_travelDTOToapproverOfficeUnitOrgNameBn.put(vm_route_travelDTO.approverOfficeUnitOrgNameBn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOToapproverOfficeUnitOrgNameBn.get(vm_route_travelDTO.approverOfficeUnitOrgNameBn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTowithdrawerOrgId.containsKey(vm_route_travelDTO.withdrawerOrgId)) {
//						mapOfVm_route_travelDTOTowithdrawerOrgId.put(vm_route_travelDTO.withdrawerOrgId, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTowithdrawerOrgId.get(vm_route_travelDTO.withdrawerOrgId).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTowithdrawerOfficeId.containsKey(vm_route_travelDTO.withdrawerOfficeId)) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeId.put(vm_route_travelDTO.withdrawerOfficeId, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTowithdrawerOfficeId.get(vm_route_travelDTO.withdrawerOfficeId).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTowithdrawerOfficeUnitId.containsKey(vm_route_travelDTO.withdrawerOfficeUnitId)) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeUnitId.put(vm_route_travelDTO.withdrawerOfficeUnitId, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTowithdrawerOfficeUnitId.get(vm_route_travelDTO.withdrawerOfficeUnitId).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTowithdrawerEmpId.containsKey(vm_route_travelDTO.withdrawerEmpId)) {
//						mapOfVm_route_travelDTOTowithdrawerEmpId.put(vm_route_travelDTO.withdrawerEmpId, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTowithdrawerEmpId.get(vm_route_travelDTO.withdrawerEmpId).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTowithdrawerPhoneNum.containsKey(vm_route_travelDTO.withdrawerPhoneNum)) {
//						mapOfVm_route_travelDTOTowithdrawerPhoneNum.put(vm_route_travelDTO.withdrawerPhoneNum, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTowithdrawerPhoneNum.get(vm_route_travelDTO.withdrawerPhoneNum).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTowithdrawerNameEn.containsKey(vm_route_travelDTO.withdrawerNameEn)) {
//						mapOfVm_route_travelDTOTowithdrawerNameEn.put(vm_route_travelDTO.withdrawerNameEn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTowithdrawerNameEn.get(vm_route_travelDTO.withdrawerNameEn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTowithdrawerNameBn.containsKey(vm_route_travelDTO.withdrawerNameBn)) {
//						mapOfVm_route_travelDTOTowithdrawerNameBn.put(vm_route_travelDTO.withdrawerNameBn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTowithdrawerNameBn.get(vm_route_travelDTO.withdrawerNameBn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTowithdrawerOfficeNameEn.containsKey(vm_route_travelDTO.withdrawerOfficeNameEn)) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeNameEn.put(vm_route_travelDTO.withdrawerOfficeNameEn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTowithdrawerOfficeNameEn.get(vm_route_travelDTO.withdrawerOfficeNameEn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTowithdrawerOfficeNameBn.containsKey(vm_route_travelDTO.withdrawerOfficeNameBn)) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeNameBn.put(vm_route_travelDTO.withdrawerOfficeNameBn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTowithdrawerOfficeNameBn.get(vm_route_travelDTO.withdrawerOfficeNameBn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTowithdrawerOfficeUnitNameEn.containsKey(vm_route_travelDTO.withdrawerOfficeUnitNameEn)) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeUnitNameEn.put(vm_route_travelDTO.withdrawerOfficeUnitNameEn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTowithdrawerOfficeUnitNameEn.get(vm_route_travelDTO.withdrawerOfficeUnitNameEn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTowithdrawerOfficeUnitNameBn.containsKey(vm_route_travelDTO.withdrawerOfficeUnitNameBn)) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeUnitNameBn.put(vm_route_travelDTO.withdrawerOfficeUnitNameBn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTowithdrawerOfficeUnitNameBn.get(vm_route_travelDTO.withdrawerOfficeUnitNameBn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTowithdrawerOfficeUnitOrgNameEn.containsKey(vm_route_travelDTO.withdrawerOfficeUnitOrgNameEn)) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeUnitOrgNameEn.put(vm_route_travelDTO.withdrawerOfficeUnitOrgNameEn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTowithdrawerOfficeUnitOrgNameEn.get(vm_route_travelDTO.withdrawerOfficeUnitOrgNameEn).add(vm_route_travelDTO);
//
//					if( ! mapOfVm_route_travelDTOTowithdrawerOfficeUnitOrgNameBn.containsKey(vm_route_travelDTO.withdrawerOfficeUnitOrgNameBn)) {
//						mapOfVm_route_travelDTOTowithdrawerOfficeUnitOrgNameBn.put(vm_route_travelDTO.withdrawerOfficeUnitOrgNameBn, new HashSet<>());
//					}
//					mapOfVm_route_travelDTOTowithdrawerOfficeUnitOrgNameBn.get(vm_route_travelDTO.withdrawerOfficeUnitOrgNameBn).add(vm_route_travelDTO);
//
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public Vm_route_travelDTO clone(Vm_route_travelDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Vm_route_travelDTO.class);
	}

	public List<Vm_route_travelDTO> clone(List<Vm_route_travelDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public Vm_route_travelDTO getVm_route_travelDTOByIDWithoutClone( long ID){
		return mapOfVm_route_travelDTOToiD.get(ID);
	}
	
	public List<Vm_route_travelDTO> getVm_route_travelList() {
		List <Vm_route_travelDTO> vm_route_travels = new ArrayList<Vm_route_travelDTO>(this.mapOfVm_route_travelDTOToiD.values());
		return vm_route_travels;
	}
	
	
	public Vm_route_travelDTO getVm_route_travelDTOByID( long ID){
		return clone(mapOfVm_route_travelDTOToiD.get(ID));
	}
	
	
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByinserted_by_user_id(long inserted_by_user_id) {
//		return new ArrayList<>( mapOfVm_route_travelDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
//		return new ArrayList<>( mapOfVm_route_travelDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByinsertion_date(long insertion_date) {
//		return new ArrayList<>( mapOfVm_route_travelDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOBylastModificationTime(long lastModificationTime) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOBysearch_column(String search_column) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByrequested_stoppage_id(long requested_stoppage_id) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTorequestedStoppageId.getOrDefault(requested_stoppage_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByrequested_stoppage_name(String requested_stoppage_name) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTorequestedStoppageName.getOrDefault(requested_stoppage_name,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByremarks(String remarks) {
//		return new ArrayList<>( mapOfVm_route_travelDTOToremarks.getOrDefault(remarks,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByrequested_start_date(long requested_start_date) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTorequestedStartDate.getOrDefault(requested_start_date,new HashSet<>()));
//	}
//
//
	public List<Vm_route_travelDTO> getVm_route_travelDTOByrequester_org_id(long requester_org_id) {
		return clone(new ArrayList<>( mapOfVm_route_travelDTOTorequesterOrgId.getOrDefault(requester_org_id,new HashSet<>())));
	}

	public List<Vm_route_travelDTO> getApprovedVm_route_travelDTOByrequester_org_id(long requester_org_id) {
		List<Vm_route_travelDTO> vm_route_travelDTOS = new ArrayList<>( mapOfVm_route_travelDTOTorequesterOrgId.getOrDefault(requester_org_id,new HashSet<>()));
		return vm_route_travelDTOS
				.stream()
				.filter(vm_route_travelDTO -> vm_route_travelDTO.status == CommonApprovalStatus.SATISFIED.getValue()
						|| vm_route_travelDTO.status == CommonApprovalStatus.WITHDRAWN.getValue() )
				.map(vm_route_travelDTO -> clone(vm_route_travelDTO))
				.collect(Collectors.toList());
	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByrequester_office_id(long requester_office_id) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTorequesterOfficeId.getOrDefault(requester_office_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByrequester_office_unit_id(long requester_office_unit_id) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTorequesterOfficeUnitId.getOrDefault(requester_office_unit_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByrequester_emp_id(long requester_emp_id) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTorequesterEmpId.getOrDefault(requester_emp_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByrequester_phone_num(String requester_phone_num) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTorequesterPhoneNum.getOrDefault(requester_phone_num,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByrequester_name_en(String requester_name_en) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTorequesterNameEn.getOrDefault(requester_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByrequester_name_bn(String requester_name_bn) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTorequesterNameBn.getOrDefault(requester_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByrequester_office_name_en(String requester_office_name_en) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTorequesterOfficeNameEn.getOrDefault(requester_office_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByrequester_office_name_bn(String requester_office_name_bn) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTorequesterOfficeNameBn.getOrDefault(requester_office_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByrequester_office_unit_name_en(String requester_office_unit_name_en) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTorequesterOfficeUnitNameEn.getOrDefault(requester_office_unit_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByrequester_office_unit_name_bn(String requester_office_unit_name_bn) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTorequesterOfficeUnitNameBn.getOrDefault(requester_office_unit_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByrequester_office_unit_org_name_en(String requester_office_unit_org_name_en) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTorequesterOfficeUnitOrgNameEn.getOrDefault(requester_office_unit_org_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByrequester_office_unit_org_name_bn(String requester_office_unit_org_name_bn) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTorequesterOfficeUnitOrgNameBn.getOrDefault(requester_office_unit_org_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOBystatus(int status) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTostatus.getOrDefault(status,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByroute_id(long route_id) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTorouteId.getOrDefault(route_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOBystoppage_id(long stoppage_id) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTostoppageId.getOrDefault(stoppage_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByfiscal_year_id(long fiscal_year_id) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTofiscalYearId.getOrDefault(fiscal_year_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByapproved_start_date(long approved_start_date) {
//		return new ArrayList<>( mapOfVm_route_travelDTOToapprovedStartDate.getOrDefault(approved_start_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOBydecision_remarks(String decision_remarks) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTodecisionRemarks.getOrDefault(decision_remarks,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByfiles_dropzone(long files_dropzone) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTofilesDropzone.getOrDefault(files_dropzone,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByapproved_date(long approved_date) {
//		return new ArrayList<>( mapOfVm_route_travelDTOToapprovedDate.getOrDefault(approved_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOBywithdrawal_date(long withdrawal_date) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTowithdrawalDate.getOrDefault(withdrawal_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByapprover_org_id(long approver_org_id) {
//		return new ArrayList<>( mapOfVm_route_travelDTOToapproverOrgId.getOrDefault(approver_org_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByapprover_office_id(long approver_office_id) {
//		return new ArrayList<>( mapOfVm_route_travelDTOToapproverOfficeId.getOrDefault(approver_office_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByapprover_office_unit_id(long approver_office_unit_id) {
//		return new ArrayList<>( mapOfVm_route_travelDTOToapproverOfficeUnitId.getOrDefault(approver_office_unit_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByapprover_emp_id(long approver_emp_id) {
//		return new ArrayList<>( mapOfVm_route_travelDTOToapproverEmpId.getOrDefault(approver_emp_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByapprover_phone_num(String approver_phone_num) {
//		return new ArrayList<>( mapOfVm_route_travelDTOToapproverPhoneNum.getOrDefault(approver_phone_num,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByapprover_name_en(String approver_name_en) {
//		return new ArrayList<>( mapOfVm_route_travelDTOToapproverNameEn.getOrDefault(approver_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByapprover_name_bn(String approver_name_bn) {
//		return new ArrayList<>( mapOfVm_route_travelDTOToapproverNameBn.getOrDefault(approver_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByapprover_office_name_en(String approver_office_name_en) {
//		return new ArrayList<>( mapOfVm_route_travelDTOToapproverOfficeNameEn.getOrDefault(approver_office_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByapprover_office_name_bn(String approver_office_name_bn) {
//		return new ArrayList<>( mapOfVm_route_travelDTOToapproverOfficeNameBn.getOrDefault(approver_office_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByapprover_office_unit_name_en(String approver_office_unit_name_en) {
//		return new ArrayList<>( mapOfVm_route_travelDTOToapproverOfficeUnitNameEn.getOrDefault(approver_office_unit_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByapprover_office_unit_name_bn(String approver_office_unit_name_bn) {
//		return new ArrayList<>( mapOfVm_route_travelDTOToapproverOfficeUnitNameBn.getOrDefault(approver_office_unit_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByapprover_office_unit_org_name_en(String approver_office_unit_org_name_en) {
//		return new ArrayList<>( mapOfVm_route_travelDTOToapproverOfficeUnitOrgNameEn.getOrDefault(approver_office_unit_org_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOByapprover_office_unit_org_name_bn(String approver_office_unit_org_name_bn) {
//		return new ArrayList<>( mapOfVm_route_travelDTOToapproverOfficeUnitOrgNameBn.getOrDefault(approver_office_unit_org_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOBywithdrawer_org_id(long withdrawer_org_id) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTowithdrawerOrgId.getOrDefault(withdrawer_org_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOBywithdrawer_office_id(long withdrawer_office_id) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTowithdrawerOfficeId.getOrDefault(withdrawer_office_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOBywithdrawer_office_unit_id(long withdrawer_office_unit_id) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTowithdrawerOfficeUnitId.getOrDefault(withdrawer_office_unit_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOBywithdrawer_emp_id(long withdrawer_emp_id) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTowithdrawerEmpId.getOrDefault(withdrawer_emp_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOBywithdrawer_phone_num(String withdrawer_phone_num) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTowithdrawerPhoneNum.getOrDefault(withdrawer_phone_num,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOBywithdrawer_name_en(String withdrawer_name_en) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTowithdrawerNameEn.getOrDefault(withdrawer_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOBywithdrawer_name_bn(String withdrawer_name_bn) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTowithdrawerNameBn.getOrDefault(withdrawer_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOBywithdrawer_office_name_en(String withdrawer_office_name_en) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTowithdrawerOfficeNameEn.getOrDefault(withdrawer_office_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOBywithdrawer_office_name_bn(String withdrawer_office_name_bn) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTowithdrawerOfficeNameBn.getOrDefault(withdrawer_office_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOBywithdrawer_office_unit_name_en(String withdrawer_office_unit_name_en) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTowithdrawerOfficeUnitNameEn.getOrDefault(withdrawer_office_unit_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOBywithdrawer_office_unit_name_bn(String withdrawer_office_unit_name_bn) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTowithdrawerOfficeUnitNameBn.getOrDefault(withdrawer_office_unit_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOBywithdrawer_office_unit_org_name_en(String withdrawer_office_unit_org_name_en) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTowithdrawerOfficeUnitOrgNameEn.getOrDefault(withdrawer_office_unit_org_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travelDTO> getVm_route_travelDTOBywithdrawer_office_unit_org_name_bn(String withdrawer_office_unit_org_name_bn) {
//		return new ArrayList<>( mapOfVm_route_travelDTOTowithdrawerOfficeUnitOrgNameBn.getOrDefault(withdrawer_office_unit_org_name_bn,new HashSet<>()));
//	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_route_travel";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


