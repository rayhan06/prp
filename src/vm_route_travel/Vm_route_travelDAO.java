package vm_route_travel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import common.ConnectionAndStatementUtil;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;
import vm_requisition.CommonApprovalStatus;
import vm_requisition.Vm_requisitionDTO;

public class Vm_route_travelDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Vm_route_travelDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Vm_route_travelMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"search_column",
			"requested_stoppage_id",
			"requested_stoppage_name",
			"remarks",
			"requested_start_date",
			"requester_org_id",
			"requester_office_id",
			"requester_office_unit_id",
			"requester_emp_id",
			"requester_phone_num",
			"requester_name_en",
			"requester_name_bn",
			"requester_office_name_en",
			"requester_office_name_bn",
			"requester_office_unit_name_en",
			"requester_office_unit_name_bn",
			"requester_office_unit_org_name_en",
			"requester_office_unit_org_name_bn",
			"status",
			"route_id",
			"stoppage_id",
			"fiscal_year_id",
			"approved_start_date",
			"decision_remarks",
			"files_dropzone",
			"approved_date",
			"withdrawal_date",
			"approver_org_id",
			"approver_office_id",
			"approver_office_unit_id",
			"approver_emp_id",
			"approver_phone_num",
			"approver_name_en",
			"approver_name_bn",
			"approver_office_name_en",
			"approver_office_name_bn",
			"approver_office_unit_name_en",
			"approver_office_unit_name_bn",
			"approver_office_unit_org_name_en",
			"approver_office_unit_org_name_bn",
			"withdrawer_org_id",
			"withdrawer_office_id",
			"withdrawer_office_unit_id",
			"withdrawer_emp_id",
			"withdrawer_phone_num",
			"withdrawer_name_en",
			"withdrawer_name_bn",
			"withdrawer_office_name_en",
			"withdrawer_office_name_bn",
			"withdrawer_office_unit_name_en",
			"withdrawer_office_unit_name_bn",
			"withdrawer_office_unit_org_name_en",
			"withdrawer_office_unit_org_name_bn",
				"isDeleted",
				"lastModificationTime"
		};
	}
	
	public Vm_route_travelDAO()
	{
		this("vm_route_travel");		
	}
	
	public void setSearchColumn(Vm_route_travelDTO vm_route_travelDTO)
	{
		vm_route_travelDTO.searchColumn = "";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.requestedStoppageName + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.remarks + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.requesterPhoneNum + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.requesterNameEn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.requesterNameBn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.requesterOfficeNameEn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.requesterOfficeNameBn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.requesterOfficeUnitNameEn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.requesterOfficeUnitNameBn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.requesterOfficeUnitOrgNameEn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.requesterOfficeUnitOrgNameBn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.decisionRemarks + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.approverPhoneNum + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.approverNameEn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.approverNameBn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.approverOfficeNameEn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.approverOfficeNameBn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.approverOfficeUnitNameEn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.approverOfficeUnitNameBn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.approverOfficeUnitOrgNameEn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.approverOfficeUnitOrgNameBn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.withdrawerPhoneNum + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.withdrawerNameEn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.withdrawerNameBn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.withdrawerOfficeNameEn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.withdrawerOfficeNameBn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.withdrawerOfficeUnitNameEn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.withdrawerOfficeUnitNameBn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.withdrawerOfficeUnitOrgNameEn + " ";
		vm_route_travelDTO.searchColumn += vm_route_travelDTO.withdrawerOfficeUnitOrgNameBn + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Vm_route_travelDTO vm_route_travelDTO = (Vm_route_travelDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(vm_route_travelDTO);
		if(isInsert)
		{
			ps.setObject(index++,vm_route_travelDTO.iD);
		}
		ps.setObject(index++,vm_route_travelDTO.insertedByUserId);
		ps.setObject(index++,vm_route_travelDTO.insertedByOrganogramId);
		ps.setObject(index++,vm_route_travelDTO.insertionDate);
		ps.setObject(index++,vm_route_travelDTO.searchColumn);
		ps.setObject(index++,vm_route_travelDTO.requestedStoppageId);
		ps.setObject(index++,vm_route_travelDTO.requestedStoppageName);
		ps.setObject(index++,vm_route_travelDTO.remarks);
		ps.setObject(index++,vm_route_travelDTO.requestedStartDate);
		ps.setObject(index++,vm_route_travelDTO.requesterOrgId);
		ps.setObject(index++,vm_route_travelDTO.requesterOfficeId);
		ps.setObject(index++,vm_route_travelDTO.requesterOfficeUnitId);
		ps.setObject(index++,vm_route_travelDTO.requesterEmpId);
		ps.setObject(index++,vm_route_travelDTO.requesterPhoneNum);
		ps.setObject(index++,vm_route_travelDTO.requesterNameEn);
		ps.setObject(index++,vm_route_travelDTO.requesterNameBn);
		ps.setObject(index++,vm_route_travelDTO.requesterOfficeNameEn);
		ps.setObject(index++,vm_route_travelDTO.requesterOfficeNameBn);
		ps.setObject(index++,vm_route_travelDTO.requesterOfficeUnitNameEn);
		ps.setObject(index++,vm_route_travelDTO.requesterOfficeUnitNameBn);
		ps.setObject(index++,vm_route_travelDTO.requesterOfficeUnitOrgNameEn);
		ps.setObject(index++,vm_route_travelDTO.requesterOfficeUnitOrgNameBn);
		ps.setObject(index++,vm_route_travelDTO.status);
		ps.setObject(index++,vm_route_travelDTO.routeId);
		ps.setObject(index++,vm_route_travelDTO.stoppageId);
		ps.setObject(index++,vm_route_travelDTO.fiscalYearId);
		ps.setObject(index++,vm_route_travelDTO.approvedStartDate);
		ps.setObject(index++,vm_route_travelDTO.decisionRemarks);
		ps.setObject(index++,vm_route_travelDTO.filesDropzone);
		ps.setObject(index++,vm_route_travelDTO.approvedDate);
		ps.setObject(index++,vm_route_travelDTO.withdrawalDate);
		ps.setObject(index++,vm_route_travelDTO.approverOrgId);
		ps.setObject(index++,vm_route_travelDTO.approverOfficeId);
		ps.setObject(index++,vm_route_travelDTO.approverOfficeUnitId);
		ps.setObject(index++,vm_route_travelDTO.approverEmpId);
		ps.setObject(index++,vm_route_travelDTO.approverPhoneNum);
		ps.setObject(index++,vm_route_travelDTO.approverNameEn);
		ps.setObject(index++,vm_route_travelDTO.approverNameBn);
		ps.setObject(index++,vm_route_travelDTO.approverOfficeNameEn);
		ps.setObject(index++,vm_route_travelDTO.approverOfficeNameBn);
		ps.setObject(index++,vm_route_travelDTO.approverOfficeUnitNameEn);
		ps.setObject(index++,vm_route_travelDTO.approverOfficeUnitNameBn);
		ps.setObject(index++,vm_route_travelDTO.approverOfficeUnitOrgNameEn);
		ps.setObject(index++,vm_route_travelDTO.approverOfficeUnitOrgNameBn);
		ps.setObject(index++,vm_route_travelDTO.withdrawerOrgId);
		ps.setObject(index++,vm_route_travelDTO.withdrawerOfficeId);
		ps.setObject(index++,vm_route_travelDTO.withdrawerOfficeUnitId);
		ps.setObject(index++,vm_route_travelDTO.withdrawerEmpId);
		ps.setObject(index++,vm_route_travelDTO.withdrawerPhoneNum);
		ps.setObject(index++,vm_route_travelDTO.withdrawerNameEn);
		ps.setObject(index++,vm_route_travelDTO.withdrawerNameBn);
		ps.setObject(index++,vm_route_travelDTO.withdrawerOfficeNameEn);
		ps.setObject(index++,vm_route_travelDTO.withdrawerOfficeNameBn);
		ps.setObject(index++,vm_route_travelDTO.withdrawerOfficeUnitNameEn);
		ps.setObject(index++,vm_route_travelDTO.withdrawerOfficeUnitNameBn);
		ps.setObject(index++,vm_route_travelDTO.withdrawerOfficeUnitOrgNameEn);
		ps.setObject(index++,vm_route_travelDTO.withdrawerOfficeUnitOrgNameBn);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Vm_route_travelDTO vm_route_travelDTO, ResultSet rs) throws SQLException
	{
		vm_route_travelDTO.iD = rs.getLong("ID");
		vm_route_travelDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		vm_route_travelDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
		vm_route_travelDTO.insertionDate = rs.getLong("insertion_date");
		vm_route_travelDTO.isDeleted = rs.getInt("isDeleted");
		vm_route_travelDTO.lastModificationTime = rs.getLong("lastModificationTime");
		vm_route_travelDTO.searchColumn = rs.getString("search_column");
		vm_route_travelDTO.requestedStoppageId = rs.getLong("requested_stoppage_id");
		vm_route_travelDTO.requestedStoppageName = rs.getString("requested_stoppage_name");
		vm_route_travelDTO.remarks = rs.getString("remarks");
		vm_route_travelDTO.requestedStartDate = rs.getLong("requested_start_date");
		vm_route_travelDTO.requesterOrgId = rs.getLong("requester_org_id");
		vm_route_travelDTO.requesterOfficeId = rs.getLong("requester_office_id");
		vm_route_travelDTO.requesterOfficeUnitId = rs.getLong("requester_office_unit_id");
		vm_route_travelDTO.requesterEmpId = rs.getLong("requester_emp_id");
		vm_route_travelDTO.requesterPhoneNum = rs.getString("requester_phone_num");
		vm_route_travelDTO.requesterNameEn = rs.getString("requester_name_en");
		vm_route_travelDTO.requesterNameBn = rs.getString("requester_name_bn");
		vm_route_travelDTO.requesterOfficeNameEn = rs.getString("requester_office_name_en");
		vm_route_travelDTO.requesterOfficeNameBn = rs.getString("requester_office_name_bn");
		vm_route_travelDTO.requesterOfficeUnitNameEn = rs.getString("requester_office_unit_name_en");
		vm_route_travelDTO.requesterOfficeUnitNameBn = rs.getString("requester_office_unit_name_bn");
		vm_route_travelDTO.requesterOfficeUnitOrgNameEn = rs.getString("requester_office_unit_org_name_en");
		vm_route_travelDTO.requesterOfficeUnitOrgNameBn = rs.getString("requester_office_unit_org_name_bn");
		vm_route_travelDTO.status = rs.getInt("status");
		vm_route_travelDTO.routeId = rs.getLong("route_id");
		vm_route_travelDTO.stoppageId = rs.getLong("stoppage_id");
		vm_route_travelDTO.fiscalYearId = rs.getLong("fiscal_year_id");
		vm_route_travelDTO.approvedStartDate = rs.getLong("approved_start_date");
		vm_route_travelDTO.decisionRemarks = rs.getString("decision_remarks");
		vm_route_travelDTO.filesDropzone = rs.getLong("files_dropzone");
		vm_route_travelDTO.approvedDate = rs.getLong("approved_date");
		vm_route_travelDTO.withdrawalDate = rs.getLong("withdrawal_date");
		vm_route_travelDTO.approverOrgId = rs.getLong("approver_org_id");
		vm_route_travelDTO.approverOfficeId = rs.getLong("approver_office_id");
		vm_route_travelDTO.approverOfficeUnitId = rs.getLong("approver_office_unit_id");
		vm_route_travelDTO.approverEmpId = rs.getLong("approver_emp_id");
		vm_route_travelDTO.approverPhoneNum = rs.getString("approver_phone_num");
		vm_route_travelDTO.approverNameEn = rs.getString("approver_name_en");
		vm_route_travelDTO.approverNameBn = rs.getString("approver_name_bn");
		vm_route_travelDTO.approverOfficeNameEn = rs.getString("approver_office_name_en");
		vm_route_travelDTO.approverOfficeNameBn = rs.getString("approver_office_name_bn");
		vm_route_travelDTO.approverOfficeUnitNameEn = rs.getString("approver_office_unit_name_en");
		vm_route_travelDTO.approverOfficeUnitNameBn = rs.getString("approver_office_unit_name_bn");
		vm_route_travelDTO.approverOfficeUnitOrgNameEn = rs.getString("approver_office_unit_org_name_en");
		vm_route_travelDTO.approverOfficeUnitOrgNameBn = rs.getString("approver_office_unit_org_name_bn");
		vm_route_travelDTO.withdrawerOrgId = rs.getLong("withdrawer_org_id");
		vm_route_travelDTO.withdrawerOfficeId = rs.getLong("withdrawer_office_id");
		vm_route_travelDTO.withdrawerOfficeUnitId = rs.getLong("withdrawer_office_unit_id");
		vm_route_travelDTO.withdrawerEmpId = rs.getLong("withdrawer_emp_id");
		vm_route_travelDTO.withdrawerPhoneNum = rs.getString("withdrawer_phone_num");
		vm_route_travelDTO.withdrawerNameEn = rs.getString("withdrawer_name_en");
		vm_route_travelDTO.withdrawerNameBn = rs.getString("withdrawer_name_bn");
		vm_route_travelDTO.withdrawerOfficeNameEn = rs.getString("withdrawer_office_name_en");
		vm_route_travelDTO.withdrawerOfficeNameBn = rs.getString("withdrawer_office_name_bn");
		vm_route_travelDTO.withdrawerOfficeUnitNameEn = rs.getString("withdrawer_office_unit_name_en");
		vm_route_travelDTO.withdrawerOfficeUnitNameBn = rs.getString("withdrawer_office_unit_name_bn");
		vm_route_travelDTO.withdrawerOfficeUnitOrgNameEn = rs.getString("withdrawer_office_unit_org_name_en");
		vm_route_travelDTO.withdrawerOfficeUnitOrgNameBn = rs.getString("withdrawer_office_unit_org_name_bn");
	}


	public Vm_route_travelDTO build(ResultSet rs)
	{
		try
		{
			Vm_route_travelDTO vm_route_travelDTO = new Vm_route_travelDTO();
			vm_route_travelDTO.iD = rs.getLong("ID");
			vm_route_travelDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			vm_route_travelDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			vm_route_travelDTO.insertionDate = rs.getLong("insertion_date");
			vm_route_travelDTO.isDeleted = rs.getInt("isDeleted");
			vm_route_travelDTO.lastModificationTime = rs.getLong("lastModificationTime");
			vm_route_travelDTO.searchColumn = rs.getString("search_column");
			vm_route_travelDTO.requestedStoppageId = rs.getLong("requested_stoppage_id");
			vm_route_travelDTO.requestedStoppageName = rs.getString("requested_stoppage_name");
			vm_route_travelDTO.remarks = rs.getString("remarks");
			vm_route_travelDTO.requestedStartDate = rs.getLong("requested_start_date");
			vm_route_travelDTO.requesterOrgId = rs.getLong("requester_org_id");
			vm_route_travelDTO.requesterOfficeId = rs.getLong("requester_office_id");
			vm_route_travelDTO.requesterOfficeUnitId = rs.getLong("requester_office_unit_id");
			vm_route_travelDTO.requesterEmpId = rs.getLong("requester_emp_id");
			vm_route_travelDTO.requesterPhoneNum = rs.getString("requester_phone_num");
			vm_route_travelDTO.requesterNameEn = rs.getString("requester_name_en");
			vm_route_travelDTO.requesterNameBn = rs.getString("requester_name_bn");
			vm_route_travelDTO.requesterOfficeNameEn = rs.getString("requester_office_name_en");
			vm_route_travelDTO.requesterOfficeNameBn = rs.getString("requester_office_name_bn");
			vm_route_travelDTO.requesterOfficeUnitNameEn = rs.getString("requester_office_unit_name_en");
			vm_route_travelDTO.requesterOfficeUnitNameBn = rs.getString("requester_office_unit_name_bn");
			vm_route_travelDTO.requesterOfficeUnitOrgNameEn = rs.getString("requester_office_unit_org_name_en");
			vm_route_travelDTO.requesterOfficeUnitOrgNameBn = rs.getString("requester_office_unit_org_name_bn");
			vm_route_travelDTO.status = rs.getInt("status");
			vm_route_travelDTO.routeId = rs.getLong("route_id");
			vm_route_travelDTO.stoppageId = rs.getLong("stoppage_id");
			vm_route_travelDTO.fiscalYearId = rs.getLong("fiscal_year_id");
			vm_route_travelDTO.approvedStartDate = rs.getLong("approved_start_date");
			vm_route_travelDTO.decisionRemarks = rs.getString("decision_remarks");
			vm_route_travelDTO.filesDropzone = rs.getLong("files_dropzone");
			vm_route_travelDTO.approvedDate = rs.getLong("approved_date");
			vm_route_travelDTO.withdrawalDate = rs.getLong("withdrawal_date");
			vm_route_travelDTO.approverOrgId = rs.getLong("approver_org_id");
			vm_route_travelDTO.approverOfficeId = rs.getLong("approver_office_id");
			vm_route_travelDTO.approverOfficeUnitId = rs.getLong("approver_office_unit_id");
			vm_route_travelDTO.approverEmpId = rs.getLong("approver_emp_id");
			vm_route_travelDTO.approverPhoneNum = rs.getString("approver_phone_num");
			vm_route_travelDTO.approverNameEn = rs.getString("approver_name_en");
			vm_route_travelDTO.approverNameBn = rs.getString("approver_name_bn");
			vm_route_travelDTO.approverOfficeNameEn = rs.getString("approver_office_name_en");
			vm_route_travelDTO.approverOfficeNameBn = rs.getString("approver_office_name_bn");
			vm_route_travelDTO.approverOfficeUnitNameEn = rs.getString("approver_office_unit_name_en");
			vm_route_travelDTO.approverOfficeUnitNameBn = rs.getString("approver_office_unit_name_bn");
			vm_route_travelDTO.approverOfficeUnitOrgNameEn = rs.getString("approver_office_unit_org_name_en");
			vm_route_travelDTO.approverOfficeUnitOrgNameBn = rs.getString("approver_office_unit_org_name_bn");
			vm_route_travelDTO.withdrawerOrgId = rs.getLong("withdrawer_org_id");
			vm_route_travelDTO.withdrawerOfficeId = rs.getLong("withdrawer_office_id");
			vm_route_travelDTO.withdrawerOfficeUnitId = rs.getLong("withdrawer_office_unit_id");
			vm_route_travelDTO.withdrawerEmpId = rs.getLong("withdrawer_emp_id");
			vm_route_travelDTO.withdrawerPhoneNum = rs.getString("withdrawer_phone_num");
			vm_route_travelDTO.withdrawerNameEn = rs.getString("withdrawer_name_en");
			vm_route_travelDTO.withdrawerNameBn = rs.getString("withdrawer_name_bn");
			vm_route_travelDTO.withdrawerOfficeNameEn = rs.getString("withdrawer_office_name_en");
			vm_route_travelDTO.withdrawerOfficeNameBn = rs.getString("withdrawer_office_name_bn");
			vm_route_travelDTO.withdrawerOfficeUnitNameEn = rs.getString("withdrawer_office_unit_name_en");
			vm_route_travelDTO.withdrawerOfficeUnitNameBn = rs.getString("withdrawer_office_unit_name_bn");
			vm_route_travelDTO.withdrawerOfficeUnitOrgNameEn = rs.getString("withdrawer_office_unit_org_name_en");
			vm_route_travelDTO.withdrawerOfficeUnitOrgNameBn = rs.getString("withdrawer_office_unit_org_name_bn");
			return vm_route_travelDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
		
	

	//need another getter for repository
	public Vm_route_travelDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		Vm_route_travelDTO vm_route_travelDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
		return vm_route_travelDTO;
	}




	public List<Vm_route_travelDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	}
	
	

	
	
	
	//add repository
	public List<Vm_route_travelDTO> getAllVm_route_travel (boolean isFirstReload)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}

	
	public List<Vm_route_travelDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}


	public List<Vm_route_travelDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		List<Object> objectList = new ArrayList<Object>();

		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat, objectList);
		return ConnectionAndStatementUtil.getListOfT(sql,objectList,this::build);
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ?";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
						|| str.equals("requested_stoppage_name")
						|| str.equals("remarks")
						|| str.equals("requested_start_date")
						|| str.equals("requested_start_date_start")
						|| str.equals("requested_start_date_end")
						|| str.equals("requester_phone_num")
						|| str.equals("requester_name_en")
						|| str.equals("requester_name_bn")
						|| str.equals("requester_office_name_en")
						|| str.equals("requester_office_name_bn")
						|| str.equals("requester_office_unit_name_en")
						|| str.equals("requester_office_unit_name_bn")
						|| str.equals("requester_office_unit_org_name_en")
						|| str.equals("requester_office_unit_org_name_bn")
						|| str.equals("approved_start_date_start")
						|| str.equals("approved_start_date_end")
						|| str.equals("decision_remarks")
						|| str.equals("approved_date_start")
						|| str.equals("approved_date_end")
						|| str.equals("withdrawal_date_start")
						|| str.equals("withdrawal_date_end")
						|| str.equals("approver_phone_num")
						|| str.equals("approver_name_en")
						|| str.equals("approver_name_bn")
						|| str.equals("approver_office_name_en")
						|| str.equals("approver_office_name_bn")
						|| str.equals("approver_office_unit_name_en")
						|| str.equals("approver_office_unit_name_bn")
						|| str.equals("approver_office_unit_org_name_en")
						|| str.equals("approver_office_unit_org_name_bn")
						|| str.equals("withdrawer_phone_num")
						|| str.equals("withdrawer_name_en")
						|| str.equals("withdrawer_name_bn")
						|| str.equals("withdrawer_office_name_en")
						|| str.equals("withdrawer_office_name_bn")
						|| str.equals("withdrawer_office_unit_name_en")
						|| str.equals("withdrawer_office_unit_name_bn")
						|| str.equals("withdrawer_office_unit_org_name_en")
						|| str.equals("withdrawer_office_unit_org_name_bn")
						|| str.equals("status")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("requested_stoppage_name"))
					{
						AllFieldSql += "" + tableName + ".requested_stoppage_name like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("remarks"))
					{
						AllFieldSql += "" + tableName + ".remarks like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requested_start_date"))
					{
						AllFieldSql += "" + tableName + ".requested_start_date = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("requested_start_date_start"))
					{
						AllFieldSql += "" + tableName + ".requested_start_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("requested_start_date_end"))
					{
						AllFieldSql += "" + tableName + ".requested_start_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("requester_phone_num"))
					{
						AllFieldSql += "" + tableName + ".requester_phone_num like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_office_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_office_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_org_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_org_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_org_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_org_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approved_start_date_start"))
					{
						AllFieldSql += "" + tableName + ".approved_start_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("approved_start_date_end"))
					{
						AllFieldSql += "" + tableName + ".approved_start_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("decision_remarks"))
					{
						AllFieldSql += "" + tableName + ".decision_remarks like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approved_date_start"))
					{
						AllFieldSql += "" + tableName + ".approved_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("approved_date_end"))
					{
						AllFieldSql += "" + tableName + ".approved_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("withdrawal_date_start"))
					{
						AllFieldSql += "" + tableName + ".withdrawal_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("withdrawal_date_end"))
					{
						AllFieldSql += "" + tableName + ".withdrawal_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("approver_phone_num"))
					{
						AllFieldSql += "" + tableName + ".approver_phone_num like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_name_en"))
					{
						AllFieldSql += "" + tableName + ".approver_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_name_bn"))
					{
						AllFieldSql += "" + tableName + ".approver_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_name_en"))
					{
						AllFieldSql += "" + tableName + ".approver_office_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_name_bn"))
					{
						AllFieldSql += "" + tableName + ".approver_office_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_unit_name_en"))
					{
						AllFieldSql += "" + tableName + ".approver_office_unit_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".approver_office_unit_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_unit_org_name_en"))
					{
						AllFieldSql += "" + tableName + ".approver_office_unit_org_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_unit_org_name_bn"))
					{
						AllFieldSql += "" + tableName + ".approver_office_unit_org_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("withdrawer_phone_num"))
					{
						AllFieldSql += "" + tableName + ".withdrawer_phone_num like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("withdrawer_name_en"))
					{
						AllFieldSql += "" + tableName + ".withdrawer_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("withdrawer_name_bn"))
					{
						AllFieldSql += "" + tableName + ".withdrawer_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("withdrawer_office_name_en"))
					{
						AllFieldSql += "" + tableName + ".withdrawer_office_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("withdrawer_office_name_bn"))
					{
						AllFieldSql += "" + tableName + ".withdrawer_office_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("withdrawer_office_unit_name_en"))
					{
						AllFieldSql += "" + tableName + ".withdrawer_office_unit_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("withdrawer_office_unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".withdrawer_office_unit_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("withdrawer_office_unit_org_name_en"))
					{
						AllFieldSql += "" + tableName + ".withdrawer_office_unit_org_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("withdrawer_office_unit_org_name_bn"))
					{
						AllFieldSql += "" + tableName + ".withdrawer_office_unit_org_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("status"))
					{
						AllFieldSql += "" + tableName + ".status = " + p_searchCriteria.get(str) + "";
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".approved_start_date desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }


    //new get dto for route withdraw using employee_record_id

	public Vm_route_travelDTO getDTOByOrganogramID (long requester_org_id)
	{
		Vm_route_travelDTO vm_route_travelDTO = null;
		try{

			String filter = " requester_org_id = "+ requester_org_id + " AND status = " + CommonApprovalStatus.SATISFIED.getValue() + " ";

			String sql = getSqlWithSearchCriteria(null, 1, 0, GETDTOS, true, userDTO, filter, false, new ArrayList<>());

			List<Vm_route_travelDTO> vm_route_travelDTOS = ConnectionAndStatementUtil.getListOfT(sql,this::build);

			if (!vm_route_travelDTOS.isEmpty()) return vm_route_travelDTOS.get(0);

//			String sql = "SELECT * ";
//
//			sql += " FROM " + tableName;
//
//			sql += " WHERE requester_org_id=" + requester_org_id;
//
//			//sql += " AND status=" + 1;
//
//			sql += " AND status=" + CommonApprovalStatus.SATISFIED.getValue();
//
//			//sql += " AND status=" + 5;
//
//			sql += " AND isDeleted=" + 0;
//
//			sql += " ORDER BY ID DESC LIMIT 1 " ;

			printSql(sql);


		}catch(Exception ex){
			ex.printStackTrace();
		}
		return vm_route_travelDTO;
	}

	public Vm_route_travelDTO getDTOByOrganogramIDForCheckStatus (long requester_org_id)
	{
		Vm_route_travelDTO vm_route_travelDTO = null;
		try{

			String filter = " requester_org_id = "+ requester_org_id + " ";

			String sql = getSqlWithSearchCriteria(null, 1, 0, GETDTOS, true, userDTO, filter, false, new ArrayList<>());

			List<Vm_route_travelDTO> vm_route_travelDTOS = ConnectionAndStatementUtil.getListOfT(sql,this::build);

			if (!vm_route_travelDTOS.isEmpty()) return vm_route_travelDTOS.get(0);

//			String sql = "SELECT * ";
//
//			sql += " FROM " + tableName;
//
//			sql += " WHERE requester_org_id=" + requester_org_id;
//
//			//sql += " AND status=" + 1;
//
//			//sql += " AND status=" + CommonApprovalStatus.SATISFIED.getValue();
//
//			//sql += " AND status=" + 5;
//
//			sql += " AND isDeleted=" + 0;
//
//			sql += " ORDER BY ID DESC LIMIT 1 " ;

			printSql(sql);

		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return vm_route_travelDTO;
	}

	public Vm_route_travelDTO getDTOByOrganogramIDForWithdrawal (long requester_org_id)
	{
		Vm_route_travelDTO vm_route_travelDTO = null;
		try{
			String filter = " requester_org_id = "+ requester_org_id + " AND status = " + CommonApprovalStatus.WITHDRAWN.getValue() + " ";

			String sql = getSqlWithSearchCriteria(null, 1, 0, GETDTOS, true, userDTO, filter, false, new ArrayList<>());

			List<Vm_route_travelDTO> vm_route_travelDTOS = ConnectionAndStatementUtil.getListOfT(sql,this::build);

			if (!vm_route_travelDTOS.isEmpty()) return vm_route_travelDTOS.get(0);

//			String sql = "SELECT * ";
//
//			sql += " FROM " + tableName;
//
//			sql += " WHERE requester_org_id=" + requester_org_id;
//
//			//sql += " AND status=" + 1;
//
//			//sql += " AND status=" + 5;
//
//			sql += " AND status=" + CommonApprovalStatus.WITHDRAWN.getValue();
//
//			sql += " AND isDeleted=" + 0;
//
//			sql += " ORDER BY ID DESC LIMIT 1 " ;

			printSql(sql);


		}catch(Exception ex){
			ex.printStackTrace();
		}
		return vm_route_travelDTO;
	}

	public Vm_route_travelDTO getDTOByRequesterEmpID (long requester_emp_record_id)
	{
		Vm_route_travelDTO vm_route_travelDTO = null;
		try{
			String filter = " requester_emp_id = "+ requester_emp_record_id + " AND status = " + CommonApprovalStatus.WITHDRAWN.getValue() + " ";

			String sql = getSqlWithSearchCriteria(null, 1, 0, GETDTOS, true, userDTO, filter, false, new ArrayList<>());

			List<Vm_route_travelDTO> vm_route_travelDTOS = ConnectionAndStatementUtil.getListOfT(sql,this::build);

			if (!vm_route_travelDTOS.isEmpty()) return vm_route_travelDTOS.get(0);

//			String sql = "SELECT * ";
//
//			sql += " FROM " + tableName;
//
//			sql += " WHERE requester_emp_id=" + requester_emp_record_id;
//
//			//sql += " AND status=" + 1;
//
//			//sql += " AND status=" + 5;
//
//			sql += " AND status=" + CommonApprovalStatus.WITHDRAWN.getValue();
//
//			sql += " AND isDeleted=" + 0;
//
//			sql += " ORDER BY ID DESC LIMIT 1 " ;

			printSql(sql);

		}catch(Exception ex){
			ex.printStackTrace();
		}
		return vm_route_travelDTO;
	}

	public Vm_route_travelDTO getSatisfiedDTOByRequesterEmpID (long requester_emp_record_id)
	{
		Vm_route_travelDTO vm_route_travelDTO = null;
		try{
			String filter = " requester_emp_id = "+ requester_emp_record_id + " AND status = " + CommonApprovalStatus.SATISFIED.getValue() + " ";

			String sql = getSqlWithSearchCriteria(null, 1, 0, GETDTOS, true, userDTO, filter, false, new ArrayList<>());

			List<Vm_route_travelDTO> vm_route_travelDTOS = ConnectionAndStatementUtil.getListOfT(sql,this::build);

			if (!vm_route_travelDTOS.isEmpty()) return vm_route_travelDTOS.get(0);

//			String sql = "SELECT * ";
//
//			sql += " FROM " + tableName;
//
//			sql += " WHERE requester_emp_id=" + requester_emp_record_id;
//
//			//sql += " AND status=" + 1;
//
//			//sql += " AND status=" + 5;
//
//			sql += " AND status=" + CommonApprovalStatus.WITHDRAWN.getValue();
//
//			sql += " AND isDeleted=" + 0;
//
//			sql += " ORDER BY ID DESC LIMIT 1 " ;

			printSql(sql);

		}catch(Exception ex){
			ex.printStackTrace();
		}
		return vm_route_travelDTO;
	}
				
}
	