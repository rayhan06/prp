package office_origin_unit_organograms;
import java.util.*; 


public class Office_origin_unit_organogramsMAPS 
{

	public HashMap<String, String> java_allfield_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_anyfield_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	public HashMap<String, String> java_SQL_map = new HashMap<String, String>();
	public HashMap<String, String> java_Text_map = new HashMap<String, String>();
	
	private static Office_origin_unit_organogramsMAPS self = null;
	
	private Office_origin_unit_organogramsMAPS()
	{
		
		java_allfield_type_map.put("office_origin_unit_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("superior_unit_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("superior_designation_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("designation_eng".toLowerCase(), "String");
		java_allfield_type_map.put("designation_bng".toLowerCase(), "String");
		java_allfield_type_map.put("designation_level".toLowerCase(), "Integer");
		java_allfield_type_map.put("designation_sequence".toLowerCase(), "Integer");
		java_allfield_type_map.put("status".toLowerCase(), "Boolean");

		java_anyfield_search_map.put("office_origin_unit_organograms.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("office_origin_unit_organograms.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put("office_origin_units.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("office_origin_units.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put("office_origin_units.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("office_origin_units.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put("office_origin_unit_organograms.designation_eng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_origin_unit_organograms.designation_bng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_origin_unit_organograms.short_name_eng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_origin_unit_organograms.short_name_bng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_origin_unit_organograms.designation_level".toLowerCase(), "Integer");
		java_anyfield_search_map.put("office_origin_unit_organograms.designation_sequence".toLowerCase(), "Integer");
		java_anyfield_search_map.put("office_origin_unit_organograms.status".toLowerCase(), "Boolean");
		java_anyfield_search_map.put("office_origin_unit_organograms.created_by".toLowerCase(), "Integer");
		java_anyfield_search_map.put("office_origin_unit_organograms.modified_by".toLowerCase(), "Integer");
		java_anyfield_search_map.put("office_origin_unit_organograms.created".toLowerCase(), "Long");
		java_anyfield_search_map.put("office_origin_unit_organograms.modified".toLowerCase(), "Long");

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("officeOriginUnitId".toLowerCase(), "officeOriginUnitId".toLowerCase());
		java_DTO_map.put("superiorUnitId".toLowerCase(), "superiorUnitId".toLowerCase());
		java_DTO_map.put("superiorDesignationId".toLowerCase(), "superiorDesignationId".toLowerCase());
		java_DTO_map.put("designationEng".toLowerCase(), "designationEng".toLowerCase());
		java_DTO_map.put("designationBng".toLowerCase(), "designationBng".toLowerCase());
		java_DTO_map.put("shortNameEng".toLowerCase(), "shortNameEng".toLowerCase());
		java_DTO_map.put("shortNameBng".toLowerCase(), "shortNameBng".toLowerCase());
		java_DTO_map.put("designationLevel".toLowerCase(), "designationLevel".toLowerCase());
		java_DTO_map.put("designationSequence".toLowerCase(), "designationSequence".toLowerCase());
		java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
		java_DTO_map.put("createdBy".toLowerCase(), "createdBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("created".toLowerCase(), "created".toLowerCase());
		java_DTO_map.put("modified".toLowerCase(), "modified".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("office_origin_unit_id".toLowerCase(), "officeOriginUnitId".toLowerCase());
		java_SQL_map.put("superior_unit_id".toLowerCase(), "superiorUnitId".toLowerCase());
		java_SQL_map.put("superior_designation_id".toLowerCase(), "superiorDesignationId".toLowerCase());
		java_SQL_map.put("designation_eng".toLowerCase(), "designationEng".toLowerCase());
		java_SQL_map.put("designation_bng".toLowerCase(), "designationBng".toLowerCase());
		java_SQL_map.put("designation_level".toLowerCase(), "designationLevel".toLowerCase());
		java_SQL_map.put("designation_sequence".toLowerCase(), "designationSequence".toLowerCase());
		java_SQL_map.put("status".toLowerCase(), "status".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Office Origin Unit Id".toLowerCase(), "officeOriginUnitId".toLowerCase());
		java_Text_map.put("Superior Unit Id".toLowerCase(), "superiorUnitId".toLowerCase());
		java_Text_map.put("Superior Designation Id".toLowerCase(), "superiorDesignationId".toLowerCase());
		java_Text_map.put("Designation Eng".toLowerCase(), "designationEng".toLowerCase());
		java_Text_map.put("Designation Bng".toLowerCase(), "designationBng".toLowerCase());
		java_Text_map.put("Short Name Eng".toLowerCase(), "shortNameEng".toLowerCase());
		java_Text_map.put("Short Name Bng".toLowerCase(), "shortNameBng".toLowerCase());
		java_Text_map.put("Designation Level".toLowerCase(), "designationLevel".toLowerCase());
		java_Text_map.put("Designation Sequence".toLowerCase(), "designationSequence".toLowerCase());
		java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
		java_Text_map.put("Created By".toLowerCase(), "createdBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Created".toLowerCase(), "created".toLowerCase());
		java_Text_map.put("Modified".toLowerCase(), "modified".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}
	
	public static Office_origin_unit_organogramsMAPS GetInstance()
	{
		if(self == null)
		{
			self = new Office_origin_unit_organogramsMAPS ();
		}
		return self;
	}
	

}