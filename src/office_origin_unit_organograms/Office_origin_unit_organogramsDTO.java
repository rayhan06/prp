package office_origin_unit_organograms;
import java.util.*; 


public class Office_origin_unit_organogramsDTO {

	public long id = 0;
	public int officeOriginUnitId = 0;
	public int superiorUnitId = 0;
	public int superiorDesignationId = 0;
    public String designationEng = "";
    public String designationBng = "";
    public String shortNameEng = "";
    public String shortNameBng = "";
	public int designationLevel = 0;
	public int designationSequence = 0;
	public boolean status = false;
	public int createdBy = 0;
	public int modifiedBy = 0;
	public long created = 0;
	public long modified = 0;
	public boolean isDeleted = false;
	public long lastModificationTime = 0;
	public long officeUnitId;
	public int jobGradeTypeCat;
	
	
    @Override
	public String toString() {
            return "$Office_origin_unit_organogramsDTO[" +
            " id = " + id +
            " officeOriginUnitId = " + officeOriginUnitId +
            " superiorUnitId = " + superiorUnitId +
            " superiorDesignationId = " + superiorDesignationId +
            " designationEng = " + designationEng +
            " designationBng = " + designationBng +
            " shortNameEng = " + shortNameEng +
            " shortNameBng = " + shortNameBng +
            " designationLevel = " + designationLevel +
            " designationSequence = " + designationSequence +
            " status = " + status +
            " createdBy = " + createdBy +
            " modifiedBy = " + modifiedBy +
            " created = " + created +
            " modified = " + modified +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}