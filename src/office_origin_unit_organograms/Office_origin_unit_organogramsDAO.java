package office_origin_unit_organograms;

import com.google.gson.Gson;
import common.ConnectionAndStatementUtil;
import dbm.DBMR;
import dbm.DBMW;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import offices.OfficesDAO;
import offices.OfficesDTO;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.CommonConstant;
import util.NavigationService2;

import java.sql.*;
import java.util.*;

public class Office_origin_unit_organogramsDAO extends NavigationService2 {

    Logger logger = Logger.getLogger(getClass());

    private final static String updateOrgTree = "UPDATE office_unit_organograms SET org_tree = '%s' WHERE ID = %d";

    public void recordUpdateTime2(Connection connection, long lastModificationTime) throws SQLException {
        recordUpdateTime(connection,"office_unit_organograms",lastModificationTime);
    }

    public long addOffice_origin_unit_organograms(Office_origin_unit_organogramsDTO office_origin_unit_organogramsDTO) throws Exception {

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();

        try {
            connection = DBMW.getInstance().getConnection();

            if (connection == null) {
                System.out.println("nullconn");
            }

            office_origin_unit_organogramsDTO.id = DBMW.getInstance().getNextSequenceId("office_origin_unit_organograms");

            String sql = "INSERT INTO office_origin_unit_organograms";

            sql += " (";
            sql += "id";
            sql += ", ";
            sql += "office_origin_unit_id";
            sql += ", ";
            sql += "superior_unit_id";
            sql += ", ";
            sql += "superior_designation_id";
            sql += ", ";
            sql += "designation_eng";
            sql += ", ";
            sql += "designation_bng";
            sql += ", ";
            sql += "short_name_eng";
            sql += ", ";
            sql += "short_name_bng";
            sql += ", ";
            sql += "designation_level";
            sql += ", ";
            sql += "designation_sequence";
            sql += ", ";
            sql += "status";
            sql += ", ";
            sql += "created_by";
            sql += ", ";
            sql += "modified_by";
            sql += ", ";
            sql += "created";
            sql += ", ";
            sql += "modified";
            sql += ", ";
            sql += "isDeleted";
            sql += ", ";
            sql += "lastModificationTime";
            sql += ")";


            sql += " VALUES(";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ")";

            printSql(sql);

            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, office_origin_unit_organogramsDTO.id);
            ps.setObject(index++, office_origin_unit_organogramsDTO.officeOriginUnitId > 0 ? office_origin_unit_organogramsDTO.officeOriginUnitId : 0);
            ps.setObject(index++, office_origin_unit_organogramsDTO.superiorUnitId > 0 ? office_origin_unit_organogramsDTO.superiorUnitId : 0);
            ps.setObject(index++, office_origin_unit_organogramsDTO.superiorDesignationId > 0 ? office_origin_unit_organogramsDTO.superiorDesignationId : 0);
            ps.setObject(index++, office_origin_unit_organogramsDTO.designationEng);
            ps.setObject(index++, office_origin_unit_organogramsDTO.designationBng);
            ps.setObject(index++, office_origin_unit_organogramsDTO.shortNameEng);
            ps.setObject(index++, office_origin_unit_organogramsDTO.shortNameBng);
            ps.setObject(index++, office_origin_unit_organogramsDTO.designationLevel);
            ps.setObject(index++, office_origin_unit_organogramsDTO.designationSequence);
            ps.setObject(index++, 1);
            ps.setObject(index++, office_origin_unit_organogramsDTO.createdBy);
            ps.setObject(index++, office_origin_unit_organogramsDTO.modifiedBy);
            ps.setObject(index++, office_origin_unit_organogramsDTO.created);
            ps.setObject(index++, office_origin_unit_organogramsDTO.modified);
            ps.setObject(index++, office_origin_unit_organogramsDTO.isDeleted);
            ps.setObject(index++, lastModificationTime);

            System.out.println(ps);
            ps.execute();

            recordUpdateTime2(connection, lastModificationTime);
            Office_origin_unit_organogramsRepository.getInstance().addNewDto(office_origin_unit_organogramsDTO);

            addOffice_unit_organograms(office_origin_unit_organogramsDTO);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_origin_unit_organogramsDTO.id;
    }

    public synchronized long addOffice_unit_organograms(Office_origin_unit_organogramsDTO office_origin_unit_organogramsDTO) throws Exception {


        //Integer officeOriginUnitId = office_origin_unit_organogramsDTO.officeOriginUnitId;

        //Office_unitsDAO office_unitsDAO = new Office_unitsDAO();
        //Office_unitsDTO office_unitsDTO = (Office_unitsDTO) office_unitsDAO.getDTOByOfficeOriginUnitId( officeOriginUnitId );
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(office_origin_unit_organogramsDTO.superiorDesignationId);

        if(officeUnitOrganograms == null){
            return -1;
        }

        OfficesDAO officesDAO = new OfficesDAO();
        OfficesDTO officesDTO = officesDAO.getOfficesDTOByOfficeOriginId(Long.valueOf(CommonConstant.DEFAULT_OFFICE_ORIGIN_ID).intValue());

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();

        long officeUnitOrganogramsId = DBMW.getInstance().getNextSequenceId("office_unit_organograms");
        String orgTree;
        if(officeUnitOrganograms.orgTree.endsWith("$")){
            orgTree = officeUnitOrganograms.orgTree.substring(0,officeUnitOrganograms.orgTree.length()-1) +"|"+ officeUnitOrganogramsId+"$";
        }else{
            orgTree = officeUnitOrganograms.orgTree + officeUnitOrganogramsId+"$";
        }

        try {
            connection = DBMW.getInstance().getConnection();

            String sql = "INSERT INTO office_unit_organograms";
            sql += " (";
            sql += "id";
            sql += ", ";
            sql += "office_id";
            sql += ", ";
            sql += "office_unit_id";
            sql += ", ";
            sql += "superior_unit_id";
            sql += ", ";
            sql += "superior_designation_id";
            sql += ", ";
            sql += "ref_origin_unit_org_id";
            sql += ", ";
            sql += "designation_eng";
            sql += ", ";
            sql += "designation_bng";
            sql += ", ";
            sql += "short_name_eng";
            sql += ", ";
            sql += "short_name_bng";
            sql += ", ";
            sql += "designation_level";
            sql += ", ";
            sql += "designation_sequence";
            sql += ", ";
            sql += "status";
            sql += ", ";
            sql += "created_by";
            sql += ", ";
            sql += "modified_by";
            sql += ", ";
            sql += "created";
            sql += ", ";
            sql += "modified";
            sql += ", ";
            sql += "role_type";
            sql += ", ";
            sql += "isDeleted";
            sql += ", ";
            sql += "lastModificationTime";
            sql += ", ";
            sql += "job_grade_type_cat";
            sql += ", ";
            sql += "org_tree";
            sql += ")";


            sql += " VALUES(";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ")";

            printSql(sql);

            ps = connection.prepareStatement(sql);


            int index = 0;

            ps.setObject(++index, officeUnitOrganogramsId);
            ps.setObject(++index, officesDTO.iD);
            ps.setObject(++index, office_origin_unit_organogramsDTO.officeUnitId);
            ps.setObject(++index, office_origin_unit_organogramsDTO.superiorUnitId);
            ps.setObject(++index, office_origin_unit_organogramsDTO.superiorDesignationId);
            ps.setObject(++index, office_origin_unit_organogramsDTO.id);
            ps.setObject(++index, office_origin_unit_organogramsDTO.designationEng);
            ps.setObject(++index, office_origin_unit_organogramsDTO.designationBng);
            ps.setObject(++index, office_origin_unit_organogramsDTO.shortNameEng);
            ps.setObject(++index, office_origin_unit_organogramsDTO.shortNameBng);
            ps.setObject(++index, office_origin_unit_organogramsDTO.designationLevel);
            ps.setObject(++index, office_origin_unit_organogramsDTO.designationSequence);
            ps.setObject(++index, 1);
            ps.setObject(++index, office_origin_unit_organogramsDTO.createdBy);
            ps.setObject(++index, office_origin_unit_organogramsDTO.modifiedBy);
            ps.setObject(++index, office_origin_unit_organogramsDTO.created);
            ps.setObject(++index, office_origin_unit_organogramsDTO.modified);
            ps.setObject(++index, CommonConstant.DEFAULT_ROLE_TYPE);
            ps.setObject(++index, office_origin_unit_organogramsDTO.isDeleted);
            ps.setObject(++index, lastModificationTime);
            ps.setObject(++index, office_origin_unit_organogramsDTO.jobGradeTypeCat);
            ps.setObject(++index, orgTree);
            System.out.println(ps);
            ps.execute();
            if(officeUnitOrganograms.orgTree.endsWith("$")){
                logger.debug("Before update parent orgTree : "+officeUnitOrganograms.orgTree);
                String orgTreeParent = officeUnitOrganograms.orgTree.substring(0,officeUnitOrganograms.orgTree.length()-1)+"|";
                ConnectionAndStatementUtil.getWriteStatement(st->{
                    String sql2 = String.format(updateOrgTree,orgTreeParent,officeUnitOrganograms.id);
                    logger.debug(sql2);
                    try {
                        st.executeUpdate(sql2);
                    } catch (SQLException ex) {
                        logger.error(ex);
                    }
                });
            }
            recordUpdateTime2(connection, lastModificationTime);
            OfficeUnitOrganogramsRepository.getInstance().reload(false);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return officeUnitOrganogramsId;
    }

    //need another getter for repository
    public Office_origin_unit_organogramsDTO getOffice_origin_unit_organogramsDTOByid(long id) throws Exception {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Office_origin_unit_organogramsDTO office_origin_unit_organogramsDTO = null;
        try {

            String sql = "SELECT * ";

            sql += " FROM office_origin_unit_organograms";

            sql += " WHERE id=" + id;

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                office_origin_unit_organogramsDTO = new Office_origin_unit_organogramsDTO();

                office_origin_unit_organogramsDTO.id = rs.getLong("id");
                office_origin_unit_organogramsDTO.officeOriginUnitId = rs.getInt("office_origin_unit_id");
                office_origin_unit_organogramsDTO.superiorUnitId = rs.getInt("superior_unit_id");
                office_origin_unit_organogramsDTO.superiorDesignationId = rs.getInt("superior_designation_id");
                office_origin_unit_organogramsDTO.designationEng = rs.getString("designation_eng");
                office_origin_unit_organogramsDTO.designationBng = rs.getString("designation_bng");
                office_origin_unit_organogramsDTO.shortNameEng = rs.getString("short_name_eng");
                office_origin_unit_organogramsDTO.shortNameBng = rs.getString("short_name_bng");
                office_origin_unit_organogramsDTO.designationLevel = rs.getInt("designation_level");
                office_origin_unit_organogramsDTO.designationSequence = rs.getInt("designation_sequence");
                office_origin_unit_organogramsDTO.status = rs.getBoolean("status");
                office_origin_unit_organogramsDTO.createdBy = rs.getInt("created_by");
                office_origin_unit_organogramsDTO.modifiedBy = rs.getInt("modified_by");
                office_origin_unit_organogramsDTO.created = rs.getLong("created");
                office_origin_unit_organogramsDTO.modified = rs.getLong("modified");
                office_origin_unit_organogramsDTO.isDeleted = rs.getBoolean("isDeleted");
                office_origin_unit_organogramsDTO.lastModificationTime = rs.getLong("lastModificationTime");

            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_origin_unit_organogramsDTO;
    }

    public long updateOffice_origin_unit_organograms(Office_origin_unit_organogramsDTO office_origin_unit_organogramsDTO) throws Exception {

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            connection = DBMW.getInstance().getConnection();

            String sql = "UPDATE office_origin_unit_organograms";

            sql += " SET ";
            sql += "office_origin_unit_id=?";
            sql += ", ";
            sql += "superior_unit_id=?";
            sql += ", ";
            sql += "superior_designation_id=?";
            sql += ", ";
            sql += "designation_eng=?";
            sql += ", ";
            sql += "designation_bng=?";
            sql += ", ";
            sql += "short_name_eng=?";
            sql += ", ";
            sql += "short_name_bng=?";
            sql += ", ";
            sql += "designation_level=?";
            sql += ", ";
            sql += "designation_sequence=?";
            sql += ", ";
            sql += "status=?";
            sql += ", ";
            sql += "created_by=?";
            sql += ", ";
            sql += "modified_by=?";
            sql += ", ";
            sql += "created=?";
            sql += ", ";
            sql += "modified=?";
            sql += ", lastModificationTime = " + lastModificationTime + "";
            sql += " WHERE id = " + office_origin_unit_organogramsDTO.id;

            printSql(sql);

            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, office_origin_unit_organogramsDTO.officeOriginUnitId);
            ps.setObject(index++, office_origin_unit_organogramsDTO.superiorUnitId);
            ps.setObject(index++, office_origin_unit_organogramsDTO.superiorDesignationId);
            ps.setObject(index++, office_origin_unit_organogramsDTO.designationEng);
            ps.setObject(index++, office_origin_unit_organogramsDTO.designationBng);
            ps.setObject(index++, office_origin_unit_organogramsDTO.shortNameEng);
            ps.setObject(index++, office_origin_unit_organogramsDTO.shortNameBng);
            ps.setObject(index++, office_origin_unit_organogramsDTO.designationLevel);
            ps.setObject(index++, office_origin_unit_organogramsDTO.designationSequence);
            ps.setObject(index++, office_origin_unit_organogramsDTO.status);
            ps.setObject(index++, office_origin_unit_organogramsDTO.createdBy);
            ps.setObject(index++, office_origin_unit_organogramsDTO.modifiedBy);
            ps.setObject(index++, office_origin_unit_organogramsDTO.created);
            ps.setObject(index++, office_origin_unit_organogramsDTO.modified);
            System.out.println(ps);
            ps.executeUpdate();


            recordUpdateTime(connection, lastModificationTime);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_origin_unit_organogramsDTO.id;
    }

    public void deleteOffice_origin_unit_organogramsByid(long id) throws Exception {
        Connection connection = null;
        Statement stmt = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            String sql = "UPDATE office_origin_unit_organograms";

            sql += " SET isDeleted=1,lastModificationTime=" + lastModificationTime + " WHERE id = " + id;

            printSql(sql);

            connection = DBMW.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.execute(sql);


            recordUpdateTime(connection, lastModificationTime);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
    }

    public List<Office_origin_unit_organogramsDTO> getDTOs(Collection recordIDs) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Office_origin_unit_organogramsDTO office_origin_unit_organogramsDTO = null;
        List<Office_origin_unit_organogramsDTO> office_origin_unit_organogramsDTOList = new ArrayList<>();
        if (recordIDs.isEmpty()) {
            return office_origin_unit_organogramsDTOList;
        }
        try {

            String sql = "SELECT * ";

            sql += " FROM office_origin_unit_organograms";

            sql += " WHERE id IN ( ";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql += ",";
                }
                sql += ((ArrayList) recordIDs).get(i);
            }
            sql += ")  order by lastModificationTime desc";

            printSql(sql);

            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                office_origin_unit_organogramsDTO = new Office_origin_unit_organogramsDTO();
                office_origin_unit_organogramsDTO.id = rs.getLong("id");
                office_origin_unit_organogramsDTO.officeOriginUnitId = rs.getInt("office_origin_unit_id");
                office_origin_unit_organogramsDTO.superiorUnitId = rs.getInt("superior_unit_id");
                office_origin_unit_organogramsDTO.superiorDesignationId = rs.getInt("superior_designation_id");
                office_origin_unit_organogramsDTO.designationEng = rs.getString("designation_eng");
                office_origin_unit_organogramsDTO.designationBng = rs.getString("designation_bng");
                office_origin_unit_organogramsDTO.shortNameEng = rs.getString("short_name_eng");
                office_origin_unit_organogramsDTO.shortNameBng = rs.getString("short_name_bng");
                office_origin_unit_organogramsDTO.designationLevel = rs.getInt("designation_level");
                office_origin_unit_organogramsDTO.designationSequence = rs.getInt("designation_sequence");
                office_origin_unit_organogramsDTO.status = rs.getBoolean("status");
                office_origin_unit_organogramsDTO.createdBy = rs.getInt("created_by");
                office_origin_unit_organogramsDTO.modifiedBy = rs.getInt("modified_by");
                office_origin_unit_organogramsDTO.created = rs.getLong("created");
                office_origin_unit_organogramsDTO.modified = rs.getLong("modified");
                office_origin_unit_organogramsDTO.isDeleted = rs.getBoolean("isDeleted");
                office_origin_unit_organogramsDTO.lastModificationTime = rs.getLong("lastModificationTime");
                System.out.println("got this DTO: " + office_origin_unit_organogramsDTO);

                office_origin_unit_organogramsDTOList.add(office_origin_unit_organogramsDTO);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_origin_unit_organogramsDTOList;

    }

    //add repository
    public List<Office_origin_unit_organogramsDTO> getAllOffice_origin_unit_organograms(boolean isFirstReload) {
        List<Office_origin_unit_organogramsDTO> office_origin_unit_organogramsDTOList = new ArrayList<>();

        String sql = "SELECT * FROM office_origin_unit_organograms";
        sql += " WHERE ";


        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        sql += " order by office_origin_unit_organograms.lastModificationTime desc";
        printSql(sql);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;


        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);


            while (rs.next()) {
                Office_origin_unit_organogramsDTO office_origin_unit_organogramsDTO = new Office_origin_unit_organogramsDTO();
                office_origin_unit_organogramsDTO.id = rs.getLong("id");
                office_origin_unit_organogramsDTO.officeOriginUnitId = rs.getInt("office_origin_unit_id");
                office_origin_unit_organogramsDTO.superiorUnitId = rs.getInt("superior_unit_id");
                office_origin_unit_organogramsDTO.superiorDesignationId = rs.getInt("superior_designation_id");
                office_origin_unit_organogramsDTO.designationEng = rs.getString("designation_eng");
                office_origin_unit_organogramsDTO.designationBng = rs.getString("designation_bng");
                office_origin_unit_organogramsDTO.shortNameEng = rs.getString("short_name_eng");
                office_origin_unit_organogramsDTO.shortNameBng = rs.getString("short_name_bng");
                office_origin_unit_organogramsDTO.designationLevel = rs.getInt("designation_level");
                office_origin_unit_organogramsDTO.designationSequence = rs.getInt("designation_sequence");
                office_origin_unit_organogramsDTO.status = rs.getBoolean("status");
                office_origin_unit_organogramsDTO.createdBy = rs.getInt("created_by");
                office_origin_unit_organogramsDTO.modifiedBy = rs.getInt("modified_by");
                office_origin_unit_organogramsDTO.created = rs.getLong("created");
                office_origin_unit_organogramsDTO.modified = rs.getLong("modified");
                office_origin_unit_organogramsDTO.isDeleted = rs.getBoolean("isDeleted");
                office_origin_unit_organogramsDTO.lastModificationTime = rs.getLong("lastModificationTime");

                office_origin_unit_organogramsDTOList.add(office_origin_unit_organogramsDTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return office_origin_unit_organogramsDTOList;
    }

    public List<Office_origin_unit_organogramsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        List<Office_origin_unit_organogramsDTO> office_origin_unit_organogramsDTOList = new ArrayList<>();

        try {

            String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS);

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Office_origin_unit_organogramsDTO office_origin_unit_organogramsDTO = new Office_origin_unit_organogramsDTO();
                office_origin_unit_organogramsDTO.id = rs.getLong("id");
                office_origin_unit_organogramsDTO.officeOriginUnitId = rs.getInt("office_origin_unit_id");
                office_origin_unit_organogramsDTO.superiorUnitId = rs.getInt("superior_unit_id");
                office_origin_unit_organogramsDTO.superiorDesignationId = rs.getInt("superior_designation_id");
                office_origin_unit_organogramsDTO.designationEng = rs.getString("designation_eng");
                office_origin_unit_organogramsDTO.designationBng = rs.getString("designation_bng");
                office_origin_unit_organogramsDTO.shortNameEng = rs.getString("short_name_eng");
                office_origin_unit_organogramsDTO.shortNameBng = rs.getString("short_name_bng");
                office_origin_unit_organogramsDTO.designationLevel = rs.getInt("designation_level");
                office_origin_unit_organogramsDTO.designationSequence = rs.getInt("designation_sequence");
                office_origin_unit_organogramsDTO.status = rs.getBoolean("status");
                office_origin_unit_organogramsDTO.createdBy = rs.getInt("created_by");
                office_origin_unit_organogramsDTO.modifiedBy = rs.getInt("modified_by");
                office_origin_unit_organogramsDTO.created = rs.getLong("created");
                office_origin_unit_organogramsDTO.modified = rs.getLong("modified");
                office_origin_unit_organogramsDTO.isDeleted = rs.getBoolean("isDeleted");
                office_origin_unit_organogramsDTO.lastModificationTime = rs.getLong("lastModificationTime");

                office_origin_unit_organogramsDTOList.add(office_origin_unit_organogramsDTO);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_origin_unit_organogramsDTOList;

    }

    public Collection getIDs() {
        Collection data = new ArrayList();
        Connection connection = null;
        Statement stmt = null;
        ResultSet resultSet = null;

        String sql = getSqlWithSearchCriteria(null, -1, -1, GETIDS);

        printSql(sql);

        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();

            for (resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("id"))) ;

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("DAO " + e, e);
        } finally {
            try {
                if (resultSet != null && !resultSet.isClosed()) {
                    resultSet.close();
                }
            } catch (Exception ex) {

            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception e) {
                logger.fatal("DAO finally :" + e);
            }
        }
        return data;
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category) {
        String sql = "SELECT ";
        if (category == GETIDS) {
            sql += "distinct office_origin_unit_organograms.ID as ID ";
        } else if (category == GETCOUNT) {
            sql += " count(office_origin_unit_organograms.ID) as countID ";
        } else if (category == GETDTOS) {
            sql += "  office_origin_unit_organograms.* ";
        }
        sql += "FROM office_origin_unit_organograms ";

        String AnyfieldSql = "";
        String AllFieldSql = "";

        if (p_searchCriteria != null) {


            Enumeration names = p_searchCriteria.keys();
            String str, value;

            AnyfieldSql = "(";

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                int i = 0;
                Iterator it = Office_origin_unit_organogramsMAPS.GetInstance().java_anyfield_search_map.entrySet().iterator();
                while (it.hasNext()) {
                    if (i > 0) {
                        AnyfieldSql += " OR  ";
                    }
                    Map.Entry pair = (Map.Entry) it.next();
                    AnyfieldSql += pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
                    i++;
                }
            }
            AnyfieldSql += ")";
            System.out.println("AnyfieldSql = " + AnyfieldSql);

            AllFieldSql = "(";
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                System.out.println(str + ": " + value);
                if (Office_origin_unit_organogramsMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()) != null && !Office_origin_unit_organogramsMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
                        && !str.equalsIgnoreCase("AnyField")
                        && value != null && !value.equalsIgnoreCase("")) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql += " AND  ";
                    }
                    if (Office_origin_unit_organogramsMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equals("String")) {
                        AllFieldSql += "office_origin_unit_organograms." + str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
                    } else {
                        AllFieldSql += "office_origin_unit_organograms." + str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
                    }
                    i++;
                }
            }

            AllFieldSql += ")";
            System.out.println("AllFieldSql = " + AllFieldSql);


        }

        if (!AnyfieldSql.equals("()") && !AnyfieldSql.equals("")) {
            sql += " join office_origin_unit_organograms on office_origin_unit_organograms.office_origin_unit_id = office_origin_unit_organograms.ID ";
            sql += " join office_origin_units on office_origin_unit_organograms.superior_unit_id = office_origin_units.ID ";
            sql += " join office_origin_units on office_origin_unit_organograms.superior_designation_id = office_origin_units.ID ";

        }
        sql += " WHERE ";
        sql += " office_origin_unit_organograms.isDeleted = false ";

        if (!AnyfieldSql.equals("()") && !AnyfieldSql.equals("")) {
            sql += " AND " + AnyfieldSql;

        }
        if (!AllFieldSql.equals("()") && !AllFieldSql.equals("")) {
            sql += " AND " + AllFieldSql;
        }


        sql += " order by office_origin_unit_organograms.lastModificationTime desc ";

        printSql(sql);

        if (limit >= 0) {
            sql += " limit " + limit;
        }
        if (offset >= 0) {
            sql += " offset " + offset;
        }

        //System.out.println("-------------- sql = " + sql);

        return sql;
    }

    public String getOfficeOriginUnitOrganogramsSingle(int id) {
        Connection connection = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        Map<Object, Object> result = new HashMap<>();

        String sql = "select * from office_origin_unit_organograms where id = " + id;

        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            resultSet = stmt.executeQuery(sql);
            while (resultSet.next()) {
                result.put("id", resultSet.getString("id"));
                result.put("office_origin_unit_id", resultSet.getString("office_origin_unit_id"));
                result.put("superior_unit_id", resultSet.getString("superior_unit_id"));
                result.put("superior_designation_id", resultSet.getString("superior_designation_id"));
                result.put("designation_eng", resultSet.getString("designation_eng"));
                result.put("designation_bng", resultSet.getString("designation_bng"));
                result.put("designation_level", resultSet.getInt("designation_level"));
                result.put("designation_sequence", resultSet.getInt("designation_sequence"));
                result.put("status", resultSet.getBoolean("status"));
                break;
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("DAO " + e, e);
        } finally {
            try {
                if (resultSet != null && !resultSet.isClosed()) {
                    resultSet.close();
                }
            } catch (Exception ex) {

            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception e) {
                logger.fatal("DAO finally :" + e);
            }
        }
        return new Gson().toJson(result);
    }

    public String getOfficeOriginUnits(int office_ministry_id, int office_layer_id, int office_origin_id) {
        String sql = "SELECT id, unit_name_eng, unit_name_bng FROM office_origin_units \n" +
                "where office_ministry_id = ? and office_layer_id = ? and office_origin_id = ?";
        List<Map<Object, Object>> result = ConnectionAndStatementUtil.getListOfT(sql,Arrays.asList(office_ministry_id,office_layer_id,office_origin_id),rs->{
            try{
                Map<Object, Object> data = new HashMap<>();
                data.put("id", rs.getString("id"));
                data.put("unit_name_eng", rs.getString("unit_name_eng"));
                data.put("unit_name_bng", rs.getString("unit_name_bng"));
                return data;
            }catch (SQLException ex){
                ex.printStackTrace();
                return null;
            }
        });
        return new Gson().toJson(result);
    }

    public String getOfficeOriginUnitOrganograms(int office_origin_unit_id) {
        String sql = "SELECT id, designation_eng, designation_bng FROM office_origin_unit_organograms where office_origin_unit_id = ?";
        List<Map<Object, Object>> result = ConnectionAndStatementUtil.getListOfT(sql,Collections.singletonList(office_origin_unit_id),rs->{
            try{
                Map<Object, Object> data = new HashMap<>();
                data.put("id", rs.getString("id"));
                data.put("designation_eng", rs.getString("designation_eng"));
                data.put("designation_bng", rs.getString("designation_bng"));
                return data;
            }catch (SQLException ex){
                ex.printStackTrace();
                return null;
            }
        });
        return new Gson().toJson(result);
    }
}