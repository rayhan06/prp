package office_origin_unit_organograms;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import pb.CategoryLanguageModel;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;


public class Office_origin_unit_organogramsRepository implements Repository {
    Office_origin_unit_organogramsDAO office_origin_unit_organogramsDAO = new Office_origin_unit_organogramsDAO();

    static Logger logger = Logger.getLogger(Office_origin_unit_organogramsRepository.class);
    Map<Long, Office_origin_unit_organogramsDTO> mapOfOffice_origin_unit_organogramsDTOToid;
    Map<Integer, Set<Office_origin_unit_organogramsDTO>> mapOfOffice_origin_unit_organogramsDTOToofficeOriginUnitId;
    Map<Integer, Set<Office_origin_unit_organogramsDTO>> mapOfOffice_origin_unit_organogramsDTOTosuperiorUnitId;
    Map<Integer, Set<Office_origin_unit_organogramsDTO>> mapOfOffice_origin_unit_organogramsDTOTosuperiorDesignationId;
    Map<String, Set<Office_origin_unit_organogramsDTO>> mapOfOffice_origin_unit_organogramsDTOTodesignationEng;
    Map<String, Set<Office_origin_unit_organogramsDTO>> mapOfOffice_origin_unit_organogramsDTOTodesignationBng;
    Map<String, Set<Office_origin_unit_organogramsDTO>> mapOfOffice_origin_unit_organogramsDTOToshortNameEng;
    Map<String, Set<Office_origin_unit_organogramsDTO>> mapOfOffice_origin_unit_organogramsDTOToshortNameBng;
    Map<Integer, Set<Office_origin_unit_organogramsDTO>> mapOfOffice_origin_unit_organogramsDTOTodesignationLevel;
    Map<Integer, Set<Office_origin_unit_organogramsDTO>> mapOfOffice_origin_unit_organogramsDTOTodesignationSequence;
    Map<Boolean, Set<Office_origin_unit_organogramsDTO>> mapOfOffice_origin_unit_organogramsDTOTostatus;
    Map<Integer, Set<Office_origin_unit_organogramsDTO>> mapOfOffice_origin_unit_organogramsDTOTocreatedBy;
    Map<Integer, Set<Office_origin_unit_organogramsDTO>> mapOfOffice_origin_unit_organogramsDTOTomodifiedBy;
    Map<Long, Set<Office_origin_unit_organogramsDTO>> mapOfOffice_origin_unit_organogramsDTOTocreated;
    Map<Long, Set<Office_origin_unit_organogramsDTO>> mapOfOffice_origin_unit_organogramsDTOTomodified;
    Map<Long, Set<Office_origin_unit_organogramsDTO>> mapOfOffice_origin_unit_organogramsDTOTolastModificationTime;

    static Office_origin_unit_organogramsRepository instance = null;

    private Office_origin_unit_organogramsRepository() {
        mapOfOffice_origin_unit_organogramsDTOToid = new ConcurrentHashMap<>();
        mapOfOffice_origin_unit_organogramsDTOToofficeOriginUnitId = new ConcurrentHashMap<>();
        mapOfOffice_origin_unit_organogramsDTOTosuperiorUnitId = new ConcurrentHashMap<>();
        mapOfOffice_origin_unit_organogramsDTOTosuperiorDesignationId = new ConcurrentHashMap<>();
        mapOfOffice_origin_unit_organogramsDTOTodesignationEng = new ConcurrentHashMap<>();
        mapOfOffice_origin_unit_organogramsDTOTodesignationBng = new ConcurrentHashMap<>();
        mapOfOffice_origin_unit_organogramsDTOToshortNameEng = new ConcurrentHashMap<>();
        mapOfOffice_origin_unit_organogramsDTOToshortNameBng = new ConcurrentHashMap<>();
        mapOfOffice_origin_unit_organogramsDTOTodesignationLevel = new ConcurrentHashMap<>();
        mapOfOffice_origin_unit_organogramsDTOTodesignationSequence = new ConcurrentHashMap<>();
        mapOfOffice_origin_unit_organogramsDTOTostatus = new ConcurrentHashMap<>();
        mapOfOffice_origin_unit_organogramsDTOTocreatedBy = new ConcurrentHashMap<>();
        mapOfOffice_origin_unit_organogramsDTOTomodifiedBy = new ConcurrentHashMap<>();
        mapOfOffice_origin_unit_organogramsDTOTocreated = new ConcurrentHashMap<>();
        mapOfOffice_origin_unit_organogramsDTOTomodified = new ConcurrentHashMap<>();
        mapOfOffice_origin_unit_organogramsDTOTolastModificationTime = new ConcurrentHashMap<>();

        RepositoryManager.getInstance().addRepository(this);
    }

    public synchronized static Office_origin_unit_organogramsRepository getInstance() {
        if (instance == null) {
            instance = new Office_origin_unit_organogramsRepository();
        }
        return instance;
    }

    public void reload(boolean reloadAll) {
        /*try {
            List<Office_origin_unit_organogramsDTO> office_origin_unit_organogramsDTOs = office_origin_unit_organogramsDAO.getAllOffice_origin_unit_organograms(reloadAll);
            for (Office_origin_unit_organogramsDTO office_origin_unit_organogramsDTO : office_origin_unit_organogramsDTOs) {
                Office_origin_unit_organogramsDTO oldOffice_origin_unit_organogramsDTO = getOffice_origin_unit_organogramsDTOByid(office_origin_unit_organogramsDTO.id);
                if (oldOffice_origin_unit_organogramsDTO != null) {
                    mapOfOffice_origin_unit_organogramsDTOToid.remove(oldOffice_origin_unit_organogramsDTO.id);

                    if (mapOfOffice_origin_unit_organogramsDTOToofficeOriginUnitId.containsKey(oldOffice_origin_unit_organogramsDTO.officeOriginUnitId)) {
                        mapOfOffice_origin_unit_organogramsDTOToofficeOriginUnitId.get(oldOffice_origin_unit_organogramsDTO.officeOriginUnitId).remove(oldOffice_origin_unit_organogramsDTO);
                    }
                    if (mapOfOffice_origin_unit_organogramsDTOToofficeOriginUnitId.get(oldOffice_origin_unit_organogramsDTO.officeOriginUnitId).isEmpty()) {
                        mapOfOffice_origin_unit_organogramsDTOToofficeOriginUnitId.remove(oldOffice_origin_unit_organogramsDTO.officeOriginUnitId);
                    }

                    if (mapOfOffice_origin_unit_organogramsDTOTosuperiorUnitId.containsKey(oldOffice_origin_unit_organogramsDTO.superiorUnitId)) {
                        mapOfOffice_origin_unit_organogramsDTOTosuperiorUnitId.get(oldOffice_origin_unit_organogramsDTO.superiorUnitId).remove(oldOffice_origin_unit_organogramsDTO);
                    }
                    if (mapOfOffice_origin_unit_organogramsDTOTosuperiorUnitId.get(oldOffice_origin_unit_organogramsDTO.superiorUnitId).isEmpty()) {
                        mapOfOffice_origin_unit_organogramsDTOTosuperiorUnitId.remove(oldOffice_origin_unit_organogramsDTO.superiorUnitId);
                    }

                    if (mapOfOffice_origin_unit_organogramsDTOTosuperiorDesignationId.containsKey(oldOffice_origin_unit_organogramsDTO.superiorDesignationId)) {
                        mapOfOffice_origin_unit_organogramsDTOTosuperiorDesignationId.get(oldOffice_origin_unit_organogramsDTO.superiorDesignationId).remove(oldOffice_origin_unit_organogramsDTO);
                    }
                    if (mapOfOffice_origin_unit_organogramsDTOTosuperiorDesignationId.get(oldOffice_origin_unit_organogramsDTO.superiorDesignationId).isEmpty()) {
                        mapOfOffice_origin_unit_organogramsDTOTosuperiorDesignationId.remove(oldOffice_origin_unit_organogramsDTO.superiorDesignationId);
                    }

                    if (mapOfOffice_origin_unit_organogramsDTOTodesignationEng.containsKey(oldOffice_origin_unit_organogramsDTO.designationEng)) {
                        mapOfOffice_origin_unit_organogramsDTOTodesignationEng.get(oldOffice_origin_unit_organogramsDTO.designationEng).remove(oldOffice_origin_unit_organogramsDTO);
                    }
                    if (mapOfOffice_origin_unit_organogramsDTOTodesignationEng.get(oldOffice_origin_unit_organogramsDTO.designationEng).isEmpty()) {
                        mapOfOffice_origin_unit_organogramsDTOTodesignationEng.remove(oldOffice_origin_unit_organogramsDTO.designationEng);
                    }

                    if (mapOfOffice_origin_unit_organogramsDTOTodesignationBng.containsKey(oldOffice_origin_unit_organogramsDTO.designationBng)) {
                        mapOfOffice_origin_unit_organogramsDTOTodesignationBng.get(oldOffice_origin_unit_organogramsDTO.designationBng).remove(oldOffice_origin_unit_organogramsDTO);
                    }
                    if (mapOfOffice_origin_unit_organogramsDTOTodesignationBng.get(oldOffice_origin_unit_organogramsDTO.designationBng).isEmpty()) {
                        mapOfOffice_origin_unit_organogramsDTOTodesignationBng.remove(oldOffice_origin_unit_organogramsDTO.designationBng);
                    }

                    if (mapOfOffice_origin_unit_organogramsDTOToshortNameEng.containsKey(oldOffice_origin_unit_organogramsDTO.shortNameEng)) {
                        mapOfOffice_origin_unit_organogramsDTOToshortNameEng.get(oldOffice_origin_unit_organogramsDTO.shortNameEng).remove(oldOffice_origin_unit_organogramsDTO);
                    }
                    if (mapOfOffice_origin_unit_organogramsDTOToshortNameEng.get(oldOffice_origin_unit_organogramsDTO.shortNameEng).isEmpty()) {
                        mapOfOffice_origin_unit_organogramsDTOToshortNameEng.remove(oldOffice_origin_unit_organogramsDTO.shortNameEng);
                    }

                    if (mapOfOffice_origin_unit_organogramsDTOToshortNameBng.containsKey(oldOffice_origin_unit_organogramsDTO.shortNameBng)) {
                        mapOfOffice_origin_unit_organogramsDTOToshortNameBng.get(oldOffice_origin_unit_organogramsDTO.shortNameBng).remove(oldOffice_origin_unit_organogramsDTO);
                    }
                    if (mapOfOffice_origin_unit_organogramsDTOToshortNameBng.get(oldOffice_origin_unit_organogramsDTO.shortNameBng).isEmpty()) {
                        mapOfOffice_origin_unit_organogramsDTOToshortNameBng.remove(oldOffice_origin_unit_organogramsDTO.shortNameBng);
                    }

                    if (mapOfOffice_origin_unit_organogramsDTOTodesignationLevel.containsKey(oldOffice_origin_unit_organogramsDTO.designationLevel)) {
                        mapOfOffice_origin_unit_organogramsDTOTodesignationLevel.get(oldOffice_origin_unit_organogramsDTO.designationLevel).remove(oldOffice_origin_unit_organogramsDTO);
                    }
                    if (mapOfOffice_origin_unit_organogramsDTOTodesignationLevel.get(oldOffice_origin_unit_organogramsDTO.designationLevel).isEmpty()) {
                        mapOfOffice_origin_unit_organogramsDTOTodesignationLevel.remove(oldOffice_origin_unit_organogramsDTO.designationLevel);
                    }

                    if (mapOfOffice_origin_unit_organogramsDTOTodesignationSequence.containsKey(oldOffice_origin_unit_organogramsDTO.designationSequence)) {
                        mapOfOffice_origin_unit_organogramsDTOTodesignationSequence.get(oldOffice_origin_unit_organogramsDTO.designationSequence).remove(oldOffice_origin_unit_organogramsDTO);
                    }
                    if (mapOfOffice_origin_unit_organogramsDTOTodesignationSequence.get(oldOffice_origin_unit_organogramsDTO.designationSequence).isEmpty()) {
                        mapOfOffice_origin_unit_organogramsDTOTodesignationSequence.remove(oldOffice_origin_unit_organogramsDTO.designationSequence);
                    }

                    if (mapOfOffice_origin_unit_organogramsDTOTostatus.containsKey(oldOffice_origin_unit_organogramsDTO.status)) {
                        mapOfOffice_origin_unit_organogramsDTOTostatus.get(oldOffice_origin_unit_organogramsDTO.status).remove(oldOffice_origin_unit_organogramsDTO);
                    }
                    if (mapOfOffice_origin_unit_organogramsDTOTostatus.get(oldOffice_origin_unit_organogramsDTO.status).isEmpty()) {
                        mapOfOffice_origin_unit_organogramsDTOTostatus.remove(oldOffice_origin_unit_organogramsDTO.status);
                    }

                    if (mapOfOffice_origin_unit_organogramsDTOTocreatedBy.containsKey(oldOffice_origin_unit_organogramsDTO.createdBy)) {
                        mapOfOffice_origin_unit_organogramsDTOTocreatedBy.get(oldOffice_origin_unit_organogramsDTO.createdBy).remove(oldOffice_origin_unit_organogramsDTO);
                    }
                    if (mapOfOffice_origin_unit_organogramsDTOTocreatedBy.get(oldOffice_origin_unit_organogramsDTO.createdBy).isEmpty()) {
                        mapOfOffice_origin_unit_organogramsDTOTocreatedBy.remove(oldOffice_origin_unit_organogramsDTO.createdBy);
                    }

                    if (mapOfOffice_origin_unit_organogramsDTOTomodifiedBy.containsKey(oldOffice_origin_unit_organogramsDTO.modifiedBy)) {
                        mapOfOffice_origin_unit_organogramsDTOTomodifiedBy.get(oldOffice_origin_unit_organogramsDTO.modifiedBy).remove(oldOffice_origin_unit_organogramsDTO);
                    }
                    if (mapOfOffice_origin_unit_organogramsDTOTomodifiedBy.get(oldOffice_origin_unit_organogramsDTO.modifiedBy).isEmpty()) {
                        mapOfOffice_origin_unit_organogramsDTOTomodifiedBy.remove(oldOffice_origin_unit_organogramsDTO.modifiedBy);
                    }

                    if (mapOfOffice_origin_unit_organogramsDTOTocreated.containsKey(oldOffice_origin_unit_organogramsDTO.created)) {
                        mapOfOffice_origin_unit_organogramsDTOTocreated.get(oldOffice_origin_unit_organogramsDTO.created).remove(oldOffice_origin_unit_organogramsDTO);
                    }
                    if (mapOfOffice_origin_unit_organogramsDTOTocreated.get(oldOffice_origin_unit_organogramsDTO.created).isEmpty()) {
                        mapOfOffice_origin_unit_organogramsDTOTocreated.remove(oldOffice_origin_unit_organogramsDTO.created);
                    }

                    if (mapOfOffice_origin_unit_organogramsDTOTomodified.containsKey(oldOffice_origin_unit_organogramsDTO.modified)) {
                        mapOfOffice_origin_unit_organogramsDTOTomodified.get(oldOffice_origin_unit_organogramsDTO.modified).remove(oldOffice_origin_unit_organogramsDTO);
                    }
                    if (mapOfOffice_origin_unit_organogramsDTOTomodified.get(oldOffice_origin_unit_organogramsDTO.modified).isEmpty()) {
                        mapOfOffice_origin_unit_organogramsDTOTomodified.remove(oldOffice_origin_unit_organogramsDTO.modified);
                    }

                    if (mapOfOffice_origin_unit_organogramsDTOTolastModificationTime.containsKey(oldOffice_origin_unit_organogramsDTO.lastModificationTime)) {
                        mapOfOffice_origin_unit_organogramsDTOTolastModificationTime.get(oldOffice_origin_unit_organogramsDTO.lastModificationTime).remove(oldOffice_origin_unit_organogramsDTO);
                    }
                    if (mapOfOffice_origin_unit_organogramsDTOTolastModificationTime.get(oldOffice_origin_unit_organogramsDTO.lastModificationTime).isEmpty()) {
                        mapOfOffice_origin_unit_organogramsDTOTolastModificationTime.remove(oldOffice_origin_unit_organogramsDTO.lastModificationTime);
                    }


                }
                if (office_origin_unit_organogramsDTO.isDeleted == false) {

                    mapOfOffice_origin_unit_organogramsDTOToid.put(office_origin_unit_organogramsDTO.id, office_origin_unit_organogramsDTO);

                    if (!mapOfOffice_origin_unit_organogramsDTOToofficeOriginUnitId.containsKey(office_origin_unit_organogramsDTO.officeOriginUnitId)) {
                        mapOfOffice_origin_unit_organogramsDTOToofficeOriginUnitId.put(office_origin_unit_organogramsDTO.officeOriginUnitId, new HashSet<>());
                    }
                    mapOfOffice_origin_unit_organogramsDTOToofficeOriginUnitId.get(office_origin_unit_organogramsDTO.officeOriginUnitId).add(office_origin_unit_organogramsDTO);

                    if (!mapOfOffice_origin_unit_organogramsDTOTosuperiorUnitId.containsKey(office_origin_unit_organogramsDTO.superiorUnitId)) {
                        mapOfOffice_origin_unit_organogramsDTOTosuperiorUnitId.put(office_origin_unit_organogramsDTO.superiorUnitId, new HashSet<>());
                    }
                    mapOfOffice_origin_unit_organogramsDTOTosuperiorUnitId.get(office_origin_unit_organogramsDTO.superiorUnitId).add(office_origin_unit_organogramsDTO);

                    if (!mapOfOffice_origin_unit_organogramsDTOTosuperiorDesignationId.containsKey(office_origin_unit_organogramsDTO.superiorDesignationId)) {
                        mapOfOffice_origin_unit_organogramsDTOTosuperiorDesignationId.put(office_origin_unit_organogramsDTO.superiorDesignationId, new HashSet<>());
                    }
                    mapOfOffice_origin_unit_organogramsDTOTosuperiorDesignationId.get(office_origin_unit_organogramsDTO.superiorDesignationId).add(office_origin_unit_organogramsDTO);

                    if (!mapOfOffice_origin_unit_organogramsDTOTodesignationEng.containsKey(office_origin_unit_organogramsDTO.designationEng)) {
                        mapOfOffice_origin_unit_organogramsDTOTodesignationEng.put(office_origin_unit_organogramsDTO.designationEng, new HashSet<>());
                    }
                    mapOfOffice_origin_unit_organogramsDTOTodesignationEng.get(office_origin_unit_organogramsDTO.designationEng).add(office_origin_unit_organogramsDTO);

                    if (!mapOfOffice_origin_unit_organogramsDTOTodesignationBng.containsKey(office_origin_unit_organogramsDTO.designationBng)) {
                        mapOfOffice_origin_unit_organogramsDTOTodesignationBng.put(office_origin_unit_organogramsDTO.designationBng, new HashSet<>());
                    }
                    mapOfOffice_origin_unit_organogramsDTOTodesignationBng.get(office_origin_unit_organogramsDTO.designationBng).add(office_origin_unit_organogramsDTO);

                    if (!mapOfOffice_origin_unit_organogramsDTOToshortNameEng.containsKey(office_origin_unit_organogramsDTO.shortNameEng)) {
                        mapOfOffice_origin_unit_organogramsDTOToshortNameEng.put(office_origin_unit_organogramsDTO.shortNameEng, new HashSet<>());
                    }
                    mapOfOffice_origin_unit_organogramsDTOToshortNameEng.get(office_origin_unit_organogramsDTO.shortNameEng).add(office_origin_unit_organogramsDTO);

                    if (!mapOfOffice_origin_unit_organogramsDTOToshortNameBng.containsKey(office_origin_unit_organogramsDTO.shortNameBng)) {
                        mapOfOffice_origin_unit_organogramsDTOToshortNameBng.put(office_origin_unit_organogramsDTO.shortNameBng, new HashSet<>());
                    }
                    mapOfOffice_origin_unit_organogramsDTOToshortNameBng.get(office_origin_unit_organogramsDTO.shortNameBng).add(office_origin_unit_organogramsDTO);

                    if (!mapOfOffice_origin_unit_organogramsDTOTodesignationLevel.containsKey(office_origin_unit_organogramsDTO.designationLevel)) {
                        mapOfOffice_origin_unit_organogramsDTOTodesignationLevel.put(office_origin_unit_organogramsDTO.designationLevel, new HashSet<>());
                    }
                    mapOfOffice_origin_unit_organogramsDTOTodesignationLevel.get(office_origin_unit_organogramsDTO.designationLevel).add(office_origin_unit_organogramsDTO);

                    if (!mapOfOffice_origin_unit_organogramsDTOTodesignationSequence.containsKey(office_origin_unit_organogramsDTO.designationSequence)) {
                        mapOfOffice_origin_unit_organogramsDTOTodesignationSequence.put(office_origin_unit_organogramsDTO.designationSequence, new HashSet<>());
                    }
                    mapOfOffice_origin_unit_organogramsDTOTodesignationSequence.get(office_origin_unit_organogramsDTO.designationSequence).add(office_origin_unit_organogramsDTO);

                    if (!mapOfOffice_origin_unit_organogramsDTOTostatus.containsKey(office_origin_unit_organogramsDTO.status)) {
                        mapOfOffice_origin_unit_organogramsDTOTostatus.put(office_origin_unit_organogramsDTO.status, new HashSet<>());
                    }
                    mapOfOffice_origin_unit_organogramsDTOTostatus.get(office_origin_unit_organogramsDTO.status).add(office_origin_unit_organogramsDTO);

                    if (!mapOfOffice_origin_unit_organogramsDTOTocreatedBy.containsKey(office_origin_unit_organogramsDTO.createdBy)) {
                        mapOfOffice_origin_unit_organogramsDTOTocreatedBy.put(office_origin_unit_organogramsDTO.createdBy, new HashSet<>());
                    }
                    mapOfOffice_origin_unit_organogramsDTOTocreatedBy.get(office_origin_unit_organogramsDTO.createdBy).add(office_origin_unit_organogramsDTO);

                    if (!mapOfOffice_origin_unit_organogramsDTOTomodifiedBy.containsKey(office_origin_unit_organogramsDTO.modifiedBy)) {
                        mapOfOffice_origin_unit_organogramsDTOTomodifiedBy.put(office_origin_unit_organogramsDTO.modifiedBy, new HashSet<>());
                    }
                    mapOfOffice_origin_unit_organogramsDTOTomodifiedBy.get(office_origin_unit_organogramsDTO.modifiedBy).add(office_origin_unit_organogramsDTO);

                    if (!mapOfOffice_origin_unit_organogramsDTOTocreated.containsKey(office_origin_unit_organogramsDTO.created)) {
                        mapOfOffice_origin_unit_organogramsDTOTocreated.put(office_origin_unit_organogramsDTO.created, new HashSet<>());
                    }
                    mapOfOffice_origin_unit_organogramsDTOTocreated.get(office_origin_unit_organogramsDTO.created).add(office_origin_unit_organogramsDTO);

                    if (!mapOfOffice_origin_unit_organogramsDTOTomodified.containsKey(office_origin_unit_organogramsDTO.modified)) {
                        mapOfOffice_origin_unit_organogramsDTOTomodified.put(office_origin_unit_organogramsDTO.modified, new HashSet<>());
                    }
                    mapOfOffice_origin_unit_organogramsDTOTomodified.get(office_origin_unit_organogramsDTO.modified).add(office_origin_unit_organogramsDTO);

                    if (!mapOfOffice_origin_unit_organogramsDTOTolastModificationTime.containsKey(office_origin_unit_organogramsDTO.lastModificationTime)) {
                        mapOfOffice_origin_unit_organogramsDTOTolastModificationTime.put(office_origin_unit_organogramsDTO.lastModificationTime, new HashSet<>());
                    }
                    mapOfOffice_origin_unit_organogramsDTOTolastModificationTime.get(office_origin_unit_organogramsDTO.lastModificationTime).add(office_origin_unit_organogramsDTO);

                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }*/
    }

    public List<Office_origin_unit_organogramsDTO> getOffice_origin_unit_organogramsList() {
        //return new ArrayList<>(this.mapOfOffice_origin_unit_organogramsDTOToid.values());
        return office_origin_unit_organogramsDAO.getAllOffice_origin_unit_organograms(true);
    }


    public Office_origin_unit_organogramsDTO getOffice_origin_unit_organogramsDTOByid(long id) {
        return mapOfOffice_origin_unit_organogramsDTOToid.get(id);
    }


    public List<Office_origin_unit_organogramsDTO> getOffice_origin_unit_organogramsDTOByoffice_origin_unit_id(int office_origin_unit_id) {
        return new ArrayList<>(mapOfOffice_origin_unit_organogramsDTOToofficeOriginUnitId.getOrDefault(office_origin_unit_id, new HashSet<>()));
    }


    public List<Office_origin_unit_organogramsDTO> getOffice_origin_unit_organogramsDTOBysuperior_unit_id(int superior_unit_id) {
        return new ArrayList<>(mapOfOffice_origin_unit_organogramsDTOTosuperiorUnitId.getOrDefault(superior_unit_id, new HashSet<>()));
    }


    public List<Office_origin_unit_organogramsDTO> getOffice_origin_unit_organogramsDTOBysuperior_designation_id(int superior_designation_id) {
        return new ArrayList<>(mapOfOffice_origin_unit_organogramsDTOTosuperiorDesignationId.getOrDefault(superior_designation_id, new HashSet<>()));
    }


    public List<Office_origin_unit_organogramsDTO> getOffice_origin_unit_organogramsDTOBydesignation_eng(String designation_eng) {
        return new ArrayList<>(mapOfOffice_origin_unit_organogramsDTOTodesignationEng.getOrDefault(designation_eng, new HashSet<>()));
    }


    public List<Office_origin_unit_organogramsDTO> getOffice_origin_unit_organogramsDTOBydesignation_bng(String designation_bng) {
        return new ArrayList<>(mapOfOffice_origin_unit_organogramsDTOTodesignationBng.getOrDefault(designation_bng, new HashSet<>()));
    }


    public List<Office_origin_unit_organogramsDTO> getOffice_origin_unit_organogramsDTOByshort_name_eng(String short_name_eng) {
        return new ArrayList<>(mapOfOffice_origin_unit_organogramsDTOToshortNameEng.getOrDefault(short_name_eng, new HashSet<>()));
    }


    public List<Office_origin_unit_organogramsDTO> getOffice_origin_unit_organogramsDTOByshort_name_bng(String short_name_bng) {
        return new ArrayList<>(mapOfOffice_origin_unit_organogramsDTOToshortNameBng.getOrDefault(short_name_bng, new HashSet<>()));
    }


    public List<Office_origin_unit_organogramsDTO> getOffice_origin_unit_organogramsDTOBydesignation_level(int designation_level) {
        return new ArrayList<>(mapOfOffice_origin_unit_organogramsDTOTodesignationLevel.getOrDefault(designation_level, new HashSet<>()));
    }


    public List<Office_origin_unit_organogramsDTO> getOffice_origin_unit_organogramsDTOBydesignation_sequence(int designation_sequence) {
        return new ArrayList<>(mapOfOffice_origin_unit_organogramsDTOTodesignationSequence.getOrDefault(designation_sequence, new HashSet<>()));
    }


    public List<Office_origin_unit_organogramsDTO> getOffice_origin_unit_organogramsDTOBystatus(boolean status) {
        return new ArrayList<>(mapOfOffice_origin_unit_organogramsDTOTostatus.getOrDefault(status, new HashSet<>()));
    }


    public List<Office_origin_unit_organogramsDTO> getOffice_origin_unit_organogramsDTOBycreated_by(int created_by) {
        return new ArrayList<>(mapOfOffice_origin_unit_organogramsDTOTocreatedBy.getOrDefault(created_by, new HashSet<>()));
    }


    public List<Office_origin_unit_organogramsDTO> getOffice_origin_unit_organogramsDTOBymodified_by(int modified_by) {
        return new ArrayList<>(mapOfOffice_origin_unit_organogramsDTOTomodifiedBy.getOrDefault(modified_by, new HashSet<>()));
    }


    public List<Office_origin_unit_organogramsDTO> getOffice_origin_unit_organogramsDTOBycreated(long created) {
        return new ArrayList<>(mapOfOffice_origin_unit_organogramsDTOTocreated.getOrDefault(created, new HashSet<>()));
    }


    public List<Office_origin_unit_organogramsDTO> getOffice_origin_unit_organogramsDTOBymodified(long modified) {
        return new ArrayList<>(mapOfOffice_origin_unit_organogramsDTOTomodified.getOrDefault(modified, new HashSet<>()));
    }


    public List<Office_origin_unit_organogramsDTO> getOffice_origin_unit_organogramsDTOBylastModificationTime(long lastModificationTime) {
        return new ArrayList<>(mapOfOffice_origin_unit_organogramsDTOTolastModificationTime.getOrDefault(lastModificationTime, new HashSet<>()));
    }

    public void addNewDto(Office_origin_unit_organogramsDTO dto){
        mapOfOffice_origin_unit_organogramsDTOToid.put(dto.id, dto);
    }


    @Override
    public String getTableName() {
        return "office_origin_unit_organograms";
    }
}


