package office_origin_unit_organograms;

import java.io.IOException;
import java.io.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import com.google.gson.Gson;
import common.ApiResponse;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import test_lib.util.Pair;
import org.apache.log4j.Logger;

import login.LoginDTO;


import sessionmanager.SessionConstants;

import util.CommonConstant;
import util.RecordNavigationManager2;

import javax.servlet.http.*;
import java.util.Date;

@WebServlet("/Office_origin_unit_organogramsServlet")
@MultipartConfig
public class Office_origin_unit_organogramsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Office_origin_unit_organogramsServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getAddPage":
                    getAddPage(request, response);
                    break;
                case "getEditPage":
                    getOffice_origin_unit_organograms(request, response);
                    break;
                case "getURL":
                    String URL = request.getParameter("URL");
                    logger.debug("URL = " + URL);
                    response.sendRedirect(URL);
                    break;
                case "search":
                    logger.debug("search requested");
                    searchOffice_origin_unit_organograms(request, response);
                    break;
                case "officeOriginUnitOrganogramsSingle": {
                    Office_origin_unit_organogramsDAO office_origin_unit_organogramsDAO = new Office_origin_unit_organogramsDAO();
                    int id = Integer.parseInt(request.getParameter("id"));
                    String data = office_origin_unit_organogramsDAO.getOfficeOriginUnitOrganogramsSingle(id);

                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();
                    out.print(data);
                    out.flush();
                    break;
                }
                case "officeOriginUnits": {
                    Office_origin_unit_organogramsDAO office_origin_unit_organogramsDAO = new Office_origin_unit_organogramsDAO();

                    int office_ministry_id = Integer.parseInt(request.getParameter("office_ministry_id"));
                    int office_layer_id = Integer.parseInt(request.getParameter("office_layer_id"));
                    int office_origin_id = Integer.parseInt(request.getParameter("office_origin_id"));

                    String data = office_origin_unit_organogramsDAO.getOfficeOriginUnits(office_ministry_id, office_layer_id, office_origin_id);

                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();

                    out.print(data);
                    out.flush();
                    break;
                }
                case "officeOriginUnitOrganograms": {
                    Office_origin_unit_organogramsDAO office_origin_unit_organogramsDAO = new Office_origin_unit_organogramsDAO();

                    int office_origin_unit_id = Integer.parseInt(request.getParameter("office_origin_unit_id"));
                    String data = office_origin_unit_organogramsDAO.getOfficeOriginUnitOrganograms(office_origin_unit_id);

                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();

                    out.print(data);
                    out.flush();
                    break;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("ID", -1L);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("office_origin_unit_organograms/office_origin_unit_organogramsEdit.jsp");
        requestDispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String actionType = request.getParameter("actionType");
            logger.debug("actionType = " + actionType);
            switch (actionType) {
                case "add":
                    addOffice_origin_unit_organograms(request, response, true);
                    break;
                case "ajax_add":
                    try{
                        addOrEdit(request, true);
                        ApiResponse.sendSuccessResponse(response,"");
                    }catch (Exception ex){
                        ex.printStackTrace();
                        ApiResponse.sendErrorResponse(response,ex.getMessage());
                    }
                    break;
                case "edit":
                    addOffice_origin_unit_organograms(request, response, false);
                    break;
                case "delete":
                    deleteOffice_origin_unit_organograms(request, response);
                    break;
                case "search":
                    searchOffice_origin_unit_organograms(request, response);
                    break;
                case "getGeo":
                    logger.debug("going to geoloc ");
                    request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void addOrEdit(HttpServletRequest request, Boolean addFlag) throws Exception {
        Office_origin_unit_organogramsDAO office_origin_unit_organogramsDAO = new Office_origin_unit_organogramsDAO();
        Office_origin_unit_organogramsDTO office_origin_unit_organogramsDTO;
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        if (addFlag) {
            office_origin_unit_organogramsDTO = new Office_origin_unit_organogramsDTO();
        } else {
            office_origin_unit_organogramsDTO = office_origin_unit_organogramsDAO.getOffice_origin_unit_organogramsDTOByid(Long.parseLong(request.getParameter("identity")));
        }
        office_origin_unit_organogramsDTO.id = CommonConstant.DEFAULT_OFFICE_ORIGIN_UNIT_ORGANOGRAM_ID;

        String Value;

        Value = request.getParameter("officeId");
        logger.debug("officeId = " + Value);
        office_origin_unit_organogramsDTO.officeUnitId = Long.parseLong(Value);

        office_origin_unit_organogramsDTO.officeOriginUnitId = 1;

        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(office_origin_unit_organogramsDTO.officeUnitId);
        office_origin_unit_organogramsDTO.superiorUnitId = Long.valueOf(officeUnitsDTO.iD).intValue();

        Value = request.getParameter("superiorDesignationId");
        logger.debug("superiorDesignationId = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            office_origin_unit_organogramsDTO.superiorDesignationId = Integer.parseInt(Value);
        } else {
            logger.debug("FieldName has a null value, not updating" + " = " + Value);
        }

        Value = request.getParameter("designationEng");
        logger.debug("designationEng = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            office_origin_unit_organogramsDTO.designationEng = (Value);
        } else {
            logger.debug("FieldName has a null value, not updating" + " = " + Value);
        }

        Value = request.getParameter("designationBng");
        logger.debug("designationBng = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            office_origin_unit_organogramsDTO.designationBng = (Value);
        } else {
            logger.debug("FieldName has a null value, not updating" + " = " + Value);
        }

        Value = request.getParameter("designationSequence");
        logger.debug("designationSequence = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            office_origin_unit_organogramsDTO.designationSequence = Integer.parseInt(Value);
        } else {
            logger.debug("FieldName has a null value, not updating" + " = " + Value);
        }

        Value = request.getParameter("jobGrade");
        logger.debug("jobGrade = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            office_origin_unit_organogramsDTO.jobGradeTypeCat = Integer.parseInt(Value);
        } else {
            logger.debug("FieldName has a null value, not updating" + " = " + Value);
        }

        if (addFlag) {
            office_origin_unit_organogramsDTO.created = new Date().getTime();
            office_origin_unit_organogramsDTO.modified = office_origin_unit_organogramsDTO.created;
            office_origin_unit_organogramsDTO.createdBy = Long.valueOf(loginDTO.userID).intValue();
            office_origin_unit_organogramsDTO.modifiedBy = office_origin_unit_organogramsDTO.createdBy;
            office_origin_unit_organogramsDAO.addOffice_unit_organograms(office_origin_unit_organogramsDTO);
        } else {
            office_origin_unit_organogramsDAO.updateOffice_origin_unit_organograms(office_origin_unit_organogramsDTO);
        }
    }

    private void addOffice_origin_unit_organograms(HttpServletRequest request, HttpServletResponse response, Boolean addFlag) throws Exception {
        addOrEdit(request, addFlag);
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        Pair<String, Boolean> result = new Pair<>("success", true);
        String r = new Gson().toJson(result);

        out.print(r);
        out.flush();
    }

    private void deleteOffice_origin_unit_organograms(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            String[] IDsToDelete = request.getParameterValues("ID");
            for (String s : IDsToDelete) {
                long id = Long.parseLong(s);
                new Office_origin_unit_organogramsDAO().deleteOffice_origin_unit_organogramsByid(id);
            }
        } catch (Exception ex) {
            logger.debug(ex);
        }
        response.sendRedirect("Office_origin_unit_organogramsServlet?actionType=search");
    }

    private void getOffice_origin_unit_organograms(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("in getOffice_origin_unit_organograms");
        Office_origin_unit_organogramsDTO office_origin_unit_organogramsDTO = null;
        try {
            office_origin_unit_organogramsDTO = new Office_origin_unit_organogramsDAO().getOffice_origin_unit_organogramsDTOByid(Long.parseLong(request.getParameter("ID")));
            request.setAttribute("ID", office_origin_unit_organogramsDTO.id);
            request.setAttribute("office_origin_unit_organogramsDTO", office_origin_unit_organogramsDTO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "office_origin_unit_organograms/office_origin_unit_organogramsInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "office_origin_unit_organograms/office_origin_unit_organogramsSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "office_origin_unit_organograms/office_origin_unit_organogramsEditBody.jsp?actionType=edit";
                } else {
                    URL = "office_origin_unit_organograms/office_origin_unit_organogramsEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void searchOffice_origin_unit_organograms(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("in  searchOffice_origin_unit_organograms 1");
        Office_origin_unit_organogramsDAO office_origin_unit_organogramsDAO = new Office_origin_unit_organogramsDAO();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        logger.debug("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager2 rnManager = new RecordNavigationManager2(
                SessionConstants.NAV_OFFICE_ORIGIN_UNIT_ORGANOGRAMS,
                request,
                office_origin_unit_organogramsDAO,
                SessionConstants.VIEW_OFFICE_ORIGIN_UNIT_ORGANOGRAMS,
                SessionConstants.SEARCH_OFFICE_ORIGIN_UNIT_ORGANOGRAMS,
                "office_origin_unit_organograms");
        try {
            logger.debug("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            logger.debug("failed to dojob" + e);
        }

        RequestDispatcher rd;
        if (!hasAjax) {
            logger.debug("Going to office_origin_unit_organograms/office_origin_unit_organogramsSearch.jsp");
            rd = request.getRequestDispatcher("office_origin_unit_organograms/office_origin_unit_organogramsSearch.jsp");
        } else {
            logger.debug("Going to office_origin_unit_organograms/office_origin_unit_organogramsSearchForm.jsp");
            rd = request.getRequestDispatcher("office_origin_unit_organograms/office_origin_unit_organogramsSearchForm.jsp");
        }
        rd.forward(request, response);
    }
}

