package employee_family_info;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import common.EmployeeCommonDAOService;
import common.NameDTO;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import org.apache.log4j.Logger;
import profession.ProfessionRepository;
import relation.RelationRepository;
import util.CommonDTO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@SuppressWarnings({"unused", "Duplicates"})
public class Employee_family_infoDAO implements EmployeeCommonDAOService<Employee_family_infoDTO> {

    private static final Logger logger = Logger.getLogger(Employee_family_infoDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (employee_records_id,first_name_en,first_name_bn,relation_type," +
            " dob,profession_type,gender_cat,religion_type,national_id_no,UBRN,TIN,passport_no,passport_issue_date,passport_expiry_date,nationality_type," +
            " blood_group_cat,occupation_type,organization_name,work_place,present_address,permanent_address,home_district,email,mobile_number_1," +
            " mobile_number_2,files_dropzone,modified_by,search_column,lastModificationTime,inserted_by_user_id,insertion_date,isDeleted,ID) " +
            " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private static final String updateQuery = "UPDATE {tableName} SET employee_records_id=?,first_name_en=?,first_name_bn=?,relation_type=?,dob=?," +
            " profession_type=?,gender_cat=?,religion_type=?,national_id_no=?,UBRN=?,TIN=?,passport_no=?,passport_issue_date=?,passport_expiry_date=?," +
            " nationality_type=?,blood_group_cat=?,occupation_type=?,organization_name=?,work_place=?,present_address=?,permanent_address=?," +
            " home_district=?,email=?,mobile_number_1=?,mobile_number_2=?,files_dropzone=?,modified_by=?,search_column=?," +
            " lastModificationTime =?  WHERE ID = ?";

    private static final String getByEmployeeId = "SELECT * FROM employee_family_info WHERE employee_records_id= ? AND isDeleted = 0 ORDER BY lastModificationTime DESC";
    String deleteForSpouse = "UPDATE employee_family_info SET isDeleted = 2,lastModificationTime= ?  WHERE ID = ?";

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Employee_family_infoDAO INSTANCE = new Employee_family_infoDAO();
    }

    public static Employee_family_infoDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    private Employee_family_infoDAO() {
        searchMap.put("first_name_en", " and (first_name_en = ?)");
        searchMap.put("relation_type", " and (relation_type = ?)");
        searchMap.put("national_id_no", " and (national_id_no = ?)");
        searchMap.put("mobile_number_1", " and (mobile_number_1 = ?)");
        searchMap.put("employee_records_id_internal", " and (employee_records_id = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    @Override
    public void set(PreparedStatement ps, Employee_family_infoDTO employee_family_infoDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(employee_family_infoDTO);
        ps.setObject(++index, employee_family_infoDTO.employeeRecordsId);
        ps.setObject(++index, employee_family_infoDTO.firstNameEn);
        ps.setObject(++index, employee_family_infoDTO.firstNameBn);
        ps.setObject(++index, employee_family_infoDTO.relationType);
        ps.setObject(++index, employee_family_infoDTO.dob);
        ps.setObject(++index, employee_family_infoDTO.professionType);
        ps.setObject(++index, employee_family_infoDTO.genderCat);
        ps.setObject(++index, employee_family_infoDTO.religionType);
        ps.setObject(++index, employee_family_infoDTO.nationalIdNo);
        ps.setObject(++index, employee_family_infoDTO.uBRN);
        ps.setObject(++index, employee_family_infoDTO.tIN);
        ps.setObject(++index, employee_family_infoDTO.passportNo);
        ps.setObject(++index, employee_family_infoDTO.passportIssueDate);
        ps.setObject(++index, employee_family_infoDTO.passportExpiryDate);
        ps.setObject(++index, employee_family_infoDTO.nationalityType);
        ps.setObject(++index, employee_family_infoDTO.bloodGroupCat);
        ps.setObject(++index, employee_family_infoDTO.occupationType);
        ps.setObject(++index, employee_family_infoDTO.organizationName);
        ps.setObject(++index, employee_family_infoDTO.workPlace);
        ps.setObject(++index, employee_family_infoDTO.presentAddress);
        ps.setObject(++index, employee_family_infoDTO.permanentAddress);
        ps.setObject(++index, employee_family_infoDTO.homeDistrict);
        ps.setObject(++index, employee_family_infoDTO.email);
        ps.setObject(++index, employee_family_infoDTO.mobileNumber1);
        ps.setObject(++index, employee_family_infoDTO.mobileNumber2);
        ps.setObject(++index, employee_family_infoDTO.filesDropzone);
        ps.setObject(++index, employee_family_infoDTO.modifiedBy);
        ps.setObject(++index, employee_family_infoDTO.searchColumn);
        ps.setObject(++index, employee_family_infoDTO.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, employee_family_infoDTO.insertedByUserId);
            ps.setObject(++index, employee_family_infoDTO.insertionDate);
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, employee_family_infoDTO.iD);
    }

    public void setSearchColumn(Employee_family_infoDTO employee_family_infoDTO) {
        StringBuilder searchColumnBuilder = new StringBuilder();
        try {
            Employee_recordsDTO employeeRecordsDTO = new Employee_recordsDAO().getEmployee_recordsDTOByID(employee_family_infoDTO.employeeRecordsId);
            if (employeeRecordsDTO != null) {
                if (employeeRecordsDTO.nameEng != null && employeeRecordsDTO.nameEng.trim().length() > 0) {
                    searchColumnBuilder.append(employeeRecordsDTO.nameEng.trim()).append(" ");
                }
                if (employeeRecordsDTO.nameBng != null && employeeRecordsDTO.nameBng.trim().length() > 0) {
                    searchColumnBuilder.append(employeeRecordsDTO.nameBng.trim()).append(" ");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        searchColumnBuilder.append(employee_family_infoDTO.firstNameEn).append(" ");
        searchColumnBuilder.append(employee_family_infoDTO.firstNameBn).append(" ");
        searchColumnBuilder.append(employee_family_infoDTO.passportNo).append(" ");
        searchColumnBuilder.append(employee_family_infoDTO.tIN).append(" ");
        searchColumnBuilder.append(employee_family_infoDTO.mobileNumber1).append(" ");
        searchColumnBuilder.append(employee_family_infoDTO.mobileNumber2).append(" ");
        employee_family_infoDTO.searchColumn = searchColumnBuilder.toString();
    }

    @Override
    public Employee_family_infoDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Employee_family_infoDTO employee_family_infoDTO = new Employee_family_infoDTO();
            employee_family_infoDTO.iD = rs.getLong("ID");
            employee_family_infoDTO.employeeRecordsId = rs.getLong("employee_records_id");
            employee_family_infoDTO.firstNameEn = rs.getString("first_name_en");
            employee_family_infoDTO.firstNameBn = rs.getString("first_name_bn");
            employee_family_infoDTO.relationType = rs.getLong("relation_type");
            employee_family_infoDTO.dob = rs.getLong("dob");
            employee_family_infoDTO.professionType = rs.getLong("profession_type");
            employee_family_infoDTO.genderCat = rs.getInt("gender_cat");
            employee_family_infoDTO.religionType = rs.getLong("religion_type");
            employee_family_infoDTO.nationalIdNo = rs.getString("national_id_no");
            employee_family_infoDTO.uBRN = rs.getString("UBRN");
            employee_family_infoDTO.tIN = rs.getString("TIN");
            employee_family_infoDTO.passportNo = rs.getString("passport_no");
            employee_family_infoDTO.passportIssueDate = rs.getLong("passport_issue_date");
            employee_family_infoDTO.passportExpiryDate = rs.getLong("passport_expiry_date");
            employee_family_infoDTO.nationalityType = rs.getLong("nationality_type");
            employee_family_infoDTO.bloodGroupCat = rs.getInt("blood_group_cat");
            employee_family_infoDTO.occupationType = rs.getLong("occupation_type");
            employee_family_infoDTO.organizationName = rs.getString("organization_name");
            employee_family_infoDTO.workPlace = rs.getString("work_place");
            employee_family_infoDTO.presentAddress = rs.getString("present_address");
            employee_family_infoDTO.permanentAddress = rs.getString("permanent_address");
            employee_family_infoDTO.homeDistrict = rs.getString("home_district");
            employee_family_infoDTO.email = rs.getString("email");
            employee_family_infoDTO.mobileNumber1 = rs.getString("mobile_number_1");
            employee_family_infoDTO.mobileNumber2 = rs.getString("mobile_number_2");
            employee_family_infoDTO.filesDropzone = rs.getLong("files_dropzone");
            employee_family_infoDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
            employee_family_infoDTO.insertionDate = rs.getLong("insertion_date");
            employee_family_infoDTO.modifiedBy = rs.getString("modified_by");
            employee_family_infoDTO.searchColumn = rs.getString("search_column");
            employee_family_infoDTO.isDeleted = rs.getInt("isDeleted");
            employee_family_infoDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return employee_family_infoDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "employee_family_info";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }


    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_family_infoDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_family_infoDTO) commonDTO, updateQuery, false);
    }

    public List<Employee_family_infoDtoWithValue> getDtoWithValueByEmployeeRecordsId(long employeeRecordsId) {
        return getByEmployeeId(employeeRecordsId)
                .stream().map(this::buildEmployeeFamilyInfo)
                .collect(Collectors.toList());
    }

    private Employee_family_infoDtoWithValue buildEmployeeFamilyInfo(Employee_family_infoDTO dto) {
        Employee_family_infoDtoWithValue model = new Employee_family_infoDtoWithValue();
        model.dto = dto;
        NameDTO relationType = RelationRepository.getInstance().getDTOByID(dto.relationType);
        model.relationEng = relationType.nameEn;
        model.relationBng = relationType.nameBn;
        NameDTO professionType = ProfessionRepository.getInstance().getDTOByID(dto.professionType);
        if (professionType != null) {
            model.professionEng = professionType.nameEn;
            model.professionBng = professionType.nameBn;
        }
        return model;
    }

    public void deleteBySpouse(long ID) throws Exception {
        long lastModificationTime = System.currentTimeMillis();
        AtomicReference<Exception> atomicReference = new AtomicReference<>();
        Connection connection = CommonDAOService.CONNECTION_THREAD_LOCAL.get();
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                ps.setObject(1, lastModificationTime);
                ps.setObject(2, ID);
                ps.executeUpdate();
                recordUpdateTime(connection, "employee_family_info", System.currentTimeMillis());
            } catch (Exception e) {
                e.printStackTrace();
                atomicReference.set(e);
            }
        }, connection, deleteForSpouse);
        if (atomicReference.get() != null) {
            throw atomicReference.get();
        }

    }
}