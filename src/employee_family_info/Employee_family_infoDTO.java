package employee_family_info;

import sessionmanager.SessionConstants;
import util.CommonEmployeeDTO;

public class Employee_family_infoDTO extends CommonEmployeeDTO {
    public String firstNameEn = "";
    public String lastNameEn = "";
    public String firstNameBn = "";
    public String lastNameBn = "";
    public long relationType = 0;
    public long dob = SessionConstants.MIN_DATE;
    public long professionType = 0;
    public int genderCat = 0;
    public long religionType = 0;
    public String nationalIdNo = "";
    public String uBRN = "";
    public String tIN = "";
    public String passportNo = "";
    public long passportIssueDate = SessionConstants.MIN_DATE;
    public long passportExpiryDate = SessionConstants.MIN_DATE;
    public long nationalityType = 0;
    public int bloodGroupCat = 0;
    public long occupationType = 0;
    public String organizationName = "";
    public String workPlace = "";
    public String presentAddress = "";
    public String permanentAddress = "";
    public String homeDistrict = "";
    public String email = "";
    public String mobileNumber1 = "";
    public String mobileNumber2 = "";
    public long filesDropzone = 0;
    public long insertedByUserId = 0;
    public long insertionDate = 0;
    public String modifiedBy = "";

    @Override
    public String toString() {
        return "$Employee_family_infoDTO[" +
                " iD = " + iD +
                " employeeRecordsId = " + employeeRecordsId +
                " firstNameEn = " + firstNameEn +
                " firstNameBn = " + firstNameBn +
                " relationType = " + relationType +
                " dob = " + dob +
                " professionType = " + professionType +
                " genderCat = " + genderCat +
                " religionType = " + religionType +
                " nationalIdNo = " + nationalIdNo +
                " uBRN = " + uBRN +
                " tIN = " + tIN +
                " passportNo = " + passportNo +
                " passportIssueDate = " + passportIssueDate +
                " passportExpiryDate = " + passportExpiryDate +
                " nationalityType = " + nationalityType +
                " bloodGroupCat = " + bloodGroupCat +
                " occupationType = " + occupationType +
                " organizationName = " + organizationName +
                " workPlace = " + workPlace +
                " presentAddress = " + presentAddress +
                " permanentAddress = " + permanentAddress +
                " homeDistrict = " + homeDistrict +
                " email = " + email +
                " mobileNumber1 = " + mobileNumber1 +
                " mobileNumber2 = " + mobileNumber2 +
                " filesDropzone = " + filesDropzone +
                " insertedByUserId = " + insertedByUserId +
                " insertionDate = " + insertionDate +
                " modifiedBy = " + modifiedBy +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}