package employee_family_info;

import common.BaseServlet;
import common.EmployeeServletService;
import employee_records.Employee_recordsRepository;
import nationality.NationalityRepository;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import profession.ProfessionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("Duplicates")
@WebServlet("/Employee_family_infoServlet")
@MultipartConfig
public class Employee_family_infoServlet extends BaseServlet implements EmployeeServletService {
    private static final long serialVersionUID = 1L;

    private final Employee_family_infoDAO employeeFamilyInfoDAO = Employee_family_infoDAO.getInstance();

    @Override
    public String getTableName() {
        return employeeFamilyInfoDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Employee_family_infoServlet";
    }

    @Override
    public Employee_family_infoDAO getCommonDAOService() {
        return employeeFamilyInfoDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Employee_family_infoDTO employee_family_infoDTO;
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        long currentTime = System.currentTimeMillis();
        if (addFlag) {
            employee_family_infoDTO = new Employee_family_infoDTO();
            long empId;
            try{
                empId = Long.parseLong(request.getParameter("empId"));
            }catch (Exception ex){
                ex.printStackTrace();
                throw new Exception(isLangEng?"empId is not found":"empId পাওয়া যায়নি");
            }
            if(Employee_recordsRepository.getInstance().getById(empId) == null){
                throw new Exception(isLangEng?"Employee information is not found":"কর্মকর্তার তথ্য পাওয়া যায়নি");
            }
            employee_family_infoDTO.employeeRecordsId = empId;
            employee_family_infoDTO.insertedByUserId = userDTO.ID;
            employee_family_infoDTO.insertionDate = currentTime;
        } else {
            employee_family_infoDTO = employeeFamilyInfoDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        employee_family_infoDTO.modifiedBy = String.valueOf(userDTO.ID);
        employee_family_infoDTO.lastModificationTime = currentTime;

        employee_family_infoDTO.firstNameEn = Jsoup.clean(request.getParameter("firstNameEn"), Whitelist.simpleText());
        employee_family_infoDTO.firstNameBn = Jsoup.clean(request.getParameter("firstNameBn"), Whitelist.simpleText());
        employee_family_infoDTO.relationType = Long.parseLong(Jsoup.clean(request.getParameter("relationType"), Whitelist.simpleText()));
        String Value;

        try {
            Date d = f.parse(request.getParameter("dob"));
            employee_family_infoDTO.dob = d.getTime();
        } catch (Exception e) {
            logger.error(e.getMessage());
            employee_family_infoDTO.dob = SessionConstants.MIN_DATE;
        }

        Value = request.getParameter("professionType");
        if (Value != null && Value.trim().length() > 0 && Utils.digitOnlyOrEmptyValid(Value.trim())) {
            long id = Long.parseLong(Value.trim());
            if (ProfessionRepository.getInstance().getDTOByID(id) != null) {
                employee_family_infoDTO.professionType = id;
            } else {
                employee_family_infoDTO.professionType = 0;
            }
        } else {
            employee_family_infoDTO.professionType = 0;
        }

        employee_family_infoDTO.genderCat = Integer.parseInt(request.getParameter("genderCat"));

        Value = request.getParameter("religionType");
        if (Value != null && Value.trim().length() > 0 && Utils.digitOnlyOrEmptyValid(Value.trim())) {
            employee_family_infoDTO.religionType = Long.parseLong(Value.trim());
        } else {
            employee_family_infoDTO.religionType = 0L;
        }

        employee_family_infoDTO.nationalIdNo = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("nationalIdNo"));
        employee_family_infoDTO.uBRN = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("uBRN"));
        employee_family_infoDTO.tIN = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("tIN"));
        employee_family_infoDTO.passportNo = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("passportNo"));

        if (request.getParameter("passportIssueDate") != null && request.getParameter("passportIssueDate").trim().length()>0) {
            try {
                employee_family_infoDTO.passportIssueDate = f.parse(request.getParameter("passportIssueDate").trim()).getTime();
            } catch (Exception e) {
                logger.error(e.getMessage());
                employee_family_infoDTO.passportIssueDate = SessionConstants.MIN_DATE;
            }
        } else {
            employee_family_infoDTO.passportIssueDate = SessionConstants.MIN_DATE;
        }

        if (request.getParameter("passportExpiryDate") != null && request.getParameter("passportExpiryDate").trim().length()>0) {
            try {
                employee_family_infoDTO.passportExpiryDate = f.parse(request.getParameter("passportExpiryDate")).getTime();
            } catch (Exception e) {
                logger.error(e.getMessage());
                employee_family_infoDTO.passportExpiryDate = SessionConstants.MIN_DATE;
            }
        } else {
            employee_family_infoDTO.passportExpiryDate = SessionConstants.MIN_DATE;
        }

        Value = request.getParameter("nationalityType");
        if (Value != null && Value.trim().length() > 0 && Utils.digitOnlyOrEmptyValid(Value.trim())) {
            long id = Long.parseLong(Value.trim());
            if (NationalityRepository.getInstance().getDTOByID(id) != null) {
                employee_family_infoDTO.nationalityType = id;
            }
        } else {
            employee_family_infoDTO.nationalityType = 0L;
        }

        Value = request.getParameter("bloodGroupCat");
        if (Value != null && Value.trim().length() > 0 && Utils.digitOnlyOrEmptyValid(Value.trim())) {
            try {
                employee_family_infoDTO.bloodGroupCat = Integer.parseInt(Value);
            } catch (Exception ex) {
                employee_family_infoDTO.bloodGroupCat = 0;
            }
        } else {
            employee_family_infoDTO.bloodGroupCat = 0;
        }

        employee_family_infoDTO.organizationName = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("organizationName"));
        employee_family_infoDTO.workPlace = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("workPlace"));

        Value = request.getParameter("presentAddress");
        if (Value != null && Value.trim().length() > 0 && Utils.checkValidAddress(Value.trim())) {
            employee_family_infoDTO.presentAddress = Value.trim();
        } else {
            employee_family_infoDTO.presentAddress = "";
        }

        Value = request.getParameter("permanentAddress");
        if (Value != null && Value.trim().length() > 0 && Utils.checkValidAddress(Value.trim())) {
            employee_family_infoDTO.permanentAddress = Value.trim();
        } else {
            employee_family_infoDTO.permanentAddress = "";
        }

        employee_family_infoDTO.homeDistrict = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("homeDistrict"));
        employee_family_infoDTO.email = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("email"));

        Value = request.getParameter("mobileNumber1");
        Value = Value == null ? null : Value.trim();
        if (Value != null && Value.length() > 0) {
            if (Utils.isMobileNumberValid("88"+Value)) {
                employee_family_infoDTO.mobileNumber1 = "88"+Value;
            } else {
                throw new Exception(isLangEng ? "Please enter valid personal mobile number (01XXXXXXXXX)" : "অনুগ্রহ করে সঠিক ব্যক্তিগত মোবাইল নাম্বার দিন (01XXXXXXXXX)");
            }
        } else {
            employee_family_infoDTO.mobileNumber1 = "";
        }

        Value = request.getParameter("mobileNumber2");
        Value = Value == null ? null : Value.trim();
        if (Value != null && Value.length() > 0) {
            if (Utils.isMobileNumberValid("88"+Value)) {
                employee_family_infoDTO.mobileNumber2 = "88"+Value;
            } else {
                throw new Exception(isLangEng ? "Please enter valid alternative mobile number(01XXXXXXXXX)" : "অনুগ্রহ করে সঠিক বিকল্প মোবাইল নাম্বার দিন (01XXXXXXXXX)");
            }
        } else {
            employee_family_infoDTO.mobileNumber2 = "";
        }

        Value = request.getParameter("filesDropzone");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        logger.debug("filesDropzone = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            if (!Value.equalsIgnoreCase("")) {
                employee_family_infoDTO.filesDropzone = Long.parseLong(Value);
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

            if (!addFlag) {
                String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    logger.debug("going to delete " + deleteArray[i]);
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        }

        if (addFlag) {
            employeeFamilyInfoDAO.add(employee_family_infoDTO);
        } else {
            employeeFamilyInfoDAO.update(employee_family_infoDTO);
        }
        return employee_family_infoDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Employee_family_infoServlet.class;
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request, 1, "family");
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request, 1, "family");
    }

    @Override
    public String getDeleteRedirectURL(HttpServletRequest request) {
        return getAddOrEditOrDeleteRedirectURL(request, 1, "family");
    }

    @Override
    public boolean getEditPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getViewPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getEditPermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getAddPermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    @Override
    public boolean getAddPagePermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        search(request);
        super.doGet(request, response);
    }
}