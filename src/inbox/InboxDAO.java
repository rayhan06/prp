package inbox;

import approval_module_map.Approval_module_mapDTO;
import approval_module_map.Approval_module_mapMAPS;
import config.GlobalConfigConstants;
import config.GlobalConfigurationRepository;
import dbm.DBMR;
import dbm.DBMW;
import inbox_movements.Inbox_movementsDAO;
import inbox_movements.Inbox_movementsDTO;
import org.apache.log4j.Logger;
import pb.TempTableDTO;
import repository.RepositoryManager;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService3;
import workflow.WorkflowController;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

public class InboxDAO extends NavigationService3 {

    Logger logger = Logger.getLogger(getClass());

    public InboxDAO(String tableName, String tempTableName, Approval_module_mapDTO approval_module_mapDTO) {
        super(tableName, tempTableName, approval_module_mapDTO);
    }

    public InboxDAO() {
        super("inbox", null, null);
    }

    public void addToInbox(long originatorid, UserDTO userDTO, String Message, String details, int type) {
        addToInbox(originatorid, userDTO, Message, details, type, -1, -1);
    }

    public void addToInbox(long originatorid, UserDTO userDTO, String message, String details, int type, long organogramID, int status) {
        // add to inbox  dg(bmd) -- 40035

        long lastModificationTime = System.currentTimeMillis();

        InboxDAO inboxDao = new InboxDAO();

        InboxDTO inboxDTO = new InboxDTO();

        long tempGlobalOrganogramID = Long.parseLong(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.ENABLE_DEFAULT_LOGIN).value);


        if (organogramID == -1) {
            inboxDTO.organogramId = SessionConstants.DEFAULT_INBOX_ORGANOGRAM_ID;
        } else {
            inboxDTO.organogramId = organogramID;
        }


        inboxDTO.userId = userDTO.ID;

        inboxDTO.submissionDate = lastModificationTime;

        inboxDTO.messageType = type;

        if (status == -1) {
            inboxDTO.currentStatus = SessionConstants.DEFAULT_INBOX_STATUS;
        } else {
            inboxDTO.currentStatus = status;
        }

        inboxDTO.subject = message;

        inboxDTO.trackingNumber = "123456";

        inboxDTO.details = details;

        inboxDTO.inboxOriginatorId = originatorid;

        inboxDTO.taskDecision = "";

        inboxDTO.taskComments = "";


        inboxDTO.createdAt = lastModificationTime;

        if (userDTO == null) {
            inboxDTO.createdBy = 0;
        } else {
            inboxDTO.createdBy = (int) userDTO.organogramID;
        }


        long tempInboxID = -1;
        try {
            tempInboxID = inboxDao.add(inboxDTO);
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            logger.error("",e1);
        }


        // add to inbox movements

        Inbox_movementsDAO tempInboxMovementDao = new Inbox_movementsDAO();

        Inbox_movementsDTO inboxMovementDTO = new Inbox_movementsDTO();

        inboxMovementDTO.inboxId = tempInboxID;

        if (userDTO == null) {
            inboxMovementDTO.fromOrganogramId = 0;
        } else {
            inboxMovementDTO.fromOrganogramId = userDTO.organogramID;
        }

        if (organogramID == -1) {
            inboxMovementDTO.toOrganogramId = SessionConstants.DEFAULT_INBOX_ORGANOGRAM_ID;
        } else {
            inboxMovementDTO.toOrganogramId = organogramID;
        }


        inboxMovementDTO.inbox_type = type;

        inboxMovementDTO.fromEmployeeUserId = userDTO.ID;

        inboxMovementDTO.createdAt = lastModificationTime;

        inboxMovementDTO.createdBy = userDTO.ID;

        if (status == -1) {
            inboxMovementDTO.currentStatus = SessionConstants.DEFAULT_INBOX_STATUS;
        } else {
            inboxMovementDTO.currentStatus = status;
        }


        inboxMovementDTO.isSeen = false;

        try {
            tempInboxMovementDao.add(inboxMovementDTO);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error("",e);
        }
    }

    public CommonDTO getInboxTabDTO(ResultSet rs, int inTabID)
    {
    	if(inTabID == InboxTabData.OUTGOIUNG_TICKET_TAB || inTabID == InboxTabData.INCOMING_TICKET_TAB)
        {
    		TicketInboxDTO ticketInboxDTO = new TicketInboxDTO();
    		try {
				ticketInboxDTO.inboxMovementId = rs.getLong("id");
				ticketInboxDTO.isSeen = rs.getBoolean("a.is_seen");
				ticketInboxDTO.dueDate = rs.getLong("support.due_date");
				ticketInboxDTO.inboxOriginatorId = rs.getLong("b.inbox_originator_id");
				
				ticketInboxDTO.ticketIssuesType = rs.getLong("support.ticket_issues_type");
				ticketInboxDTO.ticketIssuesSubtypeType = rs.getLong("support.ticket_issues_subtype_type");
				ticketInboxDTO.arrivalDate = rs.getLong("a.lastModificationTime");				
				ticketInboxDTO.ticketStatusCat = rs.getInt("support.ticket_status_cat");
				ticketInboxDTO.priorityCat = rs.getInt("support.priority_cat");
				ticketInboxDTO.description = rs.getString("support.description");
				ticketInboxDTO.issueText = rs.getString("support.issue_text");
				ticketInboxDTO.issueRaiser = rs.getLong("support.issue_raiser_organogram_id");
				
				ticketInboxDTO.supportTicketId = rs.getLong("support.id");
				
				return ticketInboxDTO;
				
			
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				logger.error("",e);
			}
        }
    	return null;
    }
    
    public String getFromPartOfQuery(int inTabID)
    {
    	String fromPart = "";
    	if(inTabID == InboxTabData.OUTGOIUNG_TICKET_TAB || inTabID == InboxTabData.INCOMING_TICKET_TAB) 
        {
    		fromPart = 
    						"    inbox b\r\n" + 
    	             		"        INNER JOIN\r\n" + 
    	             		"    inbox_movements a ON a.inbox_id = b.id\r\n" +
    	             		"        INNER JOIN\r\n" +
    	             		"    support_ticket support ON support.id = b.inbox_originator_id\r\n" ;
    	             		
        }
    	return fromPart;
    }
    
    public String getQuery(int inTabID, UserDTO userDTO)
    {
    	 String executableQuery = "";
         if(inTabID == InboxTabData.OUTGOIUNG_TICKET_TAB || inTabID == InboxTabData.INCOMING_TICKET_TAB) 
         {
         	executableQuery = "SELECT \r\n" + 
             		"    a.id,\r\n" + 
             		"    support.ticket_issues_type,\r\n" + 
             		"    support.ticket_issues_subtype_type,\r\n" + 
             		"    support.ticket_status_cat,\r\n" + 
             		"    support.priority_cat,\r\n" + 
             		"    support.id,\r\n" + 
             		"    support.description,\r\n" + 
             		"    support.due_date,\r\n" +
             		"    a.lastModificationTime,\r\n" +
             		"    support.issue_text,\r\n" +
             		"    support.issue_raiser_organogram_id,\r\n" +
             		"    b.inbox_originator_id,\r\n" + 
             		"    a.is_seen\r\n" + 
             		"FROM\r\n" + getFromPartOfQuery(inTabID);            		
         	 if(inTabID == InboxTabData.OUTGOIUNG_TICKET_TAB) 
             {
             	executableQuery += " where a.from_organogram_id = " + userDTO.organogramID;
             }
             else if(inTabID == InboxTabData.INCOMING_TICKET_TAB) 
             {
             	executableQuery += " where a.to_organogram_id = " + userDTO.organogramID;
             }
         }
         return executableQuery;
    }
    
    public String getCountQuery(int inTabID, UserDTO userDTO)
    {
    	String executableQuery = "";
        if(inTabID == InboxTabData.OUTGOIUNG_TICKET_TAB || inTabID == InboxTabData.INCOMING_TICKET_TAB) 
        {
        	executableQuery = "SELECT count(*) \r\n" +            		
            		"FROM\r\n" + getFromPartOfQuery(inTabID);            		
        	 if(inTabID == InboxTabData.OUTGOIUNG_TICKET_TAB) 
            {
            	executableQuery += " where a.from_organogram_id = " + userDTO.organogramID;
            }
            else if(inTabID == InboxTabData.INCOMING_TICKET_TAB) 
            {
            	executableQuery += " where a.to_organogram_id = " + userDTO.organogramID;
            }
        }
        return executableQuery;
    }
    
    public int getCount (int inTabID, UserDTO userDTO)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		int count = -1;
		try{
			
			String sql = getCountQuery(inTabID, userDTO);
			
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				count = rs.getInt("count(*)");
			}			
			
			
		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return count;
	}
    
    String getFilter(Hashtable p_searchCriteria, int inTabID)
    {
    	if(p_searchCriteria != null)
		{
    		if(inTabID == InboxTabData.OUTGOIUNG_TICKET_TAB || inTabID == InboxTabData.INCOMING_TICKET_TAB) 
            {
	    		String AnyfieldSql = "";
	    		String AllFieldSql = "";			
				
				Enumeration names = p_searchCriteria.keys();
				String str, value;
				
				AnyfieldSql = "(";
				
				if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
				{
					AnyfieldSql+= "support.search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
				}
				AnyfieldSql += ")";
				System.out.println("AnyfieldSql = " + AnyfieldSql);
				
				AllFieldSql = "(";
				int i = 0;
				while(names.hasMoreElements())
				{				
					str = (String) names.nextElement();
					value = (String)p_searchCriteria.get(str);
			        System.out.println(str + ": " + value);
					if(value != null && !value.equalsIgnoreCase("") && (
							 str.equals("ticket_issues_type")
							|| str.equals("ticket_issues_subtype_type")
							|| str.equals("issue_text")
							|| str.equals("priority_cat")
							|| str.equals("ticket_status_cat")
							|| str.equals("arrival_date_start")
							|| str.equals("arrival_date_end")						
					)
							
					)
					{
						if(p_searchCriteria.get(str).equals("any"))
						{
							continue;
						}
	
						if( i > 0)
						{
							AllFieldSql+= " AND  ";
						}
						
						 if(str.equals("ticket_issues_type"))
						{
							AllFieldSql += "support.ticket_issues_type = " + p_searchCriteria.get(str);
							i ++;
						}
						else if(str.equals("ticket_issues_subtype_type"))
						{
							AllFieldSql += "support.ticket_issues_subtype_type = " + p_searchCriteria.get(str);
							i ++;
						}
						else if(str.equals("issue_text"))
						{
							AllFieldSql += "support.description like '%" + p_searchCriteria.get(str) + "%'";
							i ++;
						}						
						else if(str.equals("ticket_status_cat"))
						{
							AllFieldSql += "support.ticket_status_cat = " + p_searchCriteria.get(str);
							i ++;
						}
						else if(str.equals("priority_cat"))
						{
							AllFieldSql += "support.priority_cat = " + p_searchCriteria.get(str);
							i ++;
						}
						else if(str.equals("arrival_date_start"))
						{
							AllFieldSql += "a.lastModificationTime >= " + p_searchCriteria.get(str);
							i ++;
						}
						else if(str.equals("arrival_date_end"))
						{
							AllFieldSql += "a.lastModificationTime <= " + p_searchCriteria.get(str);
							i ++;
						}
						
						
					}
				}
				
				AllFieldSql += ")";
				System.out.println("AllFieldSql = " + AllFieldSql);
				
				String sql = "";
				if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
				{
					sql += " AND " + AnyfieldSql;
					
				}
				if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
				{			
					sql += " AND " + AllFieldSql;
				}
				
				return sql;
            }
			
			
		}
    	return "";
    }

    public List<InboxTabData> getData(int inOffset, int inLimit, UserDTO userDTO, Hashtable p_searchCriteria) 
    		throws Exception
    {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        
        System.out.println("inOffset = " + inOffset + " inLimit = " + inLimit);

        List<InboxTabData> inboxTabDataList = new ArrayList<InboxTabData>();


        for (int inTabID = 0; inTabID < InboxTabData.TABCOUNT; inTabID++) 
        {
        	InboxTabData inboxTabData = new InboxTabData();
        	inboxTabDataList.add(inboxTabData);

            
        	try
        	{
            String executableQuery = getQuery(inTabID, userDTO);

            executableQuery += getFilter(p_searchCriteria, inTabID);
            
            connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
            executableQuery += "\n ORDER BY a.id desc";
            executableQuery = executableQuery + " LIMIT " + inLimit + " OFFSET " + inOffset;
            System.out.println(executableQuery);
            rs = stmt.executeQuery(executableQuery);



            inboxTabDataList.get(inTabID).tabDTOList = new ArrayList<CommonDTO>();
            while (rs.next()) 
            {
            	inboxTabDataList.get(inTabID).tabDTOList.add(getInboxTabDTO(rs, inTabID));
            }
            System.out.println("############# Fetched row count = " + inboxTabDataList.get(inTabID).tabDTOList.size() + " for tab = " + inTabID) ;

            rs.close();
        	}
        	catch (Exception ex) 
        	{
                logger.error("",ex);
            }
        	finally 
        	{
                try 
                {
                    if (stmt != null) 
                    {
                        stmt.close();
                    }
                }
                catch (Exception e) 
                {
                }

                try 
                {
                    if (connection != null)
                    {
                        DBMR.getInstance().freeConnection(connection);
                    }
                }
                catch (Exception ex2) 
                {
                }
            }
        	
        	inboxTabDataList.get(inTabID).count = getCount(inTabID, userDTO);
            
        }
        
        return inboxTabDataList;
    }


    public long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception {

        InboxDTO inboxDTO = (InboxDTO) commonDTO;

        Connection connection = null;
        PreparedStatement ps = null;

        tableName = "inbox";

        long lastModificationTime = System.currentTimeMillis();

        try {
            connection = DBMW.getInstance().getConnection();

            if (connection == null) {
                System.out.println("nullconn");
            }

            inboxDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

            String sql = "INSERT INTO " + tableName;

            sql += " (";
            sql += "id";
            sql += ", ";
            sql += "submission_date";
            sql += ", ";
            sql += "message_type";
            sql += ", ";
            sql += "current_status";
            sql += ", ";
            sql += "subject";
            sql += ", ";
            sql += "details";
            sql += ", ";
            sql += "tracking_number";
            sql += ", ";
            sql += "inbox_originator_id";
            sql += ", ";
            sql += "user_id";
            sql += ", ";
            sql += "organogram_id";
            sql += ", ";
            sql += "task_decision";
            sql += ", ";
            sql += "task_comments";
            sql += ", ";
            sql += "created_at";
            sql += ", ";
            sql += "created_by";
            sql += ", ";
            sql += "status";
            sql += ", ";
            sql += "isDeleted";
            sql += ", ";
            sql += "lastModificationTime";
            if (tempTableDTO != null) {
                sql += ", permanent_table_id, operation_type, approval_path_type, approval_order";
            }
            sql += ")";


            sql += " VALUES(";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            if (tempTableDTO != null) {
                sql += ", " + tempTableDTO.permanent_table_id + ", " + tempTableDTO.operation_type + ", " + tempTableDTO.approval_path_type + ", " + tempTableDTO.approval_order;
            }
            sql += ")";

            printSql(sql);

            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, inboxDTO.iD);
            ps.setObject(index++, inboxDTO.submissionDate);
            ps.setObject(index++, inboxDTO.messageType);
            ps.setObject(index++, inboxDTO.currentStatus);
            ps.setObject(index++, inboxDTO.subject);
            ps.setObject(index++, inboxDTO.details);
            ps.setObject(index++, Long.toString(inboxDTO.iD));
            ps.setObject(index++, inboxDTO.inboxOriginatorId);
            ps.setObject(index++, inboxDTO.userId);
            ps.setObject(index++, inboxDTO.organogramId);
            ps.setObject(index++, inboxDTO.taskDecision);
            ps.setObject(index++, inboxDTO.taskComments);
            ps.setObject(index++, inboxDTO.createdAt);
            ps.setObject(index++, inboxDTO.createdBy);
            ps.setObject(index++, inboxDTO.status);
            ps.setObject(index++, inboxDTO.isDeleted);
            ps.setObject(index++, lastModificationTime);

            System.out.println(ps);
            ps.execute();


            recordUpdateTime(connection, lastModificationTime, tableName);

        } catch (Exception ex) {
            logger.error("",ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return inboxDTO.iD;
    }

    public Map<String, String> getTabDetailsData(String tabId, String ID) throws Exception {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        InboxDTO inboxDTO = null;
        Map<String, String> tempList = new HashMap<>();
        //List<String> tempList = new ArrayList<String>();
        try {

            long tempId5 = Long.parseLong(tabId.trim());

            String sql = "select query_details from inbox_tab where id = " + tempId5;

            printSql(sql);


            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();

            rs = stmt.executeQuery(sql);

            String tempQuery = "";

            if (rs.next()) {

                tempQuery = rs.getString("query_details");

            }

            rs.close();

            //logger.debug("query :  " + tempQuery );

            long tempRowID = Long.parseLong(ID.trim());

            tempQuery = tempQuery + " where id = " + tempRowID;


            rs = stmt.executeQuery(tempQuery);


            //	int tempTrack = 1;

            int count = rs.getMetaData().getColumnCount();

            if (rs.next()) {

                for (int k = 1; k <= count; k++) {
                    try {

                        tempList.put(rs.getMetaData().getColumnName(k), rs.getObject(k).toString());
                    } catch (Exception ex) {
                        tempList.put("", "");
                    }

                }

                //tempTrack++;

            }

            //logger.debug("query 2 :  " + tempList.size() );


        } catch (Exception ex) {
            logger.error("",ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return tempList;
    }


   
    
    public InboxDTO getDTOByOriginatorAndDetails(long inbox_originator_id, String details) 
    {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        InboxDTO inboxDTO = null;
        try {

            String sql = "SELECT * ";

            sql += " FROM inbox where inbox_originator_id = " + inbox_originator_id;

            sql += " and details = '" + details + "'";

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                inboxDTO = new InboxDTO();

                inboxDTO.iD = rs.getLong("id");
                inboxDTO.submissionDate = rs.getLong("submission_date");
                inboxDTO.messageType = rs.getInt("message_type");
                inboxDTO.currentStatus = rs.getInt("current_status");
                inboxDTO.subject = rs.getString("subject");
                inboxDTO.details = rs.getString("details");
                inboxDTO.trackingNumber = rs.getString("tracking_number");
                inboxDTO.inboxOriginatorId = rs.getLong("inbox_originator_id");
                inboxDTO.userId = rs.getLong("user_id");
                inboxDTO.organogramId = rs.getLong("organogram_id");
                inboxDTO.taskDecision = rs.getString("task_decision");
                inboxDTO.taskComments = rs.getString("task_comments");
                inboxDTO.createdAt = rs.getLong("created_at");
                //inboxDTO.modifiedAt = rs.getLong("modified_at");
                inboxDTO.createdBy = rs.getLong("created_by");
                //inboxDTO.modifiedBy = rs.getInt("modified_by");
                inboxDTO.status = rs.getBoolean("status");
                inboxDTO.isDeleted = rs.getInt("isDeleted");
                inboxDTO.lastModificationTime = rs.getLong("lastModificationTime");

            }


        } catch (Exception ex) {
            logger.error("",ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return inboxDTO;
    }


    //need another getter for repository
    public CommonDTO getDTOByID(long ID, String tableName) throws Exception {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        InboxDTO inboxDTO = null;
        try {

            String sql = "SELECT * ";

            sql += " FROM " + tableName;

            sql += " WHERE id=" + ID;

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                inboxDTO = new InboxDTO();

                inboxDTO.iD = rs.getLong("id");
                inboxDTO.submissionDate = rs.getLong("submission_date");
                inboxDTO.messageType = rs.getInt("message_type");
                inboxDTO.currentStatus = rs.getInt("current_status");
                inboxDTO.subject = rs.getString("subject");
                inboxDTO.details = rs.getString("details");
                inboxDTO.trackingNumber = rs.getString("tracking_number");
                inboxDTO.inboxOriginatorId = rs.getLong("inbox_originator_id");
                inboxDTO.userId = rs.getLong("user_id");
                inboxDTO.organogramId = rs.getLong("organogram_id");
                inboxDTO.taskDecision = rs.getString("task_decision");
                inboxDTO.taskComments = rs.getString("task_comments");
                inboxDTO.createdAt = rs.getLong("created_at");
                //inboxDTO.modifiedAt = rs.getLong("modified_at");
                inboxDTO.createdBy = rs.getLong("created_by");
                //inboxDTO.modifiedBy = rs.getInt("modified_by");
                inboxDTO.status = rs.getBoolean("status");
                inboxDTO.isDeleted = rs.getInt("isDeleted");
                inboxDTO.lastModificationTime = rs.getLong("lastModificationTime");

            }


        } catch (Exception ex) {
            logger.error("",ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return inboxDTO;
    }
    
   
    
    public InboxDTO getDTOBySubjectAndOriginator(String subject, long originatorID)
    {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        InboxDTO inboxDTO = null;
        try {

            String sql = "SELECT * FROM inbox where id = (select max(id) from inbox where subject = '" + subject + "' "
            		+ "and inbox_originator_id = " + originatorID + ")";

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                inboxDTO = new InboxDTO();

                inboxDTO.iD = rs.getLong("id");
                inboxDTO.submissionDate = rs.getLong("submission_date");
                inboxDTO.messageType = rs.getInt("message_type");
                inboxDTO.currentStatus = rs.getInt("current_status");
                inboxDTO.subject = rs.getString("subject");
                inboxDTO.details = rs.getString("details");
                inboxDTO.trackingNumber = rs.getString("tracking_number");
                inboxDTO.inboxOriginatorId = rs.getLong("inbox_originator_id");
                inboxDTO.userId = rs.getLong("user_id");
                inboxDTO.organogramId = rs.getLong("organogram_id");
                inboxDTO.taskDecision = rs.getString("task_decision");
                inboxDTO.taskComments = rs.getString("task_comments");
                inboxDTO.createdAt = rs.getLong("created_at");
                //inboxDTO.modifiedAt = rs.getLong("modified_at");
                inboxDTO.createdBy = rs.getLong("created_by");
                //inboxDTO.modifiedBy = rs.getInt("modified_by");
                inboxDTO.status = rs.getBoolean("status");
                inboxDTO.isDeleted = rs.getInt("isDeleted");
                inboxDTO.lastModificationTime = rs.getLong("lastModificationTime");

            }


        } catch (Exception ex) {
            logger.error("",ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return inboxDTO;
    }


    public long update(CommonDTO commonDTO, String tableName) throws Exception {
        InboxDTO inboxDTO = (InboxDTO) commonDTO;

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            connection = DBMW.getInstance().getConnection();

            String sql = "UPDATE " + tableName;

            sql += " SET ";
            sql += "submission_date=?";
            sql += ", ";
            sql += "message_type=?";
            sql += ", ";
            sql += "current_status=?";
            sql += ", ";
            sql += "subject=?";
            sql += ", ";
            sql += "details=?";
            sql += ", ";
            sql += "tracking_number=?";
            sql += ", ";
            sql += "inbox_originator_id=?";
            sql += ", ";
            sql += "user_id=?";
            sql += ", ";
            sql += "organogram_id=?";
            sql += ", ";
            sql += "task_decision=?";
            sql += ", ";
            sql += "task_comments=?";
            sql += ", ";
            sql += "created_at=?";
            sql += ", ";
            sql += "created_by=?";
            sql += ", ";
            sql += "status=?";
            sql += ", lastModificationTime = " + lastModificationTime + "";
            sql += " WHERE id = " + inboxDTO.iD;

            printSql(sql);

            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, inboxDTO.submissionDate);
            ps.setObject(index++, inboxDTO.messageType);
            ps.setObject(index++, inboxDTO.currentStatus);
            ps.setObject(index++, inboxDTO.subject);
            ps.setObject(index++, inboxDTO.details);
            ps.setObject(index++, inboxDTO.trackingNumber);
            ps.setObject(index++, inboxDTO.inboxOriginatorId);
            ps.setObject(index++, inboxDTO.userId);
            ps.setObject(index++, inboxDTO.organogramId);
            ps.setObject(index++, inboxDTO.taskDecision);
            ps.setObject(index++, inboxDTO.taskComments);
            ps.setObject(index++, inboxDTO.createdAt);
            ps.setObject(index++, inboxDTO.createdBy);
            ps.setObject(index++, inboxDTO.status);
            System.out.println(ps);
            ps.executeUpdate();


            recordUpdateTime(connection,  lastModificationTime, tableName);


        } catch (Exception ex) {
            logger.error("",ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return inboxDTO.iD;
    }

    public List<InboxDTO> getDTOs(Collection recordIDs) {
        return getDTOs(recordIDs, tableName);
    }

    public List<InboxDTO> getDTOs(Collection recordIDs, String tableName) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        InboxDTO inboxDTO = null;
        List<InboxDTO> inboxDTOList = new ArrayList<>();
        if (recordIDs.isEmpty()) {
            return inboxDTOList;
        }
        try {

            String sql = "SELECT * ";

            sql += " FROM " + tableName;

            sql += " WHERE id IN ( ";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql += ",";
                }
                sql += ((ArrayList) recordIDs).get(i);
            }
            sql += ")  order by lastModificationTime desc";

            printSql(sql);

            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                inboxDTO = new InboxDTO();
                inboxDTO.iD = rs.getLong("id");
                inboxDTO.submissionDate = rs.getLong("submission_date");
                inboxDTO.messageType = rs.getInt("message_type");
                inboxDTO.currentStatus = rs.getInt("current_status");
                inboxDTO.subject = rs.getString("subject");
                inboxDTO.details = rs.getString("details");
                inboxDTO.trackingNumber = rs.getString("tracking_number");
                inboxDTO.inboxOriginatorId = rs.getLong("inbox_originator_id");
                inboxDTO.userId = rs.getLong("user_id");
                inboxDTO.organogramId = rs.getLong("organogram_id");
                inboxDTO.taskDecision = rs.getString("task_decision");
                inboxDTO.taskComments = rs.getString("task_comments");
                inboxDTO.createdAt = rs.getLong("created_at");

                inboxDTO.createdBy = rs.getLong("created_by");

                inboxDTO.status = rs.getBoolean("status");
                inboxDTO.isDeleted = rs.getInt("isDeleted");
                inboxDTO.lastModificationTime = rs.getLong("lastModificationTime");
                System.out.println("got this DTO: " + inboxDTO);

                inboxDTOList.add(inboxDTO);

            }

        } catch (Exception ex) {
            logger.error("",ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return inboxDTOList;

    }


    //add repository
    public List<InboxDTO> getAllInbox(boolean isFirstReload) {
        List<InboxDTO> inboxDTOList = new ArrayList<>();

        String sql = "SELECT * FROM inbox";
        sql += " WHERE ";


        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        sql += " order by inbox.lastModificationTime desc";
        printSql(sql);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;


        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);


            while (rs.next()) {
                InboxDTO inboxDTO = new InboxDTO();
                inboxDTO.iD = rs.getLong("id");
                inboxDTO.submissionDate = rs.getLong("submission_date");
                inboxDTO.messageType = rs.getInt("message_type");
                inboxDTO.currentStatus = rs.getInt("current_status");
                inboxDTO.subject = rs.getString("subject");
                inboxDTO.details = rs.getString("details");
                inboxDTO.trackingNumber = rs.getString("tracking_number");
                inboxDTO.inboxOriginatorId = rs.getLong("inbox_originator_id");
                inboxDTO.userId = rs.getLong("user_id");
                inboxDTO.organogramId = rs.getLong("organogram_id");
                inboxDTO.taskDecision = rs.getString("task_decision");
                inboxDTO.taskComments = rs.getString("task_comments");
                inboxDTO.createdAt = rs.getLong("created_at");

                inboxDTO.createdBy = rs.getLong("created_by");

                inboxDTO.status = rs.getBoolean("status");
                inboxDTO.isDeleted = rs.getInt("isDeleted");
                inboxDTO.lastModificationTime = rs.getLong("lastModificationTime");

                inboxDTOList.add(inboxDTO);
            }
        } catch (Exception ex) {
            logger.error("",ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return inboxDTOList;
    }

    public List<InboxDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset) {
        return getDTOs(p_searchCriteria, limit, offset, tableName, true, 0);
    }

    public List<InboxDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        List<InboxDTO> inboxDTOList = new ArrayList<>();

        try {

            String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, tableName, isPermanentTable, userApprovalPathType);

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                InboxDTO inboxDTO = new InboxDTO();
                inboxDTO.iD = rs.getLong("id");
                inboxDTO.submissionDate = rs.getLong("submission_date");
                inboxDTO.messageType = rs.getInt("message_type");
                inboxDTO.currentStatus = rs.getInt("current_status");
                inboxDTO.subject = rs.getString("subject");
                inboxDTO.details = rs.getString("details");
                inboxDTO.trackingNumber = rs.getString("tracking_number");
                inboxDTO.inboxOriginatorId = rs.getLong("inbox_originator_id");
                inboxDTO.userId = rs.getLong("user_id");
                inboxDTO.organogramId = rs.getLong("organogram_id");
                inboxDTO.taskDecision = rs.getString("task_decision");
                inboxDTO.taskComments = rs.getString("task_comments");
                inboxDTO.createdAt = rs.getLong("created_at");

                inboxDTO.createdBy = rs.getLong("created_by");

                inboxDTO.status = rs.getBoolean("status");
                inboxDTO.isDeleted = rs.getInt("isDeleted");
                inboxDTO.lastModificationTime = rs.getLong("lastModificationTime");

                inboxDTOList.add(inboxDTO);
            }

        } catch (Exception ex) {
            logger.error("",ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return inboxDTOList;

    }


    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType) {
        InboxMAPS maps = new InboxMAPS(tableName);
        String joinSQL = "";
        joinSQL += " join message on " + tableName + ".message_type = message.ID ";
        return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, tableName, isPermanentTable, userApprovalPathType, maps, joinSQL);
    }


    public void updateInboxStatus(Long ID, Integer status) {

        Connection connection = null;
        Statement stmt = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();

        try {
            String sql = "UPDATE " + tableName;

            sql += " SET status= " + status + ",lastModificationTime=" + lastModificationTime + " WHERE ID = " + ID;

            printSql(sql);

            connection = DBMW.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.execute(sql);

            recordUpdateTime(connection, lastModificationTime, tableName);


        } catch (Exception ex) {
            logger.error("",ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
    }

    public Integer updateInboxOriginatorId(Long oldId, Long newId) {


        if (oldId == null || newId == null)
            return -1;

        Integer rowUpdated = 0;
        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();

        try {
            connection = DBMW.getInstance().getConnection();

            if (connection == null) {
                System.out.println("nullconn");
            }

            String sql = "UPDATE inbox set inbox_originator_id = ? where inbox_originator_id = ? ";

            ps = connection.prepareStatement(sql);

            int index = 1;

            ps.setLong(index++, newId);
            ps.setLong(index++, oldId);

            System.out.println(ps);
            rowUpdated = ps.executeUpdate();

        } catch (Exception ex) {

            logger.error("",ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return rowUpdated;
    }

    public String getSearchCriteriaSql(String key, int tabIndex){
        String anyfieldSql = "(";
        int i = 0;
        Iterator it = InboxMAPS.GetInstance().inbox_anyfield_search_map.get(tabIndex).entrySet().iterator();
        while(it.hasNext())
        {
            if( i > 0)
            {
                anyfieldSql+= " OR  ";
            }
            Map.Entry pair = (Map.Entry)it.next();
            anyfieldSql+= pair.getKey() + " like '%" + key + "%'";
            i ++;
        }
        anyfieldSql += ")";

        return anyfieldSql;
    }
}
