package inbox;

import java.io.IOException;
import java.io.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import project_tracker.ProjectTrackerFilesDAO;
import role.PermissionRepository;


import sessionmanager.SessionConstants;
import ticket_forward_history.Ticket_forward_historyDAO;
import ticket_forward_history.Ticket_forward_historyDTO;
import treeView.TreeDTO;
import user.UserDTO;
import user.UserRepository;

import util.RecordNavigationManager3;

import java.util.*;
import javax.servlet.http.*;

import inbox.Constants;
import approval_module_map.*;
import config.GlobalConfigConstants;
import config.GlobalConfigurationRepository;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

/**
 * Servlet implementation class InboxServlet
 */
@WebServlet("/InboxServlet")
@MultipartConfig
public class InboxServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(InboxServlet.class);
	Approval_module_mapDAO approval_module_mapDAO = new Approval_module_mapDAO();
    Approval_module_mapDTO approval_module_mapDTO;
    String tableName = "inbox";
    String tempTableName = "inbox_temp";
	InboxDAO inboxDAO;
	Ticket_forward_historyDAO ticket_forward_historyDAO = new Ticket_forward_historyDAO();
    private final Gson gson = new Gson();
    
    public String actionType = "";
	public String servletType = "GenericTreeServlet";
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InboxServlet() 
	{
        super();
    	try
    	{
			approval_module_mapDTO = approval_module_mapDAO.getApproval_module_mapDTOByTableName("inbox");
			//inboxDAO = new InboxDAO(tableName, tempTableName, approval_module_mapDTO);
			
			inboxDAO = new InboxDAO("inbox",  "", null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		
	
		boolean isPermanentTable = true;
		
		
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_ADD))
//				{
//					getAddPage(request, response);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
			}
			else if(actionType.equals("getEditPage"))
			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_UPDATE))
//				{
//					if(isPermanentTable)
//					{
//						getInbox(request, response, tableName);
//					}
//					else
//					{
//						getInbox(request, response, tempTableName);
//					}
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}
			else if(actionType.equals("search"))
			{
				
				request.setAttribute("inboxDAO",inboxDAO);
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_MAIN))
				{
					getInboxMainPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("getApprovalPage"))
			{
//				System.out.println("getApprovalPage requested");
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_SEARCH))
//				{
//					searchInbox(request, response, tempTableName, false);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}				
			}
			else if(actionType.equals("getInboxMain"))
			{
				makeTicketForwardHistoryRead(userDTO.ID);
				request.setAttribute("inboxDAO",inboxDAO);
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_MAIN))
				{
					getInboxMainPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getInboxMainDetails"))
			{
				request.setAttribute("inboxDAO",inboxDAO);
				
				//tab_id=1&row_id=2
				
				String tempTabId = request.getParameter("tab_id");
				
				String tempRowId = request.getParameter("row_id");
				
				request.setAttribute("tab_id",tempTabId);
				
				request.setAttribute("row_id",tempRowId);
				
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_MAIN_DETAILS))
				{
					getInboxMainDetailsPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getInboxMainInternal"))
			{
				request.setAttribute("inboxDAO",inboxDAO);
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_MAIN_INTERNAL))
				{
					getInboxMainInternalPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getInboxMainExternal"))
			{
				request.setAttribute("inboxDAO",inboxDAO);
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_MAIN_EXTERNAL))
				{
					getInboxMainExternalPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if (actionType.equals("assignPathToOrganogram")) {
				
				request.setAttribute("inboxDAO",inboxDAO);
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_MAIN_DETAILS))
				{
					getInboxMainDetailsPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug("a : "+actionType + " : " + ex);
		}
	}
	
	private void makeTicketForwardHistoryRead(long currentUserId) throws Exception {
		List<Ticket_forward_historyDTO> dtos = ticket_forward_historyDAO.getDtosByForwardToUserId(currentUserId);
		for(Ticket_forward_historyDTO dto: dtos) {
			dto.isRead = 1;
			ticket_forward_historyDAO.update(dto);
		}
		
	}
	private void getInboxMainInternalPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		
		//else if (actionType.equals("assignPathToOrganogram")) {
		
		TreeDTO treeDTO = new TreeDTO();
			treeDTO.parentChildMap = new String[]{"office_ministries", "office_layers", "offices", "office_units", "office_unit_organograms"};
			treeDTO.showNameMap = new String[]{"Office Ministries", "Office Layers", "Offices", "Office Units", "Office Unit Organograms"};
			treeDTO.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_id", "office_unit_id"};
			treeDTO.englishNameColumn = new String[]{"name_eng", "layer_name_eng", "office_name_eng", "unit_name_eng", "designation_eng"};
			treeDTO.banglaNameColumn = new String[]{"name_bng", "layer_name_bng", "office_name_bng", "unit_name_bng", "designation_bng"};
			treeDTO.pageTitle = "Assign Approval Path";
			treeDTO.treeName = "approvalPathTree";
			treeDTO.treeType = "tree";
			//treeDTO.treeBody = "../inbox/inboxMainInternalBody.jsp";
			treeDTO.treeBody = "OrganogramPathTreeBody.jsp";
			treeDTO.sameTableParentColumnName = new String[]{"", "", "", "parent_unit_id", ""}; //only for recursive tables
			treeDTO.checkBoxType = 1;
			treeDTO.plusButtonType = 0;
			
			actionType ="assignPathToOrganogram";
			//}
			
			String myLayer = request.getParameter("myLayer");
			if(myLayer != null && !myLayer.equalsIgnoreCase(""))
			{
				int layer = Integer.parseInt(request.getParameter("myLayer"));
				if(treeDTO.treeType.equalsIgnoreCase("tree") && 
						layer < treeDTO.parentChildMap.length && 
						treeDTO.sameTableParentColumnName != null &&							
						(!treeDTO.sameTableParentColumnName[layer].equals("") || !treeDTO.sameTableParentColumnName[layer].equals("")))
				{
					getRecursiveNodes(request, response, layer, treeDTO);
				}
				else
				{
					getNormalNodes(request, response, layer, treeDTO);
				}
			}
			else
			{					
				getTopNodes(request, response, treeDTO);	
			}
			
		//request.setAttribute("ID", -1L);
		//RequestDispatcher requestDispatcher = request.getRequestDispatcher("inbox/inboxMainInternal.jsp");
		//requestDispatcher.forward(request, response);
	}
	
	
	private void getInboxMainExternalPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		request.setAttribute("ID", -1L);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("inbox/inboxMainExternal.jsp");
		requestDispatcher.forward(request, response);
	}
	
	private void setAttributes(int parentID, int layer, List<Integer> IDs, HttpServletRequest request, TreeDTO treeDTO)
	{

		request.setAttribute("nodeIDs", IDs);
		request.setAttribute("myLayer", layer);
		String request_layer_id_pairs = request.getParameter("layer_id_pairs");
		if(request_layer_id_pairs == null)
		{
			request_layer_id_pairs = "";
		}
		System.out.println("layer_id_pairs = " + request_layer_id_pairs);
		treeDTO.layer_id_pairs = request_layer_id_pairs;
		request.setAttribute("treeDTO", treeDTO);
		
	}
	
	private void setParametersAndForward(int parentID, String nodeJSP, HttpServletRequest request, HttpServletResponse response, TreeDTO treeDTO)
	{

		try 
		{
			String parentElementID = request.getParameter("parentElementID");
			String checkBoxChecked = request.getParameter("checkBoxChecked");
			request.getRequestDispatcher("treeView/" + nodeJSP 
					+ "?actionType=" + actionType 
					+ "&treeType=" + treeDTO.treeType 
					+ "&treeBody=" + treeDTO.treeBody 
					+ "&servletType=" + servletType
					+ "&pageTitle=" + treeDTO.pageTitle
					+ "&parentID=" + parentID
					+ "&checkBoxChecked=" + checkBoxChecked
					+ "&parentElementID=" + parentElementID
					+ treeDTO.additionalParams
					).forward(request, response);
		}
		catch (ServletException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	private String getNodeJSP(TreeDTO treeDTO)
	{
		if(treeDTO.treeType.equalsIgnoreCase("tree"))
		{
			return "treeNode.jsp";
		}
		else if(treeDTO.treeType.equalsIgnoreCase("select"))
		{
			return "dropDownNode.jsp";
			
		}
		else if(treeDTO.treeType.equalsIgnoreCase("flat"))
		{
			return "expandedTreeNode.jsp";
			
		}
		return "";
	}
	
	private void getTopNodes(HttpServletRequest request, HttpServletResponse response, TreeDTO treeDTO) throws ServletException, IOException 
	{
		System.out.println("METHOD: getTopNodes");
		try
		{
			List<Integer> IDs = GenericTree.getTopIDs(treeDTO.parentChildMap[0]);
			setAttributes(-1, 0, IDs, request, treeDTO);
			String nodeJsp = "tree.jsp";		
			setParametersAndForward(0, nodeJsp, request, response, treeDTO);			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	private void getRecursiveNodes(HttpServletRequest request, HttpServletResponse response, int layer, TreeDTO treeDTO)
    {
		System.out.println("METHOD: getRecursiveNodes");
        int id = Integer.parseInt(request.getParameter("id"));
        int parentTableID;
       
        try
        {
            parentTableID = Integer.parseInt(request.getParameter("parentID"));

            int iPrevLayer = -1;
            String prevLayer = request.getParameter("prevLayer");
            if (prevLayer != null && !prevLayer.equalsIgnoreCase(""))
            {
                iPrevLayer = Integer.parseInt(prevLayer);
            }
            System.out.println("layer = " + layer + " id = " + id + " treeDTO.parentChildMap.length = " + treeDTO.parentChildMap.length + " iPrevLayer = " + iPrevLayer);
            if (layer != 0 &&
                    (treeDTO.sameTableParentColumnName != null &&
                            (!treeDTO.sameTableParentColumnName[layer].equals("") || !treeDTO.sameTableParentColumnName[layer].equals("")))
                    && layer != iPrevLayer)  //If a recursive node suddenly starts at midpoint
            {
                parentTableID = id;
                id = 0;
                System.out.println("AAAAAAAAAAAAAAA after reshuffling layer = " + layer + " id = " + id + " parentTableID = " + parentTableID);
            }

            if (layer < treeDTO.parentChildMap.length)
            {
                List<Integer> UnitIDs = GenericTree.getRecurviveIDs(treeDTO.parentChildMap[layer], treeDTO.sameTableParentColumnName[layer], id, treeDTO.parentIDColumn[layer], parentTableID);

                List<Integer> IDs = new ArrayList<Integer>();
                List<Integer> recursiveNodeTypes = new ArrayList<Integer>();

                setAttributes(id, layer, IDs, request, treeDTO);

                if (id != 0 && layer + 1 < treeDTO.parentChildMap.length)
                {
                    List<Integer> ChildIDs = GenericTree.getChildIDs(treeDTO.parentChildMap[layer + 1], treeDTO.parentIDColumn[layer + 1], id);
                    if(ChildIDs == null)
                    {
                        ChildIDs = new ArrayList<>();

                    }
                    ChildIDs.add(-1);

                    if (ChildIDs != null)
                    {
                        System.out.println("ooooooooooooooooooo ChildIDs = " + ChildIDs.size());
                        for (int i = 0; i < ChildIDs.size(); i++) {
                            IDs.add(ChildIDs.get(i));
                            recursiveNodeTypes.add(2);
                        }
                    }
                }

                for (int i = 0; i < UnitIDs.size(); i++)
                {
                    IDs.add(UnitIDs.get(i));
                    recursiveNodeTypes.add(1);
                }
                request.setAttribute("isRecursive", 1);
                request.setAttribute("recursiveNodeTypes", recursiveNodeTypes);
                request.setAttribute("childName", treeDTO.parentChildMap[layer + 1]);
                request.setAttribute("childEnglishNameColumn", treeDTO.englishNameColumn[layer + 1]);
                request.setAttribute("childBanglaNameColumn", treeDTO.banglaNameColumn[layer + 1]);

				String nodeJsp = "treeNode.jsp";
                System.out.println("SSSSSSSSSSSS Setting parentTableID = " + parentTableID);
                setParametersAndForward(parentTableID, nodeJsp, request, response, treeDTO);

            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
	
	private void getNormalNodes(HttpServletRequest request, HttpServletResponse response, int layer, TreeDTO treeDTO) throws ServletException, IOException 
	{
		
		System.out.println("METHOD: getNormalNodes");
		try
		{
			int id = Integer.parseInt(request.getParameter("id"));
			System.out.println("NNNNNNNNNNNNNN getNormalNodes layer = " + layer + " id = " + id);
			if(layer < treeDTO.parentChildMap.length)
			{				
				List<Integer> IDs = GenericTree.getChildIDs(treeDTO.parentChildMap[layer], treeDTO.parentIDColumn[layer], id);
				setAttributes(id, layer, IDs, request, treeDTO);
				String nodeJsp = getNodeJSP(treeDTO);
				
				treeDTO.treeType = "tree";
				System.out.println("Going to geoNode, treeDTO.treeType = " + treeDTO.treeType);				
				setParametersAndForward(id, nodeJsp, request, response, treeDTO);				
			
			}
			else
			{
				System.out.println("Maximum Layer Reached");
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	
	
	
	private void getInboxMainDetailsPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		request.setAttribute("ID", -1L);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("inbox/inboxMainDetails.jsp");
		requestDispatcher.forward(request, response);
	}
	
	private void getInboxMainPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		request.setAttribute("ID", -1L);
		
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		
		boolean isPermanent = true;
		
		RecordNavigationInbox rnManager = new RecordNavigationInbox(
	        		SessionConstants.NAV_INBOX,
				request,
				inboxDAO,
				SessionConstants.VIEW_INBOX,
				SessionConstants.SEARCH_INBOX,
				tableName,
				isPermanent,
				userDTO.approvalPathID);
	        try
	        {
				System.out.println("trying to dojob");
	            rnManager.doJob(loginDTO, request);
	        }
	        catch(Exception e)
	        {
				System.out.println("failed to dojob" + e);
	        }
	        
	        String tempAjaxFlagStr =   request.getParameter("ajax");//Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.INBOX_DEFAULT_AJAX).value);//SessionConstants.DEFAULT_INBOX_ORGANOGRAM_ID;

	        int tempAjaxFlag = 0;
	        if(tempAjaxFlagStr != null)
	        {
	        	tempAjaxFlag = Integer.parseInt(tempAjaxFlagStr);
	        }
	       // int tempAjaxFlag = Integer.parseInt(tempAjaxFlagStr);
	        
	        if(tempAjaxFlag == 1)
	        {
	        	RequestDispatcher requestDispatcher = request.getRequestDispatcher("inbox/inboxMainForm.jsp");
	    		requestDispatcher.forward(request, response);
	        	
	        }
	        else
	        {
	        	RequestDispatcher requestDispatcher = request.getRequestDispatcher("inbox/inboxMain.jsp");
	    		requestDispatcher.forward(request, response);
	        	
	        }
	        
		
	}
	
//	private void getInboxMainPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
//	{
//		request.setAttribute("ID", -1L);
//		RequestDispatcher requestDispatcher = request.getRequestDispatcher("inbox/inboxMain.jsp");
//		requestDispatcher.forward(request, response);
//	}

	private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		request.setAttribute("ID", -1L);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("inbox/inboxEdit.jsp");
		requestDispatcher.forward(request, response);
	}
	private String getFileName(final Part part) 
	{
	    final String partHeader = part.getHeader("content-disposition");
	    System.out.println("Part Header = {0}" +  partHeader);
	    for (String content : part.getHeader("content-disposition").split(";")) {
	        if (content.trim().startsWith("filename")) {
	            return content.substring(
	                    content.indexOf('=') + 1).trim().replace("\"", "");
	        }
	    }
	    return null;
	}

	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		
	
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			
			
			if(actionType.equals("getInboxMain"))
			{
				request.setAttribute("inboxDAO",inboxDAO);
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_MAIN))
				{
					getInboxMainPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			if(actionType.equals("add"))
			{
				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_ADD))
//				{
//					System.out.println("going to  addInbox ");
//					addInbox(request, response, true, userDTO, tableName, true);
//				}
//				else
//				{
//					System.out.println("Not going to  addInbox ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
				
			}
			if(actionType.equals("approve"))
			{
				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_ADD))
//				{					
//					approveInbox(request, response, true, userDTO);
//				}
//				else
//				{
//					System.out.println("Not going to  addInbox ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
				
			}
			if(actionType.equals("getDTO"))
			{
				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_ADD))
//				{
//					if(isPermanentTable)
//					{
//						getDTO(request, response, tableName);
//					}
//					else
//					{
//						getDTO(request, response, tempTableName);
//					}
//				}
//				else
//				{
//					System.out.println("Not going to  addInbox ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
				
			}
			else if(actionType.equals("edit"))
			{
				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_UPDATE))
//				{
//					if(isPermanentTable)
//					{
//						addInbox(request, response, false, userDTO, tableName, isPermanentTable);
//					}
//					else
//					{
//						addInbox(request, response, false, userDTO, tempTableName, isPermanentTable);
//					}
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
			}
			else if(actionType.equals("delete"))
			{								
				deleteInbox(request, response, userDTO, isPermanentTable);				
			}	
			else if(actionType.equals("search"))
			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_SEARCH))
//				{
//					searchInbox(request, response, tableName, true);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
			
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response, String tableName) 
	{
		try 
		{
			System.out.println("In getDTO");
			InboxDTO inboxDTO = (InboxDTO)inboxDAO.getDTOByID(Long.parseLong(request.getParameter("ID")), tableName);
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(inboxDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	private void approveInbox(HttpServletRequest request, HttpServletResponse response, boolean b, UserDTO userDTO) 
	{
		try
		{
			long id = Long.parseLong(request.getParameter("idToApprove"));
			InboxDTO inboxDTO = (InboxDTO)inboxDAO.getDTOByID(id, tempTableName);
			inboxDAO.manageWriteOperations(inboxDTO, SessionConstants.APPROVE, id, userDTO);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
	}
	private void addInbox(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, String tableName, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addInbox");
			String path = getServletContext().getRealPath("/img2/");
			InboxDTO inboxDTO;
			String FileNamePrefix;
			if(addFlag == true)
			{
				inboxDTO = new InboxDTO();
				FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
			}
			else
			{
				inboxDTO = (InboxDTO)inboxDAO.getDTOByID(Long.parseLong(request.getParameter("identity")), tableName);
				FileNamePrefix = request.getParameter("identity");
			}
			
			String Value = "";
			Value = request.getParameter("submissionDate");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("submissionDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				inboxDTO.submissionDate = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("messageType");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("messageType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				inboxDTO.messageType = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("currentStatus");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("currentStatus = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				inboxDTO.currentStatus = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("subject");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("subject = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				inboxDTO.subject = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("details");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("details = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				inboxDTO.details = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("trackingNumber");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("trackingNumber = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				inboxDTO.trackingNumber = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("inboxOriginatorId");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("inboxOriginatorId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				inboxDTO.inboxOriginatorId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("userId");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("userId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				inboxDTO.userId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("organogramId");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("organogramId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				inboxDTO.organogramId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("taskDecision");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("taskDecision = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				inboxDTO.taskDecision = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("taskComments");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("taskComments = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				inboxDTO.taskComments = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("createdAt");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("createdAt = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				inboxDTO.createdAt = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("modifiedAt");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
//			System.out.println("modifiedAt = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inboxDTO.modifiedAt = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
			Value = request.getParameter("createdBy");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("createdBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				inboxDTO.createdBy = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("modifiedBy");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inboxDTO.modifiedBy = Integer.parseInt(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
			Value = request.getParameter("status");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("status = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				inboxDTO.status = Boolean.parseBoolean(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addInbox dto = " + inboxDTO);
			
			if(addFlag == true)
			{
				inboxDAO.manageWriteOperations(inboxDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				if(isPermanentTable)
				{
					inboxDAO.manageWriteOperations(inboxDTO, SessionConstants.UPDATE, -1, userDTO);
				}
				else
				{
					inboxDAO.manageWriteOperations(inboxDTO, SessionConstants.VALIDATE, -1, userDTO);
				}
				
			}
			
			
			
			
			
			
			
			
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			
			if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				getInbox(request, response, tableName);
			}
			else
			{
				response.sendRedirect("InboxServlet?actionType=search");
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void deleteInbox(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, boolean deleteOrReject) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				if(deleteOrReject)
				{
					InboxDTO inboxDTO = (InboxDTO)inboxDAO.getDTOByID(id);
					inboxDAO.manageWriteOperations(inboxDTO, SessionConstants.DELETE, id, userDTO);
					response.sendRedirect("InboxServlet?actionType=search");
				}
				else
				{
					InboxDTO inboxDTO = (InboxDTO)inboxDAO.getDTOByID(id, tempTableName);
					inboxDAO.manageWriteOperations(inboxDTO, SessionConstants.REJECT, id, userDTO);
					response.sendRedirect("InboxServlet?actionType=getApprovalPage");
				}
			}			
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		
	}

	private void getInbox(HttpServletRequest request, HttpServletResponse response, String tableName) throws ServletException, IOException
	{
		System.out.println("in getInbox");
		InboxDTO inboxDTO = null;
		try 
		{
			inboxDTO = (InboxDTO)inboxDAO.getDTOByID(Long.parseLong(request.getParameter("ID")), tableName);
			request.setAttribute("ID", inboxDTO.iD);
			request.setAttribute("inboxDTO",inboxDTO);
			request.setAttribute("inboxDAO",inboxDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "inbox/inboxInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "inbox/inboxSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "inbox/inboxEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "inbox/inboxEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	private void searchInbox(HttpServletRequest request, HttpServletResponse response, String tableName, boolean isPermanent) throws ServletException, IOException
	{
		System.out.println("in  searchInbox 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		
	
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager3 rnManager = new RecordNavigationManager3(
			null,
			request,
			inboxDAO,
			null,
			null,
			tableName,
			isPermanent,
			userDTO.approvalPathID);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("inboxDAO",inboxDAO);
        RequestDispatcher rd;
        if(hasAjax == false)
        {
        	System.out.println("Going to inbox/inboxSearch.jsp");
        	rd = request.getRequestDispatcher("inbox/inboxSearch.jsp");
        }
        else
        {
        	System.out.println("Going to inbox/inboxSearchForm.jsp");
        	rd = request.getRequestDispatcher("inbox/inboxSearchForm.jsp");
        }
		rd.forward(request, response);
	}
	
}

