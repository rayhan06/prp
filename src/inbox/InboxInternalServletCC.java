package inbox;

import java.io.IOException;
import java.util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import centre.Constants;
import inbox.InboxServlet;
import inbox_tab.Inbox_tabDAO;
import inbox_tab.Inbox_tabDTO;
import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import treeView.TreeDTO;
import treeView.TreeFactory;
import user.UserDTO;
import user.UserRepository;

import pb.*;

/**
 * Servlet implementation class TreeServlet
 */
@WebServlet("/InboxInternalServletCC")
public class InboxInternalServletCC extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	 public static Logger logger = Logger.getLogger(InboxInternalServlet.class);
	
	public String actionType = "";
	public String servletType = "InboxInternalServletCC";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InboxInternalServletCC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		
	
		try
		{
			actionType = request.getParameter("actionType");
			TreeDTO treeDTO = new TreeDTO();
			
			TreeDTO treeDTO2 = new TreeDTO();
			//System.out.println("############################# In doget2 actionType = " + actionType);
			if(actionType.equals("getOffice"))
			{
				
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.MINISTRIES_TO_OFFICES_INCLUDING_ORIGINS);
				treeDTO.pageTitle = "Office Management";
				treeDTO.treeName = "OfficeDropDownChain";
				treeDTO.treeType = "select";
				treeDTO.treeBody = "officeOriginTreeBody.jsp";
				treeDTO.checkBoxType = 0;
				treeDTO.plusButtonType = 0;
			}
			else if (actionType.equals("assignPathToOrganogram")) {
				treeDTO.parentChildMap = new String[]{"office_ministries", "office_layers", "offices", "office_units", "office_unit_organograms"};
				treeDTO.showNameMap = new String[]{"Office Ministries", "Office Layers", "Offices", "Office Units", "Office Unit Organograms"};
				treeDTO.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_id", "office_unit_id"};
				treeDTO.englishNameColumn = new String[]{"name_eng", "layer_name_eng", "office_name_eng", "unit_name_eng", "designation_eng"};
				treeDTO.banglaNameColumn = new String[]{"name_bng", "layer_name_bng", "office_name_bng", "unit_name_bng", "designation_bng"};
				treeDTO.pageTitle = "Assign Approval Path";
				treeDTO.treeName = "approvalPathTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "OrganogramPathTreeBody.jsp";
				treeDTO.sameTableParentColumnName = new String[]{"", "", "", "parent_unit_id", ""}; //only for recursive tables
				treeDTO.checkBoxType = 1;
				treeDTO.plusButtonType = 0;
				
				
				treeDTO2.parentChildMap = new String[]{"office_ministries", "office_layers", "offices", "office_units", "office_unit_organograms"};
				treeDTO2.showNameMap = new String[]{"Office Ministries", "Office Layers", "Offices", "Office Units", "Office Unit Organograms"};
				treeDTO2.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_id", "office_unit_id"};
				treeDTO2.englishNameColumn = new String[]{"name_eng", "layer_name_eng", "office_name_eng", "unit_name_eng", "designation_eng"};
				treeDTO2.banglaNameColumn = new String[]{"name_bng", "layer_name_bng", "office_name_bng", "unit_name_bng", "designation_bng"};
				treeDTO2.pageTitle = "Assign Approval Path";
				treeDTO2.treeName = "approvalPathTree";
				treeDTO2.treeType = "tree";
				treeDTO2.treeBody = "OrganogramPathTreeBody.jsp";
				treeDTO2.sameTableParentColumnName = new String[]{"", "", "", "parent_unit_id", ""}; //only for recursive tables
				treeDTO2.checkBoxType = 1;
				treeDTO2.plusButtonType = 0;
				
				
				String tabId = request.getParameter("tab_id");
				
				String rowId = request.getParameter("row_id");
				
				long tebIdLong = 0;
				
				long rowIdLong  =0;
				
				if(tabId != null && rowId != null)
				{
				
				 tebIdLong = Long.parseLong(tabId);
				
				 rowIdLong = Long.parseLong(rowId);
				 
				}
				

				
				if(rowIdLong != 0 && tebIdLong != 0)
				{
				
					Inbox_tabDAO tempDao = new Inbox_tabDAO();
					
					Inbox_tabDTO tempDto = (Inbox_tabDTO) tempDao.getDTOByID(tebIdLong, "inbox_tab");
					
//					if(tempDto.nameEn.trim().equals("inbox"))
//					{
//						
//					}
//					else if(tempDto.nameEn.trim().equalsIgnoreCase("sent"))
//					{
//						
//					}
//					else if(tempDto.nameEn.trim().equalsIgnoreCase("private quarry"))
//					{
//						
//					}
//					else if(tempDto.nameEn.trim().equalsIgnoreCase("public quarry"))
//					{
//						
//					}
//					else if(tempDto.nameEn.trim().equalsIgnoreCase("mine"))
//					{
//						
//					}
//					else if(tempDto.nameEn.trim().equalsIgnoreCase("exploration"))
//					{
//						
//					}
//					else if(tempDto.nameEn.trim().equalsIgnoreCase("bmd"))
//					{
//						
//					}
//					else if(tempDto.nameEn.trim().equalsIgnoreCase("ministry"))
//					{
//						
//					}
//					else if(tempDto.nameEn.trim().equalsIgnoreCase("dcm"))
//					{
//						
//					}
//					else if(tempDto.nameEn.trim().equalsIgnoreCase("dc office"))
//					{
//						
//					}
//					else if(tempDto.nameEn.trim().equalsIgnoreCase("tender"))
//					{
//						
//					}
//					else if(tempDto.nameEn.trim().equalsIgnoreCase("payment"))
//					{
//						
//					}
//					else
//					{
//						
//						
//					}
				
				}
				
				
				
				
				
				}
			
			
			
			
			//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TREE_VIEW))
			//if(true)
			{
				String myLayer = request.getParameter("myLayer");
				if(myLayer != null && !myLayer.equalsIgnoreCase(""))
				{
					int layer = Integer.parseInt(request.getParameter("myLayer"));
					if(treeDTO.treeType.equalsIgnoreCase("tree") && 
							layer < treeDTO.parentChildMap.length && 
							treeDTO.sameTableParentColumnName != null &&							
							(!treeDTO.sameTableParentColumnName[layer].equals("") || !treeDTO.sameTableParentColumnName[layer].equals("")))
					{
						getRecursiveNodes(request, response, layer, treeDTO, treeDTO2);
					}
					else
					{
						getNormalNodes(request, response, layer, treeDTO, treeDTO2);
					}
				}
				else
				{					
					getTopNodes(request, response, treeDTO, treeDTO2);	
				}
			}
				
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			
			logger.debug("a1 : "+actionType + " : " + ex);
		}
	}
	
	private void setAttributes(int parentID, int layer, List<Integer> IDs, HttpServletRequest request, TreeDTO treeDTO, TreeDTO treeDTO2)
	{

		request.setAttribute("nodeIDs", IDs);
		request.setAttribute("myLayer", layer);
		String request_layer_id_pairs = request.getParameter("layer_id_pairs");
		if(request_layer_id_pairs == null)
		{
			request_layer_id_pairs = "";
		}
		System.out.println("layer_id_pairs = " + request_layer_id_pairs);
		treeDTO.layer_id_pairs = request_layer_id_pairs;
		request.setAttribute("treeDTO_cc", treeDTO);
		
		//request.setAttribute("treeDTO2", treeDTO2);
		
	}
	
	private void setParametersAndForward(int parentID, String nodeJSP, HttpServletRequest request, HttpServletResponse response, TreeDTO treeDTO, TreeDTO treeDTO2)
	{

		try 
		{
			String parentElementID = request.getParameter("parentElementID");
			String checkBoxChecked = request.getParameter("checkBoxChecked");
			request.getRequestDispatcher("inbox_internal_cc/" + nodeJSP 
					+ "?actionType=" + actionType 
					+ "&treeType=" + treeDTO.treeType 
					+ "&treeBody=" + treeDTO.treeBody 
					+ "&servletType=" + servletType
					+ "&pageTitle=" + treeDTO.pageTitle
					+ "&parentID=" + parentID
					+ "&checkBoxChecked=" + checkBoxChecked
					+ "&parentElementID=" + parentElementID
					+ treeDTO.additionalParams
					).forward(request, response);
		}
		catch (ServletException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	private String getNodeJSP(TreeDTO treeDTO)
	{
		if(treeDTO.treeType.equalsIgnoreCase("tree"))
		{
			return "treeNode2.jsp";
		}
		else if(treeDTO.treeType.equalsIgnoreCase("select"))
		{
			return "dropDownNode.jsp";
			
		}
		else if(treeDTO.treeType.equalsIgnoreCase("flat"))
		{
			return "expandedTreeNode.jsp";
			
		}
		return "";
	}
	
	private void getTopNodes(HttpServletRequest request, HttpServletResponse response, TreeDTO treeDTO, TreeDTO treeDTO2) throws ServletException, IOException 
	{
		System.out.println("METHOD: getTopNodes");
		try
		{
			List<Integer> IDs = GenericTree.getTopIDs(treeDTO.parentChildMap[0]);
			setAttributes(-1, 0, IDs, request, treeDTO, treeDTO2);
			String nodeJsp = "tree2.jsp";		
			setParametersAndForward(0, nodeJsp, request, response, treeDTO, treeDTO2);			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	private void getRecursiveNodes(HttpServletRequest request, HttpServletResponse response, int layer, TreeDTO treeDTO, TreeDTO treeDTO2)
    {
		System.out.println("METHOD: getRecursiveNodes");
        int id = Integer.parseInt(request.getParameter("id"));
        int parentTableID;
       
        try
        {
            parentTableID = Integer.parseInt(request.getParameter("parentID"));

            int iPrevLayer = -1;
            String prevLayer = request.getParameter("prevLayer");
            if (prevLayer != null && !prevLayer.equalsIgnoreCase(""))
            {
                iPrevLayer = Integer.parseInt(prevLayer);
            }
           // System.out.println("layer = " + layer + " id = " + id + " treeDTO.parentChildMap.length = " + treeDTO.parentChildMap.length + " iPrevLayer = " + iPrevLayer);
            if (layer != 0 &&
                    (treeDTO.sameTableParentColumnName != null &&
                            (!treeDTO.sameTableParentColumnName[layer].equals("") || !treeDTO.sameTableParentColumnName[layer].equals("")))
                    && layer != iPrevLayer)  //If a recursive node suddenly starts at midpoint
            {
                parentTableID = id;
                id = 0;
                System.out.println("AAAAAAAAAAAAAAA after reshuffling layer = " + layer + " id = " + id + " parentTableID = " + parentTableID);
            }

            if (layer < treeDTO.parentChildMap.length)
            {
                List<Integer> UnitIDs = GenericTree.getRecurviveIDs(treeDTO.parentChildMap[layer], treeDTO.sameTableParentColumnName[layer], id, treeDTO.parentIDColumn[layer], parentTableID);

                List<Integer> IDs = new ArrayList<Integer>();
                List<Integer> recursiveNodeTypes = new ArrayList<Integer>();

                setAttributes(id, layer, IDs, request, treeDTO, treeDTO2);

                if (id != 0 && layer + 1 < treeDTO.parentChildMap.length)
                {
                    List<Integer> ChildIDs = GenericTree.getChildIDs(treeDTO.parentChildMap[layer + 1], treeDTO.parentIDColumn[layer + 1], id);
                    if(ChildIDs == null)
                    {
                        ChildIDs = new ArrayList<>();

                    }
                    ChildIDs.add(-1);

                    if (ChildIDs != null)
                    {
                        System.out.println("ooooooooooooooooooo ChildIDs = " + ChildIDs.size());
                        for (int i = 0; i < ChildIDs.size(); i++) {
                            IDs.add(ChildIDs.get(i));
                            recursiveNodeTypes.add(2);
                        }
                    }
                }

                for (int i = 0; i < UnitIDs.size(); i++)
                {
                    IDs.add(UnitIDs.get(i));
                    recursiveNodeTypes.add(1);
                }
                request.setAttribute("isRecursive", 1);
                request.setAttribute("recursiveNodeTypes", recursiveNodeTypes);
                request.setAttribute("childName", treeDTO.parentChildMap[layer + 1]);
                request.setAttribute("childEnglishNameColumn", treeDTO.englishNameColumn[layer + 1]);
                request.setAttribute("childBanglaNameColumn", treeDTO.banglaNameColumn[layer + 1]);

				String nodeJsp = "treeNode2.jsp";
                System.out.println("SSSSSSSSSSSS Setting parentTableID = " + parentTableID);
                setParametersAndForward(parentTableID, nodeJsp, request, response, treeDTO, treeDTO2);

            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
	
	private void getNormalNodes(HttpServletRequest request, HttpServletResponse response, int layer, TreeDTO treeDTO, TreeDTO treeDTO2) throws ServletException, IOException 
	{
		
		System.out.println("METHOD: getNormalNodes");
		try
		{
			int id = Integer.parseInt(request.getParameter("id"));
			System.out.println("NNNNNNNNNNNNNN getNormalNodes layer = " + layer + " id = " + id);
			
			if(treeDTO.parentChildMap == null)
			{
				treeDTO.parentChildMap = new String[]{"office_ministries", "office_layers", "offices", "office_units", "office_unit_organograms"};
				
				treeDTO2.parentChildMap = new String[]{"office_ministries", "office_layers", "offices", "office_units", "office_unit_organograms"};
			}
			
			if(treeDTO.parentIDColumn == null)
			{
				treeDTO.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_id", "office_unit_id"};
				treeDTO2.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_id", "office_unit_id"};	
			
			}
			
				
				
			if(layer < treeDTO.parentChildMap.length)
			{				
				List<Integer> IDs = GenericTree.getChildIDs(treeDTO.parentChildMap[layer], treeDTO.parentIDColumn[layer], id);
				setAttributes(id, layer, IDs, request, treeDTO, treeDTO2);
				String nodeJsp = getNodeJSP(treeDTO);
				treeDTO.treeType = "tree";
				treeDTO2.treeType = "tree";
				System.out.println("Going to geoNode, treeDTO.treeType = " + treeDTO.treeType);				
				setParametersAndForward(id, nodeJsp, request, response, treeDTO, treeDTO2);				
			
			}
			else
			{
				System.out.println("Maximum Layer Reached");
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}


