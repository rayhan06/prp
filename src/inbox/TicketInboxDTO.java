package inbox;

import util.CommonDTO;

public class TicketInboxDTO extends CommonDTO
{
	public long ticketIssuesType = 0;
	public long ticketIssuesSubtypeType = 0;
	public int ticketStatusCat = 0;
	public int priorityCat = 0;
	public long arrivalDate = 0;
	public long supportTicketId = -1;
	public long issueRaiser = -1;
	
	public String description = "";
	public String issueText = "";
	
	public long dueDate = -1;

}
