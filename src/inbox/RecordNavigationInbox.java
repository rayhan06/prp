package inbox;

import config.GlobalConfigConstants;
import config.GlobalConfigurationRepository;
import login.LoginDTO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.NavigationService3;
import util.RecordNavigator;
import workflow.WorkflowController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

//import org.apache.log4j.Logger;

public class RecordNavigationInbox {
    String m_navigatorName;
    String m_tableName;
    String m_dtoCollectionName;
    HttpServletRequest m_request;
    NavigationService3 m_service;
    String[][] m_searchFieldInfo;
    int m_countIDs = 0;
    int m_totalPage = 0;
    int m_recordsPerPage = 0;
    Hashtable m_Hashtable = null;
    HttpSession m_Session = null;
    long m_timeToSet = 0;
    boolean m_isPermanentTable = true;
    long m_userApprovalPathID = 0;

    //static Logger logger = Logger.getLogger(RecordNavigationManager.class.getName());

    public RecordNavigationInbox(String p_navigatorName, HttpServletRequest p_request, NavigationService3 p_service,
                                 String p_dtoCollectionName, String[][] p_searchFieldInfo, String p_tableName, boolean isPermanentTable, long userApprovalPathID) {
        m_navigatorName = p_navigatorName;
        m_request = p_request;
        m_service = p_service;
        m_dtoCollectionName = p_dtoCollectionName;
        m_searchFieldInfo = p_searchFieldInfo;
        m_Hashtable = null;
        m_tableName = p_tableName;
        m_isPermanentTable = isPermanentTable;
        m_userApprovalPathID = userApprovalPathID;
    }


    private void setRN(RecordNavigator rn, int tempPageNumber) {
        rn.setSearchFieldInfo(m_searchFieldInfo);
        m_totalPage = m_countIDs / rn.getPageSize();
        if (m_countIDs % rn.getPageSize() != 0) {
            m_totalPage++;
        }
        rn.setCurrentPageNo(tempPageNumber);
       
        rn.setTotalRecords(m_countIDs);
        rn.setTotalPages(m_totalPage);

        rn.m_isPermanentTable = m_isPermanentTable;
        rn.m_tableName = m_tableName;


        rn.setCurrentPageNo(tempPageNumber);
        rn.setSearchTime(m_timeToSet);

        System.out.println("after setting, rn = " + rn);

    }


    private void fillHashTable() {
        Enumeration paramList = m_request.getParameterNames();
        m_Hashtable = new Hashtable();
        do {
            if (!paramList.hasMoreElements()) {
                break;
            }
            String paramName = (String) paramList.nextElement();
            if (!paramName.equalsIgnoreCase("RECORDS_PER_PAGE") && !paramName.equalsIgnoreCase("search") && !paramName.equalsIgnoreCase(SessionConstants.HTML_SEARCH_CHECK_FIELD) && !paramName.equalsIgnoreCase("ajax")) {
                String paramValue = m_request.getParameter(paramName);


                m_Session.setAttribute(paramName, paramValue);
                m_Hashtable.put(paramName, paramValue);
            }
        } while (true);

    }

    public void doJob(LoginDTO loginDTO, HttpServletRequest request) throws Exception {


       

        InboxDAO tempInboxDao = new InboxDAO();

        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        List<InboxTabData> tabDataList = null;//new ArrayList<InboxTabData>();//tempInboxDao.getTabData2(loginDTO.userID, userDTO.userType,tempTabID);
       

        System.out.println("Doing Job");

        m_Session = m_request.getSession();

        RecordNavigator rn = (RecordNavigator) m_Session.getAttribute(m_navigatorName);

        if (rn == null) {
            rn = new RecordNavigator();
        }

        String searchCheck = m_request.getParameter("search");


        long currentTime = System.currentTimeMillis();
        long lastSearchTime = 0;
        if (m_request.getParameter("lastSearchTime") != null && !m_request.getParameter("lastSearchTime").equalsIgnoreCase("")) {
            lastSearchTime = Long.parseLong(m_request.getParameter("lastSearchTime"));
        }
        m_timeToSet = lastSearchTime;

        String pagenoStr = m_request.getParameter("pageno");


        int pageno = 1;

        if (pagenoStr != null) {
            try {
                pageno = Integer.parseInt(pagenoStr.trim());

                System.out.println("Page Size Given 3 : " + pageno);

            } catch (NumberFormatException ex) {
                //logger.fatal("Next page Size is not number ");
            }
        }

        int pageSizeInt = -1;
        String pageSize = m_request.getParameter("RECORDS_PER_PAGE");
        System.out.println("Page Size Given : " + pageSize);


        if (pageSize != null) {
            try {
                pageSizeInt = Integer.parseInt(pageSize);
                if (pageSizeInt > 0) {
                    rn.setPageSize(pageSizeInt);
                    System.out.println("Page Size Given 2 : " + pageSizeInt);
                }
            } catch (NumberFormatException ex) {
                //logger.fatal("Next page Size is not number ");
            }
        }

        if (searchCheck != null) //Normal or ajax search
        {
            System.out.println("###########Search pressed");

            fillHashTable();
            m_timeToSet = currentTime;

            setRN(rn, pageno);
        }
        else //1st time in the search page
        {
            System.out.println("###########From Menu");
            m_Session.setAttribute(m_navigatorName, null);
            m_timeToSet = currentTime;
            setRN(rn, 1);
        }


        int nextCollectionSize;
        
        nextCollectionSize = rn.getPageSize();

        System.out.println("after setting, rn 6 = " + nextCollectionSize);
        
        int initial = nextCollectionSize != 0 ? (rn.getCurrentPageNo() - 1) * rn.getPageSize() : 0;

        System.out.println("############################ limit = " + nextCollectionSize + " offset = " + initial);


        tabDataList = tempInboxDao.getData(initial, nextCollectionSize, userDTO, m_Hashtable);
        int iCount = 0;
        if(tabDataList != null)
        {
        	 for(InboxTabData tabData: tabDataList)
             {
             	if(tabData.count > iCount)
             	{
             		iCount = tabData.count;
             	}
             }
        	m_countIDs = iCount;
        	setRN(rn, pageno);
        }
       
        
        setRN(rn, pageno);


        request.getSession(true).setAttribute("navigator", rn);

        if(tabDataList == null)
        {
        	System.out.println("In rnm, tabDataList is null");
        }
        request.setAttribute("tab_data", tabDataList);
        request.setAttribute("tab_id", 1);
    }
}