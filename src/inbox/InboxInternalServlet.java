package inbox;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import edms_documents.Edms_documentsDAO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import centre.Constants;
import inbox_movements.Inbox_movementsDAO;
import inbox_movements.Inbox_movementsDTO;
import inbox_tab.Inbox_tabDAO;
import inbox_tab.Inbox_tabDTO;
import login.LoginDTO;
import permission.MenuConstants;
import project_tracker.FilesDAO;
import project_tracker.FilesDTO;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import treeView.TreeDTO;
import treeView.TreeFactory;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.RootstockUtils;
import pb.*;

/**
 * Servlet implementation class TreeServlet
 */
@WebServlet("/InboxInternalServlet")
public class InboxInternalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	 public static Logger logger = Logger.getLogger(InboxInternalServlet.class);
	
	public String actionType = "";
	public String servletType = "InboxInternalServlet";

	private final InboxDAO inboxDAO = new InboxDAO();
	private final Inbox_movementsDAO inbox_movementsDAO = new Inbox_movementsDAO();
	private final Edms_documentsDAO edms_documentsDAO = new Edms_documentsDAO();
	
	  long insertId = 0;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InboxInternalServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		
	
		try
		{
			actionType = request.getParameter("actionType");
			TreeDTO treeDTO = new TreeDTO();
			
			TreeDTO treeDTO2 = new TreeDTO();
			//System.out.println("############################# In doget2 actionType = " + actionType);
			if(actionType.equals("getOffice"))
			{
				
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.MINISTRIES_TO_OFFICES_INCLUDING_ORIGINS);
				treeDTO.pageTitle = "Office Management";
				treeDTO.treeName = "OfficeDropDownChain";
				treeDTO.treeType = "select";
				treeDTO.treeBody = "officeOriginTreeBody.jsp";
				treeDTO.checkBoxType = 0;
				treeDTO.plusButtonType = 0;
			}
			else if (actionType.equals("makeSeen")) 
			{
				long id = Long.parseLong(request.getParameter("id"));
				System.out.println("making seen, id = " + id);
				inbox_movementsDAO.setIsSeen(id);
				return;				
			}
			else if (actionType.equals("send")) {
				
				
				
			}
			else if (actionType.equals("cc")) {
				
			}
			else if (actionType.equals("forward")) {
	
			}
			
			else if ( actionType.equals( "assignPathToOrganogram" ) ) {

				treeDTO.parentChildMap = new String[]{ "office_units", "office_unit_organograms"};
				treeDTO.showNameMap = new String[]{ "Offices Units", "Office Unit Organograms"};
				treeDTO.parentIDColumn = new String[]{ "", "office_unit_id"};
				treeDTO.englishNameColumn = new String[]{ "unit_name_eng", "designation_eng"};
				treeDTO.banglaNameColumn = new String[]{ "unit_name_bng", "designation_bng"};

				treeDTO.pageTitle = "Assign Approval Path";
				treeDTO.treeName = "approvalPathTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "OrganogramPathTreeBody.jsp";
				treeDTO.sameTableParentColumnName = new String[]{  "parent_unit_id", ""}; //only for recursive tables
				treeDTO.checkBoxType = 4;
				treeDTO.plusButtonType = 0;
				
				
				treeDTO2.parentChildMap = new String[]{"office_ministries", "office_layers", "offices", "office_units", "office_unit_organograms"};
				treeDTO2.showNameMap = new String[]{"Office Ministries", "Office Layers", "Offices", "Office Units", "Office Unit Organograms"};
				treeDTO2.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_id", "office_unit_id"};
				treeDTO2.englishNameColumn = new String[]{"name_eng", "layer_name_eng", "office_name_eng", "unit_name_eng", "designation_eng"};
				treeDTO2.banglaNameColumn = new String[]{"name_bng", "layer_name_bng", "office_name_bng", "unit_name_bng", "designation_bng"};

				treeDTO2.pageTitle = "Assign Approval Path";
				treeDTO2.treeName = "approvalPathTree";
				treeDTO2.treeType = "tree";
				treeDTO2.treeBody = "OrganogramPathTreeBody.jsp";
				treeDTO2.sameTableParentColumnName = new String[]{"", "", "", "parent_unit_id", ""}; //only for recursive tables
				treeDTO2.checkBoxType = 1;
				treeDTO2.plusButtonType = 0;
				
				
				String tabId = request.getParameter("tab_id");
				
				String rowId = request.getParameter("row_id");
				
				long tebIdLong = 0;
				
				long rowIdLong  =0;
				
				if(tabId != null && rowId != null){

					 tebIdLong = Long.parseLong(tabId);
					 rowIdLong = Long.parseLong(rowId);
				}

				if(rowIdLong != 0 && tebIdLong != 0)
				{
				
					Inbox_tabDAO tempDao = new Inbox_tabDAO();
					Inbox_tabDTO tempDto = (Inbox_tabDTO) tempDao.getDTOByID(tebIdLong, "inbox_tab");
				}

				inbox_movementsDAO.setIsSeenIfMatchesToOrganogramId( rowIdLong, userDTO.organogramID, 1 );
			}

			//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TREE_VIEW))
			{
				String myLayer = request.getParameter("myLayer");
				if(myLayer != null && !myLayer.equalsIgnoreCase(""))
				{
					int layer = Integer.parseInt(request.getParameter("myLayer"));
					if(treeDTO.treeType.equalsIgnoreCase("tree") && 
							layer < treeDTO.parentChildMap.length && 
							treeDTO.sameTableParentColumnName != null &&							
							(!treeDTO.sameTableParentColumnName[layer].equals("") || !treeDTO.sameTableParentColumnName[layer].equals("")))
					{
						getRecursiveNodes(request, response, layer, treeDTO, treeDTO2);
					}
					else
					{
						getNormalNodes(request, response, layer, treeDTO, treeDTO2);
					}
				}
				else
				{					
					getTopNodes(request, response, treeDTO, treeDTO2);	
				}
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			
			logger.debug("a1 : "+actionType + " : " + ex);
		}
	}
	
	private List<FilesDTO> createProjectTrackerFilesDTOListByRequest(HttpServletRequest request,boolean addFlag) throws Exception{ 
		List<FilesDTO> projectTrackerFilesDTOList = new ArrayList<FilesDTO>();
		if(request.getParameterValues("projectTrackerFiles.iD") != null)
		{
			int projectTrackerFilesItemNo = request.getParameterValues("projectTrackerFiles.iD").length;
			
			
			for(int index=0;index<projectTrackerFilesItemNo;index++){
				FilesDTO projectTrackerFilesDTO = createProjectTrackerFilesDTOByRequestAndIndex(request,addFlag,index);
				
				projectTrackerFilesDTO.rowId = insertId;
				
				projectTrackerFilesDTOList.add(projectTrackerFilesDTO);
			}
		}	
		
		
		if(request.getParameterValues("projectTrackerFiles.titleedit.id") != null)
		{
			int projectTrackerFilesItemNo = request.getParameterValues("projectTrackerFiles.titleedit.id").length;
			
			
			for(int index=0;index<projectTrackerFilesItemNo;index++){
				FilesDTO projectTrackerFilesDTO2 = createProjectTrackerFilesDTOByRequestAndIndex2(request,addFlag,index);
				
				
			}
		}
		
		return projectTrackerFilesDTOList;
	}
	
	private FilesDTO createProjectTrackerFilesDTOByRequestAndIndex2(HttpServletRequest request, boolean addFlag,int index) throws Exception{
		
		
		String ValueTitleEdit = "";
		ValueTitleEdit = request.getParameterValues("projectTrackerFiles.titleedit")[index];

		if(ValueTitleEdit != null)
		{
			ValueTitleEdit = Jsoup.clean(ValueTitleEdit,Whitelist.simpleText());
		}
		
		String ValueTagEdit = "";
		ValueTagEdit = request.getParameterValues("projectTrackerFiles.tagedit")[index];

		if(ValueTagEdit != null)
		{
			ValueTagEdit = Jsoup.clean(ValueTagEdit,Whitelist.simpleText());
		}
		
		String ValueIDEdit = "";
		ValueIDEdit = request.getParameterValues("projectTrackerFiles.titleedit.id")[index];

		if(ValueIDEdit != null)
		{
			ValueIDEdit = Jsoup.clean(ValueIDEdit,Whitelist.simpleText());
			
			FilesDTO tempDTO = new FilesDTO();
			
			tempDTO.iD = Long.parseLong(ValueIDEdit);
			
			tempDTO.fileTag = ValueTagEdit;
			
			tempDTO.fileTitle = ValueTitleEdit;
			
			FilesDAO tempDao = new FilesDAO();
			
			tempDao.UpdateFileTileTag(tempDTO);
			
			
		}	
		
		return null;
	
	}
	
	
	
	
	
	
	private FilesDTO createProjectTrackerFilesDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{
	
		
		FilesDTO projectTrackerFilesDTO = new FilesDTO();
		String path = getServletContext().getRealPath("/img2/");		
				
		String Value = "";
		Value = request.getParameterValues("projectTrackerFiles.projectTrackerId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		
		String ValueTitle = "";
		ValueTitle = request.getParameterValues("projectTrackerFiles.title")[index];

		if(ValueTitle != null)
		{
			ValueTitle = Jsoup.clean(ValueTitle,Whitelist.simpleText());
		}
		
		String ValueTag = "";
		ValueTag = request.getParameterValues("projectTrackerFiles.tag")[index];

		if(ValueTag != null)
		{
			ValueTag = Jsoup.clean(ValueTag,Whitelist.simpleText());
		}

		//projectTrackerFilesDTO.rowId = insertId;//Long.parseLong(request.getParameterValues("projectTrackerFiles.projectTrackerId")[index]);
		
		Part part = RootstockUtils.uploadFileByIndex2("projectTrackerFiles.dataFile", path, index, request);	
		
		String name = part.getName();
		
		String tempExtention = RootstockUtils.getFileExtension(name);
		//String name = part.getName();
		
		String tempName = "";
		
		if(ValueTitle.trim().length() > 0)
		{
			tempName = ValueTitle + "." + tempExtention;
		}
		else
		{
			tempName = name;
		}
		
		projectTrackerFilesDTO.fileTag = ValueTag;
		
		projectTrackerFilesDTO.fileTitle = tempName;
		
		projectTrackerFilesDTO.fileType = part.getContentType();

		projectTrackerFilesDTO.fileData = Utils.uploadFileToByteAray(part);
		
		// update tag, title
		
		
		
		String ValueTitleEdit = "";
		
		if(request.getParameterValues("projectTrackerFiles.titleedit") != null)
		{
		ValueTitleEdit = request.getParameterValues("projectTrackerFiles.titleedit")[index];

		if(ValueTitleEdit != null)
		{
			ValueTitleEdit = Jsoup.clean(ValueTitleEdit,Whitelist.simpleText());
		}
		}
		
		String ValueTagEdit = "";
		
		if(request.getParameterValues("projectTrackerFiles.tagedit") != null)
		{
		ValueTagEdit = request.getParameterValues("projectTrackerFiles.tagedit")[index];

		if(ValueTagEdit != null)
		{
			ValueTagEdit = Jsoup.clean(ValueTagEdit,Whitelist.simpleText());
		}
		
		}
		
		String ValueIDEdit = "";
		
		if(request.getParameterValues("projectTrackerFiles.titleedit.id") != null)
		{
		ValueIDEdit = request.getParameterValues("projectTrackerFiles.titleedit.id")[index];

		if(ValueIDEdit != null)
		{
			ValueIDEdit = Jsoup.clean(ValueIDEdit,Whitelist.simpleText());
			
			FilesDTO tempDTO = new FilesDTO();
			
			tempDTO.iD = Long.parseLong(ValueIDEdit);
			
			tempDTO.fileTag = ValueTagEdit;
			
			tempDTO.fileTitle = ValueTitleEdit;
			
			FilesDAO tempDao = new FilesDAO();
			
			tempDao.UpdateFileTileTag(tempDTO);
			
			
		}
		
		}
		
		
		
		
		
		
		
		return projectTrackerFilesDTO;
	
	}
	
	
	private void setAttributes(int parentID, int layer, List<Integer> IDs, HttpServletRequest request, TreeDTO treeDTO, TreeDTO treeDTO2)
	{

		request.setAttribute("nodeIDs", IDs);
		request.setAttribute("myLayer", layer);
		String request_layer_id_pairs = request.getParameter("layer_id_pairs");
		if(request_layer_id_pairs == null)
		{
			request_layer_id_pairs = "";
		}
		System.out.println("layer_id_pairs = " + request_layer_id_pairs);
		treeDTO.layer_id_pairs = request_layer_id_pairs;
		request.setAttribute("treeDTO", treeDTO);
		
		request.setAttribute("treeDTO2", treeDTO2);
		
	}
	
	private void setParametersAndForward(int parentID, String nodeJSP, HttpServletRequest request, HttpServletResponse response, TreeDTO treeDTO, TreeDTO treeDTO2)
	{

		try 
		{
			String parentElementID = request.getParameter("parentElementID");
			String checkBoxChecked = request.getParameter("checkBoxChecked");
			request.getRequestDispatcher("inbox_internal/" + nodeJSP 
					+ "?actionType=" + actionType 
					+ "&treeType=" + treeDTO.treeType 
					+ "&treeBody=" + treeDTO.treeBody 
					+ "&servletType=" + servletType
					+ "&pageTitle=" + treeDTO.pageTitle
					+ "&parentID=" + parentID
					+ "&checkBoxChecked=" + checkBoxChecked
					+ "&parentElementID=" + parentElementID
					+ treeDTO.additionalParams
					).forward(request, response);
		}
		catch (ServletException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	private String getNodeJSP(TreeDTO treeDTO)
	{
		if(treeDTO.treeType.equalsIgnoreCase("tree"))
		{
			return "treeNode.jsp";
		}
		else if(treeDTO.treeType.equalsIgnoreCase("select"))
		{
			return "dropDownNode.jsp";
			
		}
		else if(treeDTO.treeType.equalsIgnoreCase("flat"))
		{
			return "expandedTreeNode.jsp";
			
		}
		return "";
	}
	
	private void getTopNodes(HttpServletRequest request, HttpServletResponse response, TreeDTO treeDTO, TreeDTO treeDTO2) throws ServletException, IOException 
	{
		System.out.println("METHOD: getTopNodes");
		try
		{
			List<Integer> IDs = GenericTree.getTopIDs(treeDTO.parentChildMap[0]);
			setAttributes(-1, 0, IDs, request, treeDTO, treeDTO2);
			String nodeJsp = "tree.jsp";		
			setParametersAndForward(0, nodeJsp, request, response, treeDTO, treeDTO2);			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	private void getRecursiveNodes(HttpServletRequest request, HttpServletResponse response, int layer, TreeDTO treeDTO, TreeDTO treeDTO2)
    {
		System.out.println("METHOD: getRecursiveNodes");
        int id = Integer.parseInt(request.getParameter("id"));
        int parentTableID;
       
        try
        {
            parentTableID = Integer.parseInt(request.getParameter("parentID"));

            int iPrevLayer = -1;
            String prevLayer = request.getParameter("prevLayer");
            if (prevLayer != null && !prevLayer.equalsIgnoreCase(""))
            {
                iPrevLayer = Integer.parseInt(prevLayer);
            }
           // System.out.println("layer = " + layer + " id = " + id + " treeDTO.parentChildMap.length = " + treeDTO.parentChildMap.length + " iPrevLayer = " + iPrevLayer);
            if (layer != 0 &&
                    (treeDTO.sameTableParentColumnName != null &&
                            (!treeDTO.sameTableParentColumnName[layer].equals("") || !treeDTO.sameTableParentColumnName[layer].equals("")))
                    && layer != iPrevLayer)  //If a recursive node suddenly starts at midpoint
            {
                parentTableID = id;
                id = 0;
                System.out.println("AAAAAAAAAAAAAAA after reshuffling layer = " + layer + " id = " + id + " parentTableID = " + parentTableID);
            }

            if (layer < treeDTO.parentChildMap.length)
            {
                List<Integer> UnitIDs = GenericTree.getRecurviveIDs(treeDTO.parentChildMap[layer], treeDTO.sameTableParentColumnName[layer], id, treeDTO.parentIDColumn[layer], parentTableID);

                List<Integer> IDs = new ArrayList<Integer>();
                List<Integer> recursiveNodeTypes = new ArrayList<Integer>();

                setAttributes(id, layer, IDs, request, treeDTO, treeDTO2);

                if (id != 0 && layer + 1 < treeDTO.parentChildMap.length)
                {
                    List<Integer> ChildIDs = GenericTree.getChildIDs(treeDTO.parentChildMap[layer + 1], treeDTO.parentIDColumn[layer + 1], id);
                    if(ChildIDs == null)
                    {
                        ChildIDs = new ArrayList<>();

                    }
                    ChildIDs.add(-1);

                    if (ChildIDs != null)
                    {
                        System.out.println("ooooooooooooooooooo ChildIDs = " + ChildIDs.size());
                        for (int i = 0; i < ChildIDs.size(); i++) {
                            IDs.add(ChildIDs.get(i));
                            recursiveNodeTypes.add(2);
                        }
                    }
                }

                for (int i = 0; i < UnitIDs.size(); i++)
                {
                    IDs.add(UnitIDs.get(i));
                    recursiveNodeTypes.add(1);
                }
                request.setAttribute("isRecursive", 1);
                request.setAttribute("recursiveNodeTypes", recursiveNodeTypes);
                request.setAttribute("childName", treeDTO.parentChildMap[layer + 1]);
                request.setAttribute("childEnglishNameColumn", treeDTO.englishNameColumn[layer + 1]);
                request.setAttribute("childBanglaNameColumn", treeDTO.banglaNameColumn[layer + 1]);

				String nodeJsp = "treeNode.jsp";
                System.out.println("SSSSSSSSSSSS Setting parentTableID = " + parentTableID);
                setParametersAndForward(parentTableID, nodeJsp, request, response, treeDTO, treeDTO2);

            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
	
	private void getNormalNodes(HttpServletRequest request, HttpServletResponse response, int layer, TreeDTO treeDTO, TreeDTO treeDTO2) throws ServletException, IOException 
	{
		
		System.out.println("METHOD: getNormalNodes");
		try
		{
			int id = Integer.parseInt(request.getParameter("id"));
			System.out.println("NNNNNNNNNNNNNN getNormalNodes layer = " + layer + " id = " + id);
			
			if(treeDTO.parentChildMap == null)
			{
				treeDTO.parentChildMap = new String[]{"office_ministries", "office_layers", "offices", "office_units", "office_unit_organograms"};
				
				treeDTO2.parentChildMap = new String[]{"office_ministries", "office_layers", "offices", "office_units", "office_unit_organograms"};
			}
			
			if(treeDTO.parentIDColumn == null)
			{
				treeDTO.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_id", "office_unit_id"};
				treeDTO2.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_id", "office_unit_id"};	
			
			}
			
				
				
			if(layer < treeDTO.parentChildMap.length)
			{				
				List<Integer> IDs = GenericTree.getChildIDs(treeDTO.parentChildMap[layer], treeDTO.parentIDColumn[layer], id);
				setAttributes(id, layer, IDs, request, treeDTO, treeDTO2);
				String nodeJsp = getNodeJSP(treeDTO);
				treeDTO.treeType = "tree";
				treeDTO2.treeType = "tree";
				System.out.println("Going to geoNode, treeDTO.treeType = " + treeDTO.treeType);				
				setParametersAndForward(id, nodeJsp, request, response, treeDTO, treeDTO2);				
			
			}
			else
			{
				System.out.println("Maximum Layer Reached");
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// TODO Auto-generated method stub
				LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
				UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
				

				System.out.println("doPost");
				boolean isPermanentTable = true;
				if(request.getParameter("isPermanentTable") != null)
				{
					isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
				}
				System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
				
				try
				{
					String actionType = request.getParameter("actionType");
					System.out.println("actionType = " + actionType);

					if(actionType.equals("distributeDoc")){

						Date deadlineDate = null;
						String deadline = request.getParameter( "inbox_sender_deadline" );
						SimpleDateFormat sdf = new SimpleDateFormat( CommonConstant.EDMS_DEADLINE_DATE_FORMAT );
						if( deadline != null && deadline.trim().length() != 0 )
							deadlineDate = sdf.parse( deadline );

						String mainReceiverOrganogramIdStr = request.getParameter("mainReceiver" );
						Long mainReceiverOrganogramId = null;

						if( mainReceiverOrganogramIdStr != null )
							mainReceiverOrganogramId= Long.parseLong( mainReceiverOrganogramIdStr );

						String[] ccReceivers = request.getParameterValues( "ccReceiver" );
						List<Long> ccReceiverOrgIdList = new ArrayList<>();

						if( ccReceivers != null ) {
							for ( String ccReceiver : ccReceivers ) {

								ccReceiverOrgIdList.add(Long.parseLong(ccReceiver));
							}
						}

						String note = request.getParameter("note" );
						if( note != null ){

							note = Jsoup.clean( note,Whitelist.simpleText() );
						}

						String tabId = request.getParameter("tab_id");
						String rowId = request.getParameter("row_id");

						Map<String, String> inboxTabDataMap = inboxDAO.getTabDetailsData( tabId, rowId );

						Long inboxId = Long.parseLong( inboxTabDataMap.get( "inbox_id" ) );
						InboxDTO inboxDTO = (InboxDTO) inboxDAO.getDTOByID( inboxId );

						List<Inbox_movementsDTO> inbox_movementsDTOList = getListOfInboxMovementForAllReceivers( mainReceiverOrganogramId, ccReceiverOrgIdList, note, inboxId, userDTO );

						inbox_movementsDAO.add( inbox_movementsDTOList );

						inboxDAO.updateInboxStatus( inboxId, CommonConstant.DISTRIBUTION_STATE_ID );
						inbox_movementsDAO.updateActionTaken( rowId, true );

						if( deadlineDate != null )
							edms_documentsDAO.updateDeadline( inboxDTO.inboxOriginatorId, deadlineDate.getTime() );

						RequestDispatcher rd = request.getRequestDispatcher("InboxServlet?actionType=getInboxMain");
						rd.forward(request, response);

					}
					else if(actionType.equals("send"))
					{
						
						if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_MAIN_INTERNAL))
						{
							int tempStatus = 0;
							
							long tempToID = 0;
							
							String tempNote = "";
							long tempTabID = 0, tempRowID = 0;
							
							String Value = request.getParameter("status");
							if(Value != null)
							{
								Value = Jsoup.clean(Value,Whitelist.simpleText());
							}
							//System.out.println("directorNationality = " + Value);
							if(Value != null && !Value.equalsIgnoreCase(""))
							{
								tempStatus = Integer.parseInt(Value);
							}
							else
							{
								System.out.println("FieldName has a null Value, not updating" + " = " + Value);
							}
							
							Value = request.getParameter("id");
							if(Value != null)
							{
								Value = Jsoup.clean(Value,Whitelist.simpleText());
							}
							//System.out.println("directorNationality = " + Value);
							if(Value != null && !Value.equalsIgnoreCase(""))
							{
								tempToID = Long.parseLong(Value);
							}
							else
							{
								System.out.println("FieldName has a null Value, not updating" + " = " + Value);
							}
							
							
							Value = request.getParameter("note");
							if(Value != null)
							{
								Value = Jsoup.clean(Value,Whitelist.simpleText());
							}
							System.out.println("directorNationality = " + Value);
							if(Value != null && !Value.equalsIgnoreCase(""))
							{
								tempNote = Value;
							}
							else
							{
								System.out.println("FieldName has a null Value, not updating" + " = " + Value);
							}
							
							//row_id, tab_id tempTabID
							
							Value = request.getParameter("tab_id");
							if(Value != null)
							{
								Value = Jsoup.clean(Value,Whitelist.simpleText());
							}
							System.out.println("directorNationality = " + Value);
							if(Value != null && !Value.equalsIgnoreCase(""))
							{
								tempTabID = Long.parseLong(Value);
							}
							else
							{
								System.out.println("FieldName has a null Value, not updating" + " = " + Value);
							}
							
							Value = request.getParameter("row_id");
							if(Value != null)
							{
								Value = Jsoup.clean(Value,Whitelist.simpleText());
							}
							System.out.println("directorNationality = " + Value);
							if(Value != null && !Value.equalsIgnoreCase(""))
							{
								tempRowID = Long.parseLong(Value);
							}
							else
							{
								System.out.println("FieldName has a null Value, not updating" + " = " + Value);
							}
							
							InboxDAO tempinboxDAO = new InboxDAO();
							
							
							Map<String, String>  tempData = tempinboxDAO.getTabDetailsData(Long.toString(tempTabID),Long.toString(tempRowID));
							
							Inbox_movementsDTO tempDto = new Inbox_movementsDTO();
							
							tempDto.inboxId = Long.parseLong(tempData.get("inbox_id"));
							
							tempDto.inbox_type = SessionConstants.INBOX_TYPE_SEND;
							
							tempDto.note = tempNote;
							
							tempDto.fromEmployeeUserId =loginDTO.userID;// Long.parseLong(tempData.get("from_employee_user_id"));
							
							tempDto.fromOrganogramId = loginDTO.userID;//Long.parseLong(tempData.get("from_organogram_id"));
							
							tempDto.toOrganogramId = tempToID;
							
							long lastModificationTime = System.currentTimeMillis();	
							
							tempDto.createdAt = lastModificationTime;
							
							if(tempDto.fromEmployeeUserId != 0)
							{
							
							tempDto.createdBy = loginDTO.userID;//tempDto.fromEmployeeUserId;
							}
							else
							{
								tempDto.createdBy = loginDTO.userID;//tempDto.fromOrganogramId;
							}
							
							tempDto.currentStatus = tempStatus;
							
							Inbox_movementsDAO tempMoveDao = new Inbox_movementsDAO();
							
							
							
							List<FilesDTO> FilesDTOList = createProjectTrackerFilesDTOListByRequest(request,true);	
							
							tempMoveDao.add(tempDto);
							
							
							
							//long tempTabID = 0, tempRowID = 0;
							
							if(tempTabID == SessionConstants.INBOX_TAB_INBOX)
							{
								Inbox_movementsDTO tempDto3 = (Inbox_movementsDTO) tempMoveDao.getDTOByID(tempRowID);
								
								tempDto3.is_send = 1;
								
								tempMoveDao.update(tempDto3);
								
								
							}

							tempinboxDAO.updateInboxStatus( tempDto.inboxId, tempStatus );
							//tempDto.files = FilesDTOList;
							
							// FilesDAO tempFileDao = new FilesDAO();			
							// tempFileDao.insertFiles(FilesDTOList, "inbox_movements");
							//http://119.148.4.20:8087/rootstocktest6/InboxServlet?actionType=getInboxMain
							
							RequestDispatcher rd = request.getRequestDispatcher("InboxServlet?actionType=getInboxMain");
							rd.forward(request, response);
						}
						else
						{
							System.out.println("Not going to  addBug_tracker ");
							request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
						}
						
					}
					
					
					
					
					
					
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					logger.debug(ex);
				}
		
	}

	private List<Inbox_movementsDTO> getListOfInboxMovementForAllReceivers(

			Long mainReceiverOrganogramId,
			List<Long> ccReceiverOrgIdList,
			String note,
			Long inboxId,
			UserDTO userDTO
	) {

		List<Inbox_movementsDTO> inbox_movementsDTOList = new ArrayList<>();

		Inbox_movementsDTO mainReceiverInboxMovement = getInboxMovementWithStatus( mainReceiverOrganogramId, note, inboxId, userDTO, CommonConstant.DISTRIBUTION_STATE_ID );

		if( mainReceiverInboxMovement != null )
			inbox_movementsDTOList.add( mainReceiverInboxMovement );

		for( Long ccReceiverOrgId: ccReceiverOrgIdList ){

			Inbox_movementsDTO ccReceiverInboxMovement = getInboxMovementWithStatus( ccReceiverOrgId, note, inboxId, userDTO, CommonConstant.DISTRIBUTION_FINISH_STATE_ID );

			if( ccReceiverInboxMovement != null )
				inbox_movementsDTOList.add( ccReceiverInboxMovement );
		}

		return inbox_movementsDTOList;

	}

	private Inbox_movementsDTO getInboxMovementWithStatus( Long toOrganogramId, String note, Long inboxId, UserDTO userDTO, int status ) {

		if( toOrganogramId == null )
			return null;

		Inbox_movementsDTO inboxMovementsDTO = new Inbox_movementsDTO();

		inboxMovementsDTO.inboxId = inboxId;
		inboxMovementsDTO.inbox_type = SessionConstants.INBOX_TYPE_SEND;
		inboxMovementsDTO.note = note;
		inboxMovementsDTO.fromOrganogramId = userDTO.organogramID;
		inboxMovementsDTO.fromEmployeeUserId = userDTO.ID;
		inboxMovementsDTO.toOrganogramId = toOrganogramId;
		inboxMovementsDTO.createdAt = System.currentTimeMillis();
		inboxMovementsDTO.createdBy = userDTO.ID;
		inboxMovementsDTO.currentStatus = status;

		return inboxMovementsDTO;
	}

}


