package inbox;

import util.CommonMaps;

import java.util.HashMap;


public class InboxMAPS extends CommonMaps {
    public HashMap<String, String> inbox_anyfield_search_map_tab_1 = new HashMap<String, String>();
    public HashMap<String, String> inbox_anyfield_search_map_tab_2 = new HashMap<String, String>();
    public HashMap<String, String> inbox_anyfield_search_map_tab_3 = new HashMap<String, String>();
    public HashMap<String, String> inbox_anyfield_search_map_tab_4 = new HashMap<String, String>();
    public HashMap<String, String> inbox_anyfield_search_map_tab_5 = new HashMap<String, String>();
    public HashMap<Integer, HashMap<String, String>> inbox_anyfield_search_map = new HashMap<Integer, HashMap<String, String>>();

    private static InboxMAPS self = null;
    public InboxMAPS(String tableName) {

        inbox_anyfield_search_map_tab_1.put("a.ID", "String");
        inbox_anyfield_search_map_tab_1.put("lang.languageTextEnglish", "String");
        inbox_anyfield_search_map_tab_1.put("doc.document_no", "String");
        inbox_anyfield_search_map_tab_1.put("idx.name_en", "String");
        inbox_anyfield_search_map_tab_1.put("doc.document_subject", "String");
        inbox_anyfield_search_map_tab_1.put("doc.document_date", "String");
        inbox_anyfield_search_map_tab_1.put("con.name_en", "String");
        inbox_anyfield_search_map_tab_1.put("empRecords.name_eng", "String");
        inbox_anyfield_search_map_tab_1.put("a.created_at", "String");
        inbox_anyfield_search_map_tab_1.put("doc.deadline", "String");
        inbox_anyfield_search_map_tab_1.put("a.is_seen", "String");


        inbox_anyfield_search_map_tab_2.put("a.ID", "String");
        inbox_anyfield_search_map_tab_2.put("lang.languageTextEnglish", "String");
        inbox_anyfield_search_map_tab_2.put("doc.document_no", "String");
        inbox_anyfield_search_map_tab_2.put("idx.name_en", "String");
        inbox_anyfield_search_map_tab_2.put("doc.document_subject", "String");
        inbox_anyfield_search_map_tab_2.put("doc.document_date", "String");
        inbox_anyfield_search_map_tab_2.put("con.name_en", "String");
        inbox_anyfield_search_map_tab_2.put("empRecords.name_eng", "String");
        inbox_anyfield_search_map_tab_2.put("a.created_at", "String");
        inbox_anyfield_search_map_tab_2.put("doc.deadline", "String");
        inbox_anyfield_search_map_tab_2.put("a.is_seen", "String");



        inbox_anyfield_search_map.put(1, inbox_anyfield_search_map_tab_1);
        inbox_anyfield_search_map.put(2, inbox_anyfield_search_map_tab_2);

        java_allfield_type_map.put("submission_date".toLowerCase(), "Long");
        java_allfield_type_map.put("message_type".toLowerCase(), "Boolean");
        java_allfield_type_map.put("current_status".toLowerCase(), "String");
        java_allfield_type_map.put("subject".toLowerCase(), "String");
        java_allfield_type_map.put("details".toLowerCase(), "String");
        java_allfield_type_map.put("tracking_number".toLowerCase(), "String");
        java_allfield_type_map.put("inbox_originator_id".toLowerCase(), "Long");
        java_allfield_type_map.put("user_id".toLowerCase(), "Long");
        java_allfield_type_map.put("organogram_id".toLowerCase(), "Long");
        java_allfield_type_map.put("task_decision".toLowerCase(), "String");
        java_allfield_type_map.put("task_comments".toLowerCase(), "String");
        java_allfield_type_map.put("created_at".toLowerCase(), "Long");
        java_allfield_type_map.put("modified_at".toLowerCase(), "Long");
        java_allfield_type_map.put("created_by".toLowerCase(), "Integer");
        java_allfield_type_map.put("modified_by".toLowerCase(), "Integer");
        java_allfield_type_map.put("status".toLowerCase(), "Boolean");

        java_anyfield_search_map.put(tableName + ".submission_date".toLowerCase(), "Long");
        java_anyfield_search_map.put("message.name_en".toLowerCase(), "String");
        java_anyfield_search_map.put("message.name_bn".toLowerCase(), "String");
        java_anyfield_search_map.put(tableName + ".current_status".toLowerCase(), "String");
        java_anyfield_search_map.put(tableName + ".subject".toLowerCase(), "String");
        java_anyfield_search_map.put(tableName + ".details".toLowerCase(), "String");
        java_anyfield_search_map.put(tableName + ".tracking_number".toLowerCase(), "String");
        java_anyfield_search_map.put(tableName + ".inbox_originator_id".toLowerCase(), "Long");
        java_anyfield_search_map.put(tableName + ".user_id".toLowerCase(), "Long");
        java_anyfield_search_map.put(tableName + ".organogram_id".toLowerCase(), "Long");
        java_anyfield_search_map.put(tableName + ".task_decision".toLowerCase(), "String");
        java_anyfield_search_map.put(tableName + ".task_comments".toLowerCase(), "String");
        java_anyfield_search_map.put(tableName + ".created_at".toLowerCase(), "Long");
        java_anyfield_search_map.put(tableName + ".modified_at".toLowerCase(), "Long");
        java_anyfield_search_map.put(tableName + ".created_by".toLowerCase(), "Integer");
        java_anyfield_search_map.put(tableName + ".modified_by".toLowerCase(), "Integer");
        java_anyfield_search_map.put(tableName + ".status".toLowerCase(), "Boolean");

        java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
        java_DTO_map.put("submissionDate".toLowerCase(), "submissionDate".toLowerCase());
        java_DTO_map.put("messageType".toLowerCase(), "messageType".toLowerCase());
        java_DTO_map.put("currentStatus".toLowerCase(), "currentStatus".toLowerCase());
        java_DTO_map.put("subject".toLowerCase(), "subject".toLowerCase());
        java_DTO_map.put("details".toLowerCase(), "details".toLowerCase());
        java_DTO_map.put("trackingNumber".toLowerCase(), "trackingNumber".toLowerCase());
        java_DTO_map.put("inboxOriginatorId".toLowerCase(), "inboxOriginatorId".toLowerCase());
        java_DTO_map.put("userId".toLowerCase(), "userId".toLowerCase());
        java_DTO_map.put("organogramId".toLowerCase(), "organogramId".toLowerCase());
        java_DTO_map.put("taskDecision".toLowerCase(), "taskDecision".toLowerCase());
        java_DTO_map.put("taskComments".toLowerCase(), "taskComments".toLowerCase());
        java_DTO_map.put("createdAt".toLowerCase(), "createdAt".toLowerCase());
        java_DTO_map.put("modifiedAt".toLowerCase(), "modifiedAt".toLowerCase());
        java_DTO_map.put("createdBy".toLowerCase(), "createdBy".toLowerCase());
        java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
        java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
        java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

        java_SQL_map.put("submission_date".toLowerCase(), "submissionDate".toLowerCase());
        java_SQL_map.put("message_type".toLowerCase(), "messageType".toLowerCase());
        java_SQL_map.put("current_status".toLowerCase(), "currentStatus".toLowerCase());
        java_SQL_map.put("subject".toLowerCase(), "subject".toLowerCase());
        java_SQL_map.put("details".toLowerCase(), "details".toLowerCase());
        java_SQL_map.put("tracking_number".toLowerCase(), "trackingNumber".toLowerCase());
        java_SQL_map.put("inbox_originator_id".toLowerCase(), "inboxOriginatorId".toLowerCase());
        java_SQL_map.put("user_id".toLowerCase(), "userId".toLowerCase());
        java_SQL_map.put("organogram_id".toLowerCase(), "organogramId".toLowerCase());
        java_SQL_map.put("task_decision".toLowerCase(), "taskDecision".toLowerCase());
        java_SQL_map.put("task_comments".toLowerCase(), "taskComments".toLowerCase());
        java_SQL_map.put("created_at".toLowerCase(), "createdAt".toLowerCase());
        java_SQL_map.put("modified_at".toLowerCase(), "modifiedAt".toLowerCase());
        java_SQL_map.put("created_by".toLowerCase(), "createdBy".toLowerCase());
        java_SQL_map.put("modified_by".toLowerCase(), "modifiedBy".toLowerCase());
        java_SQL_map.put("status".toLowerCase(), "status".toLowerCase());

        java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
        java_Text_map.put("Submission Date".toLowerCase(), "submissionDate".toLowerCase());
        java_Text_map.put("Message".toLowerCase(), "messageType".toLowerCase());
        java_Text_map.put("Current Status".toLowerCase(), "currentStatus".toLowerCase());
        java_Text_map.put("Subject".toLowerCase(), "subject".toLowerCase());
        java_Text_map.put("Details".toLowerCase(), "details".toLowerCase());
        java_Text_map.put("Tracking Number".toLowerCase(), "trackingNumber".toLowerCase());
        java_Text_map.put("Inbox Originator Id".toLowerCase(), "inboxOriginatorId".toLowerCase());
        java_Text_map.put("User Id".toLowerCase(), "userId".toLowerCase());
        java_Text_map.put("Organogram Id".toLowerCase(), "organogramId".toLowerCase());
        java_Text_map.put("Task Decision".toLowerCase(), "taskDecision".toLowerCase());
        java_Text_map.put("Task Comments".toLowerCase(), "taskComments".toLowerCase());
        java_Text_map.put("Created At".toLowerCase(), "createdAt".toLowerCase());
        java_Text_map.put("Modified At".toLowerCase(), "modifiedAt".toLowerCase());
        java_Text_map.put("Created By".toLowerCase(), "createdBy".toLowerCase());
        java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
        java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
        java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

    }

    public static InboxMAPS GetInstance()
    {
        if(self == null)
        {
            self = new InboxMAPS("");
        }
        return self;
    }

}