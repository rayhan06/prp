package inbox;
import java.util.*; 
import util.*; 


public class InboxDTO extends CommonDTO
{

	public long submissionDate = 0;
	public int messageType = 0;
    public int currentStatus = 0;
    public String subject = "";
    public String details = "";
    public String trackingNumber = "";
	public long inboxOriginatorId = 0;
	public long userId = 0;
	public long organogramId = 0;
    public String taskDecision = "";
    public String taskComments = "";
	public long createdAt = 0;
	//public long modifiedAt = 0;
	public long createdBy = 0;
	//public int modifiedBy = 0;
	public boolean status = false;
	
	
    @Override
	public String toString() {
            return "$InboxDTO[" +
            " id = " + iD +
            " submissionDate = " + submissionDate +
            " messageType = " + messageType +
            " currentStatus = " + currentStatus +
            " subject = " + subject +
            " details = " + details +
            " trackingNumber = " + trackingNumber +
            " inboxOriginatorId = " + inboxOriginatorId +
            " userId = " + userId +
            " organogramId = " + organogramId +
            " taskDecision = " + taskDecision +
            " taskComments = " + taskComments +
            " createdAt = " + createdAt +
           // " modifiedAt = " + modifiedAt +
            " createdBy = " + createdBy +
          //  " modifiedBy = " + modifiedBy +
            " status = " + status +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}