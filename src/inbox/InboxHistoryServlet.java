package inbox;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import inbox_movements.Inbox_movementsDAO;
import inbox_movements.Inbox_movementsDTO;
import login.LoginDTO;
import permission.MenuConstants;
import project_tracker.FilesDAO;
import project_tracker.FilesDTO;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

/**
 * Servlet implementation class InboxHistoryServlet
 */
@WebServlet("/InboxHistoryServlet")
public class InboxHistoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InboxHistoryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		
	
		
		try
		{
			String actionType = request.getParameter("actionType");
			
			String tempInboxId = request.getParameter("id");
			
			//String rowId = request.getParameter("row_id");
			
			
			
			long tempInboxIdLong = Long.parseLong(tempInboxId);
			
			//long rowIdLong = Long.parseLong(rowId);
			
			
			if(actionType.equals("getHistory"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_HISTORY))
				{
					getHistory(request, response, tempInboxIdLong);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
		}
		catch(Exception ex)
		{
			
		}
	}
	
	private void getHistory(HttpServletRequest request, HttpServletResponse response, long rowId)
	{
		try
		{
			
			Inbox_movementsDAO tempMovementDao = new Inbox_movementsDAO();
			
			List<Inbox_movementsDTO> tempDTOList = tempMovementDao.getDTOsByInboxID(rowId);
			
			for(int i=0; i < tempDTOList.size(); i++)
			{
				
				FilesDAO tempDao = new FilesDAO();
				
				 List<FilesDTO> tempList = tempDao.getAllFiles("inbox_movements",tempDTOList.get(i).ID);
				 
				 tempDTOList.get(i).files = tempList;
				 
				 if(i==0)
					{
					 InboxDAO tempInboxDAO = new InboxDAO();
					 
					 InboxDTO tempInboxDto = (InboxDTO) tempInboxDAO.getDTOByID(rowId);
					 
					 if(tempInboxDto.inboxOriginatorId > 0)
					 {
						 List<FilesDTO> tempList2 = tempDao.getAllFiles("applicant_profile",tempInboxDto.inboxOriginatorId);
						 
						 for(int k=0; k < tempList2.size(); k++)
						 {
							 tempDTOList.get(i).files.add(tempList2.get(k));
						 }
					 }
					 
						
					}
			}
			
			
			
			
			
			
		//	InboxDAO tempDao = new InboxDAO();
			
			
		//	InboxDTO tempDto2 = (InboxDTO) tempDao.getDTOByID(tempDTO.inboxId);
			
			
			request.setAttribute("data",tempDTOList);
		request.setAttribute("ID", -1L);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("inbox/inboxHistory.jsp");
		requestDispatcher.forward(request, response);
		}
		catch(Exception ex)
		{
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
