package inbox;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class InboxRepository implements Repository {
	InboxDAO inboxDAO = null;
	
	public void setDAO(InboxDAO inboxDAO)
	{
		this.inboxDAO = inboxDAO;
	}
	
	
	static Logger logger = Logger.getLogger(InboxRepository.class);
	Map<Long, InboxDTO>mapOfInboxDTOToid;
	Map<Long, Set<InboxDTO> >mapOfInboxDTOTosubmissionDate;
	Map<Integer, Set<InboxDTO> >mapOfInboxDTOTomessageType;
	Map<Integer, Set<InboxDTO> >mapOfInboxDTOTocurrentStatus;
	Map<String, Set<InboxDTO> >mapOfInboxDTOTosubject;
	Map<String, Set<InboxDTO> >mapOfInboxDTOTodetails;
	Map<String, Set<InboxDTO> >mapOfInboxDTOTotrackingNumber;
	Map<Long, Set<InboxDTO> >mapOfInboxDTOToinboxOriginatorId;
	Map<Long, Set<InboxDTO> >mapOfInboxDTOTouserId;
	Map<Long, Set<InboxDTO> >mapOfInboxDTOToorganogramId;
	Map<String, Set<InboxDTO> >mapOfInboxDTOTotaskDecision;
	Map<String, Set<InboxDTO> >mapOfInboxDTOTotaskComments;
	Map<Long, Set<InboxDTO> >mapOfInboxDTOTocreatedAt;
	Map<Long, Set<InboxDTO> >mapOfInboxDTOTomodifiedAt;
	Map<Long, Set<InboxDTO> >mapOfInboxDTOTocreatedBy;
	Map<Integer, Set<InboxDTO> >mapOfInboxDTOTomodifiedBy;
	Map<Boolean, Set<InboxDTO> >mapOfInboxDTOTostatus;
	Map<Long, Set<InboxDTO> >mapOfInboxDTOTolastModificationTime;


	static InboxRepository instance = null;  
	private InboxRepository(){
		mapOfInboxDTOToid = new ConcurrentHashMap<>();
		mapOfInboxDTOTosubmissionDate = new ConcurrentHashMap<>();
		mapOfInboxDTOTomessageType = new ConcurrentHashMap<>();
		mapOfInboxDTOTocurrentStatus = new ConcurrentHashMap<>();
		mapOfInboxDTOTosubject = new ConcurrentHashMap<>();
		mapOfInboxDTOTodetails = new ConcurrentHashMap<>();
		mapOfInboxDTOTotrackingNumber = new ConcurrentHashMap<>();
		mapOfInboxDTOToinboxOriginatorId = new ConcurrentHashMap<>();
		mapOfInboxDTOTouserId = new ConcurrentHashMap<>();
		mapOfInboxDTOToorganogramId = new ConcurrentHashMap<>();
		mapOfInboxDTOTotaskDecision = new ConcurrentHashMap<>();
		mapOfInboxDTOTotaskComments = new ConcurrentHashMap<>();
		mapOfInboxDTOTocreatedAt = new ConcurrentHashMap<>();
		mapOfInboxDTOTomodifiedAt = new ConcurrentHashMap<>();
		mapOfInboxDTOTocreatedBy = new ConcurrentHashMap<>();
		mapOfInboxDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfInboxDTOTostatus = new ConcurrentHashMap<>();
		mapOfInboxDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static InboxRepository getInstance(){
		if (instance == null){
			instance = new InboxRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(inboxDAO == null)
		{
			return;
		}
		try {
			List<InboxDTO> inboxDTOs = inboxDAO.getAllInbox(reloadAll);
			for(InboxDTO inboxDTO : inboxDTOs) {
				InboxDTO oldInboxDTO = getInboxDTOByid(inboxDTO.iD);
				if( oldInboxDTO != null ) {
					mapOfInboxDTOToid.remove(oldInboxDTO.iD);
				
					if(mapOfInboxDTOTosubmissionDate.containsKey(oldInboxDTO.submissionDate)) {
						mapOfInboxDTOTosubmissionDate.get(oldInboxDTO.submissionDate).remove(oldInboxDTO);
					}
					if(mapOfInboxDTOTosubmissionDate.get(oldInboxDTO.submissionDate).isEmpty()) {
						mapOfInboxDTOTosubmissionDate.remove(oldInboxDTO.submissionDate);
					}
					
					if(mapOfInboxDTOTomessageType.containsKey(oldInboxDTO.messageType)) {
						mapOfInboxDTOTomessageType.get(oldInboxDTO.messageType).remove(oldInboxDTO);
					}
					if(mapOfInboxDTOTomessageType.get(oldInboxDTO.messageType).isEmpty()) {
						mapOfInboxDTOTomessageType.remove(oldInboxDTO.messageType);
					}
					
					if(mapOfInboxDTOTocurrentStatus.containsKey(oldInboxDTO.currentStatus)) {
						mapOfInboxDTOTocurrentStatus.get(oldInboxDTO.currentStatus).remove(oldInboxDTO);
					}
					if(mapOfInboxDTOTocurrentStatus.get(oldInboxDTO.currentStatus).isEmpty()) {
						mapOfInboxDTOTocurrentStatus.remove(oldInboxDTO.currentStatus);
					}
					
					if(mapOfInboxDTOTosubject.containsKey(oldInboxDTO.subject)) {
						mapOfInboxDTOTosubject.get(oldInboxDTO.subject).remove(oldInboxDTO);
					}
					if(mapOfInboxDTOTosubject.get(oldInboxDTO.subject).isEmpty()) {
						mapOfInboxDTOTosubject.remove(oldInboxDTO.subject);
					}
					
					if(mapOfInboxDTOTodetails.containsKey(oldInboxDTO.details)) {
						mapOfInboxDTOTodetails.get(oldInboxDTO.details).remove(oldInboxDTO);
					}
					if(mapOfInboxDTOTodetails.get(oldInboxDTO.details).isEmpty()) {
						mapOfInboxDTOTodetails.remove(oldInboxDTO.details);
					}
					
					if(mapOfInboxDTOTotrackingNumber.containsKey(oldInboxDTO.trackingNumber)) {
						mapOfInboxDTOTotrackingNumber.get(oldInboxDTO.trackingNumber).remove(oldInboxDTO);
					}
					if(mapOfInboxDTOTotrackingNumber.get(oldInboxDTO.trackingNumber).isEmpty()) {
						mapOfInboxDTOTotrackingNumber.remove(oldInboxDTO.trackingNumber);
					}
					
					if(mapOfInboxDTOToinboxOriginatorId.containsKey(oldInboxDTO.inboxOriginatorId)) {
						mapOfInboxDTOToinboxOriginatorId.get(oldInboxDTO.inboxOriginatorId).remove(oldInboxDTO);
					}
					if(mapOfInboxDTOToinboxOriginatorId.get(oldInboxDTO.inboxOriginatorId).isEmpty()) {
						mapOfInboxDTOToinboxOriginatorId.remove(oldInboxDTO.inboxOriginatorId);
					}
					
					if(mapOfInboxDTOTouserId.containsKey(oldInboxDTO.userId)) {
						mapOfInboxDTOTouserId.get(oldInboxDTO.userId).remove(oldInboxDTO);
					}
					if(mapOfInboxDTOTouserId.get(oldInboxDTO.userId).isEmpty()) {
						mapOfInboxDTOTouserId.remove(oldInboxDTO.userId);
					}
					
					if(mapOfInboxDTOToorganogramId.containsKey(oldInboxDTO.organogramId)) {
						mapOfInboxDTOToorganogramId.get(oldInboxDTO.organogramId).remove(oldInboxDTO);
					}
					if(mapOfInboxDTOToorganogramId.get(oldInboxDTO.organogramId).isEmpty()) {
						mapOfInboxDTOToorganogramId.remove(oldInboxDTO.organogramId);
					}
					
					if(mapOfInboxDTOTotaskDecision.containsKey(oldInboxDTO.taskDecision)) {
						mapOfInboxDTOTotaskDecision.get(oldInboxDTO.taskDecision).remove(oldInboxDTO);
					}
					if(mapOfInboxDTOTotaskDecision.get(oldInboxDTO.taskDecision).isEmpty()) {
						mapOfInboxDTOTotaskDecision.remove(oldInboxDTO.taskDecision);
					}
					
					if(mapOfInboxDTOTotaskComments.containsKey(oldInboxDTO.taskComments)) {
						mapOfInboxDTOTotaskComments.get(oldInboxDTO.taskComments).remove(oldInboxDTO);
					}
					if(mapOfInboxDTOTotaskComments.get(oldInboxDTO.taskComments).isEmpty()) {
						mapOfInboxDTOTotaskComments.remove(oldInboxDTO.taskComments);
					}
					
					if(mapOfInboxDTOTocreatedAt.containsKey(oldInboxDTO.createdAt)) {
						mapOfInboxDTOTocreatedAt.get(oldInboxDTO.createdAt).remove(oldInboxDTO);
					}
					if(mapOfInboxDTOTocreatedAt.get(oldInboxDTO.createdAt).isEmpty()) {
						mapOfInboxDTOTocreatedAt.remove(oldInboxDTO.createdAt);
					}
					
//					if(mapOfInboxDTOTomodifiedAt.containsKey(oldInboxDTO.modifiedAt)) {
//						mapOfInboxDTOTomodifiedAt.get(oldInboxDTO.modifiedAt).remove(oldInboxDTO);
//					}
//					if(mapOfInboxDTOTomodifiedAt.get(oldInboxDTO.modifiedAt).isEmpty()) {
//						mapOfInboxDTOTomodifiedAt.remove(oldInboxDTO.modifiedAt);
//					}
					
					if(mapOfInboxDTOTocreatedBy.containsKey(oldInboxDTO.createdBy)) {
						mapOfInboxDTOTocreatedBy.get(oldInboxDTO.createdBy).remove(oldInboxDTO);
					}
					if(mapOfInboxDTOTocreatedBy.get(oldInboxDTO.createdBy).isEmpty()) {
						mapOfInboxDTOTocreatedBy.remove(oldInboxDTO.createdBy);
					}
					
//					if(mapOfInboxDTOTomodifiedBy.containsKey(oldInboxDTO.modifiedBy)) {
//						mapOfInboxDTOTomodifiedBy.get(oldInboxDTO.modifiedBy).remove(oldInboxDTO);
//					}
//					if(mapOfInboxDTOTomodifiedBy.get(oldInboxDTO.modifiedBy).isEmpty()) {
//						mapOfInboxDTOTomodifiedBy.remove(oldInboxDTO.modifiedBy);
//					}
					
					if(mapOfInboxDTOTostatus.containsKey(oldInboxDTO.status)) {
						mapOfInboxDTOTostatus.get(oldInboxDTO.status).remove(oldInboxDTO);
					}
					if(mapOfInboxDTOTostatus.get(oldInboxDTO.status).isEmpty()) {
						mapOfInboxDTOTostatus.remove(oldInboxDTO.status);
					}
					
					if(mapOfInboxDTOTolastModificationTime.containsKey(oldInboxDTO.lastModificationTime)) {
						mapOfInboxDTOTolastModificationTime.get(oldInboxDTO.lastModificationTime).remove(oldInboxDTO);
					}
					if(mapOfInboxDTOTolastModificationTime.get(oldInboxDTO.lastModificationTime).isEmpty()) {
						mapOfInboxDTOTolastModificationTime.remove(oldInboxDTO.lastModificationTime);
					}
					
					
				}
				if(inboxDTO.isDeleted == 0) 
				{
					
					mapOfInboxDTOToid.put(inboxDTO.iD, inboxDTO);
				
					if( ! mapOfInboxDTOTosubmissionDate.containsKey(inboxDTO.submissionDate)) {
						mapOfInboxDTOTosubmissionDate.put(inboxDTO.submissionDate, new HashSet<>());
					}
					mapOfInboxDTOTosubmissionDate.get(inboxDTO.submissionDate).add(inboxDTO);
					
					if( ! mapOfInboxDTOTomessageType.containsKey(inboxDTO.messageType)) {
						mapOfInboxDTOTomessageType.put(inboxDTO.messageType, new HashSet<>());
					}
					mapOfInboxDTOTomessageType.get(inboxDTO.messageType).add(inboxDTO);
					
					if( ! mapOfInboxDTOTocurrentStatus.containsKey(inboxDTO.currentStatus)) {
						mapOfInboxDTOTocurrentStatus.put(inboxDTO.currentStatus, new HashSet<>());
					}
					mapOfInboxDTOTocurrentStatus.get(inboxDTO.currentStatus).add(inboxDTO);
					
					if( ! mapOfInboxDTOTosubject.containsKey(inboxDTO.subject)) {
						mapOfInboxDTOTosubject.put(inboxDTO.subject, new HashSet<>());
					}
					mapOfInboxDTOTosubject.get(inboxDTO.subject).add(inboxDTO);
					
					if( ! mapOfInboxDTOTodetails.containsKey(inboxDTO.details)) {
						mapOfInboxDTOTodetails.put(inboxDTO.details, new HashSet<>());
					}
					mapOfInboxDTOTodetails.get(inboxDTO.details).add(inboxDTO);
					
					if( ! mapOfInboxDTOTotrackingNumber.containsKey(inboxDTO.trackingNumber)) {
						mapOfInboxDTOTotrackingNumber.put(inboxDTO.trackingNumber, new HashSet<>());
					}
					mapOfInboxDTOTotrackingNumber.get(inboxDTO.trackingNumber).add(inboxDTO);
					
					if( ! mapOfInboxDTOToinboxOriginatorId.containsKey(inboxDTO.inboxOriginatorId)) {
						mapOfInboxDTOToinboxOriginatorId.put(inboxDTO.inboxOriginatorId, new HashSet<>());
					}
					mapOfInboxDTOToinboxOriginatorId.get(inboxDTO.inboxOriginatorId).add(inboxDTO);
					
					if( ! mapOfInboxDTOTouserId.containsKey(inboxDTO.userId)) {
						mapOfInboxDTOTouserId.put(inboxDTO.userId, new HashSet<>());
					}
					mapOfInboxDTOTouserId.get(inboxDTO.userId).add(inboxDTO);
					
					if( ! mapOfInboxDTOToorganogramId.containsKey(inboxDTO.organogramId)) {
						mapOfInboxDTOToorganogramId.put(inboxDTO.organogramId, new HashSet<>());
					}
					mapOfInboxDTOToorganogramId.get(inboxDTO.organogramId).add(inboxDTO);
					
					if( ! mapOfInboxDTOTotaskDecision.containsKey(inboxDTO.taskDecision)) {
						mapOfInboxDTOTotaskDecision.put(inboxDTO.taskDecision, new HashSet<>());
					}
					mapOfInboxDTOTotaskDecision.get(inboxDTO.taskDecision).add(inboxDTO);
					
					if( ! mapOfInboxDTOTotaskComments.containsKey(inboxDTO.taskComments)) {
						mapOfInboxDTOTotaskComments.put(inboxDTO.taskComments, new HashSet<>());
					}
					mapOfInboxDTOTotaskComments.get(inboxDTO.taskComments).add(inboxDTO);
					
					if( ! mapOfInboxDTOTocreatedAt.containsKey(inboxDTO.createdAt)) {
						mapOfInboxDTOTocreatedAt.put(inboxDTO.createdAt, new HashSet<>());
					}
					mapOfInboxDTOTocreatedAt.get(inboxDTO.createdAt).add(inboxDTO);
					
//					if( ! mapOfInboxDTOTomodifiedAt.containsKey(inboxDTO.modifiedAt)) {
//						mapOfInboxDTOTomodifiedAt.put(inboxDTO.modifiedAt, new HashSet<>());
//					}
//					mapOfInboxDTOTomodifiedAt.get(inboxDTO.modifiedAt).add(inboxDTO);
					
					if( ! mapOfInboxDTOTocreatedBy.containsKey(inboxDTO.createdBy)) {
						mapOfInboxDTOTocreatedBy.put(inboxDTO.createdBy, new HashSet<>());
					}
					mapOfInboxDTOTocreatedBy.get(inboxDTO.createdBy).add(inboxDTO);
					
//					if( ! mapOfInboxDTOTomodifiedBy.containsKey(inboxDTO.modifiedBy)) {
//						mapOfInboxDTOTomodifiedBy.put(inboxDTO.modifiedBy, new HashSet<>());
//					}
//					mapOfInboxDTOTomodifiedBy.get(inboxDTO.modifiedBy).add(inboxDTO);
					
					if( ! mapOfInboxDTOTostatus.containsKey(inboxDTO.status)) {
						mapOfInboxDTOTostatus.put(inboxDTO.status, new HashSet<>());
					}
					mapOfInboxDTOTostatus.get(inboxDTO.status).add(inboxDTO);
					
					if( ! mapOfInboxDTOTolastModificationTime.containsKey(inboxDTO.lastModificationTime)) {
						mapOfInboxDTOTolastModificationTime.put(inboxDTO.lastModificationTime, new HashSet<>());
					}
					mapOfInboxDTOTolastModificationTime.get(inboxDTO.lastModificationTime).add(inboxDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<InboxDTO> getInboxList() {
		List <InboxDTO> inboxs = new ArrayList<InboxDTO>(this.mapOfInboxDTOToid.values());
		return inboxs;
	}
	
	
	public InboxDTO getInboxDTOByid( long id){
		return mapOfInboxDTOToid.get(id);
	}
	
	
	public List<InboxDTO> getInboxDTOBysubmission_date(long submission_date) {
		return new ArrayList<>( mapOfInboxDTOTosubmissionDate.getOrDefault(submission_date,new HashSet<>()));
	}
	
	
	public List<InboxDTO> getInboxDTOBymessage_type(boolean message_type) {
		return new ArrayList<>( mapOfInboxDTOTomessageType.getOrDefault(message_type,new HashSet<>()));
	}
	
	
	public List<InboxDTO> getInboxDTOBycurrent_status(String current_status) {
		return new ArrayList<>( mapOfInboxDTOTocurrentStatus.getOrDefault(current_status,new HashSet<>()));
	}
	
	
	public List<InboxDTO> getInboxDTOBysubject(String subject) {
		return new ArrayList<>( mapOfInboxDTOTosubject.getOrDefault(subject,new HashSet<>()));
	}
	
	
	public List<InboxDTO> getInboxDTOBydetails(String details) {
		return new ArrayList<>( mapOfInboxDTOTodetails.getOrDefault(details,new HashSet<>()));
	}
	
	
	public List<InboxDTO> getInboxDTOBytracking_number(String tracking_number) {
		return new ArrayList<>( mapOfInboxDTOTotrackingNumber.getOrDefault(tracking_number,new HashSet<>()));
	}
	
	
	public List<InboxDTO> getInboxDTOByinbox_originator_id(long inbox_originator_id) {
		return new ArrayList<>( mapOfInboxDTOToinboxOriginatorId.getOrDefault(inbox_originator_id,new HashSet<>()));
	}
	
	
	public List<InboxDTO> getInboxDTOByuser_id(long user_id) {
		return new ArrayList<>( mapOfInboxDTOTouserId.getOrDefault(user_id,new HashSet<>()));
	}
	
	
	public List<InboxDTO> getInboxDTOByorganogram_id(long organogram_id) {
		return new ArrayList<>( mapOfInboxDTOToorganogramId.getOrDefault(organogram_id,new HashSet<>()));
	}
	
	
	public List<InboxDTO> getInboxDTOBytask_decision(String task_decision) {
		return new ArrayList<>( mapOfInboxDTOTotaskDecision.getOrDefault(task_decision,new HashSet<>()));
	}
	
	
	public List<InboxDTO> getInboxDTOBytask_comments(String task_comments) {
		return new ArrayList<>( mapOfInboxDTOTotaskComments.getOrDefault(task_comments,new HashSet<>()));
	}
	
	
	public List<InboxDTO> getInboxDTOBycreated_at(long created_at) {
		return new ArrayList<>( mapOfInboxDTOTocreatedAt.getOrDefault(created_at,new HashSet<>()));
	}
	
	
	public List<InboxDTO> getInboxDTOBymodified_at(long modified_at) {
		return new ArrayList<>( mapOfInboxDTOTomodifiedAt.getOrDefault(modified_at,new HashSet<>()));
	}
	
	
	public List<InboxDTO> getInboxDTOBycreated_by(int created_by) {
		return new ArrayList<>( mapOfInboxDTOTocreatedBy.getOrDefault(created_by,new HashSet<>()));
	}
	
	
	public List<InboxDTO> getInboxDTOBymodified_by(int modified_by) {
		return new ArrayList<>( mapOfInboxDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<InboxDTO> getInboxDTOBystatus(boolean status) {
		return new ArrayList<>( mapOfInboxDTOTostatus.getOrDefault(status,new HashSet<>()));
	}
	
	
	public List<InboxDTO> getInboxDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfInboxDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "inbox";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


