ALTER TABLE `inbox_tab`
	ADD COLUMN `unseen_count_query` TEXT NULL AFTER `query_search`;

UPDATE `inbox_tab`
  SET `unseen_count_query`='select\r\n  count(*) as countId \r\nfrom\r\n  inbox b\r\n\r\n  inner join inbox_movements a on a.inbox_id = b.ID\r\n  INNER JOIN edms_documents doc on  doc.ID = b.inbox_originator_id\r\n  inner JOIN file_index_type idx on  doc.file_index_type_type = idx.ID\r\n  inner join pbrlp_contractor con on  doc.pbrlp_contractor_type = con.ID\r\n\r\n  inner join category cat on ( doc.document_type_cat = cat.value  and cat.domain_name = \'document_type\' )\r\n  inner join language_text lang on lang.ID = cat.language_id\r\n\r\n  left join employee_offices empOffices on empOffices.office_unit_organogram_id = a.from_organogram_id\r\n  left join employee_records empRecords on empRecords.id = empOffices.employee_record_id\r\n\r\nwhere\r\n  is_send = 0\r\n  and to_organogram_id = ORGANOGRAM_ID \r\n  and a.action_taken = 0\r\n  and a.is_seen = 0\r\n\r\nORDER BY a.lastModificationTime DESC'
  WHERE  `ID`=1;

UPDATE `inbox_tab`
 SET `unseen_count_query`='select\r\n  count(*) as countId \r\nfrom\r\n  inbox b\r\n\r\n  inner join inbox_movements a on a.inbox_id = b.ID\r\n  INNER JOIN edms_documents doc on  doc.ID = b.inbox_originator_id\r\n  inner JOIN file_index_type idx on  doc.file_index_type_type = idx.ID\r\n  inner join pbrlp_contractor con on  doc.pbrlp_contractor_type = con.ID\r\n\r\n  inner join category cat on ( doc.document_type_cat = cat.value  and cat.domain_name = \'document_type\' )\r\n  inner join language_text lang on lang.ID = cat.language_id\r\n\r\n  left join employee_offices empOffices on empOffices.office_unit_organogram_id = a.to_organogram_id\r\n  left join employee_records empRecords on empRecords.id = empOffices.employee_record_id\r\n\r\nwhere\r\n  is_send = 0\r\n  and from_organogram_id = ORGANOGRAM_ID \r\n  and a.action_taken = 0\r\n  and a.is_seen = 0\r\n\r\nORDER BY a.lastModificationTime DESC'
 WHERE  `ID`=2;

UPDATE `inbox_tab`
 SET `unseen_count_query`='select count(*) as countId \r\n from inbox_movements a, inbox b   \r\n where  a.inbox_id = b.ID and to_organogram_id = ORGANOGRAM_ID and a.is_seen = 0\r\n ORDER BY a.lastModificationTime DESC'
 WHERE  `ID`=3;

UPDATE `inbox_tab`
 SET `unseen_counxt_query`='select count(*) as countId \r\nfrom inbox_movements a, inbox b   \r\nwhere  a.inbox_id = b.ID and to_organogram_id = ORGANOGRAM_ID and a.is_seen = 0 \r\nORDER BY a.lastModificationTime DESC'
 WHERE  `ID`=4;

UPDATE `inbox_tab`
 SET `unseen_count_query`='select count(*) as countId \r\nfrom inbox_movements a, inbox b   \r\nwhere  a.inbox_id = b.ID and to_organogram_id = ORGANOGRAM_ID and  a.is_seen = 0\r\nORDER BY a.lastModificationTime DESC'
 WHERE  `ID`=5;



ALTER TABLE `inbox_tab`
	ADD COLUMN `default_sort` TEXT NULL AFTER `query_search`;

UPDATE `inbox_tab`
 SET `query_tab`='select\r\n  a.ID,\r\n  lang.languageTextEnglish,\r\n  doc.document_no,\r\n  idx.name_en,\r\n  doc.document_subject,\r\n  date_format( from_unixtime( doc.document_date/1000 ), \'%d/%m/%Y\' ) as document_date,\r\n  con.name_en,\r\n  empRecords.name_eng,\r\n  date_format( from_unixtime( a.created_at/1000 ), \'%d/%m/%Y\' ) as created_at,\r\n  doc.deadline,\r\n  a.is_seen\r\nfrom\r\n  inbox b\r\n\r\n  inner join inbox_movements a on a.inbox_id = b.ID\r\n  INNER JOIN edms_documents doc on  doc.ID = b.inbox_originator_id\r\n  inner JOIN file_index_type idx on  doc.file_index_type_type = idx.ID\r\n  inner join pbrlp_contractor con on  doc.pbrlp_contractor_type = con.ID\r\n\r\n  inner join category cat on ( doc.document_type_cat = cat.value  and cat.domain_name = \'document_type\' )\r\n  inner join language_text lang on lang.ID = cat.language_id\r\n\r\n  left join employee_offices empOffices on empOffices.office_unit_organogram_id = a.from_organogram_id\r\n  left join employee_records empRecords on empRecords.id = empOffices.employee_record_id\r\n\r\nwhere\r\n  is_send = 0\r\n  and to_organogram_id = ORGANOGRAM_ID \r\n  and a.action_taken = 0\r\n\r\n'
 WHERE  `ID`=1;

UPDATE `inbox_tab` SET `default_sort`='ORDER BY a.lastModificationTime DESC' WHERE  `ID`=1;

UPDATE `inbox_tab`
 SET `query_tab`='select\r\n  a.ID,\r\n  lang.languageTextEnglish,\r\n  doc.document_no,\r\n  idx.name_en,\r\n  doc.document_subject,\r\n  date_format( from_unixtime( doc.document_date/1000 ), \'%d/%m/%Y\') as document_date,\r\n  con.name_en,\r\n  empRecords.name_eng,\r\n  date_format( from_unixtime( a.created_at/1000 ), \'%d/%m/%Y\') as created_at,\r\n  doc.deadline,\r\n  a.is_seen\r\nfrom\r\n  inbox b\r\n\r\n  inner join inbox_movements a on a.inbox_id = b.ID\r\n  INNER JOIN edms_documents doc on  doc.ID = b.inbox_originator_id\r\n  inner JOIN file_index_type idx on  doc.file_index_type_type = idx.ID\r\n  inner join pbrlp_contractor con on  doc.pbrlp_contractor_type = con.ID\r\n\r\n  inner join category cat on ( doc.document_type_cat = cat.value  and cat.domain_name = \'document_type\' )\r\n  inner join language_text lang on lang.ID = cat.language_id\r\n\r\n  left join employee_offices empOffices on empOffices.office_unit_organogram_id = a.to_organogram_id\r\n  left join employee_records empRecords on empRecords.id = empOffices.employee_record_id\r\n\r\nwhere\r\n  is_send = 0\r\n  and from_organogram_id = ORGANOGRAM_ID \r\n  and a.action_taken = 0\r\n\r\n'
 WHERE  `ID`=2;

UPDATE `inbox_tab` SET `default_sort`='ORDER BY a.lastModificationTime DESC' WHERE  `ID`=2;

UPDATE `inbox_tab`
 SET `query_tab`='select a.ID, a.from_employee_user_id, a.created_at, b.subject, a.inbox_type, a.status, a.is_seen from inbox_movements a, inbox b   where  a.inbox_id = b.ID and to_organogram_id = ORGANOGRAM_ID '
 WHERE  `ID`=3;

UPDATE `inbox_tab` SET `default_sort`=' ORDER BY a.lastModificationTime DESC' WHERE  `ID`=3;

 UPDATE `inbox_tab`
  SET `query_tab`='select a.ID, a.from_employee_user_id, a.created_at, b.subject, a.inbox_type, a.status, a.is_seen from inbox_movements a, inbox b   where  a.inbox_id = b.ID and to_organogram_id = ORGANOGRAM_ID'
  WHERE  `ID`=4;

UPDATE `inbox_tab` SET `default_sort`=' ORDER BY a.lastModificationTime DESC' WHERE  `ID`=4;

UPDATE `inbox_tab`
 SET `query_tab`='select a.ID, a.from_employee_user_id, a.created_at, b.subject, a.inbox_type, a.status, a.is_seen from inbox_movements a, inbox b   where  a.inbox_id = b.ID and to_organogram_id = ORGANOGRAM_ID'
 WHERE  `ID`=5;

UPDATE `inbox_tab` SET `default_sort`=' ORDER BY a.lastModificationTime DESC' WHERE  `ID`=5;


UPDATE `inbox_tab`
 SET `tab_table_header`='<thead>\r\n<tr>\r\n<th>ID <i class="icon-arrow-down" onclick="tableHeaderClicked(1, \'ID\', \'ASC\')"></i> <i class="icon-arrow-up" onclick="tableHeaderClicked(1, \'ID\', \'DESC\')"></i></th>\r\n<th>Document Type <i class="icon-arrow-down"  onclick="tableHeaderClicked(1, \'languageTextEnglish\', \'ASC\')"></i> <i class="icon-arrow-up"  onclick="tableHeaderClicked(1, \'languageTextEnglish\', \'DESC\')"></i></th>\r\n<th>Document No <i class="icon-arrow-down"  onclick="tableHeaderClicked(1, \'document_no\', \'ASC\')"></i> <i class="icon-arrow-up" onclick="tableHeaderClicked(1, \'document_no\', \'DESC\')"></i></th>\r\n<th>File Index <i class="icon-arrow-down" onclick="tableHeaderClicked(1, \'idx.name_en\', \'ASC\')"></i> <i class="icon-arrow-up" onclick="tableHeaderClicked(1, \'idx.name_en\', \'DESC\')"></i></th>\r\n<th>Document Subject <i class="icon-arrow-down" onclick="tableHeaderClicked(1, \'document_subject\', \'ASC\')"></i> <i class="icon-arrow-up" onclick="tableHeaderClicked(1, \'document_subject\', \'DESC\')"></i></th>\r\n<th>Document Date <i class="icon-arrow-down" onclick="tableHeaderClicked(1, \'document_date\', \'ASC\')"></i> <i class="icon-arrow-up" onclick="tableHeaderClicked(1, \'document_date\', \'DESC\')"></i></th>\r\n<th>Document Source <i class="icon-arrow-down" onclick="tableHeaderClicked(1, \'con.name_en\', \'ASC\')"></i> <i class="icon-arrow-up" onclick="tableHeaderClicked(1, \'con.name_en\', \'DESC\')"></i></th>\r\n<th>Sender <i class="icon-arrow-down" onclick="tableHeaderClicked(1, \'name_eng\', \'ASC\')"></i> <i class="icon-arrow-up" onclick="tableHeaderClicked(1, \'name_eng\', \'DESC\')"></i></th>\r\n<th>Sending Date <i class="icon-arrow-down" onclick="tableHeaderClicked(1, \'deadline\', \'ASC\')"></i> <i class="icon-arrow-up" onclick="tableHeaderClicked(1, \'deadline\', \'DESC\')"></i></th>\r\n</tr>\r\n</thead>'
 WHERE  `ID`=1;

UPDATE `inbox_tab`
 SET `tab_table_header`='<thead>\r\n<tr>\r\n<th>ID <i class="icon-arrow-down" onclick="tableHeaderClicked(1, \'ID\', \'ASC\')"></i> <i class="icon-arrow-up" onclick="tableHeaderClicked(1, \'ID\', \'DESC\')"></i></th>\r\n<th>Document Type <i class="icon-arrow-down"  onclick="tableHeaderClicked(1, \'languageTextEnglish\', \'ASC\')"></i> <i class="icon-arrow-up"  onclick="tableHeaderClicked(1, \'languageTextEnglish\', \'DESC\')"></i></th>\r\n<th>Document No <i class="icon-arrow-down"  onclick="tableHeaderClicked(1, \'document_no\', \'ASC\')"></i> <i class="icon-arrow-up" onclick="tableHeaderClicked(1, \'document_no\', \'DESC\')"></i></th>\r\n<th>File Index <i class="icon-arrow-down" onclick="tableHeaderClicked(1, \'idx.name_en\', \'ASC\')"></i> <i class="icon-arrow-up" onclick="tableHeaderClicked(1, \'idx.name_en\', \'DESC\')"></i></th>\r\n<th>Document Subject <i class="icon-arrow-down" onclick="tableHeaderClicked(1, \'document_subject\', \'ASC\')"></i> <i class="icon-arrow-up" onclick="tableHeaderClicked(1, \'document_subject\', \'DESC\')"></i></th>\r\n<th>Document Date <i class="icon-arrow-down" onclick="tableHeaderClicked(1, \'document_date\', \'ASC\')"></i> <i class="icon-arrow-up" onclick="tableHeaderClicked(1, \'document_date\', \'DESC\')"></i></th>\r\n<th>Document Source <i class="icon-arrow-down" onclick="tableHeaderClicked(1, \'con.name_en\', \'ASC\')"></i> <i class="icon-arrow-up" onclick="tableHeaderClicked(1, \'con.name_en\', \'DESC\')"></i></th>\r\n<th>Sent To <i class="icon-arrow-down" onclick="tableHeaderClicked(1, \'name_eng\', \'ASC\')"></i> <i class="icon-arrow-up" onclick="tableHeaderClicked(1, \'name_eng\', \'DESC\')"></i></th>\r\n<th>Sending Date <i class="icon-arrow-down" onclick="tableHeaderClicked(1, \'deadline\', \'ASC\')"></i> <i class="icon-arrow-up" onclick="tableHeaderClicked(1, \'deadline\', \'DESC\')"></i></th>\r\n</tr>\r\n</thead>'
 WHERE  `ID`=2;