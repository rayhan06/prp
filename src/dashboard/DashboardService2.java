package dashboard;

/*
 * @author Md. Erfan Hossain
 * @created 15/09/2022 - 9:04 PM
 * @project parliament
 */

import common.ConnectionAndStatementUtil;
import geolocation.GeoLocationRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import pb.CatRepository;
import util.IntegerWrapper;
import util.StringUtils;

import java.sql.ResultSet;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

public class DashboardService2 {

    private static final Logger logger = Logger.getLogger(DashboardService2.class);

    private static final String getSQL = "SELECT gender_cat, employment_cat, employee_class_cat, office_unit_id,permanent_division " +
            " FROM employee_offices eo " +
            " inner join employee_records er on eo.employee_record_id = er.id " +
            " WHERE eo.office_unit_id IN (%s) " +
            "  AND eo.office_id = 2294 " +
            "  AND eo.is_default_role = 1 " +
            "  AND eo.incharge_label = 'Routine responsibility' " +
            "  AND er.isDeleted = 0 " +
            "  AND eo.is_default_role = 1"+
            "  AND eo.isDeleted = 0"+
            "  AND eo.status = 1";

    private static class InnerMapping {
        Integer genderCat;
        Integer employmentCat;
        Integer employeeClassCat;
        Long officeUnitId;
        Integer divisionId;
    }

    public InnerMapping buildInnerMapping(ResultSet rs) {
        try {
            InnerMapping obj = new InnerMapping();
            obj.genderCat = rs.getInt("gender_cat");
            obj.employmentCat = rs.getInt("employment_cat");
            obj.employeeClassCat = rs.getInt("employee_class_cat");
            obj.officeUnitId = rs.getLong("office_unit_id");
            obj.divisionId = rs.getInt("permanent_division");
            return obj;
        } catch (Exception ex) {
            logger.error(ex);
            return null;
        }
    }


    private DashboardService2() {
    }

    private static class InstanceHolder {
        static final DashboardService2 INSTANCE = new DashboardService2();
    }

    public static DashboardService2 getInstance() {
        return InstanceHolder.INSTANCE;
    }

    private Map<String, Object> build(Map<Integer, Long> mapCount,
                                      long officeUnitId,
                                      boolean isLangEng,
                                      String domainName,
                                      String paramKey
    ) {
        if (mapCount.size() == 0) {
            return new HashMap<>();
        }
        Map<Integer, String> domainModelMap = CatRepository.getInstance().getCategoryLanguageModelList(domainName)
                .stream()
                .collect(toMap(e -> e.categoryValue, e -> isLangEng ? e.englishText : e.banglaText));

        IntegerWrapper ai = new IntegerWrapper(0);
        return mapCount.entrySet()
                .stream()
                .filter(e -> domainModelMap.get(e.getKey()) != null)
                .collect(toMap(e -> String.valueOf(ai.incrementAndGet()), e -> getCountAndLink(domainModelMap.get(e.getKey()), e.getValue(), paramKey, e.getKey(), officeUnitId), (e1, e2) -> e1, LinkedHashMap::new));
    }

    private <T> Map<String, Object> getCountAndLink(String title, Long count, String key, T value, Long officeUnitId,String ...extraQueryParamsWithVal) {
        Map<String, Object> countLinkMap = new HashMap<>();
        countLinkMap.put("title", title);
        countLinkMap.put("count", count);
        if (value == null) {
            countLinkMap.put("link", "");
        } else {
            String link = "Employee_office_report_Servlet?actionType=reportPage&" + key + "=" + value + "&incharge_label=Routine responsibility";
            if(extraQueryParamsWithVal.length>0){
                link = Stream.of(extraQueryParamsWithVal)
                        .collect(Collectors.joining("&",link+"&",""));
            }
            if (officeUnitId != null) {
                link += "&officeUnitId=" + officeUnitId;
            }
            countLinkMap.put("link", link);
        }
        return countLinkMap;
    }

    private Map<String, Object> buildDesignationData(Map<Long, Long> mapCount, long officeUnitId, boolean isLangEng) {
        if (mapCount.isEmpty()) {
            return new HashMap<>();
        }
        long total = mapCount.values()
                .stream()
                .mapToLong(e -> e)
                .sum();
        Map<String, Object> map = new LinkedHashMap<>();
        IntegerWrapper ai = new IntegerWrapper(0);
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitId);
        if(!officeUnitsDTO.isAbstractOffice){
            map.put(String.valueOf(ai.incrementAndGet()),
                    getCountAndLink(isLangEng ? officeUnitsDTO.unitNameEng : officeUnitsDTO.unitNameBng, mapCount.get(officeUnitId), "officeUnitId", officeUnitId, null,"onlySelectedOffice=true"));
        }
        List<Office_unitsDTO> childOfficeUnit = Office_unitsRepository.getInstance().getImmediateChilds(officeUnitId)
                .stream()
                .sorted((e1, e2) -> {
                    if (e1.iD < e2.iD) {
                        return -1;
                    } else if (e1.iD > e2.iD) {
                        return 1;
                    }
                    return 0;
                })
                .collect(Collectors.toList());

        Map<Long, Office_unitsDTO> officeMap = childOfficeUnit.stream()
                .collect(Collectors.toMap(e -> e.iD, e -> e, (e1, e2) -> e1, LinkedHashMap::new));

        Map<String, Object> childObj =
                childOfficeUnit.stream()
                        .collect(Collectors.toMap(e -> e.iD,
                                e -> Office_unitsRepository.getInstance().getDescentsOfficeUnitId(e.iD)
                                        .stream()
                                        .map(e1 -> mapCount.getOrDefault(e1, 0L))
                                        .mapToLong(e1 -> e1)
                                        .sum(), (e1, e2) -> e1, LinkedHashMap::new)

                        )
                        .entrySet()
                        .stream()
                        .filter(e -> e.getValue() > 0)
                        .collect(Collectors.toMap(e -> String.valueOf(ai.incrementAndGet()),
                                e -> getCountAndLink(isLangEng ? officeMap.get(e.getKey()).unitNameEng : officeMap.get(e.getKey()).unitNameBng, e.getValue(), "officeUnitId", e.getKey(), null),
                                (e1, e2) -> e1, LinkedHashMap::new));

        map.putAll(childObj);
        if(isLangEng){
            map.put("total",total);
        }else{
            map.put("total", StringUtils.convertToBanNumber(String.valueOf(total)));
        }
        return map;
    }

    private Map<String, Object> getDesignationData(List<InnerMapping> list, long officeUnitId, boolean isLangEng) {
        Map<Long, Long> map = list.stream()
                .collect(Collectors.groupingBy(e -> e.officeUnitId, Collectors.counting()));
        return buildDesignationData(map, officeUnitId, isLangEng);
    }

    private Map<String, Object> getGenderData(List<InnerMapping> list, long officeUnitId, boolean isLangEng) {
        Map<Integer, Long> map = list.stream()
                .collect(Collectors.groupingBy(e -> e.genderCat, Collectors.counting()));
        return build(map, officeUnitId, isLangEng, "gender", "gender");
    }

    private Map<String, Object> getClassData(List<InnerMapping> list, long officeUnitId, boolean isLangEng) {
        Map<Integer, Long> map = list.stream()
                .collect(Collectors.groupingBy(e -> e.employeeClassCat, Collectors.counting()));
        return build(map, officeUnitId, isLangEng, "employee_class", "employeeClass");
    }

    private Map<String, Object> getEmploymentData(List<InnerMapping> list, long officeUnitId, boolean isLangEng) {
        Map<Integer, Long> map = list.stream()
                .collect(Collectors.groupingBy(e -> e.employmentCat, Collectors.counting()));
        return build(map, officeUnitId, isLangEng, "employment", "employmentCat");
    }

    private Map<String, Object> getDivision(List<InnerMapping> list, long officeUnitId, boolean isLangEng) {
        IntegerWrapper ai = new IntegerWrapper(0);
        return list.stream()
                .filter(e->GeoLocationRepository.getInstance().isDivision(e.divisionId))
                .collect(Collectors.groupingBy(e -> e.divisionId, () -> new TreeMap<>(Integer::compareTo), Collectors.counting()))
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
                        e -> String.valueOf(ai.incrementAndGet()),
                        e -> getCountAndLink(GeoLocationRepository.getInstance().getText(isLangEng, e.getKey()), e.getValue(),
                                "permanent_division", e.getKey(), officeUnitId),
                        (e1, e2) -> e1, LinkedHashMap::new
                ));
    }

    public void setHRDashboardData(HrDashboardModel model, Collection<Long> ofcUnitIdList, long officeUnitId, boolean isLangEng) {
        if (ofcUnitIdList == null || ofcUnitIdList.isEmpty()) {
            setDefaultHRDashboardData(model);
            return;
        }
        String offices = ofcUnitIdList.stream()
                .filter(Objects::nonNull)
                .map(String::valueOf)
                .collect(Collectors.joining(","));

        String sql = String.format(getSQL, offices);
        List<InnerMapping> list = ConnectionAndStatementUtil.getListOfT(sql, this::buildInnerMapping);
        if (list.isEmpty()) {
            setDefaultHRDashboardData(model);
            return;
        }
        model.gender = getGenderData(list, officeUnitId, isLangEng);
        model.classResult = getClassData(list, officeUnitId, isLangEng);
        model.type = getEmploymentData(list, officeUnitId, isLangEng);
        model.designation = getDesignationData(list, officeUnitId, isLangEng);
        model.division = getDivision(list, officeUnitId, isLangEng);
    }

    private void setDefaultHRDashboardData(HrDashboardModel model) {
        model.division = model.gender = model.classResult = model.designation = model.type = new HashMap<>();
    }
}
