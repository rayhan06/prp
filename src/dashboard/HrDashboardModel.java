package dashboard;

/*
 * @author Md. Erfan Hossain
 * @created 06/07/2021 - 12:05 PM
 * @project parliament
 */

import java.util.Map;
import java.util.List;

import appointment.KeyCountDTO;
import appointment.YearMonthCount;
import category.CategoryDTO;


public class HrDashboardModel {
    public Map<String, Object> division;
    public Map<String, Object> gender;
    public Map<String, Object> type;
    public Map<String, Object> classResult;
    public Map<String, Object> designation;
    public List<YearMonthCount> joining6Count;
    public List<YearMonthCount> lpr6Count;
    public List<CategoryDTO> employeeTypes;
    public Map<Long, KeyCountDTO> wingWiseCount;
}
