package dashboard;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;

import appointment.KeyCountDTO;
import gate_pass.GatePassDashboardDataModel;
import medical_equipment_name.Medical_equipment_nameDTO;

public class DashboardDTO {
	
	public static final int NATIONAL_PARLIAMENT_INDEX = 0;
	public static final int NATIONAL_PARLIAMENT_SECRETARIAT_INDEX = 1;
	public static final int OTHER_INDEX = 2;

	public String[] last7Days = new String[8];
	public String[] last6Months = new String[7];
	
	public KeyCountDTO issuedPassCounts;
	public KeyCountDTO approvedPassCounts;
	public KeyCountDTO pendingPassCounts;
	public List<KeyCountDTO> typeWisePasses;
	public List<KeyCountDTO> userWisePasses;
	
	
	
	public long[] last7DayIssuedPasses = new long[8];
	public long[] last7DayApprovedPasses = new long[8];
	public long[] last6MonthIssuesPasses= new long[7];
	public long[] last6MonthApprovedPasses= new long[7];
	public long[] last6MonthRequestedPasses= new long[7];
	
	//for doctor
	public long patientHanDledInCurrentMonth = 0;
	public long upComingAppointments = 0;

	public long[] last7DayPatients = new long[8];
	public long[] last6MonthPatients = new long[7];
	
	public long[] last7DayAppointments = new long[8];
	public long[] last6MonthAppointments = new long[7];
	
	public List<KeyCountDTO> dentalDay;
	public List<KeyCountDTO> dentalForever;
	
	//for receptionist
	public long appointmentToday = 0;
	public long appointmentThisMonth = 0;
	
	//for xrayGuy
	public long testsToday = 0;
	public long testsPending = 0;
	public long testsDone = 0;
	public List<Medical_equipment_nameDTO> xRayPlates;
	public long[] last7DayTests1 = new long[8]; //xRay or All
	public long[] last7DayTests2 = new long[8];
	
	public long[] last7DayReports1 = new long[8]; //xRay or All
	public long[] last7DayReports2 = new long[8];
	
	
	public List<Long> physiotherapists;
	public long[][] last6MonthsTherapies = new long[7][];
	
	//for Nurse
	public long pmCount = 0;
	public long actionCount = 0;
	public List<KeyCountDTO> popularActioncCount;
	
	//for physiotherapist
	public long todaysTherapies = 0;
	
	
	public long[] therapyCount;
	public String[] therapyNames;
	
	//for ticket admin
	public long solvedTicketCount = 0;
	public long closedTicketCount = 0;
	public long openTicketCount = 0;
	public long ticketCount = 0;
	
	public static final int ASSIGNED_POS = 0;
	public static final int NOT_ASSIGNED_POS = 1;
	public static final int CONDEMNED_POS = 2;
	public static final int OTHER_POS = 3;
	
	
	
	public long assignedAccessoryCount = 0;
	public long freeAccessoryCount  = 0;
	public long accessoryCount  = 0;
	
	public long assignedLicenseCount = 0;
	public long freeLicenseCount  = 0;
	public long licenseCount  = 0;

	public long[] last7DayOpenedTickets = new long[8];
	public long[] last7DayClosedTickets = new long[8];
	public long[] last6MonthsOpenedTickets = new long[7];
	public long[] last6MonthsClosedTickets = new long[7];
	
	public int issueTypeCount;	
	public long[] typewiseCompplaintCount;
	public String[] ticketTypeEn;
	public String[] ticketTypeBn;
	public long[] ticketTypeId;
	
	public int officeUnitCount;
	public long[] unitWiseCompplaintCount;
	public String[] unitNameBn;
	public String[] unitNameEn;
	
	//for Asset Admin
	
	public long assignedAssetCount = 0;
	public long freeAssetCount = 0;
	public long condemnedAssets = 0;
	public long otherAssets = 0;
	public long assetCount = 0;
	
	public Map<Long, int[] > assetCategoryStatusCount;
	public Map<Long, Integer > assetWingCount;
	public Map<Long, int[] > keyStatusCount;
	
	
	//for inventory manager
	public double  totalCost = 100;
	public int soldCount = 7;
	
	public int criticalMedicines = 0;
	public int stockOutMedicines = 0;
	
	public int totalPatients = 0;
	public int totalDrugs = 0;
	
	public int criticalReagents = 0;
	public int stockOutReagents = 0;
	
	public long[] last7DayMedicineSold = new long[8];
	public double[] last7DayMedicineCost = new double[8];
	
	public long[] last6MonthMedicineSold = new long[7];
	public double[] last6MonthMedicineCost = new double[7];
	
	public String[] popularDrugNames = new String[10];
	public String[] profitableDrugNames = new String[10];

	public long[] popularDrugCount = new long[10];
	public double[] pritableDrugMoney = new double[10];

	//	For Gate Pass Management
	public GatePassDashboardDataModel gatePassDashboardDataModel = null;

	//for Patient
	public long[] last6MonthVisits = new long[7];
	
	//for Medical Admin
	//public long[] last7DayPatients = new long[8]; //Already exists
	public long[] typeWiseDiagnosticProcedure;
	public String[] labTestTypes;
	public ArrayList<Long> doctorWiseServedPatients;
	public ArrayList<Long> doctorWiseAppointments;
	public ArrayList<String> doctorNames;
	public ArrayList<Long> drIds;
	
	public ArrayList<Long> doctorWiseServedPatientsToday;
	public ArrayList<Long> doctorWiseAppointmentsToday;
	public ArrayList<String> doctorNamesToday;
	public ArrayList<Long> drIdsToday;
	public ArrayList<Boolean> drActiveToday;
	//public String[] popularDrugNames = new String[10]; //Already exists
	//public long[] popularDrugCount = new long[10]; //Already exists
	public long[] lastSevenDaysRequisitions = new long[8];
	public double[] lastSixMonthsDriverCost = new double[7];
	public double[] lastSixMonthsActualCost = new double[7];
	
	public List<KeyCountDTO> drugohololics;
	public List<KeyCountDTO> receptionistWiseCount;
	public List<KeyCountDTO> receptionistWiseCountToday;
	public List<KeyCountDTO> otherOfficeCountCost;

	public List<KeyCountDTO> organizationWizeCountCost;
	
	public List<KeyCountDTO> specialCostCollections;

	public DashboardDTO()
	{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM-dd");
		for(int i = 0; i <= 7; i ++)
		{
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_YEAR, -i);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
						
			last7Days[i] = simpleDateFormat.format(cal.getTime());			
		}
		ArrayUtils.reverse(last7Days);
		
		simpleDateFormat = new SimpleDateFormat("MMM");
		for(int i = 0; i <= 6; i ++)
		{
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -i);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);

			
			String startDay = simpleDateFormat.format(cal.getTime());
			last6Months[i] = startDay;
			

		}
		
		ArrayUtils.reverse(last6Months);


	}
}
