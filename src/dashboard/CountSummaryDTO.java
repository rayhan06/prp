package dashboard;

public class CountSummaryDTO {

    public Long edmsDocumentsCount;
    public Long fileIndexSubtypeCount;
    public Long usersCount;

    public CountSummaryDTO(){

        this.edmsDocumentsCount = 0L;
        this.fileIndexSubtypeCount = 0L;
        this.usersCount = 0L;
    }
}
