package dashboard;

public class WorkflowStatusDTO {

    public Long inProgressCount;
    public Long terminatedCount;
    public Long completedCount;
    public Long totalCount;

    public WorkflowStatusDTO(){

        this.inProgressCount = 0L;
        this.terminatedCount = 0L;
        this.completedCount = 0L;
        this.totalCount = 0L;
    }
}
