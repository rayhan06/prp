package dashboard;


public class RfiStatusDTO {

	public Long approvedRfiCount = 0L;
    public Long rejectedRfiCount = 0L;
    public Long otherRfiCount = 0L;
    public Long totalRfiCount = 0L;
    
    public RfiStatusDTO(DocumentTypeCatDTO documentTypeCatDTO){
    	approvedRfiCount = documentTypeCatDTO.approvedRfiCount;
    	rejectedRfiCount = documentTypeCatDTO.rejectedRfiCount;
    	otherRfiCount = documentTypeCatDTO.otherRfiCount;
    	totalRfiCount = documentTypeCatDTO.totalRfiCount;
    }
    
    public RfiStatusDTO(){
    	approvedRfiCount = 0L;
    	rejectedRfiCount = 0L;
    	otherRfiCount = 0L;
    	totalRfiCount = 0L;
    }

}
