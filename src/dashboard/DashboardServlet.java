package dashboard;

/*
 * @author Md. Erfan Hossain
 * @created 06/07/2021 - 10:53 AM
 * @project parliament
 */

import com.google.gson.Gson;
import employee_office_report.InChargeLevelEnum;
import employee_offices.EmployeeOfficeRepository;
import employee_offices.EmployeeOfficesDAO;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import employee_records.Employee_recordsServlet;
import login.LoginDTO;
import office_unit_organogram.Office_unit_organogramDAO;
import office_units.Office_unitsRepository;
import pb.CatRepository;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toSet;

@WebServlet("/DashboardServlet")
@MultipartConfig
public class DashboardServlet extends HttpServlet {
    public static Logger logger = Logger.getLogger(Employee_recordsServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            String responseStr = null;
            switch (actionType) {
                case "hr_admin":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPER_ADMIN_DASHBOARD_HR)) {
                        request.getRequestDispatcher("dashboard/super_admin_hr_admin_dashboard.jsp").forward(request, response);
                        return;
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                        return;
                    }
                case "er_admin":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPER_ADMIN_DASHBOARD_ER)) {
                        request.getRequestDispatcher("dashboard/super_admin_er_admin_dashboard.jsp").forward(request, response);
                        return;
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                        return;
                    }
                case "vm_admin":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPER_ADMIN_DASHBOARD_VM)) {
                        request.getRequestDispatcher("dashboard/super_admin_vm_admin_dashboard.jsp").forward(request, response);
                        return;
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                        return;
                    }

                case "am_admin":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPER_ADMIN_DASHBOARD_AM)) {
                        request.getRequestDispatcher("dashboard/super_admin_am_admin_dashboard.jsp").forward(request, response);
                        return;
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                        return;
                    }
                case "pi_admin":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPER_ADMIN_DASHBOARD_PI)) {
                        request.getRequestDispatcher("dashboard/super_admin_pi_admin_dashboard.jsp").forward(request, response);
                        return;
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                        return;
                    }
                case "mp_admin":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPER_ADMIN_DASHBOARD_MP)) {
                        request.getRequestDispatcher("dashboard/super_admin_mp_admin_dashboard.jsp").forward(request, response);
                        return;
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                        return;
                    }
                case "gate_pass_admin":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPER_ADMIN_DASHBOARD_GATE_PASS)) {
                        DashboardDTO dashboardDTO = new DashboardService().getDashboardDTOForGatePassManagementAdmin();
                        request.setAttribute("dashboardDTO", dashboardDTO);
                        request.getRequestDispatcher("dashboard/super_admin_gate_pass_admin_dashboard.jsp").forward(request, response);
                        return;
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                        return;
                    }
                case "medical_admin":
                    if (userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE) {
                        DashboardDTO dashboardDTO = new DashboardService().getDashboardDTOForMedicalAdmin(userDTO);
                        if (dashboardDTO == null) {
                            System.out.println("null dashboard dto in servlet");
                        }
                        request.setAttribute("dashboardDTO", dashboardDTO);
                        request.setAttribute("dashboardPath", "../dashboard/medical_admin_dashboard.jsp");
                        request.getRequestDispatcher("dashboard/CommonDashboard.jsp").forward(request, response);
                        break;
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                        return;
                    }
                case "medical_inventory":
                    if (userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.INVENTORY_MANGER_ROLE) {
                        DashboardDTO dashboardDTO = new DashboardService().getDashboardDTOForInventoryManager(userDTO);
                        if (dashboardDTO == null) {
                            System.out.println("null dashboard dto in servlet");
                        }
                        request.setAttribute("dashboardDTO", dashboardDTO);
                        request.setAttribute("dashboardPath", "../dashboard/inventory_dashboard.jsp");
                        request.getRequestDispatcher("dashboard/CommonDashboard.jsp").forward(request, response);
                        break;
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                        return;
                    }
                case "ticket_admin":
                    if (userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE) {
                        DashboardDTO dashboardDTO = new DashboardService().getDashboardDTOForTicketAdmin(userDTO);
                        if (dashboardDTO == null) {
                            System.out.println("null dashboard dto in servlet");
                        }
                        request.setAttribute("dashboardDTO", dashboardDTO);
                        request.setAttribute("dashboardPath", "../dashboard/ticketDashboard.jsp");
                        request.getRequestDispatcher("dashboard/CommonDashboard.jsp").forward(request, response);
                        break;
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                        return;
                    }
                case "asset_admin":
                    if (userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE) {
                        DashboardDTO dashboardDTO = new DashboardService().getDashboardDTOForTicketAdminForAsset(userDTO);
                        if (dashboardDTO == null) {
                            System.out.println("null dashboard dto in servlet");
                        }
                        request.setAttribute("dashboardDTO", dashboardDTO);
                        request.setAttribute("dashboardPath", "../dashboard/iTAssetDashboard.jsp");
                        request.getRequestDispatcher("dashboard/CommonDashboard.jsp").forward(request, response);
                        break;
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                        return;
                    }
                case "ajax_admin":
                    long officeUnitId = Long.parseLong(request.getParameter("officeUnitId"));
                    logger.debug("officeUnitId = " + officeUnitId);
                    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
                    Employee_recordsDAO employee_recordsDAO = new Employee_recordsDAO();

                    Set<Long> officeUnitsIdSet = Office_unitsRepository.getInstance().getAllChildOfficeDTOWithParent(officeUnitId)
                            .parallelStream()
                            .map(e -> e.iD)
                            .collect(Collectors.toSet());

                    HrDashboardModel hrDashboardModel = new HrDashboardModel();
                    DashboardService2.getInstance().setHRDashboardData(hrDashboardModel, officeUnitsIdSet, officeUnitId, isLangEng);
                    hrDashboardModel.joining6Count = employee_recordsDAO.getLast6Month(isLangEng, "joining_date");
                    hrDashboardModel.lpr6Count = employee_recordsDAO.getLast6Month(isLangEng, "lpr_date");
                    hrDashboardModel.employeeTypes = CatRepository.getInstance().getCategoryDTOList("employment");
                    hrDashboardModel.wingWiseCount = Office_unit_organogramDAO.getInstance().getWingWiseCount();
                    responseStr = new Gson().toJson(hrDashboardModel);
                    break;
                case "dashboard":
                    DashboardDTO dashboardDTO = new DashboardService().getDashboardDTOByUserDTO(userDTO);
                    responseStr = new Gson().toJson(dashboardDTO);
                    break;

                case "child_offices_id": {
                    long ofcId = Long.parseLong(request.getParameter("officeUnitId"));
                    responseStr = Office_unitsRepository.getInstance().getAllChildOfficeDTOWithParent(ofcId)
                            .stream()
                            .map(e->e.iD)
                            .map(String::valueOf)
                            .collect(Collectors.joining(","));
                }
                    break;
            }
            if (responseStr != null) {
                PrintWriter pw = response.getWriter();
                pw.write(responseStr);
                pw.flush();
                pw.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
