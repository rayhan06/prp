package dashboard;



public class DocumentProcessCatDTO {

    public Long incomingCount;
    public Long outgoingCount;

    public DocumentProcessCatDTO(){

        this.incomingCount = 0L;
        this.outgoingCount = 0L;
    }
}
