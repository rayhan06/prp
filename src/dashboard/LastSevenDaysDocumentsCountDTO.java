package dashboard;

public class LastSevenDaysDocumentsCountDTO {

    String todayDate;
    String yesterdayDate;
    String last2ndDayDate;
    String last3rdDayDate;
    String last4thDayDate;
    String last5thDayDate;
    String last6thDayDate;
    String last7thDayDate;

    Long todayCount;
    Long yesterdayCount;
    Long last2ndDayCount;
    Long last3rdDayCount;
    Long last4thDayCount;
    Long last5thDayCount;
    Long last6thDayCount;
    Long last7thDayCount;

    public LastSevenDaysDocumentsCountDTO(){

        this.todayDate = "";
        this.yesterdayDate = "";
        this.last2ndDayDate = "";
        this.last3rdDayDate = "";
        this.last4thDayDate = "";
        this.last5thDayDate = "";
        this.last6thDayDate = "";
        this.last7thDayDate = "";

        this.todayCount = 0L;
        this.yesterdayCount = 0L;
        this.last2ndDayCount = 0L;
        this.last3rdDayCount = 0L;
        this.last4thDayCount = 0L;
        this.last5thDayCount = 0L;
        this.last6thDayCount = 0L;
        this.last7thDayCount = 0L;
    }
}
