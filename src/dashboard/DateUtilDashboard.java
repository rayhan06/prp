package dashboard;

import javafx.util.Pair;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtilDashboard {

    private static final String[] monthNamesInShortForm = { "", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    private static final String[] monthNamesInFullForm = { "", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

    private static final SimpleDateFormat dayMonthYearDateFormat = new SimpleDateFormat( "dd/MM/yyyy" );
    private static final SimpleDateFormat ddMMMMyyyyDateFormat = new SimpleDateFormat( "dd MMMM yyyy" );


    public static String getddMMyyyyDateStringFromTimeStamp( Long timestamp ){

        return  dayMonthYearDateFormat.format( new Date( timestamp ) );
    }

    public static String getddMMMMyyyyDateStringFromTimeStamp( Long timestamp ){

        return  ddMMMMyyyyDateFormat.format( new Date( timestamp ) );
    }

    private static Calendar clearTimes( Calendar c ) {

        c.set(Calendar.HOUR_OF_DAY,0);
        c.set(Calendar.MINUTE,0);
        c.set(Calendar.SECOND,0);
        c.set(Calendar.MILLISECOND,0);

        return c;
    }

    public static DayMonthYearDTO getDayMonthYearDTOOfTimeInMiliseconds(Long timeInMiliseconds ){

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis( timeInMiliseconds );

        int mYear = calendar.get(Calendar.YEAR);
        int mMonth = calendar.get(Calendar.MONTH);
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);

        return new DayMonthYearDTO( mDay, mMonth+1, mYear );
    }

    public static String getDayMonthYearStringInFullFormatFromTimeInMillis( Long timeInMillis ){

        DayMonthYearDTO dayMonthYearDTO = getDayMonthYearDTOOfTimeInMiliseconds( timeInMillis );

        //return dayMonthYearDTO.day.toString() + "-" + dayMonthYearDTO.month.toString();
        return dayMonthYearDTO.day.toString() + " " + monthNamesInFullForm[dayMonthYearDTO.month] + " " + dayMonthYearDTO.year.toString();
    }

    public static String getLastThDateStringInDDMMMFormat(Integer lastThDay ){

        if( lastThDay < 0 ){

            lastThDay = -1 * lastThDay;
        }

        Calendar calendarDate = Calendar.getInstance();
        calendarDate.add(Calendar.DAY_OF_YEAR,-1 * lastThDay);
        calendarDate=clearTimes( calendarDate );

        DayMonthYearDTO dayMonthYearDTO = getDayMonthYearDTOOfTimeInMiliseconds( calendarDate.getTimeInMillis() );

        //return dayMonthYearDTO.day.toString() + "-" + dayMonthYearDTO.month.toString();
        return dayMonthYearDTO.day.toString() + " " + monthNamesInShortForm[dayMonthYearDTO.month];
    }

    public static String getLastThDateStringInDDMMYYYYFormat(Integer lastThDay ){

        if( lastThDay < 0 ){

            lastThDay = -1 * lastThDay;
        }

        Calendar calendarDate = Calendar.getInstance();
        calendarDate.add(Calendar.DAY_OF_YEAR,-1 * lastThDay);
        calendarDate=clearTimes( calendarDate );

        DayMonthYearDTO dayMonthYearDTO = getDayMonthYearDTOOfTimeInMiliseconds( calendarDate.getTimeInMillis() );

        return dayMonthYearDTO.day.toString() + "-" + dayMonthYearDTO.month.toString() + "-" + dayMonthYearDTO.year.toString();
    }

    public static Pair<Long,Long> getLastThDateStartTimeAndEndTimeInMiliseconds( Integer lastThDay ){

        if( lastThDay < 0 ){

            lastThDay = -1 * lastThDay;
        }

        Calendar calendarStartDate = Calendar.getInstance();
        calendarStartDate.add(Calendar.DAY_OF_YEAR,-1 * lastThDay);
        calendarStartDate=clearTimes( calendarStartDate );

        Calendar calendarEndDate = Calendar.getInstance();
        calendarEndDate.add(Calendar.DAY_OF_YEAR,-1 * (lastThDay - 1) );
        calendarEndDate=clearTimes( calendarEndDate );

        return new Pair <Long,Long> ( calendarStartDate.getTimeInMillis(), calendarEndDate.getTimeInMillis()-1L );
    }

    public static Pair <Long,Long> getTodayAndLastSevenDaysInMiliseconds(){

        Calendar today=Calendar.getInstance();
        today=clearTimes(today);

        Calendar last7days=Calendar.getInstance();
        last7days.add(Calendar.DAY_OF_YEAR,-7);
        last7days=clearTimes(last7days);

        return new Pair <Long,Long> ( today.getTimeInMillis(), last7days.getTimeInMillis() );
    }

    public static String getDateRangeFromMiliseconds( Long val ) {

        Calendar today=Calendar.getInstance();
        today=clearTimes(today);

        Calendar yesterday=Calendar.getInstance();
        yesterday.add(Calendar.DAY_OF_YEAR,-1);
        yesterday=clearTimes(yesterday);

        Calendar last7days=Calendar.getInstance();
        last7days.add(Calendar.DAY_OF_YEAR,-7);
        last7days=clearTimes(last7days);

        Calendar last30days=Calendar.getInstance();
        last30days.add(Calendar.DAY_OF_YEAR,-30);
        last30days=clearTimes(last30days);


        if(val >today.getTimeInMillis())
        {
            return "today";
        }
        else if(val>yesterday.getTimeInMillis())
        {
            return "yesterdayDate";
        }
        else if(val>last7days.getTimeInMillis())
        {
            return "last7days";
        }
        else if(val>last30days.getTimeInMillis())
        {
            return "last30days";
        }
        else
        {
            return "moreThan30days";
        }
    }
}
