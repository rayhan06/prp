package dashboard;

import drug_information.Drug_informationDAO;
import gate_pass.Gate_passDAO;
import sessionmanager.SessionConstants;
import user.UserDTO;




public class DashboardService {

    DashboardCount dashboardCount = new DashboardCount();
    public String Language = "English";

    public static final String ASSIGNED_LICENSE_COUNT_FILTER = " is_used = 1 ";
    public static final String FREE_LICENSE_COUNT_FILTER = " is_used = 0 ";
    
    public static final String ASSIGNED_ASSET_COUNT_FILTER = " asset_list.is_assigned = 1 and is_wholesale = 0 ";
    public static final String FREE_ASSET_COUNT_FILTER = " asset_list.is_assigned = 0 and is_wholesale = 0 ";


    public static final String ACCESSORY_FILTER = " (asset_category_type = " + SessionConstants.ASSET_CAT_KEYBOARD
            + " or asset_category_type = " + SessionConstants.ASSET_CAT_MOUSE + ") ";


    public DashboardDTO getDashboardDTOForDoctor(UserDTO userDTO) throws Exception {
        DashboardDTO dashboardDTO = new DashboardDTO();
        dashboardCount.getPatientAndAppointmentsForDr(dashboardDTO, userDTO);
        dashboardCount.getLast7DayPatientsAndAppointments(dashboardDTO, userDTO.userName);
        dashboardCount.getLabTestPopularity(dashboardDTO, userDTO);
        dashboardCount.getDental(dashboardDTO);
        return dashboardDTO;
    }
    
    public DashboardDTO getDashboardDTOForNurse(UserDTO userDTO) throws Exception {
        DashboardDTO dashboardDTO = new DashboardDTO();
        dashboardCount.getCountsForNurse(dashboardDTO, userDTO);
        dashboardCount.getLast7DaysActionCount(dashboardDTO, userDTO.userName);
        dashboardCount.getActionPopularity(dashboardDTO, userDTO);
        return dashboardDTO;
    }
    
    public DashboardDTO getDashboardDTOForReceptionist(UserDTO userDTO) throws Exception {
        DashboardDTO dashboardDTO = new DashboardDTO();
        dashboardCount.getCountsForReceptionist(dashboardDTO, userDTO);
        dashboardCount.getLast7DayAppointmentsForReceptionist(dashboardDTO, userDTO.userName);
        dashboardCount.getLast6MonthAppointmentsForReceptionist(dashboardDTO, userDTO.userName);
        dashboardCount.getDrWisePrescriptionsToday(dashboardDTO, userDTO);
        dashboardCount.getReceptionistWiseCountToday(dashboardDTO);
        return dashboardDTO;
    }
    
	private DashboardDTO getDashboardDTOForXRayTechnologist(UserDTO userDTO) {
		DashboardDTO dashboardDTO = new DashboardDTO();
		dashboardCount.getCountsForXrayGuy(dashboardDTO, userDTO);
		dashboardCount.getXrayPlates(dashboardDTO);
		dashboardCount.getXRayUsgCounts(dashboardDTO, userDTO);
		return dashboardDTO;
	}
	
	private DashboardDTO getDashboardDTOForRadiologist(UserDTO userDTO) {
		DashboardDTO dashboardDTO = new DashboardDTO();
		dashboardCount.getCountsForRadiologist(dashboardDTO, userDTO);
		dashboardCount.getRadioCounts(dashboardDTO, userDTO);
		return dashboardDTO;
	}
	
	private DashboardDTO getDashboardDTOForPathologist(UserDTO userDTO) {
		DashboardDTO dashboardDTO = new DashboardDTO();
		dashboardCount.getCountsForPathologist(dashboardDTO, userDTO);
		dashboardCount.getLabTestPopularity(dashboardDTO, userDTO);
		dashboardCount.getTestCounts(dashboardDTO, userDTO.userName);
		return dashboardDTO;
	}
	
	private DashboardDTO getDashboardDTOForPathologyHead(UserDTO userDTO) {
		DashboardDTO dashboardDTO = new DashboardDTO();
		dashboardCount.getCountsForPathologyHead(dashboardDTO, userDTO);
		dashboardCount.getLabTestPopularity(dashboardDTO, userDTO);
		dashboardCount.getTestCounts(dashboardDTO, "");
		return dashboardDTO;
	}

    public DashboardDTO getDashboardDTOForPhysioTherapist(UserDTO userDTO) throws Exception {
        DashboardDTO dashboardDTO = new DashboardDTO();
        dashboardCount.getUpcomingTherapies(dashboardDTO, userDTO);
        dashboardCount.getTherapyPopularity(dashboardDTO, userDTO);
        dashboardCount.getLast7DayTherapies(dashboardDTO, userDTO.organogramID);
        dashboardCount.getLast6MonthTherapies(dashboardDTO, userDTO);
        return dashboardDTO;
    }
    
    public DashboardDTO getDashboardDTOForMedicalAdmin(UserDTO userDTO) {
        DashboardDTO dashboardDTO = new DashboardDTO();
        Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
        dashboardCount.getPopularDrugs(drug_informationDAO, dashboardDTO);
        dashboardCount.getLast7DayPatientsAndAppointments(dashboardDTO, "");
        dashboardCount.getLabTestPopularity(dashboardDTO, userDTO);
        dashboardCount.getDrWisePrescriptionsToday(dashboardDTO, userDTO);
        dashboardCount.getDrWisePrescriptions(dashboardDTO, userDTO);
        dashboardCount.getFromPersonalStock(dashboardDTO);
        dashboardCount.getReceptionistWiseCount(dashboardDTO);
        dashboardCount.getReceptionistWiseCountToday(dashboardDTO);
        dashboardCount.getCUM(dashboardDTO);
        dashboardCount.getSpecial(dashboardDTO);
        dashboardCount.getDental(dashboardDTO);
        return dashboardDTO;
    }


    private DashboardDTO getDashboardDTOForEmployee(UserDTO userDTO) {

        DashboardDTO dashboardDTO = new DashboardDTO();
        dashboardCount.getLast6MonthVisits(dashboardDTO, userDTO.userName);
        dashboardCount.getLast6MonthTickets(dashboardDTO, userDTO.organogramID);
        dashboardCount.getStatusWiseTicketCountForEmployee(dashboardDTO, userDTO.organogramID);
        return dashboardDTO;
    }

    public DashboardDTO getDashboardDTOForInventoryManager(UserDTO userDTO) {
    	System.out.println("getDashboardDTOForInventoryManager");
        DashboardDTO dashboardDTO = new DashboardDTO();
        Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
        dashboardCount.getCountsForInventoryManager(dashboardDTO, userDTO, drug_informationDAO);
        dashboardCount.getPopularDrugs(drug_informationDAO, dashboardDTO);
        dashboardCount.getProfitableDrugs(drug_informationDAO, dashboardDTO);
        dashboardCount.getLast7DayCostAndCount(dashboardDTO);
        dashboardCount.getLast6MonthCostCount(dashboardDTO);
        return dashboardDTO;
    }

    public DashboardDTO getDashboardDTOForTicketAdmin(UserDTO userDTO) throws Exception {
        DashboardDTO dashboardDTO = new DashboardDTO();
        dashboardCount.getCountsForTicketAdmin(dashboardDTO, userDTO);
        dashboardCount.getLast7DayTickets(dashboardDTO, userDTO);
        dashboardCount.getLast6MonthTickets(dashboardDTO, userDTO);
        dashboardCount.getTypewiseTickets(dashboardDTO);
        dashboardCount.getWingwiseTickets(dashboardDTO, userDTO);
        return dashboardDTO;
    }
    
    public DashboardDTO getDashboardDTOForTicketAdminForAsset(UserDTO userDTO) throws Exception {
        DashboardDTO dashboardDTO = new DashboardDTO();
        dashboardCount.getCountsForAssetAdmin(dashboardDTO);
        dashboardCount.getCategoryWiseStatusCount(dashboardDTO);
        dashboardCount.getAssetOrganogramCount(dashboardDTO);
        dashboardCount.getKeyedSoftwareStatus(dashboardDTO);
       
        return dashboardDTO;
    }
    

	private DashboardDTO getDashboardDTOForPBS(UserDTO userDTO) {
		DashboardDTO dashboardDTO = new DashboardDTO();
		dashboardCount.getCountsForPBS(dashboardDTO);
		return dashboardDTO;
	}
	
	private DashboardDTO getDashboardDTOForSEA(UserDTO userDTO) {
		DashboardDTO dashboardDTO = new DashboardDTO();
		dashboardCount.getCountsForSEA(dashboardDTO);
		return dashboardDTO;
	}


    public DashboardDTO getDashboardDTOForGatePassManagementAdmin() throws Exception {
        DashboardDTO dashboardDTO = new DashboardDTO();
        dashboardDTO.gatePassDashboardDataModel = new Gate_passDAO().getGatePassDashboardDataModel();
        return dashboardDTO;
    }
    

    
    public DashboardDTO getDashboardDTOByUserDTO(UserDTO userDTO) throws Exception {

        if (userDTO.roleID == SessionConstants.DOCTOR_ROLE) {
            return getDashboardDTOForDoctor(userDTO);
        }
        else if (userDTO.roleID == SessionConstants.NURSE_ROLE) {
            return getDashboardDTOForNurse(userDTO);
        }
        else if (userDTO.roleID == SessionConstants.INVENTORY_MANGER_ROLE || userDTO.roleID == SessionConstants.PHARMACY_PERSON) {
            return getDashboardDTOForInventoryManager(userDTO);
        } 
        else if (userDTO.roleID == SessionConstants.PARLIAMENT_EMPLOYEE_ROLE) {
            return getDashboardDTOForEmployee(userDTO);
        } 
        else if (userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE) {
            return getDashboardDTOForTicketAdmin(userDTO);
        } 
        else if (userDTO.roleID == SessionConstants.GATE_MANAGEMENT_ADMIN_ROLE) {
            return getDashboardDTOForGatePassManagementAdmin();
        } 
        else if (userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE) {
            return getDashboardDTOForMedicalAdmin(userDTO);
        } 
        else if (userDTO.roleID == SessionConstants.PHYSIOTHERAPIST_ROLE) {
            return getDashboardDTOForPhysioTherapist(userDTO);
        }
        else if (userDTO.roleID == SessionConstants.MEDICAL_RECEPTIONIST_ROLE) {
            return getDashboardDTOForReceptionist(userDTO);
        }
        else if (userDTO.roleID == SessionConstants.XRAY_GUY) {
            return getDashboardDTOForXRayTechnologist(userDTO);
        }
        else if (userDTO.roleID == SessionConstants.LAB_TECHNITIAN_ROLE) {
            return getDashboardDTOForPathologist(userDTO);
        }
        else if (userDTO.roleID == SessionConstants.RADIOLOGIST) {
            return getDashboardDTOForRadiologist(userDTO);
        }
        else if (userDTO.roleID == SessionConstants.PATHOLOGY_HEAD) {
            return getDashboardDTOForPathologyHead(userDTO);
        }
        else if (userDTO.roleID == SessionConstants.PBS_ROLE) {
            return getDashboardDTOForPBS(userDTO);
        }
        else if (userDTO.roleID == SessionConstants.SERJEANT_AT_ARMS_ROLE) {
            return getDashboardDTOForSEA(userDTO);
        }
        return null;
    }




}
