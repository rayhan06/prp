package dashboard;

public class DocumentTypeCat {

    public static final Integer drawing = 0;
    public static final Integer letter = 1;
    public static final Integer rfi = 2;
    public static final Integer qsdr = 3;
    public static final Integer ncn = 4;
    public static final Integer hse = 5;
    public static final Integer ncr = 6;

    public DocumentTypeCat(){}
}
