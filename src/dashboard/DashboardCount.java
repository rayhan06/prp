package dashboard;

import appointment.AppointmentDAO;
import appointment.KeyCountDTO;
import appointment.YearMonthCount;
import asset_model.AssetAssigneeDAO;
import asset_model.AssetAssigneeDTO;
import borrow_medicine.PersonalStockDAO;
import common_lab_report.Common_lab_reportDAO;
import config.GlobalConfigConstants;
import config.GlobalConfigurationRepository;
import doctor_time_slot.Doctor_time_slotDAO;
import doctor_time_slot.Doctor_time_slotDTO;
import drug_information.Drug_informationDAO;
import drug_information.Drug_informationDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import gate_pass.Gate_passDAO;
import lab_test.Lab_testDTO;
import lab_test.Lab_testRepository;
import language.LM;
import medical_equipment_name.Medical_equipment_nameDAO;
import medical_inventory_out.Medical_inventory_outDAO;
import medical_transaction.Medical_transactionDAO;
import nurse_action.NurseActionDetailsDAO;
import office_units.Organization;
import org.apache.log4j.Logger;
import patient_measurement.Patient_measurementDAO;
import pb.CommonDAO;
import pb.PBNameRepository;
import physiotherapy_plan.PhysiotherapyScheduleDAO;
import physiotherapy_plan.Physiotherapy_planDAO;
import prescription_details.Dental_activitiesDAO;
import prescription_details.PrescriptionLabTestDAO;
import prescription_details.PrescriptionLabTestDTO;
import prescription_details.Prescription_detailsDAO;
import sessionmanager.SessionConstants;
import software_type.SoftwareSubtypeDAO;
import support_ticket.Support_ticketDAO;
import support_ticket.Support_ticketDTO;
import user.UserDTO;
import user.UserRepository;
import util.TimeConverter;
import workflow.WorkflowController;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class DashboardCount {
	Logger logger = Logger.getLogger(getClass());
	
	Prescription_detailsDAO prescription_detailsDAO = new Prescription_detailsDAO();
	PrescriptionLabTestDAO prescriptionLabTestDAO = new PrescriptionLabTestDAO();
    AppointmentDAO appointmentDAO = new AppointmentDAO();
    PhysiotherapyScheduleDAO physiotherapyScheduleDAO = new PhysiotherapyScheduleDAO();
    Medical_inventory_outDAO medical_inventory_outDAO = new Medical_inventory_outDAO();
    Support_ticketDAO support_ticketDAO = new Support_ticketDAO();
    Patient_measurementDAO patient_measurementDAO = new Patient_measurementDAO();
    NurseActionDetailsDAO nurseActionDetailsDAO = NurseActionDetailsDAO.getInstance();
    PersonalStockDAO personalStockDAO = PersonalStockDAO.getInstance();
    AssetAssigneeDAO assetAssigneeDAO = new AssetAssigneeDAO();
    SoftwareSubtypeDAO softwareSubtypeDAO = SoftwareSubtypeDAO.getInstance();
    Medical_equipment_nameDAO medical_equipment_nameDAO = new Medical_equipment_nameDAO();
    Medical_transactionDAO medical_transactionDAO = new Medical_transactionDAO();
    Doctor_time_slotDAO doctor_time_slotDAO = new Doctor_time_slotDAO();
    Physiotherapy_planDAO physiotherapy_planDAO = new Physiotherapy_planDAO();
    
    public static final String ACCESSORY_FILTER = " (asset_category_type = " + SessionConstants.ASSET_CAT_KEYBOARD
            + " or asset_category_type = " + SessionConstants.ASSET_CAT_MOUSE + ") ";
	
    public void countToArrayFor7Days(List<KeyCountDTO> keyCounts, long[] counts, double[] costs)
	{
    	countToArrayFor7Days(keyCounts, counts, costs, null);
	}
	public void countToArrayFor7Days(List<KeyCountDTO> keyCounts, long[] counts, double[] costs, long[] counts2)
	{
		int p = 0;
        for (int i = 0; i <= 7; i++) {
            long startTime = TimeConverter.getNthDay(i - 7);
            //System.out.println(" i = " + i + " startTime " + startTime );
            if(keyCounts != null && p < keyCounts.size() && keyCounts.get(p).key == startTime)
            {
            	if(counts != null)
            	{
            		counts[i] = keyCounts.get(p).count;   
            		//System.out.println(" i = " + i + " counts[i]= " + counts[i]);
            	}
            	if(counts2 != null)
            	{
            		counts2[i] = keyCounts.get(p).count2;   
            		//System.out.println(" i = " + i + " counts[i]= " + counts[i]);
            	}
            	if(costs != null)
            	{
            		costs[i] = keyCounts.get(p).cost;   
            	}
            	         
            	p ++;
            }
        }
	}
	public void getLast7DayPatientsAndAppointments(DashboardDTO dashboardDTO, String userName)
    {
    	List<KeyCountDTO> appointmentCountDTOs = appointmentDAO.getLast7DayCount(userName);
        List<KeyCountDTO> prescriptionCountDTOs = prescription_detailsDAO.getLast7DayCount(userName);
        countToArrayFor7Days(appointmentCountDTOs, dashboardDTO.last7DayAppointments, null);
        countToArrayFor7Days(prescriptionCountDTOs, dashboardDTO.last7DayPatients, null);
    }
	
	public void getLast7DaysActionCount(DashboardDTO dashboardDTO, String userName)
    {
    	List<KeyCountDTO> actionCountDTOs = nurseActionDetailsDAO.getLast7DayCount(userName);
        
        countToArrayFor7Days(actionCountDTOs, dashboardDTO.last7DayPatients, null);
    }

	public void getLast7DayCostAndCount(DashboardDTO dashboardDTO)
    {
    	List<KeyCountDTO> costCounts = medical_transactionDAO.getLast7DayCountCost();
    	countToArrayFor7Days(costCounts, dashboardDTO.last7DayMedicineSold, dashboardDTO.last7DayMedicineCost, dashboardDTO.last7DayPatients);        
    }
	
	public void getLast7DayTherapies(DashboardDTO dashboardDTO, long therapistId)
    {
    	List<KeyCountDTO> therapyCountDTOs =  physiotherapy_planDAO.getLast7DayCount(therapistId);
    	countToArrayFor7Days(therapyCountDTOs, dashboardDTO.last7DayPatients, null);     
    }
	
	public void getLast6MonthTherapies(DashboardDTO dashboardDTO, UserDTO userDTO)
    {
    	String language = LM.getLanguage(userDTO);
    	
    	dashboardDTO.physiotherapists = new ArrayList<Long>(EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.PHYSIOTHERAPIST_ROLE));

    	List<YearMonthCount> counts = physiotherapy_planDAO.getLast6MonthCount(language.equalsIgnoreCase("english"));
    	
    	for(int i = 0; i < dashboardDTO.last6MonthsTherapies.length; i ++)
    	{
    		dashboardDTO.last6MonthsTherapies[i] = new long[dashboardDTO.physiotherapists.size()];
    	}

    	
    	long startDate = -1;
    	int i = -1;
    	for(YearMonthCount count: counts)
    	{
    		if(startDate != count.startDate)
    		{
    			i++;
    			startDate = count.startDate;
    		}
    		
    		int index = dashboardDTO.physiotherapists.indexOf(count.key);
    		if(index >= 0 && index < dashboardDTO.last6MonthsTherapies[i].length)
    		{
    			dashboardDTO.last6MonthsTherapies[i][index] = count.count;
    		}
    		
    		
    	}

    }
	
	public void getLast7DayTickets(DashboardDTO dashboardDTO, UserDTO userDTO)
    {
    	List<KeyCountDTO> openCount = support_ticketDAO.getLast7DayOpenCount(userDTO);
    	List<KeyCountDTO> closeCount = support_ticketDAO.getLast7DayCloseCount(userDTO);
    	countToArrayFor7Days(openCount, dashboardDTO.last7DayOpenedTickets, null); 
    	countToArrayFor7Days(closeCount, dashboardDTO.last7DayClosedTickets, null);        
    }
	public void countToArrayFor6Months(List<YearMonthCount> ymCounts, long[] counts, double[] costs)
	{
		countToArrayFor6Months(ymCounts, counts, costs, null);
	}
	
	public void countToArrayFor6Months(List<YearMonthCount> ymCounts, long[] counts, double[] costs, long[] counts2)
	{
		int p = 0;
        for (int i = 0; i <= 6; i++) {
        	Calendar cal = Calendar.getInstance();
    		cal.add(Calendar.MONTH, i - 6);
    		//System.out.println("cal year = " + cal.get(Calendar.YEAR) + " cal month = " + cal.get(Calendar.MONTH) + 1);
            if(ymCounts != null && p < ymCounts.size() &&
            		ymCounts.get(p).year == cal.get(Calendar.YEAR) 
            		&& ymCounts.get(p).month == cal.get(Calendar.MONTH) + 1)
            {
            	if(counts != null)
            	{
            		counts[i] = ymCounts.get(p).count;
            	}
            	if(counts2 != null)
            	{
            		counts2[i] = ymCounts.get(p).count2;
            	}
            	if(costs != null)
            	{
            		costs[i] = ymCounts.get(p).cost;
            	}
            	p++;
            }
        }
	}
	
	public void getCountsForPBS(DashboardDTO dashboardDTO) {
		Gate_passDAO gate_passDAO = new Gate_passDAO();
		dashboardDTO.issuedPassCounts = gate_passDAO.getIssuedCounts();
		dashboardDTO.approvedPassCounts = gate_passDAO.getApprovedCounts();
		dashboardDTO.userWisePasses = gate_passDAO.getUserWiseCount();
		dashboardDTO.typeWisePasses = gate_passDAO.getTypeWiseCount();
		countToArrayFor7Days(gate_passDAO.getLast7DayIssueCount(), dashboardDTO.last7DayIssuedPasses, null);
		countToArrayFor6Months(gate_passDAO.getLast6MonthIssuedCount(), dashboardDTO.last6MonthIssuesPasses, null);
		countToArrayFor6Months(gate_passDAO.getLast6MonthApprovedCount(), dashboardDTO.last6MonthApprovedPasses, null);
	}
	
	public void getCountsForSEA(DashboardDTO dashboardDTO) {
		Gate_passDAO gate_passDAO = new Gate_passDAO();
		dashboardDTO.pendingPassCounts = gate_passDAO.getPendingCounts();
		dashboardDTO.approvedPassCounts = gate_passDAO.getApprovedCounts();
		dashboardDTO.userWisePasses = gate_passDAO.getSergeantUserWiseCount();
		dashboardDTO.typeWisePasses = gate_passDAO.getTypeWiseRequestedCount();
		countToArrayFor7Days(gate_passDAO.getLast7DayApprovalCount(), dashboardDTO.last7DayApprovedPasses, null);
		countToArrayFor6Months(gate_passDAO.getLast6MonthIssuedCount(), dashboardDTO.last6MonthIssuesPasses, null);
		countToArrayFor6Months(gate_passDAO.getLast6MonthApprovedCount(), dashboardDTO.last6MonthApprovedPasses, null);
		countToArrayFor6Months(gate_passDAO.getLast6MonthRequestedCount(), dashboardDTO.last6MonthRequestedPasses, null);
	}
	
	public void getLast6MonthVisits(DashboardDTO dashboardDTO, String userName)
    {
		List<YearMonthCount> prescriptionCountDTOs = prescription_detailsDAO.getLast6MonthCount(userName);
		countToArrayFor6Months(prescriptionCountDTOs, dashboardDTO.last6MonthVisits, null);       
    }
	
	public void getLast7DayAppointmentsForReceptionist(DashboardDTO dashboardDTO, String userName)
    {
    	List<KeyCountDTO> appointmentCountDTOs = appointmentDAO.getLast7DayCountForReceptionist(userName);
    	countToArrayFor7Days(appointmentCountDTOs, dashboardDTO.last7DayAppointments, null);     
    }
	
	public void getLast6MonthAppointmentsForReceptionist(DashboardDTO dashboardDTO, String userName)
    {
		List<YearMonthCount> appointmentCountDTOs = appointmentDAO.getLast6MonthCountForReceptionist(userName);
		countToArrayFor6Months(appointmentCountDTOs, dashboardDTO.last6MonthAppointments, null);       
    }
	
	public void getLast6MonthCostCount(DashboardDTO dashboardDTO)
    {
		List<YearMonthCount> costCounts = medical_transactionDAO.getLast6MonthCostCount();
		countToArrayFor6Months(costCounts, dashboardDTO.last6MonthMedicineSold, dashboardDTO.last6MonthMedicineCost, dashboardDTO.last6MonthPatients);        
    }
	
	public void getLast6MonthTickets(DashboardDTO dashboardDTO, long organogramId)
    {		
    	List<YearMonthCount> openCount = support_ticketDAO.getLast6MonthOpenCount(organogramId);
    	List<YearMonthCount> closeCount = support_ticketDAO.getLast6MonthCloseCount(organogramId);
    	countToArrayFor6Months(openCount, dashboardDTO.last6MonthsOpenedTickets, null); 
    	countToArrayFor6Months(closeCount, dashboardDTO.last6MonthsClosedTickets, null);        
    }
	
	public void getLast6MonthTickets(DashboardDTO dashboardDTO, UserDTO userDTO)
    {
		
    	List<YearMonthCount> openCount = support_ticketDAO.getLast6MonthOpenCount(userDTO);
    	List<YearMonthCount> closeCount = support_ticketDAO.getLast6MonthCloseCount(userDTO);
    	countToArrayFor6Months(openCount, dashboardDTO.last6MonthsOpenedTickets, null); 
    	countToArrayFor6Months(closeCount, dashboardDTO.last6MonthsClosedTickets, null);        
    }
	
	public void getLabTestPopularity(DashboardDTO dashboardDTO, UserDTO userDTO)
    {
        Common_lab_reportDAO common_lab_reportDAO = new Common_lab_reportDAO();
        List<KeyCountDTO> labTestCounts = common_lab_reportDAO.getTestCount();
        dashboardDTO.labTestTypes = new String[labTestCounts.size()];
        dashboardDTO.typeWiseDiagnosticProcedure = new long[labTestCounts.size()];
        
        int j = 0;
        for(KeyCountDTO ltc: labTestCounts)
        {
        	dashboardDTO.typeWiseDiagnosticProcedure[j] = ltc.count;
        	Lab_testDTO lab_testDTO = Lab_testRepository.getInstance().getLab_testDTOByID(ltc.key);
        	if(lab_testDTO != null)
        	{
        		dashboardDTO.labTestTypes[j] = lab_testDTO.nameEn;
        	}
        	
        	j++;
        }
    }
	
	public void getActionPopularity(DashboardDTO dashboardDTO, UserDTO userDTO)
    {       
        dashboardDTO.popularActioncCount = nurseActionDetailsDAO.getActionWiseCount(userDTO.userName);
        
    }
	
	public void getTherapyPopularity(DashboardDTO dashboardDTO, UserDTO userDTO)
    {
        List<KeyCountDTO> therapyCatCount = physiotherapyScheduleDAO.getTypeCount(userDTO.organogramID); 
        dashboardDTO.therapyNames = new String[therapyCatCount.size()];
        dashboardDTO.therapyCount = new long[therapyCatCount.size()];
        String language = "English";
        if(userDTO.languageID == SessionConstants.BANGLA )
        {
        	language = "Bangla";
        }
        int j = 0;
        for(KeyCountDTO tc: therapyCatCount)
        {
        	dashboardDTO.therapyCount[j] = tc.count;
        	dashboardDTO.therapyNames[j] = CommonDAO.getName(language, "therapy_type", tc.key);
        	j++;
        }
    }
	
	public void getPatientAndAppointmentsForDr(DashboardDTO dashboardDTO, UserDTO userDTO)
    {
		String filter = " dr_user_name = '" + userDTO.userName + "' and visit_date >= " + TimeConverter.get1stDayOfMonth();

        dashboardDTO.patientHanDledInCurrentMonth = prescription_detailsDAO.getCount(filter, userDTO);

        System.out.println("patientHanDledInCurrentMonth = " + dashboardDTO.patientHanDledInCurrentMonth);

        filter = "dr_user_name = '" + userDTO.userName + "' and isCancelled = 0 and visit_date >= " + TimeConverter.getToday()
                + " and id not in (select appointment_id from prescription_details)";

        dashboardDTO.upComingAppointments = appointmentDAO.getCount(filter, userDTO);
    }
	
	public void getCountsForNurse(DashboardDTO dashboardDTO, UserDTO userDTO)
    {
		String filter = " measurer_user_name = '" + userDTO.userName + "' and lastModificationTime >= " + TimeConverter.getToday();

        dashboardDTO.pmCount = patient_measurementDAO.getCount(filter, userDTO);

        System.out.println("patientHanDledInCurrentMonth = " + dashboardDTO.patientHanDledInCurrentMonth);

        filter = " nurse_user_name = '" + userDTO.userName + "' and lastModificationTime >= " + TimeConverter.getToday();

        dashboardDTO.actionCount = nurseActionDetailsDAO.getCount(filter);
    }
	
	public void getCountsForReceptionist(DashboardDTO dashboardDTO, UserDTO userDTO)
    {

		String filter = " isDeleted = 0 and  inserted_by_user_name = '" + userDTO.userName + "' and insertion_date >= " + TimeConverter.get1stDayOfMonth();
		dashboardDTO.appointmentThisMonth = appointmentDAO.getCount(filter, userDTO);
		filter = " isDeleted = 0 and  inserted_by_user_name = '" + userDTO.userName + "' and insertion_date >= " + TimeConverter.getToday();
        dashboardDTO.appointmentToday = appointmentDAO.getCount(filter, userDTO);
    }
	
	public void getCountsForXrayGuy(DashboardDTO dashboardDTO, UserDTO userDTO) {
		String filter = " isDeleted = 0 and is_done = 1 and testing_date >= " + TimeConverter.getToday() + " and xray_technologist_user_name = '" + userDTO.userName + "'";
		dashboardDTO.testsToday = prescriptionLabTestDAO.getCount(filter, userDTO);
		filter = " isDeleted = 0 and is_done = 0 and (lab_test_type = " + SessionConstants.LAB_TEST_XRAY + " or lab_test_type = " + SessionConstants.LAB_TEST_ULTRASONOGRAM + ")";
		dashboardDTO.testsPending = prescriptionLabTestDAO.getCount(filter, userDTO);
	}
	
	public void getCountsForPathologist(DashboardDTO dashboardDTO, UserDTO userDTO) {
		String filter = " isDeleted = 0 and  is_done = 1 and testing_date >= " + TimeConverter.getToday() + " and xray_technologist_user_name = '" + userDTO.userName + "'"
				+ " and (lab_test_type != " + SessionConstants.LAB_TEST_XRAY + " and lab_test_type != " + SessionConstants.LAB_TEST_ULTRASONOGRAM + " and lab_test_type != " + SessionConstants.LAB_TEST_OTHER + ")";
		dashboardDTO.testsToday = prescriptionLabTestDAO.getCount(filter, userDTO);
		filter = " isDeleted = 0 and  is_done = 0"
				+ " and (lab_test_type != " + SessionConstants.LAB_TEST_XRAY + " and lab_test_type != " + SessionConstants.LAB_TEST_ULTRASONOGRAM + " and lab_test_type != " + SessionConstants.LAB_TEST_OTHER + ")";
		dashboardDTO.testsPending = prescriptionLabTestDAO.getCount(filter, userDTO);
	}
	
	public void getCountsForPathologyHead(DashboardDTO dashboardDTO, UserDTO userDTO) {
		String filter = " isDeleted = 0 and  is_done = 1 and approval_status = " + PrescriptionLabTestDTO.APPROVED
				+ " and (lab_test_type != " + SessionConstants.LAB_TEST_XRAY + " and lab_test_type != " + SessionConstants.LAB_TEST_ULTRASONOGRAM + " and lab_test_type != " + SessionConstants.LAB_TEST_OTHER + ")";
		dashboardDTO.testsDone = prescriptionLabTestDAO.getCount(filter, userDTO);
		filter = " isDeleted = 0 and  is_done = 1 and approval_status = " + PrescriptionLabTestDTO.PENDING
				+ " and (lab_test_type != " + SessionConstants.LAB_TEST_XRAY + " and lab_test_type != " + SessionConstants.LAB_TEST_ULTRASONOGRAM + " and lab_test_type != " + SessionConstants.LAB_TEST_OTHER + ")";
		dashboardDTO.testsPending = prescriptionLabTestDAO.getCount(filter, userDTO);
	}
	
	public void getCountsForRadiologist(DashboardDTO dashboardDTO, UserDTO userDTO) {
		String filter = " test_status = 1 and actual_delivery_date >= " + TimeConverter.getToday() + " and tester_id = '" + userDTO.userName + "'";
		dashboardDTO.testsToday = prescriptionLabTestDAO.getCount(filter, userDTO);
		filter = " is_done = 1 and test_status = 0 and (lab_test_type = " + SessionConstants.LAB_TEST_XRAY + " or lab_test_type = " + SessionConstants.LAB_TEST_ULTRASONOGRAM + ")";
		dashboardDTO.testsPending = prescriptionLabTestDAO.getCount(filter, userDTO);
	}
	
	public void getXrayPlates(DashboardDTO dashboardDTO) {
		dashboardDTO.xRayPlates = medical_equipment_nameDAO.getXrayPlates();
	}
	
	public void getXRayUsgCounts(DashboardDTO dashboardDTO, UserDTO userDTO)
    {
		List<KeyCountDTO> xRayCountDTOs = prescriptionLabTestDAO.getLast7DayTestCount(userDTO.userName, SessionConstants.LAB_TEST_XRAY);

    	countToArrayFor7Days(xRayCountDTOs, dashboardDTO.last7DayTests1, null); 
    	
    	List<KeyCountDTO> usgCountDTOs = prescriptionLabTestDAO.getLast7DayTestCount(userDTO.userName, SessionConstants.LAB_TEST_ULTRASONOGRAM);
    	countToArrayFor7Days(usgCountDTOs, dashboardDTO.last7DayTests2, null); 
    }
	
	public void getRadioCounts(DashboardDTO dashboardDTO, UserDTO userDTO)
    {
		List<KeyCountDTO> xRayCountDTOs = prescriptionLabTestDAO.getLast7DayTestCount("", SessionConstants.LAB_TEST_XRAY);

    	countToArrayFor7Days(xRayCountDTOs, dashboardDTO.last7DayTests1, null); 
    	
    	List<KeyCountDTO> usgCountDTOs = prescriptionLabTestDAO.getLast7DayTestCount("", SessionConstants.LAB_TEST_ULTRASONOGRAM);
    	countToArrayFor7Days(usgCountDTOs, dashboardDTO.last7DayTests2, null);
    	
    	List<KeyCountDTO> xRayReportCountDTOs = prescriptionLabTestDAO.getLast7DayTestCountForRadiologist(userDTO.userName, SessionConstants.LAB_TEST_XRAY);

    	countToArrayFor7Days(xRayReportCountDTOs, dashboardDTO.last7DayReports1, null); 
    	
    	List<KeyCountDTO> usgReportCountDTOs = prescriptionLabTestDAO.getLast7DayTestCountForRadiologist(userDTO.userName, SessionConstants.LAB_TEST_ULTRASONOGRAM);
    	countToArrayFor7Days(usgReportCountDTOs, dashboardDTO.last7DayReports2, null); 
    }
	
	public void getTestCounts(DashboardDTO dashboardDTO, String userName)
    {
		List<KeyCountDTO> xRayCountDTOs = prescriptionLabTestDAO.getLast7DayTestCount(userName, -1);
    	countToArrayFor7Days(xRayCountDTOs, dashboardDTO.last7DayTests1, null); 
    	
    }
	
	public void getUpcomingTherapies(DashboardDTO dashboardDTO, UserDTO userDTO)
    {
		String filter = " therapy_date = " + TimeConverter.getToday() + " and is_done = 0 "
                + " and therapist_organogram_id = " + userDTO.organogramID;
        dashboardDTO.todaysTherapies = physiotherapyScheduleDAO.getCount(filter, userDTO);


        filter = "dr_user_name = '" + userDTO.userName + "' and isCancelled = 0 and visit_date >= " + TimeConverter.getToday()
                + " and appointment.id not in (select appointment_id from physiotherapy_plan)";

        dashboardDTO.upComingAppointments = appointmentDAO.getCount(filter, userDTO);
    }
	
	public void getCountsForInventoryManager(DashboardDTO dashboardDTO, UserDTO userDTO, Drug_informationDAO drug_informationDAO)
	{		
		long startTime = TimeConverter.get1stDayOfMonth();
        long endTime = TimeConverter.get1stDayOfNextMonth();
        
        KeyCountDTO costCountDTO = medical_transactionDAO.getCostCount(startTime, endTime);
        if(costCountDTO != null)
        {
        	dashboardDTO.totalCost = costCountDTO.cost;
        	dashboardDTO.soldCount = costCountDTO.count;
        }

        //dashboardDTO.criticalMedicines = (int) drug_informationDAO.getCount("current_stock <= minimul_level_for_alert", userDTO);
        //dashboardDTO.stockOutMedicines = (int) drug_informationDAO.getCount("current_stock <= 0", userDTO);
        int validityHours = Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.PRESCIPTION_VALIDITY_HOURS).value);
	    long now = System.currentTimeMillis();	
        String filter = " medicine_count > 0 ";
		filter +=  " and medicine_added = 0 and visit_date >= " + TimeConverter.getToday()
		+ " and (" + now + " - visit_time <= " + validityHours * 1000 * 3600 + ") ";
		
        dashboardDTO.testsPending = prescription_detailsDAO.getCount(filter, userDTO);
        
        KeyCountDTO deliveredPrescriptions = medical_transactionDAO.getDeliveredPrescriptions();
        if(deliveredPrescriptions != null)
        {
        	dashboardDTO.totalPatients = deliveredPrescriptions.count;

        }
        KeyCountDTO patientsMedicines = medical_transactionDAO.getDeliveredMedicines();
        if(patientsMedicines != null)
        {
        	dashboardDTO.totalDrugs = patientsMedicines.count2;
        }

        /*Medical_reagent_nameDAO medical_reagent_nameDAO = new Medical_reagent_nameDAO();
        dashboardDTO.criticalReagents = (int) medical_reagent_nameDAO.getCount("current_stock <= stock_alert_quantity", userDTO);
        dashboardDTO.stockOutReagents = (int) medical_reagent_nameDAO.getCount("current_stock <= 0", userDTO);*/
	}
	
	public void getStatusWiseTicketCount(DashboardDTO dashboardDTO, UserDTO userDTO)
	{
		List<KeyCountDTO> ticketStatusCounts = support_ticketDAO.getTicketStatusCount(userDTO);
        for(KeyCountDTO ticketStatusCount: ticketStatusCounts)
        {
        	if(ticketStatusCount.key == Support_ticketDTO.OPEN)
        	{
        		dashboardDTO.openTicketCount = ticketStatusCount.count;
        	}
        	else if(ticketStatusCount.key == Support_ticketDTO.CLOSED)
        	{
        		dashboardDTO.closedTicketCount = ticketStatusCount.count;
        	}
        	else if(ticketStatusCount.key == Support_ticketDTO.SOLVED)
        	{
        		dashboardDTO.solvedTicketCount = ticketStatusCount.count;
        	}
        }
        dashboardDTO.ticketCount = dashboardDTO.openTicketCount + dashboardDTO.closedTicketCount + dashboardDTO.solvedTicketCount;
	}
	
	public void getStatusWiseTicketCountForEmployee(DashboardDTO dashboardDTO, long organogramId)
	{
		List<KeyCountDTO> ticketStatusCounts = support_ticketDAO.getTicketStatusCount(organogramId);
        for(KeyCountDTO ticketStatusCount: ticketStatusCounts)
        {
        	if(ticketStatusCount.key == Support_ticketDTO.OPEN)
        	{
        		dashboardDTO.openTicketCount = ticketStatusCount.count;
        	}
        	else if(ticketStatusCount.key == Support_ticketDTO.CLOSED)
        	{
        		dashboardDTO.closedTicketCount = ticketStatusCount.count;
        	}
        	else if(ticketStatusCount.key == Support_ticketDTO.SOLVED)
        	{
        		dashboardDTO.solvedTicketCount = ticketStatusCount.count;
        	}
        }
        dashboardDTO.ticketCount = dashboardDTO.openTicketCount + dashboardDTO.closedTicketCount + dashboardDTO.solvedTicketCount;
	}
	
	public void getCountsForTicketAdmin(DashboardDTO dashboardDTO, UserDTO userDTO)
	{		
        getStatusWiseTicketCount(dashboardDTO, userDTO);
	}
	
	public void getCountsForAssetAdmin(DashboardDTO dashboardDTO)
	{
		List<KeyCountDTO> assetStatusCounts = assetAssigneeDAO.getAssetStatusCount();
        for(KeyCountDTO assetStatusCount: assetStatusCounts)
        {
        	if(assetStatusCount.key == AssetAssigneeDTO.ASSIGNED)
        	{
        		dashboardDTO.assignedAssetCount = assetStatusCount.count;
        	}
        	else if(assetStatusCount.key == AssetAssigneeDTO.NOT_ASSIGNED)
        	{
        		dashboardDTO.freeAssetCount = assetStatusCount.count;
        	}
        	else if(assetStatusCount.key == AssetAssigneeDTO.CONDEMNED)
        	{
        		dashboardDTO.condemnedAssets = assetStatusCount.count;
        	}
        	else
        	{
        		dashboardDTO.otherAssets = assetStatusCount.count;
        	}
        }
        dashboardDTO.assetCount = dashboardDTO.assignedAssetCount + dashboardDTO.freeAssetCount
        		+ dashboardDTO.condemnedAssets + dashboardDTO.otherAssets;
	}
	
	public void getAssetOrganogramCount(DashboardDTO dashboardDTO)
	{
		List<KeyCountDTO> assetOrgCount = assetAssigneeDAO.getAssetOrganogramCount();
		dashboardDTO.assetWingCount = new ConcurrentHashMap<>();
		for(KeyCountDTO keyCountDTO: assetOrgCount)
        { 
			long wing = WorkflowController.getWingIdFromOrganogramId(keyCountDTO.key);
			if(wing != -1)
			{
				if(dashboardDTO.assetWingCount.getOrDefault(wing, null) == null)
	        	{
	        		dashboardDTO.assetWingCount.put(wing, 0);        		
	        	}
				
				dashboardDTO.assetWingCount.put(wing, dashboardDTO.assetWingCount.get(wing) + keyCountDTO.count);  
			}
			
        }
	}
	
	public void getKeyedSoftwareStatus(DashboardDTO dashboardDTO)
	{
		List<KeyCountDTO> softStatus = softwareSubtypeDAO.getKeyedSoftStatus();
		dashboardDTO.keyStatusCount = new ConcurrentHashMap<>();
        for(KeyCountDTO assetStatusCount: softStatus)
        {        	
    		if(dashboardDTO.keyStatusCount.getOrDefault(assetStatusCount.key, null) == null)
        	{
        		dashboardDTO.keyStatusCount.put(assetStatusCount.key, new int[2]);
        	}
    		
    		//System.out.println("cat " + assetStatusCount.key + " status = " + assetStatusCount.key2 + " count = " + assetStatusCount.count);
        	
    		int[] catWiseCount = dashboardDTO.keyStatusCount.get(assetStatusCount.key);
        	if(assetStatusCount.key2 == 1)
        	{
        		catWiseCount[DashboardDTO.ASSIGNED_POS] = assetStatusCount.count;
        	}
        	else
        	{
        		catWiseCount[DashboardDTO.NOT_ASSIGNED_POS] = assetStatusCount.count;
        	}
        	

        }
      
	}
	
	public void getCategoryWiseStatusCount(DashboardDTO dashboardDTO)
	{
		List<KeyCountDTO> assetCatStatusCounts = assetAssigneeDAO.getAssetCategoryStatusCount();
		dashboardDTO.assetCategoryStatusCount = new ConcurrentHashMap<>();
        for(KeyCountDTO assetStatusCount: assetCatStatusCounts)
        {        	
    		if(dashboardDTO.assetCategoryStatusCount.getOrDefault(assetStatusCount.key, null) == null)
        	{
        		dashboardDTO.assetCategoryStatusCount.put(assetStatusCount.key, new int[4]);
        		//System.out.println("key created for " + assetStatusCount.key);
        	}
    		
    		//System.out.println("cat " + assetStatusCount.key + " status = " + assetStatusCount.key2 + " count = " + assetStatusCount.count);
        	
    		int[] catWiseCount = dashboardDTO.assetCategoryStatusCount.get(assetStatusCount.key);
        	if(assetStatusCount.key2 == AssetAssigneeDTO.ASSIGNED)
        	{
        		catWiseCount[DashboardDTO.ASSIGNED_POS] = assetStatusCount.count;
        	}
        	else if(assetStatusCount.key2 == AssetAssigneeDTO.NOT_ASSIGNED)
        	{
        		catWiseCount[DashboardDTO.NOT_ASSIGNED_POS] = assetStatusCount.count;
        	}
        	else if(assetStatusCount.key2 == AssetAssigneeDTO.CONDEMNED)
        	{
        		catWiseCount[DashboardDTO.CONDEMNED_POS] = assetStatusCount.count;
        	}
        	else
        	{
        		catWiseCount[DashboardDTO.OTHER_POS] = assetStatusCount.count;
        	}

        }
      
	}
	
	public void getTypewiseTickets(DashboardDTO dashboardDTO) 
	{
        List<KeyCountDTO> typewiseCounts = support_ticketDAO.getTypewiseCount();
        dashboardDTO.typewiseCompplaintCount = new long[typewiseCounts.size()];
        dashboardDTO.ticketTypeEn = new String[typewiseCounts.size()];
        dashboardDTO.ticketTypeBn = new String[typewiseCounts.size()];
        dashboardDTO.ticketTypeId = new long[typewiseCounts.size()];
        int i = 0;
        if(typewiseCounts != null)
        {
	        for(KeyCountDTO typewiseCount: typewiseCounts)
	        {
	        	dashboardDTO.typewiseCompplaintCount[i] = typewiseCount.count;
	        	dashboardDTO.ticketTypeEn[i] = PBNameRepository.getInstance().getName("English", "ticket_issues", typewiseCount.key);
	        	dashboardDTO.ticketTypeBn[i] = PBNameRepository.getInstance().getName("Bangla", "ticket_issues", typewiseCount.key);
	        	dashboardDTO.ticketTypeId[i] = typewiseCount.key;
	        	i++;
	        }
        }
        dashboardDTO.issueTypeCount = i;
	 }
	
	public void getWingwiseTickets(DashboardDTO dashboardDTO, UserDTO userDTO) 
	{
        List<KeyCountDTO> unitwiseCounts = support_ticketDAO.getWingwiseCount(userDTO);
        dashboardDTO.unitWiseCompplaintCount = new long[unitwiseCounts.size()];
        dashboardDTO.unitNameEn = new String[unitwiseCounts.size()];
        dashboardDTO.unitNameBn = new String[unitwiseCounts.size()];
        int i = 0;
        if(unitwiseCounts!= null)
        {
        	for(KeyCountDTO unitwiseCount: unitwiseCounts)
            {
            	dashboardDTO.unitWiseCompplaintCount[i] = unitwiseCount.count;           	
            	dashboardDTO.unitNameEn[i] = unitwiseCount.nameEn;
            	dashboardDTO.unitNameBn[i] = unitwiseCount.nameBn;           	
            	i++;
            }
        }        
        dashboardDTO.issueTypeCount = i;
	 }
	
	 public void getPopularDrugs(Drug_informationDAO drug_informationDAO, DashboardDTO dashboardDTO) 
	 {		
        List<Drug_informationDTO> popularDrugs = drug_informationDAO.getMostPopularDrugs(TimeConverter.get1stDayOfNthtMonth(-6), TimeConverter.getNthDay(1));
        System.out.println("getPopularDrugs " + popularDrugs.size());
        for (int i = 0; i < popularDrugs.size(); i++) {
            dashboardDTO.popularDrugNames[i] = popularDrugs.get(i).nameEn;
            dashboardDTO.popularDrugCount[i] = popularDrugs.get(i).currentStock;
        }
	  }

	public void getProfitableDrugs(Drug_informationDAO drug_informationDAO, DashboardDTO dashboardDTO) 
    {
        List<Drug_informationDTO> profitableDrugs = drug_informationDAO.getMostProfitableDrugs(TimeConverter.get1stDayOfNthtMonth(-6), TimeConverter.getNthDay(1));
        System.out.println("getMostProfitableDrugs " + profitableDrugs.size());
        for (int i = 0; i < profitableDrugs.size(); i++) {
            dashboardDTO.profitableDrugNames[i] = profitableDrugs.get(i).nameEn;
            dashboardDTO.pritableDrugMoney[i] = profitableDrugs.get(i).unitPrice;
        }
    }
	
	public void getReceptionistWiseCountToday (DashboardDTO dashboardDTO)
	{
		dashboardDTO.receptionistWiseCountToday = appointmentDAO.getReceptionistAppointmentCountToday();
		Set<Long> receptionists = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.MEDICAL_RECEPTIONIST_ROLE);
		for(long receptionist: receptionists)
		{
			if(dashboardDTO.receptionistWiseCountToday
					.parallelStream()
					.filter(e -> e.key == receptionist)
					.collect(Collectors.toList()).isEmpty()
					)
			{
				logger.debug("receptionist not in list: " + receptionist);
				UserDTO recDTO = UserRepository.getUserDTOByOrganogramID(receptionist);
				if(recDTO != null)
				{
					dashboardDTO.receptionistWiseCountToday.add(new KeyCountDTO(receptionist, recDTO.userName, 0));
				}
			}
			else
			{
				logger.debug("receptionist in list: " + receptionist);
			}
		}
		for(KeyCountDTO keyCountDTO: dashboardDTO.receptionistWiseCountToday)
		{
			UserDTO recDTO = UserRepository.getUserDTOByUserName(keyCountDTO.keyStr);
			if(recDTO != null)
			{
				keyCountDTO.exists = user.LoginSessionListener.userActive(recDTO);
			}
		}
	}
	
	public void getReceptionistWiseCount (DashboardDTO dashboardDTO)
	{
		dashboardDTO.receptionistWiseCount = appointmentDAO.getReceptionistAppointmentCount();					
	}
	
	public void getDrWisePrescriptions(List<KeyCountDTO> drWiseList, ArrayList<String> doctorNames, ArrayList<Long> drIds,
			 ArrayList<Long> doctorWiseServedPatients,  ArrayList<Long> doctorWiseAppointments,
			 UserDTO userDTO)
	{
		for(KeyCountDTO drCount: drWiseList)
        {

        	UserDTO drDTO = UserRepository.getUserDTOByUserNameOnlyFromRepo(drCount.keyStr);
        	if(drDTO != null)
        	{
        		Employee_recordsDTO erDTO = Employee_recordsRepository.getInstance().getById(drDTO.employee_record_id);
        		if(erDTO != null
        				&& erDTO.nameBng != null && !erDTO.nameBng.equalsIgnoreCase("")
        				&& erDTO.nameEng != null && !erDTO.nameEng.equalsIgnoreCase(""))
        		{        			
    				if(userDTO.languageID == SessionConstants.BANGLA)
        			{
        				doctorNames.add(erDTO.nameBng);
        			}
        			else
        			{
        				doctorNames.add(erDTO.nameEng);
        			}
    				drIds.add(drDTO.organogramID);
        			doctorWiseServedPatients.add((long) drCount.count2);
                	doctorWiseAppointments.add((long) drCount.count) ;
        		}
        		
        	}
        }
	}
	
	public void getDrWisePrescriptions(DashboardDTO dashboardDTO, UserDTO userDTO)
    {
        List<KeyCountDTO> drWiseList = appointmentDAO.getDrAppointmentPrescriptionCount();
        dashboardDTO.doctorNames = new ArrayList<String>();
        dashboardDTO.drIds = new ArrayList<Long>();
        dashboardDTO.doctorWiseServedPatients = new ArrayList<Long>();
        dashboardDTO.doctorWiseAppointments = new ArrayList<Long>();
        getDrWisePrescriptions(drWiseList, dashboardDTO.doctorNames, dashboardDTO.drIds,
        		dashboardDTO.doctorWiseServedPatients,  dashboardDTO.doctorWiseAppointments,
			    userDTO);
        
    }
	
	public void getDrWisePrescriptionsToday(DashboardDTO dashboardDTO, UserDTO userDTO)
    {
        List<KeyCountDTO> drWiseList = appointmentDAO.getDrAppointmentPrescriptionCountToday();
        dashboardDTO.doctorNamesToday = new ArrayList<String>();
        dashboardDTO.drIdsToday = new ArrayList<Long>();
        dashboardDTO.doctorWiseServedPatientsToday = new ArrayList<Long>();
        dashboardDTO.doctorWiseAppointmentsToday = new ArrayList<Long>();
        dashboardDTO.drActiveToday = new ArrayList<Boolean>();
        getDrWisePrescriptions(drWiseList, dashboardDTO.doctorNamesToday, dashboardDTO.drIdsToday,
        		dashboardDTO.doctorWiseServedPatientsToday,  dashboardDTO.doctorWiseAppointmentsToday,
			    userDTO);
        
        boolean isLangEng = userDTO.languageID == SessionConstants.ENGLISH;
        
        List<Doctor_time_slotDTO> dtDTOsToday = doctor_time_slotDAO.getSlotDTOsToday();
        for(Doctor_time_slotDTO dtDTO: dtDTOsToday)
        {
        	
        	if(!dashboardDTO.drIdsToday.contains(dtDTO.doctorId))
        	{
        		
        		UserDTO drDTO = UserRepository.getUserDTOByOrganogramID(dtDTO.doctorId);
            	if(drDTO != null)
            	{
            		Employee_recordsDTO erDTO = Employee_recordsRepository.getInstance().getById(drDTO.employee_record_id);
            		if(erDTO != null
            				&& erDTO.nameBng != null && !erDTO.nameBng.equalsIgnoreCase("")
            				&& erDTO.nameEng != null && !erDTO.nameEng.equalsIgnoreCase(""))
            		{        			
        				if(userDTO.languageID == SessionConstants.BANGLA)
            			{
        					dashboardDTO.doctorNamesToday.add(erDTO.nameBng);
            			}
            			else
            			{
            				dashboardDTO.doctorNamesToday.add(erDTO.nameEng);
            			}
        				dashboardDTO.drIdsToday.add(drDTO.organogramID);
        				dashboardDTO.doctorWiseServedPatientsToday.add(0L);
        				dashboardDTO.doctorWiseAppointmentsToday.add(0L);
            		}
            		
            	}
        	}
        }
        
        for(String name: dashboardDTO.doctorNamesToday)
        {
        	logger.debug("dr name today = " + name);
        }
        
        for(long drId: dashboardDTO.drIdsToday)
        {
        	UserDTO drDTO = UserRepository.getUserDTOByOrganogramID(drId);
			if(drDTO != null)
			{
				dashboardDTO.drActiveToday.add(user.LoginSessionListener.userActive(drDTO));
			}
			else
			{
				dashboardDTO.drActiveToday.add(false);
			}
        }

    }
	
	public void getFromPersonalStock(DashboardDTO dashboardDTO) {

		dashboardDTO.organizationWizeCountCost = new ArrayList<KeyCountDTO>();
		
		dashboardDTO.organizationWizeCountCost.add(new KeyCountDTO(Organization.NATIONAL_PARLIAMENT.getId(),Organization.NATIONAL_PARLIAMENT.getNameEn(), Organization.NATIONAL_PARLIAMENT.getNameBn()));
		dashboardDTO.organizationWizeCountCost.add(new KeyCountDTO(Organization.NATIONAL_PARLIAMENT_SECRETARIAT.getId(),Organization.NATIONAL_PARLIAMENT_SECRETARIAT.getNameEn(), Organization.NATIONAL_PARLIAMENT_SECRETARIAT.getNameBn()));
		dashboardDTO.organizationWizeCountCost.add(new KeyCountDTO(Organization.OTHER.getId(),Organization.OTHER.getNameEn(), Organization.OTHER.getNameBn()));
		
		dashboardDTO.drugohololics = new ArrayList<KeyCountDTO>();
		
		List<KeyCountDTO> keyCountDTOs = prescription_detailsDAO.getUserWiseCountCosts();
		int i = 0;
		for(KeyCountDTO keyCountDTO: keyCountDTOs)
		{
			if(i < 10)
			{
				dashboardDTO.drugohololics.add(new KeyCountDTO(keyCountDTO.keyStr, keyCountDTO.cost));
			}
			if(WorkflowController.getOrganizationIdFromOrganogramId(keyCountDTO.key) == Organization.NATIONAL_PARLIAMENT.getId())
			{
				dashboardDTO.organizationWizeCountCost.get(DashboardDTO.NATIONAL_PARLIAMENT_INDEX).cost += keyCountDTO.cost;
				dashboardDTO.organizationWizeCountCost.get(DashboardDTO.NATIONAL_PARLIAMENT_INDEX).count += keyCountDTO.count;
			}
			else if(WorkflowController.getOrganizationIdFromOrganogramId(keyCountDTO.key) == Organization.NATIONAL_PARLIAMENT_SECRETARIAT.getId())
			{
				dashboardDTO.organizationWizeCountCost.get(DashboardDTO.NATIONAL_PARLIAMENT_SECRETARIAT_INDEX).cost += keyCountDTO.cost;
				dashboardDTO.organizationWizeCountCost.get(DashboardDTO.NATIONAL_PARLIAMENT_SECRETARIAT_INDEX).count += keyCountDTO.count;
			}
			else
			{
				dashboardDTO.organizationWizeCountCost.get(DashboardDTO.OTHER_INDEX).cost += keyCountDTO.cost;
				dashboardDTO.organizationWizeCountCost.get(DashboardDTO.OTHER_INDEX).count += keyCountDTO.count;
			}			
			i++;
		}
		
	}
	public void getCUM(DashboardDTO dashboardDTO) {
		dashboardDTO.otherOfficeCountCost = prescription_detailsDAO.getOtherOfficeCountCosts();
		
	}
	
	public void getDental(DashboardDTO dashboardDTO) {
		dashboardDTO.dentalDay = Dental_activitiesDAO.getInstance().getCatWiseCount(TimeConverter.getToday());
		dashboardDTO.dentalForever = Dental_activitiesDAO.getInstance().getCatWiseCount(-1);
	}
	
	
	public void getSpecial(DashboardDTO dashboardDTO) {
		dashboardDTO.specialCostCollections = new ArrayList<KeyCountDTO>();
		dashboardDTO.specialCostCollections.add(
				new KeyCountDTO
				(
						"National Parliament",
						"জাতীয় সংসদ", 
						dashboardDTO.organizationWizeCountCost.get(DashboardDTO.NATIONAL_PARLIAMENT_INDEX).cost
				));
		dashboardDTO.specialCostCollections.add(
				new KeyCountDTO
				(
						"National Parliament Secretariat",
						"জাতীয় সংসদ সচিবালয়", 
						dashboardDTO.organizationWizeCountCost.get(DashboardDTO.NATIONAL_PARLIAMENT_SECRETARIAT_INDEX).cost
				));
		
		double otherOrgCost = dashboardDTO.organizationWizeCountCost.get(DashboardDTO.OTHER_INDEX).cost;
		
		double pwdCost = 0;
		double securityCost = 0;
		double remainingCost = otherOrgCost;
		if(dashboardDTO.otherOfficeCountCost != null)
		{
			for(KeyCountDTO keyCountDTO: dashboardDTO.otherOfficeCountCost)
			{
				if(keyCountDTO.key == SessionConstants.OTHER_OFFICE_PWD)
				{
					pwdCost += keyCountDTO.cost;
					remainingCost -= keyCountDTO.cost;
				}
				else if(keyCountDTO.key == SessionConstants.OTHER_OFFICE_POLICE || keyCountDTO.key == SessionConstants.OTHER_OFFICE_ANSAR)
				{
					securityCost += keyCountDTO.cost;
					remainingCost -= keyCountDTO.cost;
				}
			}
		}
		
		System.out.println("otherOrgCost = " + otherOrgCost + " pwdCost = " + pwdCost + " remainingCost = " + remainingCost);
		
		dashboardDTO.specialCostCollections.add(
				new KeyCountDTO
				(
						"PWD",
						"পি ডাব্লিউ ডি", 
						pwdCost
				));
		
		dashboardDTO.specialCostCollections.add(
				new KeyCountDTO
				(
						"Security",
						"নিরাপত্তা", 
						securityCost
				));
		
		dashboardDTO.specialCostCollections.add(
				new KeyCountDTO
				(
						"Others",
						"অন্যান্য", 
						remainingCost
				));
	}
	
	
	


}
