package dashboard;

public class DocumentSummaryDTO {

    public Long incomingCount;
    public Long outgoingCount;
    public Long archivedCount;
    public Long totalCount;

    public DocumentSummaryDTO(){

        this.incomingCount = 0L;
        this.outgoingCount = 0L;
        this.archivedCount = 0L;
        this.totalCount = 0L;
    }
}
