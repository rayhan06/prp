package dashboard;


public class DocumentTypeCatDTO {

    public Long drawingCount;
    public Long letterCount;
    public Long approvedRfiCount;
    public Long rejectedRfiCount;
    public Long otherRfiCount;
    public Long totalRfiCount;
    public Long qsdrCount;
    public Long ncnCount;
    public Long hseCount;
    public Long ncrCount;
    public Long edmsCount;

    public DocumentTypeCatDTO() {

        this.drawingCount = 0L;
        this.letterCount = 0L;
        this.qsdrCount = 0L;
        this.ncnCount = 0L;
        this.hseCount = 0L;
        this.ncrCount = 0L;
    }
}
