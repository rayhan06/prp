package dashboard;

public class DayMonthYearDTO {

    public Integer day;
    public Integer month;
    public Integer year;

    public DayMonthYearDTO(){

        this.day = 0;
        this.month = 0;
        this.year = 0;
    }

    public DayMonthYearDTO(Integer day, Integer month, Integer year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
}
