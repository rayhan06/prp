package incident;

import common.NameRepository;

public class IncidentRepository extends NameRepository {

	private static class LazyLoader{
		static IncidentRepository INSTANCE = new IncidentRepository();
	}

	public static IncidentRepository getInstance(){
		return LazyLoader.INSTANCE;
	}

	private IncidentRepository(){
		super(IncidentDAO.getInstance(), IncidentRepository.class);
	}
}


