package incident;

import common.BaseServlet;
import common.NameDao;
import common.NameInterface;
import common.NameRepository;
import language.LC;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

@WebServlet("/IncidentServlet")
@MultipartConfig
public class IncidentServlet extends BaseServlet implements NameInterface {

	public String commonPartOfDispatchURL(){
		return  "common/name";
	}

	@Override
	public String getTableName() {
		return IncidentDAO.getInstance().getTableName();
	}

	@Override
	public String getServletName() {
		return "IncidentServlet";
	}

	@Override
	public NameDao getCommonDAOService() {
		return IncidentDAO.getInstance();
	}

	@Override
	public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
		return addT(request,addFlag,userDTO,IncidentDAO.getInstance());
	}

	@Override
	public int[] getAddPageMenuConstants() {
		return new int[] {MenuConstants.INCIDENT_ADD};
	}

	@Override
	public int[] getEditPageMenuConstants() {
		return new int[] {MenuConstants.INCIDENT_UPDATE};
	}

	@Override
	public int[] getSearchMenuConstants() {
		return new int[] {MenuConstants.INCIDENT_SEARCH};
	}

	@Override
	public Class<? extends HttpServlet> getClazz() {
		return IncidentServlet.class;
	}

	@Override
	public int getSearchTitleValue() {
		return LC.INCIDENT_SEARCH_INCIDENT_SEARCH_FORMNAME;
	}

	@Override
	public String get_p_navigatorName() {
		return SessionConstants.NAV_INCIDENT;
	}

	@Override
	public String get_p_dtoCollectionName() {
		return SessionConstants.VIEW_INCIDENT;
	}

	@Override
	public NameRepository getNameRepository() {
		return IncidentRepository.getInstance();
	}

	@Override
	public int getAddTitleValue() {
		return LC.INCIDENT_ADD_INCIDENT_ADD_FORMNAME;
	}

	@Override
	public int getEditTitleValue() {
		return LC.INCIDENT_EDIT_INCIDENT_EDIT_FORMNAME;
	}

	@Override
	public void init(HttpServletRequest request) throws ServletException {
		setCommonAttributes(request,getServletName(),getCommonDAOService());
	}
}