package incident;

import common.NameDao;

public class IncidentDAO extends NameDao {

    private IncidentDAO() {
        super("incident");
    }

    private static class LazyLoader{
        static final IncidentDAO INSTANCE = new IncidentDAO();
    }

    public static IncidentDAO getInstance(){
        return LazyLoader.INSTANCE;
    }
}
	