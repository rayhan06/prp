package pi_package_new;

import com.google.gson.Gson;
import common.ApiResponse;
import common.BaseServlet;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import pi_annual_demand.PiAnnualDemandChildDAO;
import pi_annual_demand.PiAnnualDemandChildDTO;
import pi_annual_demand.Pi_annual_demandDAO;
import pi_annual_demand.Pi_annual_demandDTO;
import pi_package_lot.Pi_package_lotDAO;
import pi_package_lot.Pi_package_lotDTO;
import user.UserDTO;
import util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@WebServlet("/Pi_package_newServlet")
@MultipartConfig
public class Pi_package_newServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_package_newServlet.class);

    @Override
    public String getTableName() {
        return Pi_package_newDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_package_newServlet";
    }

    @Override
    public Pi_package_newDAO getCommonDAOService() {
        return Pi_package_newDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PI_PACKAGE_NEW_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PI_PACKAGE_NEW_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PI_PACKAGE_NEW_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_package_newServlet.class;
    }

    private final Gson gson = new Gson();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        init(request);
        try {
            switch (request.getParameter("actionType")) {
                case "search":
                    if (Utils.checkPermission(commonLoginData.userDTO, getSearchMenuConstants()) && getSearchPagePermission(request)) {
                        Map<String, String> extraCriteriaMap = new HashMap<>();
                        extraCriteriaMap.put("office_unit_id", String.valueOf(commonLoginData.userDTO.unitID));
                        request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                        search(request, response);
                        return;
                    }
                    break;
                case "deletePackageWithValidation":
                    if (getDeletePermission(request)) {
                        String packageIdStr = request.getParameter("packageId");
                        if (packageIdStr == null) {
                            UtilCharacter.throwException("ডিলিট ব্যর্থ হয়েছে!", "Delete Failed!");
                        }
                        Long packageId = Long.parseLong(packageIdStr);
                        List<Pi_annual_demandDTO> DTOs = (List<Pi_annual_demandDTO>) Pi_annual_demandDAO.getInstance()
                                .getDTOsByParent("pi_package_new_id", packageId);
                        if (DTOs != null && DTOs.size() == 0) {
                            List<Pi_package_lotDTO> lots = (List<Pi_package_lotDTO>) Pi_package_lotDAO.getInstance()
                                    .getDTOsByParent("pi_package_new_id", packageId);
                            if (lots != null) {
                                for (Pi_package_lotDTO lot : lots) {
                                    Pi_package_lotDAO.getInstance().delete(commonLoginData.userDTO.employee_record_id, lot.iD);
                                }
                            }
                            Pi_package_newDAO.getInstance().delete(commonLoginData.userDTO.employee_record_id, packageId);
                            String successMessage = UtilCharacter.getDataByLanguage(Language, "ডিলিট সফল হয়েছে", "Delete Successful");
                            ApiResponse.sendSuccessResponse(response, successMessage);
                        } else {
                            Pi_package_newDTO packageModel = Pi_package_newDAO.getInstance().getDTOByID(packageId);
                            String errorMessage = UtilCharacter.getDataByLanguage(Language, packageModel.packageNumberBn + " ডিলিট করা যাবে না। কারণ এই প্যাকেজ বার্ষিক চাহিদা তে ব্যবহার করা হচ্ছে!",
                                    packageModel.packageNumberEn + " can not be deleted. Because this package is used in annual demand!");
                            ApiResponse.sendErrorResponse(response, errorMessage);
                        }
                    }
                    break;
                case "deleteLotWithValidation":
                    if (getDeletePermission(request)) {
                        String lotStr = request.getParameter("lotId");
                        if (lotStr == null) {
                            String errorMessage = UtilCharacter.getDataByLanguage(Language, "লট ডিলিট ব্যর্থ হয়েছে!", "Lot Delete Failed!");
                            ApiResponse.sendErrorResponse(response, errorMessage);
                        }
                        Long lotId = Long.parseLong(lotStr);
                        List<Pi_annual_demandDTO> DTOs = (List<Pi_annual_demandDTO>) Pi_annual_demandDAO.getInstance()
                                .getDTOsByParent("pi_lot_id", lotId);
                        if (DTOs != null && DTOs.size() == 0) {
                            Pi_package_lotDAO.getInstance().delete(commonLoginData.userDTO.employee_record_id, lotId);
                            String successMessage = UtilCharacter.getDataByLanguage(Language, "লট ডিলিট সফল হয়েছে", "Lot Delete Successful");
                            ApiResponse.sendSuccessResponse(response, successMessage);
                        } else {
                            Pi_package_lotDTO lotModel = Pi_package_lotDAO.getInstance().getDTOByID(lotId);
                            String errorMessage = UtilCharacter.getDataByLanguage(Language,
                                    lotModel.lotNumberBn + " ডিলিট করা যাবে না। কারণ এটা বার্ষিক চাহিদা তে ব্যবহার করা হচ্ছে!",
                                    lotModel.lotNumberEn + "  can not be deleted. Because it is used in annual demand!");
                            ApiResponse.sendErrorResponse(response, errorMessage);
                        }
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        super.doGet(request, response);
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        request.setAttribute("failureMessage", "");
        Pi_package_newDTO pi_package_newDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLanEng = Language.equalsIgnoreCase("English");

        if (addFlag) {
            pi_package_newDTO = new Pi_package_newDTO();
            pi_package_newDTO.insertionDate = pi_package_newDTO.lastModificationTime = System.currentTimeMillis();
            pi_package_newDTO.insertedBy = pi_package_newDTO.modifiedBy = String.valueOf(userDTO.ID);
        } else {
            pi_package_newDTO = Pi_package_newDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
            if (pi_package_newDTO == null) {
                throw new Exception(isLanEng ? "Package information is not found" : "প্যাকেজ তথ্য খুঁজে পাওয়া যায়নি");
            }
            pi_package_newDTO.lastModificationTime = System.currentTimeMillis();
            pi_package_newDTO.modifiedBy = String.valueOf(userDTO.ID);
        }

        String Value = "";
        Value = request.getParameter("officeUnitId");
        if (StringUtils.isValidString(Value)) {
            pi_package_newDTO.officeUnitId = Long.parseLong(Value);
        } else {
            pi_package_newDTO.officeUnitId = -1;
        }


        Value = request.getParameter("hasLot");
        pi_package_newDTO.hasLot = StringUtils.isValidString(Value);


        Value = request.getParameter("packageNumberEn");
        if (StringUtils.isValidString(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            pi_package_newDTO.packageNumberEn = (Value);
        }


        Value = request.getParameter("packageNumberBn");
        if (StringUtils.isValidString(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            pi_package_newDTO.packageNumberBn = (Value);
        }


        Value = request.getParameter("packageNameEn");
        if (StringUtils.isValidString(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            pi_package_newDTO.packageNameEn = (Value);
        }


        Value = request.getParameter("packageNameBn");
        if (StringUtils.isValidString(Value) && !pi_package_newDTO.hasLot) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            pi_package_newDTO.packageNameBn = (Value);
        }


        validatePackage(pi_package_newDTO, addFlag);
        validateLots(pi_package_newDTO, request);

        long returnedID = -1;

        if (addFlag) {
            returnedID = Pi_package_newDAO.getInstance().add(pi_package_newDTO);
            pi_package_newDTO.iD = returnedID;
        } else {
            // IF THERE IS LOT, USER CAN NOT GIVE PACKAGE NAME AND NUMBER
            List<Pi_package_lotDTO> lotDTOs = (List<Pi_package_lotDTO>) Pi_package_lotDAO.getInstance()
                    .getDTOsByParent("pi_package_new_id", pi_package_newDTO.iD);
            if (lotDTOs != null && lotDTOs.size() > 0) {
                pi_package_newDTO.hasLot = true;
                pi_package_newDTO.packageNameEn = "";
                pi_package_newDTO.packageNameBn = "";
            }
            returnedID = Pi_package_newDAO.getInstance().update(pi_package_newDTO);
        }

        List<Pi_package_lotDTO> newLotDTOs = createPiPackageLotDTOListByRequest(request, pi_package_newDTO);
        List<Pi_package_lotDTO> oldLotDTOs = (List<Pi_package_lotDTO>) Pi_package_lotDAO.getInstance()
                .getDTOsByParent("pi_package_new_id", pi_package_newDTO.iD);
        // IDS WITH -1 ARE FOR ADD
        List<Pi_package_lotDTO> lotDTOsAdd = newLotDTOs.stream().filter(dto -> dto.iD == -1).collect(Collectors.toList());
        // COMMON DTOS ARE FOR EDIT
        List<Pi_package_lotDTO> lotDTOsEdit = newLotDTOs.stream().filter(dto -> fieldContains(oldLotDTOs, dto.iD)).collect(Collectors.toList());
        // IDS NOT COMMON AND NOT -1 ARE FOR DELETE
        List<Pi_package_lotDTO> lotDTOsDelete = oldLotDTOs.stream().filter(dto -> !fieldContains(lotDTOsEdit, dto.iD)).collect(Collectors.toList());

        for (Pi_package_lotDTO lotDTO : lotDTOsAdd) {
            lotDTO.insertedBy = lotDTO.modifiedBy = String.valueOf(userDTO.employee_record_id);
            lotDTO.insertionDate = lotDTO.lastModificationTime = System.currentTimeMillis();
            Pi_package_lotDAO.getInstance().add(lotDTO);
        }
        for (Pi_package_lotDTO lotDTO : lotDTOsEdit) {
            lotDTO.modifiedBy = String.valueOf(userDTO.employee_record_id);
            lotDTO.lastModificationTime = System.currentTimeMillis();
            Pi_package_lotDAO.getInstance().update(lotDTO);
        }
        for (Pi_package_lotDTO lotDTO : lotDTOsDelete) {
            // FILTER OUT LOT IF USED IN ANNUAL DEMAND
            List<PiAnnualDemandChildDTO> dtos = (List<PiAnnualDemandChildDTO>) PiAnnualDemandChildDAO.getInstance()
                    .getDTOsByParent("pi_lot_id", lotDTO.iD);
            if (dtos != null && dtos.size() > 0)
                UtilCharacter.throwException(lotDTO.lotNumberBn + " ডিলিট করা যাবে না। কারণ এটা বার্ষিক চাহিদায় ব্যবহার হচ্ছে!",
                        lotDTO.lotNumberEn + "  can not be deleted. Because it is used in annual demand!");

            Pi_package_lotDAO.getInstance().delete(userDTO.employee_record_id, lotDTO.iD, System.currentTimeMillis());
        }

        return pi_package_newDTO;
    }

    private List<Pi_package_lotDTO> createPiPackageLotDTOListByRequest(HttpServletRequest request,
                                                                       Pi_package_newDTO pi_package_newDTO) {
        List<Pi_package_lotDTO> pi_package_lotDTOList = new ArrayList<>();

        int lotCount = getLotCount(request);

        for (int index = 0; index < lotCount; index++) {
            Pi_package_lotDTO lotDTO = createPiPackageLotDTOByRequestAndIndex(request, index);
            lotDTO.officeUnitId = pi_package_newDTO.officeUnitId;
            lotDTO.piPackageNewId = pi_package_newDTO.iD;
            pi_package_lotDTOList.add(lotDTO);
        }
        return pi_package_lotDTOList;
    }

    private Pi_package_lotDTO createPiPackageLotDTOByRequestAndIndex(HttpServletRequest request, int index) {
        Pi_package_lotDTO lotDTO;
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLanEng = Language.equalsIgnoreCase("English");

        lotDTO = new Pi_package_lotDTO();

        String Value = "";

        Value = request.getParameterValues("lot.iD")[index];
        if (StringUtils.isValidString(Value)) {
            lotDTO.iD = Long.parseLong(Value);
        } else {
            lotDTO.iD = -1;
        }

        Value = request.getParameterValues("insertionDate")[index];
        if (StringUtils.isValidString(Value)) {
            lotDTO.insertionDate = Long.parseLong(Value);
        }
        Value = request.getParameterValues("insertedBy")[index];
        if (StringUtils.isValidString(Value)) {
            lotDTO.insertedBy = Value;
        }

        Value = request.getParameterValues("lotNumberEn")[index];
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            lotDTO.lotNumberEn = Value;
        }


        Value = request.getParameterValues("lotNumberBn")[index];
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            lotDTO.lotNumberBn = Value;
        }


        Value = request.getParameterValues("lotNameEn")[index];
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            lotDTO.lotNameEn = Value;
        }


        Value = request.getParameterValues("lotNameBn")[index];
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            lotDTO.lotNameBn = Value;
        }

        return lotDTO;
    }

    private void validatePackage(Pi_package_newDTO piPackageNewDTO, Boolean addFlag) throws Exception {
        validateOfficeUnit(piPackageNewDTO.officeUnitId);
        validateIsPackageNumberNameEmpty(piPackageNewDTO);
        validatePackageNumberNameLanguage(piPackageNewDTO);
        validateIsPackageNumberNameDuplicate(piPackageNewDTO, addFlag);
    }

    private void validateOfficeUnit(long officeUnitId) throws Exception {
        if (officeUnitId == -1) {
            UtilCharacter.throwException("অনুগ্রহপূর্বক একটি দপ্তর নির্বাচন করুন", "Please Select any Office");
        }
    }

    private void validateIsPackageNumberNameEmpty(Pi_package_newDTO dto) throws Exception {
        if (dto.packageNumberEn.equals("")) {
            UtilCharacter.throwException("প্যাকেজের ইংরেজি নাম্বার লিখুন!", "Write package number English!");
        } else if (dto.packageNumberBn.equals("")) {
            UtilCharacter.throwException("প্যাকেজের বাংলা নাম্বার লিখুন!", "Write package number in Bangla!");
        } else if (dto.packageNameEn.equals("") && !dto.hasLot) {
            UtilCharacter.throwException("প্যাকেজের ইংরেজি নাম লিখুন!", "Write package name in English!");
        } else if (dto.packageNameBn.equals("") && !dto.hasLot) {
            UtilCharacter.throwException("প্যাকেজের বাংলা নাম লিখুন!", "Write package number in Bangla!");
        }
    }

    private void validatePackageNumberNameLanguage(Pi_package_newDTO dto) throws Exception {
        if (Utils.notEnglish(dto.packageNumberEn)) {
            UtilCharacter.throwException("প্যাকেজের নাম্বার ইংরেজিতে লিখুন!", "Write package number English");
        } else if (Utils.notBangla(dto.packageNumberBn)) {
            UtilCharacter.throwException("প্যাকেজের নাম্বার বাংলায় লিখুন!", "Write package number in Bangla");
        } else if (Utils.notEnglish(dto.packageNameEn) && !dto.hasLot) {
            UtilCharacter.throwException("প্যাকেজের নাম ইংরেজিতে লিখুন!", "Write package name in English");
        } else if (Utils.notBangla(dto.packageNameBn) && !dto.hasLot) {
            UtilCharacter.throwException("প্যাকেজের নাম বাংলায় লিখুন!", "Write package number in Bangla");
        }
    }

    private void validateIsPackageNumberNameDuplicate(Pi_package_newDTO piPackageNewDTO, Boolean addFlag) throws Exception {
        validateIsPackageNumberEnDuplicate(piPackageNewDTO, addFlag);
        validateIsPackageNumberBnDuplicate(piPackageNewDTO, addFlag);
        validateIsPackageNameEnDuplicate(piPackageNewDTO, addFlag);
        validateIsPackageNameBnDuplicate(piPackageNewDTO, addFlag);
    }

    private void validateIsPackageNameEnDuplicate(Pi_package_newDTO pi_package_newDTO, Boolean addFlag) throws Exception {
        String Value = pi_package_newDTO.packageNameEn;
        if (addFlag && Pi_package_newDAO.getInstance()
                .isPackageNameExistsForThisOffice(Value, pi_package_newDTO.officeUnitId) && !pi_package_newDTO.hasLot) {
            UtilCharacter.throwException("প্যাকেজের বাংলা নাম ইতিমধ্যে বিদ্যমান", "Package Bangla name already exist");
        } else if (!addFlag && Pi_package_newDAO.getInstance()
                .isPackageNameExistsForThisOffice(Value, pi_package_newDTO.officeUnitId, pi_package_newDTO.iD)
                && !pi_package_newDTO.hasLot) {
            UtilCharacter.throwException("প্যাকেজের বাংলা নাম ইতিমধ্যে বিদ্যমান", "Package Bangla name already exist");
        }
    }

    private void validateIsPackageNameBnDuplicate(Pi_package_newDTO pi_package_newDTO, Boolean addFlag) throws Exception {
        String Value = pi_package_newDTO.packageNameBn;
        if (addFlag && Pi_package_newDAO.getInstance()
                .isPackageNameExistsForThisOffice(Value, pi_package_newDTO.officeUnitId) && !pi_package_newDTO.hasLot) {
            UtilCharacter.throwException("প্যাকেজের বাংলা নাম ইতিমধ্যে বিদ্যমান", "Package Bangla name already exist");
        } else if (!addFlag && Pi_package_newDAO.getInstance()
                .isPackageNameExistsForThisOffice(Value, pi_package_newDTO.officeUnitId, pi_package_newDTO.iD)
                && !pi_package_newDTO.hasLot) {
            UtilCharacter.throwException("প্যাকেজের বাংলা নাম ইতিমধ্যে বিদ্যমান", "Package Bangla name already exist");
        }
    }

    private void validateIsPackageNumberEnDuplicate(Pi_package_newDTO pi_package_newDTO, Boolean addFlag)
            throws Exception {
        String Value = pi_package_newDTO.packageNumberEn;
        if (addFlag && Pi_package_newDAO.getInstance().isPackageNumberExistsForThisOffice(Value,
                pi_package_newDTO.officeUnitId)) {
            UtilCharacter.throwException("প্যাকেজের ইংরেজি নাম্বার ইতিমধ্যে বিদ্যমান", "Package English number already exist");
        } else if (!addFlag && Pi_package_newDAO.getInstance().isPackageNumberExistsForThisOffice(Value,
                pi_package_newDTO.officeUnitId, pi_package_newDTO.iD)) {
            UtilCharacter.throwException("প্যাকেজের ইংরেজি নাম্বার ইতিমধ্যে বিদ্যমান", "Package English number already exist");
        }
    }

    private void validateIsPackageNumberBnDuplicate(Pi_package_newDTO pi_package_newDTO, Boolean addFlag)
            throws Exception {
        String Value = pi_package_newDTO.packageNumberBn;
        if (addFlag && Pi_package_newDAO.getInstance().isPackageNumberExistsForThisOffice(Value,
                pi_package_newDTO.officeUnitId)) {
            UtilCharacter.throwException("প্যাকেজের বাংলা নাম্বার ইতিমধ্যে বিদ্যমান", "Package Bangla number already exist");
        } else if (!addFlag && Pi_package_newDAO.getInstance().isPackageNumberExistsForThisOffice(Value,
                pi_package_newDTO.officeUnitId, pi_package_newDTO.iD)) {
            UtilCharacter.throwException("প্যাকেজের বাংলা নাম্বার ইতিমধ্যে বিদ্যমান", "Package Bangla number already exist");
        }
    }

    private void validateLots(Pi_package_newDTO pi_package_newDTO, HttpServletRequest request) throws Exception {
        List<Pi_package_lotDTO> lots = createPiPackageLotDTOListByRequest(request, pi_package_newDTO);
        if (lots.size() == 0 && pi_package_newDTO.hasLot) {
            UtilCharacter.throwException("অন্তত একটা লট এন্ট্রি করুন!", "Give at least one lot entry!");
        }
        if (lots.size() > 5) {
            UtilCharacter.throwException("৫ টার বেশি লট তৈরি করা যাবে না", "You can not create more than 5 lot");
        }
        for (Pi_package_lotDTO lot : lots) {
            validateIsLotNumberEmpty(lot);
            validateIsLotNameEmpty(lot);
            validateLotNumberEn(lot.lotNumberEn);
            validateLotNumberBn(lot.lotNumberBn);
            validateLotNameEn(lot.lotNameEn);
            validateLotNameBn(lot.lotNameBn);
        }
    }

    private void validateIsLotNumberEmpty(Pi_package_lotDTO lot) throws Exception {
        if (lot.lotNumberEn.equals("") || lot.lotNumberBn.equals("")) {
            UtilCharacter.throwException("লটের নাম্বার লিখুন!", "Write lot number!");
        }
    }

    private void validateIsLotNameEmpty(Pi_package_lotDTO lot) throws Exception {
        if (lot.lotNameEn.equals("") || lot.lotNameBn.equals("")) {
            UtilCharacter.throwException("লটের নাম লিখুন!", "Write lot name!");
        }
    }

    private void validateLotNumberEn(String lotNumberEn) throws Exception {
        if (Utils.notEnglish(lotNumberEn)) {
            UtilCharacter.throwException("লটের ইংরেজি নাম্বার লিখুন!", "Write English number of lot!");
        }
    }

    private void validateLotNumberBn(String lotNumberBn) throws Exception {
        if (Utils.notBangla(lotNumberBn)) {
            UtilCharacter.throwException("লটের বাংলা নাম্বার লিখুন!", "Write Bangla number of lot!");
        }
    }

    private void validateLotNameEn(String lotNameEn) throws Exception {
        if (Utils.notEnglish(lotNameEn)) {
            UtilCharacter.throwException("লটের নাম ইংরেজিতে লিখুন!", "Write English name of lot!");
        }
    }

    private void validateLotNameBn(String lotNameBn) throws Exception {
        if (Utils.notBangla(lotNameBn)) {
            UtilCharacter.throwException("লটের নাম বাংলায় লিখুন!", "Write Bangla name of lot!");
        }
    }

    private boolean fieldContains(List<Pi_package_lotDTO> lotDTOs, Long id) {
        List<Pi_package_lotDTO> dto = lotDTOs.stream().filter(lotDTO -> lotDTO.iD == id).collect(Collectors.toList());
        return dto.size() > 0;
    }

    private int getLotCount(HttpServletRequest request) {
        if (request.getParameterValues("lot.iD") != null) {
            int lotCount = request.getParameterValues("lot.iD").length;
            return lotCount;
        }
        return 0;
    }
}

