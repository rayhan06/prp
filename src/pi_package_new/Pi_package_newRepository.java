package pi_package_new;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import pb.OptionDTO;
import pb.Utils;
import procurement_package.Procurement_packageDTO;
import recruitment_test_name.Recruitment_test_nameDTO;
import repository.Repository;
import repository.RepositoryManager;


public class Pi_package_newRepository implements Repository {
	Pi_package_newDAO pi_package_newDAO = null;

	private List<Pi_package_newDTO> pi_package_newDTOS;
	
	static Logger logger = Logger.getLogger(Pi_package_newRepository.class);
	Map<Long, Pi_package_newDTO>mapOfPi_package_newDTOToiD;
	Gson gson;

  
	private Pi_package_newRepository(){
		pi_package_newDAO = Pi_package_newDAO.getInstance();
		mapOfPi_package_newDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Pi_package_newRepository INSTANCE = new Pi_package_newRepository();
    }

    public static Pi_package_newRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Pi_package_newDTO> pi_package_newDTOs = pi_package_newDAO.getAllDTOs(reloadAll);
			for(Pi_package_newDTO pi_package_newDTO : pi_package_newDTOs) {
				Pi_package_newDTO oldPi_package_newDTO = getPi_package_newDTOByiD(pi_package_newDTO.iD);
				if( oldPi_package_newDTO != null ) {
					mapOfPi_package_newDTOToiD.remove(oldPi_package_newDTO.iD);
				
					
				}
				if(pi_package_newDTO.isDeleted == 0) 
				{
					
					mapOfPi_package_newDTOToiD.put(pi_package_newDTO.iD, pi_package_newDTO);
				
				}
			}
			pi_package_newDTOS = new ArrayList<>(mapOfPi_package_newDTOToiD.values());
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Pi_package_newDTO clone(Pi_package_newDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Pi_package_newDTO.class);
	}
	
	
	public List<Pi_package_newDTO> getPi_package_newList() {
		List <Pi_package_newDTO> pi_package_news = new ArrayList<Pi_package_newDTO>(this.mapOfPi_package_newDTOToiD.values());
		return pi_package_news;
	}
	
	public List<Pi_package_newDTO> copyPi_package_newList() {
		List <Pi_package_newDTO> pi_package_news = getPi_package_newList();
		return pi_package_news
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Pi_package_newDTO getPi_package_newDTOByiD( long iD){
		pi_package_newDTOS = new ArrayList<>(mapOfPi_package_newDTOToiD.values());
		return mapOfPi_package_newDTOToiD.get(iD);
	}
	
	public Pi_package_newDTO copyPi_package_newDTOByiD( long iD){
		return clone(mapOfPi_package_newDTOToiD.get(iD));
	}

	public String buildOptions(String language, Long selectedId) {
		List<OptionDTO> optionDTOList = null;
		if (pi_package_newDTOS != null && pi_package_newDTOS.size() > 0) {
			optionDTOList = pi_package_newDTOS.stream()
					.map(Pi_package_newDTO::getOptionDTO)
					.collect(Collectors.toList());
		}
		return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
	}

	public List<Pi_package_newDTO> getDTOsByIds(List<Long> ids){
		List<Pi_package_newDTO> dtos = new ArrayList<>();
		ids.forEach(i -> {
			Pi_package_newDTO dto = getPi_package_newDTOByiD(i);
			if(dto != null){
				dtos.add(dto);
			}
		});
		return dtos;
	}

	
	@Override
	public String getTableName() {
		return pi_package_newDAO.getTableName();
	}

	public String getText(String Language, Long id){
		Pi_package_newDTO dto = Pi_package_newRepository.getInstance().getPi_package_newDTOByiD(id);
		return dto == null ? "" :
				Language.equalsIgnoreCase("English") ? dto.packageNumberEn : dto.packageNumberBn;
	}
}


