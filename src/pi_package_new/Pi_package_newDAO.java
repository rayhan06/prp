package pi_package_new;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import pi_package_lot.Pi_package_lotDAO;
import pi_package_lot.Pi_package_lotDTO;
import util.*;

public class Pi_package_newDAO implements CommonDAOService<Pi_package_newDTO> {

    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private String[] columnNames = null;

    private Pi_package_newDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "office_unit_id",
                        "package_number_en",
                        "package_number_bn",
                        "package_name_en",
                        "package_name_bn",
                        "has_lot",
                        "search_column",
                        "inserted_by",
                        "modified_by",
                        "insertion_date",
                        "isDeleted",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("office_unit_id", " and (office_unit_id = ?)");
        searchMap.put("package_number_en", " and (package_number_en like ?)");
        searchMap.put("package_number_bn", " and (package_number_bn like ?)");
        searchMap.put("package_name_en", " and (package_name_en like ?)");
        searchMap.put("package_name_bn", " and (package_name_bn like ?)");
        searchMap.put("insertion_date_start", " and (insertion_date >= ?)");
        searchMap.put("insertion_date_end", " and (insertion_date <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Pi_package_newDAO INSTANCE = new Pi_package_newDAO();
    }

    public static Pi_package_newDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Pi_package_newDTO pi_package_newDTO) {
        pi_package_newDTO.searchColumn = "";
        pi_package_newDTO.searchColumn += pi_package_newDTO.packageNumberEn + " ";
        pi_package_newDTO.searchColumn += pi_package_newDTO.packageNumberBn + " ";
        pi_package_newDTO.searchColumn += pi_package_newDTO.packageNameEn + " ";
        pi_package_newDTO.searchColumn += pi_package_newDTO.packageNameBn + " ";
    }

    @Override
    public void set(PreparedStatement ps, Pi_package_newDTO pi_package_newDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(pi_package_newDTO);
        if (isInsert) {
            ps.setObject(++index, pi_package_newDTO.iD);
        }
        ps.setObject(++index, pi_package_newDTO.officeUnitId);
        ps.setObject(++index, pi_package_newDTO.packageNumberEn);
        ps.setObject(++index, pi_package_newDTO.packageNumberBn);
        ps.setObject(++index, pi_package_newDTO.packageNameEn);
        ps.setObject(++index, pi_package_newDTO.packageNameBn);
        ps.setObject(++index, pi_package_newDTO.hasLot);
        ps.setObject(++index, pi_package_newDTO.searchColumn);
        ps.setObject(++index, pi_package_newDTO.insertedBy);
        ps.setObject(++index, pi_package_newDTO.modifiedBy);
        ps.setObject(++index, pi_package_newDTO.insertionDate);
        if (isInsert) {
            ps.setObject(++index, pi_package_newDTO.isDeleted);
        }
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, pi_package_newDTO.iD);
        }
    }

    @Override
    public Pi_package_newDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Pi_package_newDTO pi_package_newDTO = new Pi_package_newDTO();
            int i = 0;
            pi_package_newDTO.iD = rs.getLong(columnNames[i++]);
            pi_package_newDTO.officeUnitId = rs.getLong(columnNames[i++]);
            pi_package_newDTO.packageNumberEn = rs.getString(columnNames[i++]);
            pi_package_newDTO.packageNumberBn = rs.getString(columnNames[i++]);
            pi_package_newDTO.packageNameEn = rs.getString(columnNames[i++]);
            pi_package_newDTO.packageNameBn = rs.getString(columnNames[i++]);
            pi_package_newDTO.hasLot = rs.getBoolean(columnNames[i++]);
            pi_package_newDTO.searchColumn = rs.getString(columnNames[i++]);
            pi_package_newDTO.insertedBy = rs.getString(columnNames[i++]);
            pi_package_newDTO.modifiedBy = rs.getString(columnNames[i++]);
            pi_package_newDTO.insertionDate = rs.getLong(columnNames[i++]);
            pi_package_newDTO.isDeleted = rs.getInt(columnNames[i++]);
            pi_package_newDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            return pi_package_newDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }


    public Pi_package_newDTO getDTOByID(long id) {
        Pi_package_newDTO pi_package_newDTO = null;
        try {
            pi_package_newDTO = getDTOFromID(id);
            if (pi_package_newDTO != null) {
                Pi_package_lotDAO lotDAO = Pi_package_lotDAO.getInstance();
                List<Pi_package_lotDTO> lotDTOs = (List<Pi_package_lotDTO>) lotDAO
                        .getDTOsByParent("pi_package_new_id", pi_package_newDTO.iD);
                pi_package_newDTO.pi_package_lotDTOS = lotDTOs;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return pi_package_newDTO;
    }

    @Override
    public String getTableName() {
        return "pi_package_new";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_package_newDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_package_newDTO) commonDTO, updateQuery, false);
    }

    public boolean isNameExists(String name) {
        List<Pi_package_newDTO> pi_package_newDTOs = Pi_package_newRepository.getInstance().getPi_package_newList();
        return pi_package_newDTOs.stream().anyMatch(e -> e.packageNameBn.equals(name) || e.packageNameEn.equals(name));
    }

    public boolean isPackageNumberExistsForThisOffice(String name, long officeUnitId) {
        List<Pi_package_newDTO> pi_package_newDTOs = Pi_package_newRepository.getInstance().getPi_package_newList();
        return pi_package_newDTOs.stream().anyMatch(e -> (e.packageNumberBn.equals(name) && e.officeUnitId == officeUnitId) ||
                (e.packageNumberEn.equals(name) && e.officeUnitId == officeUnitId));
    }

    // THIS FUNCTION CHECK FOR DUPLICATE EXCLUDING THE CURRENT PACKAGE.
    public boolean isPackageNumberExistsForThisOffice(String name, long officeUnitId, long currentId) {
        List<Pi_package_newDTO> pi_package_newDTOs = Pi_package_newRepository.getInstance().getPi_package_newList();
        return pi_package_newDTOs.stream().filter(dto -> dto.iD != currentId).anyMatch(e -> (e.packageNumberBn.equals(name) && e.officeUnitId == officeUnitId) ||
                (e.packageNumberEn.equals(name) && e.officeUnitId == officeUnitId));
    }

    public boolean isPackageNameExistsForThisOffice(String name, long officeUnitId) {
        List<Pi_package_newDTO> pi_package_newDTOs = Pi_package_newRepository.getInstance().getPi_package_newList();
        return pi_package_newDTOs.stream().anyMatch(e -> (e.packageNameBn.equals(name) && e.officeUnitId == officeUnitId) ||
                (e.packageNameEn.equals(name) && e.officeUnitId == officeUnitId));
    }

    // THIS FUNCTION CHECK FOR DUPLICATE EXCLUDING THE CURRENT PACKAGE.
    public boolean isPackageNameExistsForThisOffice(String name, long officeUnitId, long currentId) {
        List<Pi_package_newDTO> pi_package_newDTOs = Pi_package_newRepository.getInstance().getPi_package_newList();
        return pi_package_newDTOs.stream().filter(dto -> dto.iD != currentId).anyMatch(e -> (e.packageNameBn.equals(name) && e.officeUnitId == officeUnitId) ||
                (e.packageNameEn.equals(name) && e.officeUnitId == officeUnitId));
    }

    public boolean isPackageNumberExists(String name, long id) {
        List<Pi_package_newDTO> pi_package_newDTOs = Pi_package_newRepository.getInstance().getPi_package_newList();
        return pi_package_newDTOs.stream().anyMatch(e -> (e.packageNumberBn.equals(name) || e.packageNumberEn.equals(name)) && e.iD != id);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

}
	