package pi_package_new;
import java.util.*;

import pb.OptionDTO;
import pi_package_lot.Pi_package_lotDTO;
import recruitment_exam_venue.RecruitmentExamVenueItemDTO;
import util.*;


public class Pi_package_newDTO extends CommonDTO
{
    public long officeUnitId = -1;
    public String packageNumberEn = "";
    public String packageNumberBn = "";
    public String packageNameEn = "";
    public String packageNameBn = "";
    public boolean hasLot = false;
    public String insertedBy = "";
    public String modifiedBy = "";
	public long insertionDate = -1;
    public List<Pi_package_lotDTO> pi_package_lotDTOS = new ArrayList<>();

    public OptionDTO getOptionDTO() {

        return new OptionDTO(
                packageNumberEn,
                packageNumberBn,
                String.format("%d", iD)
        );
    }
	
	
    @Override
	public String toString() {
            return "$Pi_package_newDTO[" +
            " iD = " + iD +
            " officeUnitId=" + officeUnitId +
            " packageNameEn = " + packageNameEn +
            " packageNameBn = " + packageNameBn +
            " searchColumn = " + searchColumn +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}