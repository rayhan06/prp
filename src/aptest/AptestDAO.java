package aptest;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.*;
import pb.*;
import user.UserDTO;

public class AptestDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public AptestDAO(String tableName)
	{
		super(tableName);
		useSafeSearch = true;
		joinSQL = "";
		approvalMaps = new AptestApprovalMAPS("aptest");
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"description",
			"job_cat",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public AptestDAO()
	{
		this("aptest");		
	}
	
	public void setSearchColumn(AptestDTO aptestDTO)
	{
		aptestDTO.searchColumn = "";
		aptestDTO.searchColumn += aptestDTO.nameEn + " ";
		aptestDTO.searchColumn += aptestDTO.nameBn + " ";
		aptestDTO.searchColumn += aptestDTO.description + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		AptestDTO aptestDTO = (AptestDTO)commonDTO;
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(aptestDTO);
		if(isInsert)
		{
			ps.setObject(++index,aptestDTO.iD);
		}
		ps.setObject(++index,aptestDTO.nameEn);
		ps.setObject(++index,aptestDTO.nameBn);
		ps.setObject(++index,aptestDTO.description);
		ps.setObject(++index,aptestDTO.jobCat);
		if(isInsert)
		{
			ps.setObject(++index, 0);
			ps.setObject(++index, lastModificationTime);
		}
	}
	
	public AptestDTO build(ResultSet rs)
	{
		try
		{
			AptestDTO aptestDTO = new AptestDTO();
			int i = 0;
			aptestDTO.iD = rs.getLong(columnNames[i++]);
			aptestDTO.nameEn = rs.getString(columnNames[i++]);
			aptestDTO.nameBn = rs.getString(columnNames[i++]);
			aptestDTO.description = rs.getString(columnNames[i++]);
			aptestDTO.jobCat = rs.getInt(columnNames[i++]);
			aptestDTO.isDeleted = rs.getInt(columnNames[i++]);
			aptestDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return aptestDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		

	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {

		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".name_en like ?";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name_en")
						|| str.equals("name_bn")
						|| str.equals("description")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".name_en like ?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("name_bn"))
					{
						AllFieldSql += "" + tableName + ".name_bn like ?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("description"))
					{
						AllFieldSql += "" + tableName + ".description like ?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		
		if(category == GETDTOS)
		{
			sql += " order by " + tableName + ".lastModificationTime desc ";
		}

		//printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}

		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		if(isPermanentTable)
		{
			return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat, objectList);
		}
		else
		{
			return getSqlWithSearchCriteriaForApprovalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat, objectList);
		}
				
    }
				
}
	