package aptest;
import java.util.*; 
import util.*; 


public class AptestDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
    public String description = "";
	
	
    @Override
	public String toString() {
            return "$AptestDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " description = " + description +
            " jobCat = " + jobCat +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}