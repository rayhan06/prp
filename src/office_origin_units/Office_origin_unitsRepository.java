package office_origin_units;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Office_origin_unitsRepository implements Repository {
    Office_origin_unitsDAO office_origin_unitsDAO = new Office_origin_unitsDAO();


    static Logger logger = Logger.getLogger(Office_origin_unitsRepository.class);
    Map<Long, Office_origin_unitsDTO> mapOfOffice_origin_unitsDTOToiD;
    Map<Integer, Set<Office_origin_unitsDTO>> mapOfOffice_origin_unitsDTOToofficeMinistryId;
    Map<Integer, Set<Office_origin_unitsDTO>> mapOfOffice_origin_unitsDTOToofficeLayerId;
    Map<Integer, Set<Office_origin_unitsDTO>> mapOfOffice_origin_unitsDTOToofficeOriginId;
    Map<String, Set<Office_origin_unitsDTO>> mapOfOffice_origin_unitsDTOTounitNameBng;
    Map<String, Set<Office_origin_unitsDTO>> mapOfOffice_origin_unitsDTOTounitNameEng;
    Map<String, Set<Office_origin_unitsDTO>> mapOfOffice_origin_unitsDTOToofficeUnitCategory;
    Map<Integer, Set<Office_origin_unitsDTO>> mapOfOffice_origin_unitsDTOToparentUnitId;
    Map<Integer, Set<Office_origin_unitsDTO>> mapOfOffice_origin_unitsDTOTounitLevel;
    Map<Boolean, Set<Office_origin_unitsDTO>> mapOfOffice_origin_unitsDTOTostatus;
    Map<Integer, Set<Office_origin_unitsDTO>> mapOfOffice_origin_unitsDTOTocreatedBy;
    Map<Integer, Set<Office_origin_unitsDTO>> mapOfOffice_origin_unitsDTOTomodifiedBy;
    Map<Long, Set<Office_origin_unitsDTO>> mapOfOffice_origin_unitsDTOTocreated;
    Map<Long, Set<Office_origin_unitsDTO>> mapOfOffice_origin_unitsDTOTomodified;


    static Office_origin_unitsRepository instance = null;

    private Office_origin_unitsRepository() {
        mapOfOffice_origin_unitsDTOToiD = new ConcurrentHashMap<>();
        mapOfOffice_origin_unitsDTOToofficeMinistryId = new ConcurrentHashMap<>();
        mapOfOffice_origin_unitsDTOToofficeLayerId = new ConcurrentHashMap<>();
        mapOfOffice_origin_unitsDTOToofficeOriginId = new ConcurrentHashMap<>();
        mapOfOffice_origin_unitsDTOTounitNameBng = new ConcurrentHashMap<>();
        mapOfOffice_origin_unitsDTOTounitNameEng = new ConcurrentHashMap<>();
        mapOfOffice_origin_unitsDTOToofficeUnitCategory = new ConcurrentHashMap<>();
        mapOfOffice_origin_unitsDTOToparentUnitId = new ConcurrentHashMap<>();
        mapOfOffice_origin_unitsDTOTounitLevel = new ConcurrentHashMap<>();
        mapOfOffice_origin_unitsDTOTostatus = new ConcurrentHashMap<>();
        mapOfOffice_origin_unitsDTOTocreatedBy = new ConcurrentHashMap<>();
        mapOfOffice_origin_unitsDTOTomodifiedBy = new ConcurrentHashMap<>();
        mapOfOffice_origin_unitsDTOTocreated = new ConcurrentHashMap<>();
        mapOfOffice_origin_unitsDTOTomodified = new ConcurrentHashMap<>();

        RepositoryManager.getInstance().addRepository(this);
    }

    public synchronized static Office_origin_unitsRepository getInstance() {
        if (instance == null) {
            instance = new Office_origin_unitsRepository();
        }
        return instance;
    }

    public void reload(boolean reloadAll) {
        try {
            List<Office_origin_unitsDTO> office_origin_unitsDTOs = office_origin_unitsDAO.getAllOffice_origin_units(reloadAll);
            for (Office_origin_unitsDTO office_origin_unitsDTO : office_origin_unitsDTOs) {
                Office_origin_unitsDTO oldOffice_origin_unitsDTO = getOffice_origin_unitsDTOByID(office_origin_unitsDTO.iD);
                if (oldOffice_origin_unitsDTO != null) {
                    mapOfOffice_origin_unitsDTOToiD.remove(oldOffice_origin_unitsDTO.iD);

                    if (mapOfOffice_origin_unitsDTOToofficeMinistryId.containsKey(oldOffice_origin_unitsDTO.officeMinistryId)) {
                        mapOfOffice_origin_unitsDTOToofficeMinistryId.get(oldOffice_origin_unitsDTO.officeMinistryId).remove(oldOffice_origin_unitsDTO);
                    }
                    if (mapOfOffice_origin_unitsDTOToofficeMinistryId.get(oldOffice_origin_unitsDTO.officeMinistryId).isEmpty()) {
                        mapOfOffice_origin_unitsDTOToofficeMinistryId.remove(oldOffice_origin_unitsDTO.officeMinistryId);
                    }

                    if (mapOfOffice_origin_unitsDTOToofficeLayerId.containsKey(oldOffice_origin_unitsDTO.officeLayerId)) {
                        mapOfOffice_origin_unitsDTOToofficeLayerId.get(oldOffice_origin_unitsDTO.officeLayerId).remove(oldOffice_origin_unitsDTO);
                    }
                    if (mapOfOffice_origin_unitsDTOToofficeLayerId.get(oldOffice_origin_unitsDTO.officeLayerId).isEmpty()) {
                        mapOfOffice_origin_unitsDTOToofficeLayerId.remove(oldOffice_origin_unitsDTO.officeLayerId);
                    }

                    if (mapOfOffice_origin_unitsDTOToofficeOriginId.containsKey(oldOffice_origin_unitsDTO.officeOriginId)) {
                        mapOfOffice_origin_unitsDTOToofficeOriginId.get(oldOffice_origin_unitsDTO.officeOriginId).remove(oldOffice_origin_unitsDTO);
                    }
                    if (mapOfOffice_origin_unitsDTOToofficeOriginId.get(oldOffice_origin_unitsDTO.officeOriginId).isEmpty()) {
                        mapOfOffice_origin_unitsDTOToofficeOriginId.remove(oldOffice_origin_unitsDTO.officeOriginId);
                    }

                    if (mapOfOffice_origin_unitsDTOTounitNameBng.containsKey(oldOffice_origin_unitsDTO.unitNameBng)) {
                        mapOfOffice_origin_unitsDTOTounitNameBng.get(oldOffice_origin_unitsDTO.unitNameBng).remove(oldOffice_origin_unitsDTO);
                    }
                    if (mapOfOffice_origin_unitsDTOTounitNameBng.get(oldOffice_origin_unitsDTO.unitNameBng).isEmpty()) {
                        mapOfOffice_origin_unitsDTOTounitNameBng.remove(oldOffice_origin_unitsDTO.unitNameBng);
                    }

                    if (mapOfOffice_origin_unitsDTOTounitNameEng.containsKey(oldOffice_origin_unitsDTO.unitNameEng)) {
                        mapOfOffice_origin_unitsDTOTounitNameEng.get(oldOffice_origin_unitsDTO.unitNameEng).remove(oldOffice_origin_unitsDTO);
                    }
                    if (mapOfOffice_origin_unitsDTOTounitNameEng.get(oldOffice_origin_unitsDTO.unitNameEng).isEmpty()) {
                        mapOfOffice_origin_unitsDTOTounitNameEng.remove(oldOffice_origin_unitsDTO.unitNameEng);
                    }

                    if (mapOfOffice_origin_unitsDTOToofficeUnitCategory.containsKey(oldOffice_origin_unitsDTO.officeUnitCategory)) {
                        mapOfOffice_origin_unitsDTOToofficeUnitCategory.get(oldOffice_origin_unitsDTO.officeUnitCategory).remove(oldOffice_origin_unitsDTO);
                    }
                    if (mapOfOffice_origin_unitsDTOToofficeUnitCategory.get(oldOffice_origin_unitsDTO.officeUnitCategory).isEmpty()) {
                        mapOfOffice_origin_unitsDTOToofficeUnitCategory.remove(oldOffice_origin_unitsDTO.officeUnitCategory);
                    }

                    if (mapOfOffice_origin_unitsDTOToparentUnitId.containsKey(oldOffice_origin_unitsDTO.parentUnitId)) {
                        mapOfOffice_origin_unitsDTOToparentUnitId.get(oldOffice_origin_unitsDTO.parentUnitId).remove(oldOffice_origin_unitsDTO);
                    }
                    if (mapOfOffice_origin_unitsDTOToparentUnitId.get(oldOffice_origin_unitsDTO.parentUnitId).isEmpty()) {
                        mapOfOffice_origin_unitsDTOToparentUnitId.remove(oldOffice_origin_unitsDTO.parentUnitId);
                    }

                    if (mapOfOffice_origin_unitsDTOTounitLevel.containsKey(oldOffice_origin_unitsDTO.unitLevel)) {
                        mapOfOffice_origin_unitsDTOTounitLevel.get(oldOffice_origin_unitsDTO.unitLevel).remove(oldOffice_origin_unitsDTO);
                    }
                    if (mapOfOffice_origin_unitsDTOTounitLevel.get(oldOffice_origin_unitsDTO.unitLevel).isEmpty()) {
                        mapOfOffice_origin_unitsDTOTounitLevel.remove(oldOffice_origin_unitsDTO.unitLevel);
                    }

                    if (mapOfOffice_origin_unitsDTOTostatus.containsKey(oldOffice_origin_unitsDTO.status)) {
                        mapOfOffice_origin_unitsDTOTostatus.get(oldOffice_origin_unitsDTO.status).remove(oldOffice_origin_unitsDTO);
                    }
                    if (mapOfOffice_origin_unitsDTOTostatus.get(oldOffice_origin_unitsDTO.status).isEmpty()) {
                        mapOfOffice_origin_unitsDTOTostatus.remove(oldOffice_origin_unitsDTO.status);
                    }

                    if (mapOfOffice_origin_unitsDTOTocreatedBy.containsKey(oldOffice_origin_unitsDTO.createdBy)) {
                        mapOfOffice_origin_unitsDTOTocreatedBy.get(oldOffice_origin_unitsDTO.createdBy).remove(oldOffice_origin_unitsDTO);
                    }
                    if (mapOfOffice_origin_unitsDTOTocreatedBy.get(oldOffice_origin_unitsDTO.createdBy).isEmpty()) {
                        mapOfOffice_origin_unitsDTOTocreatedBy.remove(oldOffice_origin_unitsDTO.createdBy);
                    }

                    if (mapOfOffice_origin_unitsDTOTomodifiedBy.containsKey(oldOffice_origin_unitsDTO.modifiedBy)) {
                        mapOfOffice_origin_unitsDTOTomodifiedBy.get(oldOffice_origin_unitsDTO.modifiedBy).remove(oldOffice_origin_unitsDTO);
                    }
                    if (mapOfOffice_origin_unitsDTOTomodifiedBy.get(oldOffice_origin_unitsDTO.modifiedBy).isEmpty()) {
                        mapOfOffice_origin_unitsDTOTomodifiedBy.remove(oldOffice_origin_unitsDTO.modifiedBy);
                    }

                    if (mapOfOffice_origin_unitsDTOTocreated.containsKey(oldOffice_origin_unitsDTO.created)) {
                        mapOfOffice_origin_unitsDTOTocreated.get(oldOffice_origin_unitsDTO.created).remove(oldOffice_origin_unitsDTO);
                    }
                    if (mapOfOffice_origin_unitsDTOTocreated.get(oldOffice_origin_unitsDTO.created).isEmpty()) {
                        mapOfOffice_origin_unitsDTOTocreated.remove(oldOffice_origin_unitsDTO.created);
                    }

                    if (mapOfOffice_origin_unitsDTOTomodified.containsKey(oldOffice_origin_unitsDTO.modified)) {
                        mapOfOffice_origin_unitsDTOTomodified.get(oldOffice_origin_unitsDTO.modified).remove(oldOffice_origin_unitsDTO);
                    }
                    if (mapOfOffice_origin_unitsDTOTomodified.get(oldOffice_origin_unitsDTO.modified).isEmpty()) {
                        mapOfOffice_origin_unitsDTOTomodified.remove(oldOffice_origin_unitsDTO.modified);
                    }


                }
                if (office_origin_unitsDTO.isDeleted == false) {

                    mapOfOffice_origin_unitsDTOToiD.put(office_origin_unitsDTO.iD, office_origin_unitsDTO);

                    if (!mapOfOffice_origin_unitsDTOToofficeMinistryId.containsKey(office_origin_unitsDTO.officeMinistryId)) {
                        mapOfOffice_origin_unitsDTOToofficeMinistryId.put(office_origin_unitsDTO.officeMinistryId, new HashSet<>());
                    }
                    mapOfOffice_origin_unitsDTOToofficeMinistryId.get(office_origin_unitsDTO.officeMinistryId).add(office_origin_unitsDTO);

                    if (!mapOfOffice_origin_unitsDTOToofficeLayerId.containsKey(office_origin_unitsDTO.officeLayerId)) {
                        mapOfOffice_origin_unitsDTOToofficeLayerId.put(office_origin_unitsDTO.officeLayerId, new HashSet<>());
                    }
                    mapOfOffice_origin_unitsDTOToofficeLayerId.get(office_origin_unitsDTO.officeLayerId).add(office_origin_unitsDTO);

                    if (!mapOfOffice_origin_unitsDTOToofficeOriginId.containsKey(office_origin_unitsDTO.officeOriginId)) {
                        mapOfOffice_origin_unitsDTOToofficeOriginId.put(office_origin_unitsDTO.officeOriginId, new HashSet<>());
                    }
                    mapOfOffice_origin_unitsDTOToofficeOriginId.get(office_origin_unitsDTO.officeOriginId).add(office_origin_unitsDTO);

                    if (!mapOfOffice_origin_unitsDTOTounitNameBng.containsKey(office_origin_unitsDTO.unitNameBng)) {
                        mapOfOffice_origin_unitsDTOTounitNameBng.put(office_origin_unitsDTO.unitNameBng, new HashSet<>());
                    }
                    mapOfOffice_origin_unitsDTOTounitNameBng.get(office_origin_unitsDTO.unitNameBng).add(office_origin_unitsDTO);

                    if (!mapOfOffice_origin_unitsDTOTounitNameEng.containsKey(office_origin_unitsDTO.unitNameEng)) {
                        mapOfOffice_origin_unitsDTOTounitNameEng.put(office_origin_unitsDTO.unitNameEng, new HashSet<>());
                    }
                    mapOfOffice_origin_unitsDTOTounitNameEng.get(office_origin_unitsDTO.unitNameEng).add(office_origin_unitsDTO);

                    if (!mapOfOffice_origin_unitsDTOToofficeUnitCategory.containsKey(office_origin_unitsDTO.officeUnitCategory)) {
                        mapOfOffice_origin_unitsDTOToofficeUnitCategory.put(office_origin_unitsDTO.officeUnitCategory, new HashSet<>());
                    }
                    mapOfOffice_origin_unitsDTOToofficeUnitCategory.get(office_origin_unitsDTO.officeUnitCategory).add(office_origin_unitsDTO);

                    if (!mapOfOffice_origin_unitsDTOToparentUnitId.containsKey(office_origin_unitsDTO.parentUnitId)) {
                        mapOfOffice_origin_unitsDTOToparentUnitId.put(office_origin_unitsDTO.parentUnitId, new HashSet<>());
                    }
                    mapOfOffice_origin_unitsDTOToparentUnitId.get(office_origin_unitsDTO.parentUnitId).add(office_origin_unitsDTO);

                    if (!mapOfOffice_origin_unitsDTOTounitLevel.containsKey(office_origin_unitsDTO.unitLevel)) {
                        mapOfOffice_origin_unitsDTOTounitLevel.put(office_origin_unitsDTO.unitLevel, new HashSet<>());
                    }
                    mapOfOffice_origin_unitsDTOTounitLevel.get(office_origin_unitsDTO.unitLevel).add(office_origin_unitsDTO);

                    if (!mapOfOffice_origin_unitsDTOTostatus.containsKey(office_origin_unitsDTO.status)) {
                        mapOfOffice_origin_unitsDTOTostatus.put(office_origin_unitsDTO.status, new HashSet<>());
                    }
                    mapOfOffice_origin_unitsDTOTostatus.get(office_origin_unitsDTO.status).add(office_origin_unitsDTO);

                    if (!mapOfOffice_origin_unitsDTOTocreatedBy.containsKey(office_origin_unitsDTO.createdBy)) {
                        mapOfOffice_origin_unitsDTOTocreatedBy.put(office_origin_unitsDTO.createdBy, new HashSet<>());
                    }
                    mapOfOffice_origin_unitsDTOTocreatedBy.get(office_origin_unitsDTO.createdBy).add(office_origin_unitsDTO);

                    if (!mapOfOffice_origin_unitsDTOTomodifiedBy.containsKey(office_origin_unitsDTO.modifiedBy)) {
                        mapOfOffice_origin_unitsDTOTomodifiedBy.put(office_origin_unitsDTO.modifiedBy, new HashSet<>());
                    }
                    mapOfOffice_origin_unitsDTOTomodifiedBy.get(office_origin_unitsDTO.modifiedBy).add(office_origin_unitsDTO);

                    if (!mapOfOffice_origin_unitsDTOTocreated.containsKey(office_origin_unitsDTO.created)) {
                        mapOfOffice_origin_unitsDTOTocreated.put(office_origin_unitsDTO.created, new HashSet<>());
                    }
                    mapOfOffice_origin_unitsDTOTocreated.get(office_origin_unitsDTO.created).add(office_origin_unitsDTO);

                    if (!mapOfOffice_origin_unitsDTOTomodified.containsKey(office_origin_unitsDTO.modified)) {
                        mapOfOffice_origin_unitsDTOTomodified.put(office_origin_unitsDTO.modified, new HashSet<>());
                    }
                    mapOfOffice_origin_unitsDTOTomodified.get(office_origin_unitsDTO.modified).add(office_origin_unitsDTO);

                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public List<Office_origin_unitsDTO> getOffice_origin_unitsList() {
        List<Office_origin_unitsDTO> office_origin_unitss = new ArrayList<Office_origin_unitsDTO>(this.mapOfOffice_origin_unitsDTOToiD.values());
        return office_origin_unitss;
    }


    public Office_origin_unitsDTO getOffice_origin_unitsDTOByID(long ID) {
        return mapOfOffice_origin_unitsDTOToiD.get(ID);
    }


    public List<Office_origin_unitsDTO> getOffice_origin_unitsDTOByoffice_ministry_id(int office_ministry_id) {
        return new ArrayList<>(mapOfOffice_origin_unitsDTOToofficeMinistryId.getOrDefault(office_ministry_id, new HashSet<>()));
    }


    public List<Office_origin_unitsDTO> getOffice_origin_unitsDTOByoffice_layer_id(int office_layer_id) {
        return new ArrayList<>(mapOfOffice_origin_unitsDTOToofficeLayerId.getOrDefault(office_layer_id, new HashSet<>()));
    }


    public List<Office_origin_unitsDTO> getOffice_origin_unitsDTOByoffice_origin_id(int office_origin_id) {
        return new ArrayList<>(mapOfOffice_origin_unitsDTOToofficeOriginId.getOrDefault(office_origin_id, new HashSet<>()));
    }


    public List<Office_origin_unitsDTO> getOffice_origin_unitsDTOByunit_name_bng(String unit_name_bng) {
        return new ArrayList<>(mapOfOffice_origin_unitsDTOTounitNameBng.getOrDefault(unit_name_bng, new HashSet<>()));
    }


    public List<Office_origin_unitsDTO> getOffice_origin_unitsDTOByunit_name_eng(String unit_name_eng) {
        return new ArrayList<>(mapOfOffice_origin_unitsDTOTounitNameEng.getOrDefault(unit_name_eng, new HashSet<>()));
    }


    public List<Office_origin_unitsDTO> getOffice_origin_unitsDTOByoffice_unit_category(String office_unit_category) {
        return new ArrayList<>(mapOfOffice_origin_unitsDTOToofficeUnitCategory.getOrDefault(office_unit_category, new HashSet<>()));
    }


    public List<Office_origin_unitsDTO> getOffice_origin_unitsDTOByparent_unit_id(int parent_unit_id) {
        return new ArrayList<>(mapOfOffice_origin_unitsDTOToparentUnitId.getOrDefault(parent_unit_id, new HashSet<>()));
    }


    public List<Office_origin_unitsDTO> getOffice_origin_unitsDTOByunit_level(int unit_level) {
        return new ArrayList<>(mapOfOffice_origin_unitsDTOTounitLevel.getOrDefault(unit_level, new HashSet<>()));
    }


    public List<Office_origin_unitsDTO> getOffice_origin_unitsDTOBystatus(boolean status) {
        return new ArrayList<>(mapOfOffice_origin_unitsDTOTostatus.getOrDefault(status, new HashSet<>()));
    }


    public List<Office_origin_unitsDTO> getOffice_origin_unitsDTOBycreated_by(int created_by) {
        return new ArrayList<>(mapOfOffice_origin_unitsDTOTocreatedBy.getOrDefault(created_by, new HashSet<>()));
    }


    public List<Office_origin_unitsDTO> getOffice_origin_unitsDTOBymodified_by(int modified_by) {
        return new ArrayList<>(mapOfOffice_origin_unitsDTOTomodifiedBy.getOrDefault(modified_by, new HashSet<>()));
    }


    public List<Office_origin_unitsDTO> getOffice_origin_unitsDTOBycreated(long created) {
        return new ArrayList<>(mapOfOffice_origin_unitsDTOTocreated.getOrDefault(created, new HashSet<>()));
    }


    public List<Office_origin_unitsDTO> getOffice_origin_unitsDTOBymodified(long modified) {
        return new ArrayList<>(mapOfOffice_origin_unitsDTOTomodified.getOrDefault(modified, new HashSet<>()));
    }

    public void addNewDto(Office_origin_unitsDTO dto) {
        mapOfOffice_origin_unitsDTOToiD.put(dto.iD, dto);
    }


    @Override
    public String getTableName() {
        String tableName = "";
        try {
            tableName = "office_origin_units";
        } catch (Exception ex) {
            logger.debug("FATAL", ex);
        }
        return tableName;
    }
}


