/*
package office_origin_units;

import java.io.IOException;
import java.io.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import org.json.JSONObject;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;

import util.RecordNavigationManager;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;

*/
/**
 * Servlet implementation class Office_origin_unitsServlet
 *//*

@WebServlet("/Office_origin_unitsServlet")
@MultipartConfig
public class Office_origin_unitsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Office_origin_unitsServlet.class);

    */
/**
     * @see HttpServlet#HttpServlet()
     *//*

    public Office_origin_unitsServlet() {
        super();
    }

    */
/**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     *//*

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getAddPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_ORIGIN_UNITS_ADD)) {
                    getAddPage(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getEditPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_ORIGIN_UNITS_UPDATE)) {
                    getOffice_origin_units(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getURL")) {
                String URL = request.getParameter("URL");
                System.out.println("URL = " + URL);
                response.sendRedirect(URL);
            } else if (actionType.equals("search")) {
                System.out.println("search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_ORIGIN_UNITS_SEARCH)) {
                    searchOffice_origin_units(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getDetailsById")) {
                int id = Integer.parseInt(request.getParameter("id"));
                Office_origin_unitsDTO r = new Office_origin_unitsDAO().getOffice_origin_unitsDTOByID(id);

                response.setContentType("application/json");

                Map<String, Object> a = new HashMap<>();
                a.put("id", r.iD);
                a.put("officeMinistryId", r.officeMinistryId);
                a.put("officeLayerId", r.officeLayerId);
                a.put("officeOriginId", r.officeOriginId);
                a.put("unitNameBng", r.unitNameBng);
                a.put("unitNameEng", r.unitNameEng);
                a.put("officeUnitCategory", r.officeUnitCategory);
                a.put("parentUnitId", r.parentUnitId);
                a.put("unitLevel", r.unitLevel);
                a.put("status", r.status);
                a.put("isDeleted", r.isDeleted);

                String res = new JSONObject(a).toString();
                PrintWriter out = response.getWriter();

                out.print(res);
                out.flush();
            } else if (actionType.equals("getOriginUnitByOfficeOrigin")) {

                OfficeOriginUnitService officeOriginUnitService = new OfficeOriginUnitService();

                String officeOriginId = request.getParameter("officeOriginId");
                PrintWriter out = response.getWriter();

                out.print(officeOriginUnitService.getOfficeOriginUnitByOfficeOrigin(officeOriginId));
                out.flush();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("ID", -1L);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("office_origin_units/office_origin_unitsEdit.jsp");
        requestDispatcher.forward(request, response);
    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        System.out.println("Part Header = {0}" + partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        System.out.println("doPost");

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("add")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_ORIGIN_UNITS_ADD)) {
                    System.out.println("going to  addOffice_origin_units ");
                    addOffice_origin_units(request, response, true);
                } else {
                    System.out.println("Not going to  addOffice_origin_units ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("edit")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_ORIGIN_UNITS_UPDATE)) {
                    addOffice_origin_units(request, response, false);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("delete")) {
                deleteOffice_origin_units(request, response);
            } else if (actionType.equals("search")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_ORIGIN_UNITS_SEARCH)) {
                    searchOffice_origin_units(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getGeo")) {
                System.out.println("going to geoloc ");
                request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void addOffice_origin_units(HttpServletRequest request, HttpServletResponse response, Boolean addFlag) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addOffice_origin_units");
            String path = getServletContext().getRealPath("/img2/");
            Office_origin_unitsDAO office_origin_unitsDAO = new Office_origin_unitsDAO();
            Office_origin_unitsDTO office_origin_unitsDTO;
            String FileNamePrefix;
            if (addFlag == true) {
                office_origin_unitsDTO = new Office_origin_unitsDTO();
                FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
            } else {
                office_origin_unitsDTO = office_origin_unitsDAO.getOffice_origin_unitsDTOByID(Long.parseLong(request.getParameter("id")));
                FileNamePrefix = request.getParameter("identity");
            }

            String Value = "";
            Value = request.getParameter("officeMinistryId");
            System.out.println("officeMinistryId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_origin_unitsDTO.officeMinistryId = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }

            Value = request.getParameter("officeLayerId");
            System.out.println("officeLayerId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_origin_unitsDTO.officeLayerId = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }

            Value = request.getParameter("officeOriginId");
            System.out.println("officeOriginId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_origin_unitsDTO.officeOriginId = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }

            Value = request.getParameter("unitNameBng");
            System.out.println("unitNameBng = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_origin_unitsDTO.unitNameBng = (Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }

            Value = request.getParameter("unitNameEng");
            System.out.println("unitNameEng = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_origin_unitsDTO.unitNameEng = (Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }

            Value = request.getParameter("officeUnitCategory");
            System.out.println("officeUnitCategory = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_origin_unitsDTO.officeUnitCategory = (Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }

            Value = request.getParameter("parentUnitId");
            System.out.println("parentUnitId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_origin_unitsDTO.parentUnitId = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }

            Value = request.getParameter("unitLevel");
            System.out.println("unitLevel = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_origin_unitsDTO.unitLevel = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }

            Value = request.getParameter("status");
            System.out.println("status = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_origin_unitsDTO.status = Boolean.parseBoolean(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }

            Value = request.getParameter("createdBy");
            System.out.println("createdBy = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_origin_unitsDTO.createdBy = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }

            Value = request.getParameter("modifiedBy");
            System.out.println("modifiedBy = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_origin_unitsDTO.modifiedBy = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }

            Value = request.getParameter("created");
            System.out.println("created = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_origin_unitsDTO.created = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }

            Value = request.getParameter("modified");
            System.out.println("modified = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_origin_unitsDTO.modified = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }

            Value = request.getParameter("isDeleted");
            System.out.println("isDeleted = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_origin_unitsDTO.isDeleted = Boolean.parseBoolean(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }

            System.out.println("Done adding  addOffice_origin_units dto = " + office_origin_unitsDTO);

            if (addFlag == true) {
                office_origin_unitsDAO.addOffice_origin_units(office_origin_unitsDTO);
            } else {
                office_origin_unitsDAO.updateOffice_origin_units(office_origin_unitsDTO);

            }

            response.setContentType("application/json");
            HashMap a = new HashMap<String, Boolean>();
            a.put("success", true);

            String res = new JSONObject(a).toString();
            PrintWriter out = response.getWriter();

            out.print(res);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteOffice_origin_units(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            String[] IDsToDelete = request.getParameterValues("ID");
            for (int i = 0; i < IDsToDelete.length; i++) {
                long id = Long.parseLong(IDsToDelete[i]);
                System.out.println("###DELETING " + IDsToDelete[i]);
                new Office_origin_unitsDAO().deleteOffice_origin_unitsByID(id);
            }
        } catch (Exception ex) {
            logger.debug(ex);
        }
        response.sendRedirect("Office_origin_unitsServlet?actionType=search");
    }

    private void getOffice_origin_units(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("in getOffice_origin_units");
        Office_origin_unitsDTO office_origin_unitsDTO = null;
        try {
            office_origin_unitsDTO = new Office_origin_unitsDAO().getOffice_origin_unitsDTOByID(Long.parseLong(request.getParameter("ID")));
            request.setAttribute("ID", office_origin_unitsDTO.iD);
            request.setAttribute("office_origin_unitsDTO", office_origin_unitsDTO);

            String URL = "";

            String inPlaceEdit = (String) request.getParameter("inplaceedit");
            String inPlaceSubmit = (String) request.getParameter("inplacesubmit");
            String getBodyOnly = (String) request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "office_origin_units/office_origin_unitsInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "office_origin_units/office_origin_unitsSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "office_origin_units/office_origin_unitsEditBody.jsp?actionType=edit";
                } else {
                    URL = "office_origin_units/office_origin_unitsEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void searchOffice_origin_units(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("in  searchOffice_origin_units 1");
        Office_origin_unitsDAO office_origin_unitsDAO = new Office_origin_unitsDAO();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        String ajax = (String) request.getParameter("ajax");
        boolean hasAjax = false;
        if (ajax != null && !ajax.equalsIgnoreCase("")) {
            hasAjax = true;
        }
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager rnManager = new RecordNavigationManager(SessionConstants.NAV_OFFICE_ORIGIN_UNITS, request, office_origin_unitsDAO, SessionConstants.VIEW_OFFICE_ORIGIN_UNITS, SessionConstants.SEARCH_OFFICE_ORIGIN_UNITS);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        RequestDispatcher rd;
        if (hasAjax == false) {
            System.out.println("Going to office_origin_units/office_origin_unitsSearch.jsp");
            rd = request.getRequestDispatcher("office_origin_units/office_origin_unitsSearch.jsp");
        } else {
            System.out.println("Going to office_origin_units/office_origin_unitsSearchForm.jsp");
            rd = request.getRequestDispatcher("office_origin_units/office_origin_unitsSearchForm.jsp");
        }
        rd.forward(request, response);
    }
}

*/
