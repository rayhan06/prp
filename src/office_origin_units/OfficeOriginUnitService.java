package office_origin_units;

import com.google.gson.Gson;
import test_lib.RQueryBuilder;

import java.util.ArrayList;
import java.util.HashMap;

public class OfficeOriginUnitService {

    public String getOfficeOriginUnitByOfficeOrigin(String officeOriginId) {
        String sql = String.format("SELECT id, \n" +
                "unit_name_bng, \n" +
                "unit_name_eng \n" +
                "FROM office_origin_units \n" +
                "WHERE office_origin_units.office_origin_id = %s \n" +
                "AND office_origin_units.isDeleted = 0\n" +
                "AND office_origin_units.status = 1", officeOriginId);

        RQueryBuilder<HashMap> rQueryBuilder = new RQueryBuilder<>();
        ArrayList<HashMap> data = rQueryBuilder.setSql(sql).of(HashMap.class).buildHashMap();
        return new Gson().toJson(data);
    }
}
