package office_origin_units;
import java.util.*; 


public class Office_origin_unitsDTO {

	public long iD = 0;
	public int officeMinistryId = 0;
	public int officeLayerId = 0;
	public int officeOriginId = 0;
    public String unitNameBng = "";
    public String unitNameEng = "";
    public String officeUnitCategory = "";
	public int parentUnitId = 0;
	public int unitLevel = 0;
	public boolean status = false;
	public int createdBy = 0;
	public int modifiedBy = 0;
	public long created = 0;
	public long modified = 0;
	public boolean isDeleted = false;
	
	
    @Override
	public String toString() {
            return "$Office_origin_unitsDTO[" +
            " id = " + iD +
            " officeMinistryId = " + officeMinistryId +
            " officeLayerId = " + officeLayerId +
            " officeOriginId = " + officeOriginId +
            " unitNameBng = " + unitNameBng +
            " unitNameEng = " + unitNameEng +
            " officeUnitCategory = " + officeUnitCategory +
            " parentUnitId = " + parentUnitId +
            " unitLevel = " + unitLevel +
            " status = " + status +
            " createdBy = " + createdBy +
            " modifiedBy = " + modifiedBy +
            " created = " + created +
            " modified = " + modified +
            " isDeleted = " + isDeleted +
            "]";
    }

}