package office_origin_units;
import java.util.*; 


public class Office_origin_unitsMAPS 
{

	public HashMap<String, String> java_allfield_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_anyfield_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	public HashMap<String, String> java_SQL_map = new HashMap<String, String>();
	public HashMap<String, String> java_Text_map = new HashMap<String, String>();
	
	private static Office_origin_unitsMAPS self = null;
	
	private Office_origin_unitsMAPS()
	{
		
		java_allfield_type_map.put("office_ministry_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("office_layer_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("office_origin_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("unit_name_bng".toLowerCase(), "String");
		java_allfield_type_map.put("unit_name_eng".toLowerCase(), "String");
		java_allfield_type_map.put("office_unit_category".toLowerCase(), "String");
		java_allfield_type_map.put("parent_unit_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("unit_level".toLowerCase(), "Integer");
		java_allfield_type_map.put("status".toLowerCase(), "Boolean");

		java_anyfield_search_map.put("office_origin_units.office_ministry_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("office_origin_units.office_layer_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("office_origin_units.office_origin_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("office_origin_units.unit_name_bng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_origin_units.unit_name_eng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_unit_categories.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("office_unit_categories.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put("office_origin_units.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("office_origin_units.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put("office_origin_units.unit_level".toLowerCase(), "Integer");
		java_anyfield_search_map.put("office_origin_units.status".toLowerCase(), "Boolean");
		java_anyfield_search_map.put("office_origin_units.created_by".toLowerCase(), "Integer");
		java_anyfield_search_map.put("office_origin_units.modified_by".toLowerCase(), "Integer");
		java_anyfield_search_map.put("office_origin_units.created".toLowerCase(), "Long");
		java_anyfield_search_map.put("office_origin_units.modified".toLowerCase(), "Long");

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("officeMinistryId".toLowerCase(), "officeMinistryId".toLowerCase());
		java_DTO_map.put("officeLayerId".toLowerCase(), "officeLayerId".toLowerCase());
		java_DTO_map.put("officeOriginId".toLowerCase(), "officeOriginId".toLowerCase());
		java_DTO_map.put("unitNameBng".toLowerCase(), "unitNameBng".toLowerCase());
		java_DTO_map.put("unitNameEng".toLowerCase(), "unitNameEng".toLowerCase());
		java_DTO_map.put("officeUnitCategory".toLowerCase(), "officeUnitCategory".toLowerCase());
		java_DTO_map.put("parentUnitId".toLowerCase(), "parentUnitId".toLowerCase());
		java_DTO_map.put("unitLevel".toLowerCase(), "unitLevel".toLowerCase());
		java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
		java_DTO_map.put("createdBy".toLowerCase(), "createdBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("created".toLowerCase(), "created".toLowerCase());
		java_DTO_map.put("modified".toLowerCase(), "modified".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());

		java_SQL_map.put("office_ministry_id".toLowerCase(), "officeMinistryId".toLowerCase());
		java_SQL_map.put("office_layer_id".toLowerCase(), "officeLayerId".toLowerCase());
		java_SQL_map.put("office_origin_id".toLowerCase(), "officeOriginId".toLowerCase());
		java_SQL_map.put("unit_name_bng".toLowerCase(), "unitNameBng".toLowerCase());
		java_SQL_map.put("unit_name_eng".toLowerCase(), "unitNameEng".toLowerCase());
		java_SQL_map.put("office_unit_category".toLowerCase(), "officeUnitCategory".toLowerCase());
		java_SQL_map.put("parent_unit_id".toLowerCase(), "parentUnitId".toLowerCase());
		java_SQL_map.put("unit_level".toLowerCase(), "unitLevel".toLowerCase());
		java_SQL_map.put("status".toLowerCase(), "status".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Office Ministry Id".toLowerCase(), "officeMinistryId".toLowerCase());
		java_Text_map.put("Office Layer Id".toLowerCase(), "officeLayerId".toLowerCase());
		java_Text_map.put("Office Origin Id".toLowerCase(), "officeOriginId".toLowerCase());
		java_Text_map.put("Unit Name Bng".toLowerCase(), "unitNameBng".toLowerCase());
		java_Text_map.put("Unit Name Eng".toLowerCase(), "unitNameEng".toLowerCase());
		java_Text_map.put("Office Unit Category".toLowerCase(), "officeUnitCategory".toLowerCase());
		java_Text_map.put("Parent Unit Id".toLowerCase(), "parentUnitId".toLowerCase());
		java_Text_map.put("Unit Level".toLowerCase(), "unitLevel".toLowerCase());
		java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
		java_Text_map.put("Created By".toLowerCase(), "createdBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Created".toLowerCase(), "created".toLowerCase());
		java_Text_map.put("Modified".toLowerCase(), "modified".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
			
	}
	
	public static Office_origin_unitsMAPS GetInstance()
	{
		if(self == null)
		{
			self = new Office_origin_unitsMAPS ();
		}
		return self;
	}
	

}