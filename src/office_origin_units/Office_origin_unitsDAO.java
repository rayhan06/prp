package office_origin_units;

import dbm.DBMR;
import dbm.DBMW;
import login.LoginDTO;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.NavigationService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public class Office_origin_unitsDAO implements NavigationService {

    Logger logger = Logger.getLogger(getClass());

    private void printSql(String sql) {
        System.out.println("sql: " + sql);
    }

    private void recordUpdateTime(Connection connection, long lastModificationTime)  {
        recordUpdateTime(connection,"office_origin_units",lastModificationTime);
    }

    public long addOffice_origin_units(Office_origin_unitsDTO office_origin_unitsDTO) throws Exception {
        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();

        try {
            connection = DBMW.getInstance().getConnection();

            if (connection == null) {
                System.out.println("nullconn");
            }
            office_origin_unitsDTO.iD = DBMW.getInstance().getNextSequenceId("office_origin_units");
            String sql = "INSERT INTO office_origin_units";

            sql += " (";
            sql += "ID";
            sql += ", ";
            sql += "office_ministry_id";
            sql += ", ";
            sql += "office_layer_id";
            sql += ", ";
            sql += "office_origin_id";
            sql += ", ";
            sql += "unit_name_bng";
            sql += ", ";
            sql += "unit_name_eng";
            sql += ", ";
            sql += "office_unit_category";
            sql += ", ";
            sql += "parent_unit_id";
            sql += ", ";
            sql += "unit_level";
            sql += ", ";
            sql += "status";
            sql += ", ";
            sql += "created_by";
            sql += ", ";
            sql += "modified_by";
            sql += ", ";
            sql += "created";
            sql += ", ";
            sql += "modified";
            sql += ", ";
            sql += "isDeleted";
            sql += ", lastModificationTime)";

            sql += " VALUES(";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ?)";

            printSql(sql);

            ps = connection.prepareStatement(sql);

            int index = 1;
            /* byte ptext[] = office_origin_unitsDTO.officeUnitCategory.getBytes("ISO-8859-1");
            office_origin_unitsDTO.officeUnitCategory = new String(ptext, "UTF-8"); */
            /*
            byte ptext0[] = office_origin_unitsDTO.unitNameBng.getBytes("ISO-8859-1");
            office_origin_unitsDTO.unitNameBng = new String(ptext0, "UTF-8"); */

            ps.setObject(index++, office_origin_unitsDTO.iD);
            ps.setObject(index++, office_origin_unitsDTO.officeMinistryId);
            ps.setObject(index++, office_origin_unitsDTO.officeLayerId);
            ps.setObject(index++, office_origin_unitsDTO.officeOriginId > 0 ? office_origin_unitsDTO.officeOriginId : 0);
            ps.setObject(index++, office_origin_unitsDTO.unitNameBng);
            ps.setObject(index++, office_origin_unitsDTO.unitNameEng);
            ps.setObject(index++, office_origin_unitsDTO.officeUnitCategory);
            ps.setObject(index++, office_origin_unitsDTO.parentUnitId);
            ps.setObject(index++, office_origin_unitsDTO.unitLevel);
            ps.setObject(index++, office_origin_unitsDTO.status);
            ps.setObject(index++, office_origin_unitsDTO.createdBy);
            ps.setObject(index++, office_origin_unitsDTO.modifiedBy);
            ps.setObject(index++, office_origin_unitsDTO.created);
            ps.setObject(index++, office_origin_unitsDTO.modified);
            ps.setObject(index++, office_origin_unitsDTO.isDeleted);
            ps.setObject(index++, lastModificationTime);

            System.out.println(ps);
            ps.execute();

            recordUpdateTime(connection, lastModificationTime);
            Office_origin_unitsRepository.getInstance().addNewDto(office_origin_unitsDTO);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_origin_unitsDTO.iD;
    }

    //need another getter for repository
    public Office_origin_unitsDTO getOffice_origin_unitsDTOByID(long ID) throws Exception {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Office_origin_unitsDTO office_origin_unitsDTO = null;
        try {

            String sql = "SELECT * ";

            sql += " FROM office_origin_units";

            sql += " WHERE ID=" + ID;

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();

            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                office_origin_unitsDTO = new Office_origin_unitsDTO();

                office_origin_unitsDTO.iD = rs.getLong("ID");
                office_origin_unitsDTO.officeMinistryId = rs.getInt("office_ministry_id");
                office_origin_unitsDTO.officeLayerId = rs.getInt("office_layer_id");
                office_origin_unitsDTO.officeOriginId = rs.getInt("office_origin_id");
                office_origin_unitsDTO.unitNameBng = rs.getString("unit_name_bng");
                office_origin_unitsDTO.unitNameEng = rs.getString("unit_name_eng");
                office_origin_unitsDTO.officeUnitCategory = rs.getString("office_unit_category");
                office_origin_unitsDTO.parentUnitId = rs.getInt("parent_unit_id");
                office_origin_unitsDTO.unitLevel = rs.getInt("unit_level");
                office_origin_unitsDTO.status = rs.getBoolean("status");
                office_origin_unitsDTO.createdBy = rs.getInt("created_by");
                office_origin_unitsDTO.modifiedBy = rs.getInt("modified_by");
                office_origin_unitsDTO.created = rs.getLong("created");
                office_origin_unitsDTO.modified = rs.getLong("modified");
                office_origin_unitsDTO.isDeleted = rs.getBoolean("isDeleted");

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return office_origin_unitsDTO;
    }

    public long updateOffice_origin_units(Office_origin_unitsDTO office_origin_unitsDTO) throws Exception {

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            connection = DBMW.getInstance().getConnection();

            String sql = "UPDATE office_origin_units";

            sql += " SET ";
            sql += "office_ministry_id=?";
            sql += ", ";
            sql += "office_layer_id=?";
            sql += ", ";
            sql += "office_origin_id=?";
            sql += ", ";
            sql += "unit_name_bng=?";
            sql += ", ";
            sql += "unit_name_eng=?";
            sql += ", ";
            sql += "office_unit_category=?";
            sql += ", ";
            sql += "parent_unit_id=?";
            sql += ", ";
            sql += "unit_level=?";
            sql += ", ";
            sql += "status=?";
            sql += ", ";
            sql += "created_by=?";
            sql += ", ";
            sql += "modified_by=?";
            sql += ", ";
            sql += "created=?";
            sql += ", ";
            sql += "modified=?";
            sql += ", ";
            sql += "isDeleted=?";
            sql += ", lastModificationTime = " + lastModificationTime + "";
            sql += " WHERE ID = " + office_origin_unitsDTO.iD;

            printSql(sql);

            ps = connection.prepareStatement(sql);

            int index = 1;
            /*
            byte ptext[] = office_origin_unitsDTO.officeUnitCategory.getBytes("ISO-8859-1");
            office_origin_unitsDTO.officeUnitCategory = new String(ptext, "UTF-8");

            byte ptext0[] = office_origin_unitsDTO.unitNameBng.getBytes("ISO-8859-1");
            office_origin_unitsDTO.unitNameBng = new String(ptext0, "UTF-8");
            */

            ps.setObject(index++, office_origin_unitsDTO.officeMinistryId);
            ps.setObject(index++, office_origin_unitsDTO.officeLayerId);
            ps.setObject(index++, office_origin_unitsDTO.officeOriginId);
            ps.setObject(index++, office_origin_unitsDTO.unitNameBng);
            ps.setObject(index++, office_origin_unitsDTO.unitNameEng);
            ps.setObject(index++, office_origin_unitsDTO.officeUnitCategory);
            ps.setObject(index++, office_origin_unitsDTO.parentUnitId);
            ps.setObject(index++, office_origin_unitsDTO.unitLevel);
            ps.setObject(index++, office_origin_unitsDTO.status);
            ps.setObject(index++, office_origin_unitsDTO.createdBy);
            ps.setObject(index++, office_origin_unitsDTO.modifiedBy);
            ps.setObject(index++, office_origin_unitsDTO.created);
            ps.setObject(index++, office_origin_unitsDTO.modified);
            ps.setObject(index++, office_origin_unitsDTO.isDeleted);
            System.out.println(ps);
            ps.executeUpdate();


            recordUpdateTime(connection, lastModificationTime);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_origin_unitsDTO.iD;
    }

    public void deleteOffice_origin_unitsByID(long ID) throws Exception {
        Connection connection = null;
        Statement stmt = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            String sql = "UPDATE office_origin_units";

            sql += " SET isDeleted=1,lastModificationTime=" + lastModificationTime + " WHERE ID = " + ID;

            printSql(sql);

            connection = DBMW.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.execute(sql);


            recordUpdateTime(connection, lastModificationTime);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
    }

    public List<Office_origin_unitsDTO> getDTOs(Collection recordIDs, LoginDTO loginDTO) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Office_origin_unitsDTO office_origin_unitsDTO = null;
        List<Office_origin_unitsDTO> office_origin_unitsDTOList = new ArrayList<>();
        if (recordIDs.isEmpty()) {
            return office_origin_unitsDTOList;
        }
        try {

            String sql = "SELECT * ";

            sql += " FROM office_origin_units";

            sql += " WHERE ID IN ( ";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql += ",";
                }
                sql += ((ArrayList) recordIDs).get(i);
            }
            sql += ")  order by ID desc";

            printSql(sql);

            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                office_origin_unitsDTO = new Office_origin_unitsDTO();
                office_origin_unitsDTO.iD = rs.getLong("ID");
                office_origin_unitsDTO.officeMinistryId = rs.getInt("office_ministry_id");
                office_origin_unitsDTO.officeLayerId = rs.getInt("office_layer_id");
                office_origin_unitsDTO.officeOriginId = rs.getInt("office_origin_id");
                office_origin_unitsDTO.unitNameBng = rs.getString("unit_name_bng");
                office_origin_unitsDTO.unitNameEng = rs.getString("unit_name_eng");
                office_origin_unitsDTO.officeUnitCategory = rs.getString("office_unit_category");
                office_origin_unitsDTO.parentUnitId = rs.getInt("parent_unit_id");
                office_origin_unitsDTO.unitLevel = rs.getInt("unit_level");
                office_origin_unitsDTO.status = rs.getBoolean("status");
                office_origin_unitsDTO.createdBy = rs.getInt("created_by");
                office_origin_unitsDTO.modifiedBy = rs.getInt("modified_by");
                office_origin_unitsDTO.created = rs.getLong("created");
                office_origin_unitsDTO.modified = rs.getLong("modified");
                office_origin_unitsDTO.isDeleted = rs.getBoolean("isDeleted");
                System.out.println("got this DTO: " + office_origin_unitsDTO);

                office_origin_unitsDTOList.add(office_origin_unitsDTO);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_origin_unitsDTOList;

    }

    public Collection getIDs(LoginDTO loginDTO) {
        Collection data = new ArrayList();
        Connection connection = null;
        Statement stmt = null;
        ResultSet resultSet = null;

        String sql = "SELECT ID FROM office_origin_units";

        sql += " WHERE isDeleted = 0  order by ID desc ";

        printSql(sql);

        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();

            for (resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("ID"))) ;

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("DAO " + e, e);
        } finally {
            try {
                if (resultSet != null && !resultSet.isClosed()) {
                    resultSet.close();
                }
            } catch (Exception ex) {

            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception e) {
                logger.fatal("DAO finally :" + e);
            }
        }
        return data;
    }

    //add repository
    public List<Office_origin_unitsDTO> getAllOffice_origin_units(boolean isFirstReload) {
        List<Office_origin_unitsDTO> office_origin_unitsDTOList = new ArrayList<>();

        String sql = "SELECT * FROM office_origin_units";
        sql += " WHERE ";


        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        printSql(sql);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;


        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);


            while (rs.next()) {
                Office_origin_unitsDTO office_origin_unitsDTO = new Office_origin_unitsDTO();
                office_origin_unitsDTO.iD = rs.getLong("ID");
                office_origin_unitsDTO.officeMinistryId = rs.getInt("office_ministry_id");
                office_origin_unitsDTO.officeLayerId = rs.getInt("office_layer_id");
                office_origin_unitsDTO.officeOriginId = rs.getInt("office_origin_id");
                office_origin_unitsDTO.unitNameBng = rs.getString("unit_name_bng");
                office_origin_unitsDTO.unitNameEng = rs.getString("unit_name_eng");
                office_origin_unitsDTO.officeUnitCategory = rs.getString("office_unit_category");
                office_origin_unitsDTO.parentUnitId = rs.getInt("parent_unit_id");
                office_origin_unitsDTO.unitLevel = rs.getInt("unit_level");
                office_origin_unitsDTO.status = rs.getBoolean("status");
                office_origin_unitsDTO.createdBy = rs.getInt("created_by");
                office_origin_unitsDTO.modifiedBy = rs.getInt("modified_by");
                office_origin_unitsDTO.created = rs.getLong("created");
                office_origin_unitsDTO.modified = rs.getLong("modified");
                office_origin_unitsDTO.isDeleted = rs.getBoolean("isDeleted");
                int i = 0;
                long primaryKey = office_origin_unitsDTO.iD;
                while (i < office_origin_unitsDTOList.size() && office_origin_unitsDTOList.get(i).iD < primaryKey) {
                    i++;
                }
                office_origin_unitsDTOList.add(i, office_origin_unitsDTO);
                //office_origin_unitsDTOList.add(office_origin_unitsDTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return office_origin_unitsDTOList;
    }

    public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO) throws Exception {
        System.out.println("table: " + p_searchCriteria);
        List<Long> idList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement ps = null;

        try {

            String sql = "SELECT distinct office_origin_units.ID as ID FROM office_origin_units ";
            sql += " join office_unit_categories on office_origin_units.office_unit_category = office_unit_categories.ID ";
            sql += " join office_origin_units on office_origin_units.parent_unit_id = office_origin_units.ID ";


            Enumeration names = p_searchCriteria.keys();
            String str, value;

            String AnyfieldSql = "(";

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                int i = 0;
                Iterator it = Office_origin_unitsMAPS.GetInstance().java_anyfield_search_map.entrySet().iterator();
                while (it.hasNext()) {
                    if (i > 0) {
                        AnyfieldSql += " OR  ";
                    }
                    Map.Entry pair = (Map.Entry) it.next();
                    AnyfieldSql += pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
                    i++;
                }
            }
            AnyfieldSql += ")";
            System.out.println("AnyfieldSql = " + AnyfieldSql);

            String AllFieldSql = "(";
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                System.out.println(str + ": " + value);
                if (Office_origin_unitsMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()) != null && !Office_origin_unitsMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
                        && !str.equalsIgnoreCase("AnyField")
                        && value != null && !value.equalsIgnoreCase("")) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql += " AND  ";
                    }
                    if (Office_origin_unitsMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equals("String")) {
                        AllFieldSql += "office_origin_units." + str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
                    } else {
                        AllFieldSql += "office_origin_units." + str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
                    }
                    i++;
                }
            }

            AllFieldSql += ")";

            System.out.println("AllFieldSql = " + AllFieldSql);


            sql += " WHERE ";
            sql += " office_origin_units.isDeleted = false";


            if (!AnyfieldSql.equals("()")) {
                sql += " AND " + AnyfieldSql;

            }
            if (!AllFieldSql.equals("()")) {
                sql += " AND " + AllFieldSql;
            }

            sql += " order by office_origin_units.ID desc ";

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                idList.add(rs.getLong("ID"));
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return idList;
    }
}
	