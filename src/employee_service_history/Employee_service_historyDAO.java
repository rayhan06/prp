package employee_service_history;

import common.ConnectionAndStatementUtil;
import common.EmployeeCommonDAOService;
import employee_offices.EmployeeOfficeDTO;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import util.CommonDTO;
import util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"unused", "Duplicates"})
public class Employee_service_historyDAO implements EmployeeCommonDAOService<Employee_service_historyDTO> {

    private static final Logger logger = Logger.getLogger(Employee_service_historyDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (employee_records_id,gazetted_date,"
            + " is_cadre,encardment_date,cadre_number,cadre_batch,office,"
            + " designation,department,serving_from,serving_to,"
            + " filesDropzone,modified_by,search_column,lastModificationTime,salary,promotion_history_id,"
            + "isDeleted,insertion_date,inserted_by,ID)"
            + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private static final String updateQuery = "UPDATE {tableName} SET employee_records_id=?,gazetted_date=?,"
            + "is_cadre=?,encardment_date=?,cadre_number=?,cadre_batch=?,office=?,designation=?,department=?,"
            + "serving_from=?,serving_to=?,filesDropzone=?,modified_by=?,search_column=?,"
            + "lastModificationTime = ?,salary=? WHERE ID = ?";

    private static final String getbyPromotionHistoryIdQuery = "SELECT * FROM %s where promotion_history_id=%d AND isDeleted=0";

    private static final String getGovJobByEmployeeId = "SELECT * FROM employee_service_history WHERE employee_records_id = %d " +
            "AND isDeleted = 0 ORDER BY lastModificationTime DESC";

    private static final String getByFirstGovtJobDTO = "select esh.* from employee_service_history esh inner join (" +
            " select employee_records_id,MIN(serving_from) min_serving_from from " +
            " (select * from employee_service_history where isDeleted = 0 and employee_records_id in (%s)) A group by employee_records_id )" +
            " B on esh.employee_records_id = B.employee_records_id AND esh.serving_from = min_serving_from";

    private static final String updateLastOfficeDate = "UPDATE %s SET serving_to=%d,is_currently_working=0 where employee_records_id=%d " +
            "AND isDeleted=0 AND promotion_history_id<>%d";

    private final Map<String, String> searchMap = new HashMap<>();

    private static Employee_service_historyDAO INSTANCE = null;

    public static Employee_service_historyDAO getInstance(){
        if(INSTANCE == null){
            synchronized (Employee_service_historyDAO.class){
                if(INSTANCE == null){
                    INSTANCE = new Employee_service_historyDAO();
                }
            }
        }
        return INSTANCE;
    }

    private Employee_service_historyDAO() {
        searchMap.put("serving_from", " and (serving_from >= ?)");
        searchMap.put("serving_to", " and (serving_to <= ?)");
        searchMap.put("employee_records_id", "and (employee_records_id = ?)");
        searchMap.put("employee_records_id_internal", "and (employee_records_id IN ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    @Override
    public void set(PreparedStatement ps, Employee_service_historyDTO employee_service_historyDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(employee_service_historyDTO);
        ps.setObject(++index, employee_service_historyDTO.employeeRecordsId);
        ps.setObject(++index, employee_service_historyDTO.gazettedDate);
        ps.setBoolean(++index, employee_service_historyDTO.isCadre);
        ps.setObject(++index, employee_service_historyDTO.encardmentDate);
        ps.setString(++index, employee_service_historyDTO.cadreNumber);
        ps.setInt(++index, employee_service_historyDTO.cadreBatch);
        ps.setObject(++index, employee_service_historyDTO.office);
        ps.setObject(++index, employee_service_historyDTO.designation);
        ps.setObject(++index, employee_service_historyDTO.department);
        ps.setObject(++index, employee_service_historyDTO.servingFrom);
        ps.setObject(++index, employee_service_historyDTO.servingTo);
        ps.setObject(++index, employee_service_historyDTO.filesDropzone);
        ps.setObject(++index, employee_service_historyDTO.modifiedBy);
        ps.setObject(++index, employee_service_historyDTO.searchColumn);
        ps.setObject(++index, employee_service_historyDTO.lastModificationTime);
        ps.setObject(++index, employee_service_historyDTO.salary);
        if (isInsert) {
            ps.setObject(++index, employee_service_historyDTO.promotionHistoryId);
            ps.setObject(++index, 0);
            ps.setObject(++index, employee_service_historyDTO.insertionDate);
            ps.setObject(++index, employee_service_historyDTO.insertedBy);
        }
        ps.setObject(++index, employee_service_historyDTO.iD);
    }

    public void setSearchColumn(Employee_service_historyDTO dto) {
        StringBuilder searchBuilder = new StringBuilder();
        try {
            Employee_recordsDTO employeeRecordsDTO = new Employee_recordsDAO().getEmployee_recordsDTOByID(dto.employeeRecordsId);
            if (employeeRecordsDTO != null) {
                if (employeeRecordsDTO.nameEng != null && employeeRecordsDTO.nameEng.trim().length() > 0) {
                    searchBuilder.append(employeeRecordsDTO.nameEng.trim()).append(" ");
                }
                if (employeeRecordsDTO.nameBng != null && employeeRecordsDTO.nameBng.trim().length() > 0) {
                    searchBuilder.append(employeeRecordsDTO.nameBng.trim()).append(" ");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (dto.department != null && dto.department.trim().length() > 0) {
            searchBuilder.append(dto.department.trim()).append(" ");
        }

        if (dto.designation != null && dto.designation.trim().length() > 0) {
            searchBuilder.append(dto.designation.trim()).append(" ");
        }

        if (dto.jobResponsibility != null && dto.jobResponsibility.trim().length() > 0) {
            searchBuilder.append(dto.jobResponsibility.trim()).append(" ");
        }

        dto.searchColumn = searchBuilder.toString();
    }

    @Override
    public Employee_service_historyDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Employee_service_historyDTO employee_service_historyDTO = new Employee_service_historyDTO();
            employee_service_historyDTO.iD = rs.getLong("ID");
            employee_service_historyDTO.employeeRecordsId = rs.getLong("employee_records_id");
            employee_service_historyDTO.gazettedDate = rs.getLong("gazetted_date");
            employee_service_historyDTO.isCadre = rs.getBoolean("is_cadre");
            employee_service_historyDTO.encardmentDate = rs.getLong("encardment_date");
            employee_service_historyDTO.cadreNumber = rs.getString("cadre_number");
            employee_service_historyDTO.cadreBatch = rs.getInt("cadre_batch");
            employee_service_historyDTO.office = rs.getString("office");
            employee_service_historyDTO.designation = rs.getString("designation");
            employee_service_historyDTO.department = rs.getString("department");
            employee_service_historyDTO.servingFrom = rs.getLong("serving_from");
            employee_service_historyDTO.servingTo = rs.getLong("serving_to");
            employee_service_historyDTO.filesDropzone = rs.getLong("filesDropzone");
            employee_service_historyDTO.insertionDate = rs.getLong("insertion_date");
            employee_service_historyDTO.searchColumn = rs.getString("search_column");
            employee_service_historyDTO.insertedBy = rs.getString("inserted_by");
            employee_service_historyDTO.modifiedBy = rs.getString("modified_by");
            employee_service_historyDTO.isDeleted = rs.getInt("isDeleted");
            employee_service_historyDTO.lastModificationTime = rs.getLong("lastModificationTime");
            employee_service_historyDTO.promotionHistoryId = rs.getLong("promotion_history_id");
            employee_service_historyDTO.salary = rs.getInt("salary");
            employee_service_historyDTO.isGovtJob = rs.getBoolean("is_govt_job");
            employee_service_historyDTO.isCurrentlyWorking = rs.getBoolean("is_currently_working");
            return employee_service_historyDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "employee_service_history";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_service_historyDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_service_historyDTO) commonDTO, updateQuery, false);
    }

    public String getDesignationText(String language, long id) {
        OfficeUnitOrganograms org = OfficeUnitOrganogramsRepository.getInstance().getById(id);
        if (org != null) {
            if (language.equalsIgnoreCase("English")) return org.designation_eng;
            else return org.designation_bng;
        } else {
            return "";
        }
    }

    public List<EmployeeServiceModel> getEmployeeServiceModelList(long employeeId) {
        List<Employee_service_historyDTO> list = getByEmployeeId(employeeId);
        if (list == null || list.size() == 0) {
            return new ArrayList<>();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        return list.stream()
                .map(dto -> convertToEmployeeServiceModel(dto, sdf))
                .collect(Collectors.toList());
    }

    private EmployeeServiceModel convertToEmployeeServiceModel(Employee_service_historyDTO dto, SimpleDateFormat sdf) {
        EmployeeServiceModel model = new EmployeeServiceModel();
        model.dto = dto;
        String date;
        if (dto.gazettedDate > SessionConstants.MIN_DATE) {
            date = sdf.format(new Date(dto.gazettedDate));
            model.gazettedDateEng = date;
            model.gazettedDateBan = StringUtils.convertToBanNumber(date);
        }
        if (dto.encardmentDate > SessionConstants.MIN_DATE) {
            date = sdf.format(new Date(dto.encardmentDate));
            model.encardmentDateEng = date;
            model.encardmentDateBan = StringUtils.convertToBanNumber(date);
        }

        if (dto.servingFrom > SessionConstants.MIN_DATE) {
            date = sdf.format(new Date(dto.servingFrom));
            model.servingFromEng = date;
            model.servingFromBan = StringUtils.convertToBanNumber(date);
        }
        if (dto.servingTo > SessionConstants.MIN_DATE) {
            date = sdf.format(new Date(dto.servingTo));
            model.servingToEng = date;
            model.servingToBan = StringUtils.convertToBanNumber(date);
        }

        return model;
    }

    public List<Employee_service_historyDTO> getDTOSgovJobbyEmployeeId(long employeeId) {
        return getDTOs(String.format(getGovJobByEmployeeId, employeeId));
    }

    public List<Employee_service_historyDTO> getDTOSGovJobByEmployeeIds(List<Long> employeeIds) {
        return ConnectionAndStatementUtil.getDTOListByNumbers(getByFirstGovtJobDTO, employeeIds, this::buildObjectFromResultSet);
    }

    public List<Employee_service_historyDTO> getDTOSbyPromotionHistoryId(long promotionHistoryId) {
        return getDTOs(String.format(getbyPromotionHistoryIdQuery, getTableName(), promotionHistoryId));
    }

    public Employee_service_historyDTO getDTObyPromotionHistoryId(long promotionHistoryId) {
        List<Employee_service_historyDTO> dtoList = getDTOSbyPromotionHistoryId(promotionHistoryId);
        if (dtoList == null || dtoList.size() == 0)
            return null;
        else
            return dtoList.get(0);
    }

    private final static long MILLISECONDS_PER_DAY = 1000L * 60 * 60 * 24;

    public void updateLastOfficeDate(EmployeeOfficeDTO officeDTO, long lastOfficeDate) {
        lastOfficeDate = lastOfficeDate - MILLISECONDS_PER_DAY;
        String updateSql = String.format(updateLastOfficeDate, getTableName(), lastOfficeDate, officeDTO.employeeRecordId, officeDTO.promotionHistoryId);
        logger.debug("Last office date sql :" + updateSql);
        ConnectionAndStatementUtil.getWriteStatement(ps -> {
            try {
                ps.executeUpdate(updateSql);
            } catch (SQLException throwables) {
                logger.error("Last Office Date Update failed");
                throwables.printStackTrace();
            }
        });
    }

    public void updateAfterPromotionHistory(Employee_service_historyDTO service_historyDTO, boolean addflag) {
        if (addflag) {
            try {
                add(service_historyDTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Employee_service_historyDTO oldDTO = getDTObyPromotionHistoryId(service_historyDTO.promotionHistoryId);
            if (oldDTO == null)
                return;
            service_historyDTO.iD = oldDTO.iD;
            try {
                update(service_historyDTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
