package employee_service_history;

import sessionmanager.SessionConstants;
import util.CommonEmployeeDTO;


public class Employee_service_historyDTO extends CommonEmployeeDTO implements Comparable<Employee_service_historyDTO> {
    public long govtServiceStartDate = SessionConstants.MIN_DATE;
    public long gazettedDate = SessionConstants.MIN_DATE;
    public long encardmentDate = SessionConstants.MIN_DATE;
    public String designation = "";
    public String department = "";
    public boolean isGovtJob = false;
    public boolean isCadre = false;
    public String cadreNumber = "";
    public int cadreBatch = 0;
    public long employeeCadreId = 0;
    public long employeeBatchesId = 0;
    public long servingFrom = SessionConstants.MIN_DATE;
    public long servingTo = SessionConstants.MIN_DATE;
    public boolean isCurrentlyWorking = false;
    public String jobResponsibility = "";
    public String office = "";
    public String description = "";
    public long filesDropzone = -1;
    public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
    public long promotionHistoryId = -1;
    public int salary = 0;

    @Override
    public String toString() {
        return "Employee_service_historyDTO{" +
                "employeeRecordsId=" + employeeRecordsId +
                ", govtServiceStartDate=" + govtServiceStartDate +
                ", gazettedDate=" + gazettedDate +
                ", encardmentDate=" + encardmentDate +
                ", employeeCadreId=" + employeeCadreId +
                ", employeeBatchesId=" + employeeBatchesId +
                ", designation='" + designation + '\'' +
                ", department='" + department + '\'' +
                ", isGovtJob=" + isGovtJob +
                ", servingFrom=" + servingFrom +
                ", servingTo=" + servingTo +
                ", office=" + office +
                ", description=" + description +
                ", isCurrentlyWorking=" + isCurrentlyWorking +
                ", jobResponsibility='" + jobResponsibility + '\'' +
                ", filesDropzone=" + filesDropzone +
                ", insertionDate=" + insertionDate +
                ", insertedBy=" + insertedBy +
                ", modifiedBy=" + modifiedBy +
                ", searchColumn='" + searchColumn + '\'' +
                '}';
    }

    public Long getGovtServiceStartDate() {
        return govtServiceStartDate;
    }

    public int compareTo(Employee_service_historyDTO o) {
        return this.getGovtServiceStartDate().compareTo(o.getGovtServiceStartDate());
    }

}