package employee_service_history;


import common.BaseServlet;
import common.EmployeeServletService;
import org.apache.log4j.Logger;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("Duplicates")
@WebServlet("/Employee_service_historyServlet")
@MultipartConfig
public class Employee_service_historyServlet extends BaseServlet implements EmployeeServletService {

    private static final long serialVersionUID = 1L;

    public static final Logger logger = Logger.getLogger(Employee_service_historyServlet.class);

    private final Employee_service_historyDAO employee_service_historyDAO = Employee_service_historyDAO.getInstance();

    @Override
    public String getTableName() {
        return employee_service_historyDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Employee_service_historyServlet";
    }

    @Override
    public Employee_service_historyDAO getCommonDAOService() {
        return employee_service_historyDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Employee_service_historyDTO employee_service_historyDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        long currentTime = System.currentTimeMillis();
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        if (addFlag) {
            employee_service_historyDTO = new Employee_service_historyDTO();
            employee_service_historyDTO.employeeRecordsId = Long.parseLong(request.getParameter("empId"));
            employee_service_historyDTO.insertedBy = userDTO.userName;
            employee_service_historyDTO.insertionDate = currentTime;
        } else {
            employee_service_historyDTO = employee_service_historyDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        employee_service_historyDTO.modifiedBy = userDTO.userName;
        employee_service_historyDTO.lastModificationTime = currentTime;
        try{
            employee_service_historyDTO.salary = Integer.parseInt(request.getParameter("salary"));
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(isLangEng?"Please enter basic salary (taka)":"অনুগ্রহ করে মূল বেতন (টাকা) লিখুন");
        }
        if(employee_service_historyDTO.salary<=0){
            throw new Exception(isLangEng?"Basic salary (taka) should be greater than zero":"মূল বেতন (টাকা) অব্যশই শূন্য থেকে বড় হবে");
        }

        String value = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("gazettedDate"));
        if (value.length()>0) {
            try {
                Date d = f.parse(value);
                employee_service_historyDTO.gazettedDate = d.getTime();
            } catch (Exception e) {
                logger.error(e);
                employee_service_historyDTO.gazettedDate = SessionConstants.MIN_DATE;
            }
        }

        employee_service_historyDTO.office = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("office"));
        if(employee_service_historyDTO.office.length() == 0){
            throw new Exception(isLangEng?"Please enter office":"অনুগ্রহ করে দপ্তর লিখুন");
        }

        employee_service_historyDTO.designation = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("designation"));
        if(employee_service_historyDTO.designation.length() == 0){
            throw new Exception(isLangEng?"Please enter designation":"অনুগ্রহ করে পদবী লিখুন");
        }

        employee_service_historyDTO.department = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("department"));
        if(employee_service_historyDTO.department.length() == 0){
            throw new Exception(isLangEng?"Please enter department":"অনুগ্রহ করে শাখা লিখুন");
        }

        employee_service_historyDTO.isCadre = Boolean.parseBoolean(request.getParameter("isCadre"));
        if(employee_service_historyDTO.isCadre) {
            try{
                employee_service_historyDTO.encardmentDate = f.parse(request.getParameter("encardmentDate")).getTime();
            }catch (Exception ex){
                throw new Exception(isLangEng?"Please enter encadrement date":"অনুগ্রহ করে ক্যাডার হবার তারিখ প্রবেশ করান");
            }

            employee_service_historyDTO.cadreNumber = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("cadreNumber"));
            if(employee_service_historyDTO.cadreNumber.length() == 0){
                throw new Exception(isLangEng?"Please enter cadre number":"অনুগ্রহ করে ক্যাডার নম্বর লিখুন");
            }

            try{
                employee_service_historyDTO.cadreBatch = Integer.parseInt(request.getParameter("cadreBatch"));
            }catch (Exception ex){
                throw new Exception(isLangEng?"Please enter cadre batch number":"অনুগ্রহ করে ক্যাডার ব্যাচ নম্বর লিখুন");
            }
        }

        try{
            employee_service_historyDTO.servingFrom = f.parse(request.getParameter("servingFrom").trim()).getTime();
        }catch (Exception ex){
            throw new Exception(isLangEng?"Enter valid service start date":"চাকরি শুরু করার তারিখ প্রবেশ করান");
        }

        try{
            employee_service_historyDTO.servingTo = f.parse(request.getParameter("servingTo")).getTime();
        }catch (Exception ex){
            throw new Exception(isLangEng?"Enter valid service end date":"চাকরি শেষ হওয়ার তারিখ প্রবেশ করান");
        }

        value = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("filesDropzone"));
        if (value.length()>0) {
            employee_service_historyDTO.filesDropzone = Long.parseLong(value);
        } else {
            logger.debug("FieldName has a null value, not updating" + " = " + value);
        }

        if (addFlag) {
            employee_service_historyDAO.add(employee_service_historyDTO);
        } else {
            employee_service_historyDAO.update(employee_service_historyDTO);
        }
        return employee_service_historyDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Employee_service_historyServlet.class;
    }

    @Override
    public String getAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request,3,"service_history");
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request,3,"service_history");
    }

    @Override
    public String getEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request,3,"service_history");
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request,3,"service_history");
    }

    @Override
    public String getDeleteRedirectURL(HttpServletRequest request) {
        return getAddOrEditOrDeleteRedirectURL(request,3,"service_history");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        search(request);
        super.doGet(request, response);
    }
}