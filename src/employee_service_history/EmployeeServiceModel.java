package employee_service_history;

public class EmployeeServiceModel {
    public Employee_service_historyDTO dto;
    public String govtServiceStartDateEng="";
    public String govtServiceStartDateBan="";
    public String gazettedDateEng="";
    public String gazettedDateBan="";
    public String encardmentDateEng="";
    public String encardmentDateBan="";
    public String servingFromEng="";
    public String servingFromBan="";
    public String servingToEng="";
    public String servingToBan="";
}
