package monthwise_doctor_report;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;
import login.LoginDTO;
import pbReport.ReportService;
import pbReport.ReportTemplate;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.ReportRequestHandler;

@WebServlet("/Monthwise_doctor_report_Servlet")
public class Monthwise_doctor_report_Servlet extends HttpServlet{
	 /**
    *
    */
   private static final long serialVersionUID = 1L;
   ReportTemplate reportTemplate = new ReportTemplate();
   String[][] Criteria =
           {
                   {"criteria", "appointment", "dr_employee_record_id", "=",  "", "String", "", "","any", "dr_user_name", LC.HM_DOCTOR + "", "userNameToEmployeeRecordId"},
                   {"criteria", "appointment", "visit_date", ">=", "AND", "long", "", "", Long.MIN_VALUE + "", "startDate", LC.HM_START_DATE + ""},
                   {"criteria", "appointment", "visit_date", "<=", "AND", "long", "", "", Long.MAX_VALUE + "", "endDate", LC.HM_END_DATE + ""},
                   {"criteria", "appointment", "isCancelled", "=", "AND", "long", "", "",  "0", "",  ""},
                   {"criteria", "appointment", "available_time_slot", "!=", "AND", "long", "", "",  "0", "",  ""},
                   {"criteria", "appointment", "speciality_type", "!=", "AND", "long", "", "",  "3", "",  ""}
           };

   String[][] Display =
           {
                   {"display", "", "appointment.dr_employee_record_id", "erIdToName", ""},
                   {"display", "", "appointment.doctor_id", "dr_dept", ""},                   
                   {"display", "", "count(appointment.id)", "int", ""},
                   {"display", "", "SUM(CASE  WHEN prescription_id >= 0 THEN 1 ELSE 0  END)", "int", ""},	 
                   {"display", "", "DATE_FORMAT(FROM_UNIXTIME(appointment.visit_date / 1000),'%Y-%m')", "text", ""},
                   {"display", "", "appointment.doctor_id", "invisible", ""}
           };

   String GroupBy = "appointment.dr_employee_record_id, col_4";
   String OrderBY = "col_4 DESC";

   public Monthwise_doctor_report_Servlet() {

   }

   private final ReportService reportService = new ReportService();

   private String sql;
   ReportRequestHandler reportRequestHandler;


   protected void doGet(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {

   	LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);

       sql = " appointment";


       Display[0][4] = LM.getText(LC.DOCTOR_PERFORMANCE_REPORT_SELECT_DOCTOR, loginDTO);
       Display[1][4] = LM.getText(LC.HM_DEPARTMENT, loginDTO);
       Display[2][4] = LM.getText(LC.HM_APPOINTMENTS, loginDTO);
       Display[3][4] = LM.getText(LC.DOCTOR_PERFORMANCE_REPORT_SELECT_PRESCRITIONS, loginDTO);
       Display[4][4] = language.equalsIgnoreCase("english")?
    		   "Year-Month"
    		   :
    		   "বছর-মাস";
       Display[5][4] = "";
       
       String reportName = language.equalsIgnoreCase("english")?
    		   "Monthwise Doctor Report"
    		   :
    		   "মাসভিত্তিক ডাক্তারের রিপোর্ট";
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(2, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(3, ReportRequestHandler.RIGHT_ALIGN_INT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "monthwise_doctor_report",
				MenuConstants.MDR_DETAILS, language, reportName, "monthwise_doctor_report");


       
   }

   protected void doPost(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {
   	doGet(request, response);
   }
}
