package monthwise_xray_report;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;
import login.LoginDTO;
import pbReport.ReportService;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.ReportRequestHandler;

@WebServlet("/Monthwise_xray_report_Servlet")
public class Monthwise_xray_report_Servlet extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","prescription_details","visit_date",">=","","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","prescription_details","visit_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},			
		{"criteria","","lab_test_type","=","AND","int","","",SessionConstants.LAB_TEST_XRAY + "","", ""}		
	};
	
	String[][] Display =
	{
		{"display","","DATE_FORMAT(FROM_UNIXTIME(visit_date / 1000),'%Y-%m')","text",""},		
		{"display","","COUNT(distinct prescription_details.id)","text",""},				
		{"display","","COUNT(distinct employee_user_name)","text",""},
		{"display","","COUNT(prescription_details.id)","text",""},
		{"display","","SUM(CASE WHEN is_done = 1 THEN 1 ELSE 0 END)","text",""},
		{"display","","SUM(CASE WHEN approval_status = 1 THEN 1 ELSE 0 END)","text",""},
	};
	
	String GroupBy = "col_0 ";
	String OrderBY = "col_0 desc";
	
	ReportRequestHandler reportRequestHandler;
	
	public Monthwise_xray_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "prescription_lab_test join prescription_details on prescription_details.id = prescription_lab_test.prescription_details_id";

		Display[0][4] = language.equalsIgnoreCase("english")? "Year-Month": "বছর-মাস";
		Display[1][4] = language.equalsIgnoreCase("english")? "Prescription With X-Ray Count": "এক্স রে বিশিষ্ট প্রেসক্রিপশনের সংখ্যা";
		Display[2][4] = language.equalsIgnoreCase("english")? "Employee Count": "কর্মকর্তা/কর্মচারীর সংখ্যা";
		Display[3][4] = language.equalsIgnoreCase("english")? "Prescribed X-Ray Count": "প্রেস্ক্রিপশনক্রিত এক্স রের সংখ্যা";
		Display[4][4] = language.equalsIgnoreCase("english")? "Actual X-Ray Count": "প্রকৃত এক্স রের সংখ্যা";
		Display[5][4] = language.equalsIgnoreCase("english")? "Report Count": "রিপোর্টের সংখ্যা";

		
	    String reportName = language.equalsIgnoreCase("english")?
	    		   "Monthwise X-Ray Report"
	    		   :
	    		   "মাসভিত্তিক এক্স-রে  রিপোর্ট";
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(1, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(2, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(3, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(4, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(5, ReportRequestHandler.RIGHT_ALIGN_INT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "monthwise_xray_report",
				MenuConstants.MXR_REPORT, language, reportName, "monthwise_xray_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
