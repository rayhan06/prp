package doctor_time_slot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;
import util.TimeFormat;


public class Doctor_time_slotRepository implements Repository {
	Doctor_time_slotDAO doctor_time_slotDAO = null;
	
	public void setDAO(Doctor_time_slotDAO doctor_time_slotDAO)
	{
		this.doctor_time_slotDAO = doctor_time_slotDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Doctor_time_slotRepository.class);
	Map<Long, Doctor_time_slotDTO>mapOfDoctor_time_slotDTOToiD;
	


	static Doctor_time_slotRepository instance = null;  
	private Doctor_time_slotRepository(){
		mapOfDoctor_time_slotDTOToiD = new ConcurrentHashMap<>();
	
		this.doctor_time_slotDAO = new Doctor_time_slotDAO();
		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Doctor_time_slotRepository getInstance(){
		if (instance == null){
			instance = new Doctor_time_slotRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(doctor_time_slotDAO == null)
		{
			return;
		}
		try {
			List<Doctor_time_slotDTO> doctor_time_slotDTOs = doctor_time_slotDAO.getAllDoctor_time_slot(reloadAll);
			for(Doctor_time_slotDTO doctor_time_slotDTO : doctor_time_slotDTOs) {
				Doctor_time_slotDTO oldDoctor_time_slotDTO = getDoctor_time_slotDTOByID(doctor_time_slotDTO.iD);
				if( oldDoctor_time_slotDTO != null ) {
					mapOfDoctor_time_slotDTOToiD.remove(oldDoctor_time_slotDTO.iD);
				
					
					
					
				}
				if(doctor_time_slotDTO.isDeleted == 0) 
				{
					
					mapOfDoctor_time_slotDTOToiD.put(doctor_time_slotDTO.iD, doctor_time_slotDTO);
								
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Doctor_time_slotDTO> getDoctor_time_slotList() {
		List <Doctor_time_slotDTO> doctor_time_slots = new ArrayList<Doctor_time_slotDTO>(this.mapOfDoctor_time_slotDTOToiD.values());
		return doctor_time_slots;
	}
	
	public List<Doctor_time_slotDTO> getSlotDTOsByDrDay (long doctor, int dayCat, int weekCat){
		List <Doctor_time_slotDTO> all = new ArrayList<Doctor_time_slotDTO>(this.mapOfDoctor_time_slotDTOToiD.values());
		List <Doctor_time_slotDTO> doctor_time_slots = new ArrayList<Doctor_time_slotDTO> ();
		for(Doctor_time_slotDTO doctor_time_slotDTO: all)
		{
			if(doctor_time_slotDTO.doctorId == doctor && doctor_time_slotDTO.dayCat == dayCat 
					&& (doctor_time_slotDTO.weekCats.contains("0") || doctor_time_slotDTO.weekCats.equals("")
							|| doctor_time_slotDTO.weekCats.contains(weekCat+ "")))
			{
				doctor_time_slots.add(doctor_time_slotDTO);
			}
		}
		return doctor_time_slots;
	}
	
	public List<Doctor_time_slotDTO> getSlotDTOsByDay (int dayCat, int weekCat){
		List <Doctor_time_slotDTO> all = new ArrayList<Doctor_time_slotDTO>(this.mapOfDoctor_time_slotDTOToiD.values());
		List <Doctor_time_slotDTO> doctor_time_slots = new ArrayList<Doctor_time_slotDTO> ();
		for(Doctor_time_slotDTO doctor_time_slotDTO: all)
		{
			if(doctor_time_slotDTO.dayCat == dayCat 
					&& (doctor_time_slotDTO.weekCats.contains("0") || doctor_time_slotDTO.weekCats.equals("")
							|| doctor_time_slotDTO.weekCats.contains(weekCat+ "")))
			{
				doctor_time_slots.add(doctor_time_slotDTO);
			}
		}
		return doctor_time_slots;
	}
	
	public List<Doctor_time_slotDTO> getSlotDTOsToday (){
		int weekCat = TimeFormat.getNthWeek();
		Calendar cal = Calendar.getInstance();
        int dayCat = cal.get(Calendar.DAY_OF_WEEK) % 7;
        return getSlotDTOsByDay (dayCat, weekCat);
	}
	
	
	public Doctor_time_slotDTO getDoctor_time_slotDTOByID( long ID){
		return mapOfDoctor_time_slotDTOToiD.get(ID);
	}
	
	

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "doctor_time_slot";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


