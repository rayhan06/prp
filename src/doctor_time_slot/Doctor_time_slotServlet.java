package doctor_time_slot;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;
import shift_slot.Shift_slotDTO;
import shift_slot.Shift_slotRepository;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;
import util.TimeFormat;


import javax.servlet.http.*;


import com.google.gson.Gson;

import appointment.AppointmentDAO;
import employee_offices.EmployeeOfficeRepository;
import pb.*;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Doctor_time_slotServlet
 */
@WebServlet("/Doctor_time_slotServlet")
@MultipartConfig
public class Doctor_time_slotServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Doctor_time_slotServlet.class);

    String tableName = "doctor_time_slot";

	Doctor_time_slotDAO doctor_time_slotDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Doctor_time_slotServlet() 
	{
        super();
    	try
    	{
			doctor_time_slotDAO = new Doctor_time_slotDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(doctor_time_slotDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DOCTOR_TIME_SLOT_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DOCTOR_TIME_SLOT_UPDATE))
				{
					getDoctor_time_slot(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("viewAll"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DOCTOR_TIME_SLOT_SEARCH))
				{
					request.getRequestDispatcher("doctor_time_slot/viewAll.jsp").forward(request, response);
                }
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("addSlots"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DOCTOR_TIME_SLOT_UPDATE))
				{
					Doctor_time_slotDTO doctor_time_slotDTO = doctor_time_slotDAO.get1stDTOByDoctor(Long.parseLong(request.getParameter("doctorId")));
					if(doctor_time_slotDTO == null)
					{
						request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
					}
					else
					{
						getDoctor_time_slot(request, response, doctor_time_slotDTO.iD);
					}
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DOCTOR_TIME_SLOT_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchDoctor_time_slot(request, response, isPermanentTable, filter);
						}
						else
						{
							searchDoctor_time_slot(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchDoctor_time_slot(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DOCTOR_TIME_SLOT_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DOCTOR_TIME_SLOT_ADD))
				{
					System.out.println("going to  addDoctor_time_slot ");
					addDoctor_time_slot(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addDoctor_time_slot ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DOCTOR_TIME_SLOT_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addDoctor_time_slot ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("getSlotId"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DOCTOR_TIME_SLOT_ADD))
				{
					long dr = Long.parseLong(request.getParameter("dr"));
					int shift = Integer.parseInt(request.getParameter("shift"));
					
					Doctor_time_slotDTO doctor_time_slotDTO = doctor_time_slotDAO.getSlotDTOByDoctorShift(dr, shift);
					if(doctor_time_slotDTO == null)
					{
						response.getWriter().write("-1");
					}
					else
					{
						response.getWriter().write(doctor_time_slotDTO.iD + "");
					}
				}
				else
				{
					System.out.println("Not going to  addDoctor_time_slot ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DOCTOR_TIME_SLOT_UPDATE))
				{					
					addDoctor_time_slot(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("massEdit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DOCTOR_TIME_SLOT_UPDATE))
				{					
					massEdit(request, response, userDTO);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DOCTOR_TIME_SLOT_UPDATE))
				{
					try 
					{
						String[] IDsToDelete = request.getParameterValues("ID");
						for(int i = 0; i < IDsToDelete.length; i ++)
						{
							long id = Long.parseLong(IDsToDelete[i]);
							System.out.println("------ DELETING " + IDsToDelete[i]);
							
							
							Doctor_time_slotDTO dtDTO = doctor_time_slotDAO.getDTOByID(id);
							if(dtDTO != null)
							{
								dtDTO.isAvailable = false;
								dtDTO.deptCat = -3;
								doctor_time_slotDAO.setEmployeeOffices(dtDTO);								
							}
							doctor_time_slotDAO.delete(id);							
				
						}
						response.sendRedirect( "Doctor_time_slotServlet?actionType=search");
					}
					catch (Exception ex) 
					{
						ex.printStackTrace();
					}
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DOCTOR_TIME_SLOT_SEARCH))
				{
					searchDoctor_time_slot(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	

	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Doctor_time_slotDTO doctor_time_slotDTO = doctor_time_slotDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(doctor_time_slotDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	AppointmentDAO appointmentDAO = new AppointmentDAO();
	private void massEdit(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception 
	{
		
		long doctorId = Long.parseLong(request.getParameter("doctorId"));
		
		Doctor_time_slotDTO doctor_time_slotDTO = doctor_time_slotDAO.get1stDTOByDoctor(doctorId);
		if(doctor_time_slotDTO == null)
		{
			request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
		}
		else
		{
			String Value = request.getParameter("deptCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("deptCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				doctor_time_slotDTO.deptCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("medicalDeptCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("medicalDeptCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				doctor_time_slotDTO.medicalDeptCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("isAvailable");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("isAvailable = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				doctor_time_slotDTO.isAvailable = true;
			}
			else
			{
				doctor_time_slotDTO.isAvailable = false;
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("unavailabilityReason");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("unavailabilityReason = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				doctor_time_slotDTO.unavailabilityReason = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("qualifications");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("qualifications = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				doctor_time_slotDTO.qualifications = (Value);
			}
			else
			{
				doctor_time_slotDTO.qualifications = "";
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("durationPerPatient");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("durationPerPatient = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				doctor_time_slotDTO.durationPerPatient = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			if(appointmentDAO.isDoctor(doctor_time_slotDTO.doctorId) && doctor_time_slotDTO.durationPerPatient <= 0)
			{
				response.sendRedirect("Doctor_time_slotServlet?actionType=search");
				return;
			}
			
			if(appointmentDAO.isPhysiotherapist(doctor_time_slotDTO.doctorId))
			{
				doctor_time_slotDAO.update(doctor_time_slotDTO);
			}
				
			
			
			
			List<CatDTO> shifts = CatRepository.getDTOs("shift");
			int countSlot = 0;
			for(int day = 0; day < 7; day ++)
			{
				for(CatDTO shiftCatDTO: shifts)
				{
					int shiftCat = shiftCatDTO.value;
					if(request.getParameter(day + "_" + shiftCat) == null)
					{
						countSlot ++;
					}
				}
			}
			if(appointmentDAO.isDoctor(doctor_time_slotDTO.doctorId) && countSlot > 0)
			{
				doctor_time_slotDAO.deleteChildrenByParent(doctorId, "doctor_id");
				for(int day = 0; day < 7; day ++)
				{
					for(CatDTO shiftCatDTO: shifts)
					{
						int shiftCat = shiftCatDTO.value;										 
						if(request.getParameter(day + "_" + shiftCat) != null)
						{
							Doctor_time_slotDTO dtDTO = new Doctor_time_slotDTO();
							
							
							dtDTO.doctorId = doctor_time_slotDTO.doctorId;
							dtDTO.deptCat = doctor_time_slotDTO.deptCat;
							dtDTO.medicalDeptCat = doctor_time_slotDTO.medicalDeptCat;
							dtDTO.qualifications = doctor_time_slotDTO.qualifications;
							dtDTO.isAvailable = doctor_time_slotDTO.isAvailable;
							dtDTO.unavailabilityReason = doctor_time_slotDTO.unavailabilityReason;
							dtDTO.durationPerPatient = doctor_time_slotDTO.durationPerPatient;
							dtDTO.shiftCat = shiftCat;
							dtDTO.dayCat = day;
							
							String []weekCats = request.getParameterValues("weekCats_" + day + "_"+ shiftCat);
							if(weekCats != null)
							{
								dtDTO.weekCats = "";
								for(String weekCat: weekCats)
								{
									dtDTO.weekCats += weekCat+ ", ";
									if(weekCat.equalsIgnoreCase("0"))
									{
										break;
									}
								}
							}
							
							Shift_slotDTO shift_slotDTO = Shift_slotRepository.getInstance().getShift_slotDTOByShiftCat(dtDTO.shiftCat);
							if(shift_slotDTO != null)
							{
								dtDTO.shiftSlotId = shift_slotDTO.iD;
								dtDTO.startTime = shift_slotDTO.startTime;
								dtDTO.endTime = shift_slotDTO.endTime;

								System.out.println("##dts inserting " + day + "_"+ shiftCat);
								doctor_time_slotDAO.add(dtDTO);
								
							}
							else
							{
								System.out.println("shift_slotDTOis null for " + dtDTO.shiftCat);
							}
							
						}
					}
				}
			}
			
		}
		
		Doctor_time_slotRepository.getInstance().reload(false);
		 Utils.runIOTaskInASeparateThread(()->()->{
             logger.debug("going to updating cache all after role change");
             EmployeeOfficeRepository.getInstance().reload(true);
             logger.debug("cache updating is done");
         });
		
		Set<Long> drOrgs = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.DOCTOR_ROLE);
        logger.debug("dts update drOrgs.contains(" + doctor_time_slotDTO.doctorId + ") = " + drOrgs.contains(doctor_time_slotDTO.doctorId));


		response.sendRedirect("Doctor_time_slotServlet?actionType=search");

	}

	private void addDoctor_time_slot(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addDoctor_time_slot");
			String path = getServletContext().getRealPath("/img2/");
			Doctor_time_slotDTO doctor_time_slotDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				doctor_time_slotDTO = new Doctor_time_slotDTO();
			}
			else
			{
				doctor_time_slotDTO = doctor_time_slotDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("deptCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("deptCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				doctor_time_slotDTO.deptCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("medicalDeptCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("medicalDeptCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				doctor_time_slotDTO.medicalDeptCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("doctorId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("doctorId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				doctor_time_slotDTO.doctorId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("shiftCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("shiftCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				doctor_time_slotDTO.shiftCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			if(addFlag)
			{
				Doctor_time_slotDTO oldDTO = doctor_time_slotDAO.getSlotDTOByDoctorShift(doctor_time_slotDTO.doctorId, doctor_time_slotDTO.shiftCat);
				if(oldDTO != null)
				{
					response.sendRedirect("Doctor_time_slotServlet?actionType=search");
					return;
				}
			}
			
			AppointmentDAO appointmentDAO = new AppointmentDAO();
			if(!appointmentDAO.isDoctorOrPhysiotherapist(doctor_time_slotDTO.doctorId))
			{
				response.sendRedirect("Doctor_time_slotServlet?actionType=search");
				return;
			}
			
			Value = request.getParameter("isAvailable");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("isAvailable = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				doctor_time_slotDTO.isAvailable = true;
			}
			else
			{
				doctor_time_slotDTO.isAvailable = false;
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("unavailabilityReason");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("unavailabilityReason = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				doctor_time_slotDTO.unavailabilityReason = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("qualifications");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("qualifications = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				doctor_time_slotDTO.qualifications = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("startTime");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("startTime = " + Value);
			if(Value != null)
			{
				doctor_time_slotDTO.startTime = TimeFormat.getIn24HourFormat(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("endTime");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("endTime = " + Value);
			if(Value != null)
			{
				System.out.println(" After conversion to 24 hours: " + TimeFormat.getIn24HourFormat(Value));
				doctor_time_slotDTO.endTime = TimeFormat.getIn24HourFormat(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			if(appointmentDAO.isDoctor(doctor_time_slotDTO.doctorId) 
					&& TimeFormat.getMillisFromHourMiunte(doctor_time_slotDTO.endTime) < TimeFormat.getMillisFromHourMiunte(doctor_time_slotDTO.startTime))
			{
				response.sendRedirect("Doctor_time_slotServlet?actionType=search");
				return;
			}

			Value = request.getParameter("durationPerPatient");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("durationPerPatient = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				doctor_time_slotDTO.durationPerPatient = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			if(appointmentDAO.isDoctor(doctor_time_slotDTO.doctorId) && doctor_time_slotDTO.durationPerPatient <= 0)
			{
				response.sendRedirect("Doctor_time_slotServlet?actionType=search");
				return;
			}

			System.out.println("Done adding  addDoctor_time_slot dto = " + doctor_time_slotDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				doctor_time_slotDAO.setIsDeleted(doctor_time_slotDTO.iD, CommonDTO.OUTDATED);
				returnedID = doctor_time_slotDAO.add(doctor_time_slotDTO);
				doctor_time_slotDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = doctor_time_slotDAO.manageWriteOperations(doctor_time_slotDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = doctor_time_slotDAO.manageWriteOperations(doctor_time_slotDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			Doctor_time_slotRepository.getInstance().reload(false);
			EmployeeOfficeRepository.getInstance().reload(true);
			Set<Long> drOrgs = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.DOCTOR_ROLE);
            logger.debug("dts update drOrgs.contains(" + doctor_time_slotDTO.doctorId + ") = " + drOrgs.contains(doctor_time_slotDTO.doctorId));
			response.sendRedirect("Doctor_time_slotServlet?actionType=search");
									
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void getDoctor_time_slot(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getDoctor_time_slot");
		Doctor_time_slotDTO doctor_time_slotDTO = null;
		try 
		{
			doctor_time_slotDTO = doctor_time_slotDAO.getDTOByID(id);
			request.setAttribute("ID", doctor_time_slotDTO.iD);
			request.setAttribute("doctor_time_slotDTO",doctor_time_slotDTO);
			request.setAttribute("doctor_time_slotDAO",doctor_time_slotDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "doctor_time_slot/doctor_time_slotInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "doctor_time_slot/doctor_time_slotSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "doctor_time_slot/doctor_time_slotEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "doctor_time_slot/doctor_time_slotEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getDoctor_time_slot(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getDoctor_time_slot(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchDoctor_time_slot(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchDoctor_time_slot 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_DOCTOR_TIME_SLOT,
			request,
			doctor_time_slotDAO,
			SessionConstants.VIEW_DOCTOR_TIME_SLOT,
			SessionConstants.SEARCH_DOCTOR_TIME_SLOT,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("doctor_time_slotDAO",doctor_time_slotDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to doctor_time_slot/doctor_time_slotApproval.jsp");
	        	rd = request.getRequestDispatcher("doctor_time_slot/doctor_time_slotApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to doctor_time_slot/doctor_time_slotApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("doctor_time_slot/doctor_time_slotApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to doctor_time_slot/doctor_time_slotSearch.jsp");
	        	rd = request.getRequestDispatcher("doctor_time_slot/doctor_time_slotSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to doctor_time_slot/doctor_time_slotSearchForm.jsp");
	        	rd = request.getRequestDispatcher("doctor_time_slot/doctor_time_slotSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

