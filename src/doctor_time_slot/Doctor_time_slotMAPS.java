package doctor_time_slot;
import java.util.*; 
import util.*;


public class Doctor_time_slotMAPS extends CommonMaps
{	
	public Doctor_time_slotMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("deptCat".toLowerCase(), "deptCat".toLowerCase());
		java_DTO_map.put("doctorId".toLowerCase(), "doctorId".toLowerCase());
		java_DTO_map.put("shiftCat".toLowerCase(), "shiftCat".toLowerCase());
		java_DTO_map.put("startTime".toLowerCase(), "startTime".toLowerCase());
		java_DTO_map.put("endTime".toLowerCase(), "endTime".toLowerCase());
		java_DTO_map.put("durationPerPatient".toLowerCase(), "durationPerPatient".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("dept_cat".toLowerCase(), "deptCat".toLowerCase());
		java_SQL_map.put("doctor_id".toLowerCase(), "doctorId".toLowerCase());
		java_SQL_map.put("shift_cat".toLowerCase(), "shiftCat".toLowerCase());
		java_SQL_map.put("start_time".toLowerCase(), "startTime".toLowerCase());
		java_SQL_map.put("end_time".toLowerCase(), "endTime".toLowerCase());
		java_SQL_map.put("duration_per_patient".toLowerCase(), "durationPerPatient".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Dept".toLowerCase(), "deptCat".toLowerCase());
		java_Text_map.put("Doctor Id".toLowerCase(), "doctorId".toLowerCase());
		java_Text_map.put("Shift".toLowerCase(), "shiftCat".toLowerCase());
		java_Text_map.put("Start Time".toLowerCase(), "startTime".toLowerCase());
		java_Text_map.put("End Time".toLowerCase(), "endTime".toLowerCase());
		java_Text_map.put("Duration Per Patient".toLowerCase(), "durationPerPatient".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}