package doctor_time_slot;
import java.util.*;

import user.UserDTO;
import util.*; 


public class Doctor_time_slotDTO extends CommonDTO
{

	public int deptCat = -1;
	public int medicalDeptCat = -1;
	public long doctorId = -2;
	public int shiftCat = -2;
    public String startTime = "";
    public String endTime = "";
    public String qualifications = "";
	public long durationPerPatient = 5;
	public long shiftSlotId = 0;
	public int dayCat = 0;
	
	public String weekCats = "0";
	public List<Integer> weekList = new ArrayList<Integer>();
    
    public boolean isAvailable = true;
    public String unavailabilityReason = "";
    
    public Doctor_time_slotDTO()
    {
    	
    }
	
    public Doctor_time_slotDTO(UserDTO userDTO)
    {
    	doctorId = userDTO.organogramID;
    }
	
    @Override
	public String toString() {
            return "$Doctor_time_slotDTO[" +
            " iD = " + iD +
            " deptCat = " + deptCat +
            " doctorId = " + doctorId +
            " shiftCat = " + shiftCat +
            " startTime = " + startTime +
            " endTime = " + endTime +
            " durationPerPatient = " + durationPerPatient +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}