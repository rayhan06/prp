package doctor_time_slot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import sessionmanager.SessionConstants;
import shift_slot.Shift_slotDTO;

import java.sql.SQLException;
import java.sql.Statement;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.*;
import pb.*;
import user.UserDTO;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import user.UserRepository;
import workflow.WorkflowController;


public class Doctor_time_slotDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Doctor_time_slotDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Doctor_time_slotMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"dept_cat",
			"doctor_id",
			"shift_cat",
			"shift_slot_id",
			"day_cat",
			"start_time",
			"end_time",
			"duration_per_patient",
			"week_cats",
			"search_column",
			"qualifications",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Doctor_time_slotDAO()
	{
		this("doctor_time_slot");		
	}
	
	public void setSearchColumn(Doctor_time_slotDTO doctor_time_slotDTO)
	{
		doctor_time_slotDTO.searchColumn = "";
		doctor_time_slotDTO.searchColumn += CatDAO.getName("English", "department", doctor_time_slotDTO.deptCat) + " " + CatDAO.getName("Bangla", "department", doctor_time_slotDTO.deptCat) + " ";
		doctor_time_slotDTO.searchColumn += CatDAO.getName("English", "medical_dept", doctor_time_slotDTO.medicalDeptCat) + " " + CatDAO.getName("Bangla", "medical_dept", doctor_time_slotDTO.medicalDeptCat) + " ";
		doctor_time_slotDTO.searchColumn += CatDAO.getName("English", "shift", doctor_time_slotDTO.shiftCat) + " " + CatDAO.getName("Bangla", "shift", doctor_time_slotDTO.shiftCat) + " ";
		try {
			doctor_time_slotDTO.searchColumn += WorkflowController.getNameFromOrganogramId(doctor_time_slotDTO.doctorId, "English") + " " + WorkflowController.getNameFromOrganogramId(doctor_time_slotDTO.doctorId, "Bangla") + " ";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		doctor_time_slotDTO.searchColumn += doctor_time_slotDTO.durationPerPatient + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Doctor_time_slotDTO doctor_time_slotDTO = (Doctor_time_slotDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(doctor_time_slotDTO);
		if(isInsert)
		{
			ps.setObject(index++,doctor_time_slotDTO.iD);
		}
		ps.setObject(index++,doctor_time_slotDTO.deptCat);
		ps.setObject(index++,doctor_time_slotDTO.doctorId);
		ps.setObject(index++,doctor_time_slotDTO.shiftCat);
		ps.setObject(index++,doctor_time_slotDTO.shiftSlotId);
		ps.setObject(index++,doctor_time_slotDTO.dayCat);
		ps.setObject(index++,doctor_time_slotDTO.startTime);
		ps.setObject(index++,doctor_time_slotDTO.endTime);
		ps.setObject(index++,doctor_time_slotDTO.durationPerPatient);
		ps.setObject(index++,doctor_time_slotDTO.weekCats);
		ps.setObject(index++,doctor_time_slotDTO.searchColumn);
		ps.setObject(index++,doctor_time_slotDTO.qualifications);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
		setEmployeeOffices(doctor_time_slotDTO);
	}
	
	
	
	public Doctor_time_slotDTO build(ResultSet rs)
	{
		try
		{
			Doctor_time_slotDTO doctor_time_slotDTO = new Doctor_time_slotDTO();
			doctor_time_slotDTO.iD = rs.getLong("ID");
			doctor_time_slotDTO.deptCat = rs.getInt("dept_cat");
			doctor_time_slotDTO.doctorId = rs.getLong("doctor_id");
			doctor_time_slotDTO.shiftCat = rs.getInt("shift_cat");
			doctor_time_slotDTO.shiftSlotId = rs.getInt("shift_slot_id");
			doctor_time_slotDTO.dayCat = rs.getInt("day_cat");
			doctor_time_slotDTO.startTime = rs.getString("start_time");
			doctor_time_slotDTO.endTime = rs.getString("end_time");
			doctor_time_slotDTO.durationPerPatient = rs.getLong("duration_per_patient");
			doctor_time_slotDTO.weekCats = rs.getString("week_cats");
			doctor_time_slotDTO.searchColumn = rs.getString("search_column");
			doctor_time_slotDTO.qualifications = rs.getString("qualifications");
			doctor_time_slotDTO.isDeleted = rs.getInt("isDeleted");
			doctor_time_slotDTO.lastModificationTime = rs.getLong("lastModificationTime");
			
			doctor_time_slotDTO = getFromEmployeeOffices(doctor_time_slotDTO);
			
			String [] weekCats = doctor_time_slotDTO.weekCats.split(", ");
			if(weekCats != null)
			{				
				for(String weekCat: weekCats)
				{
					int iWeekCat = Integer.parseInt(weekCat);
					doctor_time_slotDTO.weekList.add(iWeekCat);
				}
			}
			return doctor_time_slotDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public Doctor_time_slotDTO build2(ResultSet rs)
	{
		try
		{
			Doctor_time_slotDTO doctor_time_slotDTO = new Doctor_time_slotDTO();
			doctor_time_slotDTO.iD = rs.getLong("ID");
			doctor_time_slotDTO.deptCat = rs.getInt("dept_cat");
			doctor_time_slotDTO.doctorId = rs.getLong("doctor_id");
			doctor_time_slotDTO.shiftCat = rs.getInt("shift_cat");
			doctor_time_slotDTO.shiftSlotId = rs.getInt("shift_slot_id");
			doctor_time_slotDTO.dayCat = rs.getInt("day_cat");
			doctor_time_slotDTO.startTime = rs.getString("start_time");
			doctor_time_slotDTO.endTime = rs.getString("end_time");
			doctor_time_slotDTO.durationPerPatient = rs.getLong("duration_per_patient");
			doctor_time_slotDTO.weekCats = rs.getString("week_cats");
			doctor_time_slotDTO.searchColumn = rs.getString("search_column");
			doctor_time_slotDTO.qualifications = rs.getString("qualifications");
			doctor_time_slotDTO.isDeleted = rs.getInt("isDeleted");
			doctor_time_slotDTO.lastModificationTime = rs.getLong("lastModificationTime");
			
			
			return doctor_time_slotDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public long updateShift(Shift_slotDTO shift_slotDTO)
	{
		String sql = "Update doctor_time_slot set lastModificationTime = ?"
				+ ", start_time = ?"
				+ ", end_time = ?" 
				
				+ " where isDeleted = 0 and shift_cat = " + shift_slotDTO.shiftCat;
		long lastModificationTime = System.currentTimeMillis();
		
		return (long) ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            Connection connection = model.getConnection();
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
            	int index = 1;
    			ps.setObject(index++,lastModificationTime);
    			ps.setObject(index++,shift_slotDTO.startTime );
    			ps.setObject(index++,shift_slotDTO.endTime);
    		
                ps.executeUpdate();
                recordUpdateTime(connection, lastModificationTime);
                EmployeeOfficeRepository.getInstance().reload(false);
                return shift_slotDTO.iD;
            } catch (SQLException ex) {
                logger.error(ex);
                return -1L;
            }
        }, sql);
	}
	

	public long setEmployeeOffices(Doctor_time_slotDTO doctor_time_slotDTO)
	{
		String sql = "Update employee_offices set lastModificationTime = ?"
				+ ", isAvailable = ?"
				+ ", unavailability_reason = ?" 
				+ ", dept = ?" 
				+ ", duration_per_patient = ?" 
				+ ", medical_dept_cat = ?" 
				+ ", dr_qualifications = ?" 
				+ " where isDeleted = 0 and status = 1 and office_unit_organogram_id = " + doctor_time_slotDTO.doctorId;
		long lastModificationTime = System.currentTimeMillis();
		
		return (long) ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            Connection connection = model.getConnection();
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
            	int index = 1;
    			ps.setObject(index++,lastModificationTime);
    			ps.setObject(index++,doctor_time_slotDTO.isAvailable );
    			ps.setObject(index++,doctor_time_slotDTO.unavailabilityReason);
    			ps.setObject(index++,doctor_time_slotDTO.deptCat);
    			ps.setObject(index++,doctor_time_slotDTO.durationPerPatient);
    			ps.setObject(index++,doctor_time_slotDTO.medicalDeptCat);
    			ps.setObject(index++,doctor_time_slotDTO.qualifications);

                ps.executeUpdate();
                recordUpdateTime(connection, lastModificationTime);
                EmployeeOfficeRepository.getInstance().reload(false);
                return doctor_time_slotDTO.iD;
            } catch (SQLException ex) {
                logger.error(ex);
                return -1L;
            }
        }, sql);
	}
	
	public boolean isAvailable(long drId)
	{
		EmployeeOfficeDTO drOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(drId);
		if(drOfficeDTO == null )
		{	logger.debug("drOfficeDTO == null");
			return false;
		}
		if(drOfficeDTO.isAvailable)
		{
			logger.debug("drOfficeDTO isAvailable");
			return true;
		}
		logger.debug("drOfficeDTO is Not Available");
		return false;
		
	}
	
	public Doctor_time_slotDTO getFromEO(ResultSet rs)
	{
		try
		{
			Doctor_time_slotDTO doctor_time_slotDTO = new Doctor_time_slotDTO();
			doctor_time_slotDTO.isAvailable = rs.getBoolean("isAvailable");
			doctor_time_slotDTO.unavailabilityReason = rs.getString("unavailability_reason");
			doctor_time_slotDTO.deptCat = rs.getInt("dept");
			doctor_time_slotDTO.durationPerPatient = rs.getInt("duration_per_patient");
			doctor_time_slotDTO.medicalDeptCat = rs.getInt("medical_dept_cat");
			doctor_time_slotDTO.qualifications = rs.getString("dr_qualifications");
			return doctor_time_slotDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}

	public Doctor_time_slotDTO getFromEmployeeOffices(Doctor_time_slotDTO doctor_time_slotDTO)
	{
		if(doctor_time_slotDTO == null)
		{
			return null;
		}
		String sql = "SELECT isAvailable, unavailability_reason, dept, duration_per_patient, medical_dept_cat, dr_qualifications FROM employee_offices "
				+ "where isDeleted = 0 and status = 1 and office_unit_organogram_id = " + doctor_time_slotDTO.doctorId;
		Doctor_time_slotDTO newDoctor_time_slotDTO = ConnectionAndStatementUtil.getT(sql,this::getFromEO);
		if(newDoctor_time_slotDTO != null)
		{
			doctor_time_slotDTO.isAvailable = newDoctor_time_slotDTO.isAvailable;
			doctor_time_slotDTO.unavailabilityReason = newDoctor_time_slotDTO.unavailabilityReason;
			doctor_time_slotDTO.deptCat = newDoctor_time_slotDTO.deptCat;
			doctor_time_slotDTO.durationPerPatient = newDoctor_time_slotDTO.durationPerPatient;
			doctor_time_slotDTO.medicalDeptCat = newDoctor_time_slotDTO.medicalDeptCat;
			doctor_time_slotDTO.qualifications = newDoctor_time_slotDTO.qualifications;
		}		
		if(doctor_time_slotDTO.qualifications == null)
		{
			doctor_time_slotDTO.qualifications = "";
		}
		return doctor_time_slotDTO;
	}

	public Doctor_time_slotDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Doctor_time_slotDTO doctor_time_slotDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return doctor_time_slotDTO;
	}

	
	public List<Doctor_time_slotDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}

	public Doctor_time_slotDTO getSlotDTOByDoctorShift (long doctor, long shift) throws Exception
	{
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
		
        sql += " WHERE isDeleted = 0 and doctor_id =" +  doctor + " and shift_cat = " + shift + " order by id desc limit 1";
        Doctor_time_slotDTO doctor_time_slotDTO = ConnectionAndStatementUtil.getT(sql,this::build);
        
        doctor_time_slotDTO = getFromEmployeeOffices(doctor_time_slotDTO);
		return doctor_time_slotDTO;
	}
	
	 public long deleteSlotDTOByDoctorShiftDay(long doctor, long shift, int dayCat) throws Exception {
	        long lastModificationTime = System.currentTimeMillis();
	        StringBuilder sqlBuilder = new StringBuilder("delete from ")
	                .append(tableName)
	                .append(" WHERE ")
	                .append("doctor_id = ").append(doctor)
	                .append(" and shift_cat = ").append(shift)
	                .append(" and day_cat = ").append(dayCat);
	        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
	            String sql = sqlBuilder.toString();
	            Connection connection = model.getConnection();
	            Statement stmt = model.getStatement();
	            try {
	                logger.debug(sql);
	                stmt.execute(sql);
	                recordUpdateTime(connection, lastModificationTime);
	            } catch (SQLException ex) {
	                logger.error(ex);
	            }
	        });
	        return 1;
	    }
	
	public Doctor_time_slotDTO getSlotDTOByDoctorShiftDay (long doctor, long shift, int dayCat) throws Exception
	{
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
		
        sql += " WHERE isDeleted = 0 and doctor_id =" +  doctor + " and shift_cat = " + shift + " and day_cat = " + dayCat + " order by id desc limit 1";
        Doctor_time_slotDTO doctor_time_slotDTO = ConnectionAndStatementUtil.getT(sql,this::build);
        
        doctor_time_slotDTO = getFromEmployeeOffices(doctor_time_slotDTO);
		return doctor_time_slotDTO;
	}
	
	public List<Doctor_time_slotDTO> getSlotDTOByDayWeek (int dayCat, int weekCat)
	{
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
		
        sql += " WHERE isDeleted = 0 "  
        		+ " and day_cat = " + dayCat 
        		+ " and (week_cats like '%0%' or week_cats like '%" + weekCat + "%')" 
        		+ " order by id";
        return ConnectionAndStatementUtil.getListOfT(sql,this::build2);

	}
	
	public List<Doctor_time_slotDTO> getSlotDTOsByShiftDay ( long shift, int dayCat)
	{
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
		
        sql += " WHERE isDeleted = 0 and shift_cat = " + shift + " and day_cat = " + dayCat + " order by id desc limit 1";
        return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}
	
	public List<Doctor_time_slotDTO> getSlotDTOsToday()
	{
		int weekCat = TimeFormat.getNthWeek();
		Calendar cal = Calendar.getInstance();
        int dayCat = cal.get(Calendar.DAY_OF_WEEK) % 7;
        return getSlotDTOByDayWeek (dayCat, weekCat);
	}
	
	public Doctor_time_slotDTO getSlotDTOByDoctorShiftDayWeek (long doctor, long shift, int dayCat, int weekCat) throws Exception
	{
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
		
        sql += " WHERE isDeleted = 0 and doctor_id =" +  doctor 
        		+ " and shift_cat = " + shift 
        		+ " and day_cat = " + dayCat 
        		+ " and (week_cats like '%0%' or week_cats like '%" + weekCat + "%')" 
        		+ " order by id desc limit 1";
        Doctor_time_slotDTO doctor_time_slotDTO = ConnectionAndStatementUtil.getT(sql,this::build);
        
        doctor_time_slotDTO = getFromEmployeeOffices(doctor_time_slotDTO);
		return doctor_time_slotDTO;
	}
	
	public List<Doctor_time_slotDTO> getSlotDTOsByDrDay (long doctor, int dayCat) throws Exception
	{
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
		
        sql += " WHERE isDeleted = 0 and doctor_id =" +  doctor  + " and day_cat = " + dayCat + " order by shift_cat asc";
        return ConnectionAndStatementUtil.getListOfT(sql,this::build);

	}
	
	public List<Doctor_time_slotDTO> getSlotDTOsByDrDay (long doctor, int dayCat, int weekCat) throws Exception
	{
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
		
        sql += " WHERE isDeleted = 0 and doctor_id =" +  doctor 
        		+ " and day_cat = " + dayCat 
        		+ " and (week_cats like '%0%' or week_cats like '%" + weekCat + "%')" 
        		+ " order by start_time asc";
        return ConnectionAndStatementUtil.getListOfT(sql,this::build);

	}
	
	public Doctor_time_slotDTO get1stDTOByDoctor (long doctor) throws Exception
	{
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
		
        sql += " WHERE isDeleted = 0 and doctor_id =" +  doctor + " order by id desc limit 1";
        Doctor_time_slotDTO doctor_time_slotDTO = ConnectionAndStatementUtil.getT(sql,this::build);
        
        doctor_time_slotDTO = getFromEmployeeOffices(doctor_time_slotDTO);
		return doctor_time_slotDTO;
	}
	
	public List<Doctor_time_slotDTO> getByDoctor (long doctor) throws Exception
	{
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
		
        sql += " WHERE isDeleted = 0 and doctor_id =" +  doctor ;
        return ConnectionAndStatementUtil.getListOfT(sql,this::build);
        
	}
	
	public long getSlotSize(ResultSet rs)
	{
		try {
			return rs.getLong("duration_per_patient");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0L;
		}	
	}

	public long getSlotByDoctorShift (long doctor, long shift) throws Exception
	{
		String sql = "SELECT duration_per_patient ";

		sql += " FROM " + tableName;
		
        sql += " WHERE doctor_id =" +  doctor + " and shift_cat = " + shift;
        return ConnectionAndStatementUtil.getT(sql,this::getSlotSize, 0L);			
	}
	
	public long assignSpecialityToDoctor(long doctorId, int dept) throws Exception
	{
		UserDTO doctorUserDTO = UserRepository.getUserDTOByOrganogramID(doctorId);
		if(doctorUserDTO == null)
		{
			return -1;
		}
		long lastModificationTime = System.currentTimeMillis();
		String sql = "Update employee_offices set lastModificationTime = " + lastModificationTime + ", dept = " + dept 
				+ " where isDeleted = 0 and employee_record_id = " + doctorUserDTO.employee_record_id 
				+ " and status = 1 and organogram_role = " + SessionConstants.DOCTOR_ROLE;
		
		return (long) ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            Connection connection = model.getConnection();
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                ps.executeUpdate();
                recordUpdateTime(connection, lastModificationTime);
                return doctorId;
            } catch (SQLException ex) {
                logger.error(ex);
                return -1L;
            }
        }, sql);
	}
	
	public List<Doctor_time_slotDTO> getAllDoctor_time_slot (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Doctor_time_slotDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Doctor_time_slotDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += "  " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("dept_cat")
						|| str.equals("doctor_id")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("dept_cat"))
					{
						AllFieldSql += "" + tableName + ".dept_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("doctor_id"))
					{
						AllFieldSql += "" + tableName + ".doctor_id = " + p_searchCriteria.get(str);
						i ++;
					}
					
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " group by doctor_id order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		

		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	