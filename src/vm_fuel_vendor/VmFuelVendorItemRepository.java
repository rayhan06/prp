package vm_fuel_vendor;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import vm_fuel_request.VmFuelRequestItemDAO;
import vm_fuel_request.VmFuelRequestItemDTO;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class VmFuelVendorItemRepository implements Repository {
	VmFuelVendorItemDAO vmFuelVendorItemDAO = null;
	Gson gson;

	public void setDAO(VmFuelVendorItemDAO vmFuelVendorItemDAO)
	{
		this.vmFuelVendorItemDAO = vmFuelVendorItemDAO;
		gson = new Gson();

	}


	static Logger logger = Logger.getLogger(VmFuelVendorItemRepository.class);
	Map<Long, VmFuelVendorItemDTO>mapOfVmFuelVendorItemDTOToiD;
	Map<Long, Set<VmFuelVendorItemDTO> >mapOfVmFuelVendorItemDTOToVmFuelVendorId;



	static VmFuelVendorItemRepository instance = null;
	private VmFuelVendorItemRepository(){
		mapOfVmFuelVendorItemDTOToiD = new ConcurrentHashMap<>();
		mapOfVmFuelVendorItemDTOToVmFuelVendorId = new ConcurrentHashMap<>();


		setDAO(new VmFuelVendorItemDAO());

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static VmFuelVendorItemRepository getInstance(){
		if (instance == null){
			instance = new VmFuelVendorItemRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(vmFuelVendorItemDAO == null)
		{
			return;
		}

		try {
			List<VmFuelVendorItemDTO> Vm_fuel_vendor_itemDTOs = vmFuelVendorItemDAO.getAllVmFuelVendorItem(reloadAll);
			for(VmFuelVendorItemDTO Vm_fuel_vendor_itemDTO : Vm_fuel_vendor_itemDTOs) {
				VmFuelVendorItemDTO oldVmFuelVendorItemDTO = getVmFuelVendorItemDTOByIDWithoutClone(Vm_fuel_vendor_itemDTO.iD);
				if( oldVmFuelVendorItemDTO != null ) {
					mapOfVmFuelVendorItemDTOToiD.remove(oldVmFuelVendorItemDTO.iD);

					if(mapOfVmFuelVendorItemDTOToVmFuelVendorId.containsKey(oldVmFuelVendorItemDTO.vmFuelVendorId)) {
						mapOfVmFuelVendorItemDTOToVmFuelVendorId.get(oldVmFuelVendorItemDTO.vmFuelVendorId).remove(oldVmFuelVendorItemDTO);
					}
					if(mapOfVmFuelVendorItemDTOToVmFuelVendorId.get(oldVmFuelVendorItemDTO.vmFuelVendorId).isEmpty()) {
						mapOfVmFuelVendorItemDTOToVmFuelVendorId.remove(oldVmFuelVendorItemDTO.vmFuelVendorId);
					}

					
				}
				if(Vm_fuel_vendor_itemDTO.isDeleted == 0)
				{

					mapOfVmFuelVendorItemDTOToiD.put(Vm_fuel_vendor_itemDTO.iD, Vm_fuel_vendor_itemDTO);

					if( ! mapOfVmFuelVendorItemDTOToVmFuelVendorId.containsKey(Vm_fuel_vendor_itemDTO.vmFuelVendorId)) {
						mapOfVmFuelVendorItemDTOToVmFuelVendorId.put(Vm_fuel_vendor_itemDTO.vmFuelVendorId, new HashSet<>());
					}
					mapOfVmFuelVendorItemDTOToVmFuelVendorId.get(Vm_fuel_vendor_itemDTO.vmFuelVendorId).add(Vm_fuel_vendor_itemDTO);

				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public VmFuelVendorItemDTO clone(VmFuelVendorItemDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, VmFuelVendorItemDTO.class);
	}

	public List<VmFuelVendorItemDTO> clone(List<VmFuelVendorItemDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public VmFuelVendorItemDTO getVmFuelVendorItemDTOByIDWithoutClone( long ID){
		return mapOfVmFuelVendorItemDTOToiD.get(ID);
	}
	
	public List<VmFuelVendorItemDTO> getVmFuelVendorItemList() {
		List <VmFuelVendorItemDTO> Vm_fuel_vendor_items = new ArrayList<VmFuelVendorItemDTO>(this.mapOfVmFuelVendorItemDTOToiD.values());
		return Vm_fuel_vendor_items;
	}
	
	
	public VmFuelVendorItemDTO getVmFuelVendorItemDTOByID( long ID){
		return clone(mapOfVmFuelVendorItemDTOToiD.get(ID));
	}


	public List<VmFuelVendorItemDTO> getVmFuelVendorItemDTOByVmFuelVendorId(long vmFuelVendorId) {
		return new ArrayList<>( mapOfVmFuelVendorItemDTOToVmFuelVendorId.getOrDefault(vmFuelVendorId,new HashSet<>()))
				.stream()
				.map(vmFuelVendorItemDTO -> clone(vmFuelVendorItemDTO))
				.sorted(Comparator.comparingInt(dto -> dto.vehicleFuelCat))
				.collect(Collectors.toList());
	}


//	public void deleteVmFuelRequestItemDTOByvmFuelRequestId(long vmFuelRequestId) {
//		if(mapOfVmFuelRequestItemDTOTovmFuelRequestId.containsKey(vmFuelRequestId)) {
//			mapOfVmFuelRequestItemDTOTovmFuelRequestId.remove(vmFuelRequestId);
//		}
//	}


	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_fuel_vendor_item";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


