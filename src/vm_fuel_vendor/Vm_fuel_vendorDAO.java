package vm_fuel_vendor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;
import vm_fuel_request.VmFuelRequestItemDTO;
import vm_fuel_request.VmFuelRequestItemRepository;
import vm_fuel_request.Vm_fuel_requestDTO;
import vm_vehicle.Vm_vehicleDTO;

public class Vm_fuel_vendorDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Vm_fuel_vendorDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Vm_fuel_vendorMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"name_bn",
			"name_en",
			"mobile",
			"address",
			"contract_start_date",
			"contract_end_date",
			"status",
			"files_dropzone",
			"search_column",
			"inserted_by",
			"modified_by",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Vm_fuel_vendorDAO()
	{
		this("vm_fuel_vendor");		
	}
	
	public void setSearchColumn(Vm_fuel_vendorDTO vm_fuel_vendorDTO)
	{
		vm_fuel_vendorDTO.searchColumn = "";
		vm_fuel_vendorDTO.searchColumn += vm_fuel_vendorDTO.nameBn + " ";
		vm_fuel_vendorDTO.searchColumn += vm_fuel_vendorDTO.nameEn + " ";
		vm_fuel_vendorDTO.searchColumn += vm_fuel_vendorDTO.mobile + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Vm_fuel_vendorDTO vm_fuel_vendorDTO = (Vm_fuel_vendorDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(vm_fuel_vendorDTO);
		if(isInsert)
		{
			ps.setObject(index++,vm_fuel_vendorDTO.iD);
		}
		ps.setObject(index++,vm_fuel_vendorDTO.nameBn);
		ps.setObject(index++,vm_fuel_vendorDTO.nameEn);
		ps.setObject(index++,vm_fuel_vendorDTO.mobile);
		ps.setObject(index++,vm_fuel_vendorDTO.address);
		ps.setObject(index++,vm_fuel_vendorDTO.contractStartDate);
		ps.setObject(index++,vm_fuel_vendorDTO.contractEndDate);
		ps.setObject(index++,vm_fuel_vendorDTO.status);
		ps.setObject(index++,vm_fuel_vendorDTO.filesDropzone);
		ps.setObject(index++,vm_fuel_vendorDTO.searchColumn);
		ps.setObject(index++,vm_fuel_vendorDTO.insertedBy);
		ps.setObject(index++,vm_fuel_vendorDTO.modifiedBy);
		ps.setObject(index++,vm_fuel_vendorDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Vm_fuel_vendorDTO build(ResultSet rs)
	{
		try
		{
			Vm_fuel_vendorDTO vm_fuel_vendorDTO = new Vm_fuel_vendorDTO();
			vm_fuel_vendorDTO.iD = rs.getLong("ID");
			vm_fuel_vendorDTO.nameBn = rs.getString("name_bn");
			vm_fuel_vendorDTO.nameEn = rs.getString("name_en");
			vm_fuel_vendorDTO.mobile = rs.getString("mobile");
			vm_fuel_vendorDTO.address = rs.getString("address");
			vm_fuel_vendorDTO.contractStartDate = rs.getLong("contract_start_date");
			vm_fuel_vendorDTO.contractEndDate = rs.getLong("contract_end_date");
			vm_fuel_vendorDTO.status = rs.getInt("status");
			vm_fuel_vendorDTO.filesDropzone = rs.getLong("files_dropzone");
			vm_fuel_vendorDTO.searchColumn = rs.getString("search_column");
			vm_fuel_vendorDTO.insertedBy = rs.getString("inserted_by");
			vm_fuel_vendorDTO.modifiedBy = rs.getString("modified_by");
			vm_fuel_vendorDTO.insertionDate = rs.getLong("insertion_date");
			vm_fuel_vendorDTO.isDeleted = rs.getInt("isDeleted");
			vm_fuel_vendorDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return vm_fuel_vendorDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			ex.printStackTrace();
			return null;
		}
	}

	public Vm_fuel_vendorDTO buildWithChildren(ResultSet rs)
	{
		try
		{
			Vm_fuel_vendorDTO vm_fuel_vendorDTO = new Vm_fuel_vendorDTO();
			vm_fuel_vendorDTO.iD = rs.getLong("ID");
			vm_fuel_vendorDTO.nameBn = rs.getString("name_bn");
			vm_fuel_vendorDTO.nameEn = rs.getString("name_en");
			vm_fuel_vendorDTO.mobile = rs.getString("mobile");
			vm_fuel_vendorDTO.address = rs.getString("address");
			vm_fuel_vendorDTO.contractStartDate = rs.getLong("contract_start_date");
			vm_fuel_vendorDTO.contractEndDate = rs.getLong("contract_end_date");
			vm_fuel_vendorDTO.status = rs.getInt("status");
			vm_fuel_vendorDTO.filesDropzone = rs.getLong("files_dropzone");
			vm_fuel_vendorDTO.searchColumn = rs.getString("search_column");
			vm_fuel_vendorDTO.insertedBy = rs.getString("inserted_by");
			vm_fuel_vendorDTO.modifiedBy = rs.getString("modified_by");
			vm_fuel_vendorDTO.insertionDate = rs.getLong("insertion_date");
			vm_fuel_vendorDTO.isDeleted = rs.getInt("isDeleted");
			vm_fuel_vendorDTO.lastModificationTime = rs.getLong("lastModificationTime");

			List<VmFuelVendorItemDTO> vmFuelRequestItemDTOS = VmFuelVendorItemRepository.getInstance().getVmFuelVendorItemDTOByVmFuelVendorId(vm_fuel_vendorDTO.iD);
			vm_fuel_vendorDTO.vmFuelVendorItemDTOList = vmFuelRequestItemDTOS;
			return vm_fuel_vendorDTO;
		}
		catch (Exception ex)
		{
			logger.error(ex);
			ex.printStackTrace();
			return null;
		}
	}
	
	

//	public Vm_fuel_vendorDTO getDTOByID (long id)
//	{
//		//String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
//		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? " ;
//		//Vm_fuel_vendorDTO vm_fuel_vendorDTO = ConnectionAndStatementUtil.getT(sql,this::build);
//		Vm_fuel_vendorDTO vm_fuel_vendorDTO = ConnectionAndStatementUtil.getT(sql, Arrays.asList(id), this::build);;
//		try {
//			VmFuelVendorItemDAO vmFuelVendorItemDAO = new VmFuelVendorItemDAO("vm_fuel_vendor_item");
//			List<VmFuelVendorItemDTO> vmFuelVendorItemDTOList = vmFuelVendorItemDAO.getVmFuelVendorItemDTOListByVmFuelVendorID(vm_fuel_vendorDTO.iD);
//			vm_fuel_vendorDTO.vmFuelVendorItemDTOList = vmFuelVendorItemDTOList;
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return vm_fuel_vendorDTO;
//	}

	public Vm_fuel_vendorDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? " ;
		Vm_fuel_vendorDTO vm_fuel_vendorDTO = ConnectionAndStatementUtil.getT(sql, Arrays.asList(id), this::buildWithChildren);

		return vm_fuel_vendorDTO;
	}

	


	
	public List<Vm_fuel_vendorDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	public List<Vm_fuel_vendorDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
										   String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::buildWithChildren);
	}

	public String getOptions(String Language, UserDTO userDTO, long vendorId)
	{
		List<Vm_fuel_vendorDTO> all = getDTOs(null, -1, -1, true, userDTO, " status = 1 ", false);

		String tempString = "";

		for (Vm_fuel_vendorDTO vm_fuel_vendorDTO: all) {
			String name = Language.equals("English") ? vm_fuel_vendorDTO.nameEn : vm_fuel_vendorDTO.nameBn;
			String selected = vm_fuel_vendorDTO.iD==vendorId ? "selected " : "";
			tempString += "<option " + selected + "value='" + vm_fuel_vendorDTO.iD + "'>" + Utils.getDigits(name, Language) + "</option>";
		}
		return tempString;
	}

	

	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				//AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
				AnyfieldSql+= tableName + ".search_column like ? ";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name_bn")
						|| str.equals("name_en")
						|| str.equals("mobile")
						|| str.equals("contract_start_date_start")
						|| str.equals("contract_start_date_end")
						|| str.equals("contract_end_date_start")
						|| str.equals("contract_end_date_end")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name_bn"))
					{
						//AllFieldSql += "" + tableName + ".name_bn like '%" + p_searchCriteria.get(str) + "%'";
						AllFieldSql += "" + tableName + ".name_bn like ? ";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("name_en"))
					{
						//AllFieldSql += "" + tableName + ".name_en like '%" + p_searchCriteria.get(str) + "%'";
						AllFieldSql += "" + tableName + ".name_en like ? ";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("mobile"))
					{
						//AllFieldSql += "" + tableName + ".mobile like '%" + p_searchCriteria.get(str) + "%'";
						AllFieldSql += "" + tableName + ".mobile like ? ";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("contract_start_date_start"))
					{
						//AllFieldSql += "" + tableName + ".contract_start_date >= " + p_searchCriteria.get(str);
						AllFieldSql += "" + tableName + ".contract_start_date >= ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("contract_start_date_end"))
					{
						//AllFieldSql += "" + tableName + ".contract_start_date <= " + p_searchCriteria.get(str);
						AllFieldSql += "" + tableName + ".contract_start_date <= ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("contract_end_date_start"))
					{
						//AllFieldSql += "" + tableName + ".contract_end_date >= " + p_searchCriteria.get(str);
						AllFieldSql += "" + tableName + ".contract_end_date >= ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("contract_end_date_end"))
					{
						//AllFieldSql += "" + tableName + ".contract_end_date <= " + p_searchCriteria.get(str);
						AllFieldSql += "" + tableName + ".contract_end_date <= ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						//AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						AllFieldSql += "" + tableName + ".insertion_date >= ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						//AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						AllFieldSql += "" + tableName + ".insertion_date <= ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	

				
}
	