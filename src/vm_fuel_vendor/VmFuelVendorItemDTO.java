package vm_fuel_vendor;
import java.util.*; 
import util.*; 


public class VmFuelVendorItemDTO extends CommonDTO
{

	public long vmFuelVendorId = -1;
	public int vehicleFuelCat = -1;
	public double unit = -1;
	public double price = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
	public long insertionDate = -1;
	
	public List<VmFuelVendorItemDTO> vmFuelVendorItemDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$VmFuelVendorItemDTO[" +
            " iD = " + iD +
            " vmFuelVendorId = " + vmFuelVendorId +
            " vehicleFuelCat = " + vehicleFuelCat +
            " unit = " + unit +
            " price = " + price +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}