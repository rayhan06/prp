package vm_fuel_vendor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import fiscal_year.Fiscal_yearDTO;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;
import vm_fuel_request.VmFuelRequestItemRepository;
import vm_fuel_request.Vm_fuel_requestDTO;


public class Vm_fuel_vendorRepository implements Repository {
	Vm_fuel_vendorDAO vm_fuel_vendorDAO = null;
	Gson gson;
	
	public void setDAO(Vm_fuel_vendorDAO vm_fuel_vendorDAO)
	{
		this.vm_fuel_vendorDAO = vm_fuel_vendorDAO;
		gson = new Gson();
	}
	
	
	static Logger logger = Logger.getLogger(Vm_fuel_vendorRepository.class);
	Map<Long, Vm_fuel_vendorDTO>mapOfVm_fuel_vendorDTOToiD;

	static Vm_fuel_vendorRepository instance = null;  
	private Vm_fuel_vendorRepository(){
		mapOfVm_fuel_vendorDTOToiD = new ConcurrentHashMap<>();

		setDAO(new Vm_fuel_vendorDAO());

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vm_fuel_vendorRepository getInstance(){
		if (instance == null){
			instance = new Vm_fuel_vendorRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(vm_fuel_vendorDAO == null)
		{
			return;
		}
		try {
			List<Vm_fuel_vendorDTO> vm_fuel_vendorDTOs = (List<Vm_fuel_vendorDTO>) vm_fuel_vendorDAO.getAll(reloadAll);
			for(Vm_fuel_vendorDTO vm_fuel_vendorDTO : vm_fuel_vendorDTOs) {
				Vm_fuel_vendorDTO oldVm_fuel_vendorDTO = getVm_fuel_vendorDTOByIDWithoutClone(vm_fuel_vendorDTO.iD);
				if( oldVm_fuel_vendorDTO != null ) {
					mapOfVm_fuel_vendorDTOToiD.remove(oldVm_fuel_vendorDTO.iD);
					
					
				}
				if(vm_fuel_vendorDTO.isDeleted == 0) 
				{
					
					mapOfVm_fuel_vendorDTOToiD.put(vm_fuel_vendorDTO.iD, vm_fuel_vendorDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public Vm_fuel_vendorDTO clone(Vm_fuel_vendorDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Vm_fuel_vendorDTO.class);
	}

	public List<Vm_fuel_vendorDTO> clone(List<Vm_fuel_vendorDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	public List<Vm_fuel_vendorDTO> getVm_fuel_vendorList() {
		List <Vm_fuel_vendorDTO> vm_fuel_vendors = new ArrayList<Vm_fuel_vendorDTO>(this.mapOfVm_fuel_vendorDTOToiD.values());
		return vm_fuel_vendors;
	}

	public Vm_fuel_vendorDTO getVm_fuel_vendorDTOByIDWithoutClone( long ID){
		return mapOfVm_fuel_vendorDTOToiD.get(ID);
	}
	
	
	public Vm_fuel_vendorDTO getVm_fuel_vendorDTOByID( long ID){
		//return mapOfVm_fuel_vendorDTOToiD.get(ID);
		return dtoWithChildren(clone(mapOfVm_fuel_vendorDTOToiD.get(ID)));
	}

	public Vm_fuel_vendorDTO dtoWithChildren(Vm_fuel_vendorDTO vm_fuel_vendorDTO) {
		if (vm_fuel_vendorDTO != null) {
			vm_fuel_vendorDTO.vmFuelVendorItemDTOList = VmFuelVendorItemRepository
					.getInstance()
					.getVmFuelVendorItemDTOByVmFuelVendorId(
							vm_fuel_vendorDTO.iD
					);
		}
		return vm_fuel_vendorDTO;
	}
	
	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_fuel_vendor";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}

	public String getText(long id,String language){
		Vm_fuel_vendorDTO dto = getVm_fuel_vendorDTOByID(id);
		System.out.println("dto fiscal: "+dto);
		return dto==null?"": ("Bangla".equalsIgnoreCase(language)?dto.nameBn: dto.nameEn);
	}
}


