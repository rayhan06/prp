package vm_fuel_vendor;
import java.util.*; 
import util.*; 


public class Vm_fuel_vendorDTO extends CommonDTO
{

    public String nameBn = "";
    public String nameEn = "";
    public String mobile = "";
    public String address = "";
	public long contractStartDate = System.currentTimeMillis();
	public long contractEndDate = System.currentTimeMillis();
	public int status = -1;
	public long filesDropzone = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
	public long insertionDate = -1;
	
	public List<VmFuelVendorItemDTO> vmFuelVendorItemDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Vm_fuel_vendorDTO[" +
            " iD = " + iD +
            " nameBn = " + nameBn +
            " nameEn = " + nameEn +
            " mobile = " + mobile +
            " address = " + address +
            " contractStartDate = " + contractStartDate +
            " contractEndDate = " + contractEndDate +
            " status = " + status +
            " filesDropzone = " + filesDropzone +
            " searchColumn = " + searchColumn +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}