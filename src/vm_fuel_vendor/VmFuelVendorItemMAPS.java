package vm_fuel_vendor;
import java.util.*; 
import util.*;


public class VmFuelVendorItemMAPS extends CommonMaps
{	
	public VmFuelVendorItemMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("vmFuelVendorId".toLowerCase(), "vmFuelVendorId".toLowerCase());
		java_DTO_map.put("vehicleFuelCat".toLowerCase(), "vehicleFuelCat".toLowerCase());
		java_DTO_map.put("unit".toLowerCase(), "unit".toLowerCase());
		java_DTO_map.put("price".toLowerCase(), "price".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("vehicle_fuel_cat".toLowerCase(), "vehicleFuelCat".toLowerCase());
		java_SQL_map.put("unit".toLowerCase(), "unit".toLowerCase());
		java_SQL_map.put("price".toLowerCase(), "price".toLowerCase());
		java_SQL_map.put("inserted_by".toLowerCase(), "insertedBy".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Vm Fuel Vendor Id".toLowerCase(), "vmFuelVendorId".toLowerCase());
		java_Text_map.put("Vehicle Fuel".toLowerCase(), "vehicleFuelCat".toLowerCase());
		java_Text_map.put("Unit".toLowerCase(), "unit".toLowerCase());
		java_Text_map.put("Price".toLowerCase(), "price".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}