package vm_fuel_vendor;
import java.util.*; 
import util.*;


public class Vm_fuel_vendorMAPS extends CommonMaps
{	
	public Vm_fuel_vendorMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("nameBn".toLowerCase(), "nameBn".toLowerCase());
		java_DTO_map.put("nameEn".toLowerCase(), "nameEn".toLowerCase());
		java_DTO_map.put("mobile".toLowerCase(), "mobile".toLowerCase());
		java_DTO_map.put("address".toLowerCase(), "address".toLowerCase());
		java_DTO_map.put("contractStartDate".toLowerCase(), "contractStartDate".toLowerCase());
		java_DTO_map.put("contractEndDate".toLowerCase(), "contractEndDate".toLowerCase());
		java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
		java_DTO_map.put("filesDropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("name_bn".toLowerCase(), "nameBn".toLowerCase());
		java_SQL_map.put("name_en".toLowerCase(), "nameEn".toLowerCase());
		java_SQL_map.put("mobile".toLowerCase(), "mobile".toLowerCase());
		java_SQL_map.put("address".toLowerCase(), "address".toLowerCase());
		java_SQL_map.put("contract_start_date".toLowerCase(), "contractStartDate".toLowerCase());
		java_SQL_map.put("contract_end_date".toLowerCase(), "contractEndDate".toLowerCase());
		java_SQL_map.put("status".toLowerCase(), "status".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Bangla Name".toLowerCase(), "nameBn".toLowerCase());
		java_Text_map.put("English Name".toLowerCase(), "nameEn".toLowerCase());
		java_Text_map.put("Mobile".toLowerCase(), "mobile".toLowerCase());
		java_Text_map.put("Address".toLowerCase(), "address".toLowerCase());
		java_Text_map.put("Contract Start Date".toLowerCase(), "contractStartDate".toLowerCase());
		java_Text_map.put("Contract End Date".toLowerCase(), "contractEndDate".toLowerCase());
		java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
		java_Text_map.put("Files Dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}