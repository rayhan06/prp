package vm_fuel_vendor;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import common.ApiResponse;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import files.*;



/**
 * Servlet implementation class Vm_fuel_vendorServlet
 */
@WebServlet("/Vm_fuel_vendorServlet")
@MultipartConfig
public class Vm_fuel_vendorServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Vm_fuel_vendorServlet.class);

    String tableName = "vm_fuel_vendor";

	Vm_fuel_vendorDAO vm_fuel_vendorDAO;
	CommonRequestHandler commonRequestHandler;
	FilesDAO filesDAO = new FilesDAO();
	VmFuelVendorItemDAO vmFuelVendorItemDAO;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vm_fuel_vendorServlet()
	{
        super();
    	try
    	{
			vm_fuel_vendorDAO = new Vm_fuel_vendorDAO(tableName);
			vmFuelVendorItemDAO = new VmFuelVendorItemDAO("vm_fuel_vendor_item");
			commonRequestHandler = new CommonRequestHandler(vm_fuel_vendorDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_FUEL_VENDOR_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_FUEL_VENDOR_UPDATE))
				{
					getVm_fuel_vendor(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("downloadDropzoneFile"))
			{
				commonRequestHandler.downloadDropzoneFile(request, response, filesDAO);
			}
			else if(actionType.equals("DeleteFileFromDropZone"))
			{
				commonRequestHandler.deleteFileFromDropZone(request, response, filesDAO);
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_FUEL_VENDOR_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchVm_fuel_vendor(request, response, isPermanentTable, filter);
						}
						else
						{
							searchVm_fuel_vendor(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchVm_fuel_vendor(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_FUEL_VENDOR_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_FUEL_VENDOR_ADD))
				{
					System.out.println("going to  addVm_fuel_vendor ");
					addVm_fuel_vendor(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addVm_fuel_vendor ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("UploadFilesFromDropZone"))
			{
				commonRequestHandler.UploadFilesFromDropZone(request, response, userDTO);
			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_FUEL_VENDOR_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addVm_fuel_vendor ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_FUEL_VENDOR_UPDATE))
				{
					addVm_fuel_vendor(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_FUEL_VENDOR_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_FUEL_VENDOR_SEARCH))
				{
					searchVm_fuel_vendor(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			//Vm_fuel_vendorDTO vm_fuel_vendorDTO = (Vm_fuel_vendorDTO)vm_fuel_vendorDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			Vm_fuel_vendorDTO vm_fuel_vendorDTO = Vm_fuel_vendorRepository.getInstance().getVm_fuel_vendorDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(vm_fuel_vendorDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addVm_fuel_vendor(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addVm_fuel_vendor");
			String path = getServletContext().getRealPath("/img2/");
			Vm_fuel_vendorDTO vm_fuel_vendorDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				vm_fuel_vendorDTO = new Vm_fuel_vendorDTO();
			}
			else
			{
				//vm_fuel_vendorDTO = (Vm_fuel_vendorDTO)vm_fuel_vendorDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
				vm_fuel_vendorDTO = Vm_fuel_vendorRepository.getInstance().getVm_fuel_vendorDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("nameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameBn = " + Value);
			if(Value != null)
			{
				vm_fuel_vendorDTO.nameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEn = " + Value);
			if(Value != null)
			{
				vm_fuel_vendorDTO.nameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("mobile");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("mobile = " + Value);
			if(Value != null)
			{
				vm_fuel_vendorDTO.mobile = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("address");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("address = " + Value);
			if(Value != null)
			{
				vm_fuel_vendorDTO.address = GeoLocationDAO2.getAddressToSave(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("contractStartDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("contractStartDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					vm_fuel_vendorDTO.contractStartDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("contractEndDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("contractEndDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					vm_fuel_vendorDTO.contractEndDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("status");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("status = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_fuel_vendorDTO.status = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("filesDropzone");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("filesDropzone = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				System.out.println("filesDropzone = " + Value);

				vm_fuel_vendorDTO.filesDropzone = Long.parseLong(Value);


				if(addFlag == false)
				{
					String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
					String[] deleteArray = filesDropzoneFilesToDelete.split(",");
					for(int i = 0; i < deleteArray.length; i ++)
					{
						System.out.println("going to delete " + deleteArray[i]);
						if(i>0)
						{
							filesDAO.delete(Long.parseLong(deleteArray[i]));
						}
					}
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				vm_fuel_vendorDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null)
			{
				vm_fuel_vendorDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null)
			{
				vm_fuel_vendorDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				vm_fuel_vendorDTO.insertionDate = c.getTimeInMillis();
			}


			System.out.println("Done adding  addVm_fuel_vendor dto = " + vm_fuel_vendorDTO);

			long[] returnedID = {-1};
			Utils.handleTransactionForNavigationService4(()->{
				long returnedIDInner = -1;

				if(isPermanentTable == false) //add new row for validation and make the old row outdated
				{
					vm_fuel_vendorDAO.setIsDeleted(vm_fuel_vendorDTO.iD, CommonDTO.OUTDATED);
					returnedIDInner = vm_fuel_vendorDAO.add(vm_fuel_vendorDTO);
					vm_fuel_vendorDAO.setIsDeleted(returnedIDInner, CommonDTO.WAITING_FOR_APPROVAL);
				}
				else if(addFlag == true)
				{
					returnedIDInner = vm_fuel_vendorDAO.manageWriteOperations(vm_fuel_vendorDTO, SessionConstants.INSERT, -1, userDTO);
				}
				else
				{
					returnedIDInner = vm_fuel_vendorDAO.manageWriteOperations(vm_fuel_vendorDTO, SessionConstants.UPDATE, -1, userDTO);
				}





				List<VmFuelVendorItemDTO> vmFuelVendorItemDTOList = createVmFuelVendorItemDTOListByRequest(request);
				if(addFlag == true || isPermanentTable == false) //add or validate
				{
					if(vmFuelVendorItemDTOList != null)
					{
						for(VmFuelVendorItemDTO vmFuelVendorItemDTO: vmFuelVendorItemDTOList)
						{
							vmFuelVendorItemDTO.vmFuelVendorId = vm_fuel_vendorDTO.iD;
							vmFuelVendorItemDAO.add(vmFuelVendorItemDTO);
						}
					}

				}
				else
				{
					List<Long> childIdsFromRequest = vmFuelVendorItemDAO.getChildIdsFromRequest(request, "vmFuelVendorItem");
					System.out.println("childIdsFromRequest size: "+childIdsFromRequest.size());
					//delete the removed children
					vmFuelVendorItemDAO.deleteChildrenNotInList("vm_fuel_vendor", "vm_fuel_vendor_item", vm_fuel_vendorDTO.iD, childIdsFromRequest);
					List<Long> childIDsInDatabase = vmFuelVendorItemDAO.getChilIds("vm_fuel_vendor", "vm_fuel_vendor_item", vm_fuel_vendorDTO.iD);


					if(childIdsFromRequest != null)
					{
						//System.out.println("childIdsFromRequest.size() size: "+childIdsFromRequest.size());
						for(int i = 0; i < childIdsFromRequest.size(); i ++)
						{
							Long childIDFromRequest = childIdsFromRequest.get(i);
							if(childIDsInDatabase.contains(childIDFromRequest))
							{
								VmFuelVendorItemDTO vmFuelVendorItemDTO =  createVmFuelVendorItemDTOByRequestAndIndex(request, false, i);
								vmFuelVendorItemDTO.vmFuelVendorId = vm_fuel_vendorDTO.iD;
								vmFuelVendorItemDAO.update(vmFuelVendorItemDTO);
							}
							else
							{
								VmFuelVendorItemDTO vmFuelVendorItemDTO =  createVmFuelVendorItemDTOByRequestAndIndex(request, true, i);
								vmFuelVendorItemDTO.vmFuelVendorId = vm_fuel_vendorDTO.iD;
								vmFuelVendorItemDAO.add(vmFuelVendorItemDTO);
							}
						}
					}
					else
					{
						vmFuelVendorItemDAO.deleteChildrenByParent(vm_fuel_vendorDTO.iD, "vm_fuel_vendor_id");
					}

				}

				returnedID[0] = returnedIDInner;
			});





			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getVm_fuel_vendor(request, response, returnedID[0]);
				}
				else
				{
					//response.sendRedirect("Vm_fuel_vendorServlet?actionType=search");
					ApiResponse apiResponse=null;
					apiResponse = ApiResponse.makeSuccessResponse("Vm_fuel_vendorServlet?actionType=search");

					PrintWriter pw = response.getWriter();
					pw.write(apiResponse.getJSONString());
					pw.flush();
					pw.close();
				}
			}
			else
			{
				commonRequestHandler.validate(vm_fuel_vendorDAO.getDTOByID(returnedID[0]), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}





	private List<VmFuelVendorItemDTO> createVmFuelVendorItemDTOListByRequest(HttpServletRequest request) throws Exception{
		List<VmFuelVendorItemDTO> vmFuelVendorItemDTOList = new ArrayList<VmFuelVendorItemDTO>();
		if(request.getParameterValues("vmFuelVendorItem.iD") != null)
		{
			int vmFuelVendorItemItemNo = request.getParameterValues("vmFuelVendorItem.iD").length;

			System.out.println("vmFuelVendorItemItemNo size: "+vmFuelVendorItemItemNo);
			System.out.println("vmFuelVendorItemItemNo size: "+request.getParameterValues("vmFuelVendorItem.iD").length);
			System.out.println("vmFuelVendorItemItemNo size: "+request.getParameterValues("vmFuelVendorItem.iD").length);


			for(int index=0;index<vmFuelVendorItemItemNo;index++){
				VmFuelVendorItemDTO vmFuelVendorItemDTO = createVmFuelVendorItemDTOByRequestAndIndex(request,true,index);
				vmFuelVendorItemDTOList.add(vmFuelVendorItemDTO);
			}

			return vmFuelVendorItemDTOList;
		}
		return null;
	}


	private VmFuelVendorItemDTO createVmFuelVendorItemDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{

		VmFuelVendorItemDTO vmFuelVendorItemDTO;
		if(addFlag == true )
		{
			vmFuelVendorItemDTO = new VmFuelVendorItemDTO();
		}
		else
		{
			vmFuelVendorItemDTO = vmFuelVendorItemDAO.getDTOByID(Long.parseLong(request.getParameterValues("vmFuelVendorItem.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");



		String Value = "";
		Value = request.getParameterValues("vmFuelVendorItem.vmFuelVendorId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmFuelVendorItemDTO.vmFuelVendorId = Long.parseLong(Value);

		Value = request.getParameterValues("vmFuelVendorItem.vehicleFuelCat")[index];


		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmFuelVendorItemDTO.vehicleFuelCat = Integer.parseInt(Value);

		Value = request.getParameterValues("vmFuelVendorItem.unit")[index];

		System.out.println("vmFuelVendorItem.unit: "+Value);

		if(Value != null)
		{
			System.out.println("vmFuelVendorItem.unit not null : "+Value);
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		if(Value.isEmpty()){
			System.out.println("vmFuelVendorItem.unit empty : "+Value);
			Value = "-1";
		}

		vmFuelVendorItemDTO.unit = Double.parseDouble(Value);
		Value = request.getParameterValues("vmFuelVendorItem.price")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmFuelVendorItemDTO.price = Double.parseDouble(Value);
		Value = request.getParameterValues("vmFuelVendorItem.insertedBy")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmFuelVendorItemDTO.insertedBy = (Value);
		Value = request.getParameterValues("vmFuelVendorItem.modifiedBy")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmFuelVendorItemDTO.modifiedBy = (Value);
		Value = request.getParameterValues("vmFuelVendorItem.insertionDate")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmFuelVendorItemDTO.insertionDate = Long.parseLong(Value);
		return vmFuelVendorItemDTO;

	}




	private void getVm_fuel_vendor(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getVm_fuel_vendor");
		Vm_fuel_vendorDTO vm_fuel_vendorDTO = null;
		try
		{
			vm_fuel_vendorDTO = vm_fuel_vendorDAO.getDTOByID(id);
			request.setAttribute("ID", vm_fuel_vendorDTO.iD);
			request.setAttribute("vm_fuel_vendorDTO",vm_fuel_vendorDTO);
			request.setAttribute("vm_fuel_vendorDAO",vm_fuel_vendorDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "vm_fuel_vendor/vm_fuel_vendorInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "vm_fuel_vendor/vm_fuel_vendorSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "vm_fuel_vendor/vm_fuel_vendorEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "vm_fuel_vendor/vm_fuel_vendorEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getVm_fuel_vendor(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getVm_fuel_vendor(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchVm_fuel_vendor(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchVm_fuel_vendor 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_VM_FUEL_VENDOR,
			request,
			vm_fuel_vendorDAO,
			SessionConstants.VIEW_VM_FUEL_VENDOR,
			SessionConstants.SEARCH_VM_FUEL_VENDOR,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("vm_fuel_vendorDAO",vm_fuel_vendorDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_fuel_vendor/vm_fuel_vendorApproval.jsp");
	        	rd = request.getRequestDispatcher("vm_fuel_vendor/vm_fuel_vendorApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_fuel_vendor/vm_fuel_vendorApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("vm_fuel_vendor/vm_fuel_vendorApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_fuel_vendor/vm_fuel_vendorSearch.jsp");
	        	rd = request.getRequestDispatcher("vm_fuel_vendor/vm_fuel_vendorSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_fuel_vendor/vm_fuel_vendorSearchForm.jsp");
	        	rd = request.getRequestDispatcher("vm_fuel_vendor/vm_fuel_vendorSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

