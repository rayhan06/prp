package vm_fuel_vendor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class VmFuelVendorItemDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public VmFuelVendorItemDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new VmFuelVendorItemMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"vm_fuel_vendor_id",
			"vehicle_fuel_cat",
			"unit",
			"price",
			"inserted_by",
			"modified_by",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public VmFuelVendorItemDAO()
	{
		this("vm_fuel_vendor_item");		
	}
	
	public void setSearchColumn(VmFuelVendorItemDTO vmfuelvendoritemDTO)
	{
		vmfuelvendoritemDTO.searchColumn = "";
		vmfuelvendoritemDTO.searchColumn += CatDAO.getName("English", "vehicle_fuel", vmfuelvendoritemDTO.vehicleFuelCat) + " " + CatDAO.getName("Bangla", "vehicle_fuel", vmfuelvendoritemDTO.vehicleFuelCat) + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		VmFuelVendorItemDTO vmfuelvendoritemDTO = (VmFuelVendorItemDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(vmfuelvendoritemDTO);
		if(isInsert)
		{
			ps.setObject(index++,vmfuelvendoritemDTO.iD);
		}
		ps.setObject(index++,vmfuelvendoritemDTO.vmFuelVendorId);
		ps.setObject(index++,vmfuelvendoritemDTO.vehicleFuelCat);
		ps.setObject(index++,vmfuelvendoritemDTO.unit);
		ps.setObject(index++,vmfuelvendoritemDTO.price);
		ps.setObject(index++,vmfuelvendoritemDTO.insertedBy);
		ps.setObject(index++,vmfuelvendoritemDTO.modifiedBy);
		ps.setObject(index++,vmfuelvendoritemDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public VmFuelVendorItemDTO build(ResultSet rs)
	{
		try
		{
			VmFuelVendorItemDTO vmfuelvendoritemDTO = new VmFuelVendorItemDTO();
			vmfuelvendoritemDTO.iD = rs.getLong("ID");
			vmfuelvendoritemDTO.vmFuelVendorId = rs.getLong("vm_fuel_vendor_id");
			vmfuelvendoritemDTO.vehicleFuelCat = rs.getInt("vehicle_fuel_cat");
			vmfuelvendoritemDTO.unit = rs.getDouble("unit");
			vmfuelvendoritemDTO.price = rs.getDouble("price");
			vmfuelvendoritemDTO.insertedBy = rs.getString("inserted_by");
			vmfuelvendoritemDTO.modifiedBy = rs.getString("modified_by");
			vmfuelvendoritemDTO.insertionDate = rs.getLong("insertion_date");
			vmfuelvendoritemDTO.isDeleted = rs.getInt("isDeleted");
			vmfuelvendoritemDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return vmfuelvendoritemDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			ex.printStackTrace();
			return null;
		}
	}
	
	
	public List<VmFuelVendorItemDTO> getVmFuelVendorItemDTOListByVmFuelVendorID(long vmFuelVendorID) throws Exception
	{
		//String sql = "SELECT * FROM vm_fuel_vendor_item where isDeleted=0 and vm_fuel_vendor_id="+vmFuelVendorID+" order by vm_fuel_vendor_item.lastModificationTime";
		String sql = "SELECT * FROM vm_fuel_vendor_item where isDeleted=0 and vm_fuel_vendor_id = ? order by vm_fuel_vendor_item.lastModificationTime";
		return ConnectionAndStatementUtil.getListOfT(sql,Arrays.asList(vmFuelVendorID),this::build);
	}

	public VmFuelVendorItemDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		VmFuelVendorItemDTO vmfuelvendoritemDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return vmfuelvendoritemDTO;
	}

	
	public List<VmFuelVendorItemDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<VmFuelVendorItemDTO> getAllVmFuelVendorItem (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<VmFuelVendorItemDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<VmFuelVendorItemDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}

	public CategoryLanguageModel getDTOFromModel(List<CategoryLanguageModel> modelList,Long ID){
		CategoryLanguageModel model   = modelList.stream()
													.filter(dto -> dto.categoryValue==ID).findAny().orElse(null);

		return model;


	}

	public CategoryLanguageModel getDTOFromModelCatID(List<CategoryLanguageModel> modelList,Long categoryVal){
		CategoryLanguageModel model   = modelList.stream()
				.filter(dto -> dto.categoryValue==categoryVal).findAny().orElse(null);

		return model;


	}
				
}
	