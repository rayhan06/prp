package physiotherapy_plan;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Physiotherapy_planRepository implements Repository {
	Physiotherapy_planDAO physiotherapy_planDAO = null;
	
	public void setDAO(Physiotherapy_planDAO physiotherapy_planDAO)
	{
		this.physiotherapy_planDAO = physiotherapy_planDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Physiotherapy_planRepository.class);
	Map<Long, Physiotherapy_planDTO>mapOfPhysiotherapy_planDTOToiD;
	Map<Long, Set<Physiotherapy_planDTO> >mapOfPhysiotherapy_planDTOToappointmentId;
	Map<String, Set<Physiotherapy_planDTO> >mapOfPhysiotherapy_planDTOToname;
	Map<Long, Set<Physiotherapy_planDTO> >mapOfPhysiotherapy_planDTOTodateOfBirth;
	Map<Long, Set<Physiotherapy_planDTO> >mapOfPhysiotherapy_planDTOToage;
	Map<Double, Set<Physiotherapy_planDTO> >mapOfPhysiotherapy_planDTOToheight;
	Map<Double, Set<Physiotherapy_planDTO> >mapOfPhysiotherapy_planDTOToweight;
	Map<Double, Set<Physiotherapy_planDTO> >mapOfPhysiotherapy_planDTOTobloodPressureDiastole;
	Map<Double, Set<Physiotherapy_planDTO> >mapOfPhysiotherapy_planDTOTobloodPressureSystole;
	Map<Double, Set<Physiotherapy_planDTO> >mapOfPhysiotherapy_planDTOTopulse;
	Map<Double, Set<Physiotherapy_planDTO> >mapOfPhysiotherapy_planDTOTotemperature;
	Map<Double, Set<Physiotherapy_planDTO> >mapOfPhysiotherapy_planDTOTooxygenSaturation;
	Map<Double, Set<Physiotherapy_planDTO> >mapOfPhysiotherapy_planDTOTobmi;
	Map<String, Set<Physiotherapy_planDTO> >mapOfPhysiotherapy_planDTOToremarks;
	Map<Long, Set<Physiotherapy_planDTO> >mapOfPhysiotherapy_planDTOToinsertedByUserId;
	Map<Long, Set<Physiotherapy_planDTO> >mapOfPhysiotherapy_planDTOToinsertedByOrganogramId;
	Map<Long, Set<Physiotherapy_planDTO> >mapOfPhysiotherapy_planDTOToinsertionDate;
	Map<String, Set<Physiotherapy_planDTO> >mapOfPhysiotherapy_planDTOTolastModifierUser;
	Map<Long, Set<Physiotherapy_planDTO> >mapOfPhysiotherapy_planDTOTolastModificationTime;


	static Physiotherapy_planRepository instance = null;  
	private Physiotherapy_planRepository(){
		mapOfPhysiotherapy_planDTOToiD = new ConcurrentHashMap<>();
		mapOfPhysiotherapy_planDTOToappointmentId = new ConcurrentHashMap<>();
		mapOfPhysiotherapy_planDTOToname = new ConcurrentHashMap<>();
		mapOfPhysiotherapy_planDTOTodateOfBirth = new ConcurrentHashMap<>();
		mapOfPhysiotherapy_planDTOToage = new ConcurrentHashMap<>();
		mapOfPhysiotherapy_planDTOToheight = new ConcurrentHashMap<>();
		mapOfPhysiotherapy_planDTOToweight = new ConcurrentHashMap<>();
		mapOfPhysiotherapy_planDTOTobloodPressureDiastole = new ConcurrentHashMap<>();
		mapOfPhysiotherapy_planDTOTobloodPressureSystole = new ConcurrentHashMap<>();
		mapOfPhysiotherapy_planDTOTopulse = new ConcurrentHashMap<>();
		mapOfPhysiotherapy_planDTOTotemperature = new ConcurrentHashMap<>();
		mapOfPhysiotherapy_planDTOTooxygenSaturation = new ConcurrentHashMap<>();
		mapOfPhysiotherapy_planDTOTobmi = new ConcurrentHashMap<>();
		mapOfPhysiotherapy_planDTOToremarks = new ConcurrentHashMap<>();
		mapOfPhysiotherapy_planDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfPhysiotherapy_planDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfPhysiotherapy_planDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfPhysiotherapy_planDTOTolastModifierUser = new ConcurrentHashMap<>();
		mapOfPhysiotherapy_planDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Physiotherapy_planRepository getInstance(){
		if (instance == null){
			instance = new Physiotherapy_planRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(physiotherapy_planDAO == null)
		{
			return;
		}
		try {
			List<Physiotherapy_planDTO> physiotherapy_planDTOs = physiotherapy_planDAO.getAllPhysiotherapy_plan(reloadAll);
			for(Physiotherapy_planDTO physiotherapy_planDTO : physiotherapy_planDTOs) {
				Physiotherapy_planDTO oldPhysiotherapy_planDTO = getPhysiotherapy_planDTOByID(physiotherapy_planDTO.iD);
				if( oldPhysiotherapy_planDTO != null ) {
					mapOfPhysiotherapy_planDTOToiD.remove(oldPhysiotherapy_planDTO.iD);
				
					if(mapOfPhysiotherapy_planDTOToappointmentId.containsKey(oldPhysiotherapy_planDTO.appointmentId)) {
						mapOfPhysiotherapy_planDTOToappointmentId.get(oldPhysiotherapy_planDTO.appointmentId).remove(oldPhysiotherapy_planDTO);
					}
					if(mapOfPhysiotherapy_planDTOToappointmentId.get(oldPhysiotherapy_planDTO.appointmentId).isEmpty()) {
						mapOfPhysiotherapy_planDTOToappointmentId.remove(oldPhysiotherapy_planDTO.appointmentId);
					}
					
					if(mapOfPhysiotherapy_planDTOToname.containsKey(oldPhysiotherapy_planDTO.name)) {
						mapOfPhysiotherapy_planDTOToname.get(oldPhysiotherapy_planDTO.name).remove(oldPhysiotherapy_planDTO);
					}
					if(mapOfPhysiotherapy_planDTOToname.get(oldPhysiotherapy_planDTO.name).isEmpty()) {
						mapOfPhysiotherapy_planDTOToname.remove(oldPhysiotherapy_planDTO.name);
					}
					
					if(mapOfPhysiotherapy_planDTOTodateOfBirth.containsKey(oldPhysiotherapy_planDTO.dateOfBirth)) {
						mapOfPhysiotherapy_planDTOTodateOfBirth.get(oldPhysiotherapy_planDTO.dateOfBirth).remove(oldPhysiotherapy_planDTO);
					}
					if(mapOfPhysiotherapy_planDTOTodateOfBirth.get(oldPhysiotherapy_planDTO.dateOfBirth).isEmpty()) {
						mapOfPhysiotherapy_planDTOTodateOfBirth.remove(oldPhysiotherapy_planDTO.dateOfBirth);
					}
					
					if(mapOfPhysiotherapy_planDTOToage.containsKey(oldPhysiotherapy_planDTO.age)) {
						mapOfPhysiotherapy_planDTOToage.get(oldPhysiotherapy_planDTO.age).remove(oldPhysiotherapy_planDTO);
					}
					if(mapOfPhysiotherapy_planDTOToage.get(oldPhysiotherapy_planDTO.age).isEmpty()) {
						mapOfPhysiotherapy_planDTOToage.remove(oldPhysiotherapy_planDTO.age);
					}
					
					if(mapOfPhysiotherapy_planDTOToheight.containsKey(oldPhysiotherapy_planDTO.height)) {
						mapOfPhysiotherapy_planDTOToheight.get(oldPhysiotherapy_planDTO.height).remove(oldPhysiotherapy_planDTO);
					}
					if(mapOfPhysiotherapy_planDTOToheight.get(oldPhysiotherapy_planDTO.height).isEmpty()) {
						mapOfPhysiotherapy_planDTOToheight.remove(oldPhysiotherapy_planDTO.height);
					}
					
					if(mapOfPhysiotherapy_planDTOToweight.containsKey(oldPhysiotherapy_planDTO.weight)) {
						mapOfPhysiotherapy_planDTOToweight.get(oldPhysiotherapy_planDTO.weight).remove(oldPhysiotherapy_planDTO);
					}
					if(mapOfPhysiotherapy_planDTOToweight.get(oldPhysiotherapy_planDTO.weight).isEmpty()) {
						mapOfPhysiotherapy_planDTOToweight.remove(oldPhysiotherapy_planDTO.weight);
					}
					
					if(mapOfPhysiotherapy_planDTOTobloodPressureDiastole.containsKey(oldPhysiotherapy_planDTO.bloodPressureDiastole)) {
						mapOfPhysiotherapy_planDTOTobloodPressureDiastole.get(oldPhysiotherapy_planDTO.bloodPressureDiastole).remove(oldPhysiotherapy_planDTO);
					}
					if(mapOfPhysiotherapy_planDTOTobloodPressureDiastole.get(oldPhysiotherapy_planDTO.bloodPressureDiastole).isEmpty()) {
						mapOfPhysiotherapy_planDTOTobloodPressureDiastole.remove(oldPhysiotherapy_planDTO.bloodPressureDiastole);
					}
					
					if(mapOfPhysiotherapy_planDTOTobloodPressureSystole.containsKey(oldPhysiotherapy_planDTO.bloodPressureSystole)) {
						mapOfPhysiotherapy_planDTOTobloodPressureSystole.get(oldPhysiotherapy_planDTO.bloodPressureSystole).remove(oldPhysiotherapy_planDTO);
					}
					if(mapOfPhysiotherapy_planDTOTobloodPressureSystole.get(oldPhysiotherapy_planDTO.bloodPressureSystole).isEmpty()) {
						mapOfPhysiotherapy_planDTOTobloodPressureSystole.remove(oldPhysiotherapy_planDTO.bloodPressureSystole);
					}
					
					if(mapOfPhysiotherapy_planDTOTopulse.containsKey(oldPhysiotherapy_planDTO.pulse)) {
						mapOfPhysiotherapy_planDTOTopulse.get(oldPhysiotherapy_planDTO.pulse).remove(oldPhysiotherapy_planDTO);
					}
					if(mapOfPhysiotherapy_planDTOTopulse.get(oldPhysiotherapy_planDTO.pulse).isEmpty()) {
						mapOfPhysiotherapy_planDTOTopulse.remove(oldPhysiotherapy_planDTO.pulse);
					}
					
					if(mapOfPhysiotherapy_planDTOTotemperature.containsKey(oldPhysiotherapy_planDTO.temperature)) {
						mapOfPhysiotherapy_planDTOTotemperature.get(oldPhysiotherapy_planDTO.temperature).remove(oldPhysiotherapy_planDTO);
					}
					if(mapOfPhysiotherapy_planDTOTotemperature.get(oldPhysiotherapy_planDTO.temperature).isEmpty()) {
						mapOfPhysiotherapy_planDTOTotemperature.remove(oldPhysiotherapy_planDTO.temperature);
					}
					
					if(mapOfPhysiotherapy_planDTOTooxygenSaturation.containsKey(oldPhysiotherapy_planDTO.oxygenSaturation)) {
						mapOfPhysiotherapy_planDTOTooxygenSaturation.get(oldPhysiotherapy_planDTO.oxygenSaturation).remove(oldPhysiotherapy_planDTO);
					}
					if(mapOfPhysiotherapy_planDTOTooxygenSaturation.get(oldPhysiotherapy_planDTO.oxygenSaturation).isEmpty()) {
						mapOfPhysiotherapy_planDTOTooxygenSaturation.remove(oldPhysiotherapy_planDTO.oxygenSaturation);
					}
					
					if(mapOfPhysiotherapy_planDTOTobmi.containsKey(oldPhysiotherapy_planDTO.bmi)) {
						mapOfPhysiotherapy_planDTOTobmi.get(oldPhysiotherapy_planDTO.bmi).remove(oldPhysiotherapy_planDTO);
					}
					if(mapOfPhysiotherapy_planDTOTobmi.get(oldPhysiotherapy_planDTO.bmi).isEmpty()) {
						mapOfPhysiotherapy_planDTOTobmi.remove(oldPhysiotherapy_planDTO.bmi);
					}
					
					if(mapOfPhysiotherapy_planDTOToremarks.containsKey(oldPhysiotherapy_planDTO.remarks)) {
						mapOfPhysiotherapy_planDTOToremarks.get(oldPhysiotherapy_planDTO.remarks).remove(oldPhysiotherapy_planDTO);
					}
					if(mapOfPhysiotherapy_planDTOToremarks.get(oldPhysiotherapy_planDTO.remarks).isEmpty()) {
						mapOfPhysiotherapy_planDTOToremarks.remove(oldPhysiotherapy_planDTO.remarks);
					}
					
					if(mapOfPhysiotherapy_planDTOToinsertedByUserId.containsKey(oldPhysiotherapy_planDTO.insertedByUserId)) {
						mapOfPhysiotherapy_planDTOToinsertedByUserId.get(oldPhysiotherapy_planDTO.insertedByUserId).remove(oldPhysiotherapy_planDTO);
					}
					if(mapOfPhysiotherapy_planDTOToinsertedByUserId.get(oldPhysiotherapy_planDTO.insertedByUserId).isEmpty()) {
						mapOfPhysiotherapy_planDTOToinsertedByUserId.remove(oldPhysiotherapy_planDTO.insertedByUserId);
					}
					
					if(mapOfPhysiotherapy_planDTOToinsertedByOrganogramId.containsKey(oldPhysiotherapy_planDTO.insertedByOrganogramId)) {
						mapOfPhysiotherapy_planDTOToinsertedByOrganogramId.get(oldPhysiotherapy_planDTO.insertedByOrganogramId).remove(oldPhysiotherapy_planDTO);
					}
					if(mapOfPhysiotherapy_planDTOToinsertedByOrganogramId.get(oldPhysiotherapy_planDTO.insertedByOrganogramId).isEmpty()) {
						mapOfPhysiotherapy_planDTOToinsertedByOrganogramId.remove(oldPhysiotherapy_planDTO.insertedByOrganogramId);
					}
					
					if(mapOfPhysiotherapy_planDTOToinsertionDate.containsKey(oldPhysiotherapy_planDTO.insertionDate)) {
						mapOfPhysiotherapy_planDTOToinsertionDate.get(oldPhysiotherapy_planDTO.insertionDate).remove(oldPhysiotherapy_planDTO);
					}
					if(mapOfPhysiotherapy_planDTOToinsertionDate.get(oldPhysiotherapy_planDTO.insertionDate).isEmpty()) {
						mapOfPhysiotherapy_planDTOToinsertionDate.remove(oldPhysiotherapy_planDTO.insertionDate);
					}
					
					if(mapOfPhysiotherapy_planDTOTolastModifierUser.containsKey(oldPhysiotherapy_planDTO.lastModifierUser)) {
						mapOfPhysiotherapy_planDTOTolastModifierUser.get(oldPhysiotherapy_planDTO.lastModifierUser).remove(oldPhysiotherapy_planDTO);
					}
					if(mapOfPhysiotherapy_planDTOTolastModifierUser.get(oldPhysiotherapy_planDTO.lastModifierUser).isEmpty()) {
						mapOfPhysiotherapy_planDTOTolastModifierUser.remove(oldPhysiotherapy_planDTO.lastModifierUser);
					}
					
					if(mapOfPhysiotherapy_planDTOTolastModificationTime.containsKey(oldPhysiotherapy_planDTO.lastModificationTime)) {
						mapOfPhysiotherapy_planDTOTolastModificationTime.get(oldPhysiotherapy_planDTO.lastModificationTime).remove(oldPhysiotherapy_planDTO);
					}
					if(mapOfPhysiotherapy_planDTOTolastModificationTime.get(oldPhysiotherapy_planDTO.lastModificationTime).isEmpty()) {
						mapOfPhysiotherapy_planDTOTolastModificationTime.remove(oldPhysiotherapy_planDTO.lastModificationTime);
					}
					
					
				}
				if(physiotherapy_planDTO.isDeleted == 0) 
				{
					
					mapOfPhysiotherapy_planDTOToiD.put(physiotherapy_planDTO.iD, physiotherapy_planDTO);
				
					if( ! mapOfPhysiotherapy_planDTOToappointmentId.containsKey(physiotherapy_planDTO.appointmentId)) {
						mapOfPhysiotherapy_planDTOToappointmentId.put(physiotherapy_planDTO.appointmentId, new HashSet<>());
					}
					mapOfPhysiotherapy_planDTOToappointmentId.get(physiotherapy_planDTO.appointmentId).add(physiotherapy_planDTO);
					
					if( ! mapOfPhysiotherapy_planDTOToname.containsKey(physiotherapy_planDTO.name)) {
						mapOfPhysiotherapy_planDTOToname.put(physiotherapy_planDTO.name, new HashSet<>());
					}
					mapOfPhysiotherapy_planDTOToname.get(physiotherapy_planDTO.name).add(physiotherapy_planDTO);
					
					if( ! mapOfPhysiotherapy_planDTOTodateOfBirth.containsKey(physiotherapy_planDTO.dateOfBirth)) {
						mapOfPhysiotherapy_planDTOTodateOfBirth.put(physiotherapy_planDTO.dateOfBirth, new HashSet<>());
					}
					mapOfPhysiotherapy_planDTOTodateOfBirth.get(physiotherapy_planDTO.dateOfBirth).add(physiotherapy_planDTO);
					
					if( ! mapOfPhysiotherapy_planDTOToage.containsKey(physiotherapy_planDTO.age)) {
						mapOfPhysiotherapy_planDTOToage.put(physiotherapy_planDTO.age, new HashSet<>());
					}
					mapOfPhysiotherapy_planDTOToage.get(physiotherapy_planDTO.age).add(physiotherapy_planDTO);
					
					if( ! mapOfPhysiotherapy_planDTOToheight.containsKey(physiotherapy_planDTO.height)) {
						mapOfPhysiotherapy_planDTOToheight.put(physiotherapy_planDTO.height, new HashSet<>());
					}
					mapOfPhysiotherapy_planDTOToheight.get(physiotherapy_planDTO.height).add(physiotherapy_planDTO);
					
					if( ! mapOfPhysiotherapy_planDTOToweight.containsKey(physiotherapy_planDTO.weight)) {
						mapOfPhysiotherapy_planDTOToweight.put(physiotherapy_planDTO.weight, new HashSet<>());
					}
					mapOfPhysiotherapy_planDTOToweight.get(physiotherapy_planDTO.weight).add(physiotherapy_planDTO);
					
					if( ! mapOfPhysiotherapy_planDTOTobloodPressureDiastole.containsKey(physiotherapy_planDTO.bloodPressureDiastole)) {
						mapOfPhysiotherapy_planDTOTobloodPressureDiastole.put(physiotherapy_planDTO.bloodPressureDiastole, new HashSet<>());
					}
					mapOfPhysiotherapy_planDTOTobloodPressureDiastole.get(physiotherapy_planDTO.bloodPressureDiastole).add(physiotherapy_planDTO);
					
					if( ! mapOfPhysiotherapy_planDTOTobloodPressureSystole.containsKey(physiotherapy_planDTO.bloodPressureSystole)) {
						mapOfPhysiotherapy_planDTOTobloodPressureSystole.put(physiotherapy_planDTO.bloodPressureSystole, new HashSet<>());
					}
					mapOfPhysiotherapy_planDTOTobloodPressureSystole.get(physiotherapy_planDTO.bloodPressureSystole).add(physiotherapy_planDTO);
					
					if( ! mapOfPhysiotherapy_planDTOTopulse.containsKey(physiotherapy_planDTO.pulse)) {
						mapOfPhysiotherapy_planDTOTopulse.put(physiotherapy_planDTO.pulse, new HashSet<>());
					}
					mapOfPhysiotherapy_planDTOTopulse.get(physiotherapy_planDTO.pulse).add(physiotherapy_planDTO);
					
					if( ! mapOfPhysiotherapy_planDTOTotemperature.containsKey(physiotherapy_planDTO.temperature)) {
						mapOfPhysiotherapy_planDTOTotemperature.put(physiotherapy_planDTO.temperature, new HashSet<>());
					}
					mapOfPhysiotherapy_planDTOTotemperature.get(physiotherapy_planDTO.temperature).add(physiotherapy_planDTO);
					
					if( ! mapOfPhysiotherapy_planDTOTooxygenSaturation.containsKey(physiotherapy_planDTO.oxygenSaturation)) {
						mapOfPhysiotherapy_planDTOTooxygenSaturation.put(physiotherapy_planDTO.oxygenSaturation, new HashSet<>());
					}
					mapOfPhysiotherapy_planDTOTooxygenSaturation.get(physiotherapy_planDTO.oxygenSaturation).add(physiotherapy_planDTO);
					
					if( ! mapOfPhysiotherapy_planDTOTobmi.containsKey(physiotherapy_planDTO.bmi)) {
						mapOfPhysiotherapy_planDTOTobmi.put(physiotherapy_planDTO.bmi, new HashSet<>());
					}
					mapOfPhysiotherapy_planDTOTobmi.get(physiotherapy_planDTO.bmi).add(physiotherapy_planDTO);
					
					if( ! mapOfPhysiotherapy_planDTOToremarks.containsKey(physiotherapy_planDTO.remarks)) {
						mapOfPhysiotherapy_planDTOToremarks.put(physiotherapy_planDTO.remarks, new HashSet<>());
					}
					mapOfPhysiotherapy_planDTOToremarks.get(physiotherapy_planDTO.remarks).add(physiotherapy_planDTO);
					
					if( ! mapOfPhysiotherapy_planDTOToinsertedByUserId.containsKey(physiotherapy_planDTO.insertedByUserId)) {
						mapOfPhysiotherapy_planDTOToinsertedByUserId.put(physiotherapy_planDTO.insertedByUserId, new HashSet<>());
					}
					mapOfPhysiotherapy_planDTOToinsertedByUserId.get(physiotherapy_planDTO.insertedByUserId).add(physiotherapy_planDTO);
					
					if( ! mapOfPhysiotherapy_planDTOToinsertedByOrganogramId.containsKey(physiotherapy_planDTO.insertedByOrganogramId)) {
						mapOfPhysiotherapy_planDTOToinsertedByOrganogramId.put(physiotherapy_planDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfPhysiotherapy_planDTOToinsertedByOrganogramId.get(physiotherapy_planDTO.insertedByOrganogramId).add(physiotherapy_planDTO);
					
					if( ! mapOfPhysiotherapy_planDTOToinsertionDate.containsKey(physiotherapy_planDTO.insertionDate)) {
						mapOfPhysiotherapy_planDTOToinsertionDate.put(physiotherapy_planDTO.insertionDate, new HashSet<>());
					}
					mapOfPhysiotherapy_planDTOToinsertionDate.get(physiotherapy_planDTO.insertionDate).add(physiotherapy_planDTO);
					
					if( ! mapOfPhysiotherapy_planDTOTolastModifierUser.containsKey(physiotherapy_planDTO.lastModifierUser)) {
						mapOfPhysiotherapy_planDTOTolastModifierUser.put(physiotherapy_planDTO.lastModifierUser, new HashSet<>());
					}
					mapOfPhysiotherapy_planDTOTolastModifierUser.get(physiotherapy_planDTO.lastModifierUser).add(physiotherapy_planDTO);
					
					if( ! mapOfPhysiotherapy_planDTOTolastModificationTime.containsKey(physiotherapy_planDTO.lastModificationTime)) {
						mapOfPhysiotherapy_planDTOTolastModificationTime.put(physiotherapy_planDTO.lastModificationTime, new HashSet<>());
					}
					mapOfPhysiotherapy_planDTOTolastModificationTime.get(physiotherapy_planDTO.lastModificationTime).add(physiotherapy_planDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Physiotherapy_planDTO> getPhysiotherapy_planList() {
		List <Physiotherapy_planDTO> physiotherapy_plans = new ArrayList<Physiotherapy_planDTO>(this.mapOfPhysiotherapy_planDTOToiD.values());
		return physiotherapy_plans;
	}
	
	
	public Physiotherapy_planDTO getPhysiotherapy_planDTOByID( long ID){
		return mapOfPhysiotherapy_planDTOToiD.get(ID);
	}
	
	
	public List<Physiotherapy_planDTO> getPhysiotherapy_planDTOByappointment_id(long appointment_id) {
		return new ArrayList<>( mapOfPhysiotherapy_planDTOToappointmentId.getOrDefault(appointment_id,new HashSet<>()));
	}
	
	
	public List<Physiotherapy_planDTO> getPhysiotherapy_planDTOByname(String name) {
		return new ArrayList<>( mapOfPhysiotherapy_planDTOToname.getOrDefault(name,new HashSet<>()));
	}
	
	
	public List<Physiotherapy_planDTO> getPhysiotherapy_planDTOBydate_of_birth(long date_of_birth) {
		return new ArrayList<>( mapOfPhysiotherapy_planDTOTodateOfBirth.getOrDefault(date_of_birth,new HashSet<>()));
	}
	
	
	public List<Physiotherapy_planDTO> getPhysiotherapy_planDTOByage(long age) {
		return new ArrayList<>( mapOfPhysiotherapy_planDTOToage.getOrDefault(age,new HashSet<>()));
	}
	
	
	public List<Physiotherapy_planDTO> getPhysiotherapy_planDTOByheight(double height) {
		return new ArrayList<>( mapOfPhysiotherapy_planDTOToheight.getOrDefault(height,new HashSet<>()));
	}
	
	
	public List<Physiotherapy_planDTO> getPhysiotherapy_planDTOByweight(double weight) {
		return new ArrayList<>( mapOfPhysiotherapy_planDTOToweight.getOrDefault(weight,new HashSet<>()));
	}
	
	
	public List<Physiotherapy_planDTO> getPhysiotherapy_planDTOByblood_pressure_diastole(double blood_pressure_diastole) {
		return new ArrayList<>( mapOfPhysiotherapy_planDTOTobloodPressureDiastole.getOrDefault(blood_pressure_diastole,new HashSet<>()));
	}
	
	
	public List<Physiotherapy_planDTO> getPhysiotherapy_planDTOByblood_pressure_systole(double blood_pressure_systole) {
		return new ArrayList<>( mapOfPhysiotherapy_planDTOTobloodPressureSystole.getOrDefault(blood_pressure_systole,new HashSet<>()));
	}
	
	
	public List<Physiotherapy_planDTO> getPhysiotherapy_planDTOBypulse(double pulse) {
		return new ArrayList<>( mapOfPhysiotherapy_planDTOTopulse.getOrDefault(pulse,new HashSet<>()));
	}
	
	
	public List<Physiotherapy_planDTO> getPhysiotherapy_planDTOBytemperature(double temperature) {
		return new ArrayList<>( mapOfPhysiotherapy_planDTOTotemperature.getOrDefault(temperature,new HashSet<>()));
	}
	
	
	public List<Physiotherapy_planDTO> getPhysiotherapy_planDTOByoxygen_saturation(double oxygen_saturation) {
		return new ArrayList<>( mapOfPhysiotherapy_planDTOTooxygenSaturation.getOrDefault(oxygen_saturation,new HashSet<>()));
	}
	
	
	public List<Physiotherapy_planDTO> getPhysiotherapy_planDTOBybmi(double bmi) {
		return new ArrayList<>( mapOfPhysiotherapy_planDTOTobmi.getOrDefault(bmi,new HashSet<>()));
	}
	
	
	public List<Physiotherapy_planDTO> getPhysiotherapy_planDTOByremarks(String remarks) {
		return new ArrayList<>( mapOfPhysiotherapy_planDTOToremarks.getOrDefault(remarks,new HashSet<>()));
	}
	
	
	public List<Physiotherapy_planDTO> getPhysiotherapy_planDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfPhysiotherapy_planDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Physiotherapy_planDTO> getPhysiotherapy_planDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfPhysiotherapy_planDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Physiotherapy_planDTO> getPhysiotherapy_planDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfPhysiotherapy_planDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Physiotherapy_planDTO> getPhysiotherapy_planDTOBylast_modifier_user(String last_modifier_user) {
		return new ArrayList<>( mapOfPhysiotherapy_planDTOTolastModifierUser.getOrDefault(last_modifier_user,new HashSet<>()));
	}
	
	
	public List<Physiotherapy_planDTO> getPhysiotherapy_planDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfPhysiotherapy_planDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "physiotherapy_plan";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


