package physiotherapy_plan;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;

import appointment.KeyCountDTO;
import repository.RepositoryManager;
import util.*;
import user.UserDTO;

public class PhysiotherapyScheduleDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public PhysiotherapyScheduleDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new PhysiotherapyScheduleMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"physiotherapy_plan_id",
			"therapy_date",
			"therapy_time",
			"therapy_type_type",
			"is_done",
			"remarks",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"therapist_user_id",
			"actual_therapy_time",
			"actual_therapy_date",
			"therapy_recipient_user_name",
			"therapist_organogram_id",
			
			"therapy_disease_type",
			"therapy_treatment_cat",
			"therapy_disease_name",
			
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public PhysiotherapyScheduleDAO()
	{
		this("physiotherapy_schedule");		
	}
	
	public void setSearchColumn(PhysiotherapyScheduleDTO physiotherapyscheduleDTO)
	{
	
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		PhysiotherapyScheduleDTO physiotherapyscheduleDTO = (PhysiotherapyScheduleDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(physiotherapyscheduleDTO);
		if(isInsert)
		{
			ps.setObject(index++,physiotherapyscheduleDTO.iD);
		}
		ps.setObject(index++,physiotherapyscheduleDTO.physiotherapyPlanId);
		ps.setObject(index++,physiotherapyscheduleDTO.therapyDate);
		ps.setObject(index++,physiotherapyscheduleDTO.therapyTime);
		ps.setObject(index++,physiotherapyscheduleDTO.therapyTypeType);
		ps.setObject(index++,physiotherapyscheduleDTO.isDone);
		ps.setObject(index++,physiotherapyscheduleDTO.remarks);
		ps.setObject(index++,physiotherapyscheduleDTO.insertedByUserId);
		ps.setObject(index++,physiotherapyscheduleDTO.insertedByOrganogramId);
		ps.setObject(index++,physiotherapyscheduleDTO.insertionDate);
		ps.setObject(index++,physiotherapyscheduleDTO.lastModifierUser);
		ps.setObject(index++,physiotherapyscheduleDTO.therapistUserId);
		ps.setObject(index++,physiotherapyscheduleDTO.actualTherapyTime);
		ps.setObject(index++,physiotherapyscheduleDTO.actualTherapyDate);
		ps.setObject(index++,physiotherapyscheduleDTO.therapyRecipientUserName);
		ps.setObject(index++,physiotherapyscheduleDTO.therapistOrganogramId);
		
		ps.setObject(index++,physiotherapyscheduleDTO.therapyDiseaseType);
		ps.setObject(index++,physiotherapyscheduleDTO.therapyTreatmentCat);
		ps.setObject(index++,physiotherapyscheduleDTO.therapyDiseaseName);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public PhysiotherapyScheduleDTO build(ResultSet rs)
	{
		try
		{
			PhysiotherapyScheduleDTO physiotherapyscheduleDTO = new PhysiotherapyScheduleDTO();
			physiotherapyscheduleDTO.iD = rs.getLong("ID");
			physiotherapyscheduleDTO.physiotherapyPlanId = rs.getLong("physiotherapy_plan_id");
			physiotherapyscheduleDTO.therapyDate = rs.getLong("therapy_date");
			physiotherapyscheduleDTO.therapyTime = rs.getString("therapy_time");
			physiotherapyscheduleDTO.therapyTypeType = rs.getLong("therapy_type_type");
			physiotherapyscheduleDTO.isDone = rs.getBoolean("is_done");
			physiotherapyscheduleDTO.remarks = rs.getString("remarks");
			physiotherapyscheduleDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			physiotherapyscheduleDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			physiotherapyscheduleDTO.insertionDate = rs.getLong("insertion_date");
			physiotherapyscheduleDTO.lastModifierUser = rs.getString("last_modifier_user");
			physiotherapyscheduleDTO.therapistUserId = rs.getLong("therapist_user_id");
			physiotherapyscheduleDTO.actualTherapyTime = rs.getLong("actual_therapy_time");
			physiotherapyscheduleDTO.actualTherapyDate = rs.getLong("actual_therapy_date");
			physiotherapyscheduleDTO.therapyRecipientUserName = rs.getString("therapy_recipient_user_name");
			physiotherapyscheduleDTO.therapistOrganogramId = rs.getLong("therapist_organogram_id");
			
			physiotherapyscheduleDTO.therapyDiseaseType = rs.getLong("therapy_disease_type");
			physiotherapyscheduleDTO.therapyTreatmentCat = rs.getLong("therapy_treatment_cat");
			physiotherapyscheduleDTO.therapyDiseaseName = rs.getString("therapy_disease_name");
			
			physiotherapyscheduleDTO.isDeleted = rs.getInt("isDeleted");
			physiotherapyscheduleDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return physiotherapyscheduleDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	public KeyCountDTO getTherapyDayWiseCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("therapy_date");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public List<KeyCountDTO> getLast7DayCount (long drId)
    {
		String sql = "SELECT therapy_date, COUNT(id) FROM physiotherapy_schedule where therapy_date >=" + TimeConverter.getNthDay(-7) + " and isDeleted = 0";
		if(drId != -1)
		{
			sql+= " and therapist_organogram_id = " + drId;
		}
		sql+= " group by therapy_date order by therapy_date asc";

		return ConnectionAndStatementUtil.getListOfT(sql,this::getTherapyDayWiseCount);	
    }
	
	public KeyCountDTO getTherapyCatWiseCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("therapy_type_type");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public List<KeyCountDTO> getTypeCount (long drId)
    {
		String sql = "SELECT therapy_type_type, COUNT(id) FROM physiotherapy_schedule where therapy_date >=" + TimeConverter.getNthDay(-7) + " and isDeleted = 0";
		if(drId != -1)
		{
			sql+= " and therapist_organogram_id = " + drId;
		}
		sql+= " group by therapy_type_type order by therapy_date asc";

		return ConnectionAndStatementUtil.getListOfT(sql,this::getTherapyCatWiseCount);	
    }
	
	
	public List<PhysiotherapyScheduleDTO> getPhysiotherapyScheduleDTOListByPhysiotherapyPlanID(long physiotherapyPlanID) throws Exception
	{
		String sql = "SELECT * FROM physiotherapy_schedule where isDeleted=0 and physiotherapy_plan_id="+physiotherapyPlanID+" order by physiotherapy_schedule.lastModificationTime";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}

	public PhysiotherapyScheduleDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		PhysiotherapyScheduleDTO physiotherapyscheduleDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return physiotherapyscheduleDTO;
	}

	
	public List<PhysiotherapyScheduleDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public int getCount(ResultSet rs)
	{
		try {
			return rs.getInt("count(id)");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	
	}

	public int getDoneCount (long planId)
    {
		String sql = "SELECT count(id) FROM physiotherapy_schedule";
		sql += " WHERE physiotherapy_plan_id = " + planId + " and isDeleted = 0 and is_done = 1";
		return ConnectionAndStatementUtil.getT(sql,this::getCount, 0);
    }
	
	public int getNotDoneCount (long planId)
    {
		String sql = "SELECT count(id) FROM physiotherapy_schedule";
		sql += " WHERE physiotherapy_plan_id = " + planId + " and isDeleted = 0 and is_done = 0";
		return ConnectionAndStatementUtil.getT(sql,this::getCount, 0);
    }
	
	public List<PhysiotherapyScheduleDTO> getAllPhysiotherapySchedule (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<PhysiotherapyScheduleDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<PhysiotherapyScheduleDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
				
}
	