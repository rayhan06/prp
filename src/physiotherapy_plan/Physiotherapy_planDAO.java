package physiotherapy_plan;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import appointment.KeyCountDTO;
import appointment.YearMonthCount;
import dbm.*;

import repository.RepositoryManager;

import util.*;
import workflow.WorkflowController;
import pb.*;
import user.UserDTO;

public class Physiotherapy_planDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Physiotherapy_planDAO(String tableName)
	{
		super(tableName);
		joinSQL = " join appointment on physiotherapy_plan.appointment_id = appointment.id "
				+ " join physiotherapy_schedule on physiotherapy_schedule.physiotherapy_plan_id = physiotherapy_plan.id";
		commonMaps = new Physiotherapy_planMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"appointment_id",
			"name",
			"date_of_birth",
			"age",
			"height",
			"weight",
			"blood_pressure_diastole",
			"blood_pressure_systole",
			"pulse",
			"temperature",
			"oxygen_saturation",
			"bmi",
			"remarks",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Physiotherapy_planDAO()
	{
		this("physiotherapy_plan");		
	}
	
	public void setSearchColumn(Physiotherapy_planDTO physiotherapy_planDTO)
	{
		physiotherapy_planDTO.searchColumn = "";
		physiotherapy_planDTO.searchColumn += physiotherapy_planDTO.name + " ";
		physiotherapy_planDTO.searchColumn += physiotherapy_planDTO.age + " ";
		physiotherapy_planDTO.searchColumn += physiotherapy_planDTO.remarks + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Physiotherapy_planDTO physiotherapy_planDTO = (Physiotherapy_planDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(physiotherapy_planDTO);
		if(isInsert)
		{
			ps.setObject(index++,physiotherapy_planDTO.iD);
		}
		ps.setObject(index++,physiotherapy_planDTO.appointmentId);
		ps.setObject(index++,physiotherapy_planDTO.name);
		ps.setObject(index++,physiotherapy_planDTO.dateOfBirth);
		ps.setObject(index++,physiotherapy_planDTO.age);
		ps.setObject(index++,physiotherapy_planDTO.height);
		ps.setObject(index++,physiotherapy_planDTO.weight);
		ps.setObject(index++,physiotherapy_planDTO.bloodPressureDiastole);
		ps.setObject(index++,physiotherapy_planDTO.bloodPressureSystole);
		ps.setObject(index++,physiotherapy_planDTO.pulse);
		ps.setObject(index++,physiotherapy_planDTO.temperature);
		ps.setObject(index++,physiotherapy_planDTO.oxygenSaturation);
		ps.setObject(index++,physiotherapy_planDTO.bmi);
		ps.setObject(index++,physiotherapy_planDTO.remarks);
		ps.setObject(index++,physiotherapy_planDTO.insertedByUserId);
		ps.setObject(index++,physiotherapy_planDTO.insertedByOrganogramId);
		ps.setObject(index++,physiotherapy_planDTO.insertionDate);
		ps.setObject(index++,physiotherapy_planDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Physiotherapy_planDTO build(ResultSet rs)
	{
		try
		{
			Physiotherapy_planDTO physiotherapy_planDTO = new Physiotherapy_planDTO();
			physiotherapy_planDTO.iD = rs.getLong("ID");
			physiotherapy_planDTO.appointmentId = rs.getLong("appointment_id");
			physiotherapy_planDTO.name = rs.getString("name");
			physiotherapy_planDTO.dateOfBirth = rs.getLong("date_of_birth");
			physiotherapy_planDTO.age = rs.getLong("age");
			physiotherapy_planDTO.height = rs.getDouble("height");
			physiotherapy_planDTO.weight = rs.getDouble("weight");
			physiotherapy_planDTO.bloodPressureDiastole = rs.getDouble("blood_pressure_diastole");
			physiotherapy_planDTO.bloodPressureSystole = rs.getDouble("blood_pressure_systole");
			physiotherapy_planDTO.pulse = rs.getDouble("pulse");
			physiotherapy_planDTO.temperature = rs.getDouble("temperature");
			physiotherapy_planDTO.oxygenSaturation = rs.getDouble("oxygen_saturation");
			physiotherapy_planDTO.bmi = rs.getDouble("bmi");
			physiotherapy_planDTO.remarks = rs.getString("remarks");
			physiotherapy_planDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			physiotherapy_planDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			physiotherapy_planDTO.insertionDate = rs.getLong("insertion_date");
			physiotherapy_planDTO.lastModifierUser = rs.getString("last_modifier_user");
			physiotherapy_planDTO.isDeleted = rs.getInt("isDeleted");
			physiotherapy_planDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return physiotherapy_planDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

	public Physiotherapy_planDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Physiotherapy_planDTO physiotherapy_planDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		try {
			PhysiotherapyScheduleDAO physiotherapyScheduleDAO = new PhysiotherapyScheduleDAO("physiotherapy_schedule");			
			List<PhysiotherapyScheduleDTO> physiotherapyScheduleDTOList = physiotherapyScheduleDAO.getPhysiotherapyScheduleDTOListByPhysiotherapyPlanID(physiotherapy_planDTO.iD);
			physiotherapy_planDTO.physiotherapyScheduleDTOList = physiotherapyScheduleDTOList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
		return physiotherapy_planDTO;
	}
	
	
	public List<YearMonthCount> getLast6MonthCount (boolean isLangEng)
    {
		
		String sql = "SELECT \r\n" + 
				"    DATE_FORMAT(FROM_UNIXTIME(`insertion_date` / 1000),\r\n" + 
				"            '%Y-%m') AS ym,\r\n" + 
				"    COUNT(id),\r\n" + 
				"    inserted_by_organogram_id\r\n" + 
				"FROM\r\n" + 
				"    physiotherapy_plan\r\n" + 
				"WHERE\r\n" + 
				"    insertion_date >= " + TimeConverter.get1stDayOfNthtMonth(-6) + "\r\n" + 
				"        AND isDeleted = 0\r\n" + 
				"GROUP BY ym , inserted_by_organogram_id\r\n" + 
				"ORDER BY ym ASC , inserted_by_organogram_id ASC";
		
		//System.out.println(sql);
		List<YearMonthCount> counts = ConnectionAndStatementUtil.getListOfT(sql,this::getYmCount);	
	
		return counts;
    }
	
	
	public YearMonthCount getYmCount(ResultSet rs)
	{
		try
		{
			YearMonthCount ymCount = new YearMonthCount();
			String ym = rs.getString("ym");
			ymCount.year = Integer.parseInt(ym.split("-")[0]);
			ymCount.month = Integer.parseInt(ym.split("-")[1]);
			ymCount.count = rs.getInt("count(id)");
			ymCount.key = rs.getLong("inserted_by_organogram_id");
			ymCount.startDate = ymCount.year *12 + ymCount.month;
			
			//System.out.println("year = " + ymCount.year + " month = " + ymCount.month + " count = " + ymCount.count);

			return ymCount;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public KeyCountDTO getTherapyDayWiseCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("insertion_date");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}	
	public List<KeyCountDTO> getLast7DayCount (long drId)
    {
		String sql = "SELECT insertion_date, COUNT(id) FROM physiotherapy_plan where insertion_date >=" + TimeConverter.getNthDay(-7) + " and isDeleted = 0";
		if(drId != -1)
		{
			sql+= " and inserted_by_organogram_id = " + drId;
		}
		sql+= " group by insertion_date order by insertion_date asc";

		return ConnectionAndStatementUtil.getListOfT(sql,this::getTherapyDayWiseCount);	
    }
	
	public Physiotherapy_planDTO getDTOByappointmentId (long appointmentId)
	{
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
		
        sql += " WHERE isDeleted = 0 and appointment_id =" + appointmentId;
		Physiotherapy_planDTO physiotherapy_planDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		try {
			if(physiotherapy_planDTO != null)
			{
				PhysiotherapyScheduleDAO physiotherapyScheduleDAO = new PhysiotherapyScheduleDAO("physiotherapy_schedule");			
				List<PhysiotherapyScheduleDTO> physiotherapyScheduleDTOList = physiotherapyScheduleDAO.getPhysiotherapyScheduleDTOListByPhysiotherapyPlanID(physiotherapy_planDTO.iD);
				physiotherapy_planDTO.physiotherapyScheduleDTOList = physiotherapyScheduleDTOList;
			}			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
		return physiotherapy_planDTO;
	}

	public List<Physiotherapy_planDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<Physiotherapy_planDTO> getAllPhysiotherapy_plan (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Physiotherapy_planDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Physiotherapy_planDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".name like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
				AnyfieldSql+= " or appointment.employee_record_id = " + WorkflowController.getEmployeeRecordsIdFromUserName(Utils.getDigitEnglishFromBangla(p_searchCriteria.get("AnyField").toString()));	
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name")
						|| str.equals("inserted_by_organogram_id")
						|| str.equals("visit_date_start")
						|| str.equals("visit_date_end")
						|| str.equals("employee_user_name")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					if(str.equals("employee_user_name"))
					{
						AllFieldSql += "appointment.employee_record_id = " + WorkflowController.getEmployeeRecordsIdFromUserName((String)p_searchCriteria.get(str)) ;
						i ++;
					}
					
					else if(str.equals("name"))
					{
						AllFieldSql += "" + tableName + ".name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					
					else if(str.equals("inserted_by_organogram_id"))
					{
						AllFieldSql += "" + tableName + ".inserted_by_organogram_id = " + p_searchCriteria.get(str) ;
						i ++;
					}
					
					else if(str.equals("visit_date_start"))
					{
						AllFieldSql += "appointment.visit_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("visit_date_end"))
					{
						AllFieldSql += "appointment.visit_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		if(userDTO.roleID != SessionConstants.ADMIN_ROLE
				&& userDTO.roleID != SessionConstants.MEDICAL_ADMIN_ROLE
				&& userDTO.roleID != SessionConstants.MEDICAL_RECEPTIONIST_ROLE
				&& userDTO.roleID != SessionConstants.PHYSIOTHERAPIST_ROLE)
		{
			sql += " and appointment.employee_record_id =  " + userDTO.employee_record_id;
		}
		
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	