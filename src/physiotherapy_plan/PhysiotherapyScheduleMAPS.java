package physiotherapy_plan;
import java.util.*; 
import util.*;


public class PhysiotherapyScheduleMAPS extends CommonMaps
{	
	public PhysiotherapyScheduleMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("physiotherapyPlanId".toLowerCase(), "physiotherapyPlanId".toLowerCase());
		java_DTO_map.put("therapyDate".toLowerCase(), "therapyDate".toLowerCase());
		java_DTO_map.put("therapyTime".toLowerCase(), "therapyTime".toLowerCase());
		java_DTO_map.put("therapyCat".toLowerCase(), "therapyCat".toLowerCase());
		java_DTO_map.put("isDone".toLowerCase(), "isDone".toLowerCase());
		java_DTO_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("lastModifierUser".toLowerCase(), "lastModifierUser".toLowerCase());
		java_DTO_map.put("therapistUserId".toLowerCase(), "therapistUserId".toLowerCase());
		java_DTO_map.put("actualTherapyTime".toLowerCase(), "actualTherapyTime".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("physiotherapy_plan_id".toLowerCase(), "physiotherapyPlanId".toLowerCase());
		java_SQL_map.put("therapy_date".toLowerCase(), "therapyDate".toLowerCase());
		java_SQL_map.put("therapy_time".toLowerCase(), "therapyTime".toLowerCase());
		java_SQL_map.put("therapy_type_type".toLowerCase(), "therapyCat".toLowerCase());
		java_SQL_map.put("is_done".toLowerCase(), "isDone".toLowerCase());
		java_SQL_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_SQL_map.put("therapist_user_id".toLowerCase(), "therapistUserId".toLowerCase());
		java_SQL_map.put("actual_therapy_time".toLowerCase(), "actualTherapyTime".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Physiotherapy Plan Id".toLowerCase(), "physiotherapyPlanId".toLowerCase());
		java_Text_map.put("Therapy Date".toLowerCase(), "therapyDate".toLowerCase());
		java_Text_map.put("Therapy Time".toLowerCase(), "therapyTime".toLowerCase());
		java_Text_map.put("Therapy".toLowerCase(), "therapyCat".toLowerCase());
		java_Text_map.put("Is Done".toLowerCase(), "isDone".toLowerCase());
		java_Text_map.put("Remarks".toLowerCase(), "remarks".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Last Modifier User".toLowerCase(), "lastModifierUser".toLowerCase());
		java_Text_map.put("Therapist User Id".toLowerCase(), "therapistUserId".toLowerCase());
		java_Text_map.put("Actual Therapy Time".toLowerCase(), "actualTherapyTime".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}