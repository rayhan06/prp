package physiotherapy_plan;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import prescription_details.Constants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;
import therapy_type.TherapyDiseaseDTO;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;
import util.TimeConverter;
import workflow.WorkflowController;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import appointment.AppointmentDAO;
import appointment.AppointmentDTO;
import appointment_letter.Appointment_letterDAO;
import family.FamilyDTO;
import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Physiotherapy_planServlet
 */
@WebServlet("/Physiotherapy_planServlet")
@MultipartConfig
public class Physiotherapy_planServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Physiotherapy_planServlet.class);

    String tableName = "physiotherapy_plan";

	Physiotherapy_planDAO physiotherapy_planDAO;
	CommonRequestHandler commonRequestHandler;
	PhysiotherapyScheduleDAO physiotherapyScheduleDAO;
	AppointmentDAO appointmentDAO;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Physiotherapy_planServlet() 
	{
        super();
    	try
    	{
			physiotherapy_planDAO = new Physiotherapy_planDAO(tableName);
			physiotherapyScheduleDAO = new PhysiotherapyScheduleDAO("physiotherapy_schedule");
			commonRequestHandler = new CommonRequestHandler(physiotherapy_planDAO);
			appointmentDAO= new AppointmentDAO();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PHYSIOTHERAPY_PLAN_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PHYSIOTHERAPY_PLAN_UPDATE))
				{
					getPhysiotherapy_plan(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getFormattedSearchPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PHYSIOTHERAPY_PLAN_SEARCH)
						)
				{
					String userName = Utils.getDigitEnglishFromBangla(request.getParameter("userName"));
					if(!Utils.isValidUserName(userName))
					{
						return;
					}
					int whoIsThePatientCat = Integer.parseInt(request.getParameter("whoIsThePatientCat"));
					String filter = " appointment.employee_record_id = " + WorkflowController.getEmployeeRecordsIdFromUserName(userName) + " ";
					if(whoIsThePatientCat != FamilyDTO.UNSELECTED)
					{
						filter+= " and appointment.who_is_the_patient_cat =" + whoIsThePatientCat;
					}
					System.out.println("filter = " + filter);
					
					searchPhysiotherapy_plan(request, response, isPermanentTable, filter);
					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				boolean hasAjax = false;
				String ajax = request.getParameter("ajax");
				if(ajax != null && !ajax.equalsIgnoreCase(""))
				{
					hasAjax = true;
				}
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PHYSIOTHERAPY_PLAN_SEARCH))
				{
					
					String filter = request.getParameter("filter");
					System.out.println("filter = " + filter);
					if(filter!=null)
					{
						if(filter.equalsIgnoreCase("todaysUpcomingTherapies") 
								|| (!hasAjax && !(userDTO.roleID ==  SessionConstants.ADMIN_ROLE || userDTO.roleID ==  SessionConstants.MEDICAL_ADMIN_ROLE)))
						{
							filter = " therapy_date = " + TimeConverter.getToday() + " and is_done = 0 "
									+ " and therapist_organogram_id = " + userDTO.organogramID;
						}
						else
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
						}
						searchPhysiotherapy_plan(request, response, isPermanentTable, filter);
					}
					else
					{
						searchPhysiotherapy_plan(request, response, isPermanentTable, "");
					}
					
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PHYSIOTHERAPY_PLAN_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PHYSIOTHERAPY_PLAN_ADD))
				{
					System.out.println("going to  addPhysiotherapy_plan ");
					addPhysiotherapy_plan(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addPhysiotherapy_plan ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PHYSIOTHERAPY_PLAN_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addPhysiotherapy_plan ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PHYSIOTHERAPY_PLAN_UPDATE))
				{					
					addPhysiotherapy_plan(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("addTherapy"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PHYSIOTHERAPY_PLAN_UPDATE))
				{					
					addTherapy(request, response, userDTO);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PHYSIOTHERAPY_PLAN_UPDATE))
				{
					try 
					{
						String[] IDsToDelete = request.getParameterValues("ID");
						for(int i = 0; i < IDsToDelete.length; i ++)
						{
							long id = Long.parseLong(IDsToDelete[i]);
							System.out.println("------ DELETING " + IDsToDelete[i]);
							
							
							CommonDTO commonDTO = physiotherapy_planDAO.getDTOByID(id);
							if(physiotherapyScheduleDAO.getDoneCount(commonDTO.iD) <= 0)
							{
								physiotherapy_planDAO.manageWriteOperations(commonDTO, SessionConstants.DELETE, id, userDTO);
							}
							
				
						}
						response.sendRedirect( "Physiotherapy_planServlet?actionType=search");
					}
					catch (Exception ex) 
					{
						ex.printStackTrace();
					}
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PHYSIOTHERAPY_PLAN_SEARCH))
				{
					searchPhysiotherapy_plan(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void addTherapy(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) 
	{
		
		// TODO Auto-generated method stub
		long id = Long.parseLong(request.getParameter("ID"));
		String[] scIds = request.getParameterValues("scId");
		int i = 0;
		System.out.println("addTherapy = " + id);
		for(String str_scId:  scIds)
		{
			long scId = Long.parseLong(str_scId);
			PhysiotherapyScheduleDTO physiotherapyScheduleDTO =  physiotherapyScheduleDAO.getDTOByID(scId);
			if(physiotherapyScheduleDTO != null)
			{
				physiotherapyScheduleDTO.remarks = request.getParameterValues("remarks")[i];
				
				if(!physiotherapyScheduleDTO.isDone)
				{
					physiotherapyScheduleDTO.isDone = Boolean.parseBoolean(request.getParameterValues("isDone")[i]);
					if(physiotherapyScheduleDTO.isDone)
					{
						//physiotherapyScheduleDTO.therapistUserId = userDTO.ID;
						//physiotherapyScheduleDTO.therapistOrganogramId = userDTO.organogramID;
						physiotherapyScheduleDTO.actualTherapyTime = System.currentTimeMillis();
						physiotherapyScheduleDTO.actualTherapyDate = TimeConverter.getToday();
						
						Physiotherapy_planDTO physiotherapy_planDTO = physiotherapy_planDAO.getDTOByID(physiotherapyScheduleDTO.physiotherapyPlanId);
						if(physiotherapy_planDTO != null)
						{
							AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(physiotherapy_planDTO.appointmentId);
							if(appointmentDTO != null)
							{
								physiotherapyScheduleDTO.therapyRecipientUserName = appointmentDTO.employeeUserName;
							}
						}
					}
				}
				try {
					physiotherapyScheduleDAO.update(physiotherapyScheduleDTO);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("physiotherapyScheduleDTO null for id = " + scId);
			}
			i++;
		}
		try {
			response.sendRedirect("Physiotherapy_planServlet?actionType=search");			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Physiotherapy_planDTO physiotherapy_planDTO = physiotherapy_planDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(physiotherapy_planDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addPhysiotherapy_plan(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addPhysiotherapy_plan");
			String path = getServletContext().getRealPath("/img2/");
			Physiotherapy_planDTO physiotherapy_planDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				physiotherapy_planDTO = new Physiotherapy_planDTO();
			}
			else
			{
				physiotherapy_planDTO = physiotherapy_planDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("appointmentId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("appointmentId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				physiotherapy_planDTO.appointmentId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			if(addFlag)
			{
				Physiotherapy_planDTO oldPlanDTO = physiotherapy_planDAO.getDTOByappointmentId(physiotherapy_planDTO.appointmentId);
				if(oldPlanDTO != null)
				{
					response.sendRedirect("Physiotherapy_planServlet?actionType=search");
					return;
				}
			}
			 

			Value = request.getParameter("name");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("name = " + Value);
			if(Value != null)
			{
				physiotherapy_planDTO.name = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("dateOfBirth");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("dateOfBirth = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try 
				{
					Date d = f.parse(Value);
					physiotherapy_planDTO.dateOfBirth = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("age");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("age = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				physiotherapy_planDTO.age = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("height");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("height = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				physiotherapy_planDTO.height = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("weight");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("weight = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				physiotherapy_planDTO.weight = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("bloodPressureDiastole");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("bloodPressureDiastole = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				physiotherapy_planDTO.bloodPressureDiastole = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("bloodPressureSystole");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("bloodPressureSystole = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				physiotherapy_planDTO.bloodPressureSystole = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("pulse");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("pulse = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				physiotherapy_planDTO.pulse = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("temperature");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("temperature = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				physiotherapy_planDTO.temperature = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("oxygenSaturation");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("oxygenSaturation = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				physiotherapy_planDTO.oxygenSaturation = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("bmi");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("bmi = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				physiotherapy_planDTO.bmi = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("remarks");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("remarks = " + Value);
			if(Value != null)
			{
				physiotherapy_planDTO.remarks = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				physiotherapy_planDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				physiotherapy_planDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				physiotherapy_planDTO.insertionDate = c.getTimeInMillis();
			}			


			physiotherapy_planDTO.lastModifierUser = userDTO.userName;

			
			System.out.println("Done adding  addPhysiotherapy_plan dto = " + physiotherapy_planDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				physiotherapy_planDAO.setIsDeleted(physiotherapy_planDTO.iD, CommonDTO.OUTDATED);
				returnedID = physiotherapy_planDAO.add(physiotherapy_planDTO);
				physiotherapy_planDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = physiotherapy_planDAO.manageWriteOperations(physiotherapy_planDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = physiotherapy_planDAO.manageWriteOperations(physiotherapy_planDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			AppointmentDAO appointmentDAO = new AppointmentDAO();
			AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(physiotherapy_planDTO.appointmentId);
			long therapistId = appointmentDTO.doctorId;
			
			
			
			List<PhysiotherapyScheduleDTO> physiotherapyScheduleDTOList = createPhysiotherapyScheduleDTOListByRequest(request);			
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(physiotherapyScheduleDTOList != null)
				{				
					for(PhysiotherapyScheduleDTO physiotherapyScheduleDTO: physiotherapyScheduleDTOList)
					{
						physiotherapyScheduleDTO.physiotherapyPlanId = physiotherapy_planDTO.iD;
						physiotherapyScheduleDTO.insertedByUserId = userDTO.ID;
						physiotherapyScheduleDTO.insertedByOrganogramId = userDTO.ID;
						physiotherapyScheduleDTO.insertionDate = System.currentTimeMillis();
						physiotherapyScheduleDTO.lastModifierUser = userDTO.userName;
						physiotherapyScheduleDTO.therapistOrganogramId = therapistId;
						physiotherapyScheduleDAO.add(physiotherapyScheduleDTO);
					}
				}
			
			}
			else
			{
				List<Long> childIdsFromRequest = physiotherapyScheduleDAO.getChildIdsFromRequest(request, "physiotherapySchedule");
				//delete the removed children
				physiotherapyScheduleDAO.deleteChildrenNotInList("physiotherapy_plan", "physiotherapy_schedule", physiotherapy_planDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = physiotherapyScheduleDAO.getChilIds("physiotherapy_plan", "physiotherapy_schedule", physiotherapy_planDTO.iD);
				
				
				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							PhysiotherapyScheduleDTO physiotherapyScheduleDTO =  createPhysiotherapyScheduleDTOByRequestAndIndex(request, false, i);
							physiotherapyScheduleDTO.physiotherapyPlanId = physiotherapy_planDTO.iD;
							physiotherapyScheduleDTO.lastModifierUser = userDTO.userName;
							physiotherapyScheduleDTO.therapistOrganogramId = therapistId;
							physiotherapyScheduleDAO.update(physiotherapyScheduleDTO);
						}
						else
						{
							PhysiotherapyScheduleDTO physiotherapyScheduleDTO =  createPhysiotherapyScheduleDTOByRequestAndIndex(request, true, i);
							physiotherapyScheduleDTO.physiotherapyPlanId = physiotherapy_planDTO.iD;
							physiotherapyScheduleDTO.insertedByUserId = userDTO.ID;
							physiotherapyScheduleDTO.insertedByOrganogramId = userDTO.ID;
							physiotherapyScheduleDTO.insertionDate = System.currentTimeMillis();
							physiotherapyScheduleDTO.lastModifierUser = userDTO.userName;
							physiotherapyScheduleDTO.therapistOrganogramId = therapistId;
							physiotherapyScheduleDAO.add(physiotherapyScheduleDTO);
						}
					}
				}
				else
				{
					physiotherapyScheduleDAO.deleteChildrenByParent(physiotherapy_planDTO.iD, "physiotherapy_plan_id");
				}
				
			}
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getPhysiotherapy_plan(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Physiotherapy_planServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(physiotherapy_planDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	private List<PhysiotherapyScheduleDTO> createPhysiotherapyScheduleDTOListByRequest(HttpServletRequest request) throws Exception{ 
		List<PhysiotherapyScheduleDTO> physiotherapyScheduleDTOList = new ArrayList<PhysiotherapyScheduleDTO>();
		if(request.getParameterValues("physiotherapySchedule.iD") != null) 
		{
			int physiotherapyScheduleItemNo = request.getParameterValues("physiotherapySchedule.iD").length;
			
			
			for(int index=0;index<physiotherapyScheduleItemNo;index++){
				PhysiotherapyScheduleDTO physiotherapyScheduleDTO = createPhysiotherapyScheduleDTOByRequestAndIndex(request,true,index);
				physiotherapyScheduleDTOList.add(physiotherapyScheduleDTO);
			}
			
			return physiotherapyScheduleDTOList;
		}
		return null;
	}
	
	
	private PhysiotherapyScheduleDTO createPhysiotherapyScheduleDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{
	
		PhysiotherapyScheduleDTO physiotherapyScheduleDTO;
		if(addFlag == true )
		{
			physiotherapyScheduleDTO = new PhysiotherapyScheduleDTO();
		}
		else
		{
			physiotherapyScheduleDTO = physiotherapyScheduleDAO.getDTOByID(Long.parseLong(request.getParameterValues("physiotherapySchedule.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		
				
		String Value = "";
		Value = request.getParameterValues("physiotherapySchedule.physiotherapyPlanId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		physiotherapyScheduleDTO.physiotherapyPlanId = Long.parseLong(Value);
		Value = request.getParameterValues("physiotherapySchedule.therapyDate")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		try 
		{
			Date d = f.parse(Value);
			physiotherapyScheduleDTO.therapyDate = d.getTime();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		Value = request.getParameterValues("physiotherapySchedule.therapyTime")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		physiotherapyScheduleDTO.therapyTime = (Value);
		
		Value = request.getParameterValues("physiotherapySchedule.therapyCat")[index];
		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			physiotherapyScheduleDTO.therapyTypeType = Integer.parseInt(Value);
		}
		
		Value = request.getParameterValues("physiotherapySchedule.therapyDiseaseType")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			physiotherapyScheduleDTO.therapyDiseaseType = Long.parseLong(Value);
		}
		

		Value = request.getParameterValues("physiotherapySchedule.therapyDiseaseName")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			physiotherapyScheduleDTO.therapyDiseaseName = (Value);
		}
		
		Value = request.getParameterValues("physiotherapySchedule.therapyTreatmentCat")[index];
		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			physiotherapyScheduleDTO.therapyTreatmentCat = Integer.parseInt(Value);
		}
		

		
		return physiotherapyScheduleDTO;
	
	}
	
	
	

	private void getPhysiotherapy_plan(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getPhysiotherapy_plan");
		Physiotherapy_planDTO physiotherapy_planDTO = null;
		try 
		{
			physiotherapy_planDTO = physiotherapy_planDAO.getDTOByID(id);
			request.setAttribute("ID", physiotherapy_planDTO.iD);
			request.setAttribute("physiotherapy_planDTO",physiotherapy_planDTO);
			request.setAttribute("physiotherapy_planDAO",physiotherapy_planDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "physiotherapy_plan/physiotherapy_planInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "physiotherapy_plan/physiotherapy_planSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "physiotherapy_plan/physiotherapy_planEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "physiotherapy_plan/physiotherapy_planEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getPhysiotherapy_plan(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getPhysiotherapy_plan(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchPhysiotherapy_plan(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchPhysiotherapy_plan 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_PHYSIOTHERAPY_PLAN,
			request,
			physiotherapy_planDAO,
			SessionConstants.VIEW_PHYSIOTHERAPY_PLAN,
			SessionConstants.SEARCH_PHYSIOTHERAPY_PLAN,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("physiotherapy_planDAO",physiotherapy_planDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to physiotherapy_plan/physiotherapy_planApproval.jsp");
	        	rd = request.getRequestDispatcher("physiotherapy_plan/physiotherapy_planApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to physiotherapy_plan/physiotherapy_planApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("physiotherapy_plan/physiotherapy_planApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to physiotherapy_plan/physiotherapy_planSearch.jsp");
	        	rd = request.getRequestDispatcher("physiotherapy_plan/physiotherapy_planSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to physiotherapy_plan/physiotherapy_planSearchForm.jsp");
	        	rd = request.getRequestDispatcher("physiotherapy_plan/physiotherapy_planSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

