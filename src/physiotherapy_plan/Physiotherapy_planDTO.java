package physiotherapy_plan;
import java.util.*; 
import util.*; 


public class Physiotherapy_planDTO extends CommonDTO
{

	public long appointmentId = -1;
    public String name = "";
	public long dateOfBirth = System.currentTimeMillis();
	public long age = -1;
	public double height = -1;
	public double weight = -1;
	public double bloodPressureDiastole = -1;
	public double bloodPressureSystole = -1;
	public double pulse = -1;
	public double temperature = -1;
	public double oxygenSaturation = -1;
	public double bmi = -1;
    public String remarks = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	
	public List<PhysiotherapyScheduleDTO> physiotherapyScheduleDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Physiotherapy_planDTO[" +
            " iD = " + iD +
            " appointmentId = " + appointmentId +
            " name = " + name +
            " dateOfBirth = " + dateOfBirth +
            " age = " + age +
            " height = " + height +
            " weight = " + weight +
            " bloodPressureDiastole = " + bloodPressureDiastole +
            " bloodPressureSystole = " + bloodPressureSystole +
            " pulse = " + pulse +
            " temperature = " + temperature +
            " oxygenSaturation = " + oxygenSaturation +
            " bmi = " + bmi +
            " remarks = " + remarks +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}