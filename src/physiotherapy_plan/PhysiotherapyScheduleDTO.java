package physiotherapy_plan;
import util.*; 


public class PhysiotherapyScheduleDTO extends CommonDTO
{

	public long physiotherapyPlanId = -1;
	public long therapyDate = System.currentTimeMillis();
    public String therapyTime = "";
	public long therapyTypeType = -1;
	public boolean isDone = false;
    public String remarks = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	public long therapistUserId = -1;
	public long therapistOrganogramId = -1;
	public long actualTherapyTime = -1;
	public long actualTherapyDate = -1;
	public String therapyRecipientUserName = "";
	
	public long therapyDiseaseType = -1;
	public long therapyTreatmentCat = -1;
	public String therapyDiseaseName = "";
	

	
    @Override
	public String toString() {
            return "$PhysiotherapyScheduleDTO[" +
            " iD = " + iD +
            " physiotherapyPlanId = " + physiotherapyPlanId +
            " therapyDate = " + therapyDate +
            " therapyTime = " + therapyTime +
            " therapyCat = " + therapyTypeType +
            " isDone = " + isDone +
            " remarks = " + remarks +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " therapistUserId = " + therapistUserId +
            " actualTherapyTime = " + actualTherapyTime +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}