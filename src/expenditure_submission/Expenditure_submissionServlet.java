package expenditure_submission;

import budget.BudgetDAO;
import budget.BudgetModel;
import budget.BudgetUtils;
import budget_submission_info.Budget_submission_infoDAO;
import com.google.gson.Gson;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/Expenditure_submissionServlet")
@MultipartConfig
public class Expenditure_submissionServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static final Logger logger = Logger.getLogger(Expenditure_submissionServlet.class);

    private static final String tableName = "budget";

    private final BudgetDAO budgetDAO;
    private final Gson gson = new Gson();

    public Expenditure_submissionServlet() {
        super();
        budgetDAO = BudgetDAO.getInstance();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (hasPermission(userDTO)) {
            try {
                String actionType = request.getParameter("actionType");
                switch (actionType) {

                    case "budget_actual_expenditure":
                        request.getRequestDispatcher("budget_actual_expenditure/budget_actual_expenditureEdit.jsp").forward(request, response);
                        return;
                    case "budget_actual_expenditure_preview":
                        request.getRequestDispatcher("budget_actual_expenditure/budget_actual_expenditure_preview.jsp").forward(request, response);
                        return;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                logger.debug(ex);
            }
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        if (hasPermission(userDTO)) {
            try {
                long requestBy;
                long requestTime;
                String actionType = request.getParameter("actionType");

                switch (actionType) {
                    case "insertExpenditureAmount": {
                        long budgetSelectionInfoId = Long.parseLong(request.getParameter("budgetSelectionInfoId"));
                        int budgetTypeCat = Integer.parseInt(request.getParameter("budgetTypeCat"));
                        long budgetOfficeId = BudgetUtils.FINANCE_SECTION_OFFICE_ID;

                        boolean isAbleToSubmit =  Budget_submission_infoDAO.getInstance().isAbleToSubmitExpenditure(
                                budgetSelectionInfoId, budgetOfficeId, budgetTypeCat
                        );

                        if(isAbleToSubmit){
                            requestBy = userDTO.ID;
                            requestTime = System.currentTimeMillis();
                            String expenditureValue = request.getParameter("budgetModels");
                            if (expenditureValue != null) {
                                expenditureValue = Jsoup.clean(expenditureValue, Whitelist.simpleText());
                            }
                            BudgetModel[] expenditureBudgetModels = gson.fromJson(expenditureValue, BudgetModel[].class);
                            for(BudgetModel budgetModel : expenditureBudgetModels){
                                budgetDAO.updateExpenditureAmountById(budgetModel, budgetTypeCat, requestBy, requestTime);
                            }
                        }
                        PrintWriter out = response.getWriter();
                        response.setContentType("application/json");
                        response.setCharacterEncoding("UTF-8");
                        out.print(gson.toJson("{\"success\": " + isAbleToSubmit + "}"));
                        out.flush();
                        return;
                    }
                    case "submitAllExpenditure": {
                        boolean flag = submitAllExpenditure(request, userDTO);
                        String resJson = "{\"success\": " + flag + "}";
                        PrintWriter out = response.getWriter();
                        response.setContentType("application/json");
                        response.setCharacterEncoding("UTF-8");
                        out.print(resJson);
                        out.flush();
                        return;
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                logger.debug(ex);
            }
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private boolean hasPermission(UserDTO userDTO){
        return userDTO != null && PermissionRepository.checkPermissionByRoleIDAndMenuID(
                userDTO.roleID, MenuConstants.BUDGET_ACTUAL_EXPENDITURE
        );
    }
    
    private boolean submitAllExpenditure(HttpServletRequest request,UserDTO userDTO) {
        long budgetSelectionInfoId = Long.parseLong(request.getParameter("budgetSelectionInfoId"));
        int budgetTypeCat = Integer.parseInt(request.getParameter("budgetTypeCat"));
        long requesterId = userDTO.ID;
        try{
            Budget_submission_infoDAO.getInstance().submitExpenditure(
                    budgetSelectionInfoId, BudgetUtils.FINANCE_SECTION_OFFICE_ID,
                    budgetTypeCat, requesterId
            );
        }catch (Exception ex){
            logger.error(ex);
            return false;
        }
        return true;
    }
}