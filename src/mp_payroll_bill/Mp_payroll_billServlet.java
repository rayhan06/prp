package mp_payroll_bill;

import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import com.google.gson.Gson;
import common.BaseServlet;
import election_details.Election_detailsDTO;
import election_details.Election_detailsRepository;
import election_wise_mp.Election_wise_mpDTO;
import election_wise_mp.Election_wise_mpRepository;
import login.LoginDTO;
import mp_payroll_allowance_configuration.Mp_payroll_allowance_configurationModel;
import mp_payroll_allowance_configuration.Mp_payroll_allowance_configurationRepository;
import mp_payroll_bill_allowance.Mp_payroll_bill_allowanceDAO;
import mp_payroll_bill_allowance.Mp_payroll_bill_allowanceDTO;
import mp_payroll_bill_deduction.Mp_payroll_bill_deductionDAO;
import mp_payroll_bill_deduction.Mp_payroll_bill_deductionDTO;
import mp_payroll_deduction_configuration.Mp_payroll_deduction_configurationModel;
import mp_payroll_deduction_configuration.Mp_payroll_deduction_configurationRepository;
import org.apache.log4j.Logger;
import pbReport.DateUtils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


@WebServlet("/Mp_payroll_billServlet")
@MultipartConfig
public class Mp_payroll_billServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Mp_payroll_billServlet.class);
    public static final String GRATUITY_ECONOMIC_CODE = "3257201";

    @Override
    public String getTableName() {
        return Mp_payroll_billDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Mp_payroll_billServlet";
    }

    @Override
    public Mp_payroll_billDAO getCommonDAOService() {
        return Mp_payroll_billDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.MP_PAYROLL_BILL_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.MP_PAYROLL_BILL_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.MP_PAYROLL_BILL_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Mp_payroll_billServlet.class;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "ajax_getMpPayrollData": {
                    long electionDetailsId = Long.parseLong(request.getParameter("electionDetailsId"));
                    long monthYear = new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("monthYear")).getTime();
                    String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
                    Map<String, Object> res = getMpPayrollData(electionDetailsId, monthYear, language);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().println(new Gson().toJson(res));
                    return;
                }
                case "generateBill": {
                    long iD = Long.parseLong(request.getParameter("ID"));
                    Mp_payroll_billDTO billDTO = Mp_payroll_billDAO.getInstance().getDTOByID(iD);
                    if (billDTO == null) {
                        throw new FileNotFoundException("no DTOs found");
                    }
                    request.setAttribute("payrollBillDTO", billDTO);
                    request.getRequestDispatcher("mp_payroll_bill/mp_payroll_billPreview.jsp").forward(request, response);
                    return;
                }
                case "ajax_getParliamentDateRange":{
                    long electionDetailsId = Long.parseLong(request.getParameter("electionDetailsId"));
                    Election_detailsDTO election_detailsDTO= Election_detailsRepository.getInstance().getElectionDetailsDTOByID(electionDetailsId);
                    Map<Object, Object> data = new HashMap<>();
                    data.put("startDate",DateUtils.get1stDayOfYear(election_detailsDTO.generralOathDate));
                    data.put("endDate",DateUtils.get1stDayOfNextYear(election_detailsDTO.parliamentLastDate)-1);
                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().println(new Gson().toJson(data));
                    return;
                }
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private Map<String, Object> getMpPayrollData(long electionDetailsId, long monthYear, String language) {
        List<Mp_payroll_billDTO> dtoList = Mp_payroll_billDAO.getInstance().getByElectionIdAndMonthYear(electionDetailsId, monthYear);
        List<Mp_payroll_billModel> billModels;
        boolean isAlreadyAdded = dtoList != null && !dtoList.isEmpty();
        List<Mp_payroll_allowance_configurationModel> allowanceModels = new ArrayList<>();
        List<Mp_payroll_deduction_configurationModel> deductionModels = new ArrayList<>();
        Map<String, Object> res = new HashMap<>();
        if (isAlreadyAdded) {
            billModels =
                    dtoList.stream()
                            .map(dto -> new Mp_payroll_billModel(dto, language))
                            .collect(Collectors.toList());
            if (billModels.size() > 0) {
                Mp_payroll_billModel model = billModels.get(0);
                allowanceModels = model.allowanceDTOList.stream()
                        .map(dto -> new Mp_payroll_allowance_configurationModel(dto, language))
                        .sorted(Comparator.comparing(dto -> dto.configurationId))
                        .collect(Collectors.toList());
                deductionModels = model.deductionDTOList.stream()
                        .map(dto -> new Mp_payroll_deduction_configurationModel(dto, language))
                        .sorted(Comparator.comparing(dto -> dto.configurationId))
                        .collect(Collectors.toList());
            }

        } else {
            List<AllowanceEmployeeInfoDTO> employeeInfoDTOs =
                    AllowanceEmployeeInfoRepository.getInstance().buildDTOsFromCacheForElectionId(electionDetailsId);

            billModels =
                    employeeInfoDTOs.stream()
                            .map(employeeInfoDTO -> new Mp_payroll_billModel(employeeInfoDTO, electionDetailsId, language))
                            .collect(Collectors.toList());
            allowanceModels = Mp_payroll_allowance_configurationRepository.getInstance()
                    .getActiveModels(language);
            deductionModels = Mp_payroll_deduction_configurationRepository.
                    getInstance().getActiveModels(language);
        }

        res.put("isAlreadyAdded", isAlreadyAdded);
        res.put("billModels", billModels);
        res.put("allowanceModels", allowanceModels);
        res.put("deductionModels", deductionModels);
        return res;
    }

    public void addMonthPayroll(UserInput userInput, long curTime) throws Exception {
        Mp_payroll_billDTO billDTO = new Mp_payroll_billDTO();

        billDTO.insertedBy = billDTO.modifiedBy = userInput.modifierId;
        billDTO.insertionTime = billDTO.lastModificationTime = curTime;

        billDTO.monthYear = userInput.monthYear;
        billDTO.electionDetailsId = userInput.electionDetailsId;

        Election_wise_mpDTO mpDTO = Election_wise_mpRepository.getInstance()
                .getByElectionAndRecordsId(userInput.electionDetailsId, userInput.employeeRecordsId);
        if (mpDTO != null) {
            billDTO.electionConstituencyId = mpDTO.electionConstituencyId;
        }
        AllowanceEmployeeInfoDTO employeeInfoDTO =
                AllowanceEmployeeInfoRepository.getInstance()
                        .getByEmployeeRecordId(userInput.employeeRecordsId);
        if (employeeInfoDTO == null) return;

        billDTO.allowanceEmployeeInfoId = employeeInfoDTO.iD;
        List<Mp_payroll_bill_allowanceDTO> allowanceDTOList = getAllowanceDTOList();
        billDTO.gratuity = allowanceDTOList.stream()
                .filter(dto -> dto.mpPayrollAllowanceConfigurationId == Mp_payroll_allowance_configurationRepository.getInstance().getGratuityConfigurationId())
                .map(dto -> dto.amount)
                .findAny()
                .orElse(0);
        billDTO.totalAllowance = allowanceDTOList.stream().mapToInt(dto -> dto.amount).sum();

        List<Mp_payroll_bill_deductionDTO> deductionDTOList = getDeductionDTOList();
        billDTO.totalDeduction = deductionDTOList.stream().mapToInt(dto -> dto.amount).sum();

        long mpPayrollBillId = Mp_payroll_billDAO.getInstance().add(billDTO);
        addMonthBillAllowance(allowanceDTOList, billDTO.lastModificationTime, billDTO.modifiedBy, mpPayrollBillId);
        addMonthBillDeduction(deductionDTOList, billDTO.lastModificationTime, billDTO.modifiedBy, mpPayrollBillId);
    }

    private void addMonthBillAllowance(List<Mp_payroll_bill_allowanceDTO> allowanceDTOList, long time, long modifiedId,
                                       long mpPayrollId) throws Exception {
        for (Mp_payroll_bill_allowanceDTO allowanceDTO : allowanceDTOList) {
            allowanceDTO.insertedBy = modifiedId;
            allowanceDTO.modifiedBy = modifiedId;
            allowanceDTO.insertionTime = time;
            allowanceDTO.lastModificationTime = time;
            allowanceDTO.mpPayrollBillId = mpPayrollId;
            Mp_payroll_bill_allowanceDAO.getInstance().add(allowanceDTO);
        }
    }

    private void addMonthBillDeduction(List<Mp_payroll_bill_deductionDTO> deductionDTOList, long time, long modifiedId,
                                       long mpPayrollId) throws Exception {
        for (Mp_payroll_bill_deductionDTO deductionDTO : deductionDTOList) {
            deductionDTO.insertedBy = modifiedId;
            deductionDTO.modifiedBy = modifiedId;
            deductionDTO.insertionTime = time;
            deductionDTO.lastModificationTime = time;
            deductionDTO.mpPayrollBillId = mpPayrollId;
            Mp_payroll_bill_deductionDAO.getInstance().add(deductionDTO);
        }
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Mp_payroll_billServlet.UserInput userInput = new Mp_payroll_billServlet.UserInput();
        userInput.electionDetailsId = Long.parseLong(request.getParameter("electionDetailsId"));
        userInput.monthYear = new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("monthYear")).getTime();
        if (addFlag) {
            List<Mp_payroll_billDTO> dtoList = Mp_payroll_billDAO.getInstance().getByElectionIdAndMonthYear(
                    userInput.electionDetailsId, userInput.monthYear
            );
            boolean isAlreadyAdded = dtoList != null && !dtoList.isEmpty();
            if (!isAlreadyAdded) {
                long curTime = System.currentTimeMillis();
                String[] employeeRecordsIds = request.getParameterValues("employeeRecordsId");
                userInput.modifierId = userDTO.ID;
                for (String employeeRecordsId : employeeRecordsIds) {
                    userInput.employeeRecordsId = Long.parseLong(employeeRecordsId);
                    if (userInput.employeeRecordsId < 0) continue;
                    addMonthPayroll(userInput, curTime);
                }
            }
        }
        Mp_payroll_billDTO billDTO = new Mp_payroll_billDTO();
        billDTO.monthYear = userInput.monthYear;
        billDTO.electionDetailsId = userInput.electionDetailsId;
        return billDTO;
    }

    public static List<Mp_payroll_bill_allowanceDTO> getAllowanceDTOList() {
        return Mp_payroll_allowance_configurationRepository.getInstance()
                .getAllDTOs()
                .stream()
                .map(dto -> new Mp_payroll_bill_allowanceDTO(dto.iD, dto.amount))
                .collect(Collectors.toList());
    }

    public static List<Mp_payroll_bill_deductionDTO> getDeductionDTOList() {
        return Mp_payroll_deduction_configurationRepository.getInstance()
                .getAllDTOs()
                .stream()
                .map(dto -> new Mp_payroll_bill_deductionDTO(dto.iD, dto.amount))
                .collect(Collectors.toList());
    }

    static class UserInput {
        public long modifierId;
        public long monthYear;
        public long id;
        public long employeeRecordsId;
        public long electionDetailsId;
    }
}