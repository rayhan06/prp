package mp_payroll_bill;

import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static util.StringUtils.convertToBanNumber;

@SuppressWarnings({"Duplicates"})
public class Mp_payroll_billDAO implements CommonDAOService<Mp_payroll_billDTO> {
    private static final Logger logger = Logger.getLogger(Mp_payroll_billDAO.class);


    private static final String addQuery = "INSERT INTO {tableName} (allowance_employee_info_id,election_details_id,election_constituency_id,month_year,"
            .concat("gratuity,total_allowance,total_deduction,modified_by,lastModificationTime,search_column,")
            .concat("inserted_by,insertion_time,isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    private static final String updateQuery = "UPDATE {tableName} SET allowance_employee_info_id=?,election_details_id=?,"
            .concat("election_constituency_id=?,month_year=?,gratuity=?,total_allowance=?,total_deduction=?,modified_by=?,")
            .concat("lastModificationTime=?,search_column=? WHERE ID=?");

    private static final String getByElectionIdAndMonthYear = "SELECT * FROM mp_payroll_bill WHERE election_details_id=%d AND month_year=%d AND isDeleted=0";

    private static final String enumerationReportQuery = "SELECT * FROM mp_payroll_bill WHERE election_details_id=%d AND election_constituency_id=%d AND "
            .concat("month_year>=%d AND month_year<=%d AND isDeleted=0");

    private final Map<String, String> searchMap = new HashMap<>();

    private Mp_payroll_billDAO() {

        searchMap.put("monthYear", " AND (month_year = ?) ");
        searchMap.put("electionDetailsId", " AND (election_details_id = ?) ");
        searchMap.put("electionConstituencyId", " AND (election_constituency_id = ?) ");
        searchMap.put("AnyField", " and (search_column like ?)");
    }


    private static class LazyLoader {
        static final Mp_payroll_billDAO INSTANCE = new Mp_payroll_billDAO();
    }

    public static Mp_payroll_billDAO getInstance() {
        return Mp_payroll_billDAO.LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Mp_payroll_billDTO payroll_billDTO) {
        AllowanceEmployeeInfoDTO allowanceEmployeeInfoDTO =
                AllowanceEmployeeInfoRepository.getInstance().getById(payroll_billDTO.allowanceEmployeeInfoId);
        payroll_billDTO.searchColumn = "";
        payroll_billDTO.searchColumn =
                allowanceEmployeeInfoDTO.nameEn + " " + allowanceEmployeeInfoDTO.nameBn + " "
                        + allowanceEmployeeInfoDTO.mobileNumber + " "
                        + convertToBanNumber(allowanceEmployeeInfoDTO.mobileNumber) + " "
                        + allowanceEmployeeInfoDTO.savingAccountNumber + " "
                        + convertToBanNumber(allowanceEmployeeInfoDTO.savingAccountNumber);
    }

    @Override
    public void set(PreparedStatement ps, Mp_payroll_billDTO mp_payroll_billDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(mp_payroll_billDTO);
        ps.setObject(++index, mp_payroll_billDTO.allowanceEmployeeInfoId);
        ps.setObject(++index, mp_payroll_billDTO.electionDetailsId);
        ps.setObject(++index, mp_payroll_billDTO.electionConstituencyId);
        ps.setObject(++index, mp_payroll_billDTO.monthYear);
        ps.setObject(++index, mp_payroll_billDTO.gratuity);
        ps.setObject(++index, mp_payroll_billDTO.totalAllowance);
        ps.setObject(++index, mp_payroll_billDTO.totalDeduction);
        ps.setObject(++index, mp_payroll_billDTO.modifiedBy);
        ps.setObject(++index, mp_payroll_billDTO.lastModificationTime);
        ps.setObject(++index, mp_payroll_billDTO.searchColumn);
        if (isInsert) {
            ps.setObject(++index, mp_payroll_billDTO.insertedBy);
            ps.setObject(++index, mp_payroll_billDTO.insertionTime);
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, mp_payroll_billDTO.iD);
    }

    @Override
    public Mp_payroll_billDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Mp_payroll_billDTO mp_payroll_billDTO = new Mp_payroll_billDTO();
            mp_payroll_billDTO.iD = rs.getLong("ID");
            mp_payroll_billDTO.allowanceEmployeeInfoId = rs.getLong("allowance_employee_info_id");
            mp_payroll_billDTO.electionDetailsId = rs.getLong("election_details_id");
            mp_payroll_billDTO.electionConstituencyId = rs.getLong("election_constituency_id");
            mp_payroll_billDTO.monthYear = rs.getLong("month_year");
            mp_payroll_billDTO.gratuity = rs.getInt("gratuity");
            mp_payroll_billDTO.totalAllowance = rs.getInt("total_allowance");
            mp_payroll_billDTO.totalDeduction = rs.getInt("total_deduction");
            mp_payroll_billDTO.insertedBy = rs.getLong("inserted_by");
            mp_payroll_billDTO.insertionTime = rs.getLong("insertion_time");
            mp_payroll_billDTO.isDeleted = rs.getInt("isDeleted");
            mp_payroll_billDTO.modifiedBy = rs.getLong("modified_by");
            mp_payroll_billDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return mp_payroll_billDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Mp_payroll_billDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "mp_payroll_bill";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Mp_payroll_billDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Mp_payroll_billDTO) commonDTO, updateQuery, false);
    }

    public List<Mp_payroll_billDTO> getByElectionIdAndMonthYear(long electionDetailsId, long monthYear) {
        return getDTOs(String.format(getByElectionIdAndMonthYear, electionDetailsId, monthYear));
    }

    public Mp_payroll_billDTO getSummaryDTO(long electionDetailsId, long electionConstituencyId, long startDate, long endDate) {
        List<Mp_payroll_billDTO> dtoList = getDTOs(String.format(enumerationReportQuery, electionDetailsId, electionConstituencyId, startDate, endDate));
        if (dtoList == null || dtoList.size() == 0)
            return null;
        int totalGratuity = dtoList.stream().mapToInt(dto -> dto.gratuity).sum();
        int totalAllowance = dtoList.stream().mapToInt(dto -> dto.totalAllowance).sum();
        totalAllowance = totalAllowance - totalGratuity;
        Mp_payroll_billDTO billDTO = dtoList.get(0);
        billDTO.totalAllowance = totalAllowance;
        billDTO.gratuity = totalGratuity;
        return billDTO;
    }

}
