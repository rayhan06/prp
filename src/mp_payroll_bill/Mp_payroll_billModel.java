package mp_payroll_bill;

import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import election_constituency.Election_constituencyRepository;
import election_details.Election_detailsRepository;
import election_wise_mp.Election_wise_mpDTO;
import election_wise_mp.Election_wise_mpRepository;
import mp_payroll_bill_allowance.Mp_payroll_bill_allowanceDAO;
import mp_payroll_bill_allowance.Mp_payroll_bill_allowanceDTO;
import mp_payroll_bill_deduction.Mp_payroll_bill_deductionDAO;
import mp_payroll_bill_deduction.Mp_payroll_bill_deductionDTO;
import pb.Utils;
import util.StringUtils;

import java.util.List;

import static util.UtilCharacter.getDataByLanguage;

public class Mp_payroll_billModel {
    public String employeeRecordsId = "-1";
    public String mpPayrollBillId = "-1";
    public String name = "";
    public String mobileNumber = "";
    public String savingAccountNumber = "";
    public List<Mp_payroll_bill_allowanceDTO> allowanceDTOList;
    public List<Mp_payroll_bill_deductionDTO> deductionDTOList;
    public String totalAllowance = "";
    public String totalDeduction = "";
    public int totalAllowanceInt = 0;
    public int totalDeductionInt = 0;
    public String netAmount = "";
    public String electionConstituencyName = "";
    public String electionConstituencyId = "-1";
    public String parliamentNumber = "";

    public Mp_payroll_billModel(AllowanceEmployeeInfoDTO employeeInfoDTO, long electionDetailsId, String language) {
        setEmployeeInfo(this, employeeInfoDTO, electionDetailsId, language);
        allowanceDTOList = Mp_payroll_billServlet.getAllowanceDTOList();
        deductionDTOList = Mp_payroll_billServlet.getDeductionDTOList();
        totalAllowanceInt = allowanceDTOList.stream().mapToInt(dto -> dto.amount).sum();
        totalAllowance = Utils.getDigits(totalAllowanceInt, language);
        totalDeductionInt = deductionDTOList.stream().mapToInt(dto -> dto.amount).sum();
        totalDeduction = Utils.getDigits(totalDeductionInt, language);
        netAmount = String.valueOf(totalAllowanceInt - totalDeductionInt);
        netAmount = Utils.getDigits(netAmount, language);
    }

    public Mp_payroll_billModel(Mp_payroll_billDTO billDTO, String language) {
        AllowanceEmployeeInfoDTO allowanceEmployeeInfoDTO = AllowanceEmployeeInfoRepository.getInstance().getById(billDTO.allowanceEmployeeInfoId);
        setEmployeeInfo(this, allowanceEmployeeInfoDTO, billDTO.electionDetailsId, language);
        setAllowanceInfo(this, billDTO, language);
    }

    private static void setEmployeeInfo(Mp_payroll_billModel model, AllowanceEmployeeInfoDTO employeeInfoDTO, Long electionDetailsId, String language) {
        model.employeeRecordsId = String.valueOf(employeeInfoDTO.employeeRecordId);
        model.name = getDataByLanguage(language, employeeInfoDTO.nameBn, employeeInfoDTO.nameEn);
        model.mobileNumber = StringUtils.convertBanglaIfLanguageIsBangla(language, employeeInfoDTO.mobileNumber);
        model.savingAccountNumber = StringUtils.convertBanglaIfLanguageIsBangla(language, employeeInfoDTO.savingAccountNumber);
        model.parliamentNumber = String.valueOf(Election_detailsRepository.getInstance().getElectionDetailsDTOByID(electionDetailsId).parliamentNumber);
        Election_wise_mpDTO mpDTO = Election_wise_mpRepository.getInstance().getByElectionAndRecordsId(electionDetailsId, Long.parseLong(model.employeeRecordsId));
        if (mpDTO != null) {
            model.electionConstituencyId = String.valueOf(mpDTO.electionConstituencyId);
            model.electionConstituencyName = Election_constituencyRepository.getInstance().getText(mpDTO.electionConstituencyId, language);
        }
    }

    private static void setAllowanceInfo(Mp_payroll_billModel model, Mp_payroll_billDTO billDTO, String language) {
        model.mpPayrollBillId = String.valueOf(billDTO.iD);
        model.allowanceDTOList = Mp_payroll_bill_allowanceDAO.getInstance().getByMpBillId(billDTO.iD);
        model.deductionDTOList = Mp_payroll_bill_deductionDAO.getInstance().getByMpBillId(billDTO.iD);
        model.totalAllowanceInt = model.allowanceDTOList.stream().mapToInt(dto -> dto.amount).sum();
        model.totalAllowance = model.totalAllowanceInt < 0 ? "" : String.valueOf(model.totalAllowanceInt);
        model.totalAllowance = Utils.getDigits(model.totalAllowance, language);
        model.totalDeductionInt = model.deductionDTOList.stream().mapToInt(dto -> dto.amount).sum();
        model.totalDeduction = model.totalDeductionInt < 0 ? " " : String.valueOf(model.totalDeductionInt);
        model.totalDeduction = Utils.getDigits(model.totalDeduction, language);
        int netAmountInt = model.totalAllowanceInt - model.totalDeductionInt;
        model.netAmount = netAmountInt < 0 ? "" : String.valueOf(netAmountInt);
        model.netAmount = Utils.getDigits(model.netAmount, language);
    }
}
