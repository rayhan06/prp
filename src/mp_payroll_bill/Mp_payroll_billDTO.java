package mp_payroll_bill;

import util.CommonDTO;


public class Mp_payroll_billDTO extends CommonDTO {

    public long allowanceEmployeeInfoId = -1;
    public long electionDetailsId = -1;
    public long electionConstituencyId = -1;
    public long monthYear = -1;
    public int gratuity=0;
    public int totalAllowance=0;
    public int totalDeduction=0;
    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;


    @Override
    public String toString() {
        return "$Mp_payroll_billDTO[" +
                " iD = " + iD +
                " allowanceEmployeeInfoId = " + allowanceEmployeeInfoId +
                " electionDetailsId = " + electionDetailsId +
                " electionConstituencyId = " + electionConstituencyId +
                " monthYear = " + monthYear +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " isDeleted = " + isDeleted +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}