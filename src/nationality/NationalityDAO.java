package nationality;

import common.NameDao;
public class NationalityDAO extends NameDao {

    private NationalityDAO() {
        super("nationality");
    }

    private static class LazyLoader{
        static final NationalityDAO INSTANCE = new NationalityDAO();
    }

    public static NationalityDAO getInstance(){
        return LazyLoader.INSTANCE;
    }
}
	