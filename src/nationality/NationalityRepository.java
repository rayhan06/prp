package nationality;

import common.NameRepository;

public class NationalityRepository extends NameRepository {
    private NationalityRepository() {
        super(NationalityDAO.getInstance(), NationalityRepository.class);
    }

    private static class LazyLoader {
        static NationalityRepository INSTANCE = new NationalityRepository();
    }

    public synchronized static NationalityRepository getInstance() {
        return LazyLoader.INSTANCE;
    }
}


