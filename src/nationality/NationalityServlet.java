package nationality;

import common.BaseServlet;
import common.NameDao;
import common.NameRepository;
import common.NameInterface;
import language.LC;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

/**
 * Servlet implementation class NationalityServlet
 */
@WebServlet("/NationalityServlet")
@MultipartConfig
public class NationalityServlet extends BaseServlet implements NameInterface {

	public String commonPartOfDispatchURL(){
		return  "common/name";
	}

	@Override
	public String getTableName() {
		return NationalityDAO.getInstance().getTableName();
	}

	@Override
	public String getServletName() {
		return "NationalityServlet";
	}

	@Override
	public NameDao getCommonDAOService() {
		return NationalityDAO.getInstance();
	}

	@Override
	public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
		return addT(request,addFlag,userDTO,NationalityDAO.getInstance());
	}

	@Override
	public int[] getAddPageMenuConstants() {
		return new int[] {MenuConstants.NATIONALITY_ADD};
	}

	@Override
	public int[] getEditPageMenuConstants() {
		return new int[] {MenuConstants.NATIONALITY_UPDATE};
	}

	@Override
	public int[] getSearchMenuConstants() {
		return new int[] {MenuConstants.NATIONALITY_SEARCH};
	}

	@Override
	public Class<? extends HttpServlet> getClazz() {
		return NationalityServlet.class;
	}

	@Override
	public int getSearchTitleValue() {
		return LC.NATIONALITY_SEARCH_NATIONALITY_SEARCH_FORMNAME;
	}

	@Override
	public String get_p_navigatorName() {
		return SessionConstants.NAV_NATIONALITY;
	}

	@Override
	public String get_p_dtoCollectionName() {
		return SessionConstants.VIEW_NATIONALITY;
	}

	@Override
	public NameRepository getNameRepository() {
		return NationalityRepository.getInstance();
	}

	@Override
	public int getAddTitleValue() {
		return LC.NATIONALITY_ADD_NATIONALITY_ADD_FORMNAME;
	}

	@Override
	public int getEditTitleValue() {
		return LC.NATIONALITY_EDIT_NATIONALITY_EDIT_FORMNAME;
	}

	@Override
	public void init(HttpServletRequest request) throws ServletException {
		setCommonAttributes(request,getServletName(),getCommonDAOService());
	}
}