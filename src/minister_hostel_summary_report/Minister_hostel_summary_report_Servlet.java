package minister_hostel_summary_report;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import am_minister_hostel_block.AmMinisterHostelSideRepository;
import am_minister_hostel_block.Am_minister_hostel_blockRepository;
import am_minister_hostel_level.Am_minister_hostel_levelRepository;
import am_minister_hostel_unit.Am_minister_hostel_unitRepository;
import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;
import util.UtilCharacter;


@WebServlet("/Minister_hostel_summary_report_Servlet")
public class Minister_hostel_summary_report_Servlet  extends HttpServlet {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","tr","am_minister_hostel_block_id","=","","int","","","any","amMinisterHostelBlockId", LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AMMINISTERHOSTELBLOCKTYPE + ""},
		{"criteria","tr","am_minister_hostel_side_id","=","AND","int","","","any","amMinisterHostelSideId", LC.AM_MINISTER_HOSTEL_UNIT_ADD_AMMINISTERHOSTELSIDEID + ""},
		{"criteria","tr","am_minister_hostel_unit_id","=","AND","int","","","any","amMinisterHostelUnitId", LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AMMINISTERHOSTELUNITID + ""},
		{"criteria","tr","am_minister_hostel_level_cat","=","AND","int","","","any","amMinisterHostelLevelCat", LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AMMINISTERHOSTELLEVELCAT + ""},
		{"criteria","tr","status","=","AND","int","","","any","status", LC.PI_REQUISITION_ADD_STATUS + ""},
		{"criteria","tr","isDeleted","=","AND","String","","","0","isDeleted", LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_WHERE_ISDELETED + ""}
	};

	String[][] Display =
	{
		{"display","tr","am_minister_hostel_block_id","cacheDataByDtoAttribute","","am_minister_hostel_block,blockNo"},
		{"display","tr","am_minister_hostel_side_id","catDataByDtoAttribute","","am_minister_hostel_side,am_minister_hostel_side,ministerHostelSideCat"},
		{"display","tr","am_minister_hostel_unit_id","cacheDataByDtoAttribute","","am_minister_hostel_unit,unitNumber"},
		{"display","tr","am_minister_hostel_level_cat","cat","am_minister_hostel_level"},
		{"display","tr","status","commonApprovalStatus",""}
	};

	String GroupBy = "";
	String OrderBY = "";

	ReportRequestHandler reportRequestHandler;

	public Minister_hostel_summary_report_Servlet(){

	}

	private final ReportService reportService = new ReportService();

	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		Am_minister_hostel_blockRepository.getInstance();
		AmMinisterHostelSideRepository.getInstance();
		Am_minister_hostel_unitRepository.getInstance();
		Am_minister_hostel_levelRepository.getInstance();

		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}



		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);

		sql = "am_minister_hostel_level tr ";


		List<String[]> l = new ArrayList<String[]>(Arrays.asList(Display));
		Display = l.toArray(new String[][]{});

		Display[0][4] = LM.getText(LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AMMINISTERHOSTELBLOCKTYPE, loginDTO);
		Display[1][4] = LM.getText(LC.AM_MINISTER_HOSTEL_UNIT_ADD_AMMINISTERHOSTELSIDEID, loginDTO);
		Display[2][4] = LM.getText(LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AMMINISTERHOSTELUNITID, loginDTO);
		Display[3][4] = LM.getText(LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AMMINISTERHOSTELLEVELCAT, loginDTO);
		Display[4][4] = UtilCharacter.getDataByLanguage(language, "অবস্থা", "Status");

		String reportName = UtilCharacter.getDataByLanguage(language, "মন্ত্রী হোস্টেল বরাদ্দের রিপোর্ট", "Minister Hostel Allocation Report");

		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);


		reportRequestHandler.handleReportGet(request, response, userDTO, "minister_hostel_summary_report",
				MenuConstants.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_DETAILS, language, reportName, "minister_hostel_summary_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doGet(request, response);
	}
}
