package finance;

import util.UtilCharacter;

public enum CashTypeEnum {
    SALARY(1, "Salary", "বেতন"),
    ALLOWANCE(2, "Allowance", "ভাতাদি"),
    OTHERS(3, "Miscellaneous", "বিবিধ");

    private final int value;
    private final String nameEn, nameBn;

    CashTypeEnum(int value, String nameEn, String nameBn) {
        this.value = value;
        this.nameEn = nameEn;
        this.nameBn = nameBn;
    }

    public int getValue() {
        return value;
    }

    public String getText(String language) {
        return UtilCharacter.getDataByLanguage(language, nameBn, nameEn);
    }
}
