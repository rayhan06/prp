package finance;

import util.UtilCharacter;

public enum CashStatusEnum {
    ACQUISITION(1, "Acquisition", "প্রাপ্তি"),
    PAYMENT(2, "Payment", "প্রদান");

    private final int value;
    private final String nameEn, nameBn;

    CashStatusEnum(int value, String nameEn, String nameBn) {
        this.value = value;
        this.nameEn = nameEn;
        this.nameBn = nameBn;
    }

    public int getValue() {
        return value;
    }

    public String getText(String language) {
        return UtilCharacter.getDataByLanguage(language, nameBn, nameEn);
    }
}
