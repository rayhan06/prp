package finance;

import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsRepository;
import util.UtilCharacter;

public class FinanceUtil {
    public static long FINANCE_1_OFFICE_UNIT_ID = 30L;
    public static long FINANCE_1_HEAD_ORGANOGRAM_ID = 261781L;
    public static long FINANCE_1_BUDGET_OFFICE_ID = 1L;

    public static String getFinance1HeadDesignation(String language) {
        String designation = OfficeUnitOrganogramsRepository.getInstance()
                                                            .getDesignation(language, FINANCE_1_HEAD_ORGANOGRAM_ID);
        String office = Office_unitsRepository.getInstance()
                                              .geText(language, FINANCE_1_OFFICE_UNIT_ID);

        if (designation.isEmpty() || office.isEmpty()) return "";

        return designation + " (" + office + ")";
    }

    public static String getFinance1HeadName(String language) {
        Employee_recordsDTO finance1HeadDTO = Employee_recordsRepository.getInstance().getDTOByOrganogramId(FINANCE_1_HEAD_ORGANOGRAM_ID);
        if(finance1HeadDTO == null) return "";
        return UtilCharacter.getDataByLanguage(language, finance1HeadDTO.nameBng, finance1HeadDTO.nameEng);
    }

    public static Employee_recordsDTO getFinance1HeadEmployeeRecords() {
        return Employee_recordsRepository.getInstance().getDTOByOrganogramId(FINANCE_1_HEAD_ORGANOGRAM_ID);
    }

    public static String getFinance1HeadIssuedToName(String language) {
        return getFinance1HeadDesignation(language).concat(UtilCharacter.getDataByLanguage(
                language,
                ", বাংলাদেশ জাতীয় সংসদ সচিবালয়, ঢাকা",
                ", Bangladesh National Parliament Secretariat, Dhaka"
        ));
    }

    public static void main(String[] args) {
        System.out.println(
                getFinance1HeadName("bangla") + "\n" +
                getFinance1HeadDesignation("bangla")
        );
    }
}
