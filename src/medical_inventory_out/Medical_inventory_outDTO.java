package medical_inventory_out;
import java.util.*;

import department_requisition_lot.DepartmentRequisitionItemDTO;
import department_requisition_lot.Department_requisition_lotDTO;
import drug_information.Drug_informationDTO;
import medical_equipment_name.Medical_equipment_nameDAO;
import medical_equipment_name.Medical_equipment_nameDTO;
import medical_inventory_lot.MedicalInventoryInDAO;
import medical_inventory_lot.MedicalInventoryInDTO;
import office_unit_organograms.OfficeUnitOrganogramsRepository;

import office_units.Organization;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;
import workflow.WorkflowController; 


public class Medical_inventory_outDTO extends CommonDTO
{

	public long medicalItemCat = 0;
	public long drugInformationId = 0;
	public long medicalReagentNameId = 0;
	public long medicalEquipmentNameId = 0;
	public int stockOutQuantity = 0;
	public double unitPrice = 0;
	public long transactionDate = 0;
    public String remarks = "";
	public long insertedByUserId = 0;
	public long insertedByOrganogramId = 0;
	public long insertionDate = 0;
	public long expiryDate = 0;
	public long medicalInventoryInId = 0;
	public int transactionType = MedicalInventoryInDTO.TR_RECEIVE_MEDICINE;
	public long organogramId = -1;
	public String userName = "";
	public long transactionTime = System.currentTimeMillis();
	public String patientName;
	public String phone;
	
	public long prescriptionLabTestId = -1;
	public int medicalEquipmentCat = -1;
	
	public long prescriptionDetailsId = -1;
	public long ppmId = -1;
	
	public long organizationId = -1;
	public String organizationNameEn;
	public String organizationNameBn;
	public int medicalDeptCat = -1;
	
	public long otherOfficeId = -1;
	
	public long employeeRecordId = -1;
	MedicalInventoryInDAO medicalInventoryInDAO = new MedicalInventoryInDAO();
	Medical_equipment_nameDAO medical_equipment_nameDAO = new Medical_equipment_nameDAO();
	
	public Medical_inventory_outDTO()
	{
		
	}
	
	public Medical_inventory_outDTO(Department_requisition_lotDTO department_requisition_lotDTO, DepartmentRequisitionItemDTO deepartmentRequisitionItemDTO,
			Drug_informationDTO drug_informationDTO, UserDTO userDTO)
	{
		this.transactionType = MedicalInventoryInDTO.TR_BORROW;
		drugInformationId = drug_informationDTO.iD;
		unitPrice = drug_informationDTO.unitPrice;
		stockOutQuantity = deepartmentRequisitionItemDTO.quantity;
		medicalDeptCat = department_requisition_lotDTO.medicalDeptCat;
		transactionDate = TimeConverter.getToday();
		insertionDate = transactionDate;
		if(userDTO != null)
		{
			insertedByUserId = userDTO.ID;
			insertedByOrganogramId = userDTO.organogramID;	
			employeeRecordId = userDTO.employee_record_id;
			
			userName = userDTO.userName;
			organogramId = userDTO.organogramID;
			employeeRecordId = userDTO.employee_record_id;
		}
	}
	
	public Medical_inventory_outDTO(long equipmentId, int quantity, 
			UserDTO toUserDTO, long prescriptionLabTestId, int transactionType, UserDTO insertedByUserDTO)
	{
		medicalItemCat= SessionConstants.MEDICAL_ITEM_EQUIPMENT;
		this.transactionType = transactionType;
		medicalEquipmentNameId = equipmentId;
		this.stockOutQuantity = quantity;
		Medical_equipment_nameDTO medical_equipment_nameDTO = medical_equipment_nameDAO.getDTOByID(medicalEquipmentNameId);
		this.prescriptionLabTestId = prescriptionLabTestId;
		if(medical_equipment_nameDTO != null)
		{
			unitPrice = medical_equipment_nameDTO.unitPrice;
			this.medicalEquipmentCat = medical_equipment_nameDTO.medicalEquipmentCat;
		}
		if(toUserDTO != null)
		{
			insertedByUserId = insertedByUserDTO.ID;
			insertedByOrganogramId = insertedByUserDTO.organogramID;
			userName = toUserDTO.userName;
			organogramId = toUserDTO.organogramID;
			employeeRecordId = toUserDTO.employee_record_id;
			this.patientName = WorkflowController.getNameFromUserName(userName, "english");
			if(organogramId == -1)
			{
				UserDTO dtoWithOrg = UserRepository.getUserDTOByUserName(userName);
				if(dtoWithOrg != null)
				{
					organogramId = dtoWithOrg.organogramID;
					Organization organization = OfficeUnitOrganogramsRepository.getInstance().getOrganizationByOrganogramId(organogramId);
					if(organization != null)
					{
						organizationId = organization.getId();
						organizationNameEn = organization.getNameEn();
						organizationNameBn = organization.getNameBn();
					}
				}
			}
			
		}
		transactionDate = TimeConverter.getToday();
		insertionDate = transactionDate;
	}
	
	public Medical_inventory_outDTO(Drug_informationDTO drug_informationDTO, int quantity,
			UserDTO userDTO, int transactionType, UserDTO transactionToDTO, int medicalItemType, String patientName, String phone,
			long prescriptionDetailsId, long ppmId)
	{
		this(drug_informationDTO, quantity,
				userDTO, transactionType, transactionToDTO, medicalItemType, patientName, phone, TimeConverter.getToday(),
				prescriptionDetailsId, ppmId);
	}
	
	public Medical_inventory_outDTO(Drug_informationDTO drug_informationDTO, int quantity,
			UserDTO userDTO, int transactionType, UserDTO transactionToDTO, int medicalItemType, String patientName,
			String phone, long transactionDate, long prescriptionDetailsId, long ppmId)
	{
		medicalItemCat = medicalItemType;
		drugInformationId = drug_informationDTO.iD;
		stockOutQuantity = quantity;
		this.transactionDate = transactionDate;
		this.transactionType = transactionType;
		this.patientName = patientName;
		this.phone = phone;
		this.prescriptionDetailsId = prescriptionDetailsId;
		this.ppmId = ppmId;
		
		MedicalInventoryInDTO medicalInventoryInDTO = medicalInventoryInDAO.getLastMedicineEntry(drugInformationId);
		if(medicalInventoryInDTO != null)
		{
			expiryDate = medicalInventoryInDTO.stockLotExpiryDate;
			medicalInventoryInId = medicalInventoryInDTO.iD;
			unitPrice = drug_informationDTO.unitPrice;
		}
		if(userDTO != null)
		{
			insertedByUserId = userDTO.ID;
			insertedByOrganogramId = userDTO.organogramID;
			
		}
		if(transactionToDTO != null)
		{
			userName = transactionToDTO.userName;
			organogramId = transactionToDTO.organogramID;
			employeeRecordId = transactionToDTO.employee_record_id;
			if(organogramId == -1)
			{
				UserDTO dtoWithOrg = UserRepository.getUserDTOByUserName(userName);
				if(dtoWithOrg != null)
				{
					organogramId = dtoWithOrg.organogramID;
					Organization organization = OfficeUnitOrganogramsRepository.getInstance().getOrganizationByOrganogramId(organogramId);
					if(organization != null)
					{
						organizationId = organization.getId();
						organizationNameEn = organization.getNameEn();
						organizationNameBn = organization.getNameBn();
					}
				}
			}
			
		}
		
		
		insertionDate = transactionDate;
	}

	
	
    @Override
	public String toString() {
            return "$Medical_inventory_outDTO[" +
            " iD = " + iD +
            " medicalItemCat = " + medicalItemCat +
            " drugInformationId = " + drugInformationId +
            " medicalReagentNameId = " + medicalReagentNameId +
            " medicalEquipmentNameId = " + medicalEquipmentNameId +
            " stockOutQuantity = " + stockOutQuantity +
            " unitPrice = " + unitPrice +
            " transactionDate = " + transactionDate +
            " remarks = " + remarks +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " expiryDate = " + expiryDate +
            " medicalInventoryInId = " + medicalInventoryInId +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}