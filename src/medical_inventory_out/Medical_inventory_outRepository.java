package medical_inventory_out;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Medical_inventory_outRepository implements Repository {
	Medical_inventory_outDAO medical_inventory_outDAO = null;
	
	public void setDAO(Medical_inventory_outDAO medical_inventory_outDAO)
	{
		this.medical_inventory_outDAO = medical_inventory_outDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Medical_inventory_outRepository.class);
	Map<Long, Medical_inventory_outDTO>mapOfMedical_inventory_outDTOToiD;
	Map<Long, Set<Medical_inventory_outDTO> >mapOfMedical_inventory_outDTOTomedicalItemCat;
	Map<Long, Set<Medical_inventory_outDTO> >mapOfMedical_inventory_outDTOTodrugInformationId;
	Map<Long, Set<Medical_inventory_outDTO> >mapOfMedical_inventory_outDTOTomedicalReagentNameId;
	Map<Long, Set<Medical_inventory_outDTO> >mapOfMedical_inventory_outDTOTomedicalEquipmentNameId;
	Map<Integer, Set<Medical_inventory_outDTO> >mapOfMedical_inventory_outDTOTostockOutQuantity;
	Map<Double, Set<Medical_inventory_outDTO> >mapOfMedical_inventory_outDTOTounitPrice;
	Map<Long, Set<Medical_inventory_outDTO> >mapOfMedical_inventory_outDTOTotransactionDate;
	Map<String, Set<Medical_inventory_outDTO> >mapOfMedical_inventory_outDTOToremarks;
	Map<Long, Set<Medical_inventory_outDTO> >mapOfMedical_inventory_outDTOToinsertedByUserId;
	Map<Long, Set<Medical_inventory_outDTO> >mapOfMedical_inventory_outDTOToinsertedByOrganogramId;
	Map<Long, Set<Medical_inventory_outDTO> >mapOfMedical_inventory_outDTOToinsertionDate;
	Map<Long, Set<Medical_inventory_outDTO> >mapOfMedical_inventory_outDTOToexpiryDate;
	Map<Long, Set<Medical_inventory_outDTO> >mapOfMedical_inventory_outDTOTomedicalInventoryInId;
	Map<Long, Set<Medical_inventory_outDTO> >mapOfMedical_inventory_outDTOTolastModificationTime;


	static Medical_inventory_outRepository instance = null;  
	private Medical_inventory_outRepository(){
		mapOfMedical_inventory_outDTOToiD = new ConcurrentHashMap<>();
		mapOfMedical_inventory_outDTOTomedicalItemCat = new ConcurrentHashMap<>();
		mapOfMedical_inventory_outDTOTodrugInformationId = new ConcurrentHashMap<>();
		mapOfMedical_inventory_outDTOTomedicalReagentNameId = new ConcurrentHashMap<>();
		mapOfMedical_inventory_outDTOTomedicalEquipmentNameId = new ConcurrentHashMap<>();
		mapOfMedical_inventory_outDTOTostockOutQuantity = new ConcurrentHashMap<>();
		mapOfMedical_inventory_outDTOTounitPrice = new ConcurrentHashMap<>();
		mapOfMedical_inventory_outDTOTotransactionDate = new ConcurrentHashMap<>();
		mapOfMedical_inventory_outDTOToremarks = new ConcurrentHashMap<>();
		mapOfMedical_inventory_outDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfMedical_inventory_outDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfMedical_inventory_outDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfMedical_inventory_outDTOToexpiryDate = new ConcurrentHashMap<>();
		mapOfMedical_inventory_outDTOTomedicalInventoryInId = new ConcurrentHashMap<>();
		mapOfMedical_inventory_outDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Medical_inventory_outRepository getInstance(){
		if (instance == null){
			instance = new Medical_inventory_outRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(medical_inventory_outDAO == null)
		{
			return;
		}
		try {
			List<Medical_inventory_outDTO> medical_inventory_outDTOs = medical_inventory_outDAO.getAllMedical_inventory_out(reloadAll);
			for(Medical_inventory_outDTO medical_inventory_outDTO : medical_inventory_outDTOs) {
				Medical_inventory_outDTO oldMedical_inventory_outDTO = getMedical_inventory_outDTOByID(medical_inventory_outDTO.iD);
				if( oldMedical_inventory_outDTO != null ) {
					mapOfMedical_inventory_outDTOToiD.remove(oldMedical_inventory_outDTO.iD);
				
					if(mapOfMedical_inventory_outDTOTomedicalItemCat.containsKey(oldMedical_inventory_outDTO.medicalItemCat)) {
						mapOfMedical_inventory_outDTOTomedicalItemCat.get(oldMedical_inventory_outDTO.medicalItemCat).remove(oldMedical_inventory_outDTO);
					}
					if(mapOfMedical_inventory_outDTOTomedicalItemCat.get(oldMedical_inventory_outDTO.medicalItemCat).isEmpty()) {
						mapOfMedical_inventory_outDTOTomedicalItemCat.remove(oldMedical_inventory_outDTO.medicalItemCat);
					}
					
					if(mapOfMedical_inventory_outDTOTodrugInformationId.containsKey(oldMedical_inventory_outDTO.drugInformationId)) {
						mapOfMedical_inventory_outDTOTodrugInformationId.get(oldMedical_inventory_outDTO.drugInformationId).remove(oldMedical_inventory_outDTO);
					}
					if(mapOfMedical_inventory_outDTOTodrugInformationId.get(oldMedical_inventory_outDTO.drugInformationId).isEmpty()) {
						mapOfMedical_inventory_outDTOTodrugInformationId.remove(oldMedical_inventory_outDTO.drugInformationId);
					}
					
					if(mapOfMedical_inventory_outDTOTomedicalReagentNameId.containsKey(oldMedical_inventory_outDTO.medicalReagentNameId)) {
						mapOfMedical_inventory_outDTOTomedicalReagentNameId.get(oldMedical_inventory_outDTO.medicalReagentNameId).remove(oldMedical_inventory_outDTO);
					}
					if(mapOfMedical_inventory_outDTOTomedicalReagentNameId.get(oldMedical_inventory_outDTO.medicalReagentNameId).isEmpty()) {
						mapOfMedical_inventory_outDTOTomedicalReagentNameId.remove(oldMedical_inventory_outDTO.medicalReagentNameId);
					}
					
					if(mapOfMedical_inventory_outDTOTomedicalEquipmentNameId.containsKey(oldMedical_inventory_outDTO.medicalEquipmentNameId)) {
						mapOfMedical_inventory_outDTOTomedicalEquipmentNameId.get(oldMedical_inventory_outDTO.medicalEquipmentNameId).remove(oldMedical_inventory_outDTO);
					}
					if(mapOfMedical_inventory_outDTOTomedicalEquipmentNameId.get(oldMedical_inventory_outDTO.medicalEquipmentNameId).isEmpty()) {
						mapOfMedical_inventory_outDTOTomedicalEquipmentNameId.remove(oldMedical_inventory_outDTO.medicalEquipmentNameId);
					}
					
					if(mapOfMedical_inventory_outDTOTostockOutQuantity.containsKey(oldMedical_inventory_outDTO.stockOutQuantity)) {
						mapOfMedical_inventory_outDTOTostockOutQuantity.get(oldMedical_inventory_outDTO.stockOutQuantity).remove(oldMedical_inventory_outDTO);
					}
					if(mapOfMedical_inventory_outDTOTostockOutQuantity.get(oldMedical_inventory_outDTO.stockOutQuantity).isEmpty()) {
						mapOfMedical_inventory_outDTOTostockOutQuantity.remove(oldMedical_inventory_outDTO.stockOutQuantity);
					}
					
					if(mapOfMedical_inventory_outDTOTounitPrice.containsKey(oldMedical_inventory_outDTO.unitPrice)) {
						mapOfMedical_inventory_outDTOTounitPrice.get(oldMedical_inventory_outDTO.unitPrice).remove(oldMedical_inventory_outDTO);
					}
					if(mapOfMedical_inventory_outDTOTounitPrice.get(oldMedical_inventory_outDTO.unitPrice).isEmpty()) {
						mapOfMedical_inventory_outDTOTounitPrice.remove(oldMedical_inventory_outDTO.unitPrice);
					}
					
					if(mapOfMedical_inventory_outDTOTotransactionDate.containsKey(oldMedical_inventory_outDTO.transactionDate)) {
						mapOfMedical_inventory_outDTOTotransactionDate.get(oldMedical_inventory_outDTO.transactionDate).remove(oldMedical_inventory_outDTO);
					}
					if(mapOfMedical_inventory_outDTOTotransactionDate.get(oldMedical_inventory_outDTO.transactionDate).isEmpty()) {
						mapOfMedical_inventory_outDTOTotransactionDate.remove(oldMedical_inventory_outDTO.transactionDate);
					}
					
					if(mapOfMedical_inventory_outDTOToremarks.containsKey(oldMedical_inventory_outDTO.remarks)) {
						mapOfMedical_inventory_outDTOToremarks.get(oldMedical_inventory_outDTO.remarks).remove(oldMedical_inventory_outDTO);
					}
					if(mapOfMedical_inventory_outDTOToremarks.get(oldMedical_inventory_outDTO.remarks).isEmpty()) {
						mapOfMedical_inventory_outDTOToremarks.remove(oldMedical_inventory_outDTO.remarks);
					}
					
					if(mapOfMedical_inventory_outDTOToinsertedByUserId.containsKey(oldMedical_inventory_outDTO.insertedByUserId)) {
						mapOfMedical_inventory_outDTOToinsertedByUserId.get(oldMedical_inventory_outDTO.insertedByUserId).remove(oldMedical_inventory_outDTO);
					}
					if(mapOfMedical_inventory_outDTOToinsertedByUserId.get(oldMedical_inventory_outDTO.insertedByUserId).isEmpty()) {
						mapOfMedical_inventory_outDTOToinsertedByUserId.remove(oldMedical_inventory_outDTO.insertedByUserId);
					}
					
					if(mapOfMedical_inventory_outDTOToinsertedByOrganogramId.containsKey(oldMedical_inventory_outDTO.insertedByOrganogramId)) {
						mapOfMedical_inventory_outDTOToinsertedByOrganogramId.get(oldMedical_inventory_outDTO.insertedByOrganogramId).remove(oldMedical_inventory_outDTO);
					}
					if(mapOfMedical_inventory_outDTOToinsertedByOrganogramId.get(oldMedical_inventory_outDTO.insertedByOrganogramId).isEmpty()) {
						mapOfMedical_inventory_outDTOToinsertedByOrganogramId.remove(oldMedical_inventory_outDTO.insertedByOrganogramId);
					}
					
					if(mapOfMedical_inventory_outDTOToinsertionDate.containsKey(oldMedical_inventory_outDTO.insertionDate)) {
						mapOfMedical_inventory_outDTOToinsertionDate.get(oldMedical_inventory_outDTO.insertionDate).remove(oldMedical_inventory_outDTO);
					}
					if(mapOfMedical_inventory_outDTOToinsertionDate.get(oldMedical_inventory_outDTO.insertionDate).isEmpty()) {
						mapOfMedical_inventory_outDTOToinsertionDate.remove(oldMedical_inventory_outDTO.insertionDate);
					}
					
					if(mapOfMedical_inventory_outDTOToexpiryDate.containsKey(oldMedical_inventory_outDTO.expiryDate)) {
						mapOfMedical_inventory_outDTOToexpiryDate.get(oldMedical_inventory_outDTO.expiryDate).remove(oldMedical_inventory_outDTO);
					}
					if(mapOfMedical_inventory_outDTOToexpiryDate.get(oldMedical_inventory_outDTO.expiryDate).isEmpty()) {
						mapOfMedical_inventory_outDTOToexpiryDate.remove(oldMedical_inventory_outDTO.expiryDate);
					}
					
					if(mapOfMedical_inventory_outDTOTomedicalInventoryInId.containsKey(oldMedical_inventory_outDTO.medicalInventoryInId)) {
						mapOfMedical_inventory_outDTOTomedicalInventoryInId.get(oldMedical_inventory_outDTO.medicalInventoryInId).remove(oldMedical_inventory_outDTO);
					}
					if(mapOfMedical_inventory_outDTOTomedicalInventoryInId.get(oldMedical_inventory_outDTO.medicalInventoryInId).isEmpty()) {
						mapOfMedical_inventory_outDTOTomedicalInventoryInId.remove(oldMedical_inventory_outDTO.medicalInventoryInId);
					}
					
					if(mapOfMedical_inventory_outDTOTolastModificationTime.containsKey(oldMedical_inventory_outDTO.lastModificationTime)) {
						mapOfMedical_inventory_outDTOTolastModificationTime.get(oldMedical_inventory_outDTO.lastModificationTime).remove(oldMedical_inventory_outDTO);
					}
					if(mapOfMedical_inventory_outDTOTolastModificationTime.get(oldMedical_inventory_outDTO.lastModificationTime).isEmpty()) {
						mapOfMedical_inventory_outDTOTolastModificationTime.remove(oldMedical_inventory_outDTO.lastModificationTime);
					}
					
					
				}
				if(medical_inventory_outDTO.isDeleted == 0) 
				{
					
					mapOfMedical_inventory_outDTOToiD.put(medical_inventory_outDTO.iD, medical_inventory_outDTO);
				
					if( ! mapOfMedical_inventory_outDTOTomedicalItemCat.containsKey(medical_inventory_outDTO.medicalItemCat)) {
						mapOfMedical_inventory_outDTOTomedicalItemCat.put(medical_inventory_outDTO.medicalItemCat, new HashSet<>());
					}
					mapOfMedical_inventory_outDTOTomedicalItemCat.get(medical_inventory_outDTO.medicalItemCat).add(medical_inventory_outDTO);
					
					if( ! mapOfMedical_inventory_outDTOTodrugInformationId.containsKey(medical_inventory_outDTO.drugInformationId)) {
						mapOfMedical_inventory_outDTOTodrugInformationId.put(medical_inventory_outDTO.drugInformationId, new HashSet<>());
					}
					mapOfMedical_inventory_outDTOTodrugInformationId.get(medical_inventory_outDTO.drugInformationId).add(medical_inventory_outDTO);
					
					if( ! mapOfMedical_inventory_outDTOTomedicalReagentNameId.containsKey(medical_inventory_outDTO.medicalReagentNameId)) {
						mapOfMedical_inventory_outDTOTomedicalReagentNameId.put(medical_inventory_outDTO.medicalReagentNameId, new HashSet<>());
					}
					mapOfMedical_inventory_outDTOTomedicalReagentNameId.get(medical_inventory_outDTO.medicalReagentNameId).add(medical_inventory_outDTO);
					
					if( ! mapOfMedical_inventory_outDTOTomedicalEquipmentNameId.containsKey(medical_inventory_outDTO.medicalEquipmentNameId)) {
						mapOfMedical_inventory_outDTOTomedicalEquipmentNameId.put(medical_inventory_outDTO.medicalEquipmentNameId, new HashSet<>());
					}
					mapOfMedical_inventory_outDTOTomedicalEquipmentNameId.get(medical_inventory_outDTO.medicalEquipmentNameId).add(medical_inventory_outDTO);
					
					if( ! mapOfMedical_inventory_outDTOTostockOutQuantity.containsKey(medical_inventory_outDTO.stockOutQuantity)) {
						mapOfMedical_inventory_outDTOTostockOutQuantity.put(medical_inventory_outDTO.stockOutQuantity, new HashSet<>());
					}
					mapOfMedical_inventory_outDTOTostockOutQuantity.get(medical_inventory_outDTO.stockOutQuantity).add(medical_inventory_outDTO);
					
					if( ! mapOfMedical_inventory_outDTOTounitPrice.containsKey(medical_inventory_outDTO.unitPrice)) {
						mapOfMedical_inventory_outDTOTounitPrice.put(medical_inventory_outDTO.unitPrice, new HashSet<>());
					}
					mapOfMedical_inventory_outDTOTounitPrice.get(medical_inventory_outDTO.unitPrice).add(medical_inventory_outDTO);
					
					if( ! mapOfMedical_inventory_outDTOTotransactionDate.containsKey(medical_inventory_outDTO.transactionDate)) {
						mapOfMedical_inventory_outDTOTotransactionDate.put(medical_inventory_outDTO.transactionDate, new HashSet<>());
					}
					mapOfMedical_inventory_outDTOTotransactionDate.get(medical_inventory_outDTO.transactionDate).add(medical_inventory_outDTO);
					
					if( ! mapOfMedical_inventory_outDTOToremarks.containsKey(medical_inventory_outDTO.remarks)) {
						mapOfMedical_inventory_outDTOToremarks.put(medical_inventory_outDTO.remarks, new HashSet<>());
					}
					mapOfMedical_inventory_outDTOToremarks.get(medical_inventory_outDTO.remarks).add(medical_inventory_outDTO);
					
					if( ! mapOfMedical_inventory_outDTOToinsertedByUserId.containsKey(medical_inventory_outDTO.insertedByUserId)) {
						mapOfMedical_inventory_outDTOToinsertedByUserId.put(medical_inventory_outDTO.insertedByUserId, new HashSet<>());
					}
					mapOfMedical_inventory_outDTOToinsertedByUserId.get(medical_inventory_outDTO.insertedByUserId).add(medical_inventory_outDTO);
					
					if( ! mapOfMedical_inventory_outDTOToinsertedByOrganogramId.containsKey(medical_inventory_outDTO.insertedByOrganogramId)) {
						mapOfMedical_inventory_outDTOToinsertedByOrganogramId.put(medical_inventory_outDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfMedical_inventory_outDTOToinsertedByOrganogramId.get(medical_inventory_outDTO.insertedByOrganogramId).add(medical_inventory_outDTO);
					
					if( ! mapOfMedical_inventory_outDTOToinsertionDate.containsKey(medical_inventory_outDTO.insertionDate)) {
						mapOfMedical_inventory_outDTOToinsertionDate.put(medical_inventory_outDTO.insertionDate, new HashSet<>());
					}
					mapOfMedical_inventory_outDTOToinsertionDate.get(medical_inventory_outDTO.insertionDate).add(medical_inventory_outDTO);
					
					if( ! mapOfMedical_inventory_outDTOToexpiryDate.containsKey(medical_inventory_outDTO.expiryDate)) {
						mapOfMedical_inventory_outDTOToexpiryDate.put(medical_inventory_outDTO.expiryDate, new HashSet<>());
					}
					mapOfMedical_inventory_outDTOToexpiryDate.get(medical_inventory_outDTO.expiryDate).add(medical_inventory_outDTO);
					
					if( ! mapOfMedical_inventory_outDTOTomedicalInventoryInId.containsKey(medical_inventory_outDTO.medicalInventoryInId)) {
						mapOfMedical_inventory_outDTOTomedicalInventoryInId.put(medical_inventory_outDTO.medicalInventoryInId, new HashSet<>());
					}
					mapOfMedical_inventory_outDTOTomedicalInventoryInId.get(medical_inventory_outDTO.medicalInventoryInId).add(medical_inventory_outDTO);
					
					if( ! mapOfMedical_inventory_outDTOTolastModificationTime.containsKey(medical_inventory_outDTO.lastModificationTime)) {
						mapOfMedical_inventory_outDTOTolastModificationTime.put(medical_inventory_outDTO.lastModificationTime, new HashSet<>());
					}
					mapOfMedical_inventory_outDTOTolastModificationTime.get(medical_inventory_outDTO.lastModificationTime).add(medical_inventory_outDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Medical_inventory_outDTO> getMedical_inventory_outList() {
		List <Medical_inventory_outDTO> medical_inventory_outs = new ArrayList<Medical_inventory_outDTO>(this.mapOfMedical_inventory_outDTOToiD.values());
		return medical_inventory_outs;
	}
	
	
	public Medical_inventory_outDTO getMedical_inventory_outDTOByID( long ID){
		return mapOfMedical_inventory_outDTOToiD.get(ID);
	}
	
	
	public List<Medical_inventory_outDTO> getMedical_inventory_outDTOBymedical_item_cat(long medical_item_cat) {
		return new ArrayList<>( mapOfMedical_inventory_outDTOTomedicalItemCat.getOrDefault(medical_item_cat,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_outDTO> getMedical_inventory_outDTOBydrug_information_id(long drug_information_id) {
		return new ArrayList<>( mapOfMedical_inventory_outDTOTodrugInformationId.getOrDefault(drug_information_id,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_outDTO> getMedical_inventory_outDTOBymedical_reagent_name_id(long medical_reagent_name_id) {
		return new ArrayList<>( mapOfMedical_inventory_outDTOTomedicalReagentNameId.getOrDefault(medical_reagent_name_id,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_outDTO> getMedical_inventory_outDTOBymedical_equipment_name_id(long medical_equipment_name_id) {
		return new ArrayList<>( mapOfMedical_inventory_outDTOTomedicalEquipmentNameId.getOrDefault(medical_equipment_name_id,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_outDTO> getMedical_inventory_outDTOBystock_out_quantity(int stock_out_quantity) {
		return new ArrayList<>( mapOfMedical_inventory_outDTOTostockOutQuantity.getOrDefault(stock_out_quantity,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_outDTO> getMedical_inventory_outDTOByunit_price(double unit_price) {
		return new ArrayList<>( mapOfMedical_inventory_outDTOTounitPrice.getOrDefault(unit_price,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_outDTO> getMedical_inventory_outDTOBytransaction_date(long transaction_date) {
		return new ArrayList<>( mapOfMedical_inventory_outDTOTotransactionDate.getOrDefault(transaction_date,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_outDTO> getMedical_inventory_outDTOByremarks(String remarks) {
		return new ArrayList<>( mapOfMedical_inventory_outDTOToremarks.getOrDefault(remarks,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_outDTO> getMedical_inventory_outDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfMedical_inventory_outDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_outDTO> getMedical_inventory_outDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfMedical_inventory_outDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_outDTO> getMedical_inventory_outDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfMedical_inventory_outDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_outDTO> getMedical_inventory_outDTOByexpiry_date(long expiry_date) {
		return new ArrayList<>( mapOfMedical_inventory_outDTOToexpiryDate.getOrDefault(expiry_date,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_outDTO> getMedical_inventory_outDTOBymedical_inventory_in_id(long medical_inventory_in_id) {
		return new ArrayList<>( mapOfMedical_inventory_outDTOTomedicalInventoryInId.getOrDefault(medical_inventory_in_id,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_outDTO> getMedical_inventory_outDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfMedical_inventory_outDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "medical_inventory_out";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


