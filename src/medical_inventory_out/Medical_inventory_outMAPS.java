package medical_inventory_out;
import java.util.*; 
import util.*;


public class Medical_inventory_outMAPS extends CommonMaps
{	
	public Medical_inventory_outMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("medicalItemCat".toLowerCase(), "medicalItemCat".toLowerCase());
		java_DTO_map.put("drugInformationId".toLowerCase(), "drugInformationId".toLowerCase());
		java_DTO_map.put("medicalReagentNameId".toLowerCase(), "medicalReagentNameId".toLowerCase());
		java_DTO_map.put("medicalEquipmentNameId".toLowerCase(), "medicalEquipmentNameId".toLowerCase());
		java_DTO_map.put("stockOutQuantity".toLowerCase(), "stockOutQuantity".toLowerCase());
		java_DTO_map.put("unitPrice".toLowerCase(), "unitPrice".toLowerCase());
		java_DTO_map.put("transactionDate".toLowerCase(), "transactionDate".toLowerCase());
		java_DTO_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("expiryDate".toLowerCase(), "expiryDate".toLowerCase());
		java_DTO_map.put("medicalInventoryInId".toLowerCase(), "medicalInventoryInId".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("medical_item_cat".toLowerCase(), "medicalItemCat".toLowerCase());
		java_SQL_map.put("drug_information_id".toLowerCase(), "drugInformationId".toLowerCase());
		java_SQL_map.put("medical_reagent_name_id".toLowerCase(), "medicalReagentNameId".toLowerCase());
		java_SQL_map.put("medical_equipment_name_id".toLowerCase(), "medicalEquipmentNameId".toLowerCase());
		java_SQL_map.put("stock_out_quantity".toLowerCase(), "stockOutQuantity".toLowerCase());
		java_SQL_map.put("unit_price".toLowerCase(), "unitPrice".toLowerCase());
		java_SQL_map.put("transaction_date".toLowerCase(), "transactionDate".toLowerCase());
		java_SQL_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_SQL_map.put("inserted_by_user_id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_SQL_map.put("inserted_by_organogram_id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_SQL_map.put("insertion_date".toLowerCase(), "insertionDate".toLowerCase());
		java_SQL_map.put("expiry_date".toLowerCase(), "expiryDate".toLowerCase());
		java_SQL_map.put("medical_inventory_in_id".toLowerCase(), "medicalInventoryInId".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Medical Item".toLowerCase(), "medicalItemCat".toLowerCase());
		java_Text_map.put("Drug Information Id".toLowerCase(), "drugInformationId".toLowerCase());
		java_Text_map.put("Medical Reagent Name Id".toLowerCase(), "medicalReagentNameId".toLowerCase());
		java_Text_map.put("Medical Equipment Name Id".toLowerCase(), "medicalEquipmentNameId".toLowerCase());
		java_Text_map.put("Stock Out Quantity".toLowerCase(), "stockOutQuantity".toLowerCase());
		java_Text_map.put("Unit Price".toLowerCase(), "unitPrice".toLowerCase());
		java_Text_map.put("Transaction Date".toLowerCase(), "transactionDate".toLowerCase());
		java_Text_map.put("Remarks".toLowerCase(), "remarks".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Expiry Date".toLowerCase(), "expiryDate".toLowerCase());
		java_Text_map.put("Medical Inventory In Id".toLowerCase(), "medicalInventoryInId".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}