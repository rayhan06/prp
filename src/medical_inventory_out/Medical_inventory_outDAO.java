package medical_inventory_out;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;

import appointment.KeyCountDTO;
import appointment.YearMonthCount;
import drug_information.Drug_informationDAO;
import drug_information.Drug_informationDTO;
import employee_offices.EmployeeOfficeRepository;
import medical_equipment_name.Medical_equipment_nameDAO;
import medical_equipment_name.Medical_equipment_nameDTO;
import medical_inventory_lot.MedicalInventoryInDTO;
import medical_reagent_name.Medical_reagent_nameDAO;
import medical_reagent_name.Medical_reagent_nameDTO;
import medical_transaction.Medical_transactionDAO;
import medical_transaction.Medical_transactionDTO;
import repository.RepositoryManager;

import util.*;
import pb.*;
import pb_notifications.Pb_notificationsDAO;
import user.UserDTO;

public class Medical_inventory_outDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	Medical_reagent_nameDAO medical_reagent_nameDAO = new Medical_reagent_nameDAO();
	Medical_equipment_nameDAO medical_equiment_nameDAO = new Medical_equipment_nameDAO();
	Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
	Medical_transactionDAO medical_transactionDAO = new Medical_transactionDAO();
	
	
	public Medical_inventory_outDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Medical_inventory_outMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"medical_item_cat",
			"drug_information_id",
			"medical_reagent_name_id",
			"medical_equipment_name_id",
			"stock_out_quantity",
			"unit_price",
			"transaction_date",
			"remarks",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"expiry_date",
			"medical_inventory_in_id",
			"search_column",
			"transaction_type",
			"user_name",
			"organogram_id",
			"transaction_time",
			"patient_name",
			"phone",
			"organization_id",
			"organization_name_en",
			"organization_name_bn",
			"prescription_lab_test_id",
			"medical_equipment_cat",
			"prescription_details_id",
			"ppm_id",
			"employee_record_id",
			"medical_dept_cat",
			"other_office_id",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Medical_inventory_outDAO()
	{
		this("medical_inventory_out");		
	}
	
	public void setSearchColumn(Medical_inventory_outDTO medical_inventory_outDTO)
	{
		medical_inventory_outDTO.searchColumn = "";
		medical_inventory_outDTO.searchColumn += CatDAO.getName("English", "medical_item", medical_inventory_outDTO.medicalItemCat) + " " + CatDAO.getName("Bangla", "medical_item", medical_inventory_outDTO.medicalItemCat) + " ";
		medical_inventory_outDTO.searchColumn += medical_inventory_outDTO.remarks + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Medical_inventory_outDTO medical_inventory_outDTO = (Medical_inventory_outDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(medical_inventory_outDTO);
		if(isInsert)
		{
			ps.setObject(index++,medical_inventory_outDTO.iD);
		}
		ps.setObject(index++,medical_inventory_outDTO.medicalItemCat);
		ps.setObject(index++,medical_inventory_outDTO.drugInformationId);
		ps.setObject(index++,medical_inventory_outDTO.medicalReagentNameId);
		ps.setObject(index++,medical_inventory_outDTO.medicalEquipmentNameId);
		ps.setObject(index++,medical_inventory_outDTO.stockOutQuantity);
		ps.setObject(index++,medical_inventory_outDTO.unitPrice);
		ps.setObject(index++,medical_inventory_outDTO.transactionDate);
		ps.setObject(index++,medical_inventory_outDTO.remarks);
		ps.setObject(index++,medical_inventory_outDTO.insertedByUserId);
		ps.setObject(index++,medical_inventory_outDTO.insertedByOrganogramId);
		ps.setObject(index++,medical_inventory_outDTO.insertionDate);
		ps.setObject(index++,medical_inventory_outDTO.expiryDate);
		ps.setObject(index++,medical_inventory_outDTO.medicalInventoryInId);
		ps.setObject(index++,medical_inventory_outDTO.searchColumn);
		ps.setObject(index++,medical_inventory_outDTO.transactionType);
		ps.setObject(index++,medical_inventory_outDTO.userName);
		ps.setObject(index++,medical_inventory_outDTO.organogramId);
		ps.setObject(index++,medical_inventory_outDTO.transactionTime);
		ps.setObject(index++,medical_inventory_outDTO.patientName);
		ps.setObject(index++,medical_inventory_outDTO.phone);
		ps.setObject(index++,medical_inventory_outDTO.organizationId);
		ps.setObject(index++,medical_inventory_outDTO.organizationNameEn);
		ps.setObject(index++,medical_inventory_outDTO.organizationNameBn);
		ps.setObject(index++,medical_inventory_outDTO.prescriptionLabTestId);
		ps.setObject(index++,medical_inventory_outDTO.medicalEquipmentCat);
		
		ps.setObject(index++,medical_inventory_outDTO.prescriptionDetailsId);
		ps.setObject(index++,medical_inventory_outDTO.ppmId);
		ps.setObject(index++,medical_inventory_outDTO.employeeRecordId);
		ps.setObject(index++,medical_inventory_outDTO.medicalDeptCat);
		ps.setObject(index++,medical_inventory_outDTO.otherOfficeId);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Medical_inventory_outDTO build(ResultSet rs)
	{
		try
		{
			Medical_inventory_outDTO medical_inventory_outDTO = new Medical_inventory_outDTO();
			medical_inventory_outDTO.iD = rs.getLong("ID");
			medical_inventory_outDTO.medicalItemCat = rs.getLong("medical_item_cat");
			medical_inventory_outDTO.drugInformationId = rs.getLong("drug_information_id");
			medical_inventory_outDTO.medicalReagentNameId = rs.getLong("medical_reagent_name_id");
			medical_inventory_outDTO.medicalEquipmentNameId = rs.getLong("medical_equipment_name_id");
			medical_inventory_outDTO.stockOutQuantity = rs.getInt("stock_out_quantity");
			medical_inventory_outDTO.unitPrice = rs.getDouble("unit_price");
			medical_inventory_outDTO.transactionDate = rs.getLong("transaction_date");
			medical_inventory_outDTO.remarks = rs.getString("remarks");
			medical_inventory_outDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			medical_inventory_outDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			medical_inventory_outDTO.insertionDate = rs.getLong("insertion_date");
			medical_inventory_outDTO.expiryDate = rs.getLong("expiry_date");
			medical_inventory_outDTO.medicalInventoryInId = rs.getLong("medical_inventory_in_id");
			medical_inventory_outDTO.searchColumn = rs.getString("search_column");
			medical_inventory_outDTO.transactionType = rs.getInt("transaction_type");
			medical_inventory_outDTO.userName = rs.getString("user_name");
			medical_inventory_outDTO.organogramId = rs.getLong("organogram_id");
			medical_inventory_outDTO.transactionTime = rs.getLong("transaction_time");
			medical_inventory_outDTO.patientName = rs.getString("patient_name");
			medical_inventory_outDTO.phone = rs.getString("phone");
			
			medical_inventory_outDTO.organizationId = rs.getLong("organization_id");
			medical_inventory_outDTO.organizationNameEn = rs.getString("organization_name_en");
			medical_inventory_outDTO.organizationNameBn = rs.getString("organization_name_bn");
			
			medical_inventory_outDTO.prescriptionLabTestId = rs.getLong("prescription_lab_test_id");
			medical_inventory_outDTO.medicalEquipmentCat = rs.getInt("medical_equipment_cat");
			
			medical_inventory_outDTO.prescriptionDetailsId = rs.getLong("prescription_details_id");
			medical_inventory_outDTO.ppmId = rs.getLong("ppm_id");
			
			medical_inventory_outDTO.employeeRecordId = rs.getLong("employee_record_id");
			medical_inventory_outDTO.medicalDeptCat = rs.getInt("medical_dept_cat");
			
			medical_inventory_outDTO.otherOfficeId = rs.getLong("other_office_id");
			
			medical_inventory_outDTO.isDeleted = rs.getInt("isDeleted");
			medical_inventory_outDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return medical_inventory_outDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	public long add(CommonDTO commonDTO) throws Exception
	{
		
		Medical_inventory_outDTO medical_inventory_outDTO = (Medical_inventory_outDTO)commonDTO;
		if(medical_inventory_outDTO.stockOutQuantity == 0)
		{
			return -1;
		}
		
		if(medical_inventory_outDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_REAGENT)
		{
			Medical_reagent_nameDTO medical_reagent_nameDTO = medical_reagent_nameDAO.getDTOByID(medical_inventory_outDTO.medicalReagentNameId);
			if(medical_reagent_nameDTO.currentStock - medical_inventory_outDTO.stockOutQuantity < 0)
			{
				return -1;
			}			
		}
		else if(medical_inventory_outDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_EQUIPMENT)
		{
			Medical_equipment_nameDTO medical_equipment_nameDTO =  medical_equiment_nameDAO.getDTOByID(medical_inventory_outDTO.medicalEquipmentNameId);
			if(medical_equipment_nameDTO.currentStock - medical_inventory_outDTO.stockOutQuantity < 0)
			{
				return -1;
			}
		}
		else if(medical_inventory_outDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_DRUG)
		{
			Drug_informationDTO drug_informationDTO =  drug_informationDAO.getDTOByID(medical_inventory_outDTO.drugInformationId);
			if(drug_informationDTO.availableStock - medical_inventory_outDTO.stockOutQuantity < 0)
			{
				return -1;
			}
		}
		
		super.add(commonDTO);
		
		if(medical_inventory_outDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_REAGENT)
		{
			Medical_reagent_nameDTO medical_reagent_nameDTO = medical_reagent_nameDAO.getDTOByID(medical_inventory_outDTO.medicalReagentNameId);
			medical_reagent_nameDTO.currentStock -= medical_inventory_outDTO.stockOutQuantity;
			
			medical_reagent_nameDAO.update(medical_reagent_nameDTO);
			if(medical_reagent_nameDTO.currentStock < medical_reagent_nameDTO.stockAlertQuantity)
			{
				sendNoti(SessionConstants.MEDICAL_ITEM_REAGENT, medical_reagent_nameDTO.iD, medical_reagent_nameDTO.nameEn, medical_reagent_nameDTO.nameBn);
			}
		}
		else if(medical_inventory_outDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_EQUIPMENT)
		{
			Medical_equipment_nameDTO medical_equipment_nameDTO =  medical_equiment_nameDAO.getDTOByID(medical_inventory_outDTO.medicalEquipmentNameId);
			medical_equipment_nameDTO.currentStock -= medical_inventory_outDTO.stockOutQuantity;
			medical_equiment_nameDAO.update(medical_equipment_nameDTO);
			
			if(medical_equipment_nameDTO.currentStock < medical_equipment_nameDTO.stockAlertQuantity)
			{
				sendNoti(SessionConstants.MEDICAL_ITEM_EQUIPMENT, medical_equipment_nameDTO.iD, medical_equipment_nameDTO.nameEn, medical_equipment_nameDTO.nameBn);
			}
		}
		else if(medical_inventory_outDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_DRUG)
		{
			Drug_informationDTO drug_informationDTO =  drug_informationDAO.getDTOByID(medical_inventory_outDTO.drugInformationId);
			drug_informationDTO.currentStock -= medical_inventory_outDTO.stockOutQuantity;
			drug_informationDTO.availableStock -= medical_inventory_outDTO.stockOutQuantity;
			if(drug_informationDTO.iD == Drug_informationDTO.ACCU_CHECK_ID)
			{
				drug_informationDTO.detailedCount -= medical_inventory_outDTO.stockOutQuantity * Drug_informationDTO.ACCU_SIZE;
			}
			drug_informationDAO.update(drug_informationDTO);
			
			if(drug_informationDTO.currentStock < drug_informationDTO.minimulLevelForAlert)
			{
				sendNoti(SessionConstants.MEDICAL_ITEM_DRUG, drug_informationDTO.iD, drug_informationDTO.nameEn, drug_informationDTO.nameBn);
			}
		}
		
		Medical_transactionDTO medical_transactionDTO = new Medical_transactionDTO(medical_inventory_outDTO);
		medical_transactionDAO.add(medical_transactionDTO);
		return medical_inventory_outDTO.iD;		
	}

	public Set<Long> getOrganogramIdByRole(int role)
	{
		return EmployeeOfficeRepository.getInstance().getByRole(role);		
	}
	
	public List<KeyCountDTO> getOrganizationwiseData ()
    {
		String sql = "SELECT organization_id, count(id), stock_out_quantity * unit_price, organization_name_en, organization_name_bn "
				+ " FROM " + tableName + " where transaction_type =" + MedicalInventoryInDTO.TR_RECEIVE_MEDICINE 
				+ " and isDeleted = 0 and organization_id != -1";
		
		sql+= " group by organization_id";
		
		return ConnectionAndStatementUtil.getListOfT(sql,this::getOrgCount);	
    }
	
	public KeyCountDTO getOrgCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("organization_id");
			keyCountDTO.count = rs.getInt("count(id)");
			keyCountDTO.cost = rs.getDouble("stock_out_quantity * unit_price");
			keyCountDTO.nameEn = rs.getString("organization_name_en");
			keyCountDTO.nameBn = rs.getString("organization_name_bn");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public List<KeyCountDTO> getDrugoholics ()
    {
		String sql = "SELECT \r\n" + 
				"    user_name, SUM(stock_out_quantity * unit_price) AS cost\r\n" + 
				"FROM\r\n" + 
				"    medical_inventory_out\r\n" + 
				"    where user_name is not null and transaction_type = 1\r\n" + 
				"    and medical_item_cat = 0\r\n" + 
				"GROUP BY user_name\r\n" + 
				"ORDER BY cost DESC\r\n" + 
				"limit 10";

		
		return ConnectionAndStatementUtil.getListOfT(sql,this::getDrugoholicCount);	
    }
	
	public KeyCountDTO getDrugoholicCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.keyStr = rs.getString("user_name");
			keyCountDTO.cost = rs.getDouble("cost");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public void sendNoti(int cat, long id, String nameEn, String nameBn)
	{
		Set<Long> inventoryManagers = getOrganogramIdByRole(SessionConstants.INVENTORY_MANGER_ROLE);
		Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();
		String notiMessage = nameEn + " is almost out of stock.$" + nameEn + " এর স্টক খুব কমে গেছে";
        String URL =  "Medical_requisition_lotServlet?actionType=getAddPage";
		
		for(Long orgId: inventoryManagers)
		{
			pb_notificationsDAO.addPb_notifications(orgId, System.currentTimeMillis(), URL, notiMessage);
		}
	}

	public Medical_inventory_outDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Medical_inventory_outDTO medical_inventory_outDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return medical_inventory_outDTO;
	}
	
	public KeyCountDTO getCountCost(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("insertion_date");
			keyCountDTO.count = rs.getInt("sum(stock_out_quantity)");
			keyCountDTO.cost = rs.getDouble("sum(stock_out_quantity * unit_price)");
			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	
	
	public List<KeyCountDTO> getLast7DayCountCost()
    {
		String sql = "SELECT insertion_date, sum(stock_out_quantity), sum(stock_out_quantity * unit_price) FROM medical_inventory_out where insertion_date >=" + TimeConverter.getNthDay(-7) + " and isDeleted = 0";
		sql+= " group by insertion_date order by insertion_date asc";
		
		return ConnectionAndStatementUtil.getListOfT(sql,this::getCountCost);	
    }
	
	
	public YearMonthCount getYmCountCost(ResultSet rs)
	{
		try
		{
			YearMonthCount ymCount = new YearMonthCount();
			String ym = rs.getString("ym");
			ymCount.year = Integer.parseInt(ym.split("-")[0]);
			ymCount.month = Integer.parseInt(ym.split("-")[1]);
			ymCount.count = rs.getInt("sum(stock_out_quantity)");
			ymCount.cost = rs.getDouble("sum(stock_out_quantity * unit_price)");
			
			//System.out.println("year = " + ymCount.year + " month = " + ymCount.month + " count = " + ymCount.count);

			return ymCount;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public List<YearMonthCount> getLast6MonthCostCount ()
    {
		String sql = "SELECT \r\n" + 
				"    DATE_FORMAT(FROM_UNIXTIME(`insertion_date` / 1000),\r\n" + 
				"            '%Y-%m') AS ym,\r\n" + 
				"    sum(stock_out_quantity), sum(stock_out_quantity * unit_price)\r\n" + 
				"FROM\r\n" + 
				"    medical_inventory_out\r\n" + 
				"WHERE\r\n" + 
				"    insertion_date >= " + TimeConverter.get1stDayOfNthtMonth(-6) + " AND isDeleted = 0 \r\n" + 
				"GROUP BY ym\r\n" + 
				"ORDER BY ym ASC;";
		
		//System.out.println(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::getYmCountCost);	
    }
	
	public KeyCountDTO getCountCostNoKey(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.count = rs.getInt("sum(stock_out_quantity)");
			keyCountDTO.cost = rs.getDouble("sum(stock_out_quantity * unit_price)");
			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}

	public KeyCountDTO getCostCount (long startDate, long endDate)
	{
		String sql = "SELECT sum(stock_out_quantity), sum(stock_out_quantity * unit_price) FROM medical_inventory_out where insertion_date >= " + startDate 
				+ " and insertion_date < " + endDate ;
		return ConnectionAndStatementUtil.getT(sql,this::getCountCostNoKey);
	}
	
	public int getSum(ResultSet rs) {
		int sum = 0;
		try
		{			
			sum = rs.getInt("sum(stock_out_quantity)");		
			return sum;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return 0;
		}
	}
	
	public int getSumBorrowed(int dept, long drugInformationId)
	{
		String sql = "select sum(stock_out_quantity) from medical_inventory_out where "
				+ "medical_dept_cat = " + dept + " "
				+ "and transaction_type = " + MedicalInventoryInDTO.TR_BORROW + " "
				+ "and drug_information_id = " +  drugInformationId; 
		return ConnectionAndStatementUtil.getT(sql,this::getSum, 0);		
	}
	

	
	

	
	public List<Medical_inventory_outDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<Medical_inventory_outDTO> getAllMedical_inventory_out (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Medical_inventory_outDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Medical_inventory_outDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("medical_item_cat")
						|| str.equals("transaction_date_start")
						|| str.equals("transaction_date_end")
						|| str.equals("expiry_date_start")
						|| str.equals("expiry_date_end")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("medical_item_cat"))
					{
						AllFieldSql += "" + tableName + ".medical_item_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("transaction_date_start"))
					{
						AllFieldSql += "" + tableName + ".transaction_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("transaction_date_end"))
					{
						AllFieldSql += "" + tableName + ".transaction_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("expiry_date_start"))
					{
						AllFieldSql += "" + tableName + ".expiry_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("expiry_date_end"))
					{
						AllFieldSql += "" + tableName + ".expiry_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					 
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	