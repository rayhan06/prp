package medical_inventory_out;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;
import util.TimeConverter;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Medical_inventory_outServlet
 */
@WebServlet("/Medical_inventory_outServlet")
@MultipartConfig
public class Medical_inventory_outServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Medical_inventory_outServlet.class);

    String tableName = "medical_inventory_out";

	Medical_inventory_outDAO medical_inventory_outDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Medical_inventory_outServlet() 
	{
        super();
    	try
    	{
			medical_inventory_outDAO = new Medical_inventory_outDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(medical_inventory_outDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_INVENTORY_OUT_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_INVENTORY_OUT_UPDATE))
				{
					getMedical_inventory_out(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_INVENTORY_OUT_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							if(filter.equalsIgnoreCase("getTransactionsOfThisMonth"))
							{
								filter = "insertion_date >= " + TimeConverter.get1stDayOfMonth() + " and insertion_date < " + TimeConverter.get1stDayOfNextMonth();
							}
							else
							{
								filter = ""; //shouldn't be directly used, rather manipulate it.
							}
									
							
							searchMedical_inventory_out(request, response, isPermanentTable, filter);
						}
						else
						{
							searchMedical_inventory_out(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchMedical_inventory_out(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_INVENTORY_OUT_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_INVENTORY_OUT_ADD))
				{
					System.out.println("going to  addMedical_inventory_out ");
					addMedical_inventory_out(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addMedical_inventory_out ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_INVENTORY_OUT_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addMedical_inventory_out ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_INVENTORY_OUT_UPDATE))
				{					
					addMedical_inventory_out(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{								
				deleteMedical_inventory_out(request, response, userDTO);				
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_INVENTORY_OUT_SEARCH))
				{
					searchMedical_inventory_out(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Medical_inventory_outDTO medical_inventory_outDTO = medical_inventory_outDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(medical_inventory_outDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addMedical_inventory_out(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addMedical_inventory_out");
			String path = getServletContext().getRealPath("/img2/");
			Medical_inventory_outDTO medical_inventory_outDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				medical_inventory_outDTO = new Medical_inventory_outDTO();
			}
			else
			{
				medical_inventory_outDTO = medical_inventory_outDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("medicalItemCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("medicalItemCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				medical_inventory_outDTO.medicalItemCat = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("drugInformationId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("drugInformationId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				medical_inventory_outDTO.drugInformationId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("medicalReagentNameId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("medicalReagentNameId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				medical_inventory_outDTO.medicalReagentNameId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("medicalEquipmentNameId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("medicalEquipmentNameId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				medical_inventory_outDTO.medicalEquipmentNameId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("stockOutQuantity");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("stockOutQuantity = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				medical_inventory_outDTO.stockOutQuantity = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("unitPrice");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("unitPrice = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				medical_inventory_outDTO.unitPrice = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("transactionDate");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("transactionDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				try 
				{
					Date d = f.parse(Value);
					medical_inventory_outDTO.transactionDate = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("remarks");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("remarks = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				medical_inventory_outDTO.remarks = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				medical_inventory_outDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				medical_inventory_outDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				medical_inventory_outDTO.insertionDate = c.getTimeInMillis();
			}			


			Value = request.getParameter("expiryDate");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("expiryDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				try 
				{
					Date d = f.parse(Value);
					medical_inventory_outDTO.expiryDate = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("medicalInventoryInId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("medicalInventoryInId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				medical_inventory_outDTO.medicalInventoryInId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addMedical_inventory_out dto = " + medical_inventory_outDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				medical_inventory_outDAO.setIsDeleted(medical_inventory_outDTO.iD, CommonDTO.OUTDATED);
				returnedID = medical_inventory_outDAO.add(medical_inventory_outDTO);
				medical_inventory_outDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = medical_inventory_outDAO.manageWriteOperations(medical_inventory_outDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = medical_inventory_outDAO.manageWriteOperations(medical_inventory_outDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getMedical_inventory_out(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Medical_inventory_outServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(medical_inventory_outDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void deleteMedical_inventory_out(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				
				Medical_inventory_outDTO medical_inventory_outDTO = medical_inventory_outDAO.getDTOByID(id);
				medical_inventory_outDAO.manageWriteOperations(medical_inventory_outDTO, SessionConstants.DELETE, id, userDTO);
				response.sendRedirect("Medical_inventory_outServlet?actionType=search");
				
			}			
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		
	}

	private void getMedical_inventory_out(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getMedical_inventory_out");
		Medical_inventory_outDTO medical_inventory_outDTO = null;
		try 
		{
			medical_inventory_outDTO = medical_inventory_outDAO.getDTOByID(id);
			request.setAttribute("ID", medical_inventory_outDTO.iD);
			request.setAttribute("medical_inventory_outDTO",medical_inventory_outDTO);
			request.setAttribute("medical_inventory_outDAO",medical_inventory_outDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "medical_inventory_out/medical_inventory_outInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "medical_inventory_out/medical_inventory_outSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "medical_inventory_out/medical_inventory_outEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "medical_inventory_out/medical_inventory_outEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getMedical_inventory_out(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getMedical_inventory_out(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchMedical_inventory_out(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchMedical_inventory_out 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_MEDICAL_INVENTORY_OUT,
			request,
			medical_inventory_outDAO,
			SessionConstants.VIEW_MEDICAL_INVENTORY_OUT,
			SessionConstants.SEARCH_MEDICAL_INVENTORY_OUT,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("medical_inventory_outDAO",medical_inventory_outDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to medical_inventory_out/medical_inventory_outApproval.jsp");
	        	rd = request.getRequestDispatcher("medical_inventory_out/medical_inventory_outApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to medical_inventory_out/medical_inventory_outApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("medical_inventory_out/medical_inventory_outApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to medical_inventory_out/medical_inventory_outSearch.jsp");
	        	rd = request.getRequestDispatcher("medical_inventory_out/medical_inventory_outSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to medical_inventory_out/medical_inventory_outSearchForm.jsp");
	        	rd = request.getRequestDispatcher("medical_inventory_out/medical_inventory_outSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

