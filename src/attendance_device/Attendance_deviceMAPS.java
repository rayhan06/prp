package attendance_device;

import util.CommonMaps;


public class Attendance_deviceMAPS extends CommonMaps {
    public Attendance_deviceMAPS(String tableName) {


        java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
        java_DTO_map.put("deviceNameEn".toLowerCase(), "deviceNameEn".toLowerCase());
        java_DTO_map.put("deviceNameBn".toLowerCase(), "deviceNameBn".toLowerCase());
        java_DTO_map.put("deviceCat".toLowerCase(), "deviceCat".toLowerCase());
        java_DTO_map.put("description".toLowerCase(), "description".toLowerCase());
        java_DTO_map.put("synchronizationProcessCat".toLowerCase(), "synchronizationProcessCat".toLowerCase());
        java_DTO_map.put("synchronizationDuration".toLowerCase(), "synchronizationDuration".toLowerCase());
        java_DTO_map.put("deviceIpAddress".toLowerCase(), "deviceIpAddress".toLowerCase());
        java_DTO_map.put("ftpIpAddress".toLowerCase(), "ftpIpAddress".toLowerCase());
        java_DTO_map.put("ftpUsername".toLowerCase(), "ftpUsername".toLowerCase());
        java_DTO_map.put("ftpPassword".toLowerCase(), "ftpPassword".toLowerCase());
        java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
        java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
        java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
        java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
        java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());

        java_SQL_map.put("device_name_en".toLowerCase(), "deviceNameEn".toLowerCase());
        java_SQL_map.put("device_name_bn".toLowerCase(), "deviceNameBn".toLowerCase());
        java_SQL_map.put("device_cat".toLowerCase(), "deviceCat".toLowerCase());
        java_SQL_map.put("description".toLowerCase(), "description".toLowerCase());
        java_SQL_map.put("synchronization_process_cat".toLowerCase(), "synchronizationProcessCat".toLowerCase());
        java_SQL_map.put("synchronization_duration".toLowerCase(), "synchronizationDuration".toLowerCase());
        java_SQL_map.put("device_ip_address".toLowerCase(), "deviceIpAddress".toLowerCase());
        java_SQL_map.put("ftp_ip_address".toLowerCase(), "ftpIpAddress".toLowerCase());
        java_SQL_map.put("ftp_username".toLowerCase(), "ftpUsername".toLowerCase());
        java_SQL_map.put("ftp_password".toLowerCase(), "ftpPassword".toLowerCase());

        java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
        java_Text_map.put("Device Name En".toLowerCase(), "deviceNameEn".toLowerCase());
        java_Text_map.put("Device Name Bn".toLowerCase(), "deviceNameBn".toLowerCase());
        java_Text_map.put("Device".toLowerCase(), "deviceCat".toLowerCase());
        java_Text_map.put("Description".toLowerCase(), "description".toLowerCase());
        java_Text_map.put("Synchronization Process".toLowerCase(), "synchronizationProcessCat".toLowerCase());
        java_Text_map.put("Synchronization Duration".toLowerCase(), "synchronizationDuration".toLowerCase());
        java_Text_map.put("Device Ip Address".toLowerCase(), "deviceIpAddress".toLowerCase());
        java_Text_map.put("Ftp Ip Address".toLowerCase(), "ftpIpAddress".toLowerCase());
        java_Text_map.put("Ftp Username".toLowerCase(), "ftpUsername".toLowerCase());
        java_Text_map.put("Ftp Password".toLowerCase(), "ftpPassword".toLowerCase());
        java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
        java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
        java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
        java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
        java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());

    }

}