package attendance_device;

import employee_records.EmployeeCommonDTOService;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@SuppressWarnings({"rawtypes", "unused"})
public class Attendance_deviceDAO extends NavigationService4 implements EmployeeCommonDTOService<Attendance_deviceDTO> {

    private static final Logger logger = Logger.getLogger(Attendance_deviceDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} ( device_name_en,device_name_bn,device_cat,description,synchronization_process_cat,"
            + "synchronization_duration,device_ip_address,ftp_ip_address,ftp_username,ftp_password,insertion_date,inserted_by,"
            + "modified_by,lastModificationTime,search_column,isDeleted,id) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static final String updateQuery = "UPDATE {tableName} SET device_name_en=?,device_name_bn=?,device_cat=?,description=?,synchronization_process_cat=?,"
            + "synchronization_duration=?,device_ip_address=?,ftp_ip_address=?,ftp_username=?,ftp_password=?,insertion_date=?,"
            + "inserted_by=?,modified_by=?,lastModificationTime =,search_column=? WHERE id = ?";

    private final Map<String, String> searchMap = new HashMap<>();

    private static final String tableName = "attendance_device";

    private static Attendance_deviceDAO INSTANCE = null;

    public static Attendance_deviceDAO getInstance() {
        if (INSTANCE == null) {
            synchronized (Attendance_deviceDAO.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Attendance_deviceDAO();
                }
            }
        }
        return INSTANCE;
    }

    private Attendance_deviceDAO() {
        super(tableName);
        joinSQL = "";
        useSafeSearch = true;
        searchMap.put("device_cat", " and (device_cat = ?)");
        searchMap.put("synchronization_process_cat", " and (synchronization_process_cat = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
        commonMaps = new Attendance_deviceMAPS(tableName);
    }

    public void setSearchColumn(Attendance_deviceDTO attendance_deviceDTO) {
        StringBuilder searchColumnBuilder = new StringBuilder();
        if (attendance_deviceDTO.deviceNameEn != null && attendance_deviceDTO.deviceNameEn.trim().length() > 0) {
            searchColumnBuilder.append(attendance_deviceDTO.deviceNameEn.trim()).append(" ");
        }
        if (attendance_deviceDTO.deviceNameBn != null && attendance_deviceDTO.deviceNameBn.trim().length() > 0) {
            searchColumnBuilder.append(attendance_deviceDTO.deviceNameBn.trim()).append(" ");
        }
        CategoryLanguageModel model = CatRepository.getInstance().getCategoryLanguageModel("device", attendance_deviceDTO.deviceCat);
        if (model != null) {
            searchColumnBuilder.append(model.englishText).append(" ");
            searchColumnBuilder.append(model.banglaText).append(" ");
        }
        model = CatRepository.getInstance().getCategoryLanguageModel("synchronization_process", attendance_deviceDTO.synchronizationProcessCat);
        if (model != null) {
            searchColumnBuilder.append(model.englishText).append(" ");
            searchColumnBuilder.append(model.banglaText).append(" ");
        }
        if (attendance_deviceDTO.deviceIpAddress != null && attendance_deviceDTO.deviceIpAddress.trim().length() > 0) {
            searchColumnBuilder.append(attendance_deviceDTO.deviceIpAddress.trim()).append(" ");
        }
        if (attendance_deviceDTO.ftpIpAddress != null && attendance_deviceDTO.ftpIpAddress.trim().length() > 0) {
            searchColumnBuilder.append(attendance_deviceDTO.ftpIpAddress.trim()).append(" ");
        }
        if (attendance_deviceDTO.ftpUsername != null && attendance_deviceDTO.ftpUsername.trim().length() > 0) {
            searchColumnBuilder.append(attendance_deviceDTO.ftpUsername.trim()).append(" ");
        }
        attendance_deviceDTO.searchColumn = searchColumnBuilder.toString();
    }

    public void set(PreparedStatement ps, Attendance_deviceDTO attendance_deviceDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(attendance_deviceDTO);
        ps.setObject(++index, attendance_deviceDTO.deviceNameEn);
        ps.setObject(++index, attendance_deviceDTO.deviceNameBn);
        ps.setObject(++index, attendance_deviceDTO.deviceCat);
        ps.setObject(++index, attendance_deviceDTO.description);
        ps.setObject(++index, attendance_deviceDTO.synchronizationProcessCat);
        ps.setObject(++index, attendance_deviceDTO.synchronizationDuration);
        ps.setObject(++index, attendance_deviceDTO.deviceIpAddress);
        ps.setObject(++index, attendance_deviceDTO.ftpIpAddress);
        ps.setObject(++index, attendance_deviceDTO.ftpUsername);
        ps.setObject(++index, attendance_deviceDTO.ftpPassword);
        ps.setObject(++index, attendance_deviceDTO.insertionDate);
        ps.setObject(++index, attendance_deviceDTO.insertedBy);
        ps.setObject(++index, attendance_deviceDTO.modifiedBy);
        ps.setObject(++index, System.currentTimeMillis());
        ps.setObject(++index, attendance_deviceDTO.searchColumn);
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, attendance_deviceDTO.id);
    }

    @Override
    public Attendance_deviceDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Attendance_deviceDTO attendance_deviceDTO = new Attendance_deviceDTO();
            attendance_deviceDTO.id = rs.getLong("id");
            attendance_deviceDTO.deviceNameEn = rs.getString("device_name_en");
            attendance_deviceDTO.deviceNameBn = rs.getString("device_name_bn");
            attendance_deviceDTO.deviceCat = rs.getInt("device_cat");
            attendance_deviceDTO.description = rs.getString("description");
            attendance_deviceDTO.synchronizationProcessCat = rs.getInt("synchronization_process_cat");
            attendance_deviceDTO.synchronizationDuration = rs.getInt("synchronization_duration");
            attendance_deviceDTO.deviceIpAddress = rs.getString("device_ip_address");
            attendance_deviceDTO.ftpIpAddress = rs.getString("ftp_ip_address");
            attendance_deviceDTO.ftpUsername = rs.getString("ftp_username");
            return attendance_deviceDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "attendance_device";
    }


    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Attendance_deviceDTO) commonDTO, addQuery, true);
    }

    public Attendance_deviceDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Attendance_deviceDTO) commonDTO, updateQuery, false);
    }

    public List<Attendance_deviceDTO> getAllAttendance_device(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }

    public List<Attendance_deviceDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }

    public List<Attendance_deviceDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
                                              String filter, boolean tableHasJobCat) {
        List<Object> objectList = new ArrayList<>();
        String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat, objectList);
        return getDTOs(sql, objectList);
    }

    public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                          UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList) {
        return getSearchQuery(tableName, p_searchCriteria, limit, offset, category, searchMap, objectList);
    }

    public String getDeviceName(long id, String language) {
        Attendance_deviceDTO dto = Attendance_deviceRepository.getInstance().getAttendanceDeviceDTOById(id);
        return dto == null ? "" : (language.equals("English") ? dto.deviceNameEn : dto.deviceNameBn);
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
                                           boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList) {
        return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat, objectList);
    }
}
	