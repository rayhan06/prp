package attendance_device;
import java.util.*; 
import util.*; 


public class Attendance_deviceDTO extends CommonDTO
{

	public long id = 0;
    public String deviceNameEn = "";
    public String deviceNameBn = "";
	public int deviceCat = 0;
    public String description = "";
	public int synchronizationProcessCat = 0;
	public int synchronizationDuration = 0;
    public String deviceIpAddress = "";
    public String ftpIpAddress = "";
    public String ftpUsername = "";
    public String ftpPassword = "";
	public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Attendance_deviceDTO[" +
            " id = " + id +
            " deviceNameEn = " + deviceNameEn +
            " deviceNameBn = " + deviceNameBn +
            " deviceCat = " + deviceCat +
            " description = " + description +
            " synchronizationProcessCat = " + synchronizationProcessCat +
            " synchronizationDuration = " + synchronizationDuration +
            " deviceIpAddress = " + deviceIpAddress +
            " ftpIpAddress = " + ftpIpAddress +
            " ftpUsername = " + ftpUsername +
            " ftpPassword = " + ftpPassword +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            "]";
    }

}