package attendance_device;

import com.google.gson.Gson;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;


@WebServlet("/Attendance_deviceServlet")
@MultipartConfig
public class Attendance_deviceServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Attendance_deviceServlet.class);
    private final Attendance_deviceDAO attendance_deviceDAO = Attendance_deviceDAO.getInstance();
    private final CommonRequestHandler commonRequestHandler = new CommonRequestHandler(attendance_deviceDAO);
    private final Gson gson = new Gson();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        logger.debug("In servlet, isPermanentTable = " + isPermanentTable);
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getAddPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ATTENDANCE_DEVICE_ADD)) {
                        commonRequestHandler.getAddPage(request, response);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "getEditPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ATTENDANCE_DEVICE_UPDATE)) {
                        getAttendance_device(request, response);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "getURL":
                    String URL = request.getParameter("URL");
                    logger.debug("URL = " + URL);
                    response.sendRedirect(URL);
                    break;
                case "search":
                    logger.debug("search requested");
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ATTENDANCE_DEVICE_SEARCH)) {
                        if (isPermanentTable) {
                            String filter = request.getParameter("filter");
                            logger.debug("filter = " + filter);
                            if (filter != null) {
                                filter = "";
                                searchAttendance_device(request, response, isPermanentTable, filter);
                            } else {
                                searchAttendance_device(request, response, isPermanentTable, "");
                            }
                        }
                    }
                    break;
                case "view":
                    logger.debug("view requested");
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ATTENDANCE_DEVICE_SEARCH)) {
                        commonRequestHandler.view(request, response);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                default:
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        logger.debug("In servlet, isPermanentTable = " + isPermanentTable);

        try {
            String actionType = request.getParameter("actionType");
            logger.debug("actionType = " + actionType);
            switch (actionType) {
                case "add":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ATTENDANCE_DEVICE_ADD)) {
                        logger.debug("going to  addAttendance_device ");
                        addAttendance_device(request, response, true, userDTO, true);
                    } else {
                        logger.debug("Not going to  addAttendance_device ");
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "getDTO":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ATTENDANCE_DEVICE_ADD)) {
                        getDTO(request, response);
                    } else {
                        logger.debug("Not going to  addAttendance_device ");
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "edit":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ATTENDANCE_DEVICE_UPDATE)) {
                        addAttendance_device(request, response, false, userDTO, isPermanentTable);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "delete":
                    deleteAttendance_device(request, response, userDTO);
                    break;
                case "search":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ATTENDANCE_DEVICE_SEARCH)) {
                        searchAttendance_device(request, response, true, "");
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "getGeo":
                    logger.debug("going to geoloc ");
                    request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
                    break;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getDTO(HttpServletRequest request, HttpServletResponse response) {
        try {
            Attendance_deviceDTO attendance_deviceDTO = attendance_deviceDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            String encoded = this.gson.toJson(attendance_deviceDTO);
            logger.debug("json encoded data = " + encoded);
            out.print(encoded);
            out.flush();
        } catch (Exception e) {
            logger.error(e);
        }

    }

    private void addAttendance_device(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) {
        try {
            request.setAttribute("failureMessage", "");
            Attendance_deviceDTO attendance_deviceDTO;
            if (addFlag) {
                attendance_deviceDTO = new Attendance_deviceDTO();
                attendance_deviceDTO.insertedBy = userDTO.userName;
                attendance_deviceDTO.insertionDate = new Date().getTime();
            } else {
                attendance_deviceDTO = attendance_deviceDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
            }
            attendance_deviceDTO.modifiedBy = userDTO.userName;
            attendance_deviceDTO.lastModificationTime = new Date().getTime();

            String Value;

            Value = request.getParameter("deviceNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("deviceNameEn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                attendance_deviceDTO.deviceNameEn = Value;
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("deviceNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("deviceNameBn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                attendance_deviceDTO.deviceNameBn = Value;
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("deviceCat");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("deviceCat = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                attendance_deviceDTO.deviceCat = Integer.parseInt(Value);
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("description");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("description = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                attendance_deviceDTO.description = Value;
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("synchronizationProcessCat");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("synchronizationProcessCat = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                attendance_deviceDTO.synchronizationProcessCat = Integer.parseInt(Value);
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("synchronizationDuration");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("synchronizationDuration = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                attendance_deviceDTO.synchronizationDuration = Integer.parseInt(Value);
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("deviceIpAddress");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("deviceIpAddress = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                attendance_deviceDTO.deviceIpAddress = Value;
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("ftpIpAddress");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("ftpIpAddress = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                attendance_deviceDTO.ftpIpAddress = Value;
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("ftpUsername");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("ftpUsername = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                attendance_deviceDTO.ftpUsername = Value;
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("ftpPassword");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("ftpPassword = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                attendance_deviceDTO.ftpPassword = Value;
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }
            

            Value = request.getParameter("searchColumn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("searchColumn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                attendance_deviceDTO.searchColumn = Value;
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

            List<Attendance_deviceDTO> attendanceDeviceDTOList = Attendance_deviceRepository.getInstance().getAttendanceDeviceDTOByDeviceCat(attendance_deviceDTO.deviceCat);

            boolean isDuplicate = false;

            for(Attendance_deviceDTO dto: attendanceDeviceDTOList) {
                if(dto.deviceNameBn.equalsIgnoreCase(attendance_deviceDTO.deviceNameEn)) {
                    isDuplicate = true;
                    break;
                }
            }

            if(isDuplicate) {
                response.sendRedirect("Attendance_deviceServlet?actionType=getAddPage&duplicate=true");
            } else {
                logger.debug("Done adding  addAttendance_device dto = " + attendance_deviceDTO);
                long returnedID;

                if (!isPermanentTable) {
                    attendance_deviceDAO.setIsDeleted(attendance_deviceDTO.iD, CommonDTO.OUTDATED);
                    returnedID = attendance_deviceDAO.add(attendance_deviceDTO);
                    attendance_deviceDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
                } else if (addFlag) {
                    returnedID = attendance_deviceDAO.manageWriteOperations(attendance_deviceDTO, SessionConstants.INSERT, -1, userDTO);
                } else {
                    returnedID = attendance_deviceDAO.manageWriteOperations(attendance_deviceDTO, SessionConstants.UPDATE, -1, userDTO);
                }


                if (isPermanentTable) {
                    String inPlaceSubmit = request.getParameter("inplacesubmit");

                    if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                        getAttendance_device(request, response, returnedID);
                    } else {
                        response.sendRedirect("Attendance_deviceServlet?actionType=search");
                    }
                } else {
                    commonRequestHandler.validate(attendance_deviceDAO.getDTOByID(returnedID), request, response, userDTO);
                }

            }


        } catch (Exception e) {
            logger.error(e);
        }
    }


    private void deleteAttendance_device(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
        try {
            String[] IDsToDelete = request.getParameterValues("ID");
            for (String s : IDsToDelete) {
                long id = Long.parseLong(s);
                logger.debug("------ DELETING " + s);
                Attendance_deviceDTO attendance_deviceDTO = attendance_deviceDAO.getDTOByID(id);
                attendance_deviceDAO.manageWriteOperations(attendance_deviceDTO, SessionConstants.DELETE, id, userDTO);
            }
            response.sendRedirect("Attendance_deviceServlet?actionType=search");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void getAttendance_device(HttpServletRequest request, HttpServletResponse response, long id) {
        try {
            Attendance_deviceDTO attendance_deviceDTO = attendance_deviceDAO.getDTOByID(id);
            request.setAttribute("ID", attendance_deviceDTO.iD);
            request.setAttribute("attendance_deviceDTO", attendance_deviceDTO);
            request.setAttribute("attendance_deviceDAO", attendance_deviceDAO);

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            String URL;
            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "attendance_device/attendance_deviceInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "attendance_device/attendance_deviceSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "attendance_device/attendance_deviceEditBody.jsp?actionType=edit";
                } else {
                    URL = "attendance_device/attendance_deviceEdit.jsp?actionType=edit";
                }
            }
            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (Exception e) {
            logger.error(e);
        }
    }


    private void getAttendance_device(HttpServletRequest request, HttpServletResponse response) {
        getAttendance_device(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void searchAttendance_device(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        logger.debug("in  searchAttendance_device 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        logger.debug("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_ATTENDANCE_DEVICE,
                request,
                attendance_deviceDAO,
                SessionConstants.VIEW_ATTENDANCE_DEVICE,
                SessionConstants.SEARCH_ATTENDANCE_DEVICE,
                attendance_deviceDAO.getTableName(),
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            logger.debug("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            logger.debug("failed to dojob" + e);
        }

        request.setAttribute("attendance_deviceDAO", attendance_deviceDAO);
        RequestDispatcher rd;
        if (!isPermanent) {
            if (!hasAjax) {
                logger.debug("Going to attendance_device/attendance_deviceApproval.jsp");
                rd = request.getRequestDispatcher("attendance_device/attendance_deviceApproval.jsp");
            } else {
                logger.debug("Going to attendance_device/attendance_deviceApprovalForm.jsp");
                rd = request.getRequestDispatcher("attendance_device/attendance_deviceApprovalForm.jsp");
            }
        } else {
            if (!hasAjax) {
                logger.debug("Going to attendance_device/attendance_deviceSearch.jsp");
                rd = request.getRequestDispatcher("attendance_device/attendance_deviceSearch.jsp");
            } else {
                logger.debug("Going to attendance_device/attendance_deviceSearchForm.jsp");
                rd = request.getRequestDispatcher("attendance_device/attendance_deviceSearchForm.jsp");
            }
        }
        rd.forward(request, response);
    }

}

