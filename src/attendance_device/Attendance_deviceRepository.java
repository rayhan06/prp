package attendance_device;

import employee_attendance_device.Employee_attendance_deviceDTO;
import employee_attendance_device.Employee_attendance_deviceRepository;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class Attendance_deviceRepository implements Repository {
    private static final Logger logger = Logger.getLogger(Attendance_deviceRepository.class);
    private final Attendance_deviceDAO attendanceDeviceDAO;
    private final Map<Long, Attendance_deviceDTO> mapById;
    private final Map<Integer, List<Attendance_deviceDTO>> mapByDeviceCat;
    private final Map<Integer, List<Attendance_deviceDTO>> mapBySynchronizationProcessCat;
    private List<Attendance_deviceDTO> attendanceDeviceDTOList;

    private Attendance_deviceRepository() {
        mapById = new ConcurrentHashMap<>();
        mapByDeviceCat = new ConcurrentHashMap<>();
        mapBySynchronizationProcessCat = new ConcurrentHashMap<>();
        attendanceDeviceDAO = Attendance_deviceDAO.getInstance();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        static final Attendance_deviceRepository INSTANCE = new Attendance_deviceRepository();
    }

    public synchronized static Attendance_deviceRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        List<Attendance_deviceDTO> dtoList = attendanceDeviceDAO.getAllAttendance_device(reloadAll);
        dtoList.stream()
                .peek(this::removeIfPresent)
                .filter(dto -> dto.isDeleted == 0)
                .forEach(dto -> {
                    mapById.put(dto.id, dto);
                    List<Attendance_deviceDTO> deviceList = mapByDeviceCat.getOrDefault(dto.deviceCat, new ArrayList<>());
                    deviceList.add(dto);
                    mapByDeviceCat.put(dto.deviceCat, deviceList);
                    List<Attendance_deviceDTO> synchronizationProcessList = mapBySynchronizationProcessCat.getOrDefault(dto.synchronizationProcessCat, new ArrayList<>());
                    synchronizationProcessList.add(dto);
                    mapBySynchronizationProcessCat.put(dto.synchronizationProcessCat, synchronizationProcessList);
                });
        attendanceDeviceDTOList = new ArrayList<>(mapById.values());
        attendanceDeviceDTOList.sort(Comparator.comparing(o -> o.id));
    }

    private void removeIfPresent(Attendance_deviceDTO dto) {
        if (dto != null) {
            Attendance_deviceDTO oldDTO = mapById.get(dto.id);
            if (oldDTO != null) {
                if (mapByDeviceCat.get(oldDTO.deviceCat) != null) {
                    mapByDeviceCat.get(oldDTO.deviceCat).remove(oldDTO);
                }
                if (mapBySynchronizationProcessCat.get(oldDTO.synchronizationProcessCat) != null) {
                    mapBySynchronizationProcessCat.get(oldDTO.synchronizationProcessCat).remove(oldDTO);
                }
                mapById.remove(oldDTO.id);
            }
        }
    }

    public List<Attendance_deviceDTO> getAttendanceDeviceList() {
        return attendanceDeviceDTOList == null ? new ArrayList<>() : attendanceDeviceDTOList;
    }

    public Attendance_deviceDTO getAttendanceDeviceDTOById(long id) {
        return mapById.get(id);
    }

    public List<Attendance_deviceDTO> getAttendanceDeviceDTOByDeviceCat(int deviceCat) {
        return mapByDeviceCat.getOrDefault(deviceCat, new ArrayList<>());
    }

    public List<Attendance_deviceDTO> getAttendanceDeviceDTOBySynchronizationProcessCat(int synchronizationProcessCat) {
        return mapBySynchronizationProcessCat.getOrDefault(synchronizationProcessCat, new ArrayList<>());
    }

    public String buildOptions(String language, Long selectedId) {
        return buildOptions(language, selectedId, false);
    }

    public String buildOptionsWithoutSelectOption(String language) {
        return buildOptions(language, null, true);
    }

    private String buildOptions(String language, Long selectedId, boolean withoutSelectOption) {
        List<OptionDTO> optionDTOList = null;
        if (attendanceDeviceDTOList != null && attendanceDeviceDTOList.size() > 0) {
            optionDTOList = attendanceDeviceDTOList.stream()
                    .map(dto -> new OptionDTO(dto.deviceNameEn, dto.deviceNameBn, String.valueOf(dto.id)))
                    .collect(Collectors.toList());
        }
        if (withoutSelectOption) {
            return Utils.buildOptionsWithoutSelectOption(optionDTOList, language);
        }
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String buildOptionsForEmployee(String language, Long selectedId, Long employeeRecordsId) {

        List<Employee_attendance_deviceDTO> employeeDevices = Employee_attendance_deviceRepository.getInstance().getEmployee_attendance_deviceDTOByemployee_records_id(employeeRecordsId);

        Set<Long> occupiedDeviceIds = employeeDevices.stream()
                       .map(dto -> dto.attendanceDeviceId)
                       .collect(Collectors.toSet());

        List<OptionDTO> optionDTOList = null;

        if (attendanceDeviceDTOList != null && attendanceDeviceDTOList.size() > 0) {

            optionDTOList = attendanceDeviceDTOList.stream()
                    .filter(dto -> !occupiedDeviceIds.contains(dto.id))
                    .map(dto -> new OptionDTO(dto.deviceNameEn, dto.deviceNameBn, String.valueOf(dto.id)))
                    .collect(Collectors.toList());
        }

        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    @Override
    public String getTableName() {
        return "attendance_device";
    }
}


