package vaccine_entry;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;

import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;
import javax.servlet.http.*;

import common.BaseServlet;
import com.google.gson.Gson;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Vaccine_entryServlet
 */
@WebServlet("/Vaccine_entryServlet")
@MultipartConfig
public class Vaccine_entryServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Vaccine_entryServlet.class);

    @Override
    public String getTableName() {
        return Vaccine_entryDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Vaccine_entryServlet";
    }

    @Override
    public Vaccine_entryDAO getCommonDAOService() {
        return Vaccine_entryDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.VACCINE_ENTRY_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.VACCINE_ENTRY_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.VACCINE_ENTRY_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Vaccine_entryServlet.class;
    }
    private final Gson gson = new Gson();
 	

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addVaccine_entry");
		String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		Vaccine_entryDTO vaccine_entryDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
		if(addFlag)
		{
			vaccine_entryDTO = new Vaccine_entryDTO();
		}
		else
		{
			vaccine_entryDTO = Vaccine_entryDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		Value = request.getParameter("vaccineCount");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("vaccineCount = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			vaccine_entryDTO.vaccineCount = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("description");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("description = " + Value);
		if(Value != null)
		{
			vaccine_entryDTO.description = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		vaccine_entryDTO.lastModifierUser = userDTO.userName;
		
		
		Value = request.getParameter("entryDate");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("entryDate = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			try 
			{
				Date d = f.parse(Value);
				vaccine_entryDTO.entryDate = d.getTime();
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		if(addFlag)
		{
			vaccine_entryDTO.insertedByUserId = userDTO.ID;		
			vaccine_entryDTO.insertedByOrganogramId = userDTO.organogramID;
			vaccine_entryDTO.insertedByEmployeeId = userDTO.employee_record_id;					
			vaccine_entryDTO.insertionDate = TimeConverter.getToday();
		}			

		System.out.println("Done adding  addVaccine_entry dto = " + vaccine_entryDTO);

		if(addFlag == true)
		{
			Vaccine_entryDAO.getInstance().add(vaccine_entryDTO);
		}
		else
		{				
			Vaccine_entryDAO.getInstance().update(vaccine_entryDTO);										
		}
		
		

		return vaccine_entryDTO;

	}
}

