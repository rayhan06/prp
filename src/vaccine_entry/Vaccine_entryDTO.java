package vaccine_entry;

import util.*; 


public class Vaccine_entryDTO extends CommonDTO
{

	public int vaccineCount = 0;
    public String description = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertedByEmployeeId = -1;
	public long insertionDate = -1;
	public long entryDate = TimeConverter.getToday();
	public String lastModifierUser = "";
	
	
    @Override
	public String toString() {
            return "$Vaccine_entryDTO[" +
            " iD = " + iD +
            " vaccineCount = " + vaccineCount +
            " description = " + description +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertedByEmployeeId = " + insertedByEmployeeId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}