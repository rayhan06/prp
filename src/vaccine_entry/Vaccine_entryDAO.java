package vaccine_entry;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;
import util.*;
import workflow.WorkflowController;
import pb.*;

import user.UserDTO;
import user.UserRepository;

public class Vaccine_entryDAO  implements CommonDAOService<Vaccine_entryDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Vaccine_entryDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"search_column",
			"vaccine_count",
			"description",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"inserted_by_employee_id",
			"insertion_date",
			"last_modifier_user",
			"entry_date",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("inserted_by_organogram_id"," and (inserted_by_organogram_id = ?)");
		searchMap.put("insertion_date_start"," and (entry_date >= ?)");
		searchMap.put("insertion_date_end"," and (entry_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}
	
	public int getCount (long startDate, long endDate)
    {
		String sql = "select SUM(vaccine_count) as count from"
				+ " vaccine_entry"
				+ " where isDeleted = 0" 
				+ "  and entry_date>= " + startDate
				+ "  and entry_date <= " + endDate;	
		return ConnectionAndStatementUtil.getT(sql,this::getRawCount, 0);	
    }
	
	public int getRawCount(ResultSet rs)
	{
		int count = 0;
		try
		{
			count = rs.getInt("count");

			return count;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return 0;
		}
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Vaccine_entryDAO INSTANCE = new Vaccine_entryDAO();
	}

	public static Vaccine_entryDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Vaccine_entryDTO vaccine_entryDTO)
	{
		vaccine_entryDTO.searchColumn = "";
		try {
			UserDTO nurseDTO = UserRepository.getUserDTOByUserID(vaccine_entryDTO.insertedByUserId);
			vaccine_entryDTO.searchColumn += WorkflowController.getNameFromEmployeeRecordID(vaccine_entryDTO.insertedByEmployeeId, "bangla") 
					+ " " + WorkflowController.getNameFromEmployeeRecordID(vaccine_entryDTO.insertedByEmployeeId, "english")
					;
			if(nurseDTO != null)
			{
				vaccine_entryDTO.searchColumn += " " + nurseDTO.userName + " " + Utils.getDigitBanglaFromEnglish(nurseDTO.userName);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void set(PreparedStatement ps, Vaccine_entryDTO vaccine_entryDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(vaccine_entryDTO);
		if(isInsert)
		{
			ps.setObject(++index,vaccine_entryDTO.iD);
		}
		ps.setObject(++index,vaccine_entryDTO.searchColumn);
		ps.setObject(++index,vaccine_entryDTO.vaccineCount);
		ps.setObject(++index,vaccine_entryDTO.description);
		ps.setObject(++index,vaccine_entryDTO.insertedByUserId);
		ps.setObject(++index,vaccine_entryDTO.insertedByOrganogramId);
		ps.setObject(++index,vaccine_entryDTO.insertedByEmployeeId);
		ps.setObject(++index,vaccine_entryDTO.insertionDate);
		ps.setObject(++index,vaccine_entryDTO.lastModifierUser);
		ps.setObject(++index,vaccine_entryDTO.entryDate);
		if(isInsert)
		{
			ps.setObject(++index,vaccine_entryDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,vaccine_entryDTO.iD);
		}
	}
	
	@Override
	public Vaccine_entryDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Vaccine_entryDTO vaccine_entryDTO = new Vaccine_entryDTO();
			int i = 0;
			vaccine_entryDTO.iD = rs.getLong(columnNames[i++]);
			vaccine_entryDTO.searchColumn = rs.getString(columnNames[i++]);
			vaccine_entryDTO.vaccineCount = rs.getInt(columnNames[i++]);
			vaccine_entryDTO.description = rs.getString(columnNames[i++]);
			vaccine_entryDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			vaccine_entryDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			vaccine_entryDTO.insertedByEmployeeId = rs.getLong(columnNames[i++]);
			vaccine_entryDTO.insertionDate = rs.getLong(columnNames[i++]);
			vaccine_entryDTO.lastModifierUser = rs.getString(columnNames[i++]);
			vaccine_entryDTO.entryDate = rs.getLong(columnNames[i++]);
			vaccine_entryDTO.isDeleted = rs.getInt(columnNames[i++]);
			vaccine_entryDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return vaccine_entryDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Vaccine_entryDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "vaccine_entry";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Vaccine_entryDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Vaccine_entryDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	