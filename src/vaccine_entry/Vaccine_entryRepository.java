package vaccine_entry;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Vaccine_entryRepository implements Repository {
	Vaccine_entryDAO vaccine_entryDAO = null;
	
	static Logger logger = Logger.getLogger(Vaccine_entryRepository.class);
	Map<Long, Vaccine_entryDTO>mapOfVaccine_entryDTOToiD;
	Gson gson;

  
	private Vaccine_entryRepository(){
		vaccine_entryDAO = Vaccine_entryDAO.getInstance();
		mapOfVaccine_entryDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Vaccine_entryRepository INSTANCE = new Vaccine_entryRepository();
    }

    public static Vaccine_entryRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Vaccine_entryDTO> vaccine_entryDTOs = vaccine_entryDAO.getAllDTOs(reloadAll);
			for(Vaccine_entryDTO vaccine_entryDTO : vaccine_entryDTOs) {
				Vaccine_entryDTO oldVaccine_entryDTO = getVaccine_entryDTOByiD(vaccine_entryDTO.iD);
				if( oldVaccine_entryDTO != null ) {
					mapOfVaccine_entryDTOToiD.remove(oldVaccine_entryDTO.iD);
				
					
				}
				if(vaccine_entryDTO.isDeleted == 0) 
				{
					
					mapOfVaccine_entryDTOToiD.put(vaccine_entryDTO.iD, vaccine_entryDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Vaccine_entryDTO clone(Vaccine_entryDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Vaccine_entryDTO.class);
	}
	
	
	public List<Vaccine_entryDTO> getVaccine_entryList() {
		List <Vaccine_entryDTO> vaccine_entrys = new ArrayList<Vaccine_entryDTO>(this.mapOfVaccine_entryDTOToiD.values());
		return vaccine_entrys;
	}
	
	public List<Vaccine_entryDTO> copyVaccine_entryList() {
		List <Vaccine_entryDTO> vaccine_entrys = getVaccine_entryList();
		return vaccine_entrys
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Vaccine_entryDTO getVaccine_entryDTOByiD( long iD){
		return mapOfVaccine_entryDTOToiD.get(iD);
	}
	
	public Vaccine_entryDTO copyVaccine_entryDTOByiD( long iD){
		return clone(mapOfVaccine_entryDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return vaccine_entryDAO.getTableName();
	}
}


