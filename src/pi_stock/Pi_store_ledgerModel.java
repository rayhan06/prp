package pi_stock;

public class Pi_store_ledgerModel {
    public String date = "";
    public String user = "";
    public String identificationNumber = "";
    public String transactionType = "";
    public long received = 0;
    public long issued = 0;
    public long balance = 0;
}
