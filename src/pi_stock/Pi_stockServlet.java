package pi_stock;

import com.google.gson.Gson;
import employee_offices.EmployeeOfficeRepository;
import fiscal_year.Fiscal_yearRepository;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pb.Utils;
import pbReport.DateUtils;
import permission.MenuConstants;
import pi_product_receive.Pi_product_receive_childDAO;
import pi_product_receive.Pi_product_receive_childDTO;
import pi_unique_item.*;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.StringUtils;
import util.UtilCharacter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@WebServlet("/Pi_stockServlet")
@MultipartConfig
public class Pi_stockServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_stockServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        long officeUnitId = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(userDTO.employee_record_id).officeUnitId;
        logger.debug("office Unit Id " + officeUnitId);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "ajax_getStockData": {
                    long fiscalYearId = Long.parseLong(request.getParameter("fiscalYearId"));
                    long itemId = Long.parseLong(request.getParameter("itemId"));
                    long fiscalFirstDate = Fiscal_yearRepository.getInstance().getFirstDateofFiscalYear(fiscalYearId);
                    long fiscalLastDate = Fiscal_yearRepository.getInstance().getLastDateofFiscalYear(fiscalYearId);
                    String startDateStr = request.getParameter("startDate");
                    String endDateStr = request.getParameter("endDate");
                    long startDate = (startDateStr == null || startDateStr.equals("")) ? -1 : Long.parseLong(request.getParameter("startDate"));
                    long endDate = (endDateStr == null || endDateStr.equals("")) ? -1 : Long.parseLong(request.getParameter("endDate"));
                    if (!(startDate >= fiscalFirstDate && startDate <= fiscalLastDate)) startDate = fiscalFirstDate;
                    if (!(endDate >= fiscalFirstDate && endDate <= fiscalLastDate)) endDate = fiscalLastDate;

                    fiscalFirstDate = startDate;
                    fiscalLastDate = endDate;

                    String storeOfficeUnitIdStr = request.getParameter("storeOfficeUnitId");
                    long storeOfficeUnitId = (storeOfficeUnitIdStr == null || storeOfficeUnitIdStr.equals("")) ? -1 :
                            Long.parseLong(request.getParameter("storeOfficeUnitId"));
//                    List<Pi_purchaseDTO> dtoList = Pi_purchaseDAO.getInstance().getDTOsForStockModel(storeOfficeUnitId, fiscalYearId, itemId);
                    List<Pi_product_receive_childDTO> dtoList = Pi_product_receive_childDAO.getInstance()
                            .getDTOsForStockModel(storeOfficeUnitId, fiscalYearId, itemId);
                    List<Pi_monthlyReturnModel> returnedDtoList = PiUniqueItemAssignmentDAO.getInstance().getReturnCountInDateRange(storeOfficeUnitId, itemId, fiscalFirstDate, fiscalLastDate);
                    List<Pi_stockModel> modelList = dtoList.stream().map(dto ->
                                    new Pi_stockModel(dto, storeOfficeUnitId, fiscalYearId, language))
                            .collect(Collectors.toList());
                    int initialStock = PiUniqueItemAssignmentDAO.getInstance()
                            .getStockCountBeforeFiscalYear(storeOfficeUnitId, fiscalYearId, itemId, fiscalFirstDate);
//                    int maxMonthIndex = getMaxMonthIndex(fiscalFirstDate);
                    int maxMonthIndex = getMaxMonthIndexBetweenRange(fiscalFirstDate, fiscalLastDate);
                    //setStockAmountData(modelList, officeUnitId, fiscalYearId, itemId, language);
                    long totalAmount = 0;
                    long totalDistibution = 0;
                    totalAmount = initialStock;
                    modelList = getMonthByModels(modelList, language);
                    Map<Integer, Pi_stockModel> monthModelMap = new HashMap<>();
                    Map<Integer, Pi_monthlyReturnModel> monthReturnModelMap = new HashMap<>();
                    for (Pi_stockModel model : modelList) {
                        if (monthModelMap.containsKey(model.monthSerial)) {
                            Pi_stockModel existingModel = monthModelMap.get(model.monthSerial);
                            model.purchaseAmountLong += existingModel.purchaseAmountLong;
                            model.purchaseAmount = Utils.getDigits(model.purchaseAmountLong, language);
                        }
                        monthModelMap.put(model.monthSerial, model);
                    }
                    for (Pi_monthlyReturnModel model : returnedDtoList) {
                        monthReturnModelMap.put(model.monthSerial, model);
                    }
                    List<Pi_stockModel> newModelList = new ArrayList<>();
                    int firstMonth = DateUtils.getMonth(fiscalFirstDate);
                    int year = Fiscal_yearRepository.getInstance().getFirstYear(fiscalYearId);
                    for (int i = 0; i < maxMonthIndex; i++) {
//                        int month = (i + Calendar.JULY) % 12;
                        int month = (i + firstMonth) % 12;
                        if (month == Calendar.JANUARY) {
                            year++;
                        }
                        Pi_monthlyReturnModel pi_monthlyReturnModel = monthReturnModelMap.get(month);
                        Pi_stockModel pi_stockModel = monthModelMap.get(month);
                        if (pi_stockModel == null) {
                            pi_stockModel = new Pi_stockModel(month, language);
                        }
                        long returnedCount = pi_monthlyReturnModel != null ? pi_monthlyReturnModel.returnAmountLong : 0;
                        /* we shall not add return count. Because, same item can be distributed and returned to store
                         * multiple times in the same month.
                         * pi_stockModel.purchaseAmountLong += returnedCount;
                         */
                        pi_stockModel.purchaseAmountLong += 0;
                        pi_stockModel.initialStockLong = PiUniqueItemAssignmentDAO.getInstance()
                                .getInStockCountInDateRange(storeOfficeUnitId, itemId
                                        , getFirstDayOfMonth(year, month), getFirstDayOfMonth(year, month));
                        pi_stockModel.distributedAmountLong = PiUniqueItemAssignmentDAO.getInstance()
                                .getDistributionCountInDateRange(storeOfficeUnitId, itemId
                                        , getFirstDayOfMonth(year, month), getLastDayOfMonth(year, month));
                        pi_stockModel.remainingAmountLong = PiUniqueItemAssignmentDAO.getInstance()
                                .getInStockCountInDateRange(storeOfficeUnitId, itemId
                                        , getLastDayOfMonth(year, month), getLastDayOfMonth(year, month));
                        initialStock = (int) pi_stockModel.remainingAmountLong;
                        totalAmount += pi_stockModel.purchaseAmountLong;
                        totalDistibution += pi_stockModel.distributedAmountLong;
                        pi_stockModel.setAmounts(language);
                        newModelList.add(pi_stockModel);
                    }
                    Map<String, Object> res = new HashMap<>();
                    res.put("stockModels", newModelList);
                    res.put("totalStock", totalAmount);
                    res.put("totalDistribution", totalDistibution);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().println(new Gson().toJson(res));
                    return;
                }
                case "view": {
                    if (Utils.checkPermission(userDTO, MenuConstants.PI_STOCK_VIEW_BY_MONTH)) {
                        request.getRequestDispatcher("pi_stock/pi_stockView.jsp").forward(request, response);
                    }
                    return;
                }

                case "getStoreLedgerView": {
                    if (Utils.checkPermission(userDTO, MenuConstants.PI_STOCK_VIEW_BY_STORE_LEDGER)) {
                        request.getRequestDispatcher("pi_stock/pi_stock_StoreLedgerView.jsp").forward(request, response);
                    }
                    return;
                }
                case "getStoreLedgerViewBodyTable": {
                    if (Utils.checkPermission(userDTO, MenuConstants.PI_STOCK_VIEW_BY_STORE_LEDGER)) {
                        long itemId = -1;
                        long startDate = 0;
                        long endDate = 4669831200660L;

                        String value = "";
                        value = request.getParameter("itemId");
                        if (StringUtils.isValidString(value)) {
                            itemId = Long.parseLong(value);
                        } else {
                            UtilCharacter.throwException("আইটেম পাওয়া যায় নি!", "Item Not Found!");
                        }

                        value = request.getParameter("startDate");
                        if (StringUtils.isValidString(value)) {
                            startDate = Long.parseLong(value);
                        }

                        value = request.getParameter("endDate");
                        if (StringUtils.isValidString(value)) {
                            endDate = Long.parseLong(value);
                        }

                        List<Pi_store_ledgerModel> storeLedgerModels = Pi_unique_item_transactionDAO
                                .getInstance().getDTOsForStoreLedger(itemId, startDate, endDate);
                        request.setAttribute("storeLedgerModels", storeLedgerModels);
                        request.getRequestDispatcher("pi_stock/pi_stock_StoreLedgerViewBodyTable.jsp").forward(request, response);
                    }
                    return;
                }
                case "getSummarizedStockReportView": {
                    if (Utils.checkPermission(userDTO, MenuConstants.PI_STOCK_VIEW_BY_SUMMARY_REPORT)) {
                        request.getRequestDispatcher("pi_stock/pi_stock_SummarizedStockReportView.jsp").forward(request, response);
                    }
                    return;
                }
                case "getSummarizedStockReportViewBodyTable": {
                    if (Utils.checkPermission(userDTO, MenuConstants.PI_STOCK_VIEW_BY_SUMMARY_REPORT)) {
                        long itemGroupId = -1;
                        long itemTypeId = -1;
                        long itemId = -1;
                        long startDate = 0;
                        long endDate = 4669831200660L;

                        String value = "";
                        value = request.getParameter("itemGroupId");
                        if (StringUtils.isValidString(value)) {
                            itemGroupId = Long.parseLong(value);
                        }

                        value = request.getParameter("itemTypeId");
                        if (StringUtils.isValidString(value)) {
                            itemTypeId = Long.parseLong(value);
                        }

                        value = request.getParameter("itemId");
                        if (StringUtils.isValidString(value)) {
                            itemId = Long.parseLong(value);
                        }

                        value = request.getParameter("startDate");
                        if (StringUtils.isValidString(value)) {
                            startDate = Long.parseLong(value);
                        }

                        value = request.getParameter("endDate");
                        if (StringUtils.isValidString(value)) {
                            endDate = Long.parseLong(value);
                        }

                        List<Pi_stock_summaryModel> stockSummaryModels = PiUniqueItemAssignmentDAO
                                .getInstance().getDTOsForStockSummaryTable(itemGroupId, itemTypeId, itemId, startDate, endDate);
                        request.setAttribute("stockSummaryModels", stockSummaryModels);

                        request.getRequestDispatcher("pi_stock/pi_stock_SummarizedStockReportViewBodyTable.jsp").forward(request, response);
                    }
                    return;
                }
                default:
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void setStockAmountData(List<Pi_stockModel> modelList, long officeUnitId, long fiscalYearId, long itemId, String language) {
        if (modelList == null || modelList.size() == 0)
            return;
        Pi_stockModel tmpModel = modelList.get(modelList.size() - 1);
        List<PiUniqueItemAssignmentDTO> assignmentDTOS = PiUniqueItemAssignmentDAO.getInstance().getDTOsBeforeDate(officeUnitId, fiscalYearId, itemId, getLastDayOfMonth(tmpModel.purchaseDateLong));
        long stockAmount = assignmentDTOS.stream().filter(dto -> (dto.actionType == PiActionTypeEnum.PURCHASE.getValue() || dto.stage == PiStageEnum.IN_STOCK.getValue())
                && dto.purchaseDate < modelList.get(0).purchaseDateLong).count();
//        long fiscalFirstDate= Fiscal_yearRepository.getInstance().getFirstDateofFiscalYear(fiscalYearId);
//        long stockAmount = PiUniqueItemAssignmentDAO.getInstance().getStockCountBeforeFiscalYear(officeUnitId, fiscalYearId, itemId, fiscalFirstDate);
        long prevPurchaseDate = SessionConstants.MIN_DATE;
        for (Pi_stockModel stockModel : modelList) {
            stockModel.prevPurchaseDate = prevPurchaseDate;
            stockModel.initialStockLong = stockAmount;
//            stockModel.distributedAmountLong = assignmentDTOS.stream().filter(dto -> dto.actionType == PiActionTypeEnum.DELIVERY.getValue() && dto.stage != PiStageEnum.IN_STOCK.getValue() &&
//                    dto.lastModificationTime < getLastDayOfMonth(stockModel.purchaseDateLong)).count();
            stockModel.distributedAmountLong = assignmentDTOS.stream().filter(dto -> dto.actionType == PiActionTypeEnum.PURCHASE.getValue() && dto.stage != PiStageEnum.IN_STOCK.getValue() &&
                    dto.lastModificationTime < getLastDayOfMonth(stockModel.purchaseDateLong)).count();
            stockModel.remainingAmountLong = stockModel.initialStockLong + stockModel.purchaseAmountLong - stockModel.distributedAmountLong;
            stockAmount = stockModel.initialStockLong + stockModel.purchaseAmountLong;
            prevPurchaseDate = stockModel.purchaseDateLong;
            stockModel.setAmounts(language);
        }
    }

    private List<Pi_stockModel> getMonthByModels(List<Pi_stockModel> modelList, String language) {
        ArrayList<Pi_stockModel> monthByModels = new ArrayList<>();
        if (modelList.isEmpty())
            return monthByModels;
        Pi_stockModel curModel = modelList.get(0);
        int curMonth = modelList.get(0).monthSerial;
        for (int i = 1; i < modelList.size(); i++) {
            Pi_stockModel stockModel = modelList.get(i);
            if (stockModel.monthSerial == curMonth) {
                curModel.purchaseAmountLong += stockModel.purchaseAmountLong;
            } else {
                curModel.setAmounts(language);
                monthByModels.add(curModel);
                curModel = stockModel;
                curMonth = stockModel.monthSerial;
            }
        }
        curModel.setAmounts(language);
        monthByModels.add(curModel);
        return monthByModels;
    }

    private long getLastDayOfMonth(long date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        int curYear = calendar.get(Calendar.YEAR);
        int curMonth = calendar.get(Calendar.MONTH);
        int nextYear = curYear;
        if (curMonth > Calendar.DECEMBER) {
            nextYear++;
        }
        int nextMonth = (curMonth + 1) % 12;
        Calendar nextCalendar = Calendar.getInstance();
        nextCalendar.set(Calendar.DAY_OF_MONTH, 1);
        nextCalendar.set(Calendar.MONTH, nextMonth);
        nextCalendar.set(Calendar.YEAR, nextYear);
        nextCalendar.set(Calendar.HOUR_OF_DAY, 0);
        nextCalendar.set(Calendar.MINUTE, 0);
        nextCalendar.set(Calendar.SECOND, 0);
        return nextCalendar.getTimeInMillis();
    }

    private long getFirstDayOfMonth(int year, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTimeInMillis();
    }

    private long getLastDayOfMonth(int year, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(getFirstDayOfMonth(year, month));
        calendar.add(Calendar.MONTH, 1);
        return calendar.getTimeInMillis();
    }

    private int getMaxMonthIndex(long fiscalFirstDate) {
        Calendar fiscalLastDateCalendar = Calendar.getInstance();
        fiscalLastDateCalendar.setTimeInMillis(fiscalFirstDate);
        fiscalLastDateCalendar.add(Calendar.YEAR, 1);
        Calendar currrentCalendar = Calendar.getInstance();
        if (fiscalLastDateCalendar.getTimeInMillis() < currrentCalendar.getTimeInMillis()) {
            return 12;
        } else {
            int curMonth = DateUtils.getMonth(currrentCalendar.getTimeInMillis());
            return curMonth >= Calendar.JULY ? (curMonth - Calendar.JULY) + 1 : (curMonth + 12) - Calendar.JULY + 1;
        }
    }

    private int getMaxMonthIndexBetweenRange(long fiscalFirstDate, long fiscalLastDate) {
        int month = 0;
        int firstMonth = DateUtils.getMonth(fiscalFirstDate);
        int lastMonth = DateUtils.getMonth(fiscalLastDate);
        if (lastMonth >= firstMonth) {
            month = lastMonth - firstMonth + 1;
        } else {
            month = lastMonth + 12 - firstMonth + 1;
        }
        return month;
    }
}
