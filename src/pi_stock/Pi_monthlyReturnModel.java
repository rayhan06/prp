package pi_stock;

public class Pi_monthlyReturnModel {
    public String monthName = "";
    public int monthSerial=-1;
    public String returnAmount = "0";
    public long returnAmountLong = 0;

    @Override
    public String toString() {
        return "Pi_stockModel{" +
                "monthSerial='" + monthSerial + '\'' +
                ", returnAmount='" + returnAmount + '\'' +
                ", returnAmountLong=" + returnAmountLong +
                '}';
    }
}
