package pi_stock;

public class Pi_stock_summaryModel {
    public long itemId = -1;
    public String itemName = "";
    public long openingBalance = 0;
    public long receivedQuantity = 0;
    public long totalStock = 0;
    public long issued = 0;
    public long closingBalance = 0;
}
