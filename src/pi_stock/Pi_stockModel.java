package pi_stock;

import pb.Utils;
import pbReport.DateUtils;
import pi_product_receive.Pi_product_receive_childDTO;
import pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsRepository;
import sessionmanager.SessionConstants;
import util.StringUtils;

import java.util.Calendar;

public class Pi_stockModel {
    String monthName = "";
    int monthSerial=-1;
    String initialStock = "0";
    String vendorName = "";
    String purchaseDate = "";
    String purchaseAmount = "0";
    String distributedAmount = "0";
    String remainingAmount = "0";
    String expiryDate = "";
    long purchaseDateLong = SessionConstants.MIN_DATE;
    long initialStockLong = 0;
    long purchaseAmountLong = 0;
    long distributedAmountLong = 0;
    long remainingAmountLong = 0;
    long prevPurchaseDate=SessionConstants.MIN_DATE;

    public Pi_stockModel(Pi_product_receive_childDTO productReceiveChildDTO, long officeUnitId, long fiscalYearId, String language) {
        Calendar calendar = Calendar.getInstance();
        /*receiveDate used instead of purchaseDate. Because according to business logic,
        stock count update after product receive */
        calendar.setTimeInMillis(productReceiveChildDTO.receiveDate);
        monthSerial=calendar.get(Calendar.MONTH);
        monthName = DateUtils.getMonthName(calendar.get(Calendar.MONTH), language);
        if (Pi_vendor_auctioneer_detailsRepository.getInstance().
                getPi_vendor_auctioneer_detailsDTOByiD(productReceiveChildDTO.vendorId) != null) {
            vendorName = language.equals("English") ?
                    Pi_vendor_auctioneer_detailsRepository.getInstance().
                            getPi_vendor_auctioneer_detailsDTOByiD(productReceiveChildDTO.vendorId).nameEn :
                    Pi_vendor_auctioneer_detailsRepository.getInstance().
                            getPi_vendor_auctioneer_detailsDTOByiD(productReceiveChildDTO.vendorId).nameBn;
        }
        purchaseDateLong=productReceiveChildDTO.receiveDate;
        purchaseDate = StringUtils.getFormattedDate(language, productReceiveChildDTO.receiveDate);
        purchaseAmountLong = (int) productReceiveChildDTO.quantity;
        purchaseAmount = Utils.getDigits(productReceiveChildDTO.quantity, language);
        expiryDate = StringUtils.getFormattedDate(language, productReceiveChildDTO.expiryDate);
    }

    public Pi_stockModel(int monthSerial,String language){
        this.monthSerial=monthSerial;
        this.monthName = DateUtils.getMonthName(monthSerial, language);

    }
    public void setAmounts(String language) {
        initialStock = Utils.getDigits(initialStockLong, language);
        distributedAmount = Utils.getDigits(distributedAmountLong, language);
        remainingAmount = Utils.getDigits(remainingAmountLong, language);
        purchaseAmount= Utils.getDigits(purchaseAmountLong, language);
    }

    @Override
    public String toString() {
        return "Pi_stockModel{" +
                "monthName='" + monthName + '\'' +
                ", initialStock='" + initialStock + '\'' +
                ", vendorName='" + vendorName + '\'' +
                ", purchaseDate='" + purchaseDate + '\'' +
                ", purchaseAmount='" + purchaseAmount + '\'' +
                ", distributedAmount='" + distributedAmount + '\'' +
                ", remainingAmount='" + remainingAmount + '\'' +
                ", expiryDate='" + expiryDate + '\'' +
                ", purchaseDateLong=" + purchaseDateLong +
                ", initialStockLong=" + initialStockLong +
                ", purchaseAmountLong=" + purchaseAmountLong +
                ", distributedAmountLong=" + distributedAmountLong +
                ", remainingAmountLong=" + remainingAmountLong +
                ", prevPurchaseDate=" + prevPurchaseDate +
                '}';
    }
}
