package pi_unique_item;

import java.util.*;

import util.*;


public class PiUniqueItemAssignmentDTO extends CommonDTO {

    public long piUniqueItemId = -1;
    public long officeUnitId = -1;
    public long requisitionOfficeUnitId = -1;
    public long fiscalYearId = -1;
    public long fromDate = System.currentTimeMillis();
    public long toDate = -1;
    public long purchaseDate = -1;
    public long receiveDate = -1;
    public long auctionDate = -1;
    public long piRequisitionId = -1;
    public long piPurchaseId = -1;
    public long piReceivedId = -1;
    public long orgId = -1;
    public long itemId = -1;
    public int actionType = -1;
    public int stage = -1;
    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;
    public double givenProductsCount = 0;
    public int taken = 0;
    public int returnable = 0;
    public int isPersonal = -1;

    public List<PiUniqueItemAssignmentDTO> piUniqueItemAssignmentDTOList = new ArrayList<>();

    @Override
    public String toString() {
        return "$PiUniqueItemAssignmentDTO[" +
                " iD = " + iD +
                " piUniqueItemId = " + piUniqueItemId +
                " officeUnitId = " + officeUnitId +
                " requisitionOfficeUnitId = " + requisitionOfficeUnitId +
                " fiscalYearId = " + fiscalYearId +
                " fromDate = " + fromDate +
                " toDate = " + toDate +
                " piRequisitionId = " + piRequisitionId +
                " piPurchaseId = " + piPurchaseId +
                " orgId = " + orgId +
                " itemId = " + itemId +
                " actionType = " + actionType +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " isDeleted = " + isDeleted +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                " givenProductsCount = " + givenProductsCount +
                " taken = " + taken +
                " returnable = " + returnable +
                "]";
    }

}