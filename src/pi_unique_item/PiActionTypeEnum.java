package pi_unique_item;

public enum PiActionTypeEnum {
    PURCHASE(1),
    DELIVERY(2);
    private final int value;

    PiActionTypeEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
