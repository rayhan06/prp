package pi_unique_item;

import util.CommonDTO;

public class Pi_unique_item_transactionDTO extends CommonDTO {
    public long transactionDate = System.currentTimeMillis();
    public int transactionType = -1;
    public long itemId = -1;
    public int isItemReturnable = 0;
    public String itemNameEn = "";
    public String itemNameBn = "";
    public long quantity = -1;
    public long piRequisitionId = -1;
    public long piAuctionId = -1;
    public long piPurchaseId = -1;
    public long piProductReceiveChildId = -1;
    public long employeeRecordId = -1;
    public String employeeNameEn = "";
    public String employeeNameBn = "";
    public long vendorId = -1;
    public String vendorNameEn = "";
    public String vendorNameBn = "";
    public String purchaseOrderNumber = "";
    public String goodReceivingNumber = "";
    public long itemStockCountAfterTransaction = -1;
    public long insertedBy = -1;
    public long insertionTime = System.currentTimeMillis();
    public long modifiedBy = -1;

    @Override
    public String toString() {
        return "$Pi_unique_item_transactionDTO[" +
                " iD = " + iD +
                " transactionDate = " + transactionDate +
                " transactionType = " + transactionType +
                " itemId = " + itemId +
                " isItemReturnable = " + isItemReturnable +
                " itemNameEn = " + itemNameEn +
                " itemNameBn = " + itemNameBn +
                " quantity = " + quantity +
                " employeeRecordId = " + employeeRecordId +
                " employeeNameEn = " + employeeNameEn +
                " employeeNameBn = " + employeeNameBn +
                " vendorId = " + vendorId +
                " vendorNameEn = " + vendorNameEn +
                " vendorNameBn = " + vendorNameBn +
                " purchaseOrderNumber = " + purchaseOrderNumber +
                " goodReceivingNumber = " + goodReceivingNumber +
                " itemStockCountAfterTransaction = " + itemStockCountAfterTransaction +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }
}
