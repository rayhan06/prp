package pi_unique_item;
import java.util.*; 
import util.*; 


public class Pi_unique_itemDTO extends CommonDTO
{

	public long officeUnitId = -1;    // now it is store office id
	public long fiscalYearId = -1;
	public long itemId = -1;
    public String trackingNumber = "";
	public long piPurchaseId = -1;
	public long piReceivedId = -1;
	public int status = -1;
	public long insertedBy = -1;
	public long insertionTime = -1;
	public long modifiedBy = -1;
	public int isPersonal = -1;
	
	public List<PiUniqueItemAssignmentDTO> piUniqueItemAssignmentDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Pi_unique_itemDTO[" +
            " iD = " + iD +
            " officeUnitId = " + officeUnitId +
            " fiscalYearId = " + fiscalYearId +
            " itemId = " + itemId +
            " trackingNumber = " + trackingNumber +
            " piPurchaseId = " + piPurchaseId +
            " status = " + status +
            " insertedBy = " + insertedBy +
            " insertionTime = " + insertionTime +
            " isDeleted = " + isDeleted +
            " modifiedBy = " + modifiedBy +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}