package pi_unique_item;

public enum Pi_item_transaction_typeEnum {
    PRODUCT_RECEIVED_IN_STORE_FROM_VENDOR(1),
    PRODUCT_DISBURSED_TO_EMPLOYEE_FROM_STORE(5),
    PRODUCT_RETURNED_TO_STORE_FROM_EMPLOYEE(10),
    PRODUCT_AUCTIONED_TO_VENDOR_FROM_STORE(15);

    private final int value;

    Pi_item_transaction_typeEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
