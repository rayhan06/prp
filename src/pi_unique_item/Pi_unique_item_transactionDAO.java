package pi_unique_item;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import pb.Utils;
import pi_auction_items.Pi_auction_itemsDAO;
import pi_auction_items.Pi_auction_itemsDTO;
import pi_package_auctioneer.Pi_package_auctioneerDAO;
import pi_package_auctioneer.Pi_package_auctioneerDTO;
import pi_package_auctioneer_items.Pi_package_auctioneer_itemsDAO;
import pi_package_auctioneer_items.Pi_package_auctioneer_itemsDTO;
import pi_product_receive.Pi_product_receiveDTO;
import pi_product_receive.Pi_product_receive_childDTO;
import pi_requisition.Pi_requisitionDTO;
import pi_requisition.Pi_requisition_itemDAO;
import pi_requisition.Pi_requisition_itemDTO;
import pi_stock.Pi_store_ledgerModel;
import pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsRepository;
import procurement_goods.Procurement_goodsDTO;
import procurement_goods.Procurement_goodsRepository;
import user.UserDTO;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.*;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import util.HttpRequestUtils;
import util.UtilCharacter;

public class Pi_unique_item_transactionDAO implements CommonDAOService<Pi_unique_item_transactionDTO> {
    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private final String getDTOsForStoreLedger = ("SELECT * FROM pi_unique_item_transaction WHERE item_id=%d " +
            " AND transaction_date>=%d AND transaction_date<=%d ")
            .concat("AND isDeleted=0 ").concat(" ORDER BY transaction_date");
    private final String getStockCountBetweenDateRange = ("SELECT count(distinct pi_unique_item_id) " +
            "FROM pi_unique_item_assignment WHERE " +
            " from_date >= %d AND from_date <= %d  " +
            " AND stage = 1 " +
            " AND item_id=%d ")
            .concat("AND isDeleted=0");
    private String[] columnNames = null;

    private Pi_unique_item_transactionDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "transaction_date",
                        "transaction_type_cat",
                        "item_id",
                        "is_item_returnable",
                        "item_name_en",
                        "item_name_bn",
                        "quantity",
                        "pi_requisition_id",
                        "employee_record_id",
                        "employee_name_en",
                        "employee_name_bn",
                        "pi_purchase_id",
                        "pi_product_receive_child_id",
                        "pi_auction_id",
                        "purchase_order_number",
                        "good_receiving_number",
                        "vendor_id",
                        "vendor_name_en",
                        "vendor_name_bn",
                        "item_stock_count_after_transaction",
                        "inserted_by",
                        "insertion_time",
                        "isDeleted",
                        "modified_by",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("transaction_date_start", " and (transaction_date >= ?)");
        searchMap.put("transaction_date_end", " and (transaction_date <= ?)");
        searchMap.put("transaction_type", " and (transaction_type = ?)");
        searchMap.put("item_name_en", " and (item_name_en like ?)");
        searchMap.put("item_name_bn", " and (item_name_bn like ?)");
        searchMap.put("amount", " and (amount like ?)");
        searchMap.put("employee_name_en", " and (employee_name_en like ?)");
        searchMap.put("employee_name_bn", " and (employee_name_bn like ?)");
        searchMap.put("vendor_name_en", " and (vendor_name_en like ?)");
        searchMap.put("vendor_name_bn", " and (vendor_name_bn like ?)");
        searchMap.put("purchase_order_number", " and (purchase_order_number like ?)");
        searchMap.put("good_receiving_number", " and (good_receiving_number like ?)");
        searchMap.put("item_stock_count_after_transaction", " and (item_stock_count_after_transaction like ?)");
        searchMap.put("modified_by", " and (modified_by like ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Pi_unique_item_transactionDAO INSTANCE = new Pi_unique_item_transactionDAO();
    }

    public static Pi_unique_item_transactionDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Pi_unique_item_transactionDTO pi_unique_item_transactionDTO) {
        pi_unique_item_transactionDTO.searchColumn = "";
        pi_unique_item_transactionDTO.searchColumn += pi_unique_item_transactionDTO.itemNameEn + " ";
        pi_unique_item_transactionDTO.searchColumn += pi_unique_item_transactionDTO.itemNameBn + " ";
        pi_unique_item_transactionDTO.searchColumn += pi_unique_item_transactionDTO.quantity + " ";
        pi_unique_item_transactionDTO.searchColumn += pi_unique_item_transactionDTO.employeeNameEn + " ";
        pi_unique_item_transactionDTO.searchColumn += pi_unique_item_transactionDTO.employeeNameBn + " ";
        pi_unique_item_transactionDTO.searchColumn += pi_unique_item_transactionDTO.vendorNameEn + " ";
        pi_unique_item_transactionDTO.searchColumn += pi_unique_item_transactionDTO.vendorNameBn + " ";
        pi_unique_item_transactionDTO.searchColumn += pi_unique_item_transactionDTO.purchaseOrderNumber + " ";
        pi_unique_item_transactionDTO.searchColumn += pi_unique_item_transactionDTO.goodReceivingNumber + " ";
        pi_unique_item_transactionDTO.searchColumn += pi_unique_item_transactionDTO.itemStockCountAfterTransaction + " ";
        pi_unique_item_transactionDTO.searchColumn += pi_unique_item_transactionDTO.modifiedBy + " ";
    }

    @Override
    public void set(PreparedStatement ps, Pi_unique_item_transactionDTO pi_unique_item_transactionDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(pi_unique_item_transactionDTO);
        if (isInsert) {
            ps.setObject(++index, pi_unique_item_transactionDTO.iD);
        }
        ps.setObject(++index, pi_unique_item_transactionDTO.transactionDate);
        ps.setObject(++index, pi_unique_item_transactionDTO.transactionType);
        ps.setObject(++index, pi_unique_item_transactionDTO.itemId);
        ps.setObject(++index, pi_unique_item_transactionDTO.isItemReturnable);
        ps.setObject(++index, pi_unique_item_transactionDTO.itemNameEn);
        ps.setObject(++index, pi_unique_item_transactionDTO.itemNameBn);
        ps.setObject(++index, pi_unique_item_transactionDTO.quantity);
        ps.setObject(++index, pi_unique_item_transactionDTO.piRequisitionId);
        ps.setObject(++index, pi_unique_item_transactionDTO.employeeRecordId);
        ps.setObject(++index, pi_unique_item_transactionDTO.employeeNameEn);
        ps.setObject(++index, pi_unique_item_transactionDTO.employeeNameBn);
        ps.setObject(++index, pi_unique_item_transactionDTO.piPurchaseId);
        ps.setObject(++index, pi_unique_item_transactionDTO.piProductReceiveChildId);
        ps.setObject(++index, pi_unique_item_transactionDTO.piAuctionId);
        ps.setObject(++index, pi_unique_item_transactionDTO.purchaseOrderNumber);
        ps.setObject(++index, pi_unique_item_transactionDTO.goodReceivingNumber);
        ps.setObject(++index, pi_unique_item_transactionDTO.vendorId);
        ps.setObject(++index, pi_unique_item_transactionDTO.vendorNameEn);
        ps.setObject(++index, pi_unique_item_transactionDTO.vendorNameBn);
        ps.setObject(++index, pi_unique_item_transactionDTO.itemStockCountAfterTransaction);
        ps.setObject(++index, pi_unique_item_transactionDTO.insertedBy);
        ps.setObject(++index, pi_unique_item_transactionDTO.insertionTime);
        ps.setObject(++index, pi_unique_item_transactionDTO.isDeleted);
        ps.setObject(++index, pi_unique_item_transactionDTO.modifiedBy);
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, pi_unique_item_transactionDTO.iD);
        }
    }

    @Override
    public Pi_unique_item_transactionDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Pi_unique_item_transactionDTO pi_unique_item_transactionDTO = new Pi_unique_item_transactionDTO();
            int i = 0;
            pi_unique_item_transactionDTO.iD = rs.getLong(columnNames[i++]);
            pi_unique_item_transactionDTO.transactionDate = rs.getLong(columnNames[i++]);
            pi_unique_item_transactionDTO.transactionType = rs.getInt(columnNames[i++]);
            pi_unique_item_transactionDTO.itemId = rs.getLong(columnNames[i++]);
            pi_unique_item_transactionDTO.isItemReturnable = rs.getInt(columnNames[i++]);
            pi_unique_item_transactionDTO.itemNameEn = rs.getString(columnNames[i++]);
            pi_unique_item_transactionDTO.itemNameBn = rs.getString(columnNames[i++]);
            pi_unique_item_transactionDTO.quantity = rs.getLong(columnNames[i++]);
            pi_unique_item_transactionDTO.piRequisitionId = rs.getLong(columnNames[i++]);
            pi_unique_item_transactionDTO.employeeRecordId = rs.getLong(columnNames[i++]);
            pi_unique_item_transactionDTO.employeeNameEn = rs.getString(columnNames[i++]);
            pi_unique_item_transactionDTO.employeeNameBn = rs.getString(columnNames[i++]);
            pi_unique_item_transactionDTO.piPurchaseId = rs.getLong(columnNames[i++]);
            pi_unique_item_transactionDTO.piProductReceiveChildId = rs.getLong(columnNames[i++]);
            pi_unique_item_transactionDTO.piAuctionId = rs.getLong(columnNames[i++]);
            pi_unique_item_transactionDTO.purchaseOrderNumber = rs.getString(columnNames[i++]);
            pi_unique_item_transactionDTO.goodReceivingNumber = rs.getString(columnNames[i++]);
            pi_unique_item_transactionDTO.vendorId = rs.getLong(columnNames[i++]);
            pi_unique_item_transactionDTO.vendorNameEn = rs.getString(columnNames[i++]);
            pi_unique_item_transactionDTO.vendorNameBn = rs.getString(columnNames[i++]);
            pi_unique_item_transactionDTO.itemStockCountAfterTransaction = rs.getLong(columnNames[i++]);
            pi_unique_item_transactionDTO.insertedBy = rs.getLong(columnNames[i++]);
            pi_unique_item_transactionDTO.insertionTime = rs.getLong(columnNames[i++]);
            pi_unique_item_transactionDTO.isDeleted = rs.getInt(columnNames[i++]);
            pi_unique_item_transactionDTO.modifiedBy = rs.getLong(columnNames[i++]);
            pi_unique_item_transactionDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            return pi_unique_item_transactionDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Pi_unique_item_transactionDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "pi_unique_item_transaction";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_unique_item_transactionDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_unique_item_transactionDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

    public int getStockCountBetweenDateRange(long itemId, long startDate, long endDate) {
        String sql = String.format(String.format(getStockCountBetweenDateRange, startDate, endDate, itemId));
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public List<Pi_store_ledgerModel> getDTOsForStoreLedger(long itemId, long startDate, long endDate) {
        String sql = String.format(getDTOsForStoreLedger, itemId, startDate, endDate);
        String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = "";
        Date date;
        List<Pi_unique_item_transactionDTO> transactionDTOS = Pi_unique_item_transactionDAO.getInstance().getDTOs(sql);
        List<Pi_store_ledgerModel> storeLedgerModels = new ArrayList<>();

        Pi_store_ledgerModel openingBalanceRow = new Pi_store_ledgerModel();
        openingBalanceRow.transactionType = UtilCharacter.getDataByLanguage(language,
                "শুরুর হিসাব", "Opening Balance");
        openingBalanceRow.balance = Pi_unique_item_transactionDAO.getInstance()
                .getStockCountBetweenDateRange(itemId, 0, startDate);
        storeLedgerModels.add(openingBalanceRow);
        for (Pi_unique_item_transactionDTO transactionDTO : transactionDTOS) {
            Pi_store_ledgerModel model = new Pi_store_ledgerModel();
            date = new Date(transactionDTO.transactionDate);
            formattedDate = simpleDateFormat.format(date);
            model.date = Utils.getDigits(formattedDate, language);
            if (transactionDTO.transactionType == Pi_item_transaction_typeEnum.PRODUCT_RECEIVED_IN_STORE_FROM_VENDOR.getValue()) {
                model.transactionType = UtilCharacter.getDataByLanguage(language, "মালামাল গ্রহণ", "Product Receive");
                model.identificationNumber = transactionDTO.goodReceivingNumber;
                model.user = UtilCharacter.getDataByLanguage(language, transactionDTO.vendorNameBn, transactionDTO.vendorNameEn);
                model.received = transactionDTO.quantity;
            } else if (transactionDTO.transactionType == Pi_item_transaction_typeEnum.PRODUCT_DISBURSED_TO_EMPLOYEE_FROM_STORE.getValue()) {
                model.transactionType = UtilCharacter.getDataByLanguage(language, "সরবরাহ/অধিযাচন", "Item Requisition");
                model.identificationNumber = String.valueOf(transactionDTO.piRequisitionId);
                model.user = UtilCharacter.getDataByLanguage(language, transactionDTO.employeeNameBn, transactionDTO.employeeNameEn);
                model.issued = transactionDTO.quantity;
            } else if (transactionDTO.transactionType == Pi_item_transaction_typeEnum.PRODUCT_RETURNED_TO_STORE_FROM_EMPLOYEE.getValue()) {
                model.transactionType = UtilCharacter.getDataByLanguage(language, "পণ্য ফেরত", "Item Return");
                model.identificationNumber = String.valueOf(transactionDTO.piRequisitionId);
                model.user = UtilCharacter.getDataByLanguage(language, transactionDTO.employeeNameBn, transactionDTO.employeeNameEn);
                model.received = transactionDTO.quantity;
            } else if (transactionDTO.transactionType == Pi_item_transaction_typeEnum.PRODUCT_AUCTIONED_TO_VENDOR_FROM_STORE.getValue()) {
                model.transactionType = UtilCharacter.getDataByLanguage(language, "নিলাম", "Auction");
                model.user = UtilCharacter.getDataByLanguage(language, transactionDTO.vendorNameBn, transactionDTO.vendorNameEn);
                model.issued = transactionDTO.quantity;
            }
            model.balance = transactionDTO.itemStockCountAfterTransaction;
            storeLedgerModels.add(model);
        }
        return storeLedgerModels;
    }

    public void recordItemTransactionAfterProductReceive(Pi_product_receive_childDTO productReceiveChildDTO,
                                                          Pi_product_receiveDTO productReceiveDTO, UserDTO userDTO) throws Exception {
        Pi_unique_item_transactionDTO transactionDTO = new Pi_unique_item_transactionDTO();
        transactionDTO.transactionDate = System.currentTimeMillis();
        transactionDTO.transactionType = Pi_item_transaction_typeEnum.PRODUCT_RECEIVED_IN_STORE_FROM_VENDOR.getValue();
        transactionDTO.itemId = productReceiveChildDTO.itemId;
        Procurement_goodsDTO item = Procurement_goodsRepository.getInstance()
                .getProcurement_goodsDTOByID(transactionDTO.itemId);
        if (item != null) {
            transactionDTO.itemNameEn = item.nameEn;
            transactionDTO.itemNameBn = item.nameBn;
            transactionDTO.isItemReturnable = item.returnable;
        }

        transactionDTO.quantity = (long) productReceiveChildDTO.quantity;
        transactionDTO.vendorId = productReceiveDTO.vendorId;
        transactionDTO.vendorNameEn = Pi_vendor_auctioneer_detailsRepository.getInstance()
                .getText(productReceiveChildDTO.vendorId, "English");
        transactionDTO.vendorNameBn = Pi_vendor_auctioneer_detailsRepository.getInstance()
                .getText(productReceiveChildDTO.vendorId, "Bangla");
        transactionDTO.piPurchaseId = productReceiveChildDTO.piPurchaseId;
        transactionDTO.piProductReceiveChildId = productReceiveChildDTO.iD;
        transactionDTO.purchaseOrderNumber = productReceiveDTO.purchaseOrderNumber;
        transactionDTO.goodReceivingNumber = productReceiveDTO.goodReceivingNumber;
        transactionDTO.itemStockCountAfterTransaction = PiUniqueItemAssignmentDAO.getInstance()
                .getTotalItemCountWithActionTypeForAllStore(transactionDTO.itemId, PiActionTypeEnum.PURCHASE.getValue())
                + transactionDTO.quantity;

        transactionDTO.insertedBy = transactionDTO.modifiedBy = userDTO.employee_record_id;
        transactionDTO.insertionTime = transactionDTO.lastModificationTime = System.currentTimeMillis();

        Pi_unique_item_transactionDAO.getInstance().add(transactionDTO);
    }

    public void recordItemsTransactionAfterRequisition(Pi_requisitionDTO requisitionDTO, UserDTO userDTO) throws Exception {
        List<Pi_requisition_itemDTO> items = Pi_requisition_itemDAO.getInstance()
                .getDTOsByRequisitionId(requisitionDTO.iD);
        for (Pi_requisition_itemDTO item : items) {
            recordItemTransactionAfterRequisition(item, requisitionDTO, userDTO);
        }
    }

    private void recordItemTransactionAfterRequisition(Pi_requisition_itemDTO requisitionItem, Pi_requisitionDTO requisitionDTO,
                                       UserDTO userDTO) throws Exception {
        Pi_unique_item_transactionDTO transactionDTO = new Pi_unique_item_transactionDTO();
        transactionDTO.transactionDate = System.currentTimeMillis();
        transactionDTO.transactionType = Pi_item_transaction_typeEnum.PRODUCT_DISBURSED_TO_EMPLOYEE_FROM_STORE.getValue();
        transactionDTO.itemId = requisitionItem.itemId;
        transactionDTO.piRequisitionId = requisitionItem.piRequisitionId;
        Procurement_goodsDTO item = Procurement_goodsRepository.getInstance()
                .getProcurement_goodsDTOByID(transactionDTO.itemId);
        if (item != null) {
            transactionDTO.itemNameEn = item.nameEn;
            transactionDTO.itemNameBn = item.nameBn;
            transactionDTO.isItemReturnable = item.returnable;
        }

        transactionDTO.quantity = requisitionItem.approvalTwoQuantity;
        transactionDTO.employeeRecordId = requisitionDTO.employeeRecordId;
        transactionDTO.employeeNameEn = requisitionDTO.requesterNameEng;
        transactionDTO.employeeNameBn = requisitionDTO.requesterNameBng;
        transactionDTO.itemStockCountAfterTransaction = PiUniqueItemAssignmentDAO.getInstance()
                .getTotalItemCountWithActionTypeForAllStore(transactionDTO.itemId, PiActionTypeEnum.PURCHASE.getValue())
                - transactionDTO.quantity;

        transactionDTO.insertedBy = transactionDTO.modifiedBy = userDTO.employee_record_id;
        transactionDTO.insertionTime = transactionDTO.lastModificationTime = System.currentTimeMillis();

        Pi_unique_item_transactionDAO.getInstance().add(transactionDTO);
    }

    public void recordMultipleItemTransactionAfterAuction(long auctionId, long vendorId, UserDTO userDTO) throws Exception {
        List<Pi_auction_itemsDTO> auctionItems = (List<Pi_auction_itemsDTO>) Pi_auction_itemsDAO.getInstance()
                .getDTOsByParent("pi_auction_id", auctionId);

        if (auctionItems == null || auctionItems.isEmpty()) {
            /*Do something*/
        }
        for (Pi_auction_itemsDTO auctionItemsDTO : auctionItems) {
            Pi_unique_item_transactionDTO transactionDTO = new Pi_unique_item_transactionDTO();
            transactionDTO.transactionDate = System.currentTimeMillis();
            transactionDTO.transactionType = Pi_item_transaction_typeEnum.PRODUCT_AUCTIONED_TO_VENDOR_FROM_STORE.getValue();
            transactionDTO.itemId = auctionItemsDTO.itemId;
            Procurement_goodsDTO item = Procurement_goodsRepository.getInstance()
                    .getProcurement_goodsDTOByID(transactionDTO.itemId);
            if (item != null) {
                transactionDTO.itemNameEn = item.nameEn;
                transactionDTO.itemNameBn = item.nameBn;
                transactionDTO.isItemReturnable = item.returnable;
            }

            transactionDTO.quantity = auctionItemsDTO.sellingUnit;
            transactionDTO.piAuctionId = auctionId;
            transactionDTO.vendorId = vendorId;
            transactionDTO.vendorNameEn = Pi_vendor_auctioneer_detailsRepository.getInstance()
                    .getText(transactionDTO.vendorId, "English");
            transactionDTO.vendorNameBn = Pi_vendor_auctioneer_detailsRepository.getInstance()
                    .getText(transactionDTO.vendorId, "Bangla");
            /*NO NEED TO SUBTRACT transactionDTO.quantity. BECUASE ITEM STAGE IS ALREADY AUCTIONED*/
            transactionDTO.itemStockCountAfterTransaction = PiUniqueItemAssignmentDAO.getInstance()
                    .getItemCountInStockByItemId(transactionDTO.itemId);

            transactionDTO.insertedBy = transactionDTO.modifiedBy = userDTO.employee_record_id;
            transactionDTO.insertionTime = transactionDTO.lastModificationTime = System.currentTimeMillis();

            Pi_unique_item_transactionDAO.getInstance().add(transactionDTO);
        }
    }
}
