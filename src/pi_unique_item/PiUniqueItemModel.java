package pi_unique_item;

import fiscal_year.Fiscal_yearRepository;
import pb.Utils;
import pi_purchase.Pi_purchaseDTO;
import pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsDTO;
import pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsRepository;
import procurement_goods.Procurement_goodsDTO;
import procurement_goods.Procurement_goodsRepository;
import util.HttpRequestUtils;
import util.StringUtils;

public class PiUniqueItemModel {
    public String iD = "";
    public String productName = "";
    public String givenDate = "";
    public String itemId = "-1";
    public String uniqueItemId = "-1";
    public String purchaseId = "-1";
    public String givenProductsCount = "";
    public String hiddenGivenDate = "";
    public String returnDate = "";
    public String isTaken = "";

    public PiUniqueItemModel() {
    }

    public PiUniqueItemModel(PiUniqueItemAssignmentDTO dto, String language) {
        boolean isLanguageEnglish = "English".equalsIgnoreCase(language);
        Procurement_goodsDTO procurement_goodsDTO = Procurement_goodsRepository.getInstance().getProcurement_goodsDTOByID(dto.itemId);
        iD = String.valueOf(dto.iD);
        purchaseId = String.valueOf(dto.piPurchaseId);
        itemId = String.valueOf(dto.itemId);
        uniqueItemId = String.valueOf(dto.piUniqueItemId);
        productName = isLanguageEnglish ? procurement_goodsDTO.nameEn : procurement_goodsDTO.nameBn;
        givenDate = StringUtils.getFormattedDate(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language, dto.fromDate);
        returnDate = StringUtils.getFormattedDate(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language, dto.toDate);
        hiddenGivenDate = String.valueOf(dto.fromDate);
        givenProductsCount = String.valueOf(dto.givenProductsCount);
        isTaken = String.valueOf(dto.taken);
    }


    @Override
    public String toString() {
        return "PiProductModel{" +
                "iD='" + iD + '\'' +
                ", productName='" + productName + '\'' +
                ", itemId='" + itemId + '\'' +
                ", givenDate='" + givenDate + '\'' +
                ", purchaseId='" + purchaseId + '\'' +
                ", givenProductsCount='" + givenProductsCount + '\'' +
                ", hiddenGivenDate='" + hiddenGivenDate + '\'' +
                ", uniqueItemId='" + uniqueItemId + '\'' +
                ", returnDate='" + returnDate + '\'' +
                ", isTaken='" + isTaken + '\'' +
                '}';
    }
}
