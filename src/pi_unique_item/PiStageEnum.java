package pi_unique_item;

public enum PiStageEnum {
    IN_STOCK(1),
    OUT_STOCK(2),
    AUCTIONED(3);
    private final int value;

    PiStageEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
