package pi_unique_item;

import com.google.gson.Gson;
import common.BaseServlet;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_offices.EmployeeOfficesDAO;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import pi_auction_items.PiProductModel;
import pi_purchase.Pi_purchaseDAO;
import pi_purchase.Pi_purchaseDTO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Servlet implementation class Pi_unique_itemServlet
 */
@WebServlet("/Pi_unique_itemServlet")
@MultipartConfig
public class Pi_unique_itemServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_unique_itemServlet.class);
    private final EmployeeOfficesDAO employeeOfficesDAO = new EmployeeOfficesDAO();

    @Override
    public String getTableName() {
        return Pi_unique_itemDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_unique_itemServlet";
    }

    @Override
    public Pi_unique_itemDAO getCommonDAOService() {
        return Pi_unique_itemDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PI_PURCHASE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PI_PURCHASE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PI_PURCHASE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_unique_itemServlet.class;
    }

    PiUniqueItemAssignmentDAO piUniqueItemAssignmentDAO = PiUniqueItemAssignmentDAO.getInstance();
    private final Gson gson = new Gson();


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addPi_unique_item");
            String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
            Pi_unique_itemDTO pi_unique_itemDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

            if (addFlag == true) {
                pi_unique_itemDTO = new Pi_unique_itemDTO();
            } else {
                pi_unique_itemDTO = Pi_unique_itemDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
            }

            String Value = "";

            Value = request.getParameter("officeUnitId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("officeUnitId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                pi_unique_itemDTO.officeUnitId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("fiscalYearId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("fiscalYearId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                pi_unique_itemDTO.fiscalYearId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("itemId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("itemId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                pi_unique_itemDTO.itemId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("trackingNumber");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("trackingNumber = " + Value);
            if (Value != null) {
                pi_unique_itemDTO.trackingNumber = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("piPurchaseItemsId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("piPurchaseItemsId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                pi_unique_itemDTO.piPurchaseId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("status");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("status = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                pi_unique_itemDTO.status = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("insertedBy");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("insertedBy = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                pi_unique_itemDTO.insertedBy = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("insertionTime");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("insertionTime = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                pi_unique_itemDTO.insertionTime = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("modifiedBy");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("modifiedBy = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                pi_unique_itemDTO.modifiedBy = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            System.out.println("Done adding  addPi_unique_item dto = " + pi_unique_itemDTO);

            List<PiUniqueItemAssignmentDTO> piUniqueItemAssignmentDTOList = createPiUniqueItemAssignmentDTOListByRequest(request, language);

            Utils.handleTransaction(()->{
                if (addFlag == true) {
                    Pi_unique_itemDAO.getInstance().add(pi_unique_itemDTO);
                } else {
                    Pi_unique_itemDAO.getInstance().update(pi_unique_itemDTO);
                }


                if (addFlag == true) //add or validate
                {
                    if (piUniqueItemAssignmentDTOList != null) {
                        for (PiUniqueItemAssignmentDTO piUniqueItemAssignmentDTO : piUniqueItemAssignmentDTOList) {
                            piUniqueItemAssignmentDTO.piUniqueItemId = pi_unique_itemDTO.iD;
                            piUniqueItemAssignmentDAO.add(piUniqueItemAssignmentDTO);
                        }
                    }

                }
                else {
                    List<Long> childIdsFromRequest = piUniqueItemAssignmentDAO.getChildIdsFromRequest(request, "piUniqueItemAssignment");
                    //delete the removed children
                    piUniqueItemAssignmentDAO.deleteChildrenNotInList("pi_unique_item", "pi_unique_item_assignment", pi_unique_itemDTO.iD, childIdsFromRequest);
                    List<Long> childIDsInDatabase = piUniqueItemAssignmentDAO.getChilIds("pi_unique_item", "pi_unique_item_assignment", pi_unique_itemDTO.iD);


                    if (childIdsFromRequest != null) {
                        for (int i = 0; i < childIdsFromRequest.size(); i++) {
                            Long childIDFromRequest = childIdsFromRequest.get(i);
                            if (childIDsInDatabase.contains(childIDFromRequest)) {
                                PiUniqueItemAssignmentDTO piUniqueItemAssignmentDTO = createPiUniqueItemAssignmentDTOByRequestAndIndex(request, false, i, language);
                                piUniqueItemAssignmentDTO.piUniqueItemId = pi_unique_itemDTO.iD;
                                piUniqueItemAssignmentDAO.update(piUniqueItemAssignmentDTO);
                            } else {
                                PiUniqueItemAssignmentDTO piUniqueItemAssignmentDTO = createPiUniqueItemAssignmentDTOByRequestAndIndex(request, true, i, language);
                                piUniqueItemAssignmentDTO.piUniqueItemId = pi_unique_itemDTO.iD;
                                piUniqueItemAssignmentDAO.add(piUniqueItemAssignmentDTO);
                            }
                        }
                    } else {
                        piUniqueItemAssignmentDAO.deleteChildrenByParent(pi_unique_itemDTO.iD, "pi_unique_item_id");
                    }

                }
            });

            return pi_unique_itemDTO;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private List<PiUniqueItemAssignmentDTO> createPiUniqueItemAssignmentDTOListByRequest(HttpServletRequest request, String language) throws Exception {
        List<PiUniqueItemAssignmentDTO> piUniqueItemAssignmentDTOList = new ArrayList<PiUniqueItemAssignmentDTO>();
        if (request.getParameterValues("piUniqueItemAssignment.iD") != null) {
            int piUniqueItemAssignmentItemNo = request.getParameterValues("piUniqueItemAssignment.iD").length;


            for (int index = 0; index < piUniqueItemAssignmentItemNo; index++) {
                PiUniqueItemAssignmentDTO piUniqueItemAssignmentDTO = createPiUniqueItemAssignmentDTOByRequestAndIndex(request, true, index, language);
                piUniqueItemAssignmentDTOList.add(piUniqueItemAssignmentDTO);
            }

            return piUniqueItemAssignmentDTOList;
        }
        return null;
    }

    private PiUniqueItemAssignmentDTO createPiUniqueItemAssignmentDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, String language) throws Exception {

        PiUniqueItemAssignmentDTO piUniqueItemAssignmentDTO;
        if (addFlag == true) {
            piUniqueItemAssignmentDTO = new PiUniqueItemAssignmentDTO();
        } else {
            piUniqueItemAssignmentDTO = piUniqueItemAssignmentDAO.getDTOByID(Long.parseLong(request.getParameterValues("piUniqueItemAssignment.iD")[index]));
        }
        String path = getServletContext().getRealPath("/img2/");
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");


        String Value = "";
        Value = request.getParameterValues("piUniqueItemAssignment.piUniqueItemId")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        piUniqueItemAssignmentDTO.piUniqueItemId = Long.parseLong(Value);
        Value = request.getParameterValues("piUniqueItemAssignment.officeUnitId")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        piUniqueItemAssignmentDTO.officeUnitId = Long.parseLong(Value);
        Value = request.getParameterValues("piUniqueItemAssignment.fiscalYearId")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        piUniqueItemAssignmentDTO.fiscalYearId = Long.parseLong(Value);
        Value = request.getParameterValues("piUniqueItemAssignment.fromDate")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        try {
            Date d = f.parse(Value);
            piUniqueItemAssignmentDTO.fromDate = d.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Value = request.getParameterValues("piUniqueItemAssignment.toDate")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        try {
            Date d = f.parse(Value);
            piUniqueItemAssignmentDTO.toDate = d.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Value = request.getParameterValues("piUniqueItemAssignment.piRequisitionId")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        piUniqueItemAssignmentDTO.piRequisitionId = Long.parseLong(Value);
        Value = request.getParameterValues("piUniqueItemAssignment.orgId")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        piUniqueItemAssignmentDTO.orgId = Long.parseLong(Value);
        Value = request.getParameterValues("piUniqueItemAssignment.itemId")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        piUniqueItemAssignmentDTO.itemId = Long.parseLong(Value);
        Value = request.getParameterValues("piUniqueItemAssignment.actionType")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }


        piUniqueItemAssignmentDTO.actionType = Integer.parseInt(Value);
        Value = request.getParameterValues("piUniqueItemAssignment.insertedBy")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        piUniqueItemAssignmentDTO.insertedBy = Long.parseLong(Value);
        Value = request.getParameterValues("piUniqueItemAssignment.insertionTime")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        piUniqueItemAssignmentDTO.insertionTime = Long.parseLong(Value);
        Value = request.getParameterValues("piUniqueItemAssignment.modifiedBy")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        piUniqueItemAssignmentDTO.modifiedBy = Long.parseLong(Value);
        return piUniqueItemAssignmentDTO;

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "buildGivenItemTableData":
                    long employeeOrgId = Long.parseLong(request.getParameter("employeeOrgId"));
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    List<PiUniqueItemAssignmentDTO> piUniqueItemAssignmentDTOS = PiUniqueItemAssignmentDAO.getInstance().
                            getGivenItems(employeeOrgId,PiStageEnum.OUT_STOCK.getValue(),PiActionTypeEnum.DELIVERY.getValue(),1);

                    List<PiUniqueItemModel> modelList = null;

                    if(piUniqueItemAssignmentDTOS!=null &&  piUniqueItemAssignmentDTOS.size()>0){
                        modelList = piUniqueItemAssignmentDTOS
                                .stream()
                                .map(dto -> new PiUniqueItemModel(dto, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language))
                                .collect(Collectors.toList());
                    }
                    response.getWriter().println(gson.toJson(modelList));
                    return;

                case "getGivenAndMovableItem":
                    long employeeOrgId2 = Long.parseLong(request.getParameter("employeeOrgId"));
                    long employeeRecordId = Long.parseLong(request.getParameter("employeeRecordId"));
                    EmployeeOfficeDTO employeeOfficeDTO = employeeOfficesDAO.getLastByEmployeeRecordIdAndOrganogramId(employeeRecordId, employeeOrgId2);
                    String toDateString  = employeeOfficeDTO.lastOfficeDate != SessionConstants.MIN_DATE ? " AND to_date <= " + employeeOfficeDTO.lastOfficeDate + " " : "";
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
//                    List<PiUniqueItemAssignmentDTO> piUniqueItemAssignmentDTOS2 = PiUniqueItemAssignmentDAO.getInstance().
//                            getGivenAndMovableItem(employeeOrgId2,PiStageEnum.OUT_STOCK.getValue(),PiActionTypeEnum.DELIVERY.getValue(),1,0);
                    List<PiUniqueItemAssignmentDTO> piUniqueItemAssignmentDTOS2 = PiUniqueItemAssignmentDAO.getInstance().
                            getMovableItem(employeeOrgId2,PiStageEnum.OUT_STOCK.getValue(),PiActionTypeEnum.DELIVERY.getValue(),1, employeeOfficeDTO.joiningDate, toDateString);

                    List<PiUniqueItemModel> modelList2 = null;

                    if(piUniqueItemAssignmentDTOS2!=null &&  piUniqueItemAssignmentDTOS2.size()>0){
                        modelList2 = piUniqueItemAssignmentDTOS2
                                .stream()
                                .map(dto -> new PiUniqueItemModel(dto, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language))
                                .collect(Collectors.toList());
                    }
                    response.getWriter().println(gson.toJson(modelList2));
                    return;

                case "buildReturnedItemTableData":
                    long OrgId = Long.parseLong(request.getParameter("employeeOrgId"));
                    String returnedDate = (request.getParameter("returnDate"));
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    List<PiUniqueItemAssignmentDTO> piUniqueItemAssignmentDTOS1 = PiUniqueItemAssignmentDAO.getInstance().
                            getReturnedItems(OrgId,PiStageEnum.IN_STOCK.getValue(),PiActionTypeEnum.PURCHASE.getValue(),returnedDate,1);

                    List<PiUniqueItemModel> modelList1 = null;

                    if(piUniqueItemAssignmentDTOS1!=null &&  piUniqueItemAssignmentDTOS1.size()>0){
                        modelList1 = piUniqueItemAssignmentDTOS1
                                .stream()
                                .map(dto -> new PiUniqueItemModel(dto, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language))
                                .collect(Collectors.toList());
                    }
                    response.getWriter().println(gson.toJson(modelList1));
                    return;


                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);


    }
}

