package pi_unique_item;

import card_info.AlreadyApprovedException;
import card_info.InvalidDataException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficesDAO;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import org.apache.log4j.Logger;
import pi_requisition.Pi_requisitionDAO;
import pi_requisition.Pi_requisitionDTO;
import pi_requisition.Pi_requisition_itemDAO;
import pi_requisition.Pi_requisition_itemDTO;
import pi_stock.Pi_monthlyReturnModel;
import pi_stock.Pi_stock_summaryModel;
import procurement_goods.Procurement_goodsRepository;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.UtilCharacter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class PiUniqueItemAssignmentDAO implements CommonDAOService<PiUniqueItemAssignmentDTO> {

    Logger logger = Logger.getLogger(getClass());
    private final EmployeeOfficesDAO employeeOfficesDAO = new EmployeeOfficesDAO();

    private static String addQuery = "";
    private static String updateQuery = "";
    private String[] columnNames = null;
    private final String getItemsCountWithType = ("SELECT COUNT(*) FROM pi_unique_item_assignment WHERE office_unit_id=%d " +
            " AND fiscal_year_id=%d AND item_id=%d AND action_type=%d ")
            .concat("AND isDeleted=0");

    private final String getItemsCountWithoutStore = ("SELECT COUNT(*) FROM pi_unique_item_assignment WHERE  fiscal_year_id=%d AND item_id=%d AND action_type=%d ")
            .concat("AND isDeleted=0");

    private final String getDTOsBeforeDate = ("SELECT * FROM pi_unique_item_assignment WHERE office_unit_id=%d AND " +
            " fiscal_year_id=%d AND item_id=%d AND purchase_date <=%d ")
            .concat("AND isDeleted=0");
    private final String getStockCountBeforeFiscalYear = ("SELECT count(distinct pi_unique_item_id) FROM pi_unique_item_assignment WHERE office_unit_id=%d AND " +
            " ((%d not between from_date and to_date  " +
            " and action_type = 2) " +
            " or (stage = 1)" +
            " or (stage = 3 and auction_date >= %d) )" +
            " and fiscal_year_id<%d AND item_id=%d ")
            .concat("AND isDeleted=0");
    private final String getItemsCountInStock = ("SELECT COUNT(*) FROM pi_unique_item_assignment WHERE office_unit_id=%d " +
            " AND fiscal_year_id=%d AND item_id=%d AND stage=%d %s AND %s <%d ")
            .concat("AND isDeleted=0");
    private final String getDTOsByStage = ("SELECT * FROM pi_unique_item_assignment WHERE office_unit_id=%d AND " +
            " item_id=%d AND stage=%d %s ")
            .concat("AND isDeleted=0 order by fiscal_year_id limit %d");

    private final String getTotalItemsCountWithType = ("SELECT COUNT(*) FROM pi_unique_item_assignment WHERE " +
            " office_unit_id=%d AND item_id=%d AND action_type=%d AND %s <%d ")
            .concat("AND isDeleted=0");
    private final String getTotalItemCountWithActionTypeForAllStore = ("SELECT COUNT(*) FROM pi_unique_item_assignment WHERE " +
            " item_id=%d AND action_type=%d AND pi_requisition_id=-1 ")
            .concat("AND isDeleted=0");
    private final String getStockCountForAllStoreByItemId = ("SELECT COUNT(*) FROM pi_unique_item_assignment WHERE " +
            " item_id=%d AND stage=1 ")
            .concat("AND isDeleted=0");
    private final String getTotalItemsCountWithStageOverall = ("SELECT COUNT(*) FROM pi_unique_item_assignment WHERE " +
            "  item_id=%d AND stage=%d AND %s <%d ")
            .concat("AND isDeleted=0");
    private final String getTotalItemsCountWithStage = ("SELECT COUNT(*) FROM pi_unique_item_assignment WHERE " +
            " office_unit_id=%d AND item_id=%d AND stage=%d AND %s <%d ")
            .concat("AND isDeleted=0");

    private final String getTotalItemsCountWithStageAllOver = ("SELECT COUNT(*) FROM pi_unique_item_assignment WHERE " +
            " office_unit_id=%d AND item_id=%d AND stage=%d AND %s ")
            .concat("AND isDeleted=0");

    private final String getTotalItemsCountWithStageAndPurchaseIdIncluingCurrentTime = ("SELECT COUNT(*) FROM pi_unique_item_assignment WHERE " +
            " office_unit_id=%d AND item_id=%d AND pi_purchase_id=%d AND stage=%d AND %s <= %d ")
            .concat("AND isDeleted=0");
    private final String getItemCountInStockByItemIdAndPurchaseId = ("SELECT COUNT(*) FROM pi_unique_item_assignment WHERE " +
            " item_id=%d AND pi_purchase_id=%d AND stage=1 ")
            .concat(" AND isDeleted=0 ");
    private final String getItemCountInStockByItemId = ("SELECT COUNT(*) FROM pi_unique_item_assignment WHERE " +
            " item_id=%d AND stage=1 ")
            .concat(" AND isDeleted=0 ");

    private final String getTotalItemsCountWithStageAndPurchaseIdAllOver = ("SELECT COUNT(*) FROM pi_unique_item_assignment WHERE " +
            " office_unit_id=%d AND item_id=%d AND pi_purchase_id=%d AND stage=%d AND %s ")
            .concat("AND isDeleted=0");

    private static final String UpdateStageAfterAuction =
            "UPDATE pi_unique_item_assignment SET stage = %d , auction_date = %d ,lastModificationTime = %d " +
                    " WHERE stage = %d AND item_id = %d and pi_purchase_id = %d  and office_unit_id = %d and isDeleted = 0 order by fiscal_year_id LIMIT %d ";

    private static final String UpdateStageAfterAuctionCancellation =
            "UPDATE pi_unique_item_assignment SET stage = %d , auction_date = %d " +
                    " WHERE stage = %d AND pi_purchase_id = %d and office_unit_id = %d order by fiscal_year_id LIMIT %d ";

    private final String getGivenItems = ("SELECT puia.* FROM pi_unique_item_assignment as puia WHERE " +
            " puia.org_id = %d AND puia.stage = %d AND puia.action_type = %d AND to_date=-1 AND puia.returnable = %d AND puia.isDeleted = 0 ");

    private final String updateReturnedItem = ("UPDATE pi_unique_item_assignment SET stage = %d, to_date = %d," +
            " lastModificationTime = %d,modified_by = %d " +
            " WHERE isDeleted = 0  AND ((pi_unique_item_id = %d AND action_type = %d) OR (id = %d)) AND org_id = %d ");

    private final String getReturnedItems = ("SELECT puia.* FROM pi_unique_item_assignment as puia WHERE " +
            " puia.org_id = ? AND puia.stage = ? AND puia.returnable = ? AND puia.isDeleted = ? ");

    private final String getDistributionCount = "SELECT count(pi_unique_item_id) FROM pi_unique_item_assignment WHERE office_unit_id=%d AND " +
            "item_id=%d AND ((from_date>=%d AND from_date<%d AND action_type=2) or (auction_date>=%d AND auction_date<%d AND stage=3 AND action_type=1)) AND isDeleted=0";

    private final String getInStockCountInDateRange = "SELECT count(pi_unique_item_id) FROM pi_unique_item_assignment WHERE office_unit_id=%d AND " +
            "item_id=%d AND from_date>=%d AND from_date<=%d AND stage=1 AND isDeleted=0";

    private final String getReturnedCountByMonth = "SELECT (month(from_unixtime(to_date/1000)) - 1) as monthSerial, count(pi_unique_item_id) as count FROM pi_unique_item_assignment \n" +
            "WHERE office_unit_id=%d AND \n" +
            "item_id=%d and\n" +
            "to_date between %d and %d\n" +
            "AND action_type=2\n" +
            "AND isDeleted=0\n" +
            "group by monthSerial ";

    private final String moveTakenItems = ("UPDATE pi_unique_item_assignment SET taken = %d , to_date = %d," +
            " lastModificationTime = %d,modified_by = %d " +
            " WHERE isDeleted = 0  AND id = %d AND org_id = %d ");

    private final String moveTakenItemsWithoutToDate = ("UPDATE pi_unique_item_assignment SET taken = %d , " +
            " lastModificationTime = %d,modified_by = %d " +
            " WHERE isDeleted = 0  AND id = %d AND org_id = %d ");

    private final String getGivenAndMovableItem = ("SELECT puia.* FROM pi_unique_item_assignment as puia WHERE " +
            " puia.org_id = %d AND puia.stage = %d AND puia.action_type = %d AND puia.returnable = %d AND puia.isDeleted = 0 AND " +
            "  puia.taken = %d AND to_date = %d ");

    private final String getMovableItem = ("SELECT puia.* FROM pi_unique_item_assignment as puia WHERE " +
            " puia.org_id = %d AND puia.stage = %d AND puia.action_type = %d AND puia.returnable = %d AND puia.isDeleted = 0 AND puia.from_date >= %d  %s ");

    private final String getLatestRequisitionIdForItem = ("SELECT puia.* FROM pi_unique_item_assignment as puia WHERE " +
            " puia.pi_unique_item_id = %d ")
            .concat(" AND puia.isDeleted = 0 ")
            .concat(" ORDER BY puia.from_date DESC ");
    private final String getUniqueItemAssignmentDTOInTimeRange = ("SELECT * FROM pi_unique_item_assignment " +
            " WHERE from_date >= %d  AND from_date <= %d ")
            .concat("AND isDeleted=0 ");

    private final String getPurchasedAndReceivedItemCountByItemIdAndDateRange = ("SELECT COUNT(item_id) " +
            " FROM pi_unique_item_assignment " +
            " WHERE item_id = %d " +
            " AND from_date >= %d  AND from_date <= %d " +
            " AND action_type = 1 ")
            .concat(" AND isDeleted=0 ")
            .concat(" GROUP BY item_id ");

    private final String getDistributeCountByItemIdAndDateRange = ("SELECT COUNT(item_id) " +
            " FROM pi_unique_item_assignment " +
            " WHERE item_id = %d " +
            " AND from_date >= %d  AND from_date <= %d " +
            " AND action_type = 2 ")
            .concat(" AND isDeleted=0 ")
            .concat(" GROUP BY item_id ");

    private final String getInStockCountByItemIdAndDateRange = ("SELECT COUNT(item_id) " +
            " FROM pi_unique_item_assignment " +
            " WHERE item_id = %d " +
            " AND from_date >= %d  AND from_date <= %d " +
            " AND stage = 1 ")
            .concat(" AND isDeleted=0 ")
            .concat(" GROUP BY item_id ");
    private final String getItemIdsInTimeRange = ("SELECT item_id " +
            " FROM pi_unique_item_assignment " +
            " WHERE from_date >= %d  AND from_date <= %d ")
            .concat("AND isDeleted=0 ")
            .concat(" GROUP BY item_id ");

    public PiUniqueItemAssignmentDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "pi_unique_item_id",
                        "office_unit_id",
                        "requisition_office_unit_id",
                        "fiscal_year_id",
                        "from_date",
                        "to_date",
                        "purchase_date",
                        "received_date",
                        "auction_date",
                        "pi_requisition_id",
                        "pi_purchase_id",
                        "pi_received_id",
                        "org_id",
                        "item_id",
                        "action_type",
                        "stage",
                        "taken",
                        "returnable",
                        "is_personal",
                        "inserted_by",
                        "insertion_time",
                        "modified_by",
                        "isDeleted",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("from_date_start", " and (from_date >= ?)");
        searchMap.put("from_date_end", " and (from_date <= ?)");
        searchMap.put("to_date_start", " and (to_date >= ?)");
        searchMap.put("to_date_end", " and (to_date <= ?)");
        searchMap.put("action_type", " and (action_type = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final PiUniqueItemAssignmentDAO INSTANCE = new PiUniqueItemAssignmentDAO();
    }

    public static PiUniqueItemAssignmentDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(PiUniqueItemAssignmentDTO piuniqueitemassignmentDTO) {
        piuniqueitemassignmentDTO.searchColumn = "";
    }

    @Override
    public void set(PreparedStatement ps, PiUniqueItemAssignmentDTO piuniqueitemassignmentDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(piuniqueitemassignmentDTO);
        if (isInsert) {
            ps.setObject(++index, piuniqueitemassignmentDTO.iD);
        }
        ps.setObject(++index, piuniqueitemassignmentDTO.piUniqueItemId);
        ps.setObject(++index, piuniqueitemassignmentDTO.officeUnitId);
        ps.setObject(++index, piuniqueitemassignmentDTO.requisitionOfficeUnitId);
        ps.setObject(++index, piuniqueitemassignmentDTO.fiscalYearId);
        ps.setObject(++index, piuniqueitemassignmentDTO.fromDate);
        ps.setObject(++index, piuniqueitemassignmentDTO.toDate);
        ps.setObject(++index, piuniqueitemassignmentDTO.purchaseDate);
        ps.setObject(++index, piuniqueitemassignmentDTO.receiveDate);
        ps.setObject(++index, piuniqueitemassignmentDTO.auctionDate);
        ps.setObject(++index, piuniqueitemassignmentDTO.piRequisitionId);
        ps.setObject(++index, piuniqueitemassignmentDTO.piPurchaseId);
        ps.setObject(++index, piuniqueitemassignmentDTO.piReceivedId);
        ps.setObject(++index, piuniqueitemassignmentDTO.orgId);
        ps.setObject(++index, piuniqueitemassignmentDTO.itemId);
        ps.setObject(++index, piuniqueitemassignmentDTO.actionType);
        ps.setObject(++index, piuniqueitemassignmentDTO.stage);
        ps.setObject(++index, piuniqueitemassignmentDTO.taken);
        ps.setObject(++index, piuniqueitemassignmentDTO.returnable);
        ps.setObject(++index, piuniqueitemassignmentDTO.isPersonal);
        ps.setObject(++index, piuniqueitemassignmentDTO.insertedBy);
        ps.setObject(++index, piuniqueitemassignmentDTO.insertionTime);
        ps.setObject(++index, piuniqueitemassignmentDTO.modifiedBy);
        if (isInsert) {
            ps.setObject(++index, piuniqueitemassignmentDTO.isDeleted);
        }
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, piuniqueitemassignmentDTO.iD);
        }
    }

    @Override
    public PiUniqueItemAssignmentDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            PiUniqueItemAssignmentDTO piuniqueitemassignmentDTO = new PiUniqueItemAssignmentDTO();
            int i = 0;
            piuniqueitemassignmentDTO.iD = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.piUniqueItemId = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.officeUnitId = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.requisitionOfficeUnitId = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.fiscalYearId = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.fromDate = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.toDate = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.purchaseDate = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.receiveDate = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.auctionDate = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.piRequisitionId = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.piPurchaseId = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.piReceivedId = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.orgId = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.itemId = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.actionType = rs.getInt(columnNames[i++]);
            piuniqueitemassignmentDTO.stage = rs.getInt(columnNames[i++]);
            piuniqueitemassignmentDTO.taken = rs.getInt(columnNames[i++]);
            piuniqueitemassignmentDTO.returnable = rs.getInt(columnNames[i++]);
            piuniqueitemassignmentDTO.isPersonal = rs.getInt(columnNames[i++]);
            piuniqueitemassignmentDTO.insertedBy = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.insertionTime = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.modifiedBy = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.isDeleted = rs.getInt(columnNames[i++]);
            piuniqueitemassignmentDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            return piuniqueitemassignmentDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public PiUniqueItemAssignmentDTO buildObjectFromResultSetForReturn(ResultSet rs) {
        try {
            PiUniqueItemAssignmentDTO piuniqueitemassignmentDTO = new PiUniqueItemAssignmentDTO();
            int i = 0;
            piuniqueitemassignmentDTO.iD = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.piUniqueItemId = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.officeUnitId = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.requisitionOfficeUnitId = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.fiscalYearId = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.fromDate = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.toDate = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.purchaseDate = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.receiveDate = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.auctionDate = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.piRequisitionId = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.piPurchaseId = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.piReceivedId = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.orgId = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.itemId = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.actionType = rs.getInt(columnNames[i++]);
            piuniqueitemassignmentDTO.stage = rs.getInt(columnNames[i++]);
            piuniqueitemassignmentDTO.taken = rs.getInt(columnNames[i++]);
            piuniqueitemassignmentDTO.returnable = rs.getInt(columnNames[i++]);
            piuniqueitemassignmentDTO.isPersonal = rs.getInt(columnNames[i++]);
            piuniqueitemassignmentDTO.insertedBy = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.insertionTime = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.modifiedBy = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.isDeleted = rs.getInt(columnNames[i++]);
            piuniqueitemassignmentDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            piuniqueitemassignmentDTO.givenProductsCount = rs.getDouble("given");
            return piuniqueitemassignmentDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Pi_monthlyReturnModel buildMonthlyModelFromResultSet(ResultSet rs) {
        try {
            Pi_monthlyReturnModel pi_monthlyReturnModel = new Pi_monthlyReturnModel();
            int i = 0;
            pi_monthlyReturnModel.monthSerial = rs.getInt("monthSerial");
            pi_monthlyReturnModel.returnAmountLong = rs.getLong("count");
            return pi_monthlyReturnModel;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public PiUniqueItemAssignmentDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "pi_unique_item_assignment";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((PiUniqueItemAssignmentDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((PiUniqueItemAssignmentDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

    public int getItemCountWithActionType(long officeUnitId, long fiscalYearId, long itemId, int actionType) {
//        String dateColumnInQuestion = actionType == PiActionTypeEnum.PURCHASE.getValue() ? "purchase_date" : "from_date";
        String sql = String.format(getItemsCountWithType, officeUnitId, fiscalYearId, itemId, actionType);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public int getItemsCountWithoutStore(long fiscalYearId, long itemId, int actionType) {
        String sql = String.format(getItemsCountWithoutStore, fiscalYearId, itemId, actionType);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public int getInitialStockAtDate(long officeUnitId, long fiscalYearId, long itemId, long dateInQuestion) {
        String actionTypeString = " AND action_type=" + PiActionTypeEnum.PURCHASE.getValue() + " ";
        String sql = String.format(getItemsCountInStock, officeUnitId, fiscalYearId, itemId, PiStageEnum.IN_STOCK.getValue(), actionTypeString, "purchase_date", dateInQuestion);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public List<PiUniqueItemAssignmentDTO> getDTOsBeforeDate(long officeUnitId, long fiscalYearId, long itemId, long dateInQuestion) {
        return getDTOs(String.format(getDTOsBeforeDate, officeUnitId, fiscalYearId, itemId, dateInQuestion));
    }

    public int getStockCountBeforeFiscalYear(long officeUnitId, long fiscalYearId, long itemId, long fiscalFirstDate) {
        String sql = String.format(String.format(getStockCountBeforeFiscalYear, officeUnitId, fiscalFirstDate, fiscalFirstDate, fiscalYearId, itemId));
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public List<PiUniqueItemAssignmentDTO> getInStockDTOs(long officeUnitId, long itemId, int limit) {
//        String actionTypeString = " AND action_type=" + PiActionTypeEnum.PURCHASE.getValue() + " ";
        String actionTypeString = " ";
        String sql = String.format(getDTOsByStage, officeUnitId, itemId, PiStageEnum.IN_STOCK.getValue(), actionTypeString, limit);
        return getDTOs(sql);
    }

    public int getTotalItemCountWithActionType(long officeUnitId, long itemId, int actionType, long dateInQuestion) {
        String dateColumnInQuestion = actionType == PiActionTypeEnum.PURCHASE.getValue() ? "purchase_date" : "from_date";
        String sql = String.format(getTotalItemsCountWithType, officeUnitId, itemId, actionType, dateColumnInQuestion, dateInQuestion);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public long getTotalItemCountWithActionTypeForAllStore(long itemId, int actionType) {
        String sql = String.format(getTotalItemCountWithActionTypeForAllStore, itemId, actionType);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public long getStockCountForAllStoreByItemId(long itemId) {
        String sql = String.format(getStockCountForAllStoreByItemId, itemId);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public int getTotalItemCountWithStageOverall(long itemId, int stage, long dateInQuestion) {
        String dateColumnInQuestion = stage == PiStageEnum.IN_STOCK.getValue() ? " action_type = 1 AND purchase_date" : (stage == PiStageEnum.OUT_STOCK.getValue() ? " action_type = 2 AND from_date" : " action_type = 1 AND auction_date");
        String sql = String.format(getTotalItemsCountWithStageOverall, itemId, stage, dateColumnInQuestion, dateInQuestion);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public int getTotalItemCountWithStage(long officeUnitId, long itemId, int stage, long dateInQuestion) {
//        String dateColumnInQuestion = stage == PiStageEnum.IN_STOCK.getValue() ? " action_type = 1 AND purchase_date" : (stage == PiStageEnum.OUT_STOCK.getValue() ? " action_type = 2 AND from_date" : " action_type = 1 AND auction_date");
        String dateColumnInQuestion = stage == PiStageEnum.IN_STOCK.getValue() ? " purchase_date" : (stage == PiStageEnum.OUT_STOCK.getValue() ? " action_type = 2 AND from_date" : " action_type = 1 AND auction_date");
        String sql = String.format(getTotalItemsCountWithStage, officeUnitId, itemId, stage, dateColumnInQuestion, dateInQuestion);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public int getTotalItemCountWithStageAllOver(long officeUnitId, long itemId, int stage) {
        String actionTypeString = " action_type=" + PiActionTypeEnum.PURCHASE.getValue() + " ";
        String sql = String.format(getTotalItemsCountWithStageAllOver, officeUnitId, itemId, stage, actionTypeString);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public int getTotalItemCountWithStageAndPurchaseIdIncludingCurrentTime(long officeUnitId, long itemId, long purchaseId, int stage, long dateInQuestion) {
        String dateColumnInQuestion = stage == PiStageEnum.IN_STOCK.getValue() ? " action_type = 1 AND purchase_date" : (stage == PiStageEnum.OUT_STOCK.getValue() ? " action_type = 2 AND from_date" : " action_type = 1 AND auction_date");
        String sql = String.format(getTotalItemsCountWithStageAndPurchaseIdIncluingCurrentTime, officeUnitId, itemId, purchaseId, stage, dateColumnInQuestion, dateInQuestion);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public int getItemCountInStockByItemIdAndPurchaseId(long itemId, long purchaseId) {
        String sql = String.format(getItemCountInStockByItemIdAndPurchaseId, itemId, purchaseId);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public int getItemCountInStockByItemId(long itemId) {
        String sql = String.format(getItemCountInStockByItemId, itemId);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public int getTotalItemCountWithStageAndPurchaseIdAllover(long officeUnitId, long itemId, long purchaseId, int stage) {
        String dateColumnInQuestion = stage == PiStageEnum.IN_STOCK.getValue() ? " action_type = 1 AND purchase_date" : (stage == PiStageEnum.OUT_STOCK.getValue() ? " action_type = 2 AND from_date" : " action_type = 1 AND auction_date");
        String sql = String.format(getTotalItemsCountWithStageAndPurchaseIdAllOver, officeUnitId, itemId, purchaseId, stage, dateColumnInQuestion);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public void UpdateStageAfterAuction(long stage, long pi_purchase_id, long office_unit_id, long sellingUnit, long auction_date, long prev_stage, long item_id) {

        String sql = String.format(
                UpdateStageAfterAuction, stage, auction_date, System.currentTimeMillis(),
                prev_stage, item_id, pi_purchase_id, office_unit_id, sellingUnit
        );

        logger.debug("UpdateStageAfterAuction: " + sql);

        boolean res = (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
            try {
                st.executeUpdate(sql);
                return true;
            } catch (SQLException ex) {
                logger.error(ex);
                return false;
            }
        });
    }

    public void updateReturnedItem(long stage, long toDate, long lastModificationTime, long modifiedBy,
                                   long piUniqueItemId, long actionType, long ID, long returnable, long orgId) {

        String sql = String.format(
                updateReturnedItem, stage, toDate, lastModificationTime, modifiedBy,
                piUniqueItemId, actionType, ID, orgId
        );

        logger.debug("updateReturnedItem: " + sql);

        boolean res = (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
            try {
                st.executeUpdate(sql);
                return true;
            } catch (SQLException ex) {
                logger.error(ex);
                return false;
            }
        });
    }

    public List<PiUniqueItemAssignmentDTO> getGivenItems(long employeeOrgId, long stage, long action_type, long returnable) {
        String sql = String.format(getGivenItems, employeeOrgId, stage, action_type, returnable);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }


    public List<PiUniqueItemAssignmentDTO> getReturnedItems(long orgId, long stage, long actionType, String returnDate, long returnable) throws InvalidDataException, AlreadyApprovedException {

        List<PiUniqueItemAssignmentDTO> piUniqueItemAssignmentDTOS = new ArrayList<>();
        ConnectionAndStatementUtil.getReadPrepareStatement(ps -> {
            try {
                int index = 0;
                ps.setLong(++index, orgId);
                ps.setLong(++index, stage);
                ps.setLong(++index, returnable);
                //ps.setString(++index, "%" + returnDate + "%");
                ps.setInt(++index, 0);
                logger.info(ps);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    PiUniqueItemAssignmentDTO piUniqueItemAssignmentDTO = buildObjectFromResultSet(rs);
                    piUniqueItemAssignmentDTOS.add(piUniqueItemAssignmentDTO);
                }
                return true;
            } catch (SQLException ex) {
                logger.error(ex);
                return false;
            }
        }, getReturnedItems);

        return piUniqueItemAssignmentDTOS;
    }

    public int getInStockCountInDateRange(long officeUnitId, long itemId, long firstDate, long lastDate) {
        String sql = String.format(String.format(getInStockCountInDateRange, officeUnitId, itemId,
                firstDate, lastDate, firstDate, lastDate));
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public int getDistributionCountInDateRange(long officeUnitId, long itemId, long firstDate, long lastDate) {
        String sql = String.format(String.format(getDistributionCount, officeUnitId, itemId, firstDate, lastDate, firstDate, lastDate));
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public List<Pi_monthlyReturnModel> getReturnCountInDateRange(long officeUnitId, long itemId, long firstDate, long lastDate) {
        String sql = String.format(String.format(getReturnedCountByMonth, officeUnitId, itemId, firstDate, lastDate));
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildMonthlyModelFromResultSet);
    }

    public void moveTakenItems(long taken, long toDate, long lastModificationTime, long modifiedBy, long ID, long orgId) {

        String sql = String.format(moveTakenItems, taken, toDate, lastModificationTime, modifiedBy, ID, orgId);

        logger.debug("moveTakenItems: " + sql);

        boolean res = (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
            try {
                st.executeUpdate(sql);
                return true;
            } catch (SQLException ex) {
                logger.error(ex);
                return false;
            }
        });
    }

    public void moveTakenItemsWithoutToDate(long taken, long lastModificationTime, long modifiedBy, long ID, long orgId) {

        String sql = String.format(moveTakenItemsWithoutToDate, taken, lastModificationTime, modifiedBy, ID, orgId);

        logger.debug("moveTakenItems: " + sql);

        boolean res = (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
            try {
                st.executeUpdate(sql);
                return true;
            } catch (SQLException ex) {
                logger.error(ex);
                return false;
            }
        });
    }

    public List<PiUniqueItemAssignmentDTO> getGivenAndMovableItem(long employeeOrgId, long stage, long action_type, long returnable, long taken, long toDate) {
        String sql = String.format(getGivenAndMovableItem, employeeOrgId, stage, action_type, returnable, taken, toDate);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public List<PiUniqueItemAssignmentDTO> getMovableItem(long employeeOrgId, long stage, long action_type, long returnable, long fromDate, String toDateString) {
        String sql = String.format(getMovableItem, employeeOrgId, stage, action_type, returnable, fromDate, toDateString);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public void handleInventoryAfterEmployeeAssign(long employee_record_id, long office_unit_organogram_id, long joining_date) {
        List<EmployeeOfficeDTO> employeeOfficeDTOS = employeeOfficesDAO.getActiveOfficesByEmployeeRecordId(employee_record_id);
        if (employeeOfficeDTOS != null && employeeOfficeDTOS.size() == 1) {
            assignItemsToNewDesk(employee_record_id, office_unit_organogram_id, joining_date);
        }
    }

    public void handleInventoryAfterEmployeeSuspend(long employee_record_id) {
        List<EmployeeOfficeDTO> employeeOfficeDTOS = employeeOfficesDAO.getActiveOfficesByEmployeeRecordId(employee_record_id);
        if (employeeOfficeDTOS != null && !employeeOfficeDTOS.isEmpty()) {
            EmployeeOfficeDTO employeeOfficeDTO = employeeOfficeDTOS
                    .stream()
                    .filter(dto -> dto.isDefault == 1)
                    .findFirst()
                    .orElse(employeeOfficeDTOS.get(0));
            assignItemsToNewDesk(employee_record_id, employeeOfficeDTO.officeUnitOrganogramId, employeeOfficeDTO.joiningDate);
        }
    }

    public void assignItemsToNewDesk(long employee_record_id, long office_unit_organogram_id, long joining_date) {
        EmployeeOfficeDTO employeeOfficeDTO = employeeOfficesDAO.getLastInactiveByEmployeeRecordId(employee_record_id);
        if (employeeOfficeDTO == null) return;
        long toDateTime = employeeOfficeDTO.lastOfficeDate + (24 * 60 * 60 * 1000 - 1);

        getGivenAndMovableItem(employeeOfficeDTO.officeUnitOrganogramId, PiStageEnum.OUT_STOCK.getValue(), PiActionTypeEnum.DELIVERY.getValue(), 1, 1, toDateTime)
                .stream()
                .forEach(
                        piUniqueItemAssignmentDTO -> {
                            piUniqueItemAssignmentDTO.orgId = office_unit_organogram_id;
                            piUniqueItemAssignmentDTO.fromDate = joining_date;
                            piUniqueItemAssignmentDTO.toDate = -1;
                            piUniqueItemAssignmentDTO.officeUnitId = OfficeUnitOrganogramsRepository.getInstance().getById(office_unit_organogram_id).office_unit_id;
                            try {
                                add(piUniqueItemAssignmentDTO);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                );


    }

    public long getLatestRequisitionIdForItem(long uniqueItemId) {
        String sql = String.format(getLatestRequisitionIdForItem, uniqueItemId);
        List<PiUniqueItemAssignmentDTO> dtos = PiUniqueItemAssignmentDAO.getInstance().getDTOs(sql);
        if (dtos == null || dtos.isEmpty()) {
            return -1;
        } else {
            long requisitionItemId = dtos.get(0).piRequisitionId;
            Pi_requisition_itemDTO requisitionItemDTO = Pi_requisition_itemDAO.getInstance().getDTOByID(requisitionItemId);
            if (requisitionItemDTO == null) {
                return -1;
            } else {
                return requisitionItemDTO.piRequisitionId;
            }
        }
    }

    public Long buildLongFromResultSet(ResultSet rs) {
        try {
            return rs.getLong("item_id");
        } catch (Exception e) {
            logger.debug(e);
        }
        return -1L;
    }

    public int getInStockCountByItemIdAndDateRange(long itemId, long startDate, long endDate) {
        String sql = String.format(getInStockCountByItemIdAndDateRange, itemId, startDate, endDate);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public int getPurchasedAndReceivedItemCountByItemIdAndDateRange(long itemId, long startDate, long endDate) {
        String sql = String.format(getPurchasedAndReceivedItemCountByItemIdAndDateRange, itemId, startDate, endDate);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public int getDistributeCountByItemIdAndDateRange(long itemId, long startDate, long endDate) {
        String sql = String.format(getDistributeCountByItemIdAndDateRange, itemId, startDate, endDate);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, 0);
    }

    public List<Pi_stock_summaryModel> getDTOsForStockSummaryTable(long itemGroupId, long itemTypeId, long itemId,
                                                                   long startDate, long endDate) throws Exception {
        String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        String sql = String.format(getItemIdsInTimeRange, startDate, endDate);
        List<Long> itemIDs = ConnectionAndStatementUtil.getListOfT(sql, this::buildLongFromResultSet);
        List<Pi_stock_summaryModel> summaryModels = new ArrayList<>();
        for (Long id : itemIDs) {
            Pi_stock_summaryModel summaryModel = new Pi_stock_summaryModel();
            summaryModel.itemName = Procurement_goodsRepository.getInstance().geText(language, id);
            summaryModel.openingBalance = PiUniqueItemAssignmentDAO.getInstance()
                    .getInStockCountByItemIdAndDateRange(id, 0, startDate);
            summaryModel.receivedQuantity = PiUniqueItemAssignmentDAO.getInstance()
                    .getPurchasedAndReceivedItemCountByItemIdAndDateRange(id, startDate, endDate);
            summaryModel.totalStock = summaryModel.openingBalance + summaryModel.receivedQuantity;
            summaryModel.issued = PiUniqueItemAssignmentDAO.getInstance()
                    .getDistributeCountByItemIdAndDateRange(id, startDate, endDate);
            summaryModel.closingBalance = summaryModel.totalStock - summaryModel.issued;
            summaryModels.add(summaryModel);
        }
        if (summaryModels.isEmpty()) {
            UtilCharacter.throwException("এই সময়সীমার মধ্যে কোন আইটেমের তথ্য পাওয়া যায় নি!", "No data found in this time frame!");
        }
        return summaryModels;
    }
}
