package pi_unique_item;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Pi_unique_itemDAO implements CommonDAOService<Pi_unique_itemDTO> {

    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private String[] columnNames = null;

    public Pi_unique_itemDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "office_unit_id",
                        "fiscal_year_id",
                        "item_id",
                        "tracking_number",
                        "pi_purchase_id",
                        "pi_received_id",
                        "status",
                        "is_personal",
                        "inserted_by",
                        "insertion_time",
                        "modified_by",
                        "isDeleted",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Pi_unique_itemDAO INSTANCE = new Pi_unique_itemDAO();
    }

    public static Pi_unique_itemDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Pi_unique_itemDTO pi_unique_itemDTO) {
        pi_unique_itemDTO.searchColumn = "";
    }

    @Override
    public void set(PreparedStatement ps, Pi_unique_itemDTO pi_unique_itemDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(pi_unique_itemDTO);
        if (isInsert) {
            ps.setObject(++index, pi_unique_itemDTO.iD);
        }
        ps.setObject(++index, pi_unique_itemDTO.officeUnitId);
        ps.setObject(++index, pi_unique_itemDTO.fiscalYearId);
        ps.setObject(++index, pi_unique_itemDTO.itemId);
        ps.setObject(++index, pi_unique_itemDTO.trackingNumber);
        ps.setObject(++index, pi_unique_itemDTO.piPurchaseId);
        ps.setObject(++index, pi_unique_itemDTO.piReceivedId);
        ps.setObject(++index, pi_unique_itemDTO.status);
        ps.setObject(++index, pi_unique_itemDTO.isPersonal);
        ps.setObject(++index, pi_unique_itemDTO.insertedBy);
        ps.setObject(++index, pi_unique_itemDTO.insertionTime);
        ps.setObject(++index, pi_unique_itemDTO.modifiedBy);
        if (isInsert) {
            ps.setObject(++index, pi_unique_itemDTO.isDeleted);
        }
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, pi_unique_itemDTO.iD);
        }
    }

    @Override
    public Pi_unique_itemDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Pi_unique_itemDTO pi_unique_itemDTO = new Pi_unique_itemDTO();
            int i = 0;
            pi_unique_itemDTO.iD = rs.getLong(columnNames[i++]);
            pi_unique_itemDTO.officeUnitId = rs.getLong(columnNames[i++]);
            pi_unique_itemDTO.fiscalYearId = rs.getLong(columnNames[i++]);
            pi_unique_itemDTO.itemId = rs.getLong(columnNames[i++]);
            pi_unique_itemDTO.trackingNumber = rs.getString(columnNames[i++]);
            pi_unique_itemDTO.piPurchaseId = rs.getLong(columnNames[i++]);
            pi_unique_itemDTO.piReceivedId = rs.getLong(columnNames[i++]);
            pi_unique_itemDTO.status = rs.getInt(columnNames[i++]);
            pi_unique_itemDTO.isPersonal = rs.getInt(columnNames[i++]);
            pi_unique_itemDTO.insertedBy = rs.getLong(columnNames[i++]);
            pi_unique_itemDTO.insertionTime = rs.getLong(columnNames[i++]);
            pi_unique_itemDTO.modifiedBy = rs.getLong(columnNames[i++]);
            pi_unique_itemDTO.isDeleted = rs.getInt(columnNames[i++]);
            pi_unique_itemDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            return pi_unique_itemDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Pi_unique_itemDTO getDTOByID(long id) {
        Pi_unique_itemDTO pi_unique_itemDTO = null;
        try {
            pi_unique_itemDTO = getDTOFromID(id);
            if (pi_unique_itemDTO != null) {
                PiUniqueItemAssignmentDAO piUniqueItemAssignmentDAO = PiUniqueItemAssignmentDAO.getInstance();
                List<PiUniqueItemAssignmentDTO> piUniqueItemAssignmentDTOList = (List<PiUniqueItemAssignmentDTO>) piUniqueItemAssignmentDAO.getDTOsByParent("pi_unique_item_id", pi_unique_itemDTO.iD);
                pi_unique_itemDTO.piUniqueItemAssignmentDTOList = piUniqueItemAssignmentDTOList;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return pi_unique_itemDTO;
    }

    @Override
    public String getTableName() {
        return "pi_unique_item";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_unique_itemDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_unique_itemDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

}
	