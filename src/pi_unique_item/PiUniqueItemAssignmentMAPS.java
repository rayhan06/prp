package pi_unique_item;
import java.util.*; 
import util.*;


public class PiUniqueItemAssignmentMAPS extends CommonMaps
{	
	public PiUniqueItemAssignmentMAPS(String tableName)
	{
		



		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Pi Unique Item Id".toLowerCase(), "piUniqueItemId".toLowerCase());
		java_Text_map.put("Office Unit Id".toLowerCase(), "officeUnitId".toLowerCase());
		java_Text_map.put("Fiscal Year Id".toLowerCase(), "fiscalYearId".toLowerCase());
		java_Text_map.put("From Date".toLowerCase(), "fromDate".toLowerCase());
		java_Text_map.put("To Date".toLowerCase(), "toDate".toLowerCase());
		java_Text_map.put("Pi Requisition Id".toLowerCase(), "piRequisitionId".toLowerCase());
		java_Text_map.put("Org Id".toLowerCase(), "orgId".toLowerCase());
		java_Text_map.put("Item Id".toLowerCase(), "itemId".toLowerCase());
		java_Text_map.put("Action".toLowerCase(), "actionType".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Insertion Time".toLowerCase(), "insertionTime".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}