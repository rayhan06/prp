package login_log;

import common.EmployeeCommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Login_logDAO implements EmployeeCommonDAOService<Login_logDTO> {

    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private final String[] columnNames;

    private Login_logDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "employee_records_id",
                        "userId",
                        "name_en",
                        "name_bn",
                        "employee_office_id",
                        "ip_address",
                        "userInformation",
                        "Operating_system",
                        "browser",
                        "device",
                        "isSuccessful",
                        "error_message",
                        "search_column",
                        "modified_by",
                        "inserted_by",
                        "insertion_date",
                        "lastModificationTime",
                        "isDeleted",
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("userId", " and (userId like ?)");
        searchMap.put("name_en", " and (name_en like ?)");
        searchMap.put("name_bn", " and (name_bn like ?)");
        searchMap.put("device_type", " and (device_type = ?)");
        searchMap.put("browser", " and (browser like ?)");
        searchMap.put("error_message", " and (error_message like ?)");
        searchMap.put("modified_by", " and (modified_by like ?)");
        searchMap.put("inserted_by", " and (inserted_by like ?)");
        searchMap.put("insertion_date_start", " and (insertion_date >= ?)");
        searchMap.put("insertion_date_end", " and (insertion_date <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Login_logDAO INSTANCE = new Login_logDAO();
    }

    public static Login_logDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Login_logDTO login_logDTO) {
        login_logDTO.searchColumn = "";
    }

    @Override
    public void set(PreparedStatement ps, Login_logDTO login_logDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(login_logDTO);
        if (isInsert) {
            ps.setObject(++index, login_logDTO.iD);
        }
        ps.setObject(++index, login_logDTO.employeeRecordsId);
        ps.setObject(++index, login_logDTO.userId);
        ps.setObject(++index, login_logDTO.nameEn);
        ps.setObject(++index, login_logDTO.nameBn);
        ps.setObject(++index, login_logDTO.employeeOfficeId);
        ps.setObject(++index, login_logDTO.IPAddress);
        ps.setObject(++index, login_logDTO.userInformation);
        ps.setObject(++index, login_logDTO.operatingSystem);
        ps.setObject(++index, login_logDTO.browserName);
        ps.setObject(++index, login_logDTO.deviceName);
        ps.setObject(++index, login_logDTO.isSuccessful);
        ps.setObject(++index, login_logDTO.errorMessage);
        ps.setObject(++index, login_logDTO.searchColumn);
        ps.setObject(++index, login_logDTO.modifiedBy);
        ps.setObject(++index, login_logDTO.insertedBy);
        ps.setObject(++index, login_logDTO.insertionDate);
        ps.setObject(++index, lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, login_logDTO.isDeleted);
        }
        if (!isInsert) {
            ps.setObject(++index, login_logDTO.iD);
        }
    }

    @Override
    public Login_logDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Login_logDTO login_logDTO = new Login_logDTO();
            int i = 0;
            login_logDTO.iD = rs.getLong(columnNames[i++]);
            login_logDTO.employeeRecordsId = rs.getLong(columnNames[i++]);
            login_logDTO.userId = rs.getString(columnNames[i++]);
            login_logDTO.nameEn = rs.getString(columnNames[i++]);
            login_logDTO.nameBn = rs.getString(columnNames[i++]);
            login_logDTO.employeeOfficeId = rs.getLong(columnNames[i++]);
            login_logDTO.IPAddress = rs.getString(columnNames[i++]);
            login_logDTO.userInformation = rs.getString(columnNames[i++]);
            login_logDTO.operatingSystem = rs.getString(columnNames[i++]);
            login_logDTO.browserName = rs.getString(columnNames[i++]);
            login_logDTO.deviceName = rs.getString(columnNames[i++]);
            login_logDTO.isSuccessful = rs.getBoolean(columnNames[i++]);
            login_logDTO.errorMessage = rs.getString(columnNames[i++]);
            login_logDTO.searchColumn = rs.getString(columnNames[i++]);
            login_logDTO.modifiedBy = rs.getLong(columnNames[i++]);
            login_logDTO.insertedBy = rs.getLong(columnNames[i++]);
            login_logDTO.insertionDate = rs.getLong(columnNames[i++]);
            login_logDTO.lastModificationTime = rs.getLong(columnNames[i]);
            login_logDTO.isDeleted = rs.getInt(columnNames[i++]);
            return login_logDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Login_logDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "login_log";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Login_logDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Login_logDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

}
	