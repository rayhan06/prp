package login_log;

import util.CommonEmployeeDTO;


public class Login_logDTO extends CommonEmployeeDTO
{

    public String userId = "";
    public String nameEn = "";
    public String nameBn = "";
	public long employeeOfficeId = -1;
    public String IPAddress = "";
    public String userInformation = "";
    public String operatingSystem = "";
    public String browserName = "";
    public String deviceName = "";
	public boolean isSuccessful = false;
    public String errorMessage = "";
	public long modifiedBy = -1;
	public long insertedBy = -1;
	public long insertionDate = -1;
	
	
    @Override
	public String toString() {
            return "$Login_logDTO[" +
            " iD = " + iD +
            " employeeRecordsId = " + employeeRecordsId +
            " userId = " + userId +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " employeeOfficeId = " + employeeOfficeId +
            " IPAddress = " + IPAddress +
            " userInformation = " + userInformation +
            " isSuccessful = " + isSuccessful +
            " errorMessage = " + errorMessage +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            " modifiedBy = " + modifiedBy +
            " insertedBy = " + insertedBy +
            " insertionDate = " + insertionDate +
            "]";
    }

}