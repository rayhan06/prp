package office_layer_base_settings.office_custom_layer_ministry_wise;

import test_lib.RQueryBuilder;

public class OfficeCustomLayerMinistryWiseDAO {

    public String getMinistry() {
        String sql = "select id, name_eng, name_bng from office_ministries where isDeleted = 0;";

        RQueryBuilder<OfficeCustomLayerMinistryWiseModel.OfficeMinistry> rQueryBuilder = new RQueryBuilder<>();
        return rQueryBuilder.setSql(sql).of(OfficeCustomLayerMinistryWiseModel.OfficeMinistry.class).buildJson();
    }

    public String getCustomLayerByMinistry(String ministry_id) {
        String sql = String.format("select\n" +
                "office_layers.id as layer_id,\n" +
                "office_layers.layer_name_eng,\n" +
                "office_layers.layer_name_bng,\n" +
                "office_custom_layers.id as custom_layer_id,\n" +
                "office_custom_layers.name as custom_layer_name\n" +
                "from office_layers, office_custom_layers where office_layers.office_ministry_id = %s \n" +
                "and office_custom_layers.id = office_layers.custom_layer_id\n" +
                "and office_custom_layers.isDeleted = 0 and office_layers.isDeleted = 0;", ministry_id);

        RQueryBuilder<OfficeCustomLayerMinistryWiseModel.OfficeLayerAndCustomLayer> rQueryBuilder = new RQueryBuilder<>();
        return rQueryBuilder.setSql(sql).of(OfficeCustomLayerMinistryWiseModel.OfficeLayerAndCustomLayer.class).buildJson();
    }

    public String getCustomLayer() {
        String sql = ("select id, name from office_custom_layers;");

        RQueryBuilder<OfficeCustomLayerMinistryWiseModel.CustomLayer> rQueryBuilder = new RQueryBuilder<>();
        return rQueryBuilder.setSql(sql).of(OfficeCustomLayerMinistryWiseModel.CustomLayer.class).buildJson();
    }

    public boolean updateCustomLayer(String id, String customLayerId) {
        String sql = String.format("update office_layers set custom_layer_id = %s where id = %s;", customLayerId, id);
        RQueryBuilder<Boolean> rQueryBuilder = new RQueryBuilder<>();

        return rQueryBuilder.setSql(sql).of(Boolean.class).buildUpdate();
    }
}
