package office_layer_base_settings.office_custom_layer_ministry_wise;

public class OfficeCustomLayerMinistryWiseModel {

    public static class OfficeMinistry {
        private int id;
        private String name_eng;
        private String name_bng;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName_eng() {
            return name_eng;
        }

        public void setName_eng(String name_eng) {
            this.name_eng = name_eng;
        }

        public String getName_bng() {
            return name_bng;
        }

        public void setName_bng(String name_bng) {
            this.name_bng = name_bng;
        }
    }

    public static class OfficeLayerAndCustomLayer {
        private int layer_id;
        private String layer_name_eng;
        private String layer_name_bng;
        private String custom_layer_id;
        private String custom_layer_name;

        public int getLayer_id() {
            return layer_id;
        }

        public void setLayer_id(int layer_id) {
            this.layer_id = layer_id;
        }

        public String getLayer_name_eng() {
            return layer_name_eng;
        }

        public void setLayer_name_eng(String layer_name_eng) {
            this.layer_name_eng = layer_name_eng;
        }

        public String getLayer_name_bng() {
            return layer_name_bng;
        }

        public void setLayer_name_bng(String layer_name_bng) {
            this.layer_name_bng = layer_name_bng;
        }

        public String getCustom_layer_id() {
            return custom_layer_id;
        }

        public void setCustom_layer_id(String custom_layer_id) {
            this.custom_layer_id = custom_layer_id;
        }

        public String getCustom_layer_name() {
            return custom_layer_name;
        }

        public void setCustom_layer_name(String custom_layer_name) {
            this.custom_layer_name = custom_layer_name;
        }
    }

    public static class CustomLayer {
        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
