/*
package office_layer_base_settings.office_custom_layer_ministry_wise;

import employee_batches.Employee_batchesServlet;
import login.LoginDTO;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/OfficeCustomLayerMinistryWiseServlet")
@MultipartConfig
public class OfficeCustomLayerMinistryWiseServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Employee_batchesServlet.class);

    public OfficeCustomLayerMinistryWiseServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getAddPage":
                    this.getAddPage(request, response);
                    break;
                case "getMinistry": {
                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();
                    out.print(new OfficeCustomLayerMinistryWiseDAO().getMinistry());
                    out.flush();
                    break;
                }
                case "getCustomLayerByMinistry": {
                    String ministry_id = request.getParameter("ministry_id");

                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();
                    out.print(new OfficeCustomLayerMinistryWiseDAO().getCustomLayerByMinistry(ministry_id));
                    out.flush();
                    break;
                }
                case "getCustomLayer": {

                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();
                    out.print(new OfficeCustomLayerMinistryWiseDAO().getCustomLayer());
                    out.flush();
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        System.out.println("doPost");

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("updateCustomLayer")) {

                String id = request.getParameter("id");
                String customLayerId = request.getParameter("customLayer_id");

                response.setContentType("application/json");
                PrintWriter out = response.getWriter();
                out.print(new OfficeCustomLayerMinistryWiseDAO().updateCustomLayer(id, customLayerId));
                out.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("ID", -1L);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("office_layer_base_settings/office_custom_layer_ministry_wise/OfficeCustomLayerMinistryWise.jsp");
        requestDispatcher.forward(request, response);
    }
}
*/
