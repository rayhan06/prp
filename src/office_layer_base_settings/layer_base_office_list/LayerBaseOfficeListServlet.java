/*
package office_layer_base_settings.layer_base_office_list;

import employee_batches.Employee_batchesServlet;
import login.LoginDTO;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/LayerBaseOfficeListServlet")
@MultipartConfig
public class LayerBaseOfficeListServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Employee_batchesServlet.class);

    public LayerBaseOfficeListServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getAddPage":
                    this.getAddPage(request, response);
                    break;
                case "getOffice": {
                    String customLayerId = request.getParameter("customLayerId");

                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();
                    out.print(new LayerBaseOfficeDAO().getOffices(customLayerId));
                    out.flush();
                    break;
                }
                case "getOfficeLayer": {
                    String ministryId = request.getParameter("ministryId");

                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();
                    out.print(new LayerBaseOfficeDAO().getOfficeLayers(ministryId));
                    out.flush();
                    break;
                }
                case "getCustomLayer": {
                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();
                    out.print(new LayerBaseOfficeDAO().getOfficeCustomLayers());
                    out.flush();
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        System.out.println("doPost");

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("updateLayer")) {
                String id = request.getParameter("id");
                String office_layer = request.getParameter("office_layer");
                String custom_layer = request.getParameter("custom_layer");

                response.setContentType("application/json");
                PrintWriter out = response.getWriter();
                out.print(new LayerBaseOfficeDAO().updateOfficeLayer(id, office_layer, custom_layer));
                out.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("ID", -1L);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("office_layer_base_settings/layer_base_office_list/LayerBaseOfficeList.jsp");
        requestDispatcher.forward(request, response);
    }
}*/
