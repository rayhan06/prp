package office_layer_base_settings.layer_base_office_list;

import test_lib.RQueryBuilder;

public class LayerBaseOfficeDAO {

    public LayerBaseOfficeDAO() {
    }

    public String getOffices(String customLayerId) {
        String sql = String.format("select id, office_ministry_id, office_name_eng, office_name_bng, office_layer_id, custom_layer_id from offices where custom_layer_id = %s and isDeleted = 0;", customLayerId);

        RQueryBuilder<LayerBaseOfficeModel.OfficeModel> rQueryBuilder = new RQueryBuilder<>();
        return rQueryBuilder.setSql(sql).of(LayerBaseOfficeModel.OfficeModel.class).buildJson();
    }

    public String getOfficeLayers(String ministryId) {
        String sql = String.format("select id, layer_name_eng, layer_name_bng from office_layers where office_layers.office_ministry_id = %s and isDeleted = 0 ", ministryId);

        RQueryBuilder<LayerBaseOfficeModel.OfficeLayerModel> rQueryBuilder = new RQueryBuilder<>();
        return rQueryBuilder.setSql(sql).of(LayerBaseOfficeModel.OfficeLayerModel.class).buildJson();
    }

    public String getOfficeCustomLayers() {
        String sql = ("select id, name from office_custom_layers where isDeleted = 0;");

        RQueryBuilder<LayerBaseOfficeModel.OfficeCustomLayer> rQueryBuilder = new RQueryBuilder<>();
        return rQueryBuilder.setSql(sql).of(LayerBaseOfficeModel.OfficeCustomLayer.class).buildJson();
    }

    public boolean updateOfficeLayer(String id, String office_layer, String custom_layer) {
        String sql = String.format("update offices set offices.office_layer_id = %s, offices.custom_layer_id = %s where id = %s;", office_layer, custom_layer, id);

        RQueryBuilder<Boolean> rQueryBuilder = new RQueryBuilder<>();

        return rQueryBuilder.setSql(sql).of(Boolean.class).buildUpdate();
    }
}
