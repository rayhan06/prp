/*
package office_layer_base_settings.office_custom_layers;

import employee_batches.Employee_batchesServlet;
import login.LoginDTO;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import test_lib.RModelBuilder;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/OfficeCustomLayerServlet")
@MultipartConfig
public class OfficeCustomLayerServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Employee_batchesServlet.class);

    public OfficeCustomLayerServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getAddPage":
                    this.getAddPage(request, response);
                    break;
                case "getCustomLayer": {
                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();
                    out.print(new OfficeCustomLayerDAO().getOfficeCustomLayers());
                    out.flush();
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        System.out.println("doPost");

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("addCustomLayer")) {

                RModelBuilder<CustomLayerModel.office_custom_layers> modelBuilder = new RModelBuilder<>();
                CustomLayerModel.office_custom_layers x = modelBuilder.of(CustomLayerModel.office_custom_layers.class).request(request).buildModel();
                */
/*
                byte ptext[] = x.getName().getBytes("ISO-8859-1");
                x.setName(new String(ptext, "UTF-8")); *//*


                response.setContentType("application/json");
                PrintWriter out = response.getWriter();
                out.print(new OfficeCustomLayerDAO().addCustomLayer(x));
                out.flush();
            } else if (actionType.equals("updateCustomLayer")) {

                RModelBuilder<CustomLayerModel.office_custom_layers> modelBuilder = new RModelBuilder<>();
                CustomLayerModel.office_custom_layers x = modelBuilder.of(CustomLayerModel.office_custom_layers.class).request(request).buildModel();
                */
/*
                byte ptext[] = x.getName().getBytes("ISO-8859-1");
                x.setName(new String(ptext, "UTF-8")); *//*


                response.setContentType("application/json");
                PrintWriter out = response.getWriter();
                out.print(new OfficeCustomLayerDAO().updateCustomLayer(x));
                out.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("ID", -1L);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("office_layer_base_settings/office_custom_layer/OfficeCustomLayer.jsp");
        requestDispatcher.forward(request, response);
    }
}
*/
