package office_layer_base_settings.office_custom_layers;

import dbm.DBMW;
import test_lib.RInsertQueryBuilder;
import test_lib.RQueryBuilder;
import test_lib.RUpdateQueryBuilder;

public class OfficeCustomLayerDAO {

    public String getOfficeCustomLayers() {
        String sql = ("select id, name, layer_level from office_custom_layers where isDeleted = 0;");

        RQueryBuilder<CustomLayerModel.OfficeCustomLayer> rQueryBuilder = new RQueryBuilder<>();
        String data = rQueryBuilder.setSql(sql).of(CustomLayerModel.OfficeCustomLayer.class).buildJson();
        return data;
    }

    public boolean addCustomLayer(CustomLayerModel.office_custom_layers officeCustomLayer) {

        if (officeCustomLayer.getId() == 0) {
            try {
                officeCustomLayer.setId((int) DBMW.getInstance().getNextSequenceId(CustomLayerModel.office_custom_layers.class.getSimpleName()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        RInsertQueryBuilder<CustomLayerModel.office_custom_layers> queryBuilder = new RInsertQueryBuilder<>();
        queryBuilder.of(CustomLayerModel.office_custom_layers.class).model(officeCustomLayer).buildInsert();

        return true;
    }

    public boolean updateCustomLayer(CustomLayerModel.office_custom_layers officeCustomLayer) {
        RUpdateQueryBuilder<CustomLayerModel.office_custom_layers> queryBuilder = new RUpdateQueryBuilder<>();
        queryBuilder.of(CustomLayerModel.office_custom_layers.class).model(officeCustomLayer).buildUpdate(officeCustomLayer.getId());

        return true;
    }
}
