package office_layer_base_settings.office_custom_layers;

public class CustomLayerModel {

    public static class OfficeCustomLayer {
        private int id;
        private int layer_level;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLayer_level() {
            return layer_level;
        }

        public void setLayer_level(int layer_level) {
            this.layer_level = layer_level;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class office_custom_layers {
        private int id;
        private String name;
        private int layer_level;
        private int created_by;
        private int modified_by;
        private long created;
        private long modified;
        private boolean isDeleted;
        private long lastModificationTime;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getLayer_level() {
            return layer_level;
        }

        public void setLayer_level(int layer_level) {
            this.layer_level = layer_level;
        }

        public int getCreated_by() {
            return created_by;
        }

        public void setCreated_by(int created_by) {
            this.created_by = created_by;
        }

        public int getModified_by() {
            return modified_by;
        }

        public void setModified_by(int modified_by) {
            this.modified_by = modified_by;
        }

        public long getCreated() {
            return created;
        }

        public void setCreated(long created) {
            this.created = created;
        }

        public long getModified() {
            return modified;
        }

        public void setModified(long modified) {
            this.modified = modified;
        }

        public boolean getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(boolean deleted) {
            isDeleted = deleted;
        }

        public long getLastModificationTime() {
            return lastModificationTime;
        }

        public void setLastModificationTime(long lastModificationTime) {
            this.lastModificationTime = lastModificationTime;
        }
    }
}
