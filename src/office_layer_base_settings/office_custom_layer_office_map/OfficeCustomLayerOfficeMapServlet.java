/*
package office_layer_base_settings.office_custom_layer_office_map;

import employee_batches.Employee_batchesServlet;
import login.LoginDTO;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/OfficeCustomLayerOfficeMapServlet")
@MultipartConfig
public class OfficeCustomLayerOfficeMapServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Employee_batchesServlet.class);

    public OfficeCustomLayerOfficeMapServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getAddPage":
                    this.getAddPage(request, response);
                    break;
                case "getOfficeLayer": {
                    String custom_layer_id = request.getParameter("custom_layer_id");

                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();
                    out.print(new OfficeCustomLayerOfficeMapDAO().getOfficeLayersByCustomLayerId(custom_layer_id));
                    out.flush();
                    break;
                }
                case "getOfficesByCustomLayer": {
                    String custom_layer_id = request.getParameter("custom_layer_id");

                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();
                    out.print(new OfficeCustomLayerOfficeMapDAO().getOfficesByCustomLayerId(custom_layer_id));
                    out.flush();
                    break;
                }
                case "getCustomLayer": {

                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();
                    out.print(new OfficeCustomLayerOfficeMapDAO().getCustomLayer());
                    out.flush();
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        System.out.println("doPost");

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("updateOffice")) {

                String id = request.getParameter("id");
                String office_layer_id = request.getParameter("office_layer");
                String custom_layer_id = request.getParameter("custom_layer");

                response.setContentType("application/json");
                PrintWriter out = response.getWriter();
                out.print(new OfficeCustomLayerOfficeMapDAO().updateOfficeLayer(id, office_layer_id, custom_layer_id));
                out.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("ID", -1L);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("office_layer_base_settings/office_custom_layer_office_map/OfficeCustomLayerOfficeMap.jsp");
        requestDispatcher.forward(request, response);
    }
}*/
