package office_layer_base_settings.office_custom_layer_office_map;

import test_lib.RQueryBuilder;

public class OfficeCustomLayerOfficeMapDAO {

    public String getCustomLayer() {
        String sql = ("select id, name from office_custom_layers;");

        RQueryBuilder<OfficeCustomLayerOfficeMapModel.CustomLayer> rQueryBuilder = new RQueryBuilder<>();
        String data = rQueryBuilder.setSql(sql).of(OfficeCustomLayerOfficeMapModel.CustomLayer.class).buildJson();
        return data;
    }

    public String getOfficesByCustomLayerId(String custom_layer_id) {
        String sql = String.format("select \n" +
                " offices.id as office_id,\n" +
                " offices.office_name_eng, \n" +
                " offices.office_name_bng,\n" +
                " offices.office_layer_id, \n" +
                " office_layers.layer_name_eng, \n" +
                " office_layers.layer_name_bng, \n" +
                " offices.custom_layer_id,\n" +
                " office_custom_layers.name \n" +
                "from offices, office_layers, office_custom_layers \n" +
                "where offices.custom_layer_id = %s \n" +
                "and offices.office_layer_id = office_layers.id\n" +
                "and offices.custom_layer_id = office_custom_layers.id;", custom_layer_id);

        RQueryBuilder<OfficeCustomLayerOfficeMapModel.OfficeLayerCustomLayer> rQueryBuilder = new RQueryBuilder<>();

        return rQueryBuilder.setSql(sql).of(OfficeCustomLayerOfficeMapModel.OfficeLayerCustomLayer.class).buildJson();
    }

    public String getOfficeLayersByCustomLayerId(String custom_layer_id) {
        String sql = String.format("select office_layers.id, office_layers.layer_name_eng, office_layers.layer_name_bng  from office_layers where office_layers.custom_layer_id = %s;", custom_layer_id);

        RQueryBuilder<OfficeCustomLayerOfficeMapModel.OfficeLayer> rQueryBuilder = new RQueryBuilder<>();

        return rQueryBuilder.setSql(sql).of(OfficeCustomLayerOfficeMapModel.OfficeLayer.class).buildJson();
    }

    public boolean updateOfficeLayer(String id, String office_layer_id, String custom_layer_id) {
        String sql = String.format("update offices set office_layer_id = %s, custom_layer_id = %s where id = %s;", office_layer_id, custom_layer_id, id);

        RQueryBuilder<Boolean> rQueryBuilder = new RQueryBuilder<>();

        return rQueryBuilder.setSql(sql).of(Boolean.class).buildUpdate();
    }
}
