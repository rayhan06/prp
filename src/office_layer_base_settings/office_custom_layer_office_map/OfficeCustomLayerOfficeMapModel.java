package office_layer_base_settings.office_custom_layer_office_map;

public class OfficeCustomLayerOfficeMapModel {

    public static class CustomLayer {
        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class OfficeLayerCustomLayer {
        private int office_id;
        private String office_name_eng;
        private String office_name_bng;
        private int office_layer_id;
        private String layer_name_eng;
        private String layer_name_bng;
        private int custom_layer_id;
        private String name;

        public int getOffice_id() {
            return office_id;
        }

        public void setOffice_id(int office_id) {
            this.office_id = office_id;
        }

        public String getOffice_name_eng() {
            return office_name_eng;
        }

        public void setOffice_name_eng(String office_name_eng) {
            this.office_name_eng = office_name_eng;
        }

        public String getOffice_name_bng() {
            return office_name_bng;
        }

        public void setOffice_name_bng(String office_name_bng) {
            this.office_name_bng = office_name_bng;
        }

        public int getOffice_layer_id() {
            return office_layer_id;
        }

        public void setOffice_layer_id(int office_layer_id) {
            this.office_layer_id = office_layer_id;
        }

        public String getLayer_name_eng() {
            return layer_name_eng;
        }

        public void setLayer_name_eng(String layer_name_eng) {
            this.layer_name_eng = layer_name_eng;
        }

        public String getLayer_name_bng() {
            return layer_name_bng;
        }

        public void setLayer_name_bng(String layer_name_bng) {
            this.layer_name_bng = layer_name_bng;
        }

        public int getCustom_layer_id() {
            return custom_layer_id;
        }

        public void setCustom_layer_id(int custom_layer_id) {
            this.custom_layer_id = custom_layer_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class OfficeLayer {
        private int id;
        private String layer_name_eng;
        private String layer_name_bng;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getLayer_name_eng() {
            return layer_name_eng;
        }

        public void setLayer_name_eng(String layer_name_eng) {
            this.layer_name_eng = layer_name_eng;
        }

        public String getLayer_name_bng() {
            return layer_name_bng;
        }

        public void setLayer_name_bng(String layer_name_bng) {
            this.layer_name_bng = layer_name_bng;
        }
    }
}
