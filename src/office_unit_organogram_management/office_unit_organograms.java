package office_unit_organogram_management;

public class office_unit_organograms {

    public long id;
    public int office_id;
    public long office_unit_id;
    public int superior_unit_id;
    public int superior_designation_id;
    public long ref_origin_unit_org_id;
    public int ref_sup_origin_unit_desig_id;
    public int ref_sup_origin_unit_id;
    public String designation_eng;
    public String designation_bng;
    public String short_name_eng;
    public String short_name_bng;
    public int designation_level;
    public int designation_sequence;
    public String designation_description;
    public boolean status;
    public boolean is_admin;
    public int created_by;
    public int modified_by;
    public long created;
    public long modified;
    public String role_type;
    public boolean isDeleted;
    public long lastModificationTime;
    public int approval_path_type;
    public int job_grade_type_cat;
    public String orgTree;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getOffice_id() {
        return office_id;
    }

    public void setOffice_id(int office_id) {
        this.office_id = office_id;
    }

    public long getOffice_unit_id() {
        return office_unit_id;
    }

    public void setOffice_unit_id(long office_unit_id) {
        this.office_unit_id = office_unit_id;
    }

    public int getSuperior_unit_id() {
        return superior_unit_id;
    }

    public void setSuperior_unit_id(int superior_unit_id) {
        this.superior_unit_id = superior_unit_id;
    }

    public int getSuperior_designation_id() {
        return superior_designation_id;
    }

    public void setSuperior_designation_id(int superior_designation_id) {
        this.superior_designation_id = superior_designation_id;
    }

    public long getRef_origin_unit_org_id() {
        return ref_origin_unit_org_id;
    }

    public void setRef_origin_unit_org_id(long ref_origin_unit_org_id) {
        this.ref_origin_unit_org_id = ref_origin_unit_org_id;
    }

    public int getRef_sup_origin_unit_desig_id() {
        return ref_sup_origin_unit_desig_id;
    }

    public void setRef_sup_origin_unit_desig_id(int ref_sup_origin_unit_desig_id) {
        this.ref_sup_origin_unit_desig_id = ref_sup_origin_unit_desig_id;
    }

    public int getRef_sup_origin_unit_id() {
        return ref_sup_origin_unit_id;
    }

    public void setRef_sup_origin_unit_id(int ref_sup_origin_unit_id) {
        this.ref_sup_origin_unit_id = ref_sup_origin_unit_id;
    }

    public String getDesignation_eng() {
        return designation_eng;
    }

    public void setDesignation_eng(String designation_eng) {
        this.designation_eng = designation_eng;
    }

    public String getDesignation_bng() {
        return designation_bng;
    }

    public void setDesignation_bng(String designation_bng) {
        this.designation_bng = designation_bng;
    }

    public String getShort_name_eng() {
        return short_name_eng;
    }

    public void setShort_name_eng(String short_name_eng) {
        this.short_name_eng = short_name_eng;
    }

    public String getShort_name_bng() {
        return short_name_bng;
    }

    public void setShort_name_bng(String short_name_bng) {
        this.short_name_bng = short_name_bng;
    }

    public int getDesignation_level() {
        return designation_level;
    }

    public void setDesignation_level(int designation_level) {
        this.designation_level = designation_level;
    }

    public int getDesignation_sequence() {
        return designation_sequence;
    }

    public void setDesignation_sequence(int designation_sequence) {
        this.designation_sequence = designation_sequence;
    }

    public String getDesignation_description() {
        return designation_description;
    }

    public void setDesignation_description(String designation_description) {
        this.designation_description = designation_description;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean getIs_admin() {
        return is_admin;
    }

    public void setIs_admin(boolean is_admin) {
        this.is_admin = is_admin;
    }

    public int getCreated_by() {
        return created_by;
    }

    public void setCreated_by(int created_by) {
        this.created_by = created_by;
    }

    public int getModified_by() {
        return modified_by;
    }

    public void setModified_by(int modified_by) {
        this.modified_by = modified_by;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getModified() {
        return modified;
    }

    public void setModified(long modified) {
        this.modified = modified;
    }

    public String getRole_type() {
        return role_type;
    }

    public void setRole_type(String role_type) {
        this.role_type = role_type;
    }

    public boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIdDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public long getLastModificationTime() {
        return lastModificationTime;
    }

    public void setLastModificationTime(long lastModificationTime) {
        this.lastModificationTime = lastModificationTime;
    }

    public int getApproval_path_type() {
        return approval_path_type;
    }

    public void setApproval_path_type(int approval_path_type) {
        this.approval_path_type = approval_path_type;
    }
}
