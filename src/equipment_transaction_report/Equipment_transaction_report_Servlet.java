package equipment_transaction_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Equipment_transaction_report_Servlet")
public class Equipment_transaction_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","medical_item_cat","=","","int","","","2","medicalItemCat",  ""},		
		{"criteria","","transaction_type","=","AND","int","","","any","transactionType", LC.EQUIPMENT_TRANSACTION_REPORT_WHERE_TRANSACTIONTYPE + ""},		
		{"criteria","","medical_equipment_cat","=","AND","int","","","any","medicalEquipmentCat", LC.EQUIPMENT_TRANSACTION_REPORT_WHERE_MEDICALEQUIPMENTCAT + ""},		
		{"criteria","","medical_equipment_name_id","=","AND","String","","","any","medicalEquipmentNameId", LC.EQUIPMENT_TRANSACTION_REPORT_WHERE_MEDICALEQUIPMENTNAMEID + ""},		
		{"criteria","","transaction_date",">=","AND","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","transaction_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},		
		{"criteria","","employee_record_id","=","AND","String","","","any","userName", LC.EQUIPMENT_TRANSACTION_REPORT_WHERE_USERNAME + "", "userNameToEmployeeRecordId"}		
	};
	
	String[][] Display =
	{
		{"display","","transaction_date","date",""},		
		{"display","","medical_equipment_cat","cat",""},		
		{"display","","medical_equipment_name_id","id",""},		
		{"display","","transaction_type","medical_transaction",""},		
		{"display","","quantity","int",""},		
		{"display","","employee_record_id","erIdToUserName",""},
		{"display","","employee_record_id","erIdToName",""},
		{"display","","inserted_by_user_id","userIdToName",""}	
	};
	
	String GroupBy = "";
	String OrderBY = "transaction_time DESC";
	
	ReportRequestHandler reportRequestHandler;
	
	public Equipment_transaction_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "medical_transaction";

		Display[0][4] = LM.getText(LC.HM_DATE, loginDTO);
		Display[1][4] = LM.getText(LC.HM_TYPE, loginDTO);
		Display[2][4] = LM.getText(LC.TRANSACTION_REPORT_SELECT_ITEM, loginDTO);
		Display[3][4] = LM.getText(LC.TRANSACTION_REPORT_SELECT_TRANSACTIONTYPE, loginDTO);
		Display[4][4] = LM.getText(LC.EQUIPMENT_TRANSACTION_REPORT_SELECT_QUANTITY, loginDTO);
		Display[5][4] = LM.getText(LC.HM_EMPLOYEE_ID, loginDTO);
		Display[6][4] = LM.getText(LC.HM_NAME, loginDTO);
		Display[7][4] = language.equalsIgnoreCase("english")?"Inserted By":"এন্ট্রিকর্তা";


        String reportName = LM.getText(LC.EQUIPMENT_TRANSACTION_REPORT_OTHER_EQUIPMENT_TRANSACTION_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "equipment_transaction_report",
				MenuConstants.EQUIPMENT_TRANSACTION_REPORT_DETAILS, language, reportName, "equipment_transaction_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
