package pharmacy_report;


import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;

import login.LoginDTO;
import medical_inventory_lot.MedicalInventoryInDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Pharmacy_report_Servlet")
public class Pharmacy_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","transaction_date",">=","","long","","",1 + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","transaction_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},		
		{"criteria","","employee_record_id","=","AND","String","","","any","userName",LC.PHARMACY_REPORT_WHERE_EMPLOYEEID + "", "userNameToEmployeeRecordId"},		
		{"criteria","","drug_information_id","=","AND","long","","","any","drug_information_id",LC.HM_DRUGS + ""},
		
		{"criteria","","medical_item_cat","=","AND","int","","","0","none", ""},
		{"criteria","","transaction_type","in","AND","String","","", MedicalInventoryInDTO.TR_RECEIVE_MEDICINE + ", " +  MedicalInventoryInDTO.TR_RETURN ,"none", ""}
	};
	
	String[][] Display =
	{
		{"display","","CONCAT(drug_information.name_en, ', ', strength)","basic",""},	
		{"display","","employee_record_id","erIdToUserName",""},
		{"display","","employee_record_id","erIdToName",""},		
		{"display","","user_name","username_to_phone",""},	
		{"display","","SUM(quantity)","int",""},
		{"display","","SUM(medical_transaction.quantity * medical_transaction.unit_price)","double",""}	
	};
	
	String GroupBy = "employee_record_id , drug_information_id";
	String OrderBY = "drug_information.name_en ASC";
	
	ReportRequestHandler reportRequestHandler;
	
	public Pharmacy_report_Servlet(){

	}	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");
		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "medical_transaction\r\n" + 
				"        JOIN\r\n" + 
				"    drug_information ON (drug_information.id = medical_transaction.drug_information_id)";


		Display[0][4] = LM.getText(LC.PHARMACY_REPORT_SELECT_MEDICINE, loginDTO);
		Display[1][4] = language.equalsIgnoreCase("English")? "Reference UserName":"রেফারেন্স ইউজারনেম";
		Display[2][4] = language.equalsIgnoreCase("English")? "Reference Employee":"রেফারেন্স কর্মকর্তা"      ;
		Display[3][4] = LM.getText(LC.HM_PHONE, loginDTO);
		Display[4][4] = language.equalsIgnoreCase("English")? "Received Quantity":"গৃহীত পরিমাণ";
        Display[5][4] = LM.getText(LC.HM_COST, loginDTO);

		
		String reportName = LM.getText(LC.PHARMACY_REPORT_OTHER_PHARMACY_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(4, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(5, ReportRequestHandler.RIGHT_ALIGN_FLOAT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "pharmacy_report",
				MenuConstants.PHARMACY_REPORT_DETAILS, language, reportName, "pharmacy_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
