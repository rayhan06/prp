package nothi;

import nothi_configuration.Nothi_configurationDTO;
import util.*; 


public class NothiDTO extends CommonDTO
{

	public long nothiConfigurationType = -1;
    public String nothiNumber = "";
    public String description = "";
	public long assignedOfficeId = -1;
	public long insertedByOfficeId = -1;
	public long insertedByErId = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	public long nothiDate = System.currentTimeMillis();
	public String fromOfficeName = "";
	
	public void set(Nothi_configurationDTO nothi_configurationDTO)
	{
		nothiNumber = nothi_configurationDTO.nothiNumber;
		description = nothi_configurationDTO.description;
		nothiConfigurationType = nothi_configurationDTO.iD;
	}
	
	
    @Override
	public String toString() {
            return "$NothiDTO[" +
            " iD = " + iD +
            " nothiConfigurationType = " + nothiConfigurationType +
            " nothiNumber = " + nothiNumber +
            " description = " + description +
            " searchColumn = " + searchColumn +
            " assignedOfficeId = " + assignedOfficeId +
            " insertedByOfficeId = " + insertedByOfficeId +
            " insertedByErId = " + insertedByErId +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " nothiDate = " + nothiDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}