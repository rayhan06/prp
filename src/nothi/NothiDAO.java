package nothi;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import language.LC;
import language.LM;

import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class NothiDAO  implements CommonDAOService<NothiDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private NothiDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"nothi_configuration_type",
			"nothi_number",
			"description",
			"search_column",
			"assigned_office_id",
			"inserted_by_office_id",
			"inserted_by_er_id",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"nothi_date",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("nothi_configuration_type"," and (nothi_configuration_type = ?)");
		
		searchMap.put("description"," and (description like ?)");
		searchMap.put("assigned_office_id"," and (assigned_office_id = ?)");
		searchMap.put("nothi_date_start"," and (nothi_date >= ?)");
		searchMap.put("nothi_date_end"," and (nothi_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final NothiDAO INSTANCE = new NothiDAO();
	}
    

	public static NothiDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(NothiDTO nothiDTO)
	{
		nothiDTO.searchColumn = "";
		nothiDTO.searchColumn += nothiDTO.nothiNumber + " " + Utils.getDigitBanglaFromEnglish(nothiDTO.nothiNumber) + " ";
		nothiDTO.searchColumn += nothiDTO.description + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, NothiDTO nothiDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(nothiDTO);
		if(isInsert)
		{
			ps.setObject(++index,nothiDTO.iD);
		}
		ps.setObject(++index,nothiDTO.nothiConfigurationType);
		ps.setObject(++index,nothiDTO.nothiNumber);
		ps.setObject(++index,nothiDTO.description);
		ps.setObject(++index,nothiDTO.searchColumn);
		ps.setObject(++index,nothiDTO.assignedOfficeId);
		ps.setObject(++index,nothiDTO.insertedByOfficeId);
		ps.setObject(++index,nothiDTO.insertedByErId);
		ps.setObject(++index,nothiDTO.insertedByUserId);
		ps.setObject(++index,nothiDTO.insertedByOrganogramId);
		ps.setObject(++index,nothiDTO.insertionDate);
		ps.setObject(++index,nothiDTO.lastModifierUser);
		ps.setObject(++index,nothiDTO.nothiDate);
		if(isInsert)
		{
			ps.setObject(++index,nothiDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,nothiDTO.iD);
		}
	}
	
	@Override
	public NothiDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			NothiDTO nothiDTO = new NothiDTO();
			int i = 0;
			nothiDTO.iD = rs.getLong(columnNames[i++]);
			nothiDTO.nothiConfigurationType = rs.getLong(columnNames[i++]);
			nothiDTO.nothiNumber = rs.getString(columnNames[i++]);
			nothiDTO.description = rs.getString(columnNames[i++]);
			nothiDTO.searchColumn = rs.getString(columnNames[i++]);
			nothiDTO.assignedOfficeId = rs.getLong(columnNames[i++]);
			nothiDTO.insertedByOfficeId = rs.getLong(columnNames[i++]);
			nothiDTO.insertedByErId = rs.getLong(columnNames[i++]);
			nothiDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			nothiDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			nothiDTO.insertionDate = rs.getLong(columnNames[i++]);
			nothiDTO.lastModifierUser = rs.getString(columnNames[i++]);
			nothiDTO.nothiDate = rs.getLong(columnNames[i++]);
			nothiDTO.isDeleted = rs.getInt(columnNames[i++]);
			nothiDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return nothiDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public NothiDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "nothi";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((NothiDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((NothiDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
	
	public List<NothiDTO> getAllInOffice (long officeId)
    {
		String sql = "SELECT * FROM " + getTableName() + " WHERE isDeleted = 0  " 
				+ " and assigned_office_id =  " + officeId;

		return ConnectionAndStatementUtil.getListOfT(sql,this::buildObjectFromResultSet);	
    }
	
	public NothiDTO getByNothiConfigId (long nothiConfigId)
    {
		String sql = "SELECT * FROM " + getTableName() + " WHERE isDeleted = 0  " 
				+ " and nothi_configuration_type =  " + nothiConfigId;

		return ConnectionAndStatementUtil.getT(sql,this::buildObjectFromResultSet);	
    }
	
	public String getOptionsByOffice(boolean isLangEng, long officeId)
	{
		return getOptions(isLangEng, getAllInOffice(officeId));
	}
	
	public String getOptions(boolean isLangEng)
	{
		return getOptions(isLangEng, getAllDTOs());
	}
	
	public String getOptionsAsConfig(boolean isLangEng)
	{
		return getOptionsAsConfig(isLangEng, getAllDTOs());
	}
	
	public String getOptions(boolean isLangEng, List<NothiDTO> nothiDTOs)
	{
		String options = "<option value = '-1'>" 
				+ LM.getText(LC.HM_SELECT, isLangEng)
				+ "</option>";
		
		if(nothiDTOs != null)
		{
			for(NothiDTO nothiDTO: nothiDTOs)
			{
				options += "<option value ='" + nothiDTO.iD + "'>"
						+ (Utils.getDigits(nothiDTO.nothiNumber, isLangEng))
						+ "</option>";

			}
		}
		
		return options;
	}
	
	public String getOptionsAsConfig(boolean isLangEng, List<NothiDTO> nothiDTOs)
	{
		String options = "<option value = '-1'>" 
				+ LM.getText(LC.HM_SELECT, isLangEng)
				+ "</option>";
		
		if(nothiDTOs != null)
		{
			for(NothiDTO nothiDTO: nothiDTOs)
			{
				options += "<option value ='" + nothiDTO.nothiConfigurationType + "'>"
						+ (Utils.getDigits(nothiDTO.nothiNumber, isLangEng))
						+ "</option>";

			}
		}
		
		return options;
	}
				
}
	