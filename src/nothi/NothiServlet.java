package nothi;


import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;


import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;
import workflow.WorkflowController;

import javax.servlet.http.*;
import java.util.*;

import common.ApiResponse;
import common.BaseServlet;
import language.LC;
import language.LM;
import nothi_configuration.Nothi_configurationDAO;
import nothi_configuration.Nothi_configurationDTO;
import nothi_history.Nothi_historyDAO;
import nothi_history.Nothi_historyDTO;
import pb.ErrorMessage;

import com.google.gson.Gson;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class NothiServlet
 */
@WebServlet("/NothiServlet")
@MultipartConfig
public class NothiServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(NothiServlet.class);

    @Override
    public String getTableName() {
        return NothiDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "NothiServlet";
    }

    @Override
    public NothiDAO getCommonDAOService() {
        return NothiDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.NOTHI_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.NOTHI_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.NOTHI_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return NothiServlet.class;
    }
    private final Gson gson = new Gson();
 	

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        init(request);
        String actionType = request.getParameter("actionType");
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        boolean isLangEng = LM.getLanguage(commonLoginData.loginDTO).equalsIgnoreCase("english");
        String language = LM.getLanguage(commonLoginData.loginDTO);
		if ("getDetails".equals(actionType)) {
			try {
				long id = Long.parseLong(request.getParameter("id"));
				NothiDTO nothiDTO = NothiDAO.getInstance().getDTOByID(id);
				nothiDTO.fromOfficeName = WorkflowController.getOfficeUnitName(nothiDTO.assignedOfficeId, language);
				if(nothiDTO != null)
				{
					PrintWriter out = response.getWriter();
					response.setContentType("application/json");
					response.setCharacterEncoding("UTF-8");					
					String encoded = this.gson.toJson(nothiDTO);
					System.out.println("json encoded data = " + encoded);
					out.print(encoded);
					out.flush();
				}
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}	
		else
		{
			super.doGet(request,response);
		}
    }
	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addNothi");
		String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		boolean isLangEng = language.equalsIgnoreCase("english");
		NothiDTO nothiDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
		if(addFlag)
		{
			nothiDTO = new NothiDTO();
		}
		else
		{
			nothiDTO = NothiDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		Value = request.getParameter("nothiConfigurationType");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("nothiConfigurationType = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			nothiDTO.nothiConfigurationType = Long.parseLong(Value);
			Nothi_configurationDTO nothi_configurationDTO = Nothi_configurationDAO.getInstance().getDTOByID(nothiDTO.nothiConfigurationType);
			if(nothi_configurationDTO == null)
			{
				throw new Exception(isLangEng?"Invalid nothi number":"নথি নম্বরতটি অগ্রহণযোগ্য");
			}
			nothiDTO.set(nothi_configurationDTO);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		if(addFlag)
		{
			nothiDTO.insertedByUserId = userDTO.ID;		
			nothiDTO.insertedByOrganogramId = userDTO.organogramID;
			nothiDTO.insertedByErId = userDTO.employee_record_id;
			nothiDTO.insertedByOfficeId = WorkflowController.getOfficeIdFromOrganogramId(nothiDTO.insertedByOrganogramId);
			nothiDTO.insertionDate = TimeConverter.getToday();
			nothiDTO.assignedOfficeId = SessionConstants.SECRETARY_UNIT;
		}			


		nothiDTO.lastModifierUser = userDTO.userName;


		Value = request.getParameter("nothiDate");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("nothiDate = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			try 
			{
				Date d = f.parse(Value);
				nothiDTO.nothiDate = d.getTime();
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				throw new Exception(LM.getText(LC.HM_DATE, language) + " " + ErrorMessage.getInvalidMessage(language));
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		System.out.println("Done adding  addNothi dto = " + nothiDTO);

		if(addFlag == true)
		{
			NothiDAO.getInstance().add(nothiDTO);
			Nothi_historyDTO nothi_historyDTO = new Nothi_historyDTO();
			nothi_historyDTO.setInit(nothiDTO);
			Nothi_historyDAO.getInstance().add(nothi_historyDTO);
		}
	
		

		return nothiDTO;

	}
}

