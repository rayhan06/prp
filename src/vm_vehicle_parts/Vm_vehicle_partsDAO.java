package vm_vehicle_parts;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Vm_vehicle_partsDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Vm_vehicle_partsDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Vm_vehicle_partsMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"name_bn",
			"name_en",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Vm_vehicle_partsDAO()
	{
		this("vm_vehicle_parts");		
	}
	
	public void setSearchColumn(Vm_vehicle_partsDTO vm_vehicle_partsDTO)
	{
		vm_vehicle_partsDTO.searchColumn = "";
		vm_vehicle_partsDTO.searchColumn += vm_vehicle_partsDTO.nameBn + " ";
		vm_vehicle_partsDTO.searchColumn += vm_vehicle_partsDTO.nameEn + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Vm_vehicle_partsDTO vm_vehicle_partsDTO = (Vm_vehicle_partsDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(vm_vehicle_partsDTO);
		if(isInsert)
		{
			ps.setObject(index++,vm_vehicle_partsDTO.iD);
		}
		ps.setObject(index++,vm_vehicle_partsDTO.nameBn);
		ps.setObject(index++,vm_vehicle_partsDTO.nameEn);
		ps.setObject(index++,vm_vehicle_partsDTO.searchColumn);
		ps.setObject(index++,vm_vehicle_partsDTO.insertedByUserId);
		ps.setObject(index++,vm_vehicle_partsDTO.insertedByOrganogramId);
		ps.setObject(index++,vm_vehicle_partsDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Vm_vehicle_partsDTO build(ResultSet rs)
	{
		try
		{
			Vm_vehicle_partsDTO vm_vehicle_partsDTO = new Vm_vehicle_partsDTO();
			vm_vehicle_partsDTO.iD = rs.getLong("ID");
			vm_vehicle_partsDTO.nameBn = rs.getString("name_bn");
			vm_vehicle_partsDTO.nameEn = rs.getString("name_en");
			vm_vehicle_partsDTO.searchColumn = rs.getString("search_column");
			vm_vehicle_partsDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			vm_vehicle_partsDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			vm_vehicle_partsDTO.insertionDate = rs.getLong("insertion_date");
			vm_vehicle_partsDTO.isDeleted = rs.getInt("isDeleted");
			vm_vehicle_partsDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return vm_vehicle_partsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ? ";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name_bn")
						|| str.equals("name_en")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name_bn"))
					{
						AllFieldSql += "" + tableName + ".name_bn like ?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".name_en like ?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	

				
}
	