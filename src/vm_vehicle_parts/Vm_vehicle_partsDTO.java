package vm_vehicle_parts;
import java.util.*; 
import util.*; 


public class Vm_vehicle_partsDTO extends CommonDTO
{

    public String nameBn = "";
    public String nameEn = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	
	
    @Override
	public String toString() {
            return "$Vm_vehicle_partsDTO[" +
            " iD = " + iD +
            " nameBn = " + nameBn +
            " nameEn = " + nameEn +
            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}