package vm_vehicle_parts;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import common.ApiResponse;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Vm_vehicle_partsServlet
 */
@WebServlet("/Vm_vehicle_partsServlet")
@MultipartConfig
public class Vm_vehicle_partsServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Vm_vehicle_partsServlet.class);

    String tableName = "vm_vehicle_parts";

	Vm_vehicle_partsDAO vm_vehicle_partsDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vm_vehicle_partsServlet()
	{
        super();
    	try
    	{
			vm_vehicle_partsDAO = new Vm_vehicle_partsDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(vm_vehicle_partsDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_PARTS_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_PARTS_UPDATE))
				{
					getVm_vehicle_parts(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_PARTS_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchVm_vehicle_parts(request, response, isPermanentTable, filter);
						}
						else
						{
							searchVm_vehicle_parts(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchVm_vehicle_parts(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_PARTS_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		if (userDTO == null) {
			request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			return;
		}
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_PARTS_ADD))
				{
					System.out.println("going to  addVm_vehicle_parts ");
					addVm_vehicle_parts(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addVm_vehicle_parts ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_PARTS_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addVm_vehicle_parts ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_PARTS_UPDATE))
				{
					addVm_vehicle_parts(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_PARTS_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_PARTS_SEARCH))
				{
					searchVm_vehicle_parts(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Vm_vehicle_partsDTO vm_vehicle_partsDTO = Vm_vehicle_partsRepository.getInstance().getVm_vehicle_partsDTOByID
					(Long.parseLong(request.getParameter("ID")));
//					(Vm_vehicle_partsDTO)vm_vehicle_partsDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(vm_vehicle_partsDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addVm_vehicle_parts(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		ApiResponse apiResponse;
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addVm_vehicle_parts");
			String path = getServletContext().getRealPath("/img2/");
			Vm_vehicle_partsDTO vm_vehicle_partsDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				vm_vehicle_partsDTO = new Vm_vehicle_partsDTO();
			}
			else
			{
				vm_vehicle_partsDTO = Vm_vehicle_partsRepository.getInstance().getVm_vehicle_partsDTOByID
						(Long.parseLong(request.getParameter("iD")));
//						(Vm_vehicle_partsDTO)vm_vehicle_partsDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("nameBn");
			System.out.println("nameBn = " + Value);
			Value = Jsoup.clean(Value,Whitelist.simpleText()).trim();
			if(Value != null && Value.length() > 0)
			{
				vm_vehicle_partsDTO.nameBn = (Value);
			}
			else
			{
				throw new Exception(" Invalid name Bn");
			}

			Value = request.getParameter("nameEn");
			System.out.println("nameEn = " + Value);
			Value = Jsoup.clean(Value,Whitelist.simpleText()).trim();


			if(Value != null && Value.length() > 0)
			{
				vm_vehicle_partsDTO.nameEn = (Value);
			}
			else
			{
				throw new Exception(" Invalid name En");
			}

			if(addFlag)
			{
				vm_vehicle_partsDTO.insertedByUserId = userDTO.ID;
				vm_vehicle_partsDTO.insertedByOrganogramId = userDTO.organogramID;
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				vm_vehicle_partsDTO.insertionDate = c.getTimeInMillis();
			}


			System.out.println("Done adding  addVm_vehicle_parts dto = " + vm_vehicle_partsDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				vm_vehicle_partsDAO.setIsDeleted(vm_vehicle_partsDTO.iD, CommonDTO.OUTDATED);
				returnedID = vm_vehicle_partsDAO.add(vm_vehicle_partsDTO);
				vm_vehicle_partsDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = vm_vehicle_partsDAO.manageWriteOperations(vm_vehicle_partsDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = vm_vehicle_partsDAO.manageWriteOperations(vm_vehicle_partsDTO, SessionConstants.UPDATE, -1, userDTO);
			}



			apiResponse = ApiResponse.makeSuccessResponse("Vm_vehicle_partsServlet?actionType=search");

//			response.sendRedirect("Vm_vehicle_partsServlet?actionType=search");


		}
		catch (Exception e)
		{
			e.printStackTrace();
			apiResponse = ApiResponse.makeErrorResponse(e.getMessage());
		}

		PrintWriter pw = response.getWriter();
		pw.write(apiResponse.getJSONString());
		pw.flush();
		pw.close();
	}









	private void getVm_vehicle_parts(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getVm_vehicle_parts");
		Vm_vehicle_partsDTO vm_vehicle_partsDTO = null;
		try
		{
			vm_vehicle_partsDTO = Vm_vehicle_partsRepository.getInstance().getVm_vehicle_partsDTOByID(id);
//					(Vm_vehicle_partsDTO)vm_vehicle_partsDAO.getDTOByID(id);
			request.setAttribute("ID", vm_vehicle_partsDTO.iD);
			request.setAttribute("vm_vehicle_partsDTO",vm_vehicle_partsDTO);
			request.setAttribute("vm_vehicle_partsDAO",vm_vehicle_partsDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "vm_vehicle_parts/vm_vehicle_partsInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "vm_vehicle_parts/vm_vehicle_partsSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "vm_vehicle_parts/vm_vehicle_partsEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "vm_vehicle_parts/vm_vehicle_partsEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getVm_vehicle_parts(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getVm_vehicle_parts(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchVm_vehicle_parts(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchVm_vehicle_parts 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_VM_VEHICLE_PARTS,
			request,
			vm_vehicle_partsDAO,
			SessionConstants.VIEW_VM_VEHICLE_PARTS,
			SessionConstants.SEARCH_VM_VEHICLE_PARTS,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("vm_vehicle_partsDAO",vm_vehicle_partsDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_vehicle_parts/vm_vehicle_partsApproval.jsp");
	        	rd = request.getRequestDispatcher("vm_vehicle_parts/vm_vehicle_partsApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_vehicle_parts/vm_vehicle_partsApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("vm_vehicle_parts/vm_vehicle_partsApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_vehicle_parts/vm_vehicle_partsSearch.jsp");
	        	rd = request.getRequestDispatcher("vm_vehicle_parts/vm_vehicle_partsSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_vehicle_parts/vm_vehicle_partsSearchForm.jsp");
	        	rd = request.getRequestDispatcher("vm_vehicle_parts/vm_vehicle_partsSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

