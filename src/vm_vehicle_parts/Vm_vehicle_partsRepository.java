package vm_vehicle_parts;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import recruitment_job_required_files.Recruitment_job_required_filesDTO;
import repository.Repository;
import repository.RepositoryManager;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class Vm_vehicle_partsRepository implements Repository {
	Vm_vehicle_partsDAO vm_vehicle_partsDAO = new Vm_vehicle_partsDAO();
	Gson gson = new Gson();
	
	public void setDAO(Vm_vehicle_partsDAO vm_vehicle_partsDAO)
	{
		this.vm_vehicle_partsDAO = vm_vehicle_partsDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Vm_vehicle_partsRepository.class);
	Map<Long, Vm_vehicle_partsDTO>mapOfVm_vehicle_partsDTOToiD;


	static Vm_vehicle_partsRepository instance = null;  
	private Vm_vehicle_partsRepository(){
		mapOfVm_vehicle_partsDTOToiD = new ConcurrentHashMap<>();
		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vm_vehicle_partsRepository getInstance(){
		if (instance == null){
			instance = new Vm_vehicle_partsRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(vm_vehicle_partsDAO == null)
		{
			return;
		}
		try {
			List<Vm_vehicle_partsDTO> vm_vehicle_partsDTOs = (List<Vm_vehicle_partsDTO>)
					vm_vehicle_partsDAO.getAll(reloadAll);
			for(Vm_vehicle_partsDTO vm_vehicle_partsDTO : vm_vehicle_partsDTOs) {
				Vm_vehicle_partsDTO oldVm_vehicle_partsDTO = getVm_vehicle_partsDTOByIDWithoutClone(vm_vehicle_partsDTO.iD);
				if( oldVm_vehicle_partsDTO != null ) {
					mapOfVm_vehicle_partsDTOToiD.remove(oldVm_vehicle_partsDTO.iD);
				}
				if(vm_vehicle_partsDTO.isDeleted == 0) 
				{
					
					mapOfVm_vehicle_partsDTOToiD.put(vm_vehicle_partsDTO.iD, vm_vehicle_partsDTO);
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Vm_vehicle_partsDTO> getVm_vehicle_partsList() {
		List <Vm_vehicle_partsDTO> vm_vehicle_partss = new ArrayList<Vm_vehicle_partsDTO>(this.mapOfVm_vehicle_partsDTOToiD.values());
		return clone(vm_vehicle_partss);
	}
	
	
	public Vm_vehicle_partsDTO getVm_vehicle_partsDTOByIDWithoutClone( long ID){
		return mapOfVm_vehicle_partsDTOToiD.get(ID);
	}

	public Vm_vehicle_partsDTO getVm_vehicle_partsDTOByID( long ID){
		return clone(mapOfVm_vehicle_partsDTOToiD.get(ID));
	}


	public Vm_vehicle_partsDTO clone(Vm_vehicle_partsDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Vm_vehicle_partsDTO.class);
	}

	public List<Vm_vehicle_partsDTO> clone(List<Vm_vehicle_partsDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_vehicle_parts";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}

	public String getBuildOptions(String Language, String selectedId ){
		List<OptionDTO> optionDTOList = getVm_vehicle_partsList()
				.stream()
				.map(e->new OptionDTO(e.nameEn,e.nameBn,String.valueOf(e.iD)))
				.collect(Collectors.toList());

		return Utils.buildOptionsWithoutSelectWithSelectId(optionDTOList, Language, selectedId );

	}

	public String getText(String language, long id) {
		Vm_vehicle_partsDTO dto = this.mapOfVm_vehicle_partsDTOToiD.get(id);
		return dto == null ? ""
				: (language.equalsIgnoreCase("English")) ? (dto.nameEn == null ? "" : dto.nameEn)
				: (dto.nameBn == null ? "" : dto.nameBn);
	}
}


