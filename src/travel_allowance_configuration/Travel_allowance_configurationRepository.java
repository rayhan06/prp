package travel_allowance_configuration;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class Travel_allowance_configurationRepository implements Repository {
    Travel_allowance_configurationDAO travel_allowance_configurationDAO;

    static Logger logger = Logger.getLogger(Travel_allowance_configurationRepository.class);
    Map<Long, Travel_allowance_configurationDTO> mapOfTravel_allowance_configurationDTOToiD;
    Gson gson;


    private Travel_allowance_configurationRepository() {
        travel_allowance_configurationDAO = Travel_allowance_configurationDAO.getInstance();
        mapOfTravel_allowance_configurationDTOToiD = new ConcurrentHashMap<>();
        gson = new Gson();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Travel_allowance_configurationRepository INSTANCE = new Travel_allowance_configurationRepository();
    }

    public static Travel_allowance_configurationRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        try {
            List<Travel_allowance_configurationDTO> travel_allowance_configurationDTOs = travel_allowance_configurationDAO.getAllDTOs(reloadAll);
            for (Travel_allowance_configurationDTO travel_allowance_configurationDTO : travel_allowance_configurationDTOs) {
                Travel_allowance_configurationDTO oldTravel_allowance_configurationDTO = getTravel_allowance_configurationDTOByiD(travel_allowance_configurationDTO.iD);
                if (oldTravel_allowance_configurationDTO != null) {
                    mapOfTravel_allowance_configurationDTOToiD.remove(oldTravel_allowance_configurationDTO.iD);


                }
                if (travel_allowance_configurationDTO.isDeleted == 0) {

                    mapOfTravel_allowance_configurationDTOToiD.put(travel_allowance_configurationDTO.iD, travel_allowance_configurationDTO);

                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public Travel_allowance_configurationDTO clone(Travel_allowance_configurationDTO dto) {
        String raw = gson.toJson(dto);
        return gson.fromJson(raw, Travel_allowance_configurationDTO.class);
    }


    public List<Travel_allowance_configurationDTO> getTravel_allowance_configurationList() {
        return new ArrayList<>(this.mapOfTravel_allowance_configurationDTOToiD.values());
    }

    public Travel_allowance_configurationDTO getTravel_allowance_configurationDTOByiD(long iD) {
        return mapOfTravel_allowance_configurationDTOToiD.get(iD);
    }

    @Override
    public String getTableName() {
        return travel_allowance_configurationDAO.getTableName();
    }
}


