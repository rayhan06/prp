package travel_allowance_configuration;

public enum AttendanceEnum {
    ATTENDANT(1),
    STAY(2);

    private final int value;

    AttendanceEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
