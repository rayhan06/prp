package travel_allowance_configuration;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import pb.CatDAO;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Travel_allowance_configurationDAO implements CommonDAOService<Travel_allowance_configurationDTO> {

    Logger logger = Logger.getLogger(getClass());

    private static final String addSqlQuery = "INSERT INTO {tableName} (allowance_cat,allowance_attendance_type,amount,"
            .concat("modified_by,search_column,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?)");
    private static final String updateSqlQuery = "UPDATE {tableName} SET allowance_cat=?,allowance_attendance_type=?,amount=?,"
            .concat("modified_by=?,search_column=?,lastModificationTime = ? WHERE ID = ?");
    private static final String getByCategoriesSqlQuery = "SELECT * FROM %s WHERE allowance_cat = %d AND allowance_attendance_type = %d AND isDeleted = 0";
    private final Map<String, String> searchMap = new HashMap<>();

    private Travel_allowance_configurationDAO() {

        searchMap.put("allowance_cat", " and (allowance_cat = ?)");
        searchMap.put("allowance_attendance_type", " and (allowance_attendance_type = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }


    private static class LazyLoader {
        static final Travel_allowance_configurationDAO INSTANCE = new Travel_allowance_configurationDAO();
    }

    public static Travel_allowance_configurationDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Travel_allowance_configurationDTO travel_allowance_configurationDTO) {
        travel_allowance_configurationDTO.searchColumn = "";
        travel_allowance_configurationDTO.searchColumn += CatDAO.getName("English", "travel_allowance", travel_allowance_configurationDTO.allowanceCat) + " " + CatDAO.getName("Bangla", "travel_allowance", travel_allowance_configurationDTO.allowanceCat) + " ";
        travel_allowance_configurationDTO.searchColumn += CatDAO.getName("English", "allowance_attendance", travel_allowance_configurationDTO.allowanceAttendanceType) + " " + CatDAO.getName("Bangla", "allowance_attendance", travel_allowance_configurationDTO.allowanceAttendanceType) + " ";
    }

    @Override
    public void set(PreparedStatement ps, Travel_allowance_configurationDTO travel_allowance_configurationDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(travel_allowance_configurationDTO);
        ps.setObject(++index, travel_allowance_configurationDTO.allowanceCat);
        ps.setObject(++index, travel_allowance_configurationDTO.allowanceAttendanceType);
        ps.setObject(++index, travel_allowance_configurationDTO.amount);
        ps.setObject(++index, travel_allowance_configurationDTO.modifiedBy);
        setSearchColumn(travel_allowance_configurationDTO);
        ps.setObject(++index, travel_allowance_configurationDTO.searchColumn);
        ps.setObject(++index, travel_allowance_configurationDTO.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, travel_allowance_configurationDTO.insertedBy);
            ps.setObject(++index, travel_allowance_configurationDTO.insertionTime);
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, travel_allowance_configurationDTO.iD);
    }

    @Override
    public Travel_allowance_configurationDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Travel_allowance_configurationDTO travel_allowance_configurationDTO = new Travel_allowance_configurationDTO();
            travel_allowance_configurationDTO.iD = rs.getLong("ID");
            travel_allowance_configurationDTO.allowanceCat = rs.getInt("allowance_cat");
            travel_allowance_configurationDTO.allowanceAttendanceType = rs.getInt("allowance_attendance_type");
            travel_allowance_configurationDTO.amount = rs.getInt("amount");
            travel_allowance_configurationDTO.insertionTime = rs.getLong("insertion_time");
            travel_allowance_configurationDTO.insertedBy = rs.getLong("inserted_by");
            travel_allowance_configurationDTO.modifiedBy = rs.getLong("modified_by");
            travel_allowance_configurationDTO.searchColumn = rs.getString("search_column");
            travel_allowance_configurationDTO.isDeleted = rs.getInt("isDeleted");
            travel_allowance_configurationDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return travel_allowance_configurationDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Travel_allowance_configurationDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "travel_allowance_configuration";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Travel_allowance_configurationDTO) commonDTO, addSqlQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Travel_allowance_configurationDTO) commonDTO, updateSqlQuery, false);
    }

    public Travel_allowance_configurationDTO getByCategories(int allowanceCat, int allowanceAttendanceType) {
        List<Travel_allowance_configurationDTO> travel_allowance_configurationDTOList = getDTOs(String.format(getByCategoriesSqlQuery, getTableName(), allowanceCat, allowanceAttendanceType));
        return travel_allowance_configurationDTOList.size() != 0 ? travel_allowance_configurationDTOList.get(0) : null;
    }
}
	