package travel_allowance_configuration;

import util.CommonDTO;


public class Travel_allowance_configurationDTO extends CommonDTO {

    public int allowanceCat = -1;
    public int allowanceAttendanceType = -1;
    public int amount = 0;
    public long insertedBy = 0;
    public long insertionTime = 0;
    public long modifiedBy = 0;


    @Override
    public String toString() {
        return "$Travel_allowance_configurationDTO[" +
                " iD = " + iD +
                " allowanceCat = " + allowanceCat +
                " allowanceAttendanceType = " + allowanceAttendanceType +
                " amount = " + amount +
                " searchColumn = " + searchColumn +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " isDeleted = " + isDeleted +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}