package travel_allowance_configuration;

import com.google.gson.Gson;
import common.BaseServlet;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * Servlet implementation class Travel_allowance_configurationServlet
 */
@WebServlet("/Travel_allowance_configurationServlet")
@MultipartConfig
public class Travel_allowance_configurationServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Travel_allowance_configurationServlet.class);

    @Override
    public String getTableName() {
        return Travel_allowance_configurationDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Travel_allowance_configurationServlet";
    }

    @Override
    public Travel_allowance_configurationDAO getCommonDAOService() {
        return Travel_allowance_configurationDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.TRAVEL_ALLOWANCE_CONFIGURATION_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.TRAVEL_ALLOWANCE_CONFIGURATION_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.TRAVEL_ALLOWANCE_CONFIGURATION_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Travel_allowance_configurationServlet.class;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {


        int allowanceCat = Integer.parseInt(Jsoup.clean(request.getParameter("allowanceCat"), Whitelist.simpleText()));
        int allowanceAttendanceType = Integer.parseInt(Jsoup.clean(request.getParameter("allowanceAttendanceType"), Whitelist.simpleText()));
        int amount = Integer.parseInt(Jsoup.clean(request.getParameter("amount"), Whitelist.simpleText()));

        Travel_allowance_configurationDTO travel_allowance_configurationDTO = Travel_allowance_configurationDAO.getInstance().getByCategories(allowanceCat, allowanceAttendanceType);
        if (travel_allowance_configurationDTO == null) {
            travel_allowance_configurationDTO = new Travel_allowance_configurationDTO();
        }
        travel_allowance_configurationDTO.amount = amount;
        travel_allowance_configurationDTO.modifiedBy = userDTO.employee_record_id;
        travel_allowance_configurationDTO.lastModificationTime = new Date().getTime();

        if (travel_allowance_configurationDTO.iD == -1) {
            travel_allowance_configurationDTO.allowanceCat = allowanceCat;
            travel_allowance_configurationDTO.allowanceAttendanceType = allowanceAttendanceType;
            travel_allowance_configurationDTO.insertedBy = userDTO.employee_record_id;
            travel_allowance_configurationDTO.insertionTime = new Date().getTime();
            Travel_allowance_configurationDAO.getInstance().add(travel_allowance_configurationDTO);
        } else {
            Travel_allowance_configurationDAO.getInstance().update(travel_allowance_configurationDTO);
        }

        return travel_allowance_configurationDTO;


    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            if ("getAmount".equals(actionType)) {
                int allowanceCat = Integer.parseInt(request.getParameter("allowance_cat"));
                int allowanceAttendanceType = Integer.parseInt(request.getParameter("allowance_attendance_type"));
                Travel_allowance_configurationDTO travel_allowance_configurationDTO = Travel_allowance_configurationDAO.getInstance().getByCategories(allowanceCat, allowanceAttendanceType);
                Map<String, Object> res = new HashMap<>();
                if (travel_allowance_configurationDTO == null) {
                    res.put("value", 0);
                } else {
                    res.put("value",travel_allowance_configurationDTO.amount);
                }
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().println(new Gson().toJson(res));
                return;
            }
            super.doGet(request, response);
            return;
        } catch (Exception e) {
            logger.error(e);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

}

