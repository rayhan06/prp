package travel_allowance_configuration;

public enum AllowanceEnum {
    DAILY(1),
    TRAVEL(2);

    private final int value;

    AllowanceEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
