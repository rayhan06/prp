package prescription_details;
import java.util.*; 
import util.*;


public class PrescriptionLabTestMAPS extends CommonMaps
{	
	public PrescriptionLabTestMAPS(String tableName)
	{
		
		java_allfield_type_map.put("prescription_details_id".toLowerCase(), "Long");
		java_allfield_type_map.put("lab_test_type".toLowerCase(), "Long");
		java_allfield_type_map.put("lab_test_details_types".toLowerCase(), "String");
		java_allfield_type_map.put("test_status".toLowerCase(), "String");
		java_allfield_type_map.put("tester_id".toLowerCase(), "String");
		java_allfield_type_map.put("testing_date".toLowerCase(), "Long");
		java_allfield_type_map.put("scheduled_delivery_date".toLowerCase(), "String");
		java_allfield_type_map.put("actual_delivery_date".toLowerCase(), "String");


		java_anyfield_search_map.put(tableName + ".prescription_details_id".toLowerCase(), "Long");
		java_anyfield_search_map.put("lab_test.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("lab_test.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".lab_test_details_types".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".test_status".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".tester_id".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".testing_date".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".scheduled_delivery_date".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".actual_delivery_date".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("prescriptionDetailsId".toLowerCase(), "prescriptionDetailsId".toLowerCase());
		java_DTO_map.put("labTestType".toLowerCase(), "labTestType".toLowerCase());
		java_DTO_map.put("labTestDetailsTypes".toLowerCase(), "labTestDetailsTypes".toLowerCase());
		java_DTO_map.put("testStatus".toLowerCase(), "testStatus".toLowerCase());
		java_DTO_map.put("testerId".toLowerCase(), "testerId".toLowerCase());
		java_DTO_map.put("testingDate".toLowerCase(), "testingDate".toLowerCase());
		java_DTO_map.put("scheduledDeliveryDate".toLowerCase(), "scheduledDeliveryDate".toLowerCase());
		java_DTO_map.put("actualDeliveryDate".toLowerCase(), "actualDeliveryDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("prescription_details_id".toLowerCase(), "prescriptionDetailsId".toLowerCase());
		java_SQL_map.put("lab_test_type".toLowerCase(), "labTestType".toLowerCase());
		java_SQL_map.put("lab_test_details_types".toLowerCase(), "labTestDetailsTypes".toLowerCase());
		java_SQL_map.put("test_status".toLowerCase(), "testStatus".toLowerCase());
		java_SQL_map.put("tester_id".toLowerCase(), "testerId".toLowerCase());
		java_SQL_map.put("testing_date".toLowerCase(), "testingDate".toLowerCase());
		java_SQL_map.put("scheduled_delivery_date".toLowerCase(), "scheduledDeliveryDate".toLowerCase());
		java_SQL_map.put("actual_delivery_date".toLowerCase(), "actualDeliveryDate".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Prescription Details Id".toLowerCase(), "prescriptionDetailsId".toLowerCase());
		java_Text_map.put("Lab Test".toLowerCase(), "labTestType".toLowerCase());
		java_Text_map.put("Lab Test Details Types".toLowerCase(), "labTestDetailsTypes".toLowerCase());
		java_Text_map.put("Test Status".toLowerCase(), "testStatus".toLowerCase());
		java_Text_map.put("Tester Id".toLowerCase(), "testerId".toLowerCase());
		java_Text_map.put("Testing Date".toLowerCase(), "testingDate".toLowerCase());
		java_Text_map.put("Scheduled Delivery Date".toLowerCase(), "scheduledDeliveryDate".toLowerCase());
		java_Text_map.put("Actual Delivery Date".toLowerCase(), "actualDeliveryDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}