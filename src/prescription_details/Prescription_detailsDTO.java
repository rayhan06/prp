package prescription_details;
import java.util.*;

import appointment.AppointmentDTO;
import family.FamilyDTO;
import util.*; 


public class Prescription_detailsDTO extends CommonDTO
{

	public long appointmentId = -1;
    public String name = "";
	public long dateOfBirth = 0;
	public long age = 0;
	public double height = 0;
	public double weight = 0;
	public double bloodPressureDiastole = 0;
    public String diagnosis = "";
    public String chiefComplaints = "";
    public String clinicalFeatures = "";
    public String advice = "";
    public String remarks = "";
	public boolean medicineAdded = false;
	public double bloodPressureSystole = 0;
	public double pulse = 0;
	public double temperature = 0;
	public double oxygenSaturation = 0;
	public double bmi = 0;
    public String bloodGroup = "";
    public String sugar = "";
    public String employeeUserName = "";
    
    public long employeeId = -1;
    public long employeeUnit = -1;
    public long erId = -1;
    
    public long referredTo = -1;
    public String referredToName = "";
    public String referralMessage = "";
    public long referredAppointmentId = -1;
    
    public boolean therapyNeeded = false;
    
	public int medicineCount = 0;
	public int testCount = 0;
	
	public long visitDate = 0;
	public long visitTime = 0;
	public long doctorId = 0;
	
	public int whoIsThePatientCat =FamilyDTO.OWN;
	public String drUserName = "";
	
	public String sl = "";
	public String phone = "";
	
	public boolean canBeEdited = true;
	
	public String othersOfficeBn = "";
	public String othersOfficeEn = "";
	public long othersOfficeId = -1;
	public String othersDesignationAndId = "";
	public String alternatePhone = "";
	
	public String extraRelation = "";
	
	public boolean isDental = false;
	public boolean dentalInstructionNeeded = false;
	
	public long modifiedByErId = -1;
	
	public List<PatientPrescriptionMedicine2DTO> patientPrescriptionMedicine2DTOList = new ArrayList<>();
	public List<PrescriptionLabTestDTO> prescriptionLabTestDTOList = new ArrayList<>();
	public List<Dental_activitiesDTO> dental_activitiesDTOList = new ArrayList<>();
	
	public Prescription_detailsDTO()
	{
		
	}
	
	public Prescription_detailsDTO(AppointmentDTO appointmentDTO)
	{
		this.visitDate = appointmentDTO.visitDate;
		this.doctorId = appointmentDTO.doctorId;
		this.employeeId = appointmentDTO.patientId;
		this.employeeUserName = appointmentDTO.employeeUserName;
		this.whoIsThePatientCat = appointmentDTO.whoIsThePatientCat;
		this.othersOfficeBn = appointmentDTO.othersOfficeBn;
		this.othersDesignationAndId = appointmentDTO.othersDesignationAndId;
	}
	
    @Override
	public String toString() {
            return "$Prescription_detailsDTO[" +
            " iD = " + iD +
            " appointmentId = " + appointmentId +
            " name = " + name +
            " dateOfBirth = " + dateOfBirth +
            " age = " + age +
            " height = " + height +
            " weight = " + weight +
            " bloodPressureDiastole = " + bloodPressureDiastole +
            " diagnosis = " + diagnosis +
            " chiefComplaints = " + chiefComplaints +
            " clinicalFeatures = " + clinicalFeatures +
            " advice = " + advice +
            " remarks = " + remarks +
            " medicineAdded = " + medicineAdded +
            " bloodPressureSystole = " + bloodPressureSystole +
            " pulse = " + pulse +
            " temperature = " + temperature +
            " oxygenSaturation = " + oxygenSaturation +
            " bmi = " + bmi +
            " bloodGroup = " + bloodGroup +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}