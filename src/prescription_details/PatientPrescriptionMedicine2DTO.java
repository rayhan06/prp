package prescription_details;
import java.util.*; 
import util.*; 


public class PatientPrescriptionMedicine2DTO extends CommonDTO
{

	public long prescriptionDetailsId = 0;
	public long drugInformationType = -1;
	public int dosageTypeCat = 0;
    public String dose = "";
	public int doseUnitCat = 0;
    public String frequency = "";
    public int frequencyCat = 0;
    public int quantity = 0;
    public int remainingQuantity = 0;
	public int foodInstructionsCat = 2; //no instructions
	public long suppliedDrugInformationType = -1;
	public int doseSold = 0;
	public int doseSoldToday = 0;
	public long sellerId = 0;
    public String sellerName = "";
	public long sellingDate = 0;
	public double cost = 0;
	
	public int medicineCount = 0;
	public int testCount = 0;

	public String alternateDrugName = "";
	
	public long visitTime = -1;
	public String formattedVisitTime = ""; 
	

    @Override
	public String toString() {
            return "$PatientPrescriptionMedicine2DTO[" +
            " iD = " + iD +
            " prescriptionDetailsId = " + prescriptionDetailsId +
            " drugInformationType = " + drugInformationType +
            " dosageTypeCat = " + dosageTypeCat +
            " dose = " + dose +
            " doseUnitCat = " + doseUnitCat +
            " frequency = " + frequency +
            " foodInstructionsCat = " + foodInstructionsCat +
            " suppliedDrugInformationType = " + suppliedDrugInformationType +
            " doseSold = " + doseSold +
            " sellerId = " + sellerId +
            " sellerName = " + sellerName +
            " sellingDate = " + sellingDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}