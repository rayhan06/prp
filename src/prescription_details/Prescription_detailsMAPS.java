package prescription_details;
import java.util.*; 
import util.*;


public class Prescription_detailsMAPS extends CommonMaps
{	
	public Prescription_detailsMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("appointmentId".toLowerCase(), "appointmentId".toLowerCase());
		java_DTO_map.put("name".toLowerCase(), "name".toLowerCase());
		java_DTO_map.put("dateOfBirth".toLowerCase(), "dateOfBirth".toLowerCase());
		java_DTO_map.put("age".toLowerCase(), "age".toLowerCase());
		java_DTO_map.put("height".toLowerCase(), "height".toLowerCase());
		java_DTO_map.put("weight".toLowerCase(), "weight".toLowerCase());
		java_DTO_map.put("bloodPressureDiastole".toLowerCase(), "bloodPressureDiastole".toLowerCase());
		java_DTO_map.put("diagnosis".toLowerCase(), "diagnosis".toLowerCase());
		java_DTO_map.put("chiefComplaints".toLowerCase(), "chiefComplaints".toLowerCase());
		java_DTO_map.put("clinicalFeatures".toLowerCase(), "clinicalFeatures".toLowerCase());
		java_DTO_map.put("advice".toLowerCase(), "advice".toLowerCase());
		java_DTO_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_DTO_map.put("medicineAdded".toLowerCase(), "medicineAdded".toLowerCase());
		java_DTO_map.put("bloodPressureSystole".toLowerCase(), "bloodPressureSystole".toLowerCase());
		java_DTO_map.put("pulse".toLowerCase(), "pulse".toLowerCase());
		java_DTO_map.put("temperature".toLowerCase(), "temperature".toLowerCase());
		java_DTO_map.put("oxygenSaturation".toLowerCase(), "oxygenSaturation".toLowerCase());
		java_DTO_map.put("bmi".toLowerCase(), "bmi".toLowerCase());
		java_DTO_map.put("bloodGroup".toLowerCase(), "bloodGroup".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("appointment_id".toLowerCase(), "appointmentId".toLowerCase());
		java_SQL_map.put("name".toLowerCase(), "name".toLowerCase());
		java_SQL_map.put("date_of_birth".toLowerCase(), "dateOfBirth".toLowerCase());
		java_SQL_map.put("age".toLowerCase(), "age".toLowerCase());
		java_SQL_map.put("height".toLowerCase(), "height".toLowerCase());
		java_SQL_map.put("weight".toLowerCase(), "weight".toLowerCase());
		java_SQL_map.put("blood_pressure_diastole".toLowerCase(), "bloodPressureDiastole".toLowerCase());
		java_SQL_map.put("diagnosis".toLowerCase(), "diagnosis".toLowerCase());
		java_SQL_map.put("chief_complaints".toLowerCase(), "chiefComplaints".toLowerCase());
		java_SQL_map.put("clinical_features".toLowerCase(), "clinicalFeatures".toLowerCase());
		java_SQL_map.put("advice".toLowerCase(), "advice".toLowerCase());
		java_SQL_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_SQL_map.put("medicine_added".toLowerCase(), "medicineAdded".toLowerCase());
		java_SQL_map.put("blood_pressure_systole".toLowerCase(), "bloodPressureSystole".toLowerCase());
		java_SQL_map.put("pulse".toLowerCase(), "pulse".toLowerCase());
		java_SQL_map.put("temperature".toLowerCase(), "temperature".toLowerCase());
		java_SQL_map.put("oxygen_saturation".toLowerCase(), "oxygenSaturation".toLowerCase());
		java_SQL_map.put("bmi".toLowerCase(), "bmi".toLowerCase());
		java_SQL_map.put("blood_group".toLowerCase(), "bloodGroup".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Appointment Id".toLowerCase(), "appointmentId".toLowerCase());
		java_Text_map.put("Name".toLowerCase(), "name".toLowerCase());
		java_Text_map.put("Date Of Birth".toLowerCase(), "dateOfBirth".toLowerCase());
		java_Text_map.put("Age".toLowerCase(), "age".toLowerCase());
		java_Text_map.put("Height".toLowerCase(), "height".toLowerCase());
		java_Text_map.put("Weight".toLowerCase(), "weight".toLowerCase());
		java_Text_map.put("Blood Pressure Diastole".toLowerCase(), "bloodPressureDiastole".toLowerCase());
		java_Text_map.put("Diagnosis".toLowerCase(), "diagnosis".toLowerCase());
		java_Text_map.put("Chief Complaints".toLowerCase(), "chiefComplaints".toLowerCase());
		java_Text_map.put("Clinical Features".toLowerCase(), "clinicalFeatures".toLowerCase());
		java_Text_map.put("Advice".toLowerCase(), "advice".toLowerCase());
		java_Text_map.put("Remarks".toLowerCase(), "remarks".toLowerCase());
		java_Text_map.put("Medicine Added".toLowerCase(), "medicineAdded".toLowerCase());
		java_Text_map.put("Blood Pressure Systole".toLowerCase(), "bloodPressureSystole".toLowerCase());
		java_Text_map.put("Pulse".toLowerCase(), "pulse".toLowerCase());
		java_Text_map.put("Temperature".toLowerCase(), "temperature".toLowerCase());
		java_Text_map.put("Oxygen Saturation".toLowerCase(), "oxygenSaturation".toLowerCase());
		java_Text_map.put("Bmi".toLowerCase(), "bmi".toLowerCase());
		java_Text_map.put("Blood Group".toLowerCase(), "bloodGroup".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}