package prescription_details;
import java.util.*;

import appointment.AppointmentDTO;
import util.*; 


public class Dental_activitiesDTO extends CommonDTO
{

	public long prescriptionDetailsId = -1;
	public int dentalActionCat = -1;
	public long doctorEmployeeRecordId = -1;
	public long dtOrganogramId = -1;
	public long dtUserId = -1;
	public long dtErId = -1;
    public String doctorRemarks = "";
    public String dtRemarks = "";
	public boolean isDone = false;
	public long prescriptionDate = TimeConverter.getToday();
	
	public Dental_activitiesDTO()
	{
		
	}
	
	public Dental_activitiesDTO(AppointmentDTO appointmentDTO, Prescription_detailsDTO prescription_detailsDTO)
	{
		prescriptionDetailsId = prescription_detailsDTO.iD;
		doctorEmployeeRecordId = appointmentDTO.drEmployeeRecordId;
		prescriptionDate = appointmentDTO.visitDate;
	}
	
	
    @Override
	public String toString() {
            return "$Dental_activitiesDTO[" +
            " iD = " + iD +
            " prescriptionDetailsId = " + prescriptionDetailsId +
            " dentalActionCat = " + dentalActionCat +
            " doctorEmployeeRecordId = " + doctorEmployeeRecordId +
            " dtOrganogramId = " + dtOrganogramId +
            " dtUserId = " + dtUserId +
            " dtErId = " + dtErId +
            " doctorRemarks = " + doctorRemarks +
            " dtRemarks = " + dtRemarks +
            " isDone = " + isDone +
            " prescriptionDate = " + prescriptionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}