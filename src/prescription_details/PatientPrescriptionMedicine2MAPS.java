package prescription_details;
import java.util.*; 
import util.*;


public class PatientPrescriptionMedicine2MAPS extends CommonMaps
{	
	public PatientPrescriptionMedicine2MAPS(String tableName)
	{
		
		java_allfield_type_map.put("prescription_details_id".toLowerCase(), "Long");
		java_allfield_type_map.put("drug_information_type".toLowerCase(), "Long");
		java_allfield_type_map.put("dosage_type_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("dose".toLowerCase(), "String");
		java_allfield_type_map.put("dose_unit_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("frequency".toLowerCase(), "String");
		java_allfield_type_map.put("food_instructions_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("supplied_drug_information_type".toLowerCase(), "Long");
		java_allfield_type_map.put("dose_sold".toLowerCase(), "Integer");
		java_allfield_type_map.put("seller_id".toLowerCase(), "Long");
		java_allfield_type_map.put("seller_name".toLowerCase(), "String");
		java_allfield_type_map.put("selling_date".toLowerCase(), "Long");


		java_anyfield_search_map.put(tableName + ".prescription_details_id".toLowerCase(), "Long");
		java_anyfield_search_map.put("drug_information.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("drug_information.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".dose".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".frequency".toLowerCase(), "String");
		java_anyfield_search_map.put("supplied_drug_information.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("supplied_drug_information.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".dose_sold".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".seller_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".seller_name".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".selling_date".toLowerCase(), "Long");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("prescriptionDetailsId".toLowerCase(), "prescriptionDetailsId".toLowerCase());
		java_DTO_map.put("drugInformationType".toLowerCase(), "drugInformationType".toLowerCase());
		java_DTO_map.put("dosageTypeCat".toLowerCase(), "dosageTypeCat".toLowerCase());
		java_DTO_map.put("dose".toLowerCase(), "dose".toLowerCase());
		java_DTO_map.put("doseUnitCat".toLowerCase(), "doseUnitCat".toLowerCase());
		java_DTO_map.put("frequency".toLowerCase(), "frequency".toLowerCase());
		java_DTO_map.put("foodInstructionsCat".toLowerCase(), "foodInstructionsCat".toLowerCase());
		java_DTO_map.put("suppliedDrugInformationType".toLowerCase(), "suppliedDrugInformationType".toLowerCase());
		java_DTO_map.put("doseSold".toLowerCase(), "doseSold".toLowerCase());
		java_DTO_map.put("sellerId".toLowerCase(), "sellerId".toLowerCase());
		java_DTO_map.put("sellerName".toLowerCase(), "sellerName".toLowerCase());
		java_DTO_map.put("sellingDate".toLowerCase(), "sellingDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("prescription_details_id".toLowerCase(), "prescriptionDetailsId".toLowerCase());
		java_SQL_map.put("drug_information_type".toLowerCase(), "drugInformationType".toLowerCase());
		java_SQL_map.put("dosage_type_cat".toLowerCase(), "dosageTypeCat".toLowerCase());
		java_SQL_map.put("dose".toLowerCase(), "dose".toLowerCase());
		java_SQL_map.put("dose_unit_cat".toLowerCase(), "doseUnitCat".toLowerCase());
		java_SQL_map.put("frequency".toLowerCase(), "frequency".toLowerCase());
		java_SQL_map.put("food_instructions_cat".toLowerCase(), "foodInstructionsCat".toLowerCase());
		java_SQL_map.put("supplied_drug_information_type".toLowerCase(), "suppliedDrugInformationType".toLowerCase());
		java_SQL_map.put("dose_sold".toLowerCase(), "doseSold".toLowerCase());
		java_SQL_map.put("seller_id".toLowerCase(), "sellerId".toLowerCase());
		java_SQL_map.put("seller_name".toLowerCase(), "sellerName".toLowerCase());
		java_SQL_map.put("selling_date".toLowerCase(), "sellingDate".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Prescription Details Id".toLowerCase(), "prescriptionDetailsId".toLowerCase());
		java_Text_map.put("Drug Information".toLowerCase(), "drugInformationType".toLowerCase());
		java_Text_map.put("Dosage Type Cat".toLowerCase(), "dosageTypeCat".toLowerCase());
		java_Text_map.put("Dose".toLowerCase(), "dose".toLowerCase());
		java_Text_map.put("Dose Unit Cat".toLowerCase(), "doseUnitCat".toLowerCase());
		java_Text_map.put("Frequency".toLowerCase(), "frequency".toLowerCase());
		java_Text_map.put("Food Instructions Cat".toLowerCase(), "foodInstructionsCat".toLowerCase());
		java_Text_map.put("Supplied Drug Information".toLowerCase(), "suppliedDrugInformationType".toLowerCase());
		java_Text_map.put("Dose Sold".toLowerCase(), "doseSold".toLowerCase());
		java_Text_map.put("Seller Id".toLowerCase(), "sellerId".toLowerCase());
		java_Text_map.put("Seller Name".toLowerCase(), "sellerName".toLowerCase());
		java_Text_map.put("Selling Date".toLowerCase(), "sellingDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}