package prescription_details;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;
import common_lab_report.Common_lab_reportDAO;
import lab_test.LabTestListDAO;
import lab_test.LabTestListDTO;
import pb.CommonDAO;
import pb.Utils;

import org.apache.log4j.Logger;

import appointment.KeyCountDTO;
import repository.RepositoryManager;
import sessionmanager.SessionConstants;
import util.*;
import user.UserDTO;

public class PrescriptionLabTestDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	Common_lab_reportDAO common_lab_reportDAO = new Common_lab_reportDAO();

	public PrescriptionLabTestDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new PrescriptionLabTestMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"prescription_details_id",
			"lab_test_type",
			"lab_test_details_types",
			"test_status",
			"approval_status",
			"approval_date",
			"approver_user_name",
			"approver_organogram_id",
			
			"tester_id",
			"testing_date",
			"xray_plate_type",
			"plate_quantity",
			"plate_wasted",
			"xray_report_id",
			"xray_technologist_user_name",
			"is_done",
			
			"plate_count_1",
			"plate_count_2",
			"plate_count_3",
			
			"plate_wasted_1",
			"plate_wasted_2",
			"plate_wasted_3",

			"description",
			"alternate_labtest_name",
			"alternate_labtest_details",
			"scheduled_delivery_date",
			"actual_delivery_date",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public PrescriptionLabTestDAO()
	{
		this("prescription_lab_test");		
	}
	
	public void setSearchColumn(PrescriptionLabTestDTO prescriptionlabtestDTO)
	{
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		PrescriptionLabTestDTO prescriptionlabtestDTO = (PrescriptionLabTestDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	

		if(isInsert)
		{
			ps.setObject(index++,prescriptionlabtestDTO.iD);
		}
		ps.setObject(index++,prescriptionlabtestDTO.prescriptionDetailsId);
		ps.setObject(index++,prescriptionlabtestDTO.labTestType);
		ps.setObject(index++,prescriptionlabtestDTO.labTestDetailsTypes);
		ps.setObject(index++,prescriptionlabtestDTO.testStatus);
		ps.setObject(index++,prescriptionlabtestDTO.approvalStatus);
		ps.setObject(index++,prescriptionlabtestDTO.approvalDate);
		ps.setObject(index++,prescriptionlabtestDTO.approverUserName);
		ps.setObject(index++,prescriptionlabtestDTO.approverOrganogramId);
		
		ps.setObject(index++,prescriptionlabtestDTO.testerId);
		ps.setObject(index++,prescriptionlabtestDTO.testingDate);
		ps.setObject(index++,prescriptionlabtestDTO.xRayPlateType);
		ps.setObject(index++,prescriptionlabtestDTO.plateQuantity);
		ps.setObject(index++,prescriptionlabtestDTO.plateWasted);
		ps.setObject(index++,prescriptionlabtestDTO.xRayReportId);
		ps.setObject(index++,prescriptionlabtestDTO.xRayUserName);
		ps.setObject(index++,prescriptionlabtestDTO.isDone);
		
		ps.setObject(index++,prescriptionlabtestDTO.plateUsed1);
		ps.setObject(index++,prescriptionlabtestDTO.plateUsed2);
		ps.setObject(index++,prescriptionlabtestDTO.plateUsed3);
		
		ps.setObject(index++,prescriptionlabtestDTO.plateWasted1);
		ps.setObject(index++,prescriptionlabtestDTO.plateWasted2);
		ps.setObject(index++,prescriptionlabtestDTO.plateWasted3);

		ps.setObject(index++,prescriptionlabtestDTO.description);
		ps.setObject(index++,prescriptionlabtestDTO.alternateLabTestName);
		ps.setObject(index++,prescriptionlabtestDTO.alternateLabTestDetails);
		ps.setObject(index++,prescriptionlabtestDTO.scheduledDeliveryDate);
		ps.setObject(index++,prescriptionlabtestDTO.actualDeliveryDate);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public PrescriptionLabTestDTO build(ResultSet rs)
	{
		try
		{
			PrescriptionLabTestDTO prescriptionlabtestDTO = new PrescriptionLabTestDTO();
			prescriptionlabtestDTO.iD = rs.getLong("ID");
			prescriptionlabtestDTO.prescriptionDetailsId = rs.getLong("prescription_details_id");
			prescriptionlabtestDTO.labTestType = rs.getLong("lab_test_type");
			prescriptionlabtestDTO.labTestDetailsTypes = rs.getString("lab_test_details_types");
			prescriptionlabtestDTO.testStatus = rs.getInt("test_status");
			prescriptionlabtestDTO.approvalStatus = rs.getInt("approval_status");
			prescriptionlabtestDTO.approvalDate = rs.getLong("approval_date");
			prescriptionlabtestDTO.approverUserName = rs.getString("approver_user_name");
			prescriptionlabtestDTO.approverOrganogramId = rs.getLong("approver_organogram_id");
			
			prescriptionlabtestDTO.testerId = rs.getString("tester_id");
			prescriptionlabtestDTO.testingDate = rs.getLong("testing_date");
			
			prescriptionlabtestDTO.xRayPlateType = rs.getString("xray_plate_type");
			prescriptionlabtestDTO.plateQuantity = rs.getString("plate_quantity");
			prescriptionlabtestDTO.plateWasted = rs.getString("plate_wasted");
			
			if(prescriptionlabtestDTO.plateWasted == null)
			{
				prescriptionlabtestDTO.plateWasted = "";
			}
			prescriptionlabtestDTO.xRayReportId = rs.getLong("xray_report_id");
			prescriptionlabtestDTO.xRayUserName = rs.getString("xray_technologist_user_name");
			prescriptionlabtestDTO.isDone = rs.getBoolean("is_done");
			
			prescriptionlabtestDTO.plateUsed1 = rs.getInt("plate_count_1");
			prescriptionlabtestDTO.plateUsed2 = rs.getInt("plate_count_2");
			prescriptionlabtestDTO.plateUsed3 = rs.getInt("plate_count_3");
			
			prescriptionlabtestDTO.plateWasted1 = rs.getInt("plate_wasted_1");
			prescriptionlabtestDTO.plateWasted2 = rs.getInt("plate_wasted_2");
			prescriptionlabtestDTO.plateWasted3 = rs.getInt("plate_wasted_3");

			prescriptionlabtestDTO.description = rs.getString("description");
			prescriptionlabtestDTO.alternateLabTestName = rs.getString("alternate_labtest_name");
			prescriptionlabtestDTO.alternateLabTestDetails = rs.getString("alternate_labtest_details");
			prescriptionlabtestDTO.scheduledDeliveryDate = rs.getLong("scheduled_delivery_date");
			prescriptionlabtestDTO.actualDeliveryDate = rs.getLong("actual_delivery_date");
			prescriptionlabtestDTO.isDeleted = rs.getInt("isDeleted");
			prescriptionlabtestDTO.lastModificationTime = rs.getLong("lastModificationTime");
			
			String [] plateTypes = prescriptionlabtestDTO.xRayPlateType.split(", ");
			
			prescriptionlabtestDTO.plateTypesArray = new long [plateTypes.length];
			prescriptionlabtestDTO.plateQuantityArray = new int [plateTypes.length];
			prescriptionlabtestDTO.plateWastedArray = new int [plateTypes.length];
			
			String [] plateQuantitiess = prescriptionlabtestDTO.plateQuantity.split(", ");
			String [] plateWasteds = prescriptionlabtestDTO.plateWasted.split(", ");
			
			int j = 0;
			for(String plateType: plateTypes)
			{
				prescriptionlabtestDTO.plateTypesArray[j] = Long.parseLong(plateType);
				prescriptionlabtestDTO.plateQuantityArray[j] = 0;
				if(plateQuantitiess != null && j < plateQuantitiess.length)
				{
					prescriptionlabtestDTO.plateQuantityArray[j] = Integer.parseInt(plateQuantitiess[j]);
				}
				
				prescriptionlabtestDTO.plateWastedArray[j] = 0;
				if(plateWasteds != null && j < plateWasteds.length && !plateWasteds[j].equalsIgnoreCase(""))
				{
					prescriptionlabtestDTO.plateWastedArray[j] = Integer.parseInt(plateWasteds[j]);
				}
				j++;
			}
			return prescriptionlabtestDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	public String getTestName(PrescriptionLabTestDTO prescriptionLabTestDTO)
	{
		String value = "";
		System.out.println("Found alternateLabTestName = " + prescriptionLabTestDTO.alternateLabTestName);
	
		if(prescriptionLabTestDTO.labTestType == SessionConstants.LAB_TEST_OTHER)
		{
			value = prescriptionLabTestDTO.description;
        }
		else if (prescriptionLabTestDTO.alternateLabTestName.equalsIgnoreCase("")) 
		{
            value = CommonDAO.getName(prescriptionLabTestDTO.labTestType, "lab_test", "name_en", "id");
            if (!prescriptionLabTestDTO.description.equalsIgnoreCase("")) 
            {
                value += " :" + prescriptionLabTestDTO.description;
            }
		}
		else
		{
			value = prescriptionLabTestDTO.alternateLabTestName + ": " + prescriptionLabTestDTO.description;
		}
		return value;
	}
	
	LabTestListDAO labTestListDAO = new LabTestListDAO();
	
	public String getTestDetails(PrescriptionLabTestDTO prescriptionLabTestDTO)
	{
		String[] details = prescriptionLabTestDTO.labTestDetailsTypes.split(", ");
        int j = 0;
        String value = "";
        String detailStr = "";
        for (String detail : details) {

            if (!detail.equalsIgnoreCase("") && !detail.equalsIgnoreCase("-4")) {
                value = CommonDAO.getName(Integer.parseInt(detail), "lab_test_list",  "name_en", "id");
                LabTestListDTO labTestListDTO = labTestListDAO.getDTOByID(Long.parseLong(detail));

                if (labTestListDTO != null && labTestListDTO.isTitle && !value.equalsIgnoreCase("")) {
                    value = "<b>" + value + "</b>";
                    if (j < details.length - 1) {
                        value = value + ": ";
                    }
                } else if (labTestListDTO != null && !labTestListDTO.isTitle && j < details.length - 1 && !value.equalsIgnoreCase("")) {
                    value = value + ", ";
                }
            } else if (detail.equalsIgnoreCase("-4")) {
                value = prescriptionLabTestDTO.alternateLabTestDetails;
                if (j < details.length - 1) {
                    value = value + ", ";
                }
            }
            if (!value.equalsIgnoreCase("")) {
                System.out.println("Adding value = " + value);
                detailStr += value;

            }

            j++;

        }
		return detailStr;
	}
	
	public String getApprovalStatusName(int approvalStatus, boolean isLangEng)
	{
		if(approvalStatus == PrescriptionLabTestDTO.PENDING)
		{
			if(isLangEng)
			{
				return "Approval Pending.";
			}
			else
			{
				return "অনুমোদনের অপেক্ষায়";
			}
		}
		else if(approvalStatus == PrescriptionLabTestDTO.APPROVED)
		{
			if(isLangEng)
			{
				return "Approved.";
			}
			else
			{
				return "অনুমোদিত।";
			}
		}
		else if(approvalStatus == PrescriptionLabTestDTO.REJECTED)
		{
			if(isLangEng)
			{
				return "Rejected";
			}
			else
			{
				return "বাতিল।";
			}
		}
		return "";
	}
	
	public String getProgress(PrescriptionLabTestDTO prescriptionLabTestDTO, String language)
	{
		if(prescriptionLabTestDTO.labTestType == SessionConstants.LAB_TEST_OTHER)
		{
			return "";
		}
		String detailStr = "";
		boolean isLangEng = language.equalsIgnoreCase("english");
		try {
			if(prescriptionLabTestDTO.isDone)
			{
				if(isLangEng)
				{
					detailStr = "Test Done. ";
				}
				else
				{
					detailStr = "পরীক্ষা সম্পন্ন।  ";
				}
				if(prescriptionLabTestDTO.testStatus == 1)
				{
					if(isLangEng)
					{
						detailStr += "Report written. ";
					}
					else
					{
						detailStr += "রিপোর্ট লেখা হয়েছে। ";
					}
				}
				
				if(prescriptionLabTestDTO.labTestType == SessionConstants.LAB_TEST_XRAY)
				{
					detailStr += "<br>";
					
					if(isLangEng)
					{
						detailStr += "Plates used: <br>";
					}
					else
					{
						detailStr += "ব্যবহৃত প্লেটসমূহ: <br>" ;
					}
					
					String [] plateTypes = prescriptionLabTestDTO.xRayPlateType.split(", ");
					String [] plateQuantities = prescriptionLabTestDTO.plateQuantity.split(", ");
					if(plateTypes != null)
					{
						int j = 0;
						for(String plateType: plateTypes)
						{
							detailStr += CommonDAO.getName("English", "medical_equipment_name", Long.parseLong(plateType)) + " ";
							detailStr += " (" + Utils.getDigits(plateQuantities[j], language) + ")<br>";
							j ++;
						}
					}
				}
				if(prescriptionLabTestDTO.labTestType != SessionConstants.LAB_TEST_XRAY && prescriptionLabTestDTO.labTestType != SessionConstants.LAB_TEST_ULTRASONOGRAM)
				{
					detailStr += getApprovalStatusName(prescriptionLabTestDTO.approvalStatus, isLangEng);
				}
				
			}
			else
			{
				if(isLangEng)
				{
					detailStr = "Test Not Done. ";
				}
				else
				{
					detailStr = "পরীক্ষা সম্পন্ন হয় নি। ";
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return detailStr;
	}
	
	public List<KeyCountDTO> getLast7DayTestCount (String userName, int testType)
    {
		String sql = "SELECT \r\n" + 
				"    ((testing_date DIV 86400000) * 86400000 - 21600000) AS tdate,\r\n" + 
				"    COUNT(id)\r\n" + 
				"FROM\r\n" + 
				"    prescription_lab_test\r\n" + 
				"WHERE\r\n" + 
				"    is_done = 1 ";
		if(testType >= 0)
		{
			sql += " AND lab_test_type = " + testType; 
		}
		
		sql += " and testing_date >= " + TimeConverter.getNthDay(-7) + "\r\n"; 
		if(userName != null && !userName.equalsIgnoreCase(""))
		{
			sql += 	"    and xray_technologist_user_name =  '" + userName + "'\r\n";
		}
		
		sql += " GROUP BY tdate\r\n" + 
				"ORDER BY tdate ASC;";
		
		return ConnectionAndStatementUtil.getListOfT(sql,this::getTestCount);	
    }
	
	public List<KeyCountDTO> getLast7DayTestCountForRadiologist (String userName, int testType)
    {
		String sql = "SELECT \r\n" + 
				"    ((actual_delivery_date DIV 86400000) * 86400000 - 21600000) AS tdate,\r\n" + 
				"    COUNT(id)\r\n" + 
				"FROM\r\n" + 
				"    prescription_lab_test\r\n" + 
				"WHERE\r\n" + 
				"    is_done = 1 and test_status = 1 ";
		if(testType >= 0)
		{
			sql += " AND lab_test_type = " + testType; 
		}
		
		sql += " and actual_delivery_date >= " + TimeConverter.getNthDay(-7) + "\r\n" + 
				"    and tester_id =  '" + userName + "'\r\n" + 
				"GROUP BY tdate\r\n" + 
				"ORDER BY tdate ASC;";
		
		return ConnectionAndStatementUtil.getListOfT(sql,this::getTestCount);	
    }
	
	public KeyCountDTO getTestCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("tdate");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	
	public List<PrescriptionLabTestDTO> getPrescriptionLabTestDTOListByPrescriptionDetailsID(long prescriptionDetailsID) throws Exception
	{
		String sql = "SELECT * FROM prescription_lab_test where isDeleted=0 and prescription_details_id="+prescriptionDetailsID+" order by prescription_lab_test.lastModificationTime";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}

	public PrescriptionLabTestDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		PrescriptionLabTestDTO prescriptionlabtestDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return prescriptionlabtestDTO;
	}
	
	public int getCount(ResultSet rs)
	{
		try {
			return rs.getInt("count(prescription_lab_test.id)");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	
	}

	public int getNotDoneCount(long prescriptionDetailsID, long roleId) throws Exception{
		String sql = "SELECT count(prescription_lab_test.id) FROM prescription_lab_test where prescription_details_id = " + prescriptionDetailsID
				+ " ";
		if(roleId == SessionConstants.XRAY_GUY || roleId== SessionConstants.RADIOLOGIST)
		{
			sql += " AND (lab_test_type = " 
					+ SessionConstants.LAB_TEST_XRAY + " or lab_test_type = " 
					+ SessionConstants.LAB_TEST_ULTRASONOGRAM + ") ";
			if(roleId == SessionConstants.XRAY_GUY)
			{
				sql += " and is_done = 0 ";
			}
			else
			{
				sql += " and is_done = 1 and test_status = 0 ";
			}
		}
		else if(roleId == SessionConstants.LAB_TECHNITIAN_ROLE)
		{
			sql += " AND lab_test_type >= 0 and is_done = 0 and ("
					+ "lab_test_type != " + SessionConstants.LAB_TEST_XRAY 
					+ " and lab_test_type != " + SessionConstants.LAB_TEST_ULTRASONOGRAM 
					+ " and lab_test_type != " + SessionConstants.LAB_TEST_OTHER 
					+ ") ";
		}
		else if(roleId == SessionConstants.PATHOLOGY_HEAD)
		{
			sql += " AND lab_test_type >= 0 and is_done = 1 and  approval_status = " + PrescriptionLabTestDTO.PENDING
					+ " and ("
					+ "lab_test_type != " + SessionConstants.LAB_TEST_XRAY 
					+ " and lab_test_type != " + SessionConstants.LAB_TEST_ULTRASONOGRAM 
					+ " and lab_test_type != " + SessionConstants.LAB_TEST_OTHER 
					+ ") ";
		}
		return ConnectionAndStatementUtil.getT(sql,this::getCount, 0);		
	}
	
	public int getDoneCount(long prescriptionDetailsID) throws Exception{
		String sql = "SELECT count(prescription_lab_test.id) FROM prescription_lab_test where prescription_details_id = " + prescriptionDetailsID
				+ " and id in (select prescription_lab_test_id from common_lab_report)";
		return ConnectionAndStatementUtil.getT(sql,this::getCount, 0);		
	}
	
	
	public List<PrescriptionLabTestDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<PrescriptionLabTestDTO> getAllPrescriptionLabTest (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<PrescriptionLabTestDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<PrescriptionLabTestDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
				
}
	