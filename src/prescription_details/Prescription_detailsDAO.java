package prescription_details;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.*;

import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;
import config.GlobalConfigConstants;
import config.GlobalConfigurationRepository;
import pb.Utils;

import org.apache.log4j.Logger;

import appointment.AppointmentDAO;
import appointment.AppointmentDTO;
import appointment.KeyCountDTO;
import appointment.YearMonthCount;
import repository.RepositoryManager;

import util.*;
import workflow.WorkflowController;
import user.UserDTO;

public class Prescription_detailsDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	AppointmentDAO appointmentDAO = new AppointmentDAO();
	
	public static final int PENDING = 1;
	public static final int DELIVERED = 2;
	public static final int EXPIRED = 3;
	
	public static final String addLabTestFilter = " test_count > 0  ";
	public static final String pendingTestFilter = " test_count > 0 and is_done = 0 ";
	public static final String radioPendingTestFilter = " test_count > 0 and is_done = 1 and test_status = 0 ";
	public static final String pathologyHeadPendingFilter = " test_count > 0 and is_done = 1 and approval_status =  " + PrescriptionLabTestDTO.PENDING;
	
	public static final String myTestFilter = " test_count > 0 and is_done = 1 and testing_date >= ";
	public static final String radioMyTestFilter = " test_count > 0 and test_status = 1 and actual_delivery_date >= ";
	public static final String pathologyHeadApprovedFilter = " test_count > 0 and is_done = 1 and approval_status =  " + PrescriptionLabTestDTO.APPROVED;
	
	
	public Prescription_detailsDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";

		commonMaps = new Prescription_detailsMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"appointment_id",
			"name",
			"date_of_birth",
			"age",
			"height",
			"weight",
			"blood_pressure_diastole",
			"diagnosis",
			"chief_complaints",
			"clinical_features",
			"advice",
			"remarks",
			"medicine_added",
			"blood_pressure_systole",
			"pulse",
			"temperature",
			"oxygen_saturation",
			"bmi",
			"blood_sugar",
			"blood_group",
			"search_column",
			"referred_to",
			"referred_to_name",
			"referral_message",
			"referred_appointment_id",
			"employee_user_name",
			"therapy_needed",
			"employee_id",
			"employee_unit",
			"medicine_count",
			"test_count",
			"visit_date",
			"visit_time",
			"doctor_id",
			"who_is_the_patient_cat",
			"dr_user_name",
			"sl",
			"phone",
			"can_be_edited",
			
			"others_office",
			"others_office_en",
			"others_office_id",
			"others_designation_and_id",
			
			"alternate_phone",
			"extra_relation",
			"employee_record_id",
			
			"is_dental",
			"dental_instruction_needed",
			
			"modified_by_er_id",
			
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Prescription_detailsDAO()
	{
		this("prescription_details");		
	}
	
	public void setSearchColumn(Prescription_detailsDTO prescription_detailsDTO)
	{
		prescription_detailsDTO.searchColumn = "";
		prescription_detailsDTO.searchColumn += prescription_detailsDTO.name + " ";
		prescription_detailsDTO.searchColumn += prescription_detailsDTO.employeeUserName + " ";
		prescription_detailsDTO.searchColumn += prescription_detailsDTO.sl + " ";
		prescription_detailsDTO.searchColumn += prescription_detailsDTO.phone + " ";
		prescription_detailsDTO.searchColumn += Utils.getDigitBanglaFromEnglish(prescription_detailsDTO.employeeUserName) + " ";
		prescription_detailsDTO.searchColumn += Utils.getDigitBanglaFromEnglish(prescription_detailsDTO.sl) + " ";
		prescription_detailsDTO.searchColumn += Utils.getDigitBanglaFromEnglish(prescription_detailsDTO.phone) + " ";
		
		AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(prescription_detailsDTO.appointmentId);
		prescription_detailsDTO.searchColumn += appointmentDTO.doctorName + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Prescription_detailsDTO prescription_detailsDTO = (Prescription_detailsDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(prescription_detailsDTO);
		if(isInsert)
		{
			ps.setObject(index++,prescription_detailsDTO.iD);
		}
		ps.setObject(index++,prescription_detailsDTO.appointmentId);
		ps.setObject(index++,prescription_detailsDTO.name);
		ps.setObject(index++,prescription_detailsDTO.dateOfBirth);
		ps.setObject(index++,prescription_detailsDTO.age);
		ps.setObject(index++,prescription_detailsDTO.height);
		ps.setObject(index++,prescription_detailsDTO.weight);
		ps.setObject(index++,prescription_detailsDTO.bloodPressureDiastole);
		ps.setObject(index++,prescription_detailsDTO.diagnosis);
		ps.setObject(index++,prescription_detailsDTO.chiefComplaints);
		ps.setObject(index++,prescription_detailsDTO.clinicalFeatures);
		ps.setObject(index++,prescription_detailsDTO.advice);
		ps.setObject(index++,prescription_detailsDTO.remarks);
		ps.setObject(index++,prescription_detailsDTO.medicineAdded);
		ps.setObject(index++,prescription_detailsDTO.bloodPressureSystole);
		ps.setObject(index++,prescription_detailsDTO.pulse);
		ps.setObject(index++,prescription_detailsDTO.temperature);
		ps.setObject(index++,prescription_detailsDTO.oxygenSaturation);
		ps.setObject(index++,prescription_detailsDTO.bmi);
		ps.setObject(index++,prescription_detailsDTO.sugar);
		ps.setObject(index++,prescription_detailsDTO.bloodGroup);
		ps.setObject(index++,prescription_detailsDTO.searchColumn);
		ps.setObject(index++,prescription_detailsDTO.referredTo);
		ps.setObject(index++,prescription_detailsDTO.referredToName);
		ps.setObject(index++,prescription_detailsDTO.referralMessage);
		ps.setObject(index++,prescription_detailsDTO.referredAppointmentId);
		ps.setObject(index++,prescription_detailsDTO.employeeUserName);
		ps.setObject(index++,prescription_detailsDTO.therapyNeeded);
		ps.setObject(index++,prescription_detailsDTO.employeeId);
		ps.setObject(index++,prescription_detailsDTO.employeeUnit);
		ps.setObject(index++,prescription_detailsDTO.medicineCount);
		ps.setObject(index++,prescription_detailsDTO.testCount);
		ps.setObject(index++,prescription_detailsDTO.visitDate);
		ps.setObject(index++,prescription_detailsDTO.visitTime);
		ps.setObject(index++,prescription_detailsDTO.doctorId);
		ps.setObject(index++,prescription_detailsDTO.whoIsThePatientCat);
		ps.setObject(index++,prescription_detailsDTO.drUserName);
		ps.setObject(index++,prescription_detailsDTO.sl);
		ps.setObject(index++,prescription_detailsDTO.phone);
		ps.setObject(index++,prescription_detailsDTO.canBeEdited);
		
		ps.setObject(index++,prescription_detailsDTO.othersOfficeBn);
		ps.setObject(index++,prescription_detailsDTO.othersOfficeEn);
		ps.setObject(index++,prescription_detailsDTO.othersOfficeId);
		ps.setObject(index++,prescription_detailsDTO.othersDesignationAndId);
		ps.setObject(index++,prescription_detailsDTO.alternatePhone);
		ps.setObject(index++,prescription_detailsDTO.extraRelation);
		ps.setObject(index++,prescription_detailsDTO.erId);
		
		ps.setObject(index++,prescription_detailsDTO.isDental);
		ps.setObject(index++,prescription_detailsDTO.dentalInstructionNeeded);
		
		ps.setObject(index++,prescription_detailsDTO.modifiedByErId);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Prescription_detailsDTO build(ResultSet rs)
	{
		try
		{
			Prescription_detailsDTO prescription_detailsDTO = new Prescription_detailsDTO();
			prescription_detailsDTO.iD = rs.getLong("ID");
			prescription_detailsDTO.appointmentId = rs.getLong("appointment_id");
			prescription_detailsDTO.name = rs.getString("name");
			prescription_detailsDTO.dateOfBirth = rs.getLong("date_of_birth");
			prescription_detailsDTO.age = rs.getLong("age");
			prescription_detailsDTO.height = rs.getDouble("height");
			prescription_detailsDTO.weight = rs.getDouble("weight");
			prescription_detailsDTO.bloodPressureDiastole = rs.getDouble("blood_pressure_diastole");
			prescription_detailsDTO.diagnosis = rs.getString("diagnosis");
			prescription_detailsDTO.chiefComplaints = rs.getString("chief_complaints");
			prescription_detailsDTO.clinicalFeatures = rs.getString("clinical_features");
			prescription_detailsDTO.advice = rs.getString("advice");
			prescription_detailsDTO.remarks = rs.getString("remarks");
			prescription_detailsDTO.medicineAdded = rs.getBoolean("medicine_added");
			prescription_detailsDTO.bloodPressureSystole = rs.getDouble("blood_pressure_systole");
			prescription_detailsDTO.pulse = rs.getDouble("pulse");
			prescription_detailsDTO.temperature = rs.getDouble("temperature");
			prescription_detailsDTO.oxygenSaturation = rs.getDouble("oxygen_saturation");
			prescription_detailsDTO.bmi = rs.getDouble("bmi");
			prescription_detailsDTO.sugar = rs.getString("blood_sugar");
			prescription_detailsDTO.bloodGroup = rs.getString("blood_group");
			prescription_detailsDTO.searchColumn = rs.getString("search_column");
			prescription_detailsDTO.referredTo = rs.getLong("referred_to");
			prescription_detailsDTO.referredToName = rs.getString("referred_to_name");
			prescription_detailsDTO.referralMessage = rs.getString("referral_message");
			prescription_detailsDTO.referredAppointmentId = rs.getLong("referred_appointment_id");
			prescription_detailsDTO.employeeUserName = rs.getString("employee_user_name");
			prescription_detailsDTO.therapyNeeded = rs.getBoolean("therapy_needed");
			prescription_detailsDTO.employeeId = rs.getLong("employee_id");
			prescription_detailsDTO.employeeUnit = rs.getLong("employee_unit");
			prescription_detailsDTO.medicineCount = rs.getInt("medicine_count");
			prescription_detailsDTO.testCount = rs.getInt("test_count");
			prescription_detailsDTO.visitDate = rs.getLong("visit_date");
			prescription_detailsDTO.visitTime = rs.getLong("visit_time");
			prescription_detailsDTO.doctorId = rs.getLong("doctor_id");
			prescription_detailsDTO.whoIsThePatientCat = rs.getInt("who_is_the_patient_cat");
			prescription_detailsDTO.drUserName = rs.getString("dr_user_name");
			prescription_detailsDTO.sl = rs.getString("sl");
			prescription_detailsDTO.phone = rs.getString("phone");
			prescription_detailsDTO.canBeEdited = rs.getBoolean("can_be_edited");
			
			prescription_detailsDTO.othersOfficeBn = rs.getString("others_office");
			prescription_detailsDTO.othersOfficeEn = rs.getString("others_office_en");
			prescription_detailsDTO.othersOfficeId = rs.getLong("others_office_id");
			
			prescription_detailsDTO.othersDesignationAndId = rs.getString("others_designation_and_id");
			prescription_detailsDTO.alternatePhone = rs.getString("alternate_phone");
			prescription_detailsDTO.extraRelation = rs.getString("extra_relation");
			if(prescription_detailsDTO.extraRelation == null)
			{
				prescription_detailsDTO.extraRelation = "";
			}
			
			prescription_detailsDTO.erId = rs.getLong("employee_record_id");
			
			prescription_detailsDTO.isDental = rs.getBoolean("is_dental");
			prescription_detailsDTO.dentalInstructionNeeded = rs.getBoolean("dental_instruction_needed");
			
			prescription_detailsDTO.modifiedByErId = rs.getLong("modified_by_er_id");
			
			prescription_detailsDTO.isDeleted = rs.getInt("isDeleted");
			prescription_detailsDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return prescription_detailsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	public boolean isExpired(Prescription_detailsDTO prescription_detailsDTO)
	{
		int validityHours = Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.PRESCIPTION_VALIDITY_HOURS).value);
	    long now = System.currentTimeMillis();
	    long timePassedInMillis = now - prescription_detailsDTO.visitTime;
	    long timePassedInHours = timePassedInMillis/(1000 * 3600);
	    boolean isExpired = timePassedInHours > validityHours;
        return isExpired;
	}
	public String getMedicineStatus(Prescription_detailsDTO prescription_detailsDTO, String language)
	{
		if(language.equalsIgnoreCase("english"))
		{
			if(prescription_detailsDTO.medicineAdded)
			{
				return "Delivered";
			}
			else
			{
				if(isExpired(prescription_detailsDTO))
				{
					return "Expired";
				}
				else
				{
					return "Pending";
				}
			}
		}
		else
		{
			if(prescription_detailsDTO.medicineAdded)
			{
				return "প্রদত্ত";
			}
			else
			{
				if(isExpired(prescription_detailsDTO))
				{
					return "মেয়াদ শেষ";
				}
				else
				{
					return "অপেক্ষারত";
				}
			}
		}
	}

	public boolean canAddMedicine(AppointmentDTO appointmentDTO, UserDTO userDTO )
	{
		if(appointmentDTO == null || userDTO == null)
		{
			System.out.println("null appointmentdto or userdto");
			return false;
		}
		if(getDTOByappointmentId (appointmentDTO.iD) == null)
		{
			System.out.println("no prescription");
			return false;
		}
		if(userDTO.organogramID == appointmentDTO.doctorId 
				|| userDTO.roleID == SessionConstants.INVENTORY_MANGER_ROLE
				|| userDTO.roleID == SessionConstants.PHARMACY_PERSON
				|| userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE
				|| userDTO.roleID == SessionConstants.ADMIN_ROLE)
		{
			System.out.println("Permission issues");
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	public boolean canAddLabTest(AppointmentDTO appointmentDTO, UserDTO userDTO )
	{
		if(appointmentDTO == null || userDTO == null)
		{
			System.out.println("null appointmentdto or userdto");
			return false;
		}
		if(getDTOByappointmentId (appointmentDTO.iD) == null)
		{
			System.out.println("no prescription");
			return false;
		}
		if(userDTO.organogramID == appointmentDTO.doctorId 
				|| userDTO.roleID == SessionConstants.LAB_TECHNITIAN_ROLE
				|| userDTO.roleID == SessionConstants.PATHOLOGY_HEAD
				|| userDTO.roleID == SessionConstants.XRAY_GUY
				|| userDTO.roleID == SessionConstants.RADIOLOGIST
				|| userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE
				|| userDTO.roleID == SessionConstants.ADMIN_ROLE)
		{
			System.out.println("Permission issues");
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	public boolean canViewPrescriptions(AppointmentDTO appointmentDTO, UserDTO userDTO )
	{
		if(appointmentDTO == null || userDTO == null)
		{
			System.out.println("null appointmentdto or userdto");
			return false;
		}
		if(getDTOByappointmentId (appointmentDTO.iD) == null)
		{
			System.out.println("no prescription");
			return false;
		}
		if(userDTO.roleID == SessionConstants.DOCTOR_ROLE
				|| userDTO.userName.equals(appointmentDTO.employeeUserName)
				|| userDTO.employee_record_id == appointmentDTO.erId
				|| userDTO.roleID == SessionConstants.LAB_TECHNITIAN_ROLE
				|| userDTO.roleID == SessionConstants.XRAY_GUY
				|| userDTO.roleID == SessionConstants.RADIOLOGIST
				|| userDTO.roleID == SessionConstants.MEDICAL_RECEPTIONIST_ROLE
				|| userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE
				|| userDTO.roleID == SessionConstants.ADMIN_ROLE
				|| userDTO.roleID == SessionConstants.PATHOLOGY_HEAD
				|| userDTO.roleID == SessionConstants.PHARMACY_PERSON
				|| userDTO.roleID == SessionConstants.NURSE_ROLE)
		{
			return true;
		}
		else
		{
			System.out.println("Permission issues");
			return false;
		}
		
	}
	
	public boolean canSeeAllPrescriptions(UserDTO userDTO)
	{
        return userDTO.roleID == SessionConstants.DOCTOR_ROLE
                || userDTO.roleID == SessionConstants.PHYSIOTHERAPIST_ROLE
                || userDTO.roleID == SessionConstants.NURSE_ROLE
                || userDTO.roleID == SessionConstants.MEDICAL_RECEPTIONIST_ROLE
                || userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE
                || userDTO.roleID == SessionConstants.ADMIN_ROLE
                || userDTO.roleID == SessionConstants.LAB_TECHNITIAN_ROLE
                || userDTO.roleID == SessionConstants.PATHOLOGY_HEAD
                || userDTO.roleID == SessionConstants.XRAY_GUY
                || userDTO.roleID == SessionConstants.RADIOLOGIST
                || userDTO.roleID == SessionConstants.PHARMACY_PERSON
                || userDTO.roleID == SessionConstants.INVENTORY_MANGER_ROLE;
	}
	
	public boolean canAddPrescription(AppointmentDTO appointmentDTO, UserDTO userDTO )
	{
		if(appointmentDTO == null || userDTO == null)
		{
			return false;
		}
		if(getDTOByappointmentId (appointmentDTO.iD) != null)//One prescription, one report
		{
			return false;
		}
        return userDTO.organogramID == appointmentDTO.doctorId && !appointmentDTO.isCancelled && appointmentDTO.visitDate == TimeConverter.getToday();
		
	}

	public Prescription_detailsDTO getDTOByappointmentId (long appointmentId)
	{
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
		
        sql += " WHERE isDeleted = 0 and appointment_id=" + appointmentId + " order by id desc limit 1";
        Prescription_detailsDTO prescription_detailsDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return prescription_detailsDTO;
	}
	
	

	public Prescription_detailsDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Prescription_detailsDTO prescription_detailsDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return prescription_detailsDTO;
	}

	public long addMedicine(long id) throws Exception
	{		


		long lastModificationTime = System.currentTimeMillis();	
		String sql = "UPDATE " + tableName;
		
		sql += " SET ";
		sql += "medicine_added=1";
		sql += ", ";
		
		sql += " lastModificationTime = "	+ lastModificationTime + "";
        sql += " WHERE ID = " + id;
        return (long) ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            Connection connection = model.getConnection();
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                ps.executeUpdate();
                recordUpdateTime(connection, lastModificationTime);
                return id;
            } catch (SQLException ex) {
                logger.error(ex);
                return -1L;
            }
        }, sql);
	}

	
	public List<Prescription_detailsDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	
	
	
	
	public List<Prescription_detailsDTO> getAllPrescription_details (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Prescription_detailsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}
	


	
	public List<Prescription_detailsDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public KeyCountDTO getVisitDayWiseCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("visit_date");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public List<KeyCountDTO> getLast7DayCount (String drUserName)
    {
		String sql = "SELECT visit_date, count(id) FROM prescription_details where visit_date >=" + TimeConverter.getNthDay(-7) + " and isDeleted = 0";
		if(!drUserName.equalsIgnoreCase(""))
		{
			sql+= " and dr_user_name = '" + drUserName + "'";
		}
		sql+= " group by visit_date order by visit_date asc";

		return ConnectionAndStatementUtil.getListOfT(sql,this::getVisitDayWiseCount);	
    }
	
	public KeyCountDTO getDrWiseCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("dr_user_name");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public KeyCountDTO getUserWiseCountCosts(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.keyStr = rs.getString("employee_user_name");
			keyCountDTO.count = rs.getInt("SUM(dose_sold)");
			keyCountDTO.cost = rs.getDouble("SUM(patient_prescription_medicine2.cost)");
			keyCountDTO.key = rs.getLong("employee_id");
			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public List<KeyCountDTO> getUserWiseCountCosts()
    {
		String sql = "SELECT \r\n" + 
				"    employee_user_name,\r\n" + 
				"    employee_id,\r\n" + 
				"    SUM(dose_sold),\r\n" + 
				"    SUM(patient_prescription_medicine2.cost)\r\n" + 
				"FROM\r\n" + 
				"    prescription_details join patient_prescription_medicine2 on patient_prescription_medicine2.prescription_details_id = prescription_details.id\r\n" +
				" where employee_user_name != '' and employee_user_name is not null " +
				" GROUP BY employee_user_name"
				+ " order by SUM(patient_prescription_medicine2.cost) desc";

		return ConnectionAndStatementUtil.getListOfT(sql,this::getUserWiseCountCosts);	
    }
	
	public List<KeyCountDTO> getOtherOfficeCountCosts()
    {
		String sql = "SELECT \r\n" + 
				"    others_office_id,\r\n" + 
				"    COUNT(DISTINCT prescription_details.id),\r\n" + 
				"    SUM(patient_prescription_medicine2.cost)\r\n" + 
				"FROM\r\n" + 
				"    prescription_details join patient_prescription_medicine2 on patient_prescription_medicine2.prescription_details_id = prescription_details.id\r\n" + 
				"WHERE\r\n" + 
				"    others_office_id >= 0\r\n" + 
				"GROUP BY others_office_id"
				+ " order by SUM(patient_prescription_medicine2.cost) desc";

		return ConnectionAndStatementUtil.getListOfT(sql,this::getOtherOfficeCountCosts);	
    }
	
	public KeyCountDTO getOtherOfficeCountCosts(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("others_office_id");
			keyCountDTO.count = rs.getInt("COUNT(DISTINCT prescription_details.id)");
			keyCountDTO.cost = rs.getDouble("SUM(patient_prescription_medicine2.cost)");
			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	
	
	
	public List<KeyCountDTO> getDrPrescriptionCOunt()
    {
		String sql = "select dr_user_name, count(id) from prescription_details where isDeleted = 0 group by dr_user_name order by count(id) desc;";

		return ConnectionAndStatementUtil.getListOfT(sql,this::getDrWiseCount);	
    }
	
	public List<KeyCountDTO> getDrAppointmentPrescriptionCount()
    {
		String sql = "SELECT \r\n" + 
				"    dr_user_name,\r\n" + 
				"    COUNT(id) AS appointments,\r\n" + 
				"    (SELECT \r\n" + 
				"            COUNT(id)\r\n" + 
				"        FROM\r\n" + 
				"            prescription_details\r\n" + 
				"        WHERE\r\n" + 
				"            dr_user_name = appointment.dr_user_name) AS prescriptions\r\n" + 
				"FROM\r\n" + 
				"    appointment where appointment.isCancelled = 0 \r\n" + 
				"GROUP BY dr_user_name";

		return ConnectionAndStatementUtil.getListOfT(sql,this::getDrWiseAPCount);	
    }
	
	public List<KeyCountDTO> getDrAppointmentPrescriptionCountToday()
    {
		String sql = "SELECT \r\n" + 
				"    dr_user_name,\r\n" + 
				"    COUNT(id) AS appointments,\r\n" + 
				"    (SELECT \r\n" + 
				"            COUNT(id)\r\n" + 
				"        FROM\r\n" + 
				"            prescription_details\r\n" + 
				"        WHERE\r\n" + 
				"            dr_user_name = appointment.dr_user_name" +
				"            and visit_date >= " + TimeConverter.getToday() + " \r\n" 
				+ ") AS prescriptions\r\n" + 
				"FROM\r\n" + 
				"    appointment where appointment.isCancelled = 0 \r\n" + 
				"            and visit_date >= " + TimeConverter.getToday() + " \r\n" + 
				"GROUP BY dr_user_name";

		return ConnectionAndStatementUtil.getListOfT(sql,this::getDrWiseAPCount);	
    }
	
	public KeyCountDTO getDrWiseAPCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.keyStr = rs.getString("dr_user_name");
			keyCountDTO.count = rs.getInt("appointments");
			keyCountDTO.count2 = rs.getInt("prescriptions");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public YearMonthCount getYmCount(ResultSet rs)
	{
		try
		{
			YearMonthCount ymCount = new YearMonthCount();
			String ym = rs.getString("ym");
			ymCount.year = Integer.parseInt(ym.split("-")[0]);
			ymCount.month = Integer.parseInt(ym.split("-")[1]);
			ymCount.count = rs.getInt("count(id)");
			
			//System.out.println("year = " + ymCount.year + " month = " + ymCount.month + " count = " + ymCount.count);

			return ymCount;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public List<YearMonthCount> getLast6MonthCount (String userName)
    {
		String sql = "SELECT \r\n" + 
				"    DATE_FORMAT(FROM_UNIXTIME(`visit_date` / 1000),\r\n" + 
				"            '%Y-%m') AS ym,\r\n" + 
				"    COUNT(id)\r\n" + 
				"FROM\r\n" + 
				"    prescription_details\r\n" + 
				"WHERE\r\n" + 
				"    visit_date >= " + TimeConverter.get1stDayOfNthtMonth(-6) + " AND isDeleted = 0 and employee_record_id = " 
				+ WorkflowController.getEmployeeRecordsIdFromUserName(userName) + "\r\n" + 
				"GROUP BY ym\r\n";
		
		//System.out.println(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::getYmCount);	
    }
	
	public long getLastVisitDate (long date, long erId)
    {
		String sql = "select visit_date from " + tableName 
				+ " where isDeleted = 0 and employee_record_id = " + erId
				+ " and visit_date < " + date
				+ " order by visit_date desc limit 1";
		
		//System.out.println(sql);
		return ConnectionAndStatementUtil.getT(sql,this::getVisitDate, -1L);	
    }
	
	public long getVisitDate(ResultSet rs)
	{
		long date = -1;
		try
		{
			date = rs.getLong("visit_date");
			
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		return date;
	}
	
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		if(filter.equals(addLabTestFilter) || filter.equals(pendingTestFilter) || filter.equals(myTestFilter)
				|| filter.equals(radioPendingTestFilter) || filter.equals(radioMyTestFilter)
				|| filter.equals(pathologyHeadPendingFilter) || filter.equals(pathologyHeadApprovedFilter)) //lab test
		{
			logger.debug("addLabTestFilter found in dao ");
			sql += " join prescription_lab_test on prescription_lab_test.prescription_details_id = prescription_details.id ";
		}		
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= "(" + tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'"
						 + "or employee_record_id = " 
						+ WorkflowController.getEmployeeRecordsIdFromUserName(Utils.getDigitEnglishFromBangla(p_searchCriteria.get("AnyField").toString())) + ")";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name")
						|| str.equals("doctor_id")
						|| str.equals("patient_id")
						|| str.equals("visit_date_start")
						|| str.equals("visit_date_end")
						|| str.equals("employee_user_name")
						|| str.equals("sl")
						|| str.equals("phone")
						|| str.equals("medicine_status")
					
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					if(str.equals("name"))
					{
						AllFieldSql += "" + tableName + ".name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("employee_user_name"))
					{
						AllFieldSql += "" + tableName + ".employee_record_id = " + WorkflowController.getEmployeeRecordsIdFromUserName((String)p_searchCriteria.get(str)) ;
						i ++;
					}
					else if(str.equals("visit_date_start"))
					{
						AllFieldSql += "visit_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("visit_date_end"))
					{
						AllFieldSql += "visit_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("doctor_id"))
					{
						AllFieldSql += "doctor_id = " + p_searchCriteria.get(str) + "";
						i ++;
					}
					else if(str.equals("patient_id"))
					{
						AllFieldSql += "employee_record_id =" + WorkflowController.getEmployeeRecordsIdFromUserName((String)p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("sl"))
					{
						AllFieldSql += "" + tableName + ".sl = '" + p_searchCriteria.get(str) + "'";
						i ++;
					}
					else if(str.equals("phone"))
					{
						AllFieldSql += " (" + tableName + ".phone = '" + p_searchCriteria.get(str) + "' ";
						AllFieldSql += " or " + tableName + ".alternate_phone = '" + p_searchCriteria.get(str) + "') ";
						i ++;
					}
					else if(str.equals("medicine_status"))
					{
						int medicine_status = Integer.parseInt((String)p_searchCriteria.get(str));
						int validityHours = Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.PRESCIPTION_VALIDITY_HOURS).value);
						if(medicine_status == DELIVERED)
						{
							AllFieldSql += "" + tableName + ".medicine_added = 1";
						}
						else if(medicine_status == PENDING)
						{
							AllFieldSql += "" + tableName 
									+ ".medicine_added = 0 and (" + System.currentTimeMillis() + " - " + tableName + ".visit_time)/ " + (1000 * 3600) + "<= " + validityHours;
						}
						else
						{
							AllFieldSql += "" + tableName 
									+ ".medicine_added = 0 and (" + System.currentTimeMillis() + " - " + tableName + ".visit_time)/ " + (1000 * 3600) + "> " + validityHours;
						}
						
						
						i ++;
					}
					
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase("") && !filter.equals(myTestFilter) && !filter.equals(radioMyTestFilter))
		{
			sql += " and " + filter + " ";
		}
		else if(filter.equals(myTestFilter) || filter.equals(radioMyTestFilter))
		{
			sql += " and " + filter + TimeConverter.getToday() + " ";
		}
		
		if(!canSeeAllPrescriptions(userDTO))
		{
			sql += " and employee_record_id = " + userDTO.employee_record_id + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
		if((userDTO.roleID == SessionConstants.XRAY_GUY || userDTO.roleID == SessionConstants.RADIOLOGIST) && 
				(filter.equals(addLabTestFilter) || filter.equals(pendingTestFilter) || filter.equals(myTestFilter)
						|| filter.equals(radioPendingTestFilter) || filter.equals(radioMyTestFilter)))
		{
			sql += " AND (lab_test_type = " 
					+ SessionConstants.LAB_TEST_XRAY + " or lab_test_type = " 
					+ SessionConstants.LAB_TEST_ULTRASONOGRAM + ") ";
			if(filter.equals(myTestFilter))
			{
				sql += " and xray_technologist_user_name = '" + userDTO.userName + "'";
			}
			else if(filter.equals(radioMyTestFilter))
			{
				sql += " and tester_id = '" + userDTO.userName + "'";
			}
			if(userDTO.roleID == SessionConstants.RADIOLOGIST)
			{
				sql += " and is_done = 1 ";
			}
			else if(filter.equals(pendingTestFilter))
			{
				sql += " and is_done = 0 ";
			}
			
		}
		else if((userDTO.roleID == SessionConstants.LAB_TECHNITIAN_ROLE || userDTO.roleID == SessionConstants.PATHOLOGY_HEAD) && 
				(
						filter.equals(addLabTestFilter) 
						|| filter.equals(pendingTestFilter) 
						|| filter.equals(myTestFilter)
						|| filter.equalsIgnoreCase(pathologyHeadPendingFilter)
						|| filter.equalsIgnoreCase(pathologyHeadApprovedFilter)
						))
		{
			sql += " AND lab_test_type >= 0 and ("
					+ "lab_test_type != " + SessionConstants.LAB_TEST_XRAY 
					+ " and lab_test_type != " + SessionConstants.LAB_TEST_ULTRASONOGRAM 
					+ " and lab_test_type != " + SessionConstants.LAB_TEST_OTHER 
					+ ") ";
			if(filter.equals(myTestFilter))
			{
				sql += " and xray_technologist_user_name = '" + userDTO.userName + "'";
			}
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	