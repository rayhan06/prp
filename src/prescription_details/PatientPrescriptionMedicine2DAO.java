package prescription_details;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class PatientPrescriptionMedicine2DAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public PatientPrescriptionMedicine2DAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		joinSQL += " join drug_information on " + tableName + ".drug_information_type = drug_information.ID ";
		joinSQL += " join supplied_drug_information on " + tableName + ".supplied_drug_information_type = supplied_drug_information.ID ";

		commonMaps = new PatientPrescriptionMedicine2MAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"prescription_details_id",
			"drug_information_type",
			"dosage_type_cat",
			"dose",
			"dose_unit_cat",
			"frequency",
			"food_instructions_cat",
			"supplied_drug_information_type",
			"dose_sold",
			"seller_id",
			"seller_name",
			"selling_date",
		
			"alternate_drug_name",
			"frequency_cat",
			"quantity",
			"cost",
			"remaining_quantity",
			"dose_supplied_today",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public PatientPrescriptionMedicine2DAO()
	{
		this("patient_prescription_medicine2");		
	}
	
	public void setSearchColumn(PatientPrescriptionMedicine2DTO patientprescriptionmedicine2DTO)
	{
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		PatientPrescriptionMedicine2DTO patientprescriptionmedicine2DTO = (PatientPrescriptionMedicine2DTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		if(isInsert)
		{
			ps.setObject(index++,patientprescriptionmedicine2DTO.iD);
		}
		ps.setObject(index++,patientprescriptionmedicine2DTO.prescriptionDetailsId);
		ps.setObject(index++,patientprescriptionmedicine2DTO.drugInformationType);
		ps.setObject(index++,patientprescriptionmedicine2DTO.dosageTypeCat);
		ps.setObject(index++,patientprescriptionmedicine2DTO.dose);
		ps.setObject(index++,patientprescriptionmedicine2DTO.doseUnitCat);
		ps.setObject(index++,patientprescriptionmedicine2DTO.frequency);
		ps.setObject(index++,patientprescriptionmedicine2DTO.foodInstructionsCat);
		ps.setObject(index++,patientprescriptionmedicine2DTO.suppliedDrugInformationType);
		ps.setObject(index++,patientprescriptionmedicine2DTO.doseSold);
		ps.setObject(index++,patientprescriptionmedicine2DTO.sellerId);
		ps.setObject(index++,patientprescriptionmedicine2DTO.sellerName);
		ps.setObject(index++,patientprescriptionmedicine2DTO.sellingDate);

		ps.setObject(index++,patientprescriptionmedicine2DTO.alternateDrugName);
		ps.setObject(index++,patientprescriptionmedicine2DTO.frequencyCat);
		ps.setObject(index++,patientprescriptionmedicine2DTO.quantity);
		ps.setObject(index++,patientprescriptionmedicine2DTO.cost);
		ps.setObject(index++,patientprescriptionmedicine2DTO.remainingQuantity);
		ps.setObject(index++,patientprescriptionmedicine2DTO.doseSoldToday);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public PatientPrescriptionMedicine2DTO build(ResultSet rs)
	{
		try
		{
			PatientPrescriptionMedicine2DTO patientprescriptionmedicine2DTO = new PatientPrescriptionMedicine2DTO();
			patientprescriptionmedicine2DTO.iD = rs.getLong("ID");
			patientprescriptionmedicine2DTO.prescriptionDetailsId = rs.getLong("prescription_details_id");
			patientprescriptionmedicine2DTO.drugInformationType = rs.getLong("drug_information_type");
			patientprescriptionmedicine2DTO.dosageTypeCat = rs.getInt("dosage_type_cat");
			patientprescriptionmedicine2DTO.dose = rs.getString("dose");
			patientprescriptionmedicine2DTO.doseUnitCat = rs.getInt("dose_unit_cat");
			patientprescriptionmedicine2DTO.frequency = rs.getString("frequency");
			patientprescriptionmedicine2DTO.foodInstructionsCat = rs.getInt("food_instructions_cat");
			patientprescriptionmedicine2DTO.suppliedDrugInformationType = rs.getLong("supplied_drug_information_type");
			patientprescriptionmedicine2DTO.doseSold = rs.getInt("dose_sold");
			patientprescriptionmedicine2DTO.sellerId = rs.getLong("seller_id");
			patientprescriptionmedicine2DTO.sellerName = rs.getString("seller_name");
			patientprescriptionmedicine2DTO.sellingDate = rs.getLong("selling_date");

			patientprescriptionmedicine2DTO.alternateDrugName = rs.getString("alternate_drug_name");
			patientprescriptionmedicine2DTO.frequencyCat = rs.getInt("frequency_cat");
			patientprescriptionmedicine2DTO.quantity = rs.getInt("quantity");
			patientprescriptionmedicine2DTO.cost = rs.getDouble("cost");
			patientprescriptionmedicine2DTO.remainingQuantity = rs.getInt("remaining_quantity");
			patientprescriptionmedicine2DTO.doseSoldToday = rs.getInt("dose_supplied_today");
			patientprescriptionmedicine2DTO.isDeleted = rs.getInt("isDeleted");
			patientprescriptionmedicine2DTO.lastModificationTime = rs.getLong("lastModificationTime");
			return patientprescriptionmedicine2DTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	
	public List<PatientPrescriptionMedicine2DTO> getPatientPrescriptionMedicine2DTOListByPrescriptionDetailsID(long prescriptionDetailsID) throws Exception
	{
		String sql = "SELECT * FROM patient_prescription_medicine2 where isDeleted=0 and prescription_details_id="+prescriptionDetailsID+" order by patient_prescription_medicine2.lastModificationTime";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}

	public PatientPrescriptionMedicine2DTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		PatientPrescriptionMedicine2DTO patientprescriptionmedicine2DTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return patientprescriptionmedicine2DTO;
	}
	
	public PatientPrescriptionMedicine2DTO getLastDrugReceived (long erId, long whoIsThePatientCat, long drugInformationId)
	{
		String sql = "SELECT \r\n" + 
				"    visit_time, quantity\r\n" + 
				"FROM\r\n" + 
				"    patient_prescription_medicine2\r\n" + 
				"        JOIN\r\n" + 
				"    prescription_details ON prescription_details.id = patient_prescription_medicine2.prescription_details_id\r\n" + 
				"    where \r\n" + 
				"        prescription_details.employee_record_id = " + erId + "\r\n" + 
				"        AND who_is_the_patient_cat = " + whoIsThePatientCat + "\r\n" + 
				"        AND drug_information_type = " + drugInformationId + "\r\n" + 
				"        AND prescription_details.isDeleted = 0\r\n" + 
				"        and prescription_details.id = patient_prescription_medicine2.prescription_details_id\r\n" + 
				"        AND patient_prescription_medicine2.isDeleted = 0\r\n" + 
				"ORDER BY visit_time DESC\r\n" + 
				"LIMIT 1";
		PatientPrescriptionMedicine2DTO patientprescriptionmedicine2DTO = ConnectionAndStatementUtil.getT(sql,this::getLastDrugReceived, new PatientPrescriptionMedicine2DTO());
		return patientprescriptionmedicine2DTO;
	}
	
	
	public PatientPrescriptionMedicine2DTO getLastDrugReceived(ResultSet rs)
	{
		try
		{
			SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
			PatientPrescriptionMedicine2DTO patientprescriptionmedicine2DTO = new PatientPrescriptionMedicine2DTO();
			patientprescriptionmedicine2DTO.visitTime = rs.getLong("visit_time");
			patientprescriptionmedicine2DTO.quantity = rs.getInt("quantity");
			patientprescriptionmedicine2DTO.formattedVisitTime = f.format(new Date(patientprescriptionmedicine2DTO.visitTime));
			return patientprescriptionmedicine2DTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	

	
	public List<PatientPrescriptionMedicine2DTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<PatientPrescriptionMedicine2DTO> getAllPatientPrescriptionMedicine2 (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<PatientPrescriptionMedicine2DTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<PatientPrescriptionMedicine2DTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
				
}
	