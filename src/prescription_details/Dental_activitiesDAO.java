package prescription_details;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;

import appointment.KeyCountDTO;
import util.*;
import pb.*;

public class Dental_activitiesDAO  implements CommonDAOService<Dental_activitiesDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Dental_activitiesDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"prescription_details_id",
			"dental_action_cat",
			"doctor_employee_record_id",
			"dt_organogram_id",
			"dt_user_id",
			"dt_er_id",
			"doctor_remarks",
			"dt_remarks",
			"is_done",
			"prescription_date",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("dental_action_cat"," and (dental_action_cat = ?)");
		searchMap.put("doctor_remarks"," and (doctor_remarks like ?)");
		searchMap.put("dt_remarks"," and (dt_remarks like ?)");
		searchMap.put("prescription_date_start"," and (prescription_date >= ?)");
		searchMap.put("prescription_date_end"," and (prescription_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Dental_activitiesDAO INSTANCE = new Dental_activitiesDAO();
	}

	public static Dental_activitiesDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Dental_activitiesDTO dental_activitiesDTO)
	{
		dental_activitiesDTO.searchColumn = "";
		dental_activitiesDTO.searchColumn += CatDAO.getName("English", "dental_action", dental_activitiesDTO.dentalActionCat) + " " + CatDAO.getName("Bangla", "dental_action", dental_activitiesDTO.dentalActionCat) + " ";
		dental_activitiesDTO.searchColumn += dental_activitiesDTO.doctorRemarks + " ";
		dental_activitiesDTO.searchColumn += dental_activitiesDTO.dtRemarks + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Dental_activitiesDTO dental_activitiesDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(dental_activitiesDTO);
		if(isInsert)
		{
			ps.setObject(++index,dental_activitiesDTO.iD);
		}
		ps.setObject(++index,dental_activitiesDTO.prescriptionDetailsId);
		ps.setObject(++index,dental_activitiesDTO.dentalActionCat);
		ps.setObject(++index,dental_activitiesDTO.doctorEmployeeRecordId);
		ps.setObject(++index,dental_activitiesDTO.dtOrganogramId);
		ps.setObject(++index,dental_activitiesDTO.dtUserId);
		ps.setObject(++index,dental_activitiesDTO.dtErId);
		ps.setObject(++index,dental_activitiesDTO.doctorRemarks);
		ps.setObject(++index,dental_activitiesDTO.dtRemarks);
		ps.setObject(++index,dental_activitiesDTO.isDone);
		ps.setObject(++index,dental_activitiesDTO.prescriptionDate);
		if(isInsert)
		{
			ps.setObject(++index,dental_activitiesDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,dental_activitiesDTO.iD);
		}
	}
	
	@Override
	public Dental_activitiesDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Dental_activitiesDTO dental_activitiesDTO = new Dental_activitiesDTO();
			int i = 0;
			dental_activitiesDTO.iD = rs.getLong(columnNames[i++]);
			dental_activitiesDTO.prescriptionDetailsId = rs.getLong(columnNames[i++]);
			dental_activitiesDTO.dentalActionCat = rs.getInt(columnNames[i++]);
			dental_activitiesDTO.doctorEmployeeRecordId = rs.getLong(columnNames[i++]);
			dental_activitiesDTO.dtOrganogramId = rs.getLong(columnNames[i++]);
			dental_activitiesDTO.dtUserId = rs.getLong(columnNames[i++]);
			dental_activitiesDTO.dtErId = rs.getLong(columnNames[i++]);
			dental_activitiesDTO.doctorRemarks = rs.getString(columnNames[i++]);
			dental_activitiesDTO.dtRemarks = rs.getString(columnNames[i++]);
			dental_activitiesDTO.isDone = rs.getBoolean(columnNames[i++]);
			dental_activitiesDTO.prescriptionDate = rs.getLong(columnNames[i++]);
			dental_activitiesDTO.isDeleted = rs.getInt(columnNames[i++]);
			dental_activitiesDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return dental_activitiesDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Dental_activitiesDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "dental_activities";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Dental_activitiesDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Dental_activitiesDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
	
	public List<Dental_activitiesDTO> getByPrescriptionId(long prescriptionDetailsID) throws Exception
	{
		String sql = "SELECT * FROM " + getTableName() 
		+ " where isDeleted=0 and prescription_details_id="+ prescriptionDetailsID + 
		" order by id asc ";
		return ConnectionAndStatementUtil.getListOfT(sql,this::buildObjectFromResultSet);		
	}
	
	public List<KeyCountDTO> getCatWiseCount(long startDate)
    {
		String sql = "select dental_action_cat, count(id) from dental_activities where isDeleted = 0 ";
		if(startDate >= 0)
		{
			sql += " and prescription_date >= " + startDate;
		}
		sql += " group by dental_action_cat order by count(id) desc;";

		return ConnectionAndStatementUtil.getListOfT(sql,this::getCatWiseCount);	
    }
	
	public KeyCountDTO getCatWiseCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("dental_action_cat");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
				
}
	