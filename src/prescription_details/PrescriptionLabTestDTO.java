package prescription_details;
import java.util.*; 
import util.*; 


public class PrescriptionLabTestDTO extends CommonDTO
{
	public static final int PENDING = 0;
	public static final int APPROVED = 1;
	public static final int REJECTED = 2;

	public long prescriptionDetailsId = 0;
	public long labTestType = -1;
    public String labTestDetailsTypes = "";
    public int testStatus = 0;
    public int approvalStatus = PENDING;
    public long approvalDate = 0;
    public String approverUserName = "";
    public long approverOrganogramId = -1;
    public String testerId = "";
	public long testingDate = TimeConverter.getToday();
	public long actualDeliveryDate = 0;
    public long scheduledDeliveryDate = TimeConverter.getToday();
    
    public boolean isDone = false;
    public String xRayPlateType = "-1";
    public long xRayReportId = -1;
    public String plateQuantity = "0";
    public String plateWasted = "";
    
    public int [] plateQuantityArray;
    public int [] plateWastedArray;
    public long [] plateTypesArray;
    
    public String xRayUserName = "";

    public String description = "";
    public String alternateLabTestName = "";
    public String alternateLabTestDetails= "";
    
    public int plateUsed1 = 0;
    public int plateUsed2 = 0;
    public int plateUsed3 = 0;
    
    public int plateWasted1 = 0;
    public int plateWasted2 = 0;
    public int plateWasted3 = 0;
    
    public static final long ALL = -3;
    public static final long OTHER = -4;
	
	
    @Override
	public String toString() {
            return "$PrescriptionLabTestDTO[" +
            " iD = " + iD +
            " prescriptionDetailsId = " + prescriptionDetailsId +
            " labTestType = " + labTestType +
            " labTestDetailsTypes = " + labTestDetailsTypes +
            " testStatus = " + testStatus +
            " testerId = " + testerId +
            " testingDate = " + testingDate +
            " scheduledDeliveryDate = " + scheduledDeliveryDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}