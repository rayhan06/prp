package prescription_details;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Prescription_detailsRepository implements Repository {
	Prescription_detailsDAO prescription_detailsDAO = null;
	
	public void setDAO(Prescription_detailsDAO prescription_detailsDAO)
	{
		this.prescription_detailsDAO = prescription_detailsDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Prescription_detailsRepository.class);
	Map<Long, Prescription_detailsDTO>mapOfPrescription_detailsDTOToiD;
	Map<Long, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOToappointmentId;
	Map<String, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOToname;
	Map<Long, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOTodateOfBirth;
	Map<Long, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOToage;
	Map<Double, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOToheight;
	Map<Double, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOToweight;
	Map<Double, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOTobloodPressureDiastole;
	Map<String, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOTodiagnosis;
	Map<String, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOTochiefComplaints;
	Map<String, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOToclinicalFeatures;
	Map<String, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOToadvice;
	Map<String, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOToremarks;
	Map<Boolean, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOTomedicineAdded;
	Map<Double, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOTobloodPressureSystole;
	Map<Double, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOTopulse;
	Map<Double, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOTotemperature;
	Map<Double, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOTooxygenSaturation;
	Map<Double, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOTobmi;
	Map<String, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOTobloodGroup;
	Map<String, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOTosearchColumn;
	Map<Long, Set<Prescription_detailsDTO> >mapOfPrescription_detailsDTOTolastModificationTime;


	static Prescription_detailsRepository instance = null;  
	private Prescription_detailsRepository(){
		mapOfPrescription_detailsDTOToiD = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOToappointmentId = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOToname = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOTodateOfBirth = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOToage = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOToheight = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOToweight = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOTobloodPressureDiastole = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOTodiagnosis = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOTochiefComplaints = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOToclinicalFeatures = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOToadvice = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOToremarks = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOTomedicineAdded = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOTobloodPressureSystole = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOTopulse = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOTotemperature = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOTooxygenSaturation = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOTobmi = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOTobloodGroup = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfPrescription_detailsDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Prescription_detailsRepository getInstance(){
		if (instance == null){
			instance = new Prescription_detailsRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(prescription_detailsDAO == null)
		{
			return;
		}
		try {
			List<Prescription_detailsDTO> prescription_detailsDTOs = prescription_detailsDAO.getAllPrescription_details(reloadAll);
			for(Prescription_detailsDTO prescription_detailsDTO : prescription_detailsDTOs) {
				Prescription_detailsDTO oldPrescription_detailsDTO = getPrescription_detailsDTOByID(prescription_detailsDTO.iD);
				if( oldPrescription_detailsDTO != null ) {
					mapOfPrescription_detailsDTOToiD.remove(oldPrescription_detailsDTO.iD);
				
					if(mapOfPrescription_detailsDTOToappointmentId.containsKey(oldPrescription_detailsDTO.appointmentId)) {
						mapOfPrescription_detailsDTOToappointmentId.get(oldPrescription_detailsDTO.appointmentId).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOToappointmentId.get(oldPrescription_detailsDTO.appointmentId).isEmpty()) {
						mapOfPrescription_detailsDTOToappointmentId.remove(oldPrescription_detailsDTO.appointmentId);
					}
					
					if(mapOfPrescription_detailsDTOToname.containsKey(oldPrescription_detailsDTO.name)) {
						mapOfPrescription_detailsDTOToname.get(oldPrescription_detailsDTO.name).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOToname.get(oldPrescription_detailsDTO.name).isEmpty()) {
						mapOfPrescription_detailsDTOToname.remove(oldPrescription_detailsDTO.name);
					}
					
					if(mapOfPrescription_detailsDTOTodateOfBirth.containsKey(oldPrescription_detailsDTO.dateOfBirth)) {
						mapOfPrescription_detailsDTOTodateOfBirth.get(oldPrescription_detailsDTO.dateOfBirth).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOTodateOfBirth.get(oldPrescription_detailsDTO.dateOfBirth).isEmpty()) {
						mapOfPrescription_detailsDTOTodateOfBirth.remove(oldPrescription_detailsDTO.dateOfBirth);
					}
					
					if(mapOfPrescription_detailsDTOToage.containsKey(oldPrescription_detailsDTO.age)) {
						mapOfPrescription_detailsDTOToage.get(oldPrescription_detailsDTO.age).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOToage.get(oldPrescription_detailsDTO.age).isEmpty()) {
						mapOfPrescription_detailsDTOToage.remove(oldPrescription_detailsDTO.age);
					}
					
					if(mapOfPrescription_detailsDTOToheight.containsKey(oldPrescription_detailsDTO.height)) {
						mapOfPrescription_detailsDTOToheight.get(oldPrescription_detailsDTO.height).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOToheight.get(oldPrescription_detailsDTO.height).isEmpty()) {
						mapOfPrescription_detailsDTOToheight.remove(oldPrescription_detailsDTO.height);
					}
					
					if(mapOfPrescription_detailsDTOToweight.containsKey(oldPrescription_detailsDTO.weight)) {
						mapOfPrescription_detailsDTOToweight.get(oldPrescription_detailsDTO.weight).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOToweight.get(oldPrescription_detailsDTO.weight).isEmpty()) {
						mapOfPrescription_detailsDTOToweight.remove(oldPrescription_detailsDTO.weight);
					}
					
					if(mapOfPrescription_detailsDTOTobloodPressureDiastole.containsKey(oldPrescription_detailsDTO.bloodPressureDiastole)) {
						mapOfPrescription_detailsDTOTobloodPressureDiastole.get(oldPrescription_detailsDTO.bloodPressureDiastole).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOTobloodPressureDiastole.get(oldPrescription_detailsDTO.bloodPressureDiastole).isEmpty()) {
						mapOfPrescription_detailsDTOTobloodPressureDiastole.remove(oldPrescription_detailsDTO.bloodPressureDiastole);
					}
					
					if(mapOfPrescription_detailsDTOTodiagnosis.containsKey(oldPrescription_detailsDTO.diagnosis)) {
						mapOfPrescription_detailsDTOTodiagnosis.get(oldPrescription_detailsDTO.diagnosis).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOTodiagnosis.get(oldPrescription_detailsDTO.diagnosis).isEmpty()) {
						mapOfPrescription_detailsDTOTodiagnosis.remove(oldPrescription_detailsDTO.diagnosis);
					}
					
					if(mapOfPrescription_detailsDTOTochiefComplaints.containsKey(oldPrescription_detailsDTO.chiefComplaints)) {
						mapOfPrescription_detailsDTOTochiefComplaints.get(oldPrescription_detailsDTO.chiefComplaints).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOTochiefComplaints.get(oldPrescription_detailsDTO.chiefComplaints).isEmpty()) {
						mapOfPrescription_detailsDTOTochiefComplaints.remove(oldPrescription_detailsDTO.chiefComplaints);
					}
					
					if(mapOfPrescription_detailsDTOToclinicalFeatures.containsKey(oldPrescription_detailsDTO.clinicalFeatures)) {
						mapOfPrescription_detailsDTOToclinicalFeatures.get(oldPrescription_detailsDTO.clinicalFeatures).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOToclinicalFeatures.get(oldPrescription_detailsDTO.clinicalFeatures).isEmpty()) {
						mapOfPrescription_detailsDTOToclinicalFeatures.remove(oldPrescription_detailsDTO.clinicalFeatures);
					}
					
					if(mapOfPrescription_detailsDTOToadvice.containsKey(oldPrescription_detailsDTO.advice)) {
						mapOfPrescription_detailsDTOToadvice.get(oldPrescription_detailsDTO.advice).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOToadvice.get(oldPrescription_detailsDTO.advice).isEmpty()) {
						mapOfPrescription_detailsDTOToadvice.remove(oldPrescription_detailsDTO.advice);
					}
					
					if(mapOfPrescription_detailsDTOToremarks.containsKey(oldPrescription_detailsDTO.remarks)) {
						mapOfPrescription_detailsDTOToremarks.get(oldPrescription_detailsDTO.remarks).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOToremarks.get(oldPrescription_detailsDTO.remarks).isEmpty()) {
						mapOfPrescription_detailsDTOToremarks.remove(oldPrescription_detailsDTO.remarks);
					}
					
					if(mapOfPrescription_detailsDTOTomedicineAdded.containsKey(oldPrescription_detailsDTO.medicineAdded)) {
						mapOfPrescription_detailsDTOTomedicineAdded.get(oldPrescription_detailsDTO.medicineAdded).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOTomedicineAdded.get(oldPrescription_detailsDTO.medicineAdded).isEmpty()) {
						mapOfPrescription_detailsDTOTomedicineAdded.remove(oldPrescription_detailsDTO.medicineAdded);
					}
					
					if(mapOfPrescription_detailsDTOTobloodPressureSystole.containsKey(oldPrescription_detailsDTO.bloodPressureSystole)) {
						mapOfPrescription_detailsDTOTobloodPressureSystole.get(oldPrescription_detailsDTO.bloodPressureSystole).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOTobloodPressureSystole.get(oldPrescription_detailsDTO.bloodPressureSystole).isEmpty()) {
						mapOfPrescription_detailsDTOTobloodPressureSystole.remove(oldPrescription_detailsDTO.bloodPressureSystole);
					}
					
					if(mapOfPrescription_detailsDTOTopulse.containsKey(oldPrescription_detailsDTO.pulse)) {
						mapOfPrescription_detailsDTOTopulse.get(oldPrescription_detailsDTO.pulse).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOTopulse.get(oldPrescription_detailsDTO.pulse).isEmpty()) {
						mapOfPrescription_detailsDTOTopulse.remove(oldPrescription_detailsDTO.pulse);
					}
					
					if(mapOfPrescription_detailsDTOTotemperature.containsKey(oldPrescription_detailsDTO.temperature)) {
						mapOfPrescription_detailsDTOTotemperature.get(oldPrescription_detailsDTO.temperature).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOTotemperature.get(oldPrescription_detailsDTO.temperature).isEmpty()) {
						mapOfPrescription_detailsDTOTotemperature.remove(oldPrescription_detailsDTO.temperature);
					}
					
					if(mapOfPrescription_detailsDTOTooxygenSaturation.containsKey(oldPrescription_detailsDTO.oxygenSaturation)) {
						mapOfPrescription_detailsDTOTooxygenSaturation.get(oldPrescription_detailsDTO.oxygenSaturation).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOTooxygenSaturation.get(oldPrescription_detailsDTO.oxygenSaturation).isEmpty()) {
						mapOfPrescription_detailsDTOTooxygenSaturation.remove(oldPrescription_detailsDTO.oxygenSaturation);
					}
					
					if(mapOfPrescription_detailsDTOTobmi.containsKey(oldPrescription_detailsDTO.bmi)) {
						mapOfPrescription_detailsDTOTobmi.get(oldPrescription_detailsDTO.bmi).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOTobmi.get(oldPrescription_detailsDTO.bmi).isEmpty()) {
						mapOfPrescription_detailsDTOTobmi.remove(oldPrescription_detailsDTO.bmi);
					}
					
					if(mapOfPrescription_detailsDTOTobloodGroup.containsKey(oldPrescription_detailsDTO.bloodGroup)) {
						mapOfPrescription_detailsDTOTobloodGroup.get(oldPrescription_detailsDTO.bloodGroup).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOTobloodGroup.get(oldPrescription_detailsDTO.bloodGroup).isEmpty()) {
						mapOfPrescription_detailsDTOTobloodGroup.remove(oldPrescription_detailsDTO.bloodGroup);
					}
					
					if(mapOfPrescription_detailsDTOTosearchColumn.containsKey(oldPrescription_detailsDTO.searchColumn)) {
						mapOfPrescription_detailsDTOTosearchColumn.get(oldPrescription_detailsDTO.searchColumn).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOTosearchColumn.get(oldPrescription_detailsDTO.searchColumn).isEmpty()) {
						mapOfPrescription_detailsDTOTosearchColumn.remove(oldPrescription_detailsDTO.searchColumn);
					}
					
					if(mapOfPrescription_detailsDTOTolastModificationTime.containsKey(oldPrescription_detailsDTO.lastModificationTime)) {
						mapOfPrescription_detailsDTOTolastModificationTime.get(oldPrescription_detailsDTO.lastModificationTime).remove(oldPrescription_detailsDTO);
					}
					if(mapOfPrescription_detailsDTOTolastModificationTime.get(oldPrescription_detailsDTO.lastModificationTime).isEmpty()) {
						mapOfPrescription_detailsDTOTolastModificationTime.remove(oldPrescription_detailsDTO.lastModificationTime);
					}
					
					
				}
				if(prescription_detailsDTO.isDeleted == 0) 
				{
					
					mapOfPrescription_detailsDTOToiD.put(prescription_detailsDTO.iD, prescription_detailsDTO);
				
					if( ! mapOfPrescription_detailsDTOToappointmentId.containsKey(prescription_detailsDTO.appointmentId)) {
						mapOfPrescription_detailsDTOToappointmentId.put(prescription_detailsDTO.appointmentId, new HashSet<>());
					}
					mapOfPrescription_detailsDTOToappointmentId.get(prescription_detailsDTO.appointmentId).add(prescription_detailsDTO);
					
					if( ! mapOfPrescription_detailsDTOToname.containsKey(prescription_detailsDTO.name)) {
						mapOfPrescription_detailsDTOToname.put(prescription_detailsDTO.name, new HashSet<>());
					}
					mapOfPrescription_detailsDTOToname.get(prescription_detailsDTO.name).add(prescription_detailsDTO);
					
					if( ! mapOfPrescription_detailsDTOTodateOfBirth.containsKey(prescription_detailsDTO.dateOfBirth)) {
						mapOfPrescription_detailsDTOTodateOfBirth.put(prescription_detailsDTO.dateOfBirth, new HashSet<>());
					}
					mapOfPrescription_detailsDTOTodateOfBirth.get(prescription_detailsDTO.dateOfBirth).add(prescription_detailsDTO);
					
					if( ! mapOfPrescription_detailsDTOToage.containsKey(prescription_detailsDTO.age)) {
						mapOfPrescription_detailsDTOToage.put(prescription_detailsDTO.age, new HashSet<>());
					}
					mapOfPrescription_detailsDTOToage.get(prescription_detailsDTO.age).add(prescription_detailsDTO);
					
					if( ! mapOfPrescription_detailsDTOToheight.containsKey(prescription_detailsDTO.height)) {
						mapOfPrescription_detailsDTOToheight.put(prescription_detailsDTO.height, new HashSet<>());
					}
					mapOfPrescription_detailsDTOToheight.get(prescription_detailsDTO.height).add(prescription_detailsDTO);
					
					if( ! mapOfPrescription_detailsDTOToweight.containsKey(prescription_detailsDTO.weight)) {
						mapOfPrescription_detailsDTOToweight.put(prescription_detailsDTO.weight, new HashSet<>());
					}
					mapOfPrescription_detailsDTOToweight.get(prescription_detailsDTO.weight).add(prescription_detailsDTO);
					
					if( ! mapOfPrescription_detailsDTOTobloodPressureDiastole.containsKey(prescription_detailsDTO.bloodPressureDiastole)) {
						mapOfPrescription_detailsDTOTobloodPressureDiastole.put(prescription_detailsDTO.bloodPressureDiastole, new HashSet<>());
					}
					mapOfPrescription_detailsDTOTobloodPressureDiastole.get(prescription_detailsDTO.bloodPressureDiastole).add(prescription_detailsDTO);
					
					if( ! mapOfPrescription_detailsDTOTodiagnosis.containsKey(prescription_detailsDTO.diagnosis)) {
						mapOfPrescription_detailsDTOTodiagnosis.put(prescription_detailsDTO.diagnosis, new HashSet<>());
					}
					mapOfPrescription_detailsDTOTodiagnosis.get(prescription_detailsDTO.diagnosis).add(prescription_detailsDTO);
					
					if( ! mapOfPrescription_detailsDTOTochiefComplaints.containsKey(prescription_detailsDTO.chiefComplaints)) {
						mapOfPrescription_detailsDTOTochiefComplaints.put(prescription_detailsDTO.chiefComplaints, new HashSet<>());
					}
					mapOfPrescription_detailsDTOTochiefComplaints.get(prescription_detailsDTO.chiefComplaints).add(prescription_detailsDTO);
					
					if( ! mapOfPrescription_detailsDTOToclinicalFeatures.containsKey(prescription_detailsDTO.clinicalFeatures)) {
						mapOfPrescription_detailsDTOToclinicalFeatures.put(prescription_detailsDTO.clinicalFeatures, new HashSet<>());
					}
					mapOfPrescription_detailsDTOToclinicalFeatures.get(prescription_detailsDTO.clinicalFeatures).add(prescription_detailsDTO);
					
					if( ! mapOfPrescription_detailsDTOToadvice.containsKey(prescription_detailsDTO.advice)) {
						mapOfPrescription_detailsDTOToadvice.put(prescription_detailsDTO.advice, new HashSet<>());
					}
					mapOfPrescription_detailsDTOToadvice.get(prescription_detailsDTO.advice).add(prescription_detailsDTO);
					
					if( ! mapOfPrescription_detailsDTOToremarks.containsKey(prescription_detailsDTO.remarks)) {
						mapOfPrescription_detailsDTOToremarks.put(prescription_detailsDTO.remarks, new HashSet<>());
					}
					mapOfPrescription_detailsDTOToremarks.get(prescription_detailsDTO.remarks).add(prescription_detailsDTO);
					
					if( ! mapOfPrescription_detailsDTOTomedicineAdded.containsKey(prescription_detailsDTO.medicineAdded)) {
						mapOfPrescription_detailsDTOTomedicineAdded.put(prescription_detailsDTO.medicineAdded, new HashSet<>());
					}
					mapOfPrescription_detailsDTOTomedicineAdded.get(prescription_detailsDTO.medicineAdded).add(prescription_detailsDTO);
					
					if( ! mapOfPrescription_detailsDTOTobloodPressureSystole.containsKey(prescription_detailsDTO.bloodPressureSystole)) {
						mapOfPrescription_detailsDTOTobloodPressureSystole.put(prescription_detailsDTO.bloodPressureSystole, new HashSet<>());
					}
					mapOfPrescription_detailsDTOTobloodPressureSystole.get(prescription_detailsDTO.bloodPressureSystole).add(prescription_detailsDTO);
					
					if( ! mapOfPrescription_detailsDTOTopulse.containsKey(prescription_detailsDTO.pulse)) {
						mapOfPrescription_detailsDTOTopulse.put(prescription_detailsDTO.pulse, new HashSet<>());
					}
					mapOfPrescription_detailsDTOTopulse.get(prescription_detailsDTO.pulse).add(prescription_detailsDTO);
					
					if( ! mapOfPrescription_detailsDTOTotemperature.containsKey(prescription_detailsDTO.temperature)) {
						mapOfPrescription_detailsDTOTotemperature.put(prescription_detailsDTO.temperature, new HashSet<>());
					}
					mapOfPrescription_detailsDTOTotemperature.get(prescription_detailsDTO.temperature).add(prescription_detailsDTO);
					
					if( ! mapOfPrescription_detailsDTOTooxygenSaturation.containsKey(prescription_detailsDTO.oxygenSaturation)) {
						mapOfPrescription_detailsDTOTooxygenSaturation.put(prescription_detailsDTO.oxygenSaturation, new HashSet<>());
					}
					mapOfPrescription_detailsDTOTooxygenSaturation.get(prescription_detailsDTO.oxygenSaturation).add(prescription_detailsDTO);
					
					if( ! mapOfPrescription_detailsDTOTobmi.containsKey(prescription_detailsDTO.bmi)) {
						mapOfPrescription_detailsDTOTobmi.put(prescription_detailsDTO.bmi, new HashSet<>());
					}
					mapOfPrescription_detailsDTOTobmi.get(prescription_detailsDTO.bmi).add(prescription_detailsDTO);
					
					if( ! mapOfPrescription_detailsDTOTobloodGroup.containsKey(prescription_detailsDTO.bloodGroup)) {
						mapOfPrescription_detailsDTOTobloodGroup.put(prescription_detailsDTO.bloodGroup, new HashSet<>());
					}
					mapOfPrescription_detailsDTOTobloodGroup.get(prescription_detailsDTO.bloodGroup).add(prescription_detailsDTO);
					
					if( ! mapOfPrescription_detailsDTOTosearchColumn.containsKey(prescription_detailsDTO.searchColumn)) {
						mapOfPrescription_detailsDTOTosearchColumn.put(prescription_detailsDTO.searchColumn, new HashSet<>());
					}
					mapOfPrescription_detailsDTOTosearchColumn.get(prescription_detailsDTO.searchColumn).add(prescription_detailsDTO);
					
					if( ! mapOfPrescription_detailsDTOTolastModificationTime.containsKey(prescription_detailsDTO.lastModificationTime)) {
						mapOfPrescription_detailsDTOTolastModificationTime.put(prescription_detailsDTO.lastModificationTime, new HashSet<>());
					}
					mapOfPrescription_detailsDTOTolastModificationTime.get(prescription_detailsDTO.lastModificationTime).add(prescription_detailsDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Prescription_detailsDTO> getPrescription_detailsList() {
		List <Prescription_detailsDTO> prescription_detailss = new ArrayList<Prescription_detailsDTO>(this.mapOfPrescription_detailsDTOToiD.values());
		return prescription_detailss;
	}
	
	
	public Prescription_detailsDTO getPrescription_detailsDTOByID( long ID){
		return mapOfPrescription_detailsDTOToiD.get(ID);
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOByappointment_id(long appointment_id) {
		return new ArrayList<>( mapOfPrescription_detailsDTOToappointmentId.getOrDefault(appointment_id,new HashSet<>()));
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOByname(String name) {
		return new ArrayList<>( mapOfPrescription_detailsDTOToname.getOrDefault(name,new HashSet<>()));
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOBydate_of_birth(long date_of_birth) {
		return new ArrayList<>( mapOfPrescription_detailsDTOTodateOfBirth.getOrDefault(date_of_birth,new HashSet<>()));
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOByage(long age) {
		return new ArrayList<>( mapOfPrescription_detailsDTOToage.getOrDefault(age,new HashSet<>()));
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOByheight(double height) {
		return new ArrayList<>( mapOfPrescription_detailsDTOToheight.getOrDefault(height,new HashSet<>()));
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOByweight(double weight) {
		return new ArrayList<>( mapOfPrescription_detailsDTOToweight.getOrDefault(weight,new HashSet<>()));
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOByblood_pressure_diastole(double blood_pressure_diastole) {
		return new ArrayList<>( mapOfPrescription_detailsDTOTobloodPressureDiastole.getOrDefault(blood_pressure_diastole,new HashSet<>()));
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOBydiagnosis(String diagnosis) {
		return new ArrayList<>( mapOfPrescription_detailsDTOTodiagnosis.getOrDefault(diagnosis,new HashSet<>()));
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOBychief_complaints(String chief_complaints) {
		return new ArrayList<>( mapOfPrescription_detailsDTOTochiefComplaints.getOrDefault(chief_complaints,new HashSet<>()));
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOByclinical_features(String clinical_features) {
		return new ArrayList<>( mapOfPrescription_detailsDTOToclinicalFeatures.getOrDefault(clinical_features,new HashSet<>()));
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOByadvice(String advice) {
		return new ArrayList<>( mapOfPrescription_detailsDTOToadvice.getOrDefault(advice,new HashSet<>()));
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOByremarks(String remarks) {
		return new ArrayList<>( mapOfPrescription_detailsDTOToremarks.getOrDefault(remarks,new HashSet<>()));
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOBymedicine_added(boolean medicine_added) {
		return new ArrayList<>( mapOfPrescription_detailsDTOTomedicineAdded.getOrDefault(medicine_added,new HashSet<>()));
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOByblood_pressure_systole(double blood_pressure_systole) {
		return new ArrayList<>( mapOfPrescription_detailsDTOTobloodPressureSystole.getOrDefault(blood_pressure_systole,new HashSet<>()));
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOBypulse(double pulse) {
		return new ArrayList<>( mapOfPrescription_detailsDTOTopulse.getOrDefault(pulse,new HashSet<>()));
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOBytemperature(double temperature) {
		return new ArrayList<>( mapOfPrescription_detailsDTOTotemperature.getOrDefault(temperature,new HashSet<>()));
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOByoxygen_saturation(double oxygen_saturation) {
		return new ArrayList<>( mapOfPrescription_detailsDTOTooxygenSaturation.getOrDefault(oxygen_saturation,new HashSet<>()));
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOBybmi(double bmi) {
		return new ArrayList<>( mapOfPrescription_detailsDTOTobmi.getOrDefault(bmi,new HashSet<>()));
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOByblood_group(String blood_group) {
		return new ArrayList<>( mapOfPrescription_detailsDTOTobloodGroup.getOrDefault(blood_group,new HashSet<>()));
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOBysearch_column(String search_column) {
		return new ArrayList<>( mapOfPrescription_detailsDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
	}
	
	
	public List<Prescription_detailsDTO> getPrescription_detailsDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfPrescription_detailsDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "prescription_details";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


