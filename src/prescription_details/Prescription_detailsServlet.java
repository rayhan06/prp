package prescription_details;

import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import medical_inventory_lot.MedicalInventoryInDAO;
import medical_inventory_lot.MedicalInventoryInDTO;
import medical_inventory_out.Medical_inventory_outDAO;
import medical_inventory_out.Medical_inventory_outDTO;
import permission.MenuConstants;
import restriction_config.RestrictionConfigDetailsDTO;
import restriction_config.Restriction_configRepository;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;
import util.TimeConverter;
import workflow.WorkflowController;

import java.util.*;


import javax.servlet.http.*;

import lab_test.LabTestListDAO;
import lab_test.LabTestListDTO;
import language.LC;
import language.LM;

import com.google.gson.Gson;

import appointment.AppointmentDAO;
import appointment.AppointmentDTO;
import borrow_medicine.PersonalStockDAO;
import borrow_medicine.PersonalStockDTO;
import common_lab_report.Common_lab_reportDAO;
import config.GlobalConfigConstants;
import config.GlobalConfigurationRepository;
import drug_information.Drug_informationDAO;
import drug_information.Drug_informationDTO;
import drug_information.Drug_informationRepository;
import family.FamilyDTO;

import pb.*;
import pb_notifications.Pb_notificationsDAO;
import user.*;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Prescription_detailsServlet
 */
@WebServlet("/Prescription_detailsServlet")
@MultipartConfig
public class Prescription_detailsServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Prescription_detailsServlet.class);

    String tableName = "prescription_details";

	Prescription_detailsDAO prescription_detailsDAO;
	CommonRequestHandler commonRequestHandler;
	PatientPrescriptionMedicine2DAO patientPrescriptionMedicine2DAO;
	PrescriptionLabTestDAO prescriptionLabTestDAO;
	AppointmentDAO appointmentDAO = new AppointmentDAO();
	Medical_inventory_outDAO medical_inventory_outDAO = new Medical_inventory_outDAO();
	MedicalInventoryInDAO medicalInventoryInDAO = new MedicalInventoryInDAO();
    private final Gson gson = new Gson();
    UserDAO userDAO = new UserDAO();
    Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
    Dental_activitiesDAO dental_activitiesDAO = Dental_activitiesDAO.getInstance();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Prescription_detailsServlet() 
	{
        super();
    	try
    	{
			prescription_detailsDAO = new Prescription_detailsDAO(tableName);
			patientPrescriptionMedicine2DAO = new PatientPrescriptionMedicine2DAO("patient_prescription_medicine2");
			prescriptionLabTestDAO = new PrescriptionLabTestDAO("prescription_lab_test");
			commonRequestHandler = new CommonRequestHandler(prescription_detailsDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		prescription_detailsDAO.userDTO = userDTO;
		patientPrescriptionMedicine2DAO.userDTO = userDTO;
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_DETAILS_SEARCH))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getFormattedAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_DETAILS_SEARCH))
				{
					String userName = Utils.getDigitEnglishFromBangla(request.getParameter("userName"));
					if(!Utils.isValidUserName(userName))
					{
						return;
					}
					int whoIsThePatientCat = Integer.parseInt(request.getParameter("whoIsThePatientCat"));
					AppointmentDAO appointmentDAO = new AppointmentDAO();
					long appointmentId = appointmentDAO.getMostRecentAppointmentIdTillToday(userDTO.organogramID, userName, whoIsThePatientCat);
					
					System.out.println("appointmentId = " + appointmentId);
					
					if(appointmentId == -1)
					{
						response.sendRedirect("Entry_pageServlet?actionType=getAddPage&error=" + LC.HM_YOU_MUST_CREATE_AN_APPOINTMENT_FIRST);
					}
					else
					{
						Prescription_detailsDTO prescription_detailsDTO = prescription_detailsDAO.getDTOByappointmentId(appointmentId);
						if(prescription_detailsDTO != null)
						{
							response.sendRedirect("Entry_pageServlet?actionType=getAddPage&error=" + LC.HM_YOU_MUST_CREATE_AN_APPOINTMENT_FIRST);						
						}
						else
						{
							response.sendRedirect("Entry_pageServlet?actionType=getAddPage&error=" + LC.HM_PRESCRIPTION_EXISTS);
						}
					}
					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getFormattedViewPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_DETAILS_SEARCH)
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_ADD_LAB_TEST)
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRITION_ADD_MEDICINE))
				{
					String userName = Utils.getDigitEnglishFromBangla(request.getParameter("userName"));
					if(!Utils.isValidUserName(userName))
					{
						return;
					}
					int whoIsThePatientCat = Integer.parseInt(request.getParameter("whoIsThePatientCat"));
					AppointmentDAO appointmentDAO = new AppointmentDAO();
					long appointmentId = appointmentDAO.getMostRecentAppointmentIdTillToday(userDTO.organogramID, userName, whoIsThePatientCat);
					
					System.out.println("appointmentId = " + appointmentId);
					
					if(appointmentId == -1)
					{
						response.sendRedirect("Entry_pageServlet?actionType=getAddPage&error=" + LC.HM_YOU_MUST_CREATE_AN_APPOINTMENT_FIRST);
					}
					else
					{
						Prescription_detailsDTO prescription_detailsDTO = prescription_detailsDAO.getDTOByappointmentId(appointmentId);
						if(prescription_detailsDTO != null)
						{
							response.sendRedirect("Prescription_detailsServlet?actionType=view"
									+ "&ID=" + prescription_detailsDTO.iD);						
						}
						else
						{							
							response.sendRedirect("Entry_pageServlet?actionType=getAddPage&error=" + LC.HM_NO_PRESCRIPTION_GIVEN);
						}
					}
					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getFormattedSearchPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_DETAILS_SEARCH)
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_ADD_LAB_TEST)
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRITION_ADD_MEDICINE))
				{
					String userName = Utils.getDigitEnglishFromBangla(request.getParameter("userName"));
					if(!Utils.isValidUserName(userName))
					{
						return;
					}
					int whoIsThePatientCat = Integer.parseInt(request.getParameter("whoIsThePatientCat"));
					String filter = " employee_record_id = " + WorkflowController.getEmployeeRecordsIdFromUserName(userName) + " ";
					if(whoIsThePatientCat != FamilyDTO.UNSELECTED)
					{
						filter+= " and who_is_the_patient_cat =" + whoIsThePatientCat;
					}
					System.out.println("filter = " + filter);
					
					searchPrescription_details(request, response, isPermanentTable, filter);
					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("searchByDate"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_DETAILS_SEARCH)
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_ADD_LAB_TEST)
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRITION_ADD_MEDICINE))
				{
					long dr = Long.parseLong(request.getParameter("dr"));
					long date = Long.parseLong(request.getParameter("date"));
					
					String filter = " doctor_id = " + dr + " and visit_date = " + date;
					
					System.out.println("filter = " + filter);
					
					searchPrescription_details(request, response, isPermanentTable, filter);
					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getFormattedSearchPageForNurse"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_DETAILS_SEARCH)
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_ADD_LAB_TEST)
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRITION_ADD_MEDICINE))
				{
					String userName = Utils.getDigitEnglishFromBangla(request.getParameter("userName"));
					if(!Utils.isValidUserName(userName))
					{
						return;
					}
					int whoIsThePatientCat = Integer.parseInt(request.getParameter("whoIsThePatientCat"))  ;
					String filter = " employee_record_id = " + WorkflowController.getEmployeeRecordsIdFromUserName(userName) + " ";
					if(whoIsThePatientCat != FamilyDTO.UNSELECTED)
					{
						filter+= " and who_is_the_patient_cat =" + whoIsThePatientCat;
					}
					System.out.println("filter = " + filter);
					
					searchPrescription_details(request, response, isPermanentTable, filter);
					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getMyPrescriptions"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_DETAILS_SEARCH)
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_ADD_LAB_TEST)
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRITION_ADD_MEDICINE))
				{
					String filter = " employee_record_id = " + WorkflowController.getEmployeeRecordsIdFromUserName(userDTO.userName) + " ";
					System.out.println("filter = " + filter);
					
					searchPrescription_details(request, response, isPermanentTable, filter);
					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_DETAILS_SEARCH))
				{
					getPrescription_details(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getLastDrugReceived"))
			{
				long erId = Long.parseLong(request.getParameter("erId"));
				long drugInformationId = Long.parseLong(request.getParameter("drugInformationId"));
				long whoIsThePatientCat = Long.parseLong(request.getParameter("whoIsThePatientCat"));
				
				PatientPrescriptionMedicine2DTO patientPrescriptionMedicine2DTO = patientPrescriptionMedicine2DAO.getLastDrugReceived (erId, whoIsThePatientCat, drugInformationId);
				PrintWriter out = response.getWriter();
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				
				String encoded = this.gson.toJson(patientPrescriptionMedicine2DTO);
				System.out.println("json encoded data = " + encoded);
				out.print(encoded);
				out.flush();	
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested doget");
				String ajax = request.getParameter("ajax");
				boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
                if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_DETAILS_SEARCH)
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_ADD_LAB_TEST)
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRITION_ADD_MEDICINE))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						String addLabTest = request.getParameter("addLabTest");
						String addMedicine = request.getParameter("addMedicine");
						
						if(addLabTest!= null)
						{
							filter = "addLabTest";
						}
						if(addMedicine!= null)
						{
							filter = "addMedicine";
						}
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							if(filter.equals("patientHanDledInCurrentMonth"))
							{
								filter = " dr_user_name = '" + userDTO.userName + "' and visit_date >= " + TimeConverter.get1stDayOfMonth();
							}
							else if(filter.equals("addLabTest"))
							{
								filter = Prescription_detailsDAO.addLabTestFilter;
								if(userDTO.roleID == SessionConstants.LAB_TECHNITIAN_ROLE
										|| userDTO.roleID == SessionConstants.XRAY_GUY)
								{
									if(request.getParameter("pending")!= null || (request.getParameter("myTests") == null && !hasAjax))
									{
										filter = Prescription_detailsDAO.pendingTestFilter;
									}
									else if(request.getParameter("myTests")!= null)
									{
										filter = Prescription_detailsDAO.myTestFilter;
									}
								}
								else if(userDTO.roleID == SessionConstants.RADIOLOGIST)
								{
									if(request.getParameter("pending")!= null || (request.getParameter("myTests") == null && !hasAjax))
									{
										filter = Prescription_detailsDAO.radioPendingTestFilter;
									}
									else if(request.getParameter("myTests")!= null)
									{
										filter = Prescription_detailsDAO.radioMyTestFilter;
									}
								}
								else if(userDTO.roleID == SessionConstants.PATHOLOGY_HEAD)
								{
									if(request.getParameter("pending")!= null || (request.getParameter("done") == null && !hasAjax))
									{
										filter = Prescription_detailsDAO.pathologyHeadPendingFilter;
									}
									else if(request.getParameter("done")!= null)
									{
										filter = Prescription_detailsDAO.pathologyHeadApprovedFilter;
									}
								}
								
							}
							else if(filter.equals("addMedicine"))
							{
								
								int validityHours = Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.PRESCIPTION_VALIDITY_HOURS).value);
							    long now = System.currentTimeMillis();								
								filter = " medicine_count > 0 ";
								if(!hasAjax)
								{
									filter +=  " and medicine_added = 0 and visit_date >= " + TimeConverter.getToday()
									+ " and (" + now + " - visit_time <= " + validityHours * 1000 * 3600 + ") ";
								}
								
							}
							else
							{
								filter = ""; //shouldn't be directly used, rather manipulate it.
							}
							searchPrescription_details(request, response, isPermanentTable, filter);
						}
						else
						{
							searchPrescription_details(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchPrescription_details(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			
		
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_DETAILS_SEARCH)
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_ADD_LAB_TEST)
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRITION_ADD_MEDICINE))
				{
					System.out.println("ID = " + request.getParameter("ID"));
					long id = Long.parseLong(request.getParameter("ID"));
					Prescription_detailsDTO prescription_detailsDTO = prescription_detailsDAO.getDTOByID(id);
					if(prescription_detailsDTO != null)
					{
						AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(prescription_detailsDTO.appointmentId);
						if(!prescription_detailsDAO.canViewPrescriptions(appointmentDTO, userDTO))
						{
							request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
						}
						else
						{
							request.getRequestDispatcher( "prescription_details/prescription_detailsView.jsp?ID=" + id).forward(request, response);
						}
					}
					
					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	private void giveMedicine(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws ServletException, IOException 
	{
		long id = Long.parseLong(request.getParameter("ID"));
		Prescription_detailsDTO prescription_detailsDTO = prescription_detailsDAO.getDTOByID(id);
		AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(prescription_detailsDTO.appointmentId);
		boolean hasRestriction = Restriction_configRepository.getInstance().restrictionInList(appointmentDTO.erId, RestrictionConfigDetailsDTO.MEDICAL);
		if(hasRestriction)
		{
			System.out.println("Restricted");
			response.sendRedirect("Prescription_detailsServlet?actionType=view&search");
			return;
		}
		if(!prescription_detailsDAO.canAddMedicine(appointmentDTO, userDTO))
		{
			System.out.println("Cannot add medicine");
			response.sendRedirect("Prescription_detailsServlet?actionType=view&search");
			return;
		}
		if(prescription_detailsDTO.medicineAdded)
		{
			System.out.println("Medicine is already added");
			response.sendRedirect("Prescription_detailsServlet?justView=1&actionType=view&addMedicine=1&&ID=" + id);
			return;
		}
		else
		{
			List<PatientPrescriptionMedicine2DTO> patientPrescriptionMedicine2DTOs;
			try {
				patientPrescriptionMedicine2DTOs = patientPrescriptionMedicine2DAO.getPatientPrescriptionMedicine2DTOListByPrescriptionDetailsID(prescription_detailsDTO.iD);

				
				boolean medicineDone = true;
				
				for(PatientPrescriptionMedicine2DTO patientPrescriptionMedicine2DTO: patientPrescriptionMedicine2DTOs)
				{
					Drug_informationDTO drug_informationDTO = drug_informationDAO.getDTOByID(patientPrescriptionMedicine2DTO.drugInformationType);
					UserDTO patientDTO = userDAO.getUserDTOByUsernameFromOnlyUsers(appointmentDTO.employeeUserName);
					if(drug_informationDTO != null && drug_informationDAO.canReceiveThisDrug(drug_informationDTO, appointmentDTO.patientId))
					{
						int quantity = patientPrescriptionMedicine2DTO.remainingQuantity;
						if(drug_informationDTO.availableStock - quantity <= 0)
						{
							//continue;
							quantity = drug_informationDTO.availableStock;
							
						}

						Medical_inventory_outDTO medical_inventory_outDTO = new Medical_inventory_outDTO
								(drug_informationDTO,
								quantity, userDTO, MedicalInventoryInDTO.TR_RECEIVE_MEDICINE,
								patientDTO, SessionConstants.MEDICAL_ITEM_DRUG,
								appointmentDTO.patientName, appointmentDTO.phoneNumber,
								prescription_detailsDTO.iD, patientPrescriptionMedicine2DTO.iD);	
						medical_inventory_outDTO.otherOfficeId = prescription_detailsDTO.othersOfficeId;
						medical_inventory_outDAO.add(medical_inventory_outDTO);
						
						patientPrescriptionMedicine2DTO.doseSold = patientPrescriptionMedicine2DTO.doseSold + quantity;
						patientPrescriptionMedicine2DTO.doseSoldToday = quantity;
						patientPrescriptionMedicine2DTO.remainingQuantity = patientPrescriptionMedicine2DTO.quantity - patientPrescriptionMedicine2DTO.doseSold;
						patientPrescriptionMedicine2DTO.sellerId = userDTO.ID;
						patientPrescriptionMedicine2DTO.sellerName = userDTO.userName;
						patientPrescriptionMedicine2DTO.sellingDate = medical_inventory_outDTO.transactionDate;
						patientPrescriptionMedicine2DTO.cost = medical_inventory_outDTO.unitPrice * patientPrescriptionMedicine2DTO.doseSold;
						
						patientPrescriptionMedicine2DAO.update(patientPrescriptionMedicine2DTO);
						/*if(patientPrescriptionMedicine2DTO.remainingQuantity != 0)
						{
							medicineDone = false;
						}*/
						
						PersonalStockDTO personalStockDTO = PersonalStockDAO.getInstance().getDTOByUserNameAndDrugId(prescription_detailsDTO.employeeUserName,
								patientPrescriptionMedicine2DTO.drugInformationType, SessionConstants.MEDICAL_ITEM_DRUG);
						if(personalStockDTO == null)
						{
							personalStockDTO = new PersonalStockDTO(prescription_detailsDTO, patientPrescriptionMedicine2DTO, drug_informationDTO);
							PersonalStockDAO.getInstance().add(personalStockDTO);
						}
						else
						{
							personalStockDTO.quantity += patientPrescriptionMedicine2DTO.doseSoldToday;
							PersonalStockDAO.getInstance().update(personalStockDTO);
						}
					}
				}
				 
				prescription_detailsDTO.canBeEdited = false;
				prescription_detailsDAO.update(prescription_detailsDTO);
				
				if(medicineDone)
				{
					System.out.println("Medicine done");
					prescription_detailsDAO.addMedicine(prescription_detailsDTO.iD);
					
					SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
					Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();
					String formatted_visitDate = f.format(new Date(appointmentDTO.visitDate));
					
					
					String englishText = "Medicine delivered for prescription #"  + appointmentDTO.sl + " of date " + formatted_visitDate;
							
					String banglaText =  "আপনার "
							+ Utils.getDigits(formatted_visitDate, "bangla")
							+ " তারিখের প্রেস্ক্রিপশন #" 
							+ Utils.getDigits(appointmentDTO.sl, "bangla")
							+ " এর  বিপরীতে ওষুধ প্রদান করা হয়েছে। ধন্যবাদ।";
					
					String URL =  "AppointmentServlet?actionType=view&ID=" + appointmentDTO.iD;
					UserDTO patientDTO = UserRepository.getUserDTOByUserName(appointmentDTO.employeeUserName);

					System.out.println("$$$$$$$$$$$$$$$ Sending medi delivery noti");
					pb_notificationsDAO.addPb_notificationsAndSendMailSMS(patientDTO.organogramID, System.currentTimeMillis(),
							URL, englishText, banglaText, "Medicine Delivered",
							WorkflowController.getPhoneNumberFromErId(patientDTO.employee_record_id),
							-1, false, true);
				}
				else
				{
					System.out.println("Medicine not done");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		response.sendRedirect("Prescription_detailsServlet?justView=1&actionType=search&addMedicine=1");
	}
	
	private void testLab(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		long id = Long.parseLong(request.getParameter("ID"));
		System.out.println("Adding lab test for id = " + id);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		if(request.getParameterValues("testId") != null)
		{
			String[] labTestIds = request.getParameterValues("testId");
			int i = 0;
			Common_lab_reportDAO common_lab_reportDAO = new Common_lab_reportDAO();
			for(String sTestId: labTestIds)
			{
				long testId = Long.parseLong(sTestId);
				long clrId = common_lab_reportDAO.getIdByLabTestId(testId);
				if(clrId == -1) // testing not done yet
				{
					PrescriptionLabTestDTO prescriptionLabTestDTO = prescriptionLabTestDAO.getDTOByID(testId);
					Prescription_detailsDTO prescription_detailsDTO = prescription_detailsDAO.getDTOByID(prescriptionLabTestDTO.prescriptionDetailsId);
					if(prescriptionLabTestDTO != null)
					{
						AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(prescription_detailsDTO.appointmentId);
						if(!prescription_detailsDAO.canAddLabTest(appointmentDTO, userDTO))
						{
							System.out.println("Cannot add lab test");
							response.sendRedirect("Prescription_detailsServlet?actionType=view&search");
							return;
						}
						String sTestingDate = request.getParameter("testingDate_" + prescriptionLabTestDTO.iD);
						System.out.println("testing date = " + sTestingDate);
						if(sTestingDate != null && !sTestingDate.equalsIgnoreCase(""))
						{
							
							String[] splitedDate = sTestingDate.split("/");
							cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(splitedDate[0]));
							cal.set(Calendar.MONTH, Integer.parseInt(splitedDate[1]) - 1);
							cal.set(Calendar.YEAR, Integer.parseInt(splitedDate[2]));
							System.out.println("Testing Date after parsing = " + sdf.format(cal.getTime()));
							prescriptionLabTestDTO.testingDate = cal.getTimeInMillis();
						}
						
						String sSchDeliveryDate = request.getParameter("schDate_" + prescriptionLabTestDTO.iD);
						if(sSchDeliveryDate != null && !sSchDeliveryDate.equalsIgnoreCase(""))
						{
							String[] splitedDate = sSchDeliveryDate.split("/");
							cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(splitedDate[0]));
							cal.set(Calendar.MONTH, Integer.parseInt(splitedDate[1]) - 1);
							cal.set(Calendar.YEAR, Integer.parseInt(splitedDate[2]));
							System.out.println("Delivery Date after parsing = " + sdf.format(cal.getTime()));
							prescriptionLabTestDTO.scheduledDeliveryDate = cal.getTimeInMillis();
						}
						
						String [] plateTypes = request.getParameterValues("xRayPlateType_" + prescriptionLabTestDTO.iD);
						if(plateTypes != null)
						{
							prescriptionLabTestDTO.xRayPlateType = "";
							int j = 0;
							for(String plateType: plateTypes)
							{
								if(j > 0)
								{
									prescriptionLabTestDTO.xRayPlateType += ", ";
								}
								prescriptionLabTestDTO.xRayPlateType += Integer.parseInt(plateType) + "";
								j++;
							}
						}
						
						String [] plateWasteds = request.getParameterValues("plateWasted_" + prescriptionLabTestDTO.iD);
						if(plateWasteds != null)
						{
							prescriptionLabTestDTO.plateWasted = "";
							int j = 0;
							for(String plateWasted: plateWasteds)
							{
								if(j > 0)
								{
									prescriptionLabTestDTO.plateWasted += ", ";
								}
								prescriptionLabTestDTO.plateWasted += Integer.parseInt(plateWasted) + "";
								j++;
							}
						}
						
						String [] plateQuantitiess = request.getParameterValues("plateQuantity_" + prescriptionLabTestDTO.iD);
						if(plateQuantitiess != null)
						{
							prescriptionLabTestDTO.plateQuantity = "";
							int j = 0;
							for(String plateQuantity: plateQuantitiess)
							{
								if(j > 0)
								{
									prescriptionLabTestDTO.plateQuantity += ", ";
								}
								prescriptionLabTestDTO.plateQuantity += Integer.parseInt(plateQuantity) + "";
								j++;
							}
						}
					
						
						if(request.getParameter("isDone_" + prescriptionLabTestDTO.iD) != null)
						{
							if(!prescriptionLabTestDTO.isDone)
							{
								if(plateTypes != null && plateQuantitiess != null)
								{
									int j = 0;
									prescriptionLabTestDTO.plateUsed1 = 0;
									prescriptionLabTestDTO.plateUsed2 = 0;
									prescriptionLabTestDTO.plateUsed3 = 0;
									
									prescriptionLabTestDTO.plateWasted1 = 0;
									prescriptionLabTestDTO.plateWasted2 = 0;
									prescriptionLabTestDTO.plateWasted3 = 0;
									for(String plateType: plateTypes)
									{
										int iPlateType = Integer.parseInt(plateType);
										int iPlateQuantity = Integer.parseInt(plateQuantitiess[j]);
										
										if(iPlateType == SessionConstants.PLATE_1)
										{
											if(plateWasteds[j].equalsIgnoreCase("0"))
											{
												prescriptionLabTestDTO.plateUsed1 += iPlateQuantity;
											}
											else
											{
												prescriptionLabTestDTO.plateWasted1 += iPlateQuantity;
											}
										}
										if(iPlateType == SessionConstants.PLATE_2)
										{
											if(plateWasteds[j].equalsIgnoreCase("0"))
											{
												prescriptionLabTestDTO.plateUsed2 += iPlateQuantity;
											}
											else
											{
												prescriptionLabTestDTO.plateWasted2 += iPlateQuantity;
											}
										}
										if(iPlateType == SessionConstants.PLATE_3)
										{
											if(plateWasteds[j].equalsIgnoreCase("0"))
											{
												prescriptionLabTestDTO.plateUsed3 += iPlateQuantity;
											}
											else
											{
												prescriptionLabTestDTO.plateWasted3 += iPlateQuantity;
											}
										}
										Medical_inventory_outDTO medical_inventory_outDTO = 
												new Medical_inventory_outDTO(iPlateType, iPlateQuantity, userDTO, prescriptionLabTestDTO.iD, MedicalInventoryInDTO.TR_DELIVER_EQUIPMENT, userDTO);
										medical_inventory_outDAO.add(medical_inventory_outDTO);
										j++;
									}
								}
								prescriptionLabTestDTO.isDone = true;
								prescriptionLabTestDTO.testingDate  = System.currentTimeMillis();
								prescriptionLabTestDTO.xRayUserName = userDTO.userName;
								
							}
							
						}
						

						
						//prescriptionLabTestDTO.testerId = request.getParameterValues("testerId")[i];
						//prescriptionLabTestDTO.testStatus = request.getParameterValues("status")[i];
						
						prescriptionLabTestDAO.update(prescriptionLabTestDTO);
					}
				}
				
				
				i++;
				
			}
		}
		
		
		response.sendRedirect("Prescription_detailsServlet?actionType=search&addLabTest=1");
	}
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		patientPrescriptionMedicine2DAO.userDTO = userDTO;
		prescription_detailsDAO.userDTO = userDTO;
		System.out.println("doPost");
		boolean isPermanentTable = true;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_DETAILS_SEARCH))
				{
					System.out.println("going to  addPrescription_details ");
					addPrescription_details(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addPrescription_details ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("getLabtestDetails"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_DETAILS_SEARCH))
				{
					long id = Long.parseLong(request.getParameter("id"));
					LabTestListDAO labTestListDAO = new LabTestListDAO();
					List<LabTestListDTO> labTestListDTOs = labTestListDAO.getLabTestListDTOListByLabTestID(id);
					String options = "";
					
					for(LabTestListDTO labTestListDTO: labTestListDTOs)
					{
						if(!labTestListDTO.nameEn.equalsIgnoreCase("") && labTestListDTO.showInDropdown)
						{
							options += "<option value = '" + labTestListDTO.iD + "'>" + labTestListDTO.nameEn + "</option>";
						}						
					}
					//String options = CommonDAO.getOptionsWithWhere("English", "lab_test_list", " lab_test_id = " + id);
					options = "<option value = '" + PrescriptionLabTestDTO.ALL + "'>All</option>" 
							+ options ;
					response.getWriter().write(options);
				}
				else
				{
					System.out.println("Not going to  addPrescription_details ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("addMedicine"))
			{
				giveMedicine(request, response, userDTO);
			}
			else if(actionType.equals("updateLabTest"))
			{
				testLab(request, response, userDTO);
			}
			
			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_DETAILS_SEARCH))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addPrescription_details ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_DETAILS_SEARCH))
				{					
					addPrescription_details(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			/*else if(actionType.equals("delete"))
			{								
				deletePrescription_details(request, response, userDTO);				
			}*/
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PRESCRIPTION_DETAILS_SEARCH))
				{
					searchPrescription_details(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			/*else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}*/
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Prescription_detailsDTO prescription_detailsDTO = prescription_detailsDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(prescription_detailsDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addPrescription_details(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addPrescription_details");

			Prescription_detailsDTO prescription_detailsDTO;

						
			if(addFlag == true)
			{
				prescription_detailsDTO = new Prescription_detailsDTO();
			}
			else
			{
				prescription_detailsDTO = prescription_detailsDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			if(!prescription_detailsDTO.canBeEdited)
			{
				System.out.println("Forced update prevented");
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
			
			String Value = "";
			AppointmentDTO appointmentDTO = null;
			
			if(!addFlag)
			{
				Value = request.getParameter("appointmentId");
				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("appointmentId = " + Value);
				
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					
					prescription_detailsDTO.appointmentId = Long.parseLong(Value);
					AppointmentDAO appointmentDAO = new AppointmentDAO();
					appointmentDTO = appointmentDAO.getDTOByID(prescription_detailsDTO.appointmentId);
				}
			}
			else
			{
				Value = request.getParameter("appointmentId");
				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("appointmentId = " + Value);
				
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					
					prescription_detailsDTO.appointmentId = Long.parseLong(Value);
					AppointmentDAO appointmentDAO = new AppointmentDAO();
					appointmentDTO = appointmentDAO.getDTOByID(prescription_detailsDTO.appointmentId);
					if(appointmentDTO != null)
					{
						Prescription_detailsDTO oldPrescription_detailsDTO = prescription_detailsDAO.getDTOByappointmentId(prescription_detailsDTO.appointmentId);
						if(oldPrescription_detailsDTO != null)
						{
							response.sendRedirect("Prescription_detailsServlet?actionType=view&ID=" + oldPrescription_detailsDTO.iD + "&errorMsg=" 
									+ "Failed to add multiple prescriptions for one appointment. Redirected to the old prescrition page." );
							return;
						}
						prescription_detailsDTO.employeeUserName = appointmentDTO.employeeUserName;
						prescription_detailsDTO.employeeId = appointmentDTO.patientId;
						prescription_detailsDTO.visitDate = appointmentDTO.visitDate;
						prescription_detailsDTO.doctorId = appointmentDTO.doctorId;
						prescription_detailsDTO.whoIsThePatientCat = appointmentDTO.whoIsThePatientCat;
						prescription_detailsDTO.drUserName = appointmentDTO.drUserName;
						prescription_detailsDTO.othersOfficeBn = appointmentDTO.othersOfficeBn;
						prescription_detailsDTO.othersOfficeEn = appointmentDTO.othersOfficeEn;
						prescription_detailsDTO.othersOfficeId = appointmentDTO.othersOfficeId;
						prescription_detailsDTO.othersDesignationAndId = appointmentDTO.othersDesignationAndId;
						prescription_detailsDTO.erId = appointmentDTO.erId;
						prescription_detailsDTO.isDental = appointmentDTO.isDental;
						UserDTO patientDTO = userDAO.getUserDTOByUsernameFromOnlyUsers(appointmentDTO.employeeUserName);
						if(patientDTO != null)
						{
							prescription_detailsDTO.employeeUnit = patientDTO.unitID;
						}
					}
					else
					{
						response.sendRedirect("Prescription_detailsServlet?actionType=search");
						return;
					}
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}
				
				prescription_detailsDTO.visitTime = System.currentTimeMillis();
				
				if(!prescription_detailsDAO.canAddPrescription(appointmentDTO, userDTO ))
				{
					response.sendRedirect("Prescription_detailsServlet?actionType=search");
					return;
				}

				Value = request.getParameter("name");
				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("name = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					
					prescription_detailsDTO.name = (Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}
				
				prescription_detailsDTO.sl = appointmentDTO.sl;
				prescription_detailsDTO.phone = appointmentDTO.phoneNumber;
				prescription_detailsDTO.dateOfBirth = appointmentDTO.dateOfBirth;
				prescription_detailsDTO.alternatePhone = appointmentDTO.alternatePhone;
				prescription_detailsDTO.extraRelation = appointmentDTO.extraRelation;
			}

		

			Value = request.getParameter("height2");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("height = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				prescription_detailsDTO.height = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("weight2");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("weight = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				prescription_detailsDTO.weight = Double.parseDouble(Value);
				if(prescription_detailsDTO.height != 0)
				{
					prescription_detailsDTO.bmi = prescription_detailsDTO.weight * 10000 
							/ (prescription_detailsDTO.height * prescription_detailsDTO.height);
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("sugar");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("sugar = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				prescription_detailsDTO.sugar = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("bloodPressureDiastole");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("bloodPressureDiastole = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				prescription_detailsDTO.bloodPressureDiastole = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("bloodPressureSystole");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("bloodPressureSystole = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				prescription_detailsDTO.bloodPressureSystole = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("pulse");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("pulse = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				prescription_detailsDTO.pulse = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("temperature");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("temperature = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				prescription_detailsDTO.temperature = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("oxygenSaturation");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("oxygenSaturation = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				prescription_detailsDTO.oxygenSaturation = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("diagnosis");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("diagnosis = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				prescription_detailsDTO.diagnosis = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("chiefComplaints");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("chiefComplaints = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				prescription_detailsDTO.chiefComplaints = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("clinicalFeatures");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("clinicalFeatures = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				prescription_detailsDTO.clinicalFeatures = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("advice");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("advice = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				prescription_detailsDTO.advice = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("remarks");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("remarks = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				prescription_detailsDTO.remarks = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
			String language = LM.getText(LC.APPOINTMENT_EDIT_LANGUAGE, loginDTO);
			
			if(addFlag)
			{
				Value = request.getParameter("referredTo");
				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("referredTo = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					
					prescription_detailsDTO.referredTo = Long.parseLong(Value);
					prescription_detailsDTO.referredToName = WorkflowController.getNameFromOrganogramId(prescription_detailsDTO.referredTo, language);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}
				
				Value = request.getParameter("referralMessage");
				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("referralMessage = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{				
					prescription_detailsDTO.referralMessage = Value;
								
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}
				
				Value = request.getParameter("therapyNeeded");

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("therapyNeeded = " + Value);
                prescription_detailsDTO.therapyNeeded = Value != null && !Value.equalsIgnoreCase("");
				
				Value = request.getParameter("createAppointment");
				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("createAppointment = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					System.out.println("Creating Auto Appointment calling");
					prescription_detailsDTO.referredAppointmentId = createAutoAppointment(prescription_detailsDTO);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

			}
			else
			{
				prescription_detailsDTO.modifiedByErId = userDTO.employee_record_id;
			}
			
			Value = request.getParameter("dentalInstructionNeeded");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("dentalInstructionNeeded = " + Value);
            prescription_detailsDTO.dentalInstructionNeeded = Value != null && !Value.equalsIgnoreCase("");
			
			
			System.out.println("Done adding  addPrescription_details dto = " + prescription_detailsDTO);
			long returnedID = -1;
			
		
			
			List<PatientPrescriptionMedicine2DTO> patientPrescriptionMedicine2DTOList = createPatientPrescriptionMedicine2DTOListByRequest(request);			
			List<PrescriptionLabTestDTO> prescriptionLabTestDTOList = createPrescriptionLabTestDTOListByRequest(request);
			
			int medicineCount = 0;
			if(patientPrescriptionMedicine2DTOList != null)
			{
				for(PatientPrescriptionMedicine2DTO patientPrescriptionMedicine2DTO: patientPrescriptionMedicine2DTOList)
				{
					Drug_informationDTO drug_informationDTO = Drug_informationRepository.getInstance().getDrug_informationDTOByID(patientPrescriptionMedicine2DTO.drugInformationType);
					if(drug_informationDTO != null && drug_informationDAO.canReceiveThisDrug(drug_informationDTO, prescription_detailsDTO.employeeId))
					{
						medicineCount ++;
					}
				}
				prescription_detailsDTO.medicineCount = medicineCount;
			}
			
			if(prescriptionLabTestDTOList != null)
			{
				prescription_detailsDTO.testCount = prescriptionLabTestDTOList.size();
			}
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				prescription_detailsDAO.setIsDeleted(prescription_detailsDTO.iD, CommonDTO.OUTDATED);
				returnedID = prescription_detailsDAO.add(prescription_detailsDTO);
				prescription_detailsDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = prescription_detailsDAO.manageWriteOperations(prescription_detailsDTO, SessionConstants.INSERT, -1, userDTO);
				appointmentDTO.prescriptionId = returnedID;
				appointmentDAO.update(appointmentDTO);
				
			}
			else
			{				
				returnedID = prescription_detailsDAO.manageWriteOperations(prescription_detailsDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			if(prescription_detailsDTO.isDental)
			{
				dental_activitiesDAO.deleteChildrenByParent(returnedID, "prescription_details_id");
				String []dentalCats = request.getParameterValues("dentalActionCat");
				if(dentalCats != null)
				{
					int i = 0;
					for(String sDentalCat: dentalCats)
					{
						Dental_activitiesDTO dental_activitiesDTO = new Dental_activitiesDTO(appointmentDTO, prescription_detailsDTO);
						dental_activitiesDTO.dentalActionCat = Integer.parseInt(sDentalCat);
						String sDtOrganogramId = Jsoup.clean(request.getParameterValues("dtOrganogramId")[i],Whitelist.simpleText());
						logger.debug("sDtOrganogramId = " + sDtOrganogramId 
								+ " dental_activitiesDTO.dentalActionCat = " + dental_activitiesDTO.dentalActionCat);
						if(sDtOrganogramId != null)
						{
							
							dental_activitiesDTO.dtOrganogramId = Long.parseLong(sDtOrganogramId);
							dental_activitiesDTO.dtErId = WorkflowController.getEmployeeRecordIDFromOrganogramID(dental_activitiesDTO.dtOrganogramId);
							dental_activitiesDTO.dtUserId = WorkflowController.getUserIDFromOrganogramId(dental_activitiesDTO.dtOrganogramId);
							dental_activitiesDTO.doctorRemarks = Jsoup.clean(request.getParameterValues("doctorRemarks")[i],Whitelist.simpleText());
							dental_activitiesDTO.isDone = true;
							dental_activitiesDAO.add(dental_activitiesDTO);
						}
						i++;
					}
				}
			}
			
			
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(patientPrescriptionMedicine2DTOList != null)
				{				
					for(PatientPrescriptionMedicine2DTO patientPrescriptionMedicine2DTO: patientPrescriptionMedicine2DTOList)
					{
						patientPrescriptionMedicine2DTO.prescriptionDetailsId = prescription_detailsDTO.iD; 
						patientPrescriptionMedicine2DAO.add(patientPrescriptionMedicine2DTO);
					}
				}
			
			}
			else
			{
				List<Long> childIdsFromRequest = patientPrescriptionMedicine2DAO.getChildIdsFromRequest(request, "patientPrescriptionMedicine2");
				//delete the removed children
				patientPrescriptionMedicine2DAO.hardDeleteChildrenNotInList("prescription_details", "patient_prescription_medicine2", prescription_detailsDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = patientPrescriptionMedicine2DAO.getChilIds("prescription_details", "patient_prescription_medicine2", prescription_detailsDTO.iD);
				
				
				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							PatientPrescriptionMedicine2DTO patientPrescriptionMedicine2DTO =  createPatientPrescriptionMedicine2DTOByRequestAndIndex(request, false, i);
							patientPrescriptionMedicine2DTO.prescriptionDetailsId = prescription_detailsDTO.iD; 
							patientPrescriptionMedicine2DAO.update(patientPrescriptionMedicine2DTO);
						}
						else
						{
							PatientPrescriptionMedicine2DTO patientPrescriptionMedicine2DTO =  createPatientPrescriptionMedicine2DTOByRequestAndIndex(request, true, i);
							patientPrescriptionMedicine2DTO.prescriptionDetailsId = prescription_detailsDTO.iD; 
							patientPrescriptionMedicine2DAO.add(patientPrescriptionMedicine2DTO);
						}
					}
				}
				else
				{
					patientPrescriptionMedicine2DAO.hardDeleteChildrenByParent(prescription_detailsDTO.iD, "prescription_details_id");
				}
				
			}
			
						
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(prescriptionLabTestDTOList != null)
				{				
					for(PrescriptionLabTestDTO prescriptionLabTestDTO: prescriptionLabTestDTOList)
					{
						prescriptionLabTestDTO.prescriptionDetailsId = prescription_detailsDTO.iD; 
						prescriptionLabTestDAO.add(prescriptionLabTestDTO);
					}
				}
			
			}
			else
			{
				List<Long> childIdsFromRequest = prescriptionLabTestDAO.getChildIdsFromRequest(request, "prescriptionLabTest");
				//delete the removed children
				prescriptionLabTestDAO.hardDeleteChildrenNotInList("prescription_details", "prescription_lab_test", prescription_detailsDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = prescriptionLabTestDAO.getChilIds("prescription_details", "prescription_lab_test", prescription_detailsDTO.iD);
				
				
				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							/*PrescriptionLabTestDTO prescriptionLabTestDTO =  createPrescriptionLabTestDTOByRequestAndIndex(request, false, i);
							prescriptionLabTestDTO.prescriptionDetailsId = prescription_detailsDTO.iD; 
							prescriptionLabTestDAO.update(prescriptionLabTestDTO);*/
						}
						else
						{
							PrescriptionLabTestDTO prescriptionLabTestDTO =  createPrescriptionLabTestDTOByRequestAndIndex(request, true, i);
							prescriptionLabTestDTO.prescriptionDetailsId = prescription_detailsDTO.iD; 
							prescriptionLabTestDAO.add(prescriptionLabTestDTO);
						}
					}
				}
				else
				{
					prescriptionLabTestDAO.hardDeleteChildrenByParent(prescription_detailsDTO.iD, "prescription_details_id");
				}
				
			}
			
			
			
			response.sendRedirect("Prescription_detailsServlet?actionType=view&ID=" + returnedID);
			
			
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	private long createAutoAppointment(Prescription_detailsDTO prescription_detailsDTO) throws Exception 
	{
		System.out.println("Creating Auto Appointment");
		AppointmentDAO appointmentDAO = new AppointmentDAO();
		AppointmentDTO newAppointmentDTO = new AppointmentDTO();
		AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(prescription_detailsDTO.appointmentId);
		newAppointmentDTO = appointmentDTO;
		newAppointmentDTO.doctorId = prescription_detailsDTO.referredTo;
		newAppointmentDTO.doctorName = prescription_detailsDTO.referredToName;
		newAppointmentDTO.erId = prescription_detailsDTO.erId;
		if(!appointmentDAO.isAvailableDoctorOrPhysiotherapist(newAppointmentDTO.doctorId))
		{
			return -1;
		}
		
		UserDTO drDTO = UserRepository.getUserDTOByOrganogramID(newAppointmentDTO.doctorId);
		if(drDTO != null)
		{
			newAppointmentDTO.drUserName = drDTO.userName;
		}
		if(appointmentDAO.isDoctor(prescription_detailsDTO.referredTo))
		{
			boolean created = appointmentDAO.setDateShiftSlot(newAppointmentDTO);
			if(!created)
			{
				System.out.println("Appointment Creation Failed");
				appointmentDAO.createAppointmentNoti(appointmentDTO);
				return -1;
			}
		}
		else
		{
			System.out.println("###sloting not even a doctor!!!");
		}
		newAppointmentDTO.shiftCat = appointmentDAO.shift;		
		return appointmentDAO.add(newAppointmentDTO);
	}

	private List<PatientPrescriptionMedicine2DTO> createPatientPrescriptionMedicine2DTOListByRequest(HttpServletRequest request) throws Exception{ 
		List<PatientPrescriptionMedicine2DTO> patientPrescriptionMedicine2DTOList = new ArrayList<PatientPrescriptionMedicine2DTO>();
		if(request.getParameterValues("patientPrescriptionMedicine2.iD") != null) 
		{
			int patientPrescriptionMedicine2ItemNo = request.getParameterValues("patientPrescriptionMedicine2.iD").length;
			
			
			for(int index=0;index<patientPrescriptionMedicine2ItemNo;index++){
				PatientPrescriptionMedicine2DTO patientPrescriptionMedicine2DTO = createPatientPrescriptionMedicine2DTOByRequestAndIndex(request,true,index);
				boolean drugFound = false;
				if(patientPrescriptionMedicine2DTO.drugInformationType == -1 && !patientPrescriptionMedicine2DTO.alternateDrugName.equalsIgnoreCase(""))
				{
					//ok to add
				}
				else
				{
					for(PatientPrescriptionMedicine2DTO existingDTO: patientPrescriptionMedicine2DTOList)
					{
						if(existingDTO.drugInformationType == patientPrescriptionMedicine2DTO.drugInformationType)
						{
							drugFound = true;
							break;
						}
					}
				}
				
				if(!drugFound && (patientPrescriptionMedicine2DTO.drugInformationType != -1 
						|| !patientPrescriptionMedicine2DTO.alternateDrugName.equalsIgnoreCase("")))
				{
					patientPrescriptionMedicine2DTOList.add(patientPrescriptionMedicine2DTO);
				}
				
			}
			
			return patientPrescriptionMedicine2DTOList;
		}
		return null;
	}
	private List<PrescriptionLabTestDTO> createPrescriptionLabTestDTOListByRequest(HttpServletRequest request) throws Exception{ 
		List<PrescriptionLabTestDTO> prescriptionLabTestDTOList = new ArrayList<PrescriptionLabTestDTO>();
		if(request.getParameterValues("prescriptionLabTest.iD") != null) 
		{
			int prescriptionLabTestItemNo = request.getParameterValues("prescriptionLabTest.iD").length;
			
			
			for(int index=0;index<prescriptionLabTestItemNo;index++){
				PrescriptionLabTestDTO prescriptionLabTestDTO = createPrescriptionLabTestDTOByRequestAndIndex(request,true,index);
				prescriptionLabTestDTOList.add(prescriptionLabTestDTO);
			}
			
			return prescriptionLabTestDTOList;
		}
		return null;
	}
	
	
	private PatientPrescriptionMedicine2DTO createPatientPrescriptionMedicine2DTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{
	
		PatientPrescriptionMedicine2DTO patientPrescriptionMedicine2DTO;
		if(addFlag == true )
		{
			patientPrescriptionMedicine2DTO = new PatientPrescriptionMedicine2DTO();
		}
		else
		{
			patientPrescriptionMedicine2DTO = patientPrescriptionMedicine2DAO.getDTOByID(Long.parseLong(request.getParameterValues("patientPrescriptionMedicine2.iD")[index]));
		}

		String Value = "";
		if(addFlag)
		{
			Value = request.getParameterValues("patientPrescriptionMedicine2.prescriptionDetailsId")[index];

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}

			patientPrescriptionMedicine2DTO.prescriptionDetailsId = Long.parseLong(Value);
			Value = request.getParameterValues("patientPrescriptionMedicine2.drugInformationType")[index];

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
				patientPrescriptionMedicine2DTO.drugInformationType = Long.parseLong(Value);
				
			}
			
			if(patientPrescriptionMedicine2DTO.drugInformationType == -1)
			{
				Value = request.getParameterValues("patientPrescriptionMedicine2.alternateDrugName")[index];

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
					patientPrescriptionMedicine2DTO.alternateDrugName = Value;
					System.out.println("patientPrescriptionMedicine2DTO.alternateDrugName = " + patientPrescriptionMedicine2DTO.alternateDrugName);
				}
				else
				{
					System.out.println("patientPrescriptionMedicine2DTO.alternateDrugName = null");
				}
			}
			
			

		}
		
		
		Value = request.getParameterValues("patientPrescriptionMedicine2.dosageTypeCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		patientPrescriptionMedicine2DTO.dosageTypeCat = Integer.parseInt(Value);
		Value = request.getParameterValues("patientPrescriptionMedicine2.dose")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		patientPrescriptionMedicine2DTO.dose = (Value);
		
		Value = request.getParameterValues("patientPrescriptionMedicine2.frequency")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			patientPrescriptionMedicine2DTO.frequency = (Value);
		}
		
		Value = request.getParameterValues("patientPrescriptionMedicine2.frequencyCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			patientPrescriptionMedicine2DTO.frequencyCat = Integer.parseInt(Value);
		}
		
		Value = request.getParameterValues("patientPrescriptionMedicine2.quantity")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			patientPrescriptionMedicine2DTO.quantity = Integer.parseInt(Value);
			if(patientPrescriptionMedicine2DTO.quantity < 0)
			{
				patientPrescriptionMedicine2DTO.quantity = 0;
			}
			patientPrescriptionMedicine2DTO.remainingQuantity = Integer.parseInt(Value);
		}

		
		Value = request.getParameterValues("patientPrescriptionMedicine2.foodInstructionsCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		patientPrescriptionMedicine2DTO.foodInstructionsCat = Integer.parseInt(Value);
		return patientPrescriptionMedicine2DTO;
	
	}
	
	
	private PrescriptionLabTestDTO createPrescriptionLabTestDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{
	
		PrescriptionLabTestDTO prescriptionLabTestDTO;
		if(addFlag == true )
		{
			prescriptionLabTestDTO = new PrescriptionLabTestDTO();
		}
		else
		{
			prescriptionLabTestDTO = prescriptionLabTestDAO.getDTOByID(Long.parseLong(request.getParameterValues("prescriptionLabTest.iD")[index]));
		}

		
				
		String Value = "";
		Value = request.getParameterValues("prescriptionLabTest.prescriptionDetailsId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		prescriptionLabTestDTO.prescriptionDetailsId = Long.parseLong(Value);
		Value = request.getParameterValues("prescriptionLabTest.labTestType")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		prescriptionLabTestDTO.labTestType = Long.parseLong(Value);
		Value = request.getParameterValues("prescriptionLabTest.labTestDetailsTypesHidden")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			prescriptionLabTestDTO.labTestDetailsTypes = (Value);
			if(prescriptionLabTestDTO.labTestDetailsTypes.endsWith(","))
			{
				prescriptionLabTestDTO.labTestDetailsTypes = prescriptionLabTestDTO.labTestDetailsTypes + " ";
			}
			System.out.println("prescriptionLabTestDTO.labTestDetailsTypes = " + prescriptionLabTestDTO.labTestDetailsTypes);
			
			String [] details = prescriptionLabTestDTO.labTestDetailsTypes.split(", ");
			prescriptionLabTestDTO.labTestDetailsTypes = "";
			List<Long> ids = new ArrayList<Long>();
			
			LabTestListDAO labTestListDAO = new LabTestListDAO();
			List<LabTestListDTO> labTestListDTOs = labTestListDAO.getLabTestListDTOListByLabTestID(prescriptionLabTestDTO.labTestType);
			
			for(String detail: details)
			{
				if(!detail.equalsIgnoreCase(""))
				{
					long id = Long.parseLong(detail);
					ids.add(id);
				}
				
			}
			
			List<Long> newIds = new ArrayList<Long>();
			
			if(ids.isEmpty() 
					&& prescriptionLabTestDTO.labTestType != SessionConstants.LAB_TEST_XRAY 
					&& prescriptionLabTestDTO.labTestType != SessionConstants.LAB_TEST_ULTRASONOGRAM)
			{
				ids.add(PrescriptionLabTestDTO.ALL);
			}
		
			
			
			
			if(ids.contains(PrescriptionLabTestDTO.ALL))
			{
				for(LabTestListDTO labTestListDTO: labTestListDTOs)
				{
					if(labTestListDTO.showInDropdown)
					{
						newIds.add(labTestListDTO.iD);
					}
					
				}
				
			}
			else
			{

				for(LabTestListDTO labTestListDTO: labTestListDTOs)
				{
					if(ids.contains(labTestListDTO.iD))
					{
						newIds.add(labTestListDTO.iD);
					}
					
				}
			}

			
			for(long id: newIds)
			{
				prescriptionLabTestDTO.labTestDetailsTypes += id + ", ";
			}
			
			System.out.println("Now prescriptionLabTestDTO.labTestDetailsTypes = " + prescriptionLabTestDTO.labTestDetailsTypes);
		}

		
		
		String [] details = prescriptionLabTestDTO.labTestDetailsTypes.split(", ");
		if(details != null && details[0] != null && details[0].equalsIgnoreCase("-3"))
		{
			prescriptionLabTestDTO.labTestDetailsTypes = "";
			LabTestListDAO labTestListDAO = new LabTestListDAO();
			List<LabTestListDTO> labTestListDTOList = labTestListDAO.getLabTestListDTOListByLabTestID(prescriptionLabTestDTO.labTestType);
			for(LabTestListDTO labTestListDTO: labTestListDTOList)
			{
				prescriptionLabTestDTO.labTestDetailsTypes += labTestListDTO.iD + ", ";
			}
		}
		
		Value = request.getParameterValues("prescriptionLabTest.description")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			prescriptionLabTestDTO.description = (Value);
		}
		
		Value = request.getParameterValues("prescriptionLabTest.xRayReportId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			prescriptionLabTestDTO.xRayReportId = Long.parseLong(Value);
		}
		
		Value = request.getParameterValues("prescriptionLabTest.alternateLabTestName")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			prescriptionLabTestDTO.alternateLabTestName = (Value);
		}
		
		Value = request.getParameterValues("prescriptionLabTest.alternateLabTestDetails")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			prescriptionLabTestDTO.alternateLabTestDetails = (Value);
		}

		return prescriptionLabTestDTO;
	
	}
	
	
	

	private void deletePrescription_details(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				
				Prescription_detailsDTO prescription_detailsDTO = prescription_detailsDAO.getDTOByID(id);
				prescription_detailsDAO.manageWriteOperations(prescription_detailsDTO, SessionConstants.DELETE, id, userDTO);
				patientPrescriptionMedicine2DAO.deleteChildrenByParent(prescription_detailsDTO.iD, "prescription_details_id");
				prescriptionLabTestDAO.deleteChildrenByParent(prescription_detailsDTO.iD, "prescription_details_id");
				response.sendRedirect("Prescription_detailsServlet?actionType=search");
				
			}			
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		
	}

	private void getPrescription_details(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getPrescription_details");
		Prescription_detailsDTO prescription_detailsDTO = null;
		try 
		{
			prescription_detailsDTO = prescription_detailsDAO.getDTOByID(id);
			request.setAttribute("ID", prescription_detailsDTO.iD);
			request.setAttribute("prescription_detailsDTO",prescription_detailsDTO);
			request.setAttribute("prescription_detailsDAO",prescription_detailsDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "prescription_details/prescription_detailsInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "prescription_details/prescription_detailsSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "prescription_details/prescription_detailsEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "prescription_details/prescription_detailsEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getPrescription_details(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getPrescription_details(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchPrescription_details(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchPrescription_details 1 filter = " + filter);
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_PRESCRIPTION_DETAILS,
			request,
			prescription_detailsDAO,
			SessionConstants.VIEW_PRESCRIPTION_DETAILS,
			SessionConstants.SEARCH_PRESCRIPTION_DETAILS,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("prescription_detailsDAO",prescription_detailsDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to prescription_details/prescription_detailsApproval.jsp");
	        	rd = request.getRequestDispatcher("prescription_details/prescription_detailsApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to prescription_details/prescription_detailsApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("prescription_details/prescription_detailsApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to prescription_details/prescription_detailsSearch.jsp");
	        	rd = request.getRequestDispatcher("prescription_details/prescription_detailsSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to prescription_details/prescription_detailsSearchForm.jsp");
	        	rd = request.getRequestDispatcher("prescription_details/prescription_detailsSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

