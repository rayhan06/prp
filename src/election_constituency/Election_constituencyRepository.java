package election_constituency;

import election_details.Election_detailsDTO;
import election_details.Election_detailsRepository;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.StringUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@SuppressWarnings({"unused"})
public class Election_constituencyRepository implements Repository {
    private final Election_constituencyDAO election_constituencyDAO;

    private static final Logger logger = Logger.getLogger(Election_constituencyRepository.class);

    private final Map<Long, Election_constituencyDTO> mapById;
    private final Map<Long, List<Election_constituencyDTO>> mapByElectionDetails;

    private static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private static final ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
    private static final ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();

    private Election_constituencyRepository() {
        mapById = new ConcurrentHashMap<>();
        mapByElectionDetails = new ConcurrentHashMap<>();

        election_constituencyDAO = Election_constituencyDAO.getInstance();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        static Election_constituencyRepository INSTANCE = new Election_constituencyRepository();
    }

    public static Election_constituencyRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        logger.debug("Election_constituencyRepository reload start reloadAll : " + reloadAll);
        List<Election_constituencyDTO> list = election_constituencyDAO.getAllDTOs(reloadAll);
        List<Election_constituencyDTO> tempList = list.stream()
                .filter(dto -> dto.isDeleted == 0).collect(Collectors.toList());
        if (reloadAll) {
            tempList.forEach(dto -> {
                mapById.put(dto.iD, dto);
                List<Election_constituencyDTO> electionConstituencyDTOList = mapByElectionDetails.getOrDefault(dto.electionDetailsType, new ArrayList<>());
                electionConstituencyDTOList.add(dto);
                mapByElectionDetails.put(dto.electionDetailsType, electionConstituencyDTOList);
            });
            tempList.stream()
                    .map(dto -> dto.electionDetailsType)
                    .collect(Collectors.toSet())
                    .forEach(electionDetailsType -> mapByElectionDetails.get(electionDetailsType).sort(Comparator.comparingInt(o -> o.constituencyNumber)));
        } else {
            addToMaps(tempList);
        }


        logger.debug("Election_constituencyRepository reload end reloadAll : " + reloadAll);
    }

    private void addToMaps(List<Election_constituencyDTO> dtos) {
        try {
            writeLock.lock();
            for (Election_constituencyDTO dto : dtos) {
                Election_constituencyDTO oldDTO = mapById.get(dto.iD);
                if (oldDTO == null || dto.lastModificationTime > oldDTO.lastModificationTime) {
                    if (oldDTO != null) {
                        mapById.remove(dto.iD);
                        mapByElectionDetails.get(oldDTO.electionDetailsType).remove(oldDTO);
                    }
                    if (dto.isDeleted != 0) {
                        mapById.put(dto.iD, dto);
                        List<Election_constituencyDTO> list = mapByElectionDetails.getOrDefault(dto.electionDetailsType, new ArrayList<>());
                        list.add(dto);
                        mapByElectionDetails.put(dto.electionDetailsType, list);
                    }
                }
            }
            dtos.stream()
                    .map(dto -> dto.electionDetailsType)
                    .collect(Collectors.toSet())
                    .forEach(electionDetailsType -> mapByElectionDetails.get(electionDetailsType).sort(Comparator.comparingInt(o -> o.constituencyNumber)));
        } finally {
            writeLock.unlock();
        }
    }


    public List<Election_constituencyDTO> getElection_constituencyList() {
        List<Election_constituencyDTO> electionConstituencyDTOList;
        try {
            readLock.lock();
            electionConstituencyDTOList = new ArrayList<>(this.mapById.values());
        } finally {
            readLock.unlock();
        }
        return electionConstituencyDTOList;
    }

    public Election_constituencyDTO getElection_constituencyDTOByID(long ID) {
        Election_constituencyDTO dto;
        try {
            readLock.lock();
            dto = mapById.get(ID);
            if (dto == null) {
                dto = election_constituencyDAO.getDTOFromID(ID);
                if (dto != null) {

                    ExecutorService executorService = Executors.newSingleThreadExecutor((r) -> {
                        Thread thread = new Thread(r);
                        thread.setDaemon(true);
                        return thread;
                    });

                    Election_constituencyDTO finalDto = dto;
                    executorService.execute(
                            () -> addToMaps(Collections.singletonList(finalDto))

                    );
                    executorService.shutdown();
                }

            }
        } finally {
            readLock.unlock();
        }
        return dto;

    }

    public Election_constituencyDTO getElection_constituencyDTOByConstituencyNumber(long electionDetailsType, int constituencyNumber) {
        Election_constituencyDTO election_constituencyDTO;
        try {
            readLock.lock();
            election_constituencyDTO = mapByElectionDetails.get(electionDetailsType)
                    .parallelStream()
                    .filter(dto -> dto.constituencyNumber == constituencyNumber)
                    .findAny()
                    .orElse(null);
        } finally {
            readLock.unlock();
        }
        return election_constituencyDTO;
    }

    public Map<Long, Election_constituencyDTO> getByIdList(List<Long> ids) {
        Map<Long, Election_constituencyDTO> constituencyDTOMap;
        try {
            readLock.lock();
            constituencyDTOMap = ids.stream()
                    .filter(id -> mapById.get(id) != null)
                    .map(mapById::get)
                    .collect(Collectors.toMap(e -> e.iD, e -> e, (e1, e2) -> e1));
        } finally {
            readLock.unlock();
        }
        return constituencyDTOMap;
    }

    @Override
    public String getTableName() {
        return "election_constituency";
    }

    public String getElectionConstituteById(Long id, String language) {
        Election_constituencyDTO dto = getElection_constituencyDTOByID(id);
        if (language.equalsIgnoreCase("English")) {
            return String.valueOf(dto.constituencyNumber).concat(" | ").concat(dto.constituencyNameEn);
        } else {
            return StringUtils.convertToBanNumber(String.valueOf(dto.constituencyNumber)).concat(" | ").concat(dto.constituencyNameBn);
        }
    }

    public String buildOptions(String language, Long selectedId) {
        return buildOptions(language, selectedId, getElection_constituencyList());
    }

    private OptionDTO buildOptionDTO(Election_constituencyDTO dto) {
        return new OptionDTO(String.valueOf(dto.constituencyNumber).concat(" | ").concat(dto.constituencyNameEn),
                StringUtils.convertToBanNumber(String.valueOf(dto.constituencyNumber)).concat(" | ").concat(dto.constituencyNameBn),
                String.valueOf(dto.iD));
    }

    public String buildOptions(String language, Long selectedId, List<Election_constituencyDTO> list) {
        List<OptionDTO> optionDTOList = null;
        if (list != null && list.size() > 0) {
            list.sort(Comparator.comparingInt(o -> o.constituencyNumber));
            optionDTOList = list.stream()
                    .map(this::buildOptionDTO)
                    .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String buildOptions(String language, Long selectedId, long electionDetailsType) {
        return buildOptions(language, selectedId, getElectionConstituencyListByElectionDetailsId(electionDetailsType));
    }

    public String buildOptions(String language, String employeeNumber) {
        int parliament = Integer.parseInt(employeeNumber.substring(1, 3));
        int constituency = Integer.parseInt(employeeNumber.substring(3, 6));
        Election_detailsDTO electionDetailsDTO = Election_detailsRepository.getInstance().getElection_detailsDTOByParliamentNumber(parliament);

        List<Election_constituencyDTO> dtoList = getElectionConstituencyListByElectionDetailsId(electionDetailsDTO.iD);
        Long constituencyId = dtoList.parallelStream()
                .filter(dto -> dto.constituencyNumber == constituency)
                .findAny()
                .map(dto -> dto.iD)
                .orElse(0L);

        return buildOptions(language, constituencyId, dtoList);
    }

    public String getText(long id, String language) {
        return getText(id, "English".equalsIgnoreCase(language));
    }

    public String getTextWithConstituencyNumber(long id, String language) {
        Election_constituencyDTO dto = getElection_constituencyDTOByID(id);
        return dto == null ? "" : ("English".equalsIgnoreCase(language) ? dto.constituencyNumber + "| " +
                dto.constituencyNameEn : Utils.getDigits(dto.constituencyNumber, language) + "| " + dto.constituencyNameBn);
    }

    public String getText(long id, boolean isLangEng) {
        Election_constituencyDTO dto = getElection_constituencyDTOByID(id);
        return dto == null ? "" : (isLangEng ? dto.constituencyNameEn : dto.constituencyNameBn);
    }

    public String getConstituencyNumber(long id) {
        Election_constituencyDTO dto = getElection_constituencyDTOByID(id);
        return dto == null ? "" : (dto.constituencyNumber + "");
    }

    public Set<Long> getIdsByCondition(Predicate<Election_constituencyDTO> filterFunction) {
        Set<Long> ids;
        try {
            readLock.lock();
            ids = getElection_constituencyList().stream()
                    .filter(filterFunction)
                    .map(dto -> dto.iD)
                    .collect(Collectors.toSet());
        } finally {
            readLock.unlock();

        }
        return ids;
    }

    public String getConstituencyText(long id, String language) {
        Election_constituencyDTO dto = getElection_constituencyDTOByID(id);
        return dto == null ? "" : ("English".equalsIgnoreCase(language) ? String.valueOf(dto.constituencyNumber).concat(" | ").concat(dto.constituencyNameEn)
                : StringUtils.convertToBanNumber(String.valueOf(dto.constituencyNumber)).concat(" | ").concat(dto.constituencyNameBn));
    }

    public List<Election_constituencyDTO> getElectionConstituencyListByElectionDetailsId(long electionDetailsId) {
        List<Election_constituencyDTO> electionConstituencyDTOList;
        try {
            readLock.lock();
            electionConstituencyDTOList = new ArrayList<>(mapByElectionDetails.get(electionDetailsId));
        } finally {
            readLock.unlock();
        }
        return electionConstituencyDTOList;
    }
}


