package election_constituency;

import util.CommonDTO;


public class Election_constituencyDTO extends CommonDTO {

    public long electionDetailsType = 0;
    public long geoDivisionsType = 0;
    public long geoDistrictsType = 0;
    public String thana = "";
    public int constituencyNumber = 0;
    public String constituencyNameEn = "";
    public String constituencyNameBn = "";
    public String boundaryDetails = "";
    public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
    public int isVacant = 1;
    public int isReserved = 0;


    @Override
    public String toString() {
        return "$Election_constituencyDTO[" +
                " iD = " + iD +
                " electionDetailsType = " + electionDetailsType +
                " geoDivisionType = " + geoDivisionsType +
                " geoDistrictType = " + geoDistrictsType +
                " geoThanaType = " + thana +
                " constituencyNumber = " + constituencyNumber +
                " constituencyNameEn = " + constituencyNameEn +
                " constituencyNameBn = " + constituencyNameBn +
                " boundaryDetails = " + boundaryDetails +
                " insertionDate = " + insertionDate +
                " insertedBy = " + insertedBy +
                " modifiedBy = " + modifiedBy +
                " searchColumn = " + searchColumn +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                " isVacant = " + isVacant +
                " isReserved = " + isReserved +
                "]";
    }

}