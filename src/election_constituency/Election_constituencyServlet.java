package election_constituency;

import common.BaseServlet;
import common.CommonDAOService;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

@WebServlet("/Election_constituencyServlet")
@MultipartConfig
public class Election_constituencyServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    private final Election_constituencyDAO election_constituencyDAO = Election_constituencyDAO.getInstance();

    @Override
    public String getTableName() {
        return election_constituencyDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Election_constituencyServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return election_constituencyDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Election_constituencyDTO election_constituencyDTO;
        long requestTime = Calendar.getInstance().getTimeInMillis();
        if (addFlag) {
            election_constituencyDTO = new Election_constituencyDTO();
            election_constituencyDTO.insertedBy = userDTO.userName;
            election_constituencyDTO.insertionDate = requestTime;
        } else {
            election_constituencyDTO = election_constituencyDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        election_constituencyDTO.modifiedBy = userDTO.userName;
        election_constituencyDTO.lastModificationTime = requestTime;

        election_constituencyDTO.electionDetailsType = Long.parseLong(request.getParameter("electionDetailsType"));

        try {
            election_constituencyDTO.geoDivisionsType = Long.parseLong(request.getParameter("geoDivisionType"));
        } catch (Exception ex) {
            logger.debug("no division is set in the election constituency!");
        }

        try {
            election_constituencyDTO.geoDistrictsType = Long.parseLong(request.getParameter("geoDistrictType"));
        } catch (Exception ex) {
            logger.debug("no district is set in the election constituency!");
        }

        try {
            election_constituencyDTO.thana = Jsoup.clean(request.getParameter("geoThanaType").trim(), Whitelist.simpleText());
        } catch (Exception ex) {
            logger.debug("no thana is set in the election constituency!");
        }

        election_constituencyDTO.constituencyNumber = Integer.parseInt(request.getParameter("constituencyNumber").trim());
        election_constituencyDTO.constituencyNameEn = Jsoup.clean(request.getParameter("constituencyNameEn").trim(), Whitelist.simpleText());
        election_constituencyDTO.constituencyNameBn = Jsoup.clean(request.getParameter("constituencyNameBn").trim(), Whitelist.simpleText());

        election_constituencyDTO.isVacant = Integer.parseInt(request.getParameter("isVacant").trim());

        if (request.getParameter("isReserved") != null && request.getParameter("isReserved").equals("true"))
            election_constituencyDTO.isReserved = 1;
        else
            election_constituencyDTO.isReserved = 0;

        if (request.getParameter("boundaryDetails") == null) {
            election_constituencyDTO.boundaryDetails = "";
        } else {
            election_constituencyDTO.boundaryDetails = Jsoup.clean(request.getParameter("boundaryDetails").trim(), Whitelist.simpleText());
        }

        if (addFlag) {
            election_constituencyDAO.add(election_constituencyDTO);
        } else {
            election_constituencyDAO.update(election_constituencyDTO);
        }
        return election_constituencyDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.ELECTION_CONSTITUENCY_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.ELECTION_CONSTITUENCY_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.ELECTION_CONSTITUENCY_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Election_constituencyServlet.class;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("actionType").equals("buildElectionConstituency")) {
            long electionId = Long.parseLong(request.getParameter("election_id"));
            String language = request.getParameter("language");
            Long constituency = Utils.convertToLong(request.getParameter("constituency"));
            String options = Election_constituencyRepository.getInstance().buildOptions(language, constituency, electionId);
            PrintWriter out = response.getWriter();
            out.println(options);
            out.close();
            return;
        }
        super.doGet(request, response);
    }
}