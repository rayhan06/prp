package election_constituency;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import election_details.Election_detailsDTO;
import election_details.Election_detailsRepository;
import geolocation.*;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"unused","Duplicates"})
public class Election_constituencyDAO implements CommonDAOService<Election_constituencyDTO> {

    private static final Logger logger = Logger.getLogger(Election_constituencyDAO.class);

    private static final String addSqlQuery = "INSERT INTO {tableName} (election_details_type,geo_divisions_type,geo_districts_type,thana,"
            .concat(" constituency_number,constituency_name_en,constituency_name_bn,boundary_details,insertion_date,inserted_by,modified_by,search_column,")
            .concat(" lastModificationTime,IS_VACANT,IS_RESERVED,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery = "UPDATE {tableName} SET election_details_type=?,geo_divisions_type=?,geo_districts_type=?,thana=?,"
            .concat(" constituency_number=?,constituency_name_en=?,constituency_name_bn=?,boundary_details=?,insertion_date=?,inserted_by=?,")
            .concat(" modified_by=?,search_column=?,lastModificationTime = ?,IS_VACANT = ?,IS_RESERVED = ? WHERE ID = ?");

    private static final String getByConstituencyNumberAndElectionId = "SELECT * FROM election_constituency WHERE constituency_number = ? AND election_details_type = ?";

    private static Election_constituencyDAO INSTANCE = null;

    private final Map<String, String> searchMap = new HashMap<>();

    private Election_constituencyDAO() {
        searchMap.put("election_details_type", "and (election_details_type = ?)");
        searchMap.put("geo_division_type", "and (geo_divisions_type = ?)");
        searchMap.put("geo_district_type", "and (geo_districts_type = ?)");
        searchMap.put("geo_thana_type", "and (thana like ?)");
        searchMap.put("constituency_number", "and (constituency_number = ?)");
        searchMap.put("constituency_name_en", "and (constituency_name_en like ?)");
        searchMap.put("constituency_name_bn", "and (constituency_name_bn like ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    public static Election_constituencyDAO getInstance() {
        if (INSTANCE == null) {
            synchronized (Election_constituencyDAO.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Election_constituencyDAO();
                }
            }
        }
        return INSTANCE;
    }

    public void setSearchColumn(Election_constituencyDTO dto) {
        StringBuilder column = new StringBuilder();
        Election_detailsDTO election_detailsDTO = Election_detailsRepository.getInstance().getElectionDetailsDTOByID(dto.electionDetailsType);
        if (election_detailsDTO != null)
            column.append(election_detailsDTO.parliamentNumber).append(" ");
        GeoDivisionDTO divisionDTO = GeoDivisionRepository.getInstance().getById(dto.geoDivisionsType);
        if (divisionDTO != null) {
            column.append(divisionDTO.nameBan).append(" ");
            column.append(divisionDTO.nameEng).append(" ");
        }
        GeoDistrictDTO districtDTO = GeoDistrictRepository.getInstance().getById(dto.geoDistrictsType);
        if (districtDTO != null) {
            column.append(districtDTO.nameBan).append(" ");
            column.append(districtDTO.nameEng).append(" ");
        }
        column.append(dto.constituencyNumber).append(" ");
        column.append(dto.constituencyNameEn).append(" ");
        column.append(dto.constituencyNameBn).append(" ");

        column.append(dto.thana).append(" ");
        if (dto.boundaryDetails != null && dto.boundaryDetails.trim().length() > 0) {
            column.append(dto.boundaryDetails.trim());
        }
        dto.searchColumn = column.toString();
    }

    @Override
    public void set(PreparedStatement ps, Election_constituencyDTO election_constituencyDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(election_constituencyDTO);
        ps.setObject(++index, election_constituencyDTO.electionDetailsType);
        ps.setObject(++index, election_constituencyDTO.geoDivisionsType);
        ps.setObject(++index, election_constituencyDTO.geoDistrictsType);
        ps.setObject(++index, election_constituencyDTO.thana);
        ps.setObject(++index, election_constituencyDTO.constituencyNumber);
        ps.setObject(++index, election_constituencyDTO.constituencyNameEn);
        ps.setObject(++index, election_constituencyDTO.constituencyNameBn);
        ps.setObject(++index, election_constituencyDTO.boundaryDetails);
        ps.setObject(++index, election_constituencyDTO.insertionDate);
        ps.setObject(++index, election_constituencyDTO.insertedBy);
        ps.setObject(++index, election_constituencyDTO.modifiedBy);
        ps.setObject(++index, election_constituencyDTO.searchColumn);
        ps.setObject(++index, System.currentTimeMillis());
        ps.setObject(++index, election_constituencyDTO.isVacant);
        ps.setObject(++index, election_constituencyDTO.isReserved);
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, election_constituencyDTO.iD);
    }

    @Override
    public Election_constituencyDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Election_constituencyDTO election_constituencyDTO = new Election_constituencyDTO();
            election_constituencyDTO.iD = rs.getLong("ID");
            election_constituencyDTO.electionDetailsType = rs.getLong("election_details_type");
            election_constituencyDTO.geoDivisionsType = rs.getLong("geo_divisions_type");
            election_constituencyDTO.geoDistrictsType = rs.getLong("geo_districts_type");
            election_constituencyDTO.thana = rs.getString("thana");
            election_constituencyDTO.constituencyNumber = rs.getInt("constituency_number");
            election_constituencyDTO.constituencyNameEn = rs.getString("constituency_name_en");
            election_constituencyDTO.constituencyNameBn = rs.getString("constituency_name_bn");
            election_constituencyDTO.boundaryDetails = rs.getString("boundary_details");
            election_constituencyDTO.insertionDate = rs.getLong("insertion_date");
            election_constituencyDTO.insertedBy = rs.getString("inserted_by");
            election_constituencyDTO.modifiedBy = rs.getString("modified_by");
            election_constituencyDTO.searchColumn = rs.getString("search_column");
            election_constituencyDTO.isDeleted = rs.getInt("isDeleted");
            election_constituencyDTO.lastModificationTime = rs.getLong("lastModificationTime");
            election_constituencyDTO.isVacant = rs.getInt("IS_VACANT");
            election_constituencyDTO.isReserved = rs.getInt("IS_RESERVED");
            return election_constituencyDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "election_constituency";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    private Election_constituencyDTO buildObjectFromResultSetConsumeException(ResultSet rs) {
        return buildObjectFromResultSet(rs);
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Election_constituencyDTO) commonDTO, addSqlQuery, true);
    }

    public int getConstituencyNumberById(long id) {
        return getDTOFromID(id).constituencyNumber;
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Election_constituencyDTO) commonDTO, updateSqlQuery, false);
    }

    public Election_constituencyDTO getByConstituencyNumberAndElectionId(int constituencyNumber, long electionDetailsId) {
        return ConnectionAndStatementUtil.getT(getByConstituencyNumberAndElectionId, ps -> {
            try {
                ps.setInt(1, constituencyNumber);
                ps.setLong(2, electionDetailsId);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, this::buildObjectFromResultSetConsumeException);
    }

}
	