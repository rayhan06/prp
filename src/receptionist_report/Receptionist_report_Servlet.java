package receptionist_report;


import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Receptionist_report_Servlet")
public class Receptionist_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{				
		{"criteria","appointment","insertion_date",">=","","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","appointment","insertion_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},
		{"criteria","appointment","inserted_by_erid","=","AND","String","","","any","inserted_by_user_name",LC.HM_EMPLOYEE_ID + "", "userNameToEmployeeRecordId"},
	};
	
	String[][] Display =
	{
		{"display","","inserted_by_erid","erIdToName",""},		
		{"display","","COUNT(appointment.id)","text",""},
		{"display", "", "appointment.inserted_by_erid", "invisible", ""}
		          
	};
	
	String GroupBy = "inserted_by_erid";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Receptionist_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = " appointment ";

		Display[0][4] = LM.getText(LC.RECEPTIONIST_REPORT_SELECT_INSERTEDBYUSERNAME, loginDTO);
		Display[1][4] = LM.getText(LC.RECEPTIONIST_REPORT_SELECT_COUNTID, loginDTO);
		Display[2][4] = "";

		
		String reportName = LM.getText(LC.RECEPTIONIST_REPORT_OTHER_RECEPTIONIST_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(1, ReportRequestHandler.RIGHT_ALIGN_INT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "receptionist_report",
				MenuConstants.RECEPTIONIST_REPORT_DETAILS, language, reportName, "receptionist_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
