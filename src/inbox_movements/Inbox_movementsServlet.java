//package inbox_movements;
//
//import java.io.IOException;
//import java.io.*;
//
//
//import javax.servlet.RequestDispatcher;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.MultipartConfig;
//import javax.servlet.annotation.WebServlet;
//
//
//import org.apache.log4j.Logger;
//
//import login.LoginDTO;
//import permission.MenuConstants;
//import role.PermissionRepository;
//
//
//import sessionmanager.SessionConstants;
//
//import user.UserDTO;
//import user.UserRepository;
//
//import util.RecordNavigationManager3;
//
//import java.util.*;
//import javax.servlet.http.*;
//import java.util.UUID;
//
//import inbox_movements.Constants;
//import approval_module_map.*;
//
//
//import com.google.gson.Gson;
//
//import pb.*;
//import pbReport.*;
//import org.jsoup.Jsoup;
//import org.jsoup.safety.Whitelist;
//
///**
// * Servlet implementation class Inbox_movementsServlet
// */
//@WebServlet("/Inbox_movementsServlet")
//@MultipartConfig
//public class Inbox_movementsServlet extends HttpServlet 
//{
//	private static final long serialVersionUID = 1L;
//    public static Logger logger = Logger.getLogger(Inbox_movementsServlet.class);
//	Approval_module_mapDAO approval_module_mapDAO = new Approval_module_mapDAO();
//    Approval_module_mapDTO approval_module_mapDTO;
//    String tableName = "inbox_movements";
//    String tempTableName = "inbox_movements_temp";
//	Inbox_movementsDAO inbox_movementsDAO;
//    private Gson gson = new Gson();
//    
//    /**
//     * @see HttpServlet#HttpServlet()
//     */
//    public Inbox_movementsServlet() 
//	{
//        super();
//    	try
//    	{
//			approval_module_mapDTO = approval_module_mapDAO.getApproval_module_mapDTOByTableName("inbox_movements");
//			inbox_movementsDAO = new Inbox_movementsDAO(tableName, tempTableName, approval_module_mapDTO);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//    }   
//
//	/**
//	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
//	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
//	{
//		System.out.println("In doget request = " + request);
//		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		
//	
//		boolean isPermanentTable = true;
//		if(request.getParameter("isPermanentTable") != null)
//		{
//			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
//		}
//		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
//		try
//		{
//			String actionType = request.getParameter("actionType");
//			if(actionType.equals("getAddPage"))
//			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_MOVEMENTS_ADD))
//				{
//					getAddPage(request, response);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			}
//			else if(actionType.equals("getEditPage"))
//			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_MOVEMENTS_UPDATE))
//				{
//					if(isPermanentTable)
//					{
//						getInbox_movements(request, response, tableName);
//					}
//					else
//					{
//						getInbox_movements(request, response, tempTableName);
//					}
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}						
//			}
//			else if(actionType.equals("getURL"))
//			{
//				String URL = request.getParameter("URL");
//				System.out.println("URL = " + URL);
//				response.sendRedirect(URL);			
//			}
//			else if(actionType.equals("search"))
//			{
//				System.out.println("search requested");
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_MOVEMENTS_SEARCH))
//				{
//					
//					if(isPermanentTable)
//					{
//						searchInbox_movements(request, response, tableName, isPermanentTable);
//					}
//					else
//					{
//						searchInbox_movements(request, response, tempTableName, isPermanentTable);
//					}
//				}			
//			}
//			else if(actionType.equals("getApprovalPage"))
//			{
//				System.out.println("getApprovalPage requested");
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_MOVEMENTS_SEARCH))
//				{
//					searchInbox_movements(request, response, tempTableName, false);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}				
//			}
//			else
//			{
//				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//			}
//		}
//		catch(Exception ex)
//		{
//			ex.printStackTrace();
//			logger.debug(ex);
//		}
//	}
//
//	private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
//	{
//		request.setAttribute("ID", -1L);
//		RequestDispatcher requestDispatcher = request.getRequestDispatcher("inbox_movements/inbox_movementsEdit.jsp");
//		requestDispatcher.forward(request, response);
//	}
//	private String getFileName(final Part part) 
//	{
//	    final String partHeader = part.getHeader("content-disposition");
//	    System.out.println("Part Header = {0}" +  partHeader);
//	    for (String content : part.getHeader("content-disposition").split(";")) {
//	        if (content.trim().startsWith("filename")) {
//	            return content.substring(
//	                    content.indexOf('=') + 1).trim().replace("\"", "");
//	        }
//	    }
//	    return null;
//	}
//
//	
//	
//
//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
//	{
//		// TODO Auto-generated method stub
//		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		
//	
//		System.out.println("doPost");
//		boolean isPermanentTable = true;
//		if(request.getParameter("isPermanentTable") != null)
//		{
//			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
//		}
//		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
//		
//		try
//		{
//			String actionType = request.getParameter("actionType");
//			System.out.println("actionType = " + actionType);
//			if(actionType.equals("add"))
//			{
//				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_MOVEMENTS_ADD))
//				{
//					System.out.println("going to  addInbox_movements ");
//					addInbox_movements(request, response, true, userDTO, tableName, true);
//				}
//				else
//				{
//					System.out.println("Not going to  addInbox_movements ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//				
//			}
//			if(actionType.equals("approve"))
//			{
//				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_MOVEMENTS_ADD))
//				{					
//					approveInbox_movements(request, response, true, userDTO);
//				}
//				else
//				{
//					System.out.println("Not going to  addInbox_movements ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//				
//			}
//			if(actionType.equals("getDTO"))
//			{
//				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_MOVEMENTS_ADD))
//				{
//					if(isPermanentTable)
//					{
//						getDTO(request, response, tableName);
//					}
//					else
//					{
//						getDTO(request, response, tempTableName);
//					}
//				}
//				else
//				{
//					System.out.println("Not going to  addInbox_movements ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//				
//			}
//			else if(actionType.equals("edit"))
//			{
//				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_MOVEMENTS_UPDATE))
//				{
//					if(isPermanentTable)
//					{
//						addInbox_movements(request, response, false, userDTO, tableName, isPermanentTable);
//					}
//					else
//					{
//						addInbox_movements(request, response, false, userDTO, tempTableName, isPermanentTable);
//					}
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			}
//			else if(actionType.equals("delete"))
//			{								
//				deleteInbox_movements(request, response, userDTO, isPermanentTable);				
//			}	
//			else if(actionType.equals("search"))
//			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_MOVEMENTS_SEARCH))
//				{
//					searchInbox_movements(request, response, tableName, true);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			}
//			else if(actionType.equals("getGeo"))
//			{
//				System.out.println("going to geoloc ");
//				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
//			}
//			
//		}
//		catch(Exception ex)
//		{
//			ex.printStackTrace();
//			logger.debug(ex);
//		}
//	}
//	
//	private void getDTO(HttpServletRequest request, HttpServletResponse response, String tableName) 
//	{
//		try 
//		{
//			System.out.println("In getDTO");
//			Inbox_movementsDTO inbox_movementsDTO = (Inbox_movementsDTO)inbox_movementsDAO.getDTOByID(Long.parseLong(request.getParameter("ID")), tableName);
//			PrintWriter out = response.getWriter();
//			response.setContentType("application/json");
//			response.setCharacterEncoding("UTF-8");
//			
//			String encoded = this.gson.toJson(inbox_movementsDTO);
//			System.out.println("json encoded data = " + encoded);
//			out.print(encoded);
//			out.flush();
//		}
//		catch (NumberFormatException e) 
//		{
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//	}
//	private void approveInbox_movements(HttpServletRequest request, HttpServletResponse response, boolean b, UserDTO userDTO) 
//	{
//		try
//		{
//			long id = Long.parseLong(request.getParameter("idToApprove"));
//			Inbox_movementsDTO inbox_movementsDTO = (Inbox_movementsDTO)inbox_movementsDAO.getDTOByID(id, tempTableName);
//			inbox_movementsDAO.manageWriteOperations(inbox_movementsDTO, SessionConstants.APPROVE, id, userDTO);
//		}
//		catch (Exception e) 
//		{
//			e.printStackTrace();
//		}
//		
//	}
//	private void addInbox_movements(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, String tableName, boolean isPermanentTable) throws IOException 
//	{
//		// TODO Auto-generated method stub
//		try 
//		{
//			request.setAttribute("failureMessage", "");
//			System.out.println("%%%% addInbox_movements");
//			String path = getServletContext().getRealPath("/img2/");
//			Inbox_movementsDTO inbox_movementsDTO;
//			String FileNamePrefix;
//			if(addFlag == true)
//			{
//				inbox_movementsDTO = new Inbox_movementsDTO();
//				FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
//			}
//			else
//			{
//				inbox_movementsDTO = (Inbox_movementsDTO)inbox_movementsDAO.getDTOByID(Long.parseLong(request.getParameter("identity")), tableName);
//				FileNamePrefix = request.getParameter("identity");
//			}
//			
//			String Value = "";
//			Value = request.getParameter("inboxId");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("inboxId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_movementsDTO.inboxId = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("note");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("note = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_movementsDTO.note = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("action");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("action = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_movementsDTO.action = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("toEmployeeUserId");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("toEmployeeUserId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_movementsDTO.toEmployeeUserId = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("fromEmployeeUserId");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("fromEmployeeUserId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_movementsDTO.fromEmployeeUserId = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("toOrganogramId");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("toOrganogramId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_movementsDTO.toOrganogramId = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("fromOrganogramId");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("fromOrganogramId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_movementsDTO.fromOrganogramId = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("isCurrent");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("isCurrent = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_movementsDTO.isCurrent = Boolean.parseBoolean(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("isCc");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("isCc = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_movementsDTO.isCc = Boolean.parseBoolean(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("createdAt");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("createdAt = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_movementsDTO.createdAt = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("modifiedAt");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("modifiedAt = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_movementsDTO.modifiedAt = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("createdBy");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("createdBy = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_movementsDTO.createdBy = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("modifiedBy");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("modifiedBy = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_movementsDTO.modifiedBy = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("deadlineDate");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("deadlineDate = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_movementsDTO.deadlineDate = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("currentStatus");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("currentStatus = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_movementsDTO.currentStatus = Integer.parseInt(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("isSeen");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("isSeen = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_movementsDTO.isSeen = Boolean.parseBoolean(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("assignedRole");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("assignedRole = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_movementsDTO.assignedRole = Integer.parseInt(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			
//			System.out.println("Done adding  addInbox_movements dto = " + inbox_movementsDTO);
//			
//			if(addFlag == true)
//			{
//				inbox_movementsDAO.manageWriteOperations(inbox_movementsDTO, SessionConstants.INSERT, -1, userDTO);
//			}
//			else
//			{
//				if(isPermanentTable)
//				{
//					inbox_movementsDAO.manageWriteOperations(inbox_movementsDTO, SessionConstants.UPDATE, -1, userDTO);
//				}
//				else
//				{
//					inbox_movementsDAO.manageWriteOperations(inbox_movementsDTO, SessionConstants.VALIDATE, -1, userDTO);
//				}
//				
//			}
//			
//			
//			
//			
//			
//			
//			
//			
//			String inPlaceSubmit = (String)request.getParameter("inplacesubmit");
//			
//			if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
//			{
//				getInbox_movements(request, response, tableName);
//			}
//			else
//			{
//				response.sendRedirect("Inbox_movementsServlet?actionType=search");
//			}
//					
//		}
//		catch (Exception e) 
//		{
//			e.printStackTrace();
//		}
//	}
//	
//	
//
//
//
//	
//	
//	
//
//	private void deleteInbox_movements(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, boolean deleteOrReject) throws IOException 
//	{				
//		try 
//		{
//			String[] IDsToDelete = request.getParameterValues("ID");
//			for(int i = 0; i < IDsToDelete.length; i ++)
//			{
//				long id = Long.parseLong(IDsToDelete[i]);
//				System.out.println("------ DELETING " + IDsToDelete[i]);
//				
//				if(deleteOrReject)
//				{
//					Inbox_movementsDTO inbox_movementsDTO = (Inbox_movementsDTO)inbox_movementsDAO.getDTOByID(id);
//					inbox_movementsDAO.manageWriteOperations(inbox_movementsDTO, SessionConstants.DELETE, id, userDTO);
//					response.sendRedirect("Inbox_movementsServlet?actionType=search");
//				}
//				else
//				{
//					Inbox_movementsDTO inbox_movementsDTO = (Inbox_movementsDTO)inbox_movementsDAO.getDTOByID(id, tempTableName);
//					inbox_movementsDAO.manageWriteOperations(inbox_movementsDTO, SessionConstants.REJECT, id, userDTO);
//					response.sendRedirect("Inbox_movementsServlet?actionType=getApprovalPage");
//				}
//			}			
//		}
//		catch (Exception ex) 
//		{
//			ex.printStackTrace();
//		}
//		
//	}
//
//	private void getInbox_movements(HttpServletRequest request, HttpServletResponse response, String tableName) throws ServletException, IOException
//	{
//		System.out.println("in getInbox_movements");
//		Inbox_movementsDTO inbox_movementsDTO = null;
//		try 
//		{
//			inbox_movementsDTO = (Inbox_movementsDTO)inbox_movementsDAO.getDTOByID(Long.parseLong(request.getParameter("ID")), tableName);
//			request.setAttribute("ID", inbox_movementsDTO.iD);
//			request.setAttribute("inbox_movementsDTO",inbox_movementsDTO);
//			request.setAttribute("inbox_movementsDAO",inbox_movementsDAO);
//			
//			String URL= "";
//			
//			String inPlaceEdit = (String)request.getParameter("inplaceedit");
//			String inPlaceSubmit = (String)request.getParameter("inplacesubmit");
//			String getBodyOnly = (String)request.getParameter("getBodyOnly");
//			
//			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
//			{
//				URL = "inbox_movements/inbox_movementsInPlaceEdit.jsp";	
//				request.setAttribute("inplaceedit","");				
//			}
//			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
//			{
//				URL = "inbox_movements/inbox_movementsSearchRow.jsp";
//				request.setAttribute("inplacesubmit","");					
//			}
//			else
//			{
//				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
//				{
//					URL = "inbox_movements/inbox_movementsEditBody.jsp?actionType=edit";
//				}
//				else
//				{
//					URL = "inbox_movements/inbox_movementsEdit.jsp?actionType=edit";
//				}				
//			}
//			
//			RequestDispatcher rd = request.getRequestDispatcher(URL);
//			rd.forward(request, response);
//		}
//		catch (NumberFormatException e) 
//		{
//			e.printStackTrace();
//		}
//		catch (Exception e) 
//		{
//			e.printStackTrace();
//		}
//	}
//	
//	private void searchInbox_movements(HttpServletRequest request, HttpServletResponse response, String tableName, boolean isPermanent) throws ServletException, IOException
//	{
//		System.out.println("in  searchInbox_movements 1");
//		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		
//	
//		String ajax = (String)request.getParameter("ajax");
//		boolean hasAjax = false;
//		if(ajax != null && !ajax.equalsIgnoreCase(""))
//		{
//			hasAjax = true;
//		}
//		System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
//		
//        RecordNavigationManager3 rnManager = new RecordNavigationManager3(
//			SessionConstants.NAV_INBOX_MOVEMENTS,
//			request,
//			inbox_movementsDAO,
//			SessionConstants.VIEW_INBOX_MOVEMENTS,
//			SessionConstants.SEARCH_INBOX_MOVEMENTS,
//			tableName,
//			isPermanent,
//			userDTO.approvalPathID);
//        try
//        {
//			System.out.println("trying to dojob");
//            rnManager.doJob(loginDTO);
//        }
//        catch(Exception e)
//        {
//			System.out.println("failed to dojob" + e);
//        }
//
//		request.setAttribute("inbox_movementsDAO",inbox_movementsDAO);
//        RequestDispatcher rd;
//        if(hasAjax == false)
//        {
//        	System.out.println("Going to inbox_movements/inbox_movementsSearch.jsp");
//        	rd = request.getRequestDispatcher("inbox_movements/inbox_movementsSearch.jsp");
//        }
//        else
//        {
//        	System.out.println("Going to inbox_movements/inbox_movementsSearchForm.jsp");
//        	rd = request.getRequestDispatcher("inbox_movements/inbox_movementsSearchForm.jsp");
//        }
//		rd.forward(request, response);
//	}
//	
//}
//
