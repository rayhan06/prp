package inbox_movements;
import java.util.*; 
import util.*;


public class Inbox_movementsMAPS extends CommonMaps
{	
	public Inbox_movementsMAPS(String tableName)
	{
		
		java_allfield_type_map.put("inbox_id".toLowerCase(), "Long");
		java_allfield_type_map.put("note".toLowerCase(), "String");
		java_allfield_type_map.put("action".toLowerCase(), "String");
		java_allfield_type_map.put("to_employee_user_id".toLowerCase(), "Long");
		java_allfield_type_map.put("from_employee_user_id".toLowerCase(), "Long");
		java_allfield_type_map.put("to_organogram_id".toLowerCase(), "Long");
		java_allfield_type_map.put("from_organogram_id".toLowerCase(), "Long");
		java_allfield_type_map.put("is_current".toLowerCase(), "Boolean");
		java_allfield_type_map.put("is_cc".toLowerCase(), "Boolean");
		java_allfield_type_map.put("created_at".toLowerCase(), "Long");
		java_allfield_type_map.put("modified_at".toLowerCase(), "Long");
		java_allfield_type_map.put("created_by".toLowerCase(), "Long");
		java_allfield_type_map.put("modified_by".toLowerCase(), "Long");
		java_allfield_type_map.put("deadline_date".toLowerCase(), "Long");
		java_allfield_type_map.put("current_status".toLowerCase(), "Integer");
		java_allfield_type_map.put("is_seen".toLowerCase(), "Boolean");
		java_allfield_type_map.put("assigned_role".toLowerCase(), "Integer");

		java_anyfield_search_map.put(tableName + ".inbox_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".note".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".action".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".to_employee_user_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".from_employee_user_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".to_organogram_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".from_organogram_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".is_current".toLowerCase(), "Boolean");
		java_anyfield_search_map.put(tableName + ".is_cc".toLowerCase(), "Boolean");
		java_anyfield_search_map.put(tableName + ".created_at".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".modified_at".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".created_by".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".modified_by".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".deadline_date".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".current_status".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".is_seen".toLowerCase(), "Boolean");
		java_anyfield_search_map.put(tableName + ".assigned_role".toLowerCase(), "Integer");

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("inboxId".toLowerCase(), "inboxId".toLowerCase());
		java_DTO_map.put("note".toLowerCase(), "note".toLowerCase());
		java_DTO_map.put("action".toLowerCase(), "action".toLowerCase());
		java_DTO_map.put("toEmployeeUserId".toLowerCase(), "toEmployeeUserId".toLowerCase());
		java_DTO_map.put("fromEmployeeUserId".toLowerCase(), "fromEmployeeUserId".toLowerCase());
		java_DTO_map.put("toOrganogramId".toLowerCase(), "toOrganogramId".toLowerCase());
		java_DTO_map.put("fromOrganogramId".toLowerCase(), "fromOrganogramId".toLowerCase());
		java_DTO_map.put("isCurrent".toLowerCase(), "isCurrent".toLowerCase());
		java_DTO_map.put("isCc".toLowerCase(), "isCc".toLowerCase());
		java_DTO_map.put("createdAt".toLowerCase(), "createdAt".toLowerCase());
		java_DTO_map.put("modifiedAt".toLowerCase(), "modifiedAt".toLowerCase());
		java_DTO_map.put("createdBy".toLowerCase(), "createdBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("deadlineDate".toLowerCase(), "deadlineDate".toLowerCase());
		java_DTO_map.put("currentStatus".toLowerCase(), "currentStatus".toLowerCase());
		java_DTO_map.put("isSeen".toLowerCase(), "isSeen".toLowerCase());
		java_DTO_map.put("assignedRole".toLowerCase(), "assignedRole".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("inbox_id".toLowerCase(), "inboxId".toLowerCase());
		java_SQL_map.put("note".toLowerCase(), "note".toLowerCase());
		java_SQL_map.put("action".toLowerCase(), "action".toLowerCase());
		java_SQL_map.put("to_employee_user_id".toLowerCase(), "toEmployeeUserId".toLowerCase());
		java_SQL_map.put("from_employee_user_id".toLowerCase(), "fromEmployeeUserId".toLowerCase());
		java_SQL_map.put("to_organogram_id".toLowerCase(), "toOrganogramId".toLowerCase());
		java_SQL_map.put("from_organogram_id".toLowerCase(), "fromOrganogramId".toLowerCase());
		java_SQL_map.put("is_current".toLowerCase(), "isCurrent".toLowerCase());
		java_SQL_map.put("is_cc".toLowerCase(), "isCc".toLowerCase());
		java_SQL_map.put("created_at".toLowerCase(), "createdAt".toLowerCase());
		java_SQL_map.put("modified_at".toLowerCase(), "modifiedAt".toLowerCase());
		java_SQL_map.put("created_by".toLowerCase(), "createdBy".toLowerCase());
		java_SQL_map.put("modified_by".toLowerCase(), "modifiedBy".toLowerCase());
		java_SQL_map.put("deadline_date".toLowerCase(), "deadlineDate".toLowerCase());
		java_SQL_map.put("current_status".toLowerCase(), "currentStatus".toLowerCase());
		java_SQL_map.put("is_seen".toLowerCase(), "isSeen".toLowerCase());
		java_SQL_map.put("assigned_role".toLowerCase(), "assignedRole".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Inbox Id".toLowerCase(), "inboxId".toLowerCase());
		java_Text_map.put("Note".toLowerCase(), "note".toLowerCase());
		java_Text_map.put("Action".toLowerCase(), "action".toLowerCase());
		java_Text_map.put("To Employee User Id".toLowerCase(), "toEmployeeUserId".toLowerCase());
		java_Text_map.put("From Employee User Id".toLowerCase(), "fromEmployeeUserId".toLowerCase());
		java_Text_map.put("To Organogram Id".toLowerCase(), "toOrganogramId".toLowerCase());
		java_Text_map.put("From Organogram Id".toLowerCase(), "fromOrganogramId".toLowerCase());
		java_Text_map.put("Is Current".toLowerCase(), "isCurrent".toLowerCase());
		java_Text_map.put("Is Cc".toLowerCase(), "isCc".toLowerCase());
		java_Text_map.put("Created At".toLowerCase(), "createdAt".toLowerCase());
		java_Text_map.put("Modified At".toLowerCase(), "modifiedAt".toLowerCase());
		java_Text_map.put("Created By".toLowerCase(), "createdBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Deadline Date".toLowerCase(), "deadlineDate".toLowerCase());
		java_Text_map.put("Current Status".toLowerCase(), "currentStatus".toLowerCase());
		java_Text_map.put("Is Seen".toLowerCase(), "isSeen".toLowerCase());
		java_Text_map.put("Assigned Role".toLowerCase(), "assignedRole".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}