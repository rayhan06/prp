package inbox_movements;
import java.util.*;

import project_tracker.FilesDTO;
import util.*; 


public class Inbox_movementsDTO extends CommonDTO
{

	public long ID = 0;
	public long inboxId = 0;
	public long inbox_type = 0;
    public String note = "";
    public long action = 0;
	public long toEmployeeUserId = 0;
	public long fromEmployeeUserId = 0;
	public long toOrganogramId = 0;
	public long fromOrganogramId = 0;
	//public boolean isCurrent = false;
	//public boolean isCc = false;
	public long createdAt = 0;
	//public long modifiedAt = 0;
	public long createdBy = 0;
	//public long modifiedBy = 0;
	public long deadlineDate = 0;
	public int currentStatus = 0;
	public int is_send = 0;
	public boolean isSeen = false;
	public int assignedRole = 0;
	public Boolean actionTaken = false;
	public List<FilesDTO> files;
	
	
    @Override
	public String toString() {
            return "$Inbox_movementsDTO[" +
            " id = " + ID +
            " inboxId = " + inboxId +
            " note = " + note +
            " action = " + action +
            " toEmployeeUserId = " + toEmployeeUserId +
            " fromEmployeeUserId = " + fromEmployeeUserId +
            " toOrganogramId = " + toOrganogramId +
            " fromOrganogramId = " + fromOrganogramId +
            //" isCurrent = " + isCurrent +
            //" isCc = " + isCc +
            " createdAt = " + createdAt +
          //  " modifiedAt = " + modifiedAt +
            " createdBy = " + createdBy +
           // " modifiedBy = " + modifiedBy +
            " deadlineDate = " + deadlineDate +
            " currentStatus = " + currentStatus +
            " isSeen = " + isSeen +
            " assignedRole = " + assignedRole +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}