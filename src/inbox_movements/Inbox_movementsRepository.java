package inbox_movements;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Inbox_movementsRepository implements Repository {
	Inbox_movementsDAO inbox_movementsDAO = null;
	
	public void setDAO(Inbox_movementsDAO inbox_movementsDAO)
	{
		this.inbox_movementsDAO = inbox_movementsDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Inbox_movementsRepository.class);
	Map<Long, Inbox_movementsDTO>mapOfInbox_movementsDTOToid;
	Map<Long, Set<Inbox_movementsDTO> >mapOfInbox_movementsDTOToinboxId;
	Map<String, Set<Inbox_movementsDTO> >mapOfInbox_movementsDTOTonote;
	Map<Long, Set<Inbox_movementsDTO> >mapOfInbox_movementsDTOToaction;
	Map<Long, Set<Inbox_movementsDTO> >mapOfInbox_movementsDTOTotoEmployeeUserId;
	Map<Long, Set<Inbox_movementsDTO> >mapOfInbox_movementsDTOTofromEmployeeUserId;
	Map<Long, Set<Inbox_movementsDTO> >mapOfInbox_movementsDTOTotoOrganogramId;
	Map<Long, Set<Inbox_movementsDTO> >mapOfInbox_movementsDTOTofromOrganogramId;
	Map<Boolean, Set<Inbox_movementsDTO> >mapOfInbox_movementsDTOToisCurrent;
	Map<Boolean, Set<Inbox_movementsDTO> >mapOfInbox_movementsDTOToisCc;
	Map<Long, Set<Inbox_movementsDTO> >mapOfInbox_movementsDTOTocreatedAt;
	Map<Long, Set<Inbox_movementsDTO> >mapOfInbox_movementsDTOTomodifiedAt;
	Map<Long, Set<Inbox_movementsDTO> >mapOfInbox_movementsDTOTocreatedBy;
	Map<Long, Set<Inbox_movementsDTO> >mapOfInbox_movementsDTOTomodifiedBy;
	Map<Long, Set<Inbox_movementsDTO> >mapOfInbox_movementsDTOTodeadlineDate;
	Map<Integer, Set<Inbox_movementsDTO> >mapOfInbox_movementsDTOTocurrentStatus;
	Map<Boolean, Set<Inbox_movementsDTO> >mapOfInbox_movementsDTOToisSeen;
	Map<Integer, Set<Inbox_movementsDTO> >mapOfInbox_movementsDTOToassignedRole;
	Map<Long, Set<Inbox_movementsDTO> >mapOfInbox_movementsDTOTolastModificationTime;


	static Inbox_movementsRepository instance = null;  
	private Inbox_movementsRepository(){
		mapOfInbox_movementsDTOToid = new ConcurrentHashMap<>();
		mapOfInbox_movementsDTOToinboxId = new ConcurrentHashMap<>();
		mapOfInbox_movementsDTOTonote = new ConcurrentHashMap<>();
		mapOfInbox_movementsDTOToaction = new ConcurrentHashMap<>();
		mapOfInbox_movementsDTOTotoEmployeeUserId = new ConcurrentHashMap<>();
		mapOfInbox_movementsDTOTofromEmployeeUserId = new ConcurrentHashMap<>();
		mapOfInbox_movementsDTOTotoOrganogramId = new ConcurrentHashMap<>();
		mapOfInbox_movementsDTOTofromOrganogramId = new ConcurrentHashMap<>();
		mapOfInbox_movementsDTOToisCurrent = new ConcurrentHashMap<>();
		mapOfInbox_movementsDTOToisCc = new ConcurrentHashMap<>();
		mapOfInbox_movementsDTOTocreatedAt = new ConcurrentHashMap<>();
		mapOfInbox_movementsDTOTomodifiedAt = new ConcurrentHashMap<>();
		mapOfInbox_movementsDTOTocreatedBy = new ConcurrentHashMap<>();
		mapOfInbox_movementsDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfInbox_movementsDTOTodeadlineDate = new ConcurrentHashMap<>();
		mapOfInbox_movementsDTOTocurrentStatus = new ConcurrentHashMap<>();
		mapOfInbox_movementsDTOToisSeen = new ConcurrentHashMap<>();
		mapOfInbox_movementsDTOToassignedRole = new ConcurrentHashMap<>();
		mapOfInbox_movementsDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Inbox_movementsRepository getInstance(){
		if (instance == null){
			instance = new Inbox_movementsRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(inbox_movementsDAO == null)
		{
			return;
		}
		try {
			List<Inbox_movementsDTO> inbox_movementsDTOs = inbox_movementsDAO.getAllInbox_movements(reloadAll);
			for(Inbox_movementsDTO inbox_movementsDTO : inbox_movementsDTOs) {
				Inbox_movementsDTO oldInbox_movementsDTO = getInbox_movementsDTOByid(inbox_movementsDTO.ID);
				if( oldInbox_movementsDTO != null ) {
					mapOfInbox_movementsDTOToid.remove(oldInbox_movementsDTO.ID);
				
					if(mapOfInbox_movementsDTOToinboxId.containsKey(oldInbox_movementsDTO.inboxId)) {
						mapOfInbox_movementsDTOToinboxId.get(oldInbox_movementsDTO.inboxId).remove(oldInbox_movementsDTO);
					}
					if(mapOfInbox_movementsDTOToinboxId.get(oldInbox_movementsDTO.inboxId).isEmpty()) {
						mapOfInbox_movementsDTOToinboxId.remove(oldInbox_movementsDTO.inboxId);
					}
					
					if(mapOfInbox_movementsDTOTonote.containsKey(oldInbox_movementsDTO.note)) {
						mapOfInbox_movementsDTOTonote.get(oldInbox_movementsDTO.note).remove(oldInbox_movementsDTO);
					}
					if(mapOfInbox_movementsDTOTonote.get(oldInbox_movementsDTO.note).isEmpty()) {
						mapOfInbox_movementsDTOTonote.remove(oldInbox_movementsDTO.note);
					}
					
					if(mapOfInbox_movementsDTOToaction.containsKey(oldInbox_movementsDTO.action)) {
						mapOfInbox_movementsDTOToaction.get(oldInbox_movementsDTO.action).remove(oldInbox_movementsDTO);
					}
					if(mapOfInbox_movementsDTOToaction.get(oldInbox_movementsDTO.action).isEmpty()) {
						mapOfInbox_movementsDTOToaction.remove(oldInbox_movementsDTO.action);
					}
					
					if(mapOfInbox_movementsDTOTotoEmployeeUserId.containsKey(oldInbox_movementsDTO.toEmployeeUserId)) {
						mapOfInbox_movementsDTOTotoEmployeeUserId.get(oldInbox_movementsDTO.toEmployeeUserId).remove(oldInbox_movementsDTO);
					}
					if(mapOfInbox_movementsDTOTotoEmployeeUserId.get(oldInbox_movementsDTO.toEmployeeUserId).isEmpty()) {
						mapOfInbox_movementsDTOTotoEmployeeUserId.remove(oldInbox_movementsDTO.toEmployeeUserId);
					}
					
					if(mapOfInbox_movementsDTOTofromEmployeeUserId.containsKey(oldInbox_movementsDTO.fromEmployeeUserId)) {
						mapOfInbox_movementsDTOTofromEmployeeUserId.get(oldInbox_movementsDTO.fromEmployeeUserId).remove(oldInbox_movementsDTO);
					}
					if(mapOfInbox_movementsDTOTofromEmployeeUserId.get(oldInbox_movementsDTO.fromEmployeeUserId).isEmpty()) {
						mapOfInbox_movementsDTOTofromEmployeeUserId.remove(oldInbox_movementsDTO.fromEmployeeUserId);
					}
					
					if(mapOfInbox_movementsDTOTotoOrganogramId.containsKey(oldInbox_movementsDTO.toOrganogramId)) {
						mapOfInbox_movementsDTOTotoOrganogramId.get(oldInbox_movementsDTO.toOrganogramId).remove(oldInbox_movementsDTO);
					}
					if(mapOfInbox_movementsDTOTotoOrganogramId.get(oldInbox_movementsDTO.toOrganogramId).isEmpty()) {
						mapOfInbox_movementsDTOTotoOrganogramId.remove(oldInbox_movementsDTO.toOrganogramId);
					}
					
					if(mapOfInbox_movementsDTOTofromOrganogramId.containsKey(oldInbox_movementsDTO.fromOrganogramId)) {
						mapOfInbox_movementsDTOTofromOrganogramId.get(oldInbox_movementsDTO.fromOrganogramId).remove(oldInbox_movementsDTO);
					}
					if(mapOfInbox_movementsDTOTofromOrganogramId.get(oldInbox_movementsDTO.fromOrganogramId).isEmpty()) {
						mapOfInbox_movementsDTOTofromOrganogramId.remove(oldInbox_movementsDTO.fromOrganogramId);
					}
					
//					if(mapOfInbox_movementsDTOToisCurrent.containsKey(oldInbox_movementsDTO.isCurrent)) {
//						mapOfInbox_movementsDTOToisCurrent.get(oldInbox_movementsDTO.isCurrent).remove(oldInbox_movementsDTO);
//					}
//					if(mapOfInbox_movementsDTOToisCurrent.get(oldInbox_movementsDTO.isCurrent).isEmpty()) {
//						mapOfInbox_movementsDTOToisCurrent.remove(oldInbox_movementsDTO.isCurrent);
//					}
					
//					if(mapOfInbox_movementsDTOToisCc.containsKey(oldInbox_movementsDTO.isCc)) {
//						mapOfInbox_movementsDTOToisCc.get(oldInbox_movementsDTO.isCc).remove(oldInbox_movementsDTO);
//					}
//					if(mapOfInbox_movementsDTOToisCc.get(oldInbox_movementsDTO.isCc).isEmpty()) {
//						mapOfInbox_movementsDTOToisCc.remove(oldInbox_movementsDTO.isCc);
//					}
					
					if(mapOfInbox_movementsDTOTocreatedAt.containsKey(oldInbox_movementsDTO.createdAt)) {
						mapOfInbox_movementsDTOTocreatedAt.get(oldInbox_movementsDTO.createdAt).remove(oldInbox_movementsDTO);
					}
					if(mapOfInbox_movementsDTOTocreatedAt.get(oldInbox_movementsDTO.createdAt).isEmpty()) {
						mapOfInbox_movementsDTOTocreatedAt.remove(oldInbox_movementsDTO.createdAt);
					}
					
//					if(mapOfInbox_movementsDTOTomodifiedAt.containsKey(oldInbox_movementsDTO.modifiedAt)) {
//						mapOfInbox_movementsDTOTomodifiedAt.get(oldInbox_movementsDTO.modifiedAt).remove(oldInbox_movementsDTO);
//					}
//					if(mapOfInbox_movementsDTOTomodifiedAt.get(oldInbox_movementsDTO.modifiedAt).isEmpty()) {
//						mapOfInbox_movementsDTOTomodifiedAt.remove(oldInbox_movementsDTO.modifiedAt);
//					}
					
					if(mapOfInbox_movementsDTOTocreatedBy.containsKey(oldInbox_movementsDTO.createdBy)) {
						mapOfInbox_movementsDTOTocreatedBy.get(oldInbox_movementsDTO.createdBy).remove(oldInbox_movementsDTO);
					}
					if(mapOfInbox_movementsDTOTocreatedBy.get(oldInbox_movementsDTO.createdBy).isEmpty()) {
						mapOfInbox_movementsDTOTocreatedBy.remove(oldInbox_movementsDTO.createdBy);
					}
					
//					if(mapOfInbox_movementsDTOTomodifiedBy.containsKey(oldInbox_movementsDTO.modifiedBy)) {
//						mapOfInbox_movementsDTOTomodifiedBy.get(oldInbox_movementsDTO.modifiedBy).remove(oldInbox_movementsDTO);
//					}
//					if(mapOfInbox_movementsDTOTomodifiedBy.get(oldInbox_movementsDTO.modifiedBy).isEmpty()) {
//						mapOfInbox_movementsDTOTomodifiedBy.remove(oldInbox_movementsDTO.modifiedBy);
//					}
					
					if(mapOfInbox_movementsDTOTodeadlineDate.containsKey(oldInbox_movementsDTO.deadlineDate)) {
						mapOfInbox_movementsDTOTodeadlineDate.get(oldInbox_movementsDTO.deadlineDate).remove(oldInbox_movementsDTO);
					}
					if(mapOfInbox_movementsDTOTodeadlineDate.get(oldInbox_movementsDTO.deadlineDate).isEmpty()) {
						mapOfInbox_movementsDTOTodeadlineDate.remove(oldInbox_movementsDTO.deadlineDate);
					}
					
					if(mapOfInbox_movementsDTOTocurrentStatus.containsKey(oldInbox_movementsDTO.currentStatus)) {
						mapOfInbox_movementsDTOTocurrentStatus.get(oldInbox_movementsDTO.currentStatus).remove(oldInbox_movementsDTO);
					}
					if(mapOfInbox_movementsDTOTocurrentStatus.get(oldInbox_movementsDTO.currentStatus).isEmpty()) {
						mapOfInbox_movementsDTOTocurrentStatus.remove(oldInbox_movementsDTO.currentStatus);
					}
					
					if(mapOfInbox_movementsDTOToisSeen.containsKey(oldInbox_movementsDTO.isSeen)) {
						mapOfInbox_movementsDTOToisSeen.get(oldInbox_movementsDTO.isSeen).remove(oldInbox_movementsDTO);
					}
					if(mapOfInbox_movementsDTOToisSeen.get(oldInbox_movementsDTO.isSeen).isEmpty()) {
						mapOfInbox_movementsDTOToisSeen.remove(oldInbox_movementsDTO.isSeen);
					}
					
					if(mapOfInbox_movementsDTOToassignedRole.containsKey(oldInbox_movementsDTO.assignedRole)) {
						mapOfInbox_movementsDTOToassignedRole.get(oldInbox_movementsDTO.assignedRole).remove(oldInbox_movementsDTO);
					}
					if(mapOfInbox_movementsDTOToassignedRole.get(oldInbox_movementsDTO.assignedRole).isEmpty()) {
						mapOfInbox_movementsDTOToassignedRole.remove(oldInbox_movementsDTO.assignedRole);
					}
					
					if(mapOfInbox_movementsDTOTolastModificationTime.containsKey(oldInbox_movementsDTO.lastModificationTime)) {
						mapOfInbox_movementsDTOTolastModificationTime.get(oldInbox_movementsDTO.lastModificationTime).remove(oldInbox_movementsDTO);
					}
					if(mapOfInbox_movementsDTOTolastModificationTime.get(oldInbox_movementsDTO.lastModificationTime).isEmpty()) {
						mapOfInbox_movementsDTOTolastModificationTime.remove(oldInbox_movementsDTO.lastModificationTime);
					}
					
					
				}
				if(inbox_movementsDTO.isDeleted == 0) 
				{
					
					mapOfInbox_movementsDTOToid.put(inbox_movementsDTO.ID, inbox_movementsDTO);
				
					if( ! mapOfInbox_movementsDTOToinboxId.containsKey(inbox_movementsDTO.inboxId)) {
						mapOfInbox_movementsDTOToinboxId.put(inbox_movementsDTO.inboxId, new HashSet<>());
					}
					mapOfInbox_movementsDTOToinboxId.get(inbox_movementsDTO.inboxId).add(inbox_movementsDTO);
					
					if( ! mapOfInbox_movementsDTOTonote.containsKey(inbox_movementsDTO.note)) {
						mapOfInbox_movementsDTOTonote.put(inbox_movementsDTO.note, new HashSet<>());
					}
					mapOfInbox_movementsDTOTonote.get(inbox_movementsDTO.note).add(inbox_movementsDTO);
					
					if( ! mapOfInbox_movementsDTOToaction.containsKey(inbox_movementsDTO.action)) {
						mapOfInbox_movementsDTOToaction.put(inbox_movementsDTO.action, new HashSet<>());
					}
					mapOfInbox_movementsDTOToaction.get(inbox_movementsDTO.action).add(inbox_movementsDTO);
					
					if( ! mapOfInbox_movementsDTOTotoEmployeeUserId.containsKey(inbox_movementsDTO.toEmployeeUserId)) {
						mapOfInbox_movementsDTOTotoEmployeeUserId.put(inbox_movementsDTO.toEmployeeUserId, new HashSet<>());
					}
					mapOfInbox_movementsDTOTotoEmployeeUserId.get(inbox_movementsDTO.toEmployeeUserId).add(inbox_movementsDTO);
					
					if( ! mapOfInbox_movementsDTOTofromEmployeeUserId.containsKey(inbox_movementsDTO.fromEmployeeUserId)) {
						mapOfInbox_movementsDTOTofromEmployeeUserId.put(inbox_movementsDTO.fromEmployeeUserId, new HashSet<>());
					}
					mapOfInbox_movementsDTOTofromEmployeeUserId.get(inbox_movementsDTO.fromEmployeeUserId).add(inbox_movementsDTO);
					
					if( ! mapOfInbox_movementsDTOTotoOrganogramId.containsKey(inbox_movementsDTO.toOrganogramId)) {
						mapOfInbox_movementsDTOTotoOrganogramId.put(inbox_movementsDTO.toOrganogramId, new HashSet<>());
					}
					mapOfInbox_movementsDTOTotoOrganogramId.get(inbox_movementsDTO.toOrganogramId).add(inbox_movementsDTO);
					
					if( ! mapOfInbox_movementsDTOTofromOrganogramId.containsKey(inbox_movementsDTO.fromOrganogramId)) {
						mapOfInbox_movementsDTOTofromOrganogramId.put(inbox_movementsDTO.fromOrganogramId, new HashSet<>());
					}
					mapOfInbox_movementsDTOTofromOrganogramId.get(inbox_movementsDTO.fromOrganogramId).add(inbox_movementsDTO);
					
//					if( ! mapOfInbox_movementsDTOToisCurrent.containsKey(inbox_movementsDTO.isCurrent)) {
//						mapOfInbox_movementsDTOToisCurrent.put(inbox_movementsDTO.isCurrent, new HashSet<>());
//					}
//					mapOfInbox_movementsDTOToisCurrent.get(inbox_movementsDTO.isCurrent).add(inbox_movementsDTO);
					
//					if( ! mapOfInbox_movementsDTOToisCc.containsKey(inbox_movementsDTO.isCc)) {
//						mapOfInbox_movementsDTOToisCc.put(inbox_movementsDTO.isCc, new HashSet<>());
//					}
//					mapOfInbox_movementsDTOToisCc.get(inbox_movementsDTO.isCc).add(inbox_movementsDTO);
					
					if( ! mapOfInbox_movementsDTOTocreatedAt.containsKey(inbox_movementsDTO.createdAt)) {
						mapOfInbox_movementsDTOTocreatedAt.put(inbox_movementsDTO.createdAt, new HashSet<>());
					}
					mapOfInbox_movementsDTOTocreatedAt.get(inbox_movementsDTO.createdAt).add(inbox_movementsDTO);
					
//					if( ! mapOfInbox_movementsDTOTomodifiedAt.containsKey(inbox_movementsDTO.modifiedAt)) {
//						mapOfInbox_movementsDTOTomodifiedAt.put(inbox_movementsDTO.modifiedAt, new HashSet<>());
//					}
//					mapOfInbox_movementsDTOTomodifiedAt.get(inbox_movementsDTO.modifiedAt).add(inbox_movementsDTO);
					
					if( ! mapOfInbox_movementsDTOTocreatedBy.containsKey(inbox_movementsDTO.createdBy)) {
						mapOfInbox_movementsDTOTocreatedBy.put(inbox_movementsDTO.createdBy, new HashSet<>());
					}
					mapOfInbox_movementsDTOTocreatedBy.get(inbox_movementsDTO.createdBy).add(inbox_movementsDTO);
					
//					if( ! mapOfInbox_movementsDTOTomodifiedBy.containsKey(inbox_movementsDTO.modifiedBy)) {
//						mapOfInbox_movementsDTOTomodifiedBy.put(inbox_movementsDTO.modifiedBy, new HashSet<>());
//					}
//					mapOfInbox_movementsDTOTomodifiedBy.get(inbox_movementsDTO.modifiedBy).add(inbox_movementsDTO);
					
					if( ! mapOfInbox_movementsDTOTodeadlineDate.containsKey(inbox_movementsDTO.deadlineDate)) {
						mapOfInbox_movementsDTOTodeadlineDate.put(inbox_movementsDTO.deadlineDate, new HashSet<>());
					}
					mapOfInbox_movementsDTOTodeadlineDate.get(inbox_movementsDTO.deadlineDate).add(inbox_movementsDTO);
					
					if( ! mapOfInbox_movementsDTOTocurrentStatus.containsKey(inbox_movementsDTO.currentStatus)) {
						mapOfInbox_movementsDTOTocurrentStatus.put(inbox_movementsDTO.currentStatus, new HashSet<>());
					}
					mapOfInbox_movementsDTOTocurrentStatus.get(inbox_movementsDTO.currentStatus).add(inbox_movementsDTO);
					
					if( ! mapOfInbox_movementsDTOToisSeen.containsKey(inbox_movementsDTO.isSeen)) {
						mapOfInbox_movementsDTOToisSeen.put(inbox_movementsDTO.isSeen, new HashSet<>());
					}
					mapOfInbox_movementsDTOToisSeen.get(inbox_movementsDTO.isSeen).add(inbox_movementsDTO);
					
					if( ! mapOfInbox_movementsDTOToassignedRole.containsKey(inbox_movementsDTO.assignedRole)) {
						mapOfInbox_movementsDTOToassignedRole.put(inbox_movementsDTO.assignedRole, new HashSet<>());
					}
					mapOfInbox_movementsDTOToassignedRole.get(inbox_movementsDTO.assignedRole).add(inbox_movementsDTO);
					
					if( ! mapOfInbox_movementsDTOTolastModificationTime.containsKey(inbox_movementsDTO.lastModificationTime)) {
						mapOfInbox_movementsDTOTolastModificationTime.put(inbox_movementsDTO.lastModificationTime, new HashSet<>());
					}
					mapOfInbox_movementsDTOTolastModificationTime.get(inbox_movementsDTO.lastModificationTime).add(inbox_movementsDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Inbox_movementsDTO> getInbox_movementsList() {
		List <Inbox_movementsDTO> inbox_movementss = new ArrayList<Inbox_movementsDTO>(this.mapOfInbox_movementsDTOToid.values());
		return inbox_movementss;
	}
	
	
	public Inbox_movementsDTO getInbox_movementsDTOByid( long id){
		return mapOfInbox_movementsDTOToid.get(id);
	}
	
	
	public List<Inbox_movementsDTO> getInbox_movementsDTOByinbox_id(long inbox_id) {
		return new ArrayList<>( mapOfInbox_movementsDTOToinboxId.getOrDefault(inbox_id,new HashSet<>()));
	}
	
	
	public List<Inbox_movementsDTO> getInbox_movementsDTOBynote(String note) {
		return new ArrayList<>( mapOfInbox_movementsDTOTonote.getOrDefault(note,new HashSet<>()));
	}
	
	
	public List<Inbox_movementsDTO> getInbox_movementsDTOByaction(String action) {
		return new ArrayList<>( mapOfInbox_movementsDTOToaction.getOrDefault(action,new HashSet<>()));
	}
	
	
	public List<Inbox_movementsDTO> getInbox_movementsDTOByto_employee_user_id(long to_employee_user_id) {
		return new ArrayList<>( mapOfInbox_movementsDTOTotoEmployeeUserId.getOrDefault(to_employee_user_id,new HashSet<>()));
	}
	
	
	public List<Inbox_movementsDTO> getInbox_movementsDTOByfrom_employee_user_id(long from_employee_user_id) {
		return new ArrayList<>( mapOfInbox_movementsDTOTofromEmployeeUserId.getOrDefault(from_employee_user_id,new HashSet<>()));
	}
	
	
	public List<Inbox_movementsDTO> getInbox_movementsDTOByto_organogram_id(long to_organogram_id) {
		return new ArrayList<>( mapOfInbox_movementsDTOTotoOrganogramId.getOrDefault(to_organogram_id,new HashSet<>()));
	}
	
	
	public List<Inbox_movementsDTO> getInbox_movementsDTOByfrom_organogram_id(long from_organogram_id) {
		return new ArrayList<>( mapOfInbox_movementsDTOTofromOrganogramId.getOrDefault(from_organogram_id,new HashSet<>()));
	}
	
	
	public List<Inbox_movementsDTO> getInbox_movementsDTOByis_current(boolean is_current) {
		return new ArrayList<>( mapOfInbox_movementsDTOToisCurrent.getOrDefault(is_current,new HashSet<>()));
	}
	
	
	public List<Inbox_movementsDTO> getInbox_movementsDTOByis_cc(boolean is_cc) {
		return new ArrayList<>( mapOfInbox_movementsDTOToisCc.getOrDefault(is_cc,new HashSet<>()));
	}
	
	
	public List<Inbox_movementsDTO> getInbox_movementsDTOBycreated_at(long created_at) {
		return new ArrayList<>( mapOfInbox_movementsDTOTocreatedAt.getOrDefault(created_at,new HashSet<>()));
	}
	
	
	public List<Inbox_movementsDTO> getInbox_movementsDTOBymodified_at(long modified_at) {
		return new ArrayList<>( mapOfInbox_movementsDTOTomodifiedAt.getOrDefault(modified_at,new HashSet<>()));
	}
	
	
	public List<Inbox_movementsDTO> getInbox_movementsDTOBycreated_by(long created_by) {
		return new ArrayList<>( mapOfInbox_movementsDTOTocreatedBy.getOrDefault(created_by,new HashSet<>()));
	}
	
	
	public List<Inbox_movementsDTO> getInbox_movementsDTOBymodified_by(long modified_by) {
		return new ArrayList<>( mapOfInbox_movementsDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Inbox_movementsDTO> getInbox_movementsDTOBydeadline_date(long deadline_date) {
		return new ArrayList<>( mapOfInbox_movementsDTOTodeadlineDate.getOrDefault(deadline_date,new HashSet<>()));
	}
	
	
	public List<Inbox_movementsDTO> getInbox_movementsDTOBycurrent_status(int current_status) {
		return new ArrayList<>( mapOfInbox_movementsDTOTocurrentStatus.getOrDefault(current_status,new HashSet<>()));
	}
	
	
	public List<Inbox_movementsDTO> getInbox_movementsDTOByis_seen(boolean is_seen) {
		return new ArrayList<>( mapOfInbox_movementsDTOToisSeen.getOrDefault(is_seen,new HashSet<>()));
	}
	
	
	public List<Inbox_movementsDTO> getInbox_movementsDTOByassigned_role(int assigned_role) {
		return new ArrayList<>( mapOfInbox_movementsDTOToassignedRole.getOrDefault(assigned_role,new HashSet<>()));
	}
	
	
	public List<Inbox_movementsDTO> getInbox_movementsDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfInbox_movementsDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "inbox_movements";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


