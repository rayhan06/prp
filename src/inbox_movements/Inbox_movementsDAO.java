package inbox_movements;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import project_tracker.FilesDAO;
import approval_module_map.*;

public class Inbox_movementsDAO  extends NavigationService3
{
	
	Logger logger = Logger.getLogger(getClass());

	
	public Inbox_movementsDAO(String tableName, String tempTableName, Approval_module_mapDTO approval_module_mapDTO)
	{
		super(tableName, tempTableName, approval_module_mapDTO);		
	}
	
	public Inbox_movementsDAO()
	{
		super("inbox_movements", null, null);		
	}
	
	
	
	
	public long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception
	{
		
		Inbox_movementsDTO inbox_movementsDTO = (Inbox_movementsDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				logger.debug("nullconn");
			}

			inbox_movementsDTO.ID = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "inbox_id";
			sql += ", ";
			sql += "note";
			sql += ", ";
			sql += "action";
			sql += ", ";
			sql += "to_employee_user_id";
			sql += ", ";
			sql += "from_employee_user_id";
			sql += ", ";
			sql += "to_organogram_id";
			sql += ", ";
			sql += "from_organogram_id";
			sql += ", ";			
			//sql += "is_cc";
			//sql += ", ";
			sql += "created_at";
			sql += ", ";
			//sql += "modified_at";
			//sql += ", ";
			sql += "created_by";
			sql += ", ";
			//sql += "modified_by";
			//sql += ", ";
			sql += "deadline_date";
			sql += ", ";
			sql += "status";
			sql += ", ";
			sql += "is_seen";
			sql += ", ";
			sql += "assigned_role";
			sql += ", ";
			sql += "inbox_type";
			sql += ", ";
			sql += "is_send";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
			if(tempTableDTO!=null)
			{
				sql += ", permanent_table_id, operation_type, approval_path_type, approval_order";
			}
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			//sql += "?";
			//sql += ", ";
			//sql += "?";
			//sql += ", ";
			sql += "?";
			if(tempTableDTO!=null)
			{
				sql += ", " + tempTableDTO.permanent_table_id + ", " + tempTableDTO.operation_type + ", " + tempTableDTO.approval_path_type + ", " + tempTableDTO.approval_order;
			}
			sql += ")";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,inbox_movementsDTO.ID);
			ps.setObject(index++,inbox_movementsDTO.inboxId);
			ps.setObject(index++,inbox_movementsDTO.note);
			ps.setObject(index++,inbox_movementsDTO.action);
			ps.setObject(index++,inbox_movementsDTO.toEmployeeUserId);
			ps.setObject(index++,inbox_movementsDTO.fromEmployeeUserId);
			ps.setObject(index++,inbox_movementsDTO.toOrganogramId);
			ps.setObject(index++,inbox_movementsDTO.fromOrganogramId);
			//ps.setObject(index++,inbox_movementsDTO.isCurrent);
			//ps.setObject(index++,inbox_movementsDTO.isCc);
			ps.setObject(index++,inbox_movementsDTO.createdAt);
			//ps.setObject(index++,inbox_movementsDTO.modifiedAt);
			ps.setObject(index++,inbox_movementsDTO.createdBy);
			//ps.setObject(index++,inbox_movementsDTO.modifiedBy);
			ps.setObject(index++,inbox_movementsDTO.deadlineDate);
			ps.setObject(index++,inbox_movementsDTO.currentStatus);
			ps.setObject(index++,inbox_movementsDTO.isSeen);
			ps.setObject(index++,inbox_movementsDTO.assignedRole);
			ps.setObject(index++,inbox_movementsDTO.inbox_type);
			ps.setObject(index++,inbox_movementsDTO.is_send);
			ps.setObject(index++,inbox_movementsDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			logger.debug(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime, tableName);
			

			
			ps.close();
			
			if(inbox_movementsDTO.files != null)
			{
			
			 FilesDAO tempFileDao = new FilesDAO();			
			 tempFileDao.insertFiles(inbox_movementsDTO.files, "inbox_movements",inbox_movementsDTO.ID,connection);
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return inbox_movementsDTO.ID;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID, String tableName) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_movementsDTO inbox_movementsDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				inbox_movementsDTO = new Inbox_movementsDTO();

				inbox_movementsDTO.ID = rs.getLong("ID");
				inbox_movementsDTO.inboxId = rs.getLong("inbox_id");
				inbox_movementsDTO.note = rs.getString("note");
				inbox_movementsDTO.action = rs.getLong("action");
				inbox_movementsDTO.toEmployeeUserId = rs.getLong("to_employee_user_id");
				inbox_movementsDTO.fromEmployeeUserId = rs.getLong("from_employee_user_id");
				inbox_movementsDTO.toOrganogramId = rs.getLong("to_organogram_id");
				inbox_movementsDTO.fromOrganogramId = rs.getLong("from_organogram_id");
				//inbox_movementsDTO.isCurrent = rs.getBoolean("is_current");
				//inbox_movementsDTO.isCc = rs.getBoolean("is_cc");
				inbox_movementsDTO.createdAt = rs.getLong("created_at");
				//inbox_movementsDTO.modifiedAt = rs.getLong("modified_at");
				inbox_movementsDTO.createdBy = rs.getLong("created_by");
				//inbox_movementsDTO.modifiedBy = rs.getLong("modified_by");
				inbox_movementsDTO.deadlineDate = rs.getLong("deadline_date");
				inbox_movementsDTO.currentStatus = rs.getInt("status");
				inbox_movementsDTO.isSeen = rs.getBoolean("is_seen");
				inbox_movementsDTO.assignedRole = rs.getInt("assigned_role");
				inbox_movementsDTO.inbox_type = rs.getLong("inbox_type");
				inbox_movementsDTO.is_send = rs.getInt("is_send");
				inbox_movementsDTO.isDeleted = rs.getInt("isDeleted");
				inbox_movementsDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}	
			
			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_movementsDTO;
	}
	
	public long getMaxIDByInboxIDAndToOrgId (long inboxID, long toOrgId)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		long id = -1;
		try{
			
			String sql = "select max(id) from inbox_movements where inbox_id = " + inboxID + " and to_organogram_id = " + toOrgId;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){


				id = rs.getLong("max(id)");
				
			}	
			
			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return id;
	}
	
	public long getMaxIDByInboxID (long inboxID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		long id = -1;
		try{
			
			String sql = "select max(id) from inbox_movements where inbox_id = " + inboxID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){


				id = rs.getLong("max(id)");
				
			}	
			
			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return id;
	}
	
	public void updateInboxMovementsSetCreatedBy(long inboxID, long createdBy)
	{
		Connection connection = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		
		long id = getMaxIDByInboxID(inboxID);
		
		long lastModificationTime = System.currentTimeMillis();	
		try{
			String sql = "UPDATE inbox_movements ";
			
			sql += " SET created_by = " + createdBy + ",lastModificationTime="+ lastModificationTime 
					+ " WHERE id = " + id;
			
			printSql(sql);

			connection = DBMW.getInstance().getConnection();
			stmt  = connection.createStatement();
			stmt.execute(sql);
			

			
			recordUpdateTime(connection, lastModificationTime, tableName);

			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMW.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

	}
	
	public long update(CommonDTO commonDTO, String tableName) throws Exception
	{		
		Inbox_movementsDTO inbox_movementsDTO = (Inbox_movementsDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "inbox_id=?";
			sql += ", ";
			sql += "note=?";
			sql += ", ";
			sql += "action=?";
			sql += ", ";
			sql += "to_employee_user_id=?";
			sql += ", ";
			sql += "from_employee_user_id=?";
			sql += ", ";
			sql += "to_organogram_id=?";
			sql += ", ";
			sql += "from_organogram_id=?";
			sql += ", ";			
			//sql += "is_cc=?";
			//sql += ", ";
			sql += "created_at=?";
			sql += ", ";
			//sql += "modified_at=?";
			//sql += ", ";
			sql += "created_by=?";
			sql += ", ";
			//sql += "modified_by=?";
			//sql += ", ";
			sql += "deadline_date=?";
			sql += ", ";
			sql += "status=?";
			sql += ", ";
			sql += "is_seen=?";
			sql += ", ";
			sql += "assigned_role=?";
			sql += ", ";
			sql += "inbox_type=?";
			sql += ", ";
			sql += "is_send=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + inbox_movementsDTO.ID;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,inbox_movementsDTO.inboxId);
			ps.setObject(index++,inbox_movementsDTO.note);
			ps.setObject(index++,inbox_movementsDTO.action);
			ps.setObject(index++,inbox_movementsDTO.toEmployeeUserId);
			ps.setObject(index++,inbox_movementsDTO.fromEmployeeUserId);
			ps.setObject(index++,inbox_movementsDTO.toOrganogramId);
			ps.setObject(index++,inbox_movementsDTO.fromOrganogramId);
			//ps.setObject(index++,inbox_movementsDTO.isCurrent);
			//ps.setObject(index++,inbox_movementsDTO.isCc);
			ps.setObject(index++,inbox_movementsDTO.createdAt);
			//ps.setObject(index++,inbox_movementsDTO.modifiedAt);
			ps.setObject(index++,inbox_movementsDTO.createdBy);
			//ps.setObject(index++,inbox_movementsDTO.modifiedBy);
			ps.setObject(index++,inbox_movementsDTO.deadlineDate);
			ps.setObject(index++,inbox_movementsDTO.currentStatus);
			ps.setObject(index++,inbox_movementsDTO.isSeen);
			ps.setObject(index++,inbox_movementsDTO.assignedRole);
			ps.setObject(index++,inbox_movementsDTO.inbox_type);
			ps.setObject(index++,inbox_movementsDTO.is_send);
			logger.debug(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, lastModificationTime, tableName);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return inbox_movementsDTO.ID;
	}
	
	public List<Inbox_movementsDTO> getDTOs(Collection recordIDs)
	{
		return getDTOs(recordIDs, tableName);
	}
	
	public List<Inbox_movementsDTO> getDTOs(Collection recordIDs, String tableName){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_movementsDTO inbox_movementsDTO = null;
		List<Inbox_movementsDTO> inbox_movementsDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return inbox_movementsDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				inbox_movementsDTO = new Inbox_movementsDTO();
				inbox_movementsDTO.ID = rs.getLong("ID");
				inbox_movementsDTO.inboxId = rs.getLong("inbox_id");
				inbox_movementsDTO.note = rs.getString("note");
				inbox_movementsDTO.action = rs.getLong("action");
				inbox_movementsDTO.toEmployeeUserId = rs.getLong("to_employee_user_id");
				inbox_movementsDTO.fromEmployeeUserId = rs.getLong("from_employee_user_id");
				inbox_movementsDTO.toOrganogramId = rs.getLong("to_organogram_id");
				inbox_movementsDTO.fromOrganogramId = rs.getLong("from_organogram_id");
				//inbox_movementsDTO.isCurrent = rs.getBoolean("is_current");
				//inbox_movementsDTO.isCc = rs.getBoolean("is_cc");
				inbox_movementsDTO.createdAt = rs.getLong("created_at");
				//inbox_movementsDTO.modifiedAt = rs.getLong("modified_at");
				inbox_movementsDTO.createdBy = rs.getLong("created_by");
				//inbox_movementsDTO.modifiedBy = rs.getLong("modified_by");
				inbox_movementsDTO.deadlineDate = rs.getLong("deadline_date");
				inbox_movementsDTO.currentStatus = rs.getInt("status");
				inbox_movementsDTO.isSeen = rs.getBoolean("is_seen");
				inbox_movementsDTO.assignedRole = rs.getInt("assigned_role");
				inbox_movementsDTO.inbox_type = rs.getInt("inbox_type");
				inbox_movementsDTO.is_send = rs.getInt("is_send");
				inbox_movementsDTO.isDeleted = rs.getInt("isDeleted");
				inbox_movementsDTO.lastModificationTime = rs.getLong("lastModificationTime");
				logger.debug("got this DTO: " + inbox_movementsDTO);
				
				inbox_movementsDTOList.add(inbox_movementsDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_movementsDTOList;
	
	}
	
	public List<Inbox_movementsDTO> getDTOsByInboxID(long inInboxID){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_movementsDTO inbox_movementsDTO = null;
		List<Inbox_movementsDTO> inbox_movementsDTOList = new ArrayList<>();
		
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE inbox_id = " + inInboxID + " order by created_at asc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				inbox_movementsDTO = new Inbox_movementsDTO();
				inbox_movementsDTO.ID = rs.getLong("ID");
				inbox_movementsDTO.inboxId = rs.getLong("inbox_id");
				inbox_movementsDTO.note = rs.getString("note");
				inbox_movementsDTO.action = rs.getLong("action");
				inbox_movementsDTO.toEmployeeUserId = rs.getLong("to_employee_user_id");
				inbox_movementsDTO.fromEmployeeUserId = rs.getLong("from_employee_user_id");
				inbox_movementsDTO.toOrganogramId = rs.getLong("to_organogram_id");
				inbox_movementsDTO.fromOrganogramId = rs.getLong("from_organogram_id");
				//inbox_movementsDTO.isCurrent = rs.getBoolean("is_current");
				//inbox_movementsDTO.isCc = rs.getBoolean("is_cc");
				inbox_movementsDTO.createdAt = rs.getLong("created_at");
				//inbox_movementsDTO.modifiedAt = rs.getLong("modified_at");
				inbox_movementsDTO.createdBy = rs.getLong("created_by");
				//inbox_movementsDTO.modifiedBy = rs.getLong("modified_by");
				inbox_movementsDTO.deadlineDate = rs.getLong("deadline_date");
				inbox_movementsDTO.currentStatus = rs.getInt("status");
				inbox_movementsDTO.isSeen = rs.getBoolean("is_seen");
				inbox_movementsDTO.assignedRole = rs.getInt("assigned_role");
				inbox_movementsDTO.inbox_type = rs.getInt("inbox_type");
				inbox_movementsDTO.is_send = rs.getInt("is_send");
				inbox_movementsDTO.isDeleted = rs.getInt("isDeleted");
				inbox_movementsDTO.lastModificationTime = rs.getLong("lastModificationTime");
				logger.debug("got this DTO: " + inbox_movementsDTO);
				
				inbox_movementsDTOList.add(inbox_movementsDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_movementsDTOList;
	
	}
	
	

	
	
	

	
	
	
	//add repository
	public List<Inbox_movementsDTO> getAllInbox_movements (boolean isFirstReload)
    {
		List<Inbox_movementsDTO> inbox_movementsDTOList = new ArrayList<>();

		String sql = "SELECT * FROM inbox_movements";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by inbox_movements.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Inbox_movementsDTO inbox_movementsDTO = new Inbox_movementsDTO();
				inbox_movementsDTO.ID = rs.getLong("ID");
				inbox_movementsDTO.inboxId = rs.getLong("inbox_id");
				inbox_movementsDTO.note = rs.getString("note");
				inbox_movementsDTO.action = rs.getLong("action");
				inbox_movementsDTO.toEmployeeUserId = rs.getLong("to_employee_user_id");
				inbox_movementsDTO.fromEmployeeUserId = rs.getLong("from_employee_user_id");
				inbox_movementsDTO.toOrganogramId = rs.getLong("to_organogram_id");
				inbox_movementsDTO.fromOrganogramId = rs.getLong("from_organogram_id");
				//inbox_movementsDTO.isCurrent = rs.getBoolean("is_current");
				//inbox_movementsDTO.isCc = rs.getBoolean("is_cc");
				inbox_movementsDTO.createdAt = rs.getLong("created_at");
				//inbox_movementsDTO.modifiedAt = rs.getLong("modified_at");
				inbox_movementsDTO.createdBy = rs.getLong("created_by");
				//inbox_movementsDTO.modifiedBy = rs.getLong("modified_by");
				inbox_movementsDTO.deadlineDate = rs.getLong("deadline_date");
				inbox_movementsDTO.currentStatus = rs.getInt("status");
				inbox_movementsDTO.isSeen = rs.getBoolean("is_seen");
				inbox_movementsDTO.assignedRole = rs.getInt("assigned_role");
				inbox_movementsDTO.inbox_type = rs.getInt("inbox_type");
				inbox_movementsDTO.is_send = rs.getInt("is_send");
				inbox_movementsDTO.isDeleted = rs.getInt("isDeleted");
				inbox_movementsDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				inbox_movementsDTOList.add(inbox_movementsDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return inbox_movementsDTOList;
    }
	
	public List<Inbox_movementsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset)
	{
		return getDTOs(p_searchCriteria, limit, offset, tableName, true, 0);
	}
	
	public List<Inbox_movementsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Inbox_movementsDTO> inbox_movementsDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, tableName, isPermanentTable, userApprovalPathType);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Inbox_movementsDTO inbox_movementsDTO = new Inbox_movementsDTO();
				inbox_movementsDTO.ID = rs.getLong("ID");
				inbox_movementsDTO.inboxId = rs.getLong("inbox_id");
				inbox_movementsDTO.note = rs.getString("note");
				inbox_movementsDTO.action = rs.getLong("action");
				inbox_movementsDTO.toEmployeeUserId = rs.getLong("to_employee_user_id");
				inbox_movementsDTO.fromEmployeeUserId = rs.getLong("from_employee_user_id");
				inbox_movementsDTO.toOrganogramId = rs.getLong("to_organogram_id");
				inbox_movementsDTO.fromOrganogramId = rs.getLong("from_organogram_id");
				//inbox_movementsDTO.isCurrent = rs.getBoolean("is_current");
				//inbox_movementsDTO.isCc = rs.getBoolean("is_cc");
				inbox_movementsDTO.createdAt = rs.getLong("created_at");
				//inbox_movementsDTO.modifiedAt = rs.getLong("modified_at");
				inbox_movementsDTO.createdBy = rs.getLong("created_by");
				//inbox_movementsDTO.modifiedBy = rs.getLong("modified_by");
				inbox_movementsDTO.deadlineDate = rs.getLong("deadline_date");
				inbox_movementsDTO.currentStatus = rs.getInt("status");
				inbox_movementsDTO.isSeen = rs.getBoolean("is_seen");
				inbox_movementsDTO.assignedRole = rs.getInt("assigned_role");
				inbox_movementsDTO.inbox_type = rs.getInt("inbox_type");
				inbox_movementsDTO.is_send = rs.getInt("is_send");
				inbox_movementsDTO.isDeleted = rs.getInt("isDeleted");
				inbox_movementsDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				inbox_movementsDTOList.add(inbox_movementsDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_movementsDTOList;
	
	}

		
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType)
    {
		Inbox_movementsMAPS maps = new Inbox_movementsMAPS(tableName);
		String joinSQL = "";
		return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, tableName, isPermanentTable, userApprovalPathType, maps, joinSQL);
    }

    public void add(List<Inbox_movementsDTO> inbox_movementsDTOList) throws Exception {

		if( inbox_movementsDTOList != null ){

			for( Inbox_movementsDTO inbox_movementsDTO: inbox_movementsDTOList ){

				add( inbox_movementsDTO );
			}
		}
    }
    
	public Integer setIsSeen(long id) {

		Integer rowUpdated = 0;
		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = DBMW.getInstance().getConnection();

			if (connection == null) {
				logger.debug("nullconn");
			}

			String sql = "UPDATE inbox_movements set is_seen = 1 where ID = ? ";

			ps = connection.prepareStatement(sql);

			int index = 1;

			ps.setLong(index++, id);

			logger.debug(ps);
			rowUpdated = ps.executeUpdate();

		} catch (Exception ex) {

			ex.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e) {
			}
			try {
				if (connection != null) {
					DBMW.getInstance().freeConnection(connection);
				}
			} catch (Exception ex2) {
			}
		}
		return rowUpdated;
	}

    public Integer setIsSeenIfMatchesToOrganogramId( Long rowId, Long toOrganogramId, Integer isSeen ) {

		if( rowId == null )
			return -1;

		Integer rowUpdated = 0;
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();

		try{
			connection = DBMW.getInstance().getConnection();

			if(connection == null)
			{
				logger.debug("nullconn");
			}

			String sql = "UPDATE inbox_movements set is_seen = ? where ID = ? and to_organogram_id = ?";

			ps = connection.prepareStatement( sql) ;

			int index = 1;

			ps.setInt( index++, isSeen );
			ps.setLong( index++, rowId );
			ps.setLong( index++, toOrganogramId );

			logger.debug(ps);
			rowUpdated = ps.executeUpdate();

		}catch(Exception ex){

			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return rowUpdated;
    }

	public Integer updateActionTaken( String rowIdStr, Boolean actionTaken ) {

		if( rowIdStr == null )
			return -1;

		Long rowId = Long.parseLong( rowIdStr );
		Integer rowUpdated = 0;
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();

		try{
			connection = DBMW.getInstance().getConnection();

			if(connection == null)
			{
				logger.debug("nullconn");
			}

			String sql = "UPDATE inbox_movements set action_taken = ? where ID = ?";

			ps = connection.prepareStatement( sql) ;

			int index = 1;

			ps.setBoolean( index++, actionTaken );
			ps.setLong( index++, rowId );

			logger.debug(ps);
			rowUpdated = ps.executeUpdate();

		}catch(Exception ex){

			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return rowUpdated;
	}
}
	