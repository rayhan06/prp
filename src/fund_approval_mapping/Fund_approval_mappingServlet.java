package fund_approval_mapping;

import card_info.CardApprovalResponse;
import common.BaseServlet;
import common.CommonDAOService;
import fund_management.FundApplicationStatus;
import fund_management.Fund_managementDAO;
import fund_management.Fund_managementDTO;
import fund_management.Fund_managementRepository;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import task_type.TaskTypeEnum;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


@WebServlet("/Fund_approval_mappingServlet")
@MultipartConfig
@SuppressWarnings({"Duplicates"})
public class Fund_approval_mappingServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Fund_approval_mappingServlet.class);

    @Override
    public String getTableName() {
        return Fund_approval_mappingDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Fund_approval_mappingServlet";
    }

    @Override
    public Fund_approval_mappingDAO getCommonDAOService() {
        return Fund_approval_mappingDAO.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "search":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.FUND_MANAGEMENT_SEARCH)) {
                        Map<String, String> extraCriteriaMap = new HashMap<>();
                        extraCriteriaMap.put("approverEmployeeRecordIdInternal", String.valueOf(userDTO.employee_record_id));
                        request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                        search(request, response);
                        return;
                    }
                    break;
                case "getApprovalPage":
                    long fundManagementId = Long.parseLong(request.getParameter("fundManagementId"));
                    setApprovalPageData(fundManagementId, request, userDTO);
                    request.getRequestDispatcher("fund_approval_mapping/fund_approval_mappingView.jsp").forward(request, response);
                    return;
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "rejectApplication":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.FUND_MANAGEMENT_SEARCH)) {
                        approve(request, userDTO, false);
                        response.sendRedirect("Fund_approval_mappingServlet?actionType=search");
                        return;
                    }
                    break;
                case "approveApplication":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.FUND_MANAGEMENT_SEARCH)) {
                        approve(request, userDTO, true);
                        response.sendRedirect("Fund_approval_mappingServlet?actionType=search");
                        return;
                    }
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void approve(HttpServletRequest request, UserDTO userDTO, boolean isAccepted) throws Exception {
        long requesterEmployeeRecordId = userDTO.employee_record_id;
        long fundManagementId = Long.parseLong(request.getParameter("fundManagementId"));
        Fund_managementDTO fundManagementDTO = Fund_managementRepository.getInstance().getDTOById(fundManagementId);

        FundApprovalModel model =
                new FundApprovalModel.FundApprovalModelBuilder()
                        .setFundManagementDTO(fundManagementDTO)
                        .setRequesterEmployeeRecordId(userDTO.employee_record_id)
                        .setTaskTypeId(TaskTypeEnum.FUND_MANAGEMENT.getValue())
                        .build();

        boolean sendNotificationToUser = false;
        if (isAccepted) {
            CardApprovalResponse approvalResponse = Fund_approval_mappingDAO.getInstance().movedToNextLevelOfApproval(model);
            if (!approvalResponse.hasNextApproval) {
                fundManagementDTO.fundApplicationStatusCat = FundApplicationStatus.APPROVED.getValue();
                fundManagementDTO.lastModificationTime = System.currentTimeMillis();
                fundManagementDTO.modifiedBy = requesterEmployeeRecordId;
                Fund_managementDAO.getInstance().update(fundManagementDTO);
                sendNotificationToUser = true;
            } else {
                FundApprovalNotification.getInstance().sendWaitingForApprovalNotification(
                        approvalResponse.organogramIds, fundManagementDTO
                );
            }
        } else {
            String rejectReason = request.getParameter("reject_reason");
            if (rejectReason != null) {
                rejectReason = Jsoup.clean(rejectReason, Whitelist.simpleText());
                rejectReason = rejectReason.replace("'", " ");
                rejectReason = rejectReason.replace("\"", " ");
            } else {
                rejectReason = "";
            }
            Fund_approval_mappingDAO.getInstance().rejectPendingApproval(model, rejectReason);
            fundManagementDTO.fundApplicationStatusCat = FundApplicationStatus.REJECTED.getValue();
            fundManagementDTO.comment = rejectReason;
            Fund_managementDAO.getInstance().update(fundManagementDTO);
            sendNotificationToUser = true;
        }

        if (sendNotificationToUser) {
            FundApprovalNotification.getInstance().sendApproveOrRejectNotification(
                    Collections.singletonList(fundManagementDTO.organogramId), fundManagementDTO, isAccepted
            );
        }
    }

    private void setApprovalPageData(long fundManagementId, HttpServletRequest request, UserDTO userDTO) throws Exception {
        Fund_managementDTO fund_managementDTO = Fund_managementDAO.getInstance().getDTOByID(fundManagementId);
        if (fund_managementDTO == null)
            throw new Exception("No Fund Application found with fund_managementDTO id = " + fundManagementId);
        request.setAttribute("fund_managementDTO", fund_managementDTO);

        Fund_approval_mappingDTO approverFundManagementDTO =
                Fund_approval_mappingDAO.getInstance().getByFundAndApproverRecordsId(fundManagementId, userDTO.employee_record_id);

        if (approverFundManagementDTO == null)
            throw new Exception("User Not allowed to approve Fund for fund_managementDTO id = " + fundManagementId);
        request.setAttribute("approverFundManagementDTO", approverFundManagementDTO);
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.FUND_APPROVAL_MAPPING_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.FUND_APPROVAL_MAPPING_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.FUND_APPROVAL_MAPPING_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Fund_approval_mappingServlet.class;
    }
}

