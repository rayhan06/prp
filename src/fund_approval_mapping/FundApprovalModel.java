package fund_approval_mapping;

import fund_management.Fund_managementDTO;

public class FundApprovalModel {
    private final long taskTypeId;
    private final long requesterEmployeeRecordId;
    private final Fund_managementDTO fundManagementDTO;

    private FundApprovalModel(FundApprovalModelBuilder builder) {
        taskTypeId = builder.taskTypeId;
        requesterEmployeeRecordId = builder.requesterEmployeeRecordId;
        fundManagementDTO = builder.fundManagementDTO;
    }

    public long getTaskTypeId() {
        return taskTypeId;
    }

    public long getRequesterEmployeeRecordId() {
        return requesterEmployeeRecordId;
    }

    public Fund_managementDTO getFundManagementDTO() {
        return fundManagementDTO;
    }

    public static class FundApprovalModelBuilder {
        private long taskTypeId;
        private long requesterEmployeeRecordId;
        private Fund_managementDTO fundManagementDTO;

        public FundApprovalModel build() {
            return new FundApprovalModel(this);
        }

        public FundApprovalModelBuilder setTaskTypeId(long taskTypeId) {
            this.taskTypeId = taskTypeId;
            return this;
        }

        public FundApprovalModelBuilder setRequesterEmployeeRecordId(long requesterEmployeeRecordId) {
            this.requesterEmployeeRecordId = requesterEmployeeRecordId;
            return this;
        }

        public FundApprovalModelBuilder setFundManagementDTO(Fund_managementDTO fundManagementDTO) {
            this.fundManagementDTO = fundManagementDTO;
            return this;
        }
    }
}
