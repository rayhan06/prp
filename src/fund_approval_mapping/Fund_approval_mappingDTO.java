package fund_approval_mapping;

import util.CommonDTO;

public class Fund_approval_mappingDTO extends CommonDTO {
    public long fundManagementId = -1;
    public long cardApprovalId = -1;
    public int sequence = -1;
    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;
    public int fundApplicationStatusCat = 1;
    public int fundTypeCat = 1;
    public long taskTypeId = -1;
    public long employeeRecordsId = -1;
    public long approverEmployeeRecordsId = -1;
    public String comment  = "";


    @Override
    public String toString() {
        return "$Fund_approval_mappingDTO[" +
                " iD = " + iD +
                " fundManagementId = " + fundManagementId +
                " cardApprovalId = " + cardApprovalId +
                " sequence = " + sequence +
                " isDeleted = " + isDeleted +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                " fundApplicationStatusCat = " + fundApplicationStatusCat +
                " taskTypeId = " + taskTypeId +
                " employeeRecordsId = " + employeeRecordsId +
                " approverEmployeeRecordsId = " + approverEmployeeRecordsId +
                " searchColumn = " + searchColumn +
                "]";
    }
}