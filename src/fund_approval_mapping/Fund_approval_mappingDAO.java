package fund_approval_mapping;

import card_info.*;
import common.ConnectionAndStatementUtil;
import common.EmployeeCommonDAOService;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import fund_management.FundApplicationStatus;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import org.apache.log4j.Logger;
import pb.CatDAO;
import task_type_approval_path.TaskTypeApprovalPathDTO;
import task_type_approval_path.TaskTypeApprovalPathRepository;
import util.CommonDTO;
import util.LockManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@SuppressWarnings({"Duplicates", "SameParameterValue"})
public class Fund_approval_mappingDAO implements EmployeeCommonDAOService<Fund_approval_mappingDTO> {

    private static final Logger logger = Logger.getLogger(Fund_approval_mappingDAO.class);

    private static final String addSqlQuery = "INSERT INTO {tableName} (fund_management_id, card_approval_id, sequence, fund_application_status_cat,"
            .concat("task_type_id, employee_records_id, approver_employee_records_id, fund_type_cat, comment,search_column,")
            .concat("modified_by, lastModificationTime, inserted_by, insertion_time, isDeleted, ID)")
            .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery = "UPDATE {tableName} SET fund_management_id = ?, card_approval_id = ?, sequence = ?,"
            .concat("fund_application_status_cat = ?, task_type_id = ?, employee_records_id = ?, approver_employee_records_id = ?,")
            .concat("fund_type_cat, comment  = ?, search_column = ?,modified_by = ?, lastModificationTime = ? WHERE ID = ?");

    private static final String getByFundManagementId =
            "SELECT * FROM fund_approval_mapping WHERE fund_management_id = %d AND isDeleted = 0 ORDER BY sequence DESC";

    private static final String getByFundAndApproverRecordsId =
            "SELECT * FROM fund_approval_mapping WHERE fund_management_id = %d AND approver_employee_records_id = %d";

    private static final String getByFundIdAndStatus =
            "SELECT * FROM fund_approval_mapping WHERE fund_management_id = %d AND fund_application_status_cat = %d AND isDeleted = 0";

    private static final String updateStatus =
            "UPDATE fund_approval_mapping SET fund_application_status_cat = %d,isDeleted = %d,modified_by = %d,lastModificationTime = %d WHERE ID IN (%s)";

    private static final String updateStatusByFundId =
            "UPDATE fund_approval_mapping SET fund_application_status_cat = ?,comment = ?,isDeleted =?," +
                    "modified_by = ?,lastModificationTime = ? WHERE fund_application_status_cat = ? AND fund_management_id = ?";

    private static final Map<String, String> searchMap = new HashMap<>();

    private Fund_approval_mappingDAO() {
        searchMap.put("employeeRecordId", " AND (employee_records_id = ?)");
        searchMap.put("approverEmployeeRecordIdInternal", " AND (approver_employee_records_id = ?)");
        searchMap.put("fundTypeCat", " AND (fund_type_cat = ?)");
        searchMap.put("fundApplicationStatusCat", " AND (fund_application_status_cat = ?)");
    }

    private static class LazyLoader {
        static final Fund_approval_mappingDAO INSTANCE = new Fund_approval_mappingDAO();
    }

    public static Fund_approval_mappingDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Fund_approval_mappingDTO fund_approval_mappingDTO) {
        fund_approval_mappingDTO.searchColumn = "";
        fund_approval_mappingDTO.searchColumn += fund_approval_mappingDTO.insertedBy + " ";
        fund_approval_mappingDTO.searchColumn += fund_approval_mappingDTO.modifiedBy + " ";
        fund_approval_mappingDTO.searchColumn += CatDAO.getName("English", "fund_application_status", fund_approval_mappingDTO.fundApplicationStatusCat) + " " + CatDAO.getName("Bangla", "fund_application_status", fund_approval_mappingDTO.fundApplicationStatusCat) + " ";
    }

    @Override
    public void set(PreparedStatement ps, Fund_approval_mappingDTO dto, boolean isInsert) throws SQLException {
        setSearchColumn(dto);
        int index = 0;
        ps.setLong(++index, dto.fundManagementId);
        ps.setLong(++index, dto.cardApprovalId);
        ps.setInt(++index, dto.sequence);
        ps.setInt(++index, dto.fundApplicationStatusCat);
        ps.setLong(++index, dto.taskTypeId);
        ps.setLong(++index, dto.employeeRecordsId);
        ps.setLong(++index, dto.approverEmployeeRecordsId);
        ps.setInt(++index, dto.fundTypeCat);
        ps.setString(++index, dto.comment);
        ps.setString(++index, dto.searchColumn);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, dto.insertedBy);
            ps.setObject(++index, dto.insertionTime);
            ps.setObject(++index, 0 /* isDeleted */);
        }
        ps.setObject(++index, dto.iD);
    }

    @Override
    public Fund_approval_mappingDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Fund_approval_mappingDTO dto = new Fund_approval_mappingDTO();

            dto.iD = rs.getLong("ID");
            dto.fundManagementId = rs.getLong("fund_management_id");
            dto.cardApprovalId = rs.getLong("card_approval_id");
            dto.sequence = rs.getInt("sequence");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.fundApplicationStatusCat = rs.getInt("fund_application_status_cat");
            dto.taskTypeId = rs.getLong("task_type_id");
            dto.employeeRecordsId = rs.getLong("employee_records_id");
            dto.approverEmployeeRecordsId = rs.getLong("approver_employee_records_id");
            dto.fundTypeCat = rs.getInt("fund_type_cat");
            dto.comment = rs.getString("comment");
            dto.searchColumn = rs.getString("search_column");

            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "fund_approval_mapping";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Fund_approval_mappingDTO) commonDTO, addSqlQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Fund_approval_mappingDTO) commonDTO, updateSqlQuery, false);
    }

    public Fund_approval_mappingDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }

    public CardApprovalResponse createFundApproval(FundApprovalModel model) throws Exception {
        String sql = String.format(getByFundManagementId, model.getFundManagementDTO().iD);
        Fund_approval_mappingDTO approvalMappingDTO = ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
        if (approvalMappingDTO != null) {
            throw new DuplicateCardInfoException("Approval is already created for " + model.getFundManagementDTO().iD);
        }

        CardApprovalResponse cardApprovalResponse = new CardApprovalResponse();
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance()
                                                                      .getByEmployeeRecordIdIsDefault(model.getRequesterEmployeeRecordId());
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance()
                                                                                     .getById(employeeOfficeDTO.officeUnitOrganogramId);

        TaskTypeApprovalPathDTO taskTypeApprovalPathDTO = new TaskTypeApprovalPathDTO();
        taskTypeApprovalPathDTO.officeUnitOrganogramId = officeUnitOrganograms.superior_designation_id;
        if (taskTypeApprovalPathDTO.officeUnitOrganogramId == 0) {
            logger.debug("taskTypeApprovalPathDTO.officeUnitOrganogramId = 0 (No Superior); For that card approval direct moved to next level");
            return movedToNextLevel(model, 2);
        } else {
            cardApprovalResponse.hasNextApproval = true;
            cardApprovalResponse.organogramIds = addToApprovalAndGetAddedOrganogramIds(Collections.singletonList(taskTypeApprovalPathDTO), model, 1);
        }
        return cardApprovalResponse;
    }

    private CardApprovalResponse movedToNextLevel(FundApprovalModel model, int nextLevel) {
        CardApprovalResponse response = new CardApprovalResponse();
        List<TaskTypeApprovalPathDTO> nextApprovalPath = TaskTypeApprovalPathRepository.getInstance()
                                                                                       .getByTaskTypeIdAndLevel(model.getTaskTypeId(), nextLevel);

        if (nextApprovalPath == null || nextApprovalPath.size() == 0) {
            response.hasNextApproval = false;
            response.organogramIds = null;
        } else {
            List<Long> organogramIds = addToApprovalAndGetAddedOrganogramIds(nextApprovalPath, model, nextLevel);
            response.hasNextApproval = true;
            response.organogramIds = organogramIds;
        }
        return response;
    }

    private Fund_approval_mappingDTO buildDTO(long approverEmployeeRecordId, FundApprovalModel model, int level,
                                              int isDeleted, FundApplicationStatus fundApplicationStatus) {
        Fund_approval_mappingDTO dto = new Fund_approval_mappingDTO();
        CardApprovalDTO cardApprovalDTO = CardApprovalRepository.getInstance()
                                                                .getByEmployeeRecordId(approverEmployeeRecordId, model.getRequesterEmployeeRecordId());
        if (cardApprovalDTO == null) {
            return null;
        }

        dto.fundManagementId = model.getFundManagementDTO().iD;
        dto.fundTypeCat = model.getFundManagementDTO().fundTypeCat;
        dto.fundApplicationStatusCat = fundApplicationStatus.getValue();
        dto.cardApprovalId = cardApprovalDTO.iD;
        dto.sequence = level;
        dto.taskTypeId = model.getTaskTypeId();
        dto.insertedBy = dto.modifiedBy = model.getRequesterEmployeeRecordId();
        dto.insertionTime = dto.lastModificationTime = System.currentTimeMillis();
        dto.isDeleted = isDeleted;
        dto.employeeRecordsId = model.getFundManagementDTO().employeeRecordsId;
        dto.approverEmployeeRecordsId = cardApprovalDTO.employeeRecordId;

        return dto;
    }

    private List<Long> addToApprovalAndGetAddedOrganogramIds(List<TaskTypeApprovalPathDTO> nextApprovalPath, FundApprovalModel model, int level) {
        List<Long> addedToApproval = new ArrayList<>();
        nextApprovalPath.forEach(approver -> {
            EmployeeOfficeDTO approverOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(approver.officeUnitOrganogramId);
            if (approverOfficeDTO != null) {
                Fund_approval_mappingDTO dto = buildDTO(
                        approverOfficeDTO.employeeRecordId, model, level,
                        0, FundApplicationStatus.WAITING_FOR_APPROVAL
                );
                if (dto != null) {
                    try {
                        add(dto);
                        addedToApproval.add(approverOfficeDTO.officeUnitOrganogramId);
                    } catch (Exception e) {
                        logger.error(e);
                    }
                }
            }
        });
        return addedToApproval;
    }

    public List<Fund_approval_mappingDTO> getAllApprovalDTOByFundIdNotPending(long fundManagementId) {
        String sql = String.format(getByFundManagementId, fundManagementId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet)
                                         .stream()
                                         .filter(dto -> dto.fundApplicationStatusCat != FundApplicationStatus.WAITING_FOR_APPROVAL.getValue())
                                         .filter(dto -> dto.approverEmployeeRecordsId == dto.modifiedBy)
                                         .collect(toList());
    }

    public List<Fund_approval_mappingDTO> getAllApprovalDTOByFundId(long fundManagementId) {
        String sql = String.format(getByFundManagementId, fundManagementId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet)
                                         .stream()
                                         .filter(dto -> dto.fundApplicationStatusCat == FundApplicationStatus.WAITING_FOR_APPROVAL.getValue()
                                                 || dto.approverEmployeeRecordsId == dto.modifiedBy)
                                         .collect(toList());
    }

    public Fund_approval_mappingDTO getByFundAndApproverRecordsId(long fundManagementId, long approverEmployeeId) {
        String sql = String.format(getByFundAndApproverRecordsId, fundManagementId, approverEmployeeId);
        return ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
    }

    public ValidApprovalInfo validateInput(FundApprovalModel model) throws InvalidDataException, AlreadyApprovedException {
        String sql = String.format(
                getByFundIdAndStatus,
                model.getFundManagementDTO().iD,
                FundApplicationStatus.WAITING_FOR_APPROVAL.getValue()
        );

        List<Fund_approval_mappingDTO> dtoList = ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
        if (dtoList == null || dtoList.size() == 0) {
            throw new InvalidDataException("No Pending approval is found for fundManagementId : " + model.getFundManagementDTO().iD);
        }

        Fund_approval_mappingDTO requesterApprovalDTO =
                dtoList.stream()
                       .filter(dto -> dto.approverEmployeeRecordsId == model.getRequesterEmployeeRecordId())
                       .findAny()
                       .orElse(null);

        if (requesterApprovalDTO == null) {
            sql = String.format(getByFundAndApproverRecordsId, model.getFundManagementDTO().iD, model.getRequesterEmployeeRecordId());
            requesterApprovalDTO = ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
            if (requesterApprovalDTO != null) {
                throw new AlreadyApprovedException();
            }
            throw new InvalidDataException("Invalid employee request to approve");
        }

        Set<Integer> sequenceValueSet = dtoList.stream()
                                               .map(e -> e.sequence)
                                               .collect(Collectors.toSet());
        if (sequenceValueSet.size() > 1) {
            throw new InvalidDataException(
                    "Multiple sequence value is found for Pending approval of fundManagementId : "
                            + model.getFundManagementDTO().iD
            );
        }
        return new ValidApprovalInfo(dtoList, (Integer) sequenceValueSet.toArray()[0], requesterApprovalDTO);
    }

    private static class ValidApprovalInfo {
        List<Fund_approval_mappingDTO> dtoList;
        int currentLevel;
        Fund_approval_mappingDTO approvalMappingDTO;

        public ValidApprovalInfo(List<Fund_approval_mappingDTO> dtoList, int currentLevel, Fund_approval_mappingDTO approvalMappingDTO) {
            this.dtoList = dtoList;
            this.currentLevel = currentLevel;
            this.approvalMappingDTO = approvalMappingDTO;
        }
    }

    public CardApprovalResponse movedToNextLevelOfApproval(FundApprovalModel model) throws InvalidDataException, AlreadyApprovedException {
        synchronized (LockManager.getLock(model.getFundManagementDTO().iD + "FAMMTNLA")) {
            ValidApprovalInfo validApprovalInfo = validateInput(model);

            String idsToUpdate =
                    validApprovalInfo.dtoList.stream()
                                             .map(e -> String.valueOf(e.iD))
                                             .collect(Collectors.joining(","));

            String sql = String.format(
                    updateStatus, FundApplicationStatus.APPROVED.getValue(),
                    0, model.getRequesterEmployeeRecordId(), System.currentTimeMillis(), idsToUpdate
            );

            boolean res = (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
                try {
                    st.executeUpdate(sql);
                    return true;
                } catch (SQLException ex) {
                    logger.error(ex);
                    return false;
                }
            });
            if (!res) {
                throw new InvalidDataException("Exception occurred during updating approval status");
            }
            return movedToNextLevel(model, validApprovalInfo.currentLevel + 1);
        }
    }

    public boolean rejectPendingApproval(FundApprovalModel model, String rejectReason) throws InvalidDataException, AlreadyApprovedException {
        synchronized (LockManager.getLock(model.getFundManagementDTO().iD + "FAMDRejectPA")) {
            validateInput(model);
            return (Boolean) ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
                try {
                    int index = 0;

                    ps.setInt(++index, FundApplicationStatus.REJECTED.getValue());
                    ps.setString(++index, rejectReason);
                    ps.setInt(++index, 0);
                    ps.setLong(++index, model.getRequesterEmployeeRecordId());
                    ps.setLong(++index, System.currentTimeMillis());
                    ps.setInt(++index, FundApplicationStatus.WAITING_FOR_APPROVAL.getValue());
                    ps.setObject(++index, model.getFundManagementDTO().iD);

                    logger.info(ps);
                    ps.executeUpdate();
                    return true;
                } catch (SQLException ex) {
                    logger.error(ex);
                    return false;
                }
            }, updateStatusByFundId);
        }
    }
}
	