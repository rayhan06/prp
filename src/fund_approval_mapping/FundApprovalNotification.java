package fund_approval_mapping;

import fund_management.Fund_managementDTO;
import pb_notifications.Pb_notificationsDAO;

import java.util.List;

public class FundApprovalNotification {
    private final Pb_notificationsDAO pb_notificationsDAO;

    private FundApprovalNotification(){
        pb_notificationsDAO = Pb_notificationsDAO.getInstance();
    }

    private static class LazyLoader {
        static final FundApprovalNotification INSTANCE = new FundApprovalNotification();
    }

    public static FundApprovalNotification getInstance(){
        return LazyLoader.INSTANCE;
    }

    public void sendWaitingForApprovalNotification(List<Long> organogramIds, Fund_managementDTO fundManagementDTO){
        String textEn = fundManagementDTO.employeeNameEn + "(" + fundManagementDTO.organogramNameEn + ","
                + fundManagementDTO.officeNameEn + ")'s fund application is waiting for your approval";

        String textBn = fundManagementDTO.employeeNameBn + "(" + fundManagementDTO.organogramNameBn + ","
                + fundManagementDTO.officeNameBn + ") এর ফান্ডের আবেদর আপনার আনুমোদনের জন্য অপেক্ষমান";

        String notificationMessage = textEn + "$" + textBn;

        String url = "Fund_approval_mappingServlet?actionType=getApprovalPage&fundManagementId=" + fundManagementDTO.iD;
        sendNotification(organogramIds, notificationMessage, url);
    }

    public void sendApproveOrRejectNotification(List<Long> organogramIds, Fund_managementDTO fundManagementDTO, boolean isApproved){
        String textEn = "Your application for fund has been " + (isApproved ? "accepted" : "rejected");

        String textBn = "আপনার ফান্ডের আবেদন " + (isApproved ? "গৃহীত " : " প্রত্যাখ্যাত ") + " হয়েছে";

        String notificationMessage = textEn + "$" + textBn;

        String url = "Fund_managementServlet?actionType=view&ID=" + fundManagementDTO.iD;

        sendNotification(organogramIds,notificationMessage,url);
    }

    private void sendNotification(List<Long> organogramIds,String notificationMessage,String url){
        if(organogramIds == null || organogramIds.size() == 0) return;

        Thread thread = new Thread(()->{
            long currentTime = System.currentTimeMillis();
            organogramIds.forEach(organogramId -> pb_notificationsDAO.addPb_notifications(organogramId, currentTime, url, notificationMessage));
        });
        thread.setDaemon(true);
        thread.start();
    }
}
