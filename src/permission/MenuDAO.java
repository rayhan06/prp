package permission;

import common.ConnectionAndStatementUtil;
import dbm.DBMW;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class MenuDAO {
    private static final Logger logger = Logger.getLogger(MenuDAO.class);

    private static final String deleteSql = "DELETE FROM menu WHERE menuID IN (";
    private static final String updateSql = "UPDATE menu SET parentMenuID=?,menuName=?,hyperLink=?,languageTextID=?,orderIndex=?,selectedMenuID=?,isVisible=?,"
            .concat("requestMethodType=?,icon=?,isAPI=?,menuNameBangla=?,constantName=?  WHERE menuID = ?");
    private static final String addSql = "insert into menu(parentMenuID,menuName,hyperLink,languageTextID,orderIndex,selectedMenuID,isVisible,requestMethodType,"
            .concat("icon,isAPI,menuNameBangla,constantName,menuID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)");
    private static final String getSql = "SELECT * FROM menu ORDER BY menuID";

    public static final String MENU_ID_PARAM_NAME = "menuId";

    public void deleteMenuByIDList(List<Integer> menuIDList) {
        if (menuIDList == null || menuIDList.isEmpty()) {
            return;
        }
        String sql = menuIDList.stream()
                               .map(String::valueOf)
                               .collect(Collectors.joining(",", deleteSql, ")"));
        logger.debug(sql);
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            try {
                st.execute(sql);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
        MenuRepository.getInstance().reload(menuIDList);
    }


    public void addMenuDTO(MenuDTO menuDTO) {
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                menuDTO.menuID = (int) DBMW.getInstance().getNextSequenceId("menu");
                setObjectForPrepareStatement(ps, menuDTO);
                ps.execute();
            } catch (Exception e) {
                logger.error(e);
            }
        }, addSql);
        MenuRepository.getInstance().reload();
    }


    public void updateMenuDTO(MenuDTO menuDTO) {
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                setObjectForPrepareStatement(ps, menuDTO);
                ps.executeUpdate();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }, updateSql);
        MenuRepository.getInstance().reload();
    }


    public List<MenuDTO> getAllMenuList() {
        return ConnectionAndStatementUtil.getListOfT(getSql, this::buildMenuDTO);
    }

    private MenuDTO buildMenuDTO(ResultSet rs) {
        try {
            MenuDTO menuDTO = new MenuDTO();
            menuDTO.menuID = rs.getInt("menuID");
            menuDTO.parentMenuID = rs.getInt("parentMenuID");
            menuDTO.menuName = rs.getString("menuName");
            menuDTO.hyperLink = rs.getString("hyperLink");
            menuDTO.languageTextID = rs.getInt("languageTextID");
            menuDTO.orderIndex = rs.getInt("orderIndex");
            menuDTO.selectedMenuID = rs.getInt("selectedMenuID");
            menuDTO.isVisible = (rs.getInt("isVisible") == 1);
            menuDTO.requestMethodType = rs.getInt("requestMethodType");
            menuDTO.icon = rs.getString("icon");
            menuDTO.isAPI = (rs.getInt("isAPI") == 1);
            menuDTO.menuNameBangla = rs.getString("menuNameBangla");
            menuDTO.constant = rs.getString("constantName");
            return menuDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            ex.printStackTrace();
            return null;
        }
    }


    public void updateMenuDTOs(List<MenuDTO> menuDTOList) {
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            menuDTOList.forEach(menuDTO -> {
                try {
                    setObjectForPrepareStatement(ps, menuDTO);
                    ps.executeUpdate();
                } catch (SQLException ex) {
                    logger.error(ex);
                }
            });
        }, updateSql);
        MenuRepository.getInstance().reload();
    }

    private void setObjectForPrepareStatement(PreparedStatement ps, MenuDTO menuDTO) throws SQLException {
        int index = 0;
        ps.setObject(++index, menuDTO.parentMenuID);
        ps.setObject(++index, menuDTO.menuName);
        ps.setObject(++index, menuDTO.hyperLink);
        ps.setObject(++index, menuDTO.languageTextID);
        ps.setObject(++index, menuDTO.orderIndex);
        ps.setObject(++index, menuDTO.selectedMenuID);
        ps.setObject(++index, menuDTO.isVisible);
        ps.setObject(++index, menuDTO.requestMethodType);
        ps.setObject(++index, menuDTO.icon);
        ps.setObject(++index, menuDTO.isAPI);
        ps.setObject(++index, menuDTO.menuNameBangla);
        ps.setObject(++index, menuDTO.constant);
        ps.setObject(++index, menuDTO.menuID);
    }

    // appends menuId in the end of hyperLinks in menu table
    private static void appendsMenuIdInHyperlink() {
        String selectQuery = "SELECT menuID, hyperLink FROM menu";
        class MenuModel {
            long menuID;
            String hyperLink;

            @Override
            public String toString() {
                return "menuID=" + menuID + "\nhyperLink=" + hyperLink;
            }
        }

        List<MenuModel> menuModels = ConnectionAndStatementUtil.getListOfT(selectQuery, rs -> {
            try {
                MenuModel menuModel = new MenuModel();
                menuModel.menuID = rs.getLong("menuID");
                menuModel.hyperLink = rs.getString("hyperLink");
                return menuModel;
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        });

        List<MenuModel> modifiedMenuModelsWithMenuId =
                menuModels.stream()
                          .filter(menuModel -> menuModel.hyperLink.trim().matches(".*actionType=.+$"))
                          // add menuId param
                          //.peek(menuModel -> menuModel.hyperLink += "&" + MENU_ID_PARAM_NAME + "=" + menuModel.menuID)
                          // removes menuId param
                          .peek(menuModel -> menuModel.hyperLink = menuModel.hyperLink.replaceAll("&" + MENU_ID_PARAM_NAME + "=\\d+",""))
                          .collect(Collectors.toList());

        System.out.println(modifiedMenuModelsWithMenuId.size());

        String updateQuery = "UPDATE menu SET hyperLink = ? WHERE menuID = ?";
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            modifiedMenuModelsWithMenuId.forEach(menuModel -> {
                try {
                    ps.setString(1, menuModel.hyperLink);
                    ps.setLong(2, menuModel.menuID);
                    logger.debug(ps);
                    ps.executeUpdate();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            });
        }, updateQuery);
    }
    public static void main(String[] args) {
        // appendsMenuIdInHyperlink();
    }
}