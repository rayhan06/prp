package permission;

import java.io.PrintWriter;
import java.util.stream.Collectors;

public class MenuConstantGenerator {

	public static void main(String[] arrgs) throws Exception{
		String preText = "\tint ";
		String FILE_NAME = "MenuConstants";
		String PACKAGE_NAME = "permission";
		String fileAbsolutePath  = "src/"+PACKAGE_NAME.replaceAll("\\.", "/")+"/"+FILE_NAME+".java";
		String menuConstants = MenuRepository.getAllMenuDTOListSortedByMenuId()
				.stream()
				.map(menuDTO -> preText+menuDTO.constant.replaceAll(" ","")+"="+menuDTO.menuID+";\n")
				.collect(Collectors.joining("","package "+PACKAGE_NAME+";\n\n@SuppressWarnings({\"unused\"})\npublic interface "+FILE_NAME+"{\n","}"));

		System.out.println("File path "+fileAbsolutePath);
		PrintWriter writer = new PrintWriter(fileAbsolutePath, "UTF-8");
		writer.print(menuConstants);
		writer.close();
	}
}