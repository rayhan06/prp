package permission;

import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import role.PermissionRepository;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@SuppressWarnings("ALL")
public class MenuRepository {
    private static final Logger logger = Logger.getLogger(MenuRepository.class);
    private Map<Integer, MenuDTO> mapOfMenuToMenuID;
    private List<MenuDTO> preorderMenuList;
    private Map<Integer, List<MenuDTO>> mapByRequestType;
    private static final Map<Long, Map<Integer, String>> mapByRoleIdThenLanguage = new ConcurrentHashMap<>();

    private MenuRepository() {
        mapOfMenuToMenuID = new ConcurrentHashMap<>();
        reload();
    }

    public List<Integer> getRootToMenuByMenuID(int menuID) {
        List<Integer> pathFromMenuToRoot = new ArrayList<>();
        MenuDTO menuDTO = getMenuDTOByMenuID(menuID);
        while (menuDTO != null) {
            pathFromMenuToRoot.add(menuDTO.getMenuID());
            menuDTO = getMenuDTOByMenuID(menuDTO.getParentMenuID());
        }
        Collections.reverse(pathFromMenuToRoot);
        return pathFromMenuToRoot;
    }

    private static MenuRepository INSTANCE;

    public static MenuRepository getInstance() {
        if(INSTANCE == null){
            synchronized (MenuRepository.class){
                if(INSTANCE == null){
                    INSTANCE = new MenuRepository();
                }
            }
        }
        return INSTANCE;
    }

    private MenuDTO cloneMenu(MenuDTO menuDTO) {
        MenuDTO menuDTOClone = new MenuDTO();
        menuDTOClone.hyperLink = menuDTO.hyperLink;
        menuDTOClone.languageTextID = menuDTO.languageTextID;
        menuDTOClone.menuID = menuDTO.menuID;
        menuDTOClone.menuName = menuDTO.menuName;
        menuDTOClone.parentMenuID = menuDTO.parentMenuID;
        menuDTOClone.isVisible = menuDTO.isVisible;
        menuDTOClone.orderIndex = menuDTO.orderIndex;
        menuDTOClone.requestMethodType = menuDTO.requestMethodType;
        menuDTOClone.selectedMenuID = menuDTO.selectedMenuID;
        menuDTOClone.icon = menuDTO.icon;
        menuDTOClone.isAPI = menuDTO.isAPI;
        menuDTOClone.menuNameBangla = menuDTO.menuNameBangla;
        menuDTOClone.constant = menuDTO.constant;
        return menuDTOClone;
    }

    public List<Integer> getSubtreeMenuIDListByMenuID(int menuID) {
        List<Integer> menuIDList = new ArrayList<>();

        MenuDTO menuDTO = mapOfMenuToMenuID.get(menuID);

        if (menuDTO != null) {
            menuIDList.add(menuID);
        } else {
            return menuIDList;
        }

        for (MenuDTO childMenuDTO : menuDTO.getChildMenuList()) {
            menuIDList.addAll(getSubtreeMenuIDListByMenuID(childMenuDTO.menuID));
        }

        return menuIDList;
    }
    
    
    private String normalizeHyperlink(String hyperlink) {
        if (hyperlink == null) {
            return null;
        }
        String[] hyperlinkParts = hyperlink.split("\\?");
        if (hyperlinkParts.length < 2) {
            return hyperlink;
        }
        String baseURI = hyperlinkParts[0];
        String queryString = hyperlinkParts[1];

        String[] queryParts = queryString.split("&");
        String queryLinkToAdd = "";
        String actionType = "";
        for(String queryPart: queryParts)
        {
        	if(queryPart.startsWith("actionType"))
        	{
        		queryLinkToAdd = queryPart;
        		actionType = queryLinkToAdd.split("=")[1];
        		if(actionType.equalsIgnoreCase("view"))
        		{
        			actionType = "search";
        		}
        		else if(actionType.equalsIgnoreCase("getEditPage"))
        		{
        			actionType = "getAddPage";
        		}
        		else if(actionType.equalsIgnoreCase("massAssign"))
        		{
        			actionType = "viewUserAssets";
        		}
        		break;
        	}
        }
        return baseURI + "?actionType=" + actionType;
       
    }

    public MenuDTO getMenuDTOByHyperlinkAndRequestType(String hyperlink, int requestType) {
        String normalizedHyperlink = normalizeHyperlink(hyperlink);
        System.out.println("normalizedHyperlink = " + normalizedHyperlink);
        return getMenuDTOForNonEmptyHyperLink(requestType).parallelStream()
                .filter(e -> matchHyperLink(normalizedHyperlink, e.hyperLink))
                .findAny()
                .orElse(null);
    }

    private boolean matchHyperLink(String baseLink, String testerLink) {
        testerLink = testerLink.replaceAll("\\?", "\\\\?").replaceAll("\\.", "\\\\.");
        testerLink = normalizeHyperlink(testerLink);
        return baseLink.matches(testerLink + ".*");
    }

    public MenuDTO getMenuDTOByMenuID(int menuID) {
        MenuDTO menuDTO = mapOfMenuToMenuID.get(menuID);
        return menuDTO == null ? null : cloneMenu(menuDTO);
    }

    public List<MenuDTO> getInvisibleRootMenuList() {
        return getMenuList(getRootMenuList(), e -> !e.isVisible, e -> e);
    }

    public List<MenuDTO> getVisibleRootMenuList() {
        return getMenuList(getRootMenuList(), e -> e.isVisible, e -> e);
    }

    private List<MenuDTO> getMenuList(List<MenuDTO> list, Predicate<MenuDTO> filter, Function<MenuDTO, MenuDTO> transform) {
        return list.parallelStream()
                .filter(filter)
                .map(transform)
                .collect(Collectors.toList());
    }

    public List<MenuDTO> getRootMenuList() {
        Map<Integer, MenuDTO> mapOfMenuDTOCloneToMenuID = mapOfMenuToMenuID.values().parallelStream()
                .map(this::cloneMenu)
                .collect(Collectors.toMap(menuDTOClone -> menuDTOClone.menuID, menuDTOClone -> menuDTOClone));

        List<MenuDTO> allCloneMenu = new ArrayList<>(mapOfMenuDTOCloneToMenuID.values());
        allCloneMenu.sort(Comparator.comparingInt(o -> o.orderIndex));

        allCloneMenu.stream()
                .filter(e -> e.parentMenuID != -1)
                .forEach(menuDTO -> {
                    MenuDTO parentMenuDTO = mapOfMenuDTOCloneToMenuID.get(menuDTO.parentMenuID);
                    if (parentMenuDTO != null) {
                        parentMenuDTO.addChildPermission(menuDTO);
                    }
                });

        return allCloneMenu.stream()
                .filter(menuDTO -> menuDTO.parentMenuID == -1)
                .collect(Collectors.toList());

    }

    private List<MenuDTO> getPreorderMenuList(MenuDTO menuDTO) {
        List<MenuDTO> resultMenuList = new ArrayList<>();

        resultMenuList.add(menuDTO);
        for (MenuDTO childMenu : menuDTO.getChildMenuList()) {
            resultMenuList.addAll(getPreorderMenuList(childMenu));
        }

        return resultMenuList;
    }

    public List<MenuDTO> getPreorderMenuList() {
        return getMenuList(this.preorderMenuList, e -> true, this::cloneMenu);
    }

    public List<MenuDTO> getPreorderNonAPIMenuList() {
        return getMenuList(this.preorderMenuList, e -> !e.isAPI, this::cloneMenu);
    }

    public List<MenuDTO> getPreorderAPIMenuList() {
        return getMenuList(this.preorderMenuList, e -> e.isAPI, this::cloneMenu);
    }

    public List<MenuDTO> getAllMenuList() {
        return getMenuList(this.preorderMenuList, e -> true, this::cloneMenu);
    }

    public void reload(List<Integer> menuIDList) {
        try {
            menuIDList.parallelStream()
                    .forEach(e -> mapOfMenuToMenuID.remove(e));
            reload();
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public void reload() {
        try{
            logger.debug("MenuRepository loading start");
            List<MenuDTO> allMenuList = new MenuDAO().getAllMenuList();

            mapOfMenuToMenuID = allMenuList.parallelStream()
                    .collect(Collectors.toMap(menuDTO -> menuDTO.menuID, menuDTO -> menuDTO));

            allMenuList.parallelStream()
                    .filter(menuDTO -> mapOfMenuToMenuID.containsKey(menuDTO.parentMenuID))
                    .forEach(menuDTO -> mapOfMenuToMenuID.get(menuDTO.parentMenuID).addChildPermission(menuDTO));

            this.preorderMenuList = allMenuList.stream()
                    .filter(menuDTO -> menuDTO.parentMenuID == -1)
                    .flatMap(rootMenu -> getPreorderMenuList(rootMenu).stream())
                    .collect(Collectors.toList());

            mapByRequestType = allMenuList.stream()
                    .filter(e -> e.hyperLink != null && e.hyperLink.trim().length() > 0)
                    .collect(Collectors.groupingBy(e -> e.requestMethodType));

            logger.debug("MenuRepository loading completed");
        }catch (Exception ex){
            logger.error("Menu repository reload failed "+ex);
            ex.printStackTrace();
            reload();
        }
    }

    public List<MenuDTO> getMenuDTOForNonEmptyHyperLink(int requestMethodType) {
        if (mapByRequestType == null) {
            return new ArrayList<>();
        }
        return mapByRequestType.get(requestMethodType);
    }

    private boolean hasAnyVisibleChild(MenuDTO menuDTO) {
        boolean hasAnyVisibleChild = false;
        for (MenuDTO childMenuDTO : menuDTO.getChildMenuList()) {
            if (childMenuDTO.isVisible) {
                hasAnyVisibleChild = true;
                break;
            }
        }
        return hasAnyVisibleChild;
    }

    private boolean hasChildMenuPermissionByMenuDTOAndRoleID(MenuDTO menuDTO, long roleID) {
        boolean hasChildMenuPermission = false;
        if (menuDTO.getChildMenuList() != null) {
            for (MenuDTO childMenuDTO : menuDTO.getChildMenuList()) {
                boolean hasMenuPermission = PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(roleID, childMenuDTO.menuID);
                if (hasMenuPermission) {
                    hasChildMenuPermission = true;
                    break;
                }
            }
        }
        return hasChildMenuPermission;
    }

    public void printMenu(StringBuilder out, MenuDTO menuDTO, String context, int languageID, long roleID, Integer layer ) {

        boolean hasPermission = PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(roleID, menuDTO.menuID);
        if (!hasPermission || !menuDTO.isVisible) {
            return;
        }

        out.append("<li class=\"kt-menu__item  kt-menu__item--submenu\" aria-haspopup=\"true\" data-ktmenu-submenu-toggle=\"hover\" id=\"" + menuDTO.getMenuID() + "\">\r\n" );
        if (menuDTO.getHyperLink() != null && menuDTO.getHyperLink().trim().length() != 0) {
            out.append("<a href=\"" + context + menuDTO.getHyperLink() + "\" class=\"kt-menu__link \" >\r\n");
        } else {
            out.append("<a href=\"javascript:;\" class=\"kt-menu__link kt-menu__toggle\">\r\n");
        }
        String layericon = "";
        if( layer == 1 ) {
            out.append("<img src='" + context + "assets/images/menu_icons/" + menuDTO.menuID + "@3x.png' height='100%'/> &nbsp;");
        }else{
            switch (layer){
                case 2:
                    layericon = "<img src='" + context + "assets/images/menu_icons/sub@3x.png' style='width:10px; height:10px; margin-right:5px;'/>";
                    break;
                case 3:
                    layericon = "<img src='" + context + "assets/images/menu_icons/sub_sub@3x.png' style='width:10px; height:10px; margin-right:5px;'/>";
                    break;
                case 4:
                    layericon = "<img src='" + context + "assets/images/menu_icons/dot@3x.png' style='width:10px; height:10px; margin-right:5px;'/>";
                    break;
                case 5:
                    layericon = "<img src='" + context + "assets/images/menu_icons/sub_sub_press@3x.png' style='width:10px; height:10px; margin-right:5px;margin-left:15px'/>";
                    break;
            }
        }

        out.append( "<span class=\"kt-menu__link-text\" >" + layericon + (languageID == CommonConstant.Language_ID_English ? menuDTO.getMenuName() : menuDTO.menuNameBangla) + "</span>\n");

        boolean hasAnyVisibleChild = hasAnyVisibleChild(menuDTO);

        if (hasAnyVisibleChild) {

            boolean hasChildMenuPermission = hasChildMenuPermissionByMenuDTOAndRoleID(menuDTO, roleID);


            if (hasChildMenuPermission) {

                if (menuDTO.getChildMenuList() != null && !menuDTO.getChildMenuList().isEmpty()) {
                    out.append("<i class=\"kt-menu__ver-arrow la la-angle-right\"></i>");
                }
            }

            out.append("</a>\n");


            if (hasChildMenuPermission) {

                out.append("<div class=\"kt-menu__submenu \"> <span class=\"kt-menu__arrow\"></span> <ul class=\"kt-menu__subnav\">\n");
                for (MenuDTO childMenuDTO : menuDTO.getChildMenuList()) {

                    printMenu( out, childMenuDTO, context, languageID,roleID, layer+1 );

                }
                out.append("</ul></div></li>\n");
            }

        } else {
            out.append("</a></li>\n");
        }
    }

    public String getSideBar(LoginDTO loginDTO, String context) {
        UserDTO userDTO;
        if (loginDTO.isOisf == 1) {
            userDTO = UserRepository.getInstance().getUserDTOByOrganogramID(loginDTO.userID);
        } else {
            userDTO = UserRepository.getInstance().getUserDTOByUserID(loginDTO);
        }
        long roleID = (userDTO == null ? -1 : userDTO.roleID);
        int language = userDTO != null ? LM.getLanguageIDByUserDTO(userDTO) : CommonConstant.Language_ID_Bangla;
        logger.debug("language : " + language);
        if (mapByRoleIdThenLanguage.get(roleID) == null || mapByRoleIdThenLanguage.get(roleID).get(language) == null) {
            buildSideBar(context, roleID, language, false);
        }
        return mapByRoleIdThenLanguage.get(roleID).get(language);
    }

    private synchronized void buildSideBar(String context, long roleID, int language, boolean reset) {
        if (!reset && mapByRoleIdThenLanguage.get(roleID) != null && mapByRoleIdThenLanguage.get(roleID).get(language) != null) {
            return;
        }
        logger.debug("Building sidebar for context : " + context);
        logger.debug("roleID : " + roleID);
        logger.debug("language : " + language);
        logger.debug("reset : " + reset);
        List<MenuDTO> rootMenuList = getVisibleRootMenuList();
        StringBuilder htmlContent = new StringBuilder();
        for (MenuDTO menuDTO : rootMenuList) {
            printMenu(htmlContent, menuDTO, context, language, roleID,1);
        }
        Map<Integer, String> sideBarByRoleId = mapByRoleIdThenLanguage.get(roleID);
        if (sideBarByRoleId == null) {
            sideBarByRoleId = new ConcurrentHashMap<>();
        }
        sideBarByRoleId.put(language, htmlContent.toString());
        mapByRoleIdThenLanguage.put(roleID, sideBarByRoleId);
    }

    public void reBuildSideBar(String context, long roleID) {
        ExecutorService executorService = Executors.newSingleThreadExecutor(r -> {
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            return thread;
        });
        executorService.execute(() -> {
            buildSideBar(context, roleID, CommonConstant.Language_ID_English, true);
            buildSideBar(context, roleID, CommonConstant.Language_ID_Bangla, true);
            executorService.shutdown();
        });
    }

    public static List<MenuDTO> getAllMenuDTOListSortedByMenuId(){
        return new MenuDAO().getAllMenuList();
    }
}