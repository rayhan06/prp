package employee_license_key;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Employee_license_keyRepository implements Repository {
	Employee_license_keyDAO employee_license_keyDAO = null;
	
	public void setDAO(Employee_license_keyDAO employee_license_keyDAO)
	{
		this.employee_license_keyDAO = employee_license_keyDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Employee_license_keyRepository.class);
	Map<Long, Employee_license_keyDTO>mapOfEmployee_license_keyDTOToiD;
	Map<Long, Set<Employee_license_keyDTO> >mapOfEmployee_license_keyDTOToassetLicenseKeyId;
	Map<Long, Set<Employee_license_keyDTO> >mapOfEmployee_license_keyDTOToemployeeRecordsId;
	Map<String, Set<Employee_license_keyDTO> >mapOfEmployee_license_keyDTOToremarks;
	Map<Long, Set<Employee_license_keyDTO> >mapOfEmployee_license_keyDTOToinsertionDate;
	Map<String, Set<Employee_license_keyDTO> >mapOfEmployee_license_keyDTOToinsertedBy;
	Map<String, Set<Employee_license_keyDTO> >mapOfEmployee_license_keyDTOTomodifiedBy;
	Map<Long, Set<Employee_license_keyDTO> >mapOfEmployee_license_keyDTOTolastModificationTime;
	Map<String, Set<Employee_license_keyDTO> >mapOfEmployee_license_keyDTOTosearchColumn;


	static Employee_license_keyRepository instance = null;  
	private Employee_license_keyRepository(){
		mapOfEmployee_license_keyDTOToiD = new ConcurrentHashMap<>();
		mapOfEmployee_license_keyDTOToassetLicenseKeyId = new ConcurrentHashMap<>();
		mapOfEmployee_license_keyDTOToemployeeRecordsId = new ConcurrentHashMap<>();
		mapOfEmployee_license_keyDTOToremarks = new ConcurrentHashMap<>();
		mapOfEmployee_license_keyDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfEmployee_license_keyDTOToinsertedBy = new ConcurrentHashMap<>();
		mapOfEmployee_license_keyDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfEmployee_license_keyDTOTolastModificationTime = new ConcurrentHashMap<>();
		mapOfEmployee_license_keyDTOTosearchColumn = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Employee_license_keyRepository getInstance(){
		if (instance == null){
			instance = new Employee_license_keyRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(employee_license_keyDAO == null)
		{
			return;
		}
		try {
			List<Employee_license_keyDTO> employee_license_keyDTOs = employee_license_keyDAO.getAllEmployee_license_key(reloadAll);
			for(Employee_license_keyDTO employee_license_keyDTO : employee_license_keyDTOs) {
				Employee_license_keyDTO oldEmployee_license_keyDTO = getEmployee_license_keyDTOByID(employee_license_keyDTO.iD);
				if( oldEmployee_license_keyDTO != null ) {
					mapOfEmployee_license_keyDTOToiD.remove(oldEmployee_license_keyDTO.iD);
				
					if(mapOfEmployee_license_keyDTOToassetLicenseKeyId.containsKey(oldEmployee_license_keyDTO.assetLicenseKeyId)) {
						mapOfEmployee_license_keyDTOToassetLicenseKeyId.get(oldEmployee_license_keyDTO.assetLicenseKeyId).remove(oldEmployee_license_keyDTO);
					}
					if(mapOfEmployee_license_keyDTOToassetLicenseKeyId.get(oldEmployee_license_keyDTO.assetLicenseKeyId).isEmpty()) {
						mapOfEmployee_license_keyDTOToassetLicenseKeyId.remove(oldEmployee_license_keyDTO.assetLicenseKeyId);
					}
					
					if(mapOfEmployee_license_keyDTOToemployeeRecordsId.containsKey(oldEmployee_license_keyDTO.employeeRecordsId)) {
						mapOfEmployee_license_keyDTOToemployeeRecordsId.get(oldEmployee_license_keyDTO.employeeRecordsId).remove(oldEmployee_license_keyDTO);
					}
					if(mapOfEmployee_license_keyDTOToemployeeRecordsId.get(oldEmployee_license_keyDTO.employeeRecordsId).isEmpty()) {
						mapOfEmployee_license_keyDTOToemployeeRecordsId.remove(oldEmployee_license_keyDTO.employeeRecordsId);
					}
					
					if(mapOfEmployee_license_keyDTOToremarks.containsKey(oldEmployee_license_keyDTO.remarks)) {
						mapOfEmployee_license_keyDTOToremarks.get(oldEmployee_license_keyDTO.remarks).remove(oldEmployee_license_keyDTO);
					}
					if(mapOfEmployee_license_keyDTOToremarks.get(oldEmployee_license_keyDTO.remarks).isEmpty()) {
						mapOfEmployee_license_keyDTOToremarks.remove(oldEmployee_license_keyDTO.remarks);
					}
					
					if(mapOfEmployee_license_keyDTOToinsertionDate.containsKey(oldEmployee_license_keyDTO.insertionDate)) {
						mapOfEmployee_license_keyDTOToinsertionDate.get(oldEmployee_license_keyDTO.insertionDate).remove(oldEmployee_license_keyDTO);
					}
					if(mapOfEmployee_license_keyDTOToinsertionDate.get(oldEmployee_license_keyDTO.insertionDate).isEmpty()) {
						mapOfEmployee_license_keyDTOToinsertionDate.remove(oldEmployee_license_keyDTO.insertionDate);
					}
					
					if(mapOfEmployee_license_keyDTOToinsertedBy.containsKey(oldEmployee_license_keyDTO.insertedBy)) {
						mapOfEmployee_license_keyDTOToinsertedBy.get(oldEmployee_license_keyDTO.insertedBy).remove(oldEmployee_license_keyDTO);
					}
					if(mapOfEmployee_license_keyDTOToinsertedBy.get(oldEmployee_license_keyDTO.insertedBy).isEmpty()) {
						mapOfEmployee_license_keyDTOToinsertedBy.remove(oldEmployee_license_keyDTO.insertedBy);
					}
					
					if(mapOfEmployee_license_keyDTOTomodifiedBy.containsKey(oldEmployee_license_keyDTO.modifiedBy)) {
						mapOfEmployee_license_keyDTOTomodifiedBy.get(oldEmployee_license_keyDTO.modifiedBy).remove(oldEmployee_license_keyDTO);
					}
					if(mapOfEmployee_license_keyDTOTomodifiedBy.get(oldEmployee_license_keyDTO.modifiedBy).isEmpty()) {
						mapOfEmployee_license_keyDTOTomodifiedBy.remove(oldEmployee_license_keyDTO.modifiedBy);
					}
					
					if(mapOfEmployee_license_keyDTOTolastModificationTime.containsKey(oldEmployee_license_keyDTO.lastModificationTime)) {
						mapOfEmployee_license_keyDTOTolastModificationTime.get(oldEmployee_license_keyDTO.lastModificationTime).remove(oldEmployee_license_keyDTO);
					}
					if(mapOfEmployee_license_keyDTOTolastModificationTime.get(oldEmployee_license_keyDTO.lastModificationTime).isEmpty()) {
						mapOfEmployee_license_keyDTOTolastModificationTime.remove(oldEmployee_license_keyDTO.lastModificationTime);
					}
					
					if(mapOfEmployee_license_keyDTOTosearchColumn.containsKey(oldEmployee_license_keyDTO.searchColumn)) {
						mapOfEmployee_license_keyDTOTosearchColumn.get(oldEmployee_license_keyDTO.searchColumn).remove(oldEmployee_license_keyDTO);
					}
					if(mapOfEmployee_license_keyDTOTosearchColumn.get(oldEmployee_license_keyDTO.searchColumn).isEmpty()) {
						mapOfEmployee_license_keyDTOTosearchColumn.remove(oldEmployee_license_keyDTO.searchColumn);
					}
					
					
				}
				if(employee_license_keyDTO.isDeleted == 0) 
				{
					
					mapOfEmployee_license_keyDTOToiD.put(employee_license_keyDTO.iD, employee_license_keyDTO);
				
					if( ! mapOfEmployee_license_keyDTOToassetLicenseKeyId.containsKey(employee_license_keyDTO.assetLicenseKeyId)) {
						mapOfEmployee_license_keyDTOToassetLicenseKeyId.put(employee_license_keyDTO.assetLicenseKeyId, new HashSet<>());
					}
					mapOfEmployee_license_keyDTOToassetLicenseKeyId.get(employee_license_keyDTO.assetLicenseKeyId).add(employee_license_keyDTO);
					
					if( ! mapOfEmployee_license_keyDTOToemployeeRecordsId.containsKey(employee_license_keyDTO.employeeRecordsId)) {
						mapOfEmployee_license_keyDTOToemployeeRecordsId.put(employee_license_keyDTO.employeeRecordsId, new HashSet<>());
					}
					mapOfEmployee_license_keyDTOToemployeeRecordsId.get(employee_license_keyDTO.employeeRecordsId).add(employee_license_keyDTO);
					
					if( ! mapOfEmployee_license_keyDTOToremarks.containsKey(employee_license_keyDTO.remarks)) {
						mapOfEmployee_license_keyDTOToremarks.put(employee_license_keyDTO.remarks, new HashSet<>());
					}
					mapOfEmployee_license_keyDTOToremarks.get(employee_license_keyDTO.remarks).add(employee_license_keyDTO);
					
					if( ! mapOfEmployee_license_keyDTOToinsertionDate.containsKey(employee_license_keyDTO.insertionDate)) {
						mapOfEmployee_license_keyDTOToinsertionDate.put(employee_license_keyDTO.insertionDate, new HashSet<>());
					}
					mapOfEmployee_license_keyDTOToinsertionDate.get(employee_license_keyDTO.insertionDate).add(employee_license_keyDTO);
					
					if( ! mapOfEmployee_license_keyDTOToinsertedBy.containsKey(employee_license_keyDTO.insertedBy)) {
						mapOfEmployee_license_keyDTOToinsertedBy.put(employee_license_keyDTO.insertedBy, new HashSet<>());
					}
					mapOfEmployee_license_keyDTOToinsertedBy.get(employee_license_keyDTO.insertedBy).add(employee_license_keyDTO);
					
					if( ! mapOfEmployee_license_keyDTOTomodifiedBy.containsKey(employee_license_keyDTO.modifiedBy)) {
						mapOfEmployee_license_keyDTOTomodifiedBy.put(employee_license_keyDTO.modifiedBy, new HashSet<>());
					}
					mapOfEmployee_license_keyDTOTomodifiedBy.get(employee_license_keyDTO.modifiedBy).add(employee_license_keyDTO);
					
					if( ! mapOfEmployee_license_keyDTOTolastModificationTime.containsKey(employee_license_keyDTO.lastModificationTime)) {
						mapOfEmployee_license_keyDTOTolastModificationTime.put(employee_license_keyDTO.lastModificationTime, new HashSet<>());
					}
					mapOfEmployee_license_keyDTOTolastModificationTime.get(employee_license_keyDTO.lastModificationTime).add(employee_license_keyDTO);
					
					if( ! mapOfEmployee_license_keyDTOTosearchColumn.containsKey(employee_license_keyDTO.searchColumn)) {
						mapOfEmployee_license_keyDTOTosearchColumn.put(employee_license_keyDTO.searchColumn, new HashSet<>());
					}
					mapOfEmployee_license_keyDTOTosearchColumn.get(employee_license_keyDTO.searchColumn).add(employee_license_keyDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Employee_license_keyDTO> getEmployee_license_keyList() {
		List <Employee_license_keyDTO> employee_license_keys = new ArrayList<Employee_license_keyDTO>(this.mapOfEmployee_license_keyDTOToiD.values());
		return employee_license_keys;
	}
	
	
	public Employee_license_keyDTO getEmployee_license_keyDTOByID( long ID){
		return mapOfEmployee_license_keyDTOToiD.get(ID);
	}
	
	
	public List<Employee_license_keyDTO> getEmployee_license_keyDTOByasset_license_key_id(long asset_license_key_id) {
		return new ArrayList<>( mapOfEmployee_license_keyDTOToassetLicenseKeyId.getOrDefault(asset_license_key_id,new HashSet<>()));
	}
	
	
	public List<Employee_license_keyDTO> getEmployee_license_keyDTOByemployee_records_id(long employee_records_id) {
		return new ArrayList<>( mapOfEmployee_license_keyDTOToemployeeRecordsId.getOrDefault(employee_records_id,new HashSet<>()));
	}
	
	
	public List<Employee_license_keyDTO> getEmployee_license_keyDTOByremarks(String remarks) {
		return new ArrayList<>( mapOfEmployee_license_keyDTOToremarks.getOrDefault(remarks,new HashSet<>()));
	}
	
	
	public List<Employee_license_keyDTO> getEmployee_license_keyDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfEmployee_license_keyDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Employee_license_keyDTO> getEmployee_license_keyDTOByinserted_by(String inserted_by) {
		return new ArrayList<>( mapOfEmployee_license_keyDTOToinsertedBy.getOrDefault(inserted_by,new HashSet<>()));
	}
	
	
	public List<Employee_license_keyDTO> getEmployee_license_keyDTOBymodified_by(String modified_by) {
		return new ArrayList<>( mapOfEmployee_license_keyDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Employee_license_keyDTO> getEmployee_license_keyDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfEmployee_license_keyDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}
	
	
	public List<Employee_license_keyDTO> getEmployee_license_keyDTOBysearch_column(String search_column) {
		return new ArrayList<>( mapOfEmployee_license_keyDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "employee_license_key";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


