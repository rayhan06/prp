package employee_license_key;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Employee_license_keyDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Employee_license_keyDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Employee_license_keyMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"asset_license_key_id",
			"employee_records_id",
			"assign_or_revoke",
			"remarks",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"isDeleted",
			"search_column",
			"lastModificationTime"
		};
	}
	
	public Employee_license_keyDAO()
	{
		this("employee_license_key");		
	}
	
	public void setSearchColumn(Employee_license_keyDTO employee_license_keyDTO)
	{
		employee_license_keyDTO.searchColumn = "";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Employee_license_keyDTO employee_license_keyDTO = (Employee_license_keyDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(employee_license_keyDTO);
		if(isInsert)
		{
			ps.setObject(index++,employee_license_keyDTO.iD);
		}
		ps.setObject(index++,employee_license_keyDTO.assetLicenseKeyId);
		ps.setObject(index++,employee_license_keyDTO.employeeRecordsId);
		ps.setObject(index++,employee_license_keyDTO.assignOrRevoke);
		ps.setObject(index++,employee_license_keyDTO.remarks);
		ps.setObject(index++,employee_license_keyDTO.insertionDate);
		ps.setObject(index++,employee_license_keyDTO.insertedBy);
		ps.setObject(index++,employee_license_keyDTO.modifiedBy);
		if(isInsert)
		{
			ps.setObject(index++, 0);
		}
		ps.setObject(index++,employee_license_keyDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Employee_license_keyDTO employee_license_keyDTO, ResultSet rs) throws SQLException
	{
		employee_license_keyDTO.iD = rs.getLong("ID");
		employee_license_keyDTO.assetLicenseKeyId = rs.getLong("asset_license_key_id");
		employee_license_keyDTO.employeeRecordsId = rs.getLong("employee_records_id");
		employee_license_keyDTO.assignOrRevoke = rs.getInt("assign_or_revoke");
		employee_license_keyDTO.remarks = rs.getString("remarks");
		employee_license_keyDTO.insertionDate = rs.getLong("insertion_date");
		employee_license_keyDTO.insertedBy = rs.getString("inserted_by");
		employee_license_keyDTO.modifiedBy = rs.getString("modified_by");
		employee_license_keyDTO.isDeleted = rs.getInt("isDeleted");
		employee_license_keyDTO.lastModificationTime = rs.getLong("lastModificationTime");
		employee_license_keyDTO.searchColumn = rs.getString("search_column");
	}
	
	
	
	
		
	

	//need another getter for repository
	public Employee_license_keyDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Employee_license_keyDTO employee_license_keyDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				employee_license_keyDTO = new Employee_license_keyDTO();

				get(employee_license_keyDTO, rs);

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return employee_license_keyDTO;
	}
	
	
	
	
	public List<Employee_license_keyDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Employee_license_keyDTO employee_license_keyDTO = null;
		List<Employee_license_keyDTO> employee_license_keyDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return employee_license_keyDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				employee_license_keyDTO = new Employee_license_keyDTO();
				get(employee_license_keyDTO, rs);
				System.out.println("got this DTO: " + employee_license_keyDTO);
				
				employee_license_keyDTOList.add(employee_license_keyDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return employee_license_keyDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Employee_license_keyDTO> getAllEmployee_license_key (boolean isFirstReload)
    {
		List<Employee_license_keyDTO> employee_license_keyDTOList = new ArrayList<>();

		String sql = "SELECT * FROM employee_license_key";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by employee_license_key.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Employee_license_keyDTO employee_license_keyDTO = new Employee_license_keyDTO();
				get(employee_license_keyDTO, rs);
				
				employee_license_keyDTOList.add(employee_license_keyDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return employee_license_keyDTOList;
    }

	
	public List<Employee_license_keyDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Employee_license_keyDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Employee_license_keyDTO> employee_license_keyDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Employee_license_keyDTO employee_license_keyDTO = new Employee_license_keyDTO();
				get(employee_license_keyDTO, rs);
				
				employee_license_keyDTOList.add(employee_license_keyDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return employee_license_keyDTOList;
	
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("remarks")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("remarks"))
					{
						AllFieldSql += "" + tableName + ".remarks like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	