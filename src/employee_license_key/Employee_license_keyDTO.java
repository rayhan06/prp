package employee_license_key;
import java.util.*; 
import util.*; 


public class Employee_license_keyDTO extends CommonDTO
{

	public long assetLicenseKeyId = -1;
	public long employeeRecordsId = -1;
	public int assignOrRevoke = -1;
    public String remarks = "";
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Employee_license_keyDTO[" +
            " iD = " + iD +
            " assetLicenseKeyId = " + assetLicenseKeyId +
            " employeeRecordsId = " + employeeRecordsId +
            " remarks = " + remarks +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            "]";
    }

}