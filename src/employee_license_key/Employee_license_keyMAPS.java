package employee_license_key;
import java.util.*; 
import util.*;


public class Employee_license_keyMAPS extends CommonMaps
{	
	public Employee_license_keyMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("assetLicenseKeyId".toLowerCase(), "assetLicenseKeyId".toLowerCase());
		java_DTO_map.put("employeeRecordsId".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_DTO_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());

		java_SQL_map.put("asset_license_key_id".toLowerCase(), "assetLicenseKeyId".toLowerCase());
		java_SQL_map.put("employee_records_id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_SQL_map.put("remarks".toLowerCase(), "remarks".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Asset License Key Id".toLowerCase(), "assetLicenseKeyId".toLowerCase());
		java_Text_map.put("Employee Records Id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_Text_map.put("Remarks".toLowerCase(), "remarks".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
			
	}

}