package employee_offices;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import designations.DesignationsDTO;
import employee_assign.EmployeeAssignDTO;
import employee_office_report.InChargeLevelEnum;
import employee_records.EmployeeFlatInfoDTO;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import office_unit_organogram.Office_unit_organogramDAO;
import office_unit_organogram.Office_unit_organogramDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_unit_organograms.Office_unit_organogramsDAO;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;

import appointment.KeyCountDTO;
import pb.Utils;
import repository.RepositoryManager;
import sessionmanager.SessionConstants;
import util.CommonDTO;
import util.HttpRequestUtils;
import workflow.WingModel;
import workflow.WorkflowController;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@SuppressWarnings({"unused", "Duplicates"})
public class EmployeeOfficesDAO implements CommonDAOService<EmployeeOfficeDTO> {
    private static final Logger logger = Logger.getLogger(EmployeeOfficesDAO.class);

    private static final String getByOUIandOUOIandOI = "select * from employee_offices where office_unit_id = %d and office_unit_organogram_id = %d  "
            .concat(" and office_id = %d and (employee_offices.last_office_date is null or employee_offices.last_office_date = 0)")
            .concat(" and isDeleted = 0");

    private static final String getByOfficeUnitId = "select * from employee_offices where office_unit_id = %d and isDeleted = 0 and status=1";

    private static final String getEmployeeOfficeByEmployeeRecordId = "select * from employee_offices where employee_record_id = %d and isDeleted = 0 and is_default_role = 1";

    private static final String getByEmployeeRecordIdIgnoringStatusAndIsDeleted = "select * from employee_offices where employee_record_id = %d and is_default_role = 1";

    private static final String getEmployeeActiveOfficeByEmployeeRecordId = "select * from employee_offices where employee_record_id = %d and isDeleted = 0 and status = 1 and is_default_role = 1 and"
            + " (select is_virtual from office_unit_organograms where id = employee_offices.office_unit_organogram_id and isDeleted = 0) = 0";

    private static final String getLastEmployeeInactiveOfficeByEmployeeRecordId = "select * from employee_offices where employee_record_id = %d and isDeleted = 0 and status=0 order by last_office_date desc limit 1";

    private static final String getEmployeeOfficeByEmployeeRecordIdAndIsDefault = "select * from employee_offices where employee_record_id = %d and isDeleted = 0 and isDefault = 1 and status = 1 and is_default_role = 1";

    private static final String getForTrue = "SELECT * FROM employee_offices WHERE isDeleted = 0 AND status = 1";

    private static final String getForTrueStatusZero = "SELECT * FROM employee_offices WHERE isDeleted = 0 AND status = 0";

    private static final String getForStatusZeroAndEmpId = "SELECT * FROM employee_offices WHERE employee_record_id = %d and isDeleted = 0 AND status = 0";

    private static final String getForFalse = "SELECT * FROM employee_offices WHERE lastModificationTime >= %d order by lastModificationTime desc";

    private static final String getByIdSQL = "SELECT * FROM employee_offices WHERE  id = %d and isDeleted = 0";

    private static final String getByEmployeeRecordIdAndOUOI = "SELECT * FROM employee_offices WHERE  employee_record_id = %d and office_unit_organogram_id = %d and isDeleted = 0 and isDefault = 1 and status=1";

    private static final String getLastByEmployeeRecordIdAndOUOIIncludingNonDefault = "SELECT * FROM employee_offices WHERE  employee_record_id = %d and office_unit_organogram_id = %d and isDeleted = 0 order by joining_date desc limit 1";

    private static final String getByEmpIds = "SELECT * FROM employee_offices WHERE employee_record_id IN (%s) AND isDeleted = 0 AND (last_office_date = -62135791200000 OR last_office_date> %d)";

    private static final String getByOfficeUnitOrganogramId = "SELECT * FROM employee_offices WHERE office_unit_organogram_id = %d AND status = 1 AND isDeleted = 0";

    private static final String getDefaultByOfficeUnitOrganogramId = "SELECT * FROM employee_offices WHERE office_unit_organogram_id = %d AND is_default_role = 1 and isDeleted = 0 and status=1";

    private static final String getDefaultByErId = "SELECT * FROM employee_offices WHERE employee_record_id = %d AND is_default_role = 1 and isDeleted = 0 and status=1";

    private static final String getByErId = "SELECT * FROM employee_offices WHERE employee_record_id = %d and isDeleted = 0 and status=1";

    private static final String getLastByErId = "SELECT * FROM employee_offices WHERE employee_record_id = %d and isDeleted = 0 order by id desc limit 1";

    private static final String getByPromotionHistoryIdQuery = "SELECT * FROM %s where promotion_history_id=%d AND isDeleted=0";

    private static final String getCountByOfficeUnitIdIsDeletedOrNot = "select count(*) from employee_offices where office_unit_id = %d";

    private static final String deactivateStatusSQL = "UPDATE employee_offices SET status = 0,status_change_date=?,lastModificationTime=?" +
            " WHERE employee_record_id = ? AND status = 1";

    private final static long MILLISECONDS_PER_DAY = 1000L * 60 * 60 * 24;

    private static final String addSqlQuery = "INSERT INTO {tableName} (employee_record_id,identification_number,office_id,office_unit_id,office_unit_organogram_id,designation," +
            "designation_level,designation_sequence,promotion_history_id,office_name_en," +
            "office_name_bn,unit_name_en,unit_name_bn,designation_en,designation_bn," +
            "is_default_role,office_head,summary_nothi_post_type," +
            "incharge_label,main_role_id,joining_date,last_office_date," +
            "status,status_change_date,show_unit,job_grade_type_cat, grade_type_level ,layer_1,layer_2,created,modified,created_by,modified_by,role_type,lastModificationTime,isDeleted,isDefault, organogram_role,salary_grade_type,id)" +
            "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private static final String updateSqlQuery = "UPDATE {tableName} SET employee_record_id=?,identification_number=?,office_id=?,office_unit_id=?,office_unit_organogram_id=?,designation=?," +
            "designation_level=?,designation_sequence=?,promotion_history_id=?,office_name_en=?," +
            "office_name_bn=?,unit_name_en=?,unit_name_bn=?,designation_en=?,designation_bn=?," +
            "is_default_role=?,office_head=?,summary_nothi_post_type=?," +
            "incharge_label=?,main_role_id=?,joining_date=?,last_office_date=?," +
            "status=?,status_change_date=?,show_unit=?,job_grade_type_cat=?, grade_type_level =?,"
            + " layer_1=?,layer_2=?,modified=?,modified_by=?,role_type=?,lastModificationTime=?,isDefault=?,organogram_role=?,salary_grade_type=? WHERE id=?";

    private static final String updateLastOfficeDate = "UPDATE %s SET last_office_date=%d where employee_record_id=%d " +
            "AND isDeleted=0 AND isDefault=1 AND promotion_history_id<>%d";

    private static final String deleteNonDefaults = "UPDATE %s SET isDeleted = 1 where office_unit_organogram_id=%d " +
            "AND isDeleted=0 AND is_default_role = 0 AND lastModificationTime= %d";

    private static final String deleteNonDefaultsByErId = "UPDATE %s SET isDeleted = 1 where employee_record_id=%d " +
            "AND isDeleted=0 AND is_default_role = 0";

    private static final String getByEmployeeRecordIdWithMinJoiningDate = "select eo.* from employee_offices eo" +
            " inner join ( select employee_record_id,MIN(joining_date) min_joining_date from" +
            " (select * from employee_offices where  employee_record_id in (%s))" +
            " A group by employee_record_id ) B on eo.employee_record_id = B.employee_record_id AND eo.joining_date = min_joining_date";

    private static final String getEmployeeOfficeByEmployeeRecordIdIsDeleteOrNot = "select * from employee_offices where employee_record_id = %d order by joining_date desc";

    private static final String getActiveByEmployeeRecordIdAndInChargeLevel = "SELECT * FROM employee_offices WHERE employee_record_id = %d AND incharge_label = '%s' AND status = 1 AND isDeleted = 0";

    private static final String getIdsByEmpIds = "SELECT id FROM employee_offices WHERE employee_record_id IN (%s) AND isDeleted = 0 ";

    private static final String tableName = "employee_offices";

    private static final String updateDesignation = "UPDATE employee_offices SET designation_en = '%s',designation_bn= '%s'," +
            " lastModificationTime = %d WHERE office_unit_organogram_id = %d AND status = 1";

    private static final String updateOffice = "UPDATE employee_offices SET office_unit_id = %d, unit_name_en = '%s'," +
            " unit_name_bn= '%s',designation_en = '%s',designation_bn= '%s'," +
            " lastModificationTime = %d WHERE office_unit_organogram_id = %d AND status = 1";

    private static final String updateOfficeByOfficeUnitId = "UPDATE employee_offices SET unit_name_en = '%s'," +
            " unit_name_bn= '%s', lastModificationTime = %d WHERE office_unit_id = %d AND status = 1";

    private static final String getAllDTOsAfterLastModificationTime = "SELECT * FROM %s WHERE lastModificationTime >= %d";

    private static final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final EmployeeOfficesDAO INSTANCE = new EmployeeOfficesDAO();
    }

    public static EmployeeOfficesDAO getInstance() {
        return EmployeeOfficesDAO.LazyLoader.INSTANCE;
    }

    private static final String updateDesignationNameByIds = "UPDATE employee_offices SET designation_en = '%s',designation_bn= '%s'," +
            " lastModificationTime = %d,job_grade_type_cat = %d WHERE office_unit_organogram_id IN (%s) AND status = 1";

    public void updateDesignationNameByDesignationsDTO(DesignationsDTO dto) throws Exception {
        String ids = OfficeUnitOrganogramsRepository.getInstance()
                .getByDesignationId(dto.iD)
                .stream()
                .map(e -> String.valueOf(e.id))
                .collect(Collectors.joining(","));
        if (ids.isEmpty()) {
            return;
        }
        String sql = String.format(updateDesignationNameByIds, dto.nameEn, dto.nameBn, dto.lastModificationTime, dto.jobGradeCat, ids);

        Utils.wrapWithAtomicReference(ar ->
                ConnectionAndStatementUtil.getWriteStatement(st -> {
                    try {
                        logger.debug(sql);
                        st.executeUpdate(sql);
                        recordUpdateTime(CommonDAOService.CONNECTION_THREAD_LOCAL.get(), getTableName(), dto.lastModificationTime);
                        Utils.addToRepoList(getTableName());
                    } catch (SQLException e) {
                        logger.error(e.getMessage());
                        ar.set(e);
                        e.printStackTrace();
                    }
                }, CommonDAOService.CONNECTION_THREAD_LOCAL.get())
        );
    }

    public void updateDesignationByOfcUnitOrganogram(Office_unit_organogramDTO dto,
                                                     boolean isOfficeUpdated) throws Exception {

        Utils.wrapWithAtomicReference(ar ->
                ConnectionAndStatementUtil.getWriteStatement(st -> {
                    String sql = String.format(updateDesignation, dto.designationEng, dto.designationBng, dto.lastModificationTime, dto.iD);
                    logger.debug(sql);
                    try {
                        st.executeUpdate(sql);
                        if (isOfficeUpdated) {
                            updateOfficeByOfcUnitOrganogramIdAndOfficeUnitId(dto, false);
                        }
                        recordUpdateTime(CommonDAOService.CONNECTION_THREAD_LOCAL.get(), getTableName(), dto.lastModificationTime);
                        Utils.addToRepoList(getTableName());
                    } catch (Exception e) {
                        e.printStackTrace();
                        ar.set(e);
                    }
                }, CommonDAOService.CONNECTION_THREAD_LOCAL.get())
        );

    }

    public void updateOfficeByOfcUnitOrganogramIdAndOfficeUnitId(Office_unit_organogramDTO officeUnitOrganogramDTO) throws Exception {
        updateOfficeByOfcUnitOrganogramIdAndOfficeUnitDTO(officeUnitOrganogramDTO,
                Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitOrganogramDTO.officeUnitId), true);
    }

    public void updateOfficeByOfcUnitOrganogramIdAndOfficeUnitId(Office_unit_organogramDTO officeUnitOrganogramDTO,
                                                                 boolean updateCache) throws Exception {
        updateOfficeByOfcUnitOrganogramIdAndOfficeUnitDTO(officeUnitOrganogramDTO,
                Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitOrganogramDTO.officeUnitId), updateCache);
    }

    public void updateOfficeByOfcUnitOrganogramIdAndOfficeUnitDTO(Office_unit_organogramDTO officeUnitOrganogramDTO,
                                                                  Office_unitsDTO dto) throws Exception {
        updateOfficeByOfcUnitOrganogramIdAndOfficeUnitDTO(officeUnitOrganogramDTO, dto, true);
    }

    private void updateOfficeByOfcUnitOrganogramIdAndOfficeUnitDTO(Office_unit_organogramDTO officeUnitOrganogramDTO,
                                                                   Office_unitsDTO dto,
                                                                   boolean updateCache) throws Exception {
        Utils.wrapWithAtomicReference(ar ->
                ConnectionAndStatementUtil.getWriteStatement(st -> {
                    String sql = String.format(updateOffice, dto.iD, dto.unitNameEng, dto.unitNameBng, officeUnitOrganogramDTO.designationEng,
                            officeUnitOrganogramDTO.designationBng, officeUnitOrganogramDTO.lastModificationTime, officeUnitOrganogramDTO.iD);
                    logger.debug(sql);
                    try {
                        st.executeUpdate(sql);
                        if (updateCache) {
                            recordUpdateTime(CommonDAOService.CONNECTION_THREAD_LOCAL.get(), getTableName(), officeUnitOrganogramDTO.lastModificationTime);
                        }
                        Utils.addToRepoList(getTableName());
                    } catch (SQLException e) {
                        e.printStackTrace();
                        ar.set(e);
                    }
                }, CommonDAOService.CONNECTION_THREAD_LOCAL.get())
        );
    }

    public void updateOfficeByOfcUnitOrganogramIdAndOfficeUnitDTO(long officeUnitId,
                                                                  String unitNameEng,
                                                                  String unitNameBng,
                                                                  long lastModificationTime) throws Exception {
        AtomicReference<Exception> atomicReference = new AtomicReference<>();
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            String sql = String.format(updateOfficeByOfficeUnitId, unitNameEng, unitNameBng, lastModificationTime, officeUnitId);
            logger.debug(sql);
            try {
                st.executeUpdate(sql);
                recordUpdateTime(CommonDAOService.CONNECTION_THREAD_LOCAL.get(), getTableName(), lastModificationTime);
            } catch (SQLException e) {
                e.printStackTrace();
                atomicReference.set(e);
            }
        }, CommonDAOService.CONNECTION_THREAD_LOCAL.get());

        if (atomicReference.get() != null) {
            throw atomicReference.get();
        }
    }

    private EmployeeOfficeDTO buildEmployeeOfficesDTO(ResultSet rs) {
        try {
            EmployeeOfficeDTO dto = new EmployeeOfficeDTO();
            dto.id = rs.getLong("id");
            dto.iD = dto.id;
            dto.employeeRecordId = rs.getLong("employee_record_id");
            dto.identificationNumber = rs.getString("identification_number");
            dto.officeId = rs.getInt("office_id");
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.officeUnitOrganogramId = rs.getLong("office_unit_organogram_id");
            dto.designation = rs.getString("designation");
            dto.designationLevel = rs.getInt("designation_level");
            dto.designationSequence = rs.getInt("designation_sequence");
            dto.isDefaultRole = rs.getInt("is_default_role");
            dto.officeHead = rs.getInt("office_head");
            dto.summaryNothiPostType = rs.getInt("summary_nothi_post_type");
            dto.inchargeLabel = rs.getString("incharge_label");
            dto.mainRoleId = rs.getInt("main_role_id");
            dto.joiningDate = rs.getLong("joining_date");
            dto.lastOfficeDate = rs.getLong("last_office_date");
            dto.status = rs.getInt("status");
            dto.statusChangeDate = rs.getLong("status_change_date");
            dto.showUnit = rs.getInt("show_unit");
            dto.designationEn = rs.getString("designation_en");
            dto.designationBn = rs.getString("designation_bn");
            dto.unitNameBn = rs.getString("unit_name_bn");
            dto.officeNameBn = rs.getString("office_name_bn");
            dto.unitNameEn = rs.getString("unit_name_en");
            dto.officeNameBn = rs.getString("office_name_en");
            dto.protikolpoStatus = rs.getInt("protikolpo_status");
            dto.created = rs.getLong("created");
            dto.modified = rs.getLong("modified");
            dto.createdBy = rs.getLong("created_by");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.roleType = rs.getInt("role_type");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.dept = rs.getInt("dept");
            dto.isDefault = rs.getInt("isDefault");
            dto.organogramRole = rs.getInt("organogram_role");
            dto.jobGradeTypeCat = rs.getInt("job_grade_type_cat");
            dto.gradeTypeLevel = rs.getInt("grade_type_level");
            dto.layer1 = rs.getInt("layer_1");
            dto.layer2 = rs.getInt("layer_2");
            dto.isAvailable = rs.getBoolean("isAvailable");
            dto.medicalDeptCat = rs.getInt("medical_dept_cat");
            dto.promotionHistoryId = rs.getLong("promotion_history_id");
            dto.salaryGradeType = rs.getLong("salary_grade_type");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public List<EmployeeOfficeDTO> getEmployeeOfficeDTO(long officeUnitId, long officeId, long officeUnitOrganogramId) {
        String sql = String.format(getByOUIandOUOIandOI, officeUnitId, officeUnitOrganogramId, officeId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildEmployeeOfficesDTO);
    }

    public List<EmployeeOfficeDTO> getByOfficeUnitId(long officeUnitId) {
        String sql = String.format(getByOfficeUnitId, officeUnitId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildEmployeeOfficesDTO);
    }

    public Map<String, Integer> getDesignationCountList(long officeUnitId, String language) {
        List<EmployeeOfficeDTO> dtoList = getByOfficeUnitId(officeUnitId);
        if (dtoList.size() == 0) {
            return new HashMap<>();
        }
        Map<Long, Integer> countMap = dtoList.stream()
                .collect(Collectors.groupingBy(dto -> dto.officeUnitOrganogramId, Collectors.collectingAndThen(Collectors.counting(), Long::intValue)));
        return countMap.entrySet()
                .stream()
                .collect(Collectors.toMap(e -> OfficeUnitOrganogramsRepository.getInstance().getDesignation(language, e.getKey()), Map.Entry::getValue, (e1, e2) -> e1));
    }

    public Map<String, Integer> getAgeRangeList(long officeUnitId) {
        List<EmployeeOfficeDTO> dtoList = getByOfficeUnitId(officeUnitId);
        if (dtoList.size() == 0) {
            return new HashMap<>();
        }
        Set<Long> employeeRecordIdSet = dtoList.stream()
                .filter(dto -> dto.employeeRecordId != null)
                .map(dto -> dto.employeeRecordId)
                .collect(Collectors.toSet());
        List<Employee_recordsDTO> employeeRecordsDTOList = new Employee_recordsDAO().getDTOs(employeeRecordIdSet);
        if (employeeRecordsDTOList == null || employeeRecordsDTOList.size() == 0) {
            return new HashMap<>();
        }
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Map<String, Integer> ageMap = employeeRecordsDTOList.stream()
                .map(employee -> Utils.calculateAge(employee.dateOfBirth))
                .collect(Collectors.groupingBy(age -> Utils.calculateAgeRange(age, isLangEng), Collectors.collectingAndThen(Collectors.counting(), Long::intValue)));
        return new TreeMap<>(ageMap);
    }

    public List<KeyCountDTO> getUnitWiseCount() {
        String sql = "SELECT \r\n" +
                "    office_unit_id, COUNT(id)\r\n" +
                "FROM\r\n" +
                "    employee_offices\r\n" +
                "WHERE\r\n" +
                "    isDeleted = 0 AND status = 1\r\n" +
                "        AND last_office_date = -62135791200000\r\n" +
                "        AND office_unit_id != - 1\r\n" +
                "        AND is_default_role = 1\r\n" +
                "GROUP BY office_unit_id;";

        return ConnectionAndStatementUtil.getListOfT(sql, this::getUnitWiseCount);
    }

    public KeyCountDTO getUnitWiseCount(ResultSet rs) {
        try {
            KeyCountDTO keyCountDTO = new KeyCountDTO();
            keyCountDTO.key = rs.getLong("office_unit_id");
            keyCountDTO.count = rs.getInt("count(id)");

            return keyCountDTO;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public Map<Long, KeyCountDTO> getWingWiseCount() {
        List<KeyCountDTO> unitWiseCount = getUnitWiseCount();
        Map<Long, KeyCountDTO> wingWiseCount = new ConcurrentHashMap<>();

        for (KeyCountDTO keyCountDTO : unitWiseCount) {
            WingModel wing = WorkflowController.getWingFromOfficeUnitId(keyCountDTO.key);

            if (wingWiseCount.getOrDefault(wing.id, null) == null) {
                KeyCountDTO wingKey = new KeyCountDTO();
                if (wing.id != SessionConstants.MP_OFFICE) {
                    wingKey.key = wing.id;
                    wingKey.count = 0;
                    wingKey.nameEn = wing.nameEn;
                    wingKey.nameBn = wing.nameBn;
                    wingWiseCount.put(wing.id, wingKey);
                    logger.debug("put" + wing.id + ": " + wingKey.nameEn);
                }

            }

            KeyCountDTO wingKey = wingWiseCount.get(wing.id);
            wingKey.count += keyCountDTO.count;
            wingWiseCount.put(wing.id, wingKey);
        }

        logger.debug("Fetching wingwiseCount");
        for (Map.Entry<Long, KeyCountDTO> entry : wingWiseCount.entrySet()) {
            logger.debug(entry.getKey() + ": " + entry.getValue().nameEn + " " + entry.getValue().count);
        }
        return wingWiseCount;
    }


    public List<EmployeeOfficeDTO> getByEmployeeRecordIdIgnoringStatus(long employeeRecordId) {
        String sql = String.format(getEmployeeOfficeByEmployeeRecordId, employeeRecordId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildEmployeeOfficesDTO);
    }

    public List<EmployeeOfficeDTO> getByEmployeeRecordIdIgnoringStatusAndIsDeleted(long employeeRecordId) {
        String sql = String.format(getByEmployeeRecordIdIgnoringStatusAndIsDeleted, employeeRecordId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildEmployeeOfficesDTO);
    }

    public List<EmployeeOfficeDTO> getActiveOfficesByEmployeeRecordId(long employeeRecordId) {
        String sql = String.format(getEmployeeActiveOfficeByEmployeeRecordId, employeeRecordId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildEmployeeOfficesDTO);
    }

    public List<EmployeeOfficeDTO> getByEmployeeRecordIdIsDeleteOrNot(long employeeRecordId) {
        String sql = String.format(getEmployeeOfficeByEmployeeRecordIdIsDeleteOrNot, employeeRecordId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildEmployeeOfficesDTO);
    }

    public EmployeeOfficeDTO getByEmployeeRecordIdIsDefault(long employeeRecordId) {
        String sql = String.format(getEmployeeOfficeByEmployeeRecordIdAndIsDefault, employeeRecordId);
        return ConnectionAndStatementUtil.getT(sql, this::buildEmployeeOfficesDTO);
    }

    public EmployeeOfficeDTO getByEmployeeRecordIdAndOUOI(long employeeRecordId, long officeUnitUrganogramId) {
        String sql = String.format(getByEmployeeRecordIdAndOUOI, employeeRecordId, officeUnitUrganogramId);
        return ConnectionAndStatementUtil.getT(sql, this::buildEmployeeOfficesDTO);
    }

    public EmployeeOfficeDTO getLastByEmployeeRecordIdAndOrganogramId(long employeeRecordId, long officeUnitUrganogramId) {
        String sql = String.format(getLastByEmployeeRecordIdAndOUOIIncludingNonDefault, employeeRecordId, officeUnitUrganogramId);
        return ConnectionAndStatementUtil.getT(sql, this::buildEmployeeOfficesDTO);
    }

    public EmployeeOfficeDTO getLastInactiveByEmployeeRecordId(long employeeRecordId) {
        String sql = String.format(getLastEmployeeInactiveOfficeByEmployeeRecordId, employeeRecordId);
        return ConnectionAndStatementUtil.getT(sql, this::buildEmployeeOfficesDTO);
    }

    public List<EmployeeOfficeDTO> getDTOs(boolean reloadAll) {
        if (reloadAll) {
            return ConnectionAndStatementUtil.getListOfT(getForTrue, this::buildEmployeeOfficesDTO);
        } else {
            return ConnectionAndStatementUtil.getListOfT(String.format(getForFalse, RepositoryManager.lastModifyTime), this::buildEmployeeOfficesDTO);
        }
    }

    public List<EmployeeOfficeDTO> getAllDTOsExactLastModificationTime(long time) {
        return ConnectionAndStatementUtil.getListOfT(String.format(getAllDTOsAfterLastModificationTime, "employee_offices", time),
                this::buildEmployeeOfficesDTO);
    }

    public List<EmployeeOfficeDTO> getStatusZeroDTOs(boolean reloadAll) {
        if (reloadAll) {
            return ConnectionAndStatementUtil.getListOfT(getForTrueStatusZero, this::buildEmployeeOfficesDTO);
        } else {
            return ConnectionAndStatementUtil.getListOfT(String.format(getForFalse, RepositoryManager.lastModifyTime), this::buildEmployeeOfficesDTO);
        }
    }

    public List<EmployeeOfficeDTO> getForStatusZeroAndEmpId(long empId) {
        String sql = String.format(getForStatusZeroAndEmpId, empId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildEmployeeOfficesDTO);
    }

    public EmployeeOfficeDTO getById(long id) {
        return ConnectionAndStatementUtil.getT(String.format(getByIdSQL, id), this::buildEmployeeOfficesDTO);
    }

    public List<EmployeeOfficeDTO> getByEmpIds(List<Long> empIds) {
        if (empIds == null || empIds.size() == 0) {
            return new ArrayList<>();
        }
        String ids = empIds.stream()
                .map(String::valueOf)
                .collect(Collectors.joining(","));
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return ConnectionAndStatementUtil.getListOfT(String.format(getByEmpIds, ids, c.getTimeInMillis()), this::buildEmployeeOfficesDTO);
    }

    public List<Employee_recordsDTO> getAllDescentEmployeeRecordDTOs(long superEmployeeRecord) {
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(superEmployeeRecord);
        if (employeeRecordsDTO == null) return new ArrayList<>();
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employeeRecordsDTO.iD);
        if (employeeOfficeDTO == null) {
            return new ArrayList<>();
        }
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        if (officeUnitOrganograms == null || officeUnitOrganograms.orgTree.endsWith("$")) {
            return new ArrayList<>();
        }
        List<Long> organogramIdList = new Office_unit_organogramsDAO().getAllIdByOrgTree(officeUnitOrganograms.orgTree);
        return getAllDescentEmployeeRecordDTOs(superEmployeeRecord, organogramIdList);
    }

    public List<Long> getAllDescentEmployeeRecordIds(long superEmployeeRecord) {
        return getAllDescentEmployeeRecordDTOs(superEmployeeRecord)
                .stream()
                .map(emp -> emp.iD)
                .collect(Collectors.toList());
    }

    public List<EmployeeFlatInfoDTO> getAllDescentEmployeeFlatInfoDTOs(long superEmployeeRecord) {
        return getAllDescentEmployeeRecordDTOs(superEmployeeRecord)
                .stream()
                .map(this::convertToEmployeeFlatInfoDTO)
                .collect(Collectors.toList());
    }

    public boolean getIsLeafNode(long superEmployeeRecord) {
        EmployeeOfficeDTO employeeOfficeDTO = getByEmployeeRecordIdIsDefault(superEmployeeRecord);
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        return officeUnitOrganograms.orgTree.endsWith("$");
    }

    public List<Employee_recordsDTO> getAllDescentEmployeeRecordDTOsForSameOfficeId(long superEmployeeRecord) {
        EmployeeOfficeDTO employeeOfficeDTO = getByEmployeeRecordIdIsDefault(superEmployeeRecord);
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        if (officeUnitOrganograms.orgTree.endsWith("$")) {
            return new ArrayList<>();
        }
        List<Long> organogramIdList = new Office_unit_organogramsDAO().getAllIdForSameOfficeId(officeUnitOrganograms.orgTree, employeeOfficeDTO.officeUnitId);
        return getAllDescentEmployeeRecordDTOs(superEmployeeRecord, organogramIdList);
    }

    public List<Long> getAllDescentEmployeeRecordIdsForSameOfficeId(long superEmployeeRecord) {
        return getAllDescentEmployeeRecordDTOsForSameOfficeId(superEmployeeRecord)
                .stream()
                .map(emp -> emp.iD)
                .collect(Collectors.toList());
    }

    public List<EmployeeFlatInfoDTO> getAllDescentEmployeeFlatInfoDTOsForSameOfficeId(long superEmployeeRecord) {
        return getAllDescentEmployeeRecordDTOsForSameOfficeId(superEmployeeRecord)
                .stream()
                .map(this::convertToEmployeeFlatInfoDTO)
                .collect(Collectors.toList());
    }

    public List<EmployeeAssignDTO> getAllDescentEmployeeAssignDTOsForSameOfficeId(long superEmployeeRecord) {
        return getAllDescentEmployeeRecordDTOsForSameOfficeId(superEmployeeRecord)
                .stream()
                .map(this::convertToEmployeeAssignDTO)
                .collect(Collectors.toList());
    }

    private List<Employee_recordsDTO> getAllDescentEmployeeRecordDTOs(long superEmployeeRecord, List<Long> organogramIdList) {
        if (organogramIdList == null || organogramIdList.size() == 0) {
            return new ArrayList<>();
        }
        List<Employee_recordsDTO> employeeRecordsDTOList = Employee_recordsRepository.getInstance().getByDesignationId(organogramIdList);
        if (employeeRecordsDTOList == null || employeeRecordsDTOList.size() == 0) {
            return new ArrayList<>();
        }
        return employeeRecordsDTOList.stream()
                .filter(e -> e.iD != superEmployeeRecord)
                .collect(Collectors.toList());
    }

    private EmployeeFlatInfoDTO convertToEmployeeFlatInfoDTO(Employee_recordsDTO dto) {
        EmployeeFlatInfoDTO infoDTO = new EmployeeFlatInfoDTO();
        infoDTO.employeeRecordsId = dto.iD;
        infoDTO.employeeNameEn = dto.nameEng;
        infoDTO.employeeNameBn = dto.nameBng;
        infoDTO.employeeUserName = dto.employeeNumber;
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(dto.iD);
        infoDTO.officeUnitId = employeeOfficeDTO.officeUnitId;
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(infoDTO.officeUnitId);
        if (officeUnitsDTO != null) {
            infoDTO.officeNameEn = officeUnitsDTO.unitNameEng;
            infoDTO.officeNameBn = officeUnitsDTO.unitNameBng;
        }

        infoDTO.organogramId = employeeOfficeDTO.officeUnitOrganogramId;
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(infoDTO.organogramId);
        if (officeUnitOrganograms != null) {
            infoDTO.organogramNameEn = officeUnitOrganograms.designation_eng;
            infoDTO.organogramNameBn = officeUnitOrganograms.designation_bng;
        }
        return infoDTO;
    }

    @Override
    public void set(PreparedStatement ps, EmployeeOfficeDTO employeeOfficeDTO, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setObject(++index, employeeOfficeDTO.employeeRecordId);
        ps.setObject(++index, employeeOfficeDTO.identificationNumber);
        ps.setObject(++index, employeeOfficeDTO.officeId);
        ps.setObject(++index, employeeOfficeDTO.officeUnitId);
        ps.setObject(++index, employeeOfficeDTO.officeUnitOrganogramId);
        ps.setObject(++index, employeeOfficeDTO.designation);
        ps.setObject(++index, employeeOfficeDTO.designationLevel);
        ps.setObject(++index, employeeOfficeDTO.designationSequence);
        ps.setObject(++index, employeeOfficeDTO.promotionHistoryId);
        ps.setObject(++index, employeeOfficeDTO.officeNameEn);
        ps.setObject(++index, employeeOfficeDTO.officeNameBn);
        ps.setObject(++index, employeeOfficeDTO.unitNameEn);
        ps.setObject(++index, employeeOfficeDTO.unitNameBn);
        ps.setObject(++index, employeeOfficeDTO.designationEn);
        ps.setObject(++index, employeeOfficeDTO.designationBn);
        ps.setObject(++index, employeeOfficeDTO.isDefaultRole);
        ps.setObject(++index, employeeOfficeDTO.officeHead);
        ps.setObject(++index, employeeOfficeDTO.summaryNothiPostType);
        ps.setObject(++index, employeeOfficeDTO.inchargeLabel);
        ps.setObject(++index, employeeOfficeDTO.mainRoleId);
        ps.setObject(++index, employeeOfficeDTO.joiningDate);
        ps.setObject(++index, employeeOfficeDTO.lastOfficeDate);
        ps.setObject(++index, employeeOfficeDTO.status);
        ps.setObject(++index, employeeOfficeDTO.statusChangeDate);
        ps.setObject(++index, employeeOfficeDTO.showUnit);
        ps.setObject(++index, employeeOfficeDTO.jobGradeTypeCat);
        ps.setObject(++index, employeeOfficeDTO.gradeTypeLevel);
        ps.setObject(++index, employeeOfficeDTO.layer1);
        ps.setObject(++index, employeeOfficeDTO.layer2);
        if (isInsert)
            ps.setObject(++index, employeeOfficeDTO.created);
        ps.setObject(++index, employeeOfficeDTO.modified);
        if (isInsert)
            ps.setObject(++index, employeeOfficeDTO.createdBy);
        ps.setObject(++index, employeeOfficeDTO.modifiedBy);
        ps.setObject(++index, employeeOfficeDTO.roleType);
        ps.setObject(++index, employeeOfficeDTO.lastModificationTime);
        if (isInsert)
            ps.setObject(++index, employeeOfficeDTO.isDeleted);
        ps.setObject(++index, employeeOfficeDTO.isDefault);
        ps.setObject(++index, employeeOfficeDTO.organogramRole);
        ps.setObject(++index, employeeOfficeDTO.salaryGradeType);
        ps.setObject(++index, employeeOfficeDTO.iD);
    }

    @Override
    public EmployeeOfficeDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            EmployeeOfficeDTO dto = new EmployeeOfficeDTO();
            dto.iD = rs.getLong("id");
            dto.employeeRecordId = rs.getLong("employee_record_id");
            dto.identificationNumber = rs.getString("identification_number");
            dto.officeId = rs.getInt("office_id");
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.officeUnitOrganogramId = rs.getLong("office_unit_organogram_id");
            dto.designation = rs.getString("designation");
            dto.designationLevel = rs.getInt("designation_level");
            dto.designationSequence = rs.getInt("designation_sequence");
            dto.promotionHistoryId = rs.getLong("promotion_history_id");
            dto.officeNameEn = rs.getString("office_name_en");
            dto.officeNameBn = rs.getString("office_name_bn");
            dto.unitNameEn = rs.getString("unit_name_en");
            dto.unitNameBn = rs.getString("unit_name_bn");
            dto.designationEn = rs.getString("designation_en");
            dto.designationBn = rs.getString("designation_bn");
            dto.isDefaultRole = rs.getInt("is_default_role");
            dto.officeHead = rs.getInt("office_head");
            dto.summaryNothiPostType = rs.getInt("summary_nothi_post_type");
            dto.inchargeLabel = rs.getString("incharge_label");
            dto.mainRoleId = rs.getInt("main_role_id");
            dto.joiningDate = rs.getLong("joining_date");
            dto.lastOfficeDate = rs.getLong("last_office_date");
            dto.status = rs.getInt("status");
            dto.statusChangeDate = rs.getLong("status_change_date");
            dto.showUnit = rs.getInt("show_unit");
            dto.designationEn = rs.getString("designation_en");
            dto.unitNameBn = rs.getString("unit_name_bn");
            dto.officeNameBn = rs.getString("office_name_bn");
            dto.unitNameEn = rs.getString("unit_name_en");
            dto.officeNameBn = rs.getString("office_name_en");
            dto.protikolpoStatus = rs.getInt("protikolpo_status");
            dto.jobGradeTypeCat = rs.getInt("job_grade_type_cat");
            dto.gradeTypeLevel = rs.getInt("grade_type_level");
            dto.layer1 = rs.getInt("layer_1");
            dto.layer2 = rs.getInt("layer_2");
            dto.created = rs.getLong("created");
            dto.modified = rs.getLong("modified");
            dto.createdBy = rs.getLong("created_by");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.roleType = rs.getInt("role_type");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.dept = rs.getInt("dept");
            dto.isDefault = rs.getInt("isDefault");
            dto.isAvailable = rs.getBoolean("isAvailable");

            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((EmployeeOfficeDTO) commonDTO, addSqlQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((EmployeeOfficeDTO) commonDTO, updateSqlQuery, false);
    }

    public List<EmployeeOfficeDTO> getDTOSbyPromotionHistoryId(long promotionHistoryId) {
        return getDTOs(String.format(getByPromotionHistoryIdQuery, tableName, promotionHistoryId));
    }

    public EmployeeOfficeDTO getDTObyPromotionHistoryId(long promotionHistoryId) {
        List<EmployeeOfficeDTO> dtoList = getDTOSbyPromotionHistoryId(promotionHistoryId);
        if (dtoList == null || dtoList.size() == 0)
            return null;
        else
            return dtoList.get(0);
    }

    private static final String updateLastOffice = "UPDATE %s SET last_office_date = %d , status = 0, lastModificationTime = %d" +
            "  where employee_record_id=%d AND isDeleted=0 AND isDefault=1 AND status = 1";

    public void updateLastOffice(EmployeeOfficeDTO officeDTO, long lastOfficeDate, long lastModificationTime) throws Exception {
        lastOfficeDate = lastOfficeDate - MILLISECONDS_PER_DAY;
        String updateSql = String.format(updateLastOffice, tableName, lastOfficeDate, lastModificationTime, officeDTO.employeeRecordId);
        logger.debug("Last office sql :" + updateSql);
        Utils.wrapWithAtomicReference(ar ->
                ConnectionAndStatementUtil.getWriteStatement(ps -> {
                    try {
                        ps.executeUpdate(updateSql);
                        Utils.addToRepoList(getTableName());
                    } catch (SQLException ex) {
                        logger.error("Last Office Update failed");
                        ex.printStackTrace();
                        ar.set(ex);
                    }
                }, CommonDAOService.CONNECTION_THREAD_LOCAL.get())
        );
    }

    private static final String updateLastOfficeAll = "UPDATE %s SET last_office_date=%d , status = 0,is_lpr_time_ofc = 1," +
            "lastModificationTime= %d where employee_record_id=%d AND isDeleted=0 AND status=1";

    public void updateLastOfficeAll(long erId, long lastOfficeDate) {
        lastOfficeDate = lastOfficeDate - MILLISECONDS_PER_DAY;
        long lastModificationTime = System.currentTimeMillis();
        String updateSql = String.format(updateLastOfficeAll, tableName, lastOfficeDate, lastModificationTime, erId);
        logger.debug("Last office sql :" + updateSql);
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            try {
                model.getStatement().executeUpdate(updateSql);
                recordUpdateTime(model.getConnection(), getTableName(), lastModificationTime);
            } catch (SQLException ex) {
                logger.error("Last Office Update failed");
                logger.error(ex);
            }
        });
    }

    public void deleteNonDefaultsByOrg(long orgId, long lastModificationTime) {

        String updateSql = String.format(deleteNonDefaults, tableName, orgId, lastModificationTime);
        logger.debug("deleteNonDefaultsByOrg sql :" + updateSql);
        ConnectionAndStatementUtil.getWriteStatement(ps -> {
            try {
                ps.executeUpdate(updateSql);
            } catch (SQLException throwables) {
                logger.error("deleteNonDefaultsByOrg failed");
                throwables.printStackTrace();
            }
        }, CommonDAOService.CONNECTION_THREAD_LOCAL.get());
    }

    public void deleteNonDefaultsByErId(long erId) {

        String updateSql = String.format(deleteNonDefaultsByErId, tableName, erId);
        logger.debug("deleteNonDefaultsByOrg sql :" + updateSql);
        ConnectionAndStatementUtil.getWriteStatement(ps -> {
            try {
                ps.executeUpdate(updateSql);
            } catch (SQLException throwables) {
                logger.error("deleteNonDefaultsByOrg failed");
                throwables.printStackTrace();
            }
        });
    }


    public void updateLastOfficeDate(long employeeRecordId, long lastOfficeDate, long promotionHistoryId) {
        String updateSql = String.format(updateLastOfficeDate, tableName, lastOfficeDate, employeeRecordId, promotionHistoryId);
        logger.debug("Last office date sql :" + updateSql);
        ConnectionAndStatementUtil.getWriteStatement(ps -> {
            try {
                ps.executeUpdate(updateSql);
            } catch (SQLException throwables) {
                logger.error("Last Office Date Update failed");
                throwables.printStackTrace();
            }
        });
    }

    public void updateAfterPromotionHistory(EmployeeOfficeDTO officeDTO, boolean addFlag, long lastModificationTime) throws Exception {
        if (addFlag) {
            add(officeDTO);
            Office_unit_organogramDAO.getInstance().updateVacancy(
                    officeDTO.officeUnitOrganogramId, officeDTO.employeeRecordId, false, officeDTO.modifiedBy, lastModificationTime);
        } else {
            EmployeeOfficeDTO oldDTO = getDTObyPromotionHistoryId(officeDTO.promotionHistoryId);
            if (oldDTO == null)
                return;
            officeDTO.iD = oldDTO.iD;
            update(officeDTO);
        }
    }

    private EmployeeAssignDTO convertToEmployeeAssignDTO(Employee_recordsDTO dto) {
        EmployeeAssignDTO assignDTO = new EmployeeAssignDTO();
        assignDTO.employee_id = dto.iD;
        assignDTO.employee_name_eng = dto.nameEng;
        assignDTO.employee_name_bng = dto.nameBng;
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(dto.iD);
        assignDTO.officeId = employeeOfficeDTO.officeId;
        assignDTO.gradeTypeLevel = employeeOfficeDTO.gradeTypeLevel;
        assignDTO.office_unit_id = employeeOfficeDTO.officeUnitId;
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(assignDTO.office_unit_id);
        if (officeUnitsDTO != null) {
            assignDTO.unit_name_eng = officeUnitsDTO.unitNameEng == null ? "" : officeUnitsDTO.unitNameEng;
            assignDTO.unit_name_bng = officeUnitsDTO.unitNameBng == null ? "" : officeUnitsDTO.unitNameBng;
        }
        assignDTO.username = dto.employeeNumber;

        assignDTO.organogram_id = employeeOfficeDTO.officeUnitOrganogramId;
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(assignDTO.organogram_id);
        if (officeUnitOrganograms != null) {
            assignDTO.organogram_name_eng = officeUnitOrganograms.designation_eng == null ? "" : officeUnitOrganograms.designation_eng;
            assignDTO.organogram_name_bng = officeUnitOrganograms.designation_bng == null ? "" : officeUnitOrganograms.designation_bng;
        }
        assignDTO.joining_date = dto.joiningDate;

        return assignDTO;
    }

    public EmployeeOfficeDTO getByOfficeUnitOrganogramId(long officeUnitOrganogramId) {
        String sql = String.format(getByOfficeUnitOrganogramId, officeUnitOrganogramId);
        return ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
    }

    public EmployeeOfficeDTO getDefaultByOfficeUnitOrganogramId(long officeUnitOrganogramId) {
        String sql = String.format(getDefaultByOfficeUnitOrganogramId, officeUnitOrganogramId);
        return ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
    }

    public EmployeeOfficeDTO getDefaultByERId(long erId) {
        String sql = String.format(getDefaultByErId, erId);
        return ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
    }

    public List<EmployeeOfficeDTO> getByERId(long erId) {
        String sql = String.format(getByErId, erId);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }

    public EmployeeOfficeDTO getLastByERId(long erId) {
        String sql = String.format(getLastByErId, erId);
        return ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
    }

    public List<EmployeeOfficeDTO> getByEmployeeRecordIdsWithMinJoiningDate(List<Long> empIds) {
        return ConnectionAndStatementUtil.getDTOListByNumbers(getByEmployeeRecordIdWithMinJoiningDate, empIds, this::buildObjectFromResultSet);
    }

    public long getCountByOfficeUnitIdIsDeletedOrNot(long officeUnitId) {
        String sql = String.format(getCountByOfficeUnitIdIsDeletedOrNot, officeUnitId);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getLong(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return 0L;
            }
        }, 0L);
    }

    public void deactivateStatus(long empId, long changeStatusDate, long lastModification, Connection connection) throws Exception {
        AtomicReference<Exception> atomicReference = new AtomicReference<>();
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                ps.setObject(1, changeStatusDate);
                ps.setObject(2, lastModification);
                ps.setObject(3, empId);
                ps.executeUpdate();
                recordUpdateTime(connection, "employee_offices", lastModification);
            } catch (SQLException ex) {
                ex.printStackTrace();
                atomicReference.set(ex);
            }
        }, connection, deactivateStatusSQL);
        if (atomicReference.get() != null) {
            throw atomicReference.get();
        }
    }

    public List<EmployeeOfficeDTO> getActiveByEmployeeRecordIdAndInChargeLevel(long empId, InChargeLevelEnum inChargeLevelEnum) {
        String sql = String.format(getActiveByEmployeeRecordIdAndInChargeLevel, empId, inChargeLevelEnum.getValue());
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildEmployeeOfficesDTO);
    }

    private static final String findByOfficeUnitIdsSql =
            "SELECT * FROM employee_offices WHERE office_unit_id IN (%s) AND isDeleted = 0 AND incharge_label = '%s'";

    public List<EmployeeOfficeDTO> findRoutineResponsiblyDTOsByOfficeIdsIgnoringStatus(Set<Long> officeUnitIds) {
        if (officeUnitIds == null || officeUnitIds.isEmpty()) {
            return new ArrayList<>();
        }
        String officeUnitIdsStr =
                officeUnitIds.stream()
                        .map(Objects::toString)
                        .collect(Collectors.joining(","));
        return ConnectionAndStatementUtil.getListOfT(
                String.format(
                        findByOfficeUnitIdsSql,
                        officeUnitIdsStr,
                        InChargeLevelEnum.ROUTINE_RESPONSIBILITY.getValue()
                ),
                this::buildEmployeeOfficesDTO
        );
    }

    public List<Long> getIdsByEmployeeIds(List<Long> empIds) {
        if (empIds == null || empIds.isEmpty()) {
            return new ArrayList<>();
        }
        return ConnectionAndStatementUtil.getDTOListByNumbers(getIdsByEmpIds, empIds, rs -> {
            try {
                return rs.getLong(1);
            } catch (SQLException ex) {
                return null;
            }
        });
    }

    private static final String deleteByIds = "UPDATE employee_offices SET isDeleted = 1,last_office_date = %d,lastModificationTime=%d,status = 0 WHERE id in (%s)";

    public void deleteByIds(List<Long> idList, long lastModificationTime, long lastOfficeDate) throws Exception {
        if (idList == null || idList.isEmpty()) {
            return;
        }
        AtomicReference<Exception> ar = new AtomicReference<>();
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            String ids = idList.stream().map(String::valueOf).collect(Collectors.joining(","));
            String sql = String.format(deleteByIds, lastOfficeDate, lastModificationTime, ids);
            logger.debug(sql);
            try {
                st.execute(sql);
                recordUpdateTime(CommonDAOService.CONNECTION_THREAD_LOCAL.get(), getTableName(), lastModificationTime);
            } catch (SQLException e) {
                e.printStackTrace();
                ar.set(e);
            }
        }, CommonDAOService.CONNECTION_THREAD_LOCAL.get());
        if (ar.get() != null) {
            throw ar.get();
        }
    }

    // this is used because same row is cloned for multiple role support!
    // multiple rows exist for the same organogramId and status = 1 because of users various roles!
    // yap, the meaning of employee_offices is destroyed now!
    // BE AWARE, you may also need to rethink your module if you see duplicate users!
    // in my opinion this one to many relation could have been solved by extracting the role table
    // or just simply making roleId a comma seperated list! but again I am not designing the system!
    public static List<EmployeeOfficeDTO> groupByOrganogramAndGetAny(List<EmployeeOfficeDTO> employeeOfficeDTOList) {
        return new ArrayList<>(
                employeeOfficeDTOList.stream()
                        .collect(Collectors.toMap(
                                employeeOfficeDTO -> employeeOfficeDTO.officeUnitOrganogramId,
                                e -> e,
                                (e1, e2) -> e1
                        )).values()
        );
    }

    private static final String getByOrgIdAndLastOfficeDateSQL =
            "SELECT * FROM employee_offices WHERE office_unit_organogram_id = %d AND last_office_date = %d";

    public List<EmployeeOfficeDTO> getByOrgIdAndLastOfficeDate(long organogramId, long last_office_date) {
        String sql = String.format(getByOrgIdAndLastOfficeDateSQL, organogramId, last_office_date);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildEmployeeOfficesDTO);
    }
}