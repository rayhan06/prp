package employee_offices;


public class Employee_officesDTO {

	public long employeeRecordId = 0;
	public long officeUnitId = 0;
	public String officeUnitName = "";
	public long officeUnitOrganogramId = 0;
	public String designation = "";
	public String employeeName = "";
	public long id;
	public long joiningDate;
	public long lastOfficeDate;
	public int status;

	public boolean isDeleted = false;
	public long lastModificationTime = 0;

	
    @Override
	public String toString() {
            return "$Employee_recordsDTO[" +
            " employeeRecordId = " + employeeRecordId +
            " officeUnitId = " + officeUnitId +
			" officeUnitName = " + officeUnitName +
            " officeUnitOrganogramId = " + officeUnitOrganogramId +
            " designation = " + designation +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}