package employee_offices;

import common.ApiResponse;
import common.RoleEnum;
import login.LoginDTO;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/EmployeeOfficesServlet")
@MultipartConfig
public class EmployeeOfficesServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(EmployeeOfficesServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        boolean deletePermission = userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId() || userDTO.roleID == RoleEnum.DATA_ENTRY.getRoleId();
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "ajax_delete":
                    if (deletePermission) {
                        long id = Long.parseLong(request.getParameter("ID"));
                        EmployeeOfficesDAO.getInstance().delete(userDTO.employee_record_id, id);
                        response.sendRedirect(getDeleteRedirectURL(request, 3, "parliament_service_history"));
                    }
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
            ApiResponse.sendErrorResponse(response, ex.getMessage());
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    public String getDeleteRedirectURL(HttpServletRequest request, int tab, String data) {
        if (request.getParameter("tab") != null) {
            long empId = Long.parseLong(request.getParameter("empId"));
            String userId = request.getParameter("userId");
            UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
            if (userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId() || userDTO.roleID == RoleEnum.DATA_ENTRY.getRoleId()) {
                return "Employee_recordsServlet?actionType=viewMultiForm&data=" + data + "&tab=" + tab + "&ID=" + empId + "&userId=" + userId;
            } else {
                return "Employee_recordsServlet?actionType=viewMyProfile&data=" + data + "&tab=" + tab + "&ID=" + empId + "&userId=" + userId;
            }
        } else {
            return "Employee_managementServlet?actionType=search";
        }
    }
}
