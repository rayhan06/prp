package employee_offices;

/*
 * @author Md. Erfan Hossain
 * @created 08/03/2021 - 8:27 PM
 * @project parliament
 */

public class EmployeeModel {
    public String userName;
    public String name;
    public String officeAndDesignation;

    @Override
    public String toString() {
        return "EmployeeModel{" +
                "userName='" + userName + '\'' +
                ", name='" + name + '\'' +
                ", officeAndDesignation='" + officeAndDesignation + '\'' +
                '}';
    }
}
