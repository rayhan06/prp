package employee_offices;

import employee_assign.EmployeeAssignDTO;
import employee_office_report.InChargeLevelEnum;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import user.UserDTO;
import user.UserRepository;
import util.IntegerWrapper;
import util.LockManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

@SuppressWarnings({"unused"})
public class EmployeeOfficeRepository implements Repository {
    private static final Logger logger = Logger.getLogger(EmployeeOfficeRepository.class);
    private static final EmployeeOfficesDAO employeeOfficesDAO = EmployeeOfficesDAO.getInstance();
    private Map<Long, EmployeeOfficeDTO> mapById;
    private Map<Long, List<EmployeeOfficeDTO>> mapByEmployeeId;
    private Map<Long, EmployeeOfficeDTO> mapByOrganogramId;
    private List<EmployeeOfficeDTO> employeeOfficeDTOList;
    private static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private static final ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
    private static final ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();

    private EmployeeOfficeRepository() {
        init();
        RepositoryManager.getInstance().addRepository(this);
    }

    private void init() {
        mapById = new ConcurrentHashMap<>();
        mapByEmployeeId = new ConcurrentHashMap<>();
        mapByOrganogramId = new ConcurrentHashMap<>();
    }


    private static class LazyLoader {
        static final EmployeeOfficeRepository INSTANCE = new EmployeeOfficeRepository();
    }

    public static EmployeeOfficeRepository getInstance() {
        return EmployeeOfficeRepository.LazyLoader.INSTANCE;
    }


    @Override
    public void reloadWithExactModificationTime(long time) {
        List<EmployeeOfficeDTO> dtoList = employeeOfficesDAO.getAllDTOsExactLastModificationTime(time);
        doCaching(dtoList);
    }


    @Override
    public void reload(boolean reloadAll) {
        logger.debug("Reload really started for EmployeeOfficeRepository, reloadAll : " + reloadAll);
        if (reloadAll) {
            init();
        }
        List<EmployeeOfficeDTO> dtoList = employeeOfficesDAO.getDTOs(reloadAll);
        doCaching(dtoList);
        logger.debug("Reload ended for EmployeeOfficeRepository, reloadAll : " + reloadAll);
    }

    private void doCaching(List<EmployeeOfficeDTO> dtoList) {
        if (dtoList!=null && dtoList.size() > 0) {
            try {
                writeLock.lock();
                dtoList.stream()
                        .peek(this::removeIfPresent)
                        .filter(dto -> dto.isDeleted == 0 && dto.status == 1)
                        .forEach(this::updateCache);
                employeeOfficeDTOList = new ArrayList<>(mapById.values());
            } finally {
                writeLock.unlock();
            }
        }
    }

    private void updateCache(EmployeeOfficeDTO dto) {
        if (dto != null && dto.isDeleted == 0 && dto.status == 1) {
            mapById.put(dto.id, dto);
            List<EmployeeOfficeDTO> list = mapByEmployeeId.getOrDefault(dto.employeeRecordId, new ArrayList<>());
            list.add(dto);
            mapByEmployeeId.put(dto.employeeRecordId, list);
            mapByOrganogramId.put(dto.officeUnitOrganogramId, dto);
        }
    }

    private void removeIfPresent(EmployeeOfficeDTO dto) {
        if (dto != null) {
            EmployeeOfficeDTO oldDTO = mapById.get(dto.id);
            if (oldDTO != null) {
                if (mapByEmployeeId.get(dto.employeeRecordId) != null && mapByEmployeeId.get(dto.employeeRecordId).size() > 0) {
                    mapByEmployeeId.get(dto.employeeRecordId).remove(oldDTO);
                }
                mapById.remove(dto.id);
                mapByOrganogramId.remove(dto.officeUnitOrganogramId);
                employeeOfficeDTOList.remove(oldDTO);
            }
        }
    }

    public EmployeeOfficeDTO getById(long id) {
        try {
            readLock.lock();
            EmployeeOfficeDTO employeeOfficeDTO = mapById.get(id);
            if (mapById.get(id) == null) {
                synchronized (LockManager.getLock(id + "EOR")) {
                    if (mapById.get(id) == null) {
                        EmployeeOfficeDTO dto = employeeOfficesDAO.getById(id);
                        if (dto != null) {
                            readLock.unlock();
                            writeLock.lock();
                            try {
                                employeeOfficeDTO = dto;
                                if(dto.status == 1 && dto.isDeleted == 0){
                                    updateCache(dto);
                                    employeeOfficeDTOList = new ArrayList<>(mapById.values());
                                }
                                readLock.lock();
                            } finally {
                                writeLock.unlock();
                            }
                        }
                    }
                }
            }
            return employeeOfficeDTO;
        } finally {
            readLock.unlock();
        }
    }

    public List<EmployeeOfficeDTO> getByEmployeeRecordId(long employeeRecordId) {
        try {
            readLock.lock();
            if (mapByEmployeeId.get(employeeRecordId) == null || mapByEmployeeId.get(employeeRecordId).size() == 0) {
                synchronized (LockManager.getLock(employeeRecordId + "EROR")) {
                    if (mapByEmployeeId.get(employeeRecordId) == null || mapByEmployeeId.get(employeeRecordId).size() == 0) {
                        List<EmployeeOfficeDTO> dtoList = employeeOfficesDAO.getByEmployeeRecordIdIgnoringStatus(employeeRecordId);
                        readLock.unlock();
                        doCaching(dtoList);
                        readLock.lock();
                    }
                }
            }
            return mapByEmployeeId.get(employeeRecordId) == null ? new ArrayList<>() : mapByEmployeeId.get(employeeRecordId);
        } finally {
            readLock.unlock();
        }
    }

    public EmployeeOfficeDTO getByEmployeeRecordIdIsDefault(long employeeRecordId) {
        try {
            readLock.lock();
            List<EmployeeOfficeDTO> dtoList = mapByEmployeeId.get(employeeRecordId);
            if (dtoList != null && dtoList.size() > 0) {
                EmployeeOfficeDTO dto = dtoList.stream()
                        .filter(e -> e.isDefault == 1)
                        .filter(e -> e.isDefaultRole == 1)
                        .findAny()
                        .orElse(null);
                if (dto != null) {
                    return dto;
                }
            }
        } finally {
            readLock.unlock();
        }
        EmployeeOfficeDTO dto = employeeOfficesDAO.getByEmployeeRecordIdIsDefault(employeeRecordId);
        if (dto != null) {
            try {
                writeLock.lock();
                removeIfPresent(dto);
                updateCache(dto);
                employeeOfficeDTOList = new ArrayList<>(mapById.values());
            } finally {
                writeLock.unlock();
            }
        }
        return dto;
    }

    public EmployeeOfficeDTO getFromCacheByEmployeeRecordIdIsDefault(long employeeRecordId) {
        try {
            readLock.lock();
            List<EmployeeOfficeDTO> dtoList = mapByEmployeeId.get(employeeRecordId);
            if (dtoList == null || dtoList.size() == 0) {
                return null;
            }
            return dtoList.stream()
                    .filter(e -> e.isDefault == 1)
                    .findAny()
                    .orElse(null);
        } finally {
            readLock.unlock();
        }
    }

    //workflow
    public EmployeeOfficeDTO getByEmployeeRecordIdIsDefaultOrAny(long employeeRecordId) {
        List<EmployeeOfficeDTO> dtoList = getByEmployeeRecordId(employeeRecordId);
        try {
            readLock.lock();
            EmployeeOfficeDTO dto = dtoList.stream()
                    .filter(e -> e.isDefault == 1)
                    .filter(e -> e.officeUnitOrganogramId >= 0)
                    .findAny()
                    .orElse(null);
            if (dto == null) {
                dto = dtoList.stream()
                        .filter(e -> e.officeUnitOrganogramId >= 0)
                        .findAny()
                        .orElse(null);
            }
            if (dto != null) {
                return dto;
            }
        } finally {
            readLock.unlock();
        }

        return employeeOfficesDAO.getForStatusZeroAndEmpId(employeeRecordId)
                .stream()
                .filter(e -> e.officeUnitOrganogramId >= 0)
                .findAny()
                .orElse(null);
    }

    @Override
    public String getTableName() {
        return "employee_offices";
    }

    public Set<Long> getEmpIdByOrganogramId(Set<Long> organogramIdList) {
        try {
            readLock.lock();
            return employeeOfficeDTOList
                    .parallelStream()
                    .filter(e -> organogramIdList.contains(e.officeUnitOrganogramId) && e.inchargeLabel.equals(InChargeLevelEnum.ROUTINE_RESPONSIBILITY.getValue()))
                    .map(e -> e.employeeRecordId)
                    .collect(toSet());
        } finally {
            readLock.unlock();
        }
    }

    private Set<Long> getChildOfficeIdWithParentIdSet(Office_unitsDTO office) {
        return Office_unitsRepository.getInstance().getAllChildOfficeDTOWithParent(office.iD)
                .stream()
                .map(e -> e.iD)
                .collect(toSet());
    }

    private Set<Long> getEmployeeIdSet(Set<Long> officeUnitsIdSet) {
        try {
            readLock.lock();
            return getEmployeeOfficeDTOs(officeUnitsIdSet)
                    .stream()
                    .filter(e -> e.status == 1)
                    .filter(e -> e.inchargeLabel.equals(InChargeLevelEnum.ROUTINE_RESPONSIBILITY.getValue()))
                    .map(e -> e.employeeRecordId)
                    .collect(toSet());
        } finally {
            readLock.unlock();
        }
    }

    public Map<String, Object> getDesignationCountList(long officeUnitId, String language) {
        return getDesignationCountList(officeUnitId, language, false);
    }

    public Map<String, Object> getDesignationCountList(long officeUnitId, String language, boolean withParent) {
        return getDesignationCountList(officeUnitId, "English".equalsIgnoreCase(language), withParent);
    }
    
    public Set<Long> getByOffice(long office) {
        try {
            readLock.lock();
            return employeeOfficeDTOList.parallelStream()
                    .filter(e -> e.officeUnitId == office)
                    .map(e -> e.officeUnitOrganogramId)
                    .collect(toSet());
        } finally {
            readLock.unlock();
        }
    }

    public Map<String, Object> getDesignationCountList(long officeUnitId, boolean isLanguageEnglish, boolean withParent) {
        List<Office_unitsDTO> list = Office_unitsRepository.getInstance().getChildList(officeUnitId);
        if (withParent) {
            list.add(Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitId));
        }
        IntegerWrapper ai = new IntegerWrapper(0);
        Map<String, Object> result = list
                .stream()
                .collect(Collectors.toMap(e -> e, this::getChildOfficeIdWithParentIdSet))
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
                        e -> String.valueOf(ai.incrementAndGet()),
                        e -> getDesignationCountAndLink(isLanguageEnglish ? e.getKey().unitNameEng : e.getKey().unitNameBng, getEmployeeIdSet(e.getValue()).size(), e.getKey().iD)
                ));
        if (officeUnitId == 1) {
            List<Employee_recordsDTO> employeeRecordsDTOList = Employee_recordsRepository.getInstance().getEmployee_recordsList().stream().filter(dto -> dto.officerTypeCat == 3).collect(toList());
            Map<String, Object> countLinkMap = new HashMap<>();
            countLinkMap.put("title", isLanguageEnglish ? "Others" : "অন্যান্য");
            countLinkMap.put("count", employeeRecordsDTOList.size());
            countLinkMap.put("link", "Employee_managementServlet?actionType=search&empOfficerCat=3");
            result.put(ai.incrementAndGet() + "", countLinkMap);
        }
        return result;
    }

    private Map<String, Object> getDesignationCountAndLink(String title, int count, long officeUnitId) {
        Map<String, Object> countLinkMap = new HashMap<>();
        countLinkMap.put("title", title);
        countLinkMap.put("count", count);
        countLinkMap.put("link", "Employee_office_report_Servlet?actionType=reportPage&incharge_label=1&officeUnitId=" + officeUnitId);
        return countLinkMap;
    }

    public List<EmployeeOfficeDTO> getEmployeeOfficeDTOListByOfficeUnitId(long officeUnitId) {
        try {
            readLock.lock();
            List<EmployeeOfficeDTO> dtoList =
                    employeeOfficeDTOList.parallelStream()
                            .filter(dto -> dto.officeUnitId == officeUnitId)
                            .collect(toList());
            return EmployeeOfficesDAO.groupByOrganogramAndGetAny(dtoList);
        } finally {
            readLock.unlock();
        }
    }

    public List<EmployeeOfficeDTO> getEmployeeOfficeDTOs(Set<Long> officeUnitIds) {
        try {
            readLock.lock();
            List<EmployeeOfficeDTO> dtoList =
                    employeeOfficeDTOList.parallelStream()
                            .filter(dto -> officeUnitIds.contains(dto.officeUnitId))
                            .collect(toList());
            return EmployeeOfficesDAO.groupByOrganogramAndGetAny(dtoList);
        } finally {
            readLock.unlock();
        }
    }

    public List<EmployeeOfficeDTO> getNonVirtualEmployeeOfficeDTOs(Set<Long> officeUnitIds) {
        try {
            readLock.lock();
            return getEmployeeOfficeDTOs(officeUnitIds)
                    .stream()
                    .filter(this::isDesignationNotVirtual)
                    .collect(toList());
        } finally {
            readLock.unlock();
        }
    }

    private boolean isDesignationNotVirtual(EmployeeOfficeDTO dto) {
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(dto.officeUnitOrganogramId);
        return officeUnitOrganograms != null && !officeUnitOrganograms.isVirtual;
    }

    public List<EmployeeAssignDTO> getEmployeeAssignDTOs(long officeUnitId) {
        try {
            readLock.lock();
            return getEmployeeOfficeDTOListByOfficeUnitId(officeUnitId)
                    .stream()
                    .map(this::convertToEmployeeAssignDTO)
                    .collect(toList());
        } finally {
            readLock.unlock();
        }

    }

    public EmployeeOfficeDTO getByOfficeUnitOrganogramId(long officeUnitOrganogramId) {
        try {
            readLock.lock();
            if (mapByOrganogramId.get(officeUnitOrganogramId) == null) {
                synchronized (LockManager.getLock(officeUnitOrganogramId + "OUOR")) {
                    if (mapByOrganogramId.get(officeUnitOrganogramId) == null) {
                        EmployeeOfficeDTO dto = employeeOfficesDAO.getDefaultByOfficeUnitOrganogramId(officeUnitOrganogramId);
                        if (dto != null) {
                            readLock.unlock();
                            writeLock.lock();
                            try {
                                removeIfPresent(dto);
                                updateCache(dto);
                                employeeOfficeDTOList = new ArrayList<>(mapById.values());
                                readLock.lock();
                            } finally {
                                writeLock.unlock();
                            }
                        }
                    }
                }
            }
            return mapByOrganogramId.get(officeUnitOrganogramId);
        } finally {
            readLock.unlock();
        }
    }

    public EmployeeOfficeDTO getByOfficeUnitOrganogramIdRepoOnly(long officeUnitOrganogramId) {

        return mapByOrganogramId.getOrDefault(officeUnitOrganogramId, null);
    }


    public EmployeeOfficeDTO getActiveByOfficeUnitOrganogramId(long officeUnitOrganogramId) {

        EmployeeOfficeDTO employeeOfficeDTO = getByOfficeUnitOrganogramId(officeUnitOrganogramId);
        return (employeeOfficeDTO == null || employeeOfficeDTO.status != 1) ? null : employeeOfficeDTO;
    }

    private EmployeeAssignDTO convertToEmployeeAssignDTO(EmployeeOfficeDTO employeeOfficeDTO) {
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId);
        EmployeeAssignDTO dto = new EmployeeAssignDTO();
        dto.employee_id = employeeOfficeDTO.employeeRecordId;
        dto.employee_name_eng = employeeRecordsDTO.nameEng;
        dto.employee_name_bng = employeeRecordsDTO.nameBng;
        dto.officeId = employeeOfficeDTO.officeId;
        dto.office_unit_id = employeeOfficeDTO.officeUnitId;
        dto.unit_name_eng = employeeOfficeDTO.unitNameEn;
        dto.unit_name_bng = employeeOfficeDTO.unitNameBn;
        dto.username = employeeRecordsDTO.employeeNumber;
        dto.organogram_id = employeeOfficeDTO.officeUnitOrganogramId;
        dto.organogram_name_eng = employeeOfficeDTO.designationEn;
        dto.organogram_name_bng = employeeOfficeDTO.designationBn;
        return dto;
    }

    public Map<String, Object> getGenderList(long officeUnitId, String language) {
        Set<Long> officeUnitsIdSet = Office_unitsRepository.getInstance().getAllChildOfficeDTOWithParent(officeUnitId)
                .stream()
                .map(e -> e.iD)
                .collect(Collectors.toSet());
        try {
            readLock.lock();
            Set<Long> employeeRecordIdSet = getEmployeeOfficeDTOs(officeUnitsIdSet)
                    .stream()
                    .map(e -> e.employeeRecordId)
                    .collect(toSet());
            List<Employee_recordsDTO> employeeRecordsDTOList = Employee_recordsRepository.getInstance().getByIds(employeeRecordIdSet);
            return getGenderList(employeeRecordsDTOList, officeUnitId, language);
        } finally {
            readLock.unlock();
        }
    }

    public Map<String, Object> getGenderList(List<Employee_recordsDTO> employeeRecordsDTOList, long officeUnitId, String language) {
        return getGenderList(employeeRecordsDTOList, officeUnitId, "English".equalsIgnoreCase(language));
    }

    public Map<String, Object> getGenderList(List<Employee_recordsDTO> employeeRecordsDTOList, long officeUnitId, boolean isLangEng) {
        if (employeeRecordsDTOList == null || employeeRecordsDTOList.size() == 0) {
            return new HashMap<>();
        }
        Map<Integer, Long> mapCount = employeeRecordsDTOList.stream()
                .collect(groupingBy(e -> e.gender, counting()));
        IntegerWrapper ai = new IntegerWrapper(0);

        Map<Integer, String> genderModelMap = CatRepository.getInstance().getCategoryLanguageModelList("gender")
                .stream()
                .collect(toMap(e -> e.categoryValue, e -> isLangEng ? e.englishText : e.banglaText));
        return mapCount.entrySet()
                .stream()
                .filter(e -> genderModelMap.get(e.getKey()) != null)

                .collect(toMap(e -> String.valueOf(ai.incrementAndGet()), e -> getGenderCountAndLink(genderModelMap.get(e.getKey()), e.getValue(), e.getKey(), officeUnitId)));
    }

    private Map<String, Object> getGenderCountAndLink(String title, Long count, Integer key, long officeUnitId) {
        Map<String, Object> countLinkMap = new HashMap<>();
        countLinkMap.put("title", title);
        countLinkMap.put("count", count);
        countLinkMap.put("link", "Employee_office_report_Servlet?actionType=reportPage&gender=" + key + "&officeUnitId=" + officeUnitId);
        return countLinkMap;
    }

    public Set<Long> getByRole(int role) {
        try {
            readLock.lock();
            return employeeOfficeDTOList.parallelStream()
                    .filter(e -> e.organogramRole == role)
                    .map(e -> e.officeUnitOrganogramId)
                    .collect(toSet());
        } finally {
            readLock.unlock();
        }
    }
    
    public Set<Long> getErIdByRole(int role) {
        try {
            readLock.lock();
            return employeeOfficeDTOList.parallelStream()
                    .filter(e -> e.organogramRole == role)
                    .map(e -> e.employeeRecordId)
                    .collect(toSet());
        } finally {
            readLock.unlock();
        }
    }
    
    
    public Set<Long> getByRole(int role, int medicalDept) {
        try {
            readLock.lock();
            return employeeOfficeDTOList.parallelStream()
                    .filter(e -> e.organogramRole == role)
                    .filter(e -> e.medicalDeptCat == medicalDept)
                    .map(e -> e.officeUnitOrganogramId)
                 
                    .collect(toSet());
        } finally {
            readLock.unlock();
        }
    }

    public Set<Long> getByRoles(ArrayList<Integer> roles) {
        try {
            readLock.lock();
            return employeeOfficeDTOList.parallelStream()
                    .filter(e -> roles.contains(e.organogramRole))
                    .map(e -> e.officeUnitOrganogramId)
                    .collect(toSet());
        } finally {
            readLock.unlock();
        }
    }

    public String buildOptions(String language) {
        try {
            readLock.lock();
            return Utils.buildOptions(employeeOfficeDTOList.stream()
                    .filter(e -> e.isDeleted == 0 && e.status == 1)
                    .map(this::buildOptionDTO)
                    .filter(Objects::nonNull)
                    .collect(toList()), language, null);
        } finally {
            readLock.unlock();
        }
    }

    private OptionDTO buildOptionDTO(EmployeeOfficeDTO dto) {
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(dto.officeUnitId);
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(dto.officeUnitOrganogramId);
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(dto.employeeRecordId);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByEmployeeRecordId(dto.employeeRecordId);
        if (officeUnitsDTO == null || officeUnitOrganograms == null || employeeRecordsDTO == null || userDTO == null || userDTO.ID == 0) {
            return null;
        }
        return new OptionDTO(employeeRecordsDTO.nameEng + ", " + officeUnitOrganograms.designation_eng + ", " + officeUnitsDTO.unitNameEng,
                employeeRecordsDTO.nameBng + ", " + officeUnitOrganograms.designation_bng + ", " + officeUnitsDTO.unitNameBng, String.valueOf(userDTO.ID));
    }

    public String buildOptionsForOrganogram(String language) {
        try {
            readLock.lock();
            return Utils.buildOptions(employeeOfficeDTOList.stream()
                    .filter(e -> e.isDeleted == 0 && e.status == 1)
                    .map(this::buildOptionDTOForOrganogram)
                    .filter(Objects::nonNull)
                    .collect(toList()), language, null);
        } finally {
            readLock.unlock();
        }
    }

    private OptionDTO buildOptionDTOForOrganogram(EmployeeOfficeDTO dto) {
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(dto.officeUnitId);
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(dto.officeUnitOrganogramId);
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(dto.employeeRecordId);
        if (officeUnitsDTO == null || officeUnitOrganograms == null || employeeRecordsDTO == null) {
            return null;
        }
        return new OptionDTO(employeeRecordsDTO.nameEng + ", " + officeUnitOrganograms.designation_eng + ", " + officeUnitsDTO.unitNameEng,
                employeeRecordsDTO.nameBng + ", " + officeUnitOrganograms.designation_bng + ", " + officeUnitsDTO.unitNameBng, String.valueOf(dto.officeUnitOrganogramId));
    }

    public void removedFormCacheByEmpId(long empId) {
        try {
            readLock.lock();
            List<EmployeeOfficeDTO> list = mapByEmployeeId.get(empId);
            if (list != null && list.size() > 0) {
                readLock.unlock();
                writeLock.lock();
                try {
                    mapByEmployeeId.remove(empId);
                    list.forEach(e -> {
                        mapById.remove(e.iD);
                        mapByOrganogramId.remove(e.officeUnitOrganogramId);
                    });
                    employeeOfficeDTOList = new ArrayList<>(mapById.values());
                    readLock.lock();
                } finally {
                    writeLock.unlock();
                }
            }
        } finally {
            readLock.unlock();
        }
    }

    public void removedFormCacheByEmpIdInSeparateThread(long empId) {
        Thread thread = new Thread(() -> removedFormCacheByEmpId(empId));
        thread.setDaemon(true);
        thread.start();
    }

    public void updateCache(List<Long> ids) {
        if (ids == null || ids.isEmpty()) {
            return;
        }
        List<EmployeeOfficeDTO> list = employeeOfficesDAO.getDTOs(ids);
        if (list == null || list.isEmpty()) {
            return;
        }
        doCaching(list);
    }

    public void updateCache(long organogramId, long last_office_date) {
        List<EmployeeOfficeDTO> dtoList = employeeOfficesDAO.getByOrgIdAndLastOfficeDate(organogramId, last_office_date);
        doCaching(dtoList);
    }
}