package employee_offices;

import sessionmanager.SessionConstants;
import util.CommonDTO;

public class EmployeeOfficeDTO extends CommonDTO implements Comparable<EmployeeOfficeDTO> {
    public long id;
    public Long employeeRecordId;
    public String identificationNumber = "";
    public Integer officeId;
    public Long officeUnitId = -1L;
    public Long officeUnitOrganogramId = -1L;
    public String designation;
    public Integer designationLevel = 0;
    public Integer designationSequence = 0;
    public Integer isDefaultRole = 1;
    public Integer officeHead = 0;
    public Integer summaryNothiPostType = 0;
    public String inchargeLabel = "";
    public Integer mainRoleId = 0;
    public Long joiningDate = SessionConstants.MIN_DATE;
    public Long lastOfficeDate = SessionConstants.MIN_DATE;
    public Integer status = 1;
    public Long statusChangeDate = SessionConstants.MIN_DATE;
    public Integer showUnit = 1;
    public long promotionHistoryId = -1;
    public String designationEn;
    public String designationBn;
    public String unitNameBn;
    public String officeNameBn;
    public String unitNameEn;
    public String officeNameEn;
    public Integer protikolpoStatus = 1;
    public Long created = SessionConstants.MIN_DATE;
    public Long modified = SessionConstants.MIN_DATE;
    public Long createdBy;
    public Long modifiedBy;
    public Integer roleType = 0;
    public Integer dept = 0;
    public Integer isDefault;
    public Boolean isAvailable = false;
    public int organogramRole = SessionConstants.PARLIAMENT_EMPLOYEE_ROLE;
    public int jobGradeTypeCat = -1;
    public int layer1 = 0;
    public int layer2 = 0;
    public long gradeTypeLevel = 0;
    public int medicalDeptCat = -1;
    public long salaryGradeType = 0;


    public Long getJoiningDate() {
        return joiningDate;
    }

    @Override
    public int compareTo(EmployeeOfficeDTO o) {
        return this.getJoiningDate().compareTo(o.getJoiningDate());
    }

    public boolean isActiveInTimeRange(long startInclusive, long endExclusive) {
        if(joiningDate == null || joiningDate == SessionConstants.MIN_DATE) {
            return false;
        }
        if(lastOfficeDate == null || lastOfficeDate == SessionConstants.MIN_DATE) {
            return joiningDate < endExclusive;
        }
        final long ONE_DAY_IN_MILLIS = (24L * 60) * 60 * 1000;
        long lastOfficeTime = lastOfficeDate + ONE_DAY_IN_MILLIS - 1;
        // joiningDate is 12AM of the date. Employee is active upto 11:59PM that date.

        boolean lastOfficeTimeBeforeStart = lastOfficeTime < startInclusive;
        boolean joiningDateAfterEnd = endExclusive <= joiningDate;

        return !(lastOfficeTimeBeforeStart || joiningDateAfterEnd);
    }

    @Override
    public String toString() {
        return "EmployeeOfficeDTO{" +
                "id=" + id +
                ", employeeRecordId=" + employeeRecordId +
                ", identificationNumber='" + identificationNumber + '\'' +
                ", officeId=" + officeId +
                ", officeUnitId=" + officeUnitId +
                ", officeUnitOrganogramId=" + officeUnitOrganogramId +
                ", designation='" + designation + '\'' +
                ", designationLevel=" + designationLevel +
                ", designationSequence=" + designationSequence +
                ", isDefaultRole=" + isDefaultRole +
                ", officeHead=" + officeHead +
                ", summaryNothiPostType=" + summaryNothiPostType +
                ", inchargeLabel='" + inchargeLabel + '\'' +
                ", mainRoleId=" + mainRoleId +
                ", joiningDate=" + joiningDate +
                ", lastOfficeDate=" + lastOfficeDate +
                ", status=" + status +
                ", statusChangeDate=" + statusChangeDate +
                ", showUnit=" + showUnit +
                ", promotionHistoryId=" + promotionHistoryId +
                ", designationEn='" + designationEn + '\'' +
                ", designationBn='" + designationBn + '\'' +
                ", unitNameBn='" + unitNameBn + '\'' +
                ", officeNameBn='" + officeNameBn + '\'' +
                ", unitNameEn='" + unitNameEn + '\'' +
                ", officeNameEn='" + officeNameEn + '\'' +
                ", protikolpoStatus=" + protikolpoStatus +
                ", created=" + created +
                ", modified=" + modified +
                ", createdBy=" + createdBy +
                ", modifiedBy=" + modifiedBy +
                ", roleType=" + roleType +
                ", dept=" + dept +
                ", isDefault=" + isDefault +
                ", isAvailable=" + isAvailable +
                ", organogramRole=" + organogramRole +
                ", jobGradeTypeCat=" + jobGradeTypeCat +
                ", layer1=" + layer1 +
                ", layer2=" + layer2 +
                '}';
    }
}
