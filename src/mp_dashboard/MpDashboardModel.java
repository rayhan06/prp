package mp_dashboard;

/*
 * @author Md. Erfan Hossain
 * @created 28/12/2021 - 11:58 AM
 * @project parliament
 */

import java.util.Map;

public class MpDashboardModel {
    public Map<Integer, Object> party;
    public Map<Long, Object> division;
    public Map<Integer, Object> gender;
    public Map<Integer, Object> age;
}
