package mp_dashboard;

import category.CategoryDTO;
import com.google.gson.Gson;
import election_constituency.Election_constituencyRepository;
import election_wise_mp.Election_wise_mpDTO;
import election_wise_mp.Election_wise_mpRepository;
import employee_records.Employee_recordsRepository;
import employee_records.MpStatusEnum;
import geolocation.GeoDivisionRepository;
import pb.CatRepository;
import pb.Utils;
import political_party.Political_partyRepository;
import util.HttpRequestUtils;
import util.IntegerWrapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;


@WebServlet("/MpDashboardServlet")
@MultipartConfig
public class MpDashboardServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private Map<Integer, Object> getPartyWiseResult(long electionDetailsId, List<Election_wise_mpDTO> election_wise_mpDTOList, boolean isLangEng) {
        IntegerWrapper iw = new IntegerWrapper();
        return election_wise_mpDTOList
                .parallelStream()
                .filter(dto -> dto.politicalPartyId != 0)
                .collect(groupingBy(dto -> dto.politicalPartyId, counting()))
                .entrySet().stream()
                .filter(e -> Political_partyRepository.getInstance().getPolitical_partyDTOByID(e.getKey()) != null)
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue))
                .entrySet().stream()
                .collect(Collectors.toMap(e -> iw.incrementAndGet(),
                        e -> getPartyCountAndLink(Political_partyRepository.getInstance().getText(e.getKey(), isLangEng),
                                e.getValue(), e.getKey(), electionDetailsId)));
    }

    private Map<Long, Object> getDivisionWiseResult(long electionDetailsId, List<Election_wise_mpDTO> election_wise_mpDTOList, boolean isLangEng) {
        Map<Long,Long> divisionWiseCount = election_wise_mpDTOList
                .parallelStream()
                .map(dto -> Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(dto.electionConstituencyId))
                .collect(groupingBy(dto -> dto.geoDivisionsType, counting()));
        long undefinedSize = divisionWiseCount.entrySet()
                .stream()
                .filter(e->GeoDivisionRepository.getInstance().getById(e.getKey()) == null)
                .mapToLong(Map.Entry::getValue)
                .sum();
        Map<Long,Object> result = divisionWiseCount.entrySet()
                .stream()
                .filter(e->GeoDivisionRepository.getInstance().getById(e.getKey()) != null)
                .collect(Collectors.toMap(Map.Entry::getKey, e -> getCountAndLink(GeoDivisionRepository.getInstance().getText(isLangEng, e.getKey()), e.getValue(), electionDetailsId,null,null)));
        if(undefinedSize >0){
            result.put(Long.MAX_VALUE,getCountAndLink(isLangEng?"Undefined":"অজানা",undefinedSize,electionDetailsId,"division","0"));
        }
        return result;
    }

    private Map<Integer, Object> getGenderWiseResult(long electionDetailsId, List<Election_wise_mpDTO> election_wise_mpDTOList, boolean isLangEng) {
        IntegerWrapper iw = new IntegerWrapper();
        Map<Integer, CategoryDTO> mapByGenderValue = CatRepository.getInstance()
                .getCategoryDTOList("gender")
                .stream()
                .collect(Collectors.toMap(e -> e.value, e -> e));
        Map<Integer,Integer> temp = election_wise_mpDTOList.parallelStream()
                .filter(dto -> dto.employeeRecordsId != 0)
                .map(dto -> Employee_recordsRepository.getInstance().getById(dto.employeeRecordsId))
                .filter(Objects::nonNull)
                .collect(groupingBy(dto -> dto.gender, collectingAndThen(counting(), Long::intValue)));
        int undefinedGenderSize = temp.entrySet()
                .stream()
                .filter(e->mapByGenderValue.get(e.getKey())== null)
                .mapToInt(Map.Entry::getValue)
                .sum();
        Map<Integer ,Object> result = temp.entrySet()
                .stream()
                .filter(e->mapByGenderValue.get(e.getKey())!=null)
                .collect(toMap(e -> iw.incrementAndGet(),
                        e -> getCountAndLink(isLangEng ? mapByGenderValue.get(e.getKey()).nameEn : mapByGenderValue.get(e.getKey()).nameBn,e.getValue(), electionDetailsId,"gender",String.valueOf(e.getKey()))));
        if(undefinedGenderSize >0){
            result.put(iw.incrementAndGet(),getCountAndLink(isLangEng?"Undefined":"অজানা",undefinedGenderSize,electionDetailsId,"gender","0"));
        }
        return result;
    }

    private Map<Integer, Object> getAgeWiseResult(long electionDetailsId, List<Election_wise_mpDTO> election_wise_mpDTOList, boolean isLangEng) {
        return election_wise_mpDTOList.stream()
                .map(dto -> Utils.calculateAge(dto.dateOfBirth))
                .collect(Collectors.groupingBy(Utils::getCalculateAgeRangeIndex, Collectors.collectingAndThen(Collectors.counting(), Long::intValue)))
                .entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> getCountAndLink(Utils.getAgeRangeTextByIndex(e.getKey(),isLangEng), e.getValue(), electionDetailsId,null,null)));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
            List<Election_wise_mpDTO> election_wise_mpDTOList;
            String responseJsonData;
            switch (request.getParameter("actionType")) {
                case "ajax_mp_dashboard": {
                    long electionDetailsId = Long.parseLong(request.getParameter("electionDetailsId"));
                    election_wise_mpDTOList = Election_wise_mpRepository.getInstance()
                            .getElection_wise_mpDTOByElectionDetailsId(electionDetailsId)
                            .parallelStream()
                            .filter(e->e.isDeleted == 0 && (e.mpStatus == MpStatusEnum.ACTIVE.getValue() || e.mpStatus == MpStatusEnum.COMPLETED.getValue()))
                            .collect(toList());
                    MpDashboardModel model = new MpDashboardModel();
                    model.party = getPartyWiseResult(electionDetailsId, election_wise_mpDTOList, isLangEng);
                    model.division = getDivisionWiseResult(electionDetailsId, election_wise_mpDTOList, isLangEng);
                    model.gender = getGenderWiseResult(electionDetailsId, election_wise_mpDTOList, isLangEng);
                    model.age = getAgeWiseResult(electionDetailsId, election_wise_mpDTOList, isLangEng);
                    responseJsonData = new Gson().toJson(model);
                }
                break;
                case "party_count": {
                    long electionDetailsId = Long.parseLong(request.getParameter("electionDetailsId"));
                    election_wise_mpDTOList = Election_wise_mpRepository.getInstance().getElection_wise_mpDTOByElectionDetailsId(electionDetailsId);
                    Map<Integer, Object> result = getPartyWiseResult(electionDetailsId, election_wise_mpDTOList, isLangEng);
                    responseJsonData = new Gson().toJson(result);
                    break;
                }
                case "division_count": {
                    long electionDetailsId = Long.parseLong(request.getParameter("electionDetailsId"));
                    election_wise_mpDTOList = Election_wise_mpRepository.getInstance().getElection_wise_mpDTOByElectionDetailsId(electionDetailsId);
                    Map<Long, Object> result = getDivisionWiseResult(electionDetailsId, election_wise_mpDTOList, isLangEng);
                    responseJsonData = new Gson().toJson(result);
                    break;
                }
                case "gender_count": {
                    long electionDetailsId = Long.parseLong(request.getParameter("electionDetailsId"));
                    election_wise_mpDTOList = Election_wise_mpRepository.getInstance().getElection_wise_mpDTOByElectionDetailsId(electionDetailsId);
                    Map<Integer, Object> result = getGenderWiseResult(electionDetailsId, election_wise_mpDTOList, isLangEng);
                    responseJsonData = new Gson().toJson(result);
                    break;
                }
                case "age_count": {
                    long electionDetailsId = Long.parseLong(request.getParameter("electionDetailsId"));
                    election_wise_mpDTOList = Election_wise_mpRepository.getInstance().getElection_wise_mpDTOByElectionDetailsId(electionDetailsId);
                    Map<Integer, Object> result = getAgeWiseResult(electionDetailsId, election_wise_mpDTOList, isLangEng);
                    responseJsonData = new Gson().toJson(result);
                    break;
                }
                default:
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    return;
            }
            response.setContentType("application/json");
            PrintWriter out = response.getWriter();
            out.print(responseJsonData);
            out.flush();
            out.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Map<String, Object> getCountAndLink(String title,long count, long electionDetailsId,String param,String paramValue) {
        Map<String, Object> countLinkMap = new HashMap<>();
        countLinkMap.put("title", title);
        countLinkMap.put("count", count);
        String link = "Mp_report_Servlet?actionType=reportPage&electionDetailsId=" + electionDetailsId;
        if(param != null && paramValue != null){
            link += "&"+param+"="+paramValue;
        }
        countLinkMap.put("link", link);
        return countLinkMap;
    }

    private Map<String, Object> getPartyCountAndLink(String title ,long count, long partyId, long electionDetailsId) {
        Map<String, Object> countLinkMap = new HashMap<>();
        countLinkMap.put("title", title);
        countLinkMap.put("count", count);
        countLinkMap.put("link", "Mp_report_Servlet?actionType=reportPage&electionDetailsId=" + electionDetailsId + "&politicalPartyId=" + partyId);
        return countLinkMap;
    }


}