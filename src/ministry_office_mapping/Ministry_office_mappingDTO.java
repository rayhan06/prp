package ministry_office_mapping;

import util.CommonDTO;


public class Ministry_office_mappingDTO extends CommonDTO
{

	public long ministryOfficeId = -1;
	public long employeeRecordsId = -1;
	public long electionDetailsId = -1;
	public long electionWiseMpId = -1;
	public int ministersCat = -1;
	public long startDate = System.currentTimeMillis();
	public long endDate = System.currentTimeMillis();
	public long insertionDate = -1;
	public long insertedBy = -1;
	public long modifiedBy = -1;
	
	
    @Override
	public String toString() {
            return "$Ministry_office_mappingDTO[" +
            " iD = " + iD +
            " ministryOfficeId = " + ministryOfficeId +
            " employeeRecordsId = " + employeeRecordsId +
            " electionDetailsId = " + electionDetailsId +
            " electionWiseMpId = " + electionWiseMpId +
            " ministersCat = " + ministersCat +
            " startDate = " + startDate +
            " endDate = " + endDate +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}