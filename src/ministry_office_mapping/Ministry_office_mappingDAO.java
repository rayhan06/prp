package ministry_office_mapping;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.*;
import java.sql.SQLException;

import committees.CommitteesRepository;
import committees_mapping.Committees_mappingDAO;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import employee_records.CommitteesInfoModel;
import employee_records.Employee_recordsRepository;
import employee_records.MinistryInfoModel;
import ministry_office.Ministry_officeRepository;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Ministry_office_mappingDAO  implements CommonDAOService<Ministry_office_mappingDTO>
{

	private static final Logger logger = Logger.getLogger(Ministry_office_mappingDAO.class);

	private static final String addQuery = "insert into {tableName} (ministry_office_id, employee_records_id, election_details_id, " +
			"election_wise_mp_id, ministers_cat, start_date, end_date, search_column, lastModificationTime, modified_by, " +
			"insertion_date, inserted_by, isDeleted, ID) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static final String updateQuery = "update {tableName} set ministry_office_id=?, employee_records_id=?, election_details_id=?, election_wise_mp_id=?, " +
			"ministers_cat=?, start_date=?, end_date=?, search_column=?, lastModificationTime=?, modified_by=?, isDeleted=? where ID=?";

	private static final String getByEmpRecIdAndElectionDetailsId = "select * from ministry_office_mapping where employee_records_id = ? and election_details_id = ?";

	private Ministry_office_mappingDAO() {
		searchMap.put("ministers_cat"," and (ministers_cat = ?)");
		searchMap.put("ministry_office_id"," and (ministry_office_id = ?)");
		searchMap.put("minister_name"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Ministry_office_mappingDAO INSTANCE = new Ministry_office_mappingDAO();
	}

	public static Ministry_office_mappingDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Ministry_office_mappingDTO ministry_office_mappingDTO)
	{
		ministry_office_mappingDTO.searchColumn = "";
		ministry_office_mappingDTO.searchColumn += Employee_recordsRepository.getInstance().getEmployeeName(ministry_office_mappingDTO.employeeRecordsId, "English");
		ministry_office_mappingDTO.searchColumn += " ";
		ministry_office_mappingDTO.searchColumn += Employee_recordsRepository.getInstance().getEmployeeName(ministry_office_mappingDTO.employeeRecordsId, "Bangla");
		ministry_office_mappingDTO.searchColumn += " ";
		ministry_office_mappingDTO.searchColumn += Ministry_officeRepository.getInstance().getText("English", ministry_office_mappingDTO.ministryOfficeId);
		ministry_office_mappingDTO.searchColumn += " ";
		ministry_office_mappingDTO.searchColumn += Ministry_officeRepository.getInstance().getText("English", ministry_office_mappingDTO.ministryOfficeId);
	}
	
	@Override
	public void set(PreparedStatement ps, Ministry_office_mappingDTO ministry_office_mappingDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();

		setSearchColumn(ministry_office_mappingDTO);

		ps.setObject(++index,ministry_office_mappingDTO.ministryOfficeId);
		ps.setObject(++index,ministry_office_mappingDTO.employeeRecordsId);
		ps.setObject(++index,ministry_office_mappingDTO.electionDetailsId);
		ps.setObject(++index,ministry_office_mappingDTO.electionWiseMpId);
		ps.setObject(++index,ministry_office_mappingDTO.ministersCat);
		ps.setObject(++index,ministry_office_mappingDTO.startDate);
		ps.setObject(++index,ministry_office_mappingDTO.endDate);
		ps.setObject(++index,ministry_office_mappingDTO.searchColumn);
		ps.setObject(++index,lastModificationTime);
		ps.setObject(++index,ministry_office_mappingDTO.modifiedBy);

		if(isInsert)
		{
			ps.setObject(++index,ministry_office_mappingDTO.insertionDate);
			ps.setObject(++index,ministry_office_mappingDTO.insertedBy);
		}

		ps.setObject(++index,ministry_office_mappingDTO.isDeleted);
		ps.setObject(++index,ministry_office_mappingDTO.iD);
	}
	
	@Override
	public Ministry_office_mappingDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Ministry_office_mappingDTO ministry_office_mappingDTO = new Ministry_office_mappingDTO();

			ministry_office_mappingDTO.iD = rs.getLong("ID");
			ministry_office_mappingDTO.ministryOfficeId = rs.getLong("ministry_office_id");
			ministry_office_mappingDTO.employeeRecordsId = rs.getLong("employee_records_id");
			ministry_office_mappingDTO.electionDetailsId = rs.getLong("election_details_id");
			ministry_office_mappingDTO.electionWiseMpId = rs.getLong("election_wise_mp_id");
			ministry_office_mappingDTO.ministersCat = rs.getInt("ministers_cat");
			ministry_office_mappingDTO.startDate = rs.getLong("start_date");
			ministry_office_mappingDTO.endDate = rs.getLong("end_date");
			ministry_office_mappingDTO.insertionDate = rs.getLong("insertion_date");
			ministry_office_mappingDTO.insertedBy = rs.getLong("inserted_by");
			ministry_office_mappingDTO.modifiedBy = rs.getLong("modified_by");
			ministry_office_mappingDTO.searchColumn = rs.getString("search_column");
			ministry_office_mappingDTO.isDeleted = rs.getInt("isDeleted");
			ministry_office_mappingDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return ministry_office_mappingDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Ministry_office_mappingDTO getDTOByID (long id) {
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "ministry_office_mapping";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Ministry_office_mappingDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Ministry_office_mappingDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }

	public static List<MinistryInfoModel> getMinistryOfficeModels(long employeeRecordsId, long electionDetailsId) {
		return ConnectionAndStatementUtil.getListOfT(getByEmpRecIdAndElectionDetailsId, ps -> {
			try {
				ps.setLong(1, employeeRecordsId);
				ps.setLong(2,electionDetailsId);
			} catch (SQLException ex) {
				logger.error(ex);
				ex.printStackTrace();
			}
		}, Ministry_office_mappingDAO::buildMinistryOfficeInfoModelFromResultSet);
	}

	private static MinistryInfoModel buildMinistryOfficeInfoModelFromResultSet(ResultSet rs) {
		MinistryInfoModel ministryInfoModel = new MinistryInfoModel();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

		try {
			ministryInfoModel.ministryNameId = rs.getLong("ministry_office_id");
			ministryInfoModel.designation = CatRepository.getName("English", "ministers", rs.getInt("ministers_cat"));
			ministryInfoModel.startDate = simpleDateFormat.format(new Date(rs.getLong("start_date")));
			ministryInfoModel.endDate = simpleDateFormat.format(new Date(rs.getLong("end_date")));

			return ministryInfoModel;
		} catch (SQLException sqlException) {
			sqlException.printStackTrace();
			return null;
		}
	}
				
}