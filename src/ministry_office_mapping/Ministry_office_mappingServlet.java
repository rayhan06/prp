package ministry_office_mapping;


import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

import committees_mapping.Committees_mappingDAO;
import committees_mapping.Committees_mappingDTO;
import committees_mapping.Committees_mappingServlet;
import election_constituency.Election_constituencyRepository;
import election_wise_mp.Election_wise_mpDTO;
import election_wise_mp.Election_wise_mpRepository;
import employee_records.Employee_recordsRepository;
import login.LoginDTO;
import org.apache.log4j.Logger;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;
import javax.servlet.http.*;
import java.util.*;
import language.LC;
import language.LM;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Ministry_office_mappingServlet
 */
@WebServlet("/Ministry_office_mappingServlet")
@MultipartConfig
public class Ministry_office_mappingServlet extends BaseServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Ministry_office_mappingServlet.class);

    @Override
    public String getTableName() {
        return Ministry_office_mappingDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Ministry_office_mappingServlet";
    }

    @Override
    public Ministry_office_mappingDAO getCommonDAOService() {
        return Ministry_office_mappingDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.MINISTRY_OFFICE_MAPPING_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.MINISTRY_OFFICE_MAPPING_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.MINISTRY_OFFICE_MAPPING_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Ministry_office_mappingServlet.class;
    }


	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception
	{
		// TODO Auto-generated method stub
		try
		{
			String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
			Ministry_office_mappingDTO ministry_office_mappingDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
			request.setAttribute("failureMessage", "");
			String Value = "";

			if(addFlag)
				ministry_office_mappingDTO = new Ministry_office_mappingDTO();
			else
				ministry_office_mappingDTO = Ministry_office_mappingDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));



			Value = request.getParameter("ministryOfficeId");
			if(Value != null && !Value.equalsIgnoreCase("")) {
				Value = Jsoup.clean(Value,Whitelist.simpleText());
				ministry_office_mappingDTO.ministryOfficeId = Long.parseLong(Value);
			} else {
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}


			Value = request.getParameter("employeeRecordsId");
			if(Value != null && !Value.equalsIgnoreCase("")) {
				Value = Jsoup.clean(Value,Whitelist.simpleText());
				ministry_office_mappingDTO.employeeRecordsId = Long.parseLong(Value);
			} else {
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}


			Value = request.getParameter("electionDetailsId");
			if(Value != null && !Value.equalsIgnoreCase("")) {
				Value = Jsoup.clean(Value,Whitelist.simpleText());
				ministry_office_mappingDTO.electionDetailsId = Long.parseLong(Value);
			} else {
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}


			Value = request.getParameter("electionWiseMpId");
			if(Value != null && !Value.equalsIgnoreCase("") && !Value.equalsIgnoreCase("-1")) {
				Value = Jsoup.clean(Value,Whitelist.simpleText());
				ministry_office_mappingDTO.electionWiseMpId = Long.parseLong(Value);
			} else {
				try {
					ministry_office_mappingDTO.electionWiseMpId = Election_wise_mpRepository.getInstance().getByElectionAndRecordsId(ministry_office_mappingDTO.electionDetailsId, ministry_office_mappingDTO.employeeRecordsId).iD;
				} catch (Exception ex) {
					logger.debug("Technocrat Minister!");
					ministry_office_mappingDTO.electionWiseMpId = -1;
				}
			}


			Value = request.getParameter("ministersCat");
			if(Value != null && !Value.equalsIgnoreCase("")) {
				Value = Jsoup.clean(Value,Whitelist.simpleText());
				ministry_office_mappingDTO.ministersCat = Integer.parseInt(Value);
			} else {
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}


			Value = request.getParameter("startDate");
			if(Value != null && !Value.equalsIgnoreCase("")) {
				Value = Jsoup.clean(Value,Whitelist.simpleText());
				try
				{
					Date d = f.parse(Value);
					ministry_office_mappingDTO.startDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw new Exception(LM.getText(LC.MINISTRY_OFFICE_MAPPING_ADD_STARTDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
				}
			} else {
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}


			Value = request.getParameter("endDate");
			if(Value != null && !Value.equalsIgnoreCase("")) {
				Value = Jsoup.clean(Value,Whitelist.simpleText());
				try
				{
					Date d = f.parse(Value);
					ministry_office_mappingDTO.endDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw new Exception(LM.getText(LC.MINISTRY_OFFICE_MAPPING_ADD_ENDDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
				}
			}
			else {
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				ministry_office_mappingDTO.insertionDate = TimeConverter.getToday();
				ministry_office_mappingDTO.insertedBy = userDTO.employee_record_id;
			}

			ministry_office_mappingDTO.modifiedBy = userDTO.employee_record_id;

			long returnedID = -1;


			if(addFlag == true)
			{
				returnedID = Ministry_office_mappingDAO.getInstance().add(ministry_office_mappingDTO);
			}
			else
			{
				returnedID = Ministry_office_mappingDAO.getInstance().update(ministry_office_mappingDTO);
			}


			return ministry_office_mappingDTO;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		if (userDTO == null) {
			request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			return;
		}
		try {
			String actionType = request.getParameter("actionType");
			long electionDetailsId;
			long electionConstituencyId;
			String options;
			String value;
			Map<String, Object> res;
			Election_wise_mpDTO election_wise_mpDTO;
			String employeeName;

			String language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
			switch (actionType) {
				case "getElectionConstituency":
					electionDetailsId = Long.parseLong(request.getParameter("electionDetailsId"));
					value = request.getParameter("electionConstituencyId");
					if (value != null && !value.equals("")) {
						electionConstituencyId = Long.parseLong(value);
					} else {
						electionConstituencyId = 0L;
					}
					options = Election_constituencyRepository.getInstance().buildOptions(language, electionConstituencyId, electionDetailsId);
					response.setContentType("text/html; charset=UTF-8");
					response.getWriter().println(options);
					return;
				case "getActiveMP":
					electionDetailsId = Long.parseLong(request.getParameter("electionDetailsId"));
					electionConstituencyId = Long.parseLong(request.getParameter("electionConstituencyId"));

					res = new HashMap<>();
					election_wise_mpDTO = Election_wise_mpRepository.getInstance().getByElectionAndConstituency(electionDetailsId, electionConstituencyId);
					employeeName = Employee_recordsRepository.getInstance().getEmployeeName(election_wise_mpDTO.employeeRecordsId, language);
					res.put("employeeName", employeeName);
					res.put("employeeRecordsId", election_wise_mpDTO.employeeRecordsId);
					res.put("electionWiseMpId", election_wise_mpDTO.iD);
					response.setContentType("application/json");
					response.setCharacterEncoding("UTF-8");
					response.getWriter().println(gson.toJson(res));
					return;
				case "getEditPageRecord":
				    long employeeRecordsId = Long.parseLong(request.getParameter("employeeRecordsId"));
					electionDetailsId = Long.parseLong(request.getParameter("electionDetailsId"));

					options = Committees_mappingDAO.buildOptions(language, employeeRecordsId, electionDetailsId);

					response.setContentType("text/html; charset=UTF-8");
					response.getWriter().println(options);
					return;
				default:
					super.doGet(request, response);
					return;
			}
		} catch (Exception e) {
			logger.error(e);
		}
		request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
	}
}

