package ministry_office_mapping;

import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@SuppressWarnings({"unused"})
public class Ministry_office_mappingRepository implements Repository {
    private final Ministry_office_mappingDAO ministry_office_mappingDAO;

    private static final Logger logger = Logger.getLogger(Ministry_office_mappingRepository.class);

    private final Map<Long, Ministry_office_mappingDTO> mapById;
    private final Map<Long, List<Ministry_office_mappingDTO>> mapByEmployeeRecordId;

    private Ministry_office_mappingRepository() {
        mapById = new ConcurrentHashMap<>();
        mapByEmployeeRecordId = new ConcurrentHashMap<>();

        ministry_office_mappingDAO = Ministry_office_mappingDAO.getInstance();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        static Ministry_office_mappingRepository INSTANCE = new Ministry_office_mappingRepository();
    }

    public synchronized static Ministry_office_mappingRepository getInstance() {
        return Ministry_office_mappingRepository.LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        logger.debug("Ministry_office_mappingRepository reload start reloadAll : "+reloadAll);
        List<Ministry_office_mappingDTO> list = ministry_office_mappingDAO.getAllDTOs(reloadAll);
        list.stream()
                .peek(this::removeIfPresent)
                .filter(dto -> dto.isDeleted == 0)
                .forEach(this::addToMaps);
        logger.debug("Ministry_office_mappingRepository reload end reloadAll : "+reloadAll);
    }

    private void addToMaps(Ministry_office_mappingDTO dto){
        mapById.put(dto.iD, dto);
        List<Ministry_office_mappingDTO> list = mapByEmployeeRecordId.getOrDefault(dto.employeeRecordsId,new ArrayList<>());
        list.add(dto);
        mapByEmployeeRecordId.put(dto.employeeRecordsId,list);
    }

    private void removeIfPresent(Ministry_office_mappingDTO dto) {
        Ministry_office_mappingDTO oldDTO = mapById.get(dto.iD);
        if(oldDTO!=null){
            mapById.remove(dto.iD);
            mapByEmployeeRecordId.get(oldDTO.employeeRecordsId).remove(oldDTO);
        }
    }

    public List<Ministry_office_mappingDTO> getMinistry_office_mappingDTOList() {
        return new ArrayList<>(this.mapById.values());
    }

    public Ministry_office_mappingDTO getMinistry_office_mappingDTOByID(long ID) {
        if(mapById.get(ID) == null){
            Ministry_office_mappingDTO dto = ministry_office_mappingDAO.getDTOFromID(ID);
            if(dto!=null){
                addToMaps(dto);
            }
            return dto;
        }
        return mapById.get(ID);
    }

    public List<Ministry_office_mappingDTO> getMinistry_office_mappingDTOsByEmployeeRecordId(long employeeRecordId) {
        List<Ministry_office_mappingDTO> ministry_office_mappingDTOS = mapByEmployeeRecordId.get(employeeRecordId);

        if (ministry_office_mappingDTOS==null)
            return new ArrayList<>();
        else
            return ministry_office_mappingDTOS;
    }


    @Override
    public String getTableName() {
        return "ministry_office_mapping";
    }
}



