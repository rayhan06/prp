package medical_inventory_lot;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;
import java.util.*;
import javax.servlet.http.*;
import com.google.gson.Gson;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Medical_inventory_lotServlet
 */
@WebServlet("/Medical_inventory_lotServlet")
@MultipartConfig
public class Medical_inventory_lotServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Medical_inventory_lotServlet.class);

    String tableName = "medical_inventory_lot";

	Medical_inventory_lotDAO medical_inventory_lotDAO;
	CommonRequestHandler commonRequestHandler;
	MedicalInventoryInDAO medicalInventoryInDAO;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Medical_inventory_lotServlet() 
	{
        super();
    	try
    	{
			medical_inventory_lotDAO = new Medical_inventory_lotDAO(tableName);
			medicalInventoryInDAO = new MedicalInventoryInDAO("medical_inventory_in");
			commonRequestHandler = new CommonRequestHandler(medical_inventory_lotDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_INVENTORY_LOT_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_INVENTORY_LOT_UPDATE))
				{
					getMedical_inventory_lot(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("finalize"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_INVENTORY_LOT_UPDATE))
				{
					finalize(request, response, userDTO);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_INVENTORY_LOT_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchMedical_inventory_lot(request, response, isPermanentTable, filter);
						}
						else
						{
							searchMedical_inventory_lot(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchMedical_inventory_lot(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_INVENTORY_LOT_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	private void finalize(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception {
		
		long id = Long.parseLong(request.getParameter("ID"));
		Medical_inventory_lotDTO medical_inventory_lotDTO = medical_inventory_lotDAO.getDTOByID(id);
		if(medical_inventory_lotDTO == null)
		{
			response.sendRedirect("Medical_inventory_lotServlet?actionType=search");
			return;
		}
		
		List<MedicalInventoryInDTO> medicalInventoryInDTOs = medicalInventoryInDAO.getMedicalInventoryInDTOListByMedicalInventoryLotID(id);
		for(MedicalInventoryInDTO medicalInventoryInDTO: medicalInventoryInDTOs)
		{
			medicalInventoryInDAO.insertStock(medicalInventoryInDTO);
		}
		
		medical_inventory_lotDTO.isFinal = true;
		medical_inventory_lotDAO.update(medical_inventory_lotDTO);
		
		 
		
		try {
			response.sendRedirect("Medical_inventory_lotServlet?actionType=search");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_INVENTORY_LOT_ADD))
				{
					System.out.println("going to  addMedical_inventory_lot ");
					addMedical_inventory_lot(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addMedical_inventory_lot ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if (actionType.equals("getEdate")) {
				

				long drugInfoId = Long.parseLong(request.getParameter("drugInfoId"));
				List<MedicalInventoryInDTO> medicalInventoryInDTOs = medicalInventoryInDAO.getMedicalInventoryInDTOListByDrugInfoId(drugInfoId);
				String options = "<option value='-1'>Select</option>";
				for(MedicalInventoryInDTO medicalInventoryInDTO: medicalInventoryInDTOs)
				{
					options += "<option "
							+ "stock = '" + medicalInventoryInDTO.remainingStock + "' "
							+ "value = '" + medicalInventoryInDTO.iD + "' "
							+ "edate='" + medicalInventoryInDTO.stockLotExpiryDate + "' >";
					options += f.format(new Date(medicalInventoryInDTO.stockLotExpiryDate));
					options += ", " + medicalInventoryInDTO.remainingStock + " pieces in stock";
					options += "</option>";
				}
				
				response.getWriter().write(options);
				
			}
			
			else if (actionType.equals("getEdateList")) 
			{
				

				long drugInfoId = Long.parseLong(request.getParameter("drugInfoId"));
				List<MedicalInventoryInDTO> medicalInventoryInDTOs = medicalInventoryInDAO.getMedicalInventoryInDTOListByDrugInfoId(drugInfoId);
			
				PrintWriter out = response.getWriter();
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				
				String encoded = this.gson.toJson(medicalInventoryInDTOs);
				System.out.println("json encoded data = " + encoded);
				out.print(encoded);
				out.flush();
				
			}
			
			else if (actionType.equals("getReagentEdateList")) 
			{
				

				long reagentId = Long.parseLong(request.getParameter("reagentId"));
				List<MedicalInventoryInDTO> medicalInventoryInDTOs = medicalInventoryInDAO.getMedicalInventoryInDTOListByReagentId(reagentId);
			
				PrintWriter out = response.getWriter();
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				
				String encoded = this.gson.toJson(medicalInventoryInDTOs);
				System.out.println("json encoded data = " + encoded);
				out.print(encoded);
				out.flush();
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_INVENTORY_LOT_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addMedical_inventory_lot ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_INVENTORY_LOT_UPDATE))
				{					
					addMedical_inventory_lot(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				/*if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_INVENTORY_LOT_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}*/
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_INVENTORY_LOT_SEARCH))
				{
					searchMedical_inventory_lot(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Medical_inventory_lotDTO medical_inventory_lotDTO = medical_inventory_lotDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(medical_inventory_lotDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addMedical_inventory_lot(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addMedical_inventory_lot");
			String path = getServletContext().getRealPath("/img2/");
			Medical_inventory_lotDTO medical_inventory_lotDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				medical_inventory_lotDTO = new Medical_inventory_lotDTO();
			}
			else
			{
				medical_inventory_lotDTO = medical_inventory_lotDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("parliamentSupplierType");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("parliamentSupplierType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				medical_inventory_lotDTO.parliamentSupplierType = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("relatedPoId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("relatedPoId = " + Value);
			if(Value != null)
			{
				medical_inventory_lotDTO.relatedPoId = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("stockLotNumber");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("stockLotNumber = " + Value);
			if(Value != null)
			{
				medical_inventory_lotDTO.stockLotNumber = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("remarks");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("remarks = " + Value);
			if(Value != null)
			{
				medical_inventory_lotDTO.remarks = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				medical_inventory_lotDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				medical_inventory_lotDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				medical_inventory_lotDTO.insertionDate = c.getTimeInMillis();
			}			


			System.out.println("Done adding  addMedical_inventory_lot dto = " + medical_inventory_lotDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				medical_inventory_lotDAO.setIsDeleted(medical_inventory_lotDTO.iD, CommonDTO.OUTDATED);
				returnedID = medical_inventory_lotDAO.add(medical_inventory_lotDTO);
				medical_inventory_lotDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = medical_inventory_lotDAO.manageWriteOperations(medical_inventory_lotDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = medical_inventory_lotDAO.manageWriteOperations(medical_inventory_lotDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			List<MedicalInventoryInDTO> medicalInventoryInDTOList = createMedicalInventoryInDTOListByRequest(request, userDTO);			
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(medicalInventoryInDTOList != null)
				{				
					for(MedicalInventoryInDTO medicalInventoryInDTO: medicalInventoryInDTOList)
					{
						medicalInventoryInDTO.medicalInventoryLotId = medical_inventory_lotDTO.iD; 
						
						medicalInventoryInDTO.insertedByUserId = userDTO.ID;						
						medicalInventoryInDTO.insertedByOrganogramId = userDTO.organogramID;
						medicalInventoryInDTO.insertionDate = medical_inventory_lotDTO.insertionDate;
						medicalInventoryInDTO.transactionDate = medicalInventoryInDTO.insertionDate;
						medicalInventoryInDTO.employeeRecordId = userDTO.employee_record_id;	
						medicalInventoryInDAO.add(medicalInventoryInDTO, false);
					}
				}
			
			}
			else
			{
				List<Long> childIdsFromRequest = medicalInventoryInDAO.getChildIdsFromRequest(request, "medicalInventoryIn");
				//delete the removed children
				medicalInventoryInDAO.deleteChildrenNotInList("medical_inventory_lot", "medical_inventory_in", medical_inventory_lotDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = medicalInventoryInDAO.getChilIds("medical_inventory_lot", "medical_inventory_in", medical_inventory_lotDTO.iD);
				
				
				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							MedicalInventoryInDTO medicalInventoryInDTO =  createMedicalInventoryInDTOByRequestAndIndex(request, false, i, userDTO);
							medicalInventoryInDTO.medicalInventoryLotId = medical_inventory_lotDTO.iD; 
							
							medicalInventoryInDAO.update(medicalInventoryInDTO);
						}
						else
						{
							MedicalInventoryInDTO medicalInventoryInDTO =  createMedicalInventoryInDTOByRequestAndIndex(request, true, i, userDTO);
							medicalInventoryInDTO.medicalInventoryLotId = medical_inventory_lotDTO.iD; 
							medicalInventoryInDTO.insertedByUserId = userDTO.ID;						
							medicalInventoryInDTO.insertedByOrganogramId = userDTO.organogramID;
							medicalInventoryInDTO.insertionDate = medical_inventory_lotDTO.insertionDate;
							medicalInventoryInDTO.employeeRecordId = userDTO.employee_record_id;
							medicalInventoryInDAO.add(medicalInventoryInDTO, false);
						}
					}
				}
				else
				{
					medicalInventoryInDAO.deleteChildrenByParent(medical_inventory_lotDTO.iD, "medical_inventory_lot_id");
				}
				
			}
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getMedical_inventory_lot(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Medical_inventory_lotServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(medical_inventory_lotDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	private List<MedicalInventoryInDTO> createMedicalInventoryInDTOListByRequest(HttpServletRequest request, UserDTO userDTO) throws Exception{ 
		List<MedicalInventoryInDTO> medicalInventoryInDTOList = new ArrayList<MedicalInventoryInDTO>();
		if(request.getParameterValues("medicalInventoryIn.iD") != null) 
		{
			int medicalInventoryInItemNo = request.getParameterValues("medicalInventoryIn.iD").length;
			
			
			for(int index=0;index<medicalInventoryInItemNo;index++){
				MedicalInventoryInDTO medicalInventoryInDTO = createMedicalInventoryInDTOByRequestAndIndex(request,true,index, userDTO);
				if(medicalInventoryInDTO != null)
				{
					medicalInventoryInDTOList.add(medicalInventoryInDTO);
				}
				
			}
			
			return medicalInventoryInDTOList;
		}
		return null;
	}
	
	
	private MedicalInventoryInDTO createMedicalInventoryInDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index, UserDTO userDTO) throws Exception{
	
		MedicalInventoryInDTO medicalInventoryInDTO;
		if(addFlag == true )
		{
			medicalInventoryInDTO = new MedicalInventoryInDTO();
		}
		else
		{
			medicalInventoryInDTO = medicalInventoryInDAO.getDTOByID(Long.parseLong(request.getParameterValues("medicalInventoryIn.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		
				
		String Value = "";
		Value = request.getParameterValues("medicalInventoryIn.medicalInventoryLotId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		medicalInventoryInDTO.medicalInventoryLotId = Long.parseLong(Value);
		
		/*Value = request.getParameterValues("medicalInventoryIn.medicalDeptCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			medicalInventoryInDTO.medicalDeptCat = Integer.parseInt(Value);
		}*/

		
		Value = request.getParameterValues("medicalInventoryIn.medicalItemCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		medicalInventoryInDTO.medicalItemCat = Long.parseLong(Value);
		Value = request.getParameterValues("medicalInventoryIn.drugInformationType")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		medicalInventoryInDTO.drugInformationType = Long.parseLong(Value);
		Value = request.getParameterValues("medicalInventoryIn.medicineGenericNameId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		medicalInventoryInDTO.medicineGenericNameId = Long.parseLong(Value);
		Value = request.getParameterValues("medicalInventoryIn.pharmaCompanyNameId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		medicalInventoryInDTO.pharmaCompanyNameId = Long.parseLong(Value);
		Value = request.getParameterValues("medicalInventoryIn.medicalReagentNameType")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		medicalInventoryInDTO.medicalReagentNameType = Long.parseLong(Value);
		Value = request.getParameterValues("medicalInventoryIn.medicalEquipmentNameType")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		medicalInventoryInDTO.medicalEquipmentNameType = Long.parseLong(Value);
		Value = request.getParameterValues("medicalInventoryIn.stockInQuantity")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		if(!Value.equalsIgnoreCase(""))
		{
			medicalInventoryInDTO.stockInQuantity = Integer.parseInt(Value);
		}
		else
		{
			return null;
		}
		if(userDTO.roleID != SessionConstants.ADMIN_ROLE && medicalInventoryInDTO.stockInQuantity < 0)
		{
			medicalInventoryInDTO.stockInQuantity = 0;
		}

		Value = request.getParameterValues("medicalInventoryIn.unitPrice")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		if(!Value.equalsIgnoreCase(""))
		{
			medicalInventoryInDTO.unitPrice = Double.parseDouble(Value);
		}
		
		Value = request.getParameterValues("medicalInventoryIn.scanCode")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		medicalInventoryInDTO.scanCode = (Value);
		Value = request.getParameterValues("medicalInventoryIn.supplyDate")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		try 
		{
			Date d = f.parse(Value);
			medicalInventoryInDTO.supplyDate = d.getTime();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		Value = request.getParameterValues("medicalInventoryIn.stockLotExpiryDate")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		try 
		{
			Date d = f.parse(Value);
			medicalInventoryInDTO.stockLotExpiryDate = d.getTime();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		Value = request.getParameterValues("medicalInventoryIn.insertedByUserId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		medicalInventoryInDTO.insertedByUserId = userDTO.ID;
		medicalInventoryInDTO.insertedByOrganogramId = userDTO.organogramID;
		medicalInventoryInDTO.userName = userDTO.userName;
		medicalInventoryInDTO.organogramId = userDTO.organogramID;

		medicalInventoryInDTO.insertionDate = Long.parseLong(Value);
		medicalInventoryInDTO.transactionType = MedicalInventoryInDTO.TR_STOCK_IN;
		return medicalInventoryInDTO;
	
	}
	
	
	

	private void getMedical_inventory_lot(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getMedical_inventory_lot");
		Medical_inventory_lotDTO medical_inventory_lotDTO = null;
		try 
		{
			medical_inventory_lotDTO = medical_inventory_lotDAO.getDTOByID(id);
			request.setAttribute("ID", medical_inventory_lotDTO.iD);
			request.setAttribute("medical_inventory_lotDTO",medical_inventory_lotDTO);
			request.setAttribute("medical_inventory_lotDAO",medical_inventory_lotDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "medical_inventory_lot/medical_inventory_lotInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "medical_inventory_lot/medical_inventory_lotSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "medical_inventory_lot/medical_inventory_lotEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "medical_inventory_lot/medical_inventory_lotEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getMedical_inventory_lot(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getMedical_inventory_lot(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchMedical_inventory_lot(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchMedical_inventory_lot 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_MEDICAL_INVENTORY_LOT,
			request,
			medical_inventory_lotDAO,
			SessionConstants.VIEW_MEDICAL_INVENTORY_LOT,
			SessionConstants.SEARCH_MEDICAL_INVENTORY_LOT,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("medical_inventory_lotDAO",medical_inventory_lotDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to medical_inventory_lot/medical_inventory_lotApproval.jsp");
	        	rd = request.getRequestDispatcher("medical_inventory_lot/medical_inventory_lotApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to medical_inventory_lot/medical_inventory_lotApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("medical_inventory_lot/medical_inventory_lotApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to medical_inventory_lot/medical_inventory_lotSearch.jsp");
	        	rd = request.getRequestDispatcher("medical_inventory_lot/medical_inventory_lotSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to medical_inventory_lot/medical_inventory_lotSearchForm.jsp");
	        	rd = request.getRequestDispatcher("medical_inventory_lot/medical_inventory_lotSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

