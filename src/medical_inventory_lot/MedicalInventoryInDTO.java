package medical_inventory_lot;
import java.util.*;

import deliver_or_return_medicine.DeliverOrReturnDetailsDTO;
import drug_information.Drug_informationDAO;
import drug_information.Drug_informationDTO;
import medical_equipment_name.Medical_equipment_nameDAO;
import medical_equipment_name.Medical_equipment_nameDTO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*; 


public class MedicalInventoryInDTO extends CommonDTO
{
	
	public static final int TR_STOCK_IN = 0;
	public static final int TR_RECEIVE_MEDICINE = 1;
	public static final int TR_BORROW = 2;
	public static final int TR_RETURN = 3;
	public static final int TR_ASSIGN = 4;
	public static final int TR_UNASSIGN = 5;
	public static final int TR_DELIVER_MEDICINE = 6;
	public static final int TR_DELIVER_EQUIPMENT = 8;
	public static final int TR_BORROW_EQUIPMENT = 9;
	//public static final int TR_MANUALLY_RECEIVE_MEDICINE = 7;

	public long medicalInventoryLotId = -1;
	public int departmentCat = -1;
	public int medicalDeptCat = -1;
	public long medicalItemCat = -1;
	public long drugInformationType = -1;
	public long medicineGenericNameId = -1;
	public long pharmaCompanyNameId = -1;
	public long medicalReagentNameType = -1;
	public long medicalEquipmentNameType = -1;
	public int stockInQuantity = 0;
	public int remainingStock = 0;
	public double unitPrice = 0;
    public String scanCode = "";
	public long supplyDate = System.currentTimeMillis();
	public long stockLotExpiryDate = System.currentTimeMillis();
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = 0;
	public long transactionDate = 0;
	public String expiryDateStr = "";
	public long organogramId = -1;
	public String userName = "";
	public long transactionTime = System.currentTimeMillis();
	
	public long employeeRecordId = -1;
	
	public int transactionType = TR_STOCK_IN;
	public long otherOfficeId = -1;
	
	MedicalInventoryInDAO medicalInventoryInDAO = new MedicalInventoryInDAO();
	Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
	Medical_equipment_nameDAO medical_equipment_nameDAO = new Medical_equipment_nameDAO();
	
	public MedicalInventoryInDTO() {}
	
	public MedicalInventoryInDTO(DeliverOrReturnDetailsDTO deliverOrReturnDetailsDTO,
			UserDTO userDTO, UserDTO transactionToDTO, int medicalItemType)
	{
		this(deliverOrReturnDetailsDTO,
				userDTO, transactionToDTO, medicalItemType, TimeConverter.getToday());
	}
	
	public MedicalInventoryInDTO(DeliverOrReturnDetailsDTO deliverOrReturnDetailsDTO,
			UserDTO userDTO, UserDTO transactionToDTO, int medicalItemType, long transactionDate)
	{
		medicalItemCat = medicalItemType;
		if(deliverOrReturnDetailsDTO.isDelivery)
		{
			transactionType = TR_DELIVER_MEDICINE;
		}
		else
		{
			transactionType = TR_RETURN;
		}
		stockInQuantity = deliverOrReturnDetailsDTO.quantity;
		organogramId = deliverOrReturnDetailsDTO.doctorId;
		userName = deliverOrReturnDetailsDTO.doctorUserName;
		otherOfficeId = deliverOrReturnDetailsDTO.otherOfficeId;
		if(medicalItemCat == SessionConstants.MEDICAL_ITEM_DRUG)
		{
			drugInformationType = deliverOrReturnDetailsDTO.drugInformationType;
			medicineGenericNameId = deliverOrReturnDetailsDTO.medicineGenericNameId;
			
			Drug_informationDTO drug_informationDTO = drug_informationDAO.getDTOByID(drugInformationType);
			if(drug_informationDTO != null)
			{
				unitPrice = drug_informationDTO.unitPrice;
			}
		}
		else if(medicalItemCat == SessionConstants.MEDICAL_ITEM_EQUIPMENT)
		{
			medicalEquipmentNameType = deliverOrReturnDetailsDTO.drugInformationType;
			Medical_equipment_nameDTO medical_equipment_nameDTO = medical_equipment_nameDAO.getDTOByID(medicalEquipmentNameType);
			if(medical_equipment_nameDTO != null)
			{
				unitPrice = medical_equipment_nameDTO.unitPrice;
			}
		}
		
		
		insertionDate = TimeConverter.getToday();
		this.transactionDate = transactionDate;
		if(userDTO != null)
		{
			insertedByUserId = userDTO.ID;
			insertedByOrganogramId = userDTO.organogramID;
			
		}
		
		if(transactionToDTO != null)
		{
			userName = transactionToDTO.userName;
			organogramId = transactionToDTO.organogramID;
			employeeRecordId = transactionToDTO.employee_record_id;
		}
	}
	
	public MedicalInventoryInDTO(int medicalDeptCat, Drug_informationDTO drug_informationDTO, int quantity, UserDTO userDTO, UserDTO transactionToDTO)
	{
		insertionDate = TimeConverter.getToday();
		transactionDate = insertionDate;
		if(userDTO != null)
		{
			insertedByUserId = userDTO.ID;
			insertedByOrganogramId = userDTO.organogramID;
			
		}
		
		if(transactionToDTO != null)
		{
			userName = transactionToDTO.userName;
			organogramId = transactionToDTO.organogramID;
			employeeRecordId = transactionToDTO.employee_record_id;
		}
		
		drugInformationType = drug_informationDTO.iD;
		medicineGenericNameId = drug_informationDTO.medicineGenericNameType;
		unitPrice = drug_informationDTO.unitPrice;
		this.medicalDeptCat = medicalDeptCat;
		stockInQuantity = quantity;
		transactionType = TR_DELIVER_MEDICINE;
		medicalItemCat = SessionConstants.MEDICAL_ITEM_DRUG;
	}
	

	
	
	
    @Override
	public String toString() {
            return "$MedicalInventoryInDTO[" +
            " iD = " + iD +
            " medicalInventoryLotId = " + medicalInventoryLotId +
            " departmentCat = " + departmentCat +
            " medicalItemCat = " + medicalItemCat +
            " drugInformationType = " + drugInformationType +
            " medicineGenericNameId = " + medicineGenericNameId +
            " pharmaCompanyNameId = " + pharmaCompanyNameId +
            " medicalReagentNameType = " + medicalReagentNameType +
            " medicalEquipmentNameType = " + medicalEquipmentNameType +
            " stockInQuantity = " + stockInQuantity +
            " unitPrice = " + unitPrice +
            " scanCode = " + scanCode +
            " supplyDate = " + supplyDate +
            " stockLotExpiryDate = " + stockLotExpiryDate +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}