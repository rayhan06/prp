package medical_inventory_lot;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Medical_inventory_lotRepository implements Repository {
	Medical_inventory_lotDAO medical_inventory_lotDAO = null;
	
	public void setDAO(Medical_inventory_lotDAO medical_inventory_lotDAO)
	{
		this.medical_inventory_lotDAO = medical_inventory_lotDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Medical_inventory_lotRepository.class);
	Map<Long, Medical_inventory_lotDTO>mapOfMedical_inventory_lotDTOToiD;
	Map<Long, Set<Medical_inventory_lotDTO> >mapOfMedical_inventory_lotDTOToparliamentSupplierType;
	Map<String, Set<Medical_inventory_lotDTO> >mapOfMedical_inventory_lotDTOTorelatedPoId;
	Map<String, Set<Medical_inventory_lotDTO> >mapOfMedical_inventory_lotDTOTostockLotNumber;
	Map<String, Set<Medical_inventory_lotDTO> >mapOfMedical_inventory_lotDTOToremarks;
	Map<Long, Set<Medical_inventory_lotDTO> >mapOfMedical_inventory_lotDTOToinsertedByUserId;
	Map<Long, Set<Medical_inventory_lotDTO> >mapOfMedical_inventory_lotDTOToinsertedByOrganogramId;
	Map<Long, Set<Medical_inventory_lotDTO> >mapOfMedical_inventory_lotDTOToinsertionDate;
	Map<String, Set<Medical_inventory_lotDTO> >mapOfMedical_inventory_lotDTOTosearchColumn;
	Map<Long, Set<Medical_inventory_lotDTO> >mapOfMedical_inventory_lotDTOTolastModificationTime;


	static Medical_inventory_lotRepository instance = null;  
	private Medical_inventory_lotRepository(){
		mapOfMedical_inventory_lotDTOToiD = new ConcurrentHashMap<>();
		mapOfMedical_inventory_lotDTOToparliamentSupplierType = new ConcurrentHashMap<>();
		mapOfMedical_inventory_lotDTOTorelatedPoId = new ConcurrentHashMap<>();
		mapOfMedical_inventory_lotDTOTostockLotNumber = new ConcurrentHashMap<>();
		mapOfMedical_inventory_lotDTOToremarks = new ConcurrentHashMap<>();
		mapOfMedical_inventory_lotDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfMedical_inventory_lotDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfMedical_inventory_lotDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfMedical_inventory_lotDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfMedical_inventory_lotDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Medical_inventory_lotRepository getInstance(){
		if (instance == null){
			instance = new Medical_inventory_lotRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(medical_inventory_lotDAO == null)
		{
			return;
		}
		try {
			List<Medical_inventory_lotDTO> medical_inventory_lotDTOs = medical_inventory_lotDAO.getAllMedical_inventory_lot(reloadAll);
			for(Medical_inventory_lotDTO medical_inventory_lotDTO : medical_inventory_lotDTOs) {
				Medical_inventory_lotDTO oldMedical_inventory_lotDTO = getMedical_inventory_lotDTOByID(medical_inventory_lotDTO.iD);
				if( oldMedical_inventory_lotDTO != null ) {
					mapOfMedical_inventory_lotDTOToiD.remove(oldMedical_inventory_lotDTO.iD);
				
					if(mapOfMedical_inventory_lotDTOToparliamentSupplierType.containsKey(oldMedical_inventory_lotDTO.parliamentSupplierType)) {
						mapOfMedical_inventory_lotDTOToparliamentSupplierType.get(oldMedical_inventory_lotDTO.parliamentSupplierType).remove(oldMedical_inventory_lotDTO);
					}
					if(mapOfMedical_inventory_lotDTOToparliamentSupplierType.get(oldMedical_inventory_lotDTO.parliamentSupplierType).isEmpty()) {
						mapOfMedical_inventory_lotDTOToparliamentSupplierType.remove(oldMedical_inventory_lotDTO.parliamentSupplierType);
					}
					
					if(mapOfMedical_inventory_lotDTOTorelatedPoId.containsKey(oldMedical_inventory_lotDTO.relatedPoId)) {
						mapOfMedical_inventory_lotDTOTorelatedPoId.get(oldMedical_inventory_lotDTO.relatedPoId).remove(oldMedical_inventory_lotDTO);
					}
					if(mapOfMedical_inventory_lotDTOTorelatedPoId.get(oldMedical_inventory_lotDTO.relatedPoId).isEmpty()) {
						mapOfMedical_inventory_lotDTOTorelatedPoId.remove(oldMedical_inventory_lotDTO.relatedPoId);
					}
					
					if(mapOfMedical_inventory_lotDTOTostockLotNumber.containsKey(oldMedical_inventory_lotDTO.stockLotNumber)) {
						mapOfMedical_inventory_lotDTOTostockLotNumber.get(oldMedical_inventory_lotDTO.stockLotNumber).remove(oldMedical_inventory_lotDTO);
					}
					if(mapOfMedical_inventory_lotDTOTostockLotNumber.get(oldMedical_inventory_lotDTO.stockLotNumber).isEmpty()) {
						mapOfMedical_inventory_lotDTOTostockLotNumber.remove(oldMedical_inventory_lotDTO.stockLotNumber);
					}
					
					if(mapOfMedical_inventory_lotDTOToremarks.containsKey(oldMedical_inventory_lotDTO.remarks)) {
						mapOfMedical_inventory_lotDTOToremarks.get(oldMedical_inventory_lotDTO.remarks).remove(oldMedical_inventory_lotDTO);
					}
					if(mapOfMedical_inventory_lotDTOToremarks.get(oldMedical_inventory_lotDTO.remarks).isEmpty()) {
						mapOfMedical_inventory_lotDTOToremarks.remove(oldMedical_inventory_lotDTO.remarks);
					}
					
					if(mapOfMedical_inventory_lotDTOToinsertedByUserId.containsKey(oldMedical_inventory_lotDTO.insertedByUserId)) {
						mapOfMedical_inventory_lotDTOToinsertedByUserId.get(oldMedical_inventory_lotDTO.insertedByUserId).remove(oldMedical_inventory_lotDTO);
					}
					if(mapOfMedical_inventory_lotDTOToinsertedByUserId.get(oldMedical_inventory_lotDTO.insertedByUserId).isEmpty()) {
						mapOfMedical_inventory_lotDTOToinsertedByUserId.remove(oldMedical_inventory_lotDTO.insertedByUserId);
					}
					
					if(mapOfMedical_inventory_lotDTOToinsertedByOrganogramId.containsKey(oldMedical_inventory_lotDTO.insertedByOrganogramId)) {
						mapOfMedical_inventory_lotDTOToinsertedByOrganogramId.get(oldMedical_inventory_lotDTO.insertedByOrganogramId).remove(oldMedical_inventory_lotDTO);
					}
					if(mapOfMedical_inventory_lotDTOToinsertedByOrganogramId.get(oldMedical_inventory_lotDTO.insertedByOrganogramId).isEmpty()) {
						mapOfMedical_inventory_lotDTOToinsertedByOrganogramId.remove(oldMedical_inventory_lotDTO.insertedByOrganogramId);
					}
					
					if(mapOfMedical_inventory_lotDTOToinsertionDate.containsKey(oldMedical_inventory_lotDTO.insertionDate)) {
						mapOfMedical_inventory_lotDTOToinsertionDate.get(oldMedical_inventory_lotDTO.insertionDate).remove(oldMedical_inventory_lotDTO);
					}
					if(mapOfMedical_inventory_lotDTOToinsertionDate.get(oldMedical_inventory_lotDTO.insertionDate).isEmpty()) {
						mapOfMedical_inventory_lotDTOToinsertionDate.remove(oldMedical_inventory_lotDTO.insertionDate);
					}
					
					if(mapOfMedical_inventory_lotDTOTosearchColumn.containsKey(oldMedical_inventory_lotDTO.searchColumn)) {
						mapOfMedical_inventory_lotDTOTosearchColumn.get(oldMedical_inventory_lotDTO.searchColumn).remove(oldMedical_inventory_lotDTO);
					}
					if(mapOfMedical_inventory_lotDTOTosearchColumn.get(oldMedical_inventory_lotDTO.searchColumn).isEmpty()) {
						mapOfMedical_inventory_lotDTOTosearchColumn.remove(oldMedical_inventory_lotDTO.searchColumn);
					}
					
					if(mapOfMedical_inventory_lotDTOTolastModificationTime.containsKey(oldMedical_inventory_lotDTO.lastModificationTime)) {
						mapOfMedical_inventory_lotDTOTolastModificationTime.get(oldMedical_inventory_lotDTO.lastModificationTime).remove(oldMedical_inventory_lotDTO);
					}
					if(mapOfMedical_inventory_lotDTOTolastModificationTime.get(oldMedical_inventory_lotDTO.lastModificationTime).isEmpty()) {
						mapOfMedical_inventory_lotDTOTolastModificationTime.remove(oldMedical_inventory_lotDTO.lastModificationTime);
					}
					
					
				}
				if(medical_inventory_lotDTO.isDeleted == 0) 
				{
					
					mapOfMedical_inventory_lotDTOToiD.put(medical_inventory_lotDTO.iD, medical_inventory_lotDTO);
				
					if( ! mapOfMedical_inventory_lotDTOToparliamentSupplierType.containsKey(medical_inventory_lotDTO.parliamentSupplierType)) {
						mapOfMedical_inventory_lotDTOToparliamentSupplierType.put(medical_inventory_lotDTO.parliamentSupplierType, new HashSet<>());
					}
					mapOfMedical_inventory_lotDTOToparliamentSupplierType.get(medical_inventory_lotDTO.parliamentSupplierType).add(medical_inventory_lotDTO);
					
					if( ! mapOfMedical_inventory_lotDTOTorelatedPoId.containsKey(medical_inventory_lotDTO.relatedPoId)) {
						mapOfMedical_inventory_lotDTOTorelatedPoId.put(medical_inventory_lotDTO.relatedPoId, new HashSet<>());
					}
					mapOfMedical_inventory_lotDTOTorelatedPoId.get(medical_inventory_lotDTO.relatedPoId).add(medical_inventory_lotDTO);
					
					if( ! mapOfMedical_inventory_lotDTOTostockLotNumber.containsKey(medical_inventory_lotDTO.stockLotNumber)) {
						mapOfMedical_inventory_lotDTOTostockLotNumber.put(medical_inventory_lotDTO.stockLotNumber, new HashSet<>());
					}
					mapOfMedical_inventory_lotDTOTostockLotNumber.get(medical_inventory_lotDTO.stockLotNumber).add(medical_inventory_lotDTO);
					
					if( ! mapOfMedical_inventory_lotDTOToremarks.containsKey(medical_inventory_lotDTO.remarks)) {
						mapOfMedical_inventory_lotDTOToremarks.put(medical_inventory_lotDTO.remarks, new HashSet<>());
					}
					mapOfMedical_inventory_lotDTOToremarks.get(medical_inventory_lotDTO.remarks).add(medical_inventory_lotDTO);
					
					if( ! mapOfMedical_inventory_lotDTOToinsertedByUserId.containsKey(medical_inventory_lotDTO.insertedByUserId)) {
						mapOfMedical_inventory_lotDTOToinsertedByUserId.put(medical_inventory_lotDTO.insertedByUserId, new HashSet<>());
					}
					mapOfMedical_inventory_lotDTOToinsertedByUserId.get(medical_inventory_lotDTO.insertedByUserId).add(medical_inventory_lotDTO);
					
					if( ! mapOfMedical_inventory_lotDTOToinsertedByOrganogramId.containsKey(medical_inventory_lotDTO.insertedByOrganogramId)) {
						mapOfMedical_inventory_lotDTOToinsertedByOrganogramId.put(medical_inventory_lotDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfMedical_inventory_lotDTOToinsertedByOrganogramId.get(medical_inventory_lotDTO.insertedByOrganogramId).add(medical_inventory_lotDTO);
					
					if( ! mapOfMedical_inventory_lotDTOToinsertionDate.containsKey(medical_inventory_lotDTO.insertionDate)) {
						mapOfMedical_inventory_lotDTOToinsertionDate.put(medical_inventory_lotDTO.insertionDate, new HashSet<>());
					}
					mapOfMedical_inventory_lotDTOToinsertionDate.get(medical_inventory_lotDTO.insertionDate).add(medical_inventory_lotDTO);
					
					if( ! mapOfMedical_inventory_lotDTOTosearchColumn.containsKey(medical_inventory_lotDTO.searchColumn)) {
						mapOfMedical_inventory_lotDTOTosearchColumn.put(medical_inventory_lotDTO.searchColumn, new HashSet<>());
					}
					mapOfMedical_inventory_lotDTOTosearchColumn.get(medical_inventory_lotDTO.searchColumn).add(medical_inventory_lotDTO);
					
					if( ! mapOfMedical_inventory_lotDTOTolastModificationTime.containsKey(medical_inventory_lotDTO.lastModificationTime)) {
						mapOfMedical_inventory_lotDTOTolastModificationTime.put(medical_inventory_lotDTO.lastModificationTime, new HashSet<>());
					}
					mapOfMedical_inventory_lotDTOTolastModificationTime.get(medical_inventory_lotDTO.lastModificationTime).add(medical_inventory_lotDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Medical_inventory_lotDTO> getMedical_inventory_lotList() {
		List <Medical_inventory_lotDTO> medical_inventory_lots = new ArrayList<Medical_inventory_lotDTO>(this.mapOfMedical_inventory_lotDTOToiD.values());
		return medical_inventory_lots;
	}
	
	
	public Medical_inventory_lotDTO getMedical_inventory_lotDTOByID( long ID){
		return mapOfMedical_inventory_lotDTOToiD.get(ID);
	}
	
	
	public List<Medical_inventory_lotDTO> getMedical_inventory_lotDTOByparliament_supplier_type(long parliament_supplier_type) {
		return new ArrayList<>( mapOfMedical_inventory_lotDTOToparliamentSupplierType.getOrDefault(parliament_supplier_type,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_lotDTO> getMedical_inventory_lotDTOByrelated_po_id(String related_po_id) {
		return new ArrayList<>( mapOfMedical_inventory_lotDTOTorelatedPoId.getOrDefault(related_po_id,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_lotDTO> getMedical_inventory_lotDTOBystock_lot_number(String stock_lot_number) {
		return new ArrayList<>( mapOfMedical_inventory_lotDTOTostockLotNumber.getOrDefault(stock_lot_number,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_lotDTO> getMedical_inventory_lotDTOByremarks(String remarks) {
		return new ArrayList<>( mapOfMedical_inventory_lotDTOToremarks.getOrDefault(remarks,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_lotDTO> getMedical_inventory_lotDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfMedical_inventory_lotDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_lotDTO> getMedical_inventory_lotDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfMedical_inventory_lotDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_lotDTO> getMedical_inventory_lotDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfMedical_inventory_lotDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_lotDTO> getMedical_inventory_lotDTOBysearch_column(String search_column) {
		return new ArrayList<>( mapOfMedical_inventory_lotDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
	}
	
	
	public List<Medical_inventory_lotDTO> getMedical_inventory_lotDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfMedical_inventory_lotDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "medical_inventory_lot";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


