package medical_inventory_lot;
import java.util.*; 
import util.*;


public class Medical_inventory_lotMAPS extends CommonMaps
{	
	public Medical_inventory_lotMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("parliamentSupplierType".toLowerCase(), "parliamentSupplierType".toLowerCase());
		java_DTO_map.put("relatedPoId".toLowerCase(), "relatedPoId".toLowerCase());
		java_DTO_map.put("stockLotNumber".toLowerCase(), "stockLotNumber".toLowerCase());
		java_DTO_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("parliament_supplier_type".toLowerCase(), "parliamentSupplierType".toLowerCase());
		java_SQL_map.put("related_po_id".toLowerCase(), "relatedPoId".toLowerCase());
		java_SQL_map.put("stock_lot_number".toLowerCase(), "stockLotNumber".toLowerCase());
		java_SQL_map.put("remarks".toLowerCase(), "remarks".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Parliament Supplier".toLowerCase(), "parliamentSupplierType".toLowerCase());
		java_Text_map.put("Related Po Id".toLowerCase(), "relatedPoId".toLowerCase());
		java_Text_map.put("Stock Lot Number".toLowerCase(), "stockLotNumber".toLowerCase());
		java_Text_map.put("Remarks".toLowerCase(), "remarks".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}