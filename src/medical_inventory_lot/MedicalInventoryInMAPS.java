package medical_inventory_lot;
import java.util.*; 
import util.*;


public class MedicalInventoryInMAPS extends CommonMaps
{	
	public MedicalInventoryInMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("medicalInventoryLotId".toLowerCase(), "medicalInventoryLotId".toLowerCase());
		java_DTO_map.put("departmentCat".toLowerCase(), "departmentCat".toLowerCase());
		java_DTO_map.put("medicalItemCat".toLowerCase(), "medicalItemCat".toLowerCase());
		java_DTO_map.put("drugInformationType".toLowerCase(), "drugInformationType".toLowerCase());
		java_DTO_map.put("medicineGenericNameId".toLowerCase(), "medicineGenericNameId".toLowerCase());
		java_DTO_map.put("pharmaCompanyNameId".toLowerCase(), "pharmaCompanyNameId".toLowerCase());
		java_DTO_map.put("medicalReagentNameType".toLowerCase(), "medicalReagentNameType".toLowerCase());
		java_DTO_map.put("medicalEquipmentNameType".toLowerCase(), "medicalEquipmentNameType".toLowerCase());
		java_DTO_map.put("stockInQuantity".toLowerCase(), "stockInQuantity".toLowerCase());
		java_DTO_map.put("unitPrice".toLowerCase(), "unitPrice".toLowerCase());
		java_DTO_map.put("scanCode".toLowerCase(), "scanCode".toLowerCase());
		java_DTO_map.put("supplyDate".toLowerCase(), "supplyDate".toLowerCase());
		java_DTO_map.put("stockLotExpiryDate".toLowerCase(), "stockLotExpiryDate".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("medical_inventory_lot_id".toLowerCase(), "medicalInventoryLotId".toLowerCase());
		java_SQL_map.put("department_cat".toLowerCase(), "departmentCat".toLowerCase());
		java_SQL_map.put("medical_item_cat".toLowerCase(), "medicalItemCat".toLowerCase());
		java_SQL_map.put("drug_information_type".toLowerCase(), "drugInformationType".toLowerCase());
		java_SQL_map.put("medicine_generic_name_id".toLowerCase(), "medicineGenericNameId".toLowerCase());
		java_SQL_map.put("pharma_company_name_id".toLowerCase(), "pharmaCompanyNameId".toLowerCase());
		java_SQL_map.put("medical_reagent_name_type".toLowerCase(), "medicalReagentNameType".toLowerCase());
		java_SQL_map.put("medical_equipment_name_type".toLowerCase(), "medicalEquipmentNameType".toLowerCase());
		java_SQL_map.put("stock_in_quantity".toLowerCase(), "stockInQuantity".toLowerCase());
		java_SQL_map.put("unit_price".toLowerCase(), "unitPrice".toLowerCase());
		java_SQL_map.put("scan_code".toLowerCase(), "scanCode".toLowerCase());
		java_SQL_map.put("supply_date".toLowerCase(), "supplyDate".toLowerCase());
		java_SQL_map.put("stock_lot_expiry_date".toLowerCase(), "stockLotExpiryDate".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Medical Inventory Lot Id".toLowerCase(), "medicalInventoryLotId".toLowerCase());
		java_Text_map.put("Department".toLowerCase(), "departmentCat".toLowerCase());
		java_Text_map.put("Medical Item".toLowerCase(), "medicalItemCat".toLowerCase());
		java_Text_map.put("Drug Information".toLowerCase(), "drugInformationType".toLowerCase());
		java_Text_map.put("Medicine Generic Name Id".toLowerCase(), "medicineGenericNameId".toLowerCase());
		java_Text_map.put("Pharma Company Name Id".toLowerCase(), "pharmaCompanyNameId".toLowerCase());
		java_Text_map.put("Medical Reagent Name".toLowerCase(), "medicalReagentNameType".toLowerCase());
		java_Text_map.put("Medical Equipment Name".toLowerCase(), "medicalEquipmentNameType".toLowerCase());
		java_Text_map.put("Stock In Quantity".toLowerCase(), "stockInQuantity".toLowerCase());
		java_Text_map.put("Unit Price".toLowerCase(), "unitPrice".toLowerCase());
		java_Text_map.put("Scan Code".toLowerCase(), "scanCode".toLowerCase());
		java_Text_map.put("Supply Date".toLowerCase(), "supplyDate".toLowerCase());
		java_Text_map.put("Stock Lot Expiry Date".toLowerCase(), "stockLotExpiryDate".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}