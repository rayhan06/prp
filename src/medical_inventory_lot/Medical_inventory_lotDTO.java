package medical_inventory_lot;
import java.util.*; 
import util.*; 


public class Medical_inventory_lotDTO extends CommonDTO
{

	public long parliamentSupplierType = -1;
    public String relatedPoId = "";
    public String stockLotNumber = "";
    public String remarks = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	
	public boolean isFinal = false;
	
	public List<MedicalInventoryInDTO> medicalInventoryInDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Medical_inventory_lotDTO[" +
            " iD = " + iD +
            " parliamentSupplierType = " + parliamentSupplierType +
            " relatedPoId = " + relatedPoId +
            " stockLotNumber = " + stockLotNumber +
            " remarks = " + remarks +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}