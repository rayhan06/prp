package medical_inventory_lot;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Medical_inventory_lotDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Medical_inventory_lotDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Medical_inventory_lotMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"parliament_supplier_type",
			"related_po_id",
			"stock_lot_number",
			"remarks",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"search_column",
			"is_final",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Medical_inventory_lotDAO()
	{
		this("medical_inventory_lot");		
	}
	
	public void setSearchColumn(Medical_inventory_lotDTO medical_inventory_lotDTO)
	{
		medical_inventory_lotDTO.searchColumn = "";
		medical_inventory_lotDTO.searchColumn += CommonDAO.getName("English", "parliament_supplier", medical_inventory_lotDTO.parliamentSupplierType) + " " + CommonDAO.getName("Bangla", "parliament_supplier", medical_inventory_lotDTO.parliamentSupplierType) + " ";
		medical_inventory_lotDTO.searchColumn += medical_inventory_lotDTO.stockLotNumber + " ";
		medical_inventory_lotDTO.searchColumn += medical_inventory_lotDTO.remarks + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Medical_inventory_lotDTO medical_inventory_lotDTO = (Medical_inventory_lotDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(medical_inventory_lotDTO);
		if(isInsert)
		{
			ps.setObject(index++,medical_inventory_lotDTO.iD);
		}
		ps.setObject(index++,medical_inventory_lotDTO.parliamentSupplierType);
		ps.setObject(index++,medical_inventory_lotDTO.relatedPoId);
		ps.setObject(index++,medical_inventory_lotDTO.stockLotNumber);
		ps.setObject(index++,medical_inventory_lotDTO.remarks);
		ps.setObject(index++,medical_inventory_lotDTO.insertedByUserId);
		ps.setObject(index++,medical_inventory_lotDTO.insertedByOrganogramId);
		ps.setObject(index++,medical_inventory_lotDTO.insertionDate);
		ps.setObject(index++,medical_inventory_lotDTO.searchColumn);
		ps.setObject(index++,medical_inventory_lotDTO.isFinal);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Medical_inventory_lotDTO build(ResultSet rs)
	{
		try
		{
			Medical_inventory_lotDTO medical_inventory_lotDTO = new Medical_inventory_lotDTO();
			medical_inventory_lotDTO.iD = rs.getLong("ID");
			medical_inventory_lotDTO.parliamentSupplierType = rs.getLong("parliament_supplier_type");
			medical_inventory_lotDTO.relatedPoId = rs.getString("related_po_id");
			medical_inventory_lotDTO.stockLotNumber = rs.getString("stock_lot_number");
			medical_inventory_lotDTO.remarks = rs.getString("remarks");
			medical_inventory_lotDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			medical_inventory_lotDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			medical_inventory_lotDTO.insertionDate = rs.getLong("insertion_date");
			medical_inventory_lotDTO.searchColumn = rs.getString("search_column");
			medical_inventory_lotDTO.isFinal = rs.getBoolean("is_final");
			medical_inventory_lotDTO.isDeleted = rs.getInt("isDeleted");
			medical_inventory_lotDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return medical_inventory_lotDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

	public Medical_inventory_lotDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Medical_inventory_lotDTO medical_inventory_lotDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		try {
			MedicalInventoryInDAO medicalInventoryInDAO = new MedicalInventoryInDAO("medical_inventory_in");			
			List<MedicalInventoryInDTO> medicalInventoryInDTOList = medicalInventoryInDAO.getMedicalInventoryInDTOListByMedicalInventoryLotID(medical_inventory_lotDTO.iD);
			medical_inventory_lotDTO.medicalInventoryInDTOList = medicalInventoryInDTOList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
		return medical_inventory_lotDTO;
	}

	
	public List<Medical_inventory_lotDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<Medical_inventory_lotDTO> getAllMedical_inventory_lot (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Medical_inventory_lotDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Medical_inventory_lotDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("parliament_supplier_type")
						|| str.equals("stock_lot_number")
						|| str.equals("remarks")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
						|| str.equals("drug_id")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					if(str.equals("parliament_supplier_type"))
					{
						AllFieldSql += "" + tableName + ".parliament_supplier_type = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("stock_lot_number"))
					{
						AllFieldSql += "" + tableName + ".stock_lot_number like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("remarks"))
					{
						AllFieldSql += "" + tableName + ".remarks like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("drug_id"))
					{
						AllFieldSql += Long.parseLong((String) p_searchCriteria.get(str)) 
								+ " in (select drug_information_type from medical_inventory_in where medical_inventory_lot_id = medical_inventory_lot.id)" ;
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	