package medical_inventory_lot;


import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.*;

import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import drug_information.Drug_informationDAO;
import drug_information.Drug_informationDTO;
import medical_equipment_name.Medical_equipment_nameDAO;
import medical_equipment_name.Medical_equipment_nameDTO;
import medical_inventory_out.Medical_inventory_outDAO;
import medical_reagent_name.Medical_reagent_nameDAO;
import medical_reagent_name.Medical_reagent_nameDTO;
import medical_transaction.Medical_transactionDAO;
import medical_transaction.Medical_transactionDTO;
import repository.RepositoryManager;

import util.*;

import user.UserDTO;

public class MedicalInventoryInDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	Medical_reagent_nameDAO medical_reagent_nameDAO = new Medical_reagent_nameDAO();
	Medical_equipment_nameDAO medical_equiment_nameDAO = new Medical_equipment_nameDAO();
	Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
	Medical_inventory_outDAO medical_inventory_outDAO = new Medical_inventory_outDAO();
	Medical_transactionDAO medical_transactionDAO = new Medical_transactionDAO();
	

	public MedicalInventoryInDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		joinSQL += " join drug_information on " + tableName + ".drug_information_type = drug_information.ID ";
		joinSQL += " join medical_reagent_name on " + tableName + ".medical_reagent_name_type = medical_reagent_name.ID ";
		joinSQL += " join medical_equipment_name on " + tableName + ".medical_equipment_name_type = medical_equipment_name.ID ";

		commonMaps = new MedicalInventoryInMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"medical_inventory_lot_id",
			"department_cat",
			"medical_item_cat",
			"medical_dept_cat",
			"drug_information_type",
			"medicine_generic_name_id",
			"pharma_company_name_id",
			"medical_reagent_name_type",
			"medical_equipment_name_type",
			"stock_in_quantity",
			"unit_price",
			"scan_code",
			"supply_date",
			"stock_lot_expiry_date",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"transaction_date",
			"transaction_type",
			"user_name",
			"organogram_id",
			"transaction_time",
			"employee_record_id",
			"other_office_id",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public MedicalInventoryInDAO()
	{
		this("medical_inventory_in");		
	}
	
	public void setSearchColumn(MedicalInventoryInDTO medicalinventoryinDTO)
	{
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		MedicalInventoryInDTO medicalinventoryinDTO = (MedicalInventoryInDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		if(isInsert)
		{
			ps.setObject(index++,medicalinventoryinDTO.iD);
		}
		ps.setObject(index++,medicalinventoryinDTO.medicalInventoryLotId);
		ps.setObject(index++,medicalinventoryinDTO.departmentCat);
		ps.setObject(index++,medicalinventoryinDTO.medicalItemCat);
		ps.setObject(index++,medicalinventoryinDTO.medicalDeptCat);
		ps.setObject(index++,medicalinventoryinDTO.drugInformationType);
		if(medicalinventoryinDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_DRUG)
		{
			Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
			Drug_informationDTO drug_informationDTO =  drug_informationDAO.getDTOByID(medicalinventoryinDTO.drugInformationType);
			ps.setObject(index++,drug_informationDTO.medicineGenericNameType);
			ps.setObject(index++,drug_informationDTO.pharmaCompanyNameType);
		}
		else
		{
			ps.setObject(index++,-1);
			ps.setObject(index++,-1);
		}
		ps.setObject(index++,medicalinventoryinDTO.medicalReagentNameType);
		ps.setObject(index++,medicalinventoryinDTO.medicalEquipmentNameType);
		ps.setObject(index++,medicalinventoryinDTO.stockInQuantity);
		ps.setObject(index++,medicalinventoryinDTO.unitPrice);
		ps.setObject(index++,medicalinventoryinDTO.scanCode);
		ps.setObject(index++,medicalinventoryinDTO.supplyDate);
		ps.setObject(index++,medicalinventoryinDTO.stockLotExpiryDate);
		ps.setObject(index++,medicalinventoryinDTO.insertedByUserId);
		ps.setObject(index++,medicalinventoryinDTO.insertedByOrganogramId);
		ps.setObject(index++,medicalinventoryinDTO.insertionDate);
		ps.setObject(index++,medicalinventoryinDTO.transactionDate);
		ps.setObject(index++,medicalinventoryinDTO.transactionType);
		ps.setObject(index++,medicalinventoryinDTO.userName);
		ps.setObject(index++,medicalinventoryinDTO.organogramId);
		ps.setObject(index++,medicalinventoryinDTO.transactionTime);
		ps.setObject(index++,medicalinventoryinDTO.employeeRecordId);
		ps.setObject(index++,medicalinventoryinDTO.otherOfficeId);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
		
		
	}
	
	public MedicalInventoryInDTO build(ResultSet rs)
	{
		try
		{
			MedicalInventoryInDTO medicalinventoryinDTO = new MedicalInventoryInDTO();
			medicalinventoryinDTO.iD = rs.getLong("ID");
			medicalinventoryinDTO.medicalInventoryLotId = rs.getLong("medical_inventory_lot_id");
			medicalinventoryinDTO.departmentCat = rs.getInt("department_cat");
			medicalinventoryinDTO.medicalItemCat = rs.getLong("medical_item_cat");
			medicalinventoryinDTO.medicalDeptCat = rs.getInt("medical_dept_cat");
			medicalinventoryinDTO.drugInformationType = rs.getLong("drug_information_type");
		
			medicalinventoryinDTO.medicineGenericNameId = rs.getLong("medicine_generic_name_id");
			medicalinventoryinDTO.pharmaCompanyNameId = rs.getLong("pharma_company_name_id");
		
			medicalinventoryinDTO.medicalReagentNameType = rs.getLong("medical_reagent_name_type");
			medicalinventoryinDTO.medicalEquipmentNameType = rs.getLong("medical_equipment_name_type");
			medicalinventoryinDTO.stockInQuantity = rs.getInt("stock_in_quantity");
			medicalinventoryinDTO.unitPrice = rs.getDouble("unit_price");
			medicalinventoryinDTO.scanCode = rs.getString("scan_code");
			medicalinventoryinDTO.supplyDate = rs.getLong("supply_date");
			medicalinventoryinDTO.stockLotExpiryDate = rs.getLong("stock_lot_expiry_date");
			medicalinventoryinDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			medicalinventoryinDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			medicalinventoryinDTO.insertionDate = rs.getLong("insertion_date");
			medicalinventoryinDTO.transactionDate = rs.getLong("transaction_date");
			medicalinventoryinDTO.transactionType = rs.getInt("transaction_type");
			medicalinventoryinDTO.userName = rs.getString("user_name");
			medicalinventoryinDTO.organogramId = rs.getLong("organogram_id");
			medicalinventoryinDTO.transactionTime = rs.getLong("transaction_time");
			medicalinventoryinDTO.employeeRecordId = rs.getLong("employee_record_id");
			medicalinventoryinDTO.otherOfficeId = rs.getLong("other_office_id");
			medicalinventoryinDTO.isDeleted = rs.getInt("isDeleted");
			medicalinventoryinDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return medicalinventoryinDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	public long add(CommonDTO commonDTO) throws Exception
	{
		return add(commonDTO,  true);
	}
	
	public void insertStock(MedicalInventoryInDTO medicalinventoryinDTO) throws Exception
	{
		if(medicalinventoryinDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_REAGENT)
		{
			Medical_reagent_nameDTO medical_reagent_nameDTO = medical_reagent_nameDAO.getDTOByID(medicalinventoryinDTO.medicalReagentNameType);
			medical_reagent_nameDTO.currentStock += medicalinventoryinDTO.stockInQuantity;
			if(medical_reagent_nameDTO.currentStock > 0 && medicalinventoryinDTO.transactionType ==MedicalInventoryInDTO.TR_STOCK_IN)
			{
				medical_reagent_nameDTO.stockAlertQuantity = (int)(Drug_informationDTO.DEFAULT_STOCK_ALERT_PERCENTAGE * medical_reagent_nameDTO.currentStock);
			}
			medical_reagent_nameDAO.update(medical_reagent_nameDTO);
			if(medical_reagent_nameDTO.currentStock < medical_reagent_nameDTO.stockAlertQuantity)
			{
				medical_inventory_outDAO.sendNoti(SessionConstants.MEDICAL_ITEM_REAGENT, medical_reagent_nameDTO.iD, medical_reagent_nameDTO.nameEn, medical_reagent_nameDTO.nameBn);
			}
		}
		else if(medicalinventoryinDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_EQUIPMENT)
		{				
			Medical_equipment_nameDTO medical_equipment_nameDTO =  medical_equiment_nameDAO.getDTOByID(medicalinventoryinDTO.medicalEquipmentNameType);
			medical_equipment_nameDTO.currentStock += medicalinventoryinDTO.stockInQuantity;
			if(medical_equipment_nameDTO.currentStock > 0 && medicalinventoryinDTO.transactionType ==MedicalInventoryInDTO.TR_STOCK_IN)
			{
				medical_equipment_nameDTO.stockAlertQuantity = (int)(Drug_informationDTO.DEFAULT_STOCK_ALERT_PERCENTAGE * medical_equipment_nameDTO.currentStock);
				medical_equipment_nameDTO.unitPrice = medicalinventoryinDTO.unitPrice;
			}
			medical_equiment_nameDAO.update(medical_equipment_nameDTO);
			
			if(medical_equipment_nameDTO.currentStock < medical_equipment_nameDTO.stockAlertQuantity)
			{
				medical_inventory_outDAO.sendNoti(SessionConstants.MEDICAL_ITEM_EQUIPMENT, medical_equipment_nameDTO.iD, medical_equipment_nameDTO.nameEn, medical_equipment_nameDTO.nameBn);
			}
		}
		else if(medicalinventoryinDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_DRUG)
		{				
			Drug_informationDTO drug_informationDTO =  drug_informationDAO.getDTOByID(medicalinventoryinDTO.drugInformationType);
			drug_informationDTO.currentStock += medicalinventoryinDTO.stockInQuantity;
			
			if(drug_informationDTO.currentStock > 0 && medicalinventoryinDTO.transactionType ==MedicalInventoryInDTO.TR_STOCK_IN)
			{
				drug_informationDTO.minimulLevelForAlert = (int)(Drug_informationDTO.DEFAULT_STOCK_ALERT_PERCENTAGE * drug_informationDTO.currentStock);
				drug_informationDTO.unitPrice = medicalinventoryinDTO.unitPrice;
			}

			drug_informationDAO.update(drug_informationDTO);
			
			if(drug_informationDTO.currentStock < drug_informationDTO.minimulLevelForAlert)
			{
				medical_inventory_outDAO.sendNoti(SessionConstants.MEDICAL_ITEM_DRUG, drug_informationDTO.iD, drug_informationDTO.nameEn, drug_informationDTO.nameBn);
			}
		}
		Medical_transactionDTO medical_transactionDTO = new Medical_transactionDTO(medicalinventoryinDTO);
		try {
			medical_transactionDAO.add(medical_transactionDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean isValid(MedicalInventoryInDTO medicalinventoryinDTO )
	{
		if(medicalinventoryinDTO.stockInQuantity == 0)
		{
			return false;
		}
		if(medicalinventoryinDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_REAGENT)
		{			
			Medical_reagent_nameDTO medical_reagent_nameDTO = medical_reagent_nameDAO.getDTOByID(medicalinventoryinDTO.medicalReagentNameType);
            return medical_reagent_nameDTO != null && medical_reagent_nameDTO.currentStock + medicalinventoryinDTO.stockInQuantity >= 0;
		}
		else if(medicalinventoryinDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_EQUIPMENT)
		{
			Medical_equipment_nameDTO medical_equipment_nameDTO =  medical_equiment_nameDAO.getDTOByID(medicalinventoryinDTO.medicalEquipmentNameType);
            return medical_equipment_nameDTO != null && medical_equipment_nameDTO.currentStock + medicalinventoryinDTO.stockInQuantity >= 0;
		}
		else if(medicalinventoryinDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_DRUG)
		{
			Drug_informationDTO drug_informationDTO =  drug_informationDAO.getDTOByID(medicalinventoryinDTO.drugInformationType);
            return drug_informationDTO != null && drug_informationDTO.currentStock + medicalinventoryinDTO.stockInQuantity >= 0;
		}
		return true;
	}

	public long add(CommonDTO commonDTO, boolean insertStock) throws Exception
	{		
		MedicalInventoryInDTO medicalinventoryinDTO = (MedicalInventoryInDTO)commonDTO;
		if(!isValid(medicalinventoryinDTO))
		{
			return -1;
		}
		
		super.add(commonDTO);
		
		if(insertStock)
		{
			insertStock(medicalinventoryinDTO);
		}
		return medicalinventoryinDTO.iD;		
	}
	
	public long update(MedicalInventoryInDTO medicalinventoryinDTO) throws Exception
	{
		if(!isValid(medicalinventoryinDTO))
		{
			return -1;
		}
		
		super.update(medicalinventoryinDTO);
		
		return medicalinventoryinDTO.iD;	
	}
	
	
	public List<MedicalInventoryInDTO> getMedicalInventoryInDTOListByMedicalInventoryLotID(long medicalInventoryLotID) throws Exception
	{
		String sql = "SELECT * FROM medical_inventory_in where isDeleted=0 and medical_inventory_lot_id="+medicalInventoryLotID+" order by medical_inventory_in.lastModificationTime";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}

	public MedicalInventoryInDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		MedicalInventoryInDTO medicalinventoryinDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return medicalinventoryinDTO;
	}

	
	public List<MedicalInventoryInDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public MedicalInventoryInDTO buildForRemainingStock(ResultSet rs)
	{
		SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
		MedicalInventoryInDTO medicalinventoryinDTO = build(rs);
		medicalinventoryinDTO.remainingStock = medicalinventoryinDTO.stockInQuantity - getSoldSumByInventoryInId(medicalinventoryinDTO.iD);
		medicalinventoryinDTO.expiryDateStr = f.format(medicalinventoryinDTO.stockLotExpiryDate);
		if(medicalinventoryinDTO.remainingStock > 0)
		{
			return medicalinventoryinDTO;
		}
		
		return null;
	}

	public List<MedicalInventoryInDTO> getMedicalInventoryInDTOListByDrugInfoId(long drugInfoId) throws Exception{
		String sql = "SELECT * FROM medical_inventory_in where isDeleted= 0 "
				+ "and medical_item_cat = " + SessionConstants.MEDICAL_ITEM_DRUG + " "
				+ "and drug_information_type = " + drugInfoId;
		return ConnectionAndStatementUtil.getListOfT(sql,this::buildForRemainingStock);			
	}
	
	
	
	public List<MedicalInventoryInDTO> getMedicalInventoryInDTOListOfThisYearByDrugInfoId(long drugInfoId) throws Exception{
		
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_YEAR, 1);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		String sql = "SELECT * FROM medical_inventory_in where isDeleted= 0 "
				+ "and medical_item_cat = " + SessionConstants.MEDICAL_ITEM_DRUG + " "
				+ "and drug_information_type = " + drugInfoId
				+ " and insertion_date >= " + calendar.getTimeInMillis();
		
		return ConnectionAndStatementUtil.getListOfT(sql,this::buildForRemainingStock);
	}
	
	public List<MedicalInventoryInDTO> getMedicalInventoryInDTOListByReagentId(long reagentId) throws Exception{
		String sql = "SELECT * FROM medical_inventory_in where isDeleted= 0 "
				+ "and medical_item_cat =  " + SessionConstants.MEDICAL_ITEM_REAGENT + " "
				+ "and medical_reagent_name_type = " + reagentId;
		return ConnectionAndStatementUtil.getListOfT(sql,this::buildForRemainingStock);
	
	}
	
	public List<MedicalInventoryInDTO> getMedicalInventoryInDTOListOfThisYearByReagentId(long reagentId) throws Exception{

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_YEAR, 1);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		String sql = "SELECT * FROM medical_inventory_in where isDeleted= 0 "
				+ "and medical_item_cat =  " + SessionConstants.MEDICAL_ITEM_REAGENT + " "
				+ "and medical_reagent_name_type = " + reagentId
				+ " and insertion_date >= " + calendar.getTimeInMillis();
		
		return ConnectionAndStatementUtil.getListOfT(sql,this::buildForRemainingStock);
	}
	
	public int getSoldSum(ResultSet rs)
	{
		try {
			return rs.getInt("sum(stock_out_quantity)");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}	
	}
   

	
	public int getSoldSumByInventoryInId (long InventoryInId)
	{
		String sql = "SELECT sum(stock_out_quantity) FROM medical_inventory_out where isDeleted = 0 and medical_inventory_in_id = " + InventoryInId;
		return ConnectionAndStatementUtil.getT(sql,this::getSoldSum, 0);		
	}
	
	public MedicalInventoryInDTO getLastMedicineEntry(long drugInfoId)
	{
		String sql = "SELECT \r\n" + 
				"    *\r\n" + 
				"FROM\r\n" + 
				"    medical_inventory_in\r\n" + 
				"WHERE\r\n" + 
				"    drug_information_type = " + drugInfoId + " \r\n" + 
				"        AND medical_item_cat = 0\r\n" + 
				"        AND isDeleted = 0\r\n" + 
				"        AND stock_in_quantity > 0\r\n" + 
				"ORDER BY id DESC\r\n" + 
				"LIMIT 1";
			
		return ConnectionAndStatementUtil.getT(sql,this::build);
	}
	
	public List<MedicalInventoryInDTO> getAllMedicalInventoryIn (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<MedicalInventoryInDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<MedicalInventoryInDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
				
}
	