package overtime_allowance;

import office_units.Office_unitsRepository;

import java.util.List;

import static util.StringUtils.convertBanglaIfLanguageIsBangla;
import static util.UtilCharacter.getDataByLanguage;

public class Overtime_summaryModel {
    public String billDescription = "";
    public String officeName = "";
    public long billAmount = 0;

    public static final String BILL_DESCRIPTION_BN = "%s কার্যালয়ের %s জনের নিয়মিত অধিকাল ভাতা";
    public static final String BILL_DESCRIPTION_EN = "%s office's overtime bill of %s employees";

    public static Overtime_summaryModel from(List<Overtime_allowanceDTO> overtimeAllowanceDTOsByOfficeUnitId,
                                             String language) {
        Overtime_summaryModel overtimeSummaryModel = new Overtime_summaryModel();
        if (overtimeAllowanceDTOsByOfficeUnitId != null && !overtimeAllowanceDTOsByOfficeUnitId.isEmpty()) {
            Overtime_allowanceDTO allowanceDTO = overtimeAllowanceDTOsByOfficeUnitId.get(0);
            overtimeSummaryModel.officeName = Office_unitsRepository.getInstance().geText(language, allowanceDTO.officeUnitId);
            overtimeSummaryModel.billDescription = String.format(
                    getDataByLanguage(language, BILL_DESCRIPTION_BN, BILL_DESCRIPTION_EN),
                    overtimeSummaryModel.officeName,
                    convertBanglaIfLanguageIsBangla(language, String.format("%d", overtimeAllowanceDTOsByOfficeUnitId.size()))
            );
            overtimeSummaryModel.billAmount =
                    overtimeAllowanceDTOsByOfficeUnitId.stream()
                                                       .mapToLong(dto -> dto.totalAmount)
                                                       .sum();
        }
        return overtimeSummaryModel;
    }
}
