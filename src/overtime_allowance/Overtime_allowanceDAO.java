package overtime_allowance;

import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import common.ConnectionAndStatementUtil;
import common.EmployeeCommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static util.StringUtils.convertToBanNumber;

@SuppressWarnings({"Duplicates"})
public class Overtime_allowanceDAO implements EmployeeCommonDAOService<Overtime_allowanceDTO> {
    private static final Logger logger = Logger.getLogger(Overtime_allowanceDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (overtime_bill_id,budget_office_id,budget_register_id,bill_register_id,allowance_employee_info_id,designation_prefix,serial_no,employee_records_id,office_unit_id,organogram_id,"
                    .concat("ka_daily_rate,ka_days,kha_daily_rate,kha_days,overtime_types,revenue_stamp_deduction,search_column,modified_by,lastModificationTime,")
                    .concat("inserted_by,insertion_date,isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET overtime_bill_id=?,budget_office_id=?,budget_register_id=?,bill_register_id=?,allowance_employee_info_id=?,designation_prefix=?,serial_no=?,employee_records_id=?,office_unit_id=?,"
                    .concat("organogram_id=?,ka_daily_rate=?,ka_days=?,kha_daily_rate=?,kha_days=?,overtime_types=?,revenue_stamp_deduction=?,search_column=?,modified_by=?,")
                    .concat("lastModificationTime=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private Overtime_allowanceDAO() {
        searchMap.put("AnyField", " AND (search_column LIKE ?) ");
        searchMap.put("budgetOfficeId", " AND (budget_office_id = ?) ");
    }

    private static class LazyLoader {
        static final Overtime_allowanceDAO INSTANCE = new Overtime_allowanceDAO();
    }

    public static Overtime_allowanceDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Overtime_allowanceDTO overtime_allowanceDTO) {
        AllowanceEmployeeInfoDTO allowanceEmployeeInfoDTO =
                AllowanceEmployeeInfoRepository.getInstance().getById(overtime_allowanceDTO.allowanceEmployeeInfoId);

        overtime_allowanceDTO.searchColumn =
                allowanceEmployeeInfoDTO.nameEn + " " + allowanceEmployeeInfoDTO.nameBn + " "
                + allowanceEmployeeInfoDTO.officeNameEn + " "
                + allowanceEmployeeInfoDTO.officeNameBn + " "
                + allowanceEmployeeInfoDTO.organogramNameEn + " "
                + allowanceEmployeeInfoDTO.organogramNameBn + " "
                + allowanceEmployeeInfoDTO.mobileNumber + " "
                + convertToBanNumber(allowanceEmployeeInfoDTO.mobileNumber) + " "
                + allowanceEmployeeInfoDTO.savingAccountNumber + " "
                + convertToBanNumber(allowanceEmployeeInfoDTO.savingAccountNumber);
    }

    @Override
    public void set(PreparedStatement ps, Overtime_allowanceDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.overtimeBillId);
        ps.setLong(++index, dto.budgetOfficeId);
        ps.setLong(++index, dto.budgetRegisterId);
        ps.setLong(++index, dto.billRegisterId);
        ps.setLong(++index, dto.allowanceEmployeeInfoId);
        ps.setString(++index, dto.designationPrefix);
        ps.setInt(++index, dto.serialNo);
        ps.setLong(++index, dto.employeeRecordsId);
        ps.setLong(++index, dto.officeUnitId);
        ps.setLong(++index, dto.organogramId);
        ps.setLong(++index, dto.kaDailyRate);
        ps.setLong(++index, dto.kaDays);
        ps.setLong(++index, dto.khaDailyRate);
        ps.setLong(++index, dto.khaDays);
        ps.setString(++index, dto.getOvertimeTypesString());
        ps.setInt(++index, dto.revenueStampDeduction);
        ps.setString(++index, dto.searchColumn);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionDate);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Overtime_allowanceDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Overtime_allowanceDTO dto = new Overtime_allowanceDTO();
            dto.iD = rs.getLong("ID");
            dto.overtimeBillId = rs.getLong("overtime_bill_id");
            dto.budgetOfficeId = rs.getLong("budget_office_id");
            dto.budgetRegisterId = rs.getLong("budget_register_id");
            dto.billRegisterId = rs.getLong("bill_register_id");
            dto.allowanceEmployeeInfoId = rs.getLong("allowance_employee_info_id");
            dto.designationPrefix = rs.getString("designation_prefix");
            dto.serialNo = rs.getInt("serial_no");
            dto.employeeRecordsId = rs.getLong("employee_records_id");
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.organogramId = rs.getLong("organogram_id");
            dto.kaDailyRate = rs.getLong("ka_daily_rate");
            dto.kaDays = rs.getLong("ka_days");
            dto.khaDailyRate = rs.getLong("kha_daily_rate");
            dto.khaDays = rs.getLong("kha_days");
            dto.calculateAndSetTotalAmount();
            dto.overtimeTypes = Overtime_allowanceDTO.getOvertimeTypesFromString(rs.getString("overtime_types"));
            dto.revenueStampDeduction = rs.getInt("revenue_stamp_deduction");
            dto.netAmount = dto.totalAmount - dto.revenueStampDeduction;
            dto.searchColumn = rs.getString("search_column");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionDate = rs.getLong("insertion_date");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "overtime_allowance";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Overtime_allowanceDTO) commonDTO, addSqlQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Overtime_allowanceDTO) commonDTO, updateSqlQuery, false);
    }


    private static final String findByOvertimeBillIdSql =
            "SELECT * FROM overtime_allowance WHERE overtime_bill_id=? AND isDeleted=0";

    public List<Overtime_allowanceDTO> findByOvertimeBillId(long overtimeBillId) {
        return getDTOs(
                findByOvertimeBillIdSql,
                Collections.singletonList(overtimeBillId)
        );
    }

    private static final String findByOvertimeBillIdAndEmployeeIdSql =
            "SELECT * FROM overtime_allowance WHERE overtime_bill_id=%d AND employee_records_id =%d and isDeleted=0";

    public Overtime_allowanceDTO findByOvertimeBillIdAndEmployeeId(long overtimeBillId, long employeeRecordsId) {
        return ConnectionAndStatementUtil.getT(
                String.format(findByOvertimeBillIdAndEmployeeIdSql, overtimeBillId, employeeRecordsId),
                this::buildObjectFromResultSet
        );
    }

    private static final String deleteByOvertimeBillIdSql =
            "UPDATE overtime_allowance SET isDeleted = 1,modified_by = %d,lastModificationTime = %d WHERE overtime_bill_id = %d";

    public void deleteByOvertimeBillId(long overtimeBillId, long modifiedBy, long modificationTime) {
        String sql = String.format(deleteByOvertimeBillIdSql, modifiedBy, modificationTime, overtimeBillId);
        logger.debug(sql);
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            try {
                model.getStatement().executeUpdate(sql);
                recordUpdateTime(model.getConnection(), getTableName(), modificationTime);
                return true;
            } catch (SQLException ex) {
                ex.printStackTrace();
                return false;
            }
        });
    }

    private Overtime_allowanceEmployeeIdModel buildEmployeeIdModelFromResultSet(ResultSet rs) {
        try {
            Overtime_allowanceEmployeeIdModel model = new Overtime_allowanceEmployeeIdModel();
            model.overtimeAllowanceId = rs.getLong("oa.id");
            model.employeeRecordsId = rs.getLong("oa.employee_records_id");
            model.officeUnitId = rs.getLong("oa.office_unit_id");
            model.organogramId = rs.getLong("oa.organogram_id");
            model.isInPreviewStage = rs.getBoolean("ob.is_in_preview_stage");
            return model;
        } catch (Exception exception) {
            logger.error(exception.getMessage());
        }
        return null;
    }

    private static final String findAddedEmployeeIdModelSql =
            "select oa.id, " +
            "       oa.employee_records_id, " +
            "       oa.office_unit_id, " +
            "       oa.organogram_id, " +
            "       ob.is_in_preview_stage " +
            "from overtime_bill ob left join overtime_allowance oa on ob.ID = oa.overtime_bill_id " +
            "where ob.isDeleted = 0 " +
            "  and oa.isDeleted = 0 " +
            "  and ob.office_unit_id = %d " +
            "  and ob.ot_employee_type_id = %d " +
            "  and ob.ot_bill_submission_config_id = %d ";

    public List<Overtime_allowanceEmployeeIdModel> getAddedEmployeeIdModel(long officeUnitsId,
                                                                           long otEmployeeTypeId,
                                                                           long otBillSubmissionConfigId) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(findAddedEmployeeIdModelSql, officeUnitsId, otEmployeeTypeId, otBillSubmissionConfigId),
                this::buildEmployeeIdModelFromResultSet
        );
    }
}