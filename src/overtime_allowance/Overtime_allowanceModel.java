package overtime_allowance;

import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import bangladehi_number_format_util.BangladeshiNumberFormatter;
import overtime_bill.OvertimeType;
import util.HttpRequestUtils;
import util.StringUtils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@SuppressWarnings({"Duplicates"})
public class Overtime_allowanceModel {
    public String employeeRecordsId = "-1";
    public Long overtimeAllowanceId = -1L;
    public Integer serialNo = 0;
    public String name = "";
    public String nid = "";
    public String officeName = "";
    public String designationPrefix = "";
    public String organogramName = "";
    public String mobileNumber = "";
    public String savingAccountNumber = "";
    public String kaDailyRate = "";
    public String kaDays = "";
    public Integer kaDaysInt;
    public String kaAmount = "";
    public String khaDailyRate = "";
    public String khaDays = "";
    public Integer khaDaysInt;
    public String khaAmount = "";
    public String totalAmount = "";
    public String revenueStampDeduction = "";
    public String netAmount = "";
    public long totalAmountLong = 0;
    public long revenueStampDeductionLong = 0;
    public long netAmountLong = 0;
    public List<String> overtimeTypes = new ArrayList<>();


    public static void fixOrdering(List<Overtime_allowanceModel> allowanceModels) {
        if (allowanceModels != null) {
            allowanceModels.sort(Comparator.comparingInt(allowanceModel -> allowanceModel.serialNo));
        }
    }

    public void calculateAndSetKaKhaDays() {
        kaDaysInt = 0;
        khaDaysInt = 0;
        for (String overtimeType : overtimeTypes) {
            if (OvertimeType.KA.getValue().equals(overtimeType)) ++kaDaysInt;
            else if (OvertimeType.KHA.getValue().equals(overtimeType)) ++khaDaysInt;
        }
        kaDays = String.format("%d", kaDaysInt);
        khaDays = String.format("%d", khaDaysInt);
    }

    public boolean isAllOtDaysBlank() {
        return overtimeTypes.stream()
                            .allMatch(otTypeValue -> OvertimeType.BLANK.getValue().equals(otTypeValue));
    }

    public void convertNumbersToSpecifiedLanguage(String language) {
        kaDailyRate = StringUtils.convertBanglaIfLanguageIsBangla(language, kaDailyRate);
        kaDays = StringUtils.convertBanglaIfLanguageIsBangla(language, kaDays);
        kaAmount = StringUtils.convertBanglaIfLanguageIsBangla(language, kaAmount);
        khaDailyRate = StringUtils.convertBanglaIfLanguageIsBangla(language, khaDailyRate);
        khaDays = StringUtils.convertBanglaIfLanguageIsBangla(language, khaDays);
        khaAmount = StringUtils.convertBanglaIfLanguageIsBangla(language, khaAmount);
        totalAmount = StringUtils.convertBanglaIfLanguageIsBangla(language, totalAmount);
        revenueStampDeduction = StringUtils.convertBanglaIfLanguageIsBangla(language, revenueStampDeduction);
        netAmount = StringUtils.convertBanglaIfLanguageIsBangla(language, netAmount);
    }

    public Overtime_allowanceModel() {
    }

    public Overtime_allowanceModel(AllowanceEmployeeInfoDTO employeeInfoDTO, String language) {
        this();
        setEmployeeInfo(this, employeeInfoDTO, language);
        convertNumbersToSpecifiedLanguage(language);
    }

    public Overtime_allowanceModel(Overtime_allowanceDTO allowanceDTO, String language) {
        this();
        AllowanceEmployeeInfoDTO allowanceEmployeeInfoDTO = AllowanceEmployeeInfoRepository.getInstance().getById(allowanceDTO.allowanceEmployeeInfoId);
        allowanceEmployeeInfoDTO.designationPrefix = allowanceDTO.designationPrefix;

        setEmployeeInfo(this, allowanceEmployeeInfoDTO, language);
        setAllowanceInfo(this, allowanceDTO);
        convertNumbersToSpecifiedLanguage(language);
    }

    private static void setEmployeeInfo(Overtime_allowanceModel model, AllowanceEmployeeInfoDTO employeeInfoDTO, String language) {
        boolean isLangBn = !"English".equalsIgnoreCase(language);
        model.employeeRecordsId = String.valueOf(employeeInfoDTO.employeeRecordId);
        model.designationPrefix = employeeInfoDTO.designationPrefix;
        model.name = isLangBn ? employeeInfoDTO.nameBn : employeeInfoDTO.nameEn;
        model.officeName = isLangBn ? employeeInfoDTO.officeNameBn : employeeInfoDTO.officeNameEn;
        model.organogramName = model.designationPrefix + (isLangBn ? employeeInfoDTO.organogramNameBn : employeeInfoDTO.organogramNameEn);

        model.nid = employeeInfoDTO.nid;
        model.mobileNumber = employeeInfoDTO.mobileNumber;
        model.savingAccountNumber = employeeInfoDTO.savingAccountNumber;
        if (isLangBn) {
            model.nid = StringUtils.convertToBanNumber(model.nid);
            model.mobileNumber = StringUtils.convertToBanNumber(model.mobileNumber);
            model.savingAccountNumber = StringUtils.convertToBanNumber(model.savingAccountNumber);
        }
    }

    private static void setAllowanceInfo(Overtime_allowanceModel model, Overtime_allowanceDTO allowanceDTO) {
        model.serialNo = allowanceDTO.serialNo;
        model.overtimeTypes = allowanceDTO.getOvertimeTypeStringValueList();
        model.kaDailyRate = String.format("%d", allowanceDTO.kaDailyRate);
        model.kaDays = String.format("%d", allowanceDTO.kaDays);
        model.kaAmount = String.format("%d", allowanceDTO.kaDailyRate * allowanceDTO.kaDays);
        model.khaDailyRate = String.format("%d", allowanceDTO.khaDailyRate);
        model.khaDays = String.format("%d", allowanceDTO.khaDays);
        model.khaAmount = String.format("%d", allowanceDTO.khaDailyRate * allowanceDTO.khaDays);
        model.overtimeAllowanceId = allowanceDTO.iD;
        model.totalAmountLong = Math.max(allowanceDTO.totalAmount, 0);
        model.netAmountLong = Math.max(allowanceDTO.netAmount, 0);
        model.revenueStampDeductionLong = Math.max(allowanceDTO.revenueStampDeduction, 0);
        if (model.totalAmountLong == 0) {
            model.netAmountLong = 0;
            model.revenueStampDeductionLong = 0;
        }
        model.totalAmount = String.format("%d", model.totalAmountLong);
        model.netAmount = String.format("%d", model.netAmountLong);
        model.revenueStampDeduction = String.format("%d", model.revenueStampDeductionLong);
    }

    public static Overtime_allowanceModel mergeSameEmployeeIdModels(List<Overtime_allowanceModel> modelList) {
        if (modelList == null || modelList.isEmpty()) {
            boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
            throw new IllegalArgumentException(isLangEn ? "Invalid Data In database" : "ডাটাবেজে তথ্য সঠিক নেই");
        }
        Overtime_allowanceModel mergedModel = modelList.get(0);
        int mergedOvertimeByDayOfMonthSize = mergedModel.overtimeTypes.size();
        for (int i = 0; i < mergedOvertimeByDayOfMonthSize; ++i) {
            String overtimeTypeString = OvertimeType.NONE.getValue();
            for (Overtime_allowanceModel model : modelList) {
                boolean isNonNoneValue = i < model.overtimeTypes.size()
                                         && OvertimeType.isNonNoneValue(model.overtimeTypes.get(i));
                if (isNonNoneValue) {
                    overtimeTypeString = model.overtimeTypes.get(i);
                    break;
                }
            }
            mergedModel.overtimeTypes.set(i, overtimeTypeString);
        }
        return mergedModel;
    }

    public static final List<Object> excelTitleRow = Arrays.asList(
            "ক্র. নং", "নাম", "পদবী", "মোবাইল নম্বর", "সঞ্চয়ী হিসাব নম্বর", "টাকার পরিমাণ", "রাজস্ব স্ট্যাম্প বাবদ কর্তন", "নীট দাবী"
    );

    public List<Object> getExcelRow(int index) {
        List<Object> excelRow = new ArrayList<>();
        excelRow.add(StringUtils.convertToBanNumber(String.format("%d", index + 1)));
        excelRow.add(name);
        excelRow.add(organogramName);
        excelRow.add(StringUtils.convertToEngNumber(mobileNumber));
        excelRow.add(StringUtils.convertToEngNumber(savingAccountNumber));
        excelRow.add(totalAmountLong);
        excelRow.add(revenueStampDeductionLong);
        excelRow.add(netAmountLong);
        return excelRow;
    }
}
