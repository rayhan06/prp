package overtime_allowance;

import office_unit_organograms.OfficeUnitOrganogramsRepository;
import overtime_bill.OvertimeType;
import util.CommonDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


public class Overtime_allowanceDTO extends CommonDTO {
    public long overtimeBillId = -1;
    public long budgetRegisterId = -1;
    public long billRegisterId = -1;
    public long budgetOfficeId = -1;
    public long allowanceEmployeeInfoId = -1;
    public String designationPrefix = "";
    public int serialNo = 0;
    public long employeeRecordsId = -1;
    public long officeUnitId = -1;
    public long organogramId = -1;
    public long kaDailyRate = 0;
    public long kaDays = 0;
    public long khaDailyRate = 0;
    public long khaDays = 0;
    public List<OvertimeType> overtimeTypes = new ArrayList<>();
    public long totalAmount = -1;
    public int revenueStampDeduction = -1;
    public long netAmount = -1;
    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionDate = -1;

    public static Comparator<Overtime_allowanceDTO> compareOfficeUnitThenOrganogramOrderValue
            = Comparator.comparingLong((Overtime_allowanceDTO dto) -> dto.officeUnitId)
                        .thenComparing(dto -> OfficeUnitOrganogramsRepository.getInstance().getOrderValueById(dto.organogramId));

    public static Comparator<Overtime_allowanceDTO> orderingComparator = Comparator.comparingInt(dto -> dto.serialNo);

    public String getOvertimeTypesString() {
        if (overtimeTypes == null) {
            return "";
        }
        return overtimeTypes.stream()
                            .map(OvertimeType::getValue)
                            .collect(Collectors.joining(","));
    }

    public static List<OvertimeType> getOvertimeTypesFromString(String overtimeTypesString) {
        if (overtimeTypesString == null || overtimeTypesString.isEmpty()) {
            return new ArrayList<>();
        }
        return Arrays.stream(overtimeTypesString.split(","))
                     .map(OvertimeType::getByValue)
                     .collect(Collectors.toList());
    }


    public void calculateAndSetTotalAmount() {
        totalAmount = kaDailyRate * kaDays + khaDailyRate * khaDays;
    }

    public List<String> getOvertimeTypeStringValueList() {
        if (overtimeTypes == null) {
            return new ArrayList<>();
        }
        return overtimeTypes.stream()
                            .map(OvertimeType::getValue)
                            .collect(Collectors.toList());
    }

    public void setOvertimeTypesFromStringValues(List<String> overtimeTypeStringValues) {
        if (overtimeTypeStringValues == null) {
            overtimeTypes = new ArrayList<>();
            return;
        }
        overtimeTypes = overtimeTypeStringValues.stream()
                                                .map(OvertimeType::getByValue)
                                                .collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "Overtime_allowanceDTO{" +
               "iD=" + iD +
               ", overtimeBillId=" + overtimeBillId +
               ", budgetRegisterId=" + budgetRegisterId +
               ", billRegisterId=" + billRegisterId +
               ", budgetOfficeId=" + budgetOfficeId +
               ", allowanceEmployeeInfoId=" + allowanceEmployeeInfoId +
               ", employeeRecordsId=" + employeeRecordsId +
               ", officeUnitId=" + officeUnitId +
               ", organogramId=" + organogramId +
               ", kaDailyRate=" + kaDailyRate +
               ", kaDays=" + kaDays +
               ", khaDailyRate=" + khaDailyRate +
               ", khaDays=" + khaDays +
               ", overtimeTypes='" + overtimeTypes + "'" +
               ", totalAmount=" + totalAmount +
               ", revenueStampDeduction=" + revenueStampDeduction +
               ", netAmount=" + netAmount +
               ", modifiedBy=" + modifiedBy +
               ", insertedBy=" + insertedBy +
               ", insertionDate=" + insertionDate +
               ", isDeleted=" + isDeleted +
               ", lastModificationTime=" + lastModificationTime +
               '}';
    }
}