package overtime_allowance;

import allowance_configure.*;
import bangladehi_number_format_util.BangladeshiNumberFormatter;
import bill_register.Bill_registerServlet;
import budget.BudgetUtils;
import budget_mapping.Budget_mappingDTO;
import budget_mapping.Budget_mappingRepository;
import budget_office.Budget_officeRepository;
import budget_register.Budget_registerDAO;
import budget_register.Budget_registerDTO;
import budget_register.Budget_registerMetadata;
import budget_selection_info.BudgetSelectionInfoRepository;
import com.google.gson.Gson;
import common.ApiResponse;
import common.BaseServlet;
import common.CustomException;
import finance.CashTypeEnum;
import finance.FinanceUtil;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import ot_bill_type_config.OT_bill_type_configDTO;
import ot_bill_type_config.OT_bill_type_configRepository;
import ot_employee_type.OT_employee_typeRepository;
import overtime_bill.*;
import overtime_bill_finance.Overtime_bill_financeDAO;
import overtime_bill_finance.Overtime_bill_financeDTO;
import pb.Utils;
import pbReport.GenerateExcelReportService;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@SuppressWarnings({"Duplicates"})
@WebServlet("/Overtime_allowanceServlet")
@MultipartConfig
public class Overtime_allowanceServlet extends BaseServlet implements GenerateExcelReportService {
    private static final long serialVersionUID = 1L;
    public static final String BILL_DESCRIPTION = "%s-এ কর্মরত %s জন %s কর্মকর্তা/কর্মচারীর %s তারিখের %s অধিকাল ভাতা";
    public static final CashTypeEnum CASH_TYPE = CashTypeEnum.ALLOWANCE;
    private final Logger logger = Logger.getLogger(Overtime_allowanceServlet.class);

    @Override
    public String getTableName() {
        return Overtime_allowanceDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Overtime_allowanceServlet";
    }

    @Override
    public Overtime_allowanceDAO getCommonDAOService() {
        return Overtime_allowanceDAO.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "ajax_getOvertimeAllowanceData": {
                    Long otBillSubmissionConfigId = Utils.parseOptionalLong(
                            request.getParameter("otBillSubmissionConfigId"),
                            null,
                            null
                    );
                    Long budgetMappingId = Utils.parseOptionalLong(
                            request.getParameter("budgetMappingId"),
                            null,
                            null
                    );
                    Long economicSubCodeId = Utils.parseOptionalLong(
                            request.getParameter("economicSubCodeId"),
                            null,
                            null
                    );
                    String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
                    Map<String, Object> res = getOvertimeAllowanceData(otBillSubmissionConfigId, budgetMappingId, economicSubCodeId, language);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().println(new Gson().toJson(res));
                    return;
                }
                case "prepareBill": {
                    Long overtimeBillFinanceId = Utils.parseOptionalLong(
                            request.getParameter("overtimeBillFinanceId"),
                            null,
                            null
                    );
                    Overtime_bill_financeDTO billFinanceDTO = Overtime_bill_financeDAO.getInstance().getDTOFromID(overtimeBillFinanceId);
                    if (billFinanceDTO == null) {
                        throw new Exception("Overtime_bill_financeDTO missing for id=" + overtimeBillFinanceId);
                    }
                    List<Overtime_billDTO> billDTOs =
                            Overtime_billDAO.getInstance().findByOvertimeBillFinanceId(overtimeBillFinanceId);
                    if (billDTOs == null || billDTOs.isEmpty()) {
                        throw new FileNotFoundException("no DTOs found");
                    }

                    request.setAttribute("billFinanceDTO", billFinanceDTO);
                    request.setAttribute("billDTOs", billDTOs);

                    OT_bill_type_configDTO otBillTypeConfigDTO =
                            OT_bill_type_configRepository
                                    .getInstance()
                                    .findByBudgetMappingAndEconomicCode(billFinanceDTO.budgetMappingId, billFinanceDTO.economicSubCodeId);
                    if (otBillTypeConfigDTO == null) {
                        throw new Exception(
                                "OT_bill_type_configDTO missing for budgetMappingId=" + billFinanceDTO.budgetMappingId
                                + " economicSubCodeId=" + billFinanceDTO.economicSubCodeId
                        );
                    }
                    request.getRequestDispatcher(otBillTypeConfigDTO.financeBillViewTemplate).forward(request, response);
                    return;
                }
                case "prepareBankStatement": {
                    Long overtimeBillFinanceId = Utils.parseOptionalLong(
                            request.getParameter("overtimeBillFinanceId"),
                            null,
                            null
                    );
                    Overtime_bill_financeDTO billFinanceDTO = Overtime_bill_financeDAO.getInstance().getDTOFromID(overtimeBillFinanceId);
                    if (billFinanceDTO == null) {
                        throw new Exception("Overtime_bill_financeDTO missing for id=" + overtimeBillFinanceId);
                    }
                    request.setAttribute("allowanceModels", getOvertimeAllowanceModelForBankStatement(overtimeBillFinanceId));
                    request.setAttribute("budgetRegisterId", billFinanceDTO.budgetRegisterId);
                    request.setAttribute("overtimeBillFinanceId", overtimeBillFinanceId);
                    request.getRequestDispatcher("overtime_allowance/overtime_allowanceBankStatement.jsp").forward(request, response);
                    return;
                }
                case "prepareBankStatementXl": {
                    Long overtimeBillFinanceId = Utils.parseOptionalLong(
                            request.getParameter("overtimeBillFinanceId"),
                            null,
                            null
                    );
                    Overtime_bill_financeDTO billFinanceDTO = Overtime_bill_financeDAO.getInstance().getDTOFromID(overtimeBillFinanceId);
                    if (billFinanceDTO == null) {
                        throw new Exception("Overtime_bill_financeDTO missing for id=" + overtimeBillFinanceId);
                    }
                    Budget_registerDTO budgetRegisterDTO = Budget_registerDAO.getInstance().getDTOFromID(billFinanceDTO.budgetRegisterId);
                    List<Overtime_allowanceModel> allowanceModel = getOvertimeAllowanceModelForBankStatement(overtimeBillFinanceId);

                    List<List<Object>> excelRows =
                            IntStream.range(0, allowanceModel.size())
                                     .mapToObj(index -> allowanceModel.get(index).getExcelRow(index))
                                     .collect(Collectors.toList());
                    excelRows.add(0, Overtime_allowanceModel.excelTitleRow);

                    XSSFWorkbook workbook = createXl(excelRows, null, budgetRegisterDTO.description, "bangla", userDTO);
                    response.setContentType("application/vnd.ms-excel");
                    String fileName = "OT Bank Statement - "
                                      + Budget_officeRepository.getInstance().getText(budgetRegisterDTO.budgetOfficeId, "english")
                                      + " - "
                                      + billFinanceDTO.getBillDateText(true)
                                      + ".xlsx";
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                    workbook.write(response.getOutputStream());
                    workbook.close();
                    return;
                }
                case "missingOfficesList": {
                    if (Utils.checkPermission(userDTO, MenuConstants.OT_BILL_MISSING_OFFICES)) {
                        request.getRequestDispatcher("overtime_allowance/overtime_allowanceMissingOffices.jsp").forward(request, response);
                        return;
                    }
                }
                case "ajax_getBillNotSubmittedOffices": {
                    Long otBillSubmissionConfigId = Utils.parseOptionalLong(
                            request.getParameter("otBillSubmissionConfigId"),
                            null,
                            null
                    );
                    Long budgetOfficeId = Utils.parseOptionalLong(
                            request.getParameter("budgetOfficeId"),
                            null,
                            null
                    );
                    Long otEmployeeTypeId = Utils.parseOptionalLong(
                            request.getParameter("otEmployeeTypeId"),
                            null,
                            null
                    );

                    String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";

                    Map<String, Object> res = getBillNotSubmittedOfficeNames(otBillSubmissionConfigId, budgetOfficeId, otEmployeeTypeId, language);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().println(new Gson().toJson(res));
                    return;
                }
                case "getConfigurationPage": {
                    if (Utils.checkPermission(userDTO, MenuConstants.OVERTIME_ALLOWANCE_CONFIGURATION)) {
                        setConfigurationPageData(request);
                        request.getRequestDispatcher("overtime_allowance/overtime_allowanceConfig.jsp").forward(request, response);
                        return;
                    }
                    return;
                }
                case "prepareNote": {
                    if (Utils.checkPermission(userDTO, MenuConstants.OVERTIME_ALLOWANCE_PREPARE_NOTE)) {
                        request.getRequestDispatcher("overtime_allowance/overtime_allowancePrepareNote.jsp")
                               .forward(request, response);
                        return;
                    }
                    return;
                }
                case "ajax_prepareNote": {
                    String otBillSubmissionConfigIdsStr = request.getParameter("otBillSubmissionConfigIds");
                    List<Long> otBillSubmissionConfigIds = new ArrayList<>();
                    if (otBillSubmissionConfigIdsStr != null) {
                        otBillSubmissionConfigIds =
                                Arrays.stream(otBillSubmissionConfigIdsStr.split(","))
                                      .filter(s -> s.length() > 0)
                                      .map(s -> Utils.parseOptionalLong(s, null, null))
                                      .filter(Objects::nonNull)
                                      .collect(Collectors.toList());
                    }
                    List<Long> budgetRegisterIds = Overtime_billDAO.getInstance().getBudgetRegisterIdBySubmissionConfigId(otBillSubmissionConfigIds);
                    List<Budget_registerDTO> budgetRegisterDTOs = Budget_registerDAO.getInstance().getDTOs(budgetRegisterIds);
                    request.setAttribute("budgetRegisterDTOs", budgetRegisterDTOs);

                    request.getRequestDispatcher("overtime_allowance/overtime_allowanceNoteSummaryTable.jsp")
                           .forward(request, response);
                    return;
                }
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;

            if ("ajax_configure".equals(actionType)) {
                if (Utils.checkPermission(userDTO, MenuConstants.OVERTIME_ALLOWANCE_CONFIGURATION)) {
                    try {
                        configureOvertimeAllowance(request, userDTO);
                        ApiResponse.sendSuccessResponse(response, "Overtime_billServlet?actionType=search");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        logger.error(ex);
                        ApiResponse.sendErrorResponse(response, ex.getMessage());
                    }
                } else {
                    ApiResponse.sendErrorResponse(response, isLangEn ? "You have no permission to do this task" : "আপনার এই ক্রিয়া সম্পাদনের অনুমতি নেই");
                }
            } else {
                super.doPost(request, response);
            }
            return;
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    static class AllowanceConfigInput {
        AllowanceCatEnum allowanceCatEnum;
        Integer dailyRate;
        Integer revenueStampDeduction;
        String ordinanceText;
        Long modifiedBy;
        Long modificationTime;
    }

    private List<Overtime_allowanceModel> getOvertimeAllowanceModelForBankStatement(long overtimeBillFinanceId) throws FileNotFoundException {
        List<Overtime_billDTO> billDTOs =
                Overtime_billDAO.getInstance().findByOvertimeBillFinanceId(overtimeBillFinanceId);
        if (billDTOs == null || billDTOs.isEmpty()) {
            throw new FileNotFoundException("no DTOs found");
        }
        return billDTOs.stream()
                       .sequential()
                       .sorted(Comparator.comparingInt(billDto -> billDto.financeSerialNumber))
                       .flatMap(overtimeBillDTO ->
                               Overtime_allowanceDAO
                                       .getInstance()
                                       .findByOvertimeBillId(overtimeBillDTO.iD)
                                       .stream()
                                       .sequential()
                                       .sorted(Comparator.comparingInt(allowanceDto -> allowanceDto.serialNo))
                       ).map(dto -> new Overtime_allowanceModel(dto, "BANGLA"))
                       .collect(Collectors.toList());
    }

    private Allowance_configureDTO getOvertimeAllowanceConfig(AllowanceConfigInput allowanceConfigInput) {
        Allowance_configureDTO allowanceConfigureDTO = new Allowance_configureDTO();
        allowanceConfigureDTO.allowanceCat = allowanceConfigInput.allowanceCatEnum.getValue();
        allowanceConfigureDTO.amountType = AllowanceTypeEnum.FIXED.getValue();
        allowanceConfigureDTO.allowanceAmount = allowanceConfigInput.dailyRate;
        allowanceConfigureDTO.taxDeductionType = AllowanceTypeEnum.FIXED.getValue();
        allowanceConfigureDTO.taxDeduction = allowanceConfigInput.revenueStampDeduction;
        allowanceConfigureDTO.ordinanceText = allowanceConfigInput.ordinanceText;
        allowanceConfigureDTO.insertedBy = allowanceConfigureDTO.modifiedBy = allowanceConfigInput.modifiedBy;
        allowanceConfigureDTO.insertionTime = allowanceConfigureDTO.lastModificationTime = allowanceConfigInput.modificationTime;
        return allowanceConfigureDTO;
    }

    private void configureOvertimeAllowance(HttpServletRequest request, UserDTO userDTO) throws Exception {
        boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        AllowanceConfigInput allowanceConfigInput = new AllowanceConfigInput();
        allowanceConfigInput.modifiedBy = userDTO.ID;
        allowanceConfigInput.modificationTime = System.currentTimeMillis();

        allowanceConfigInput.allowanceCatEnum = AllowanceCatEnum.OVERTIME_A_ALLOWANCE;
        allowanceConfigInput.dailyRate = Utils.parseMandatoryInt(
                request.getParameter("kaDailyRate"),
                0,
                isLangEn ? "Enter valid ক Daily Rate" : "সঠিক ক-এর দৈনিক হার লিখুন"
        );
        allowanceConfigInput.ordinanceText = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("ordinanceText"));
        allowanceConfigInput.revenueStampDeduction = Utils.parseMandatoryInt(
                request.getParameter("revenueStampDeduction"),
                0,
                isLangEn ? "Enter valid Revenue Stamp Deduction" : "সঠিক রাজস্ব স্ট্যাম্প বাবদ কর্তন লিখুন"
        );
        Allowance_configureDTO kaAllowanceConfigureDTO = getOvertimeAllowanceConfig(allowanceConfigInput);
        Allowance_configureServlet.addOrUpdate(kaAllowanceConfigureDTO);

        allowanceConfigInput.allowanceCatEnum = AllowanceCatEnum.OVERTIME_B_ALLOWANCE;
        allowanceConfigInput.dailyRate = Utils.parseMandatoryInt(
                request.getParameter("khaDailyRate"),
                0,
                isLangEn ? "Enter valid খ Daily Rate" : "সঠিক খ-এর দৈনিক হার লিখুন"
        );
        Allowance_configureDTO khaAllowanceConfigureDTO = getOvertimeAllowanceConfig(allowanceConfigInput);
        Allowance_configureServlet.addOrUpdate(khaAllowanceConfigureDTO);
    }

    private void setConfigurationPageData(HttpServletRequest request) {
        Allowance_configureDTO kaType = Allowance_configureDAO.getInstance().getByAllowanceCatEnum(AllowanceCatEnum.OVERTIME_A_ALLOWANCE);
        if (kaType != null) {
            request.setAttribute("kaDailyRate", kaType.allowanceAmount);
            request.setAttribute("ordinanceText", kaType.ordinanceText);
            request.setAttribute("revenueStampDeduction", kaType.taxDeduction);
        }
        Allowance_configureDTO khaType = Allowance_configureDAO.getInstance().getByAllowanceCatEnum(AllowanceCatEnum.OVERTIME_B_ALLOWANCE);
        if (khaType != null) {
            request.setAttribute("khaDailyRate", khaType.allowanceAmount);
        }
    }

    private Map<String, Object> getBillNotSubmittedOfficeNames(long otBillSubmissionConfigId, long budgetOfficeId, long otEmployeeTypeId, String language) {
        boolean isLangEn = "english".equalsIgnoreCase(language);
        Map<String, Object> res = new HashMap<>();
        res.put("success", false);
        res.put("message", isLangEn ? "Server Error" : "সার্ভারে সমস্যা");
        try {
            res.put("success", true);
            res.put("message", "");
            Set<Long> submittedOfficeUnitIds =
                    Overtime_billDAO.getInstance()
                                    .findOfficeApprovedBills(budgetOfficeId, otEmployeeTypeId, otBillSubmissionConfigId)
                                    .stream()
                                    .map(overtimeBillDTO -> overtimeBillDTO.officeUnitId)
                                    .collect(Collectors.toSet());
            res.put(
                    "missingOfficesName",
                    getMissingOfficesName(budgetOfficeId, otEmployeeTypeId, isLangEn, submittedOfficeUnitIds)
            );
        } catch (Exception e) {
            logger.error(e);
        }
        return res;
    }

    private List<String> getMissingOfficesName(long budgetOfficeId, long otEmployeeTypeId, boolean isLangEn,
                                               Set<Long> addedOfficeUnitIds) {
        return OT_employee_typeRepository
                .getInstance()
                .getAllPossibleOfficeUnits(budgetOfficeId, otEmployeeTypeId)
                .stream()
                .filter(officeUnitDTO -> !addedOfficeUnitIds.contains(officeUnitDTO.iD))
                .map(officeUnitDTO -> isLangEn ? officeUnitDTO.unitNameEng : officeUnitDTO.unitNameBng)
                .collect(Collectors.toList());
    }

    private Map<String, Object> getOvertimeAllowanceData(Long otBillSubmissionConfigId, Long budgetMappingId,
                                                         Long economicSubCodeId, String language) {
        boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Map<String, Object> res = new HashMap<>();
        res.put("success", false);
        String error = null;
        if (budgetMappingId == null) {
            error = isLangEn ? "Select Operation Code" : "অপারেশন কোড বাছাই করুন";
        }
        if (economicSubCodeId == null) {
            error = isLangEn ? "Select Economic Code" : "অর্থনৈতিক কোড বাছাই করুন";
        }
        if (otBillSubmissionConfigId == null) {
            error = isLangEn ? "Select Bill Date" : "বিলের তারিখ বাছাই করুন";
        }
        if (error != null) {
            res.put("error", error);
            return res;
        }

        List<Overtime_billDTO> officeApprovedBills =
                Overtime_billDAO.getInstance()
                                .findOfficeApprovedBillsByBudgetMappingEconomicSubcode(budgetMappingId, economicSubCodeId, otBillSubmissionConfigId);

        List<Overtime_billDTO> financeBillNotPreparedDtoList =
                Overtime_billDAO.getInstance()
                                .findFinanceBillNotPreparedDtoListByBudgetMappingEconomicSubcode(officeApprovedBills);
        if (financeBillNotPreparedDtoList.isEmpty()) {
            res.put("error", isLangEn ? "No new bill has been found" : "নতুন কোন বিল পাওয়া যায়নি");
            return res;
        }
        res.put("success", true);


        long billAmount =
                financeBillNotPreparedDtoList
                        .stream()
                        .mapToLong(overtimeBillDTO -> overtimeBillDTO.billAmount)
                        .sum();
        res.put("billAmount", billAmount);
        String billAmountStr = BangladeshiNumberFormatter.getFormattedNumber(
                StringUtils.convertBanglaIfLanguageIsBangla(
                        language,
                        String.format("%d", billAmount)
                )
        );
        res.put("billAmount", billAmountStr);

        List<Overtime_billSummaryModel> overtimeBillSummaryModels =
                financeBillNotPreparedDtoList
                        .stream()
                        .sorted(Comparator.comparingInt(billDTO -> billDTO.financeSerialNumber))
                        .map(overtimeBillDTO -> new Overtime_billSummaryModel(overtimeBillDTO, language))
                        .collect(Collectors.toList());
        res.put("overtimeBillSummaryModels", overtimeBillSummaryModels);
        return res;
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return "Overtime_bill_financeServlet?actionType=view&ID=" + commonDTO.iD;
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAjaxAddRedirectURL(request, commonDTO);
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        if (!addFlag) {
            throw new CustomException("no edit support given");
        }
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        boolean isLanguageEnglish = language.equalsIgnoreCase("English");
        UserInput userInput = new UserInput();

        userInput.budgetMappingId = Utils.parseMandatoryLong(
                request.getParameter("budgetMappingId"),
                isLanguageEnglish ? "Select Operation Code" : "অপারেশন কোড বাছাই করুন"
        );
        userInput.economicSubCodeId = Utils.parseMandatoryLong(
                request.getParameter("economicSubCodeId"),
                isLanguageEnglish ? "Select Economic Code" : "অর্থনৈতিক কোড বাছাই করুন"
        );
        userInput.otBillSubmissionConfigId = Utils.parseMandatoryLong(
                request.getParameter("otBillSubmissionConfigId"),
                isLanguageEnglish ? "Select Bill Date" : "বিলের তারিখ বাছাই করুন"
        );

        List<Overtime_billDTO> financeBillNotPreparedDtoList =
                Overtime_billDAO.getInstance().findFinanceBillNotPreparedDtoListByBudgetMappingEconomicSubcode(
                        userInput.budgetMappingId,
                        userInput.economicSubCodeId,
                        userInput.otBillSubmissionConfigId
                );
        if (financeBillNotPreparedDtoList.isEmpty()) {
            throw new Exception(isLanguageEnglish ? "No valid bill found" : "কোন সঠিক বিল পাওয়া যায়নি");
        }

        userInput.modifierId = userDTO.ID;
        userInput.modificationTime = System.currentTimeMillis();
        userInput.billAmount =
                financeBillNotPreparedDtoList
                        .stream()
                        .mapToLong(overtimeBillDTO -> overtimeBillDTO.billAmount)
                        .sum();
        userInput.employeeCount =
                financeBillNotPreparedDtoList
                        .stream()
                        .mapToInt(overtimeBillDTO -> (int) overtimeBillDTO.employeeCount)
                        .sum();
        userInput.anyOvertimeBillDTO = financeBillNotPreparedDtoList.get(0);

        Utils.handleTransaction(() -> {
            Budget_registerDTO addedBudget_registerDTO = addBudgetRegisterDTO(userInput);

            userInput.budgetRegisterId = addedBudget_registerDTO.iD;
            userInput.billRegisterId = Bill_registerServlet.addBillRegister(addedBudget_registerDTO, CASH_TYPE, true).iD;
            userInput.overtimeBillFinanceDTO = addOvertimeBillFinanceDTO(userInput);

            financeBillNotPreparedDtoList.sort(Comparator.comparing(billDTO -> billDTO.officeUnitId));
            int financeSerialNumber = 1;
            for (Overtime_billDTO billDTO : financeBillNotPreparedDtoList) {
                billDTO.modifiedBy = userInput.modifierId;
                billDTO.lastModificationTime = userInput.modificationTime;
                billDTO.financeBillStatus = Overtime_billStatus.FINANCE_BILL_PREPARED.getValue();
                billDTO.financeSerialNumber = financeSerialNumber++;
                billDTO.budgetRegisterId = userInput.budgetRegisterId;
                billDTO.billRegisterId = userInput.billRegisterId;
                billDTO.overtimeBillFinanceId = userInput.overtimeBillFinanceDTO.iD;
                Overtime_billDAO.getInstance().update(billDTO);
            }
        });
        // used overtimeBillFinanceId in getAjaxAddRedirectURL to redirect to preview page
        return userInput.overtimeBillFinanceDTO;
    }

    public static String getBillDescription(UserInput userInput) {
        String budgetOfficeNameBn = Budget_mappingRepository.getInstance().getOperationTextWithoutCode("Bangla", userInput.budgetMappingId);
        String employeeTypeText = OT_employee_typeRepository.getInstance().getTextById(userInput.anyOvertimeBillDTO.otEmployeeTypeId, false);
        return String.format(
                BILL_DESCRIPTION,
                budgetOfficeNameBn,
                StringUtils.convertToBanNumber(String.valueOf(userInput.employeeCount)),
                employeeTypeText,
                userInput.anyOvertimeBillDTO.getBillDateRangeText(false),
                userInput.anyOvertimeBillDTO.getBillTypeText(false)
        );
    }

    private Overtime_bill_financeDTO addOvertimeBillFinanceDTO(UserInput userInput) throws Exception {
        Overtime_bill_financeDTO billFinanceDTO = new Overtime_bill_financeDTO();

        billFinanceDTO.budgetMappingId = userInput.budgetMappingId;
        billFinanceDTO.economicSubCodeId = userInput.economicSubCodeId;
        billFinanceDTO.otBillSubmissionConfigId = userInput.otBillSubmissionConfigId;
        billFinanceDTO.billStartDate = userInput.anyOvertimeBillDTO.billStartDate;
        billFinanceDTO.billEndDate = userInput.anyOvertimeBillDTO.billEndDate;
        billFinanceDTO.overtimeBillTypeCat = userInput.anyOvertimeBillDTO.overtimeBillTypeCat;
        billFinanceDTO.budgetRegisterId = userInput.budgetRegisterId;
        billFinanceDTO.billRegisterId = userInput.billRegisterId;
        billFinanceDTO.totalEmployeeCount = userInput.employeeCount;
        billFinanceDTO.totalBillAmount = userInput.billAmount;

        billFinanceDTO.insertionTime = billFinanceDTO.lastModificationTime = userInput.modificationTime;
        billFinanceDTO.insertedBy = billFinanceDTO.modifiedBy = userInput.modifierId;

        Overtime_bill_financeDAO.getInstance().add(billFinanceDTO);
        return billFinanceDTO;
    }

    private Budget_registerDTO addBudgetRegisterDTO(UserInput userInput) throws Exception {
        Budget_registerDTO budgetRegisterDTO = new Budget_registerDTO();

        budgetRegisterDTO.recipientName = FinanceUtil.getFinance1HeadDesignation("bangla");
        budgetRegisterDTO.issueNumber = "";
        budgetRegisterDTO.issueDate = SessionConstants.MIN_DATE;
        budgetRegisterDTO.description = getBillDescription(userInput);
        budgetRegisterDTO.billAmount = userInput.billAmount;

        Long budgetSelectionInfoId =
                BudgetSelectionInfoRepository.getInstance()
                                             .getId(BudgetUtils.getEconomicYear(userInput.anyOvertimeBillDTO.billStartDate));
        if (budgetSelectionInfoId != null) {
            budgetRegisterDTO.budgetSelectionInfoId = budgetSelectionInfoId;
        }
        Budget_mappingDTO budgetMappingDTO =
                Budget_mappingRepository.getInstance()
                                        .getById(userInput.budgetMappingId);
        if (budgetMappingDTO != null) {
            budgetRegisterDTO.budgetMappingId = budgetMappingDTO.iD;
            budgetRegisterDTO.budgetOfficeId = budgetMappingDTO.budgetOfficeId;
        }
        budgetRegisterDTO.economicSubCodeId = userInput.economicSubCodeId;

        Overtime_billBudgetRegisterMetadata metadata = new Overtime_billBudgetRegisterMetadata();
        metadata.employeeCount = userInput.employeeCount;
        metadata.billStartDate = userInput.anyOvertimeBillDTO.billStartDate;
        metadata.billEndDate = userInput.anyOvertimeBillDTO.billEndDate;
        budgetRegisterDTO.budgetRegisterMetadata = new Budget_registerMetadata(metadata);

        budgetRegisterDTO.insertionTime
                = budgetRegisterDTO.lastModificationTime
                = userInput.modificationTime;
        budgetRegisterDTO.insertedBy
                = budgetRegisterDTO.modifiedBy
                = userInput.modifierId;

        Budget_registerDAO.getInstance().add(budgetRegisterDTO);
        return budgetRegisterDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.OVERTIME_ALLOWANCE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.OVERTIME_ALLOWANCE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.OVERTIME_ALLOWANCE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Overtime_allowanceServlet.class;
    }

    static class UserInput {
        public Long budgetMappingId;
        public Long economicSubCodeId;
        public Long otBillSubmissionConfigId;
        public Overtime_bill_financeDTO overtimeBillFinanceDTO;
        Overtime_billDTO anyOvertimeBillDTO;
        public Long modificationTime;
        public Long id;
        public Long billAmount;
        public Integer employeeCount;
        public Long budgetRegisterId;
        public Long billRegisterId;
        public Long modifierId;
    }
}