package vm_fuel_request_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Vm_fuel_request_report_Servlet")
public class Vm_fuel_request_report_Servlet  extends HttpServlet {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","tr","application_date",">=","","long","","",Long.MIN_VALUE + "","application_start_date", LC.HM_START_DATE + ""},
		{"criteria","tr","application_date","<=","AND","long","","",Long.MAX_VALUE + "","application_end_date", LC.HM_END_DATE + ""},
		{"criteria","tr","approved_date",">=","AND","long","","",Long.MIN_VALUE + "","endDate", LC.HM_END_DATE + ""},
		{"criteria","tr","approved_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},
		{"criteria","tr","requester_emp_id","=","AND","String","","","any","requesterEmpId", LC.VM_FUEL_REQUEST_REPORT_WHERE_REQUESTEREMPID + ""},
		{"criteria","tr","vehicle_type","=","AND","int","","","any","vehicleType", LC.VM_FUEL_REQUEST_REPORT_WHERE_VEHICLETYPE + ""},
		{"criteria","tr","fiscal_year_id","=","AND","String","","","any","fiscalYearId", LC.VM_FUEL_REQUEST_REPORT_WHERE_FISCALYEARID + ""},
		{"criteria","tr","status","=","AND","String","","","any","status", LC.VM_FUEL_REQUEST_REPORT_WHERE_STATUS + ""},
		{"criteria","tr","isDeleted","=","AND","String","","","0","isDeleted", LC.VM_FUEL_REQUEST_REPORT_WHERE_ISDELETED + ""}
	};

	String[][] Display =
	{
		{"display","tr","vehicle_type","given_vehicle_type_check",""},
		{"display","tr","vehicle_id","reg_no",""},
		{"display","tr","vehicle_driver_assignment_id","vehicle_driver_assignment_id_check",""},
		{"display","tr","last_fuel_date","date",""},
		{"display","tr","application_date","date",""},
		{"display","tr","requester_name_bn","text",""},
		{"display","tr","approved_date","date",""},
		{"display","tr","approver_name_bn","text",""},
		{"display","tr","payment_receiver_name_bn","text",""},
		{"display","tr","status","vm_fuel_request_approval_status_check",""},
		{"display","tr","fiscal_year_id","fiscal_year_id_check",""},
		{"display","tr","vendor_id","vendor_id_check",""},
		{"display","tr","sarok_number","text",""}
	};

	String GroupBy = "";
	String OrderBY = "";

	ReportRequestHandler reportRequestHandler;

	public Vm_fuel_request_report_Servlet(){

	}

	private final ReportService reportService = new ReportService();

	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}

		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);

		sql = "vm_fuel_request tr";

		Display[0][4] = LM.getText(LC.VM_FUEL_REQUEST_REPORT_SELECT_VEHICLETYPE, loginDTO);
		Display[1][4] = LM.getText(LC.VM_FUEL_REQUEST_REPORT_SELECT_VEHICLEID, loginDTO);
		Display[2][4] = LM.getText(LC.VM_FUEL_REQUEST_REPORT_SELECT_VEHICLEDRIVERASSIGNMENTID, loginDTO);
		Display[3][4] = LM.getText(LC.VM_FUEL_REQUEST_REPORT_SELECT_LASTFUELDATE, loginDTO);
		Display[4][4] = LM.getText(LC.VM_FUEL_REQUEST_REPORT_SELECT_APPLICATIONDATE, loginDTO);
		Display[5][4] = LM.getText(LC.VM_FUEL_REQUEST_REPORT_SELECT_REQUESTERNAMEBN, loginDTO);
		Display[6][4] = LM.getText(LC.VM_FUEL_REQUEST_REPORT_SELECT_APPROVEDDATE, loginDTO);
		Display[7][4] = LM.getText(LC.VM_FUEL_REQUEST_REPORT_SELECT_APPROVERNAMEBN, loginDTO);
		Display[8][4] = LM.getText(LC.VM_FUEL_REQUEST_REPORT_SELECT_PAYMENTRECEIVERNAMEBN, loginDTO);
		Display[9][4] = LM.getText(LC.VM_FUEL_REQUEST_REPORT_SELECT_STATUS, loginDTO);
		Display[10][4] = LM.getText(LC.VM_FUEL_REQUEST_REPORT_SELECT_FISCALYEARID, loginDTO);
		Display[11][4] = LM.getText(LC.VM_FUEL_REQUEST_REPORT_SELECT_VENDORID, loginDTO);
		Display[12][4] = LM.getText(LC.VM_FUEL_REQUEST_REPORT_SELECT_SAROKNUMBER, loginDTO);


		String reportName = LM.getText(LC.VM_FUEL_REQUEST_REPORT_OTHER_VM_FUEL_REQUEST_REPORT, loginDTO);

		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);


		reportRequestHandler.handleReportGet(request, response, userDTO, "vm_fuel_request_report",
				MenuConstants.VM_FUEL_REQUEST_REPORT_DETAILS, language, reportName, "vm_fuel_request_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doGet(request, response);
	}
}
