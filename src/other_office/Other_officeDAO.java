package other_office;

import common.NameDao;

public class Other_officeDAO extends NameDao {

    private Other_officeDAO() {
        super("other_office");
    }

    private static class LazyLoader{
        static final Other_officeDAO INSTANCE = new Other_officeDAO();
    }

    public static Other_officeDAO getInstance(){
        return LazyLoader.INSTANCE;
    }
}