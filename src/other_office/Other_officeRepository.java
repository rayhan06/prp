package other_office;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import common.NameRepository;
import pb.OptionDTO;
import pb.OrderByEnum;
import pb.Utils;
import sessionmanager.SessionConstants;


public class Other_officeRepository extends NameRepository {

	private Other_officeRepository(){
		super(Other_officeDAO.getInstance(),Other_officeRepository.class);
	}

	private static class Other_officeRepositoryLoader{
		static Other_officeRepository INSTANCE = new Other_officeRepository();
	}

	public synchronized static Other_officeRepository getInstance(){
		return Other_officeRepositoryLoader.INSTANCE;
	}
	
	 public String buildOptions(String language, String selectedId, OrderByEnum orderByEnum, boolean withSelectOption, boolean isMultiSelect) {
	        List<OptionDTO> optionDTOList = null;
	        if (nameDTOList != null && nameDTOList.size() > 0) {
	            optionDTOList = nameDTOList.stream().filter(dto ->dto.iD != SessionConstants.OTHER_OFFICE_PARLIAMENT)
	                    .map(dto -> new OptionDTO(dto.nameEn, dto.nameBn, String.valueOf(dto.iD)))
	                    .collect(Collectors.toList());
	        }
	        if (optionDTOList != null && orderByEnum == OrderByEnum.DSC) {
	            Collections.reverse(optionDTOList);
	        }
	        if (isMultiSelect) {
	            return Utils.buildOptionsMultipleSelection(optionDTOList, language, selectedId);
	        } else {
	            if (withSelectOption) {
	                return Utils.buildOptions(optionDTOList, language, selectedId);
	            } else {
	                return Utils.buildOptionsWithoutSelectWithSelectId(optionDTOList, language, selectedId);
	            }
	        }
	    }
	
}


