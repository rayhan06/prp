-- auto-generated definition
create table other_office
(
    ID                   bigint unsigned null,
    name_en              varchar(255)    null,
    name_bn              varchar(255)    null,
    isDeleted            int default 0   null,
    lastModificationTime bigint          null,
    search_column        varchar(2000)   null,
    modified_by          bigint          null,
    inserted_by          bigint          null,
    insertion_date       bigint          null
) engine = MyISAM
  collate = utf8_unicode_ci;

INSERT INTO vbsequencer (table_name, next_id, table_LastModificationTime)
VALUES ('other_office', 1, 0);

INSERT INTO menu (menuID,
                  parentMenuID,
                  menuName,
                  menuNameBangla,
                  languageTextID,
                  orderIndex,
                  selectedMenuID,
                  isVisible,
                  requestMethodType,
                  hyperLink,
                  icon,
                  isAPI,
                  constantName,
                  isDeleted,
                  lastModificationTime)
VALUES (1117510,
        1139700,
        'OTHER OFFICE',
        'অন্যান্য অফিস',
        -1,
        243,
        -1,
        1,
        1,
        '',
        'fa fa-asterisk',
        0,
        'OTHER_OFFICE',
        0,
        0);

INSERT INTO menu (menuID,
                  parentMenuID,
                  menuName,
                  menuNameBangla,
                  languageTextID,
                  orderIndex,
                  selectedMenuID,
                  isVisible,
                  requestMethodType,
                  hyperLink,
                  icon,
                  isAPI,
                  constantName,
                  isDeleted,
                  lastModificationTime)
VALUES (1117511,
        1117510,
        'ADD',
        'যোগ করুন',
        -1,
        244,
        -1,
        1,
        1,
        'Other_officeServlet?actionType=getAddPage',
        'fa fa-arrows',
        0,
        'OTHER_OFFICE_ADD',
        0,
        0);

INSERT INTO menu (menuID,
                  parentMenuID,
                  menuName,
                  menuNameBangla,
                  languageTextID,
                  orderIndex,
                  selectedMenuID,
                  isVisible,
                  requestMethodType,
                  hyperLink,
                  icon,
                  isAPI,
                  constantName,
                  isDeleted,
                  lastModificationTime)
VALUES (1117512,
        1117510,
        'EDIT',
        'এডিট করুন',
        -1,
        245,
        -1,
        0,
        1,
        'Other_officeServlet?actionType=edit',
        'fa fa-arrows',
        0,
        'OTHER_OFFICE_UPDATE',
        0,
        0);

INSERT INTO
;

menu
(
    menuID,
    parentMenuID,
    menuName,
    menuNameBangla,
    languageTextID,
    orderIndex,
    selectedMenuID,
    isVisible,
    requestMethodType,
    hyperLink,
    icon,
    isAPI,
    constantName,
    isDeleted,
    lastModificationTime
)
VALUES
(
    1117513,
    1117510,
    'SEARCH',
    'খুঁজুন',
    -1,
    246,
    -1,
    1,
    1,
    'Other_officeServlet?actionType=search',
    'fa fa-arrows',
    0,
    'OTHER_OFFICE_SEARCH',
    0,
    0
);

INSERT INTO menu_permission (roleID, menuID)
VALUES (1, 1117510);

INSERT INTO menu_permission (roleID, menuID)
VALUES (10402, 1117510);

INSERT INTO menu_permission (roleID, menuID)
VALUES (1, 1117511);

INSERT INTO menu_permission (roleID, menuID)
VALUES (10402, 1117511);

INSERT INTO menu_permission (roleID, menuID)
VALUES (1, 1117512);

INSERT INTO menu_permission (roleID, menuID)
VALUES (10402, 1117512);

INSERT INTO menu_permission (roleID, menuID)
VALUES (1, 1117513);

INSERT INTO menu_permission (roleID, menuID)
VALUES (10402, 1117513);

NSERT INTO prp.language_text (
    ID,
    menuID,
    languageTextEnglish,
    languageTextBangla,
    languageConstantPrefix,
    languageConstant,
    isDeleted,
    lastModificationTime
)
VALUES
    (
        1312350,
        1117511,
        'ID',
        'আইডি',
        'OTHER_OFFICE_ADD',
        'ID',
        0,
        0
    );

INSERT INTO prp.language_text (ID,
                               menuID,
                               languageTextEnglish,
                               languageTextBangla,
                               languageConstantPrefix,
                               languageConstant,
                               isDeleted,
                               lastModificationTime)
VALUES (1312351,
        1117511,
        'English Name',
        'ইংরেজি নাম',
        'OTHER_OFFICE_ADD',
        'NAMEEN',
        0,
        1618842934978);

INSERT INTO prp.language_text (ID,
                               menuID,
                               languageTextEnglish,
                               languageTextBangla,
                               languageConstantPrefix,
                               languageConstant,
                               isDeleted,
                               lastModificationTime)
VALUES (1312352,
        1117511,
        'Bangla Name',
        'বাংলা নাম',
        'OTHER_OFFICE_ADD',
        'NAMEBN',
        0,
        0);

INSERT INTO prp.language_text (ID,
                               menuID,
                               languageTextEnglish,
                               languageTextBangla,
                               languageConstantPrefix,
                               languageConstant,
                               isDeleted,
                               lastModificationTime)
VALUES (1312353,
        1117511,
        'IsDeleted',
        'ডিলিট হয়েছে',
        'OTHER_OFFICE_ADD',
        'ISDELETED',
        0,
        0);

INSERT INTO prp.language_text (ID,
                               menuID,
                               languageTextEnglish,
                               languageTextBangla,
                               languageConstantPrefix,
                               languageConstant,
                               isDeleted,
                               lastModificationTime)
VALUES (1312354,
        1117511,
        'LastModificationTime',
        'শেষ পরিবর্তনের সময়',
        'OTHER_OFFICE_ADD',
        'LASTMODIFICATIONTIME',
        0,
        0);

INSERT INTO prp.language_text (ID,
                               menuID,
                               languageTextEnglish,
                               languageTextBangla,
                               languageConstantPrefix,
                               languageConstant,
                               isDeleted,
                               lastModificationTime)
VALUES (1312365,
        1117511,
        'OTHER OFFICE ADD',
        'অন্যান্য অফিস যোগ করুন',
        'OTHER_OFFICE_ADD',
        'OTHER_OFFICE_ADD_FORMNAME',
        0,
        0);

INSERT INTO prp.language_text (ID,
                               menuID,
                               languageTextEnglish,
                               languageTextBangla,
                               languageConstantPrefix,
                               languageConstant,
                               isDeleted,
                               lastModificationTime)
VALUES (1312366,
        1117511,
        'ADD',
        'যোগ করুন',
        'OTHER_OFFICE_ADD',
        'OTHER_OFFICE_ADD_BUTTON',
        0,
        0);

INSERT INTO prp.language_text (ID,
                               menuID,
                               languageTextEnglish,
                               languageTextBangla,
                               languageConstantPrefix,
                               languageConstant,
                               isDeleted,
                               lastModificationTime)
VALUES (1312367,
        1117511,
        'SUBMIT',
        'সাবমিট',
        'OTHER_OFFICE_ADD',
        'OTHER_OFFICE_SUBMIT_BUTTON',
        0,
        0);

INSERT INTO prp.language_text (ID,
                               menuID,
                               languageTextEnglish,
                               languageTextBangla,
                               languageConstantPrefix,
                               languageConstant,
                               isDeleted,
                               lastModificationTime)
VALUES (1312368,
        1117511,
        'CANCEL',
        'বাতিল',
        'OTHER_OFFICE_ADD',
        'OTHER_OFFICE_CANCEL_BUTTON',
        0,
        0);

INSERT INTO prp.language_text (ID,
                               menuID,
                               languageTextEnglish,
                               languageTextBangla,
                               languageConstantPrefix,
                               languageConstant,
                               isDeleted,
                               lastModificationTime)
VALUES (1312360,
        1117513,
        'ID',
        'আইডি',
        'OTHER_OFFICE_SEARCH',
        'ID',
        0,
        0);

INSERT INTO prp.language_text (ID,
                               menuID,
                               languageTextEnglish,
                               languageTextBangla,
                               languageConstantPrefix,
                               languageConstant,
                               isDeleted,
                               lastModificationTime)
VALUES (1312361,
        1117513,
        'English Name',
        'ইংরেজি নাম',
        'OTHER_OFFICE_SEARCH',
        'NAMEEN',
        0,
        1618842934978);

INSERT INTO prp.language_text (ID,
                               menuID,
                               languageTextEnglish,
                               languageTextBangla,
                               languageConstantPrefix,
                               languageConstant,
                               isDeleted,
                               lastModificationTime)
VALUES (1312362,
        1117513,
        'Bangla Name',
        'বাংলা নাম',
        'OTHER_OFFICE_SEARCH',
        'NAMEBN',
        0,
        0);

INSERT INTO prp.language_text (ID,
                               menuID,
                               languageTextEnglish,
                               languageTextBangla,
                               languageConstantPrefix,
                               languageConstant,
                               isDeleted,
                               lastModificationTime)
VALUES (1312363,
        1117513,
        'IsDeleted',
        'ডিলিট হয়েছে',
        'OTHER_OFFICE_SEARCH',
        'ISDELETED',
        0,
        0);

INSERT INTO prp.language_text (ID,
                               menuID,
                               languageTextEnglish,
                               languageTextBangla,
                               languageConstantPrefix,
                               languageConstant,
                               isDeleted,
                               lastModificationTime)
VALUES (1312364,
        1117513,
        'LastModificationTime',
        'শেষ পরিবর্তনের সময়',
        'OTHER_OFFICE_SEARCH',
        'LASTMODIFICATIONTIME',
        0,
        0);

INSERT INTO prp.language_text (ID,
                               menuID,
                               languageTextEnglish,
                               languageTextBangla,
                               languageConstantPrefix,
                               languageConstant,
                               isDeleted,
                               lastModificationTime)
VALUES (1312373,
        1117513,
        'OTHER OFFICE SEARCH',
        'অন্যান্য অফিস খুঁজুন',
        'OTHER_OFFICE_SEARCH',
        'OTHER_OFFICE_SEARCH_FORMNAME',
        0,
        0);

INSERT INTO prp.language_text (ID,
                               menuID,
                               languageTextEnglish,
                               languageTextBangla,
                               languageConstantPrefix,
                               languageConstant,
                               isDeleted,
                               lastModificationTime)
VALUES (1312374,
        1117513,
        'SEARCH',
        'অনুসন্ধান করুন',
        'OTHER_OFFICE_SEARCH',
        'OTHER_OFFICE_SEARCH_BUTTON',
        0,
        0);

INSERT INTO prp.language_text (ID,
                               menuID,
                               languageTextEnglish,
                               languageTextBangla,
                               languageConstantPrefix,
                               languageConstant,
                               isDeleted,
                               lastModificationTime)
VALUES (1312375,
        1117513,
        'DELETE',
        'মুছুন',
        'OTHER_OFFICE_SEARCH',
        'OTHER_OFFICE_DELETE_BUTTON',
        0,
        0);

INSERT INTO prp.language_text (ID,
                               menuID,
                               languageTextEnglish,
                               languageTextBangla,
                               languageConstantPrefix,
                               languageConstant,
                               isDeleted,
                               lastModificationTime)
VALUES (1312376,
        1117513,
        'EDIT',
        'পরিবর্তন',
        'OTHER_OFFICE_SEARCH',
        'OTHER_OFFICE_EDIT_BUTTON',
        0,
        1618844048735);

INSERT INTO prp.language_text (ID,
                               menuID,
                               languageTextEnglish,
                               languageTextBangla,
                               languageConstantPrefix,
                               languageConstant,
                               isDeleted,
                               lastModificationTime)
VALUES (1312377,
        1117513,
        'CANCEL',
        'বাতিল',
        'OTHER_OFFICE_SEARCH',
        'OTHER_OFFICE_CANCEL_BUTTON',
        0,
        0);

INSERT INTO prp.language_text (ID,
                               menuID,
                               languageTextEnglish,
                               languageTextBangla,
                               languageConstantPrefix,
                               languageConstant,
                               isDeleted,
                               lastModificationTime)
VALUES (1312378,
        1117513,
        'OTHER OFFICE',
        'অন্যান্য অফিস',
        'OTHER_OFFICE_SEARCH',
        'ANYFIELD',
        0,
        0);