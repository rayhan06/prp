package other_office;

import common.BaseServlet;
import common.NameDao;
import common.NameInterface;
import common.NameRepository;
import language.LC;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

@WebServlet("/Other_officeServlet")
@MultipartConfig
public class Other_officeServlet extends BaseServlet implements NameInterface {
    private final Other_officeDAO otherOfficeDAO = Other_officeDAO.getInstance();

    public String commonPartOfDispatchURL() {
        return "common/name";
    }

    @Override
    public String getTableName() {
        return otherOfficeDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Other_officeServlet";
    }

    @Override
    public NameDao getCommonDAOService() {
        return otherOfficeDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return addT(request, addFlag, userDTO, otherOfficeDAO);
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.OTHER_OFFICE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.OTHER_OFFICE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.OTHER_OFFICE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Other_officeServlet.class;
    }

    @Override
    public int getSearchTitleValue() {
        return LC.OTHER_OFFICE_SEARCH_OTHER_OFFICE_SEARCH_FORMNAME;
    }

    @Override
    public String get_p_navigatorName() {
        return SessionConstants.NAV_OTHER_OFFICE;
    }

    @Override
    public String get_p_dtoCollectionName() {
        return SessionConstants.VIEW_OTHER_OFFICE;
    }

    @Override
    public NameRepository getNameRepository() {
        return Other_officeRepository.getInstance();
    }

    @Override
    public int getAddTitleValue() {
        return LC.OTHER_OFFICE_ADD_OTHER_OFFICE_ADD_FORMNAME;
    }

    @Override
    public int getEditTitleValue() {
        return LC.OTHER_OFFICE_ADD_OTHER_OFFICE_ADD_FORMNAME;
    }

    @Override
    public void init(HttpServletRequest request) throws ServletException {
        setCommonAttributes(request, getServletName(), getCommonDAOService());
    }
}