package asset_manufacturer;


import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.*;

import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;



import repository.RepositoryManager;

import util.*;
import user.UserDTO;

public class Asset_manufacturerDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Asset_manufacturerDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Asset_manufacturerMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"company_website",
			"support_website",
			"manufacturer_contact_name",
			"support_contact_1",
			"support_contact_2",
			"support_contact_3",
			"support_email",
			"company_address",
			"image_dropzone",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Asset_manufacturerDAO()
	{
		this("asset_manufacturer");		
	}
	
	public void setSearchColumn(Asset_manufacturerDTO asset_manufacturerDTO)
	{
		asset_manufacturerDTO.searchColumn = "";
		asset_manufacturerDTO.searchColumn += asset_manufacturerDTO.nameEn + " ";
		asset_manufacturerDTO.searchColumn += asset_manufacturerDTO.nameBn + " ";
		asset_manufacturerDTO.searchColumn += asset_manufacturerDTO.manufacturerContactName + " ";
		asset_manufacturerDTO.searchColumn += asset_manufacturerDTO.supportContact1 + " ";
		asset_manufacturerDTO.searchColumn += asset_manufacturerDTO.supportContact2 + " ";
		asset_manufacturerDTO.searchColumn += asset_manufacturerDTO.supportContact3 + " ";
		asset_manufacturerDTO.searchColumn += asset_manufacturerDTO.supportEmail + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Asset_manufacturerDTO asset_manufacturerDTO = (Asset_manufacturerDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(asset_manufacturerDTO);
		if(isInsert)
		{
			ps.setObject(index++,asset_manufacturerDTO.iD);
		}
		ps.setObject(index++,asset_manufacturerDTO.nameEn);
		ps.setObject(index++,asset_manufacturerDTO.nameBn);
		ps.setObject(index++,asset_manufacturerDTO.companyWebsite);
		ps.setObject(index++,asset_manufacturerDTO.supportWebsite);
		ps.setObject(index++,asset_manufacturerDTO.manufacturerContactName);
		ps.setObject(index++,asset_manufacturerDTO.supportContact1);
		ps.setObject(index++,asset_manufacturerDTO.supportContact2);
		ps.setObject(index++,asset_manufacturerDTO.supportContact3);
		ps.setObject(index++,asset_manufacturerDTO.supportEmail);
		ps.setObject(index++,asset_manufacturerDTO.companyAddress);
		ps.setObject(index++,asset_manufacturerDTO.imageDropzone);
		ps.setObject(index++,asset_manufacturerDTO.searchColumn);
		ps.setObject(index++,asset_manufacturerDTO.insertedByUserId);
		ps.setObject(index++,asset_manufacturerDTO.insertedByOrganogramId);
		ps.setObject(index++,asset_manufacturerDTO.insertionDate);
		ps.setObject(index++,asset_manufacturerDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Asset_manufacturerDTO build(ResultSet rs)
	{
		try
		{
			Asset_manufacturerDTO asset_manufacturerDTO = new Asset_manufacturerDTO();
			asset_manufacturerDTO.iD = rs.getLong("ID");
			asset_manufacturerDTO.nameEn = rs.getString("name_en");
			asset_manufacturerDTO.nameBn = rs.getString("name_bn");
			asset_manufacturerDTO.companyWebsite = rs.getString("company_website");
			asset_manufacturerDTO.supportWebsite = rs.getString("support_website");
			asset_manufacturerDTO.manufacturerContactName = rs.getString("manufacturer_contact_name");
			asset_manufacturerDTO.supportContact1 = rs.getString("support_contact_1");
			asset_manufacturerDTO.supportContact2 = rs.getString("support_contact_2");
			asset_manufacturerDTO.supportContact3 = rs.getString("support_contact_3");
			asset_manufacturerDTO.supportEmail = rs.getString("support_email");
			asset_manufacturerDTO.companyAddress = rs.getString("company_address");
			asset_manufacturerDTO.imageDropzone = rs.getLong("image_dropzone");
			asset_manufacturerDTO.searchColumn = rs.getString("search_column");
			asset_manufacturerDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			asset_manufacturerDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			asset_manufacturerDTO.insertionDate = rs.getLong("insertion_date");
			asset_manufacturerDTO.lastModifierUser = rs.getString("last_modifier_user");
			asset_manufacturerDTO.isDeleted = rs.getInt("isDeleted");
			asset_manufacturerDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return asset_manufacturerDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

	public Asset_manufacturerDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Asset_manufacturerDTO asset_manufacturerDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return asset_manufacturerDTO;
	}

	
	public List<Asset_manufacturerDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<Asset_manufacturerDTO> getAllAsset_manufacturer (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Asset_manufacturerDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Asset_manufacturerDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name_en")
						|| str.equals("name_bn")
						|| str.equals("manufacturer_contact_name")
						|| str.equals("support_contact_1")
						|| str.equals("support_contact_2")
						|| str.equals("support_contact_3")
						|| str.equals("support_email")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".name_en like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("name_bn"))
					{
						AllFieldSql += "" + tableName + ".name_bn like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("manufacturer_contact_name"))
					{
						AllFieldSql += "" + tableName + ".manufacturer_contact_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("support_contact_1"))
					{
						AllFieldSql += "" + tableName + ".support_contact_1 like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("support_contact_2"))
					{
						AllFieldSql += "" + tableName + ".support_contact_2 like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("support_contact_3"))
					{
						AllFieldSql += "" + tableName + ".support_contact_3 like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("support_email"))
					{
						AllFieldSql += "" + tableName + ".support_email like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	