package asset_manufacturer;
import java.util.*; 
import util.*; 


public class Asset_manufacturerDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
    public String companyWebsite = "";
    public String supportWebsite = "";
    public String manufacturerContactName = "";
    public String supportContact1 = "";
    public String supportContact2 = "";
    public String supportContact3 = "";
    public String supportEmail = "";
    public String companyAddress = "";
	public long imageDropzone = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	
	
    @Override
	public String toString() {
            return "$Asset_manufacturerDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " companyWebsite = " + companyWebsite +
            " supportWebsite = " + supportWebsite +
            " manufacturerContactName = " + manufacturerContactName +
            " supportContact1 = " + supportContact1 +
            " supportContact2 = " + supportContact2 +
            " supportContact3 = " + supportContact3 +
            " supportEmail = " + supportEmail +
            " companyAddress = " + companyAddress +
            " imageDropzone = " + imageDropzone +
            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}