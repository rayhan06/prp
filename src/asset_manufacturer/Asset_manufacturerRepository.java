package asset_manufacturer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Asset_manufacturerRepository implements Repository {
	Asset_manufacturerDAO asset_manufacturerDAO = null;
	
	public void setDAO(Asset_manufacturerDAO asset_manufacturerDAO)
	{
		this.asset_manufacturerDAO = asset_manufacturerDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Asset_manufacturerRepository.class);
	Map<Long, Asset_manufacturerDTO>mapOfAsset_manufacturerDTOToiD;
	Map<String, Set<Asset_manufacturerDTO> >mapOfAsset_manufacturerDTOTonameEn;
	Map<String, Set<Asset_manufacturerDTO> >mapOfAsset_manufacturerDTOTonameBn;
	Map<String, Set<Asset_manufacturerDTO> >mapOfAsset_manufacturerDTOTocompanyWebsite;
	Map<String, Set<Asset_manufacturerDTO> >mapOfAsset_manufacturerDTOTosupportWebsite;
	Map<String, Set<Asset_manufacturerDTO> >mapOfAsset_manufacturerDTOTomanufacturerContactName;
	Map<String, Set<Asset_manufacturerDTO> >mapOfAsset_manufacturerDTOTosupportContact1;
	Map<String, Set<Asset_manufacturerDTO> >mapOfAsset_manufacturerDTOTosupportContact2;
	Map<String, Set<Asset_manufacturerDTO> >mapOfAsset_manufacturerDTOTosupportContact3;
	Map<String, Set<Asset_manufacturerDTO> >mapOfAsset_manufacturerDTOTosupportEmail;
	Map<String, Set<Asset_manufacturerDTO> >mapOfAsset_manufacturerDTOTocompanyAddress;
	Map<Long, Set<Asset_manufacturerDTO> >mapOfAsset_manufacturerDTOToimageDropzone;
	Map<String, Set<Asset_manufacturerDTO> >mapOfAsset_manufacturerDTOTosearchColumn;
	Map<Long, Set<Asset_manufacturerDTO> >mapOfAsset_manufacturerDTOToinsertedByUserId;
	Map<Long, Set<Asset_manufacturerDTO> >mapOfAsset_manufacturerDTOToinsertedByOrganogramId;
	Map<Long, Set<Asset_manufacturerDTO> >mapOfAsset_manufacturerDTOToinsertionDate;
	Map<String, Set<Asset_manufacturerDTO> >mapOfAsset_manufacturerDTOTolastModifierUser;
	Map<Long, Set<Asset_manufacturerDTO> >mapOfAsset_manufacturerDTOTolastModificationTime;


	static Asset_manufacturerRepository instance = null;  
	private Asset_manufacturerRepository(){
		mapOfAsset_manufacturerDTOToiD = new ConcurrentHashMap<>();
		mapOfAsset_manufacturerDTOTonameEn = new ConcurrentHashMap<>();
		mapOfAsset_manufacturerDTOTonameBn = new ConcurrentHashMap<>();
		mapOfAsset_manufacturerDTOTocompanyWebsite = new ConcurrentHashMap<>();
		mapOfAsset_manufacturerDTOTosupportWebsite = new ConcurrentHashMap<>();
		mapOfAsset_manufacturerDTOTomanufacturerContactName = new ConcurrentHashMap<>();
		mapOfAsset_manufacturerDTOTosupportContact1 = new ConcurrentHashMap<>();
		mapOfAsset_manufacturerDTOTosupportContact2 = new ConcurrentHashMap<>();
		mapOfAsset_manufacturerDTOTosupportContact3 = new ConcurrentHashMap<>();
		mapOfAsset_manufacturerDTOTosupportEmail = new ConcurrentHashMap<>();
		mapOfAsset_manufacturerDTOTocompanyAddress = new ConcurrentHashMap<>();
		mapOfAsset_manufacturerDTOToimageDropzone = new ConcurrentHashMap<>();
		mapOfAsset_manufacturerDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfAsset_manufacturerDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfAsset_manufacturerDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfAsset_manufacturerDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfAsset_manufacturerDTOTolastModifierUser = new ConcurrentHashMap<>();
		mapOfAsset_manufacturerDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Asset_manufacturerRepository getInstance(){
		if (instance == null){
			instance = new Asset_manufacturerRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(asset_manufacturerDAO == null)
		{
			return;
		}
		try {
			List<Asset_manufacturerDTO> asset_manufacturerDTOs = asset_manufacturerDAO.getAllAsset_manufacturer(reloadAll);
			for(Asset_manufacturerDTO asset_manufacturerDTO : asset_manufacturerDTOs) {
				Asset_manufacturerDTO oldAsset_manufacturerDTO = getAsset_manufacturerDTOByID(asset_manufacturerDTO.iD);
				if( oldAsset_manufacturerDTO != null ) {
					mapOfAsset_manufacturerDTOToiD.remove(oldAsset_manufacturerDTO.iD);
				
					if(mapOfAsset_manufacturerDTOTonameEn.containsKey(oldAsset_manufacturerDTO.nameEn)) {
						mapOfAsset_manufacturerDTOTonameEn.get(oldAsset_manufacturerDTO.nameEn).remove(oldAsset_manufacturerDTO);
					}
					if(mapOfAsset_manufacturerDTOTonameEn.get(oldAsset_manufacturerDTO.nameEn).isEmpty()) {
						mapOfAsset_manufacturerDTOTonameEn.remove(oldAsset_manufacturerDTO.nameEn);
					}
					
					if(mapOfAsset_manufacturerDTOTonameBn.containsKey(oldAsset_manufacturerDTO.nameBn)) {
						mapOfAsset_manufacturerDTOTonameBn.get(oldAsset_manufacturerDTO.nameBn).remove(oldAsset_manufacturerDTO);
					}
					if(mapOfAsset_manufacturerDTOTonameBn.get(oldAsset_manufacturerDTO.nameBn).isEmpty()) {
						mapOfAsset_manufacturerDTOTonameBn.remove(oldAsset_manufacturerDTO.nameBn);
					}
					
					if(mapOfAsset_manufacturerDTOTocompanyWebsite.containsKey(oldAsset_manufacturerDTO.companyWebsite)) {
						mapOfAsset_manufacturerDTOTocompanyWebsite.get(oldAsset_manufacturerDTO.companyWebsite).remove(oldAsset_manufacturerDTO);
					}
					if(mapOfAsset_manufacturerDTOTocompanyWebsite.get(oldAsset_manufacturerDTO.companyWebsite).isEmpty()) {
						mapOfAsset_manufacturerDTOTocompanyWebsite.remove(oldAsset_manufacturerDTO.companyWebsite);
					}
					
					if(mapOfAsset_manufacturerDTOTosupportWebsite.containsKey(oldAsset_manufacturerDTO.supportWebsite)) {
						mapOfAsset_manufacturerDTOTosupportWebsite.get(oldAsset_manufacturerDTO.supportWebsite).remove(oldAsset_manufacturerDTO);
					}
					if(mapOfAsset_manufacturerDTOTosupportWebsite.get(oldAsset_manufacturerDTO.supportWebsite).isEmpty()) {
						mapOfAsset_manufacturerDTOTosupportWebsite.remove(oldAsset_manufacturerDTO.supportWebsite);
					}
					
					if(mapOfAsset_manufacturerDTOTomanufacturerContactName.containsKey(oldAsset_manufacturerDTO.manufacturerContactName)) {
						mapOfAsset_manufacturerDTOTomanufacturerContactName.get(oldAsset_manufacturerDTO.manufacturerContactName).remove(oldAsset_manufacturerDTO);
					}
					if(mapOfAsset_manufacturerDTOTomanufacturerContactName.get(oldAsset_manufacturerDTO.manufacturerContactName).isEmpty()) {
						mapOfAsset_manufacturerDTOTomanufacturerContactName.remove(oldAsset_manufacturerDTO.manufacturerContactName);
					}
					
					if(mapOfAsset_manufacturerDTOTosupportContact1.containsKey(oldAsset_manufacturerDTO.supportContact1)) {
						mapOfAsset_manufacturerDTOTosupportContact1.get(oldAsset_manufacturerDTO.supportContact1).remove(oldAsset_manufacturerDTO);
					}
					if(mapOfAsset_manufacturerDTOTosupportContact1.get(oldAsset_manufacturerDTO.supportContact1).isEmpty()) {
						mapOfAsset_manufacturerDTOTosupportContact1.remove(oldAsset_manufacturerDTO.supportContact1);
					}
					
					if(mapOfAsset_manufacturerDTOTosupportContact2.containsKey(oldAsset_manufacturerDTO.supportContact2)) {
						mapOfAsset_manufacturerDTOTosupportContact2.get(oldAsset_manufacturerDTO.supportContact2).remove(oldAsset_manufacturerDTO);
					}
					if(mapOfAsset_manufacturerDTOTosupportContact2.get(oldAsset_manufacturerDTO.supportContact2).isEmpty()) {
						mapOfAsset_manufacturerDTOTosupportContact2.remove(oldAsset_manufacturerDTO.supportContact2);
					}
					
					if(mapOfAsset_manufacturerDTOTosupportContact3.containsKey(oldAsset_manufacturerDTO.supportContact3)) {
						mapOfAsset_manufacturerDTOTosupportContact3.get(oldAsset_manufacturerDTO.supportContact3).remove(oldAsset_manufacturerDTO);
					}
					if(mapOfAsset_manufacturerDTOTosupportContact3.get(oldAsset_manufacturerDTO.supportContact3).isEmpty()) {
						mapOfAsset_manufacturerDTOTosupportContact3.remove(oldAsset_manufacturerDTO.supportContact3);
					}
					
					if(mapOfAsset_manufacturerDTOTosupportEmail.containsKey(oldAsset_manufacturerDTO.supportEmail)) {
						mapOfAsset_manufacturerDTOTosupportEmail.get(oldAsset_manufacturerDTO.supportEmail).remove(oldAsset_manufacturerDTO);
					}
					if(mapOfAsset_manufacturerDTOTosupportEmail.get(oldAsset_manufacturerDTO.supportEmail).isEmpty()) {
						mapOfAsset_manufacturerDTOTosupportEmail.remove(oldAsset_manufacturerDTO.supportEmail);
					}
					
					if(mapOfAsset_manufacturerDTOTocompanyAddress.containsKey(oldAsset_manufacturerDTO.companyAddress)) {
						mapOfAsset_manufacturerDTOTocompanyAddress.get(oldAsset_manufacturerDTO.companyAddress).remove(oldAsset_manufacturerDTO);
					}
					if(mapOfAsset_manufacturerDTOTocompanyAddress.get(oldAsset_manufacturerDTO.companyAddress).isEmpty()) {
						mapOfAsset_manufacturerDTOTocompanyAddress.remove(oldAsset_manufacturerDTO.companyAddress);
					}
					
					if(mapOfAsset_manufacturerDTOToimageDropzone.containsKey(oldAsset_manufacturerDTO.imageDropzone)) {
						mapOfAsset_manufacturerDTOToimageDropzone.get(oldAsset_manufacturerDTO.imageDropzone).remove(oldAsset_manufacturerDTO);
					}
					if(mapOfAsset_manufacturerDTOToimageDropzone.get(oldAsset_manufacturerDTO.imageDropzone).isEmpty()) {
						mapOfAsset_manufacturerDTOToimageDropzone.remove(oldAsset_manufacturerDTO.imageDropzone);
					}
					
					if(mapOfAsset_manufacturerDTOTosearchColumn.containsKey(oldAsset_manufacturerDTO.searchColumn)) {
						mapOfAsset_manufacturerDTOTosearchColumn.get(oldAsset_manufacturerDTO.searchColumn).remove(oldAsset_manufacturerDTO);
					}
					if(mapOfAsset_manufacturerDTOTosearchColumn.get(oldAsset_manufacturerDTO.searchColumn).isEmpty()) {
						mapOfAsset_manufacturerDTOTosearchColumn.remove(oldAsset_manufacturerDTO.searchColumn);
					}
					
					if(mapOfAsset_manufacturerDTOToinsertedByUserId.containsKey(oldAsset_manufacturerDTO.insertedByUserId)) {
						mapOfAsset_manufacturerDTOToinsertedByUserId.get(oldAsset_manufacturerDTO.insertedByUserId).remove(oldAsset_manufacturerDTO);
					}
					if(mapOfAsset_manufacturerDTOToinsertedByUserId.get(oldAsset_manufacturerDTO.insertedByUserId).isEmpty()) {
						mapOfAsset_manufacturerDTOToinsertedByUserId.remove(oldAsset_manufacturerDTO.insertedByUserId);
					}
					
					if(mapOfAsset_manufacturerDTOToinsertedByOrganogramId.containsKey(oldAsset_manufacturerDTO.insertedByOrganogramId)) {
						mapOfAsset_manufacturerDTOToinsertedByOrganogramId.get(oldAsset_manufacturerDTO.insertedByOrganogramId).remove(oldAsset_manufacturerDTO);
					}
					if(mapOfAsset_manufacturerDTOToinsertedByOrganogramId.get(oldAsset_manufacturerDTO.insertedByOrganogramId).isEmpty()) {
						mapOfAsset_manufacturerDTOToinsertedByOrganogramId.remove(oldAsset_manufacturerDTO.insertedByOrganogramId);
					}
					
					if(mapOfAsset_manufacturerDTOToinsertionDate.containsKey(oldAsset_manufacturerDTO.insertionDate)) {
						mapOfAsset_manufacturerDTOToinsertionDate.get(oldAsset_manufacturerDTO.insertionDate).remove(oldAsset_manufacturerDTO);
					}
					if(mapOfAsset_manufacturerDTOToinsertionDate.get(oldAsset_manufacturerDTO.insertionDate).isEmpty()) {
						mapOfAsset_manufacturerDTOToinsertionDate.remove(oldAsset_manufacturerDTO.insertionDate);
					}
					
					if(mapOfAsset_manufacturerDTOTolastModifierUser.containsKey(oldAsset_manufacturerDTO.lastModifierUser)) {
						mapOfAsset_manufacturerDTOTolastModifierUser.get(oldAsset_manufacturerDTO.lastModifierUser).remove(oldAsset_manufacturerDTO);
					}
					if(mapOfAsset_manufacturerDTOTolastModifierUser.get(oldAsset_manufacturerDTO.lastModifierUser).isEmpty()) {
						mapOfAsset_manufacturerDTOTolastModifierUser.remove(oldAsset_manufacturerDTO.lastModifierUser);
					}
					
					if(mapOfAsset_manufacturerDTOTolastModificationTime.containsKey(oldAsset_manufacturerDTO.lastModificationTime)) {
						mapOfAsset_manufacturerDTOTolastModificationTime.get(oldAsset_manufacturerDTO.lastModificationTime).remove(oldAsset_manufacturerDTO);
					}
					if(mapOfAsset_manufacturerDTOTolastModificationTime.get(oldAsset_manufacturerDTO.lastModificationTime).isEmpty()) {
						mapOfAsset_manufacturerDTOTolastModificationTime.remove(oldAsset_manufacturerDTO.lastModificationTime);
					}
					
					
				}
				if(asset_manufacturerDTO.isDeleted == 0) 
				{
					
					mapOfAsset_manufacturerDTOToiD.put(asset_manufacturerDTO.iD, asset_manufacturerDTO);
				
					if( ! mapOfAsset_manufacturerDTOTonameEn.containsKey(asset_manufacturerDTO.nameEn)) {
						mapOfAsset_manufacturerDTOTonameEn.put(asset_manufacturerDTO.nameEn, new HashSet<>());
					}
					mapOfAsset_manufacturerDTOTonameEn.get(asset_manufacturerDTO.nameEn).add(asset_manufacturerDTO);
					
					if( ! mapOfAsset_manufacturerDTOTonameBn.containsKey(asset_manufacturerDTO.nameBn)) {
						mapOfAsset_manufacturerDTOTonameBn.put(asset_manufacturerDTO.nameBn, new HashSet<>());
					}
					mapOfAsset_manufacturerDTOTonameBn.get(asset_manufacturerDTO.nameBn).add(asset_manufacturerDTO);
					
					if( ! mapOfAsset_manufacturerDTOTocompanyWebsite.containsKey(asset_manufacturerDTO.companyWebsite)) {
						mapOfAsset_manufacturerDTOTocompanyWebsite.put(asset_manufacturerDTO.companyWebsite, new HashSet<>());
					}
					mapOfAsset_manufacturerDTOTocompanyWebsite.get(asset_manufacturerDTO.companyWebsite).add(asset_manufacturerDTO);
					
					if( ! mapOfAsset_manufacturerDTOTosupportWebsite.containsKey(asset_manufacturerDTO.supportWebsite)) {
						mapOfAsset_manufacturerDTOTosupportWebsite.put(asset_manufacturerDTO.supportWebsite, new HashSet<>());
					}
					mapOfAsset_manufacturerDTOTosupportWebsite.get(asset_manufacturerDTO.supportWebsite).add(asset_manufacturerDTO);
					
					if( ! mapOfAsset_manufacturerDTOTomanufacturerContactName.containsKey(asset_manufacturerDTO.manufacturerContactName)) {
						mapOfAsset_manufacturerDTOTomanufacturerContactName.put(asset_manufacturerDTO.manufacturerContactName, new HashSet<>());
					}
					mapOfAsset_manufacturerDTOTomanufacturerContactName.get(asset_manufacturerDTO.manufacturerContactName).add(asset_manufacturerDTO);
					
					if( ! mapOfAsset_manufacturerDTOTosupportContact1.containsKey(asset_manufacturerDTO.supportContact1)) {
						mapOfAsset_manufacturerDTOTosupportContact1.put(asset_manufacturerDTO.supportContact1, new HashSet<>());
					}
					mapOfAsset_manufacturerDTOTosupportContact1.get(asset_manufacturerDTO.supportContact1).add(asset_manufacturerDTO);
					
					if( ! mapOfAsset_manufacturerDTOTosupportContact2.containsKey(asset_manufacturerDTO.supportContact2)) {
						mapOfAsset_manufacturerDTOTosupportContact2.put(asset_manufacturerDTO.supportContact2, new HashSet<>());
					}
					mapOfAsset_manufacturerDTOTosupportContact2.get(asset_manufacturerDTO.supportContact2).add(asset_manufacturerDTO);
					
					if( ! mapOfAsset_manufacturerDTOTosupportContact3.containsKey(asset_manufacturerDTO.supportContact3)) {
						mapOfAsset_manufacturerDTOTosupportContact3.put(asset_manufacturerDTO.supportContact3, new HashSet<>());
					}
					mapOfAsset_manufacturerDTOTosupportContact3.get(asset_manufacturerDTO.supportContact3).add(asset_manufacturerDTO);
					
					if( ! mapOfAsset_manufacturerDTOTosupportEmail.containsKey(asset_manufacturerDTO.supportEmail)) {
						mapOfAsset_manufacturerDTOTosupportEmail.put(asset_manufacturerDTO.supportEmail, new HashSet<>());
					}
					mapOfAsset_manufacturerDTOTosupportEmail.get(asset_manufacturerDTO.supportEmail).add(asset_manufacturerDTO);
					
					if( ! mapOfAsset_manufacturerDTOTocompanyAddress.containsKey(asset_manufacturerDTO.companyAddress)) {
						mapOfAsset_manufacturerDTOTocompanyAddress.put(asset_manufacturerDTO.companyAddress, new HashSet<>());
					}
					mapOfAsset_manufacturerDTOTocompanyAddress.get(asset_manufacturerDTO.companyAddress).add(asset_manufacturerDTO);
					
					if( ! mapOfAsset_manufacturerDTOToimageDropzone.containsKey(asset_manufacturerDTO.imageDropzone)) {
						mapOfAsset_manufacturerDTOToimageDropzone.put(asset_manufacturerDTO.imageDropzone, new HashSet<>());
					}
					mapOfAsset_manufacturerDTOToimageDropzone.get(asset_manufacturerDTO.imageDropzone).add(asset_manufacturerDTO);
					
					if( ! mapOfAsset_manufacturerDTOTosearchColumn.containsKey(asset_manufacturerDTO.searchColumn)) {
						mapOfAsset_manufacturerDTOTosearchColumn.put(asset_manufacturerDTO.searchColumn, new HashSet<>());
					}
					mapOfAsset_manufacturerDTOTosearchColumn.get(asset_manufacturerDTO.searchColumn).add(asset_manufacturerDTO);
					
					if( ! mapOfAsset_manufacturerDTOToinsertedByUserId.containsKey(asset_manufacturerDTO.insertedByUserId)) {
						mapOfAsset_manufacturerDTOToinsertedByUserId.put(asset_manufacturerDTO.insertedByUserId, new HashSet<>());
					}
					mapOfAsset_manufacturerDTOToinsertedByUserId.get(asset_manufacturerDTO.insertedByUserId).add(asset_manufacturerDTO);
					
					if( ! mapOfAsset_manufacturerDTOToinsertedByOrganogramId.containsKey(asset_manufacturerDTO.insertedByOrganogramId)) {
						mapOfAsset_manufacturerDTOToinsertedByOrganogramId.put(asset_manufacturerDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfAsset_manufacturerDTOToinsertedByOrganogramId.get(asset_manufacturerDTO.insertedByOrganogramId).add(asset_manufacturerDTO);
					
					if( ! mapOfAsset_manufacturerDTOToinsertionDate.containsKey(asset_manufacturerDTO.insertionDate)) {
						mapOfAsset_manufacturerDTOToinsertionDate.put(asset_manufacturerDTO.insertionDate, new HashSet<>());
					}
					mapOfAsset_manufacturerDTOToinsertionDate.get(asset_manufacturerDTO.insertionDate).add(asset_manufacturerDTO);
					
					if( ! mapOfAsset_manufacturerDTOTolastModifierUser.containsKey(asset_manufacturerDTO.lastModifierUser)) {
						mapOfAsset_manufacturerDTOTolastModifierUser.put(asset_manufacturerDTO.lastModifierUser, new HashSet<>());
					}
					mapOfAsset_manufacturerDTOTolastModifierUser.get(asset_manufacturerDTO.lastModifierUser).add(asset_manufacturerDTO);
					
					if( ! mapOfAsset_manufacturerDTOTolastModificationTime.containsKey(asset_manufacturerDTO.lastModificationTime)) {
						mapOfAsset_manufacturerDTOTolastModificationTime.put(asset_manufacturerDTO.lastModificationTime, new HashSet<>());
					}
					mapOfAsset_manufacturerDTOTolastModificationTime.get(asset_manufacturerDTO.lastModificationTime).add(asset_manufacturerDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Asset_manufacturerDTO> getAsset_manufacturerList() {
		List <Asset_manufacturerDTO> asset_manufacturers = new ArrayList<Asset_manufacturerDTO>(this.mapOfAsset_manufacturerDTOToiD.values());
		return asset_manufacturers;
	}
	
	
	public Asset_manufacturerDTO getAsset_manufacturerDTOByID( long ID){
		return mapOfAsset_manufacturerDTOToiD.get(ID);
	}
	
	
	public List<Asset_manufacturerDTO> getAsset_manufacturerDTOByname_en(String name_en) {
		return new ArrayList<>( mapOfAsset_manufacturerDTOTonameEn.getOrDefault(name_en,new HashSet<>()));
	}
	
	
	public List<Asset_manufacturerDTO> getAsset_manufacturerDTOByname_bn(String name_bn) {
		return new ArrayList<>( mapOfAsset_manufacturerDTOTonameBn.getOrDefault(name_bn,new HashSet<>()));
	}
	
	
	public List<Asset_manufacturerDTO> getAsset_manufacturerDTOBycompany_website(String company_website) {
		return new ArrayList<>( mapOfAsset_manufacturerDTOTocompanyWebsite.getOrDefault(company_website,new HashSet<>()));
	}
	
	
	public List<Asset_manufacturerDTO> getAsset_manufacturerDTOBysupport_website(String support_website) {
		return new ArrayList<>( mapOfAsset_manufacturerDTOTosupportWebsite.getOrDefault(support_website,new HashSet<>()));
	}
	
	
	public List<Asset_manufacturerDTO> getAsset_manufacturerDTOBymanufacturer_contact_name(String manufacturer_contact_name) {
		return new ArrayList<>( mapOfAsset_manufacturerDTOTomanufacturerContactName.getOrDefault(manufacturer_contact_name,new HashSet<>()));
	}
	
	
	public List<Asset_manufacturerDTO> getAsset_manufacturerDTOBysupport_contact_1(String support_contact_1) {
		return new ArrayList<>( mapOfAsset_manufacturerDTOTosupportContact1.getOrDefault(support_contact_1,new HashSet<>()));
	}
	
	
	public List<Asset_manufacturerDTO> getAsset_manufacturerDTOBysupport_contact_2(String support_contact_2) {
		return new ArrayList<>( mapOfAsset_manufacturerDTOTosupportContact2.getOrDefault(support_contact_2,new HashSet<>()));
	}
	
	
	public List<Asset_manufacturerDTO> getAsset_manufacturerDTOBysupport_contact_3(String support_contact_3) {
		return new ArrayList<>( mapOfAsset_manufacturerDTOTosupportContact3.getOrDefault(support_contact_3,new HashSet<>()));
	}
	
	
	public List<Asset_manufacturerDTO> getAsset_manufacturerDTOBysupport_email(String support_email) {
		return new ArrayList<>( mapOfAsset_manufacturerDTOTosupportEmail.getOrDefault(support_email,new HashSet<>()));
	}
	
	
	public List<Asset_manufacturerDTO> getAsset_manufacturerDTOBycompany_address(String company_address) {
		return new ArrayList<>( mapOfAsset_manufacturerDTOTocompanyAddress.getOrDefault(company_address,new HashSet<>()));
	}
	
	
	public List<Asset_manufacturerDTO> getAsset_manufacturerDTOByimage_dropzone(long image_dropzone) {
		return new ArrayList<>( mapOfAsset_manufacturerDTOToimageDropzone.getOrDefault(image_dropzone,new HashSet<>()));
	}
	
	
	public List<Asset_manufacturerDTO> getAsset_manufacturerDTOBysearch_column(String search_column) {
		return new ArrayList<>( mapOfAsset_manufacturerDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
	}
	
	
	public List<Asset_manufacturerDTO> getAsset_manufacturerDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfAsset_manufacturerDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Asset_manufacturerDTO> getAsset_manufacturerDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfAsset_manufacturerDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Asset_manufacturerDTO> getAsset_manufacturerDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfAsset_manufacturerDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Asset_manufacturerDTO> getAsset_manufacturerDTOBylast_modifier_user(String last_modifier_user) {
		return new ArrayList<>( mapOfAsset_manufacturerDTOTolastModifierUser.getOrDefault(last_modifier_user,new HashSet<>()));
	}
	
	
	public List<Asset_manufacturerDTO> getAsset_manufacturerDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfAsset_manufacturerDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "asset_manufacturer";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


