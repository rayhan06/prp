package inbox_originator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Inbox_originatorRepository implements Repository {
	Inbox_originatorDAO inbox_originatorDAO = null;
	
	public void setDAO(Inbox_originatorDAO inbox_originatorDAO)
	{
		this.inbox_originatorDAO = inbox_originatorDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Inbox_originatorRepository.class);
	Map<Long, Inbox_originatorDTO>mapOfInbox_originatorDTOToid;
	Map<Long, Set<Inbox_originatorDTO> >mapOfInbox_originatorDTOTobsadId;
	Map<Long, Set<Inbox_originatorDTO> >mapOfInbox_originatorDTOTolastModificationTime;


	static Inbox_originatorRepository instance = null;  
	private Inbox_originatorRepository(){
		mapOfInbox_originatorDTOToid = new ConcurrentHashMap<>();
		mapOfInbox_originatorDTOTobsadId = new ConcurrentHashMap<>();
		mapOfInbox_originatorDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Inbox_originatorRepository getInstance(){
		if (instance == null){
			instance = new Inbox_originatorRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(inbox_originatorDAO == null)
		{
			return;
		}
		try {
			List<Inbox_originatorDTO> inbox_originatorDTOs = inbox_originatorDAO.getAllInbox_originator(reloadAll);
			for(Inbox_originatorDTO inbox_originatorDTO : inbox_originatorDTOs) {
				Inbox_originatorDTO oldInbox_originatorDTO = getInbox_originatorDTOByid(inbox_originatorDTO.id);
				if( oldInbox_originatorDTO != null ) {
					mapOfInbox_originatorDTOToid.remove(oldInbox_originatorDTO.id);
				
					if(mapOfInbox_originatorDTOTobsadId.containsKey(oldInbox_originatorDTO.bsadId)) {
						mapOfInbox_originatorDTOTobsadId.get(oldInbox_originatorDTO.bsadId).remove(oldInbox_originatorDTO);
					}
					if(mapOfInbox_originatorDTOTobsadId.get(oldInbox_originatorDTO.bsadId).isEmpty()) {
						mapOfInbox_originatorDTOTobsadId.remove(oldInbox_originatorDTO.bsadId);
					}
					
					if(mapOfInbox_originatorDTOTolastModificationTime.containsKey(oldInbox_originatorDTO.lastModificationTime)) {
						mapOfInbox_originatorDTOTolastModificationTime.get(oldInbox_originatorDTO.lastModificationTime).remove(oldInbox_originatorDTO);
					}
					if(mapOfInbox_originatorDTOTolastModificationTime.get(oldInbox_originatorDTO.lastModificationTime).isEmpty()) {
						mapOfInbox_originatorDTOTolastModificationTime.remove(oldInbox_originatorDTO.lastModificationTime);
					}
					
					
				}
				if(inbox_originatorDTO.isDeleted == 0) 
				{
					
					mapOfInbox_originatorDTOToid.put(inbox_originatorDTO.id, inbox_originatorDTO);
				
					if( ! mapOfInbox_originatorDTOTobsadId.containsKey(inbox_originatorDTO.bsadId)) {
						mapOfInbox_originatorDTOTobsadId.put(inbox_originatorDTO.bsadId, new HashSet<>());
					}
					mapOfInbox_originatorDTOTobsadId.get(inbox_originatorDTO.bsadId).add(inbox_originatorDTO);
					
					if( ! mapOfInbox_originatorDTOTolastModificationTime.containsKey(inbox_originatorDTO.lastModificationTime)) {
						mapOfInbox_originatorDTOTolastModificationTime.put(inbox_originatorDTO.lastModificationTime, new HashSet<>());
					}
					mapOfInbox_originatorDTOTolastModificationTime.get(inbox_originatorDTO.lastModificationTime).add(inbox_originatorDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Inbox_originatorDTO> getInbox_originatorList() {
		List <Inbox_originatorDTO> inbox_originators = new ArrayList<Inbox_originatorDTO>(this.mapOfInbox_originatorDTOToid.values());
		return inbox_originators;
	}
	
	
	public Inbox_originatorDTO getInbox_originatorDTOByid( long id){
		return mapOfInbox_originatorDTOToid.get(id);
	}
	
	
	public List<Inbox_originatorDTO> getInbox_originatorDTOBybsad_id(long bsad_id) {
		return new ArrayList<>( mapOfInbox_originatorDTOTobsadId.getOrDefault(bsad_id,new HashSet<>()));
	}
	
	
	public List<Inbox_originatorDTO> getInbox_originatorDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfInbox_originatorDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "inbox_originator";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


