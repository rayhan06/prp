package inbox_originator;
import java.util.*; 
import util.*; 


public class Inbox_originatorDTO extends CommonDTO
{

	public long id = 0;
	public long bsadId = 0;
	
	
    @Override
	public String toString() {
            return "$Inbox_originatorDTO[" +
            " id = " + id +
            " bsadId = " + bsadId +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}