package inbox_originator;
import java.util.*; 
import util.*;


public class Inbox_originatorMAPS extends CommonMaps
{	
	public Inbox_originatorMAPS(String tableName)
	{
		
		java_allfield_type_map.put("bsad_id".toLowerCase(), "Long");

		java_anyfield_search_map.put(tableName + ".bsad_id".toLowerCase(), "Long");

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("bsadId".toLowerCase(), "bsadId".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("bsad_id".toLowerCase(), "bsadId".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Bsad Id".toLowerCase(), "bsadId".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}