package am_reliable_family_member;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;

import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;
import javax.servlet.http.*;
import java.util.*;


import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;
import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Am_reliable_family_memberServlet
 */
@WebServlet("/Am_reliable_family_memberServlet")
@MultipartConfig
public class Am_reliable_family_memberServlet extends BaseServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Am_reliable_family_memberServlet.class);

    @Override
    public String getTableName() {
        return Am_reliable_family_memberDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Am_reliable_family_memberServlet";
    }

    @Override
    public Am_reliable_family_memberDAO getCommonDAOService() {
        return Am_reliable_family_memberDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.AM_RELIABLE_FAMILY_MEMBER_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.AM_RELIABLE_FAMILY_MEMBER_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.AM_RELIABLE_FAMILY_MEMBER_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Am_reliable_family_memberServlet.class;
    }
    private final Gson gson = new Gson();


	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addAm_reliable_family_member");
			String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
			Am_reliable_family_memberDTO am_reliable_family_memberDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				am_reliable_family_memberDTO = new Am_reliable_family_memberDTO();
			}
			else
			{
				am_reliable_family_memberDTO = Am_reliable_family_memberDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("employeeRecordsId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeRecordsId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_reliable_family_memberDTO.employeeRecordsId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("employeeFamilyInfoId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeFamilyInfoId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_reliable_family_memberDTO.employeeFamilyInfoId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("remarks");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("remarks = " + Value);
			if(Value != null)
			{
				am_reliable_family_memberDTO.remarks = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				am_reliable_family_memberDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				am_reliable_family_memberDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				am_reliable_family_memberDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				am_reliable_family_memberDTO.insertionDate = TimeConverter.getToday();
			}


			am_reliable_family_memberDTO.lastModifierUser = userDTO.userName;


			System.out.println("Done adding  addAm_reliable_family_member dto = " + am_reliable_family_memberDTO);
			long returnedID = -1;


			if(addFlag == true)
			{
				returnedID = Am_reliable_family_memberDAO.getInstance().add(am_reliable_family_memberDTO);
			}
			else
			{
				returnedID = Am_reliable_family_memberDAO.getInstance().update(am_reliable_family_memberDTO);
			}


			return am_reliable_family_memberDTO;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
}

