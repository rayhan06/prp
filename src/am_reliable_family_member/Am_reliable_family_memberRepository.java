package am_reliable_family_member;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Am_reliable_family_memberRepository implements Repository {
	Am_reliable_family_memberDAO am_reliable_family_memberDAO = null;
	Gson gson = null;

	static Logger logger = Logger.getLogger(Am_reliable_family_memberRepository.class);
	Map<Long, Am_reliable_family_memberDTO>mapOfAm_reliable_family_memberDTOToiD;
	Map<Long, List<Am_reliable_family_memberDTO>>mapOfAm_reliable_family_memberDTOToEmployeeRecordId;

  
	private Am_reliable_family_memberRepository(){
		gson = new Gson();
		am_reliable_family_memberDAO = Am_reliable_family_memberDAO.getInstance();
		mapOfAm_reliable_family_memberDTOToiD = new ConcurrentHashMap<>();
		mapOfAm_reliable_family_memberDTOToEmployeeRecordId = new ConcurrentHashMap<>();
		am_reliable_family_memberDAO = Am_reliable_family_memberDAO.getInstance();

		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Am_reliable_family_memberRepository INSTANCE = new Am_reliable_family_memberRepository();
    }

    public static Am_reliable_family_memberRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Am_reliable_family_memberDTO> am_reliable_family_memberDTOs = am_reliable_family_memberDAO.getAllDTOs(reloadAll);
			for(Am_reliable_family_memberDTO am_reliable_family_memberDTO : am_reliable_family_memberDTOs) {
				Am_reliable_family_memberDTO oldAm_reliable_family_memberDTO = getAm_reliable_family_memberDTOByIDWithoutClone(am_reliable_family_memberDTO.iD);
				if( oldAm_reliable_family_memberDTO != null ) {
					mapOfAm_reliable_family_memberDTOToiD.remove(oldAm_reliable_family_memberDTO.iD);

					if (mapOfAm_reliable_family_memberDTOToEmployeeRecordId.get(oldAm_reliable_family_memberDTO.employeeRecordsId) != null) {
						mapOfAm_reliable_family_memberDTOToEmployeeRecordId.get(oldAm_reliable_family_memberDTO.employeeRecordsId).remove(oldAm_reliable_family_memberDTO);
					}
				}
				if(am_reliable_family_memberDTO.isDeleted == 0) 
				{
					
					mapOfAm_reliable_family_memberDTOToiD.put(am_reliable_family_memberDTO.iD, am_reliable_family_memberDTO);

					List<Am_reliable_family_memberDTO> am_reliable_family_memberDTOS = mapOfAm_reliable_family_memberDTOToEmployeeRecordId.getOrDefault(am_reliable_family_memberDTO.employeeRecordsId, new ArrayList<>());
					am_reliable_family_memberDTOS.add(am_reliable_family_memberDTO);
					mapOfAm_reliable_family_memberDTOToEmployeeRecordId.put(am_reliable_family_memberDTO.employeeRecordsId, am_reliable_family_memberDTOS);

				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Am_reliable_family_memberDTO> getAm_reliable_family_memberList() {
		List <Am_reliable_family_memberDTO> am_reliable_family_members = new ArrayList<Am_reliable_family_memberDTO>(this.mapOfAm_reliable_family_memberDTOToiD.values());
		return am_reliable_family_members;
	}


	public Am_reliable_family_memberDTO clone(Am_reliable_family_memberDTO dto) {
		if (dto == null)return null;
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Am_reliable_family_memberDTO.class);
	}

	public List<Am_reliable_family_memberDTO> clone(List<Am_reliable_family_memberDTO> dtoList) {
		if (dtoList == null)return new ArrayList<>();
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	public Am_reliable_family_memberDTO getAm_reliable_family_memberDTOByIDWithoutClone( long ID){
		return mapOfAm_reliable_family_memberDTOToiD.get(ID);
	}
	
	public Am_reliable_family_memberDTO getAm_reliable_family_memberDTOByID( long ID){
		return clone(mapOfAm_reliable_family_memberDTOToiD.get(ID));
	}

	public List<Am_reliable_family_memberDTO> getAm_reliable_family_memberDTOByUnitID( long ID){
		return clone(mapOfAm_reliable_family_memberDTOToEmployeeRecordId.get(ID));
	}
	
	@Override
	public String getTableName() {
		return "am_reliable_family_member";
	}
}


