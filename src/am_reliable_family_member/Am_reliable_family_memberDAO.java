package am_reliable_family_member;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import user.UserDTO;
import util.*;
import pb.*;

import javax.servlet.http.HttpServletRequest;

public class Am_reliable_family_memberDAO  implements CommonDAOService<Am_reliable_family_memberDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	public Am_reliable_family_memberDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"employee_records_id",
			"employee_family_info_id",
			"remarks",
				"is_reliable",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("remarks"," and (remarks like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Am_reliable_family_memberDAO INSTANCE = new Am_reliable_family_memberDAO();
	}

	public static Am_reliable_family_memberDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Am_reliable_family_memberDTO am_reliable_family_memberDTO)
	{
		am_reliable_family_memberDTO.searchColumn = "";
		am_reliable_family_memberDTO.searchColumn += am_reliable_family_memberDTO.remarks + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Am_reliable_family_memberDTO am_reliable_family_memberDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(am_reliable_family_memberDTO);
		if(isInsert)
		{
			ps.setObject(++index,am_reliable_family_memberDTO.iD);
		}
		ps.setObject(++index,am_reliable_family_memberDTO.employeeRecordsId);
		ps.setObject(++index,am_reliable_family_memberDTO.employeeFamilyInfoId);
		ps.setObject(++index,am_reliable_family_memberDTO.remarks);
		ps.setObject(++index,am_reliable_family_memberDTO.isReliable);
		ps.setObject(++index,am_reliable_family_memberDTO.searchColumn);
		ps.setObject(++index,am_reliable_family_memberDTO.insertedByUserId);
		ps.setObject(++index,am_reliable_family_memberDTO.insertedByOrganogramId);
		ps.setObject(++index,am_reliable_family_memberDTO.insertionDate);
		ps.setObject(++index,am_reliable_family_memberDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(++index,am_reliable_family_memberDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,am_reliable_family_memberDTO.iD);
		}
	}
	
	@Override
	public Am_reliable_family_memberDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Am_reliable_family_memberDTO am_reliable_family_memberDTO = new Am_reliable_family_memberDTO();
			int i = 0;
			am_reliable_family_memberDTO.iD = rs.getLong(columnNames[i++]);
			am_reliable_family_memberDTO.employeeRecordsId = rs.getLong(columnNames[i++]);
			am_reliable_family_memberDTO.employeeFamilyInfoId = rs.getLong(columnNames[i++]);
			am_reliable_family_memberDTO.remarks = rs.getString(columnNames[i++]);
			am_reliable_family_memberDTO.isReliable = rs.getInt(columnNames[i++]);
			am_reliable_family_memberDTO.searchColumn = rs.getString(columnNames[i++]);
			am_reliable_family_memberDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			am_reliable_family_memberDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			am_reliable_family_memberDTO.insertionDate = rs.getLong(columnNames[i++]);
			am_reliable_family_memberDTO.lastModifierUser = rs.getString(columnNames[i++]);
			am_reliable_family_memberDTO.isDeleted = rs.getInt(columnNames[i++]);
			am_reliable_family_memberDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return am_reliable_family_memberDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Am_reliable_family_memberDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "am_reliable_family_member";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_reliable_family_memberDTO) commonDTO,addQuery,true);
	}


	public void bulkAddOrUpdate(HttpServletRequest request, UserDTO userDTO, long requesterEmpId) throws Exception {

    	String[] familyMemberId = request.getParameterValues("familyMemberId");
    	String[] reliableFamilyMemberRemarks = request.getParameterValues("reliableFamilyMemberRemarks");
    	String[] isReliable = request.getParameterValues("isReliableValues");


    	HashMap<Long, Am_reliable_family_memberDTO> existingReliableIdsForUpdate = new HashMap<>();

    	List<Am_reliable_family_memberDTO> existing = getByEmployeeRecordId(requesterEmpId);
		existing
				.stream()
				.forEach(dto -> {
					existingReliableIdsForUpdate.put(dto.employeeFamilyInfoId, dto);
				});

		if (familyMemberId != null) {
			for (int i=0; i<familyMemberId.length; i++) {

				long ID = Long.parseLong(familyMemberId[i]);

				if (existingReliableIdsForUpdate.get(ID) != null) {
					Am_reliable_family_memberDTO am_reliable_family_memberDTO = existingReliableIdsForUpdate.get(ID);

					am_reliable_family_memberDTO.remarks = reliableFamilyMemberRemarks[i];
					am_reliable_family_memberDTO.isReliable = Integer.parseInt(isReliable[i]);

					update(am_reliable_family_memberDTO);
				}
				else {

					Am_reliable_family_memberDTO am_reliable_family_memberDTO = new Am_reliable_family_memberDTO();

					am_reliable_family_memberDTO.employeeRecordsId = requesterEmpId;
					am_reliable_family_memberDTO.employeeFamilyInfoId = Long.parseLong(familyMemberId[i]);
					am_reliable_family_memberDTO.remarks = reliableFamilyMemberRemarks[i];
					am_reliable_family_memberDTO.isReliable = Integer.parseInt(isReliable[i]);

					am_reliable_family_memberDTO.searchColumn = "";
					am_reliable_family_memberDTO.insertedByUserId = userDTO.ID;
					am_reliable_family_memberDTO.insertedByOrganogramId = userDTO.organogramID;
					am_reliable_family_memberDTO.insertionDate = TimeConverter.getToday();
					am_reliable_family_memberDTO.lastModifierUser = userDTO.userName;

					add(am_reliable_family_memberDTO);
				}

			}
		}

	}



	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_reliable_family_memberDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }

	public List<Am_reliable_family_memberDTO> getByEmployeeRecordId(long empId){
		String sql = "select * from " + getTableName() +
				" where " +
				" employee_records_id = " + empId +
				" order by lastModificationTime desc ";
		return getDTOs(sql, this::buildObjectFromResultSet);
	}
    
}
	