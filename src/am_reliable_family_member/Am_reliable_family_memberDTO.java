package am_reliable_family_member;
import java.util.*; 
import util.*; 


public class Am_reliable_family_memberDTO extends CommonDTO
{

	public long employeeRecordsId = -1;
	public long employeeFamilyInfoId = -1;
	public int isReliable = 0;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	
	
    @Override
	public String toString() {
            return "$Am_reliable_family_memberDTO[" +
            " iD = " + iD +
            " employeeRecordsId = " + employeeRecordsId +
            " employeeFamilyInfoId = " + employeeFamilyInfoId +
            " remarks = " + remarks +
            " isReliable = " + isReliable +
            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}