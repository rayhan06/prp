package leave_reliever_mapping;

import common.EmployeeCommonDAOService;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.EmployeeFlatInfoDTO;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused", "Duplicates"})
public class Leave_reliever_mappingDAO implements EmployeeCommonDAOService<Leave_reliever_mappingDTO> {
    private static final Logger logger = Logger.getLogger(Leave_reliever_mappingDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (organogram_id,leave_reliever_1,leave_reliever_2,"
            .concat("modified_by,lastModificationTime,insertion_date,inserted_by,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery = "UPDATE {tableName} SET organogram_id=?,leave_reliever_1=?,"
            .concat("leave_reliever_2=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final String getByOrganogramId = "SELECT * FROM leave_reliever_mapping WHERE organogram_id=%d AND isDeleted=0";

    private static final Map<String, String> searchMap = new HashMap<>();

    private Leave_reliever_mappingDAO() {
        searchMap.put("organogram_id", "and (organogram_id = ?)");
        searchMap.put("leave_reliever_1", "and (leave_reliever_1 = ?)");
        searchMap.put("leave_reliever_2", "and (leave_reliever_2 = ?)");
    }

    private static class LazyLoader {
        static final Leave_reliever_mappingDAO INSTANCE = new Leave_reliever_mappingDAO();
    }

    public static Leave_reliever_mappingDAO getInstance() {
        return Leave_reliever_mappingDAO.LazyLoader.INSTANCE;
    }


    public void setSearchColumn(Leave_reliever_mappingDTO leave_reliever_mappingDTO) {
        leave_reliever_mappingDTO.searchColumn = "";
        leave_reliever_mappingDTO.searchColumn += leave_reliever_mappingDTO.organogramId + " ";
        leave_reliever_mappingDTO.searchColumn += leave_reliever_mappingDTO.leaveReliever1 + " ";
        leave_reliever_mappingDTO.searchColumn += leave_reliever_mappingDTO.leaveReliever2 + " ";
    }

    @Override
    public void set(PreparedStatement ps, Leave_reliever_mappingDTO leave_reliever_mappingDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(leave_reliever_mappingDTO);
        ps.setObject(++index, leave_reliever_mappingDTO.organogramId);
        ps.setObject(++index, leave_reliever_mappingDTO.leaveReliever1);
        ps.setObject(++index, leave_reliever_mappingDTO.leaveReliever2);
        ps.setObject(++index, leave_reliever_mappingDTO.modifiedBy);
        ps.setObject(++index, leave_reliever_mappingDTO.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, leave_reliever_mappingDTO.insertionDate);
            ps.setObject(++index, leave_reliever_mappingDTO.insertedBy);
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, leave_reliever_mappingDTO.iD);
    }

    @Override
    public Leave_reliever_mappingDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Leave_reliever_mappingDTO leave_reliever_mappingDTO = new Leave_reliever_mappingDTO();
            leave_reliever_mappingDTO.iD = rs.getLong("ID");
            leave_reliever_mappingDTO.organogramId = rs.getLong("organogram_id");
            leave_reliever_mappingDTO.leaveReliever1 = rs.getLong("leave_reliever_1");
            leave_reliever_mappingDTO.leaveReliever2 = rs.getLong("leave_reliever_2");
            leave_reliever_mappingDTO.insertionDate = rs.getLong("insertion_date");
            leave_reliever_mappingDTO.insertedBy = rs.getString("inserted_by");
            leave_reliever_mappingDTO.modifiedBy = rs.getString("modified_by");
            leave_reliever_mappingDTO.isDeleted = rs.getInt("isDeleted");
            leave_reliever_mappingDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return leave_reliever_mappingDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "leave_reliever_mapping";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Leave_reliever_mappingDTO) commonDTO, updateQuery, false);
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Leave_reliever_mappingDTO) commonDTO, addQuery, true);
    }

    public List<Leave_reliever_mappingDTO> getDtosByOrganogramId(long organogramId) {
        return getDTOs(String.format(getByOrganogramId, organogramId));
    }

    public List<Leave_reliever_mappingDTO> getAllLeave_reliever_mapping(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }


    public static EmployeeFlatInfoDTO buildFlatDTO(long organogramId) {
        EmployeeFlatInfoDTO employeeFlatInfoDTO = new EmployeeFlatInfoDTO();
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(organogramId);
        if (employeeOfficeDTO != null) {
            Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId);
            employeeFlatInfoDTO.employeeRecordsId = employeeRecordsDTO.iD;
            employeeFlatInfoDTO.employeeNameEn = employeeRecordsDTO.nameEng;
            employeeFlatInfoDTO.employeeNameBn = employeeRecordsDTO.nameBng;
            employeeFlatInfoDTO.employeeUserName = employeeRecordsDTO.employeeNumber;

            OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(organogramId);
            employeeFlatInfoDTO.officeUnitId = officeUnitOrganograms.office_unit_id;
            Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeFlatInfoDTO.officeUnitId);
            employeeFlatInfoDTO.officeNameEn = officeUnitsDTO.unitNameEng;
            employeeFlatInfoDTO.officeNameBn = officeUnitsDTO.unitNameBng;

            employeeFlatInfoDTO.organogramId = organogramId;
            employeeFlatInfoDTO.organogramNameEn = officeUnitOrganograms.designation_eng;
            employeeFlatInfoDTO.organogramNameBn = officeUnitOrganograms.designation_bng;
        }
        return employeeFlatInfoDTO;
    }
}
	