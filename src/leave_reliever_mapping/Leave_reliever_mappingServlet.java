package leave_reliever_mapping;

import common.BaseServlet;
import employee_records.EmployeeFlatInfoDTO;
import login.LoginDTO;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Leave_reliever_mappingServlet")
@MultipartConfig
public class Leave_reliever_mappingServlet extends BaseServlet {

    @Override
    public String getTableName() {
        return "leave_reliever_mapping";
    }

    @Override
    public String getServletName() {
        return "Leave_reliever_mappingServlet";
    }

    @Override
    public Leave_reliever_mappingDAO getCommonDAOService() {
        return Leave_reliever_mappingDAO.getInstance();
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        long currentTime = System.currentTimeMillis();
        boolean isLangEng = userDTO.languageID == CommonConstant.Language_ID_English;
        Leave_reliever_mappingDTO leave_reliever_mappingDTO;

        if (addFlag) {
            leave_reliever_mappingDTO = new Leave_reliever_mappingDTO();
            leave_reliever_mappingDTO.insertionDate = currentTime;
            leave_reliever_mappingDTO.insertedBy = String.valueOf(userDTO.employee_record_id);
        } else {
            leave_reliever_mappingDTO = getCommonDAOService().getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        leave_reliever_mappingDTO.modifiedBy = String.valueOf(userDTO.employee_record_id);
        leave_reliever_mappingDTO.lastModificationTime = currentTime;

        leave_reliever_mappingDTO.organogramId = Long.parseLong(Jsoup.clean(request.getParameter("organogramId"), Whitelist.simpleText()));
        if (leave_reliever_mappingDTO.organogramId <= 0)
            throw new Exception(
                    isLangEng ? "Invalid Designation"
                            : "অবৈধ ছুটি প্রার্থী"
            );

        leave_reliever_mappingDTO.leaveReliever1 = Long.parseLong(Jsoup.clean(request.getParameter("leaveReliever1"), Whitelist.simpleText()));
        if (leave_reliever_mappingDTO.leaveReliever1 <= 0)
            throw new Exception(
                    isLangEng ? "Invalid Leave Substitute 1"
                            : "অবৈধ ছুটি চলাকালীন বিকল্প ১"
            );

        leave_reliever_mappingDTO.leaveReliever2 = Long.parseLong(Jsoup.clean(request.getParameter("leaveReliever2"), Whitelist.simpleText()));
        if (leave_reliever_mappingDTO.leaveReliever2 <= 0)
            throw new Exception(
                    isLangEng ? "Invalid Leave Substitute 2"
                            : "অবৈধ ছুটি চলাকালীন বিকল্প ২"
            );

        if (addFlag) {
            getCommonDAOService().add(leave_reliever_mappingDTO);
        } else {
            getCommonDAOService().update(leave_reliever_mappingDTO);
        }
        return leave_reliever_mappingDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.LEAVE_RELIEVER_MAPPING_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.LEAVE_RELIEVER_MAPPING_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.LEAVE_RELIEVER_MAPPING_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Leave_reliever_mappingServlet.class;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            if ("getEditPage".equals(actionType)) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LEAVE_RELIEVER_MAPPING_UPDATE)) {
                    long iD = Long.parseLong(Jsoup.clean(request.getParameter("ID"), Whitelist.simpleText()));
                    setFlatInfoDTO(request, iD);
                    super.doGet(request, response);
                    return;
                }
            } else {
                super.doGet(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void setFlatInfoDTO(HttpServletRequest request, long iD) {
        Leave_reliever_mappingDTO leave_reliever_mappingDTO = Leave_reliever_mappingDAO.getInstance().getDTOFromID(iD);
        EmployeeFlatInfoDTO leaveRequesterDTO = Leave_reliever_mappingDAO.buildFlatDTO(leave_reliever_mappingDTO.organogramId);
        EmployeeFlatInfoDTO leaveReliever1DTO = Leave_reliever_mappingDAO.buildFlatDTO(leave_reliever_mappingDTO.leaveReliever1);
        EmployeeFlatInfoDTO leaveReliever2DTO = Leave_reliever_mappingDAO.buildFlatDTO(leave_reliever_mappingDTO.leaveReliever2);
        request.setAttribute("requesterDTO", leaveRequesterDTO);
        request.setAttribute("leaveReliever1DTO", leaveReliever1DTO);
        request.setAttribute("leaveReliever2DTO", leaveReliever2DTO);
    }

}