package leave_reliever_mapping;
import java.util.*;

import sessionmanager.SessionConstants;
import util.*;


public class Leave_reliever_mappingDTO extends CommonDTO
{

	public long organogramId = 0L;
	public long leaveReliever1 = 0L;
	public long leaveReliever2 = 0L;
	public long insertionDate = SessionConstants.MIN_DATE;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Leave_reliever_mappingDTO[" +
            " iD = " + iD +
            " organogramId = " + organogramId +
            " leaveReliever1 = " + leaveReliever1 +
            " leaveReliever2 = " + leaveReliever2 +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}