package leave_reliever_mapping;

import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class Leave_reliever_mappingRepository implements Repository {

    private static final Logger logger = Logger.getLogger(Leave_reliever_mappingRepository.class);
    private final Map<Long, Leave_reliever_mappingDTO> mapById;
    private final Map<Long, List<Leave_reliever_mappingDTO>> mapByorganogramId;
    private List<Leave_reliever_mappingDTO> leave_reliever_mappingDTOList;

    private Leave_reliever_mappingRepository() {
        mapById = new ConcurrentHashMap<>();
        mapByorganogramId = new ConcurrentHashMap<>();
        leave_reliever_mappingDTOList = new ArrayList<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        static Leave_reliever_mappingRepository INSTANCE = new Leave_reliever_mappingRepository();
    }

    public synchronized static Leave_reliever_mappingRepository getInstance() {
        return Leave_reliever_mappingRepository.LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        logger.debug("Leave_Reliever_Mapping loading start for reloadAll: " + reloadAll);
        List<Leave_reliever_mappingDTO> leave_reliever_mappingDTOs = Leave_reliever_mappingDAO.getInstance().getAllLeave_reliever_mapping(reloadAll);
        if (leave_reliever_mappingDTOs != null && leave_reliever_mappingDTOs.size() > 0) {
            leave_reliever_mappingDTOs.stream()
                    .peek(dto -> removeIfPresent(mapById.get(dto.iD)))
                    .filter(dto -> dto.isDeleted == 0)
                    .forEach(dto -> {
                        mapById.put(dto.iD, dto);
                        List<Leave_reliever_mappingDTO> list = mapByorganogramId.getOrDefault(dto.organogramId, new ArrayList<>());
                        list.add(dto);
                        mapByorganogramId.put(dto.organogramId, list);
                    });
            leave_reliever_mappingDTOList = new ArrayList<>(mapById.values());
            leave_reliever_mappingDTOList.sort(Comparator.comparing(o -> o.iD));
            mapByorganogramId.values().forEach(ls -> ls.sort(Comparator.comparing(o -> o.iD)));
        }
        logger.debug("Leave_Reliever_Mapping loading end for reloadAll: " + reloadAll);
    }

    private void removeIfPresent(Leave_reliever_mappingDTO oldDTO) {
        if (oldDTO == null) {
            return;
        }
        if (mapById.get(oldDTO.iD) != null) {
            mapById.remove(oldDTO.iD);
        }
        if (mapByorganogramId.get(oldDTO.organogramId) != null) {
            mapByorganogramId.remove(oldDTO.organogramId);
        }
        mapByorganogramId.values()
                .forEach(childSet -> childSet.remove(oldDTO));

    }

    public void deleteDtoById(long id) {
        Leave_reliever_mappingDTO oldLeave_reliever_mappingDTO = mapById.get(id);
        if (oldLeave_reliever_mappingDTO == null) return;

        mapById.remove(oldLeave_reliever_mappingDTO.iD);

        if (mapByorganogramId.containsKey(oldLeave_reliever_mappingDTO.organogramId)) {
            mapByorganogramId.get(oldLeave_reliever_mappingDTO.organogramId).remove(oldLeave_reliever_mappingDTO);
        }
        if (mapByorganogramId.get(oldLeave_reliever_mappingDTO.organogramId).isEmpty()) {
            mapByorganogramId.remove(oldLeave_reliever_mappingDTO.organogramId);
        }
    }

    public List<Leave_reliever_mappingDTO> getLeave_reliever_mappingList() {
        return leave_reliever_mappingDTOList;
    }

    public Leave_reliever_mappingDTO getLeave_reliever_mappingDTOByID(long ID) {
        return mapById.get(ID);
    }

    public List<Leave_reliever_mappingDTO> getLeave_reliever_mappingDTOByorganogram_id(long organogram_id) {
        if (mapByorganogramId.get(organogram_id) == null) {
            synchronized (LockManager.getLock(organogram_id + "LRMR")) {
                if (mapByorganogramId.get(organogram_id) == null) {
                    List<Leave_reliever_mappingDTO> list = Leave_reliever_mappingDAO.getInstance().getDtosByOrganogramId(organogram_id);
                    if (list != null && list.size() > 0) {
                        mapByorganogramId.put(organogram_id, list);
                        leave_reliever_mappingDTOList.addAll(list);
                        leave_reliever_mappingDTOList.sort(Comparator.comparing(o -> o.iD));
                    }
                }
            }
        }
        return new ArrayList<>(mapByorganogramId.getOrDefault(organogram_id, new ArrayList<>()));
    }

    @Override
    public String getTableName() {
        return "leave_reliever_mapping";
    }
}