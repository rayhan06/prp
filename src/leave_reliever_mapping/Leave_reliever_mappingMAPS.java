package leave_reliever_mapping;

import java.util.*;

import util.*;


public class Leave_reliever_mappingMAPS extends CommonMaps {
    public Leave_reliever_mappingMAPS(String tableName) {
        java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
        java_DTO_map.put("organogramId".toLowerCase(), "organogramId".toLowerCase());
        java_DTO_map.put("leaveReliever1".toLowerCase(), "leaveReliever1".toLowerCase());
        java_DTO_map.put("leaveReliever2".toLowerCase(), "leaveReliever2".toLowerCase());
        java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
        java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
        java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
        java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

        java_SQL_map.put("organogram_id".toLowerCase(), "organogramId".toLowerCase());
        java_SQL_map.put("leave_reliever_1".toLowerCase(), "leaveReliever1".toLowerCase());
        java_SQL_map.put("leave_reliever_2".toLowerCase(), "leaveReliever2".toLowerCase());

        java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
        java_Text_map.put("Organogram Id".toLowerCase(), "organogramId".toLowerCase());
        java_Text_map.put("Leave Reliever 1".toLowerCase(), "leaveReliever1".toLowerCase());
        java_Text_map.put("Leave Reliever 2".toLowerCase(), "leaveReliever2".toLowerCase());
        java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
        java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
        java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
        java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

    }
}