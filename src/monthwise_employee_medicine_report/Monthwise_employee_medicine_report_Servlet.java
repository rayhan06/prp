package monthwise_employee_medicine_report;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;
import login.LoginDTO;
import medical_inventory_lot.MedicalInventoryInDTO;

import office_unit_organograms.OfficeUnitOrganogramsRepository;
import pbReport.ReportService;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.ReportRequestHandler;

@WebServlet("/Monthwise_employee_medicine_report_Servlet")
public class Monthwise_employee_medicine_report_Servlet extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria;
	
	String[][] Display;
	
	
	String GroupBy = " employee_record_id, col_7 ";
	String OrderBY = " id desc ";
	
	ReportRequestHandler reportRequestHandler;
	
	public Monthwise_employee_medicine_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = 
				"    medical_transaction  ";
		

		
		ArrayList<String[]> criteriaList = new ArrayList<String[]>();
		
		String [] sCrit = {"criteria","","transaction_type","in","","String","","", MedicalInventoryInDTO.TR_RECEIVE_MEDICINE + ", " +  MedicalInventoryInDTO.TR_RETURN,"none", ""};
		criteriaList.add(sCrit);
		
		criteriaList.add(new String[]{"criteria","","medical_item_cat","=","AND","int","","","0","none", ""});
		
		
		
		String startDate = request.getParameter("startDate");
		if(startDate != null && !startDate.equalsIgnoreCase("") && !startDate.equalsIgnoreCase("0"))
		{
			criteriaList.add(new String[] {"criteria","","transaction_date",">=","AND","long","","",1 + "","startDate", LC.HM_START_DATE + ""});
			

		}
		String endDate = request.getParameter("endDate");
		if(endDate != null && !endDate.equalsIgnoreCase("")&& !endDate.equalsIgnoreCase("1651341600000"))
		{
			criteriaList.add(new String[] {"criteria","","transaction_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""});
			
		}
		
		String userName = request.getParameter("userName");
		if(userName != null && !userName.equalsIgnoreCase(""))
		{
			criteriaList.add(new String[] {"criteria","","employee_record_id","=","AND","String","","","any","userName",LC.PHARMACY_REPORT_WHERE_EMPLOYEEID + "", "userNameToEmployeeRecordId"});
		}
		
		String officeUnitId = request.getParameter("officeUnitId");
		if(officeUnitId != null && !officeUnitId.equalsIgnoreCase(""))
		{
			Set<Long> organogramsList = OfficeUnitOrganogramsRepository.getInstance().getAllDescentsOrganogramIdsInclusive(Long.parseLong(officeUnitId));
			String sOrg = "";
			int i = 0;
			for(Long officeUnitOrganogram: organogramsList)
			{
				if(i > 0)
				{
					sOrg += ", ";
				}
				sOrg += officeUnitOrganogram;
				i++;
			}
			if(!sOrg.equalsIgnoreCase(""))
			{
				criteriaList.add(new String[] {"criteria","","organogram_id","in","AND","String","","",sOrg,"userName", ""});
			}
		}
		
		String mp = request.getParameter("mp");
		if(mp != null && !mp.equalsIgnoreCase(""))
		{
			criteriaList.add(new String[] {"criteria","","user_name","like","AND","exact","","","0%","none",""});
		}
		
		Criteria = new String[criteriaList.size()][];
		int i = 0;
		for(String [] sCrtiFromList: criteriaList)
		{
			Criteria[i++] = sCrtiFromList;
		}
		
		
		Display= new String[8][4];
		Display[0] = new String[]{"display","",
				"employee_record_id",
				"erIdToOffice",
				LM.getText(LC.HM_OFFICE, loginDTO)};
		Display[1] = new String[]{"display","",
				"employee_record_id",
				"erIdToUserName",
				language.equalsIgnoreCase("english")?"Reference UserID":"রেফারেন্স ইউজারআইডি"};
		Display[2] = new String[]{"display","",
				"employee_record_id",
				"erIdToName",
				LM.getText(LC.HM_REFERENCE_EMPLOYEE, loginDTO)};
		Display[3] = new String[]{"display","",
				"employee_record_id",
				"erIdToOrganogram",
				LM.getText(LC.HM_DESIGNATION, loginDTO)};
		Display[4] = new String[]{"display","",
				"SUM(quantity)",
				"int",
				language.equalsIgnoreCase("english")?"Medicine Count":"ঔষধের সংখ্যা"};

		Display[5] = new String[]{"display","",
				"SUM(quantity * unit_price)",
				"double",
				LM.getText(LC.EMPLOYEE_MEDICAL_REPORT_SELECT_COST, loginDTO)};
		Display[6] = new String[]{"display","",
				"count(distinct prescription_details_id) ", 
				"int",
				language.equalsIgnoreCase("english")?"Prescriptions Used":"জমাকৃত প্রেসক্রিপশন"};

		Display[7] = new String[]{"display","",
				"DATE_FORMAT(FROM_UNIXTIME(transaction_date / 1000),'%Y-%m')",
				"text",
				language.equalsIgnoreCase("english")?
			    		   "Year-Month"
			    		   :
			    		   "বছর-মাস"};


		
		String reportName = language.equalsIgnoreCase("english")?
				"Monthwise Employee Medicince Report"
				:"মাসভিত্তিক কর্মচারীর ঔষধের  রিপোর্ট";
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(4, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(5, ReportRequestHandler.RIGHT_ALIGN_FLOAT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(6, ReportRequestHandler.RIGHT_ALIGN_INT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "monthwise_employee_medicine_report",
				MenuConstants.MER_DETAILS, language, reportName, "monthwise_employee_medicine_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}

}
