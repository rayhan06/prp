package recruitment_test_name;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import job_applicant_application.JobApplicantApplicationModel;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import recruitment_exam_venue.RecruitmentExamVenueItemModel;
import recruitment_seat_plan.RecruitmentSeatPlanChildDTO;
import recruitment_seat_plan.RecruitmentSeatPlanChildRepository;
import role.PermissionRepository;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;

import javax.servlet.http.*;
import java.util.*;
import java.util.stream.Collectors;


import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;
import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;


/**
 * Servlet implementation class Recruitment_test_nameServlet
 */
@WebServlet("/Recruitment_test_nameServlet")
@MultipartConfig
public class Recruitment_test_nameServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Recruitment_test_nameServlet.class);

    @Override
    public String getTableName() {
        return Recruitment_test_nameDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Recruitment_test_nameServlet";
    }

    @Override
    public Recruitment_test_nameDAO getCommonDAOService() {
        return Recruitment_test_nameDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.RECRUITMENT_TEST_NAME_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.RECRUITMENT_TEST_NAME_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.RECRUITMENT_TEST_NAME_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Recruitment_test_nameServlet.class;
    }

    private final Gson gson = new Gson();


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {

        request.setAttribute("failureMessage", "");
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        Recruitment_test_nameDTO recruitment_test_nameDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLanEng = Language.equalsIgnoreCase("English");

        if (addFlag) {
            recruitment_test_nameDTO = new Recruitment_test_nameDTO();

            recruitment_test_nameDTO.insertionDate = recruitment_test_nameDTO.lastModificationTime = System.currentTimeMillis();
            recruitment_test_nameDTO.insertedBy = recruitment_test_nameDTO.modifiedBy = String.valueOf(userDTO.ID);
        } else {
            recruitment_test_nameDTO = Recruitment_test_nameDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
            if (recruitment_test_nameDTO == null) {
                throw new Exception(isLanEng ? "Recruitment test information is not found" : "নিয়োগ পরীক্ষার তথ্য খুঁজে পাওয়া যায়নি");
            }
            recruitment_test_nameDTO.lastModificationTime = System.currentTimeMillis();
            recruitment_test_nameDTO.modifiedBy = String.valueOf(userDTO.ID);
        }

        String Value = "";

        Value = request.getParameter("nameEn");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide recruitment test english name" : "অনুগ্রহপূর্বক নিয়োগ পরীক্ষার ইংরেজি নাম দিন");
        }
        boolean hasNonEnglish =
                Value.chars()
                        .anyMatch(ch -> !(ch >= ' ' && ch <= '~'));
        if (!hasNonEnglish) {
            recruitment_test_nameDTO.nameEn = (Value);
        } else {
            throw new Exception(isLanEng ? "Please provide recruitment test english name" : "অনুগ্রহপূর্বক নিয়োগ পরীক্ষার ইংরেজি নাম দিন");
        }


        Value = request.getParameter("nameBn");

        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide recruitment test bangla name" : "অনুগ্রহপূর্বক নিয়োগ পরীক্ষার বাংলা নাম দিন");
        }

        boolean hasAllBangla = Value.chars().allMatch(Utils::isValidBanglaCharacter);
        if (!hasAllBangla) {
            throw new Exception(isLanEng ? "Please provide recruitment test bangla name" : "অনুগ্রহপূর্বক নিয়োগ পরীক্ষার বাংলা নাম দিন");
        }

        recruitment_test_nameDTO.nameBn = (Value);

        Value = request.getParameter("searchColumn");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null) {
            recruitment_test_nameDTO.searchColumn = (Value);
        } else {
        }

        logger.debug("Done adding  addRecruitment_test_name dto = " + recruitment_test_nameDTO);

        if (addFlag) {
            Recruitment_test_nameDAO.getInstance().add(recruitment_test_nameDTO);
        } else {
            Recruitment_test_nameDAO.getInstance().update(recruitment_test_nameDTO);
        }
        return recruitment_test_nameDTO;

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {

                case "updatePublishStatus":
                    RecruitmentTestNameModel model = null;
                    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.RECRUITMENT_TEST_NAME_ADD)) {
                        int curPublishStatus = Integer.parseInt((request.getParameter("curPublishStatus")));
                        Long id = Long.valueOf((request.getParameter("id")));
                        response.setContentType("application/json");
                        response.setCharacterEncoding("UTF-8");

                        int publishStatusCat = -1;
                        String statusText = "";
                        String otherStatusText = "";
                        String color = "";

                        if(curPublishStatus == Recruitment_test_nameEnum.Unpublish.getValue()){
                            publishStatusCat = Recruitment_test_nameEnum.Publish.getValue();
                        }
                        else if(curPublishStatus == Recruitment_test_nameEnum.Publish.getValue()){
                            publishStatusCat = Recruitment_test_nameEnum.Unpublish.getValue();
                        }

                        if(Recruitment_test_nameDAO.getInstance().updatePublishStatus(publishStatusCat,userDTO.fullName,id)){
                            statusText = CatRepository.getInstance().getText(Language,"recruitment_test_publish",curPublishStatus);
                            otherStatusText = CatRepository.getInstance().getText(Language,"recruitment_test_publish",publishStatusCat);
                            color = Recruitment_test_nameEnum.getColor(publishStatusCat);

                            model = new RecruitmentTestNameModel(publishStatusCat,statusText,otherStatusText,color,Language);
                            Recruitment_test_nameRepository.getInstance().reload(false);
                        }

                    }
                    response.getWriter().println(gson.toJson(model));
                    return;
                default:
                    super.doGet(request, response);
                    return;

            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }
}

