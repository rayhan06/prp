package recruitment_test_name;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import election_details.Election_detailsDTO;
import org.apache.log4j.Logger;
import com.google.gson.Gson;

import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;


public class Recruitment_test_nameRepository implements Repository {
	Recruitment_test_nameDAO recruitment_test_nameDAO = null;
	
	static Logger logger = Logger.getLogger(Recruitment_test_nameRepository.class);
	Map<Long, Recruitment_test_nameDTO>mapOfRecruitment_test_nameDTOToiD;
	Gson gson;

	private List<Recruitment_test_nameDTO> recruitment_test_nameDTOS;
  
	private Recruitment_test_nameRepository(){
		recruitment_test_nameDAO = Recruitment_test_nameDAO.getInstance();
		mapOfRecruitment_test_nameDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Recruitment_test_nameRepository INSTANCE = new Recruitment_test_nameRepository();
    }

    public static Recruitment_test_nameRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Recruitment_test_nameDTO> recruitment_test_nameDTOs = recruitment_test_nameDAO.getAllDTOs(reloadAll);
			for(Recruitment_test_nameDTO recruitment_test_nameDTO : recruitment_test_nameDTOs) {
				Recruitment_test_nameDTO oldRecruitment_test_nameDTO = getRecruitment_test_nameDTOByiD(recruitment_test_nameDTO.iD);
				if( oldRecruitment_test_nameDTO != null ) {
					mapOfRecruitment_test_nameDTOToiD.remove(oldRecruitment_test_nameDTO.iD);
				
					
				}
				if(recruitment_test_nameDTO.isDeleted == 0) 
				{
					
					mapOfRecruitment_test_nameDTOToiD.put(recruitment_test_nameDTO.iD, recruitment_test_nameDTO);
				
				}
			}
			recruitment_test_nameDTOS = new ArrayList<>(mapOfRecruitment_test_nameDTOToiD.values());
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Recruitment_test_nameDTO clone(Recruitment_test_nameDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Recruitment_test_nameDTO.class);
	}
	
	
	public List<Recruitment_test_nameDTO> getRecruitment_test_nameList() {
		List <Recruitment_test_nameDTO> recruitment_test_names = new ArrayList<Recruitment_test_nameDTO>(this.mapOfRecruitment_test_nameDTOToiD.values());
		return recruitment_test_names;
	}
	
	public List<Recruitment_test_nameDTO> copyRecruitment_test_nameList() {
		List <Recruitment_test_nameDTO> recruitment_test_names = getRecruitment_test_nameList();
		return recruitment_test_names
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Recruitment_test_nameDTO getRecruitment_test_nameDTOByiD( long iD){
		recruitment_test_nameDTOS = new ArrayList<>(mapOfRecruitment_test_nameDTOToiD.values());
		return mapOfRecruitment_test_nameDTOToiD.get(iD);
	}
	
	public Recruitment_test_nameDTO copyRecruitment_test_nameDTOByiD( long iD){
		return clone(mapOfRecruitment_test_nameDTOToiD.get(iD));
	}

	public String buildOptions(String language, Long selectedId) {
		List<OptionDTO> optionDTOList = null;
		if (recruitment_test_nameDTOS != null && recruitment_test_nameDTOS.size() > 0) {
			optionDTOList = recruitment_test_nameDTOS.stream()
					.map(Recruitment_test_nameDTO::getOptionDTO)
					.collect(Collectors.toList());
		}
		return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
	}

	
	@Override
	public String getTableName() {
		return recruitment_test_nameDAO.getTableName();
	}
}


