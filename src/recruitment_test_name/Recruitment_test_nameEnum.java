package recruitment_test_name;

import java.util.Arrays;

public enum Recruitment_test_nameEnum {
    Unpublish(1, "green"),
    Publish(2, "red");

    private final int value;
    private final String color;

    Recruitment_test_nameEnum(int value, String color) {
        this.value = value;
        this.color = color;
    }

    public int getValue() {
        return value;
    }

    public String getColor() {
        return color;
    }

    public static String getColor(int value) {
        return Arrays.stream(values()).
                filter(e->e.value == value).
                map(c -> c.color).
                findFirst().
                orElse("");
    }
}
