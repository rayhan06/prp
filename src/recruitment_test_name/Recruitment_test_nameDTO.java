package recruitment_test_name;
import java.util.*;

import pb.OptionDTO;
import util.*;


public class Recruitment_test_nameDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
    public String insertedBy = "";
    public String modifiedBy = "";
	public long insertionDate = -1;
	public int recruitmentTestNamePublishCat = 1;// by default unpublish

    public OptionDTO getOptionDTO() {

        return new OptionDTO(
                nameEn,
                nameBn,
                String.format("%d", iD)
        );
    }
	
	
    @Override
	public String toString() {
            return "$Recruitment_test_nameDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " searchColumn = " + searchColumn +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}