package recruitment_test_name;

public class RecruitmentTestNameModel {

    public String recruitmentTestNamePublishStatusCat = "";
    public String recruitmentTestNamePublishStatusText = "";
    public String recruitmentTestNamePublishOtherStatusText = "";
    public String color = "";

    public RecruitmentTestNameModel(){ }
    public RecruitmentTestNameModel(int statusCat,String text,String otherText, String clr, String language){
        boolean isLanguageEnglish = "English".equalsIgnoreCase(language);

        recruitmentTestNamePublishStatusCat = String.valueOf(statusCat);
        recruitmentTestNamePublishStatusText = text;
        recruitmentTestNamePublishOtherStatusText = otherText;
        color = clr;

    }
    @Override
    public String toString() {
        return "PiProductModel{" +
                "recruitmentTestNamePublishStatusCat='" + recruitmentTestNamePublishStatusCat + '\'' +
                "recruitmentTestNamePublishStatusText='" + recruitmentTestNamePublishStatusText + '\'' +
                "recruitmentTestNamePublishOtherStatusText='" + recruitmentTestNamePublishOtherStatusText + '\'' +
                "color='" + color + '\'' +
                '}';
    }
}
