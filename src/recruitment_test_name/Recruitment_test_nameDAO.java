package recruitment_test_name;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;

import am_house_allocation_approval_mapping.AmHouseAllocationApprovalModel;
import am_house_allocation_request.AmHouseAllocationRequestStatus;
import am_office_assignment.Am_office_assignmentDTO;
import am_office_assignment_request.AmOfficeAssignmentRequestStatus;
import card_info.AlreadyApprovedException;
import card_info.InvalidDataException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Recruitment_test_nameDAO  implements CommonDAOService<Recruitment_test_nameDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Recruitment_test_nameDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"recruitment_test_name_publish_cat",
			"search_column",
			"inserted_by",
			"modified_by",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("name_en"," and (name_en like ?)");
		searchMap.put("name_bn"," and (name_bn like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private static final String getLatestRecruitmentTestExam =
			"SELECT * FROM recruitment_test_name WHERE isDeleted = 0 ORDER BY insertion_date DESC LIMIT 1";

	private static final String updatePublishStatusById =
			"UPDATE recruitment_test_name SET recruitment_test_name_publish_cat = ?, isDeleted =?," +
					"modified_by = ?,lastModificationTime = ? WHERE id = ? ";

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Recruitment_test_nameDAO INSTANCE = new Recruitment_test_nameDAO();
	}

	public static Recruitment_test_nameDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Recruitment_test_nameDTO recruitment_test_nameDTO)
	{
		recruitment_test_nameDTO.searchColumn = "";
		recruitment_test_nameDTO.searchColumn += recruitment_test_nameDTO.nameEn + " ";
		recruitment_test_nameDTO.searchColumn += recruitment_test_nameDTO.nameBn + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Recruitment_test_nameDTO recruitment_test_nameDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(recruitment_test_nameDTO);
		if(isInsert)
		{
			ps.setObject(++index,recruitment_test_nameDTO.iD);
		}
		ps.setObject(++index,recruitment_test_nameDTO.nameEn);
		ps.setObject(++index,recruitment_test_nameDTO.nameBn);
		ps.setObject(++index,recruitment_test_nameDTO.recruitmentTestNamePublishCat);
		ps.setObject(++index,recruitment_test_nameDTO.searchColumn);
		ps.setObject(++index,recruitment_test_nameDTO.insertedBy);
		ps.setObject(++index,recruitment_test_nameDTO.modifiedBy);
		ps.setObject(++index,recruitment_test_nameDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(++index,recruitment_test_nameDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,recruitment_test_nameDTO.iD);
		}
	}
	
	@Override
	public Recruitment_test_nameDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Recruitment_test_nameDTO recruitment_test_nameDTO = new Recruitment_test_nameDTO();
			int i = 0;
			recruitment_test_nameDTO.iD = rs.getLong(columnNames[i++]);
			recruitment_test_nameDTO.nameEn = rs.getString(columnNames[i++]);
			recruitment_test_nameDTO.nameBn = rs.getString(columnNames[i++]);
			recruitment_test_nameDTO.recruitmentTestNamePublishCat = rs.getInt(columnNames[i++]);
			recruitment_test_nameDTO.searchColumn = rs.getString(columnNames[i++]);
			recruitment_test_nameDTO.insertedBy = rs.getString(columnNames[i++]);
			recruitment_test_nameDTO.modifiedBy = rs.getString(columnNames[i++]);
			recruitment_test_nameDTO.insertionDate = rs.getLong(columnNames[i++]);
			recruitment_test_nameDTO.isDeleted = rs.getInt(columnNames[i++]);
			recruitment_test_nameDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return recruitment_test_nameDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Recruitment_test_nameDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "recruitment_test_name";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Recruitment_test_nameDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Recruitment_test_nameDTO) commonDTO,updateQuery,false);
	}



	public Recruitment_test_nameDTO getLatestRecruitmentTestExam() {
		Recruitment_test_nameDTO recruitment_test_nameDTO = ConnectionAndStatementUtil.getT(getLatestRecruitmentTestExam, this::buildObjectFromResultSet);
		return recruitment_test_nameDTO;
	}

	public boolean updatePublishStatus(int publishCat,String modifiedBy,Long id) throws InvalidDataException, AlreadyApprovedException {
		synchronized (LockManager.getLock(id + "RecPublishStatusPA")) {
			return (Boolean) ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
				try {
					int index = 0;

					ps.setInt(++index, publishCat);
					ps.setInt(++index, 0);
					ps.setString(++index, modifiedBy);
					ps.setLong(++index, System.currentTimeMillis());
					ps.setLong(++index, id);

					logger.info(ps);
					ps.executeUpdate();
					return true;
				} catch (SQLException ex) {
					logger.error(ex);
					return false;
				}
			}, updatePublishStatusById);
		}
	}

	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }
				
}
	