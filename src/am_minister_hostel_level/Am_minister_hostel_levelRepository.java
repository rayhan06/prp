package am_minister_hostel_level;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;
import vm_requisition.CommonApprovalStatus;


public class Am_minister_hostel_levelRepository implements Repository {
	Am_minister_hostel_levelDAO am_minister_hostel_levelDAO = null;
	Gson gson = null;

	static Logger logger = Logger.getLogger(Am_minister_hostel_levelRepository.class);
	Map<Long, Am_minister_hostel_levelDTO>mapOfAm_minister_hostel_levelDTOToiD;
	Map<Long, List<Am_minister_hostel_levelDTO>>mapOfAm_minister_hostel_levelDTOToUnitId;

  
	private Am_minister_hostel_levelRepository(){
		gson = new Gson();
		mapOfAm_minister_hostel_levelDTOToiD = new ConcurrentHashMap<>();
		mapOfAm_minister_hostel_levelDTOToUnitId = new ConcurrentHashMap<>();
		am_minister_hostel_levelDAO = Am_minister_hostel_levelDAO.getInstance();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Am_minister_hostel_levelRepository INSTANCE = new Am_minister_hostel_levelRepository();
    }

    public static Am_minister_hostel_levelRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Am_minister_hostel_levelDTO> am_minister_hostel_levelDTOs = am_minister_hostel_levelDAO.getAllDTOs(reloadAll);
			for(Am_minister_hostel_levelDTO am_minister_hostel_levelDTO : am_minister_hostel_levelDTOs) {
				Am_minister_hostel_levelDTO oldAm_minister_hostel_levelDTO = getAm_minister_hostel_levelDTOByIDWithoutClone(am_minister_hostel_levelDTO.iD);
				if( oldAm_minister_hostel_levelDTO != null ) {
					mapOfAm_minister_hostel_levelDTOToiD.remove(oldAm_minister_hostel_levelDTO.iD);

					if (mapOfAm_minister_hostel_levelDTOToUnitId.get(oldAm_minister_hostel_levelDTO.amMinisterHostelUnitId) != null) {
						mapOfAm_minister_hostel_levelDTOToUnitId.get(oldAm_minister_hostel_levelDTO.amMinisterHostelUnitId).remove(oldAm_minister_hostel_levelDTO);
					}

				}
				if(am_minister_hostel_levelDTO.isDeleted == 0) 
				{
					
					mapOfAm_minister_hostel_levelDTOToiD.put(am_minister_hostel_levelDTO.iD, am_minister_hostel_levelDTO);

					List<Am_minister_hostel_levelDTO> minister_hostel_blockDTOS = mapOfAm_minister_hostel_levelDTOToUnitId.getOrDefault(am_minister_hostel_levelDTO.amMinisterHostelUnitId, new ArrayList<>());
					minister_hostel_blockDTOS.add(am_minister_hostel_levelDTO);
					mapOfAm_minister_hostel_levelDTOToUnitId.put(am_minister_hostel_levelDTO.amMinisterHostelUnitId, minister_hostel_blockDTOS);

				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Am_minister_hostel_levelDTO> getAm_minister_hostel_levelList() {
		List <Am_minister_hostel_levelDTO> am_minister_hostel_levels = new ArrayList<Am_minister_hostel_levelDTO>(this.mapOfAm_minister_hostel_levelDTOToiD.values());
		return am_minister_hostel_levels;
	}

	public Am_minister_hostel_levelDTO clone(Am_minister_hostel_levelDTO dto) {
		if (dto == null)return null;
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Am_minister_hostel_levelDTO.class);
	}

	public List<Am_minister_hostel_levelDTO> clone(List<Am_minister_hostel_levelDTO> dtoList) {
		if (dtoList == null)return new ArrayList<>();
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	public Am_minister_hostel_levelDTO getAm_minister_hostel_levelDTOByIDWithoutClone( long ID){
		return mapOfAm_minister_hostel_levelDTOToiD.get(ID);
	}

	public Am_minister_hostel_levelDTO getAm_minister_hostel_levelDTOByID( long ID){
		return clone(mapOfAm_minister_hostel_levelDTOToiD.get(ID));
	}

	public List<Am_minister_hostel_levelDTO> getAm_minister_hostel_levelDTOByUnitID( long ID){
//		return clone(mapOfAm_minister_hostel_levelDTOToUnitId.get(ID)
//		.stream()
//		.filter(dto -> dto.status == CommonApprovalStatus.AVAILABLE.getValue())
//		.collect(Collectors.toList()));
		return clone(mapOfAm_minister_hostel_levelDTOToUnitId.get(ID)
				.stream()
				.collect(Collectors.toList()));
	}

	public List<Am_minister_hostel_levelDTO> getAllAm_minister_hostel_levelDTOByUnitID( long ID){
		return clone(mapOfAm_minister_hostel_levelDTOToUnitId.get(ID)
				.stream()
				.filter(dto -> dto.isDeleted == 0)
				.collect(Collectors.toList()));
	}

	@Override
	public String getDtoJsonById(long ID) {
		return gson.toJson(getAm_minister_hostel_levelDTOByID(ID));
	}

	@Override
	public String getTableName() {
		return "am_minister_hostel_level";
	}
}


