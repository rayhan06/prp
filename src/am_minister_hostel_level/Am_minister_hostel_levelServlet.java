package am_minister_hostel_level;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import files.FilesDTO;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;

import role.RoleDTO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;
import javax.servlet.http.*;
import java.util.*;


import language.LC;
import language.LM;
import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import vm_requisition.CommonApprovalStatus;


/**
 * Servlet implementation class Am_minister_hostel_levelServlet
 */
@WebServlet("/Am_minister_hostel_levelServlet")
@MultipartConfig
public class Am_minister_hostel_levelServlet extends BaseServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Am_minister_hostel_levelServlet.class);

    @Override
    public String getTableName() {
        return Am_minister_hostel_levelDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Am_minister_hostel_levelServlet";
    }

    @Override
    public Am_minister_hostel_levelDAO getCommonDAOService() {
        return Am_minister_hostel_levelDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.AM_MINISTER_HOSTEL_LEVEL_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.AM_MINISTER_HOSTEL_LEVEL_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.AM_MINISTER_HOSTEL_LEVEL_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Am_minister_hostel_levelServlet.class;
    }
    private final Gson gson = new Gson();


    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
		init(request);
		try {
			switch (request.getParameter("actionType")) {
				case "getAddPage":
					if (Utils.checkPermission(commonLoginData.userDTO,getAddPageMenuConstants()) && getAddPagePermission(request)) {
						finalize(request);
						request.getRequestDispatcher(commonPartOfDispatchURL()+"Edit.jsp").forward(request,response);
						return;
					}
					break;
				case "getEditPage":
					if (Utils.checkPermission(commonLoginData.userDTO, getEditPageMenuConstants()) && getEditPagePermission(request)) {
						setDTOForJsp(request);
						finalize(request);
						request.getRequestDispatcher(commonPartOfDispatchURL()+"Edit.jsp?actionType=edit").forward(request,response);
						return;
					}
					break;
				case "getLevelOptionsByUnitId":
					getLevelOptionsByUnitId(request, response);
					return;
				case "getAllLevelOptionsByUnitId":
					getAllLevelOptionsByUnitId(request, response);
					return;
				case "downloadDropzoneFile": {
					long id = Long.parseLong(request.getParameter("id"));
					FilesDTO filesDTO = (FilesDTO) filesDAO.getDTOByID(id);
					Utils.ProcessFile(request, response, filesDTO.fileTitle, filesDTO.inputStream);
					return;
				}
				case "DeleteFileFromDropZone": {
					long id = Long.parseLong(request.getParameter("id"));
					logger.debug("In delete file");
					filesDAO.hardDeleteByID(id);
					response.getWriter().write("Deleted");
					return;
				}
				case "getURL":
					String URL = request.getParameter("URL");
					response.sendRedirect(URL);
					return;
				case "search":
					if (Utils.checkPermission(commonLoginData.userDTO, getSearchMenuConstants()) && getSearchPagePermission(request)) {
						UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
						RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
//						boolean isAdmin = role.ID == SessionConstants.VEHICLE_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;
						boolean isAdmin = role.ID == SessionConstants.ADMIN_ROLE || role.ID == SessionConstants.ASSET_ADMIN_ROLE;
						if (!isAdmin) {
							return;
						}
						search(request, response);
						return;
					}
					break;
				case "view":
					logger.debug("view requested");
					if (Utils.checkPermission(commonLoginData.userDTO, getViewMenuConstants()) && getViewPagePermission(request)) {
						setDTOForJsp(request);
						finalize(request);
						request.getRequestDispatcher(commonPartOfDispatchURL() + "View.jsp?ID=" + getId(request)).forward(request,response);
						return;
					}
					break;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
		}
		request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
	}


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String actionType = request.getParameter("actionType");
		CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();

		if ("setNotAvailable".equals(actionType) &&Utils.checkPermission(commonLoginData.userDTO, getEditPageMenuConstants())
				&& getEditPermission(request)) {
			try {
				Utils.handleTransaction(()->{
					Am_minister_hostel_levelDTO am_minister_hostel_levelDTO = Am_minister_hostel_levelRepository.getInstance().getAm_minister_hostel_levelDTOByID
							(Long.parseLong(request.getParameter("levelId")));
					if (am_minister_hostel_levelDTO == null) {
						throw new Exception("Level not found");
					}

					if(am_minister_hostel_levelDTO.status != CommonApprovalStatus.AVAILABLE.getValue()){
						throw new Exception("Status Have to be Available");
					}

					am_minister_hostel_levelDTO.not_available_reason = Jsoup.clean(request.getParameter("remarks"),
							Whitelist.simpleText());
					am_minister_hostel_levelDTO.status = CommonApprovalStatus.NOT_AVAILABLE.getValue();
					am_minister_hostel_levelDTO.lastModificationTime = System.currentTimeMillis();
					am_minister_hostel_levelDTO.lastModifierUser = commonLoginData.userDTO.userName;
					Am_minister_hostel_levelDAO.getInstance().update(am_minister_hostel_levelDTO);

					String redirectUrl = "Am_minister_hostel_levelServlet?actionType=view&ID=" + am_minister_hostel_levelDTO.iD;
					ApiResponse.sendSuccessResponse(response, redirectUrl);
				});


			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}

		super.doPost(request,response);
	}
	
	@Override
	public boolean getEditPermission(HttpServletRequest request) {
		return checkEditPermission(request);
	}

	@Override
	public boolean getEditPagePermission(HttpServletRequest request) {
		return checkEditPagePermission(request);
	}

	@Override
	public boolean getAddPermission(HttpServletRequest request) {
		return checkEditPermission(request);
	}

	@Override
	public boolean getAddPagePermission(HttpServletRequest request) {
		return checkEditPermission(request);
	}

	@Override
	public boolean getViewPagePermission(HttpServletRequest request) {
		return checkEditPagePermission(request);
	}

	private boolean checkEditPagePermission(HttpServletRequest request) {
		UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
		RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
		boolean isAdmin = role.ID == SessionConstants.ADMIN_ROLE || role.ID == SessionConstants.ASSET_ADMIN_ROLE;
		return isAdmin;
//		if (isAdmin) {
//			return true;
//		}
//		Am_minister_hostel_levelDTO am_minister_hostel_levelDTO = getCommonDAOService().getDTOFromID(Long.parseLong(request.getParameter("ID")));
//		if (am_minister_hostel_levelDTO == null) {
//			return false;
//		}
//		return am_minister_hostel_levelDTO.insertedByOrganogramId == userDTO.organogramID;
	}

	private boolean checkEditPermission(HttpServletRequest request) {
		UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
		RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
		boolean isAdmin = role.ID == SessionConstants.ADMIN_ROLE || role.ID == SessionConstants.ASSET_ADMIN_ROLE;
		return isAdmin;
//		if (isAdmin) {
//			return true;
//		}
//		Am_minister_hostel_levelDTO am_minister_hostel_levelDTO = getCommonDAOService().getDTOFromID(Long.parseLong(request.getParameter("iD")));
//		if (am_minister_hostel_levelDTO == null) {
//			return false;
//		}
//		return am_minister_hostel_levelDTO.insertedByOrganogramId == userDTO.organogramID;
	}
	
	
	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception
	{
		// TODO Auto-generated method stub

			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addAm_minister_hostel_level");
			String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
			Am_minister_hostel_levelDTO am_minister_hostel_levelDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				am_minister_hostel_levelDTO = new Am_minister_hostel_levelDTO();
			}
			else
			{
				am_minister_hostel_levelDTO = Am_minister_hostel_levelDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("amMinisterHostelBlockId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("amMinisterHostelBlockType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_minister_hostel_levelDTO.amMinisterHostelBlockId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("amMinisterHostelSideId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("amMinisterHostelSideId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_minister_hostel_levelDTO.amMinisterHostelSideId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("amMinisterHostelUnitId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("amMinisterHostelUnitId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_minister_hostel_levelDTO.amMinisterHostelUnitId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("amMinisterHostelLevelCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("amMinisterHostelLevelCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_minister_hostel_levelDTO.amMinisterHostelLevelCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				am_minister_hostel_levelDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				am_minister_hostel_levelDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				am_minister_hostel_levelDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				am_minister_hostel_levelDTO.insertionDate = TimeConverter.getToday();
			}


			am_minister_hostel_levelDTO.lastModifierUser = userDTO.userName;


			System.out.println("Done adding  addAm_minister_hostel_level dto = " + am_minister_hostel_levelDTO);

		int existingCount = Am_minister_hostel_levelDAO.getInstance().getCount(null, "",
				" and am_minister_hostel_unit_id = " + am_minister_hostel_levelDTO.amMinisterHostelUnitId +
						" and am_minister_hostel_level_cat = '" + am_minister_hostel_levelDTO.amMinisterHostelLevelCat + "' " +
						" and iD != " + am_minister_hostel_levelDTO.iD);
		if (existingCount > 0) {
			String errMSg = language.equals("English") ?
					"Sorry, another level was created with this name under same unit" :
					"দুঃখিত, এই নামে একই ইউনিটে আরেকটি পাশ বিদ্যমান";
			throw new Exception(errMSg);
		}

		Utils.handleTransaction(()->{
			if(addFlag == true)
			{
				am_minister_hostel_levelDTO.status = CommonApprovalStatus.AVAILABLE.getValue();
				Am_minister_hostel_levelDAO.getInstance().add(am_minister_hostel_levelDTO);
			}
			else
			{
				Am_minister_hostel_levelDAO.getInstance().update(am_minister_hostel_levelDTO);
			}
		});


			return am_minister_hostel_levelDTO;


	}

	//getAllLevelOptionsByUnitId

	private void getAllLevelOptionsByUnitId(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getSideOptionsByBlockId");
			LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
			String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);
			List<Am_minister_hostel_levelDTO> amMinisterHostelLevelDTOS = Am_minister_hostel_levelRepository.getInstance().getAllAm_minister_hostel_levelDTOByUnitID(Long.parseLong(request.getParameter("unitId")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String tempString = "";
			tempString += Utils.buildSelectOption(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language.equalsIgnoreCase("english"));

			for (Am_minister_hostel_levelDTO am_minister_hostel_levelDTO : amMinisterHostelLevelDTOS) {
				String level = CatRepository.getName(Language, "am_minister_hostel_level", am_minister_hostel_levelDTO.amMinisterHostelLevelCat);
				tempString += "<option value='" + am_minister_hostel_levelDTO.iD + "'>" + (level) + "</option>";
			}

//			String encoded = this.gson.toJson(tempString);
//			System.out.println("json encoded data = " + encoded);
			out.print(tempString);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void getLevelOptionsByUnitId(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getSideOptionsByBlockId");
			LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
			String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);
			List<Am_minister_hostel_levelDTO> amMinisterHostelLevelDTOS =  Am_minister_hostel_levelRepository.getInstance().
					getAm_minister_hostel_levelDTOByUnitID(Long.parseLong(request.getParameter("unitId")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String tempString = "";
			tempString += Utils.buildSelectOption(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language.equalsIgnoreCase("english"));

			for (Am_minister_hostel_levelDTO am_minister_hostel_levelDTO : amMinisterHostelLevelDTOS) {
				String level = CatRepository.getName(Language, "am_minister_hostel_level", am_minister_hostel_levelDTO.amMinisterHostelLevelCat);
				tempString += "<option value='" + am_minister_hostel_levelDTO.iD + "'>" + (level) + "</option>";
			}

//			String encoded = this.gson.toJson(tempString);
//			System.out.println("json encoded data = " + encoded);
			out.print(tempString);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

