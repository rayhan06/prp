package am_minister_hostel_level;
import java.util.*; 
import util.*; 


public class Am_minister_hostel_levelDTO extends CommonDTO
{

	public long amMinisterHostelBlockId = -1;
	public long amMinisterHostelSideId = -1;
	public long amMinisterHostelUnitId = -1;
	public int amMinisterHostelLevelCat = -1;
	public int status = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	public String not_available_reason = "";
	
	
    @Override
	public String toString() {
            return "$Am_minister_hostel_levelDTO[" +
            " iD = " + iD +
            " amMinisterHostelBlockId = " + amMinisterHostelBlockId +
            " amMinisterHostelSideId = " + amMinisterHostelSideId +
            " amMinisterHostelUnitId = " + amMinisterHostelUnitId +
            " amMinisterHostelLevelCat = " + amMinisterHostelLevelCat +
            " status = " + status +
            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}