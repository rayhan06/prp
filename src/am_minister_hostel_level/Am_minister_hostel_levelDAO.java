package am_minister_hostel_level;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Am_minister_hostel_levelDAO  implements CommonDAOService<Am_minister_hostel_levelDTO>
{

	Logger logger = Logger.getLogger(getClass());

	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Am_minister_hostel_levelDAO()
	{
		columnNames = new String[]
		{
			"ID",
			"am_minister_hostel_block_id",
			"am_minister_hostel_side_id",
			"am_minister_hostel_unit_id",
			"am_minister_hostel_level_cat",
			"status",
			"not_available_reason",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

		searchMap.put("am_minister_hostel_block_id"," and (am_minister_hostel_block_id = ?)");
		searchMap.put("am_minister_hostel_level_cat"," and (am_minister_hostel_level_cat = ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("status"," and (status = ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Am_minister_hostel_levelDAO INSTANCE = new Am_minister_hostel_levelDAO();
	}

	public static Am_minister_hostel_levelDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}

	public void setSearchColumn(Am_minister_hostel_levelDTO am_minister_hostel_levelDTO)
	{
		am_minister_hostel_levelDTO.searchColumn = "";
		am_minister_hostel_levelDTO.searchColumn += CommonDAO.getName(am_minister_hostel_levelDTO.amMinisterHostelBlockId, "am_minister_hostel_block", "block_no", "id") ;
		am_minister_hostel_levelDTO.searchColumn += CatDAO.getName("English", "am_minister_hostel_level", am_minister_hostel_levelDTO.amMinisterHostelLevelCat) + " " + CatDAO.getName("Bangla", "am_minister_hostel_level", am_minister_hostel_levelDTO.amMinisterHostelLevelCat) + " ";
	}

	@Override
	public void set(PreparedStatement ps, Am_minister_hostel_levelDTO am_minister_hostel_levelDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();
		setSearchColumn(am_minister_hostel_levelDTO);
		if(isInsert)
		{
			ps.setObject(++index,am_minister_hostel_levelDTO.iD);
		}
		ps.setObject(++index,am_minister_hostel_levelDTO.amMinisterHostelBlockId);
		ps.setObject(++index,am_minister_hostel_levelDTO.amMinisterHostelSideId);
		ps.setObject(++index,am_minister_hostel_levelDTO.amMinisterHostelUnitId);
		ps.setObject(++index,am_minister_hostel_levelDTO.amMinisterHostelLevelCat);
		ps.setObject(++index,am_minister_hostel_levelDTO.status);
		ps.setObject(++index,am_minister_hostel_levelDTO.not_available_reason);
		ps.setObject(++index,am_minister_hostel_levelDTO.searchColumn);
		ps.setObject(++index,am_minister_hostel_levelDTO.insertedByUserId);
		ps.setObject(++index,am_minister_hostel_levelDTO.insertedByOrganogramId);
		ps.setObject(++index,am_minister_hostel_levelDTO.insertionDate);
		ps.setObject(++index,am_minister_hostel_levelDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(++index,am_minister_hostel_levelDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,am_minister_hostel_levelDTO.iD);
		}
	}

	@Override
	public Am_minister_hostel_levelDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Am_minister_hostel_levelDTO am_minister_hostel_levelDTO = new Am_minister_hostel_levelDTO();
			int i = 0;
			am_minister_hostel_levelDTO.iD = rs.getLong(columnNames[i++]);
			am_minister_hostel_levelDTO.amMinisterHostelBlockId = rs.getLong(columnNames[i++]);
			am_minister_hostel_levelDTO.amMinisterHostelSideId = rs.getLong(columnNames[i++]);
			am_minister_hostel_levelDTO.amMinisterHostelUnitId = rs.getLong(columnNames[i++]);
			am_minister_hostel_levelDTO.amMinisterHostelLevelCat = rs.getInt(columnNames[i++]);
			am_minister_hostel_levelDTO.status = rs.getInt(columnNames[i++]);
			am_minister_hostel_levelDTO.not_available_reason = rs.getString(columnNames[i++]);
			am_minister_hostel_levelDTO.searchColumn = rs.getString(columnNames[i++]);
			am_minister_hostel_levelDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			am_minister_hostel_levelDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			am_minister_hostel_levelDTO.insertionDate = rs.getLong(columnNames[i++]);
			am_minister_hostel_levelDTO.lastModifierUser = rs.getString(columnNames[i++]);
			am_minister_hostel_levelDTO.isDeleted = rs.getInt(columnNames[i++]);
			am_minister_hostel_levelDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return am_minister_hostel_levelDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	public Am_minister_hostel_levelDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "am_minister_hostel_level";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_minister_hostel_levelDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_minister_hostel_levelDTO) commonDTO,updateQuery,false);
	}

	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
				
}
	