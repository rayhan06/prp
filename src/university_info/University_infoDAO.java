package university_info;

import common.ConnectionAndStatementUtil;
import dbm.DBMR;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class University_infoDAO extends NavigationService4 {

    Logger logger = Logger.getLogger(getClass());

    private static final String getByIds = "SELECT * FROM university_info WHERE ID IN (%s)";
    public University_infoDAO(String tableName) {
        super(tableName);
        joinSQL = "";
        commonMaps = new University_infoMAPS(tableName);
        columnNames = new String[]
                {
                        "iD",
                        "university_name_en",
                        "university_name_bn",
                        "insertion_date",
                        "inserted_by",
                        "modified_by",
                        "isDeleted",
                        "lastModificationTime"
                };
    }

    public University_infoDAO() {
        this("university_info");
    }

    public void setSearchColumn(University_infoDTO university_infoDTO) {
        university_infoDTO.searchColumn = "";
        university_infoDTO.searchColumn += university_infoDTO.universityNameEn + " ";
        university_infoDTO.searchColumn += university_infoDTO.universityNameBn + " ";

    }

    public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException {
        University_infoDTO university_infoDTO = (University_infoDTO) commonDTO;
        int index = 1;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(university_infoDTO);
        if (isInsert) {
            ps.setObject(index++, university_infoDTO.iD);
        }
        ps.setObject(index++, university_infoDTO.universityNameEn);
        ps.setObject(index++, university_infoDTO.universityNameBn);
        ps.setObject(index++, university_infoDTO.insertionDate);
        ps.setObject(index++, university_infoDTO.insertedBy);
        ps.setObject(index++, university_infoDTO.modifiedBy);
        if (isInsert) {
            ps.setObject(index++, 0);
            ps.setObject(index++, lastModificationTime);
        }
    }

    public void get(University_infoDTO university_infoDTO, ResultSet rs) throws SQLException {
        university_infoDTO.iD = rs.getLong("id");
        university_infoDTO.universityNameEn = rs.getString("university_name_en");
        university_infoDTO.universityNameBn = rs.getString("university_name_bn");
        university_infoDTO.insertionDate = rs.getLong("insertion_date");
        university_infoDTO.insertedBy = rs.getString("inserted_by");
        university_infoDTO.modifiedBy = rs.getString("modified_by");
        university_infoDTO.isDeleted = rs.getInt("isDeleted");
        university_infoDTO.lastModificationTime = rs.getLong("lastModificationTime");
    }

    private University_infoDTO buildDTO(ResultSet rs){
        try{
            University_infoDTO dto = new University_infoDTO();
            get(dto,rs);
            return dto;
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    public List<University_infoDTO> getByIds(List<Long>ids){
        if(ids == null || ids.size() == 0){
            return new ArrayList<>();
        }
        return ConnectionAndStatementUtil.getDTOListByNumbers(getByIds,ids,this::buildDTO);
    }


    //need another getter for repository
    public University_infoDTO getDTOByID(long ID) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        University_infoDTO university_infoDTO = null;
        try {

            String sql = "SELECT * ";

            sql += " FROM " + tableName;

            sql += " WHERE id=" + ID;

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                university_infoDTO = new University_infoDTO();

                get(university_infoDTO, rs);

            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return university_infoDTO;
    }


    public List<University_infoDTO> getDTOs(Collection recordIDs) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        University_infoDTO university_infoDTO = null;
        List<University_infoDTO> university_infoDTOList = new ArrayList<>();
        if (recordIDs.isEmpty()) {
            return university_infoDTOList;
        }
        try {

            String sql = "SELECT * ";

            sql += " FROM " + tableName;

            sql += " WHERE id IN ( ";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql += ",";
                }
                sql += ((ArrayList) recordIDs).get(i);
            }
            sql += ")  order by lastModificationTime desc";

            printSql(sql);

            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                university_infoDTO = new University_infoDTO();
                get(university_infoDTO, rs);
                System.out.println("got this DTO: " + university_infoDTO);

                university_infoDTOList.add(university_infoDTO);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return university_infoDTOList;

    }


    //add repository
    public List<University_infoDTO> getAllUniversity_info(boolean isFirstReload) {
        List<University_infoDTO> university_infoDTOList = new ArrayList<>();

        String sql = "SELECT * FROM university_info";
        sql += " WHERE ";


        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        sql += " order by university_info.lastModificationTime desc";
        printSql(sql);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;


        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);


            while (rs.next()) {
                University_infoDTO university_infoDTO = new University_infoDTO();
                get(university_infoDTO, rs);

                university_infoDTOList.add(university_infoDTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return university_infoDTOList;
    }


    public List<University_infoDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }


    public List<University_infoDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
                                            String filter, boolean tableHasJobCat) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        List<University_infoDTO> university_infoDTOList = new ArrayList<>();

        try {

            String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                University_infoDTO university_infoDTO = new University_infoDTO();
                get(university_infoDTO, rs);

                university_infoDTOList.add(university_infoDTO);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return university_infoDTOList;

    }

    public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                          UserDTO userDTO, String filter, boolean tableHasJobCat) {
        boolean viewAll = false;

        if (p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null) {
            System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
            viewAll = true;
        } else {
            System.out.println("ViewAll is null ");
        }

        String sql = "SELECT ";
        if (category == GETIDS) {
            sql += " " + tableName + ".ID as ID ";
        } else if (category == GETCOUNT) {
            sql += " count( " + tableName + ".ID) as countID ";
        } else if (category == GETDTOS) {
            sql += " distinct " + tableName + ".* ";
        }
        sql += "FROM " + tableName + " ";
        sql += joinSQL;


        String AnyfieldSql = "";
        String AllFieldSql = "";

        if (p_searchCriteria != null) {


            Enumeration names = p_searchCriteria.keys();
            String str, value;

            AnyfieldSql = "(";

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                AnyfieldSql += tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
            }
            AnyfieldSql += ")";
            System.out.println("AnyfieldSql = " + AnyfieldSql);

            AllFieldSql = "(";
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                System.out.println(str + ": " + value);
                if (value != null && !value.equalsIgnoreCase("") && (
                        str.equals("university_name_en")
                                || str.equals("university_name_bn")
                                || str.equals("insertion_date_start")
                                || str.equals("insertion_date_end")
                )

                ) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql += " AND  ";
                    }

                    if (str.equals("university_name_en")) {
                        AllFieldSql += "" + tableName + ".university_name_en like '%" + p_searchCriteria.get(str) + "%'";
                        i++;
                    } else if (str.equals("university_name_bn")) {
                        AllFieldSql += "" + tableName + ".university_name_bn like '%" + p_searchCriteria.get(str) + "%'";
                        i++;
                    } else if (str.equals("insertion_date_start")) {
                        AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
                        i++;
                    } else if (str.equals("insertion_date_end")) {
                        AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
                        i++;
                    }


                }
            }

            AllFieldSql += ")";
            System.out.println("AllFieldSql = " + AllFieldSql);


        }


        sql += " WHERE ";

        sql += " (" + tableName + ".isDeleted = 0 ";
        sql += ")";


        if (!filter.equalsIgnoreCase("")) {
            sql += " and " + filter + " ";
        }

        if (!AnyfieldSql.equals("()") && !AnyfieldSql.equals("")) {
            sql += " AND " + AnyfieldSql;

        }
        if (!AllFieldSql.equals("()") && !AllFieldSql.equals("")) {
            sql += " AND " + AllFieldSql;
        }


        sql += " order by " + tableName + ".lastModificationTime desc ";

        printSql(sql);

        if (limit >= 0) {
            sql += " limit " + limit;
        }
        if (offset >= 0) {
            sql += " offset " + offset;
        }

        System.out.println("-------------- sql = " + sql);

        return sql;
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
                                           boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat) {
        return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
    }

    public List<University_infoDTO> university_infoDTOList(boolean isFirstReload) {
        List<University_infoDTO> universityInfoDTOArrayList = new ArrayList<>();

        String sql = "SELECT * FROM " + tableName;
        sql += " WHERE ";


        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        //sql += " order by recruitment_job_description.lastModificationTime desc";
        printSql(sql);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;


        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);


            while (rs.next()) {
                University_infoDTO university_infoDTO = new University_infoDTO();
                get(university_infoDTO, rs);

                universityInfoDTOArrayList.add(university_infoDTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return universityInfoDTOArrayList;
    }

    public String getUniversityList(String language, long selectedId) {
        List<OptionDTO> optionDTOList = University_infoRepository.getInstance().getUniversity_infoList()
                .stream()
                .map(dto->new OptionDTO(dto.universityNameEn,dto.universityNameBn,String.valueOf(dto.iD)))
                .collect(Collectors.toList());
        optionDTOList.add(new OptionDTO("Others","অন্যান্য","-100"));
        return Utils.buildOptions(optionDTOList,language,String.valueOf(selectedId));
    }
}