package university_info;
import java.util.*; 
import util.*;


public class University_infoMAPS extends CommonMaps
{	
	public University_infoMAPS(String tableName)
	{
		

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("universityNameEn".toLowerCase(), "universityNameEn".toLowerCase());
		java_DTO_map.put("universityNameBn".toLowerCase(), "universityNameBn".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("university_name_en".toLowerCase(), "universityNameEn".toLowerCase());
		java_SQL_map.put("university_name_bn".toLowerCase(), "universityNameBn".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("University Name En".toLowerCase(), "universityNameEn".toLowerCase());
		java_Text_map.put("University Name Bn".toLowerCase(), "universityNameBn".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}