package university_info;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;



import java.util.ArrayList;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class University_infoServlet
 */
@WebServlet("/University_infoServlet")
@MultipartConfig
public class University_infoServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(University_infoServlet.class);

    String tableName = "university_info";

	University_infoDAO university_infoDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public University_infoServlet()
	{
        super();
    	try
    	{
			university_infoDAO = new University_infoDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(university_infoDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.UNIVERSITY_INFO_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.UNIVERSITY_INFO_UPDATE))
				{
					getUniversity_info(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getUploadPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.UNIVERSITY_INFO_UPLOAD))
				{
					commonRequestHandler.geUploadPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.UNIVERSITY_INFO_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchUniversity_info(request, response, isPermanentTable, filter);
						}
						else
						{
							searchUniversity_info(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchUniversity_info(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.UNIVERSITY_INFO_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	public ArrayList<University_infoDTO> ReadXLsToArraylist(HttpServletRequest request, String fileName) throws IOException
	{
		String path = getServletContext().getRealPath("/img2/");
	    File excelFile = new File(path + File.separator
                + fileName);
	    FileInputStream fis = new FileInputStream(excelFile);


	    XSSFWorkbook workbook = new XSSFWorkbook(fis);
	    XSSFSheet sheet = workbook.getSheetAt(0);
	    Iterator<Row> rowIt = sheet.iterator();
	    ArrayList<String> Rows = new ArrayList<String>();
	    ArrayList<University_infoDTO> university_infoDTOs = new ArrayList<University_infoDTO>();


	    University_infoDTO university_infoDTO;

		String failureMessage = "";
	    int i = 0;

		University_infoMAPS university_infoMAPS = new University_infoMAPS("university_info");
	    while(rowIt.hasNext())
	    {
			Row row = rowIt.next();

			Iterator<Cell> cellIterator = row.cellIterator();

			university_infoDTO = new University_infoDTO();

			int j = 0;

			try
			{
				while (cellIterator.hasNext())
				{
					Cell cell = cellIterator.next();


					if(i == 0)
					{
						Rows.add(cell.toString());
						System.out.println("Rows found: " + cell + " ");
					}
					else
					{

						String rowName = Rows.get(j).toLowerCase();
						System.out.println("rowname: " + rowName + " rowname from map = " + university_infoMAPS.java_Text_map.get(rowName));

						if(rowName == null || rowName.equalsIgnoreCase(""))
						{
							System.out.println("null row name");
							break;
						}
						if(cell == null || cell.toString().equalsIgnoreCase(""))
						{
							System.out.println("null cell");
							j++;
							continue;
						}
						else
						{
							System.out.println("Inserting Value = " + cell + " to row " + rowName );

						}
						if(university_infoMAPS.java_Text_map.get(rowName) != null && university_infoMAPS.java_Text_map.get(rowName).equalsIgnoreCase("id"))
						{
							university_infoDTO.iD = (long)Double.parseDouble(cell.toString());
						}
						else if(university_infoMAPS.java_Text_map.get(rowName) != null && university_infoMAPS.java_Text_map.get(rowName).equalsIgnoreCase("universityNameEn"))
						{
							university_infoDTO.universityNameEn = (cell.toString());
						}
						else if(university_infoMAPS.java_Text_map.get(rowName) != null && university_infoMAPS.java_Text_map.get(rowName).equalsIgnoreCase("universityNameBn"))
						{
							university_infoDTO.universityNameBn = (cell.toString());
						}
						else if(university_infoMAPS.java_Text_map.get(rowName) != null && university_infoMAPS.java_Text_map.get(rowName).equalsIgnoreCase("insertionDate"))
						{
							university_infoDTO.insertionDate = (long)Double.parseDouble(cell.toString());
						}
						else if(university_infoMAPS.java_Text_map.get(rowName) != null && university_infoMAPS.java_Text_map.get(rowName).equalsIgnoreCase("insertedBy"))
						{
							university_infoDTO.insertedBy = (cell.toString());
						}
						else if(university_infoMAPS.java_Text_map.get(rowName) != null && university_infoMAPS.java_Text_map.get(rowName).equalsIgnoreCase("modifiedBy"))
						{
							university_infoDTO.modifiedBy = (cell.toString());
						}
					}
					j ++;

				  }


				  if(i != 0)
				  {
					  System.out.println("INSERTING to the list: " + university_infoDTO);
					  university_infoDTOs.add(university_infoDTO);
				  }
			}
			catch (Exception e)
			{
				e.printStackTrace();
				failureMessage += (i + 1) + " ";
			}
			i ++;

			System.out.println();

	    }
		if(failureMessage.equalsIgnoreCase(""))
		{
			failureMessage = " Successfully parsed all rows";
		}
		else
		{
			failureMessage = " Failed on rows: " + failureMessage;
		}
		request.setAttribute("failureMessage", failureMessage);

	    workbook.close();
	    fis.close();
	    return university_infoDTOs;

	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.UNIVERSITY_INFO_ADD))
				{
					System.out.println("going to  addUniversity_info ");
					addUniversity_info(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addUniversity_info ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.UNIVERSITY_INFO_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addUniversity_info ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.UNIVERSITY_INFO_UPDATE))
				{
					addUniversity_info(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("upload"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.UNIVERSITY_INFO_UPLOAD))
				{
					uploadUniversity_info(request, response, false);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("uploadConfirmed"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.UNIVERSITY_INFO_UPLOAD))
				{
					System.out.println("uploadConfirmed");
					addUniversity_infos(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.UNIVERSITY_INFO_SEARCH))
				{
					searchUniversity_info(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			University_infoDTO university_infoDTO = university_infoDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(university_infoDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	private void uploadUniversity_info(HttpServletRequest request, HttpServletResponse response, Boolean addFlag) throws IOException
	{
		System.out.println("%%%% ajax upload called");
		Part filePart_university_infoDatabase;
		try
		{
			filePart_university_infoDatabase = request.getPart("university_infoDatabase");
			String Value = commonRequestHandler.getFileName(filePart_university_infoDatabase);
			System.out.println("university_infoDatabase = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					String FileName = "university_infodatabase";
					String path = getServletContext().getRealPath("/img2/");
					Utils.uploadFile(filePart_university_infoDatabase, FileName, path);
					ArrayList<University_infoDTO> university_infoDTOs = ReadXLsToArraylist(request, FileName);
					HttpSession session = request.getSession(true);
					session.setAttribute("university_infoDTOs", university_infoDTOs);

					RequestDispatcher rd = request.getRequestDispatcher("university_info/university_infoReview.jsp?actionType=edit");
					rd.forward(request, response);
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	private void addUniversity_infos(HttpServletRequest request, HttpServletResponse response) throws IOException
		{
		 String[] paramValues = request.getParameterValues("id");
         for (int i = 0; i < paramValues.length; i++)
         {
			String paramValue = paramValues[i];
			University_infoDTO university_infoDTO = new University_infoDTO();
			try
			{
				String Value = "";

				if(request.getParameterValues("id") != null)
				{
					Value = request.getParameterValues("id")[i];
					if(Value != null)
					{
						Value = Jsoup.clean(Value,Whitelist.simpleText());
					}
					System.out.println("id = " + Value);
					if(Value != null && !Value.equalsIgnoreCase(""))
					{
						university_infoDTO.iD = Long.parseLong(Value);
					}
					else
					{
						System.out.println("FieldName has a null Value, not updating" + " = " + Value);
					}
				}
				if(request.getParameterValues("universityNameEn") != null)
				{
					Value = request.getParameterValues("universityNameEn")[i];
					if(Value != null)
					{
						Value = Jsoup.clean(Value,Whitelist.simpleText());
					}
					System.out.println("universityNameEn = " + Value);
					if(Value != null && !Value.equalsIgnoreCase(""))
					{
						university_infoDTO.universityNameEn = (Value);
					}
					else
					{
						System.out.println("FieldName has a null Value, not updating" + " = " + Value);
					}
				}
				if(request.getParameterValues("universityNameBn") != null)
				{
					Value = request.getParameterValues("universityNameBn")[i];
					if(Value != null)
					{
						Value = Jsoup.clean(Value,Whitelist.simpleText());
					}
					System.out.println("universityNameBn = " + Value);
					if(Value != null && !Value.equalsIgnoreCase(""))
					{
						university_infoDTO.universityNameBn = (Value);
					}
					else
					{
						System.out.println("FieldName has a null Value, not updating" + " = " + Value);
					}
				}
				if(request.getParameterValues("insertionDate") != null)
				{
					Value = request.getParameterValues("insertionDate")[i];
					if(Value != null)
					{
						Value = Jsoup.clean(Value,Whitelist.simpleText());
					}
					System.out.println("insertionDate = " + Value);
					if(Value != null && !Value.equalsIgnoreCase(""))
					{
						university_infoDTO.insertionDate = Long.parseLong(Value);
					}
					else
					{
						System.out.println("FieldName has a null Value, not updating" + " = " + Value);
					}
				}
				if(request.getParameterValues("insertedBy") != null)
				{
					Value = request.getParameterValues("insertedBy")[i];
					if(Value != null)
					{
						Value = Jsoup.clean(Value,Whitelist.simpleText());
					}
					System.out.println("insertedBy = " + Value);
					if(Value != null && !Value.equalsIgnoreCase(""))
					{
						university_infoDTO.insertedBy = (Value);
					}
					else
					{
						System.out.println("FieldName has a null Value, not updating" + " = " + Value);
					}
				}
				if(request.getParameterValues("modifiedBy") != null)
				{
					Value = request.getParameterValues("modifiedBy")[i];
					if(Value != null)
					{
						Value = Jsoup.clean(Value,Whitelist.simpleText());
					}
					System.out.println("modifiedBy = " + Value);
					if(Value != null && !Value.equalsIgnoreCase(""))
					{
						university_infoDTO.modifiedBy = (Value);
					}
					else
					{
						System.out.println("FieldName has a null Value, not updating" + " = " + Value);
					}
				}



				System.out.println("Done adding  addUniversity_info dto = " + university_infoDTO);


				university_infoDAO.add(university_infoDTO);




			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

         }
		 response.sendRedirect("University_infoServlet?actionType=search");
	}

	private void addUniversity_info(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addUniversity_info");
			String path = getServletContext().getRealPath("/img2/");
			University_infoDTO university_infoDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				university_infoDTO = new University_infoDTO();
			}
			else
			{
				university_infoDTO = university_infoDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			String FileNamePrefix;
			if(addFlag == true)
			{
				FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
			}
			else
			{
				FileNamePrefix = request.getParameter("iD");
			}

			String Value = "";

			Value = request.getParameter("universityNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("universityNameEn = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				university_infoDTO.universityNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("universityNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("universityNameBn = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				university_infoDTO.universityNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				university_infoDTO.insertionDate = c.getTimeInMillis();
			}


			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				university_infoDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				university_infoDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addUniversity_info dto = " + university_infoDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				university_infoDAO.setIsDeleted(university_infoDTO.iD, CommonDTO.OUTDATED);
				returnedID = university_infoDAO.add(university_infoDTO);
				university_infoDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = university_infoDAO.manageWriteOperations(university_infoDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = university_infoDAO.manageWriteOperations(university_infoDTO, SessionConstants.UPDATE, -1, userDTO);
			}









			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getUniversity_info(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("University_infoServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(university_infoDAO.getDTOByID(returnedID), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}









	private void getUniversity_info(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getUniversity_info");
		University_infoDTO university_infoDTO = null;
		try
		{
			university_infoDTO = university_infoDAO.getDTOByID(id);
			request.setAttribute("ID", university_infoDTO.iD);
			request.setAttribute("university_infoDTO",university_infoDTO);
			request.setAttribute("university_infoDAO",university_infoDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "university_info/university_infoInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "university_info/university_infoSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "university_info/university_infoEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "university_info/university_infoEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getUniversity_info(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getUniversity_info(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchUniversity_info(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchUniversity_info 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_UNIVERSITY_INFO,
			request,
			university_infoDAO,
			SessionConstants.VIEW_UNIVERSITY_INFO,
			SessionConstants.SEARCH_UNIVERSITY_INFO,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("university_infoDAO",university_infoDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to university_info/university_infoApproval.jsp");
	        	rd = request.getRequestDispatcher("university_info/university_infoApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to university_info/university_infoApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("university_info/university_infoApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to university_info/university_infoSearch.jsp");
	        	rd = request.getRequestDispatcher("university_info/university_infoSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to university_info/university_infoSearchForm.jsp");
	        	rd = request.getRequestDispatcher("university_info/university_infoSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

