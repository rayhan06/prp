package university_info;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class University_infoRepository implements Repository {
	University_infoDAO university_infoDAO = null;
	
	public void setDAO(University_infoDAO university_infoDAO)
	{
		this.university_infoDAO = university_infoDAO;
	}
	
	
	static Logger logger = Logger.getLogger(University_infoRepository.class);
	Map<Long, University_infoDTO>mapOfUniversity_infoDTOToid;
//	Map<String, Set<University_infoDTO> >mapOfUniversity_infoDTOTouniversityNameEn;
//	Map<String, Set<University_infoDTO> >mapOfUniversity_infoDTOTouniversityNameBn;
//	Map<Long, Set<University_infoDTO> >mapOfUniversity_infoDTOToinsertionDate;
//	Map<String, Set<University_infoDTO> >mapOfUniversity_infoDTOToinsertedBy;
//	Map<String, Set<University_infoDTO> >mapOfUniversity_infoDTOTomodifiedBy;
//	Map<Long, Set<University_infoDTO> >mapOfUniversity_infoDTOTolastModificationTime;


	static University_infoRepository instance = null;  
	private University_infoRepository(){
		mapOfUniversity_infoDTOToid = new ConcurrentHashMap<>();
//		mapOfUniversity_infoDTOTouniversityNameEn = new ConcurrentHashMap<>();
//		mapOfUniversity_infoDTOTouniversityNameBn = new ConcurrentHashMap<>();
//		mapOfUniversity_infoDTOToinsertionDate = new ConcurrentHashMap<>();
//		mapOfUniversity_infoDTOToinsertedBy = new ConcurrentHashMap<>();
//		mapOfUniversity_infoDTOTomodifiedBy = new ConcurrentHashMap<>();
//		mapOfUniversity_infoDTOTolastModificationTime = new ConcurrentHashMap<>();

		setDAO(new University_infoDAO());
		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static University_infoRepository getInstance(){
		if (instance == null){
			instance = new University_infoRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(university_infoDAO == null)
		{
			return;
		}
		try {
			List<University_infoDTO> university_infoDTOs = university_infoDAO.getAllUniversity_info(reloadAll);
			for(University_infoDTO university_infoDTO : university_infoDTOs) {
				University_infoDTO oldUniversity_infoDTO = getUniversity_infoDTOByid(university_infoDTO.iD);
				if( oldUniversity_infoDTO != null ) {
					mapOfUniversity_infoDTOToid.remove(oldUniversity_infoDTO.iD);
				
//					if(mapOfUniversity_infoDTOTouniversityNameEn.containsKey(oldUniversity_infoDTO.universityNameEn)) {
//						mapOfUniversity_infoDTOTouniversityNameEn.get(oldUniversity_infoDTO.universityNameEn).remove(oldUniversity_infoDTO);
//					}
//					if(mapOfUniversity_infoDTOTouniversityNameEn.get(oldUniversity_infoDTO.universityNameEn).isEmpty()) {
//						mapOfUniversity_infoDTOTouniversityNameEn.remove(oldUniversity_infoDTO.universityNameEn);
//					}
//
//					if(mapOfUniversity_infoDTOTouniversityNameBn.containsKey(oldUniversity_infoDTO.universityNameBn)) {
//						mapOfUniversity_infoDTOTouniversityNameBn.get(oldUniversity_infoDTO.universityNameBn).remove(oldUniversity_infoDTO);
//					}
//					if(mapOfUniversity_infoDTOTouniversityNameBn.get(oldUniversity_infoDTO.universityNameBn).isEmpty()) {
//						mapOfUniversity_infoDTOTouniversityNameBn.remove(oldUniversity_infoDTO.universityNameBn);
//					}
//
//					if(mapOfUniversity_infoDTOToinsertionDate.containsKey(oldUniversity_infoDTO.insertionDate)) {
//						mapOfUniversity_infoDTOToinsertionDate.get(oldUniversity_infoDTO.insertionDate).remove(oldUniversity_infoDTO);
//					}
//					if(mapOfUniversity_infoDTOToinsertionDate.get(oldUniversity_infoDTO.insertionDate).isEmpty()) {
//						mapOfUniversity_infoDTOToinsertionDate.remove(oldUniversity_infoDTO.insertionDate);
//					}
//
//					if(mapOfUniversity_infoDTOToinsertedBy.containsKey(oldUniversity_infoDTO.insertedBy)) {
//						mapOfUniversity_infoDTOToinsertedBy.get(oldUniversity_infoDTO.insertedBy).remove(oldUniversity_infoDTO);
//					}
//					if(mapOfUniversity_infoDTOToinsertedBy.get(oldUniversity_infoDTO.insertedBy).isEmpty()) {
//						mapOfUniversity_infoDTOToinsertedBy.remove(oldUniversity_infoDTO.insertedBy);
//					}
//
//					if(mapOfUniversity_infoDTOTomodifiedBy.containsKey(oldUniversity_infoDTO.modifiedBy)) {
//						mapOfUniversity_infoDTOTomodifiedBy.get(oldUniversity_infoDTO.modifiedBy).remove(oldUniversity_infoDTO);
//					}
//					if(mapOfUniversity_infoDTOTomodifiedBy.get(oldUniversity_infoDTO.modifiedBy).isEmpty()) {
//						mapOfUniversity_infoDTOTomodifiedBy.remove(oldUniversity_infoDTO.modifiedBy);
//					}
//
//					if(mapOfUniversity_infoDTOTolastModificationTime.containsKey(oldUniversity_infoDTO.lastModificationTime)) {
//						mapOfUniversity_infoDTOTolastModificationTime.get(oldUniversity_infoDTO.lastModificationTime).remove(oldUniversity_infoDTO);
//					}
//					if(mapOfUniversity_infoDTOTolastModificationTime.get(oldUniversity_infoDTO.lastModificationTime).isEmpty()) {
//						mapOfUniversity_infoDTOTolastModificationTime.remove(oldUniversity_infoDTO.lastModificationTime);
//					}
					
					
				}
				if(university_infoDTO.isDeleted == 0) 
				{
					
					mapOfUniversity_infoDTOToid.put(university_infoDTO.iD, university_infoDTO);
				
//					if( ! mapOfUniversity_infoDTOTouniversityNameEn.containsKey(university_infoDTO.universityNameEn)) {
//						mapOfUniversity_infoDTOTouniversityNameEn.put(university_infoDTO.universityNameEn, new HashSet<>());
//					}
//					mapOfUniversity_infoDTOTouniversityNameEn.get(university_infoDTO.universityNameEn).add(university_infoDTO);
//
//					if( ! mapOfUniversity_infoDTOTouniversityNameBn.containsKey(university_infoDTO.universityNameBn)) {
//						mapOfUniversity_infoDTOTouniversityNameBn.put(university_infoDTO.universityNameBn, new HashSet<>());
//					}
//					mapOfUniversity_infoDTOTouniversityNameBn.get(university_infoDTO.universityNameBn).add(university_infoDTO);
//
//					if( ! mapOfUniversity_infoDTOToinsertionDate.containsKey(university_infoDTO.insertionDate)) {
//						mapOfUniversity_infoDTOToinsertionDate.put(university_infoDTO.insertionDate, new HashSet<>());
//					}
//					mapOfUniversity_infoDTOToinsertionDate.get(university_infoDTO.insertionDate).add(university_infoDTO);
//
//					if( ! mapOfUniversity_infoDTOToinsertedBy.containsKey(university_infoDTO.insertedBy)) {
//						mapOfUniversity_infoDTOToinsertedBy.put(university_infoDTO.insertedBy, new HashSet<>());
//					}
//					mapOfUniversity_infoDTOToinsertedBy.get(university_infoDTO.insertedBy).add(university_infoDTO);
//
//					if( ! mapOfUniversity_infoDTOTomodifiedBy.containsKey(university_infoDTO.modifiedBy)) {
//						mapOfUniversity_infoDTOTomodifiedBy.put(university_infoDTO.modifiedBy, new HashSet<>());
//					}
//					mapOfUniversity_infoDTOTomodifiedBy.get(university_infoDTO.modifiedBy).add(university_infoDTO);
//
//					if( ! mapOfUniversity_infoDTOTolastModificationTime.containsKey(university_infoDTO.lastModificationTime)) {
//						mapOfUniversity_infoDTOTolastModificationTime.put(university_infoDTO.lastModificationTime, new HashSet<>());
//					}
//					mapOfUniversity_infoDTOTolastModificationTime.get(university_infoDTO.lastModificationTime).add(university_infoDTO);
//
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<University_infoDTO> getUniversity_infoList() {
		List <University_infoDTO> university_infos = new ArrayList<University_infoDTO>(this.mapOfUniversity_infoDTOToid.values());
		return university_infos;
	}
	
	
	public University_infoDTO getUniversity_infoDTOByid( long id){
		return mapOfUniversity_infoDTOToid.get(id);
	}
	
	
//	public List<University_infoDTO> getUniversity_infoDTOByuniversity_name_en(String university_name_en) {
//		return new ArrayList<>( mapOfUniversity_infoDTOTouniversityNameEn.getOrDefault(university_name_en,new HashSet<>()));
//	}
//
//
//	public List<University_infoDTO> getUniversity_infoDTOByuniversity_name_bn(String university_name_bn) {
//		return new ArrayList<>( mapOfUniversity_infoDTOTouniversityNameBn.getOrDefault(university_name_bn,new HashSet<>()));
//	}
//
//
//	public List<University_infoDTO> getUniversity_infoDTOByinsertion_date(long insertion_date) {
//		return new ArrayList<>( mapOfUniversity_infoDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
//	}
//
//
//	public List<University_infoDTO> getUniversity_infoDTOByinserted_by(String inserted_by) {
//		return new ArrayList<>( mapOfUniversity_infoDTOToinsertedBy.getOrDefault(inserted_by,new HashSet<>()));
//	}
//
//
//	public List<University_infoDTO> getUniversity_infoDTOBymodified_by(String modified_by) {
//		return new ArrayList<>( mapOfUniversity_infoDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
//	}
//
//
//	public List<University_infoDTO> getUniversity_infoDTOBylastModificationTime(long lastModificationTime) {
//		return new ArrayList<>( mapOfUniversity_infoDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
//	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "university_info";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


