package university_info;
import java.util.*; 
import util.*; 


public class University_infoDTO extends CommonDTO
{

    public String universityNameEn = "";
    public String universityNameBn = "";
	public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";


    @Override
    public String toString() {
        return "University_infoDTO{" +
                "universityNameEn='" + universityNameEn + '\'' +
                ", universityNameBn='" + universityNameBn + '\'' +
                ", insertionDate=" + insertionDate +
                ", insertedBy='" + insertedBy + '\'' +
                ", modifiedBy='" + modifiedBy + '\'' +
                '}';
    }

}