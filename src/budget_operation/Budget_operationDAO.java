package budget_operation;

import common.EmployeeCommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class Budget_operationDAO implements EmployeeCommonDAOService<Budget_operationDTO> {
    private static final Logger logger = Logger.getLogger(Budget_operationDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (code,description_eng,description_bng,"
            .concat("modified_by,lastModificationTime,insertion_time,inserted_by,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery = "UPDATE {tableName} SET code=?,description_eng=?,description_bng=?,"
            .concat("modified_by=?,lastModificationTime=? WHERE ID=?");

    private static Budget_operationDAO INSTANCE = null;

    public static Budget_operationDAO getInstance() {
        if (INSTANCE == null) {
            synchronized (Budget_operationDAO.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Budget_operationDAO();
                }
            }
        }
        return INSTANCE;
    }

    private Budget_operationDAO() {
    }

    @Override
    public void set(PreparedStatement ps, Budget_operationDTO budget_operationDTO, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setObject(++index, budget_operationDTO.code);
        ps.setObject(++index, budget_operationDTO.descriptionEn);
        ps.setObject(++index, budget_operationDTO.descriptionBn);
        ps.setObject(++index, budget_operationDTO.modifiedBy);
        ps.setObject(++index, budget_operationDTO.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, budget_operationDTO.insertionTime);
            ps.setObject(++index, budget_operationDTO.insertedBy);
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, budget_operationDTO.iD);
    }

    @Override
    public Budget_operationDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Budget_operationDTO dto = new Budget_operationDTO();
            dto.iD = rs.getLong("ID");
            dto.code = rs.getString("code");
            dto.descriptionEn = rs.getString("description_eng");
            dto.descriptionBn = rs.getString("description_bng");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "budget_operation";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return null;
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Budget_operationDTO) commonDTO, updateQuery, false);
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Budget_operationDTO) commonDTO, addQuery, true);
    }

    public Budget_operationDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }

    public List<Budget_operationDTO> getAllBudgetOperation(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }
}