package budget_operation;

import economic_sub_code.EconomicSubCodeModel;

import java.util.List;

public class BudgetOperationModel {
    public String budgetMappingId = "";
    public String description = "";
    public String operationCode = "";
    public String institutionalName = "";
    public String institutionalCode = "";
    public String officeName = "";
    public String officeCode = "";
    public String budgetCatName = "";
    public String economicYear = "";
    public List<EconomicSubCodeModel> economicSubCodeModels;
}
