package budget_operation;

import pb.OptionDTO;
import util.CommonDTO;
import util.StringUtils;

public class Budget_operationDTO extends CommonDTO {
    public String code = "";
    public String descriptionEn = "";
    public String descriptionBn = "";
    public long insertedBy = 0;
    public long insertionTime = 0;
    public long modifiedBy = 0;

    public OptionDTO getOptionDTO(boolean withCode) {
        return new OptionDTO(
                (withCode ? code.concat(" - ") : "").concat(descriptionEn),
                (withCode ? StringUtils.convertToBanNumber(code).concat(" - ") : "").concat(descriptionBn),
                String.valueOf(iD),
                "",
                code
        );
    }

    @Override
    public String toString() {
        return "Budget_operationDTO{" +
               "code='" + code + '\'' +
               ", descriptionEn='" + descriptionEn + '\'' +
               ", descriptionBn='" + descriptionBn + '\'' +
               ", insertedBy=" + insertedBy +
               ", insertionTime=" + insertionTime +
               ", modifiedBy=" + modifiedBy +
               '}';
    }
}
