package budget_operation;

import budget_office.Budget_officeDTO;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import util.LockManager;
import util.StringUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class Budget_operationRepository {
    private static final Logger logger = Logger.getLogger(Budget_operationRepository.class);
    private final Budget_operationDAO budget_operationDAO;
    private Map<Long, Budget_operationDTO> mapById;
    private List<Budget_operationDTO> budget_operationDTOList;

    private Budget_operationRepository() {
        budget_operationDAO = Budget_operationDAO.getInstance();
        mapById = new ConcurrentHashMap<>();
        reload();
    }

    private static class BudgetOperationRepositoryLazyLoader {
        final static Budget_operationRepository INSTANCE = new Budget_operationRepository();
    }

    public static Budget_operationRepository getInstance() {
        return Budget_operationRepository.BudgetOperationRepositoryLazyLoader.INSTANCE;
    }

    private void reload() {
        logger.debug("Budget Operation Repository loading start for reload: ");
        List<Budget_operationDTO> budget_operationDTOS = budget_operationDAO.getAllBudgetOperation(true);
        if (budget_operationDTOS != null && budget_operationDTOS.size() > 0) {
            budget_operationDTOList = budget_operationDTOS.stream()
                    .filter(dto -> dto.isDeleted == 0)
                    .collect(Collectors.collectingAndThen(Collectors.toList(), ls -> {
                        ls.sort(Comparator.comparing(o -> o.iD));
                        return ls;
                    }));
            mapById = budget_operationDTOList.stream()
                    .collect(Collectors.toMap(e -> e.iD, e -> e, (e1, e2) -> e1));
        } else {
            budget_operationDTOList = new ArrayList<>();
            mapById = new ConcurrentHashMap<>();
        }
        logger.debug("Budget Operation Repository loading end for reload: ");
    }

    public Budget_operationDTO getById(long id) {

        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "BOPR")) {
                if (mapById.get(id) == null) {
                    Budget_operationDTO dto = budget_operationDAO.getDTOFromIdDeletedOrNot(id);
                    if (dto != null) {
                        mapById.put(dto.iD, dto);
                        if (dto.isDeleted == 0) {
                            budget_operationDTOList.add(dto);
                        }
                    }
                }
            }
        }
        return mapById.get(id);
    }

    public String getCode(long id, String language) {
        Budget_operationDTO dto = getById(id);
        if (dto == null) return "";
        return StringUtils.convertBanglaIfLanguageIsBangla(language, dto.code);
    }

    public List<Budget_operationDTO> getBudget_operationDTOList() {
        return budget_operationDTOList;
    }

    public String getTextById(String language, long id) {
        Budget_operationDTO dto = getById(id);
        String code = dto.code;
        if (!language.equalsIgnoreCase("ENGLISH")) {
            code = StringUtils.convertToBanNumber(code);
            return code + " - " + dto.descriptionBn;
        }
        return code + " - " + dto.descriptionEn;
    }

    public String getTextWithoutCodeById(String language, long id) {
        Budget_operationDTO dto = getById(id);
        if (dto == null) return "";

        return language.equalsIgnoreCase("ENGLISH")
                ? dto.descriptionEn
                : dto.descriptionBn;
    }

    public String getCode(String language, long id) {
        Budget_operationDTO dto = getById(id);
        if (dto == null) return "";
        return StringUtils.convertBanglaIfLanguageIsBangla(language, dto.code);
    }

    public String buildOperationCodes(String language, Long selectedId) {
        List<OptionDTO> optionDTOList = mapById.values().stream()
                .sorted((a, b) -> a.code.compareToIgnoreCase(b.code)) // sort in ascending order
                .map(dto -> dto.getOptionDTO(true))
                .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }
}