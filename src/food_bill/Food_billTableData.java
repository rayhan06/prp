package food_bill;

import food_allowance.Food_allowanceModel;

import java.util.ArrayList;
import java.util.List;

public class Food_billTableData {
    public String errorMessage = null;
    public boolean isEditable = false;
    public List<Long> sortedBillDates = new ArrayList<>();
    public List<Food_allowanceModel> allowanceModels = new ArrayList<>();
    public List<Long> deletedFoodAllowanceIds = new ArrayList<>();
}
