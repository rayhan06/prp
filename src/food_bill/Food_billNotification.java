package food_bill;

import bill_approval_history.Bill_approval_historyDAO;
import bill_approval_history.Bill_approval_historyDTO;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import parliament_session.Parliament_sessionRepository;
import pb.CatRepository;
import pb_notifications.Pb_notificationsDAO;
import sessionmanager.SessionConstants;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class Food_billNotification {
    private final Logger logger = Logger.getLogger(Food_billNotification.class);

    private final Pb_notificationsDAO pb_notificationsDAO;

    private Food_billNotification() {
        pb_notificationsDAO = Pb_notificationsDAO.getInstance();
    }

    private static class LazyLoader {
        static final Food_billNotification INSTANCE = new Food_billNotification();
    }

    public static Food_billNotification getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void send(Food_billDTO foodBillDTO, List<Bill_approval_historyDTO> nextApprovalHistoryDTOs) {
        if (nextApprovalHistoryDTOs == null || nextApprovalHistoryDTOs.isEmpty()) {
            return;
        }
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(foodBillDTO.officeUnitId);
        String officeNameEn = "", officeNameBn = "";
        if (officeUnitsDTO != null) {
            officeNameEn = officeUnitsDTO.unitNameEng;
            officeNameBn = officeUnitsDTO.unitNameBng;
        }
        String monthNameEn = foodBillDTO.getSessionAndMonthText(true);
        String monthNameBn = foodBillDTO.getSessionAndMonthText(false);
        String billTypeTextEn = foodBillDTO.getBillTypeText(true);
        String billTypeTextBn = foodBillDTO.getBillTypeText(false);

        String textEn = billTypeTextEn + " Food bill of " + monthNameEn + " of " + officeNameEn + " is waiting for your approval";
        String textBn = monthNameBn + " -এর " + officeNameBn + "-এর " + billTypeTextBn +" খাবার ভাতার বিল আপনার আনুমোদনের জন্য অপেক্ষমান";

        String notificationMessage = textEn + "$" + textBn;
        String url = "Food_billServlet?actionType=view&ID=" + foodBillDTO.iD;
        sendNotification(
                nextApprovalHistoryDTOs.stream()
                                       .map(approvalHistoryDTO -> approvalHistoryDTO.organogramId)
                                       .collect(Collectors.toList()),
                notificationMessage,
                url
        );
    }

    public void sendModificationNotification(Food_billDTO foodBillDTO,
                                             Bill_approval_historyDTO approvalHistoryOfUser) {
        if (foodBillDTO.isInPreviewStage) {
            logger.debug(String.format(
                    "Food_billDTO[id=%d] - modification notification NOT SENT - cause isInPreviewStage",
                    foodBillDTO.iD
            ));
            return;
        }
        List<Long> organogramIdsForNotification =
                Bill_approval_historyDAO
                        .getInstance()
                        .findAllApprovedHistory(foodBillDTO.getTableName(), foodBillDTO.getId())
                        .stream()
                        .map(approvalHistoryDTO -> approvalHistoryDTO.organogramId)
                        .filter(organogramId -> !Objects.equals(organogramId, approvalHistoryOfUser.organogramId))
                        .collect(Collectors.toList());
        if (foodBillDTO.preparedOrgId > 0) {
            organogramIdsForNotification.add(foodBillDTO.preparedOrgId);
        }
        if (organogramIdsForNotification.isEmpty()) {
            logger.debug(String.format(
                    "Food_billDTO[id=%d] - modification notification NOT SENT - cause organogramIdsForNotification isEmpty",
                    foodBillDTO.iD
            ));
            return;
        }
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(foodBillDTO.officeUnitId);
        String officeNameEn = "", officeNameBn = "";
        if (officeUnitsDTO != null) {
            officeNameEn = officeUnitsDTO.unitNameEng;
            officeNameBn = officeUnitsDTO.unitNameBng;
        }
        String monthNameEn = foodBillDTO.getSessionAndMonthText(true);
        String monthNameBn = foodBillDTO.getSessionAndMonthText(false);
        String billTypeTextEn = foodBillDTO.getBillTypeText(true);
        String billTypeTextBn = foodBillDTO.getBillTypeText(false);

        String textEn = monthNameEn + " month's " + billTypeTextEn + " Food Bill of " + officeNameEn + " is modified by "
                        + approvalHistoryOfUser.nameEn + "," + approvalHistoryOfUser.orgNameEn;
        String textBn = monthNameBn + " মাসের, " + officeNameBn + "-এর " + billTypeTextBn + "খাবার ভাতার বিল "
                        + approvalHistoryOfUser.nameBn + "," + approvalHistoryOfUser.orgNameBn
                        + " পরিবর্তন করেছেন";
        String notificationMessage = textEn + "$" + textBn;
        String url = "Food_billServlet?actionType=view&ID=" + foodBillDTO.iD;
        sendNotification(organogramIdsForNotification, notificationMessage, url);
    }

    private void sendNotification(List<Long> organogramIds, String notificationMessage, String url) {
        if (organogramIds == null || organogramIds.size() == 0) return;

        Thread thread = new Thread(() -> {
            long currentTime = System.currentTimeMillis();
            organogramIds.forEach(organogramId -> pb_notificationsDAO.addPb_notifications(organogramId, currentTime, url, notificationMessage));
        });
        thread.setDaemon(true);
        thread.start();
    }
}
