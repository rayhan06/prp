package food_bill;


import allowance_configure.AllowanceCatEnum;
import allowance_configure.Allowance_configureDAO;
import allowance_configure.Allowance_configureDTO;
import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import bangladehi_number_format_util.BangladeshiNumberFormatter;
import bill_approval_history.BillApprovalResponse;
import bill_approval_history.BillApprovalStatus;
import bill_approval_history.Bill_approval_historyDAO;
import bill_approval_history.Bill_approval_historyDTO;
import bill_register.Bill_registerServlet;
import budget.BudgetUtils;
import budget_mapping.Budget_mappingDTO;
import budget_mapping.Budget_mappingRepository;
import budget_office.Budget_officeRepository;
import budget_register.Budget_registerDAO;
import budget_register.Budget_registerDTO;
import budget_selection_info.BudgetSelectionInfoRepository;
import com.google.gson.Gson;
import common.*;
import finance.CashTypeEnum;
import finance.FinanceUtil;
import food_allowance.Food_allowanceDAO;
import food_allowance.Food_allowanceDTO;
import food_allowance.Food_allowanceEmployeeIdModel;
import food_allowance.Food_allowanceModel;
import food_bill_finance.Food_bill_financeDAO;
import food_bill_finance.Food_bill_financeDTO;
import food_bill_submission_config.Food_bill_submission_configDAO;
import food_bill_submission_config.Food_bill_submission_configDTO;
import food_bill_type_config.Food_bill_type_configDTO;
import food_bill_type_config.Food_bill_type_configRepository;
import login.LoginDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.OfficeUnitTypeEnum;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import ot_type_calendar.OT_type_CalendarDAO;
import ot_type_calendar.OT_type_CalendarDTO;
import ot_type_calendar.OT_type_CalendarServlet;
import overtime_bill.OvertimeType;
import overtime_bill.Overtime_billStatus;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static overtime_bill.Overtime_billStatus.PENDING;

@SuppressWarnings({"Duplicates", "unchecked"})
@WebServlet("/Food_billServlet")
@MultipartConfig
public class Food_billServlet extends BaseServlet {

    private static final Logger logger = Logger.getLogger(Food_billServlet.class);
    public static final Set<Integer> ONE_LAYER_OFFICE_TYPES = new HashSet<>(Arrays.asList(
            OfficeUnitTypeEnum.SPEAKER.getValue(),
            OfficeUnitTypeEnum.SECRETARY.getValue(),
            OfficeUnitTypeEnum.WINGS.getValue(),
            OfficeUnitTypeEnum.BRANCH.getValue(),
            OfficeUnitTypeEnum.DIVISION.getValue(),
            OfficeUnitTypeEnum.UNIT.getValue(),
            OfficeUnitTypeEnum.NATIONAL_PARLIAMENT.getValue()
    ));
    public static final CashTypeEnum CASH_TYPE = CashTypeEnum.ALLOWANCE;

    private static class UserInput {
        public String addEditRequestSource;
        Long billRegisterId;
        Long budgetRegisterId;
        Food_bill_financeDTO foodBillFinanceDTO;
        Integer employeeCount;
        Long billAmount;
        Long dailyRate;
        Integer revenueStampDeduction;
        Food_allowanceModel foodAllowanceModel;
        Long employeeRecordsId;
        Long foodBillId;
        Long foodBillSubmissionConfigId;
        Long officeUnitsId;
        Long budgetOfficeId;
        Long budgetMappingId;
        Long economicSubCodeId;
        Food_bill_submission_configDTO foodBillSubmissionConfigDTO;
        Long modifiedTime;
        Long modifierId;
        Long modificationTime;
        Long minimumMonthTimeStamp;
        String language;
        String sessionNameText;
        String billTypeText;
        Boolean isLangEn;
        Boolean isInPreviewStage;
        List<Long> sortedBillDateList;
        List<Integer> sortedBillDateOtTypeList;
        List<Long> deletedFoodAllowanceIds;
        List<Food_allowanceModel> allowanceModelListFromRequest;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        String actionType = request.getParameter("actionType");
        try {
            switch (actionType) {
                case "getAddPage":
                    if (Utils.checkPermission(userDTO, MenuConstants.FOOD_BILL_ADD)) {
                        request.getRequestDispatcher("food_bill/food_billEdit.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "getEditPage":
                    if (Utils.checkPermission(userDTO, MenuConstants.FOOD_BILL_ADD)) {
                        Long id = Utils.parseOptionalLong(request.getParameter("ID"), null, null);
                        String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

                        Food_billModel foodBillModel = getFoodBillModelFromDB(id, language);
                        request.setAttribute("foodBillDTO", foodBillModel.foodBillDTO);
                        Food_billTableData foodBillTableData = new Food_billTableData();
                        foodBillTableData.sortedBillDates = foodBillModel.foodBillDTO.sortedBillDateList;
                        foodBillTableData.allowanceModels = foodBillModel.allowanceModels;
                        try {
                            foodBillModel.foodBillDTO.checkEditabilityOrThrowException(userDTO, language);
                            foodBillTableData.isEditable = true;
                        } catch (Exception ex) {
                            logger.error(ex);
                            request.setAttribute("notEditableCause", ex.getMessage());
                            foodBillTableData.isEditable = false;
                        }
                        request.setAttribute("foodBillTableData", foodBillTableData);
                        request.getRequestDispatcher("food_bill/food_billEdit.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "ajax_getFoodBillData":
                    if (Utils.checkPermission(userDTO, MenuConstants.FOOD_BILL_ADD)) {
                        UserInput userInput = new UserInput();
                        userInput.foodBillSubmissionConfigId = Utils.parseOptionalLong(
                                request.getParameter("foodBillSubmissionConfigId"),
                                -1L,
                                null
                        );
                        userInput.officeUnitsId = Utils.parseOptionalLong(
                                request.getParameter("officeUnitsId"),
                                -1L,
                                null
                        );
                        userInput.language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
                        userInput.isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
                        request.setAttribute("foodBillTableData", getFoodBillTableData(userInput));
                        request.getRequestDispatcher("food_bill/foodBillTable.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "search": {
                    Map<String, String> extraCriteriaMap;
                    extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
                    if (extraCriteriaMap == null) extraCriteriaMap = new HashMap<>();
                    extraCriteriaMap.put("isInPreview", "0");
                    if (!isAllowedToSeeAllOfficeBill(userDTO)) {
                        Office_unitsDTO usersAllowedOfficeUnitDTO = getOfficeUnitFromUser(userDTO);
                        long usersAllowedOfficeUnitId = usersAllowedOfficeUnitDTO == null ? -1 : usersAllowedOfficeUnitDTO.iD;
                        String restrictedViewWhereClause = String.format(
                                Food_billDAO.restrictedViewWhereClause,
                                usersAllowedOfficeUnitId,
                                userDTO.organogramID
                        );
                        request.setAttribute("Food_billServlet.restrictedViewWhereClause", restrictedViewWhereClause);
                    }
                    String isInPreview = request.getParameter("isInPreview");
                    if (isInPreview == null) {
                        request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                    }
                    super.doGet(request, response);
                    return;
                }
                case "getPrepareBillPage":
                    if (Utils.checkPermission(userDTO, MenuConstants.FOOD_BILL_FINANCE)) {
                        request.getRequestDispatcher("food_bill/food_billPrepareBill.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "ajax_getBillDataForFinance": {
                    Long budgetMappingId = Utils.parseOptionalLong(
                            request.getParameter("budgetMappingId"),
                            null,
                            null
                    );
                    Long economicSubCodeId = Utils.parseOptionalLong(
                            request.getParameter("economicSubCodeId"),
                            null,
                            null
                    );
                    Long foodBillSubmissionConfigId = Utils.parseOptionalLong(
                            request.getParameter("foodBillSubmissionConfigId"),
                            null,
                            null
                    );
                    String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
                    Map<String, Object> billDataForFinance = getBillDataForFinance(
                            budgetMappingId, economicSubCodeId, foodBillSubmissionConfigId, language
                    );
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().println(new Gson().toJson(billDataForFinance));
                    return;
                }
                case "missingOfficesList": {
                    if (Utils.checkPermission(userDTO, MenuConstants.FOOD_BILL_MISSING_OFFICES)) {
                        request.getRequestDispatcher("food_bill/food_billMissingOffices.jsp").forward(request, response);
                        return;
                    }
                }
                case "ajax_getMissingOfficesList": {
                    Map<String, Object> res = new HashMap<>();
                    try {
                        Map<String, Object> missingOfficesList = getMissingOfficesList(request);
                        res.put("success", true);
                        res.put("error", "");
                        res.put("data", missingOfficesList);
                    } catch (Exception e) {
                        res = new HashMap<>();
                        res.put("success", false);
                        res.put("error", e.getMessage());
                        res.put("data", new HashMap<>());
                    }
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().println(new Gson().toJson(res));
                    return;
                }
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private Map<String, Object> getBillDataForFinance(Long budgetMappingId, Long economicSubCodeId,
                                                      Long foodBillSubmissionConfigId, String language) {
        boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Map<String, Object> res = new HashMap<>();
        res.put("success", false);
        String error = null;
        if (budgetMappingId == null) {
            error = isLangEn ? "Select Operation Code" : "অপারেশন কোড বাছাই করুন";
        }
        if (economicSubCodeId == null) {
            error = isLangEn ? "Select Economic Code" : "অর্থনৈতিক কোড বাছাই করুন";
        }
        if (foodBillSubmissionConfigId == null) {
            error = isLangEn ? "Select Parliament Session" : "সংসদ অধিবেশন বাছাই করুন";
        }
        if (error != null) {
            res.put("error", error);
            return res;
        }
        List<Food_billDTO> financeBillNotPreparedDtoList =
                Food_billDAO.getInstance().findBillNotPreparedDtoList(budgetMappingId, economicSubCodeId, foodBillSubmissionConfigId);
        if (financeBillNotPreparedDtoList.isEmpty()) {
            res.put("error", isLangEn ? "No new bill has been found" : "নতুন কোন বিল পাওয়া যায়নি");
            return res;
        }
        res.put("success", true);

        long billAmount = 0, totalEmployeeCount = 0;
        for (Food_billDTO foodBillDTO : financeBillNotPreparedDtoList) {
            billAmount += foodBillDTO.billAmount;
            totalEmployeeCount += foodBillDTO.employeeCount;
        }
        String billAmountStr = BangladeshiNumberFormatter.getFormattedNumber(
                StringUtils.convertBanglaIfLanguageIsBangla(language, String.format("%d", billAmount))
        );
        res.put("billAmount", billAmountStr);

        String totalEmployeeCountStr = StringUtils.convertBanglaIfLanguageIsBangla(language, String.format("%d", totalEmployeeCount));
        res.put("totalEmployeeCount", totalEmployeeCountStr);

        List<Food_billSummaryModel> foodBillSummaryModels =
                financeBillNotPreparedDtoList.stream()
                                             .map(foodBillDTO -> new Food_billSummaryModel(foodBillDTO, language))
                                             .collect(Collectors.toList());
        res.put("foodBillSummaryModels", foodBillSummaryModels);
        return res;
    }

    private Map<String, Object> getMissingOfficesList(HttpServletRequest request) throws Exception {
        UserInput userInput = new UserInput();
        userInput.language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        userInput.isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        userInput.budgetOfficeId = Utils.parseMandatoryLong(
                request.getParameter("budgetOfficeId"),
                userInput.isLangEn ? "Select Office" : "কার্যালয় বাছাই করুন"
        );
        userInput.foodBillSubmissionConfigId = Utils.parseMandatoryLong(
                request.getParameter("foodBillSubmissionConfigId"),
                userInput.isLangEn ? "Select Parliament Session" : "সংসদ অধিবেশন বাছাই করুন"
        );
        Map<String, Object> res = new HashMap<>();
        List<Food_billDTO> officeApprovedBillDTOs =
                Food_billDAO.getInstance()
                            .getOfficeApprovedBillDTOs(
                                    userInput.budgetOfficeId,
                                    userInput.foodBillSubmissionConfigId
                            );
        res.put("missingOfficesName", getMissingOfficesName(officeApprovedBillDTOs, userInput.budgetOfficeId, userInput.isLangEn));
        return res;
    }

    private List<String> getMissingOfficesName(List<Food_billDTO> submittedBillDTOs, long budgetOfficeId, boolean isLangEn) {
        Set<Long> submittedOfficeUnitIds = submittedBillDTOs.stream()
                                                            .map(billDTO -> billDTO.officeUnitId)
                                                            .collect(Collectors.toSet());
        return Budget_officeRepository
                .getInstance()
                .getOfficeUnitIdSet(budgetOfficeId)
                .stream()
                .map(officeUnitId -> Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitId))
                .filter(officeUnitDTO -> Food_billServlet.isValidOfficeType(officeUnitDTO.officeOrder))
                .filter(officeUnitDTO -> !submittedOfficeUnitIds.contains(officeUnitDTO.iD))
                .map(officeUnitDTO -> isLangEn ? officeUnitDTO.unitNameEng : officeUnitDTO.unitNameBng)
                .collect(Collectors.toList());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        String actionType = request.getParameter("actionType");
        try {
            switch (actionType) {
                case "ajax_approve": {
                    Map<String, Object> res = new HashMap<>();
                    try {
                        approve(request, userDTO);
                        res.put("success", true);
                        res.put("message", "");
                    } catch (Exception ex) {
                        logger.error(ex);
                        res.put("success", false);
                        res.put("message", ex.getMessage());
                    }
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().print(gson.toJson(res));
                    break;
                }
                case "ajax_submit": {
                    Map<String, Object> res = submitFoodBill(request, userDTO);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().print(gson.toJson(res));
                    break;
                }
                case "ajax_prepareBill_add": {
                    try {
                        String redirectUrl = prepareBillFinance(request, true, userDTO);
                        ApiResponse.sendSuccessResponse(response, redirectUrl);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        logger.error(ex);
                        ApiResponse.sendErrorResponse(response, ex.getMessage());
                    }
                    break;
                }
                case "ajax_prepareBill_edit": {
                    try {
                        String redirectUrl = prepareBillFinance(request, false, userDTO);
                        ApiResponse.sendSuccessResponse(response, redirectUrl);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        logger.error(ex);
                        ApiResponse.sendErrorResponse(response, ex.getMessage());
                    }
                    break;
                }
                case "ajax_deleteBill": {
                    Map<String, Object> res = deleteBill(request, userDTO);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().print(gson.toJson(res));
                    break;
                }
                default:
                    super.doPost(request, response);
                    break;
            }
            return;
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    public void deleteBillDtoOrThrow(String idStr, UserDTO userDTO, boolean isLangEn) throws Exception {
        long id = Utils.parseMandatoryLong(idStr, "invalid id=" + idStr);
        Food_billDTO billDTO = Food_billDAO.getInstance().getDTOFromID(id);
        if (billDTO == null) {
            throw new CustomException(isLangEn ? "Already deleted!" : "ইতোমধ্যে ডিলিট করা হয়েছে!");
        }
        if (!billDTO.isDeletableByUser(userDTO)) {
            throw new CustomException(isLangEn ? "You are not allowed to delete" : "আপনার ডিলিট করার অনুমতি নেই");
        }
        if (billDTO.hasFinancePreparedBill()) {
            throw new CustomException(
                    isLangEn ? "Unable to delete because Finance has already prepared this bill!"
                             : "ডিলিট করতে ব্যর্থ হয়েছে কারণ অর্থ শাখা বিল প্রস্তুত করে ফেলেছে"
            );
        }
        boolean isDeleted = Food_billDAO.getInstance().deleteBillById(id, userDTO.ID, System.currentTimeMillis());
        if (!isDeleted) {
            throw new RuntimeException("failed to delete food_billDTO.id=" + id);
        }
    }

    private Map<String, Object> deleteBill(HttpServletRequest request, UserDTO userDTO) {
        boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Map<String, Object> res = new HashMap<>();
        res.put("success", true);
        res.put("message", isLangEn ? "Deleted successfully" : "সফলভাবে ডিলিট করা হয়েছে");

        try {
            deleteBillDtoOrThrow(request.getParameter("ID"), userDTO, isLangEn);
        } catch (CustomException customException) {
            logger.error(customException);
            res.put("success", false);
            res.put("message", customException.getMessage());
        } catch (Exception e) {
            logger.error(e);
            res.put("success", false);
            res.put("message", isLangEn ? "Failed to delete" : "ডিলিট করতে ব্যর্থ হয়েছে");
        }
        return res;
    }

    private Food_bill_financeDTO addFoodBillFinanceDTO(UserInput userInput) throws Exception {
        Food_bill_financeDTO billFinanceDTO = new Food_bill_financeDTO();

        billFinanceDTO.budgetMappingId = userInput.budgetMappingId;
        billFinanceDTO.economicSubCodeId = userInput.economicSubCodeId;
        billFinanceDTO.foodBillSubmissionConfigId = userInput.foodBillSubmissionConfigId;
        billFinanceDTO.parliamentNumber = userInput.foodBillSubmissionConfigDTO.parliamentNumber;
        billFinanceDTO.sessionNumber = userInput.foodBillSubmissionConfigDTO.sessionNumber;
        billFinanceDTO.foodBillTypeCat = userInput.foodBillSubmissionConfigDTO.foodBillTypeCat;
        billFinanceDTO.budgetRegisterId = userInput.budgetRegisterId;
        billFinanceDTO.billRegisterId = userInput.billRegisterId;
        billFinanceDTO.totalEmployeeCount = userInput.employeeCount;
        billFinanceDTO.totalBillAmount = userInput.billAmount;

        billFinanceDTO.insertionTime = billFinanceDTO.lastModificationTime = userInput.modificationTime;
        billFinanceDTO.insertedBy = billFinanceDTO.modifiedBy = userInput.modifierId;

        Food_bill_financeDAO.getInstance().add(billFinanceDTO);
        return billFinanceDTO;
    }

    public String prepareBillFinance(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        if (!addFlag) {
            throw new CustomException("no edit support given");
        }
        UserInput userInput = new UserInput();
        userInput.language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        userInput.isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        userInput.budgetMappingId = Utils.parseMandatoryLong(
                request.getParameter("budgetMappingId"),
                userInput.isLangEn ? "Select Operation Code" : "অপারেশন কোড বাছাই করুন"
        );
        userInput.economicSubCodeId = Utils.parseMandatoryLong(
                request.getParameter("economicSubCodeId"),
                userInput.isLangEn ? "Select Economic Code" : "অর্থনৈতিক কোড বাছাই করুন"
        );
        userInput.foodBillSubmissionConfigId = Utils.parseMandatoryLong(
                request.getParameter("foodBillSubmissionConfigId"),
                userInput.isLangEn ? "Select Parliament Session" : "সংসদ অধিবেশন বাছাই করুন"
        );
        userInput.foodBillSubmissionConfigDTO = Food_bill_submission_configDAO.getInstance().getDTOFromID(userInput.foodBillSubmissionConfigId);
        if (userInput.foodBillSubmissionConfigDTO == null) {
            logger.error("No Food_bill_submission_configDTO found with id=" + userInput.foodBillSubmissionConfigId);
            throw new CustomException(userInput.isLangEn ? "Select Parliament Session" : "সংসদ অধিবেশন বাছাই করুন");
        }
        List<Food_billDTO> billNotPreparedDtoList =
                Food_billDAO.getInstance()
                            .findBillNotPreparedDtoList(
                                    userInput.budgetMappingId,
                                    userInput.economicSubCodeId,
                                    userInput.foodBillSubmissionConfigId
                            );
        if (billNotPreparedDtoList.isEmpty()) {
            throw new CustomException(userInput.isLangEn ? "No valid bill found" : "কোন সঠিক বিল পাওয়া যায়নি");
        }
        userInput.modifierId = userDTO.ID;
        userInput.modificationTime = System.currentTimeMillis();
        userInput.billAmount = 0L;
        userInput.employeeCount = 0;
        userInput.minimumMonthTimeStamp = Long.MAX_VALUE;
        for (Food_billDTO foodBillDTO : billNotPreparedDtoList) {
            userInput.billAmount += foodBillDTO.billAmount;
            userInput.employeeCount += (int) foodBillDTO.employeeCount;
            userInput.minimumMonthTimeStamp = Math.min(
                    userInput.minimumMonthTimeStamp,
                    foodBillDTO.getSortedMonthStream().min(Long::compareTo).orElse(Long.MAX_VALUE)
            );
        }
        Utils.handleTransaction(() -> {
            userInput.sessionNameText = billNotPreparedDtoList.get(0).getSessionAndMonthText(false);
            userInput.billTypeText = billNotPreparedDtoList.get(0).getBillTypeText(false);
            Budget_registerDTO addedBudget_registerDTO = addBudgetRegisterDTO(userInput);
            userInput.budgetRegisterId = addedBudget_registerDTO.iD;
            userInput.billRegisterId = Bill_registerServlet.addBillRegister(addedBudget_registerDTO, CASH_TYPE, true).iD;
            userInput.foodBillFinanceDTO = addFoodBillFinanceDTO(userInput);
            int financeSerialNumber = 1;
            for (Food_billDTO billDTO : billNotPreparedDtoList) {
                billDTO.modifiedBy = userInput.modifierId;
                billDTO.lastModificationTime = userInput.modificationTime;
                billDTO.financeBillStatus = Overtime_billStatus.FINANCE_BILL_PREPARED.getValue();
                billDTO.financeSerialNumber = financeSerialNumber++;
                billDTO.budgetRegisterId = userInput.budgetRegisterId;
                billDTO.billRegisterId = userInput.billRegisterId;
                billDTO.foodBillFinanceId = userInput.foodBillFinanceDTO.iD;
                Food_billDAO.getInstance().update(billDTO);
            }
        });
        return "Food_bill_financeServlet?actionType=view&ID=" + userInput.foodBillFinanceDTO.iD;
    }

    public static final String BILL_DESCRIPTION = "%s-এ কর্মরত %s জন কর্মকর্তা/কর্মচারীর %s এর %s খাবার বিল";

    public static String getBillDescription(UserInput userInput) {
        String budgetOfficeNameBn = Budget_mappingRepository.getInstance().getOperationTextWithoutCode("Bangla", userInput.budgetMappingId);
        return String.format(
                BILL_DESCRIPTION,
                budgetOfficeNameBn,
                StringUtils.convertToBanNumber(String.valueOf(userInput.employeeCount)),
                userInput.sessionNameText,
                userInput.billTypeText
        );
    }

    private Budget_registerDTO addBudgetRegisterDTO(UserInput userInput) throws Exception {
        Budget_registerDTO budgetRegisterDTO = new Budget_registerDTO();

        budgetRegisterDTO.recipientName = FinanceUtil.getFinance1HeadDesignation("bangla");
        budgetRegisterDTO.issueNumber = "";
        budgetRegisterDTO.issueDate = SessionConstants.MIN_DATE;
        budgetRegisterDTO.description = getBillDescription(userInput);
        budgetRegisterDTO.billAmount = userInput.billAmount;

        Long budgetSelectionInfoId =
                BudgetSelectionInfoRepository
                        .getInstance()
                        .getId(BudgetUtils.getEconomicYear(userInput.minimumMonthTimeStamp));
        if (budgetSelectionInfoId != null)
            budgetRegisterDTO.budgetSelectionInfoId = budgetSelectionInfoId;

        Budget_mappingDTO budgetMappingDTO = Budget_mappingRepository.getInstance().getById(userInput.budgetMappingId);
        if (budgetMappingDTO != null) {
            budgetRegisterDTO.budgetMappingId = budgetMappingDTO.iD;
            budgetRegisterDTO.budgetOfficeId = budgetMappingDTO.budgetOfficeId;
        }
        budgetRegisterDTO.economicSubCodeId = userInput.economicSubCodeId;

        budgetRegisterDTO.insertionTime
                = budgetRegisterDTO.lastModificationTime
                = userInput.modificationTime;
        budgetRegisterDTO.insertedBy
                = budgetRegisterDTO.modifiedBy
                = userInput.modifierId;

        Budget_registerDAO.getInstance().add(budgetRegisterDTO);
        return budgetRegisterDTO;
    }


    private void approve(HttpServletRequest request, UserDTO userDTO) throws Exception {
        Long id = Utils.parseOptionalLong(request.getParameter("id"), null, null);
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";

        Food_billDTO foodBillDTO = Food_billDAO.getInstance().getDTOFromID(id);
        // not done approval history editability check, because it is done inside Bill_approval_historyDAO.getInstance().approve()
        foodBillDTO.checkEditabilityOrThrowException(userDTO, language, false);
        List<Bill_approval_historyDTO> nextApprovalHistoryDTOs = approveAndGetNextLayerApprovers(foodBillDTO, userDTO);
        Food_billNotification.getInstance().send(foodBillDTO, nextApprovalHistoryDTOs);
    }

    private List<Bill_approval_historyDTO> approveAndGetNextLayerApprovers(Food_billDTO foodBillDTO, UserDTO userDTO) throws Exception {
        BillApprovalResponse approvalResponse =
                Bill_approval_historyDAO.getInstance()
                                        .approveWitAlreadyAddedApprover(foodBillDTO, userDTO);
        if (!approvalResponse.hasNextApproval) {
            foodBillDTO.approvalStatus = BillApprovalStatus.APPROVED.getValue();
        }
        foodBillDTO.currentApprovalLevel = approvalResponse.nextLevel;
        foodBillDTO.modifiedBy = userDTO.ID;
        foodBillDTO.lastModificationTime = System.currentTimeMillis();
        Food_billDAO.getInstance().update(foodBillDTO);
        return approvalResponse.nextApprovalHistoryDTOs;
    }

    private Map<String, Object> submitFoodBill(HttpServletRequest request, UserDTO userDTO) {
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        boolean isLangEn = language.equalsIgnoreCase("English");
        Long id = Utils.parseOptionalLong(request.getParameter("id"), null, null);
        Map<String, Object> res = new HashMap<>();
        res.put("success", false);
        res.put("message", getServletName() + "?actionType=search");

        if (id == null) {
            res.put("message", isLangEn ? "Invalid id" : "ভুল আইডি");
            return res;
        }
        Food_billDTO foodBillDTO = Food_billDAO.getInstance().getDTOFromID(id);
        if (foodBillDTO == null) {
            res.put("message", (isLangEn ? "No record found with id=" : "কোন তথ্য পাওয়া যায়নি যার আইডি=") + id);
            return res;
        }
        try {
            Utils.handleTransaction(() -> {
                foodBillDTO.checkEditabilityOrThrowException(userDTO, language);
                foodBillDTO.setPreparedInfo(userDTO, language);
                Food_billDAO.getInstance().update(foodBillDTO);
                List<Bill_approval_historyDTO> historyOfCurrentLevel = Bill_approval_historyDAO.getInstance().findByLevel(
                        foodBillDTO.getTableName(), foodBillDTO.iD, foodBillDTO.currentApprovalLevel
                );
                Optional<Bill_approval_historyDTO> usersHistoryOptional = Bill_approval_historyDTO.findUsersHistory(historyOfCurrentLevel, userDTO);
                List<Bill_approval_historyDTO> nextApprovalHistoryDTOs;
                if (usersHistoryOptional.isPresent()) {
                    nextApprovalHistoryDTOs = approveAndGetNextLayerApprovers(foodBillDTO, userDTO);
                } else {
                    nextApprovalHistoryDTOs = historyOfCurrentLevel;
                }
                Food_billNotification.getInstance().send(foodBillDTO, nextApprovalHistoryDTOs);
            });
        } catch (CustomException e) {
            logger.error(e);
            res.put("message", e.getMessage());
            return res;
        } catch (Exception e) {
            logger.error(e);
            res.put("message", isLangEn ? "Failed to submit" : "সাবমিট করতে ব্যর্থ হয়েছে");
            return res;
        }
        res.put("success", true);
        return res;
    }

    @Override
    public String getWhereClause(HttpServletRequest request) {
        return (String) request.getAttribute("Food_billServlet.restrictedViewWhereClause");
    }

    @Override
    public String getSortClause(HttpServletRequest request) {
        return " approval_status ASC, lastModificationTime DESC ";
    }

    private Set<Long> getAllowedOfficeUnitIds(long officeUnitId) {
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitId);
        if (officeUnitsDTO == null) return new HashSet<>();
        if (ONE_LAYER_OFFICE_TYPES.contains(officeUnitsDTO.officeOrder)) {
            return new HashSet<>(Collections.singletonList(officeUnitId));
        }
        return new HashSet<>();
    }

    private Food_allowanceModel getFoodAllowanceModel(AllowanceEmployeeInfoDTO employeeInfoDTO, UserInput userInput) {
        Food_allowanceModel model = new Food_allowanceModel(employeeInfoDTO, userInput.language);
        model.sortedBillDateOtTypeList =
                Stream.generate(OvertimeType.NONE::getIntValue)
                      .limit(userInput.sortedBillDateOtTypeList.size())
                      .collect(Collectors.toList());
        for (int i = 0; i < model.sortedBillDateOtTypeList.size(); ++i) {
            long otBillDate = userInput.sortedBillDateList.get(i);
            if (employeeInfoDTO.isEmployeeActiveInThisTime(otBillDate)) {
                model.sortedBillDateOtTypeList.set(i, userInput.sortedBillDateOtTypeList.get(i));
            }
        }
        model.calculateAndSetDays();
        return model;
    }

    public Food_billModel getFoodBillModelFromDB(Long id, String language) {
        if (id == null) throw new IllegalArgumentException("ID can not be null");

        Food_billDTO foodBillDTO = Food_billDAO.getInstance().getDTOFromID(id);
        if (foodBillDTO == null) throw new NoSuchElementException("No Food_billDTO fond with ID=" + id);

        List<Food_allowanceDTO> allowanceDTOs = Food_allowanceDAO.getInstance().findByFoodBillId(foodBillDTO.iD);
        if (allowanceDTOs == null || allowanceDTOs.isEmpty())
            throw new NoSuchElementException("No Food_allowanceDTO list fond with foodBillId=" + id);
        if (foodBillDTO.isInPreviewStage) {
            allowanceDTOs.sort(Food_allowanceDTO.compareOfficeUnitThenOrganogramOrderValue);
        }
        Food_billModel foodBillModel = new Food_billModel();
        foodBillModel.foodBillDTO = foodBillDTO;
        foodBillModel.allowanceModels =
                allowanceDTOs.stream()
                             .map(allowanceDTO -> new Food_allowanceModel(allowanceDTO, language))
                             .collect(Collectors.toList());
        return foodBillModel;
    }

    private Food_billTableData getFoodBillTableData(UserInput userInput) {
        Food_billTableData foodBillTableData = new Food_billTableData();

        Set<Long> officeUnitIds = getAllowedOfficeUnitIds(userInput.officeUnitsId);
        if (officeUnitIds.isEmpty()) {
            foodBillTableData.errorMessage = userInput.isLangEn ? "This Office is not allowed to submit bill" : "এই কার্যালয়ের বিল জমা দেয়ার অনুমতি নেই";
            return foodBillTableData;
        }
        // IMPORTANT 16-11-2022 Temporary Ban on OT-Food bill Submission for MP Offices
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(userInput.officeUnitsId);
        if (officeUnitsDTO.parentUnitId == 9) {
            foodBillTableData.errorMessage =
                    userInput.isLangEn
                    ? "It is requested not to file through PRP software until further instructions regarding the food allowance of Personal Assistants of Hon'ble Members of Parliament"
                    : "মাননীয় সংসদ সদস্যগণের ব্যক্তিগত সহকারী এর খাবার ভাতা বিল পরবর্তী নির্দেশনা না দেয়া পর্যন্ত পি আর পি সফটওয়্যার এর মাধ্যমে দাখিল না করার জন্য অনুরোধ করা হলো";
            return foodBillTableData;
        }
        Food_bill_submission_configDTO submissionConfigDTO = Food_bill_submission_configDAO.getInstance().getDTOFromID(userInput.foodBillSubmissionConfigId);
        if (submissionConfigDTO == null) {
            foodBillTableData.errorMessage = userInput.isLangEn ? "Select correct bill date" : "সঠিক বিলের তারিখ বাছাই করুন";
            return foodBillTableData;
        }
        List<OT_type_CalendarDTO> kaOtTypeSortedByDate =
                OT_type_CalendarDAO.getInstance()
                                   .findByParliamentInfoSortedByDate(
                                           submissionConfigDTO.electionDetailsId,
                                           submissionConfigDTO.parliamentSessionId
                                   );
        userInput.sortedBillDateList =
                kaOtTypeSortedByDate.stream()
                                    .map(ot_type_calendarDTO -> ot_type_calendarDTO.otDate)
                                    .collect(Collectors.toList());
        if (kaOtTypeSortedByDate.isEmpty()) {
            foodBillTableData.errorMessage = userInput.isLangEn ? "No Food bill days found for input parliament session"
                                                                : "এই সংসদ অধিবেশনের জন্য কোন খাবার বিলের দিন পাওয়া যায়নি";
            return foodBillTableData;
        }


        List<Food_allowanceEmployeeIdModel> addedEmployeeIdModel = Food_allowanceDAO.getInstance().getAddedEmployeeIdModel(
                userInput.officeUnitsId, userInput.foodBillSubmissionConfigId
        );
        Set<Long> excludedEmployeeRecordIds =
                addedEmployeeIdModel
                        .stream()
                        .filter(employeeIdModel -> !employeeIdModel.isInPreviewStage)
                        .map(employeeIdModel -> employeeIdModel.employeeRecordsId)
                        .collect(Collectors.toSet());
        long minDateInclusive = userInput.sortedBillDateList.get(0);
        long maxDateExclusive = userInput.sortedBillDateList.get(userInput.sortedBillDateList.size() - 1) + OT_type_CalendarServlet.ONE_DAY_IN_MILLIS;
        List<AllowanceEmployeeInfoDTO> employeeInfoDTOs =
                AllowanceEmployeeInfoRepository.getInstance().buildDTOsForFoodBill(
                        officeUnitIds,
                        excludedEmployeeRecordIds,
                        minDateInclusive,
                        maxDateExclusive
                );
        userInput.sortedBillDateOtTypeList = OT_type_CalendarServlet.getSortedBillDateOtTypeListForFoodBill(kaOtTypeSortedByDate);
        Map<Long, Food_allowanceEmployeeIdModel> allowanceEmployeeIdModelByEmployeeId =
                addedEmployeeIdModel.stream()
                                    .collect(Collectors.toMap(
                                            employeeIdModel -> employeeIdModel.employeeRecordsId,
                                            Function.identity(),
                                            (e1, e2) -> e1
                                    ));
        foodBillTableData.allowanceModels = new ArrayList<>(
                employeeInfoDTOs
                        .stream()
                        .sequential()
                        .map(employeeInfoDTO -> getFoodAllowanceModel(employeeInfoDTO, userInput))
                        .collect(Collectors.groupingBy(
                                model -> model.employeeRecordsId,
                                LinkedHashMap::new,
                                Collectors.collectingAndThen(
                                        Collectors.toList(),
                                        Food_allowanceModel::mergeSameEmployeeIdModels
                                )
                        )).values()
        );

        Set<Long> addedEmployeeRecordIds = new HashSet<>();
        for (int i = 0; i < foodBillTableData.allowanceModels.size(); ++i) {
            Food_allowanceModel model = foodBillTableData.allowanceModels.get(i);
            model.serialNo = i + 1;
            addedEmployeeRecordIds.add(model.employeeRecordsId);
            model.foodAllowanceId =
                    Optional.ofNullable(allowanceEmployeeIdModelByEmployeeId.get(model.employeeRecordsId))
                            .map(employeeIdModel -> employeeIdModel.foodAllowanceId)
                            .orElse(-1L);
        }
        foodBillTableData.isEditable = true;
        foodBillTableData.sortedBillDates = userInput.sortedBillDateList;
        foodBillTableData.deletedFoodAllowanceIds =
                addedEmployeeIdModel.stream()
                                    .filter(idModel -> idModel.isInPreviewStage)
                                    .filter(idModel -> !addedEmployeeRecordIds.contains(idModel.employeeRecordsId))
                                    .map(idModel -> idModel.foodAllowanceId)
                                    .collect(Collectors.toList());
        return foodBillTableData;
    }

    private Food_billDTO getNewFoodBillDTO(UserInput userInput) {
        Food_billDTO foodBillDTO = new Food_billDTO();
        foodBillDTO.insertedBy = userInput.modifierId;
        foodBillDTO.insertionTime = userInput.modifiedTime;
        foodBillDTO.officeUnitId = userInput.officeUnitsId;
        foodBillDTO.foodBillSubmissionConfigId = userInput.foodBillSubmissionConfigId;
        foodBillDTO.budgetOfficeId = userInput.budgetOfficeId;
        foodBillDTO.financeBillStatus = PENDING.getValue();
        foodBillDTO.employeeCount = 0L;
        foodBillDTO.billAmount = 0L;
        return foodBillDTO;
    }

    public Food_allowanceDTO getFoodAllowance(UserInput userInput, Food_allowanceDTO alreadyAddedAllowanceDTO) {
        Food_allowanceDTO foodAllowanceDTO = alreadyAddedAllowanceDTO;
        if (foodAllowanceDTO == null) {
            foodAllowanceDTO = new Food_allowanceDTO();
            foodAllowanceDTO.insertedBy = userInput.modifierId;
            foodAllowanceDTO.insertionDate = userInput.modifiedTime;
        }
        foodAllowanceDTO.modifiedBy = userInput.modifierId;
        foodAllowanceDTO.lastModificationTime = userInput.modifiedTime;
        if (userInput.isInPreviewStage) {
            foodAllowanceDTO.officeUnitId = userInput.officeUnitsId;
            foodAllowanceDTO.budgetOfficeId = userInput.budgetOfficeId;
            AllowanceEmployeeInfoDTO employeeInfoDTO =
                    AllowanceEmployeeInfoRepository.getInstance()
                                                   .getByEmployeeRecordId(userInput.employeeRecordsId);
            if (employeeInfoDTO == null) {
                return null;
            }
            foodAllowanceDTO.allowanceEmployeeInfoId = employeeInfoDTO.iD;
            foodAllowanceDTO.officeUnitId = employeeInfoDTO.officeUnitId;
            foodAllowanceDTO.organogramId = employeeInfoDTO.organogramId;
            foodAllowanceDTO.designationPrefix = employeeInfoDTO.getDesignationPrefixWithEmploymentCat();
            foodAllowanceDTO.employeeRecordsId = employeeInfoDTO.employeeRecordId;
            foodAllowanceDTO.dailyRate = userInput.dailyRate;
            foodAllowanceDTO.revenueStampDeduction = userInput.revenueStampDeduction;
        }
        foodAllowanceDTO.serialNo = userInput.foodAllowanceModel.serialNo;
        foodAllowanceDTO.setSortedBillDateOtTypeListFromInts(userInput.foodAllowanceModel.sortedBillDateOtTypeList);
        foodAllowanceDTO.days = userInput.foodAllowanceModel.days;
        foodAllowanceDTO.calculateAndSetTotalAmount();
        return foodAllowanceDTO;
    }

    private UserInput parseUserInput(HttpServletRequest request, UserDTO userDTO) throws Exception {
        UserInput userInput = new UserInput();
        userInput.isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        userInput.language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

        userInput.foodBillSubmissionConfigId = Utils.parseMandatoryLong(
                request.getParameter("foodBillSubmissionConfigId"),
                userInput.isLangEn ? "Select Parliament Session" : "সংসদ অধিবেশন বাছাই করুন"
        );
        userInput.officeUnitsId = Utils.parseMandatoryLong(
                request.getParameter("officeUnitsId"),
                userInput.isLangEn ? "Select Office" : "দপ্তর বাছাই করুন"
        );
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(userInput.officeUnitsId);
        if (officeUnitsDTO == null) {
            throw new Exception(userInput.isLangEn ? "Select Correct Office" : "সঠিক দপ্তর বাছাই করুন");
        }
        if (!isValidOfficeType(officeUnitsDTO.officeOrder)) {
            throw new Exception(
                    userInput.isLangEn
                    ? String.format("%s is not allowed to submit Food Bill", officeUnitsDTO.unitNameEng)
                    : String.format("%s-এর খাবার ভাতার বিল জমা দেবার অনুমতি নেই", officeUnitsDTO.unitNameEng)
            );
        }
        userInput.addEditRequestSource = request.getParameter("source");
        userInput.budgetOfficeId = Budget_officeRepository.getInstance().getBudgetOfficeIdByOfficeUnitId(userInput.officeUnitsId);
        userInput.modifiedTime = System.currentTimeMillis();
        userInput.modifierId = userDTO.ID;
        try {
            userInput.deletedFoodAllowanceIds = Arrays.asList(gson.fromJson(
                    request.getParameter("deletedFoodAllowanceIds"),
                    Long[].class
            ));
            userInput.allowanceModelListFromRequest =
                    Arrays.stream(gson.fromJson(
                                  request.getParameter("foodAllowanceModels"),
                                  Food_allowanceModel[].class
                          ))
                          .filter(allowanceModel -> !userInput.deletedFoodAllowanceIds.contains(allowanceModel.foodAllowanceId))
                          .collect(Collectors.toList());
        } catch (Exception ex) {
            logger.error(ex);
            throw new Exception(userInput.isLangEn ? "Wrong Employee Info" : "কর্মকর্তা/কর্মচারীর তথ্য ভুল দেয়া হয়েছে");
        }
        return userInput;
    }

    private Food_billDTO saveFoodBillDTO(Food_billDTO foodBillDTO, UserInput userInput, UserDTO userDTO) throws Exception {
        if (foodBillDTO == null) {
            throw new CustomException(userInput.isLangEn ? "No record found to edit" : "এডিট করার মত কোন তথ্য পাওয়া যায়নি");
        }
        if (foodBillDTO.isInPreviewStage) {
            foodBillDTO.isInPreviewStage = "preview".equals(userInput.addEditRequestSource);
        }
        userInput.isInPreviewStage = foodBillDTO.isInPreviewStage;
        Optional<Bill_approval_historyDTO> optionalApprovalHistoryOfUser = foodBillDTO.checkEditabilityOrThrowException(userDTO, userInput.language);

        Map<Long, Food_allowanceDTO> allowanceDTOById = new HashMap<>();
        if (foodBillDTO.iD > 0) {
            allowanceDTOById =
                    Food_allowanceDAO.getInstance()
                                     .findByFoodBillId(foodBillDTO.iD)
                                     .stream()
                                     .collect(Collectors.toMap(allowanceDTO -> allowanceDTO.iD, allowanceDTO -> allowanceDTO));
        }
        Map<Long, Food_allowanceDTO> allowanceDTOByEmployeeRecordId =
                allowanceDTOById.values()
                                .stream()
                                .collect(Collectors.toMap(allowanceDTO -> allowanceDTO.employeeRecordsId, allowanceDTO -> allowanceDTO, (e1, e2) -> e1));
        List<Food_allowanceDTO> allowanceDTOs = new ArrayList<>();
        for (Food_allowanceModel foodAllowanceModel : userInput.allowanceModelListFromRequest) {
            userInput.employeeRecordsId = foodAllowanceModel.employeeRecordsId;
            userInput.foodAllowanceModel = foodAllowanceModel;
            userInput.foodAllowanceModel.calculateAndSetDays();
            Food_allowanceDTO alreadyAddedAllowanceDTO;
            if (foodAllowanceModel.foodAllowanceId < 0) {
                alreadyAddedAllowanceDTO = allowanceDTOByEmployeeRecordId.get(userInput.employeeRecordsId);
            } else {
                alreadyAddedAllowanceDTO = allowanceDTOById.get(foodAllowanceModel.foodAllowanceId);
            }
            Food_allowanceDTO allowanceDTO = getFoodAllowance(userInput, alreadyAddedAllowanceDTO);
            if (allowanceDTO != null) {
                allowanceDTOs.add(allowanceDTO);
            }
        }
        if (allowanceDTOs.isEmpty()) {
            throw new Exception(userInput.isLangEn ? "No valid employee data found to add" : "যোগ করার জন্য কোন সঠিক কর্মকর্তা/কর্মচারীর পাওয়া যায়নি");
        }
        foodBillDTO.setSummaryEmployeeInfo(allowanceDTOs);
        Utils.handleTransaction(() -> {
            if (foodBillDTO.iD < 0) {
                Food_billDAO.getInstance().add(foodBillDTO);
            } else {
                Food_billDAO.getInstance().update(foodBillDTO);
            }
            Food_billDAO.getInstance().insertApproverIntoBillHistory(foodBillDTO);
            foodBillDTO.billAmount = 0L;
            foodBillDTO.employeeCount = 0L;
            // not done with lambda to throw exception above
            for (Food_allowanceDTO allowanceDTO : allowanceDTOs) {
                allowanceDTO.foodBillId = foodBillDTO.iD;
                saveAllowanceAndUpdateBillDTO(allowanceDTO, userInput, foodBillDTO);
            }
            boolean hasDeletedAllowance = userInput.deletedFoodAllowanceIds != null
                                          && !userInput.deletedFoodAllowanceIds.isEmpty();
            if (hasDeletedAllowance) {
                Food_allowanceDAO.getInstance().delete(
                        userInput.modifierId,
                        userInput.deletedFoodAllowanceIds,
                        userInput.modifiedTime
                );
            }
            Food_billDAO.getInstance().update(foodBillDTO);
            optionalApprovalHistoryOfUser.ifPresent(
                    approvalHistoryDTO ->
                            Food_billNotification
                                    .getInstance()
                                    .sendModificationNotification(foodBillDTO, approvalHistoryDTO)
            );
        });
        return foodBillDTO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        UserInput userInput = parseUserInput(request, userDTO);
        Food_billDTO foodBillDTO;
        if (addFlag) {
            Food_bill_submission_configDTO submissionConfigDTO = Food_bill_submission_configDAO.getInstance().getDTOFromID(userInput.foodBillSubmissionConfigId);
            if (submissionConfigDTO == null) {
                throw new CustomException(
                        userInput.isLangEn ? "Food Allowance Economic Code configuration missing"
                                           : "খাবার ভাতার অর্থনৈতিক কোডের কনফিগারেশন পাওয়া যায়নি"
                );
            }
            boolean isAllowedToPrepareBill = submissionConfigDTO.isAllowedToSubmitByTime(System.currentTimeMillis());
            if (!isAllowedToPrepareBill) {
                throw new CustomException(userInput.isLangEn ? "Not Allowed to prepare this bill date now" : "এখন এই বিল প্রস্তুত করার অনুমতি নেই");
            }
            foodBillDTO = Food_billDAO.getInstance().findBillDtoForOffice(
                    userInput.foodBillSubmissionConfigId, userInput.officeUnitsId, true
            );
            if (foodBillDTO == null) {
                foodBillDTO = getNewFoodBillDTO(userInput);
                userInput.allowanceModelListFromRequest =
                        userInput.allowanceModelListFromRequest
                                .stream()
                                .filter(allowanceModel -> allowanceModel.foodAllowanceId < 0)
                                .collect(Collectors.toList());
            }
            if(!submissionConfigDTO.isOfficeUnitIdAllowed(foodBillDTO.officeUnitId)) {
                throw new CustomException(
                        userInput.isLangEn ? "Office not allowed to submit bill"
                                           : "অফিসের বিল জমা দেওয়ার অনুমতি নেই"
                );
            }
            foodBillDTO.setDataFromSubmissionConfig(submissionConfigDTO);

            foodBillDTO.sortedBillDateList = OT_type_CalendarDAO.getInstance().getSortedBillDatedByParliamentInfo(
                    foodBillDTO.electionDetailsId, foodBillDTO.parliamentSessionId
            );
            Allowance_configureDTO allowanceConfigureDTO =
                    Allowance_configureDAO.getInstance().getByAllowanceCatEnum(AllowanceCatEnum.FOOD_ALLOWANCE);
            if (allowanceConfigureDTO == null) {
                throw new Exception(
                        userInput.isLangEn ? "Food bill amount is not configured"
                                           : "খাবার বিলের পরিমান নির্ধারিত হয়নি"
                );
            }
            foodBillDTO.ordinanceText = allowanceConfigureDTO.ordinanceText;
            foodBillDTO.dailyRate = allowanceConfigureDTO.allowanceAmount;
            foodBillDTO.revenueStampDeduction = allowanceConfigureDTO.taxDeduction;
            foodBillDTO.budgetMappingId = Food_bill_type_configRepository.FOOD_BILL_BUDGET_MAPPING_ID;
            foodBillDTO.economicSubCodeId = Food_bill_type_configRepository.FOOD_BILL_SUB_CODE_ID;
            userInput.dailyRate = foodBillDTO.dailyRate;
            userInput.revenueStampDeduction = foodBillDTO.revenueStampDeduction;

            Food_bill_type_configDTO foodBillTypeConfigDTO =
                    Food_bill_type_configRepository
                            .getInstance()
                            .findByBudgetOfficeOfficeUnit(userInput.budgetOfficeId, userInput.officeUnitsId);
            if (foodBillTypeConfigDTO == null) {
                throw new Exception(
                        userInput.isLangEn ? "Food Allowance approval configuration missing"
                                           : "খাবার ভাতার অনুমোদন কনফিগারেশন পাওয়া যায়নি"
                );
            }
            foodBillDTO.taskTypeId = foodBillTypeConfigDTO.taskTypeId;
        } else {
            userInput.foodBillId = Utils.parseMandatoryLong(
                    request.getParameter("id"),
                    userInput.isLangEn ? "Invalid id to edit" : "এডিট করার জন্য ভুল আইডি দেয়া হয়েছে"
            );
            foodBillDTO = Food_billDAO.getInstance().getDTOFromID(userInput.foodBillId);
            userInput.dailyRate = foodBillDTO.dailyRate;
            userInput.revenueStampDeduction = foodBillDTO.revenueStampDeduction;
            userInput.allowanceModelListFromRequest =
                    userInput.allowanceModelListFromRequest
                            .stream()
                            .filter(allowanceModel -> allowanceModel.foodAllowanceId > 0)
                            .collect(Collectors.toList());
        }
        return saveFoodBillDTO(foodBillDTO, userInput, userDTO);
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        boolean isSourcePreview = "preview".equals(request.getParameter("source"));
        return getServletName()
               + (isSourcePreview ? "?actionType=view&ID=" + commonDTO.iD : "?actionType=search");
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAjaxAddRedirectURL(request, commonDTO);
    }

    private void saveAllowanceAndUpdateBillDTO(Food_allowanceDTO allowanceDTO,
                                               UserInput userInput,
                                               Food_billDTO outFoodBillDTO) throws Exception {
        if (allowanceDTO.iD < 0) {
            Food_allowanceDAO.getInstance().add(allowanceDTO);
        } else {
            Food_allowanceDAO.getInstance().update(allowanceDTO);
        }
        outFoodBillDTO.billAmount += allowanceDTO.totalAmount;
        ++outFoodBillDTO.employeeCount;
        outFoodBillDTO.lastModificationTime = userInput.modifiedTime;
        outFoodBillDTO.modifiedBy = userInput.modifierId;
    }


    public static boolean isAllowedToSeeAllOfficeBill(UserDTO userDTO) {
        if (userDTO == null) return false;
        return userDTO.roleID == RoleEnum.ADMIN.getRoleId()
               || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId()
               || userDTO.roleID == RoleEnum.DATA_ENTRY.getRoleId();
    }

    public static boolean isValidOfficeType(int officeOrder) {
        return ONE_LAYER_OFFICE_TYPES.contains(officeOrder);
    }

    public static Office_unitsDTO getOfficeUnitFromUser(UserDTO userDTO) {
        long organogramId = userDTO.organogramID;
        OfficeUnitOrganograms organogramDTO = OfficeUnitOrganogramsRepository.getInstance().getById(organogramId);
        if (organogramDTO == null) return null;
        Office_unitsDTO current = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(organogramDTO.office_unit_id);
        while (current != null) {
            if (isValidOfficeType(current.officeOrder)) {
                return current;
            }
            current = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(current.parentUnitId);
        }
        return null;
    }

    @Override
    public String getTableName() {
        return Food_billDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Food_billServlet";
    }

    @Override
    public Food_billDAO getCommonDAOService() {
        return Food_billDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.FOOD_BILL_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.FOOD_BILL_ADD};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.FOOD_BILL_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Food_billServlet.class;
    }
}
