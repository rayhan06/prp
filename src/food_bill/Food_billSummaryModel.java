package food_bill;

import bangladehi_number_format_util.BangladeshiNumberFormatter;
import office_units.Office_unitsRepository;
import util.StringUtils;

public class Food_billSummaryModel {
    public int financeSerialNumber;
    public String officeName;
    public String billAmount;
    public String employeeCount;
    public String sessionAndMonths;
    public long billAmountLong;
    public long employeeCountLong;
    public long foodBillId;

    public Food_billSummaryModel(Food_billDTO foodBillDTO, String language) {
        officeName = Office_unitsRepository.getInstance().geText(language, foodBillDTO.officeUnitId);
        employeeCountLong = foodBillDTO.employeeCount;
        employeeCount = StringUtils.convertBanglaIfLanguageIsBangla(language, String.format("%d", employeeCountLong));
        sessionAndMonths = foodBillDTO.getSessionAndMonthText("english".equalsIgnoreCase(language));
        foodBillId = foodBillDTO.iD;
        billAmountLong = foodBillDTO.billAmount;
        billAmount = BangladeshiNumberFormatter.getFormattedNumber(
                StringUtils.convertBanglaIfLanguageIsBangla(
                        language,
                        String.format("%d", foodBillDTO.billAmount)
                )
        );
        financeSerialNumber = foodBillDTO.financeSerialNumber;
    }
}
