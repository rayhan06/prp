package food_bill;

import bill_approval_history.BillApprovalStatus;
import bill_approval_history.Bill_approval_historyDAO;
import bill_approval_history.Bill_approval_historyDTO;
import bill_approval_mapping.Bill_approval_mappingDAO;
import bill_approval_mapping.Bill_approval_mappingDTO;
import bill_register.Bill_registerDAO;
import budget_register.Budget_registerDAO;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import designations.DesignationsEnum;
import food_allowance.Food_allowanceDAO;
import food_allowance.Food_allowanceDTO;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import overtime_bill.BillStatusResponse;
import overtime_bill.BillStatusView;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;
import static overtime_bill.Overtime_billStatus.PENDING;

@SuppressWarnings({"Duplicates"})
public class Food_billDAO implements CommonDAOService<Food_billDTO> {
    private static final Logger logger = Logger.getLogger(Food_billDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (food_bill_submission_config_id,food_bill_type_cat,election_details_id,parliament_session_id,is_in_preview_stage,office_unit_id,budget_office_id,budget_mapping_id,economic_sub_code_id,"
                    .concat("prepared_org_id,prepared_name_bn,prepared_org_nameBn,prepared_office_nameBn,prepared_signature,prepared_time,")
                    .concat("current_approval_level,approval_status,task_type_id,finance_bill_status,finance_serial_number,budget_register_id,bill_register_id,food_bill_finance_id,ordinance_text,daily_rate,revenue_stamp_deduction,employee_count,bill_amount,sorted_bill_dates,modified_by,lastModificationTime,")
                    .concat("inserted_by,insertion_time,isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET food_bill_submission_config_id=?,food_bill_type_cat=?,election_details_id=?,parliament_session_id=?,is_in_preview_stage=?,office_unit_id=?,budget_office_id=?,budget_mapping_id=?,economic_sub_code_id=?,"
                    .concat("prepared_org_id=?,prepared_name_bn=?,prepared_org_nameBn=?,prepared_office_nameBn=?,prepared_signature=?,prepared_time=?,")
                    .concat("current_approval_level=?,approval_status=?,task_type_id=?,finance_bill_status=?,finance_serial_number=?,budget_register_id=?,bill_register_id=?,food_bill_finance_id=?,ordinance_text=?,daily_rate=?,revenue_stamp_deduction=?,employee_count=?,bill_amount=?,sorted_bill_dates=?,modified_by=?,lastModificationTime=? ")
                    .concat("WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    public static final String restrictedViewWhereClause =
            " AND (office_unit_id=%d OR food_bill.id IN (SELECT approvable_dto_id FROM bill_approval_history "
                    .concat("WHERE table_name='food_bill' AND organogram_id=%d AND isDeleted=0)) ");

    private Food_billDAO() {
        searchMap.put("electionDetailsId", " AND (election_details_id = ?) ");
        searchMap.put("parliamentSessionId", " AND (parliament_session_id = ?) ");
        searchMap.put("office_unit_id", " AND (office_unit_id = ?) ");
        searchMap.put("foodBillTypeCat", " AND (food_bill_type_cat = ?) ");
        searchMap.put("isInPreview", " AND (is_in_preview_stage = ?) ");
    }

    private static class LazyLoader {
        static final Food_billDAO INSTANCE = new Food_billDAO();
    }

    public static Food_billDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Food_billDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.foodBillSubmissionConfigId);
        ps.setInt(++index, dto.foodBillTypeCat);
        ps.setLong(++index, dto.electionDetailsId);
        ps.setLong(++index, dto.parliamentSessionId);
        ps.setBoolean(++index, dto.isInPreviewStage);
        ps.setLong(++index, dto.officeUnitId);
        ps.setLong(++index, dto.budgetOfficeId);
        ps.setLong(++index, dto.budgetMappingId);
        ps.setLong(++index, dto.economicSubCodeId);
        ps.setLong(++index, dto.preparedOrgId);
        ps.setString(++index, dto.preparedNameBn);
        ps.setString(++index, dto.preparedOrgNameBn);
        ps.setString(++index, dto.preparedOfficeNameBn);
        ps.setBytes(++index, dto.preparedSignature);
        ps.setLong(++index, dto.preparedTime);
        ps.setInt(++index, dto.currentApprovalLevel);
        ps.setInt(++index, dto.approvalStatus);
        ps.setLong(++index, dto.taskTypeId);
        ps.setInt(++index, dto.financeBillStatus);
        ps.setInt(++index, dto.financeSerialNumber);
        ps.setLong(++index, dto.budgetRegisterId);
        ps.setLong(++index, dto.billRegisterId);
        ps.setLong(++index, dto.foodBillFinanceId);
        ps.setString(++index, dto.ordinanceText);
        ps.setLong(++index, dto.dailyRate);
        ps.setInt(++index, dto.revenueStampDeduction);
        ps.setLong(++index, dto.employeeCount);
        ps.setLong(++index, dto.billAmount);
        ps.setString(++index, dto.getSortedBillDateListString());
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Food_billDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Food_billDTO dto = new Food_billDTO();
            dto.iD = rs.getLong("ID");
            dto.foodBillSubmissionConfigId = rs.getLong("food_bill_submission_config_id");
            dto.foodBillTypeCat = rs.getInt("food_bill_type_cat");
            dto.electionDetailsId = rs.getLong("election_details_id");
            dto.parliamentSessionId = rs.getLong("parliament_session_id");
            dto.isInPreviewStage = rs.getBoolean("is_in_preview_stage");
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.budgetOfficeId = rs.getLong("budget_office_id");
            dto.budgetMappingId = rs.getLong("budget_mapping_id");
            dto.economicSubCodeId = rs.getLong("economic_sub_code_id");
            dto.preparedOrgId = rs.getLong("prepared_org_id");
            dto.preparedNameBn = rs.getString("prepared_name_bn");
            dto.preparedOrgNameBn = rs.getString("prepared_org_nameBn");
            dto.preparedOfficeNameBn = rs.getString("prepared_office_nameBn");
            dto.preparedSignature = rs.getBytes("prepared_signature");
            dto.preparedTime = rs.getLong("prepared_time");
            dto.currentApprovalLevel = rs.getInt("current_approval_level");
            dto.approvalStatus = rs.getInt("approval_status");
            dto.taskTypeId = rs.getLong("task_type_id");
            dto.financeBillStatus = rs.getInt("finance_bill_status");
            dto.financeSerialNumber = rs.getInt("finance_serial_number");
            dto.budgetRegisterId = rs.getLong("budget_register_id");
            dto.billRegisterId = rs.getLong("bill_register_id");
            dto.foodBillFinanceId = rs.getLong("food_bill_finance_id");
            dto.ordinanceText = rs.getString("ordinance_text");
            dto.dailyRate = rs.getLong("daily_rate");
            dto.revenueStampDeduction = rs.getInt("revenue_stamp_deduction");
            dto.employeeCount = rs.getLong("employee_count");
            dto.billAmount = rs.getLong("bill_amount");
            dto.setSortedBillDateListFromString(rs.getString("sorted_bill_dates"));
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "food_bill";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Food_billDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Food_billDTO) commonDTO, updateSqlQuery, false);
    }

    private static final String findBillDtoForOfficeSql =
            "select * " +
            "from food_bill " +
            "where food_bill_submission_config_id = %d " +
            "  and office_unit_id = %d " +
            "  and is_in_preview_stage = %d and isDeleted = 0";

    public Food_billDTO findBillDtoForOffice(long foodBillSubmissionConfigId, long officeUnitsId, boolean isInPreview) {
        int isInPreviewStage = isInPreview ? 1 : 0;
        return ConnectionAndStatementUtil.getT(
                String.format(findBillDtoForOfficeSql, foodBillSubmissionConfigId, officeUnitsId, isInPreviewStage),
                this::buildObjectFromResultSet
        );
    }

    private static final String getByBudgetCodeAndSubmissionConfigIdSql =
            "SELECT * FROM food_bill WHERE budget_mapping_id=%d AND economic_sub_code_id=%d AND food_bill_submission_config_id=%d AND isDeleted=0";

    public List<Food_billDTO> getByBudgetCodeAndSubmissionConfigId(long budgetMappingId, long economicSubCodeId, long foodBillSubmissionConfigId) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(getByBudgetCodeAndSubmissionConfigIdSql, budgetMappingId, economicSubCodeId, foodBillSubmissionConfigId),
                this::buildObjectFromResultSet
        );
    }

    private static final String getByParliamentInfoAndBudgetOfficeIdSql =
            "SELECT * FROM food_bill WHERE budget_office_id=%d AND food_bill_submission_config_id=%d AND isDeleted=0";

    public List<Food_billDTO> getByBudgetOfficeAndConfigId(long budgetOfficeId, long foodBillSubmissionConfigId) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(getByParliamentInfoAndBudgetOfficeIdSql, budgetOfficeId, foodBillSubmissionConfigId),
                this::buildObjectFromResultSet
        );
    }

    public List<Food_billDTO> getOfficeApprovedBillDTOs(long budgetOfficeId, long foodBillSubmissionConfigId) {
        return getByBudgetOfficeAndConfigId(budgetOfficeId, foodBillSubmissionConfigId)
                .stream()
                .filter(Food_billDTO::isApproved)
                .sorted(Food_billDTO.compareOfficeUnitId)
                .collect(Collectors.toList());
    }

    public List<Food_billDTO> getApprovedBillDTOsByBudgetAndSubmissionId(long budgetMappingId, long economicSubCodeId, long foodBillSubmissionConfigId) {
        return getByBudgetCodeAndSubmissionConfigId(budgetMappingId, economicSubCodeId, foodBillSubmissionConfigId)
                .stream()
                .filter(Food_billDTO::isApproved)
                .collect(Collectors.toList());
    }

    public List<Food_billDTO> findBillNotPreparedDtoList(List<Food_billDTO> foodBillDTOList) {
        if (foodBillDTOList == null) {
            return Collections.emptyList();
        }
        return foodBillDTOList
                .stream()
                .filter(overtimeBillDTO -> !overtimeBillDTO.hasFinancePreparedBill())
                .sorted(Food_billDTO.compareOfficeUnitId)
                .collect(toList());
    }

    public List<Food_billDTO> findBillNotPreparedDtoList(long budgetMappingId, long economicSubCodeId, long otBillSubmissionConfigId) {
        return findBillNotPreparedDtoList(
                getApprovedBillDTOsByBudgetAndSubmissionId(
                        budgetMappingId, economicSubCodeId, otBillSubmissionConfigId
                )
        );
    }

    @SuppressWarnings("SameParameterValue")
    private boolean isApproverOfDesignation(List<Bill_approval_historyDTO> approvalHistoryDTOs, DesignationsEnum designationsEnum) {
        return approvalHistoryDTOs
                .stream()
                .map(approvalHistoryDTO -> approvalHistoryDTO.organogramId)
                .map(organogramId -> OfficeUnitOrganogramsRepository.getInstance().getById(organogramId))
                .filter(Objects::nonNull)
                .allMatch(officeUnitOrganograms -> officeUnitOrganograms.isDesignation(designationsEnum));
    }

    public void insertApproverIntoBillHistory(Food_billDTO foodBillDTO) throws Exception {
        if (!foodBillDTO.isInPreviewStage) {
            return;
        }
        Bill_approval_historyDAO approvalHistoryDAO = Bill_approval_historyDAO.getInstance();
        approvalHistoryDAO.deleteAllLevelHistory(
                foodBillDTO, foodBillDTO.modifiedBy, foodBillDTO.lastModificationTime
        );
        List<Bill_approval_mappingDTO> billApprovalMappings =
                Bill_approval_mappingDAO.getInstance().findAllLevelsByTaskType(foodBillDTO.taskTypeId);

        List<Bill_approval_historyDTO> approvalHistoryDTOs = approvalHistoryDAO.getDTOsFromMappingDTOs(billApprovalMappings, foodBillDTO);

        Office_unitsDTO billOfficeUnitDto = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(foodBillDTO.officeUnitId);
        // শাখার নিচে উপসচিব হলে সরাসরি অর্থ শাখায় যাবে
        if (billOfficeUnitDto.hasBranchInAncestorNotSelf()) {
            approvalHistoryDTOs = approvalHistoryDAO.getDTOsWithCircuitBreaker(
                    approvalHistoryDTOs,
                    approvalHistoryDTOsOfALevel -> isApproverOfDesignation(approvalHistoryDTOsOfALevel, DesignationsEnum.DEPUTY_SECRETARY),
                    approvalHistoryDTOsOfALevel -> false // don't take next levels after circuit breaker
            );
        }
        approvalHistoryDAO.addAll(approvalHistoryDTOs);

        foodBillDTO.currentApprovalLevel = 0;
        foodBillDTO.approvalStatus = BillApprovalStatus.PENDING.getValue();
    }

    public boolean deleteBillById(long id, long modifiedBy, long modificationTime) {
        boolean isDeleted = delete(modifiedBy, id, modificationTime);
        if (!isDeleted) {
            return false;
        }
        Food_allowanceDAO.getInstance().deleteByFoodBillId(id, modifiedBy, modificationTime);
        return true;
    }

    private static class IdFromFoodBill {
        long budgetRegisterId;
        long billRegisterId;

        static IdFromFoodBill buildFromResultSet(ResultSet rs) {
            try {
                IdFromFoodBill idFromFoodBill = new IdFromFoodBill();
                idFromFoodBill.budgetRegisterId = rs.getLong("budget_register_id");
                idFromFoodBill.billRegisterId = rs.getLong("bill_register_id");
                return idFromFoodBill;
            } catch (Exception e) {
                logger.error(e);
            }
            return null;
        }
    }

    private static final String getIdFromFoodBillByFinanceIdSql =
            "select budget_register_id, " +
            "       bill_register_id " +
            "from food_bill " +
            "where food_bill_finance_id = %d " +
            "  and isDeleted = 0";

    private List<IdFromFoodBill> getIdFromFoodBillByFinanceId(long foodBillFinanceId) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(getIdFromFoodBillByFinanceIdSql, foodBillFinanceId),
                IdFromFoodBill::buildFromResultSet
        );
    }

    private static final String cascadeFinanceBillDeletedSql =
            "update food_bill " +
            "set modified_by = %d," +
            "    lastModificationTime = %d," +
            "    finance_bill_status = %d," +
            "    finance_serial_number = -1," +
            "    budget_register_id = -1," +
            "    bill_register_id = -1," +
            "    food_bill_finance_id = -1 " +
            "where food_bill_finance_id = %d";

    public void cascadeFinanceBillDeleted(long foodBillFinanceId, long modifiedBy, long modificationTime) {
        String sql = String.format(
                cascadeFinanceBillDeletedSql,
                modifiedBy, modificationTime,
                PENDING.getValue(),
                foodBillFinanceId
        );
        logger.debug(sql);
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            try {
                model.getStatement().executeUpdate(sql);
                recordUpdateTime(model.getConnection(), getTableName(), modificationTime);
                return true;
            } catch (SQLException ex) {
                ex.printStackTrace();
                return false;
            }
        });
    }

    public void financeBillDeleted(long foodBillFinanceId, long modifiedBy, long modificationTime) {
        List<IdFromFoodBill> idFromFoodBillByFinanceId = getIdFromFoodBillByFinanceId(foodBillFinanceId);

        cascadeFinanceBillDeleted(foodBillFinanceId, modifiedBy, modificationTime);

        Budget_registerDAO.getInstance().delete(
                modifiedBy,
                idFromFoodBillByFinanceId
                        .stream()
                        .map(idFromFoodBill -> idFromFoodBill.budgetRegisterId)
                        .collect(toList()),
                modificationTime
        );

        Bill_registerDAO.getInstance().delete(
                modifiedBy,
                idFromFoodBillByFinanceId
                        .stream()
                        .map(idFromFoodBill -> idFromFoodBill.billRegisterId)
                        .collect(toList()),
                modificationTime
        );
    }

    private static final String findByFoodBillFinanceIdSql = "SELECT * FROM food_bill WHERE food_bill_finance_id=%d AND isDeleted=0";

    public List<Food_billDTO> findByFoodBillFinanceId(long foodBillFinanceId) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(findByFoodBillFinanceIdSql, foodBillFinanceId),
                this::buildObjectFromResultSet
        );
    }

    public BillStatusResponse getStatusForEmployeeDashboard(long foodBillSubmissionConfigId, long officeUnitsId, long employeeRecordId, boolean isLangEn) {
        BillStatusResponse response = new BillStatusResponse();
        Food_billDTO billDto = findBillDtoForOffice(foodBillSubmissionConfigId, officeUnitsId, false);
        if (billDto == null) {
            response.setStatusAndItsMessage(BillStatusView.NOT_SUBMITTED, isLangEn);
            return response;
        }
        Food_allowanceDTO employeeAllowanceId =
                Food_allowanceDAO.getInstance().findByFoodBillIdAndEmployeeId(billDto.iD, employeeRecordId);
        if (employeeAllowanceId == null) {
            response.setStatusAndItsMessage(BillStatusView.SUBMITTED_WITHOUT_EMPLOYEE, isLangEn);
            return response;
        }
        if (billDto.hasFinancePreparedBill()) {
            response.setStatusAndItsMessage(BillStatusView.FINANCE_PREPARED, isLangEn);
            return response;
        }
        if (billDto.isApproved()) {
            response.setStatusAndItsMessage(BillStatusView.SENT_TO_FINANCE, isLangEn);
            return response;
        }
        response.setStatusAndItsMessage(BillStatusView.WAITING_FOR_APPROVAL, isLangEn);
        response.message =
                Bill_approval_historyDAO
                        .getInstance()
                        .findByLevel(billDto.getTableName(), billDto.getId(), billDto.getLevel())
                        .stream()
                        .findFirst()
                        .map(dto -> isLangEn ? " at " + dto.officeNameEn : dto.officeNameBn + "-এ ")
                        .map(officeName -> String.format(response.message, officeName))
                        .orElse(response.message.replaceAll(Pattern.quote("%s"), ""));
        return response;
    }
}
