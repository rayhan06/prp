package food_bill;

import food_allowance.Food_allowanceModel;

import java.util.List;

public class Food_billModel {
    public Food_billDTO foodBillDTO;
    public List<Food_allowanceModel> allowanceModels;
}
