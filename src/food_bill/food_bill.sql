create table food_bill
(
    ID                             bigint primary key,
    food_bill_submission_config_id bigint,
    election_details_id            bigint,
    parliament_session_id          bigint,
    is_in_preview_stage            tinyint,
    office_unit_id                 bigint,
    budget_office_id               bigint,
    finance_bill_status            int,
    budget_register_id             bigint,
    bill_register_id               bigint,
    ordinance_text                 varchar(4096),
    daily_rate                     bigint,
    employee_count                 bigint,
    bill_amount                    bigint,
    sorted_bill_dates              varchar(4096),
    prepared_org_id                bigint        default -1,
    prepared_name_bn               varchar(1024) default '',
    prepared_org_nameBn            varchar(1024) default '',
    prepared_office_nameBn         varchar(1024) default '',
    prepared_signature             longblob,
    prepared_time                  bigint        default -62135791200000,
    current_approval_level         int,
    approval_status                int,
    task_type_id                   bigint,

    modified_by                    bigint,
    lastModificationTime           bigint        default -62135791200000,
    inserted_by                    bigint,
    insertion_time                 bigint        default -62135791200000,
    isDeleted                      int
)
    engine = MyISAM;

insert into vb_sequencer (table_name, next_id, table_LastModificationTime) value ('food_bill', 1, 0);


