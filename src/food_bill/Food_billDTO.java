package food_bill;

import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import bill_approval_history.ApprovableDTO;
import bill_approval_history.BillApprovalStatus;
import bill_approval_history.Bill_approval_historyDAO;
import bill_approval_history.Bill_approval_historyDTO;
import com.google.gson.Gson;
import common.CustomException;
import election_details.Election_detailsRepository;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import food_allowance.Food_allowanceDTO;
import food_bill_submission_config.Food_bill_submission_configDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import parliament_session.Parliament_sessionRepository;
import pb.CatRepository;
import pbReport.DateUtils;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static overtime_bill.Overtime_billStatus.FINANCE_BILL_PREPARED;
import static overtime_bill.Overtime_billStatus.PENDING;

@SuppressWarnings({"Duplicates"})
public class Food_billDTO extends CommonDTO implements ApprovableDTO {
    private static final Gson gson = new Gson();

    public long foodBillSubmissionConfigId = -1;
    public int foodBillTypeCat = 1;
    public long electionDetailsId = -1;
    public long parliamentSessionId = -1;
    public long officeUnitId = -1;
    public long budgetOfficeId = -1;
    public long budgetMappingId = -1;
    public long economicSubCodeId = -1;
    public boolean isInPreviewStage = true;
    public long preparedOrgId;
    public String preparedNameBn;
    public String preparedOrgNameBn;
    public String preparedOfficeNameBn;
    public byte[] preparedSignature;
    public long preparedTime;
    public int financeBillStatus = PENDING.getValue();
    public int financeSerialNumber = -1;
    public long budgetRegisterId = -1;
    public long billRegisterId = -1;
    public long foodBillFinanceId = -1;
    public String ordinanceText = "";
    public long dailyRate = 0;
    public int revenueStampDeduction = 0;
    public long employeeCount = 0;
    public long billAmount = 0;
    public List<Long> sortedBillDateList = new ArrayList<>();
    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = -1;
    public int currentApprovalLevel;
    public int approvalStatus;
    public Long taskTypeId;

    // not yet added in db
    public String summaryEmployeeNameBn = "";
    public String summaryEmployeeDesignationBn = "";

    public static Comparator<Food_billDTO> compareOfficeUnitId = Comparator.comparing(dto -> dto.officeUnitId);

    public String getSortedBillDateListString() {
        return gson.toJson(sortedBillDateList);
    }

    public void setSortedBillDateListFromString(String sortedBillDateListString) {
        sortedBillDateList = Arrays.asList(
                gson.fromJson(sortedBillDateListString, Long[].class)
        );
    }

    public Stream<Long> getSortedMonthStream() {
        if (sortedBillDateList == null) {
            return Stream.empty();
        }
        return sortedBillDateList
                .stream()
                .map(DateUtils::get1stDayOfMonth)
                .distinct()
                .sorted();
    }

    public String getSessionMonthNames(boolean isLangEn) {
        String language = isLangEn ? "english" : "bangla";
        return getSortedMonthStream()
                .map(monthTimeStamp -> DateUtils.getMonthYear(monthTimeStamp, language, "/"))
                .collect(Collectors.joining(", "));
    }

    public String getBillNameText(boolean isLangEn) {
        String parliamentNo = Election_detailsRepository.getInstance().getText(electionDetailsId, isLangEn);
        String sessionNo = Parliament_sessionRepository.getInstance().getSessionNo(parliamentSessionId, isLangEn);
        return String.format(
                isLangEn ? "%s-th Parliament | %s-th Session (%s)" : "%s-তম সংসদ | %s-তম অধিবেশন (%s)",
                parliamentNo,
                sessionNo,
                CatRepository.getInstance().getText(false, SessionConstants.FOOD_BILL_TYPE_CAT_DOMAIN_NAME, foodBillTypeCat)
        );
    }

    public String getSessionAndMonthText(boolean isLangEn) {
        String sessionNo = Parliament_sessionRepository.getInstance().getSessionNo(parliamentSessionId, isLangEn);
        String monthNames = getSessionMonthNames(isLangEn);
        if (!monthNames.isEmpty()) {
            monthNames = String.format("(%s)", monthNames);
        }
        return String.format(
                isLangEn ? "%sth Session %s" : "%sতম অধিবেশন %s",
                sessionNo,
                monthNames
        );
    }

    public String getBillTypeText(boolean isLangEn) {
        return CatRepository.getInstance().getText(isLangEn, SessionConstants.FOOD_BILL_TYPE_CAT_DOMAIN_NAME, foodBillTypeCat);
    }

    public boolean isApproved() {
        if (isInPreviewStage) {
            return false;
        }
        return approvalStatus == BillApprovalStatus.APPROVED.getValue();
    }

    public boolean hasFinancePreparedBill() {
        return financeBillStatus == FINANCE_BILL_PREPARED.getValue();
    }

    public boolean isEditable(UserDTO userDTO, String language) {
        try {
            checkEditabilityOrThrowException(userDTO, language);
            return true;
        } catch (IllegalAccessException ex) {
            return false;
        }
    }

    public boolean isDeletableByUser(UserDTO userDTO) {
        if (isInPreviewStage) {
            return true;
        }
        return Food_billServlet.isAllowedToSeeAllOfficeBill(userDTO);
    }

    public Optional<Bill_approval_historyDTO> checkEditabilityOrThrowException(UserDTO userDTO, String language, boolean doApprovalHistoryCheck) throws IllegalAccessException {
        boolean isLangEng = "english".equalsIgnoreCase(language);
        if (isApproved()) {
            throw new IllegalAccessException(
                    isLangEng ? "Approval has been completed. Not Editable Now."
                              : "অনুমোদন সম্পন্ন হয়েছে এখন পরিবর্তনযোগ্য নয়"
            );
        }
        if (hasFinancePreparedBill()) {
            throw new IllegalAccessException(
                    isLangEng ? "Finance already prepared bill. Can not edit!"
                              : "অর্থশাখা বিল প্রস্তুত করে ফেলেছে। এখন এডিটযোগ্য নয়!"
            );
        }

        if (isInPreviewStage) {
            return Optional.empty();
        }

        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(userDTO.employee_record_id);
        if (employeeOfficeDTO == null) {
            throw new IllegalAccessException(
                    isLangEng ? "You are not allowed to edit"
                              : "আপনার এডিটের অনুমতি নেই"
            );
        }
        if (doApprovalHistoryCheck) {
            Optional<Bill_approval_historyDTO> optionalApprovalHistory =
                    Bill_approval_historyDAO
                            .getInstance()
                            .findByLevel(getTableName(), iD, currentApprovalLevel)
                            .stream()
                            .filter(approvalHistoryDTO -> approvalHistoryDTO.isThisUsersHistory(userDTO))
                            .findAny();
            if (optionalApprovalHistory.isPresent()) {
                return optionalApprovalHistory;
            }
            throw new IllegalAccessException(isLangEng ? "You are not allowed to approve" : "আপনার অনুমোদন করার অনুমতি নেই");
        }
        return Optional.empty();
    }

    public Optional<Bill_approval_historyDTO> checkEditabilityOrThrowException(UserDTO userDTO, String language) throws IllegalAccessException {
        return checkEditabilityOrThrowException(userDTO, language, true);
    }

    public void setPreparedInfo(UserDTO userDTO, String language) {
        boolean isLangEng = "english".equalsIgnoreCase(language);
        if (isApproved()) {
            throw new CustomException(
                    isLangEng ? "Approval has been completed. Can Not Prepare Bill Now."
                              : "অনুমোদন সম্পন্ন হয়েছে এখন বিল প্রস্তুতযোগ্য নয়"
            );
        }
        if (hasFinancePreparedBill()) {
            throw new CustomException(
                    isLangEng ? "Finance already prepared bill. Can Not Prepare Bill Now!"
                              : "অর্থশাখা বিল প্রস্তুত করে ফেলেছে। এখন বিল প্রস্তুতযোগ্য নয়!"
            );
        }

        if (!isInPreviewStage) {
            throw new CustomException(
                    isLangEng ? "Bill has already been prepared."
                              : "বিল ইতো মধ্যে প্রস্তুত করা হয়ে গেছে!"
            );
        }

        isInPreviewStage = false;
        preparedOrgId = -1;
        preparedNameBn = "(এডমিন)";
        preparedOfficeNameBn = "(এডমিন)";
        preparedOrgNameBn = "(এডমিন)";
        preparedSignature = null;
        preparedTime = lastModificationTime = System.currentTimeMillis();
        modifiedBy = userDTO.ID;

        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(userDTO.employee_record_id);
        if (employeeOfficeDTO == null) return;

        Employee_recordsDTO employeeRecords = Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId);
        if (employeeRecords != null) {
            if (employeeRecords.employeeClass == 4) {
                throw new CustomException(
                        isLangEng ? "4th Class Employee can not submit bill"
                                  : "৪র্থ শ্রেণির কর্মচারী বিল জমা দিতে পারবেন না"
                );
            }
            preparedNameBn = employeeRecords.nameBng;
            preparedSignature = employeeRecords.signature;
            if (preparedSignature == null || preparedSignature.length == 0) {
                throw new CustomException(
                        isLangEng ? "Please upload your signature!"
                                  : "আপনার স্বাক্ষর আপলোড করুন!"
                );
            }
        }
        Office_unitsDTO officeUnits = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
        if (officeUnits != null) {
            preparedOfficeNameBn = officeUnits.unitNameBng;
        }
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        if (officeUnitOrganograms != null) {
            preparedOrgNameBn = officeUnitOrganograms.designation_bng;
            preparedOrgId = officeUnitOrganograms.id;
        }
    }

    public void setDataFromSubmissionConfig(Food_bill_submission_configDTO submissionConfigDTO) {
        foodBillSubmissionConfigId = submissionConfigDTO.iD;
        electionDetailsId = submissionConfigDTO.electionDetailsId;
        parliamentSessionId = submissionConfigDTO.parliamentSessionId;
        foodBillTypeCat = submissionConfigDTO.foodBillTypeCat;
    }

    public void setSummaryEmployeeInfo(List<Food_allowanceDTO> allowanceDTOs) {
        if (allowanceDTOs == null || allowanceDTOs.isEmpty()) {
            return;
        }
        Food_allowanceDTO firstEmployeeData = allowanceDTOs.stream()
                                                           .min(Food_allowanceDTO.orderingComparator)
                                                           .get();
        AllowanceEmployeeInfoDTO allowanceEmployeeInfoDTO =
                AllowanceEmployeeInfoRepository.getInstance().getById(firstEmployeeData.allowanceEmployeeInfoId);
        if (allowanceEmployeeInfoDTO != null) {
            summaryEmployeeNameBn = allowanceEmployeeInfoDTO.nameBn;
            summaryEmployeeDesignationBn = allowanceEmployeeInfoDTO.organogramNameBn;
        }
    }

    @Override
    public String getTableName() {
        return Food_billDAO.getInstance().getTableName();
    }

    @Override
    public Long getId() {
        return iD;
    }

    @Override
    public Integer getLevel() {
        return currentApprovalLevel;
    }

    @Override
    public Long getOfficeUnitId() {
        return officeUnitId;
    }

    @Override
    public Long getTaskTypeId() {
        return taskTypeId;
    }
}
