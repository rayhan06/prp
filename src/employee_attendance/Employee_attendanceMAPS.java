package employee_attendance;
import java.util.*; 
import util.*;


public class Employee_attendanceMAPS extends CommonMaps
{	
	public Employee_attendanceMAPS(String tableName)
	{
		

		java_DTO_map.put("ID".toLowerCase(), "ID".toLowerCase());
		java_DTO_map.put("employeeRecordsType".toLowerCase(), "employeeRecordsType".toLowerCase());
		java_DTO_map.put("attendanceDeviceType".toLowerCase(), "attendanceDeviceType".toLowerCase());
		java_DTO_map.put("employeeAttendanceDeviceType".toLowerCase(), "employeeAttendanceDeviceType".toLowerCase());
		java_DTO_map.put("inTimestamp".toLowerCase(), "inTimestamp".toLowerCase());
		java_DTO_map.put("inTime".toLowerCase(), "inTime".toLowerCase());
		java_DTO_map.put("outTimestamp".toLowerCase(), "outTimestamp".toLowerCase());
		java_DTO_map.put("outTime".toLowerCase(), "outTime".toLowerCase());
		java_DTO_map.put("isAbsent".toLowerCase(), "isAbsent".toLowerCase());
		java_DTO_map.put("isOvertime".toLowerCase(), "isOvertime".toLowerCase());
		java_DTO_map.put("isApproved".toLowerCase(), "isApproved".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("employee_records_type".toLowerCase(), "employeeRecordsType".toLowerCase());
		java_SQL_map.put("attendance_device_type".toLowerCase(), "attendanceDeviceType".toLowerCase());
		java_SQL_map.put("employee_attendance_device_type".toLowerCase(), "employeeAttendanceDeviceType".toLowerCase());
		java_SQL_map.put("in_timestamp".toLowerCase(), "inTimestamp".toLowerCase());
		java_SQL_map.put("out_timestamp".toLowerCase(), "outTimestamp".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "ID".toLowerCase());
		java_Text_map.put("Employee Records".toLowerCase(), "employeeRecordsType".toLowerCase());
		java_Text_map.put("Attendance Device".toLowerCase(), "attendanceDeviceType".toLowerCase());
		java_Text_map.put("Employee Attendance Device".toLowerCase(), "employeeAttendanceDeviceType".toLowerCase());
		java_Text_map.put("In Timestamp".toLowerCase(), "inTimestamp".toLowerCase());
		java_Text_map.put("In Time".toLowerCase(), "inTime".toLowerCase());
		java_Text_map.put("Out Timestamp".toLowerCase(), "outTimestamp".toLowerCase());
		java_Text_map.put("Out Time".toLowerCase(), "outTime".toLowerCase());
		java_Text_map.put("Is Absent".toLowerCase(), "isAbsent".toLowerCase());
		java_Text_map.put("Is Overtime".toLowerCase(), "isOvertime".toLowerCase());
		java_Text_map.put("Is Approved".toLowerCase(), "isApproved".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}