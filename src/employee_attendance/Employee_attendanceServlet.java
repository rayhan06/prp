package employee_attendance;

import java.io.IOException;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import employee_attendance_device.Employee_attendance_deviceDAO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsRepository;
import language.LC;
import language.LM;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import org.apache.log4j.Logger;

import login.LoginDTO;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import test_lib.util.Pair;
import user.UserDTO;
import user.UserRepository;
import util.*;

import java.util.*;
import javax.servlet.http.*;


import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.google.gson.Gson;

import attendance_alert_details.Attendance_alert_detailsDAO;
import pb.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficesDAO;


/**
 * Servlet implementation class Employee_attendanceServlet
 */
@WebServlet("/Employee_attendanceServlet")
@MultipartConfig
public class Employee_attendanceServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Employee_attendanceServlet.class);

    String tableName = "employee_attendance";

    Employee_attendanceDAO employee_attendanceDAO;
    Employee_attendance_deviceDAO employee_attendance_deviceDAO;
    CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Employee_attendanceServlet() {
        super();
        try {
            employee_attendanceDAO = new Employee_attendanceDAO(tableName);
            commonRequestHandler = new CommonRequestHandler(employee_attendanceDAO);
            employee_attendance_deviceDAO = Employee_attendance_deviceDAO.getInstance();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getAddPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ATTENDANCE_ADD)) {
                    commonRequestHandler.getAddPage(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getEditPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ATTENDANCE_UPDATE)) {
                    getEmployee_attendance(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getURL")) {
                String URL = request.getParameter("URL");
                System.out.println("URL = " + URL);
                response.sendRedirect(URL);
            } else if (actionType.equals("search")) {
                System.out.println("search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ATTENDANCE_SEARCH)) {

                    searchEmployee_attendance(request, response, true, "");

                }
            } else if (actionType.equals("view")) {
                System.out.println("view requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ATTENDANCE_SEARCH)) {
                    commonRequestHandler.view(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if(actionType.equals("downloadAttendanceReport")) {
                String fromDate = request.getParameter("fromDate");
                String toDate = request.getParameter("toDate");
                String month = request.getParameter("month");
                String year = request.getParameter("year");
                String employeeRecordsId = request.getParameter("employeeRecordsId");
                String decentEmployeeRecordsId = request.getParameter("decentEmployeeRecordsId");
                String searchReportType = request.getParameter("searchReportType");
                String searchDate = request.getParameter("searchDate");
                String officeUnitsId = request.getParameter("officeUnitsId");
                List<AttendanceReportDetails> report = getAttendanceReport(officeUnitsId, searchReportType, employeeRecordsId, decentEmployeeRecordsId, searchDate, fromDate, toDate, month, year, userDTO);
                //List<AttendanceReportDetails> report = new Employee_attendanceDAO().getAttendanceReportData(fromDate,toDate,month,year,employeeRecordsId);
                if (searchReportType.equals("4"))
                    downloadExcelForEmployeeOvertime(report, request, response, loginDTO);
                else
                    downloadExcelForEmployeeAttendance(report, request, response, loginDTO);

            } else {
                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void downloadExcelForEmployeeOvertime(List<AttendanceReportDetails> report,HttpServletRequest request, HttpServletResponse response,LoginDTO loginDTO) {
        try {
            String file_name = "employees-overtime-report.xlsx";
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename="+file_name);

            XSSFWorkbook workbook    = UtilCharacter.newWorkBook();
            int row_index=0;
            int cell_col_index=0;
            boolean addMerged=true;

            Sheet sheet = UtilCharacter.newSheet(workbook, "overtime_report");
            Row newRow = sheet.createRow(row_index);
            IndexedColors indexedColor = IndexedColors.ROYAL_BLUE;
            boolean isBold=true;

            String language = LM.getLanguageIDByUserDTO(UserRepository.getUserDTOByUserID(loginDTO)) == CommonConstant.Language_ID_English ? "English" : "Bangla";

            UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_EMPLOYEERECORDSTYPE, loginDTO),row_index,row_index,cell_col_index,cell_col_index+2,addMerged,newRow,indexedColor,isBold);
            cell_col_index+=3;
            UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.HM_DESIGNATION, loginDTO),row_index,row_index,cell_col_index,cell_col_index+2,addMerged,newRow,indexedColor,isBold);
            cell_col_index+=3;
            UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_PRESENT, loginDTO),row_index,row_index,cell_col_index,cell_col_index++,false,newRow,indexedColor,isBold);
            cell_col_index+=1;
            UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_ABSENT, loginDTO),row_index,row_index,cell_col_index,cell_col_index++,false,newRow,indexedColor,isBold);


            for(AttendanceReportDetails dto: report) {
                indexedColor = IndexedColors.ROYAL_BLUE;
                row_index++;
                cell_col_index=0;
                newRow = sheet.createRow(row_index);

                String employeeName = Employee_recordsRepository.getInstance().getEmployeeName(dto.employeeRecordsId, language);
                EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(dto.employeeRecordsId);
                OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
                String designation = "English".equalsIgnoreCase(language) ?officeUnitOrganograms.designation_eng:officeUnitOrganograms.designation_bng;
                String presentDays = Utils.getDigits(dto.presentDays, language);
                String absentDays = Utils.getDigits(dto.absentDays, language);
                UtilCharacter.alreadyCreatedRow(workbook,sheet,employeeName,row_index,row_index,cell_col_index,cell_col_index+2,addMerged,newRow,indexedColor,isBold);
                cell_col_index+=3;
                UtilCharacter.alreadyCreatedRow(workbook,sheet,designation,row_index,row_index,cell_col_index,cell_col_index+2,addMerged,newRow,indexedColor,isBold);
                cell_col_index+=3;
                UtilCharacter.alreadyCreatedRow(workbook,sheet,presentDays,row_index,row_index,cell_col_index,cell_col_index++,false,newRow,indexedColor,isBold);
                cell_col_index+=1;
                UtilCharacter.alreadyCreatedRow(workbook,sheet,absentDays,row_index,row_index,cell_col_index,cell_col_index++,false,newRow,indexedColor,isBold);
            }
            workbook.write(response.getOutputStream());
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void downloadExcelForEmployeeAttendance(List<AttendanceReportDetails> report,HttpServletRequest request, HttpServletResponse response,LoginDTO loginDTO) {
        try {
        String file_name = "employees-attendance-report.xlsx";
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename="+file_name);

        XSSFWorkbook workbook    = UtilCharacter.newWorkBook();
        int row_index=0;
        int cell_col_index=0;
        boolean addMerged=true;

        Sheet sheet = UtilCharacter.newSheet(workbook, "attendance_report");
        Row newRow = sheet.createRow(row_index);
        IndexedColors indexedColor = IndexedColors.ROYAL_BLUE;
        boolean isBold=true;

        String language = LM.getLanguageIDByUserDTO(UserRepository.getUserDTOByUserID(loginDTO)) == CommonConstant.Language_ID_English ? "English" : "Bangla";

        UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_EMPLOYEERECORDSTYPE, loginDTO),row_index,row_index,cell_col_index,cell_col_index+2,addMerged,newRow,indexedColor,isBold);
        cell_col_index+=3;
        UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.HM_DATE, loginDTO),row_index,row_index,cell_col_index,cell_col_index+1,addMerged,newRow,indexedColor,isBold);
        cell_col_index+=2;
        UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_INTIME, loginDTO),row_index,row_index,cell_col_index,cell_col_index+1,addMerged,newRow,indexedColor,isBold);
        cell_col_index+=2;
        UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_OUTTIME, loginDTO),row_index,row_index,cell_col_index,cell_col_index++,false,newRow,indexedColor,isBold);


        for(AttendanceReportDetails dto: report) {
            indexedColor = IndexedColors.ROYAL_BLUE;
            row_index++;
            cell_col_index=0;
            newRow = sheet.createRow(row_index);

            String employeeName = Employee_recordsRepository.getInstance().getEmployeeName(dto.employeeRecordsId, language);
            String date = StringUtils.getFormattedDate(language, dto.date);
            String inTimeStr = "";
            if(dto.isHoliday) {
                inTimeStr = dto.holidayNote;
            } else if(dto.isOnLeave) {
                inTimeStr = dto.leaveNote;
            } else if(dto.isAbsent) {
                inTimeStr = language.equals("English") ? "Absent" : "অনুপস্থিত";
            } else {
                inTimeStr = TimeFormat.getInAmPmFormat(dto.inTime);
                inTimeStr = Utils.getDigits(inTimeStr, language);
            }

            String outTime = TimeFormat.getInAmPmFormat(dto.outTime);
            outTime = Utils.getDigits(outTime, language);

            UtilCharacter.alreadyCreatedRow(workbook,sheet,employeeName,row_index,row_index,cell_col_index,cell_col_index+2,addMerged,newRow,indexedColor,isBold);
            cell_col_index+=3;
            UtilCharacter.alreadyCreatedRow(workbook,sheet,date,row_index,row_index,cell_col_index,cell_col_index+1,addMerged,newRow,indexedColor,isBold);
            cell_col_index+=2;
            if(dto.isAbsent) indexedColor = IndexedColors.RED;
            else if(dto.isLate) indexedColor = IndexedColors.ORANGE;
            UtilCharacter.alreadyCreatedRow(workbook,sheet,inTimeStr,row_index,row_index,cell_col_index,cell_col_index+1,addMerged,newRow,indexedColor,isBold);
            cell_col_index+=2;
            if(dto.isAbsent) indexedColor = IndexedColors.RED;
            else if(dto.isEarlyLeave) indexedColor = IndexedColors.ORANGE;
            else indexedColor = IndexedColors.ROYAL_BLUE;
            UtilCharacter.alreadyCreatedRow(workbook,sheet,outTime,row_index,row_index,cell_col_index,cell_col_index++,false,newRow,indexedColor,isBold);
        }
            workbook.write(response.getOutputStream());
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        System.out.println("doPost");
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("add")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ATTENDANCE_ADD)) {
                    System.out.println("going to  addEmployee_attendance ");
                    addEmployee_attendance(request, response, true, userDTO, true);
                } else {
                    System.out.println("Not going to  addEmployee_attendance ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("getDTO")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ATTENDANCE_ADD)) {
                    getDTO(request, response);
                } else {
                    System.out.println("Not going to  addEmployee_attendance ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("edit")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ATTENDANCE_UPDATE)) {
                    addEmployee_attendance(request, response, false, userDTO, isPermanentTable);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("delete")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_UPDATE)) {
                    commonRequestHandler.delete(request, response, userDTO);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            }else if (actionType.equals("approve")) {
              /*  String[] IDsToDelete = request.getParameterValues("ID");
                for (String empid_date: IDsToDelete) {
                    long empid = Long.parseLong(empid_date.split("_")[0]);
                    long date = Long.parseLong(empid_date.split("_")[1]);
                    employee_attendanceDAO.setIsApproved(empid,date);
                }*/
                response.sendRedirect( "Employee_attendanceServlet?actionType=search");
            }  else if(actionType.equals("approveAttendance")) {

                String approvedIds = request.getParameter("approvedIds");
                String comments = request.getParameter("comment");

                try {
                    approvedEmployeeAbsentDays(approvedIds,comments,userDTO.employee_record_id);
                } catch (Exception e) {
                }

                response.sendRedirect( "Employee_attendanceServlet?actionType=search");
            } else if (actionType.equals("search")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ATTENDANCE_SEARCH)) {
                    searchEmployee_attendance(request, response, true, "");
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getGeo")) {
                System.out.println("going to geoloc ");
                request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
            } else if ( actionType.equals("approveLateInEarlyOut")) {

                String approvedIds = request.getParameter("lateApprovedIds");
                String comments = request.getParameter("comment");

                try {
                    String[] ids = approvedIds.split(";");
                    for (String empid_date: ids) {
                        long empid = Long.parseLong(empid_date.split("_")[0]);
                        long date = Long.parseLong(empid_date.split("_")[1]);
                        employee_attendanceDAO.setIsApproved(empid,date,comments,userDTO.employee_record_id);
                    }
                    response.sendRedirect( "Employee_attendanceServlet?actionType=search");
                } catch (Exception e) {
                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }
    private void approvedEmployeeAbsentDays(String approvedIds, String comment,long currentEmployeeRecordsId) throws Exception {
        String[] employeeIds = approvedIds.split(";");
        for(int i=0;i<employeeIds.length;i++) {
            String[] employeeIdMonthYear = employeeIds[i].split("_");
            if(employeeIdMonthYear.length>0) {
                long employeeRecordsId = Long.parseLong(employeeIdMonthYear[0]);
                long fromDate = new Employee_attendanceDAO().constructDateTime(employeeIdMonthYear[1],employeeIdMonthYear[2],true);
                long toDate = new Employee_attendanceDAO().constructDateTime(employeeIdMonthYear[1],employeeIdMonthYear[2],false);
                new Employee_attendanceDAO().approveEachEmployeeAbsentDays(employeeRecordsId, currentEmployeeRecordsId,fromDate,toDate,comment);
            }
        }
        //approveEachEmployeeAbsentDays(Long employeeRecordsId, Long approvedByEmployeeRecordsId, long fromDate, long toDate,String comment)
    }

    private void getDTO(HttpServletRequest request, HttpServletResponse response) {
        try {
            System.out.println("In getDTO");
            Employee_attendanceDTO employee_attendanceDTO = employee_attendanceDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            String encoded = this.gson.toJson(employee_attendanceDTO);
            System.out.println("json encoded data = " + encoded);
            out.print(encoded);
            out.flush();
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void addEmployee_attendance(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addEmployee_attendance");
            String path = getServletContext().getRealPath("/img2/");
            Employee_attendanceDTO employee_attendanceDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

            if (addFlag == true) {
                employee_attendanceDTO = new Employee_attendanceDTO();
            } else {
                employee_attendanceDTO = employee_attendanceDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
            }

            String Value = "";

            Value = request.getParameter("ID");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("ID = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                employee_attendanceDTO.ID = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            long empRetType = 0, atTDivType = 0;

            Value = request.getParameter("employeeRecordsType");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("employeeRecordsType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                empRetType = employee_attendanceDTO.employeeRecordsType = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("attendanceDeviceType");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("attendanceDeviceType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                atTDivType = employee_attendanceDTO.attendanceDeviceType = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Long Value2 = employee_attendance_deviceDAO.getEmployeeIdInEmployeeAttendanceDevice(empRetType, atTDivType);
            System.out.println("employeeAttendanceDeviceType = " + Value2);
            if (Value2 != null) {

                employee_attendanceDTO.employeeAttendanceDeviceType = Value2;
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value2);
            }

//			Value = request.getParameter("employeeAttendanceDeviceType");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("employeeAttendanceDeviceType = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//
//				employee_attendanceDTO.employeeAttendanceDeviceType = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}

            Value = request.getParameter("inTimestamp");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("inTimestamp = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                try {
                    Date d = f.parse(Value);
                    employee_attendanceDTO.inTimestamp = d.getTime();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("inTime");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("inTime = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                employee_attendanceDTO.inTime = Value;
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("outTimestamp");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("outTimestamp = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                try {
                    Date d = f.parse(Value);
                    employee_attendanceDTO.outTimestamp = d.getTime();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("outTime");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("outTime = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                employee_attendanceDTO.outTime = Value;
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("isAbsent");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("isAbsent = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                employee_attendanceDTO.isAbsent = 1;
            } else {
                employee_attendanceDTO.isAbsent = 0;
            }

            Value = request.getParameter("isOvertime");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("isOvertime = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                employee_attendanceDTO.isOvertime = 1;
            } else {
                employee_attendanceDTO.isOvertime = 0;
            }

            Value = request.getParameter("isApproved");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("isApproved = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                employee_attendanceDTO.isApproved = 1;
            } else {
                employee_attendanceDTO.isApproved = 0;
            }

            if (addFlag) {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);

                employee_attendanceDTO.insertionDate = c.getTimeInMillis();
            }


            Value = request.getParameter("insertedBy");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("insertedBy = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                employee_attendanceDTO.insertedBy = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("modifiedBy");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("modifiedBy = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                employee_attendanceDTO.modifiedBy = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            System.out.println("Done adding  addEmployee_attendance dto = " + employee_attendanceDTO);
            long returnedID = -1;

            if (isPermanentTable == false) //add new row for validation and make the old row outdated
            {
                employee_attendanceDAO.setIsDeleted(employee_attendanceDTO.ID, CommonDTO.OUTDATED);
                returnedID = employee_attendanceDAO.add(employee_attendanceDTO);
                employee_attendanceDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
            } else if (addFlag == true) {
                returnedID = employee_attendanceDAO.manageWriteOperations(employee_attendanceDTO, SessionConstants.INSERT, -1, userDTO);

                Utils.addSuccessMessage( request, "Employee attendance added successfully" );
            } else {
                returnedID = employee_attendanceDAO.manageWriteOperations(employee_attendanceDTO, SessionConstants.UPDATE, -1, userDTO);

                Utils.addSuccessMessage( request, "Employee attendance edited successfully" );
            }

            new Attendance_alert_detailsDAO().alertEmployeeIfLateEntry(employee_attendanceDTO.employeeRecordsType, employee_attendanceDTO.inTimestamp, employee_attendanceDTO.inTime, employee_attendanceDTO.outTime);


            if (isPermanentTable) {
                String inPlaceSubmit = request.getParameter("inplacesubmit");

                if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                    getEmployee_attendance(request, response, returnedID);
                } else {
                    response.sendRedirect("Employee_attendanceServlet?actionType=search");
                }
            } else {
                commonRequestHandler.validate(employee_attendanceDAO.getDTOByID(returnedID), request, response, userDTO);
            }

        } catch (Exception e) {
            e.printStackTrace();

            Utils.addErrorMessage( request, " Employee attendance couldn't be saved" );
            response.sendRedirect("Employee_attendanceServlet?actionType=getAddPage");
        }
    }


    private void getEmployee_attendance(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {
        System.out.println("in getEmployee_attendance");
        Employee_attendanceDTO employee_attendanceDTO = null;
        try {
            employee_attendanceDTO = employee_attendanceDAO.getDTOByID(id);
            request.setAttribute("ID", employee_attendanceDTO.iD);
            request.setAttribute("employee_attendanceDTO", employee_attendanceDTO);
            request.setAttribute("employee_attendanceDAO", employee_attendanceDAO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "employee_attendance/employee_attendanceInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "employee_attendance/employee_attendanceSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "employee_attendance/employee_attendanceEditBody.jsp?actionType=edit";
                } else {
                    URL = "employee_attendance/employee_attendanceEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getEmployee_attendance(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getEmployee_attendance(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void searchEmployee_attendance(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException, ParseException {
        System.out.println("in  searchEmployee_attendance 1");

        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        String fromDate = request.getParameter("in_timestamp_start");
        String toDate = request.getParameter("in_timestamp_end");
        String month = request.getParameter("month");
        String year = request.getParameter("year");
        String employeeRecordsId = request.getParameter("employee_records_id");
        String officeUnitsId = request.getParameter("office_units_id");
        String decentEmployeeRecordsId = request.getParameter("decent_employee_records_id");
        String searchReportType = request.getParameter("search_report_type");
        String searchDate = request.getParameter("search_date");

        List<AttendanceReportDetails> employeeAttendanceReportDetails = getAttendanceReport(officeUnitsId, searchReportType, employeeRecordsId, decentEmployeeRecordsId, searchDate, fromDate, toDate, month, year, userDTO);

        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");

        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

       /* RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_EMPLOYEE_ATTENDANCE,
                request,
                employee_attendanceDAO,
                SessionConstants.VIEW_EMPLOYEE_ATTENDANCE,
                SessionConstants.SEARCH_EMPLOYEE_ATTENDANCE,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }*/

        request.setAttribute("employeeAttendanceReportDetails",employeeAttendanceReportDetails);
        request.setAttribute("employee_attendanceDAO", employee_attendanceDAO);
        request.setAttribute("fromDate", fromDate);
        request.setAttribute("toDate",toDate);
        request.setAttribute("month", month);
        request.setAttribute("year", year);
        request.setAttribute("employeeRecordsId",employeeRecordsId);
        request.setAttribute("decentEmployeeRecordsId", decentEmployeeRecordsId);
        request.setAttribute("searchReportType",searchReportType);
        request.setAttribute("searchDate",searchDate);
        request.setAttribute("officeUnitsId",officeUnitsId);

        RequestDispatcher rd;
        if (!isPermanent) {
            if (hasAjax == false) {
                System.out.println("Going to employee_attendance/employee_attendanceApproval.jsp");
                rd = request.getRequestDispatcher("employee_attendance/employee_attendanceApproval.jsp");
            } else {
                System.out.println("Going to employee_attendance/employee_attendanceApprovalForm.jsp");
                rd = request.getRequestDispatcher("employee_attendance/employee_attendanceApprovalForm.jsp");
            }
        } else {
            if (hasAjax == false) {
                System.out.println("Going to employee_attendance/employee_attendanceSearch.jsp");
                rd = request.getRequestDispatcher("employee_attendance/employee_attendanceSearch.jsp");
            } else {
                System.out.println("Going to employee_attendance/employee_attendanceSearchForm.jsp");
                rd = request.getRequestDispatcher("employee_attendance/employee_attendanceSearchForm.jsp");
            }
        }
        rd.forward(request, response);
    }

    private List<AttendanceReportDetails> getAttendanceReport(String officeUnitsId,String searchReportType, String employee_records_id, String decentEmployeeRecordsId,String searchDate, String fromDate, String toDate,String month, String year,  UserDTO userDTO) throws ParseException {
        List<AttendanceReportDetails> employeeAttendanceReportDetails = new ArrayList<>();

        List<Long> employeeRecordsId = getEmployeeRecordsId(employee_records_id,decentEmployeeRecordsId,userDTO.employee_record_id,officeUnitsId);
        Pair<Long,Long> dateRange = new Employee_attendanceDAO().parseDateRangeFromSearchParam(fromDate,toDate,month,year,searchDate);

        if(employeeRecordsId==null || employeeRecordsId.size()==0 || dateRange==null || dateRange.getKey()==null || dateRange.getValue()==null)
            return employeeAttendanceReportDetails;

        if(searchReportType != null && searchReportType.equalsIgnoreCase("1")) {
            employeeAttendanceReportDetails = new Employee_attendanceDAO().getLateAttendanceReportData(searchDate, employeeRecordsId);
        } else if(searchReportType != null && searchReportType.equalsIgnoreCase("2")) {
            employeeAttendanceReportDetails = new Employee_attendanceDAO().getEarlyOutAttendanceReportData(searchDate, employeeRecordsId);
        } else if(searchReportType != null && searchReportType.equalsIgnoreCase("3")) {
            employeeAttendanceReportDetails = new Employee_attendanceDAO().getOvertimeReport(employeeRecordsId,dateRange.getKey(),dateRange.getValue());
        }  else if(searchReportType != null && searchReportType.equalsIgnoreCase("4")) {
            employeeAttendanceReportDetails = new Employee_attendanceDAO().getPresentAbsentReport(employeeRecordsId, dateRange.getKey(), dateRange.getValue());
        } else if(searchReportType != null && searchReportType.equalsIgnoreCase("5")){
            employeeAttendanceReportDetails = new Employee_attendanceDAO().getAttendanceReportMultipleEmployeeDateRange(employeeRecordsId,dateRange.getKey(),dateRange.getValue());
        } else { // like 5, attendance
            employeeAttendanceReportDetails = new Employee_attendanceDAO().getAttendanceReportMultipleEmployeeDateRange(employeeRecordsId,dateRange.getKey(),dateRange.getValue());
        }

        return employeeAttendanceReportDetails;

    }

    private List<Long> getEmployeeRecordsId(String employeeRecordsId, String decentEmployeeRecordsId, long currentLoginEmployeeRecordsId, String officeUnitId) {
        if(employeeRecordsId!=null && !employeeRecordsId.isEmpty() && isNumeric(employeeRecordsId)) {
            return Arrays.asList(Long.parseLong(employeeRecordsId));
        } else if(decentEmployeeRecordsId!=null && !decentEmployeeRecordsId.isEmpty() && isNumeric(decentEmployeeRecordsId)) {
            return Arrays.asList(Long.parseLong(decentEmployeeRecordsId));
        } if(officeUnitId!=null && !officeUnitId.isEmpty() && isNumeric(officeUnitId)) {
            List<EmployeeOfficeDTO> employeeOfficeDTOS = EmployeeOfficesDAO.getInstance().getByOfficeUnitId(Long.parseLong(officeUnitId));
            return employeeOfficeDTOS.stream()
                    .map(dto -> dto.employeeRecordId)
                    .collect(Collectors.toList());
        } else {
            return Arrays.asList(currentLoginEmployeeRecordsId);
            //return EmployeeOfficesDAO.getInstance().getAllDescentEmployeeRecordIds(currentLoginEmployeeRecordsId);
        }
    }

    public boolean isNumeric(String strNum) {
        Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");
        if (strNum == null) {
            return false;
        }
        return pattern.matcher(strNum).matches();
    }

}

