package employee_attendance;

public class AttendanceReportDetails {
    public long employeeRecordsId = 0;
    public long date;
    public String inTime;
    public String outTime;
    public boolean isAbsent = false;
    public boolean isPresent = false;
    public boolean isHoliday = false;
    public boolean isLate = false;
    public boolean isEarlyLeave = false;
    public String holidayNote;
    public boolean isOnLeave = false;
    public String leaveNote;
    public String entryCellBackgroundColor;
    public String outCellBackgroundColor;
    public int overtimeDays = 0;
    public int workingDays = 0;
    public int presentDays = 0;
    public int absentDays = 0;
    public int totalLateInRange = 0;
    public int totalEarlyOutRange = 0;

    public long getDate() {
        return date;
    }
}
