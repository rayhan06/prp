package employee_attendance;

public class EmployeeLastAttendanceSummary {

    public int totalWorkingDays;
    public int totalPresent;
    public int totalLate;
    public int totalEarlyOut;
}
