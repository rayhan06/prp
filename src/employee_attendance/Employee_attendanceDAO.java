package employee_attendance;

import attendance_alert_details.Attendance_alert_detailsDAO;
import attendance_device.Attendance_deviceDAO;
import common.ConnectionAndStatementUtil;
import dbm.DBMR;
import employee_absent_approved_history.Employee_absent_approved_historyDAO;
import employee_absent_approved_history.Employee_absent_approved_historyDTO;
import employee_leave_details.Employee_leave_detailsDAO;
import employee_leave_details.Employee_leave_detailsDTO;
import employee_offices.EmployeeOfficesDAO;
import employee_records.EmployeeFlatInfoDTO;
import employee_records.Employee_recordsDAO;
import holiday.HolidayRepository;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import sessionmanager.SessionConstants;
import test_lib.util.Pair;
import user.UserDTO;
import util.CommonDTO;
import util.DateUtils;
import util.NavigationService4;
import util.TimeFormat;
import workflow.WorkflowController;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;
import java.util.stream.Collectors;

public class Employee_attendanceDAO extends NavigationService4 {

    Logger logger = Logger.getLogger(getClass());
    private final static String notFound = "NotFound";
    private final static String absentBackgroundColor = "#ef9d9d";
    private final static String lateBackgroundColor = "#ffb720";
    private final static String presentBackgroundColor = "#6eff6e";
    private final static String regularShiftOutTime = "18:00:00";
    public Employee_attendanceDAO(String tableName) {
        super(tableName);
        joinSQL = "";
        commonMaps = new Employee_attendanceMAPS(tableName);
        columnNames = new String[]
                {
                        "ID",
                        "employee_records_type",
                        "attendance_device_type",
                        "employee_attendance_device_type",
                        "late_approval_comment",
                        "late_approved_by_emp_records_id",
                        "in_timestamp",
                        "in_time",
                        "out_timestamp",
                        "out_time",
                        "is_absent",
                        "is_overtime",
                        "is_approved",
                        "insertion_date",
                        "inserted_by",
                        "modified_by",
                        "search_column",
                        "isDeleted",
                        "lastModificationTime"
                };
    }

    public Employee_attendanceDAO() {
        this("employee_attendance");
    }

    public void setSearchColumn(Employee_attendanceDTO employee_attendanceDTO) {
        employee_attendanceDTO.searchColumn = "";
        employee_attendanceDTO.searchColumn += Attendance_deviceDAO.getInstance().getDeviceName(employee_attendanceDTO.attendanceDeviceType, "English");
        employee_attendanceDTO.searchColumn += Attendance_deviceDAO.getInstance().getDeviceName(employee_attendanceDTO.attendanceDeviceType, "Bangla");
        employee_attendanceDTO.searchColumn += Employee_recordsDAO.getEmployeeName(employee_attendanceDTO.employeeRecordsType, "English");
        employee_attendanceDTO.searchColumn += Employee_recordsDAO.getEmployeeName(employee_attendanceDTO.employeeRecordsType, "Bangla");
        employee_attendanceDTO.searchColumn += employee_attendanceDTO.inTime;
        employee_attendanceDTO.searchColumn += employee_attendanceDTO.outTime;
        employee_attendanceDTO.searchColumn += employee_attendanceDTO.inTimestamp;
        employee_attendanceDTO.searchColumn += employee_attendanceDTO.outTimestamp;

    }

    public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException {
        Employee_attendanceDTO employee_attendanceDTO = (Employee_attendanceDTO) commonDTO;


        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
        long inTimeMS = 0, outTimeMS = 0;
        try {
            inTimeMS = sdf.parse(employee_attendanceDTO.inTime).getTime();
            outTimeMS = sdf.parse(employee_attendanceDTO.outTime).getTime();
        } catch (Exception ex) {
            logger.debug(ex);
        }


        int index = 1;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(employee_attendanceDTO);
        if (isInsert) {
            ps.setObject(index++, employee_attendanceDTO.iD);
        }
        ps.setObject(index++, employee_attendanceDTO.employeeRecordsType);
        ps.setObject(index++, employee_attendanceDTO.attendanceDeviceType);
        ps.setObject(index++, employee_attendanceDTO.employeeAttendanceDeviceType);
        ps.setObject(index++, employee_attendanceDTO.lateApprovalComment);
        ps.setObject(index++, employee_attendanceDTO.lateApprovedByEmpRecordsId);
        ps.setObject(index++, employee_attendanceDTO.inTimestamp);
        ps.setObject(index++, new Time(inTimeMS));
        ps.setObject(index++, employee_attendanceDTO.outTimestamp);
        if(outTimeMS==0) {
            ps.setObject(index++, null);
        } else {
            ps.setObject(index++, new Time(outTimeMS));
        }

        ps.setObject(index++, employee_attendanceDTO.isAbsent);
        ps.setObject(index++, employee_attendanceDTO.isOvertime);
        ps.setObject(index++, employee_attendanceDTO.isApproved);
        ps.setObject(index++, employee_attendanceDTO.insertionDate);
        ps.setObject(index++, employee_attendanceDTO.insertedBy);
        ps.setObject(index++, employee_attendanceDTO.modifiedBy);
        ps.setObject(index++, employee_attendanceDTO.searchColumn);
        if (isInsert) {
            ps.setObject(index++, 0);
            ps.setObject(index++, lastModificationTime);
        }
    }

    public void get(Employee_attendanceDTO employee_attendanceDTO, ResultSet rs) throws SQLException {
        employee_attendanceDTO.ID = employee_attendanceDTO.iD = rs.getLong("ID");
        employee_attendanceDTO.employeeRecordsType = rs.getLong("employee_records_type");
        employee_attendanceDTO.attendanceDeviceType = rs.getLong("attendance_device_type");
        employee_attendanceDTO.employeeAttendanceDeviceType = rs.getLong("employee_attendance_device_type");
        employee_attendanceDTO.lateApprovalComment = rs.getString("late_approval_comment");
        employee_attendanceDTO.lateApprovedByEmpRecordsId = rs.getLong("late_approved_by_emp_records_id");
        employee_attendanceDTO.inTimestamp = rs.getLong("in_timestamp");
        employee_attendanceDTO.inTime = rs.getString("in_time");
        employee_attendanceDTO.outTimestamp = rs.getLong("out_timestamp");
        employee_attendanceDTO.outTime = rs.getString("out_time");
        employee_attendanceDTO.isAbsent = rs.getInt("is_absent");
        employee_attendanceDTO.isOvertime = rs.getInt("is_overtime");
        employee_attendanceDTO.isApproved = rs.getInt("is_approved");
        employee_attendanceDTO.insertionDate = rs.getLong("insertion_date");
        employee_attendanceDTO.insertedBy = rs.getString("inserted_by");
        employee_attendanceDTO.modifiedBy = rs.getString("modified_by");
        employee_attendanceDTO.searchColumn = rs.getString("search_column");
        employee_attendanceDTO.isDeleted = rs.getInt("isDeleted");
        employee_attendanceDTO.lastModificationTime = rs.getLong("lastModificationTime");
    }

    public void setIsApproved(long employee_records_type, long in_timestamp, String comments, long approvedByRecordsId) {
        Connection connection = null;
        PreparedStatement stmt = null;
        try {

            String sql = "UPDATE " + tableName + " SET is_approved = ? , late_approval_comment = ? , late_approved_by_emp_records_id = ?";

            sql += " WHERE employee_records_type=? AND in_timestamp=?";

            printSql(sql);

            connection = DBMR.getInstance().getConnection();

            stmt = connection.prepareStatement(sql);

            stmt.setObject(1, 1);
            stmt.setObject(2, comments);
            stmt.setObject(3, approvedByRecordsId);
            stmt.setObject(4, employee_records_type);
            stmt.setObject(5, in_timestamp);

            stmt.executeUpdate();


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
    }

    //need another getter for repository
    public Employee_attendanceDTO getDTOByID(long ID) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Employee_attendanceDTO employee_attendanceDTO = null;
        try {

            String sql = "SELECT * ";

            sql += " FROM " + tableName;

            sql += " WHERE ID=" + ID;

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                employee_attendanceDTO = new Employee_attendanceDTO();

                get(employee_attendanceDTO, rs);

            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return employee_attendanceDTO;
    }

    public List<Long> getEmployeesByOfficeUnitId(long officeUnitId){
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        long employeeRecordId = 0;
        List<Long> employeeRecordsIds = new ArrayList<>();
        try {

            String sql = "SELECT employee_record_id, name_eng, name_bng FROM employee_records R INNER JOIN " +
                    "employee_offices O ON R.id=O.employee_record_id WHERE O.office_unit_id="+officeUnitId;

            logger.debug(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                employeeRecordId = rs.getLong("employee_record_id");
                employeeRecordsIds.add(employeeRecordId);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return employeeRecordsIds;
    }


    public List<Employee_attendanceDTO> getDTOs(Collection recordIDs) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Employee_attendanceDTO employee_attendanceDTO = null;
        List<Employee_attendanceDTO> employee_attendanceDTOList = new ArrayList<>();
        if (recordIDs.isEmpty()) {
            return employee_attendanceDTOList;
        }
        try {

            String sql = "SELECT * ";

            sql += " FROM " + tableName;

            sql += " WHERE ID IN ( ";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql += ",";
                }
                sql += ((ArrayList) recordIDs).get(i);
            }
            sql += ")  order by lastModificationTime desc";

            printSql(sql);

            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                employee_attendanceDTO = new Employee_attendanceDTO();
                get(employee_attendanceDTO, rs);
                System.out.println("got this DTO: " + employee_attendanceDTO);

                employee_attendanceDTOList.add(employee_attendanceDTO);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return employee_attendanceDTOList;

    }


    public List<Employee_attendanceDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }


    public List<Employee_attendanceDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
                                                String filter, boolean tableHasJobCat) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        List<Employee_attendanceDTO> employee_attendanceDTOList = new ArrayList<>();

        try {

            String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee_attendanceDTO employee_attendanceDTO = new Employee_attendanceDTO();
                get(employee_attendanceDTO, rs);

                employee_attendanceDTOList.add(employee_attendanceDTO);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return employee_attendanceDTOList;

    }

    public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                          UserDTO userDTO, String filter, boolean tableHasJobCat) {
        boolean viewAll = false;
        String employeeRecordsId = "";
        if (p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null) {
            System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
            viewAll = true;
        } else {
            System.out.println("ViewAll is null ");
        }

        String sql = "SELECT ";
        if (category == GETIDS) {
            sql += " " + tableName + ".ID as ID ";
        } else if (category == GETCOUNT) {
            sql += " count( " + tableName + ".ID) as countID ";
        } else if (category == GETDTOS) {
            sql += " distinct " + tableName + ".* ";
        }
        sql += "FROM " + tableName + " ";
        sql += joinSQL;


        String AnyfieldSql = "";
        String AllFieldSql = "";

        if (p_searchCriteria != null) {


            Enumeration names = p_searchCriteria.keys();
            String str, value;

            AnyfieldSql = "(";

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                AnyfieldSql += tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
            }
            AnyfieldSql += ")";
            System.out.println("AnyfieldSql = " + AnyfieldSql);

            AllFieldSql = "(";
            int i = 0;

            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                System.out.println(str + ": " + value);
                if (value != null && !value.equalsIgnoreCase("") && (
                        str.equals("in_timestamp_start")
                                || str.equals("in_timestamp_end")
                                || str.equals("out_timestamp_start")
                                || str.equals("out_timestamp_end")
                                || str.equals("insertion_date_start")
                                || str.equals("insertion_date_end")
                                || str.equals("employee_records_id")
                )

                ) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0 && !str.equals("employee_records_id")) {
                        AllFieldSql += " AND  ";
                    }

                    if (str.equals("in_timestamp_start")) {
                        AllFieldSql += "" + tableName + ".in_timestamp >= " + p_searchCriteria.get(str);
                        i++;
                    } else if (str.equals("in_timestamp_end")) {
                        AllFieldSql += "" + tableName + ".in_timestamp <= " + p_searchCriteria.get(str);
                        i++;
                    } else if (str.equals("out_timestamp_start")) {
                        AllFieldSql += "" + tableName + ".out_timestamp >= " + p_searchCriteria.get(str);
                        i++;
                    } else if (str.equals("out_timestamp_end")) {
                        AllFieldSql += "" + tableName + ".out_timestamp <= " + p_searchCriteria.get(str);
                        i++;
                    } else if (str.equals("insertion_date_start")) {
                        AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
                        i++;
                    } else if (str.equals("insertion_date_end")) {
                        AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
                        i++;
                    } else if (str.equals("employee_records_id")) {
                        employeeRecordsId = p_searchCriteria.get(str)+"";
                    }


                }
            }

            AllFieldSql += ")";
            System.out.println("AllFieldSql = " + AllFieldSql);


        }


        sql += " WHERE ";

        sql += " (" + tableName + ".isDeleted = 0 ";
        sql += ")";


        if (!filter.equalsIgnoreCase("")) {
            sql += " and " + filter + " ";
        }
        boolean AnyOrAll = false;
        if (!AnyfieldSql.equals("()") && !AnyfieldSql.equals("")) {
            sql += " AND " + AnyfieldSql;
            AnyOrAll = true;
        }
        if (!AllFieldSql.equals("()") && !AllFieldSql.equals("")) {
            sql += " AND " + AllFieldSql;
            AnyOrAll = true;
        }
        if(!employeeRecordsId.equals("")) {
            sql += " AND " + tableName + ".employee_records_type = " + employeeRecordsId;
        } else {
            if(!AnyOrAll){
                try {
                    sql += " AND " + tableName + ".employee_records_type = " + WorkflowController.getEmployeeRecordsIdFromUserId(userDTO.ID);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }



        sql += " order by " + tableName + ".lastModificationTime desc ";

        printSql(sql);

        if (limit >= 0) {
            sql += " limit " + 1000000; // temporary for testing purpose
        }
        if (offset >= 0) {
            sql += " offset " + offset;
        }

        System.out.println("-------------- sql = " + sql);

        return sql;
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
                                           boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat) {
        return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
    }


    public List<AttendanceReportDetails> getLateAttendanceReportData(String date, List<Long> employeeRecordsIds) {

        List<Employee_attendanceDTO> allAttendance;

        if(date != null && !date.isEmpty()) {
            allAttendance = new Employee_attendanceDAO().getAttendanceDetailsByDate(Long.parseLong(date));
        } else {
            return new ArrayList<>();
        }

        Set<Long> employeeRecordsSet = new HashSet<>(employeeRecordsIds);
        Map<Long,String> employeeToInTimeMap = new HashMap<>();
        Set<Long> approvedEmployeeId = new HashSet<>();

        for(Employee_attendanceDTO dto: allAttendance) {

            if(employeeRecordsSet.contains(dto.employeeRecordsType)) {

                if(dto.isApproved==1) {
                    approvedEmployeeId.add(dto.employeeRecordsType);
                }

                if(employeeToInTimeMap.containsKey(dto.employeeRecordsType)) {
                    employeeToInTimeMap.put(dto.employeeRecordsType, getMinTime(employeeToInTimeMap.get(dto.employeeRecordsType), dto.inTime));
                } else {
                    employeeToInTimeMap.put(dto.employeeRecordsType, dto.inTime);
                }
            }

        }

        List<AttendanceReportDetails> attendanceReportDetails = new ArrayList<>();

        for(Map.Entry<Long,String> entry: employeeToInTimeMap.entrySet()) {
            boolean isLate = new Attendance_alert_detailsDAO().isEmployeeLate(entry.getKey(),Long.parseLong(date),TimeFormat.getInAmPmFormat(entry.getValue()), false);
            if(isLate && !approvedEmployeeId.contains(entry.getKey())) {
                AttendanceReportDetails report = new AttendanceReportDetails();
                report.employeeRecordsId = entry.getKey();
                report.date = Long.parseLong(date);
                report.inTime = entry.getValue();
                report.isPresent = true;
                report.isLate = true;
                report.entryCellBackgroundColor = lateBackgroundColor;
                attendanceReportDetails.add(report);
            }
        }

        return attendanceReportDetails;
    }

    public List<AttendanceReportDetails> getEarlyOutAttendanceReportData(String date, List<Long> employeeRecordsIds) {

        List<Employee_attendanceDTO> allAttendance;

        if(date != null && !date.isEmpty()) {
            allAttendance = new Employee_attendanceDAO().getAttendanceDetailsByDate(Long.parseLong(date));
        } else {
            return new ArrayList<>();
        }

        Set<Long> employeeRecordsSet = new HashSet<>(employeeRecordsIds);
        Map<Long,String> employeeToOutTimeMap = new HashMap<>();
        Set<Long> approvedEmployeeId = new HashSet<>();

        for(Employee_attendanceDTO dto: allAttendance) {

            if(employeeRecordsSet.contains(dto.employeeRecordsType)) {

                if(dto.isApproved==1) {
                    approvedEmployeeId.add(dto.employeeRecordsType);
                }

                if(employeeToOutTimeMap.containsKey(dto.employeeRecordsType)) {
                    employeeToOutTimeMap.put(dto.employeeRecordsType, getMaxTime(employeeToOutTimeMap.get(dto.employeeRecordsType), dto.inTime));
                } else {
                    employeeToOutTimeMap.put(dto.employeeRecordsType, dto.inTime);
                }

            }
        }
        List<AttendanceReportDetails> attendanceReportDetails = new ArrayList<>();
        for(Map.Entry<Long,String> entry: employeeToOutTimeMap.entrySet()) {
            boolean isEarlyLeave = new Attendance_alert_detailsDAO().isEmployeeLeaveEarly(entry.getKey(),Long.parseLong(date),TimeFormat.getInAmPmFormat(entry.getValue()), false);
            if(isEarlyLeave && !approvedEmployeeId.contains(entry.getKey())) {
                AttendanceReportDetails report = new AttendanceReportDetails();
                report.employeeRecordsId = entry.getKey();
                report.date = Long.parseLong(date);
                report.outTime = entry.getValue();
                report.isPresent = true;
                report.isEarlyLeave = true;
                report.outCellBackgroundColor = lateBackgroundColor;
                attendanceReportDetails.add(report);
            }
        }
        return attendanceReportDetails;
    }


    public List<AttendanceReportDetails> getAttendanceReportMultipleEmployeeDateRange(List<Long> employeeRecordsIds, long startDate, long endDate) {
        List<AttendanceReportDetails> attendanceReportDetails = new ArrayList<>();
        for(Long employeeRecordsId: employeeRecordsIds) {
            attendanceReportDetails.addAll( getAttendanceReportSingleEmployeeDateRange(employeeRecordsId, startDate,endDate) );
        }
        return attendanceReportDetails;
    }

    public List<AttendanceReportDetails> getAttendanceReportMultipleEmployeeSingleDate(List<Long> employeeRecordsIds, String date) {
        long dateLong;
        List<AttendanceReportDetails> attendanceReportDetails = new ArrayList<>();
        if(date != null && !date.isEmpty()) {
            dateLong = Long.parseLong(date);
        } else {
            dateLong = getCurrentTimeInMs(false);
        }

        for(Long id: employeeRecordsIds)       {
            attendanceReportDetails.addAll(getAttendanceReportSingleEmployeeDateRange(id,dateLong,dateLong));
        }
        return attendanceReportDetails;
    }

    public Pair<Long,Long> parseDateRangeFromSearchParam(String fromDate, String toDate, String month, String year, String searchDate) {

        Long startDate = null;
        Long endDate = null;
        try {
            if(fromDate != null && !fromDate.equals("") && !fromDate.equalsIgnoreCase("nan")) {
                startDate = Long.parseLong(fromDate);
            } else if(month != null && !month.isEmpty() && year != null && !year.isEmpty()) {
                startDate = constructDateTime(month, year, true);
            } else if(searchDate != null && !searchDate.equals("") && !searchDate.equalsIgnoreCase("nan")){
                startDate = Long.parseLong(searchDate);
            } else {
               // startDate = getCurrentTimeInMs(true);
            }

            if(toDate != null && !toDate.equals("") && !toDate.equalsIgnoreCase("nan")) {
                endDate = Long.parseLong(toDate);
            } else if(month != null && !month.isEmpty() && year != null && !year.isEmpty()) {
                endDate = constructDateTime(month, year, false);
            } else if(searchDate != null && !searchDate.equals("") && !searchDate.equalsIgnoreCase("nan")){
                endDate = Long.parseLong(searchDate);
            } else {
               // endDate = getCurrentTimeInMs(false);
            }
        } catch (Exception e) {

        }
        return new Pair<>(startDate,endDate);
    }

    public void approveEachEmployeeAbsentDays(Long employeeRecordsId, Long approvedByEmployeeRecordsId, long fromDate, long toDate,String comment) throws Exception {

        if(employeeRecordsId<=0) return;

        List<Employee_attendanceDTO> employeeAttendanceDTOS = getEmployeeAttendanceInRangeUniqueDate(fromDate,toDate,employeeRecordsId);
        Set<Long> presentDate = new HashSet<>();

        for(Employee_attendanceDTO dto: employeeAttendanceDTOS) {
            presentDate.add(dto.inTimestamp);
        }

        Map<Long,String> holidayMap = HolidayRepository.getInstance().getHolidayToDescriptionMap();

        while(fromDate<=toDate) {

            if(!presentDate.contains(fromDate) && !holidayMap.containsKey(fromDate)) {

                Employee_absent_approved_historyDTO employee_absent_approved_historyDTO = new Employee_absent_approved_historyDTO();
                employee_absent_approved_historyDTO.employeeRecordsId = employeeRecordsId;
                employee_absent_approved_historyDTO.absentDate = fromDate;
                employee_absent_approved_historyDTO.comments = comment;
                employee_absent_approved_historyDTO.approvedByEmpRecordsId = approvedByEmployeeRecordsId;
                employee_absent_approved_historyDTO.approvalDate = getCurrentTimeInMs(false);

                new Employee_absent_approved_historyDAO().add(employee_absent_approved_historyDTO);
            }

            fromDate += (24*60*60*1000); // +1 day
        }
    }

    public List<AttendanceReportDetails> getPresentAbsentReport(List<Long> employeeRecordsIds, long startDate, long endDate) {

        List<AttendanceReportDetails> attendanceReportDetails = new ArrayList<>();

        int numberOfWorkingDays=numberOfWorkingDays(startDate,endDate);

        for(Long employeeRecordsId : employeeRecordsIds) {
            AttendanceReportDetails attendanceReport = new AttendanceReportDetails();
            attendanceReport.employeeRecordsId = employeeRecordsId;
            attendanceReport.workingDays = numberOfWorkingDays;
            attendanceReport.presentDays = numberOfPresentDays(employeeRecordsId, startDate,endDate) + numberOfApprovedDays(employeeRecordsId, startDate, endDate);
            attendanceReport.absentDays = numberOfWorkingDays - attendanceReport.presentDays;
            attendanceReportDetails.add(attendanceReport);
        }

        return attendanceReportDetails;
    }

    public AttendanceReportDetails employeePresentAbsentReport(long employeeRecordsId, long startDate, long endDate) {
        int numberOfWorkingDays=numberOfWorkingDays(startDate,endDate);
        AttendanceReportDetails attendanceReport = new AttendanceReportDetails();
        attendanceReport.employeeRecordsId = employeeRecordsId;
        attendanceReport.workingDays = numberOfWorkingDays;
        attendanceReport.presentDays = numberOfPresentDays(employeeRecordsId, startDate,endDate) + numberOfApprovedDays(employeeRecordsId, startDate, endDate);
        attendanceReport.absentDays = numberOfWorkingDays - attendanceReport.presentDays;
        return attendanceReport;
    }

    private int numberOfPresentDays(long employeeRecordsId, long startDate, long endDate) {
        List<Employee_attendanceDTO> employeeAttendanceDTOS = getEmployeeAttendanceInRangeUniqueDate(startDate,endDate,employeeRecordsId);
        return employeeAttendanceDTOS.size();
    }

    private int numberOfApprovedDays(long employeeRecordsId, long startDate, long endDate) {
        return new Employee_absent_approved_historyDAO().approvedDaysInRange(employeeRecordsId, startDate,endDate);
    }

    public int numberOfWorkingDays(long startDate, long endDate) {
        Map<Long,String> holidayMap = HolidayRepository.getInstance().getHolidayToDescriptionMap();
        int holidayCount = 0;
        int totalDays = 0;
        while(startDate<=endDate) {
            if(holidayMap.containsKey(startDate)) holidayCount++;
            totalDays++;
            startDate += (24*60*60*1000); // +1 day
        }
        return totalDays-holidayCount;
    }

    public List<AttendanceReportDetails> getOvertimeReport(List<Long> employeeRecordsIds, long startDate, long endDate) {
        List<AttendanceReportDetails> attendanceReportDetails = new ArrayList<>();
        for(Long employeeRecordsId : employeeRecordsIds) {
            AttendanceReportDetails attendanceReport = new AttendanceReportDetails();
            attendanceReport.employeeRecordsId = employeeRecordsId;
            attendanceReport.overtimeDays = getEmployeeOvertimeDaysInDateRange(employeeRecordsId, startDate,endDate);
            attendanceReportDetails.add(attendanceReport);
        }
        return attendanceReportDetails;
    }

    private int getEmployeeOvertimeDaysInDateRange(long employeeRecordsId, long startDate, long endDate) {
        List<Employee_attendanceDTO> employeeAttendanceDTOS = getEmployeeOvertimeDTOInRange(startDate,endDate,employeeRecordsId);

        long outTime = getTimeMs(new SimpleDateFormat("HH:mm:ss"), regularShiftOutTime);

        long employeeOutTime;
        int overTimeDays=0;

        for(Employee_attendanceDTO dto: employeeAttendanceDTOS) {

            String employeeOutTimeStr = getMaxTime(dto.inTime,dto.outTime);

            employeeOutTime = getTimeMs(new SimpleDateFormat("HH:mm:ss"), employeeOutTimeStr);

            if(employeeOutTime>outTime) overTimeDays++;
        }

        return overTimeDays;
    }
    private long getTimeMs(SimpleDateFormat sdf,String time) {
        long timeMS = 0;
        try {
            timeMS = sdf.parse(time).getTime();
        } catch (Exception ex) {
            logger.debug(ex);
        }
        return timeMS;
    }

    public Pair<Integer,Integer> getEmployeePresentAndLateCountInDateRange(long employeeRecordsId, long fromDate, long toDate) {
        List<Employee_attendanceDTO> allAttendanceDTOS = getEmployeeIntimeDTOInRange(fromDate,toDate,employeeRecordsId);
        boolean isLate;
        int lateCount=0;
        for(Employee_attendanceDTO dto: allAttendanceDTOS) {
            isLate = new Attendance_alert_detailsDAO().isEmployeeLate(employeeRecordsId,dto.inTimestamp,TimeFormat.getInAmPmFormat(dto.inTime), false);
            if(isLate) lateCount++;
        }
        return new Pair<>(allAttendanceDTOS.size(),lateCount);
    }

    public Pair<Integer,Integer> getEmployeePresentAndEarlyOutCountInDateRange(long employeeRecordsId, long fromDate, long toDate) {
        List<Employee_attendanceDTO> employeeAttendanceDTOS = getEmployeeOvertimeDTOInRange(fromDate,toDate,employeeRecordsId);

        long employeeOutTime;
        int earlyOutCount=0;

        for(Employee_attendanceDTO dto: employeeAttendanceDTOS) {

            String employeeOutTimeStr = getMaxTime(dto.inTime,dto.outTime);

            boolean isEarlyLeave = new Attendance_alert_detailsDAO().isEmployeeLeaveEarly(employeeRecordsId,dto.inTimestamp,TimeFormat.getInAmPmFormat(employeeOutTimeStr), false);
            if(isEarlyLeave) {
                earlyOutCount++;
            }
        }
        return new Pair<>(employeeAttendanceDTOS.size(),earlyOutCount);
    }

    public Pair<Integer,Integer> getEmployeeWorkingDayAndAbsentCountInDateRange(long employeeRecordsId, long fromDate, long toDate) {

        AttendanceReportDetails reportDetails = employeePresentAbsentReport(employeeRecordsId,fromDate,toDate);

        return new Pair<>(reportDetails.workingDays, reportDetails.absentDays);
    }

    public AttendanceReportDetails getEmployeeAttendanceHistoryInRange(long employeeRecordsId, long startTime, long endTime) {
        List<Employee_attendanceDTO> allAttendanceDTOS = getEmployeeAttendanceInRange(startTime,endTime,employeeRecordsId);

        Map<Long, Pair<String,String>> attendanceMap = new HashMap<>();
        Set<Long> approvedDate = new HashSet<>();
        for(Employee_attendanceDTO dto: allAttendanceDTOS) {

            employeeRecordsId = dto.employeeRecordsType;
            if(dto.isApproved==1) {
                approvedDate.add(dto.inTimestamp);
            }
            if(attendanceMap.containsKey(dto.inTimestamp)) {

                Pair bothTime = attendanceMap.get(dto.inTimestamp);
                attendanceMap.put(dto.inTimestamp,new Pair(getMinTime(dto.inTime,bothTime.getKey().toString()),getMaxTime(dto.inTime,bothTime.getValue().toString())));

            } else {

                attendanceMap.put(dto.inTimestamp,new Pair(getMinTime(dto.inTime,dto.outTime),getMaxTime(dto.inTime,dto.outTime)));

            }
        }



       // List<AttendanceReportDetails> attendanceReportDetails = new ArrayList<>();
        List<Employee_leave_detailsDTO> leaveDetailsDTOS =  Employee_leave_detailsDAO.getInstance().getByEmployeeId(employeeRecordsId);
                //new Employee_leave_detailsDAO().getLeaveDetailsByEmployeeInRange(startTime,endTime,employeeRecordsId);

      //  Map<Long,String> holidayMap = HolidayRepository.getInstance().getHolidayToDescriptionMap();

        int totalLateIn = 0;
        int totalEarlyOut = 0;
        int totalPresent = 0;
        int totalAbsent = 0;
        int totalWorkingDays = 0;

        while(startTime <= endTime) {

            AttendanceReportDetails report = new AttendanceReportDetails();

            report.employeeRecordsId = employeeRecordsId;
            report.date = startTime;

            if(attendanceMap.containsKey(startTime)) {
                Pair bothTime = attendanceMap.get(startTime);
                report.inTime = bothTime.getKey().toString();
                report.outTime = bothTime.getValue().toString();

                totalPresent++;

                if(!approvedDate.contains(report.date)) {

                    boolean isLate = new Attendance_alert_detailsDAO().isEmployeeLate(employeeRecordsId,report.date,TimeFormat.getInAmPmFormat(report.inTime), false);
                    boolean isEarlyLeave = new Attendance_alert_detailsDAO().isEmployeeLeaveEarly(employeeRecordsId,report.date,TimeFormat.getInAmPmFormat(report.outTime), false);

                    if(isLate) {
                        totalLateIn++;
                    }
                    if(isEarlyLeave) {
                        totalEarlyOut++;
                    }
                }


            } else {

                boolean leaveStatus = isEmployeeOnLeave(leaveDetailsDTOS, report.date);

                if(leaveStatus==false) {

                    totalAbsent++;
                }
            }

          //  attendanceReportDetails.add(report);
            startTime += (24*60*60*1000); // +1 day
        }
        AttendanceReportDetails reportDetails = new AttendanceReportDetails();

        reportDetails.presentDays = totalPresent;
        reportDetails.absentDays = totalAbsent;
        reportDetails.totalLateInRange = totalLateIn;
        reportDetails.totalEarlyOutRange = totalEarlyOut;
        reportDetails.workingDays = totalPresent + totalAbsent;

        return reportDetails;
    }

    private List<AttendanceReportDetails> getAttendanceReportSingleEmployeeDateRange(long employeeRecordsId, long startTime, long endTime) {

        List<Employee_attendanceDTO> allAttendanceDTOS = getEmployeeAttendanceInRange(startTime,endTime,employeeRecordsId);

        Map<Long, Pair<String,String>> attendanceMap = new HashMap<>();
        Set<Long> approvedDate = new HashSet<>();
        for(Employee_attendanceDTO dto: allAttendanceDTOS) {

            employeeRecordsId = dto.employeeRecordsType;
            if(dto.isApproved==1) {
                approvedDate.add(dto.inTimestamp);
            }
            if(attendanceMap.containsKey(dto.inTimestamp)) {

                Pair bothTime = attendanceMap.get(dto.inTimestamp);
                attendanceMap.put(dto.inTimestamp,new Pair(getMinTime(dto.inTime,bothTime.getKey().toString()),getMaxTime(dto.inTime,bothTime.getValue().toString())));

            } else {

                attendanceMap.put(dto.inTimestamp,new Pair(getMinTime(dto.inTime,dto.outTime),getMaxTime(dto.inTime,dto.outTime)));

            }
        }



        List<AttendanceReportDetails> attendanceReportDetails = new ArrayList<>();
        List<Employee_leave_detailsDTO> leaveDetailsDTOS =  Employee_leave_detailsDAO.getInstance().getByEmployeeId(employeeRecordsId);
            //    new Employee_leave_detailsDAO().getLeaveDetailsByEmployeeInRange(startTime,endTime,employeeRecordsId);
        Map<Long,String> holidayMap = HolidayRepository.getInstance().getHolidayToDescriptionMap();

        while(startTime <= endTime) {

            AttendanceReportDetails report = new AttendanceReportDetails();

            report.employeeRecordsId = employeeRecordsId;
            report.date = startTime;
            report.entryCellBackgroundColor = presentBackgroundColor;
            report.outCellBackgroundColor = presentBackgroundColor;

            if(attendanceMap.containsKey(startTime)) {
                Pair bothTime = attendanceMap.get(startTime);
                report.inTime = bothTime.getKey().toString();
                report.outTime = bothTime.getValue().toString();
                report.isPresent = true;

                if(!approvedDate.contains(report.date)) {
                    boolean isLate = new Attendance_alert_detailsDAO().isEmployeeLate(employeeRecordsId,report.date,TimeFormat.getInAmPmFormat(report.inTime), false);
                    boolean isEarlyLeave = new Attendance_alert_detailsDAO().isEmployeeLeaveEarly(employeeRecordsId,report.date,TimeFormat.getInAmPmFormat(report.outTime), false);

                    if(isLate) {
                        report.entryCellBackgroundColor = lateBackgroundColor;
                        report.isLate = true;
                    }
                    if(isEarlyLeave) {
                        report.outCellBackgroundColor = lateBackgroundColor;
                        report.isEarlyLeave = true;
                    }
                }


            } else if(holidayMap.containsKey(report.date)) {

                report.isHoliday = true;
                report.holidayNote = holidayMap.get(report.date);
                if(report.holidayNote==null || report.holidayNote.isEmpty())
                    report.holidayNote = "Holiday";

            } else {

                boolean leaveStatus = isEmployeeOnLeave(leaveDetailsDTOS, report.date);

                if(leaveStatus==false) {

                    report.isAbsent = true;
                    report.entryCellBackgroundColor = absentBackgroundColor;
                    report.outCellBackgroundColor = absentBackgroundColor;

                } else {

                    report.isOnLeave = true;
                    report.leaveNote = "On Leave";

                }
            }

            attendanceReportDetails.add(report);
            startTime += (24*60*60*1000); // +1 day
        }

        return attendanceReportDetails.stream()
                .sorted(Comparator.comparing(AttendanceReportDetails::getDate))
                .collect(Collectors.toList());
    }

    public boolean isEmployeeOnLeave(List<Employee_leave_detailsDTO> leaveDetailsDTOS, long date ) {

        for(Employee_leave_detailsDTO dto: leaveDetailsDTOS) {
            if(date>=dto.leaveStartDate && date<=dto.leaveEndDate) return true;
        }

        return false;
    }
    public long constructDateTime(String month,String year,boolean isFirstDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String day="1";
        if(!isFirstDate) {
            day = String.valueOf(DateUtils.getDaysInMonth(Integer.parseInt(month)-1, Integer.parseInt(year)));
        }
        Date d = null;
        try {
            d = dateFormat.parse(day.concat("/").concat(month).concat("/").concat(year));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long currentDate = getCurrentTimeInMs(false);
        if(d.getTime()>currentDate)
            return currentDate;
        else
           return d.getTime();
    }
    public long getCurrentTimeInMs(boolean isFirstDateOfMonth) {
        Calendar c = Calendar.getInstance();   // this takes current date
        if(isFirstDateOfMonth) {
            c.set(Calendar.DAY_OF_MONTH, 1);
        }
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime().getTime();
    }
    public String getMinTime(String time1,String time2) {

        if(time1==null) return time2;
        if(time2==null) return time1;

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

        Date d1 = null;
        try {
            d1 = sdf.parse(time1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date d2 = null;

        try {
            d2 = sdf.parse(time2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(d1.getTime() < d2.getTime()) return time1;
        else return time2;
    }
    public String getMaxTime(String time1,String time2) {

        if(time1==null) return time2;
        if(time2==null) return time1;

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

        Date d1 = null;
        try {
            d1 = sdf.parse(time1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date d2 = null;

        try {
            d2 = sdf.parse(time2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(d1.getTime() < d2.getTime()) return time2;
        else return time1;
    }
    public List<Employee_attendanceDTO> getDTOs(String sql){

        if(sql==null){
            return new ArrayList<>();
        }

        return ConnectionAndStatementUtil.getListOfT(sql, rs1->{
            Employee_attendanceDTO dto = new Employee_attendanceDTO();
            try {
                get(dto, rs1);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return dto;
        });
    }

    public List<Employee_attendanceDTO> getDTOsForOvertime(String sql){

        if(sql==null){
            return new ArrayList<>();
        }

        return ConnectionAndStatementUtil.getListOfT(sql, rs1->{
            Employee_attendanceDTO dto = new Employee_attendanceDTO();
            try {
                dto.employeeRecordsType = rs1.getLong("employee_records_type");
                dto.inTimestamp = rs1.getLong("in_timestamp");
                dto.inTime = rs1.getString("in_time");
                dto.outTime = rs1.getString("out_time");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return dto;
        });
    }

    public List<Employee_attendanceDTO> getDTOsForIntime(String sql){

        if(sql==null){
            return new ArrayList<>();
        }

        return ConnectionAndStatementUtil.getListOfT(sql, rs1->{
            Employee_attendanceDTO dto = new Employee_attendanceDTO();
            try {
                dto.employeeRecordsType = rs1.getLong("employee_records_type");
                dto.inTimestamp = rs1.getLong("in_timestamp");
                dto.inTime = rs1.getString("in_time");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return dto;
        });
    }

    public List<Employee_attendanceDTO> getAttendanceDetailsByDate(long employeeRecordsId, long date) {

        String sql = "Select * from " + tableName + " where employee_records_type = " + employeeRecordsId + " and in_timestamp = " + date + " and isDeleted = 0 order by in_time asc";

        List<Employee_attendanceDTO> dtos = getDTOs(sql);

        return   dtos;
    }

    public List<Employee_attendanceDTO> getAttendanceDetailsByDate(long date) {

        String sql = "Select * from " + tableName + " where in_timestamp = " + date + " and isDeleted = 0 order by in_time asc";

        List<Employee_attendanceDTO> dtos = getDTOs(sql);

        return   dtos;
    }

    public List<Employee_attendanceDTO> getEmployeeAttendanceInRange(long from,long to, long employeeRecordsId) {

        String sql = "Select * from " + tableName + " where employee_records_type = " + employeeRecordsId + " and in_timestamp >= " + from + " and in_timestamp <= " + to + " and isDeleted = 0 order by in_time asc";

        List<Employee_attendanceDTO> dtos = getDTOs(sql);

        return   dtos;
    }

    public List<Employee_attendanceDTO> getEmployeeAttendanceInRange(long from,long to) {

        String sql = "Select * from " + tableName + " where in_timestamp >= " + from + " and in_timestamp <= " + to + " and isDeleted = 0 order by in_time asc";

        List<Employee_attendanceDTO> dtos = getDTOs(sql);

        return   dtos;
    }

    public List<Employee_attendanceDTO> getEmployeeAttendanceInRangeWithDeviceId(long from,long to, long attendanceDeviceId ) {

        String sql = "Select * from " + tableName + " where in_timestamp >= " + from + " and in_timestamp <= " + to +  " and attendance_device_type =" + attendanceDeviceId + " and isDeleted = 0 order by in_time asc";

        List<Employee_attendanceDTO> dtos = getDTOs(sql);

        return   dtos;
    }

    public List<Employee_attendanceDTO> getEmployeeAttendanceInRangeUniqueDate(long from,long to, long employeeRecordsId) {

        String sql = "Select * from " + tableName + " where employee_records_type = " + employeeRecordsId + " and in_timestamp >= " + from + " and in_timestamp <= " + to + " group by in_timestamp";

        List<Employee_attendanceDTO> dtos = getDTOs(sql);

        return   dtos;
    }

    public List<Employee_attendanceDTO> getEmployeeIntimeDTOInRange(long from,long to, long employeeRecordsId) {
        String sql = "Select employee_records_type, in_timestamp, MIN(in_time) as in_time " +
                "from " + tableName
                + " where employee_records_type = " + employeeRecordsId + " and in_timestamp >= " + from + " and in_timestamp <= " + to
                + " group by in_timestamp";

        List<Employee_attendanceDTO> dtos = getDTOsForIntime(sql);

        return  dtos;
    }

    public List<Employee_attendanceDTO> getEmployeeOvertimeDTOInRange(long from,long to, long employeeRecordsId) {
        String sql = "Select employee_records_type, in_timestamp, MAX(in_time) as in_time, Max(out_time) as out_time " +
                "from " + tableName
                + " where employee_records_type = " + employeeRecordsId + " and in_timestamp >= " + from + " and in_timestamp <= " + to
                + " group by in_timestamp";

        List<Employee_attendanceDTO> dtos = getDTOsForOvertime(sql);

        return  dtos;
    }

    public String buildOptionReportType(String language) {
        List<OptionDTO> optionDTOList = new ArrayList<>();
        optionDTOList.add(new OptionDTO("Attendance", "হাজিরা", "5"));
        optionDTOList.add(new OptionDTO("Late in", "বিলম্ব", "1"));
        optionDTOList.add(new OptionDTO("Early out", "অগ্র প্রস্থান", "2"));
        optionDTOList.add(new OptionDTO("Overtime", "অধিকাল ভাতা", "4"));
        return Utils.buildOptions(optionDTOList,language,null);
    }
    public String buildOptionsDescentEmployee(String language, long superEmployeeRecordId) {
        List<OptionDTO> optionDTOList = new ArrayList<>();
        try {
            List<EmployeeFlatInfoDTO> employeeInfos = EmployeeOfficesDAO.getInstance().getAllDescentEmployeeFlatInfoDTOs(superEmployeeRecordId);
            optionDTOList = employeeInfos.stream()
                    .map(emp -> new OptionDTO(emp.employeeNameEn, emp.employeeNameBn, String.valueOf(emp.employeeRecordsId)))
                    .collect(Collectors.toList());
        } catch (NullPointerException e) {

        }
        return Utils.buildOptions(optionDTOList,language,null);
    }
    public static boolean hasPermissionToSearchOfficeAndEmployee(UserDTO userDTO) {
        return userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.ATTENDANCE_ADMIN_ROLE;
    }
}
	