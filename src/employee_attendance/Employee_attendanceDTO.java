package employee_attendance;
import java.util.*; 
import util.*; 


public class Employee_attendanceDTO extends CommonDTO
{

	public long ID = 0;
	public long employeeRecordsType = 0;
	public long attendanceDeviceType = 0;
	public long employeeAttendanceDeviceType = 0;
	public long inTimestamp = 0;
	public String inTime = "";
	public long outTimestamp = 0;
	public String outTime = "";
	public int isAbsent = 0;
	public int isOvertime = 0;
	public int isApproved = 0;
	public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
    public String lateApprovalComment = "";
	public long lateApprovedByEmpRecordsId = -1;

	public long getInTimestamp() {
		return inTimestamp;
	}

	public long getInsertionDate() {
		return insertionDate;
	}

	@Override
	public String toString() {
            return "$Employee_attendanceDTO[" +
            " ID = " + ID +
            " employeeRecordsType = " + employeeRecordsType +
            " attendanceDeviceType = " + attendanceDeviceType +
            " employeeAttendanceDeviceType = " + employeeAttendanceDeviceType +
            " inTimestamp = " + inTimestamp +
            " inTime = " + inTime +
            " outTimestamp = " + outTimestamp +
            " outTime = " + outTime +
            " isAbsent = " + isAbsent +
            " isOvertime = " + isOvertime +
            " isApproved = " + isApproved +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}