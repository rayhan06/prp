package employee_attendance;

import attendance_alert_details.Attendance_alert_detailsDAO;
import employee_leave_details.Employee_leave_detailsDAO;
import employee_leave_details.Employee_leave_detailsDTO;
import holiday.HolidayRepository;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import test_lib.util.Pair;
import util.TimeFormat;

import java.util.*;

public class EmployeeAttendanceRepository implements Repository {

    private static final Logger logger = Logger.getLogger(EmployeeAttendanceRepository.class);

    private final Employee_attendanceDAO employee_attendanceDAO;
    long lastUpdateDate;

    Map<Long, Map<Long,EmployeeAttendanceByDate>> employeeAttendanceLast30DaysMap;

    public EmployeeAttendanceRepository() {

        employee_attendanceDAO = new Employee_attendanceDAO();
        employeeAttendanceLast30DaysMap = new HashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class Lazy {
        static final EmployeeAttendanceRepository INSTANCE = new EmployeeAttendanceRepository();
    }

    public synchronized static EmployeeAttendanceRepository getInstance() {

        return Lazy.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        long currentDateInMs = new Employee_attendanceDAO().getCurrentTimeInMs(false);
        lastUpdateDate = currentDateInMs;
        for(int i=1;i<30;i++) {

            List<Employee_attendanceDTO> allAttendance = new Employee_attendanceDAO().getAttendanceDetailsByDate(currentDateInMs);

            constructEmployeesAttendanceMap(allAttendance, currentDateInMs);

            currentDateInMs -= (24*60*60*1000); // -1 day
        }
    }

    public void updateAttendanceCache() {

        long currentDateInMs = new Employee_attendanceDAO().getCurrentTimeInMs(false);
        long runningMS = currentDateInMs;

        while(runningMS>=lastUpdateDate) {
            List<Employee_attendanceDTO> allAttendance = new Employee_attendanceDAO().getAttendanceDetailsByDate(currentDateInMs);

            constructEmployeesAttendanceMap(allAttendance, runningMS);

            runningMS -= (24*60*60*1000); // -1 day
        }
        lastUpdateDate = currentDateInMs;

        long before30DaysMs = currentDateInMs;
        for(int i=0;i<30;i++) before30DaysMs-= (24*60*60*1000);

        for(Map.Entry<Long, Map<Long,EmployeeAttendanceByDate>> rootEntry: employeeAttendanceLast30DaysMap.entrySet()) {

            for(Map.Entry<Long,EmployeeAttendanceByDate> entry: rootEntry.getValue().entrySet()) {
                if(entry.getKey() <= before30DaysMs) {
                    rootEntry.getValue().remove(entry.getKey());
                }
            }
        }
    }

    private void constructEmployeesAttendanceMap(List<Employee_attendanceDTO> attendanceData, long date) {

        Map<Long, Pair<String,String>> employeeWithTimeMap = new HashMap<>();

        Set<Long> approvedEmployee = new HashSet<>();
        long employeeRecordsId;

        for(Employee_attendanceDTO dto: attendanceData) {

            employeeRecordsId = dto.employeeRecordsType;

            if(dto.isApproved==1) {
                approvedEmployee.add(employeeRecordsId);
            }

            if(employeeWithTimeMap.containsKey(employeeRecordsId)) {

                Pair bothTime = employeeWithTimeMap.get(employeeRecordsId);
                employeeWithTimeMap.put(employeeRecordsId,
                        new Pair(new Employee_attendanceDAO().getMinTime(dto.inTime,bothTime.getKey().toString()),
                        new Employee_attendanceDAO().getMaxTime(dto.inTime,bothTime.getValue().toString())));
            } else {

                employeeWithTimeMap.put(employeeRecordsId,new Pair(new Employee_attendanceDAO().getMinTime(dto.inTime,dto.outTime),new Employee_attendanceDAO().getMaxTime(dto.inTime,dto.outTime)));

            }
        }

        for(Map.Entry<Long, Pair<String,String>> entry: employeeWithTimeMap.entrySet()) {

            EmployeeAttendanceByDate employeeAttendanceByDate = new EmployeeAttendanceByDate();
            employeeAttendanceByDate.isPresent = true;

            if(!approvedEmployee.contains(entry.getKey())) {

                employeeAttendanceByDate.isLate = new Attendance_alert_detailsDAO().isEmployeeLate(entry.getKey(),date, TimeFormat.getInAmPmFormat(entry.getValue().getKey()), false);
                employeeAttendanceByDate.isEarlyLeave = new Attendance_alert_detailsDAO().isEmployeeLeaveEarly(entry.getKey(),date,TimeFormat.getInAmPmFormat(entry.getValue().getValue()), false);
            }
            if(!employeeAttendanceLast30DaysMap.containsKey(entry.getKey())) {

                employeeAttendanceLast30DaysMap.put(entry.getKey(), new HashMap<>());
            }
            employeeAttendanceLast30DaysMap.get(entry.getKey()).put(date, employeeAttendanceByDate);
        }
    }

    public Map<Long, Map<Long,EmployeeAttendanceByDate>> getEmployeeAttendanceLast30DaysMap() {
        return employeeAttendanceLast30DaysMap;
    }

    public EmployeeLastAttendanceSummary getEmployeeAttendanceSummaryLast30Days(long employeeRecordsId) {

        EmployeeLastAttendanceSummary summary = new EmployeeLastAttendanceSummary();

        Map<Long,EmployeeAttendanceByDate> employeeDateWiseAttendanceMap = employeeAttendanceLast30DaysMap.get(employeeRecordsId);
        Map<Long,String> holidayMap = HolidayRepository.getInstance().getHolidayToDescriptionMap();
        List<Employee_leave_detailsDTO> leaveDetailsDTOS =  Employee_leave_detailsDAO.getInstance().getByEmployeeId(employeeRecordsId);

        long currentDateInMs = new Employee_attendanceDAO().getCurrentTimeInMs(false);

        int totalWorkingDays = 0;
        int totalPresent = 0;
        int totalLate = 0;
        int totalEarlyOut = 0;

        for(int i=1;i<30;i++) {

            if(!holidayMap.containsKey(currentDateInMs)) {

                totalWorkingDays++;

                if(employeeDateWiseAttendanceMap != null && employeeDateWiseAttendanceMap.containsKey(currentDateInMs)) {

                    totalPresent++;
                    EmployeeAttendanceByDate attendanceStatus = employeeDateWiseAttendanceMap.get(currentDateInMs);

                    if(attendanceStatus.isLate) totalLate++;
                    if(attendanceStatus.isEarlyLeave) totalEarlyOut++;

                } else {
                    boolean leaveStatus = new Employee_attendanceDAO().isEmployeeOnLeave(leaveDetailsDTOS, currentDateInMs);

                    if(leaveStatus) totalPresent++;
                }
            }

            currentDateInMs -= (24*60*60*1000); // -1 day
        }

        summary.totalWorkingDays = totalWorkingDays;
        summary.totalPresent = totalPresent;
        summary.totalLate = totalLate;
        summary.totalEarlyOut = totalEarlyOut;

        return summary;
    }

    @Override
    public String getTableName() {
        return "employee_attendance";
    }
}
