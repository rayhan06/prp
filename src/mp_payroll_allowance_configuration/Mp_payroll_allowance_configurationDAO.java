package mp_payroll_allowance_configuration;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class Mp_payroll_allowance_configurationDAO implements CommonDAOService<Mp_payroll_allowance_configurationDTO> {
    private static final Logger logger = Logger.getLogger(Mp_payroll_allowance_configurationDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (economic_sub_code_id,amount,modified_by,lastModificationTime,"
                    .concat("isDeleted,inserted_by,insertion_time,ID) VALUES(?,?,?,?,?,?,?,?)");

    private static final String updateQuery =
            "UPDATE {tableName} SET economic_sub_code_id=?,amount=?,modified_by=?,lastModificationTime=? WHERE ID=?";

    private static final String getAllActiveCode = "SELECT * FROM mp_payroll_allowance_configuration WHERE isDeleted=0";

    private static final String updateByCodeId =
            "UPDATE mp_payroll_allowance_configuration SET amount=%d,modified_by=%d,lastModificationTime=%d WHERE economic_sub_code_id=%d";

    private final Map<String, String> searchMap = new HashMap<>();

    private Mp_payroll_allowance_configurationDAO() {
        searchMap.put("economic_sub_code_id", " AND (esc.code like ?) ");


    }
    private static class LazyLoader {
        static final Mp_payroll_allowance_configurationDAO INSTANCE = new Mp_payroll_allowance_configurationDAO();
    }

    public static Mp_payroll_allowance_configurationDAO getInstance() {
        return Mp_payroll_allowance_configurationDAO.LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Mp_payroll_allowance_configurationDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.economicSubCodeId);
        ps.setLong(++index, dto.amount);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setInt(++index, 0 /* isDeleted */);
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Mp_payroll_allowance_configurationDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Mp_payroll_allowance_configurationDTO dto = new Mp_payroll_allowance_configurationDTO();
            dto.iD = rs.getLong("ID");
            dto.economicSubCodeId = rs.getLong("economic_sub_code_id");
            dto.amount = rs.getInt("amount");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Mp_payroll_allowance_configurationDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "mp_payroll_allowance_configuration";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Mp_payroll_allowance_configurationDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Mp_payroll_allowance_configurationDTO) commonDTO, updateQuery, false);
    }

    public void updateWithNewList(List<Mp_payroll_allowance_configurationDTO> newList, long requestBy, long requestTime) throws Exception {
        if (newList == null) newList = new ArrayList<>();
        Set<Long> newSubCodes = newList.stream()
                                       .map(dto -> dto.economicSubCodeId)
                                       .collect(Collectors.toSet());
        List<Mp_payroll_allowance_configurationDTO> oldList = getDTOs(getAllActiveCode);
        List<Long> idsToDelete = oldList.stream()
                                        .filter(dto -> !newSubCodes.contains(dto.economicSubCodeId))
                                        .map(dto -> dto.iD)
                                        .collect(Collectors.toList());
        if (!idsToDelete.isEmpty())
            delete(requestBy, idsToDelete, requestTime);
        Set<Long> oldSubCodes = oldList.stream()
                                       .map(dto -> dto.economicSubCodeId)
                                       .collect(Collectors.toSet());
        for (Mp_payroll_allowance_configurationDTO dto : newList) {
            if (!oldSubCodes.contains(dto.economicSubCodeId)) {
                add(dto);
            } else {
                updateAmount(dto.amount, requestBy, requestTime, dto.economicSubCodeId);
            }
        }
    }

    public void updateAmount(int amount, long requestBy, long requestTime, long subCodeId) {
        String updateSql = String.format(updateByCodeId, amount, requestBy, requestTime, subCodeId);
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            try {
                model.getStatement().executeUpdate(updateSql);
                recordUpdateTime(model.getConnection(), getTableName(), requestTime);
                return true;
            } catch (SQLException ex) {
                ex.printStackTrace();
                return false;
            }
        });
    }

    public List<Mp_payroll_allowance_configurationDTO> getAllActiveCodes() {
        return getDTOs(getAllActiveCode);
    }
}