package mp_payroll_allowance_configuration;

import economic_sub_code.EconomicSubCodeModel;
import economic_sub_code.Economic_sub_codeDTO;
import economic_sub_code.Economic_sub_codeRepository;
import mp_payroll_bill.Mp_payroll_billServlet;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;
import static util.StringUtils.convertToBanNumber;


public class Mp_payroll_allowance_configurationRepository implements Repository {
    private static final Logger logger = Logger.getLogger(Mp_payroll_allowance_configurationRepository.class);
    private final Mp_payroll_allowance_configurationDAO dao;
    private final Map<Long, Mp_payroll_allowance_configurationDTO> mapById;

    private Mp_payroll_allowance_configurationRepository() {
        dao = Mp_payroll_allowance_configurationDAO.getInstance();
        mapById = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Mp_payroll_allowance_configurationRepository INSTANCE = new Mp_payroll_allowance_configurationRepository();
    }

    public static Mp_payroll_allowance_configurationRepository getInstance() {
        return Mp_payroll_allowance_configurationRepository.LazyLoader.INSTANCE;
    }

    @Override
    public void reload(boolean reloadAll) {
        logger.debug("MP Payroll_allowance_configurationRepository reload start for, reloadAll : " + reloadAll);
        List<Mp_payroll_allowance_configurationDTO> dtoList = dao.getAllDTOs(reloadAll);
        if (dtoList != null && dtoList.size() > 0) {
            dtoList.stream()
                    .peek(this::removeIfPresent)
                    .filter(dto -> dto.isDeleted == 0)
                    .forEach(dto -> mapById.put(dto.iD, dto));
        }
        logger.debug("MP Payroll_allowance_configurationRepository reload end for, reloadAll : " + reloadAll);
    }

    private void removeIfPresent(Mp_payroll_allowance_configurationDTO newDTO) {
        if (newDTO == null) return;
        if (mapById.get(newDTO.iD) != null) {
            mapById.remove(newDTO.iD);
        }
    }

    public List<Mp_payroll_allowance_configurationDTO> getAllDTOs() {
        return new ArrayList<>(mapById.values());
    }

    public Mp_payroll_allowance_configurationDTO getDTO(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "MPACR")) {
                if (mapById.get(id) == null) {
                    Mp_payroll_allowance_configurationDTO dto = dao.getDTOByID(id);
                    if (dto != null) {
                        mapById.put(dto.iD, dto);
                    }
                }
            }
        }
        return mapById.get(id);
    }
    public Mp_payroll_allowance_configurationDTO getDTODeletedOrNot(long id) {
        if (mapById.get(id) == null) {
            return dao.getDTOFromIdDeletedOrNot(id);
        }
        return mapById.get(id);
    }

    @Override
    public String getTableName() {
        return "mp_payroll_allowance_configuration";
    }

    public List<Mp_payroll_allowance_configurationDTO> getFromDB() {
        return Mp_payroll_allowance_configurationDAO.getInstance().getAllActiveCodes();
    }

    public List<EconomicSubCodeModel> getEconomicSubCodeModels(String language) {
        return getFromDB().stream()
                .map(allowanceConfig -> allowanceConfig.economicSubCodeId)
                .map(subCodeId -> Economic_sub_codeRepository.getInstance().getDTOByID(subCodeId))
                .map(subCodeDTO -> new EconomicSubCodeModel(subCodeDTO, language))
                .collect(toList());
    }

    public String getAllowanceName(long id, String language) {
        Mp_payroll_allowance_configurationDTO dto = getDTO(id);
        if (dto == null) return "";
        Economic_sub_codeDTO subCodeDTO = Economic_sub_codeRepository.getInstance().getDTOByID(dto.economicSubCodeId);
        return "english".equalsIgnoreCase(language)
                ? subCodeDTO.code.concat(" - ").concat(subCodeDTO.descriptionEn)
                : convertToBanNumber(subCodeDTO.code).concat(" - ").concat(subCodeDTO.descriptionBn);
    }

    public Map<Long, Integer> getAmountBySubCodeId() {
        return getFromDB().stream().collect(Collectors.toMap(dto -> dto.economicSubCodeId, dto -> dto.amount));
    }

    public String buildOptions(Long selectedId, String language) {
        List<OptionDTO> optionDTOList =
                getAllDTOs().stream()
                        .map(Mp_payroll_allowance_configurationDTO::getOptionDTO)
                        .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public List<Mp_payroll_allowance_configurationModel> getActiveModels(String language) {
        return getAllDTOs().stream()
                .map(dto -> new Mp_payroll_allowance_configurationModel(dto, language))
                .sorted(Comparator.comparing(dto -> dto.configurationId))
                .collect(Collectors.toList());
    }

    public long getGratuityConfigurationId() {
        List<Economic_sub_codeDTO> dtoList = Economic_sub_codeRepository.getInstance().getAllDTOs().stream()
                .filter(dto -> dto.isDeleted == 0 && dto.code.equalsIgnoreCase(Mp_payroll_billServlet.GRATUITY_ECONOMIC_CODE))
                .collect(Collectors.toList());
        if (dtoList.size() > 0) {
            List<Mp_payroll_allowance_configurationDTO> tmpList = getAllDTOs().stream()
                    .filter(dto -> dto.economicSubCodeId == dtoList.get(0).iD)
                    .collect(Collectors.toList());
            if (tmpList.size() > 0)
                return tmpList.get(0).iD;
        }
        return -1;
    }
}


