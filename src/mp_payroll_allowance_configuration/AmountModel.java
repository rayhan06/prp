package mp_payroll_allowance_configuration;

public class AmountModel {
    public long economicSubCodeId;
    public int amount;

    @Override
    public String toString() {
        return "AmountModel{" +
                "economicSubCodeId=" + economicSubCodeId +
                ", amount=" + amount +
                '}';
    }
}
