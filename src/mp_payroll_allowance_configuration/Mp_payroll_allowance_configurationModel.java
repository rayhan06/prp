package mp_payroll_allowance_configuration;

import economic_sub_code.Economic_sub_codeDTO;
import economic_sub_code.Economic_sub_codeRepository;
import mp_payroll_bill_allowance.Mp_payroll_bill_allowanceDTO;
import pb.Utils;

public class Mp_payroll_allowance_configurationModel {
    public String configurationId;
    public String name;
    public String code;
    public String amount;

    public Mp_payroll_allowance_configurationModel(Mp_payroll_allowance_configurationDTO dto, String language) {
        configurationId = String.valueOf(dto.iD);
        Economic_sub_codeDTO codeDTO = Economic_sub_codeRepository.getInstance().getDTOByID(dto.economicSubCodeId);
        name = language.equalsIgnoreCase("ENGLISH") ? codeDTO.descriptionEn : codeDTO.descriptionBn;
        code = Utils.getDigits(codeDTO.code, language);
        amount = Utils.getDigits(dto.amount, language);
    }

    public Mp_payroll_allowance_configurationModel(Mp_payroll_bill_allowanceDTO dto, String language) {
        configurationId = String.valueOf(dto.mpPayrollAllowanceConfigurationId);
        Mp_payroll_allowance_configurationDTO payroll_allowance_configurationDTO = Mp_payroll_allowance_configurationRepository.getInstance().getDTODeletedOrNot(dto.mpPayrollAllowanceConfigurationId);
        Economic_sub_codeDTO codeDTO = Economic_sub_codeRepository.getInstance().getDTOByID(payroll_allowance_configurationDTO.economicSubCodeId);
        name = language.equalsIgnoreCase("ENGLISH") ? codeDTO.descriptionEn : codeDTO.descriptionBn;
        code = Utils.getDigits(codeDTO.code, language);
        amount = Utils.getDigits(payroll_allowance_configurationDTO.amount, language);
    }
}
