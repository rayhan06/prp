package mp_payroll_allowance_configuration;

import economic_sub_code.Economic_sub_codeDTO;
import economic_sub_code.Economic_sub_codeRepository;
import pb.OptionDTO;
import util.CommonDTO;

import static util.StringUtils.convertToBanNumber;


public class Mp_payroll_allowance_configurationDTO extends CommonDTO implements Comparable<Mp_payroll_allowance_configurationDTO> {

    public long economicSubCodeId = -1;
    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;
    public int amount = 0;

    public OptionDTO getOptionDTO() {
        Economic_sub_codeDTO subCodeDTO = Economic_sub_codeRepository.getInstance().getDTOByID(economicSubCodeId);
        return new OptionDTO(
                subCodeDTO.code.concat(" - ").concat(subCodeDTO.descriptionEn),
                convertToBanNumber(subCodeDTO.code).concat(" - ").concat(subCodeDTO.descriptionBn),
                String.valueOf(iD)
        );
    }

    @Override
    public String toString() {
        return "$Mp_payroll_allowance_configurationDTO[" +
                " iD = " + iD +
                " economicSubCodeId = " + economicSubCodeId +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " isDeleted = " + isDeleted +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

    @Override
    public int compareTo(Mp_payroll_allowance_configurationDTO dto) {
        String code1 = Economic_sub_codeRepository.getInstance().getDTOByID(economicSubCodeId).code;
        String code2 = Economic_sub_codeRepository.getInstance().getDTOByID(dto.economicSubCodeId).code;
        return code1.compareTo(code2);
    }
}