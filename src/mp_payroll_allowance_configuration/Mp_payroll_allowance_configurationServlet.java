package mp_payroll_allowance_configuration;

import com.google.gson.Gson;
import common.BaseServlet;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;



@WebServlet("/Mp_payroll_allowance_configurationServlet")
@MultipartConfig
@SuppressWarnings({"Duplicates"})
public class Mp_payroll_allowance_configurationServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static final long EMPLOYEE_REMUNERATION_ECONOMIC_CODE = 6L;
    public static final long BUDGET_MAPPING_ID = 9L;
    public static final Set<Long> selectedEconomicGroups = new HashSet<>();

    static {
        selectedEconomicGroups.add(31L);
        selectedEconomicGroups.add(32L);
    }

    public Mp_payroll_allowance_configurationDTO buildDTO(Mp_payroll_allowance_configurationServlet.UserInput userInput) {
        Mp_payroll_allowance_configurationDTO allowanceConfigDTO = new Mp_payroll_allowance_configurationDTO();
        allowanceConfigDTO.insertedBy = allowanceConfigDTO.modifiedBy = userInput.modifiedBy;
        allowanceConfigDTO.insertionTime = allowanceConfigDTO.lastModificationTime = userInput.modificationTime;
        allowanceConfigDTO.economicSubCodeId = userInput.economicSubCodeId;
        allowanceConfigDTO.amount = userInput.amount;
        return allowanceConfigDTO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        List<Mp_payroll_allowance_configurationDTO> newDTOs = new ArrayList<>();
        Mp_payroll_allowance_configurationServlet.UserInput userInput = new Mp_payroll_allowance_configurationServlet.UserInput();
        userInput.modifiedBy = userDTO.ID;
        userInput.modificationTime = System.currentTimeMillis();
        long[] economicSubCodeIds = new Gson().fromJson(request.getParameter("economicSubCodes"), long[].class);
        String Value = request.getParameter("amountValues");
        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        List<AmountModel> amountList = Arrays.asList(gson.fromJson(Value, AmountModel[].class));
        Map<Long, Integer> amountMap = amountList.stream()
                .collect(Collectors.toMap(e -> e.economicSubCodeId, e -> e.amount));
        for (long economicSubCodeId : economicSubCodeIds) {
            userInput.economicSubCodeId = economicSubCodeId;
            userInput.amount = amountMap.get(economicSubCodeId);
            newDTOs.add(buildDTO(userInput));
        }
        Mp_payroll_allowance_configurationDAO.getInstance().updateWithNewList(
                newDTOs, userInput.modifiedBy, userInput.modificationTime
        );
        return null;
    }

    @Override
    public String getTableName() {
        return Mp_payroll_allowance_configurationDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Mp_payroll_allowance_configurationServlet";
    }

    @Override
    public Mp_payroll_allowance_configurationDAO getCommonDAOService() {
        return Mp_payroll_allowance_configurationDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.MP_PAYROLL_ALLOWANCE_CONFIGURATION_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.MP_PAYROLL_ALLOWANCE_CONFIGURATION_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.MP_PAYROLL_ALLOWANCE_CONFIGURATION_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Mp_payroll_allowance_configurationServlet.class;
    }
    public String getTableJoinClause(HttpServletRequest request) {
        return " join economic_sub_code esc on esc.id=mp_payroll_allowance_configuration.economic_sub_code_id";
    }

    static class UserInput {
        long economicSubCodeId;
        long modifiedBy;
        long modificationTime;
        int amount;
    }
}

