package budget_modification;

import sessionmanager.SessionConstants;
import util.CommonDTO;

public class Budget_modificationDTO extends CommonDTO {
    public long budgetSelectionInfoId = -1L;
    public long budgetOfficeId = -1L;
    public long budgetOperationId = -1L;
    public long budgetMappingId = -1L;
    public Budget_modificationType modificationType =  Budget_modificationType.INVALID;
    public long budgetId  = -1L;
    public long economicSubCodeId  = -1L;
    public double oldAmount = .0d;
    public double newAmount = .0d;
    public long fileDropzone = -1;
    public String oldBudgetDtoJson;
    public String newBudgetDtoJson;

    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;
}
