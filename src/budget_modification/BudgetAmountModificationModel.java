package budget_modification;

import budget.BudgetDTO;
import budget.BudgetUtils;

public class BudgetAmountModificationModel {
    public long budgetDtoId;
    public double finalAmount;
    public double revisedFinalAmount;
    public double initialExpenditureAmount;

    public static BudgetAmountModificationModel getEmptyInstance() {
        return new BudgetAmountModificationModel();
    }

    private BudgetAmountModificationModel() {
        budgetDtoId = -1;
        finalAmount = 0;
        revisedFinalAmount = 0;
        initialExpenditureAmount = 0;
    }

    public BudgetAmountModificationModel(BudgetDTO budgetDTO) {
        this();
        this.budgetDtoId = budgetDTO.iD;
        this.finalAmount = budgetDTO.finalAmount * BudgetUtils.BUDGET_AMOUNT_UNIT;
        this.revisedFinalAmount = budgetDTO.revisedFinalAmount * BudgetUtils.BUDGET_AMOUNT_UNIT;
        this.initialExpenditureAmount = budgetDTO.initialExpenditureAmount * BudgetUtils.BUDGET_AMOUNT_UNIT;
    }
}
