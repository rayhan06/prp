package budget_modification;

import budget.BudgetDAO;
import budget.BudgetDTO;
import budget.BudgetUtils;
import budget_mapping.Budget_mappingDTO;
import budget_mapping.Budget_mappingRepository;
import com.google.gson.Gson;
import common.ApiResponse;
import common.BaseServlet;
import common.CustomException;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonLoginData;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
@WebServlet("/Budget_modificationServlet")
@MultipartConfig
public class Budget_modificationServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    private final Logger logger = Logger.getLogger(Budget_modificationServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        String actionType = request.getParameter("actionType");
        try {
            switch (actionType) {
                case "addBudgetCode":
                    if (Utils.checkPermission(userDTO, MenuConstants.BUDGET_MODIFICATION_ADD_BUDGET_CODE)) {
                        request.getRequestDispatcher("budget_modification/budget_modificationAddBudgetCode.jsp")
                               .forward(request, response);
                        return;
                    }
                    break;
                case "changeAmount":
                    if (Utils.checkPermission(userDTO, MenuConstants.BUDGET_MODIFICATION_CHANGE_AMOUNT)) {
                        request.getRequestDispatcher("budget_modification/budget_modificationChangeAmount.jsp")
                               .forward(request, response);
                        return;
                    }
                    break;
                case "ajax_getBudgetAmountModificationModel": {
                    Map<String, Object> res = new HashMap<>();
                    try {
                        res.put("success", true);
                        res.put("data", getBudgetAmountModificationModel(request));
                    } catch (Exception e) {
                        logger.error(e);
                        res.put("success", false);
                    }
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().println(new Gson().toJson(res));
                    return;
                }
                case "search":
                case "view":
                case "downloadDropzoneFile":
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private static class UserInput {
        Long id;
        Long budgetSelectionInfoId;
        Long budgetInstitutionalGroupId, budgetOfficeId, budgetOperationId, budgetMappingId;
        Integer budgetCat;
        Long economicGroupId, economicCodeId, economicSubCodeId;
        Long budgetId;
        Long fileDropzone;
        Budget_modificationType modificationType;
        Long modifiedBy, modificationTime;
        Double oldAmount, newAmount;
        Boolean isLangEn;
        String oldBudgetDtoJson;
        String newBudgetDtoJson;
    }

    private BudgetAmountModificationModel getBudgetAmountModificationModel(HttpServletRequest request) throws Exception {
        String parameter = request.getParameter("budgetSelectionInfoId");
        long budgetSelectionInfoId = Utils.parseMandatoryLong(
                parameter,
                "budgetSelectionInfoId=" + parameter + " is invalid"
        );
        parameter = request.getParameter("budgetMappingId");
        long budgetMappingId = Utils.parseMandatoryLong(
                parameter,
                "budgetMappingId=" + parameter + " is invalid"
        );
        parameter = request.getParameter("economicSubCodeId");
        long economicSubCodeId = Utils.parseMandatoryLong(
                parameter,
                "economicSubCodeId=" + parameter + " is invalid"
        );
        BudgetDTO budgetDTO =
                BudgetDAO.getInstance()
                         .getDTOByBudgetMappingAndSubCode(
                                 budgetSelectionInfoId,
                                 budgetMappingId,
                                 economicSubCodeId
                         );
        if (budgetDTO == null) {
            throw new CustomException(String.format(
                    "No BudgetDTO found with budgetSelectionInfoId=%d, budgetMappingId=%d, economicSubCodeId=%d",
                    budgetSelectionInfoId,
                    budgetMappingId,
                    economicSubCodeId
            ));
        }
        return new BudgetAmountModificationModel(budgetDTO);
    }

    private void updateBudgetDtoAndSetOldNewBudgetDto(HttpServletRequest request, UserInput outUserInput) throws Exception {
        if (outUserInput.economicSubCodeId == null) {
            throw new IllegalArgumentException(outUserInput.isLangEn ? "Select Economic Code" : "অর্থনৈতিক কোড নির্বাচন করুন");
        }
        long budgetDtoId = Utils.parseOptionalLong(
                request.getParameter("budgetDtoId"),
                -1L,
                outUserInput.isLangEn ? "Select Economic Code" : "অর্থনৈতিক কোড নির্বাচন করুন"
        );
        BudgetDTO budgetDTO =
                BudgetDAO.getInstance()
                         .getDTOByBudgetMappingAndSubCode(
                                 outUserInput.budgetSelectionInfoId,
                                 outUserInput.budgetMappingId,
                                 outUserInput.economicSubCodeId
                         );
        boolean budgetDtoIsInvalid = budgetDTO == null || budgetDtoId != budgetDTO.iD;
        if (budgetDtoIsInvalid) {
            throw new IllegalArgumentException(outUserInput.isLangEn ? "No budget sector found" : "কোন বাজেট খাত পাওয়া যায়নি");
        }
        outUserInput.oldAmount = budgetDTO.finalAmount;
        outUserInput.oldBudgetDtoJson = gson.toJson(budgetDTO);

        BudgetAmountModificationModel emptyModificationModel = BudgetAmountModificationModel.getEmptyInstance();
        budgetDTO.finalAmount = Utils.parseOptionalDouble(
                request.getParameter("finalAmount"),
                emptyModificationModel.finalAmount,
                null
        );
        budgetDTO.finalAmount /= BudgetUtils.BUDGET_AMOUNT_UNIT;
        budgetDTO.revisedFinalAmount = Utils.parseOptionalDouble(
                request.getParameter("revisedFinalAmount"),
                emptyModificationModel.revisedFinalAmount,
                null
        );
        budgetDTO.revisedFinalAmount /= BudgetUtils.BUDGET_AMOUNT_UNIT;
        budgetDTO.initialExpenditureAmount = Utils.parseOptionalDouble(
                request.getParameter("initialExpenditureAmount"),
                emptyModificationModel.initialExpenditureAmount,
                null
        );
        budgetDTO.initialExpenditureAmount /= BudgetUtils.BUDGET_AMOUNT_UNIT;
        budgetDTO.modifiedBy = outUserInput.modifiedBy;
        budgetDTO.lastModificationTime = outUserInput.modificationTime;

        BudgetDAO.getInstance().update(budgetDTO);
        outUserInput.budgetId = budgetDTO.iD;
        outUserInput.newAmount = budgetDTO.finalAmount;
        outUserInput.newBudgetDtoJson = gson.toJson(budgetDTO);
        outUserInput.modificationType = Budget_modificationType.BUDGET_AMOUNT_CHANGE;
    }

    private UserInput getCommonUserInput(HttpServletRequest request, UserDTO userDTO) throws Exception {
        UserInput userInput = new UserInput();
        userInput.isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        userInput.modifiedBy = userDTO.ID;
        userInput.modificationTime = System.currentTimeMillis();
        userInput.budgetSelectionInfoId = Utils.parseMandatoryLong(
                request.getParameter("budgetSelectionInfoId"),
                userInput.isLangEn ? "Select Economic Year" : "অর্থ বছর নির্বাচন করুন"
        );
        userInput.budgetMappingId = Utils.parseMandatoryLong(
                request.getParameter("budgetMappingId"),
                userInput.isLangEn ? "Select Operation Code" : "অপারেশন কোড নির্বাচন করুন"
        );
        Budget_mappingDTO budgetMapping = Budget_mappingRepository.getInstance().getById(userInput.budgetMappingId);
        if (budgetMapping == null) {
            throw new IllegalArgumentException(
                    userInput.isLangEn ? "Select Correct Operation Code" : "সঠিক অপারেশন কোড নির্বাচন করুন"
            );
        }
        userInput.budgetInstitutionalGroupId = budgetMapping.budgetInstitutionalGroupId;
        userInput.budgetCat = budgetMapping.budgetCat;
        userInput.budgetOfficeId = budgetMapping.budgetOfficeId;
        userInput.budgetOperationId = budgetMapping.budgetOperationId;

        userInput.economicGroupId = Utils.parseOptionalLong(
                request.getParameter("economicGroupId"),
                null,
                null
        );
        userInput.economicCodeId = Utils.parseOptionalLong(
                request.getParameter("economicCodeId"),
                null,
                null
        );
        userInput.economicSubCodeId = Utils.parseOptionalLong(
                request.getParameter("economicSubCodeId"),
                null,
                null
        );
        userInput.oldAmount = Utils.parseOptionalDouble(
                request.getParameter("oldAmount"),
                .0d,
                null
        );
        userInput.newAmount = Utils.parseOptionalDouble(
                request.getParameter("newAmount"),
                .0d,
                null
        );
        userInput.fileDropzone = Utils.parseOptionalLong(
                request.getParameter("fileDropzone"),
                -1L,
                null
        );
        return userInput;
    }

    private void addBudget_modificationDTO(UserInput userInput) throws Exception {
        Budget_modificationDTO budgetModificationDTO = new Budget_modificationDTO();

        budgetModificationDTO.insertedBy = userInput.modifiedBy;
        budgetModificationDTO.insertionTime = userInput.modificationTime;
        budgetModificationDTO.modifiedBy = userInput.modifiedBy;
        budgetModificationDTO.lastModificationTime = userInput.modificationTime;

        budgetModificationDTO.budgetSelectionInfoId = userInput.budgetSelectionInfoId;
        budgetModificationDTO.budgetOfficeId = userInput.budgetOfficeId;
        budgetModificationDTO.budgetOperationId = userInput.budgetOperationId;
        budgetModificationDTO.budgetMappingId = userInput.budgetMappingId;
        budgetModificationDTO.modificationType = userInput.modificationType;
        budgetModificationDTO.budgetId = userInput.budgetId;
        budgetModificationDTO.economicSubCodeId = userInput.economicSubCodeId;
        budgetModificationDTO.oldAmount = userInput.oldAmount;
        budgetModificationDTO.newAmount = userInput.newAmount;
        budgetModificationDTO.fileDropzone = userInput.fileDropzone;
        budgetModificationDTO.oldBudgetDtoJson = userInput.oldBudgetDtoJson;
        budgetModificationDTO.newBudgetDtoJson = userInput.newBudgetDtoJson;

        getCommonDAOService().add(budgetModificationDTO);
    }

    private void addNewBudgetDtoAndSetOldNewBudgetDto(UserInput outUserInput) throws Exception {
        if (outUserInput.economicSubCodeId == null) {
            throw new IllegalArgumentException(outUserInput.isLangEn ? "Select Economic Code" : "অর্থনৈতিক কোড নির্বাচন করুন");
        }
        BudgetDTO budgetDTO =
                BudgetDAO.getInstance()
                         .getDTOByBudgetMappingAndSubCode(
                                 outUserInput.budgetSelectionInfoId,
                                 outUserInput.budgetMappingId,
                                 outUserInput.economicSubCodeId
                         );
        if (budgetDTO != null) {
            throw new IllegalArgumentException(
                    outUserInput.isLangEn ? "Selected Economic Code is already added" : "নির্বাচিত অর্থনৈতিক কোড ইতোমধ্যে সংযুক্ত"
            );
        }
        budgetDTO = new BudgetDTO();
        budgetDTO.budgetSelectionInfoId = outUserInput.budgetSelectionInfoId;
        budgetDTO.budgetMappingId = outUserInput.budgetMappingId;
        budgetDTO.budgetOfficeId = outUserInput.budgetOfficeId;
        budgetDTO.budgetOperationId = outUserInput.budgetOperationId;

        budgetDTO.economicGroupId = outUserInput.economicGroupId;
        budgetDTO.economicCodeId = outUserInput.economicCodeId;
        budgetDTO.economicSubCodeId = outUserInput.economicSubCodeId;

        budgetDTO.insertBy = budgetDTO.modifiedBy = outUserInput.modifiedBy;
        budgetDTO.insertionTime = budgetDTO.lastModificationTime = outUserInput.modificationTime;
        BudgetDAO.getInstance().add(budgetDTO);
        outUserInput.budgetId = budgetDTO.iD;
        outUserInput.oldBudgetDtoJson = null;
        outUserInput.newBudgetDtoJson = gson.toJson(budgetDTO);
        outUserInput.modificationType = Budget_modificationType.CODE_ADDED;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();

        String actionType = request.getParameter("actionType");
        try {
            switch (actionType) {
                case "ajax_addBudgetCode":
                    if (Utils.checkPermission(userDTO, MenuConstants.BUDGET_MODIFICATION_ADD_BUDGET_CODE)) {
                        try {
                            UserInput userInput = getCommonUserInput(request, userDTO);
                            Utils.handleTransaction(() -> {
                                addNewBudgetDtoAndSetOldNewBudgetDto(userInput);
                                addBudget_modificationDTO(userInput);
                            });
                            ApiResponse.sendSuccessResponse(response, "Budget_modificationServlet?actionType=search");
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                    } else {
                        ApiResponse.sendErrorResponse(response, commonLoginData.isLangEng ? "You have no permission to do this task" : "আপনার এই ক্রিয়া সম্পাদনের অনুমতি নেই");
                    }
                    return;
                case "ajax_changeAmount":
                    if (Utils.checkPermission(userDTO, MenuConstants.BUDGET_MODIFICATION_CHANGE_AMOUNT)) {
                        try {
                            UserInput userInput = getCommonUserInput(request, userDTO);
                            Utils.handleTransaction(() -> {
                                updateBudgetDtoAndSetOldNewBudgetDto(request, userInput);
                                addBudget_modificationDTO(userInput);
                            });
                            ApiResponse.sendSuccessResponse(response, "Budget_modificationServlet?actionType=search");
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                    } else {
                        ApiResponse.sendErrorResponse(response, commonLoginData.isLangEng ? "You have no permission to do this task" : "আপনার এই ক্রিয়া সম্পাদনের অনুমতি নেই");
                    }
                    return;
                default:
                    super.doPost(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    public String getTableName() {
        return getCommonDAOService().getTableName();
    }

    @Override
    public String getServletName() {
        return "Budget_modificationServlet";
    }

    @Override
    public Budget_modificationDAO getCommonDAOService() {
        return Budget_modificationDAO.getInstance();
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.BUDGET_MODIFICATION_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Budget_modificationServlet.class;
    }
}
