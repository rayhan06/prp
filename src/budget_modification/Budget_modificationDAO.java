package budget_modification;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Budget_modificationDAO implements CommonDAOService<Budget_modificationDTO> {
    private static final Logger logger = Logger.getLogger(Budget_modificationDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (budget_selection_info_id,budget_office_id,budget_operation_id,budget_mapping_id,"
                    .concat("modification_type,budget_id,economic_sub_code_id,old_amount,new_amount,file_dropzone,")
                    .concat("old_budget_dto_json,new_budget_dto_json,modified_by,lastModificationTime,")
                    .concat("inserted_by,insertion_time,isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET budget_selection_info_id=?,budget_office_id=?,budget_operation_id=?,budget_mapping_id=?,"
                    .concat("modification_type=?,budget_id=?,economic_sub_code_id=?,old_amount=?,new_amount=?,file_dropzone=?,")
                    .concat("old_budget_dto_json=?,new_budget_dto_json=?,modified_by=?,lastModificationTime=? ")
                    .concat("WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private Budget_modificationDAO() {
        searchMap.put("modification_type", " AND (modification_type = ?) ");
        searchMap.put("budget_selection_info_id", " AND (budget_selection_info_id = ?) ");
        searchMap.put("budget_office_id", " AND (budget_office_id = ?) ");
        searchMap.put("budget_operation_id", " AND (budget_operation_id = ?) ");
        searchMap.put("budget_mapping_id", " AND (budget_mapping_id = ?) ");
    }

    private static class LazyLoader {
        static final Budget_modificationDAO INSTANCE = new Budget_modificationDAO();
    }

    public static Budget_modificationDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Budget_modificationDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.budgetSelectionInfoId);
        ps.setLong(++index, dto.budgetOfficeId);
        ps.setLong(++index, dto.budgetOperationId);
        ps.setLong(++index, dto.budgetMappingId);
        ps.setInt(++index, dto.modificationType.getValue());
        ps.setLong(++index, dto.budgetId);
        ps.setLong(++index, dto.economicSubCodeId);
        ps.setDouble(++index, dto.oldAmount);
        ps.setDouble(++index, dto.newAmount);
        ps.setLong(++index, dto.fileDropzone);
        ps.setString(++index, dto.oldBudgetDtoJson);
        ps.setString(++index, dto.newBudgetDtoJson);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Budget_modificationDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Budget_modificationDTO dto = new Budget_modificationDTO();
            dto.iD = rs.getLong("ID");
            dto.budgetSelectionInfoId = rs.getLong("budget_selection_info_id");
            dto.budgetOfficeId = rs.getLong("budget_office_id");
            dto.budgetOperationId = rs.getLong("budget_operation_id");
            dto.budgetMappingId = rs.getLong("budget_mapping_id");
            dto.modificationType = Budget_modificationType.getByValue(rs.getInt("modification_type"));
            dto.budgetId = rs.getLong("budget_id");
            dto.economicSubCodeId = rs.getLong("economic_sub_code_id");
            dto.oldAmount = rs.getDouble("old_amount");
            dto.newAmount = rs.getDouble("new_amount");
            dto.fileDropzone = rs.getLong("file_dropzone");
            dto.oldBudgetDtoJson = rs.getString("old_budget_dto_json");
            dto.newBudgetDtoJson = rs.getString("new_budget_dto_json");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "budget_modification";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Budget_modificationDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Budget_modificationDTO) commonDTO, updateSqlQuery, false);
    }
}