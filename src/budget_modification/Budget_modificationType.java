package budget_modification;

import pb.OptionDTO;
import pb.Utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public enum Budget_modificationType {
    INVALID(-1, "INVALID TYPE", "INVALID TYPE"),
    CODE_ADDED(1, "Add Budget Code", "বাজেট কোড সংযুক্তি"),
    BUDGET_AMOUNT_CHANGE(2, "Change Budget Amount", "বাজেটের পরিমাণ পরিবর্তন"),
    ;

    private final int value;
    private final String nameEn, nameBn;
    private static final Map<Integer, Budget_modificationType> mapByValue;

    static {
        mapByValue = Arrays.stream(values())
                           .collect(Collectors.toMap(
                                   modificationType -> modificationType.value,
                                   modificationType -> modificationType,
                                   (t1, t2) -> t1
                           ));
    }

    Budget_modificationType(int value, String nameEn, String nameBn) {
        this.value = value;
        this.nameEn = nameEn;
        this.nameBn = nameBn;
    }

    public int getValue() {
        return value;
    }

    public OptionDTO getOptionDTO() {
        return new OptionDTO(nameEn, nameBn, String.format("%d", value));
    }

    public static Budget_modificationType getByValue(int value) {
        return mapByValue.getOrDefault(value, INVALID);
    }

    public static String buildOptions(String language, String selectedId) {
        List<OptionDTO> optionDTOList =
                mapByValue.values()
                          .stream()
                          .filter(type -> type.value > 0)
                          .map(Budget_modificationType::getOptionDTO)
                          .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, selectedId);
    }

    public String getName(boolean isLangEn) {
        return isLangEn ? nameEn : nameBn;
    }
}
