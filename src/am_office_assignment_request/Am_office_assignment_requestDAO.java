package am_office_assignment_request;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

import am_dashboard.CountDTO;
import am_office_approval_mapping.Am_office_approval_mappingDTO;
import common.CommonDAOService;
import fund_management.Fund_managementDTO;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.*;
import pb.*;
import user.UserDTO;
import vm_incident.Vm_incidentDAO;
import vm_incident.Vm_incidentDTO;
import vm_requisition.CommonApprovalStatus;

import static java.util.stream.Collectors.toList;

public class Am_office_assignment_requestDAO  implements CommonDAOService<Am_office_assignment_requestDTO>
{

	private static final Logger logger = Logger.getLogger(Am_office_assignment_requestDAO.class);

	private static final String addQuery = "INSERT INTO {tableName} (assignment_date,decision_date," +
			"description_text,status,files_dropzone," +
			"requester_org_id,requester_office_id," +
			"requester_office_unit_id,requester_emp_id," +
			"requester_phone_num,requester_name_en,requester_name_bn,requester_office_name_en,requester_office_name_bn," +
			"requester_office_unit_name_en," +
			"requester_office_unit_name_bn,requester_office_unit_org_name_en,requester_office_unit_org_name_bn," +
			"approver_org_id,approver_office_id,approver_office_unit_id," +
			"approver_emp_id,approver_phone_num,approver_name_en," +
			"approver_name_bn,approver_office_name_en,approver_office_name_bn,approver_office_unit_name_en," +
			"approver_office_unit_name_bn,approver_office_unit_org_name_en," +
			"approver_office_unit_org_name_bn," +
			"withdrawer_org_id,withdrawer_office_id,withdrawer_office_unit_id," +
			"withdrawer_emp_id,withdrawer_phone_num,withdrawer_name_en," +
			"withdrawer_name_bn,withdrawer_office_name_en,withdrawer_office_name_bn,withdrawer_office_unit_name_en," +
			"withdrawer_office_unit_name_bn,withdrawer_office_unit_org_name_en," +
			"withdrawer_office_unit_org_name_bn,logged_in_user_role," +
			"insertion_date,inserted_by,modified_by,search_column,lastModificationTime," +
			"comment,withdrawal_date,withdrawal_reason," +
			"isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static final String updateQuery = "UPDATE {tableName} SET assignment_date = ?,decision_date=?,description_text=?," +
			"status=?,files_dropzone=?," +
			"requester_org_id=?,requester_office_id=?,requester_office_unit_id=?,requester_emp_id=?," +
			"requester_phone_num=?,requester_name_en=?,requester_name_bn=?,requester_office_name_en=?,requester_office_name_bn=?,requester_office_unit_name_en=?," +
			"requester_office_unit_name_bn=?,requester_office_unit_org_name_en=?,requester_office_unit_org_name_bn=?," +
			"approver_org_id=?," +
			"approver_office_id=?,approver_office_unit_id=?," +
			"approver_emp_id=?,approver_phone_num=?,approver_name_en=?," +
			"approver_name_bn=?,approver_office_name_en=?," +
			"approver_office_name_bn=?,approver_office_unit_name_en=?,approver_office_unit_name_bn=?," +
			"approver_office_unit_org_name_en=?," +
			"approver_office_unit_org_name_bn=?," +
			"withdrawer_org_id=?," +
			"withdrawer_office_id=?,withdrawer_office_unit_id=?," +
			"withdrawer_emp_id=?,withdrawer_phone_num=?,withdrawer_name_en=?," +
			"withdrawer_name_bn=?,withdrawer_office_name_en=?," +
			"withdrawer_office_name_bn=?,withdrawer_office_unit_name_en=?,withdrawer_office_unit_name_bn=?," +
			"withdrawer_office_unit_org_name_en=?," +
			"withdrawer_office_unit_org_name_bn=?,logged_in_user_role=?," +
			"insertion_date=?,inserted_by=?,modified_by=?,search_column=?," +
			"lastModificationTime=?,comment = ?, withdrawal_date = ?, withdrawal_reason = ?  WHERE ID = ?";

	private static final String getAllByEmployeeRecordIdAndStatus =
			"SELECT * FROM am_office_assignment_request WHERE requester_emp_id = %d AND status = %d AND isDeleted = 0 ORDER BY lastModificationTime DESC";

	private final Map<String,String> searchMap = new HashMap<>();

	private static class LazyLoader{
		static final Am_office_assignment_requestDAO INSTANCE = new Am_office_assignment_requestDAO();
	}

	public static Am_office_assignment_requestDAO getInstance(){
		return Am_office_assignment_requestDAO.LazyLoader.INSTANCE;
	}

	private Am_office_assignment_requestDAO() {
		searchMap.put("amOfficeAssignmentStatusCat", " AND (status = ?)");
		searchMap.put("assignment_date_start", " AND (insertion_date = ?)");
		searchMap.put("assignment_date_end", " AND (assignment_date = ?)");
		searchMap.put("requester_emp_id"," and (requester_emp_id = ?)");
	}

	public void setSearchColumn(Am_office_assignment_requestDTO am_office_assignment_requestDTO)
	{
		am_office_assignment_requestDTO.searchColumn = "";
	}

	@Override
	public void set(PreparedStatement ps, Am_office_assignment_requestDTO am_office_assignment_requestDTO, boolean isInsert) throws SQLException {
		setSearchColumn(am_office_assignment_requestDTO);
		int index = 0;

		ps.setObject(++index,am_office_assignment_requestDTO.assignmentDate);
		ps.setObject(++index,am_office_assignment_requestDTO.decisionDate);
		ps.setObject(++index,am_office_assignment_requestDTO.descriptionText);
		ps.setObject(++index,am_office_assignment_requestDTO.status);
		ps.setObject(++index,am_office_assignment_requestDTO.filesDropzone);
		ps.setObject(++index,am_office_assignment_requestDTO.requesterOrgId);
		ps.setObject(++index,am_office_assignment_requestDTO.requesterOfficeId);
		ps.setObject(++index,am_office_assignment_requestDTO.requesterOfficeUnitId);
		ps.setObject(++index,am_office_assignment_requestDTO.requesterEmpId);
		ps.setObject(++index,am_office_assignment_requestDTO.requesterPhoneNum);
		ps.setObject(++index,am_office_assignment_requestDTO.requesterNameEn);
		ps.setObject(++index,am_office_assignment_requestDTO.requesterNameBn);
		ps.setObject(++index,am_office_assignment_requestDTO.requesterOfficeNameEn);
		ps.setObject(++index,am_office_assignment_requestDTO.requesterOfficeNameBn);
		ps.setObject(++index,am_office_assignment_requestDTO.requesterOfficeUnitNameEn);
		ps.setObject(++index,am_office_assignment_requestDTO.requesterOfficeUnitNameBn);
		ps.setObject(++index,am_office_assignment_requestDTO.requesterOfficeUnitOrgNameEn);
		ps.setObject(++index,am_office_assignment_requestDTO.requesterOfficeUnitOrgNameBn);
		ps.setObject(++index,am_office_assignment_requestDTO.approverOrgId);
		ps.setObject(++index,am_office_assignment_requestDTO.approverOfficeId);
		ps.setObject(++index,am_office_assignment_requestDTO.approverOfficeUnitId);
		ps.setObject(++index,am_office_assignment_requestDTO.approverEmpId);
		ps.setObject(++index,am_office_assignment_requestDTO.approverPhoneNum);
		ps.setObject(++index,am_office_assignment_requestDTO.approverNameEn);
		ps.setObject(++index,am_office_assignment_requestDTO.approverNameBn);
		ps.setObject(++index,am_office_assignment_requestDTO.approverOfficeNameEn);
		ps.setObject(++index,am_office_assignment_requestDTO.approverOfficeNameBn);
		ps.setObject(++index,am_office_assignment_requestDTO.approverOfficeUnitNameEn);
		ps.setObject(++index,am_office_assignment_requestDTO.approverOfficeUnitNameBn);
		ps.setObject(++index,am_office_assignment_requestDTO.approverOfficeUnitOrgNameEn);
		ps.setObject(++index,am_office_assignment_requestDTO.approverOfficeUnitOrgNameBn);
		ps.setObject(++index,am_office_assignment_requestDTO.withdrawerOrgId);
		ps.setObject(++index,am_office_assignment_requestDTO.withdrawerOfficeId);
		ps.setObject(++index,am_office_assignment_requestDTO.withdrawerOfficeUnitId);
		ps.setObject(++index,am_office_assignment_requestDTO.withdrawerEmpId);
		ps.setObject(++index,am_office_assignment_requestDTO.withdrawerPhoneNum);
		ps.setObject(++index,am_office_assignment_requestDTO.withdrawerNameEn);
		ps.setObject(++index,am_office_assignment_requestDTO.withdrawerNameBn);
		ps.setObject(++index,am_office_assignment_requestDTO.withdrawerOfficeNameEn);
		ps.setObject(++index,am_office_assignment_requestDTO.withdrawerOfficeNameBn);
		ps.setObject(++index,am_office_assignment_requestDTO.withdrawerOfficeUnitNameEn);
		ps.setObject(++index,am_office_assignment_requestDTO.withdrawerOfficeUnitNameBn);
		ps.setObject(++index,am_office_assignment_requestDTO.withdrawerOfficeUnitOrgNameEn);
		ps.setObject(++index,am_office_assignment_requestDTO.withdrawerOfficeUnitOrgNameBn);
		ps.setObject(++index,am_office_assignment_requestDTO.loggedInUserRole);
		ps.setObject(++index,am_office_assignment_requestDTO.insertionDate);
		ps.setObject(++index,am_office_assignment_requestDTO.insertedBy);
		ps.setObject(++index,am_office_assignment_requestDTO.modifiedBy);
		ps.setObject(++index,am_office_assignment_requestDTO.searchColumn);
		ps.setObject(++index, am_office_assignment_requestDTO.lastModificationTime);
		ps.setObject(++index, am_office_assignment_requestDTO.comment);
		ps.setObject(++index, am_office_assignment_requestDTO.withdrawalDate);
		ps.setObject(++index, am_office_assignment_requestDTO.withdrawalReason);
		if (isInsert) {
			ps.setObject(++index, 0);
		}
		ps.setObject(++index, am_office_assignment_requestDTO.iD);
	}

	@Override
	public Am_office_assignment_requestDTO buildObjectFromResultSet(ResultSet rs) {
		try {
			Am_office_assignment_requestDTO am_office_assignment_requestDTO = new Am_office_assignment_requestDTO();
			am_office_assignment_requestDTO.iD = rs.getLong("ID");
			am_office_assignment_requestDTO.assignmentDate = rs.getLong("assignment_date");
			am_office_assignment_requestDTO.decisionDate = rs.getLong("decision_date");
			am_office_assignment_requestDTO.descriptionText = rs.getString("description_text");
			am_office_assignment_requestDTO.status = rs.getInt("status");
			am_office_assignment_requestDTO.filesDropzone = rs.getLong("files_dropzone");
			am_office_assignment_requestDTO.requesterOrgId = rs.getLong("requester_org_id");
			am_office_assignment_requestDTO.requesterOfficeId = rs.getLong("requester_office_id");
			am_office_assignment_requestDTO.requesterOfficeUnitId = rs.getLong("requester_office_unit_id");
			am_office_assignment_requestDTO.requesterEmpId = rs.getLong("requester_emp_id");
			am_office_assignment_requestDTO.requesterPhoneNum = rs.getString("requester_phone_num");
			am_office_assignment_requestDTO.requesterNameEn = rs.getString("requester_name_en");
			am_office_assignment_requestDTO.requesterNameBn = rs.getString("requester_name_bn");
			am_office_assignment_requestDTO.requesterOfficeNameEn = rs.getString("requester_office_name_en");
			am_office_assignment_requestDTO.requesterOfficeNameBn = rs.getString("requester_office_name_bn");
			am_office_assignment_requestDTO.requesterOfficeUnitNameEn = rs.getString("requester_office_unit_name_en");
			am_office_assignment_requestDTO.requesterOfficeUnitNameBn = rs.getString("requester_office_unit_name_bn");
			am_office_assignment_requestDTO.requesterOfficeUnitOrgNameEn = rs.getString("requester_office_unit_org_name_en");
			am_office_assignment_requestDTO.requesterOfficeUnitOrgNameBn = rs.getString("requester_office_unit_org_name_bn");
			am_office_assignment_requestDTO.approverOrgId = rs.getLong("approver_org_id");
			am_office_assignment_requestDTO.approverOfficeId = rs.getLong("approver_office_id");
			am_office_assignment_requestDTO.approverOfficeUnitId = rs.getLong("approver_office_unit_id");
			am_office_assignment_requestDTO.approverEmpId = rs.getLong("approver_emp_id");
			am_office_assignment_requestDTO.approverPhoneNum = rs.getString("approver_phone_num");
			am_office_assignment_requestDTO.approverNameEn = rs.getString("approver_name_en");
			am_office_assignment_requestDTO.approverNameBn = rs.getString("approver_name_bn");
			am_office_assignment_requestDTO.approverOfficeNameEn = rs.getString("approver_office_name_en");
			am_office_assignment_requestDTO.approverOfficeNameBn = rs.getString("approver_office_name_bn");
			am_office_assignment_requestDTO.approverOfficeUnitNameEn = rs.getString("approver_office_unit_name_en");
			am_office_assignment_requestDTO.approverOfficeUnitNameBn = rs.getString("approver_office_unit_name_bn");
			am_office_assignment_requestDTO.approverOfficeUnitOrgNameEn = rs.getString("approver_office_unit_org_name_en");
			am_office_assignment_requestDTO.approverOfficeUnitOrgNameBn = rs.getString("approver_office_unit_org_name_bn");
			am_office_assignment_requestDTO.withdrawerOrgId = rs.getLong("withdrawer_org_id");
			am_office_assignment_requestDTO.withdrawerOfficeId = rs.getLong("withdrawer_office_id");
			am_office_assignment_requestDTO.withdrawerOfficeUnitId = rs.getLong("withdrawer_office_unit_id");
			am_office_assignment_requestDTO.withdrawerEmpId = rs.getLong("withdrawer_emp_id");
			am_office_assignment_requestDTO.withdrawerPhoneNum = rs.getString("withdrawer_phone_num");
			am_office_assignment_requestDTO.withdrawerNameEn = rs.getString("withdrawer_name_en");
			am_office_assignment_requestDTO.withdrawerNameBn = rs.getString("withdrawer_name_bn");
			am_office_assignment_requestDTO.withdrawerOfficeNameEn = rs.getString("withdrawer_office_name_en");
			am_office_assignment_requestDTO.withdrawerOfficeNameBn = rs.getString("withdrawer_office_name_bn");
			am_office_assignment_requestDTO.withdrawerOfficeUnitNameEn = rs.getString("withdrawer_office_unit_name_en");
			am_office_assignment_requestDTO.withdrawerOfficeUnitNameBn = rs.getString("withdrawer_office_unit_name_bn");
			am_office_assignment_requestDTO.withdrawerOfficeUnitOrgNameEn = rs.getString("withdrawer_office_unit_org_name_en");
			am_office_assignment_requestDTO.withdrawerOfficeUnitOrgNameBn = rs.getString("withdrawer_office_unit_org_name_bn");
			am_office_assignment_requestDTO.loggedInUserRole = rs.getLong("logged_in_user_role");
			am_office_assignment_requestDTO.insertionDate = rs.getLong("insertion_date");
			am_office_assignment_requestDTO.insertedBy = rs.getString("inserted_by");
			am_office_assignment_requestDTO.modifiedBy = rs.getString("modified_by");
			am_office_assignment_requestDTO.isDeleted = rs.getInt("isDeleted");
			am_office_assignment_requestDTO.lastModificationTime = rs.getLong("lastModificationTime");
			am_office_assignment_requestDTO.searchColumn = rs.getString("search_column");
			am_office_assignment_requestDTO.comment = rs.getString("comment");
			am_office_assignment_requestDTO.withdrawalDate = rs.getLong("withdrawal_date");
			am_office_assignment_requestDTO.withdrawalReason = rs.getString("withdrawal_reason");
			return am_office_assignment_requestDTO;
		} catch (SQLException ex) {
			logger.error(ex);
			return null;
		}
	}

	@Override
	public String getTableName() {
		return "am_office_assignment_request";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_office_assignment_requestDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_office_assignment_requestDTO) commonDTO,updateQuery,false);
	}

	public Am_office_assignment_requestDTO getDTOByID(long ID) {
		return getDTOFromID(ID);
	}

	public List<Am_office_assignment_requestDTO> getAllByEmployeeRecordIdAndStatus(long employeeRecordId) {
		String sql = String.format(getAllByEmployeeRecordIdAndStatus, employeeRecordId, CommonApprovalStatus.SATISFIED.getValue());
		return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
	}

	public List<CountDTO> getCountByStatus(){
		String countQuery = "SELECT status, COUNT(*) AS count FROM "
				+ getTableName() + " where isDeleted = 0 and status > 0 GROUP BY status  ";
		return ConnectionAndStatementUtil.getListOfT(countQuery,this::getCountBuild);
	}

	public CountDTO getCountBuild(ResultSet rs)
	{
		try
		{
			CountDTO countDTO = new CountDTO();
			countDTO.type4 = rs.getInt("status");
			countDTO.type5 = rs.getInt("count");
			return countDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
}
	