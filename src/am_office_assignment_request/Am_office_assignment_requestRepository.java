package am_office_assignment_request;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Am_office_assignment_requestRepository implements Repository {
	Am_office_assignment_requestDAO am_office_assignment_requestDAO = null;
	
	public void setDAO(Am_office_assignment_requestDAO am_office_assignment_requestDAO)
	{
		this.am_office_assignment_requestDAO = am_office_assignment_requestDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Am_office_assignment_requestRepository.class);
	Map<Long, Am_office_assignment_requestDTO>mapOfAm_office_assignment_requestDTOToiD;

  
	private Am_office_assignment_requestRepository(){
		am_office_assignment_requestDAO =  Am_office_assignment_requestDAO.getInstance();
		mapOfAm_office_assignment_requestDTOToiD = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Am_office_assignment_requestRepository INSTANCE = new Am_office_assignment_requestRepository();
    }

    public static Am_office_assignment_requestRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Am_office_assignment_requestDTO> am_office_assignment_requestDTOs = am_office_assignment_requestDAO.getAllDTOs(reloadAll);
            for(Am_office_assignment_requestDTO am_office_assignment_requestDTO : am_office_assignment_requestDTOs) {
				Am_office_assignment_requestDTO oldAm_office_assignment_requestDTO = getAm_office_assignment_requestDTOByID(am_office_assignment_requestDTO.iD);
				if( oldAm_office_assignment_requestDTO != null ) {
					mapOfAm_office_assignment_requestDTOToiD.remove(oldAm_office_assignment_requestDTO.iD);


				}
				if(am_office_assignment_requestDTO.isDeleted == 0)
				{

					mapOfAm_office_assignment_requestDTOToiD.put(am_office_assignment_requestDTO.iD, am_office_assignment_requestDTO);

				}
			}

		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Am_office_assignment_requestDTO> getAm_office_assignment_requestList() {
		List <Am_office_assignment_requestDTO> am_office_assignment_requests = new ArrayList<Am_office_assignment_requestDTO>(this.mapOfAm_office_assignment_requestDTOToiD.values());
		return am_office_assignment_requests;
	}
	
	
	public Am_office_assignment_requestDTO getAm_office_assignment_requestDTOByID( long ID){
		return mapOfAm_office_assignment_requestDTOToiD.get(ID);
	}

	
	@Override
	public String getTableName() {
		return "am_office_assignment_request";
	}
}


