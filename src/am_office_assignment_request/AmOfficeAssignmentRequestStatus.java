package am_office_assignment_request;

import java.util.Arrays;

public enum AmOfficeAssignmentRequestStatus {
    WAITING_FOR_APPROVAL(1, "#22ccc1"),
    APPROVED(3, "green"),
    REJECTED(4, "crimson"),
    WITHDRAWN(15, "#9400D3"),
    ;

    private final int value;
    private final String color;

    AmOfficeAssignmentRequestStatus(int value, String color) {
        this.value = value;
        this.color = color;
    }

    public int getValue() {
        return value;
    }

    public String getColor() {
        return color;
    }

    public static String getColor(int value) {
        return Arrays.stream(values())
                     .filter(status -> status.value == value)
                     .map(AmOfficeAssignmentRequestStatus::getColor)
                     .findFirst()
                     .orElse("");
    }
}
