package am_office_assignment_request;

import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

import am_office_approval_mapping.AmOfficeApprovalModel;
import am_office_approval_mapping.AmOfficeApprovalNotification;
import am_office_approval_mapping.Am_office_approval_mappingDAO;
import card_info.CardApprovalResponse;
import common.BaseServlet;
import common.CommonDAOService;
import common.RoleEnum;
import employee_assign.EmployeeSearchModalUtil;
import employee_assign.EmployeeSearchModel;
import files.FilesDAO;
import office_units.Office_unitsRepository;
import offices.OfficesDTO;
import offices.OfficesRepository;
import org.apache.log4j.Logger;
import permission.MenuConstants;
import role.PermissionRepository;
import role.RoleDTO;
import role_actions.Role_actionsRepository;
import task_type.TaskTypeEnum;
import user.UserDTO;
import util.*;

import javax.servlet.http.*;

import com.google.gson.Gson;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import vm_requisition.CommonApprovalStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"rawtypes", "Duplicates"})
@WebServlet("/Am_office_assignment_requestServlet")
@MultipartConfig
public class Am_office_assignment_requestServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Am_office_assignment_requestServlet.class);

    @Override
    public String getTableName() {
        return Am_office_assignment_requestDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Am_office_assignment_requestServlet";
    }

    @Override
    public CommonDAOService getCommonDAOService() {
        return Am_office_assignment_requestDAO.getInstance();
    }

    FilesDAO filesDAO = new FilesDAO();
    private final Gson gson = new Gson();


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {

        Am_office_assignment_requestDTO am_office_assignment_requestDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLanEng = Language.equalsIgnoreCase("English");

        if (addFlag) {
            am_office_assignment_requestDTO = new Am_office_assignment_requestDTO();
            am_office_assignment_requestDTO.insertionDate = am_office_assignment_requestDTO.lastModificationTime = System.currentTimeMillis();

            am_office_assignment_requestDTO.insertedBy = am_office_assignment_requestDTO.modifiedBy = String.valueOf(userDTO.ID);

            EmployeeSearchModel model = (gson.fromJson
                    (EmployeeSearchModalUtil.getEmployeeSearchModelJson
                                    (userDTO.employee_record_id,
                                            userDTO.unitID,
                                            userDTO.organogramID),
                            EmployeeSearchModel.class)
            );


            am_office_assignment_requestDTO.requesterEmpId = model.employeeRecordId;
            am_office_assignment_requestDTO.requesterOfficeUnitId = model.officeUnitId;
            am_office_assignment_requestDTO.requesterOrgId = model.organogramId;

            am_office_assignment_requestDTO.requesterNameEn = model.employeeNameEn;
            am_office_assignment_requestDTO.requesterNameBn = model.employeeNameBn;
            am_office_assignment_requestDTO.requesterOfficeUnitNameEn = model.officeUnitNameEn;
            am_office_assignment_requestDTO.requesterOfficeUnitNameBn = model.officeUnitNameBn;
            am_office_assignment_requestDTO.requesterOfficeUnitOrgNameEn = model.organogramNameEn;
            am_office_assignment_requestDTO.requesterOfficeUnitOrgNameBn = model.organogramNameBn;

            am_office_assignment_requestDTO.requesterPhoneNum = model.phoneNumber;

            OfficesDTO requesterOffice = OfficesRepository.getInstance().getOfficesDTOByID(am_office_assignment_requestDTO.requesterOfficeId);

            if (requesterOffice != null) {
                am_office_assignment_requestDTO.requesterOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(model.officeUnitId).officeId;

                am_office_assignment_requestDTO.requesterOfficeNameEn = requesterOffice.officeNameEng;
                am_office_assignment_requestDTO.requesterOfficeNameBn = requesterOffice.officeNameBng;
            }


        } else {
            am_office_assignment_requestDTO = Am_office_assignment_requestDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
            if (am_office_assignment_requestDTO == null) {
                throw new Exception(isLanEng ? "Office assignment request information is not found" : "অফিস অনুরোধের তথ্য খুঁজে পাওয়া যায় নি");
            }
            am_office_assignment_requestDTO.lastModificationTime = System.currentTimeMillis();
            am_office_assignment_requestDTO.modifiedBy = String.valueOf(userDTO.ID);
        }

        String Value = "";


        List<Am_office_assignment_requestDTO> am_office_assignment_requestDTOS = Am_office_assignment_requestRepository
                .getInstance().getAm_office_assignment_requestList();

        boolean isRequesterCurrentRollAlreadyApplied = am_office_assignment_requestDTOS.stream().filter(e -> userDTO.employee_record_id != -1
                && e.requesterEmpId == userDTO.employee_record_id).anyMatch(dto -> dto.loggedInUserRole == userDTO.roleID);

        if (isRequesterCurrentRollAlreadyApplied) {
            RoleDTO roleDTO = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
            String rollName = "";
            if (roleDTO != null) {
                rollName = isLanEng ? roleDTO.roleName : roleDTO.roleBn;
            }
            String errorTextEn = am_office_assignment_requestDTO.requesterNameEn + " is already applied for a office with the role " + rollName;
            String errorTextBn = am_office_assignment_requestDTO.requesterNameBn + " ইতিমধ্যে " + rollName + " এই রোল দিয়ে একটি অফিসের জন্য আবেদন করেছেন";
            throw new Exception(isLanEng ? errorTextEn : errorTextBn);
        }

        am_office_assignment_requestDTO.loggedInUserRole = userDTO.roleID;
        if (request.getParameter("assignmentDate") == null) {
            throw new Exception(isLanEng ? "Please provide allocation date" : "অনুগ্রহপূর্বক বরাদ্দ অনুরোধের তারিখ প্রদান করুন");
        }
        am_office_assignment_requestDTO.assignmentDate = f.parse(request.getParameter("assignmentDate")).getTime();

        if (request.getParameter("descriptiontext") == null || request.getParameter("descriptiontext").isEmpty()) {
            throw new Exception(isLanEng ? "Please provide description" : "অনুগ্রহপূর্বক আবেদনের বিবরণ প্রদান করুন");
        }
        am_office_assignment_requestDTO.descriptionText = Jsoup.clean(request.getParameter("descriptiontext"), Whitelist.simpleText());
        am_office_assignment_requestDTO.status = CommonApprovalStatus.PENDING.getValue();


        Value = request.getParameter("filesDropzone");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        System.out.println("filesDropzone = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {

            System.out.println("filesDropzone = " + Value);

            am_office_assignment_requestDTO.filesDropzone = Long.parseLong(Value);


            if (addFlag == false) {
                String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    System.out.println("going to delete " + deleteArray[i]);
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        } else {
            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
        }


        if (addFlag) {
            Am_office_assignment_requestDAO.getInstance().add(am_office_assignment_requestDTO);

            AmOfficeApprovalModel model =
                    new AmOfficeApprovalModel.AmOfficeApprovalModelBuilder()
                            .setAmOfficeAssignmentRequestDTO(am_office_assignment_requestDTO)
                            .setRequesterEmployeeRecordId(userDTO.employee_record_id)
                            .setTaskTypeId(TaskTypeEnum.AM_OFFICE_ASSIGNMENT.getValue())
                            .build();
            CardApprovalResponse cardApprovalResponse = Am_office_approval_mappingDAO.getInstance().createAmOfficeApproval(model);
            AmOfficeApprovalNotification.getInstance().sendWaitingForApprovalNotification(
                    cardApprovalResponse.organogramIds, am_office_assignment_requestDTO
            );

        } else {
            Am_office_assignment_requestDAO.getInstance().update(am_office_assignment_requestDTO);
        }

        return am_office_assignment_requestDTO;


    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.AM_OFFICE_ASSIGNMENT_REQUEST_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.AM_OFFICE_ASSIGNMENT_REQUEST_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.AM_OFFICE_ASSIGNMENT_REQUEST_SEARCH};
    }

    @Override
    public Class getClazz() {
        return Am_office_assignment_requestServlet.class;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String actionType = request.getParameter("actionType");

        if ("search".equals(actionType)) {

            String whereClause = " and requester_emp_id = " + HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.employee_record_id;
            //whereClause += " and status = "+CommonApprovalStatus.PENDING.getValue();

            Map<String, String> params = HttpRequestUtils.buildRequestParams(request);
            if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) != null) {
                Map<String, String> extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
                extraCriteriaMap.forEach(params::put);
            }
            RecordNavigator recordNavigator = getCommonDAOService().getRecordNavigator(params, null, whereClause, null);
            request.setAttribute(RECORD_NAVIGATOR, recordNavigator);
            String ajax = request.getParameter("ajax");
            String url;
            if (ajax != null && ajax.equalsIgnoreCase("true")) {
                url = commonPartOfDispatchURL() + "SearchForm.jsp";
            } else {
                url = "pb/search.jsp";
            }
            request.getRequestDispatcher(url).forward(request, response);
            return;

        }

        super.doGet(request, response);
    }

}

