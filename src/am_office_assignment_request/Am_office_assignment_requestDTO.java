package am_office_assignment_request;
import java.util.*; 
import util.*; 


public class Am_office_assignment_requestDTO extends CommonDTO
{

	public long assignmentDate = System.currentTimeMillis();
	public long decisionDate = System.currentTimeMillis();
    public String descriptionText = "";
	public int status = -1;
	public long filesDropzone = -1;
	public long requesterOrgId = -1;
	public long requesterOfficeId = -1;
	public long requesterOfficeUnitId = -1;
	public long requesterEmpId = -1;
    public String requesterPhoneNum = "";
    public String requesterNameEn = "";
    public String requesterNameBn = "";
    public String requesterOfficeNameEn = "";
    public String requesterOfficeNameBn = "";
    public String requesterOfficeUnitNameEn = "";
    public String requesterOfficeUnitNameBn = "";
    public String requesterOfficeUnitOrgNameEn = "";
    public String requesterOfficeUnitOrgNameBn = "";
	public long approverOrgId = -1;
	public long approverOfficeId = -1;
	public long approverOfficeUnitId = -1;
	public long approverEmpId = -1;
    public String approverPhoneNum = "";
    public String approverNameEn = "";
    public String approverNameBn = "";
    public String approverOfficeNameEn = "";
    public String approverOfficeNameBn = "";
    public String approverOfficeUnitNameEn = "";
    public String approverOfficeUnitNameBn = "";
    public String approverOfficeUnitOrgNameEn = "";
    public String approverOfficeUnitOrgNameBn = "";
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
    //public String searchColumn = "";
    public String comment  = "";

    public long withdrawalDate = -1;
    public String withdrawalReason  = "";

    public long withdrawerOrgId = -1;
    public long withdrawerOfficeId = -1;
    public long withdrawerOfficeUnitId = -1;
    public long withdrawerEmpId = -1;
    public String withdrawerPhoneNum = "";
    public String withdrawerNameEn = "";
    public String withdrawerNameBn = "";
    public String withdrawerOfficeNameEn = "";
    public String withdrawerOfficeNameBn = "";
    public String withdrawerOfficeUnitNameEn = "";
    public String withdrawerOfficeUnitNameBn = "";
    public String withdrawerOfficeUnitOrgNameEn = "";
    public String withdrawerOfficeUnitOrgNameBn = "";

    public long loggedInUserRole = -1;
	
    @Override
	public String toString() {
            return "$Am_office_assignment_requestDTO[" +
            " iD = " + iD +
            " assignmentDate = " + assignmentDate +
            " decisionDate = " + decisionDate +
            " descriptionText = " + descriptionText +
            " status = " + status +
            " filesDropzone = " + filesDropzone +
            " requesterOrgId = " + requesterOrgId +
            " requesterOfficeId = " + requesterOfficeId +
            " requesterOfficeUnitId = " + requesterOfficeUnitId +
            " requesterEmpId = " + requesterEmpId +
            " requesterPhoneNum = " + requesterPhoneNum +
            " requesterNameEn = " + requesterNameEn +
            " requesterNameBn = " + requesterNameBn +
            " requesterOfficeNameEn = " + requesterOfficeNameEn +
            " requesterOfficeNameBn = " + requesterOfficeNameBn +
            " requesterOfficeUnitNameEn = " + requesterOfficeUnitNameEn +
            " requesterOfficeUnitNameBn = " + requesterOfficeUnitNameBn +
            " requesterOfficeUnitOrgNameEn = " + requesterOfficeUnitOrgNameEn +
            " requesterOfficeUnitOrgNameBn = " + requesterOfficeUnitOrgNameBn +
            " approverOrgId = " + approverOrgId +
            " approverOfficeId = " + approverOfficeId +
            " approverOfficeUnitId = " + approverOfficeUnitId +
            " approverEmpId = " + approverEmpId +
            " approverPhoneNum = " + approverPhoneNum +
            " approverNameEn = " + approverNameEn +
            " approverNameBn = " + approverNameBn +
            " approverOfficeNameEn = " + approverOfficeNameEn +
            " approverOfficeNameBn = " + approverOfficeNameBn +
            " approverOfficeUnitNameEn = " + approverOfficeUnitNameEn +
            " approverOfficeUnitNameBn = " + approverOfficeUnitNameBn +
            " approverOfficeUnitOrgNameEn = " + approverOfficeUnitOrgNameEn +
            " approverOfficeUnitOrgNameBn = " + approverOfficeUnitOrgNameBn +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            " comment = " + comment +
            " withdrawalDate = " + withdrawalDate +
            " withdrawalReason = " + withdrawalReason +
            " withdrawerOrgId = " + withdrawerOrgId +
            " withdrawerOfficeId = " + withdrawerOfficeId +
            " withdrawerOfficeUnitId = " + withdrawerOfficeUnitId +
            " withdrawerEmpId = " + withdrawerEmpId +
            " withdrawerPhoneNum = " + withdrawerPhoneNum +
            " withdrawerNameEn = " + withdrawerNameEn +
            " withdrawerNameBn = " + withdrawerNameBn +
            " withdrawerOfficeNameEn = " + withdrawerOfficeNameEn +
            " withdrawerOfficeNameBn = " + withdrawerOfficeNameBn +
            " withdrawerOfficeUnitNameEn = " + withdrawerOfficeUnitNameEn +
            " withdrawerOfficeUnitNameBn = " + withdrawerOfficeUnitNameBn +
            " withdrawerOfficeUnitOrgNameEn = " + withdrawerOfficeUnitOrgNameEn +
            " withdrawerOfficeUnitOrgNameBn = " + withdrawerOfficeUnitOrgNameBn +
            "]";
    }

}