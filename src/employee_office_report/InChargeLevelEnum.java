package employee_office_report;

/*
 * @author Md. Erfan Hossain
 * @created 12/04/2021 - 12:15 AM
 * @project parliament
 */

import pb.OptionDTO;
import pb.Utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum InChargeLevelEnum {
    ROUTINE_RESPONSIBILITY("Routine Responsibility","রুটিন দায়িত্ব","Routine responsibility"),
    ADDITIONAL_RESPONSIBILITY("Additional Responsibility","অতিরিক্ত দায়িত্ব","Additional responsibility"),
    ATTACHMENT_RESPONSIBILITY("Attachment Responsibility","সংযুক্তি দায়িত্ব","Attachment responsibility");

    private final String engText;
    private final String bngText;
    private final String value;

    InChargeLevelEnum(String engText, String bngText,String value) {
        this.engText = engText;
        this.bngText = bngText;
        this.value = value;
    }

    public String getEngText() {
        return engText;
    }

    public String getBngText() {
        return bngText;
    }

    public String getValue() {
        return value;
    }

    private static String buildOptionsForEnglish;
    private static String buildOptionsForBangla;
    private static volatile Map<String,InChargeLevelEnum> mapEnumByValue = null;

    private static void loadMap(){
        if(mapEnumByValue == null){
            synchronized (InChargeLevelEnum.class){
                if(mapEnumByValue == null){
                    mapEnumByValue = Stream.of(InChargeLevelEnum.values())
                            .collect(Collectors.toMap(e->e.value,e->e));
                }
            }
        }
    }

    public static String getBuildOptions(String language){
        return getBuildOptionsWithSelectedItem(language,null);
    }

    public static String getBuildOptionsWithSelectedItem(String language, String selectedItem){
        if(buildOptionsForEnglish == null || buildOptionsForBangla == null){
            synchronized (InChargeLevelEnum.class){
                if(buildOptionsForEnglish == null || buildOptionsForBangla == null){
                    List<OptionDTO> list = Arrays.stream(InChargeLevelEnum.values())
                            .map(inChargeLevelEnum -> new OptionDTO(inChargeLevelEnum.engText,inChargeLevelEnum.bngText,inChargeLevelEnum.value))
                            .collect(Collectors.toList());
                    buildOptionsForEnglish = Utils.buildOptions(list,"English",selectedItem);
                    buildOptionsForBangla = Utils.buildOptions(list,"Bangla",selectedItem);
                }
            }
        }
        return "English".equalsIgnoreCase(language)?buildOptionsForEnglish:buildOptionsForBangla;
    }

    public static String getTextByValue(String language,String value){
        return getTextByValue("English".equalsIgnoreCase(language),value);
    }

    public static String getTextByValue(boolean isLangEng,String value){
        if(value == null){
            return "";
        }
        if(mapEnumByValue == null){
            loadMap();
        }
        return mapEnumByValue.get(value) == null? "" : isLangEng?mapEnumByValue.get(value).engText:mapEnumByValue.get(value).bngText;
    }

    public static InChargeLevelEnum getByValue(String value){
        if(value == null){
            return null;
        }
        if(mapEnumByValue == null){
            loadMap();
        }
        return mapEnumByValue.get(value);
    }
}