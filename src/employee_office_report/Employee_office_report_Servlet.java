package employee_office_report;


import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pbReport.PBReportUtils;
import pbReport.ReportCommonService;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"unused", "Duplicates"})
@WebServlet("/Employee_office_report_Servlet")
public class Employee_office_report_Servlet extends HttpServlet implements ReportCommonService {
    private static final Logger logger = Logger.getLogger(Employee_office_report_Servlet.class);

    private static final long serialVersionUID = 1L;

    private static final Set<String> searchParam = new HashSet<>(Arrays.asList(
            "age_end", "age_start", "empOfficerCat", "employmentCat", "employeeClass", "gender", "officeUnitIds", "incharge_label",
            "officeUnitOrganogramId", "employeeRecordId", "present_division", "permanent_division", "home_district", "nameEng", "nameBng"
    ));

    private final Map<String, String[]> stringMap = new HashMap<>();

    private static final String sql = " employee_offices eo inner join employee_records er on eo.employee_record_id = er.id "
                                      + "left join employee_seniority es on er.id = es.employee_record_id ";

    private String[][] Criteria;

    private String[][] Display;

    public Employee_office_report_Servlet() {
        stringMap.put("officeUnitIds", new String[]{"criteria", "eo", "office_unit_id", "IN", "AND", "String", "", "", "", "officeUnitIdList"
                , "officeUnitIds", String.valueOf(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITID), "office_units", null, "true", "1"});
        stringMap.put("officeUnitOrganogramId", new String[]{"criteria", "eo", "office_unit_organogram_id", "=", "AND", "long", "", "", "any", "officeUnitOrganogramId"
                , "officeUnitOrganogramId", String.valueOf(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITORGANOGRAMID), "office_unit_organogram", null, "true", "2"});
        stringMap.put("employeeRecordId", new String[]{"criteria", "eo", "employee_record_id", "=", "AND", "long", "", "", "any", "employeeRecordId"
                , "employeeRecordId", String.valueOf(LC.SERVICE_HISTORY_REPORT_WHERE_EMPLOYEERECORDSID), "employee_records_id", null, "true", "3"});
        stringMap.put("gender", new String[]{"criteria", "er", "gender_cat", "=", "AND", "long", "", "", "any",
                "gender", "gender", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_GENDER), "category", "gender", "true", "4"});
        stringMap.put("employeeClass", new String[]{"criteria", "er", "employee_class_cat", "=", "AND", "long", "", "", "any",
                "employeeClass", "employeeClass", String.valueOf(LC.EMPLOYEE_EMPLOYEE_CLASS), "category", "employee_class", "true", "5"});
        stringMap.put("employmentCat", new String[]{"criteria", "er", "employment_cat", "=", "AND", "long", "", "", "any", "employmentCat"
                , "employmentCat", String.valueOf(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY), "category", "employment", "true", "6"});
        stringMap.put("empOfficerCat", new String[]{"criteria", "er", "emp_officer_cat", "=", "AND", "long", "", "", "any", "empOfficerCat"
                , "empOfficerCat", String.valueOf(LC.EMPLOYEE_OFFICER_TYPE), "category", "emp_officer", "true", "7"});
        stringMap.put("present_division", new String[]{"criteria", "er", "present_division", "=", "AND", "long", "", "", "", "present_division",
                "present_division", String.valueOf(LC.EMPLOYEE_OFFICE_REPORT_PRESENT_DIVISION), "location", null, "true", "8"});
        stringMap.put("permanent_division", new String[]{"criteria", "er", "permanent_division", "=", "AND", "long", "", "", "", "permanent_division"
                , "permanent_division", String.valueOf(LC.EMPLOYEE_OFFICE_REPORT_PERMANENT_DIVISION), "location", null, "true", "9"});
        stringMap.put("home_district", new String[]{"criteria", "er", "home_district", "=", "AND", "long", "", "", "", "home_district"
                , "home_district", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT), "location", null, "true", "10"});
        stringMap.put("age_start", new String[]{"criteria", "er", "date_of_birth", "<=", "AND", "long", "", "", "", "ageStart",
                "age_start", String.valueOf(LC.HR_REPORT_AGE_RANGE_START), "number", null, "true", "11"});
        stringMap.put("age_end", new String[]{"criteria", "er", "date_of_birth", ">=", "AND", "long", "", "", "", "ageEnd"
                , "age_end", String.valueOf(LC.HR_REPORT_AGE_RANGE_TO), "number", null, "true", "12"});
        stringMap.put("nameEng", new String[]{"criteria", "er", "name_eng", "Like", "AND", "String", "", "", "any", "nameEng",
                "nameEng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEENG), "text", null, "true", "13"});
        stringMap.put("nameBng", new String[]{"criteria", "er", "name_bng", "Like", "AND", "String", "", "", "any", "nameBng",
                "nameBng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG), "text", null, "true", "14"});
        stringMap.put("office_id", new String[]{"criteria", "eo", "office_id", "=", "AND", "long", "", "", "2294", "office_id", null, null, null, null, null, null});
        stringMap.put("incharge_label", new String[]{"criteria", "eo", "incharge_label", "IN", "AND", "String", "", "", "any", "incharge_label", null, null, null, null, null, null});
        stringMap.put("status", new String[]{"criteria", "eo", "status", "=", "AND", "long", "", "", "1", "status", null, null, null, null, null, null});
        stringMap.put("isDeleted_1", new String[]{"criteria", "er", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted", null, null, null, null, null, null});
        stringMap.put("isDeleted", new String[]{"criteria", "eo", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted", null, null, null, null, null, null});
        stringMap.put("is_default_role", new String[]{"criteria", "eo", "is_default_role", "=", "AND", "long", "", "", "1", "is_default_role", null, null, null, null, null, null});
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        String language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
        boolean isLangEng = userDTO.languageID == CommonConstant.Language_ID_English;
        Set<String> inputs = PBReportUtils.prepareInputSets(searchParam, request);
        inputs.add("office_id");
        inputs.add("isDeleted_1");
        inputs.add("isDeleted");
        inputs.add("status");
        inputs.add("is_default_role");
        boolean showLastWorkingColumn = true;
        Display = new String[][]{
                {"display", "eo", "employee_record_id", "employee_records_id", LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_EMPLOYEERECORDID, loginDTO)},
                {"display", "eo", "employee_record_id", "user_name", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_USER_ID, loginDTO)},
                {"display", "er", "personal_mobile", "number", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_MOBILE_NUMBER, loginDTO)},
                {"display", "er", "personal_email", "plain", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_EMAIL, loginDTO)},
                {"display", "er", "home_district", "location", LM.getText(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT, loginDTO)},
                {"display", "eo", "employee_record_id", "id_to_present_address", LM.getText(LC.EMPLOYEE_RECORDS_ADD_PRESENTADDRESS, loginDTO)},
                {"display", "eo", "employee_record_id", "id_to_permanent_address", LM.getText(LC.EMPLOYEE_RECORDS_ADD_PERMANENTADDRESS, loginDTO)},
                {"display", "er", "date_of_birth", "complete_age", LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_AGE, loginDTO)},
                {"display", "er", "gender_cat", "category", LM.getText(LC.EMPLOYEE_RECORDS_ADD_GENDER, loginDTO)},
                {"display", "eo", "office_unit_organogram_id", "office_unit_organogram", LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO)},
                {"display", "eo", "office_unit_id", "office_unit", LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_OFFICEUNITID, loginDTO)},
                {"display", "er", "employee_class_cat", "category", LM.getText(LC.EMPLOYEE_EMPLOYEE_CLASS, loginDTO)},
                {"display", "er", "employment_cat", "category", LM.getText(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY, loginDTO)},
                {"display", "er", "emp_officer_cat", "category", LM.getText(LC.EMPLOYEE_OFFICER_TYPE, loginDTO)},
                {"display", "eo", "joining_date", "date", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_JOININGDATE, loginDTO)},
                {"display", "er", "lpr_date", "date", LM.getText(LC.EMPLOYEE_RECORDS_ADD_LPR_DATE, loginDTO)},
                {"display", "eo", "last_office_date", "date", isLangEng ? "Last office date" : "অফিসের শেষ তারিখ"},
                {"display", "eo", "incharge_label", "incharge_label", isLangEng ? "In charge Level" : "ইনচার্জ লেভেল"},
                {"display", "er", "present_division", "location", LM.getText(LC.EMPLOYEE_OFFICE_REPORT_PRESENT_DIVISION, loginDTO)},
                {"display", "er", "permanent_division", "location", LM.getText(LC.EMPLOYEE_OFFICE_REPORT_PERMANENT_DIVISION, loginDTO)}
        };
        Criteria = PBReportUtils.prepareCriteria(inputs, stringMap);

        if (inputs.contains("officeUnitIds") || inputs.contains("age_start") || inputs.contains("age_end")) {
            for (String[] arr : Criteria) {
                switch (arr[9]) {
                    case "officeUnitIdList":
                        arr[8] = getOfficeIdsFromOfficeUnitIds(request).stream()
                                                                       .map(String::valueOf)
                                                                       .collect(Collectors.joining(","));
                        break;
                    case "ageStart":
                        long ageStart = Long.parseLong(request.getParameter("age_start"));
                        arr[8] = String.valueOf(calculateLongDateValue(ageStart));
                        break;
                    case "ageEnd":
                        long ageEnd = Long.parseLong(request.getParameter("age_end"));
                        arr[8] = String.valueOf(calculateLongDateValue(ageEnd));
                        break;
                }
            }
        }
        request.setAttribute("dontUseOutDatedModal", true);
        if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_OFFICE_REPORT_DETAILS)) {
            reportGenerate(request, response);
        }
    }

    private long calculateLongDateValue(long ageYears) {
        LocalDate ld = LocalDate.now();
        ld = ld.minusYears(ageYears);
        return Date.from(ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()).getTime();
    }

    @Override
    public String[][] getCriteria() {
        return Criteria;
    }

    @Override
    public String[][] getDisplay() {
        return Display;
    }

    @Override
    public String getSQL() {
        return sql;
    }

    @Override
    public int getLCForReportName() {
        return LC.EMPLOYEE_OFFICE_REPORT_OTHER_EMPLOYEE_OFFICE_REPORT;
    }

    @Override
    public String getFileName() {
        return "employee_office_report";
    }

    @Override
    public String getTableName() {
        return "employee_office_report";
    }

    @Override
    public String getOrderBy(HttpServletRequest request) {
        String orderBy = ReportCommonService.super.getOrderBy(request);
        if (orderBy == null || orderBy.isEmpty()) {
            orderBy = " es.serial ";
        } else {
            orderBy += ",es.serial ";
        }
        return orderBy;
    }
}