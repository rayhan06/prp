package OfficeUnitsEdms;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.RepositoryManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class OfficeUnitsEdmsDAO {

    private static final Logger logger = Logger.getLogger(OfficeUnitsEdmsDAO.class);

    private static final String getByTrue = "SELECT * FROM office_units WHERE isDeleted =  0 order by office_units.lastModificationTime desc";
    private static final String getByFalse = "SELECT * FROM office_units WHERE lastModificationTime >=  %d order by office_units.lastModificationTime desc";

    private OfficeUnitsEdmsDTO buildOfficeUnitsEdmsDTO(ResultSet rs) {
        try {
            OfficeUnitsEdmsDTO officeUnitsEdmsDTO = new OfficeUnitsEdmsDTO();
            officeUnitsEdmsDTO.iD = rs.getLong("ID");
            officeUnitsEdmsDTO.officeMinistryId = rs.getInt("office_ministry_id");
            officeUnitsEdmsDTO.officeLayerId = rs.getInt("office_layer_id");
            officeUnitsEdmsDTO.officeId = rs.getInt("office_id");
            officeUnitsEdmsDTO.officeOriginUnitId = rs.getInt("office_origin_unit_id");
            officeUnitsEdmsDTO.unitNameBng = rs.getString("unit_name_bng");
            officeUnitsEdmsDTO.unitNameEng = rs.getString("unit_name_eng");
            officeUnitsEdmsDTO.officeUnitCategory = rs.getString("office_unit_category");
            officeUnitsEdmsDTO.parentUnitId = rs.getLong("parent_unit_id");
            officeUnitsEdmsDTO.parentOriginUnitId = rs.getInt("parent_origin_unit_id");
            officeUnitsEdmsDTO.unitNothiCode = rs.getString("unit_nothi_code");
            officeUnitsEdmsDTO.unitLevel = rs.getInt("unit_level");
            officeUnitsEdmsDTO.sarokNoStart = rs.getInt("sarok_no_start");
            officeUnitsEdmsDTO.email = rs.getString("email");
            officeUnitsEdmsDTO.phone = rs.getString("phone");
            officeUnitsEdmsDTO.fax = rs.getString("fax");
            officeUnitsEdmsDTO.activeStatus = rs.getBoolean("active_status");
            officeUnitsEdmsDTO.modifiedBy = rs.getInt("modified_by");
            officeUnitsEdmsDTO.created = rs.getLong("created");
            officeUnitsEdmsDTO.modified = rs.getLong("modified");
            officeUnitsEdmsDTO.status = rs.getBoolean("status");
            officeUnitsEdmsDTO.isDeleted = rs.getInt("isDeleted");
            officeUnitsEdmsDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return officeUnitsEdmsDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public List<OfficeUnitsEdmsDTO> getAll(Boolean isFirstReload) {
        if (isFirstReload) {
            return ConnectionAndStatementUtil.getListOfT(getByTrue, this::buildOfficeUnitsEdmsDTO);
        } else {
            String sql = String.format(getByFalse, RepositoryManager.lastModifyTime);
            return ConnectionAndStatementUtil.getListOfT(sql, this::buildOfficeUnitsEdmsDTO);
        }
    }
}
