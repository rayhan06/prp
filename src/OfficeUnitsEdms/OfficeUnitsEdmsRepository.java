package OfficeUnitsEdms;

import common.NameDTO;
import repository.Repository;
import repository.RepositoryManager;

import java.util.List;

public class OfficeUnitsEdmsRepository implements Repository {
    private List<OfficeUnitsEdmsDTO> officeUnitsEdmsDTOList;
    private final OfficeUnitsEdmsDAO officeUnitsEdmsDAO;

    private OfficeUnitsEdmsRepository(){
        officeUnitsEdmsDAO = new OfficeUnitsEdmsDAO();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader{
        static OfficeUnitsEdmsRepository INSTANCE = new OfficeUnitsEdmsRepository();
    }

    public static OfficeUnitsEdmsRepository getInstance(){
        return LazyLoader.INSTANCE;
    }

    /*
    * reload method's implementation is incomplete.
    * only implements for reloadAll = true.
    * There has no option to add, update or delete from ui.
    * So, there has no probability call this method by reloadAll value false.
    * */
    @Override
    public void reload(boolean reloadAll) {
        officeUnitsEdmsDTOList = officeUnitsEdmsDAO.getAll(reloadAll);
    }

    @Override
    public String getTableName() {
        return "office_units";
    }

    public List<OfficeUnitsEdmsDTO> getAll(){
        return officeUnitsEdmsDTOList;
    }

    public String buildOptions(String language) {
        boolean isLanguageEng = language.equals("English");
        StringBuilder options = new StringBuilder();
        options.append("<option value = '-1'>");
        if (language.equals("English")) {
            options.append("Select</option>");
        } else {
            options.append("বাছাই করুন</option>");
        }
        if (officeUnitsEdmsDTOList!=null && officeUnitsEdmsDTOList.size() > 0) {
            for (OfficeUnitsEdmsDTO dto : officeUnitsEdmsDTOList) {
                options.append("<option value = '")
                       .append(dto.iD)
                       .append("'>")
                       .append(isLanguageEng ? dto.unitNameEng : dto.unitNameBng)
                       .append("</option>");
            }
        }
        return options.toString();
    }
}
