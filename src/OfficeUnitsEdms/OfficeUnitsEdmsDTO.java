package OfficeUnitsEdms;

import util.CommonDTO;

public class OfficeUnitsEdmsDTO extends CommonDTO {

    public int officeMinistryId = 0;
    public int officeLayerId = 0;
    public int officeId = 0;
    public int officeOriginUnitId = 0;
    public String unitNameBng = "";
    public String unitNameEng = "";
    public String officeUnitCategory = "";
    public long parentUnitId = 0;
    public int parentOriginUnitId = 0;
    public String unitNothiCode = "";
    public int unitLevel = 0;
    public int sarokNoStart = 0;
    public String email = "";
    public String phone = "";
    public String fax = "";
    public boolean activeStatus = false;
    public int createdBy = 0;
    public int modifiedBy = 0;
    public long created = 0;
    public long modified = 0;
    public boolean status = false;
}
