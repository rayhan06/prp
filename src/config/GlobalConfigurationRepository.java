package config;

import org.apache.log4j.Logger;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class GlobalConfigurationRepository{
    private static final Logger logger = Logger.getLogger(GlobalConfigurationRepository.class);
    private static final GlobalConfigDAO globalConfigDAO = new GlobalConfigDAO();
    private static Map<Integer, GlobalConfigDTO> globalConfigDTOByID = new ConcurrentHashMap<>();
    private static Map<Integer, Map<Integer, GlobalConfigDTO>> globalConfigDTOByGroupID = new ConcurrentHashMap<>();
    private List<GlobalConfigDTO> globalConfigDTOList;

    private GlobalConfigurationRepository(){
        logger.debug("GlobalConfigurationRepository load start");
        reload();
        logger.debug("GlobalConfigurationRepository load end");
    }

    private static class LazyLoader {
        static GlobalConfigurationRepository INSTANCE = new GlobalConfigurationRepository();
    }

    public static GlobalConfigurationRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public List<GlobalConfigDTO> getConfigsByGroupID(int groupID) {
        Map<Integer, GlobalConfigDTO> map = globalConfigDTOByGroupID.get(groupID);
        return map == null ? new ArrayList<>() : new ArrayList<>(map.values());
    }

    public List<GlobalConfigDTO> getAllConfigs() {
        return globalConfigDTOList;
    }

    public static GlobalConfigDTO getGlobalConfigDTOByID(int configID) {
        GlobalConfigDTO dto = globalConfigDTOByID.get(configID);
        return dto == null ? new GlobalConfigDTO() : dto;
    }

    public void reload() {
        logger.debug("GlobalConfigurationRepository reload start");
        globalConfigDTOList = globalConfigDAO.getAllDTOs();
        globalConfigDTOByID = globalConfigDTOList.stream()
                .collect(Collectors.toMap(dto->dto.ID,dto->dto));
        globalConfigDTOByGroupID = globalConfigDTOList.stream()
                .collect(Collectors.collectingAndThen(Collectors.groupingBy(dto->dto.groupID),tempMap->{
                    Map<Integer, Map<Integer, GlobalConfigDTO>> result = new HashMap<>();
                    tempMap.forEach((key,val)-> result.put(key,val.stream().collect(Collectors.toMap(e->e.ID, e->e))));
                    return result;
                }));
        logger.debug("GlobalConfigurationRepository reload end");
    }
}
