package config;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Kayesh Parvez
 */
public class GlobalConfigDAO {
    private static final Logger logger = Logger.getLogger(GlobalConfigDAO.class);
    private static final String updateQuery = "UPDATE global_config SET value=? WHERE ID = ?";
    private static final String getAll = "select * from global_config";

    public void updateGlobalConfiguration(ArrayList<GlobalConfigDTO> globalConfigurationDTOList) {
        if(globalConfigurationDTOList!=null && globalConfigurationDTOList.size()>0){
            ConnectionAndStatementUtil.getWritePrepareStatement(ps->{
                try {
                    globalConfigurationDTOList.forEach(dto->{
                        int index = 0;
                        try {
                            ps.setObject(++index, dto.value);
                            ps.setObject(++index, dto.ID);
                            ps.addBatch();
                        } catch (SQLException ex) {
                            logger.error(ex);
                        }
                    });
                    ps.executeBatch();
                    GlobalConfigurationRepository.getInstance().reload();
                } catch (SQLException ex) {
                    logger.error(ex);
                }
            },updateQuery);
        }

        /*Connection connection = null;
        PreparedStatement ps = null;

        try {
            connection = DBMW.getInstance().getConnection();

            String sql = "UPDATE global_config SET value=? WHERE ID = ?";

            ps = connection.prepareStatement(sql);

            for (GlobalConfigDTO globalConfigDTO : globalConfigurationDTOList) {
                int index = 1;

                ps.setObject(index++, globalConfigDTO.value);
                ps.setObject(index++, globalConfigDTO.ID);

                ps.addBatch();
            }

            ps.executeBatch();

            GlobalConfigurationRepository.getInstance().reload(true);

        } catch (Exception ex) {
            logger.fatal("", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }*/
    }


    public List<GlobalConfigDTO> getAllDTOs(){
        return ConnectionAndStatementUtil.getListOfT(getAll,this::buildGlobalConfigDTO);
    }

    private GlobalConfigDTO buildGlobalConfigDTO(ResultSet resultSet){
        try{
            GlobalConfigDTO globalConfigDTO = new GlobalConfigDTO();
            globalConfigDTO.ID = resultSet.getInt("ID");
            globalConfigDTO.name = resultSet.getString("name");
            globalConfigDTO.value = resultSet.getString("value");
            globalConfigDTO.comments = resultSet.getString("comments");
            globalConfigDTO.groupID = resultSet.getInt("groupID");
            return globalConfigDTO;
        }catch (SQLException ex){
            logger.error(ex);
            return null;
        }
    }
}
