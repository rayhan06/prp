package config;

import employee_records.Constants;
import login.LoginDTO;
import org.apache.log4j.Logger;
import permission.MenuConstants;
import repository.Repository;
import repository.RepositoryManager;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.JSPConstant;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Kayesh Parvez
 */
@WebServlet("/GlobalConfigurationServlet")
public class GlobalConfigurationServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    Logger logger = Logger.getLogger(GlobalConfigurationServlet.class);

    public GlobalConfigurationServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");

            if ("getEditPage".equals(actionType)) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GLOBAL_SETTINGS_VIEW)) {
                    getEditPage(request, response);
                } else {
                    request.getRequestDispatcher(JSPConstant.ERROR_GLOBAL).forward(request, response);
                }
            }
        } catch (Exception ex) {
            logger.fatal("", ex);
        }
    }

    private void getEditPage(HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.getRequestDispatcher("configuration/config.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "edit":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GLOBAL_SETTINGS_UPDATE)) {
                        updateGlobalConfiguration(request, response);
                    } else {
                        request.getRequestDispatcher(JSPConstant.ERROR_GLOBAL).forward(request, response);
                    }
                    break;
                case "search":
                    searchGlobalSettings(request, response);
                    break;
                case "reloadTable":
                	if(userDTO.userName.equals("superadmin")/*userDTO.roleID == SessionConstants.ADMIN_ROLE*/)
                	{
	                	String tableName = request.getParameter("tableName");
	                	Repository repository = RepositoryManager.getInstance().getRepository(tableName);
	                	String message = "Failed to reload " + tableName;
	                	if(repository != null)
	                	{
	                		repository.reload(true);
	                		message = "Succeeded to reload " + tableName;
	                		logger.debug("Reloaded successfully " + tableName);
	                	}
	                	else
	                	{
	                		logger.debug("Reloaded unsuccessfully " + tableName);
	                	}
	                	request.setAttribute("message", message);
	                	request.getRequestDispatcher("configuration/config.jsp").forward(request, response);
                	}
                	else 
                	{
                        request.getRequestDispatcher(JSPConstant.ERROR_GLOBAL).forward(request, response);
                    }
                	break;
                case "getEditPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GLOBAL_SETTINGS_VIEW)) {
                        getEditPage(request, response);
                    } else {
                        request.getRequestDispatcher(JSPConstant.ERROR_GLOBAL).forward(request, response);
                    }
                    break;
            }
        } catch (Exception ex) {
            logger.fatal("", ex);
        }
    }

    private void searchGlobalSettings(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int groupID = Integer.parseInt(request.getParameter("groupID"));
        List<GlobalConfigDTO> list = GlobalConfigurationRepository.getInstance().getConfigsByGroupID(groupID);
        request.setAttribute("list", list);
        request.getRequestDispatcher("GlobalConfigurationServlet?actionType=getEditPage").forward(request, response);
    }

    private void updateGlobalConfiguration(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String[] IDs = request.getParameterValues("ID");
        String[] groupIDs = request.getParameterValues("groupID");
        String[] values = request.getParameterValues("value");
        ArrayList<GlobalConfigDTO> globalConfigurationDTOList = new ArrayList<>();
        for (int i = 0; i < IDs.length; i++) {
            try {
                GlobalConfigDTO globalConfigDTO = new GlobalConfigDTO();
                globalConfigDTO.ID = Integer.parseInt(IDs[i]);
                globalConfigDTO.value = values[i];
                globalConfigDTO.groupID = Integer.parseInt(groupIDs[i]);
                globalConfigurationDTOList.add(globalConfigDTO);
            } catch (Exception ex) {
                logger.fatal("", ex);
            }
        }
        GlobalConfigDAO globalConfigDAO = new GlobalConfigDAO();
        globalConfigDAO.updateGlobalConfiguration(globalConfigurationDTOList);
        response.sendRedirect("GlobalConfigurationServlet?actionType=getEditPage");
    }
}
