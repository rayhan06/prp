package new_user_task;

public class CommentModel {
    public long recordId;
    public String comment;

    @Override
    public String toString() {
        return "CommentModel{" +
                "recordId=" + recordId +
                ", comment='" + comment + '\'' +
                '}';
    }
}
