package new_user_task;

import common.BaseServlet;
import common.CommonDAOService;
import common.EmployeeServletService;
import common.RoleEnum;
import employee_records.EmpOfficeModel;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@WebServlet("/New_user_taskServlet")
@MultipartConfig
public class New_user_taskServlet extends BaseServlet implements EmployeeServletService {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(New_user_taskServlet.class);
    private static final String tableName = "new_user_task";
    private final New_user_taskDAO new_user_taskDAO = New_user_taskDAO.getInstance();

    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    public String getServletName() {
        return "New_user_taskServlet";
    }

    @Override
    public New_user_taskDAO getCommonDAOService() {
        return new_user_taskDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        request.setAttribute("failureMessage", "");
        long currentTime = System.currentTimeMillis();
        long employeeRecordsId;
        String Value;
        Value = Jsoup.clean(request.getParameter("userName"), Whitelist.simpleText());

        employeeRecordsId = UserRepository.getUserDTOByUserName(Value).employee_record_id;

        Value = Jsoup.clean(request.getParameter("notified"), Whitelist.simpleText());

        EmpOfficeModel[] notifiedList = gson.fromJson(Value, EmpOfficeModel[].class);
        Value = Jsoup.clean(request.getParameter("addedComments"), Whitelist.simpleText());

        List<CommentModel> commentList = Arrays.asList(gson.fromJson(Value, CommentModel[].class));
        Map<Long, String> commentMap = commentList.stream()
                .collect(Collectors.toMap(e -> e.recordId, e -> e.comment));
        for (EmpOfficeModel e : notifiedList) {
            New_user_taskDTO new_user_taskDTO = new New_user_taskDTO();
            new_user_taskDTO.employeeRecordsId = employeeRecordsId;
            new_user_taskDTO.notifiedRecordsId = e.employeeRecordId;
            new_user_taskDTO.comment = commentMap.get(e.employeeRecordId);
            new_user_taskDTO.insertionTime = currentTime;
            new_user_taskDTO.insertedBy = userDTO.ID;
            new_user_taskDTO.lastModificationTime = currentTime;
            new_user_taskDTO.modifiedBy = userDTO.ID;
            new_user_taskDTO.statusCat = TaskStatusEnum.NOT_DONE.getValue();
            new_user_taskDAO.add(new_user_taskDTO);
            NewUserTaskNotification.getInstance().sendUserTaskNotification(e.organogramId, new_user_taskDTO);
        }
        return null;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return New_user_taskServlet.class;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            switch (request.getParameter("actionType")) {
                case "search": {
                    boolean isAdmin = userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId();
                    if (!isAdmin) {
                        Map<String, String> extraCriteriaMap;
                        if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) == null) {
                            extraCriteriaMap = new HashMap<>();
                        } else {
                            extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
                        }
                        extraCriteriaMap.put("notified_records_id_internal", String.valueOf(userDTO.employee_record_id));
                        request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                    }
                    super.doGet(request, response);
                    return;
                }
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);


    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            logger.debug("actionType = " + actionType);
            if ("updateTaskStatus".equals(actionType)) {
                long newUserTaskId = Long.parseLong(request.getParameter("iD"));
                New_user_taskDTO new_user_taskDTO = new_user_taskDAO.getDTOByID(newUserTaskId);
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.NEW_USER_TASK_ADD) || userDTO.employee_record_id == new_user_taskDTO.notifiedRecordsId) {
                    new_user_taskDTO.statusCat = TaskStatusEnum.DONE.getValue();
                    new_user_taskDTO.lastModificationTime = System.currentTimeMillis();
                    new_user_taskDTO.modifiedBy = userDTO.ID;
                    new_user_taskDAO.update(new_user_taskDTO);
                    response.sendRedirect("New_user_taskServlet?actionType=view&ID=" + newUserTaskId);
                    return;
                }
                response.sendRedirect("New_user_taskServlet?actionType=search");
                return;
            }
            super.doPost(request, response);
            return;
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }


}