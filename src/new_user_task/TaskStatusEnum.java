package new_user_task;


import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum TaskStatusEnum {
    DONE(1, "green"),
    NOT_DONE(2, "crimson");

    private final int value;
    private final String backgroundColor;
    private static Map<Integer, TaskStatusEnum> mapByValue = null;

    TaskStatusEnum(int value, String backgroundColor) {
        this.value = value;
        this.backgroundColor = backgroundColor;
    }

    public int getValue() {
        return value;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    private static synchronized void reloadMap() {
        mapByValue = Stream.of(TaskStatusEnum.values())
                .collect(Collectors.toMap(e -> e.value, e -> e));
    }

    public static String getColor(int value) {
        if(mapByValue == null){
            synchronized (TaskStatusEnum.class){
                if(mapByValue == null){
                    reloadMap();
                }
            }
        }
        return mapByValue.get(value) == null ? "red" : mapByValue.get(value).backgroundColor;
    }
}