package new_user_task;

import util.CommonEmployeeDTO;


public class New_user_taskDTO extends CommonEmployeeDTO {

    public long notifiedRecordsId = -1;
    public String comment = "";
    public int statusCat = TaskStatusEnum.NOT_DONE.getValue();
    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;


    @Override
    public String toString() {
        return "$New_user_taskDTO[" +
                " iD = " + iD +
                " employeeRecordsId = " + employeeRecordsId +
                " notifiedRecordsId = " + notifiedRecordsId +
                " comment = " + comment +
                " status = " + statusCat +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " isDeleted = " + isDeleted +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}