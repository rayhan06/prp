package new_user_task;

import common.EmployeeCommonDAOService;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"unused"})
public class New_user_taskDAO implements EmployeeCommonDAOService<New_user_taskDTO> {

    private static final Logger logger = Logger.getLogger(New_user_taskDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (employee_records_id,notified_records_id,comment,status_cat,"
            .concat("modified_by,lastModificationTime,search_column,insertion_time,inserted_by,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery = "UPDATE {tableName} SET employee_records_id=?,notified_records_id=?,comment=?,status_cat=?,"
            .concat("modified_by=?,lastModificationTime=?,search_column=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private static final String tableName = "new_user_task";

    private static class LazyLoader {
        static final New_user_taskDAO INSTANCE = new New_user_taskDAO();
    }

    public static New_user_taskDAO getInstance() {
        return New_user_taskDAO.LazyLoader.INSTANCE;
    }

    private New_user_taskDAO() {
        searchMap.put("employee_records_id", "and (employee_records_id = ?)");
        searchMap.put("notified_records_id", "and (notified_records_id = ?)");
        searchMap.put("status_cat", "and (status_cat = ?)");
        searchMap.put("notified_records_id_internal", "and (notified_records_id = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    public void setSearchColumn(New_user_taskDTO new_user_taskDTO) {
        new_user_taskDTO.searchColumn = "";
        Employee_recordsDTO employeeDT0 = Employee_recordsRepository.getInstance().getById(new_user_taskDTO.employeeRecordsId);
        Employee_recordsDTO notifiedDT0 = Employee_recordsRepository.getInstance().getById(new_user_taskDTO.notifiedRecordsId);
        new_user_taskDTO.searchColumn += employeeDT0.nameEng + " ";
        new_user_taskDTO.searchColumn += employeeDT0.nameBng + " ";
        new_user_taskDTO.searchColumn += notifiedDT0.nameEng + " ";
        new_user_taskDTO.searchColumn += notifiedDT0.nameBng + " ";
    }

    @Override
    public void set(PreparedStatement ps, New_user_taskDTO new_user_taskDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(new_user_taskDTO);
        ps.setObject(++index, new_user_taskDTO.employeeRecordsId);
        ps.setObject(++index, new_user_taskDTO.notifiedRecordsId);
        ps.setObject(++index, new_user_taskDTO.comment);
        ps.setObject(++index, new_user_taskDTO.statusCat);
        ps.setObject(++index, new_user_taskDTO.insertionTime);
        ps.setObject(++index, new_user_taskDTO.lastModificationTime);
        ps.setObject(++index, new_user_taskDTO.searchColumn);
        if (isInsert) {
            ps.setObject(++index, new_user_taskDTO.insertedBy);
            ps.setObject(++index, new_user_taskDTO.modifiedBy);
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, new_user_taskDTO.iD);
    }

    @Override
    public New_user_taskDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            New_user_taskDTO new_user_taskDTO = new New_user_taskDTO();
            new_user_taskDTO.iD = rs.getLong("ID");
            new_user_taskDTO.employeeRecordsId = rs.getLong("employee_records_id");
            new_user_taskDTO.notifiedRecordsId = rs.getLong("notified_records_id");
            new_user_taskDTO.comment = rs.getString("comment");
            new_user_taskDTO.statusCat = rs.getInt("status_cat");
            new_user_taskDTO.insertedBy = rs.getLong("inserted_by");
            new_user_taskDTO.insertionTime = rs.getLong("insertion_time");
            new_user_taskDTO.isDeleted = rs.getInt("isDeleted");
            new_user_taskDTO.modifiedBy = rs.getLong("modified_by");
            new_user_taskDTO.lastModificationTime = rs.getLong("lastModificationTime");
            new_user_taskDTO.searchColumn = rs.getString("search_column");
            return new_user_taskDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((New_user_taskDTO) commonDTO, updateQuery, false);
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((New_user_taskDTO) commonDTO, addQuery, true);
    }

    public New_user_taskDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }


}
	