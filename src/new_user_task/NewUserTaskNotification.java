package new_user_task;

import employee_office_report.InChargeLevelEnum;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import pb_notifications.Pb_notificationsDAO;

import java.util.List;

public class NewUserTaskNotification {
    private static final Logger logger = Logger.getLogger(NewUserTaskNotification.class);

    private final Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();

    private NewUserTaskNotification(){}

    private static class NewUserTaskNotificationNewLazyLoader{
        static final NewUserTaskNotification INSTANCE = new NewUserTaskNotification();
    }

    public static NewUserTaskNotification getInstance(){
        return NewUserTaskNotificationNewLazyLoader.INSTANCE;
    }

    public void sendUserTaskNotification(long organogramId,New_user_taskDTO new_user_taskDTO){
        Employee_recordsDTO newUserRecordDTO= Employee_recordsRepository.getInstance().getById(new_user_taskDTO.employeeRecordsId);
        if(newUserRecordDTO == null){
            return;
        }
        List<EmployeeOfficeDTO> officeDTOList = EmployeeOfficeRepository.getInstance().getByEmployeeRecordId(new_user_taskDTO.employeeRecordsId);
        String ofcDesignationEng = "",ofcDesignationBng = "";
        if(officeDTOList.size() == 0){
            logger.debug("Office is not found");
            EmployeeOfficeDTO employeeOfficeDTO = officeDTOList.stream()
                    .filter(dto->InChargeLevelEnum.ROUTINE_RESPONSIBILITY.getEngText().equals(dto.inchargeLabel))
                    .findAny()
                    .orElse(null);
            if(employeeOfficeDTO == null){
                employeeOfficeDTO = officeDTOList.get(0);
            }
            Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
            if(officeUnitsDTO!=null){
                OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
                if(officeUnitOrganograms!=null){
                    ofcDesignationEng = " ("+officeUnitOrganograms.designation_eng+","+officeUnitOrganograms.designation_bng+")";
                    ofcDesignationBng = " ("+officeUnitOrganograms.designation_bng+","+officeUnitOrganograms.designation_bng+")";
                }
            }
        }
        String notificationEngText="A new task is assigned for "+newUserRecordDTO.nameEng+ofcDesignationEng+ ". Task is : "+new_user_taskDTO.comment;
        String notificationBngText="আপনাকে একটি দায়িত্ব অর্প্ন করা হয়েছে "+newUserRecordDTO.nameBng +ofcDesignationBng+" জন্য। দায়িত্বটি হলো : "+new_user_taskDTO.comment;
        String notificationMessage = notificationEngText +"$"+notificationBngText;
        String url = "New_user_taskServlet?actionType=view&ID="+new_user_taskDTO.iD;
        sendNotification(organogramId,notificationMessage,url);
    }

    private void sendNotification(long organogramId, String notificationMessage, String url){
        Thread thread = new Thread(()->{
            long currentTime = System.currentTimeMillis();
            pb_notificationsDAO.addPb_notifications(organogramId, currentTime, url, notificationMessage);
        });
        thread.setDaemon(true);
        thread.start();
    }
}