package lab_test;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.*;
import user.UserDTO;

public class LabTestListDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public LabTestListDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new LabTestListMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"lab_test_id",
			"ordering",
			"name_en",
			"name_bn",
			"normal_range",
			"is_title",
			"unit",
			"show_in_dropdown",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public LabTestListDAO()
	{
		this("lab_test_list");		
	}
	
	public void setSearchColumn(LabTestListDTO labtestlistDTO)
	{
		labtestlistDTO.searchColumn = "";
		labtestlistDTO.searchColumn += labtestlistDTO.nameEn + " ";
		labtestlistDTO.searchColumn += labtestlistDTO.nameBn + " ";
		labtestlistDTO.searchColumn += labtestlistDTO.normalRange + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		LabTestListDTO labtestlistDTO = (LabTestListDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(labtestlistDTO);
		if(isInsert)
		{
			ps.setObject(index++,labtestlistDTO.iD);
		}
		ps.setObject(index++,labtestlistDTO.labTestId);
		ps.setObject(index++,labtestlistDTO.ordering);
		ps.setObject(index++,labtestlistDTO.nameEn);
		ps.setObject(index++,labtestlistDTO.nameBn);
		ps.setObject(index++,labtestlistDTO.normalRange);
		ps.setObject(index++,labtestlistDTO.isTitle);
		ps.setObject(index++,labtestlistDTO.unit);
		ps.setObject(index++,labtestlistDTO.showInDropdown);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public LabTestListDTO build(ResultSet rs)
	{
		try
		{
			LabTestListDTO labtestlistDTO = new LabTestListDTO();
			labtestlistDTO.iD = rs.getLong("ID");
			labtestlistDTO.labTestId = rs.getLong("lab_test_id");
			labtestlistDTO.ordering = rs.getInt("ordering");
			labtestlistDTO.nameEn = rs.getString("name_en");
			labtestlistDTO.nameBn = rs.getString("name_bn");
			labtestlistDTO.normalRange = rs.getString("normal_range");
			labtestlistDTO.isTitle = rs.getBoolean("is_title");
			labtestlistDTO.unit = rs.getString("unit");
			labtestlistDTO.showInDropdown = rs.getBoolean("show_in_dropdown");
			if(labtestlistDTO.unit == null)
			{
				labtestlistDTO.unit = "";
			}
			labtestlistDTO.isDeleted = rs.getInt("isDeleted");
			labtestlistDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return labtestlistDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	
	public List<LabTestListDTO> getLabTestListDTOListByLabTestID(long labTestID) throws Exception
	{
		String sql = "SELECT * FROM lab_test_list where isDeleted=0 and lab_test_id="+labTestID+" order by lab_test_list.ordering asc";
		
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}

	public LabTestListDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		LabTestListDTO labtestlistDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return labtestlistDTO;
	}

	
	public List<LabTestListDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<LabTestListDTO> getAllLabTestList (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<LabTestListDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<LabTestListDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
				
}
	