package lab_test;

import java.io.IOException;
import java.io.*;



import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;


import com.google.gson.Gson;


import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Lab_testServlet
 */
@WebServlet("/Lab_testServlet")
@MultipartConfig
public class Lab_testServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Lab_testServlet.class);

    String tableName = "lab_test";

	Lab_testDAO lab_testDAO;
	CommonRequestHandler commonRequestHandler;
	LabTestListDAO labTestListDAO;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Lab_testServlet() 
	{
        super();
    	try
    	{
			lab_testDAO = new Lab_testDAO(tableName);
			labTestListDAO = new LabTestListDAO("lab_test_list");
			commonRequestHandler = new CommonRequestHandler(lab_testDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LAB_TEST_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LAB_TEST_UPDATE))
				{
					getLab_test(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LAB_TEST_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchLab_test(request, response, isPermanentTable, filter);
						}
						else
						{
							searchLab_test(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchLab_test(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LAB_TEST_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LAB_TEST_ADD))
				{
					System.out.println("going to  addLab_test ");
					addLab_test(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addLab_test ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LAB_TEST_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addLab_test ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LAB_TEST_UPDATE))
				{					
					addLab_test(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LAB_TEST_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LAB_TEST_SEARCH))
				{
					searchLab_test(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Lab_testDTO lab_testDTO = lab_testDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(lab_testDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addLab_test(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addLab_test");

			Lab_testDTO lab_testDTO;

						
			if(addFlag == true)
			{
				lab_testDTO = new Lab_testDTO();
			}
			else
			{
				lab_testDTO = lab_testDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("nameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEn = " + Value);
			if(Value != null)
			{
				lab_testDTO.nameEn = (Value);
				lab_testDTO.nameBn = lab_testDTO.nameEn;
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}


			Value = request.getParameter("normalRangeColumnName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("normalRangeColumnName = " + Value);
			if(Value != null)
			{
				lab_testDTO.normalRangeColumnName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("description");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("description = " + Value);
			if(Value != null)
			{
				lab_testDTO.description = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("ordering");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("ordering = " + Value);
			if(Value != null)
			{
				lab_testDTO.ordering = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("showInDropdown");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("showInDropdown = " + Value);
            lab_testDTO.showInDropdown = Value != null;

			Value = request.getParameter("specimen");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("specimen = " + Value);
			if(Value != null)
			{
				lab_testDTO.specimen = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				lab_testDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addLab_test dto = " + lab_testDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				lab_testDAO.setIsDeleted(lab_testDTO.iD, CommonDTO.OUTDATED);
				returnedID = lab_testDAO.add(lab_testDTO);
				lab_testDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = lab_testDAO.manageWriteOperations(lab_testDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = lab_testDAO.manageWriteOperations(lab_testDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			List<LabTestListDTO> labTestListDTOList = createLabTestListDTOListByRequest(request);			
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(labTestListDTOList != null)
				{				
					for(LabTestListDTO labTestListDTO: labTestListDTOList)
					{
						labTestListDTO.labTestId = lab_testDTO.iD; 
						labTestListDAO.add(labTestListDTO);
					}
				}
			
			}
			else
			{
				List<Long> childIdsFromRequest = labTestListDAO.getChildIdsFromRequest(request, "labTestList");
				//delete the removed children
				labTestListDAO.deleteChildrenNotInList("lab_test", "lab_test_list", lab_testDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = labTestListDAO.getChilIds("lab_test", "lab_test_list", lab_testDTO.iD);
				
				
				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							LabTestListDTO labTestListDTO =  createLabTestListDTOByRequestAndIndex(request, false, i);
							labTestListDTO.labTestId = lab_testDTO.iD; 
							labTestListDAO.update(labTestListDTO);
						}
						else
						{
							LabTestListDTO labTestListDTO =  createLabTestListDTOByRequestAndIndex(request, true, i);
							labTestListDTO.labTestId = lab_testDTO.iD; 
							labTestListDAO.add(labTestListDTO);
						}
					}
				}
				else
				{
					labTestListDAO.deleteChildrenByParent(lab_testDTO.iD, "lab_test_id");
				}
				
			}
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getLab_test(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Lab_testServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(lab_testDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	private List<LabTestListDTO> createLabTestListDTOListByRequest(HttpServletRequest request) throws Exception{ 
		List<LabTestListDTO> labTestListDTOList = new ArrayList<LabTestListDTO>();
		if(request.getParameterValues("labTestList.iD") != null) 
		{
			int labTestListItemNo = request.getParameterValues("labTestList.iD").length;
			
			
			for(int index=0;index<labTestListItemNo;index++){
				LabTestListDTO labTestListDTO = createLabTestListDTOByRequestAndIndex(request,true,index);
				labTestListDTOList.add(labTestListDTO);
			}
			
			return labTestListDTOList;
		}
		return null;
	}
	
	
	private LabTestListDTO createLabTestListDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{
	
		LabTestListDTO labTestListDTO;
		if(addFlag == true )
		{
			labTestListDTO = new LabTestListDTO();
		}
		else
		{
			labTestListDTO = labTestListDAO.getDTOByID(Long.parseLong(request.getParameterValues("labTestList.iD")[index]));
		}

		
				
		String Value = "";
		Value = request.getParameterValues("labTestList.labTestId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		labTestListDTO.labTestId = Long.parseLong(Value);
		Value = request.getParameterValues("labTestList.nameEn")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			labTestListDTO.nameEn = (Value);
			labTestListDTO.nameBn = labTestListDTO.nameEn;
		}


		Value = request.getParameterValues("labTestList.normalRange")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			labTestListDTO.normalRange = (Value);
		}
		
		Value = request.getParameterValues("labTestList.unit")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			labTestListDTO.unit = (Value);
		}

		
		Value = request.getParameterValues("labTestList.isTitle")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			labTestListDTO.isTitle = Boolean.parseBoolean(Value);
		}
		
		Value = request.getParameterValues("labTestList.ordering")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("ordering = " + Value);
		if(Value != null)
		{
			labTestListDTO.ordering = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		Value = request.getParameterValues("labTestList.showInDropdown")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("showInDropdown = " + Value);
		if(Value != null)
		{
			labTestListDTO.showInDropdown = Boolean.parseBoolean(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		
		return labTestListDTO;
	
	}
	
	
	

	private void getLab_test(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getLab_test");
		Lab_testDTO lab_testDTO = null;
		try 
		{
			lab_testDTO = lab_testDAO.getDTOByID(id);
			request.setAttribute("ID", lab_testDTO.iD);
			request.setAttribute("lab_testDTO",lab_testDTO);
			request.setAttribute("lab_testDAO",lab_testDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "lab_test/lab_testInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "lab_test/lab_testSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "lab_test/lab_testEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "lab_test/lab_testEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getLab_test(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getLab_test(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchLab_test(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchLab_test 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_LAB_TEST,
			request,
			lab_testDAO,
			SessionConstants.VIEW_LAB_TEST,
			SessionConstants.SEARCH_LAB_TEST,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("lab_testDAO",lab_testDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to lab_test/lab_testApproval.jsp");
	        	rd = request.getRequestDispatcher("lab_test/lab_testApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to lab_test/lab_testApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("lab_test/lab_testApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to lab_test/lab_testSearch.jsp");
	        	rd = request.getRequestDispatcher("lab_test/lab_testSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to lab_test/lab_testSearchForm.jsp");
	        	rd = request.getRequestDispatcher("lab_test/lab_testSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

