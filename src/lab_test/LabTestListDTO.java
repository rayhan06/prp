package lab_test;
import java.util.*; 
import util.*; 


public class LabTestListDTO extends CommonDTO
{

	public long labTestId = -1;
    public String nameEn = "";
    public String nameBn = "";
    public String normalRange = "";
	public boolean isTitle = false;
    public String unit = "";
    public int ordering = 0;
    public boolean showInDropdown = true;
	
	public List<LabTestListDTO> labTestListDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$LabTestListDTO[" +
            " iD = " + iD +
            " labTestId = " + labTestId +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " normalRange = " + normalRange +
            " isTitle = " + isTitle +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}