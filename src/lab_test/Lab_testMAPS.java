package lab_test;
import java.util.*; 
import util.*;


public class Lab_testMAPS extends CommonMaps
{	
	public Lab_testMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("nameEn".toLowerCase(), "nameEn".toLowerCase());
		java_DTO_map.put("nameBn".toLowerCase(), "nameBn".toLowerCase());
		java_DTO_map.put("normalRangeColumnName".toLowerCase(), "normalRangeColumnName".toLowerCase());
		java_DTO_map.put("description".toLowerCase(), "description".toLowerCase());
		java_DTO_map.put("specimen".toLowerCase(), "specimen".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("name_en".toLowerCase(), "nameEn".toLowerCase());
		java_SQL_map.put("name_bn".toLowerCase(), "nameBn".toLowerCase());
		java_SQL_map.put("normal_range_column_name".toLowerCase(), "normalRangeColumnName".toLowerCase());
		java_SQL_map.put("description".toLowerCase(), "description".toLowerCase());
		java_SQL_map.put("specimen".toLowerCase(), "specimen".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("English Name".toLowerCase(), "nameEn".toLowerCase());
		java_Text_map.put("Bangla Name".toLowerCase(), "nameBn".toLowerCase());
		java_Text_map.put("Normal Range Column Name".toLowerCase(), "normalRangeColumnName".toLowerCase());
		java_Text_map.put("Description".toLowerCase(), "description".toLowerCase());
		java_Text_map.put("Specimen".toLowerCase(), "specimen".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}