package lab_test;
import java.util.*; 
import util.*;


public class LabTestListMAPS extends CommonMaps
{	
	public LabTestListMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("labTestId".toLowerCase(), "labTestId".toLowerCase());
		java_DTO_map.put("nameEn".toLowerCase(), "nameEn".toLowerCase());
		java_DTO_map.put("nameBn".toLowerCase(), "nameBn".toLowerCase());
		java_DTO_map.put("normalRange".toLowerCase(), "normalRange".toLowerCase());
		java_DTO_map.put("isTitle".toLowerCase(), "isTitle".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("lab_test_id".toLowerCase(), "labTestId".toLowerCase());
		java_SQL_map.put("name_en".toLowerCase(), "nameEn".toLowerCase());
		java_SQL_map.put("name_bn".toLowerCase(), "nameBn".toLowerCase());
		java_SQL_map.put("normal_range".toLowerCase(), "normalRange".toLowerCase());
		java_SQL_map.put("is_title".toLowerCase(), "isTitle".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Lab Test Id".toLowerCase(), "labTestId".toLowerCase());
		java_Text_map.put("English Name".toLowerCase(), "nameEn".toLowerCase());
		java_Text_map.put("Bangla Name".toLowerCase(), "nameBn".toLowerCase());
		java_Text_map.put("Normal Range".toLowerCase(), "normalRange".toLowerCase());
		java_Text_map.put("Is Title".toLowerCase(), "isTitle".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}