package lab_test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Lab_testRepository implements Repository {
	Lab_testDAO lab_testDAO = null;
	
	public void setDAO(Lab_testDAO lab_testDAO)
	{
		this.lab_testDAO = lab_testDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Lab_testRepository.class);
	Map<Long, Lab_testDTO>mapOfLab_testDTOToiD;



	static Lab_testRepository instance = null;  
	private Lab_testRepository(){
		mapOfLab_testDTOToiD = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Lab_testRepository getInstance(){
		if (instance == null){
			instance = new Lab_testRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(lab_testDAO == null)
		{
			lab_testDAO = new Lab_testDAO();
		}
		try {
			List<Lab_testDTO> lab_testDTOs = lab_testDAO.getAllLab_test(reloadAll);
			for(Lab_testDTO lab_testDTO : lab_testDTOs) {
				Lab_testDTO oldLab_testDTO = getLab_testDTOByID(lab_testDTO.iD);
				if( oldLab_testDTO != null ) {
					mapOfLab_testDTOToiD.remove(oldLab_testDTO.iD);
				}
				if(lab_testDTO.isDeleted == 0) 
				{
					mapOfLab_testDTOToiD.put(lab_testDTO.iD, lab_testDTO);
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Lab_testDTO> getLab_testList() {
		List <Lab_testDTO> lab_tests = new ArrayList<Lab_testDTO>(this.mapOfLab_testDTOToiD.values());
		return lab_tests;
	}
	
	public List<Lab_testDTO> getVisibleLab_testList() {
		List <Lab_testDTO> lab_tests = getLab_testList();
		List <Lab_testDTO> lab_testsToReturn = new ArrayList<Lab_testDTO>();
		for(Lab_testDTO Lab_testDTO: lab_tests)
		{
			if(Lab_testDTO.showInDropdown)
			{
				lab_testsToReturn.add(Lab_testDTO);
			}
		}
		return lab_testsToReturn;
	}
	
	public String getOptions(boolean isLangEng, long defaultVal)
	{
		List<Lab_testDTO> tests = getVisibleLab_testList();
		String options = "<option value ='-1'>" +  (isLangEng?"Select":"বাছাই করুন" ) 
				+ "</option>";
		for(Lab_testDTO test: tests)
		{
			options+= "<option value = '" + test.iD + "' " 
					+ (test.iD == defaultVal?"selected":"") + ">" + test.nameEn + "</option>";
		}
		return options;
	}
	
	
	public Lab_testDTO getLab_testDTOByID( long ID){
		return mapOfLab_testDTOToiD.get(ID);
	}
	

	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "lab_test";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


