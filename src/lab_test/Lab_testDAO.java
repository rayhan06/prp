package lab_test;


import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.*;

import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import repository.RepositoryManager;

import util.*;

import user.UserDTO;

public class Lab_testDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Lab_testDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Lab_testMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"normal_range_column_name",
			"description",
			"specimen",
			"search_column",
			"ordering",
			"show_in_dropdown",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Lab_testDAO()
	{
		this("lab_test");		
	}
	
	public void setSearchColumn(Lab_testDTO lab_testDTO)
	{
		lab_testDTO.searchColumn = "";
		lab_testDTO.searchColumn += lab_testDTO.nameEn + " ";
		lab_testDTO.searchColumn += lab_testDTO.nameBn + " ";
		lab_testDTO.searchColumn += lab_testDTO.normalRangeColumnName + " ";
		lab_testDTO.searchColumn += lab_testDTO.description + " ";
		lab_testDTO.searchColumn += lab_testDTO.specimen + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Lab_testDTO lab_testDTO = (Lab_testDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(lab_testDTO);
		if(isInsert)
		{
			ps.setObject(index++,lab_testDTO.iD);
		}
		ps.setObject(index++,lab_testDTO.nameEn);
		ps.setObject(index++,lab_testDTO.nameBn);
		ps.setObject(index++,lab_testDTO.normalRangeColumnName);
		ps.setObject(index++,lab_testDTO.description);
		ps.setObject(index++,lab_testDTO.specimen);
		ps.setObject(index++,lab_testDTO.searchColumn);
		ps.setObject(index++,lab_testDTO.ordering);
		ps.setObject(index++,lab_testDTO.showInDropdown);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Lab_testDTO build(ResultSet rs)
	{
		try
		{
			Lab_testDTO lab_testDTO = new Lab_testDTO();
			lab_testDTO.iD = rs.getLong("ID");
			lab_testDTO.nameEn = rs.getString("name_en");
			lab_testDTO.nameBn = rs.getString("name_bn");
			lab_testDTO.normalRangeColumnName = rs.getString("normal_range_column_name");
			lab_testDTO.description = rs.getString("description");
			lab_testDTO.specimen = rs.getString("specimen");
			lab_testDTO.searchColumn = rs.getString("search_column");
			lab_testDTO.ordering = rs.getInt("ordering");
			lab_testDTO.showInDropdown = rs.getBoolean("show_in_dropdown");
			lab_testDTO.isDeleted = rs.getInt("isDeleted");
			lab_testDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return lab_testDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

	public Lab_testDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Lab_testDTO lab_testDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		try {
			LabTestListDAO labTestListDAO = new LabTestListDAO("lab_test_list");			
			List<LabTestListDTO> labTestListDTOList = labTestListDAO.getLabTestListDTOListByLabTestID(lab_testDTO.iD);
			lab_testDTO.labTestListDTOList = labTestListDTOList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
		return lab_testDTO;
	}

	
	public List<Lab_testDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<Lab_testDTO> getAllLab_test (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload)
		{
			sql+=" isDeleted =  0  ";
		}
		else
		{
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".ordering asc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Lab_testDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Lab_testDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name_en")
						|| str.equals("name_bn")
						|| str.equals("normal_range_column_name")
						|| str.equals("description")
						|| str.equals("specimen")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".name_en like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("name_bn"))
					{
						AllFieldSql += "" + tableName + ".name_bn like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("normal_range_column_name"))
					{
						AllFieldSql += "" + tableName + ".normal_range_column_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("description"))
					{
						AllFieldSql += "" + tableName + ".description like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("specimen"))
					{
						AllFieldSql += "" + tableName + ".specimen like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".ordering asc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	