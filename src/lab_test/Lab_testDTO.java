package lab_test;
import java.util.*; 
import util.*; 


public class Lab_testDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
    public String normalRangeColumnName = "";
    public String description = "";
    public String specimen = "";
    public int ordering = 0;
    public boolean showInDropdown = true;
	
	public List<LabTestListDTO> labTestListDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Lab_testDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " normalRangeColumnName = " + normalRangeColumnName +
            " description = " + description +
            " specimen = " + specimen +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}