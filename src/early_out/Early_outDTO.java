package early_out;
import java.util.*; 
import util.*; 


public class Early_outDTO extends CommonDTO
{

	public long organogramId = -1;
	public long superiorOrgId = -1;
    public String userId = "";
	public long leaveDate = System.currentTimeMillis();
	public long endDate = System.currentTimeMillis();
	public long startTime = -1;
	public long endTime = -1;
    public String reason = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	
	
    @Override
	public String toString() {
            return "$Early_outDTO[" +
            " iD = " + iD +
            " organogramId = " + organogramId +
            " userId = " + userId +
            " leaveDate = " + leaveDate +
            " startTime = " + startTime +
            " endTime = " + endTime +
            " reason = " + reason +
            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}