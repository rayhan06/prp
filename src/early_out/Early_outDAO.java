package early_out;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Early_outDAO  implements CommonDAOService<Early_outDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Early_outDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"organogram_id",
			"superior_org_id",
			"user_id",
			"leave_date",
			"end_date",
			"start_time",
			"end_time",
			"reason",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("leave_date_start"," and (leave_date >= ?)");
		searchMap.put("leave_date_end"," and (leave_date <= ?)");
		searchMap.put("organogram_id"," and (organogram_id = ?)");
		searchMap.put("superior_org_id"," and (superior_org_id = ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Early_outDAO INSTANCE = new Early_outDAO();
	}

	public static Early_outDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Early_outDTO early_outDTO)
	{
		early_outDTO.searchColumn = "";
		early_outDTO.searchColumn += early_outDTO.reason + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Early_outDTO early_outDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(early_outDTO);
		if(isInsert)
		{
			ps.setObject(++index,early_outDTO.iD);
		}
		ps.setObject(++index,early_outDTO.organogramId);
		ps.setObject(++index,early_outDTO.superiorOrgId);
		ps.setObject(++index,early_outDTO.userId);
		ps.setObject(++index,early_outDTO.leaveDate);
		ps.setObject(++index,early_outDTO.endDate);
		ps.setObject(++index,early_outDTO.startTime);
		ps.setObject(++index,early_outDTO.endTime);
		ps.setObject(++index,early_outDTO.reason);
		ps.setObject(++index,early_outDTO.searchColumn);
		ps.setObject(++index,early_outDTO.insertedByUserId);
		ps.setObject(++index,early_outDTO.insertedByOrganogramId);
		ps.setObject(++index,early_outDTO.insertionDate);
		ps.setObject(++index,early_outDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(++index,early_outDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,early_outDTO.iD);
		}
	}
	
	@Override
	public Early_outDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Early_outDTO early_outDTO = new Early_outDTO();
			int i = 0;
			early_outDTO.iD = rs.getLong(columnNames[i++]);
			early_outDTO.organogramId = rs.getLong(columnNames[i++]);
			early_outDTO.superiorOrgId = rs.getLong(columnNames[i++]);
			early_outDTO.userId = rs.getString(columnNames[i++]);
			early_outDTO.leaveDate = rs.getLong(columnNames[i++]);
			early_outDTO.endDate = rs.getLong(columnNames[i++]);
			early_outDTO.startTime = rs.getLong(columnNames[i++]);
			early_outDTO.endTime = rs.getLong(columnNames[i++]);
			early_outDTO.reason = rs.getString(columnNames[i++]);
			early_outDTO.searchColumn = rs.getString(columnNames[i++]);
			early_outDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			early_outDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			early_outDTO.insertionDate = rs.getLong(columnNames[i++]);
			early_outDTO.lastModifierUser = rs.getString(columnNames[i++]);
			early_outDTO.isDeleted = rs.getInt(columnNames[i++]);
			early_outDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return early_outDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Early_outDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "early_out";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Early_outDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Early_outDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
				
}
	