package early_out;


import java.text.SimpleDateFormat;



import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;


import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;

import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import permission.MenuConstants;


import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;
import workflow.WorkflowController;

import javax.servlet.http.*;
import java.util.*;


import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;
import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import pbReport.*;
import pb_notifications.Pb_notificationsDAO;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Early_outServlet
 */
@WebServlet("/Early_outServlet")
@MultipartConfig
public class Early_outServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Early_outServlet.class);
    Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();

    @Override
    public String getTableName() {
        return Early_outDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Early_outServlet";
    }

    @Override
    public Early_outDAO getCommonDAOService() {
        return Early_outDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.EARLY_OUT_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.EARLY_OUT_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.EARLY_OUT_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Early_outServlet.class;
    }
    private final Gson gson = new Gson();
    
    @Override
    public String getWhereClause(HttpServletRequest request){
    	CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
    	if(commonLoginData.userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE    	
    		||commonLoginData.userDTO.roleID == SessionConstants.ADMIN_ROLE)
    	{
    		return null;
    	}
    	else
    	{
    		return " and (organogram_id = " + commonLoginData.userDTO.organogramID 
    				+ " or superior_org_id = " + commonLoginData.userDTO.organogramID + ")";
    	}
    }
    
    private void sendLeaveNoti(Early_outDTO early_outDTO)
    {
    	OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(early_outDTO.organogramId);
    	if(officeUnitOrganograms != null)
    	{
    		try {
    			String englishName = WorkflowController.getNameFromOrganogramId(early_outDTO.organogramId, "english");
    			String banglaName = WorkflowController.getNameFromOrganogramId(early_outDTO.organogramId, "bangla");
    			
    			String orgEng = officeUnitOrganograms.designation_eng;
    			String orgBng = officeUnitOrganograms.designation_bng;
    			
    			Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitOrganograms.office_unit_id);
    			if(office_unitsDTO != null)
    			{
    				String officeEng = office_unitsDTO.unitNameEng;
    				String officeBng = office_unitsDTO.unitNameBng;
    				
    				String englishText = orgEng + " " + englishName + " (" + officeEng + ")"
    						+ " will remain absent from office from " 
    						+ TimeConverter.hourMinuteFormat.format(early_outDTO.startTime)
    						+ " to "
    						+ TimeConverter.hourMinuteFormat.format(early_outDTO.endTime)
    						+ " on " + TimeConverter.df.format(early_outDTO.leaveDate);
    				
    				String banglaText = orgBng + " " + banglaName + " (" + officeBng + ")"
    						+ " অফিসে থাকবেন না " 
    						+ Utils.getDigits(TimeConverter.df.format(early_outDTO.leaveDate), "bangla") + " তারিখে "
    						+ Utils.getDigits(TimeConverter.hourMinuteFormat.format(early_outDTO.startTime), "bangla")
    						+ " থেকে "
    						+ Utils.getDigits(TimeConverter.hourMinuteFormat.format(early_outDTO.endTime), "bangla")
    						+ " পর্যন্ত ";
        			
    				String URL =  "Early_outServlet?actionType=view&ID=" + early_outDTO.iD;

        			pb_notificationsDAO.addPb_notificationsAndSendMailSMS(early_outDTO.superiorOrgId, System.currentTimeMillis(), URL, 
        					englishText, banglaText, "Employe leaving early");
    			}
    			
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	}
    	
    }
    
 	

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		// TODO Auto-generated method stub
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addEarly_out");
		String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
		Early_outDTO early_outDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
		if(addFlag == true)
		{
			early_outDTO = new Early_outDTO();
		}
		else
		{
			early_outDTO = Early_outDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		early_outDTO.organogramId = userDTO.organogramID;
		early_outDTO.userId = userDTO.userName;
		
		OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(early_outDTO.organogramId);
    	if(officeUnitOrganograms == null)
    	{
    		throw new Exception(ErrorMessage.getGenericInvalidMessage(language));
    	}			
		early_outDTO.superiorOrgId = officeUnitOrganograms.superior_designation_id;


		Value = request.getParameter("leaveDate");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("leaveDate = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			try 
			{
				Date d = f.parse(Value);
				early_outDTO.leaveDate = d.getTime();
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				throw new Exception(LM.getText(LC.EARLY_OUT_ADD_LEAVEDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		Value = request.getParameter("endDate");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("endDate = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			try 
			{
				Date d = f.parse(Value);
				early_outDTO.endDate = d.getTime();
				
				if(early_outDTO.endDate < early_outDTO.leaveDate)
				{
					throw new Exception(LM.getText(LC.EARLY_OUT_ADD_LEAVEDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				throw new Exception(LM.getText(LC.EARLY_OUT_ADD_LEAVEDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("startTime");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("startTime = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			early_outDTO.startTime = TimeConverter.hourMinuteFormat.parse(Value).getTime();
		}
		else
		{
			throw new Exception(LM.getText(LC.EARLY_OUT_ADD_STARTTIME, language) + " " + ErrorMessage.getInvalidMessage(language));
		}

		Value = request.getParameter("endTime");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("endTime = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			early_outDTO.endTime = TimeConverter.hourMinuteFormat.parse(Value).getTime();
			if(early_outDTO.endDate == early_outDTO.leaveDate && early_outDTO.endTime < early_outDTO.startTime)
			{
				throw new Exception(LM.getText(LC.EARLY_OUT_ADD_STARTTIME, language) + " " + ErrorMessage.getInvalidMessage(language));
			}
		}
		else
		{
			throw new Exception(LM.getText(LC.EARLY_OUT_ADD_ENDTIME, language) + " " + ErrorMessage.getInvalidMessage(language));
		}

		Value = request.getParameter("reason");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("reason = " + Value);
		if(Value != null)
		{
			early_outDTO.reason = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("searchColumn");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("searchColumn = " + Value);
		if(Value != null)
		{
			early_outDTO.searchColumn = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		if(addFlag)
		{
			early_outDTO.insertedByUserId = userDTO.ID;
		}


		if(addFlag)
		{
			early_outDTO.insertedByOrganogramId = userDTO.organogramID;
		}


		if(addFlag)
		{				
			early_outDTO.insertionDate = TimeConverter.getToday();
		}			


		early_outDTO.lastModifierUser = userDTO.userName;

		
		System.out.println("Done adding  addEarly_out dto = " + early_outDTO);
		long returnedID = -1;
		
		
		if(addFlag == true)
		{
			returnedID = Early_outDAO.getInstance().add(early_outDTO);
		}
		else
		{				
			returnedID = Early_outDAO.getInstance().update(early_outDTO);										
		}
		
		sendLeaveNoti(early_outDTO);
		
	
		return early_outDTO;
					
		
	}
}

