package early_out;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Early_outRepository implements Repository {
	Early_outDAO early_outDAO = null;
	
	static Logger logger = Logger.getLogger(Early_outRepository.class);
	Map<Long, Early_outDTO>mapOfEarly_outDTOToiD;
	Gson gson;

  
	private Early_outRepository(){
		early_outDAO = Early_outDAO.getInstance();
		mapOfEarly_outDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Early_outRepository INSTANCE = new Early_outRepository();
    }

    public static Early_outRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Early_outDTO> early_outDTOs = early_outDAO.getAllDTOs(reloadAll);
			for(Early_outDTO early_outDTO : early_outDTOs) {
				Early_outDTO oldEarly_outDTO = getEarly_outDTOByiD(early_outDTO.iD);
				if( oldEarly_outDTO != null ) {
					mapOfEarly_outDTOToiD.remove(oldEarly_outDTO.iD);
				
					
				}
				if(early_outDTO.isDeleted == 0) 
				{
					
					mapOfEarly_outDTOToiD.put(early_outDTO.iD, early_outDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Early_outDTO clone(Early_outDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Early_outDTO.class);
	}
	
	
	public List<Early_outDTO> getEarly_outList() {
		List <Early_outDTO> early_outs = new ArrayList<Early_outDTO>(this.mapOfEarly_outDTOToiD.values());
		return early_outs;
	}
	
	public List<Early_outDTO> copyEarly_outList() {
		List <Early_outDTO> early_outs = getEarly_outList();
		return early_outs
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Early_outDTO getEarly_outDTOByiD( long iD){
		return mapOfEarly_outDTOToiD.get(iD);
	}
	
	public Early_outDTO copyEarly_outDTOByiD( long iD){
		return clone(mapOfEarly_outDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return early_outDAO.getTableName();
	}
}


