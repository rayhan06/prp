package recruitment_job_req_trains_and_certs;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import common.ApiResponse;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Recruitment_job_req_trains_and_certsServlet
 */
@WebServlet("/Recruitment_job_req_trains_and_certsServlet")
@MultipartConfig
public class Recruitment_job_req_trains_and_certsServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Recruitment_job_req_trains_and_certsServlet.class);

    String tableName = "recruitment_job_req_trains_and_certs";

	Recruitment_job_req_trains_and_certsDAO recruitment_job_req_trains_and_certsDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Recruitment_job_req_trains_and_certsServlet()
	{
        super();
    	try
    	{
			recruitment_job_req_trains_and_certsDAO = new Recruitment_job_req_trains_and_certsDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(recruitment_job_req_trains_and_certsDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS_UPDATE))
				{
					getRecruitment_job_req_trains_and_certs(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchRecruitment_job_req_trains_and_certs(request, response, isPermanentTable, filter);
						}
						else
						{
							searchRecruitment_job_req_trains_and_certs(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchRecruitment_job_req_trains_and_certs(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS_ADD))
				{
					System.out.println("going to  addRecruitment_job_req_trains_and_certs ");
					addRecruitment_job_req_trains_and_certs(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addRecruitment_job_req_trains_and_certs ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addRecruitment_job_req_trains_and_certs ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS_UPDATE))
				{
					addRecruitment_job_req_trains_and_certs(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				deleteRecruitment_job_req_trains_and_certs(request, response, userDTO);
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS_SEARCH))
				{
					searchRecruitment_job_req_trains_and_certs(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Recruitment_job_req_trains_and_certsDTO recruitment_job_req_trains_and_certsDTO = Recruitment_job_req_trains_and_certsRepository.getInstance().
					getRecruitment_job_req_trains_and_certsDTOByID(Long.parseLong(request.getParameter("ID")));
//					(Recruitment_job_req_trains_and_certsDTO)recruitment_job_req_trains_and_certsDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(recruitment_job_req_trains_and_certsDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addRecruitment_job_req_trains_and_certs(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		ApiResponse apiResponse;
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addRecruitment_job_req_trains_and_certs");
			String path = getServletContext().getRealPath("/img2/");
			Recruitment_job_req_trains_and_certsDTO recruitment_job_req_trains_and_certsDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				recruitment_job_req_trains_and_certsDTO = new Recruitment_job_req_trains_and_certsDTO();
			}
			else
			{
				recruitment_job_req_trains_and_certsDTO = Recruitment_job_req_trains_and_certsRepository.getInstance().
						getRecruitment_job_req_trains_and_certsDTOByID(Long.parseLong(request.getParameter("iD")));
//						(Recruitment_job_req_trains_and_certsDTO)recruitment_job_req_trains_and_certsDAO.getDTOByID
//								(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("nameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameBn = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				recruitment_job_req_trains_and_certsDTO.nameBn = (Value);
			}
			else
			{
				throw new Exception(" Invalid name bn");
			}

			Value = request.getParameter("nameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEn = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				recruitment_job_req_trains_and_certsDTO.nameEn = (Value);
			}
			else
			{
				throw new Exception(" Invalid name en");
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				recruitment_job_req_trains_and_certsDTO.insertionDate = c.getTimeInMillis();
			}


			if(addFlag)
			{
				recruitment_job_req_trains_and_certsDTO.insertedByUserId = userDTO.ID;
			}


			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				recruitment_job_req_trains_and_certsDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addRecruitment_job_req_trains_and_certs dto = " + recruitment_job_req_trains_and_certsDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				recruitment_job_req_trains_and_certsDAO.setIsDeleted(recruitment_job_req_trains_and_certsDTO.iD, CommonDTO.OUTDATED);
				returnedID = recruitment_job_req_trains_and_certsDAO.add(recruitment_job_req_trains_and_certsDTO);
				recruitment_job_req_trains_and_certsDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = recruitment_job_req_trains_and_certsDAO.manageWriteOperations(recruitment_job_req_trains_and_certsDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = recruitment_job_req_trains_and_certsDAO.manageWriteOperations(recruitment_job_req_trains_and_certsDTO, SessionConstants.UPDATE, -1, userDTO);
			}







			apiResponse = ApiResponse.makeSuccessResponse("Recruitment_job_req_trains_and_certsServlet?actionType=search");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			apiResponse = ApiResponse.makeErrorResponse(e.getMessage());
		}

		PrintWriter pw = response.getWriter();
		pw.write(apiResponse.getJSONString());
		pw.flush();
		pw.close();
	}









	private void deleteRecruitment_job_req_trains_and_certs(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException
	{
		try
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);


				Recruitment_job_req_trains_and_certsDTO recruitment_job_req_trains_and_certsDTO = Recruitment_job_req_trains_and_certsRepository.getInstance().
						getRecruitment_job_req_trains_and_certsDTOByID(id);
//						(Recruitment_job_req_trains_and_certsDTO)recruitment_job_req_trains_and_certsDAO.getDTOByID(id);
				recruitment_job_req_trains_and_certsDAO.manageWriteOperations(recruitment_job_req_trains_and_certsDTO, SessionConstants.DELETE, id, userDTO);
				response.sendRedirect("Recruitment_job_req_trains_and_certsServlet?actionType=search");

			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

	}

	private void getRecruitment_job_req_trains_and_certs(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getRecruitment_job_req_trains_and_certs");
		Recruitment_job_req_trains_and_certsDTO recruitment_job_req_trains_and_certsDTO = null;
		try
		{
			recruitment_job_req_trains_and_certsDTO = Recruitment_job_req_trains_and_certsRepository.getInstance().
					getRecruitment_job_req_trains_and_certsDTOByID(id);
//					(Recruitment_job_req_trains_and_certsDTO)recruitment_job_req_trains_and_certsDAO.getDTOByID(id);
			request.setAttribute("ID", recruitment_job_req_trains_and_certsDTO.iD);
			request.setAttribute("recruitment_job_req_trains_and_certsDTO",recruitment_job_req_trains_and_certsDTO);
			request.setAttribute("recruitment_job_req_trains_and_certsDAO",recruitment_job_req_trains_and_certsDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "recruitment_job_req_trains_and_certs/recruitment_job_req_trains_and_certsInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "recruitment_job_req_trains_and_certs/recruitment_job_req_trains_and_certsSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "recruitment_job_req_trains_and_certs/recruitment_job_req_trains_and_certsEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "recruitment_job_req_trains_and_certs/recruitment_job_req_trains_and_certsEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getRecruitment_job_req_trains_and_certs(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getRecruitment_job_req_trains_and_certs(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchRecruitment_job_req_trains_and_certs(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchRecruitment_job_req_trains_and_certs 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS,
			request,
			recruitment_job_req_trains_and_certsDAO,
			SessionConstants.VIEW_RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS,
			SessionConstants.SEARCH_RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("recruitment_job_req_trains_and_certsDAO",recruitment_job_req_trains_and_certsDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to recruitment_job_req_trains_and_certs/recruitment_job_req_trains_and_certsApproval.jsp");
	        	rd = request.getRequestDispatcher("recruitment_job_req_trains_and_certs/recruitment_job_req_trains_and_certsApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to recruitment_job_req_trains_and_certs/recruitment_job_req_trains_and_certsApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("recruitment_job_req_trains_and_certs/recruitment_job_req_trains_and_certsApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to recruitment_job_req_trains_and_certs/recruitment_job_req_trains_and_certsSearch.jsp");
	        	rd = request.getRequestDispatcher("recruitment_job_req_trains_and_certs/recruitment_job_req_trains_and_certsSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to recruitment_job_req_trains_and_certs/recruitment_job_req_trains_and_certsSearchForm.jsp");
	        	rd = request.getRequestDispatcher("recruitment_job_req_trains_and_certs/recruitment_job_req_trains_and_certsSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

