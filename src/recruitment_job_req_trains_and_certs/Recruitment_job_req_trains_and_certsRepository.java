package recruitment_job_req_trains_and_certs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import pb.OptionDTO;
import pb.Utils;
import recruitment_job_required_files.Recruitment_job_required_filesDTO;
import repository.Repository;
import repository.RepositoryManager;
import vm_route.VmRouteStoppageDTO;


public class Recruitment_job_req_trains_and_certsRepository implements Repository {
	Recruitment_job_req_trains_and_certsDAO recruitment_job_req_trains_and_certsDAO = null;
	Gson gson = new Gson();
	
	public void setDAO(Recruitment_job_req_trains_and_certsDAO recruitment_job_req_trains_and_certsDAO)
	{
		this.recruitment_job_req_trains_and_certsDAO = recruitment_job_req_trains_and_certsDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Recruitment_job_req_trains_and_certsRepository.class);
	Map<Long, Recruitment_job_req_trains_and_certsDTO>mapOfRecruitment_job_req_trains_and_certsDTOToiD;


	static Recruitment_job_req_trains_and_certsRepository instance = null;  
	private Recruitment_job_req_trains_and_certsRepository(){
		mapOfRecruitment_job_req_trains_and_certsDTOToiD = new ConcurrentHashMap<>();
		setDAO(new Recruitment_job_req_trains_and_certsDAO());

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Recruitment_job_req_trains_and_certsRepository getInstance(){
		if (instance == null){
			instance = new Recruitment_job_req_trains_and_certsRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(recruitment_job_req_trains_and_certsDAO == null)
		{
			return;
		}
		try {
			List<Recruitment_job_req_trains_and_certsDTO> recruitment_job_req_trains_and_certsDTOs = (List<Recruitment_job_req_trains_and_certsDTO>) recruitment_job_req_trains_and_certsDAO.getAll(reloadAll);
			for(Recruitment_job_req_trains_and_certsDTO recruitment_job_req_trains_and_certsDTO : recruitment_job_req_trains_and_certsDTOs) {
				Recruitment_job_req_trains_and_certsDTO oldRecruitment_job_req_trains_and_certsDTO =
						getRecruitment_job_req_trains_and_certsDTOByIDWithoutClone(recruitment_job_req_trains_and_certsDTO.iD);
				if( oldRecruitment_job_req_trains_and_certsDTO != null ) {
					mapOfRecruitment_job_req_trains_and_certsDTOToiD.remove(oldRecruitment_job_req_trains_and_certsDTO.iD);
					
					
				}
				if(recruitment_job_req_trains_and_certsDTO.isDeleted == 0) 
				{
					
					mapOfRecruitment_job_req_trains_and_certsDTOToiD.put(recruitment_job_req_trains_and_certsDTO.iD, recruitment_job_req_trains_and_certsDTO);
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Recruitment_job_req_trains_and_certsDTO> getRecruitment_job_req_trains_and_certsList() {
		List <Recruitment_job_req_trains_and_certsDTO> recruitment_job_req_trains_and_certss = new ArrayList<Recruitment_job_req_trains_and_certsDTO>(this.mapOfRecruitment_job_req_trains_and_certsDTOToiD.values());
		return clone(recruitment_job_req_trains_and_certss);
	}
	
	
	public Recruitment_job_req_trains_and_certsDTO getRecruitment_job_req_trains_and_certsDTOByIDWithoutClone( long ID){
		return mapOfRecruitment_job_req_trains_and_certsDTOToiD.get(ID);
	}

	public Recruitment_job_req_trains_and_certsDTO getRecruitment_job_req_trains_and_certsDTOByID( long ID){
		return clone(mapOfRecruitment_job_req_trains_and_certsDTOToiD.get(ID));
	}

	public Recruitment_job_req_trains_and_certsDTO clone(Recruitment_job_req_trains_and_certsDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Recruitment_job_req_trains_and_certsDTO.class);
	}

	public List<Recruitment_job_req_trains_and_certsDTO> clone(List<Recruitment_job_req_trains_and_certsDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "recruitment_job_req_trains_and_certs";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}

	public String getBuildOptions(String Language, String selectedId ){
		List<OptionDTO> optionDTOList = getRecruitment_job_req_trains_and_certsList()
				.stream()
				.map(e->new OptionDTO(e.nameEn,e.nameBn,String.valueOf(e.iD)))
				.collect(Collectors.toList());

		return Utils.buildOptionsWithoutSelectWithSelectId(optionDTOList, Language, selectedId );

	}

	public String getText(String language, long id) {
		Recruitment_job_req_trains_and_certsDTO dto = this.mapOfRecruitment_job_req_trains_and_certsDTOToiD.get(id);
		return dto == null ? ""
				: (language.equalsIgnoreCase("English")) ? (dto.nameEn == null ? "" : dto.nameEn)
				: (dto.nameBn == null ? "" : dto.nameBn);
	}
}


