package recruitment_job_req_trains_and_certs;
import java.util.*; 
import util.*; 


public class Recruitment_job_req_trains_and_certsDTO extends CommonDTO
{

    public String nameBn = "";
    public String nameEn = "";
	public long insertionDate = 0;
	public long insertedByUserId = 0;
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Recruitment_job_req_trains_and_certsDTO[" +
            " iD = " + iD +
            " nameBn = " + nameBn +
            " nameEn = " + nameEn +
            " insertionDate = " + insertionDate +
            " insertedByUserId = " + insertedByUserId +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}