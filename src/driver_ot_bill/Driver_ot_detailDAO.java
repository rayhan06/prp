package driver_ot_bill;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import holiday.HolidayDAO;
import holiday.HolidayDTO;
import org.apache.log4j.Logger;
import pbReport.DateUtils;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class Driver_ot_detailDAO implements CommonDAOService<Driver_ot_detailDTO> {
    private static final Logger logger = Logger.getLogger(Driver_ot_detailDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (driver_ot_bill_id,driver_ot_date,driver_ot_date_type,office_hour_from,office_hour_to,"
                    .concat("extra_hour_morning_from,extra_hour_morning_to,extra_hour_afternoon_from,extra_hour_afternoon_to,")
                    .concat("modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET driver_ot_bill_id=?,driver_ot_date=?,driver_ot_date_type=?,office_hour_from=?,office_hour_to=?,"
                    .concat("extra_hour_morning_from=?,extra_hour_morning_to=?,extra_hour_afternoon_from=?,extra_hour_afternoon_to=?,")
                    .concat("modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private Driver_ot_detailDAO() {

    }

    private static class LazyLoader {
        static final Driver_ot_detailDAO INSTANCE = new Driver_ot_detailDAO();
    }

    public static Driver_ot_detailDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Driver_ot_detailDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.driverOtBillId);
        ps.setLong(++index, dto.driverOtDate);
        ps.setInt(++index, dto.driverOtDateType.getValue());
        ps.setInt(++index, dto.officeHourFrom);
        ps.setInt(++index, dto.officeHourTo);
        ps.setInt(++index, dto.extraHourMorningFrom);
        ps.setInt(++index, dto.extraHourMorningTo);
        ps.setInt(++index, dto.extraHourAfternoonFrom);
        ps.setInt(++index, dto.extraHourAfternoonTo);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Driver_ot_detailDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Driver_ot_detailDTO dto = new Driver_ot_detailDTO();
            dto.iD = rs.getLong("ID");
            dto.driverOtBillId = rs.getLong("driver_ot_bill_id");
            dto.driverOtDate = rs.getLong("driver_ot_date");
            dto.driverOtDateType = DriverOtDateType.getByValue(rs.getInt("driver_ot_date_type"));
            dto.officeHourFrom = rs.getInt("office_hour_from");
            dto.officeHourTo = rs.getInt("office_hour_to");
            dto.extraHourMorningFrom = rs.getInt("extra_hour_morning_from");
            dto.extraHourMorningTo = rs.getInt("extra_hour_morning_to");
            dto.extraHourAfternoonFrom = rs.getInt("extra_hour_afternoon_from");
            dto.extraHourAfternoonTo = rs.getInt("extra_hour_afternoon_to");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "driver_ot_detail";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Driver_ot_detailDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Driver_ot_detailDTO) commonDTO, updateSqlQuery, false);
    }

    private static final String findByDriverOtBillIdSql = "select * from driver_ot_detail where driver_ot_bill_id=%d and isDeleted=0";

    public List<Driver_ot_detailDTO> getByDriverOtBillId(long driverOtBillId) {
        List<Driver_ot_detailDTO> dtoList = getDTOs(String.format(findByDriverOtBillIdSql, driverOtBillId));
        dtoList.sort(Comparator.comparingLong(dto -> dto.driverOtDate));
        return dtoList;
    }

    private DriverOtDetailModel buildNewModel(long driverOtDate,
                                              Map<Long, List<HolidayDTO>> holidaysByDate,
                                              Driver_ot_bill_configDTO driverOtBillConfigDTO) {
        DriverOtDetailModel model = new DriverOtDetailModel();
        model.officeHourFrom = driverOtBillConfigDTO.minOfficeHour;
        model.officeHourTo = driverOtBillConfigDTO.maxOfficeHour;

        model.driverOtDate = driverOtDate;
        model.driverOtDateType = DriverOtDateType.WORK_DAY.getValue();
        int dayOfWeek = DateUtils.getDayOfWeek(model.driverOtDate);
        if (dayOfWeek == Calendar.FRIDAY) {
            model.driverOtDateType = DriverOtDateType.FRIDAY.getValue();
        } else if (dayOfWeek == Calendar.SATURDAY) {
            model.driverOtDateType = DriverOtDateType.SATURDAY.getValue();
        } else if (holidaysByDate.containsKey(driverOtDate)) {
            model.driverOtDateType = DriverOtDateType.GOVT_HOLIDAY.getValue();
        }
        if (model.driverOtDateType != DriverOtDateType.WORK_DAY.getValue()) {
            model.officeHourFrom = -1;
            model.officeHourTo = -1;
        }
        model.calculateTotalHours();
        return model;
    }

    public List<DriverOtDetailModel> buildSortedModelList(long startDate, long endDate, Driver_ot_bill_configDTO driverOtBillConfigDTO) {
        Map<Long, List<HolidayDTO>> holidaysByDate = new HolidayDAO().findByTimeRangeGroupedByDate(startDate, endDate + 1);
        List<DriverOtDetailModel> modelList = new ArrayList<>();
        while (startDate <= endDate) {
            modelList.add(buildNewModel(startDate, holidaysByDate, driverOtBillConfigDTO));
            startDate += DateUtils.ONE_DAY_IN_MILLIS;
        }
        return modelList;
    }

    public List<DriverOtDetailModel> buildSortedModelFromBillDto(Driver_ot_billDTO driverOtBillDTO) {
        return getByDriverOtBillId(driverOtBillDTO.iD)
                .stream()
                .sorted(Comparator.comparingLong(dto -> dto.driverOtDate))
                .map(DriverOtDetailModel::new)
                .collect(Collectors.toList());
    }

    private static final String deleteByDriverOtBillIdSql =
            "UPDATE driver_ot_detail SET isDeleted = 1,modified_by = %d,lastModificationTime = %d WHERE driver_ot_bill_id = %d";

    public void deleteByDriverOtBillId(long driverOtBillId, long modifiedBy, long modificationTime) {
        String sql = String.format(deleteByDriverOtBillIdSql, modifiedBy, modificationTime, driverOtBillId);
        logger.debug(sql);
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            try {
                model.getStatement().executeUpdate(sql);
                recordUpdateTime(model.getConnection(), getTableName(), modificationTime);
                return true;
            } catch (SQLException ex) {
                ex.printStackTrace();
                return false;
            }
        });
    }
}
