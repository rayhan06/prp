package driver_ot_bill;

import java.util.ArrayList;
import java.util.List;

public class DriverOtBillTableData {
    public static final int NOON = 12;

    public String errorMessage = null;
    public boolean isEditable = false;
    public int minOfficeHour;
    public int maxOfficeHour;
    public int minExtraHour;
    public int maxExtraHour;
    public List<DriverOtDetailModel> driverOtDetailModels = new ArrayList<>();
}
