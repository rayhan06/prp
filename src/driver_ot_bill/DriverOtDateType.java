package driver_ot_bill;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum DriverOtDateType {
    WORK_DAY(0, ""),
    FRIDAY(1, "শুক্রবার"),
    SATURDAY(2, "শনিবার"),
    GOVT_HOLIDAY(3, "স:ছুটি");

    private final int value;
    private final String banglaText;

    private static final Map<Integer, DriverOtDateType> mapByValue =
            Arrays.stream(values())
                  .collect(Collectors.toMap(
                          DriverOtDateType::getValue,
                          Function.identity()
                  ));

    DriverOtDateType(int value, String banglaText) {
        this.value = value;
        this.banglaText = banglaText;
    }

    public int getValue() {
        return value;
    }

    public String getBanglaText() {
        return banglaText;
    }

    public static DriverOtDateType getByValue(int value) {
        return mapByValue.get(value);
    }
}
