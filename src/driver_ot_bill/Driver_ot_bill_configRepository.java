package driver_ot_bill;

import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Driver_ot_bill_configRepository implements Repository {
    private static final Logger logger = Logger.getLogger(Driver_ot_bill_configRepository.class);
    public static final long configId = 1L;

    private final Map<Long, Driver_ot_bill_configDTO> mapById;
    private final Driver_ot_bill_configDAO dao;

    private Driver_ot_bill_configRepository() {
        mapById = new ConcurrentHashMap<>();
        dao = Driver_ot_bill_configDAO.getInstance();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Driver_ot_bill_configRepository INSTANCE = new Driver_ot_bill_configRepository();
    }

    public static Driver_ot_bill_configRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public String getTableName() {
        return dao.getTableName();
    }

    public synchronized void reload(boolean reloadAll) {
        logger.debug("Driver_ot_bill_configRepository loading start for reloadAll: " + reloadAll);
        List<Driver_ot_bill_configDTO> dtoList = dao.getAllDTOs(reloadAll);
        if (dtoList != null && dtoList.size() > 0) {
            dtoList.stream()
                   .peek(this::removeIfPresent)
                   .filter(dto -> dto.isDeleted == 0)
                   .forEach(dto -> mapById.put(dto.iD, dto));
        }
        logger.debug("Driver_ot_bill_configRepository loading end for reloadAll: " + reloadAll);
    }

    private void removeIfPresent(Driver_ot_bill_configDTO dto) {
        if (mapById.get(dto.iD) != null) {
            mapById.remove(dto.iD);
        }
    }

    public Driver_ot_bill_configDTO getConfig() {
        return mapById.get(configId);
    }
}
