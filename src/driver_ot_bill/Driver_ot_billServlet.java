package driver_ot_bill;

import bill_approval_history.BillApprovalResponse;
import bill_approval_history.BillApprovalStatus;
import bill_approval_history.Bill_approval_historyDAO;
import bill_approval_history.Bill_approval_historyDTO;
import common.*;
import employee_bank_information.Employee_bank_informationDAO;
import employee_bank_information.Employee_bank_informationDTO;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import grade_wise_pay_scale.Grade_wise_pay_scaleRepository;
import login.LoginDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import ot_bill_submission_config.OT_bill_submission_configDAO;
import ot_bill_submission_config.OT_bill_submission_configDTO;
import pb.CatRepository;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.StringUtils;
import vm_vehicle.Vm_vehicleDTO;
import vm_vehicle.Vm_vehicleRepository;
import vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentDAO;
import vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates", "unchecked"})
@WebServlet("/Driver_ot_billServlet")
@MultipartConfig
public class Driver_ot_billServlet extends BaseServlet {
    private static final Logger logger = Logger.getLogger(Driver_ot_billServlet.class);
    private static final long TASK_TYPE_ID = 14L;
    public static final long DRIVER_OT_BILL_BUDGET_MAPPING_ID = 1L;
    public static final long DRIVER_OT_BILL_SUB_CODE_ID = 602L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        String actionType = request.getParameter("actionType");
        try {
            switch (actionType) {
                case "ajax_getDriverOtBillData":
                    if (Utils.checkPermission(userDTO, MenuConstants.DRIVER_OT_BILL_ADD)) {
                        Long employeeOfficesId = Utils.parseOptionalLong(
                                request.getParameter("employeeOfficesId"),
                                null, null
                        );

                        Long otBillSubmissionConfigId = Utils.parseOptionalLong(
                                request.getParameter("otBillSubmissionConfigId"),
                                null, null
                        );
                        request.setAttribute("driverOtBillTableData", getDriverOtBillData(employeeOfficesId, otBillSubmissionConfigId));
                        request.getRequestDispatcher("driver_ot_bill/driver_ot_billTable.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "getEditPage":
                    if (Utils.checkPermission(userDTO, MenuConstants.DRIVER_OT_BILL_ADD)) {
                        Long id = Utils.parseOptionalLong(request.getParameter("ID"), null, null);
                        String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

                        Driver_ot_billDTO driverOtBillDTO = Driver_ot_billDAO.getInstance().getDTOFromID(id);
                        request.setAttribute("driverOtBillDTO", driverOtBillDTO);

                        DriverOtBillTableData driverOtBillTableData = new DriverOtBillTableData();
                        driverOtBillTableData.driverOtDetailModels = Driver_ot_detailDAO.getInstance().buildSortedModelFromBillDto(driverOtBillDTO);
                        driverOtBillTableData.minExtraHour = driverOtBillDTO.minExtraHour;
                        driverOtBillTableData.minOfficeHour = driverOtBillDTO.minOfficeHour;
                        driverOtBillTableData.maxOfficeHour = driverOtBillDTO.maxOfficeHour;
                        driverOtBillTableData.maxExtraHour = driverOtBillDTO.maxExtraHour;
                        try {
                            driverOtBillDTO.checkEditabilityOrThrowException(userDTO, language);
                            driverOtBillTableData.isEditable = true;
                        } catch (Exception ex) {
                            logger.error(ex);
                            request.setAttribute("notEditableCause", ex.getMessage());
                            driverOtBillTableData.isEditable = false;
                        }
                        request.setAttribute("driverOtBillTableData", driverOtBillTableData);
                        request.getRequestDispatcher("driver_ot_bill/driver_ot_billEdit.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "getConfigPage":
                    if (Utils.checkPermission(userDTO, MenuConstants.DRIVER_OT_BILL_CONFIG_ADD)) {
                        request.getRequestDispatcher("driver_ot_bill/driver_ot_bill_configEdit.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "search": {
                    Map<String, String> extraCriteriaMap;
                    extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
                    if (extraCriteriaMap == null) extraCriteriaMap = new HashMap<>();
                    extraCriteriaMap.put("isInPreview", "0");
                    if (!isAllowedToSeeAllOfficeBill(userDTO)) {
                        Office_unitsDTO usersAllowedOfficeUnitDTO = getOfficeUnitFromUser(userDTO);
                        long usersAllowedOfficeUnitId = usersAllowedOfficeUnitDTO == null ? -1 : usersAllowedOfficeUnitDTO.iD;
                        String restrictedViewWhereClause = String.format(
                                Driver_ot_billDAO.restrictedViewWhereClause,
                                usersAllowedOfficeUnitId,
                                userDTO.organogramID
                        );
                        request.setAttribute("Driver_ot_billServlet.restrictedViewWhereClause", restrictedViewWhereClause);
                    }
                    String isInPreview = request.getParameter("isInPreview");
                    if (isInPreview == null) {
                        request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                    }
                    super.doGet(request, response);
                    return;
                }
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    public String getWhereClause(HttpServletRequest request) {
        return (String) request.getAttribute("Driver_ot_billServlet.restrictedViewWhereClause");
    }

    @Override
    public String getSortClause(HttpServletRequest request) {
        return " approval_status ASC, lastModificationTime DESC ";
    }

    public static Office_unitsDTO getOfficeUnitFromUser(UserDTO userDTO) {
        long organogramId = userDTO.organogramID;
        OfficeUnitOrganograms organogramDTO = OfficeUnitOrganogramsRepository.getInstance().getById(organogramId);
        if (organogramDTO == null) {
            return null;
        }
        return Office_unitsRepository.getInstance().getOffice_unitsDTOByID(organogramDTO.office_unit_id);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        String actionType = request.getParameter("actionType");
        try {
            switch (actionType) {
                case "ajax_approve": {
                    Map<String, Object> res = new HashMap<>();
                    try {
                        approve(request, userDTO);
                        res.put("success", true);
                        res.put("message", "");
                    } catch (Exception ex) {
                        logger.error(ex);
                        res.put("success", false);
                        res.put("message", ex.getMessage());
                    }
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().print(gson.toJson(res));
                    return;
                }
                case "ajax_submit": {
                    Map<String, Object> res = submitDriverOvertimeBill(request, userDTO);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().print(gson.toJson(res));
                    return;
                }
                case "ajax_deleteBill": {
                    Map<String, Object> res = deleteBill(request, userDTO);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().print(gson.toJson(res));
                    return;
                }
                case "ajax_saveConfig": {
                    if (Utils.checkPermission(userDTO, MenuConstants.DRIVER_OT_BILL_CONFIG_ADD)) {
                        try {
                            saveConfig(request, userDTO);
                            ApiResponse.sendSuccessResponse(response, "Driver_ot_billServlet?actionType=search");
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                    } else {
                        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
                        ApiResponse.sendErrorResponse(response, isLangEng ? "You do not have permission to do this task" : "আপনার এই ক্রিয়া সম্পাদনের অনুমতি নেই");
                    }
                    return;
                }
                default: {
                    super.doPost(request, response);
                    return;
                }
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void saveConfig(HttpServletRequest request, UserDTO userDTO) throws Exception {
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;

        long id = Utils.parseMandatoryLong(
                request.getParameter("iD"),
                isLangEng ? "No configuration found" : "কোন কনফিগারেশন পাওয়া যায়নি"
        );
        Driver_ot_bill_configDTO driverOtBillConfigDTO = Driver_ot_bill_configDAO.getInstance().getDTOFromID(id);
        if (driverOtBillConfigDTO == null) {
            throw new CustomException(isLangEng ? "No configuration found" : "কোন কনফিগারেশন পাওয়া যায়নি");
        }
        driverOtBillConfigDTO.modifiedBy = userDTO.ID;
        driverOtBillConfigDTO.lastModificationTime = System.currentTimeMillis();

        driverOtBillConfigDTO.ordinanceText = Utils.doJsoupCleanOrReturnDefault(
                request.getParameter("ordinanceText"),
                driverOtBillConfigDTO.ordinanceText
        );
        driverOtBillConfigDTO.minOfficeHour = Utils.parseMandatoryInt(
                request.getParameter("minOfficeHour"),
                driverOtBillConfigDTO.minOfficeHour,
                isLangEng ? "Select Office Time (Morning)" : "সকালের অফিসের সময় বাছাই করুন"
        );
        driverOtBillConfigDTO.maxOfficeHour = Utils.parseMandatoryInt(
                request.getParameter("maxOfficeHour"),
                driverOtBillConfigDTO.maxOfficeHour,
                isLangEng ? "Select Office Time (Afternoon)" : "বিকালের অফিসের সময় বাছাই করুন"
        );
        if (driverOtBillConfigDTO.minOfficeHour > driverOtBillConfigDTO.maxOfficeHour) {
            throw new CustomException(isLangEng ? "Office end time has to be after begin time" : "অফিসের শেষ সময় শুরু হওয়ার পরে হতে হবে");
        }

        driverOtBillConfigDTO.minExtraHour = Utils.parseMandatoryInt(
                request.getParameter("minExtraHour"),
                driverOtBillConfigDTO.minExtraHour,
                isLangEng ? "Select Extra Hour Start (Morning)" : "অতিরিক্ত সময় শুরু (সকাল) বাছাই করুন"
        );
        if (driverOtBillConfigDTO.minExtraHour > driverOtBillConfigDTO.minOfficeHour) {
            throw new CustomException(isLangEng ? "Extra Hour must start before office start" : "অতিরিক্ত সময় অফিস শুরুর আগে শুরু হতে হবে");
        }

        driverOtBillConfigDTO.maxExtraHour = Utils.parseMandatoryInt(
                request.getParameter("maxExtraHour"),
                driverOtBillConfigDTO.maxExtraHour,
                isLangEng ? "Select Extra Hour End (Afternoon/Night)" : "অতিরিক্ত সময় শেষ (বিকাল/রাত) বাছাই করুন"
        );
        if (driverOtBillConfigDTO.maxExtraHour < driverOtBillConfigDTO.maxOfficeHour) {
            throw new CustomException(isLangEng ? "Extra Hour must end before office ends" : "অতিরিক্ত সময় অফিস শেষের পরে শেষ হতে হবে");
        }

        driverOtBillConfigDTO.maxOvertimeHours = Utils.parseMandatoryInt(
                request.getParameter("maxOvertimeHours"),
                driverOtBillConfigDTO.maxOvertimeHours,
                isLangEng ? "Input Maximum number of hours of overtime paid" : "সর্বোচ্চ যত ঘন্টা অতিরিক্ত সময়ের টাকা পাবে লিখুন"
        );

        Driver_ot_bill_configDAO.getInstance().update(driverOtBillConfigDTO);
    }

    private void approve(HttpServletRequest request, UserDTO userDTO) throws Exception {
        Long id = Utils.parseOptionalLong(request.getParameter("id"), null, null);
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";

        Driver_ot_billDTO driverOtBillDTO = Driver_ot_billDAO.getInstance().getDTOFromID(id);
        // not done approval history editability check, because it is done inside Bill_approval_historyDAO.getInstance().approve()
        driverOtBillDTO.checkEditabilityOrThrowException(userDTO, language, false);
        List<Bill_approval_historyDTO> nextApprovalHistoryDTOs = approveAndGetNextLayerApprovers(driverOtBillDTO, userDTO);
        Driver_ot_billNotification.getInstance().send(driverOtBillDTO, nextApprovalHistoryDTOs);
    }

    private Map<String, Object> submitDriverOvertimeBill(HttpServletRequest request, UserDTO userDTO) {
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        boolean isLangEn = language.equalsIgnoreCase("English");
        Long id = Utils.parseOptionalLong(request.getParameter("id"), null, null);
        Map<String, Object> res = new HashMap<>();
        res.put("success", false);
        res.put("message", getServletName() + "?actionType=search");

        if (id == null) {
            res.put("message", isLangEn ? "Invalid id" : "ভুল আইডি");
            return res;
        }
        Driver_ot_billDTO driverOtBillDTO = Driver_ot_billDAO.getInstance().getDTOFromID(id);
        if (driverOtBillDTO == null) {
            res.put("message", (isLangEn ? "No record found with id=" : "কোন তথ্য পাওয়া যায়নি যার আইডি=") + id);
            return res;
        }
        try {
            Utils.handleTransaction(() -> {
                driverOtBillDTO.checkEditabilityOrThrowException(userDTO, language);
                driverOtBillDTO.setPreparedInfo(userDTO, language);
                Driver_ot_billDAO.getInstance().update(driverOtBillDTO);
                List<Bill_approval_historyDTO> historyOfCurrentLevel = Bill_approval_historyDAO.getInstance().findByLevel(
                        driverOtBillDTO.getTableName(), driverOtBillDTO.iD, driverOtBillDTO.currentApprovalLevel
                );
                Optional<Bill_approval_historyDTO> usersHistoryOptional = Bill_approval_historyDTO.findUsersHistory(historyOfCurrentLevel, userDTO);
                List<Bill_approval_historyDTO> nextApprovalHistoryDTOs;
                if (usersHistoryOptional.isPresent()) {
                    nextApprovalHistoryDTOs = approveAndGetNextLayerApprovers(driverOtBillDTO, userDTO);
                } else {
                    nextApprovalHistoryDTOs = historyOfCurrentLevel;
                }
                Driver_ot_billNotification.getInstance().send(driverOtBillDTO, nextApprovalHistoryDTOs);
            });
        } catch (CustomException e) {
            logger.error(e);
            res.put("message", e.getMessage());
            return res;
        } catch (Exception e) {
            logger.error(e);
            res.put("message", isLangEn ? "Failed to submit" : "সাবমিট করতে ব্যর্থ হয়েছে");
            return res;
        }
        res.put("success", true);
        return res;
    }

    private List<Bill_approval_historyDTO> approveAndGetNextLayerApprovers(Driver_ot_billDTO driverOtBillDTO, UserDTO userDTO) throws Exception {
        BillApprovalResponse approvalResponse =
                Bill_approval_historyDAO.getInstance()
                                        .approveWitAlreadyAddedApprover(driverOtBillDTO, userDTO);
        if (!approvalResponse.hasNextApproval) {
            driverOtBillDTO.approvalStatus = BillApprovalStatus.APPROVED.getValue();
        }
        driverOtBillDTO.currentApprovalLevel = approvalResponse.nextLevel;
        driverOtBillDTO.modifiedBy = userDTO.ID;
        driverOtBillDTO.lastModificationTime = System.currentTimeMillis();
        Driver_ot_billDAO.getInstance().update(driverOtBillDTO);
        return approvalResponse.nextApprovalHistoryDTOs;
    }

    public void deleteBillDtoOrThrow(String idStr, UserDTO userDTO, boolean isLangEn) throws Exception {
        long id = Utils.parseMandatoryLong(idStr, "invalid id=" + idStr);
        Driver_ot_billDTO billDTO = Driver_ot_billDAO.getInstance().getDTOFromID(id);
        if (billDTO == null) {
            throw new CustomException(isLangEn ? "Already deleted!" : "ইতোমধ্যে ডিলিট করা হয়েছে!");
        }
        if (!billDTO.isDeletableByUser(userDTO)) {
            throw new CustomException(isLangEn ? "You are not allowed to delete" : "আপনার ডিলিট করার অনুমতি নেই");
        }
        if (billDTO.hasFinancePreparedBill()) {
            throw new CustomException(
                    isLangEn ? "Unable to delete because Finance has already prepared this bill!"
                             : "ডিলিট করতে ব্যর্থ হয়েছে কারণ অর্থ শাখা বিল প্রস্তুত করে ফেলেছে"
            );
        }
        boolean isDeleted = Driver_ot_billDAO.getInstance().deleteBillById(id, userDTO.ID, System.currentTimeMillis());
        if (!isDeleted) {
            throw new RuntimeException("failed to delete overtime_billDTO.id=" + id);
        }
    }

    private Map<String, Object> deleteBill(HttpServletRequest request, UserDTO userDTO) {
        boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Map<String, Object> res = new HashMap<>();
        res.put("success", true);
        res.put("message", isLangEn ? "Deleted successfully" : "সফল ভাবে ডিলিট করা হয়েছে");

        try {
            deleteBillDtoOrThrow(request.getParameter("ID"), userDTO, isLangEn);
        } catch (CustomException customException) {
            logger.error(customException);
            res.put("success", false);
            res.put("message", customException.getMessage());
        } catch (Exception e) {
            logger.error(e);
            res.put("success", false);
            res.put("message", isLangEn ? "Failed to delete" : "ডিলিট করতে ব্যর্থ হয়েছে");
        }
        return res;
    }

    public static boolean isAllowedToSeeAllOfficeBill(UserDTO userDTO) {
        if (userDTO == null) return false;
        return userDTO.roleID == RoleEnum.ADMIN.getRoleId()
               || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId()
               || userDTO.roleID == RoleEnum.DATA_ENTRY.getRoleId();
    }

    private DriverOtBillTableData getDriverOtBillData(Long employeeOfficesId, Long otBillSubmissionConfigId) {
        boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        DriverOtBillTableData tableData = new DriverOtBillTableData();
        if (employeeOfficesId == null) {
            tableData.errorMessage = isLangEn ? "Select Driver" : "গাড়ী চালক বাছাই করুন";
            return tableData;
        }
        if (otBillSubmissionConfigId == null) {
            tableData.errorMessage = isLangEn ? "Bill Date" : "বিলের তারিখ";
            return tableData;
        }
        Driver_ot_bill_configDTO driverOtBillConfigDTO = Driver_ot_bill_configRepository.getInstance().getConfig();
        if (driverOtBillConfigDTO == null) {
            tableData.errorMessage = isLangEn ? "No configuration found for Office , extra hour" : "অফিসে, অতিরিক্ত সময়ের জন্য কোন কনফিগারেশন পাওয়া যায়নি";
            return tableData;
        }
        tableData.minExtraHour = driverOtBillConfigDTO.minExtraHour;
        tableData.minOfficeHour = driverOtBillConfigDTO.minOfficeHour;
        tableData.maxOfficeHour = driverOtBillConfigDTO.maxOfficeHour;
        tableData.maxExtraHour = driverOtBillConfigDTO.maxExtraHour;
        Driver_ot_billDTO driverOtBillDTO = Driver_ot_billDAO.getInstance().getBillForEmployee(employeeOfficesId, otBillSubmissionConfigId);
        if (driverOtBillDTO != null && !driverOtBillDTO.isInPreviewStage) {
            tableData.errorMessage = isLangEn ? "This bill is already submitted" : "এই বিল ইতিমধ্যেই জমা দেওয়া হয়েছে";
            return tableData;
        }
        OT_bill_submission_configDTO submissionConfigDTO = OT_bill_submission_configDAO.getInstance().getDTOFromID(otBillSubmissionConfigId);
        if (submissionConfigDTO == null) {
            tableData.errorMessage = isLangEn ? "No configuration found for Bill Date" : "বিলের তারিখ জন্য কোন কনফিগারেশন পাওয়া যায়নি";
            return tableData;
        }
        tableData.driverOtDetailModels =
                Driver_ot_detailDAO.getInstance().buildSortedModelList(
                        submissionConfigDTO.billStartDate,
                        submissionConfigDTO.billEndDate,
                        driverOtBillConfigDTO
                );
        if (tableData.driverOtDetailModels.isEmpty()) {
            tableData.errorMessage = isLangEn ? "No date found for Bill Date" : "বিলের তারিখ জন্য কোন তারিখ পাওয়া যায়নি";
            return tableData;
        }
        tableData.isEditable = true;
        return tableData;
    }

    private UserInput parseUserInput(HttpServletRequest request, UserDTO userDTO) throws Exception {
        UserInput userInput = new UserInput();
        userInput.isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        userInput.language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        userInput.employeeOfficesId = Utils.parseMandatoryLong(
                request.getParameter("employeeOfficesId"),
                userInput.isLangEn ? "Select Driver" : "গাড়ী চালক বাছাই করুন"
        );
        userInput.otBillSubmissionConfigId = Utils.parseMandatoryLong(
                request.getParameter("otBillSubmissionConfigId"),
                userInput.isLangEn ? "Select Bill Date" : "বিলের তারিখ বাছাই করুন"
        );
        userInput.modifiedTime = System.currentTimeMillis();
        userInput.modifierId = userDTO.ID;
        try {
            userInput.driverOtDetailModels =
                    Arrays.stream(gson.fromJson(request.getParameter("driverOtDetailModels"), DriverOtDetailModel[].class))
                          .collect(Collectors.toList());
        } catch (Exception ex) {
            logger.error(ex);
            throw new CustomException(userInput.isLangEn ? "Wrong Employee Info" : "কর্মকর্তা/কর্মচারীর তথ্য ভুল দেয়া হয়েছে");
        }
        return userInput;
    }

    private List<String> getVehicleIdsLatestFirst(List<Vm_vehicle_driver_assignmentDTO> assignedVehicles) {
        Comparator<Vm_vehicle_driver_assignmentDTO> compareLastModifyTimeReversed =
                Comparator.comparingLong((Vm_vehicle_driver_assignmentDTO dto) -> dto.lastModificationTime)
                          .reversed();

        return assignedVehicles
                .stream()
                .sorted(compareLastModifyTimeReversed)
                .map(dto -> dto.vehicleIdMultiple)
                .flatMap(commaSeperatedIds -> Arrays.stream(commaSeperatedIds.split(",")))
                .collect(Collectors.toList());
    }

    private void updateConfigurationData(Driver_ot_billDTO driverOtBillDTO, UserInput userInput) {
        // office information
        CustomException driverNotFoundError = new CustomException(userInput.isLangEn ? "Driver Information not Found" : "ড্রাইভারের তথ্য পাওয়া যায়নি");
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getById(userInput.employeeOfficesId);
        if (employeeOfficeDTO == null) {
            throw driverNotFoundError;
        }
        driverOtBillDTO.employeeOfficesId = userInput.employeeOfficesId;
        driverOtBillDTO.employeeRecordId = employeeOfficeDTO.employeeRecordId;
        driverOtBillDTO.officeUnitId = employeeOfficeDTO.officeUnitId;
        driverOtBillDTO.organogramId = employeeOfficeDTO.officeUnitOrganogramId;

        // employee information
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(driverOtBillDTO.employeeRecordId);
        if (employeeRecordsDTO == null) {
            throw driverNotFoundError;
        }
        driverOtBillDTO.employeeNameBn = employeeRecordsDTO.nameBng;
        driverOtBillDTO.basicSalary = Grade_wise_pay_scaleRepository.getInstance().getPayScale(employeeOfficeDTO.gradeTypeLevel);
        if (driverOtBillDTO.basicSalary < 0) {
            throw new CustomException(userInput.isLangEn ? "Driver Basic salary is invalid" : "ড্রাইভারের মূল বেতন ঠিক নেই");
        }
        Employee_bank_informationDTO savingsAccountInfoDTO =
                Employee_bank_informationDAO.getInstance().getEmployeeSavingsAccountInfoByEmployeeId(driverOtBillDTO.employeeRecordId);
        if (savingsAccountInfoDTO != null) {
            driverOtBillDTO.bankAccountNumber = savingsAccountInfoDTO.bankAccountNumber;
        }

        driverOtBillDTO.otBillSubmissionConfigId = userInput.otBillSubmissionConfigId;
        OT_bill_submission_configDTO submissionConfigDTO = OT_bill_submission_configDAO.getInstance().getDTOFromID(userInput.otBillSubmissionConfigId);
        if (submissionConfigDTO == null) {
            throw new CustomException(userInput.isLangEn ? "No configuration found for Bill Date" : "বিলের তারিখ জন্য কোন কনফিগারেশন পাওয়া যায়নি");
        }
        driverOtBillDTO.billStartDate = submissionConfigDTO.billStartDate;
        driverOtBillDTO.billEndDate = submissionConfigDTO.billEndDate;
        driverOtBillDTO.overtimeBillTypeCat = submissionConfigDTO.overtimeBillTypeCat;

        List<Vm_vehicle_driver_assignmentDTO> assignedVehicles = Vm_vehicle_driver_assignmentDAO.getInstance().getByEmployeeAndTimeRange(
                driverOtBillDTO.employeeRecordId,
                driverOtBillDTO.billStartDate,
                driverOtBillDTO.billEndDate
        );
        List<String> vehicleIdsLatestFirst = getVehicleIdsLatestFirst(assignedVehicles);
        CustomException noVehicleFoundError = new CustomException(userInput.isLangEn ? "No assigned vehicle found for driver" : "ড্রাইভারের জন্য নির্ধারিত কোনো গাড়ি পাওয়া যায়নি");
        if (vehicleIdsLatestFirst.isEmpty()) {
            throw noVehicleFoundError;
        }
        driverOtBillDTO.vehicleIds = String.join(",", vehicleIdsLatestFirst);

        long latestVehicleId = Long.parseLong(vehicleIdsLatestFirst.get(0));
        Vm_vehicleDTO vehicle = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(latestVehicleId);
        if (vehicle == null) {
            throw noVehicleFoundError;
        }
        driverOtBillDTO.vehicleId = vehicle.iD;
        driverOtBillDTO.vehicleNumberBn = StringUtils.convertToBanNumber(vehicle.regNo);
        driverOtBillDTO.vehicleTypeBn = CatRepository.getInstance().getText(false, "vehicle_type", vehicle.vehicleTypeCat);

        driverOtBillDTO.taskTypeId = TASK_TYPE_ID;
        driverOtBillDTO.budgetMappingId = DRIVER_OT_BILL_BUDGET_MAPPING_ID;
        driverOtBillDTO.economicSubCodeId = DRIVER_OT_BILL_SUB_CODE_ID;

        Driver_ot_bill_configDTO driverOtBillConfigDTO = Driver_ot_bill_configRepository.getInstance().getConfig();
        if (driverOtBillConfigDTO == null) {
            throw new CustomException(userInput.isLangEn ? "No configuration found for Office , extra hour" : "অফিসে, অতিরিক্ত সময়ের জন্য কোন কনফিগারেশন পাওয়া যায়নি");
        }
        driverOtBillDTO.ordinanceText = driverOtBillConfigDTO.ordinanceText;
        driverOtBillDTO.minOfficeHour = driverOtBillConfigDTO.minOfficeHour;
        driverOtBillDTO.maxOfficeHour = driverOtBillConfigDTO.maxOfficeHour;
        driverOtBillDTO.minExtraHour = driverOtBillConfigDTO.minExtraHour;
        driverOtBillDTO.maxExtraHour = driverOtBillConfigDTO.maxExtraHour;
        driverOtBillDTO.maxOvertimeHours = driverOtBillConfigDTO.maxOvertimeHours;
    }

    private Driver_ot_detailDTO buildDetailDto(DriverOtDetailModel detailModel, UserInput userInput, Driver_ot_detailDTO detailDTO) {
        if (detailDTO == null) {
            detailDTO = new Driver_ot_detailDTO();
            detailDTO.insertedBy = userInput.modifierId;
            detailDTO.insertionTime = userInput.modifiedTime;
        }
        detailDTO.modifiedBy = userInput.modifierId;
        detailDTO.lastModificationTime = userInput.modifiedTime;

        detailDTO.driverOtDate = detailModel.driverOtDate;
        detailDTO.driverOtDateType = DriverOtDateType.getByValue(detailModel.driverOtDateType);

        detailDTO.officeHourFrom = detailModel.officeHourFrom;
        detailDTO.officeHourTo = detailModel.officeHourTo;

        detailDTO.extraHourMorningFrom = detailModel.extraHourMorningFrom;
        detailDTO.extraHourMorningTo = detailModel.extraHourMorningTo;

        detailDTO.extraHourAfternoonFrom = detailModel.extraHourAfternoonFrom;
        detailDTO.extraHourAfternoonTo = detailModel.extraHourAfternoonTo;

        return detailDTO;
    }

    private Driver_ot_billDTO updateAndSaveDriverOtBillDTO(Driver_ot_billDTO driverOtBillDTO, UserInput userInput) throws Exception {
        if (driverOtBillDTO == null) {
            throw new CustomException(userInput.isLangEn ? "No record found to edit" : "এডিট করার মত কোন তথ্য পাওয়া যায়নি");
        }
        Driver_ot_detailDAO driverOtDetailDAO = Driver_ot_detailDAO.getInstance();

        Map<Long, Driver_ot_detailDTO> billDetailsByDate = new HashMap<>();
        if (driverOtBillDTO.iD > 0) {
            billDetailsByDate = driverOtDetailDAO.getByDriverOtBillId(driverOtBillDTO.iD)
                                                 .stream()
                                                 .collect(Collectors.toMap(
                                                         detailDTO -> detailDTO.driverOtDate,
                                                         Function.identity(),
                                                         (e1, e2) -> e1
                                                 ));
        }

        List<DriverOtDetailModel> driverOtDetailModels =
                userInput.driverOtDetailModels
                        .stream()
                        .sorted(Comparator.comparingLong(dto -> dto.driverOtDate))
                        .peek(DriverOtDetailModel::validateRanges)
                        .peek(DriverOtDetailModel::calculateTotalHours)
                        .collect(Collectors.toList());

        List<Driver_ot_detailDTO> driverOtDetailDTOs = new ArrayList<>();

        driverOtBillDTO.overtimeDays = 0;
        driverOtBillDTO.overtimeHours = 0;
        for (DriverOtDetailModel detailModel : driverOtDetailModels) {
            Driver_ot_detailDTO alreadyAddedDto = billDetailsByDate.get(detailModel.driverOtDate);
            Driver_ot_detailDTO otDetailDTO = buildDetailDto(detailModel, userInput, alreadyAddedDto);
            driverOtDetailDTOs.add(otDetailDTO);
            billDetailsByDate.remove(detailModel.driverOtDate);
            ++driverOtBillDTO.overtimeDays;
            driverOtBillDTO.overtimeHours += detailModel.totalExtraHour;
        }
        driverOtBillDTO.calculateAndSetBillAmount();
        List<Long> idsToBeDeleted =
                billDetailsByDate.values()
                                 .stream()
                                 .map(detailDTO -> detailDTO.iD)
                                 .collect(Collectors.toList());
        Driver_ot_billDAO driverOtBillDAO = Driver_ot_billDAO.getInstance();
        Utils.handleTransaction(() -> {
            if (driverOtBillDTO.iD < 0) {
                driverOtBillDAO.add(driverOtBillDTO);
            } else {
                driverOtBillDAO.update(driverOtBillDTO);
            }
            driverOtBillDAO.insertApproverIntoBillHistory(driverOtBillDTO);

            for (Driver_ot_detailDTO driverOtDetailDTO : driverOtDetailDTOs) {
                driverOtDetailDTO.driverOtBillId = driverOtBillDTO.iD;
                if (driverOtDetailDTO.iD < 0) {
                    driverOtDetailDAO.add(driverOtDetailDTO);
                } else {
                    driverOtDetailDAO.update(driverOtDetailDTO);
                }
            }

            if (idsToBeDeleted.size() > 0) {
                driverOtDetailDAO.delete(userInput.modifierId, idsToBeDeleted, userInput.modifiedTime);
            }

            driverOtBillDAO.update(driverOtBillDTO);
        });
        return driverOtBillDTO;
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getServletName() + "?actionType=view&ID=" + commonDTO.iD;
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAjaxAddRedirectURL(request, commonDTO);
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        UserInput userInput = parseUserInput(request, userDTO);
        long currentTime = System.currentTimeMillis();
        Driver_ot_billDTO driverOtBillDTO;
        if (addFlag) {
            driverOtBillDTO = Driver_ot_billDAO.getInstance().getBillForEmployee(userInput.employeeOfficesId, userInput.otBillSubmissionConfigId);
            if (driverOtBillDTO != null && !driverOtBillDTO.isInPreviewStage) {
                throw new CustomException(String.format(
                        "can not ADD. driver overtime bill already submitted. employeeOfficesId=%d, otBillSubmissionConfigId=%d",
                        userInput.employeeOfficesId, userInput.otBillSubmissionConfigId
                ));
            }
            OT_bill_submission_configDTO submissionConfigDTO = OT_bill_submission_configDAO.getInstance().getDTOFromID(userInput.otBillSubmissionConfigId);
            if (submissionConfigDTO == null) {
                throw new CustomException(
                        userInput.isLangEn ? "No configuration found for Bill Date"
                                           : "বিলের তারিখ জন্য কোন কনফিগারেশন পাওয়া যায়নি"
                );
            }
            boolean isAllowedToPrepareBill = submissionConfigDTO.isAllowedToSubmitByTime(System.currentTimeMillis());
            if (!isAllowedToPrepareBill) {
                throw new CustomException(
                        userInput.isLangEn ? "Not Allowed to prepare this bill date now"
                                           : "এখন এই বিলের তারিখের বিল প্রস্তুত করার অনুমতি নেই"
                );
            }
            if (driverOtBillDTO == null) {
                driverOtBillDTO = new Driver_ot_billDTO();
                driverOtBillDTO.insertedBy = userDTO.ID;
                driverOtBillDTO.insertionTime = currentTime;
            }
            updateConfigurationData(driverOtBillDTO, userInput);
        } else {
            driverOtBillDTO = Driver_ot_billDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("id")));
        }
        return updateAndSaveDriverOtBillDTO(driverOtBillDTO, userInput);
    }

    static class UserInput {
        Long employeeOfficesId, otBillSubmissionConfigId;
        List<DriverOtDetailModel> driverOtDetailModels;
        Long modifiedTime, modifierId;
        Boolean isLangEn;
        String language;
    }

    @Override
    public String getServletName() {
        return "Driver_ot_billServlet";
    }

    @Override
    public Driver_ot_billDAO getCommonDAOService() {
        return Driver_ot_billDAO.getInstance();
    }

    @Override
    public String getTableName() {
        return getCommonDAOService().getTableName();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.DRIVER_OT_BILL_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.DRIVER_OT_BILL_ADD};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.DRIVER_OT_BILL_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Driver_ot_billServlet.class;
    }
}
