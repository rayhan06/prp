package driver_ot_bill;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Driver_ot_bill_configDAO implements CommonDAOService<Driver_ot_bill_configDTO> {
    private static final Logger logger = Logger.getLogger(Driver_ot_bill_configDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (ordinance_text,min_office_hour,max_office_hour,min_extra_hour,max_extra_hour,max_overtime_hours,"
                    .concat("modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET ordinance_text=?,min_office_hour=?,max_office_hour=?,min_extra_hour=?,max_extra_hour=?,max_overtime_hours=?,"
                    .concat("modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private Driver_ot_bill_configDAO() {
    }

    private static class LazyLoader {
        static final Driver_ot_bill_configDAO INSTANCE = new Driver_ot_bill_configDAO();
    }

    public static Driver_ot_bill_configDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Driver_ot_bill_configDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setString(++index, dto.ordinanceText);
        ps.setInt(++index, dto.minOfficeHour);
        ps.setInt(++index, dto.maxOfficeHour);
        ps.setInt(++index, dto.minExtraHour);
        ps.setInt(++index, dto.maxExtraHour);
        ps.setInt(++index, dto.maxOvertimeHours);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Driver_ot_bill_configDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Driver_ot_bill_configDTO dto = new Driver_ot_bill_configDTO();
            dto.iD = rs.getLong("ID");
            dto.ordinanceText = rs.getString("ordinance_text");
            dto.minOfficeHour = rs.getInt("min_office_hour");
            dto.maxOfficeHour = rs.getInt("max_office_hour");
            dto.minExtraHour = rs.getInt("min_extra_hour");
            dto.maxExtraHour = rs.getInt("max_extra_hour");
            dto.maxOvertimeHours = rs.getInt("max_overtime_hours");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "driver_ot_bill_config";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        throw new UnsupportedOperationException("Driver_ot_bill_configDTO dont support add operation");
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Driver_ot_bill_configDTO) commonDTO, updateSqlQuery, false);
    }
}
