package driver_ot_bill;

import bill_approval_history.ApprovableDTO;
import bill_approval_history.BillApprovalStatus;
import bill_approval_history.Bill_approval_historyDAO;
import bill_approval_history.Bill_approval_historyDTO;
import common.CustomException;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import pb.CatRepository;
import pbReport.DateUtils;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;
import util.StringUtils;

import java.util.Optional;

import static overtime_bill.Overtime_billStatus.FINANCE_BILL_PREPARED;
import static overtime_bill.Overtime_billStatus.PENDING;

@SuppressWarnings({"Duplicates"})
public class Driver_ot_billDTO extends CommonDTO  implements ApprovableDTO {
    public boolean isInPreviewStage = true;

    public long employeeOfficesId = -1;
    public long employeeRecordId = -1;
    public String employeeNameBn = "";
    public long officeUnitId = -1;
    public long organogramId = -1;
    public String vehicleIds = "";
    public long vehicleId;
    public String vehicleNumberBn = "";
    public String vehicleTypeBn;
    public int basicSalary = -1;
    public String bankAccountNumber = "";

    public long otBillSubmissionConfigId = -1;
    public long billStartDate = SessionConstants.MIN_DATE;
    public long billEndDate = SessionConstants.MIN_DATE;
    public int overtimeBillTypeCat = 1;
    public long budgetMappingId = -1;
    public long economicSubCodeId = -1;
    public String ordinanceText = "";
    public int minOfficeHour;
    public int maxOfficeHour;
    public int minExtraHour;
    public int maxExtraHour;
    public int maxOvertimeHours;

    public int overtimeDays;
    public int overtimeHours;
    public long billAmount;
    public long preparedOrgId;
    public String preparedNameBn;
    public String preparedOrgNameBn;
    public String preparedOfficeNameBn;
    public byte[] preparedSignature;
    public long preparedTime;

    public int currentApprovalLevel;
    public int approvalStatus = 1;
    public Long taskTypeId;

    // ----> not yet added in DB
    public int financeBillStatus = PENDING.getValue();
    public int financeSerialNumber = -1;
    // <---- not yet added in DB

    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = -1;

    public String getBillDateTypeText(boolean isLangEn) {
        return String.format("%s (%s)", getBillDateRangeText(isLangEn), getBillTypeText(isLangEn));
    }

    public String getBillDateRangeText(boolean isLangEn) {
        return DateUtils.getDateRangeString(
                billStartDate,
                billEndDate,
                isLangEn
        );
    }

    public String getBillTypeText(boolean isLangEn) {
        return CatRepository.getInstance().getText(
                isLangEn,
                SessionConstants.OT_BILL_TYPE_CAT_DOMAIN_NAME,
                overtimeBillTypeCat
        );
    }

    public String getEmployeeInfo(boolean isLangEn) {
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeRecordId);
        if (employeeRecordsDTO == null) {
            return "";
        }
        String username = employeeRecordsDTO.employeeNumber;
        String name = employeeRecordsDTO.nameEng;
        if (!isLangEn) {
            username = StringUtils.convertToBanNumber(username);
            name = employeeRecordsDTO.nameBng;
        }
        return String.format("%s - %s", username, name);
    }

    public static double getRate(double a, double b) {
        double scale = Math.pow(10, 2);
        return Math.round((a / (b + .0)) * scale) / scale;
    }

    public static String getRateStringForView(double a) {
        return String.format("%.02f", a);
    }

    public int getMaxTotalOfficeHours() {
        return maxOfficeHour - minOfficeHour;
    }

    public double getSalaryPerDay() {
        return getRate(basicSalary, overtimeDays);
    }

    public double getHourlyRate() {
        return getRate(getSalaryPerDay(), getMaxTotalOfficeHours());
    }

    public void calculateAndSetBillAmount() {
        int billOvertimeHours = Math.min(overtimeHours, maxOvertimeHours);
        billAmount = (long) Math.floor(getHourlyRate() * billOvertimeHours);
    }

    public boolean isApproved() {
        if (isInPreviewStage) {
            return false;
        }
        return approvalStatus == BillApprovalStatus.APPROVED.getValue();
    }

    public boolean hasFinancePreparedBill() {
        return financeBillStatus == FINANCE_BILL_PREPARED.getValue();
    }

    public Optional<Bill_approval_historyDTO> checkEditabilityOrThrowException(UserDTO userDTO, String language, boolean doApprovalHistoryCheck) throws IllegalAccessException {
        boolean isLangEng = "english".equalsIgnoreCase(language);
        if (isApproved()) {
            throw new IllegalAccessException(
                    isLangEng ? "Approval has been completed. Not Editable Now."
                              : "অনুমোদন সম্পন্ন হয়েছে এখন পরিবর্তনযোগ্য নয়"
            );
        }
        if (hasFinancePreparedBill()) {
            throw new IllegalAccessException(
                    isLangEng ? "Finance already prepared bill. Can not edit!"
                              : "অর্থশাখা বিল প্রস্তুত করে ফেলেছে। এখন এডিটযোগ্য নয়!"
            );
        }

        if (isInPreviewStage) {
            return Optional.empty();
        }

        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(userDTO.employee_record_id);
        if (employeeOfficeDTO == null) {
            throw new IllegalAccessException(
                    isLangEng ? "You are not allowed to edit"
                              : "আপনার এডিটের অনুমতি নেই"
            );
        }
        if (doApprovalHistoryCheck) {
            Optional<Bill_approval_historyDTO> optionalApprovalHistory =
                    Bill_approval_historyDAO
                            .getInstance()
                            .findByLevel(getTableName(), iD, currentApprovalLevel)
                            .stream()
                            .filter(approvalHistoryDTO -> approvalHistoryDTO.isThisUsersHistory(userDTO))
                            .findAny();
            if(optionalApprovalHistory.isPresent()) {
                return optionalApprovalHistory;
            }
            throw new IllegalAccessException(isLangEng ? "You are not allowed to approve" : "আপনার অনুমোদন করার অনুমতি নেই");
        }
        return Optional.empty();
    }

    public Optional<Bill_approval_historyDTO> checkEditabilityOrThrowException(UserDTO userDTO, String language) throws IllegalAccessException {
        return checkEditabilityOrThrowException(userDTO, language, true);
    }

    public boolean isEditable(UserDTO userDTO, String language) {
        try {
            checkEditabilityOrThrowException(userDTO, language);
            return true;
        } catch (IllegalAccessException ex) {
            return false;
        }
    }

    public boolean isDeletableByUser(UserDTO userDTO) {
        if (isInPreviewStage) {
            return true;
        }
        return Driver_ot_billServlet.isAllowedToSeeAllOfficeBill(userDTO);
    }

    public void setPreparedInfo(UserDTO userDTO, String language)  {
        boolean isLangEng = "english".equalsIgnoreCase(language);
        if (isApproved()) {
            throw new CustomException(
                    isLangEng ? "Approval has been completed. Can Not Prepare Bill Now."
                              : "অনুমোদন সম্পন্ন হয়েছে এখন বিল প্রস্তুতযোগ্য নয়"
            );
        }
        if (hasFinancePreparedBill()) {
            throw new CustomException(
                    isLangEng ? "Finance already prepared bill. Can Not Prepare Bill Now!"
                              : "অর্থশাখা বিল প্রস্তুত করে ফেলেছে। এখন বিল প্রস্তুতযোগ্য নয়!"
            );
        }

        if (!isInPreviewStage) {
            throw new CustomException(
                    isLangEng ? "Bill has already been prepared."
                              : "বিল ইতো মধ্যে প্রস্তুত করা হয়ে গেছে!"
            );
        }

        isInPreviewStage = false;
        preparedOrgId = -1;
        preparedNameBn = "(এডমিন)";
        preparedOfficeNameBn = "(এডমিন)";
        preparedOrgNameBn = "(এডমিন)";
        preparedSignature = null;
        preparedTime = lastModificationTime = System.currentTimeMillis();
        modifiedBy = userDTO.ID;

        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(userDTO.employee_record_id);
        if (employeeOfficeDTO == null) return;

        Employee_recordsDTO employeeRecords = Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId);
        if (employeeRecords != null) {
            if(employeeRecords.employeeClass == 4) {
                throw new CustomException(
                        isLangEng ? "4th Class Employee can not submit bill"
                                  : "৪র্থ শ্রেণির কর্মচারী বিল জমা দিতে পারবেন না"
                );
            }
            preparedNameBn = employeeRecords.nameBng;
            preparedSignature = employeeRecords.signature;
            if(preparedSignature == null || preparedSignature.length == 0) {
                throw new CustomException(
                        isLangEng ? "Please upload your signature!"
                                  : "আপনার স্বাক্ষর আপলোড করুন!"
                );
            }
        }
        Office_unitsDTO officeUnits = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
        if (officeUnits != null) {
            preparedOfficeNameBn = officeUnits.unitNameBng;
        }
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        if (officeUnitOrganograms != null) {
            preparedOrgNameBn = officeUnitOrganograms.designation_bng;
            preparedOrgId = officeUnitOrganograms.id;
        }
    }

    @Override
    public String getTableName() {
        return Driver_ot_billDAO.getInstance().getTableName();
    }

    @Override
    public Long getId() {
        return iD;
    }

    @Override
    public Integer getLevel() {
        return currentApprovalLevel;
    }

    @Override
    public Long getOfficeUnitId() {
        return officeUnitId;
    }

    @Override
    public Long getTaskTypeId() {
        return taskTypeId;
    }
}
