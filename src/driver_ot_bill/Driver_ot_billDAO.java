package driver_ot_bill;

import bill_approval_history.BillApprovalStatus;
import bill_approval_history.Bill_approval_historyDAO;
import bill_approval_mapping.Bill_approval_mappingDAO;
import bill_approval_mapping.Bill_approval_mappingDTO;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import employee_assign.EmployeeAssignDTO;
import employee_records.Employee_recordsRepository;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import util.CommonDTO;
import util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class Driver_ot_billDAO implements CommonDAOService<Driver_ot_billDTO> {
    private static final Logger logger = Logger.getLogger(Driver_ot_billDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (is_in_preview_stage,employee_offices_id,employee_record_id,employee_name_bn,office_unit_id,"
                    .concat("organogram_id,vehicle_ids,vehicle_id,vehicle_number_bn,vehicle_type_bn,basic_salary,bank_account_number,ot_bill_submission_config_id,")
                    .concat("bill_start_date,bill_end_date,overtime_bill_type_cat,")
                    .concat("budget_mapping_id,economic_sub_code_id,ordinance_text,min_office_hour,max_office_hour,")
                    .concat("min_extra_hour,max_extra_hour,max_overtime_hours,overtime_days,")
                    .concat("overtime_hours,bill_amount,prepared_org_id,prepared_name_bn,prepared_org_name_bn,")
                    .concat("prepared_office_name_bn,prepared_signature,prepared_time,current_approval_level,approval_status,task_type_id,")
                    .concat("modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET is_in_preview_stage=?,employee_offices_id=?,employee_record_id=?,employee_name_bn=?,office_unit_id=?,"
                    .concat("organogram_id=?,vehicle_ids=?,vehicle_id=?,vehicle_number_bn=?,vehicle_type_bn=?,basic_salary=?,bank_account_number=?,")
                    .concat("ot_bill_submission_config_id=?,bill_start_date=?,bill_end_date=?,overtime_bill_type_cat=?,")
                    .concat("budget_mapping_id=?,economic_sub_code_id=?,ordinance_text=?,min_office_hour=?,max_office_hour=?,")
                    .concat("min_extra_hour=?,max_extra_hour=?,max_overtime_hours=?,overtime_days=?,overtime_hours=?,")
                    .concat("bill_amount=?,prepared_org_id=?,prepared_name_bn=?,prepared_org_name_bn=?,")
                    .concat("prepared_office_name_bn=?,prepared_signature=?,prepared_time=?,current_approval_level=?,")
                    .concat("approval_status=?,task_type_id=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    public static final String restrictedViewWhereClause =
            " AND (office_unit_id=%d OR driver_ot_bill.id IN (SELECT approvable_dto_id FROM bill_approval_history "
                    .concat("WHERE table_name='driver_ot_bill' AND organogram_id=%d AND isDeleted=0)) ");

    private Driver_ot_billDAO() {
        searchMap.put("monthYearFrom", " AND (bill_end_date >= ?) ");
        searchMap.put("monthYearTo", " AND (bill_start_date <= ?) ");
        searchMap.put("overtimeBillTypeCat", " AND (overtime_bill_type_cat = ?) ");
        // TODO: 01-Nov-22 add in dto
        // searchMap.put("financeSerialNumber", " AND (finance_serial_number = ?) ");
        searchMap.put("isInPreview", " AND (is_in_preview_stage = ?) ");
    }

    private static class LazyLoader {
        static final Driver_ot_billDAO INSTANCE = new Driver_ot_billDAO();
    }

    public static Driver_ot_billDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Driver_ot_billDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setBoolean(++index, dto.isInPreviewStage);
        ps.setLong(++index, dto.employeeOfficesId);
        ps.setLong(++index, dto.employeeRecordId);
        ps.setString(++index, dto.employeeNameBn);
        ps.setLong(++index, dto.officeUnitId);
        ps.setLong(++index, dto.organogramId);
        ps.setString(++index, dto.vehicleIds);
        ps.setLong(++index, dto.vehicleId);
        ps.setString(++index, dto.vehicleNumberBn);
        ps.setString(++index, dto.vehicleTypeBn);
        ps.setInt(++index, dto.basicSalary);
        ps.setString(++index, dto.bankAccountNumber);
        ps.setLong(++index, dto.otBillSubmissionConfigId);
        ps.setLong(++index, dto.billStartDate);
        ps.setLong(++index, dto.billEndDate);
        ps.setInt(++index, dto.overtimeBillTypeCat);
        ps.setLong(++index, dto.budgetMappingId);
        ps.setLong(++index, dto.economicSubCodeId);
        ps.setString(++index, dto.ordinanceText);
        ps.setInt(++index, dto.minOfficeHour);
        ps.setInt(++index, dto.maxOfficeHour);
        ps.setInt(++index, dto.minExtraHour);
        ps.setInt(++index, dto.maxExtraHour);
        ps.setInt(++index, dto.maxOvertimeHours);
        ps.setInt(++index, dto.overtimeDays);
        ps.setInt(++index, dto.overtimeHours);
        ps.setLong(++index, dto.billAmount);
        ps.setLong(++index, dto.preparedOrgId);
        ps.setString(++index, dto.preparedNameBn);
        ps.setString(++index, dto.preparedOrgNameBn);
        ps.setString(++index, dto.preparedOfficeNameBn);
        ps.setBytes(++index, dto.preparedSignature);
        ps.setLong(++index, dto.preparedTime);
        ps.setInt(++index, dto.currentApprovalLevel);
        ps.setInt(++index, dto.approvalStatus);
        ps.setLong(++index, dto.taskTypeId);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Driver_ot_billDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Driver_ot_billDTO dto = new Driver_ot_billDTO();
            dto.iD = rs.getLong("ID");
            dto.isInPreviewStage = rs.getBoolean("is_in_preview_stage");
            dto.employeeOfficesId = rs.getLong("employee_offices_id");
            dto.employeeRecordId = rs.getLong("employee_record_id");
            dto.employeeNameBn = rs.getString("employee_name_bn");
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.organogramId = rs.getLong("organogram_id");
            dto.vehicleIds = rs.getString("vehicle_ids");
            dto.vehicleId = rs.getLong("vehicle_id");
            dto.vehicleNumberBn = rs.getString("vehicle_number_bn");
            dto.vehicleTypeBn = rs.getString("vehicle_type_bn");
            dto.basicSalary = rs.getInt("basic_salary");
            dto.bankAccountNumber = rs.getString("bank_account_number");
            dto.otBillSubmissionConfigId = rs.getLong("ot_bill_submission_config_id");
            dto.billStartDate = rs.getLong("bill_start_date");
            dto.billEndDate = rs.getLong("bill_end_date");
            dto.overtimeBillTypeCat = rs.getInt("overtime_bill_type_cat");
            dto.budgetMappingId = rs.getLong("budget_mapping_id");
            dto.economicSubCodeId = rs.getLong("economic_sub_code_id");
            dto.ordinanceText = rs.getString("ordinance_text");
            dto.minOfficeHour = rs.getInt("min_office_hour");
            dto.maxOfficeHour = rs.getInt("max_office_hour");
            dto.minExtraHour = rs.getInt("min_extra_hour");
            dto.maxExtraHour = rs.getInt("max_extra_hour");
            dto.maxOvertimeHours = rs.getInt("max_overtime_hours");
            dto.overtimeDays = rs.getInt("overtime_days");
            dto.overtimeHours = rs.getInt("overtime_hours");
            dto.billAmount = rs.getLong("bill_amount");
            dto.preparedOrgId = rs.getLong("prepared_org_id");
            dto.preparedNameBn = rs.getString("prepared_name_bn");
            dto.preparedOrgNameBn = rs.getString("prepared_org_name_bn");
            dto.preparedOfficeNameBn = rs.getString("prepared_office_name_bn");
            dto.preparedSignature = rs.getBytes("prepared_signature");
            dto.preparedTime = rs.getLong("prepared_time");
            dto.currentApprovalLevel = rs.getInt("current_approval_level");
            dto.approvalStatus = rs.getInt("approval_status");
            dto.taskTypeId = rs.getLong("task_type_id");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "driver_ot_bill";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Driver_ot_billDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Driver_ot_billDTO) commonDTO, updateSqlQuery, false);
    }

    private OptionDTO getOptionDto(EmployeeAssignDTO employeeAssignDTO) {
        String englishText = String.format(
                "%s - %s", employeeAssignDTO.username, employeeAssignDTO.employee_name_eng
        );
        String banglaText = String.format(
                "%s - %s",
                StringUtils.convertToBanNumber(employeeAssignDTO.username), employeeAssignDTO.employee_name_bng
        );
        return new OptionDTO(
                englishText,
                banglaText,
                String.valueOf(employeeAssignDTO.employee_office_id),
                "",
                employeeAssignDTO.username
        );
    }

    public String buildOptionsDriverEmployeeOfficeIds(String language) {
        List<OptionDTO> driverOptionDTOs =
                Employee_recordsRepository
                        .getInstance()
                        .getDriverEmployeeAssignDTO()
                        .stream()
                        .map(this::getOptionDto)
                        .collect(Collectors.toList());
        return Utils.buildOptions(driverOptionDTOs, language, null);
    }

    private static final String findByOfficesSubmissionConfig = "select * from driver_ot_bill " +
                                                                "where employee_offices_id=%d " +
                                                                "  and ot_bill_submission_config_id=%d " +
                                                                "  and isDeleted=0";

    public Driver_ot_billDTO getBillForEmployee(long employeeOfficesId, long otBillSubmissionConfigId) {
        return ConnectionAndStatementUtil.getT(
                String.format(findByOfficesSubmissionConfig, employeeOfficesId, otBillSubmissionConfigId),
                this::buildObjectFromResultSet
        );
    }

    public boolean deleteBillById(long id, long modifiedBy, long modificationTime) {
        boolean isDeleted = delete(modifiedBy, id, modificationTime);
        if (!isDeleted) {
            return false;
        }
        Driver_ot_detailDAO.getInstance().deleteByDriverOtBillId(id, modifiedBy, modificationTime);
        return true;
    }

    public void insertApproverIntoBillHistory(Driver_ot_billDTO driverOtBillDTO) throws Exception {
        if(!driverOtBillDTO.isInPreviewStage) {
            return;
        }
        Bill_approval_historyDAO approvalHistoryDAO = Bill_approval_historyDAO.getInstance();

        approvalHistoryDAO.deleteAllLevelHistory(driverOtBillDTO, driverOtBillDTO.modifiedBy, driverOtBillDTO.lastModificationTime);

        List<Bill_approval_mappingDTO> billApprovalMappings =
                Bill_approval_mappingDAO.getInstance().findAllLevelsByTaskType(driverOtBillDTO.taskTypeId);

        approvalHistoryDAO.addAndGetAddedDTOs(billApprovalMappings, driverOtBillDTO);

        driverOtBillDTO.currentApprovalLevel = 0;
        driverOtBillDTO.approvalStatus = BillApprovalStatus.PENDING.getValue();
    }
}
