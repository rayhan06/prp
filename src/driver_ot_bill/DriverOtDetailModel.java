package driver_ot_bill;

import pb.OptionDTO;
import sessionmanager.SessionConstants;
import util.DateTimeUtil;
import util.StringUtils;

public class DriverOtDetailModel {
    public long id = -1;
    public long driverOtBillId = -1;
    public long driverOtDate = SessionConstants.MIN_DATE;
    public int driverOtDateType = DriverOtDateType.WORK_DAY.getValue();

    public int officeHourFrom = -1;
    public int officeHourTo = -1;
    public int totalOfficeHour = 0;

    public int extraHourMorningFrom = -1;
    public int extraHourMorningTo = -1;
    public int totalExtraHourMorning = 0;

    public int extraHourAfternoonFrom = -1;
    public int extraHourAfternoonTo = -1;
    public int totalExtraHourAfternoon = 0;

    public int totalExtraHour = 0;

    public static final OptionDTO nothingSelected = new OptionDTO("-", "-", "-1", "", "");

    public DriverOtDetailModel() {
        calculateTotalHours();
    }

    public DriverOtDetailModel(Driver_ot_detailDTO detailDTO) {
        id = detailDTO.iD;
        driverOtBillId = detailDTO.driverOtBillId;
        driverOtDate = detailDTO.driverOtDate;
        driverOtDateType = detailDTO.driverOtDateType.getValue();
        officeHourFrom = detailDTO.officeHourFrom;
        officeHourTo = detailDTO.officeHourTo;
        extraHourMorningFrom = detailDTO.extraHourMorningFrom;
        extraHourMorningTo = detailDTO.extraHourMorningTo;
        extraHourAfternoonFrom = detailDTO.extraHourAfternoonFrom;
        extraHourAfternoonTo = detailDTO.extraHourAfternoonTo;
        calculateTotalHours();
    }

    private static boolean isRangeInvalid(int startHour, int endHour) {
        return startHour < 0 || endHour < 0 || endHour > 24 || startHour > endHour;
    }

    private static int getHours(int startHour, int endHour) {
        return isRangeInvalid(startHour, endHour) ? 0 : endHour - startHour;
    }

    public void validateRanges() {
        if (isRangeInvalid(officeHourFrom, officeHourTo)) {
            officeHourFrom = -1;
            officeHourTo = -1;
        }
        if (isRangeInvalid(extraHourMorningFrom, extraHourMorningTo)) {
            extraHourMorningFrom = -1;
            extraHourMorningTo = -1;
        }
        if (isRangeInvalid(extraHourAfternoonFrom, extraHourAfternoonTo)) {
            extraHourAfternoonFrom = -1;
            extraHourAfternoonTo = -1;
        }
    }

    public void calculateTotalHours() {
        totalOfficeHour = getHours(officeHourFrom, officeHourTo);
        totalExtraHourMorning = getHours(extraHourMorningFrom, extraHourMorningTo);
        totalExtraHourAfternoon = getHours(extraHourAfternoonFrom, extraHourAfternoonTo);
        totalExtraHour = totalExtraHourMorning + totalExtraHourAfternoon;
    }

    public static String getHourValueForView(int hour, String language) {
        if (hour <= 0 || hour > 24) {
            return "-";
        }
        return StringUtils.convertBanglaIfLanguageIsBangla(
                language,
                String.valueOf(DateTimeUtil.getHourValue(hour, false))
        );
    }

    public static String getHourRangeForView(int startHour, int endHour, String language) {
        if (isRangeInvalid(startHour, endHour)) {
            return "-";
        }
        String hourRange = String.format(
                "%d-%d",
                DateTimeUtil.getHourValue(startHour, false),
                DateTimeUtil.getHourValue(endHour, false)
        );
        return StringUtils.convertBanglaIfLanguageIsBangla(language, hourRange);
    }
}
