package driver_ot_bill;

import util.CommonDTO;

public class Driver_ot_bill_configDTO extends CommonDTO {
    public String ordinanceText = "";
    public int minOfficeHour;
    public int maxOfficeHour;
    public int minExtraHour;
    public int maxExtraHour;
    public int maxOvertimeHours;

    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = -1;
}
