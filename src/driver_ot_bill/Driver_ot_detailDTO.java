package driver_ot_bill;

import sessionmanager.SessionConstants;
import util.CommonDTO;

public class Driver_ot_detailDTO extends CommonDTO {
    public long driverOtBillId = -1;
    public long driverOtDate = SessionConstants.MIN_DATE;
    public DriverOtDateType driverOtDateType = DriverOtDateType.WORK_DAY;

    public int officeHourFrom = -1;
    public int officeHourTo = -1;

    public int extraHourMorningFrom = -1;
    public int extraHourMorningTo = -1;

    public int extraHourAfternoonFrom = -1;
    public int extraHourAfternoonTo = -1;

    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;
}
