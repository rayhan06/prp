package driver_ot_bill;

import bill_approval_history.Bill_approval_historyDTO;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import pb_notifications.Pb_notificationsDAO;

import java.util.List;
import java.util.stream.Collectors;


@SuppressWarnings({"Duplicates"})
public class Driver_ot_billNotification {
    private final Pb_notificationsDAO pb_notificationsDAO;

    private Driver_ot_billNotification() {
        pb_notificationsDAO = Pb_notificationsDAO.getInstance();
    }

    private static class LazyLoader {
        static final Driver_ot_billNotification INSTANCE = new Driver_ot_billNotification();
    }

    public static Driver_ot_billNotification getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void send(Driver_ot_billDTO driverOtBillDTO, List<Bill_approval_historyDTO> nextApprovalHistoryDTOs) {
        if (nextApprovalHistoryDTOs == null || nextApprovalHistoryDTOs.isEmpty()) {
            return;
        }
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(driverOtBillDTO.employeeRecordId);

        String textEn = String.format("Driver %s's overtime bill is waiting for your approval", employeeRecordsDTO.nameEng);
        String textBn = String.format("গাড়ী চালক %s-এর অধিকাল ভাতার বিল আপনার অনুমোদনের অপেক্ষায়", employeeRecordsDTO.nameBng);

        String notificationMessage = textEn + "$" + textBn;
        String url = "Driver_ot_billServlet?actionType=view&ID=" + driverOtBillDTO.iD;

        sendNotification(
                nextApprovalHistoryDTOs.stream()
                                       .map(approvalHistoryDTO -> approvalHistoryDTO.organogramId)
                                       .collect(Collectors.toList()),
                notificationMessage,
                url
        );
    }

    private void sendNotification(List<Long> organogramIds, String notificationMessage, String url) {
        if (organogramIds == null || organogramIds.size() == 0) return;

        Thread thread = new Thread(() -> {
            long currentTime = System.currentTimeMillis();
            organogramIds.forEach(organogramId -> pb_notificationsDAO.addPb_notifications(organogramId, currentTime, url, notificationMessage));
        });
        thread.setDaemon(true);
        thread.start();
    }
}
