package job_applicant_reference;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Job_applicant_referenceDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public Job_applicant_referenceDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
//		commonMaps = new Job_applicant_referenceMAPS(tableName);
	}

	@Override
	public Collection getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
		return null;
	}

	public Job_applicant_referenceDAO()
	{
		this("job_applicant_reference");		
	}
	
	public void get(Job_applicant_referenceDTO job_applicant_referenceDTO, ResultSet rs) throws SQLException
	{
		job_applicant_referenceDTO.iD = rs.getLong("ID");
		job_applicant_referenceDTO.jobApplicantId = rs.getLong("job_applicant_id");
		job_applicant_referenceDTO.jobId = rs.getLong("job_id");
		job_applicant_referenceDTO.nameEng = rs.getString("name_eng");
		job_applicant_referenceDTO.nameBng = rs.getString("name_bng");
		job_applicant_referenceDTO.designation = rs.getString("designation");
		job_applicant_referenceDTO.department = rs.getString("department");
		job_applicant_referenceDTO.mailAddress = rs.getString("mailAddress");
		job_applicant_referenceDTO.relationshipCat = rs.getLong("relationship_cat");
		job_applicant_referenceDTO.phoneNo = rs.getString("phoneNo");
		job_applicant_referenceDTO.alternatePhoneNo = rs.getString("alternatePhoneNo");
		job_applicant_referenceDTO.refAddress = rs.getString("ref_address");
		job_applicant_referenceDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		job_applicant_referenceDTO.insertionDate = rs.getLong("insertion_date");
		job_applicant_referenceDTO.modifiedBy = rs.getString("modified_by");
		job_applicant_referenceDTO.isDeleted = rs.getInt("isDeleted");
		job_applicant_referenceDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}

		
	

	//need another getter for repository
	public Job_applicant_referenceDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Job_applicant_referenceDTO job_applicant_referenceDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				job_applicant_referenceDTO = new Job_applicant_referenceDTO();

				get(job_applicant_referenceDTO, rs);

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return job_applicant_referenceDTO;
	}
	
	
	public List<Job_applicant_referenceDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Job_applicant_referenceDTO job_applicant_referenceDTO = null;
		List<Job_applicant_referenceDTO> job_applicant_referenceDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return job_applicant_referenceDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				job_applicant_referenceDTO = new Job_applicant_referenceDTO();
				get(job_applicant_referenceDTO, rs);
				System.out.println("got this DTO: " + job_applicant_referenceDTO);
				
				job_applicant_referenceDTOList.add(job_applicant_referenceDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return job_applicant_referenceDTOList;
	
	}





	
	//add repository
	public List<Job_applicant_referenceDTO> getAllJob_applicant_reference (boolean isFirstReload)
    {
		List<Job_applicant_referenceDTO> job_applicant_referenceDTOList = new ArrayList<>();

		String sql = "SELECT * FROM job_applicant_reference";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by job_applicant_reference.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Job_applicant_referenceDTO job_applicant_referenceDTO = new Job_applicant_referenceDTO();
				get(job_applicant_referenceDTO, rs);
				
				job_applicant_referenceDTOList.add(job_applicant_referenceDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return job_applicant_referenceDTOList;
    }

	public List<Job_applicant_referenceDTO> getDTOByJobApplicantID (long ID)
	{
		List<Job_applicant_referenceDTO> job_applicant_referenceDTOList = new ArrayList<>();

		String sql = "SELECT * FROM job_applicant_reference";
		sql += " WHERE job_applicant_id=" + ID;

		sql+=" AND isDeleted =  0";

		sql += " order by job_applicant_reference.lastModificationTime desc";
		printSql(sql);

		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;


		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);


			while(rs.next()){
				Job_applicant_referenceDTO job_applicant_referenceDTO = new Job_applicant_referenceDTO();
				get(job_applicant_referenceDTO, rs);

				job_applicant_referenceDTOList.add(job_applicant_referenceDTO);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}

		return job_applicant_referenceDTOList;
	}
				
}
	