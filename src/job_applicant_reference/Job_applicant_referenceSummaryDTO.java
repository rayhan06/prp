package job_applicant_reference;

import util.CommonDTO;


public class Job_applicant_referenceSummaryDTO extends CommonDTO
{
    public long jobId = 0;
	public long jobApplicantId = 0;
    public String nameEng = "";
    public String nameBng = "";
    public String designation = "";
    public String department = "";
    public String mailAddress = "";
	public long relationshipCat = 0;
    public String phoneNo = "";
    public String alternatePhoneNo = "";
    public String refAddress = "";
	public long insertedByUserId = 0;
	public long insertionDate = 0;
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Job_applicant_referenceDTO[" +
            " iD = " + iD +
            " jobApplicantId = " + jobApplicantId +
            " nameEng = " + nameEng +
            " nameBng = " + nameBng +
            " designation = " + designation +
            " department = " + department +
            " mailAddress = " + mailAddress +
            " relationshipCat = " + relationshipCat +
            " phoneNo = " + phoneNo +
            " alternatePhoneNo = " + alternatePhoneNo +
            " refAddress = " + refAddress +
            " insertedByUserId = " + insertedByUserId +
            " insertionDate = " + insertionDate +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}