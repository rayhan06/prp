package job_applicant_reference;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Job_applicant_referenceRepository implements Repository {
	Job_applicant_referenceDAO job_applicant_referenceDAO = null;
	
	public void setDAO(Job_applicant_referenceDAO job_applicant_referenceDAO)
	{
		this.job_applicant_referenceDAO = job_applicant_referenceDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Job_applicant_referenceRepository.class);
	Map<Long, Job_applicant_referenceDTO>mapOfJob_applicant_referenceDTOToiD;
	Map<Long, Set<Job_applicant_referenceDTO> >mapOfJob_applicant_referenceDTOTojobApplicantId;
	Map<String, Set<Job_applicant_referenceDTO> >mapOfJob_applicant_referenceDTOTonameEng;
	Map<String, Set<Job_applicant_referenceDTO> >mapOfJob_applicant_referenceDTOTonameBng;
	Map<String, Set<Job_applicant_referenceDTO> >mapOfJob_applicant_referenceDTOTodesignation;
	Map<String, Set<Job_applicant_referenceDTO> >mapOfJob_applicant_referenceDTOTodepartment;
	Map<String, Set<Job_applicant_referenceDTO> >mapOfJob_applicant_referenceDTOTomailAddress;
	Map<Long, Set<Job_applicant_referenceDTO> >mapOfJob_applicant_referenceDTOTorelationshipCat;
	Map<String, Set<Job_applicant_referenceDTO> >mapOfJob_applicant_referenceDTOTophoneNo;
	Map<String, Set<Job_applicant_referenceDTO> >mapOfJob_applicant_referenceDTOToalternatePhoneNo;
	Map<String, Set<Job_applicant_referenceDTO> >mapOfJob_applicant_referenceDTOTorefAddress;
	Map<Long, Set<Job_applicant_referenceDTO> >mapOfJob_applicant_referenceDTOToinsertedByUserId;
	Map<Long, Set<Job_applicant_referenceDTO> >mapOfJob_applicant_referenceDTOToinsertionDate;
	Map<String, Set<Job_applicant_referenceDTO> >mapOfJob_applicant_referenceDTOTomodifiedBy;
	Map<Long, Set<Job_applicant_referenceDTO> >mapOfJob_applicant_referenceDTOTolastModificationTime;


	static Job_applicant_referenceRepository instance = null;  
	private Job_applicant_referenceRepository(){
		mapOfJob_applicant_referenceDTOToiD = new ConcurrentHashMap<>();
		mapOfJob_applicant_referenceDTOTojobApplicantId = new ConcurrentHashMap<>();
		mapOfJob_applicant_referenceDTOTonameEng = new ConcurrentHashMap<>();
		mapOfJob_applicant_referenceDTOTonameBng = new ConcurrentHashMap<>();
		mapOfJob_applicant_referenceDTOTodesignation = new ConcurrentHashMap<>();
		mapOfJob_applicant_referenceDTOTodepartment = new ConcurrentHashMap<>();
		mapOfJob_applicant_referenceDTOTomailAddress = new ConcurrentHashMap<>();
		mapOfJob_applicant_referenceDTOTorelationshipCat = new ConcurrentHashMap<>();
		mapOfJob_applicant_referenceDTOTophoneNo = new ConcurrentHashMap<>();
		mapOfJob_applicant_referenceDTOToalternatePhoneNo = new ConcurrentHashMap<>();
		mapOfJob_applicant_referenceDTOTorefAddress = new ConcurrentHashMap<>();
		mapOfJob_applicant_referenceDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfJob_applicant_referenceDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfJob_applicant_referenceDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfJob_applicant_referenceDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Job_applicant_referenceRepository getInstance(){
		if (instance == null){
			instance = new Job_applicant_referenceRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(job_applicant_referenceDAO == null)
		{
			return;
		}
		try {
			List<Job_applicant_referenceDTO> job_applicant_referenceDTOs = job_applicant_referenceDAO.getAllJob_applicant_reference(reloadAll);
			for(Job_applicant_referenceDTO job_applicant_referenceDTO : job_applicant_referenceDTOs) {
				Job_applicant_referenceDTO oldJob_applicant_referenceDTO = getJob_applicant_referenceDTOByID(job_applicant_referenceDTO.iD);
				if( oldJob_applicant_referenceDTO != null ) {
					mapOfJob_applicant_referenceDTOToiD.remove(oldJob_applicant_referenceDTO.iD);
				
					if(mapOfJob_applicant_referenceDTOTojobApplicantId.containsKey(oldJob_applicant_referenceDTO.jobApplicantId)) {
						mapOfJob_applicant_referenceDTOTojobApplicantId.get(oldJob_applicant_referenceDTO.jobApplicantId).remove(oldJob_applicant_referenceDTO);
					}
					if(mapOfJob_applicant_referenceDTOTojobApplicantId.get(oldJob_applicant_referenceDTO.jobApplicantId).isEmpty()) {
						mapOfJob_applicant_referenceDTOTojobApplicantId.remove(oldJob_applicant_referenceDTO.jobApplicantId);
					}
					
					if(mapOfJob_applicant_referenceDTOTonameEng.containsKey(oldJob_applicant_referenceDTO.nameEng)) {
						mapOfJob_applicant_referenceDTOTonameEng.get(oldJob_applicant_referenceDTO.nameEng).remove(oldJob_applicant_referenceDTO);
					}
					if(mapOfJob_applicant_referenceDTOTonameEng.get(oldJob_applicant_referenceDTO.nameEng).isEmpty()) {
						mapOfJob_applicant_referenceDTOTonameEng.remove(oldJob_applicant_referenceDTO.nameEng);
					}
					
					if(mapOfJob_applicant_referenceDTOTonameBng.containsKey(oldJob_applicant_referenceDTO.nameBng)) {
						mapOfJob_applicant_referenceDTOTonameBng.get(oldJob_applicant_referenceDTO.nameBng).remove(oldJob_applicant_referenceDTO);
					}
					if(mapOfJob_applicant_referenceDTOTonameBng.get(oldJob_applicant_referenceDTO.nameBng).isEmpty()) {
						mapOfJob_applicant_referenceDTOTonameBng.remove(oldJob_applicant_referenceDTO.nameBng);
					}
					
					if(mapOfJob_applicant_referenceDTOTodesignation.containsKey(oldJob_applicant_referenceDTO.designation)) {
						mapOfJob_applicant_referenceDTOTodesignation.get(oldJob_applicant_referenceDTO.designation).remove(oldJob_applicant_referenceDTO);
					}
					if(mapOfJob_applicant_referenceDTOTodesignation.get(oldJob_applicant_referenceDTO.designation).isEmpty()) {
						mapOfJob_applicant_referenceDTOTodesignation.remove(oldJob_applicant_referenceDTO.designation);
					}
					
					if(mapOfJob_applicant_referenceDTOTodepartment.containsKey(oldJob_applicant_referenceDTO.department)) {
						mapOfJob_applicant_referenceDTOTodepartment.get(oldJob_applicant_referenceDTO.department).remove(oldJob_applicant_referenceDTO);
					}
					if(mapOfJob_applicant_referenceDTOTodepartment.get(oldJob_applicant_referenceDTO.department).isEmpty()) {
						mapOfJob_applicant_referenceDTOTodepartment.remove(oldJob_applicant_referenceDTO.department);
					}
					
					if(mapOfJob_applicant_referenceDTOTomailAddress.containsKey(oldJob_applicant_referenceDTO.mailAddress)) {
						mapOfJob_applicant_referenceDTOTomailAddress.get(oldJob_applicant_referenceDTO.mailAddress).remove(oldJob_applicant_referenceDTO);
					}
					if(mapOfJob_applicant_referenceDTOTomailAddress.get(oldJob_applicant_referenceDTO.mailAddress).isEmpty()) {
						mapOfJob_applicant_referenceDTOTomailAddress.remove(oldJob_applicant_referenceDTO.mailAddress);
					}
					
					if(mapOfJob_applicant_referenceDTOTorelationshipCat.containsKey(oldJob_applicant_referenceDTO.relationshipCat)) {
						mapOfJob_applicant_referenceDTOTorelationshipCat.get(oldJob_applicant_referenceDTO.relationshipCat).remove(oldJob_applicant_referenceDTO);
					}
					if(mapOfJob_applicant_referenceDTOTorelationshipCat.get(oldJob_applicant_referenceDTO.relationshipCat).isEmpty()) {
						mapOfJob_applicant_referenceDTOTorelationshipCat.remove(oldJob_applicant_referenceDTO.relationshipCat);
					}
					
					if(mapOfJob_applicant_referenceDTOTophoneNo.containsKey(oldJob_applicant_referenceDTO.phoneNo)) {
						mapOfJob_applicant_referenceDTOTophoneNo.get(oldJob_applicant_referenceDTO.phoneNo).remove(oldJob_applicant_referenceDTO);
					}
					if(mapOfJob_applicant_referenceDTOTophoneNo.get(oldJob_applicant_referenceDTO.phoneNo).isEmpty()) {
						mapOfJob_applicant_referenceDTOTophoneNo.remove(oldJob_applicant_referenceDTO.phoneNo);
					}
					
					if(mapOfJob_applicant_referenceDTOToalternatePhoneNo.containsKey(oldJob_applicant_referenceDTO.alternatePhoneNo)) {
						mapOfJob_applicant_referenceDTOToalternatePhoneNo.get(oldJob_applicant_referenceDTO.alternatePhoneNo).remove(oldJob_applicant_referenceDTO);
					}
					if(mapOfJob_applicant_referenceDTOToalternatePhoneNo.get(oldJob_applicant_referenceDTO.alternatePhoneNo).isEmpty()) {
						mapOfJob_applicant_referenceDTOToalternatePhoneNo.remove(oldJob_applicant_referenceDTO.alternatePhoneNo);
					}
					
					if(mapOfJob_applicant_referenceDTOTorefAddress.containsKey(oldJob_applicant_referenceDTO.refAddress)) {
						mapOfJob_applicant_referenceDTOTorefAddress.get(oldJob_applicant_referenceDTO.refAddress).remove(oldJob_applicant_referenceDTO);
					}
					if(mapOfJob_applicant_referenceDTOTorefAddress.get(oldJob_applicant_referenceDTO.refAddress).isEmpty()) {
						mapOfJob_applicant_referenceDTOTorefAddress.remove(oldJob_applicant_referenceDTO.refAddress);
					}
					
					if(mapOfJob_applicant_referenceDTOToinsertedByUserId.containsKey(oldJob_applicant_referenceDTO.insertedByUserId)) {
						mapOfJob_applicant_referenceDTOToinsertedByUserId.get(oldJob_applicant_referenceDTO.insertedByUserId).remove(oldJob_applicant_referenceDTO);
					}
					if(mapOfJob_applicant_referenceDTOToinsertedByUserId.get(oldJob_applicant_referenceDTO.insertedByUserId).isEmpty()) {
						mapOfJob_applicant_referenceDTOToinsertedByUserId.remove(oldJob_applicant_referenceDTO.insertedByUserId);
					}
					
					if(mapOfJob_applicant_referenceDTOToinsertionDate.containsKey(oldJob_applicant_referenceDTO.insertionDate)) {
						mapOfJob_applicant_referenceDTOToinsertionDate.get(oldJob_applicant_referenceDTO.insertionDate).remove(oldJob_applicant_referenceDTO);
					}
					if(mapOfJob_applicant_referenceDTOToinsertionDate.get(oldJob_applicant_referenceDTO.insertionDate).isEmpty()) {
						mapOfJob_applicant_referenceDTOToinsertionDate.remove(oldJob_applicant_referenceDTO.insertionDate);
					}
					
					if(mapOfJob_applicant_referenceDTOTomodifiedBy.containsKey(oldJob_applicant_referenceDTO.modifiedBy)) {
						mapOfJob_applicant_referenceDTOTomodifiedBy.get(oldJob_applicant_referenceDTO.modifiedBy).remove(oldJob_applicant_referenceDTO);
					}
					if(mapOfJob_applicant_referenceDTOTomodifiedBy.get(oldJob_applicant_referenceDTO.modifiedBy).isEmpty()) {
						mapOfJob_applicant_referenceDTOTomodifiedBy.remove(oldJob_applicant_referenceDTO.modifiedBy);
					}
					
					if(mapOfJob_applicant_referenceDTOTolastModificationTime.containsKey(oldJob_applicant_referenceDTO.lastModificationTime)) {
						mapOfJob_applicant_referenceDTOTolastModificationTime.get(oldJob_applicant_referenceDTO.lastModificationTime).remove(oldJob_applicant_referenceDTO);
					}
					if(mapOfJob_applicant_referenceDTOTolastModificationTime.get(oldJob_applicant_referenceDTO.lastModificationTime).isEmpty()) {
						mapOfJob_applicant_referenceDTOTolastModificationTime.remove(oldJob_applicant_referenceDTO.lastModificationTime);
					}
					
					
				}
				if(job_applicant_referenceDTO.isDeleted == 0) 
				{
					
					mapOfJob_applicant_referenceDTOToiD.put(job_applicant_referenceDTO.iD, job_applicant_referenceDTO);
				
					if( ! mapOfJob_applicant_referenceDTOTojobApplicantId.containsKey(job_applicant_referenceDTO.jobApplicantId)) {
						mapOfJob_applicant_referenceDTOTojobApplicantId.put(job_applicant_referenceDTO.jobApplicantId, new HashSet<>());
					}
					mapOfJob_applicant_referenceDTOTojobApplicantId.get(job_applicant_referenceDTO.jobApplicantId).add(job_applicant_referenceDTO);
					
					if( ! mapOfJob_applicant_referenceDTOTonameEng.containsKey(job_applicant_referenceDTO.nameEng)) {
						mapOfJob_applicant_referenceDTOTonameEng.put(job_applicant_referenceDTO.nameEng, new HashSet<>());
					}
					mapOfJob_applicant_referenceDTOTonameEng.get(job_applicant_referenceDTO.nameEng).add(job_applicant_referenceDTO);
					
					if( ! mapOfJob_applicant_referenceDTOTonameBng.containsKey(job_applicant_referenceDTO.nameBng)) {
						mapOfJob_applicant_referenceDTOTonameBng.put(job_applicant_referenceDTO.nameBng, new HashSet<>());
					}
					mapOfJob_applicant_referenceDTOTonameBng.get(job_applicant_referenceDTO.nameBng).add(job_applicant_referenceDTO);
					
					if( ! mapOfJob_applicant_referenceDTOTodesignation.containsKey(job_applicant_referenceDTO.designation)) {
						mapOfJob_applicant_referenceDTOTodesignation.put(job_applicant_referenceDTO.designation, new HashSet<>());
					}
					mapOfJob_applicant_referenceDTOTodesignation.get(job_applicant_referenceDTO.designation).add(job_applicant_referenceDTO);
					
					if( ! mapOfJob_applicant_referenceDTOTodepartment.containsKey(job_applicant_referenceDTO.department)) {
						mapOfJob_applicant_referenceDTOTodepartment.put(job_applicant_referenceDTO.department, new HashSet<>());
					}
					mapOfJob_applicant_referenceDTOTodepartment.get(job_applicant_referenceDTO.department).add(job_applicant_referenceDTO);
					
					if( ! mapOfJob_applicant_referenceDTOTomailAddress.containsKey(job_applicant_referenceDTO.mailAddress)) {
						mapOfJob_applicant_referenceDTOTomailAddress.put(job_applicant_referenceDTO.mailAddress, new HashSet<>());
					}
					mapOfJob_applicant_referenceDTOTomailAddress.get(job_applicant_referenceDTO.mailAddress).add(job_applicant_referenceDTO);
					
					if( ! mapOfJob_applicant_referenceDTOTorelationshipCat.containsKey(job_applicant_referenceDTO.relationshipCat)) {
						mapOfJob_applicant_referenceDTOTorelationshipCat.put(job_applicant_referenceDTO.relationshipCat, new HashSet<>());
					}
					mapOfJob_applicant_referenceDTOTorelationshipCat.get(job_applicant_referenceDTO.relationshipCat).add(job_applicant_referenceDTO);
					
					if( ! mapOfJob_applicant_referenceDTOTophoneNo.containsKey(job_applicant_referenceDTO.phoneNo)) {
						mapOfJob_applicant_referenceDTOTophoneNo.put(job_applicant_referenceDTO.phoneNo, new HashSet<>());
					}
					mapOfJob_applicant_referenceDTOTophoneNo.get(job_applicant_referenceDTO.phoneNo).add(job_applicant_referenceDTO);
					
					if( ! mapOfJob_applicant_referenceDTOToalternatePhoneNo.containsKey(job_applicant_referenceDTO.alternatePhoneNo)) {
						mapOfJob_applicant_referenceDTOToalternatePhoneNo.put(job_applicant_referenceDTO.alternatePhoneNo, new HashSet<>());
					}
					mapOfJob_applicant_referenceDTOToalternatePhoneNo.get(job_applicant_referenceDTO.alternatePhoneNo).add(job_applicant_referenceDTO);
					
					if( ! mapOfJob_applicant_referenceDTOTorefAddress.containsKey(job_applicant_referenceDTO.refAddress)) {
						mapOfJob_applicant_referenceDTOTorefAddress.put(job_applicant_referenceDTO.refAddress, new HashSet<>());
					}
					mapOfJob_applicant_referenceDTOTorefAddress.get(job_applicant_referenceDTO.refAddress).add(job_applicant_referenceDTO);
					
					if( ! mapOfJob_applicant_referenceDTOToinsertedByUserId.containsKey(job_applicant_referenceDTO.insertedByUserId)) {
						mapOfJob_applicant_referenceDTOToinsertedByUserId.put(job_applicant_referenceDTO.insertedByUserId, new HashSet<>());
					}
					mapOfJob_applicant_referenceDTOToinsertedByUserId.get(job_applicant_referenceDTO.insertedByUserId).add(job_applicant_referenceDTO);
					
					if( ! mapOfJob_applicant_referenceDTOToinsertionDate.containsKey(job_applicant_referenceDTO.insertionDate)) {
						mapOfJob_applicant_referenceDTOToinsertionDate.put(job_applicant_referenceDTO.insertionDate, new HashSet<>());
					}
					mapOfJob_applicant_referenceDTOToinsertionDate.get(job_applicant_referenceDTO.insertionDate).add(job_applicant_referenceDTO);
					
					if( ! mapOfJob_applicant_referenceDTOTomodifiedBy.containsKey(job_applicant_referenceDTO.modifiedBy)) {
						mapOfJob_applicant_referenceDTOTomodifiedBy.put(job_applicant_referenceDTO.modifiedBy, new HashSet<>());
					}
					mapOfJob_applicant_referenceDTOTomodifiedBy.get(job_applicant_referenceDTO.modifiedBy).add(job_applicant_referenceDTO);
					
					if( ! mapOfJob_applicant_referenceDTOTolastModificationTime.containsKey(job_applicant_referenceDTO.lastModificationTime)) {
						mapOfJob_applicant_referenceDTOTolastModificationTime.put(job_applicant_referenceDTO.lastModificationTime, new HashSet<>());
					}
					mapOfJob_applicant_referenceDTOTolastModificationTime.get(job_applicant_referenceDTO.lastModificationTime).add(job_applicant_referenceDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Job_applicant_referenceDTO> getJob_applicant_referenceList() {
		List <Job_applicant_referenceDTO> job_applicant_references = new ArrayList<Job_applicant_referenceDTO>(this.mapOfJob_applicant_referenceDTOToiD.values());
		return job_applicant_references;
	}
	
	
	public Job_applicant_referenceDTO getJob_applicant_referenceDTOByID( long ID){
		return mapOfJob_applicant_referenceDTOToiD.get(ID);
	}
	
	
	public List<Job_applicant_referenceDTO> getJob_applicant_referenceDTOByjob_applicant_id(long job_applicant_id) {
		return new ArrayList<>( mapOfJob_applicant_referenceDTOTojobApplicantId.getOrDefault(job_applicant_id,new HashSet<>()));
	}
	
	
	public List<Job_applicant_referenceDTO> getJob_applicant_referenceDTOByname_eng(String name_eng) {
		return new ArrayList<>( mapOfJob_applicant_referenceDTOTonameEng.getOrDefault(name_eng,new HashSet<>()));
	}
	
	
	public List<Job_applicant_referenceDTO> getJob_applicant_referenceDTOByname_bng(String name_bng) {
		return new ArrayList<>( mapOfJob_applicant_referenceDTOTonameBng.getOrDefault(name_bng,new HashSet<>()));
	}
	
	
	public List<Job_applicant_referenceDTO> getJob_applicant_referenceDTOBydesignation(String designation) {
		return new ArrayList<>( mapOfJob_applicant_referenceDTOTodesignation.getOrDefault(designation,new HashSet<>()));
	}
	
	
	public List<Job_applicant_referenceDTO> getJob_applicant_referenceDTOBydepartment(String department) {
		return new ArrayList<>( mapOfJob_applicant_referenceDTOTodepartment.getOrDefault(department,new HashSet<>()));
	}
	
	
	public List<Job_applicant_referenceDTO> getJob_applicant_referenceDTOBymailAddress(String mailAddress) {
		return new ArrayList<>( mapOfJob_applicant_referenceDTOTomailAddress.getOrDefault(mailAddress,new HashSet<>()));
	}
	
	
	public List<Job_applicant_referenceDTO> getJob_applicant_referenceDTOByrelationship_cat(long relationship_cat) {
		return new ArrayList<>( mapOfJob_applicant_referenceDTOTorelationshipCat.getOrDefault(relationship_cat,new HashSet<>()));
	}
	
	
	public List<Job_applicant_referenceDTO> getJob_applicant_referenceDTOByphoneNo(String phoneNo) {
		return new ArrayList<>( mapOfJob_applicant_referenceDTOTophoneNo.getOrDefault(phoneNo,new HashSet<>()));
	}
	
	
	public List<Job_applicant_referenceDTO> getJob_applicant_referenceDTOByalternatePhoneNo(String alternatePhoneNo) {
		return new ArrayList<>( mapOfJob_applicant_referenceDTOToalternatePhoneNo.getOrDefault(alternatePhoneNo,new HashSet<>()));
	}
	
	
	public List<Job_applicant_referenceDTO> getJob_applicant_referenceDTOByref_address(String ref_address) {
		return new ArrayList<>( mapOfJob_applicant_referenceDTOTorefAddress.getOrDefault(ref_address,new HashSet<>()));
	}
	
	
	public List<Job_applicant_referenceDTO> getJob_applicant_referenceDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfJob_applicant_referenceDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Job_applicant_referenceDTO> getJob_applicant_referenceDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfJob_applicant_referenceDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Job_applicant_referenceDTO> getJob_applicant_referenceDTOBymodified_by(String modified_by) {
		return new ArrayList<>( mapOfJob_applicant_referenceDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Job_applicant_referenceDTO> getJob_applicant_referenceDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfJob_applicant_referenceDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "job_applicant_reference";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


