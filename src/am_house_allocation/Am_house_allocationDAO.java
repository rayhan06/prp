package am_house_allocation;

import am_house.Am_houseDTO;
import am_house.Am_houseRepository;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Am_house_allocationDAO implements CommonDAOService<Am_house_allocationDTO> {

    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private String[] columnNames = null;

    private static final String getByAmHouseAllocationRequestId =
            "SELECT * FROM am_house_allocation WHERE request_id = %d AND isDeleted = 0";
    private static final String houseReceiveDateQuery =
            "Update am_house_allocation " +
                    " SET receive_date=? " +
                    " WHERE ID=? ";

    public Am_house_allocationDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "house_id",
                        "am_house_old_new_cat",
                        "am_house_location_cat",
                        "am_house_class_cat",
                        "am_house_class_sub_cat",
                        "allocation_date",
                        "receive_date",
                        "request_id",
                        "committee_id",
                        "requester_org_id",
                        "requester_office_id",
                        "requester_office_unit_id",
                        "requester_emp_id",
                        "requester_phone_num",
                        "requester_name_en",
                        "requester_name_bn",
                        "requester_office_name_en",
                        "requester_office_name_bn",
                        "requester_office_unit_name_en",
                        "requester_office_unit_name_bn",
                        "requester_office_unit_org_name_en",
                        "requester_office_unit_org_name_bn",
                        "status",
                        "files_dropzone",
                        "am_house_allocation_quota",
                        "withdrawn_date",
                        "inserted_by_user_id",
                        "inserted_by_organogram_id",
                        "modified_by",
                        "insertion_date",
                        "search_column",
                        "isDeleted",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("am_house_old_new_cat", " and (am_house_old_new_cat = ?)");
        searchMap.put("am_house_location_cat", " and (am_house_location_cat = ?)");
        searchMap.put("am_house_class_cat", " and (am_house_class_cat = ?)");
        searchMap.put("am_house_class_sub_cat", " and (am_house_class_sub_cat = ?)");
        searchMap.put("allocation_date_start", " and (allocation_date >= ?)");
        searchMap.put("allocation_date_end", " and (allocation_date <= ?)");
//		searchMap.put("modified_by"," and (modified_by like ?)");
        searchMap.put("insertion_date_start", " and (insertion_date >= ?)");
        searchMap.put("insertion_date_end", " and (insertion_date <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Am_house_allocationDAO INSTANCE = new Am_house_allocationDAO();
    }

    public static Am_house_allocationDAO getInstance() {
        return Am_house_allocationDAO.LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Am_house_allocationDTO am_house_allocationDTO) {
        am_house_allocationDTO.searchColumn = "";
        CategoryLanguageModel model = CatRepository.getInstance().getCategoryLanguageModel
                ("am_house_old_new", am_house_allocationDTO.amHouseOldNewCat);
        if (model != null) {
            am_house_allocationDTO.searchColumn += model.englishText + " " + model.banglaText + " ";
        }

        model = CatRepository.getInstance().getCategoryLanguageModel
                ("am_house_location", am_house_allocationDTO.amHouseLocationCat);
        if (model != null) {
            am_house_allocationDTO.searchColumn += model.englishText + " " + model.banglaText + " ";
        }

        model = CatRepository.getInstance().getCategoryLanguageModel
                ("am_house_class", am_house_allocationDTO.amHouseClassCat);
        if (model != null) {
            am_house_allocationDTO.searchColumn += model.englishText + " " + model.banglaText + " ";
        }

        Am_houseDTO houseDTO = Am_houseRepository.getInstance().getAm_houseDTOByID(am_house_allocationDTO.houseId);
        if (houseDTO != null) {
            am_house_allocationDTO.searchColumn += houseDTO.houseNumber;
        }


    }

    @Override
    public void set(PreparedStatement ps, Am_house_allocationDTO am_house_allocationDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(am_house_allocationDTO);
        if (isInsert) {
            ps.setObject(++index, am_house_allocationDTO.iD);
        }
        ps.setObject(++index, am_house_allocationDTO.houseId);
        ps.setObject(++index, am_house_allocationDTO.amHouseOldNewCat);
        ps.setObject(++index, am_house_allocationDTO.amHouseLocationCat);
        ps.setObject(++index, am_house_allocationDTO.amHouseClassCat);
        ps.setObject(++index, am_house_allocationDTO.amHouseClassSubCat);
        ps.setObject(++index, am_house_allocationDTO.allocationDate);
        ps.setObject(++index, am_house_allocationDTO.receiveDate);
        ps.setObject(++index, am_house_allocationDTO.requestId);
        ps.setObject(++index, am_house_allocationDTO.committeeId);
        ps.setObject(++index, am_house_allocationDTO.requesterOrgId);
        ps.setObject(++index, am_house_allocationDTO.requesterOfficeId);
        ps.setObject(++index, am_house_allocationDTO.requesterOfficeUnitId);
        ps.setObject(++index, am_house_allocationDTO.requesterEmpId);
        ps.setObject(++index, am_house_allocationDTO.requesterPhoneNum);
        ps.setObject(++index, am_house_allocationDTO.requesterNameEn);
        ps.setObject(++index, am_house_allocationDTO.requesterNameBn);
        ps.setObject(++index, am_house_allocationDTO.requesterOfficeNameEn);
        ps.setObject(++index, am_house_allocationDTO.requesterOfficeNameBn);
        ps.setObject(++index, am_house_allocationDTO.requesterOfficeUnitNameEn);
        ps.setObject(++index, am_house_allocationDTO.requesterOfficeUnitNameBn);
        ps.setObject(++index, am_house_allocationDTO.requesterOfficeUnitOrgNameEn);
        ps.setObject(++index, am_house_allocationDTO.requesterOfficeUnitOrgNameBn);
        ps.setObject(++index, am_house_allocationDTO.status);
        ps.setObject(++index, am_house_allocationDTO.filesDropzone);
        ps.setObject(++index, am_house_allocationDTO.amHouseAllocationQuota);
        ps.setObject(++index, am_house_allocationDTO.withdrawn_date);
        ps.setObject(++index, am_house_allocationDTO.insertedByUserId);
        ps.setObject(++index, am_house_allocationDTO.insertedByOrganogramId);
        ps.setObject(++index, am_house_allocationDTO.modifiedBy);
        ps.setObject(++index, am_house_allocationDTO.insertionDate);
        ps.setObject(++index, am_house_allocationDTO.searchColumn);
        if (isInsert) {
            ps.setObject(++index, am_house_allocationDTO.isDeleted);
        }
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, am_house_allocationDTO.iD);
        }
    }

    @Override
    public Am_house_allocationDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Am_house_allocationDTO am_house_allocationDTO = new Am_house_allocationDTO();
            int i = 0;
            am_house_allocationDTO.iD = rs.getLong(columnNames[i++]);
            am_house_allocationDTO.houseId = rs.getLong(columnNames[i++]);
            am_house_allocationDTO.amHouseOldNewCat = rs.getInt(columnNames[i++]);
            am_house_allocationDTO.amHouseLocationCat = rs.getInt(columnNames[i++]);
            am_house_allocationDTO.amHouseClassCat = rs.getInt(columnNames[i++]);
            am_house_allocationDTO.amHouseClassSubCat = rs.getInt(columnNames[i++]);
            am_house_allocationDTO.allocationDate = rs.getLong(columnNames[i++]);
            am_house_allocationDTO.receiveDate = rs.getLong(columnNames[i++]);
            am_house_allocationDTO.requestId = rs.getLong(columnNames[i++]);
            am_house_allocationDTO.committeeId = rs.getLong(columnNames[i++]);
            am_house_allocationDTO.requesterOrgId = rs.getLong(columnNames[i++]);
            am_house_allocationDTO.requesterOfficeId = rs.getLong(columnNames[i++]);
            am_house_allocationDTO.requesterOfficeUnitId = rs.getLong(columnNames[i++]);
            am_house_allocationDTO.requesterEmpId = rs.getLong(columnNames[i++]);
            am_house_allocationDTO.requesterPhoneNum = rs.getString(columnNames[i++]);
            am_house_allocationDTO.requesterNameEn = rs.getString(columnNames[i++]);
            am_house_allocationDTO.requesterNameBn = rs.getString(columnNames[i++]);
            am_house_allocationDTO.requesterOfficeNameEn = rs.getString(columnNames[i++]);
            am_house_allocationDTO.requesterOfficeNameBn = rs.getString(columnNames[i++]);
            am_house_allocationDTO.requesterOfficeUnitNameEn = rs.getString(columnNames[i++]);
            am_house_allocationDTO.requesterOfficeUnitNameBn = rs.getString(columnNames[i++]);
            am_house_allocationDTO.requesterOfficeUnitOrgNameEn = rs.getString(columnNames[i++]);
            am_house_allocationDTO.requesterOfficeUnitOrgNameBn = rs.getString(columnNames[i++]);
            am_house_allocationDTO.status = rs.getInt(columnNames[i++]);
            am_house_allocationDTO.filesDropzone = rs.getLong(columnNames[i++]);
            am_house_allocationDTO.amHouseAllocationQuota = rs.getInt(columnNames[i++]);
            am_house_allocationDTO.withdrawn_date = rs.getLong(columnNames[i++]);
            am_house_allocationDTO.insertedByUserId = rs.getLong(columnNames[i++]);
            am_house_allocationDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
            am_house_allocationDTO.modifiedBy = rs.getLong(columnNames[i++]);
            am_house_allocationDTO.insertionDate = rs.getLong(columnNames[i++]);
            am_house_allocationDTO.searchColumn = rs.getString(columnNames[i++]);
            am_house_allocationDTO.isDeleted = rs.getInt(columnNames[i++]);
            am_house_allocationDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            return am_house_allocationDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Am_house_allocationDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "am_house_allocation";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Am_house_allocationDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Am_house_allocationDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

    public int getCountByHouseId(long houseID) {
        String countQuery = "SELECT count(*) as countID FROM "
                + getTableName() + " where isDeleted = 0 and house_id = ? ";
        return ConnectionAndStatementUtil.getT(countQuery, Arrays.asList(houseID), rs -> {
            try {
                return rs.getInt("countID");
            } catch (SQLException ex) {
                ex.printStackTrace();
                return 0;
            }
        }, 0);
    }

    public Am_house_allocationDTO getByAmHouseAllocationRequestId(Long amHouseAllocationRequestId) {
        String sql = String.format(getByAmHouseAllocationRequestId, amHouseAllocationRequestId);
        Am_house_allocationDTO am_house_allocationDTO = ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
        return am_house_allocationDTO;
    }

}
	