package am_house_allocation;

import am_house.Am_houseDAO;
import am_house.Am_houseDTO;
import am_house.Am_houseRepository;
import am_house_allocation_approval_mapping.Am_house_allocation_approval_mappingDAO;
import am_house_allocation_approval_mapping.Am_house_allocation_approval_mappingDTO;
import am_house_allocation_request.AmHouseAllocationRequestStatus;
import am_house_allocation_request.Am_house_allocation_requestDAO;
import am_house_allocation_request.Am_house_allocation_requestDTO;
import am_office_approval_mapping.Am_office_approval_mappingDAO;
import am_office_approval_mapping.Am_office_approval_mappingDTO;
import am_office_assignment_request.Am_office_assignment_requestDAO;
import am_office_assignment_request.Am_office_assignment_requestDTO;
import common.ApiResponse;
import common.BaseServlet;
import common.CommonDAOService;
import dbm.DBMW;
import files.FilesDAO;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import pb.Utils;
import permission.MenuConstants;
import role.PermissionRepository;
import role.RoleDTO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;
import vm_requisition.CommonApprovalStatus;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@WebServlet("/Am_house_allocationServlet")
@MultipartConfig
public class Am_house_allocationServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Am_house_allocationServlet.class);

    @Override
    public String getTableName() {
        return Am_house_allocationDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Am_house_allocationServlet";
    }

    @Override
    public Am_house_allocationDAO getCommonDAOService() {
        return Am_house_allocationDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.AM_HOUSE_ALLOCATION_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.AM_HOUSE_ALLOCATION_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.AM_HOUSE_ALLOCATION_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Am_house_allocationServlet.class;
    }

    FilesDAO filesDAO = new FilesDAO();

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Am_house_allocationDTO am_house_allocationDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

        if (Boolean.TRUE.equals(addFlag)) {
            am_house_allocationDTO = new Am_house_allocationDTO();
            am_house_allocationDTO.insertionDate = am_house_allocationDTO.lastModificationTime
                    = System.currentTimeMillis();
            am_house_allocationDTO.insertedByUserId = userDTO.ID;
            am_house_allocationDTO.insertedByOrganogramId = userDTO.organogramID;

        } else {
            am_house_allocationDTO = Am_house_allocationDAO.getInstance().getDTOByID
                    (Long.parseLong(request.getParameter("iD")));
            if (am_house_allocationDTO == null) {
                UtilCharacter.throwException("বাসা বরাদ্দ খুঁজে পাওয়া যায় নি  ", "House Allocation not found");
            }
            am_house_allocationDTO.lastModificationTime = System.currentTimeMillis();
            am_house_allocationDTO.modifiedBy = userDTO.ID;
        }

        if (request.getParameter("amHouseAllocationRequestId") == null || request.getParameter("amHouseAllocationRequestId").equals("-1")) {
            UtilCharacter.throwException("বাসা বরাদ্দ অনুরোধ খুঁজে পাওয়া যায় নি ",
                    "House assignment request information is not found");
        }
        String Val = request.getParameter("amHouseAllocationRequestId");
        am_house_allocationDTO.requestId = Long.parseLong(Val);
        Am_house_allocation_requestDTO am_house_allocation_requestDTO = Am_house_allocation_requestDAO.getInstance().getDTOFromID(am_house_allocationDTO.requestId);

//        if (am_house_allocation_requestDTO.status != AmHouseAllocationRequestStatus.WAITING_FOR_APPROVAL.getValue()) {
//        if (am_house_allocation_requestDTO.status != CommonApprovalStatus.REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue()) {
//            UtilCharacter.throwException("বাসা বরাদ্দ অনুরোধ এর অবস্থা অনুমোদনের জন্যে অপেক্ষমান নয় ",
//                    "Status not Waiting For Approval/ Pending");
//        }

        am_house_allocation_requestDTO.status = AmHouseAllocationRequestStatus.APPROVED.getValue();
        am_house_allocation_requestDTO.lastModificationTime = System.currentTimeMillis();

        long prevHouseId = am_house_allocationDTO.houseId;

        am_house_allocationDTO.houseId = Long.parseLong(request.getParameter("houseId"));
        Am_houseDTO houseDTO = Am_houseRepository.getInstance().getAm_houseDTOByID(am_house_allocationDTO.houseId);
        if (houseDTO != null) {
            Boolean addLogic = Boolean.TRUE.equals(addFlag) &&
                    houseDTO.status != CommonApprovalStatus.AVAILABLE.getValue();
            Boolean editLogic = Boolean.FALSE.equals(addFlag) && prevHouseId != am_house_allocationDTO.houseId
                    && houseDTO.status != CommonApprovalStatus.AVAILABLE.getValue();
            if (addLogic || editLogic) {
                UtilCharacter.throwException("বাসার অবস্থা ব্যবহারযোগ্য নয়  ", "House not Available");
            }
        } else {
            UtilCharacter.throwException("বাসা খুঁজে পাওয়া যায় নি ", "House not found");

        }

        am_house_allocationDTO.amHouseOldNewCat = Integer.parseInt(request.getParameter("amHouseOldNewCat"));
        CategoryLanguageModel categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel
                ("am_house_old_new", am_house_allocationDTO.amHouseOldNewCat);
        if (categoryLanguageModel == null) {
            UtilCharacter.throwException("নতুন বাসা/পুরাতন বাসা খুঁজে পাওয়া যায় নি", "House old/new is invalid");
        }

        am_house_allocationDTO.amHouseLocationCat = Integer.parseInt(request.getParameter("amHouseLocationCat"));
        categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel
                ("am_house_location", am_house_allocationDTO.amHouseLocationCat);
        if (categoryLanguageModel == null) {
            UtilCharacter.throwException("বাসার অবস্থান খুঁজে পাওয়া যায় নি ", "House location is invalid");
        }

        if (!StringUtils.isValidString(request.getParameter("amHouseAllocationQuota"))) {
            UtilCharacter.throwException("অনুগ্রহ করে বাসা বরাদ্দের কোটা প্রদান করুন", "Please provide quota for housing allocation");
        }
        am_house_allocationDTO.amHouseAllocationQuota = Integer.parseInt(request.getParameter("amHouseAllocationQuota"));
        categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel
                ("asset_house_allocation_quota", am_house_allocationDTO.amHouseAllocationQuota);
        if (categoryLanguageModel == null) {
            UtilCharacter.throwException("অনুগ্রহ করে বাসা বরাদ্দের কোটা প্রদান করুন", "Please provide quota for housing allocation");
        }

        am_house_allocationDTO.amHouseClassCat = Integer.parseInt(request.getParameter("amHouseClassCat"));
        categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel
                ("am_house_class", am_house_allocationDTO.amHouseClassCat);
        if (categoryLanguageModel == null) {
            UtilCharacter.throwException("বাসার শ্রেণী খুঁজে পাওয়া যায় নি ", "House class is invalid");
        }

        String houseSubClassCat = request.getParameter("amHouseSubClassCat");
        if(houseSubClassCat != null && !houseSubClassCat.equals("")){
            am_house_allocationDTO.amHouseClassSubCat = Integer.parseInt(houseSubClassCat);
        }

        String Value = Jsoup.clean(request.getParameter("allocationDate"), Whitelist.simpleText());
        if (Value != null && !Value.equalsIgnoreCase("")) {
            try {
                Date d = f.parse(Value);
                am_house_allocationDTO.allocationDate = d.getTime();
            } catch (Exception e) {
                e.printStackTrace();
                UtilCharacter.throwException("অবৈধ বরাদ্দ তারিখ  ", "Allocation Date inValid");
            }
        } else {
            UtilCharacter.throwException("অবৈধ বরাদ্দ তারিখ  ", "Allocation Date inValid");
        }

        Value = request.getParameter("filesDropzone");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        System.out.println("filesDropzone = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {

            System.out.println("filesDropzone = " + Value);

            am_house_allocationDTO.filesDropzone = Long.parseLong(Value);


            if (addFlag == false) {
                String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    System.out.println("going to delete " + deleteArray[i]);
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        } else {
            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
        }

        am_house_allocationDTO.requestId = am_house_allocation_requestDTO.iD;
        am_house_allocationDTO.committeeId = Long.parseLong(request.getParameter("committeeId"));

        am_house_allocationDTO.requesterEmpId = am_house_allocation_requestDTO.requesterEmpId;
        am_house_allocationDTO.requesterOfficeId = am_house_allocation_requestDTO.requesterOfficeId;
        am_house_allocationDTO.requesterOfficeUnitId = am_house_allocation_requestDTO.requesterOfficeUnitId;
        am_house_allocationDTO.requesterOrgId = am_house_allocation_requestDTO.requesterOrgId;
        am_house_allocationDTO.requesterNameBn = am_house_allocation_requestDTO.requesterNameBn;
        am_house_allocationDTO.requesterNameEn = am_house_allocation_requestDTO.requesterNameEn;
        am_house_allocationDTO.requesterOfficeNameBn = am_house_allocation_requestDTO.requesterOfficeNameBn;
        am_house_allocationDTO.requesterOfficeNameEn = am_house_allocation_requestDTO.requesterOfficeNameEn;
        am_house_allocationDTO.requesterOfficeUnitNameEn = am_house_allocation_requestDTO.requesterOfficeUnitNameEn;
        am_house_allocationDTO.requesterOfficeUnitNameBn = am_house_allocation_requestDTO.requesterOfficeUnitNameBn;
        am_house_allocationDTO.requesterOfficeUnitOrgNameBn = am_house_allocation_requestDTO.requesterOfficeUnitOrgNameBn;
        am_house_allocationDTO.requesterOfficeUnitOrgNameEn = am_house_allocation_requestDTO.requesterOfficeUnitOrgNameEn;
        am_house_allocationDTO.requesterPhoneNum = am_house_allocation_requestDTO.requesterPhoneNum;
        am_house_allocationDTO.status = CommonApprovalStatus.SATISFIED.getValue();


        if (Boolean.TRUE.equals(addFlag)) {
            getCommonDAOService().add(am_house_allocationDTO);
        } else {
            getCommonDAOService().update(am_house_allocationDTO);
        }
        Am_house_allocation_requestDAO.getInstance().update(am_house_allocation_requestDTO);

        houseDTO.status = CommonApprovalStatus.USED.getValue();
        houseDTO.lastModificationTime = am_house_allocationDTO.insertionDate;
        Am_houseDAO.getInstance().update(houseDTO);
        Am_house_allocation_approval_mappingDAO.getInstance().approve(request,userDTO,am_house_allocationDTO.requestId,true);

        return am_house_allocationDTO;

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {

                case "getAddPage":
                    long amHouseAllocationRequestId = Long.parseLong(request.getParameter("amHouseAllocationRequestId"));
                    setApprovalPageData(amHouseAllocationRequestId, request, userDTO);
                    request.getRequestDispatcher("am_house_allocation/am_house_allocationEdit.jsp").forward(request, response);
                    return;
                case "updateHouseReceiveDate":
                    Long id = Long.parseLong(request.getParameter("id"));
                    long receiveTime = f.parse(request.getParameter("houseReceiveDateUnixTime")).getTime();
                    Am_house_allocationDTO dto = Am_house_allocationDAO.getInstance().getDTOByID(id);
                    dto.receiveDate = receiveTime;
                    dto.lastModificationTime = System.currentTimeMillis();
                    Am_house_allocationDAO.getInstance().update(dto);
                    try {
                        ApiResponse.sendSuccessResponse(response, "Am_house_allocationServlet?actionType=search");
                    } catch (Exception ex) {
                        logger.error(ex);
                        ApiResponse.sendErrorResponse(response, ex.getMessage());
                    }
                    return;
                case "search":
                    if (Utils.checkPermission(commonLoginData.userDTO, getSearchMenuConstants()) && getSearchPagePermission(request)) {
                        RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
                        boolean isAdmin = role.ID == SessionConstants.ADMIN_ROLE || role.ID == SessionConstants.ASSET_ADMIN_ROLE;
                        
                        String whereClause = "";
                        if (!isAdmin) {
                            whereClause = " and requester_org_id = "+ HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.organogramID;
                        }

                        Map<String,String> params = HttpRequestUtils.buildRequestParams(request);
                        RecordNavigator recordNavigator = getCommonDAOService().getRecordNavigator(params,null,whereClause,null);
                        request.setAttribute(RECORD_NAVIGATOR,recordNavigator);
                        String ajax = request.getParameter("ajax");
                        String url;
                        if(ajax != null && ajax.equalsIgnoreCase("true")){
                            url = commonPartOfDispatchURL()+"SearchForm.jsp";
                        }else{
                            url = "pb/search.jsp";
                        }
                        request.getRequestDispatcher(url).forward(request, response);
                        return;
                    }
                    break;
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);


    }

    private void setApprovalPageData(long amHouseAllocationRequestId, HttpServletRequest request, UserDTO userDTO) throws Exception {
        Am_house_allocation_requestDTO am_house_allocation_requestDTO = Am_house_allocation_requestDAO.getInstance().getDTOByID(amHouseAllocationRequestId);
        if (am_house_allocation_requestDTO == null)
            throw new Exception("No application for house allocation is not found with id = " + amHouseAllocationRequestId);
        request.setAttribute("am_house_allocation_requestDTO", am_house_allocation_requestDTO);

        Am_house_allocation_approval_mappingDTO approverAmHouseAllocationRequestDTO =
                Am_house_allocation_approval_mappingDAO.getInstance().getByAmHouseAllocationRequestAndApproverRecordsId(amHouseAllocationRequestId, userDTO.employee_record_id);

        if (approverAmHouseAllocationRequestDTO == null)
            throw new Exception("User Not allowed to approve  for house with  id = " + amHouseAllocationRequestId);
        request.setAttribute("approverAmHouseAllocationRequestDTO", approverAmHouseAllocationRequestDTO);
    }
}

