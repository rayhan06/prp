package am_house_allocation;

import java.util.*;

import util.*;


public class Am_house_allocationDTO extends CommonDTO {

    public long houseId = -1;
    public int amHouseOldNewCat = -1;
    public int amHouseLocationCat = -1;
    public int amHouseClassCat = -1;
    public int amHouseClassSubCat = -1;
    public long allocationDate = System.currentTimeMillis();
    public long receiveDate = -1;
    public long requestId = -1;
    public long committeeId = -1;
    public long insertedByUserId = -1;
    public long insertedByOrganogramId = -1;
    public long modifiedBy = -1;
    public long insertionDate = -1;

    public long requesterOrgId = -1;
    public long requesterOfficeId = -1;
    public long requesterOfficeUnitId = -1;
    public long requesterEmpId = -1;
    public String requesterPhoneNum = "";
    public String requesterNameEn = "";
    public String requesterNameBn = "";
    public String requesterOfficeNameEn = "";
    public String requesterOfficeNameBn = "";
    public String requesterOfficeUnitNameEn = "";
    public String requesterOfficeUnitNameBn = "";
    public String requesterOfficeUnitOrgNameEn = "";
    public String requesterOfficeUnitOrgNameBn = "";

    public int status = -1;
    public long withdrawn_date = -1;

    public long filesDropzone = -1;
    public int amHouseAllocationQuota = -1;


    @Override
    public String toString() {
        return "$Am_house_allocationDTO[" +
                " iD = " + iD +
                " filesDropzone = " + filesDropzone +
                " houseId = " + houseId +
                " amHouseOldNewCat = " + amHouseOldNewCat +
                " amHouseLocationCat = " + amHouseLocationCat +
                " amHouseClassCat = " + amHouseClassCat +
                " amHouseClassSubCat = " + amHouseClassSubCat +
                " allocationDate = " + allocationDate +
                " requestId = " + requestId +
                " committeeId = " + committeeId +
                " insertedByUserId = " + insertedByUserId +
                " insertedByOrganogramId = " + insertedByOrganogramId +
                " modifiedBy = " + modifiedBy +
                " insertionDate = " + insertionDate +
                " searchColumn = " + searchColumn +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}