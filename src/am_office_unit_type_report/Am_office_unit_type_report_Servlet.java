package am_office_unit_type_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;


@WebServlet("/Am_office_unit_type_report_Servlet")
public class Am_office_unit_type_report_Servlet extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    String[][] Criteria =
            {
                    {"criteria", "v2", "isDeleted", "=", "", "String", "", "", "0", "isDeleted", LC.AM_OFFICE_UNIT_TYPE_REPORT_WHERE_ISDELETED + ""},
                    {"criteria", "v2", "office_unit_id", "=", "AND", "String", "", "", "any", "officeUnitId", LC.AM_OFFICE_UNIT_TYPE_REPORT_WHERE_OFFICEUNITID + ""},
                    {"criteria", "v2", "insertion_date", ">=", "AND", "long", "", "", Long.MIN_VALUE + "", "startDate", LC.HM_START_DATE + ""},
                    {"criteria", "v2", "insertion_date", "<=", "AND", "long", "", "", Long.MAX_VALUE + "", "endDate", LC.HM_END_DATE + ""},
            };

    String[][] Display =
            {
                    {"display", "v2", "concatedOfcTypeAndOfcUnit", "concated_to_office", ""},
                    {"display", "", "COUNT(*) ", "text", ""},
                    {"display", "", "SUM(CASE WHEN v2.STATUS = 1 THEN 1 ELSE 0 END) ", "text", ""},
                    {"display", "", "SUM(CASE WHEN v2.STATUS = 3 THEN 1 ELSE 0 END) ", "text", ""},
                    {"display", "", "SUM(CASE WHEN v2.STATUS = 4 THEN 1 ELSE 0 END) ", "text", ""},
                    {"display", "", "SUM(CASE WHEN v2.STATUS = 15 THEN 1 ELSE 0 END) ", "text", ""},
                    //{"display", "", "insertion_date", "date", ""},
            };

    String GroupBy = "v2.concatedOfcTypeAndOfcUnit";
    String OrderBY = "";

    ReportRequestHandler reportRequestHandler;

    public Am_office_unit_type_report_Servlet() {

    }

    private final ReportService reportService = new ReportService();

    private String sql;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String language = "english";
        if (userDTO != null && userDTO.languageID == SessionConstants.BANGLA) {
            language = "bangla";
        }

        String actionType = request.getParameter("actionType");

        System.out.println("In ssservlet doget, actiontype = " + actionType);

        sql = "am_office_assignment v2";

        Display[0][4] = LM.getText(LC.AM_OFFICE_UNIT_TYPE_REPORT_SELECT_CONCATEDOFCTYPEANDOFCUNIT, loginDTO);
        Display[1][4] = LM.getText(LC.AM_TYPE_OF_HOUSE_REPORT_SELECT_TOTAL, loginDTO);
        Display[2][4] = LM.getText(LC.AM_TYPE_OF_HOUSE_REPORT_SELECT_PENDING, loginDTO);
        Display[3][4] = LM.getText(LC.AM_TYPE_OF_HOUSE_REPORT_SELECT_APPROVED, loginDTO);
        Display[4][4] = LM.getText(LC.AM_TYPE_OF_HOUSE_REPORT_SELECT_CANCELLED, loginDTO);
        Display[5][4] = LM.getText(LC.AM_TYPE_OF_HOUSE_REPORT_SELECT_WITHDRAWN, loginDTO);
        //Display[6][4] = LM.getText(LC.INSPECTION_REPORT_ADD_SUBMISSIONDATE, loginDTO);


        String reportName = LM.getText(LC.AM_OFFICE_UNIT_TYPE_REPORT_OTHER_AM_OFFICE_UNIT_TYPE_REPORT, loginDTO);

        reportRequestHandler = new ReportRequestHandler(null,
                Criteria, Display, GroupBy, OrderBY, sql,
                reportService);


        reportRequestHandler.handleReportGet(request, response, userDTO, "am_office_unit_type_report",
                MenuConstants.AM_OFFICE_UNIT_TYPE_REPORT_DETAILS, language, reportName, "am_office_unit_type_report");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
