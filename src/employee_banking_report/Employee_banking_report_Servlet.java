package employee_banking_report;

import language.LC;
import language.LM;
import login.LoginDTO;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import org.apache.log4j.Logger;
import pb.Utils;
import pbReport.ReportService;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.ReportRequestHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@WebServlet("/Employee_banking_report_Servlet")
public class Employee_banking_report_Servlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Employee_banking_report_Servlet.class);

    String[][] Criteria;

    String[][] Display;

    String GroupBy = "";
    String OrderBY = " employee_records_id asc ";

    ReportRequestHandler reportRequestHandler;

    public Employee_banking_report_Servlet() {
    }

    private final ReportService reportService = new ReportService();

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String language = "english";
        if (userDTO != null && userDTO.languageID == SessionConstants.BANGLA) {
            language = "bangla";
        }

        String actionType = request.getParameter("actionType");

        logger.info("In ssservlet doget, actiontype = " + actionType);

        String sql = "  employee_bank_information join employee_records on employee_records.id = employee_bank_information.employee_records_id ";

        List<String[]> criteriaList = new ArrayList<>();
        criteriaList.add(new String[]{"criteria", "employee_bank_information", "isDeleted", "=", "", "int", "", "", "0", "none", ""});

        String userName = request.getParameter("userName");
        if (userName != null && !userName.equalsIgnoreCase("")) {
            criteriaList.add(new String[]{"criteria", "", "employee_number", "=", "AND", "String", "", "", "any", "userName", LC.PHARMACY_REPORT_WHERE_EMPLOYEEID + ""});
        }

        String officeUnitId = request.getParameter("officeUnitId");
        if (officeUnitId != null && !officeUnitId.equalsIgnoreCase("")) {
            List<String> userNames = OfficeUnitOrganogramsRepository.getInstance().getAllDescentsUserNamesInclusive(Long.parseLong(officeUnitId));
            String sNames = userNames.parallelStream()
                    .filter(Utils::digitOnlyOrEmptyValid)
                    .collect(Collectors.joining(","));

            if (!sNames.equalsIgnoreCase("")) {
                criteriaList.add(new String[]{"criteria", "", "employee_number", "in", "AND", "String", "", "", sNames, "userName", ""});
            }
        }

        String mp = request.getParameter("mp");
        if (mp != null && !mp.equalsIgnoreCase("")) {
            criteriaList.add(new String[]{"criteria", "", "employee_number", "like", "AND", "exact", "", "", "0%", "none", ""});
        }

        criteriaList.add(new String[]{"criteria", "", "bank_name_type", "=", "AND", "int", "", "", "any", "bank_name_type", ""});
        criteriaList.add(new String[]{"criteria", "employee_records", "employee_class_cat", "=", "AND", "int", "", "", "any", "employeeClassCat", String.valueOf(LC.EMPLOYEE_EMPLOYEE_CLASS)});
        criteriaList.add(new String[]{"criteria", "employee_records", "emp_officer_cat", "=", "AND", "int", "", "", "any", "empOfficerCat", String.valueOf(LC.EMPLOYEE_OFFICER_TYPE)});
        criteriaList.add(new String[]{"criteria", "employee_records", "employment_cat", "=", "AND", "int", "", "", "any", "employmentCat", String.valueOf(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY)});


        Criteria = new String[criteriaList.size()][];
        int i = 0;
        for (String[] sCrtiFromList : criteriaList) {
            Criteria[i++] = sCrtiFromList;
        }


        Display = new String[9][4];
        Display[0] = new String[]{"display", "",
                "employee_records_id",
                "erIdToOffice",
                LM.getText(LC.HM_OFFICE, loginDTO)};
        Display[1] = new String[]{"display", "",
                "employee_number",
                "text",
                language.equalsIgnoreCase("english") ? "UserID" : "ইউজার আইডি"};
        Display[2] = new String[]{"display", "",
                "employee_records_id",
                "erIdToName",
                LM.getText(LC.HM_NAME, loginDTO)};
        Display[3] = new String[]{"display", "",
                "employee_records_id",
                "erIdToPhone",
                LM.getText(LC.HM_PHONE, loginDTO)};
        Display[4] = new String[]{"display", "",
                "employee_records_id",
                "erIdToOrganogram",
                LM.getText(LC.HM_DESIGNATION, loginDTO)};
        Display[5] = new String[]{"display", "",
                "bank_name_type",
                "type",
                language.equalsIgnoreCase("english") ? "Bank" : "ব্যাংক"};

        Display[6] = new String[]{"display", "",
                "branch_name",
                "basic",
                language.equalsIgnoreCase("english") ? "Branch" : "শাখা"};

        Display[7] = new String[]{"display", "",
                "account_cat",
                "cat",
                language.equalsIgnoreCase("english") ? "Account Type" : "অ্যাকাউন্টের ধরণ"};

        Display[8] = new String[]{"display", "",
                "bank_account_number",
                "text",
                language.equalsIgnoreCase("english") ? "Account Number" : "অ্যাকাউন্ট নাম্বার"};


        String reportName = language.equalsIgnoreCase("english") ?
                "Employee Banking Report"
                : "কর্মচারীর ব্যাংকিং  রিপোর্ট";

        reportRequestHandler = new ReportRequestHandler(null,
                Criteria, Display, GroupBy, OrderBY, sql,
                reportService);


        reportRequestHandler.handleReportGet(request, response, userDTO, "employee_banking_report",
                MenuConstants.EB_REPORT, language, reportName, "employee_banking_report");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}