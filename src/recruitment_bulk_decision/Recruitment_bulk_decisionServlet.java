package recruitment_bulk_decision;

import com.google.gson.Gson;
import job_applicant_application.*;
import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import parliament_job_applicant.Parliament_job_applicantDAO;
import parliament_job_applicant.Parliament_job_applicantDTO;
import parliament_job_applicant.Parliament_job_applicantRepository;
import pb.CommonDAO;
import pb.Utils;
import permission.MenuConstants;
import recruitment_bulk_attendance.Recruitment_bulk_attendanceDAO;
import recruitment_bulk_attendance.Recruitment_bulk_attendanceDTO;

import recruitment_bulk_mark_entry.Recruitment_bulk_mark_entryDTO;
import recruitment_bulk_mark_entry.Recruitment_bulk_mark_entryMAPS;
import recruitment_job_description.Recruitment_job_descriptionDAO;
import recruitment_job_description.Recruitment_job_descriptionDTO;
import recruitment_job_description.Recruitment_job_descriptionRepository;
import recruitment_job_specific_exam_type.JobSpecificExamTypeDao;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import sms.SmsService;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;
import util.UtilCharacter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Servlet implementation class Recruitment_bulk_mark_entryServlet
 */
@WebServlet("/Recruitment_bulk_decisionServlet")
@MultipartConfig
public class Recruitment_bulk_decisionServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Recruitment_bulk_decisionServlet.class);

    String tableName = "recruitment_bulk_mark_entry";
    String parentAttTableName = "recruitment_bulk_attendance";
	String parentTableName = "job_applicant_application";

    //Job_applicant_applicationDAO jobApplicantApplicationDAO;
//	Recruitment_bulk_attendanceDAO recruitment_bulk_attendanceDAO;
	Job_applicant_applicationDAO job_applicant_applicationDAO;
    CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Recruitment_bulk_decisionServlet()
	{
        super();
    	try
    	{
//			recruitment_bulk_mark_entryDAO = new Recruitment_bulk_mark_entryDAO(tableName);
//			recruitment_bulk_attendanceDAO = new  Recruitment_bulk_attendanceDAO(parentAttTableName);
			job_applicant_applicationDAO = new Job_applicant_applicationDAO(parentTableName);
			commonRequestHandler = new CommonRequestHandler(job_applicant_applicationDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");

            if (actionType.equals("getCandidateInfoExcelList")) {


                String actionMessage = request.getParameter("message");

                if(actionMessage!=null ){

					//System.out.println("actiiiinMsg: "+actionMessage);

                	if(actionMessage.equalsIgnoreCase("success")){
						actionMessage="";
						request.setAttribute("message","success");
					}
                	else{
						actionMessage="";
						request.setAttribute("message","error");
					}


                }

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_CANDIDATE_LIST)) {
                    getCandidateList(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            }



			else if(actionType.equals("getUploadPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_MARK_ENTRY_UPLOAD))
				{
					//commonRequestHandler.geUploadPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}

			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_MARK_ENTRY_SEARCH))
				{
					//commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);

			if (actionType.equals("DownloadTemplate")) {

				String isDownload = request.getParameter("download_excel");
				String job_id = request.getParameter("job");
				String level_id = request.getParameter("level");

				List<Job_applicant_applicationDTO> job_applicant_applicationDTO_job_level = job_applicant_applicationDAO.getIDDTOsByJobLevelID(Integer.parseInt(job_id), Integer.parseInt(level_id));

				String job_name = request.getParameter("job_name");
				String level_name = request.getParameter("level_name");
				response.setContentType("application/vnd.ms-excel");
				//response.setHeader("Content-Disposition", "attachment; filename=employee_grade.xlsx");
				String file_name = "bulk_decision_"+job_name+"_"+level_name+".xlsx";
				response.setHeader("Content-Disposition", "attachment; filename="+file_name);

				XSSFWorkbook workbook    = UtilCharacter.newWorkBook();
				Sheet sheet = UtilCharacter.newSheet(workbook, LM.getText(LC.CANDIDATE_LIST_CANDIDATE_LIST,loginDTO));

				int row_index=0;
				int cell_col_index=0;
				boolean addMerged=true;
				UtilCharacter.newRow(workbook,sheet,job_name+","+level_name,row_index,row_index++,0,2,addMerged);

				addMerged =false;
				//row_index++;
				Row newRow = sheet.createRow(row_index);
				IndexedColors indexedColor = IndexedColors.BLACK1;
				boolean isBold=true;

				UtilCharacter.alreadyCreatedRow(workbook,sheet,"Roll",row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,true);
				UtilCharacter.alreadyCreatedRow(workbook,sheet,"Reason",row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,true);

				for (Job_applicant_applicationDTO records : job_applicant_applicationDTO_job_level) {

					row_index++;
					cell_col_index = 0;
					newRow = sheet.createRow(row_index);

					UtilCharacter.alreadyCreatedRow(workbook, sheet, String.valueOf(records.rollNumber), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold);
					UtilCharacter.alreadyCreatedRow(workbook,sheet,"",row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,true);




				}

				workbook.write(response.getOutputStream()); // Write workbook to response.
				workbook.close();

			}


			else if(actionType.equals("upload"))
			{
				String job_id = request.getParameter("job_id");
				String level_id = request.getParameter("level_id");
				boolean existence=true;
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_MARK_ENTRY_UPLOAD))
				{
					if(existence){
						uploadRecruitment_bulk_decision_entry(request, response, false);
					}
					else {

					}

				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}



			else if(actionType.equals("uploadConfirmed"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_MARK_ENTRY_UPLOAD))
				{
					System.out.println("uploadConfirmed");
					addRecruitment_bulk_decision(request, response,userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_UPDATE))
				{
					//commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}

			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}


		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void uploadRecruitment_bulk_decision_entry(HttpServletRequest request, HttpServletResponse response, Boolean addFlag) throws IOException
	{
		System.out.println("%%%% ajax upload called");
		Part filePart_recruitment_bulk_mark_entryDatabase;
		try
		{
			filePart_recruitment_bulk_mark_entryDatabase = request.getPart("testing_excelDatabase");
			String Value = commonRequestHandler.getFileName(filePart_recruitment_bulk_mark_entryDatabase);
			System.out.println("recruitment_bulk_decision_entryDatabase = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					String FileName = "recruitment_bulk_decision_entrydatabase";
					String path = getServletContext().getRealPath("/img2/");
					System.out.println("filePath: "+path);
					Utils.uploadFile(filePart_recruitment_bulk_mark_entryDatabase, FileName, path);
					ArrayList<Job_applicant_applicationDTO> job_applicant_applicationDTOS = ReadXLsToArraylist(request, FileName);
					HttpSession session = request.getSession(true);
					session.setAttribute("job_applicant_applicationDTOS", job_applicant_applicationDTOS);

					RequestDispatcher rd = request.getRequestDispatcher("recruitment_candidate_list/recruitment_bulk_decisionReview.jsp?actionType=edit");
					rd.forward(request, response);
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public ArrayList<Job_applicant_applicationDTO> ReadXLsToArraylist(HttpServletRequest request, String fileName) throws IOException
	{
		String path = getServletContext().getRealPath("/img2/");
		File excelFile = new File(path + File.separator
				+ fileName);
		FileInputStream fis = new FileInputStream(excelFile);


		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workbook.getSheetAt(0);
		Iterator<Row> rowIt = sheet.iterator();
		ArrayList<String> Rows = new ArrayList<String>();
		ArrayList<Job_applicant_applicationDTO> job_applicant_applicationDTOS = new ArrayList<Job_applicant_applicationDTO>();


		Job_applicant_applicationDTO job_applicant_applicationDTO;

		String failureMessage = "";
		int i = 0;

		Job_applicant_applicationMAPS job_applicant_applicationMAPS = new Job_applicant_applicationMAPS(parentTableName);
		//int truncateRow=0;
		int rowNumber=0;
		while(rowIt.hasNext())
		{

			Row row = rowIt.next();

			Iterator<Cell> cellIterator = row.cellIterator();

			job_applicant_applicationDTO = new Job_applicant_applicationDTO();

			int j = 0;

			try
			{
				if(rowNumber==0){
					rowNumber++;
					continue;
				}
				while (cellIterator.hasNext())
				{
					Cell cell = cellIterator.next();



					if(i == 0)
					{
						Rows.add(cell.toString());
						System.out.println("Rows found: " + cell + " ");
					}
					else
					{

						String rowName = Rows.get(j).toLowerCase();
						System.out.println("rowname: " + rowName + " rowname from map = " + job_applicant_applicationMAPS.java_Text_map.get(rowName));

						if(rowName == null || rowName.equalsIgnoreCase(""))
						{
							System.out.println("null row name");
							break;
						}
						if(cell == null || cell.toString().equalsIgnoreCase(""))
						{
							System.out.println("null cell");
							j++;
							continue;
						}
						else
						{
							//System.out.println("Inserting Value = " + Integer.parseInt(cell.toString()) + " to row " + rowName );

						}
						//System.out.println("maps roll2: "+job_applicant_applicationMAPS.java_Text_map.get(rowName));
						if(job_applicant_applicationMAPS.java_Text_map.get(rowName) != null && job_applicant_applicationMAPS.java_Text_map.get(rowName).equalsIgnoreCase("rollNumber"))
						{

							job_applicant_applicationDTO.rollNumber = String.valueOf( Math.round(Float.parseFloat(cell.toString())));
						}
						else if(job_applicant_applicationMAPS.java_Text_map.get(rowName) != null && job_applicant_applicationMAPS.java_Text_map.get(rowName).equalsIgnoreCase("decision_remarks"))
						{
							job_applicant_applicationDTO.decision_remarks = (cell.toString());
						}
					}
					j ++;

				}


				if(i != 0)
				{
					System.out.println("INSERTING to the list: " + job_applicant_applicationDTO);
					job_applicant_applicationDTOS.add(job_applicant_applicationDTO);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				failureMessage += (i + 1) + " ";
			}
			i ++;


			System.out.println();

		}
		if(failureMessage.equalsIgnoreCase(""))
		{
			failureMessage = " Successfully parsed all rows";
		}
		else
		{
			failureMessage = " Failed on rows: " + failureMessage;
		}
		request.setAttribute("failureMessage", failureMessage);

		workbook.close();
		fis.close();
		return job_applicant_applicationDTOS;

	}

	private void addRecruitment_bulk_decision(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException
	{
		String[] paramValues = request.getParameterValues("rollNumber");
		for (int i = 0; i < paramValues.length; i++)
		{
			String paramValue = paramValues[i];
			//Job_applicant_applicationDTO  job_applicant_applicationDTO  = job_applicant_applicationDAO.getDTOByJobIdAndLevelAndRollNumber (Long.parseLong(request.getParameterValues("jobId")[i]),Long.parseLong(request.getParameterValues("levelId")[i]),request.getParameterValues("rollNumber")[i]);

			Job_applicant_applicationDTO  job_applicant_applicationDTO  = Job_applicant_applicationRepository.getInstance().getJob_applicant_applicationDTOByJobIdAndLevelAndRollNumber(Long.parseLong(request.getParameterValues("jobId")[i]),Long.parseLong(request.getParameterValues("levelId")[i]),request.getParameterValues("rollNumber")[i]).get(0);

			try
			{
				String Value = "";

				if(job_applicant_applicationDTO!=null){

					if(request.getParameterValues("jobId") != null)
					{
						Value = request.getParameterValues("jobId")[i];
						if(Value != null)
						{
							Value = Jsoup.clean(Value,Whitelist.simpleText());
						}
						System.out.println("jobId = " + Value);
						if(Value != null && !Value.equalsIgnoreCase(""))
						{
						}
						else
						{
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
					}
					if(request.getParameterValues("levelId") != null)
					{
						Value = request.getParameterValues("levelId")[i];
						if(Value != null)
						{
							Value = Jsoup.clean(Value,Whitelist.simpleText());
						}
						System.out.println("levelId = " + Value);
						if(Value != null && !Value.equalsIgnoreCase(""))
						{

						}
						else
						{
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
					}
					if(request.getParameterValues("approve") != null)
					{
						Value = request.getParameterValues("approve")[i];
						if(Value != null)
						{
							Value = Jsoup.clean(Value,Whitelist.simpleText());
						}
						System.out.println("approve = " + Value);
						if(Value != null && !Value.equalsIgnoreCase(""))
						{

							
							//job_applicant_applicationDTO.isRejected = Value.equalsIgnoreCase("REJECTED")?Boolean.TRUE:Boolean.FALSE;
							job_applicant_applicationDTO.isRejected = Value.equalsIgnoreCase("REJECTED")?isRejected(job_applicant_applicationDTO,userDTO):Boolean.FALSE;
							job_applicant_applicationDTO.isSelected = Value.equalsIgnoreCase("REJECTED")?0: isSelected(job_applicant_applicationDTO,userDTO);

						}
						else
						{
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
					}
					if(request.getParameterValues("rollNumber") != null)
					{
						Value = request.getParameterValues("rollNumber")[i];
						if(Value != null)
						{
							Value = Jsoup.clean(Value,Whitelist.simpleText());
						}
						System.out.println("roll = " + Value);
						if(Value != null && !Value.equalsIgnoreCase(""))
						{
							//job_applicant_applicationDTO.rollNumber = Value;
							//job_applicant_applicationDTO.rollNumber = Value;
							//job_applicant_applicationDTO.iD =  recruitment_bulk_mark_entryDTO.roll-1000;
						}
						else
						{
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
					}
					if(request.getParameterValues("decision_remarks") != null)
					{
						Value = request.getParameterValues("decision_remarks")[i];
						if(Value != null)
						{
							Value = Jsoup.clean(Value,Whitelist.simpleText());
						}
						System.out.println("decision_remarks = " + Value);
						if(Value != null && !Value.equalsIgnoreCase(""))
						{
							job_applicant_applicationDTO.decision_remarks = Value;
						}
						else
						{
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
					}


					if(request.getParameterValues("modifiedBy") != null)
					{
						Value = request.getParameterValues("modifiedBy")[i];
						if(Value != null)
						{
							Value = Jsoup.clean(Value,Whitelist.simpleText());
						}
						System.out.println("modifiedBy = " + Value);
						if(Value != null && !Value.equalsIgnoreCase(""))
						{
							job_applicant_applicationDTO.modifiedBy = (Value);
						}
						else
						{
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
					}
					job_applicant_applicationDTO.lastModificationTime = System.currentTimeMillis();

					System.out.println("Done adding  addRecruitment_bulk_mark_entry dto = " + job_applicant_applicationDTO);
					job_applicant_applicationDAO.update(job_applicant_applicationDTO);

				}






			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

		}
		response.sendRedirect("Recruitment_bulk_decisionServlet?actionType=getCandidateInfoExcelList&message=success");
	}

	public int isSelected(Job_applicant_applicationDTO job_applicant_applicationDTO,UserDTO userDTO) throws Exception {

		int selected=0;
		Job_applicant_applicationServlet jobApplicantApplicationServlet = new Job_applicant_applicationServlet();

		int nextLevel = new JobSpecificExamTypeDao().
				getNextLevelFromJobIdAndCurrentLevel(job_applicant_applicationDTO.jobId, job_applicant_applicationDTO.level);
		if(nextLevel > 0){
			// next level data add
			jobApplicantApplicationServlet.deleteNextLevelData(job_applicant_applicationDTO);
			jobApplicantApplicationServlet.duplicateEntryWithNextLevel(job_applicant_applicationDTO, nextLevel, userDTO);
			selected = 1;
		} else {
			selected = 2;
		}
		job_applicant_applicationDAO.update(job_applicant_applicationDTO);

		// send msg
		Parliament_job_applicantDTO jobApplicantDTO = Parliament_job_applicantRepository.getInstance()
				.getParliament_job_applicantDTOByID(job_applicant_applicationDTO.jobApplicantId);
//				new Parliament_job_applicantDAO().
//				getDTOByID(job_applicant_applicationDTO.jobApplicantId);
		Recruitment_job_descriptionDTO jobDescriptionDTO =
				Recruitment_job_descriptionRepository.getInstance().
						getRecruitment_job_descriptionDTOByID(job_applicant_applicationDTO.jobId);
		String msg = "";
		if(selected == 1){
			msg = "You have been shortlisted for next level of " + jobDescriptionDTO.jobTitleEn + " post.";
		} else if(selected == 2){
			msg = "You have been final-shortlisted for the post of " + jobDescriptionDTO.jobTitleEn + ".";
		}
		SmsService.send(jobApplicantDTO.contactNumber,
				String.format("Dear %s, " + msg, jobApplicantDTO.nameEn ));
		new Job_applicant_applicationServlet().sendMail(jobApplicantDTO.email,
				String.format("Dear %s, " + msg, jobApplicantDTO.nameEn ),
				"Parliament Recruitment");


		return  selected;

	}
	public boolean isRejected(Job_applicant_applicationDTO job_applicant_applicationDTO,UserDTO userDTO) throws Exception {

		boolean rejected=true;
		Job_applicant_applicationServlet jobApplicantApplicationServlet = new Job_applicant_applicationServlet();

		job_applicant_applicationDAO.update(job_applicant_applicationDTO);

		// next steps data delete
		jobApplicantApplicationServlet.deleteNextLevelData(job_applicant_applicationDTO);
		return  rejected;

	}


    void getCandidateList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd;
        rd = request.getRequestDispatcher("recruitment_candidate_list/recruitment_job_specific_candidate_decision_info_excel_list.jsp");
        rd.forward(request, response);
    }

}

