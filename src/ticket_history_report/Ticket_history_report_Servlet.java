package ticket_history_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Ticket_history_report_Servlet")
public class Ticket_history_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","orig_support_orgs","like","","String","","","any","forwardToOrganogramId", LC.HM_ASSIGNED_TO + ""},		
		{"criteria","","insertion_date",">=","AND","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","insertion_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},		
		{"criteria","support_ticket","priority_cat","=","AND","int","","","any","priorityCat", LC.TICKET_HISTORY_REPORT_WHERE_PRIORITYCAT + ""},
		{"criteria","","issue_raiser_organogram_id","=","AND","int","","","any","issueRaiserOrganogramId", LC.SOLVED_TICKET_REPORT_WHERE_ISSUERAISERORGANOGRAMID + ""},	
		{"criteria","support_ticket","ticket_status_cat","=","AND","int","","","any","ticketStatusCat", LC.HM_STATUS + ""}		
	};
	
	String[][] Display =
	{
		{"display","support_ticket","id","text",""},		
		{"display","support_ticket","ticket_issues_type","type",""},		
		{"display","support_ticket","ticket_issues_subtype_type","type",""},		
		{"display","","description","basic",""},	
		{"display","","issue_raiser_organogram_id","organogram_and_wing",""},
		{"display","support_ticket","ticket_status_cat","cat",""},		
		{"display","support_ticket","priority_cat","cat",""},		
		{"display","","orig_support_orgs","organograms",""},		
		{"display","","insertion_date","date",""}		
	};
	
	String GroupBy = "";
	String OrderBY = "insertion_date DESC";
	
	ReportRequestHandler reportRequestHandler;
	
	public Ticket_history_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		boolean isLangEng = true;
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
			isLangEng = false;
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "support_ticket ";
		

		Display[0][4] = isLangEng ? "Ticket#" : "টিকেট#";
		Display[1][4] = isLangEng ? "Problem Type" : "সমস্যার ধরণ";
		Display[2][4] = isLangEng ? "Problem Sub Type" : "সমস্যার উপধরণ";
		Display[3][4] = isLangEng ? "Problem Details" : "সমস্যার বিবরণ";
		Display[4][4] = isLangEng ? "Issue Raiser" : "সমস্যা উত্থাপনকারী";
		Display[5][4] = isLangEng ? "Status" : "অবস্থা";
		Display[6][4] = isLangEng ? "Priority" : "গুরুত্ব";
		Display[7][4] = isLangEng ? "Support Engineer" : "সাপোর্ট ইঞ্জিনিয়ার";
		Display[8][4] = LM.getText(LC.HM_DATE, loginDTO);

		
		String reportName = LM.getText(LC.TICKET_HISTORY_REPORT_OTHER_TICKET_HISTORY_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "ticket_history_report",
				MenuConstants.TICKET_HISTORY_REPORT_DETAILS, language, reportName, "ticket_history_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
