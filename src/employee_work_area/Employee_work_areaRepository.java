package employee_work_area;

import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.CommonDTO;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class Employee_work_areaRepository implements Repository {
	private final Employee_work_areaDAO employee_work_areaDAO;

	private static final Logger logger = Logger.getLogger(Employee_work_areaRepository.class);
	private final Map<Long, Employee_work_areaDTO>mapOfEmployee_work_areaDTOToiD;
	List<Employee_work_areaDTO> employeeWorkAreaDTOList;

	private Employee_work_areaRepository(){
		employee_work_areaDAO = new Employee_work_areaDAO();
		mapOfEmployee_work_areaDTOToiD = new ConcurrentHashMap<>();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
		static Employee_work_areaRepository INSTANCE = new Employee_work_areaRepository();
	}

	public synchronized static Employee_work_areaRepository getInstance(){
		return LazyLoader.INSTANCE;
	}

	public void reload(boolean reloadAll){
		employee_work_areaDAO.getAllEmployee_work_area(reloadAll)
				.stream()
				.peek(this::removeDTOIfPresent)
				.filter(dto->dto.isDeleted == 0)
				.forEach(dto->mapOfEmployee_work_areaDTOToiD.put(dto.iD,dto));
		employeeWorkAreaDTOList = new ArrayList<>(this.mapOfEmployee_work_areaDTOToiD.values());
		employeeWorkAreaDTOList.sort(Comparator.comparingLong(o -> o.iD));
	}

	private void removeDTOIfPresent(Employee_work_areaDTO dto){
		if(dto!=null){
			if(mapOfEmployee_work_areaDTOToiD.get(dto.iD)!=null){
				mapOfEmployee_work_areaDTOToiD.remove(dto.iD);
			}
		}
	}
	
	public List<Employee_work_areaDTO> getEmployee_work_areaList() {
		return employeeWorkAreaDTOList == null ? new ArrayList<>() : employeeWorkAreaDTOList;
	}
	
	
	public Employee_work_areaDTO getEmployee_work_areaDTOByID(long id){
		if(mapOfEmployee_work_areaDTOToiD.get(id) == null){
			synchronized (this){
				if(mapOfEmployee_work_areaDTOToiD.get(id) == null){
					try {
						CommonDTO dto = employee_work_areaDAO.getDTOByID(id);
						if(dto != null){
							mapOfEmployee_work_areaDTOToiD.put(dto.iD, (Employee_work_areaDTO) dto);
						}
					} catch (Exception e) {
						logger.error(e);
					}
				}
			}
		}
		return mapOfEmployee_work_areaDTOToiD.get(id);
	}

	public String buildOptions(String language, Long selectedId) {
		List<OptionDTO> optionDTOList = null;
		if (employeeWorkAreaDTOList!=null && employeeWorkAreaDTOList.size() > 0){
			optionDTOList = employeeWorkAreaDTOList.stream()
					.map(dto->new OptionDTO(dto.nameEn,dto.nameBn,String.valueOf(dto.iD)))
					.collect(Collectors.toList());
		}
		return Utils.buildOptions(optionDTOList,language,selectedId == null ? null :String.valueOf(selectedId));
	}

	public String getText(long id,String language){
		Employee_work_areaDTO dto = getEmployee_work_areaDTOByID(id);
		return dto == null ? "" : ("Bangla".equalsIgnoreCase(language) ? dto.nameBn : dto.nameEn);
	}
	
	@Override
	public String getTableName() {
		return "employee_work_area";
	}
}


