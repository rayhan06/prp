package employee_work_area;

import common.CommonDTOService;
import org.apache.log4j.Logger;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.List;

@SuppressWarnings({"rawtypes","Duplicates"})
public class Employee_work_areaDAO extends NavigationService4 implements CommonDTOService<Employee_work_areaDTO> {

    private static final Logger logger = Logger.getLogger(Employee_work_areaDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (name_en,name_bn,description,lastModificationTime,isDeleted,ID) VALUES(?,?,?,?,?,?)";

    private static final String updateQuery = "UPDATE {tableName} SET name_en=?,name_bn=?,description=?,lastModificationTime =? WHERE ID = ?";

    public Employee_work_areaDAO(String tableName) {
        super(tableName);
        joinSQL = "";
        commonMaps = new Employee_work_areaMAPS(tableName);
    }

    public Employee_work_areaDAO() {
        this("employee_work_area");
    }

    @Override
    public void set(PreparedStatement ps, Employee_work_areaDTO employee_work_areaDTO, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setObject(++index, employee_work_areaDTO.nameEn);
        ps.setObject(++index, employee_work_areaDTO.nameBn);
        ps.setObject(++index, employee_work_areaDTO.description);
        ps.setObject(++index, System.currentTimeMillis());
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, employee_work_areaDTO.iD);
    }

    @Override
    public Employee_work_areaDTO buildObjectFromResultSet(ResultSet rs){
        try{
            Employee_work_areaDTO employee_work_areaDTO = new Employee_work_areaDTO();
            employee_work_areaDTO.iD = rs.getLong("ID");
            employee_work_areaDTO.nameEn = rs.getString("name_en");
            employee_work_areaDTO.nameBn = rs.getString("name_bn");
            employee_work_areaDTO.description = rs.getString("description");
            employee_work_areaDTO.isDeleted = rs.getInt("isDeleted");
            employee_work_areaDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return employee_work_areaDTO;
        }catch (SQLException ex){
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "employee_work_area";
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_work_areaDTO) commonDTO, addQuery, true);
    }

    public CommonDTO getDTOByID(long ID) throws Exception {
        return getDTOFromID(ID);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_work_areaDTO) commonDTO, updateQuery, false);
    }

    public List<Employee_work_areaDTO> getAllEmployee_work_area(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }

    public List<Employee_work_areaDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }

    public List<Employee_work_areaDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
                                               String filter, boolean tableHasJobCat) {
        String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
        return getDTOs(sql);
    }

}
	