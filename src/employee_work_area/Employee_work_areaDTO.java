package employee_work_area;

import util.CommonDTO;

public class Employee_work_areaDTO extends CommonDTO {

    public String nameEn = "";
    public String nameBn = "";
    public String description = "";

    @Override
    public String toString() {
        return "$Employee_work_areaDTO[" +
                " iD = " + iD +
                " nameEn = " + nameEn +
                " nameBn = " + nameBn +
                " description = " + description +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}