package employee_work_area;

import util.CommonMaps;


public class Employee_work_areaMAPS extends CommonMaps
{	
	public Employee_work_areaMAPS(String tableName)
	{
		
		java_allfield_type_map.put("name_en".toLowerCase(), "String");
		java_allfield_type_map.put("name_bn".toLowerCase(), "String");
		java_allfield_type_map.put("description".toLowerCase(), "String");


		java_anyfield_search_map.put(tableName + ".name_en".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".description".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("nameEn".toLowerCase(), "nameEn".toLowerCase());
		java_DTO_map.put("nameBn".toLowerCase(), "nameBn".toLowerCase());
		java_DTO_map.put("description".toLowerCase(), "description".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("name_en".toLowerCase(), "nameEn".toLowerCase());
		java_SQL_map.put("name_bn".toLowerCase(), "nameBn".toLowerCase());
		java_SQL_map.put("description".toLowerCase(), "description".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("English Name".toLowerCase(), "nameEn".toLowerCase());
		java_Text_map.put("Bangla Name".toLowerCase(), "nameBn".toLowerCase());
		java_Text_map.put("Description".toLowerCase(), "description".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}