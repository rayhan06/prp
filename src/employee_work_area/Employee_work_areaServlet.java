package employee_work_area;

import com.google.gson.Gson;
import common.ApiResponse;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet("/Employee_work_areaServlet")
@MultipartConfig
public class Employee_work_areaServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Employee_work_areaServlet.class);
    private static final String tableName = "employee_work_area";
    private final Employee_work_areaDAO employee_work_areaDAO;
    private final CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    public Employee_work_areaServlet() {
        super();
        employee_work_areaDAO = new Employee_work_areaDAO(tableName);
        commonRequestHandler = new CommonRequestHandler(employee_work_areaDAO);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getAddPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_WORK_AREA_ADD)) {
                        commonRequestHandler.getAddPage(request, response);
                        return;
                    }
                    break;
                case "getEditPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_WORK_AREA_UPDATE)) {
                        getEmployee_work_area(request, response);
                        return;
                    }
                    break;
                case "getURL":
                    response.sendRedirect(request.getParameter("URL"));
                    return;
                case "search":
                    logger.debug("search requested");
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_WORK_AREA_SEARCH)) {
                        searchEmployee_work_area(request, response);
                        return;
                    }
                    break;
                case "view":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_WORK_AREA_SEARCH)) {
                        commonRequestHandler.view(request, response);
                        return;
                    }
                    break;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "ajax_add":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_WORK_AREA_ADD)) {
                        try {
                            addEmployee_work_area(request,true, userDTO);
                            ApiResponse.sendSuccessResponse(response, "Employee_work_areaServlet?actionType=search");
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    }
                    break;
                case "getDTO":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_WORK_AREA_ADD)) {
                        getDTO(request, response);
                        return;
                    }
                    break;
                case "ajax_edit":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_WORK_AREA_UPDATE)) {
                        try {
                            addEmployee_work_area(request, false, userDTO);
                            ApiResponse.sendSuccessResponse(response, "Employee_work_areaServlet?actionType=search");
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    }
                    break;
                case "delete":
                    deleteEmployee_work_area(request, response, userDTO);
                    return;
                case "search":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_WORK_AREA_SEARCH)) {
                        searchEmployee_work_area(request, response);
                        return;
                    }
                    break;
                case "getGeo":
                    request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void getDTO(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Employee_work_areaDTO employee_work_areaDTO = (Employee_work_areaDTO) employee_work_areaDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        String encoded = this.gson.toJson(employee_work_areaDTO);
        out.print(encoded);
        out.flush();
    }

    private void addEmployee_work_area(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isEnglish = userDTO.languageID == CommonConstant.Language_ID_English;

        Employee_work_areaDTO employee_work_areaDTO;
        if (addFlag) {
            employee_work_areaDTO = new Employee_work_areaDTO();
        } else {
            employee_work_areaDTO = (Employee_work_areaDTO) employee_work_areaDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
        }
        employee_work_areaDTO.lastModificationTime = System.currentTimeMillis();

        employee_work_areaDTO.nameEn = Jsoup.clean(request.getParameter("nameEn"), Whitelist.simpleText());
        if(StringUtils.isBlank(employee_work_areaDTO.nameEn))
            throw new Exception(isEnglish? "Please insert English Name" : "ইংরেজি নাম দিন");

        employee_work_areaDTO.nameBn = Jsoup.clean(request.getParameter("nameBn"), Whitelist.simpleText());
        if(StringUtils.isBlank(employee_work_areaDTO.nameBn))
            throw new Exception(isEnglish? "Please insert Bangla Name" : "বাংলা নাম দিন");

        employee_work_areaDTO.description = Jsoup.clean(request.getParameter("description"), Whitelist.simpleText());
        if(StringUtils.isBlank(employee_work_areaDTO.description))
            throw new Exception(isEnglish? "Please insert Description" : "বিবরণ দিন");

        employee_work_areaDAO.manageWriteOperations(
                employee_work_areaDTO,
                addFlag ? SessionConstants.INSERT : SessionConstants.UPDATE,
                -1, userDTO
        );
    }


    private void deleteEmployee_work_area(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception {
        String[] IDsToDelete = request.getParameterValues("ID");
        for (String s : IDsToDelete) {
            long id = Long.parseLong(s);
            Employee_work_areaDTO employee_work_areaDTO = (Employee_work_areaDTO) employee_work_areaDAO.getDTOByID(id);
            employee_work_areaDAO.manageWriteOperations(employee_work_areaDTO, SessionConstants.DELETE, id, userDTO);
        }
        response.sendRedirect("Employee_work_areaServlet?actionType=search");
    }

    private void getEmployee_work_area(HttpServletRequest request, HttpServletResponse response, long id) throws Exception {
        Employee_work_areaDTO employee_work_areaDTO = (Employee_work_areaDTO) employee_work_areaDAO.getDTOByID(id);

        request.setAttribute("ID", employee_work_areaDTO.iD);
        request.setAttribute("employee_work_areaDTO", employee_work_areaDTO);
        request.setAttribute("employee_work_areaDAO", employee_work_areaDAO);

        String URL;
        String getBodyOnly = request.getParameter("getBodyOnly");
        if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
            URL = "employee_work_area/employee_work_areaEditBody.jsp?actionType=edit";
        } else {
            URL = "employee_work_area/employee_work_areaEdit.jsp?actionType=edit";
        }
        request.getRequestDispatcher(URL).forward(request, response);
    }


    private void getEmployee_work_area(HttpServletRequest request, HttpServletResponse response) throws Exception {
        getEmployee_work_area(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void searchEmployee_work_area(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");

        logger.debug("ajax = " + ajax + " hasAjax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_EMPLOYEE_WORK_AREA,
                request,
                employee_work_areaDAO,
                SessionConstants.VIEW_EMPLOYEE_WORK_AREA,
                SessionConstants.SEARCH_EMPLOYEE_WORK_AREA,
                tableName,
                true,
                userDTO,
                "",
                true
        );

        rnManager.doJob(loginDTO);

        request.setAttribute("employee_work_areaDAO", employee_work_areaDAO);

        if (!hasAjax) {
            logger.debug("Going to employee_work_area/employee_work_areaSearch.jsp");
            request.getRequestDispatcher("employee_work_area/employee_work_areaSearch.jsp").forward(request, response);
        } else {
            logger.debug("Going to employee_work_area/employee_work_areaSearchForm.jsp");
            request.getRequestDispatcher("employee_work_area/employee_work_areaSearchForm.jsp").forward(request, response);
        }
    }
}

