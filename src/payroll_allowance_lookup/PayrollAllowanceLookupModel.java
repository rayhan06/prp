package payroll_allowance_lookup;

import payroll_allowance_configuration.Payroll_allowance_configurationRepository;

public class PayrollAllowanceLookupModel {
    public long lookupDTOId;
    public long payrollAllowanceConfigId;
    public long amount;
    public String allowanceName;

    public PayrollAllowanceLookupModel(Payroll_allowance_lookupDTO dto, String language) {
        lookupDTOId = dto.iD;
        payrollAllowanceConfigId = dto.payrollAllowanceConfigId;
        amount = Math.max(0, dto.amount);
        allowanceName = Payroll_allowance_configurationRepository.getInstance().getAllowanceName(
                payrollAllowanceConfigId, language
        );
    }
}
