package payroll_allowance_lookup;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import office_unit_organograms.SameDesignationGroup;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class Payroll_allowance_lookupDAO implements CommonDAOService<Payroll_allowance_lookupDTO> {
    private static final Logger logger = Logger.getLogger(Payroll_allowance_lookupDAO.class);
    private static final String addQuery =
            "INSERT INTO {tableName} (organogram_id,organogram_key,employment_cat,payroll_allowance_config_id,amount,"
                    .concat("modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?,?)");
    private static final String updateQuery =
            "UPDATE {tableName} SET organogram_id=?,organogram_key=?,employment_cat=?,payroll_allowance_config_id=?,"
                    .concat("amount=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final String getByEmploymentOrganogram =
            "SELECT * FROM payroll_allowance_lookup WHERE employment_cat = ? AND organogram_key = ? AND isDeleted = 0";

    private final Map<String, String> searchMap = new HashMap<>();

    private Payroll_allowance_lookupDAO() {
        searchMap.put("organogram_key", " and (organogram_key = ?)");
    }

    private static class LazyLoader {
        static final Payroll_allowance_lookupDAO INSTANCE = new Payroll_allowance_lookupDAO();
    }

    public static Payroll_allowance_lookupDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Payroll_allowance_lookupDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.organogramId);
        ps.setString(++index, dto.organogramKey);
        ps.setInt(++index, dto.employmentCat);
        ps.setLong(++index, dto.payrollAllowanceConfigId);
        ps.setLong(++index, dto.amount);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Payroll_allowance_lookupDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Payroll_allowance_lookupDTO dto = new Payroll_allowance_lookupDTO();
            dto.iD = rs.getLong("ID");
            dto.organogramId = rs.getLong("organogram_id");
            dto.organogramKey = rs.getString("organogram_key");
            dto.employmentCat = rs.getInt("employment_cat");
            dto.payrollAllowanceConfigId = rs.getLong("payroll_allowance_config_id");
            dto.amount = rs.getLong("amount");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "payroll_allowance_lookup";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Payroll_allowance_lookupDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Payroll_allowance_lookupDTO) commonDTO, updateQuery, false);
    }

    public List<Payroll_allowance_lookupDTO> getDTOs(int employmentCat, String organogramKey) {
        return getDTOs(
                getByEmploymentOrganogram,
                Arrays.asList(employmentCat, organogramKey)
        );
    }

    public List<Payroll_allowance_lookupDTO> getLatestLookupDTOs(Set<Long> latestConfigIds, int employmentCat, String organogramKey,
                                                                 long requestBy, long requestTime) {
        List<Payroll_allowance_lookupDTO> currentDTOs = getDTOs(employmentCat, organogramKey);
        if (currentDTOs == null) currentDTOs = new ArrayList<>();

        Map<Boolean, List<Payroll_allowance_lookupDTO>> presentInCurrent =
                currentDTOs.stream()
                           .collect(Collectors.partitioningBy(dto -> latestConfigIds.contains(dto.payrollAllowanceConfigId)));

        deleteMissingConfigIds(requestBy, requestTime, presentInCurrent.get(false));

        List<Payroll_allowance_lookupDTO> latestLookupDTOs = presentInCurrent.getOrDefault(true, new ArrayList<>());
        latestLookupDTOs.addAll(getNewAdditionDTOs(latestConfigIds, currentDTOs));

        return latestLookupDTOs;
    }

    private List<Payroll_allowance_lookupDTO> getNewAdditionDTOs(Set<Long> latestConfigIds,
                                                                 List<Payroll_allowance_lookupDTO> currentDTOs) {
        Set<Long> currentConfigIds = currentDTOs.stream()
                                                .map(configDTO -> configDTO.payrollAllowanceConfigId)
                                                .collect(Collectors.toSet());
        return latestConfigIds.stream()
                              .filter(configId -> !currentConfigIds.contains(configId))
                              .map(Payroll_allowance_lookupDTO::new)
                              .collect(Collectors.toList());
    }

    private void deleteMissingConfigIds(long requestBy, long requestTime, List<Payroll_allowance_lookupDTO> missingFromCurrent) {
        if (missingFromCurrent == null || missingFromCurrent.isEmpty())
            return;
        delete(
                requestBy,
                missingFromCurrent.stream()
                                  .map(dto -> dto.iD)
                                  .collect(Collectors.toList()),
                requestTime
        );
    }

    private static final String deleteByAllowanceConfigIdSQLQuery =
            "UPDATE payroll_allowance_lookup SET isDeleted = 1,lastModificationTime = %d,modified_by = %d "
                    .concat("WHERE payroll_allowance_config_id IN (%s)");

    @SuppressWarnings("UnusedReturnValue")
    public boolean deleteByAllowanceConfigId(List<Long> allowanceConfigIds, long modifiedBy, long deleteTime) {
        if (allowanceConfigIds == null || allowanceConfigIds.isEmpty()) return true;

        String idStr = allowanceConfigIds.stream()
                                         .distinct()
                                         .map(String::valueOf)
                                         .collect(Collectors.joining(","));
        String sql = String.format(deleteByAllowanceConfigIdSQLQuery, deleteTime, modifiedBy, idStr);
        logger.debug("--- sql " + sql);
        return (boolean) ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            try {
                model.getStatement().executeUpdate(sql);
                recordUpdateTime(model.getConnection(), getTableName(), deleteTime);
                return true;
            } catch (SQLException ex) {
                ex.printStackTrace();
                return false;
            }
        });
    }

    public void addLookupDTO(Payroll_allowance_lookupServlet.UserInput userInput, boolean addFlag) throws Exception {
        Payroll_allowance_lookupDTO lookupDTO;
        if (addFlag) {
            lookupDTO = new Payroll_allowance_lookupDTO();
            lookupDTO.insertedBy = userInput.modifierId;
            lookupDTO.insertionTime = userInput.lastModificationTime;
        } else {
            lookupDTO = Payroll_allowance_lookupDAO.getInstance().getDTOFromID(userInput.lookupDTOId);
            if (lookupDTO == null) {
                logger.error("Trying to edit missing Payroll_allowance_lookupDTO with ID = " + userInput.lookupDTOId);
                return;
            }
        }
        lookupDTO.modifiedBy = userInput.modifierId;
        lookupDTO.lastModificationTime = userInput.lastModificationTime;

        lookupDTO.organogramId = userInput.organogramId;
        lookupDTO.organogramKey = new SameDesignationGroup(lookupDTO.organogramId).organogramKey;

        lookupDTO.employmentCat = userInput.employmentCat;
        lookupDTO.payrollAllowanceConfigId = userInput.configId;
        lookupDTO.amount = userInput.amount;

        if (addFlag) {
            Payroll_allowance_lookupDAO.getInstance().add(lookupDTO);
        } else {
            Payroll_allowance_lookupDAO.getInstance().update(lookupDTO);
        }
    }
}