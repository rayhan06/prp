package payroll_allowance_lookup;

import util.CommonDTO;

public class Payroll_allowance_lookupDTO extends CommonDTO {
    public long organogramId = -1;
    public String organogramKey = "";
    public int employmentCat = -1;
    public long payrollAllowanceConfigId = -1;
    public long amount = -1;
    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;

    public Payroll_allowance_lookupDTO(long payrollAllowanceConfigId) {
        this.payrollAllowanceConfigId = payrollAllowanceConfigId;
    }

    public Payroll_allowance_lookupDTO() {
    }

    @Override
    public String toString() {
        return "$Payroll_allowance_lookupDTO[" +
                " iD = " + iD +
                " organogramKey = " + organogramKey +
                " employmentCat = " + employmentCat +
                " payrollAllowanceConfigId = " + payrollAllowanceConfigId +
                " amount = " + amount +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " isDeleted = " + isDeleted +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }
}