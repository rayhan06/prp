package payroll_allowance_lookup;

import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class Payroll_allowance_lookupRepository implements Repository {
    private static final Logger logger = Logger.getLogger(Payroll_allowance_lookupRepository.class);

    private final Payroll_allowance_lookupDAO dao;
    private final Map<Long, Payroll_allowance_lookupDTO> mapById;
    private final Map<String, List<Payroll_allowance_lookupDTO>> mapByOrganogramKey;

    private Payroll_allowance_lookupRepository() {
        dao = Payroll_allowance_lookupDAO.getInstance();
        mapById = new ConcurrentHashMap<>();
        mapByOrganogramKey = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Payroll_allowance_lookupRepository INSTANCE = new Payroll_allowance_lookupRepository();
    }

    public static Payroll_allowance_lookupRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        logger.debug("Payroll_allowance_lookupRepository reload start for, reloadAll : " + reloadAll);
        List<Payroll_allowance_lookupDTO> dtoList = dao.getAllDTOs(reloadAll);
        if (dtoList != null && dtoList.size() > 0) {
            dtoList.stream()
                   .peek(this::removeIfPresent)
                   .filter(dto -> dto.isDeleted == 0)
                   .forEach(this::storeInCache);
        }
        logger.debug("Payroll_allowance_lookupRepository reload end for, reloadAll : " + reloadAll);
    }

    private void removeIfPresent(Payroll_allowance_lookupDTO dto) {
        if (dto == null) return;

        if (mapById.get(dto.iD) != null) {
            mapById.remove(dto.iD);
        }
        List<Payroll_allowance_lookupDTO> dtoList = mapByOrganogramKey.get(dto.organogramKey);
        if (dtoList != null) {
            dtoList.removeIf(d -> d.iD == dto.iD);
        }
    }

    private void storeInCache(Payroll_allowance_lookupDTO dto) {
        if (dto == null) return;

        mapById.put(dto.iD, dto);

        List<Payroll_allowance_lookupDTO> dtoListByOrganogramKey = mapByOrganogramKey.getOrDefault(
                dto.organogramKey, new ArrayList<>()
        );
        dtoListByOrganogramKey.add(dto);
        mapByOrganogramKey.put(dto.organogramKey, dtoListByOrganogramKey);
    }

    public List<Payroll_allowance_lookupDTO> getPayroll_allowance_lookupList() {
        return new ArrayList<>(this.mapById.values());
    }

    public Payroll_allowance_lookupDTO getDTOById(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "PALRGDBI")) {
                if (mapById.get(id) == null) {
                    storeInCache(dao.getDTOFromID(id));
                }
            }
        }
        return mapById.get(id);
    }

    public List<Payroll_allowance_lookupDTO> getDTOByOrganogramKey(int employmentCat, String organogramKey) {
        return mapByOrganogramKey.getOrDefault(organogramKey.trim(), new ArrayList<>())
                                 .stream()
                                 .filter(allowanceLookupDTO -> allowanceLookupDTO.employmentCat == employmentCat)
                                 .collect(Collectors.toList());
    }

    @Override
    public String getTableName() {
        return  dao.getTableName();
    }
}