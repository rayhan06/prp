package eksheba_common;

import java.util.ArrayList;

public class EkshebaUtil {

    public static int getBBSCode(ArrayList<String> r) {
        try {
            return !r.isEmpty() ? Integer.parseInt(r.get(0)) : 0;
        } catch (Exception ignore) {
            return 0;
        }
    }
}
