package eksheba_common;

import dbm.DBMR;
import org.apache.poi.ss.formula.functions.T;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class EkshebaCommonDAO {

    private static ArrayList<String> getBBSCodeList(String sql, String requiredColumn) {
        ResultSet res;
        Statement stmt = null;
        Connection connection = null;

        ArrayList<String> result = new ArrayList<>();

        try {
            System.out.println("EkshebaCommonDAO sql = " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();

            res = stmt.executeQuery(sql);
            while (res != null && res.next()) {
                String r = res.getString(requiredColumn);
                if(r.length() > 0)  r = r.trim();
                result.add(r);
            }

        } catch (Exception ex) {
            System.out.println(ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        return result;
    }

    public static ArrayList<String> getSingleColumnDataById(String tableName, String requiredColumn, int id, String whereCondition) {
        if (tableName == null || requiredColumn == null || tableName.length() == 0 || requiredColumn.length() == 0)
            return null;

        String sql = "SELECT " + requiredColumn + " from " + tableName + " " + ((whereCondition != null && whereCondition.length() > 0) ? whereCondition : "where id = " + id);

        return getBBSCodeList(sql, requiredColumn);
    }
}
