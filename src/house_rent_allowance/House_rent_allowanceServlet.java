package house_rent_allowance;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

import com.google.gson.Gson;
import common.BaseServlet;
import org.apache.log4j.Logger;
import login.LoginDTO;
import org.json.JSONArray;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;
import javax.servlet.http.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;


@WebServlet("/House_rent_allowanceServlet")
@MultipartConfig
public class House_rent_allowanceServlet extends BaseServlet
{
	private static final long serialVersionUID = 1L;
	public static int HOUSE_RENT_SUBCODE=3111310;
    public static Logger logger = Logger.getLogger(House_rent_allowanceServlet.class);

	@Override
	public String getTableName() {
		return House_rent_allowanceDAO.getInstance().getTableName();
	}

	@Override
	public String getServletName() {
		return "House_rent_allowanceServlet";
	}

	@Override
	public House_rent_allowanceDAO getCommonDAOService() {
		return House_rent_allowanceDAO.getInstance();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		if (userDTO == null) {
			request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			return;
		}

		try {
			String actionType = request.getParameter("actionType");
			if ("ajax_getHouseRentModel".equals(actionType)) {
				long employeeRecordId = Long.parseLong(request.getParameter("empRecId"));
				String language = request.getParameter("language");
				long startDate = Long.parseLong(request.getParameter("startDate"));
				long endDate = Long.parseLong(request.getParameter("endDate"));

				HouseRentDataModel model = House_rent_allowanceDAO.getHouseRentDataModel(employeeRecordId, startDate, endDate, language);

				String jsonInString = new Gson().toJson(model);
				PrintWriter out = response.getWriter();
				out.println(jsonInString);
				out.close();
			}else if("viewBill".equals(actionType)){
				if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.HOUSE_RENT_ALLOWANCE_SEARCH)) {
					request.getRequestDispatcher(commonPartOfDispatchURL() + "Bill.jsp").forward(request, response);
					return;
				} else {
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}else if("ajax_submitHouseRentModelData".equals(actionType)) {
				String modelData = request.getParameter("modelData");

				List<House_rent_allowanceDTO> houseRentAllowanceDTOs = House_rent_allowanceDAO.getDTOsWithAjaxData(modelData, userDTO);

				for (House_rent_allowanceDTO dto: houseRentAllowanceDTOs) {
					House_rent_allowanceDAO.getInstance().add(dto);
				}
			} else {
				super.doGet(request, response);
			}
			return;
		} catch (Exception ex) {
			logger.error(ex);
		}
		request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
	}



	@Override
	public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
		long currentTime = System.currentTimeMillis();
		House_rent_allowanceDTO houseRentAllowanceDTO;
		if (addFlag) {
			houseRentAllowanceDTO = new House_rent_allowanceDTO();
			houseRentAllowanceDTO.insertedBy = userDTO.ID;
			houseRentAllowanceDTO.insertionTime = currentTime;
		} else {
			houseRentAllowanceDTO = House_rent_allowanceDAO.getInstance().getDTOFromID(
					Long.parseLong(request.getParameter("iD"))
			);
		}
		houseRentAllowanceDTO.modifiedBy = userDTO.ID;
		houseRentAllowanceDTO.lastModificationTime = currentTime;

		houseRentAllowanceDTO.employeeRecordId = Long.parseLong(Jsoup.clean(
				request.getParameter("employeeRecordId"), Whitelist.simpleText()
		));

		houseRentAllowanceDTO.houseRentStart = new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("houseRentStart")).getTime();
		houseRentAllowanceDTO.houseRentEnd = new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("houseRentEnd")).getTime();

		houseRentAllowanceDTO.totalAmount = Double.parseDouble(Jsoup.clean(
				request.getParameter("totalAmount"), Whitelist.simpleText()));
		houseRentAllowanceDTO.revenueDeduction = Double.parseDouble(Jsoup.clean(
				request.getParameter("revenueDeduction"), Whitelist.simpleText()));
		houseRentAllowanceDTO.netAmount = Double.parseDouble(Jsoup.clean(
				request.getParameter("netAmount"), Whitelist.simpleText()));


		if (addFlag){
			House_rent_allowanceDAO.getInstance().add(houseRentAllowanceDTO);
		}
		else{
			House_rent_allowanceDAO.getInstance().update(houseRentAllowanceDTO);
		}
		return houseRentAllowanceDTO;
	}

	@Override
	public int[] getAddPageMenuConstants() {
		return new int[]{MenuConstants.HOUSE_RENT_ALLOWANCE_ADD};
	}

	@Override
	public int[] getEditPageMenuConstants() {
		return new int[]{MenuConstants.HOUSE_RENT_ALLOWANCE_UPDATE};
	}

	@Override
	public int[] getSearchMenuConstants() {
		return new int[]{MenuConstants.HOUSE_RENT_ALLOWANCE_SEARCH};
	}

	@Override
	public Class<? extends HttpServlet> getClazz() {
		return House_rent_allowanceServlet.class;
	}

}

