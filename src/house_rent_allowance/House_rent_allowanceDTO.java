package house_rent_allowance;
import java.util.*;

import sessionmanager.SessionConstants;
import util.*;


public class House_rent_allowanceDTO extends CommonDTO
{

	public long employeeRecordId = -1;
	public String employeeNameEng = "";
	public String employeeNameBng = "";
	public String employeeMobileNumber = "";
	public String employeeSavingsAccountNo = "";
	public long houseRentStart = SessionConstants.MIN_DATE;
	public long houseRentEnd = SessionConstants.MIN_DATE;
	public double totalAmount = -1;
	public double revenueDeduction = -1;
	public double netAmount = -1;
	public long modifiedBy = -1;
	public long insertedBy = -1;
	public long insertionTime = -1;
	public String voucherNumber="";
	
    @Override
	public String toString() {
            return "$House_rent_allowanceDTO[" +
            " iD = " + iD +
            " employeeRecordId = " + employeeRecordId +
            " houseRentStart = " + houseRentStart +
            " houseRentEnd = " + houseRentEnd +
            " totalAmount = " + totalAmount +
            " revenueDeduction = " + revenueDeduction +
            " netAmount = " + netAmount +
            " modifiedBy = " + modifiedBy +
            " lastModificationTime = " + lastModificationTime +
            " insertedBy = " + insertedBy +
            " insertionTime = " + insertionTime +
            " isDeleted = " + isDeleted +
            "]";
    }

}