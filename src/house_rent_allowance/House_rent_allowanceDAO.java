package house_rent_allowance;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

import allowance_configure.Allowance_configureDTO;
import allowance_configure.Allowance_configureRepository;
import bill_management.Bill_managementDAO;
import bill_management.Bill_managementDTO;
import com.lowagie.text.ExceptionConverter;
import common.CommonDAOService;
import employee_bank_information.EmployeeBankInfoModel;
import employee_bank_information.Employee_bank_informationDAO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import java.util.stream.Collectors;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.*;
import pb.*;
import user.UserDTO;

public class House_rent_allowanceDAO implements CommonDAOService<House_rent_allowanceDTO>
{
	private static final Logger logger = Logger.getLogger(House_rent_allowanceDAO.class);
	private static final String HOUSE_RENT_VOUCHER_PREFIX="HR-";
	private static final String addSqlQuery = "insert into {tableName} (employee_record_id, employee_name_eng, employee_name_bng, employee_personal_mobile_number, "
		.concat("employee_savings_account_number, house_rent_start, house_rent_end, total_amount, revenue_deduction, net_amount, ")
		.concat("modified_by, lastModificationTime, inserted_by, insertion_time, isDeleted, ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

	private static final String updateSqlQuery =
			"UPDATE {tableName} SET employee_record_id=?,employee_name_eng=?,employee_name_bng=?,employee_personal_mobile_number=?,employee_savings_account_number=?,"
					.concat("house_rent_start=?,house_rent_end=?,total_amount=?,revenue_deduction=?,net_amount=?,modified_by=?,lastModificationTime=? WHERE ID=?");

	private static final Map<String, String> searchMap = new HashMap<>();

	private House_rent_allowanceDAO() {
		searchMap.put("house_rent_start", " AND (house_rent_start >= ?)");
		searchMap.put("house_rent_end", " AND (house_rent_end <= ?)");
		searchMap.put("employee_name_eng"," AND (employee_name_eng like ?)");
		searchMap.put("employee_name_bng"," AND (employee_name_bng like ?)");
		searchMap.put("employee_personal_mobile_number"," AND (employee_personal_mobile_number = ?)");
		searchMap.put("employee_savings_account_number"," AND (employee_savings_account_number = ?)");
	}

	private static class LazyLoader {
		static final House_rent_allowanceDAO INSTANCE = new House_rent_allowanceDAO();
	}

	public static House_rent_allowanceDAO getInstance() {
		return House_rent_allowanceDAO.LazyLoader.INSTANCE;
	}

	@Override
	public void set(PreparedStatement ps, House_rent_allowanceDTO house_rent_allowanceDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		ps.setObject(++index,house_rent_allowanceDTO.employeeRecordId);
		ps.setObject(++index,house_rent_allowanceDTO.employeeNameEng);
		ps.setObject(++index,house_rent_allowanceDTO.employeeNameBng);
		ps.setObject(++index,house_rent_allowanceDTO.employeeMobileNumber);
		ps.setObject(++index,house_rent_allowanceDTO.employeeSavingsAccountNo);
		ps.setObject(++index,house_rent_allowanceDTO.houseRentStart);
		ps.setObject(++index,house_rent_allowanceDTO.houseRentEnd);
		ps.setObject(++index,house_rent_allowanceDTO.totalAmount);
		ps.setObject(++index,house_rent_allowanceDTO.revenueDeduction);
		ps.setObject(++index,house_rent_allowanceDTO.netAmount);
		ps.setObject(++index,house_rent_allowanceDTO.modifiedBy);
		ps.setObject(++index,house_rent_allowanceDTO.lastModificationTime);
		if (isInsert) {
			ps.setLong(++index, house_rent_allowanceDTO.insertedBy);
			ps.setLong(++index, house_rent_allowanceDTO.insertionTime);
			ps.setInt(++index, 0 /* isDeleted */);
		}
		ps.setLong(++index, house_rent_allowanceDTO.iD);
	}

	@Override
	public House_rent_allowanceDTO buildObjectFromResultSet(ResultSet rs) {
		try
		{
			House_rent_allowanceDTO house_rent_allowanceDTO = new House_rent_allowanceDTO();
			int i = 0;
			house_rent_allowanceDTO.iD = rs.getLong("ID");
			house_rent_allowanceDTO.voucherNumber=HOUSE_RENT_VOUCHER_PREFIX+house_rent_allowanceDTO.iD;
			house_rent_allowanceDTO.employeeRecordId = rs.getLong("employee_record_id");
			house_rent_allowanceDTO.employeeNameEng = rs.getString("employee_name_eng");
			house_rent_allowanceDTO.employeeNameBng = rs.getString("employee_name_bng");
			house_rent_allowanceDTO.employeeMobileNumber = rs.getString("employee_personal_mobile_number");
			house_rent_allowanceDTO.employeeSavingsAccountNo = rs.getString("employee_savings_account_number");
			house_rent_allowanceDTO.houseRentStart = rs.getLong("house_rent_start");
			house_rent_allowanceDTO.houseRentEnd = rs.getLong("house_rent_end");
			house_rent_allowanceDTO.totalAmount = rs.getDouble("total_amount");
			house_rent_allowanceDTO.revenueDeduction = rs.getDouble("revenue_deduction");
			house_rent_allowanceDTO.netAmount = rs.getDouble("net_amount");
			house_rent_allowanceDTO.modifiedBy = rs.getLong("modified_by");
			house_rent_allowanceDTO.lastModificationTime = rs.getLong("lastModificationTime");
			house_rent_allowanceDTO.insertedBy = rs.getLong("inserted_by");
			house_rent_allowanceDTO.insertionTime = rs.getLong("insertion_time");
			house_rent_allowanceDTO.isDeleted = rs.getInt("isDeleted");
			return house_rent_allowanceDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	@Override
	public String getTableName() {
		return "house_rent_allowance";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((House_rent_allowanceDTO) commonDTO, addSqlQuery, true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((House_rent_allowanceDTO) commonDTO, updateSqlQuery, false);
	}

	private static double getAllowanceAmount(int numberOfMonth) {
		double salary = 45000.0;
		double allowanceAmount;

		int allowanceCategoryValue = 9;   // allowance_configuration table category value
		int staticAllowance = 1;          // allowance_configuration table category value
		int percentageAllowance = 2;		// allowance_configuration table category value

		Allowance_configureDTO allowance_configureDTO = Allowance_configureRepository.getInstance().getByAllowanceCat(allowanceCategoryValue);

		if (allowance_configureDTO.amountType == staticAllowance) {
			allowanceAmount = salary + allowance_configureDTO.allowanceAmount;
		} else if (allowance_configureDTO.amountType == percentageAllowance) {
			allowanceAmount = (salary * allowance_configureDTO.allowanceAmount) / 100.0;
		} else {
			allowanceAmount = salary;
		}

		return allowanceAmount * numberOfMonth;
	}

	public static HouseRentDataModel getHouseRentDataModel(long employeeRecordId, long houseRentStart, long houseRentEnd, String language) {
		HouseRentDataModel model = new HouseRentDataModel();

		model.name = Employee_recordsRepository.getInstance().getEmployeeName(employeeRecordId, language);

		try {
			OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employeeRecordId).officeUnitOrganogramId);
			model.designation = language.equalsIgnoreCase("English") ? officeUnitOrganograms.designation_eng : officeUnitOrganograms.designation_bng;
		} catch (Exception ex) {
			System.out.println("got Exception in employee office and organogram table!");
		}

		try {
			model.mobileNumber = (Utils.getDigits(Employee_recordsRepository.getInstance().getById(employeeRecordId).personalMobile, language)).substring(2);
		} catch (Exception ex) {
			System.out.println("got Exception in while fetching mobile number!");
		}

		try {
			List<EmployeeBankInfoModel> employeeBankInfoList = Employee_bank_informationDAO.getInstance().getEmployeeBankInfoModelListByEmployeeId(employeeRecordId);
			model.savingsAccountNumber = employeeBankInfoList.stream().filter(employeeBankInfoModel -> employeeBankInfoModel.dto.accountCat == 1).map(employeeBankInfoModel -> Utils.getDigits(employeeBankInfoModel.dto.bankAccountNumber, language)).collect(Collectors.joining());
		} catch (Exception ex) {
			System.out.println("got Exception in employee Bank info table!");
		}

		Calendar houseRentStartDateCal = Calendar.getInstance(), houseRentEndDateCal = Calendar.getInstance();
		houseRentStartDateCal.setTimeInMillis(houseRentStart);
		houseRentEndDateCal.setTimeInMillis(houseRentEnd);

		int numberOfmonth = 12 * (houseRentEndDateCal.get(Calendar.YEAR) - houseRentStartDateCal.get(Calendar.YEAR)) +
				(houseRentEndDateCal.get(Calendar.MONTH) - houseRentStartDateCal.get(Calendar.MONTH)) + 1;
		model.month = Utils.getDigits(numberOfmonth, language);

		return model;
	}

	public static List<House_rent_allowanceDTO> getDTOsWithAjaxData(String ajaxData, UserDTO userDTO) {
		List<House_rent_allowanceDTO> houseRentAllowanceDTOS = new ArrayList<>();
		Employee_recordsDTO employee_recordsDTO;
		List<EmployeeBankInfoModel> employeeBankInfoList;
		House_rent_allowanceDTO dto;

		String[] splitData;
		String[] rawData = ajaxData.split(";");

		for (String str: rawData) {
			splitData = str.split(",");

			dto = new House_rent_allowanceDTO();
			dto.employeeRecordId = Long.parseLong(splitData[2]);

			try {
				employee_recordsDTO = Employee_recordsRepository.getInstance().getById(dto.employeeRecordId);
				dto.employeeNameEng = employee_recordsDTO.nameEng;
				dto.employeeNameBng = employee_recordsDTO.nameBng;
				dto.employeeMobileNumber = employee_recordsDTO.personalMobile;
			} catch (Exception ex) {
				logger.debug("Exception while fetching Employee Record: "+ex);
			}

			try {
				employeeBankInfoList = Employee_bank_informationDAO.getInstance().getEmployeeBankInfoModelListByEmployeeId(dto.employeeRecordId);
				dto.employeeSavingsAccountNo = employeeBankInfoList.stream().filter(employeeBankInfoModel -> employeeBankInfoModel.dto.accountCat == 1).map(employeeBankInfoModel -> employeeBankInfoModel.dto.bankAccountNumber).collect(Collectors.joining(", "));
			}catch (Exception ex) {
				logger.debug("Exception while fetching savings account number: " + ex);
			}

			dto.houseRentStart = Long.parseLong(splitData[0]);
			dto.houseRentEnd = Long.parseLong(splitData[1]);

			try {
				dto.revenueDeduction = Double.parseDouble(splitData[3]);
			} catch (ArrayIndexOutOfBoundsException aex) {
				dto.revenueDeduction = 0;
				logger.debug(aex);
			}

			try {
				dto.totalAmount = Double.parseDouble(splitData[4]);
			} catch (ArrayIndexOutOfBoundsException aex) {
				dto.totalAmount = 0;
				logger.debug(aex);
			}

			dto.netAmount = dto.totalAmount - dto.revenueDeduction;
			dto.insertedBy = userDTO.employee_record_id;
			dto.modifiedBy = userDTO.employee_record_id;
			dto.lastModificationTime = System.currentTimeMillis();
			dto.insertionTime = dto.lastModificationTime;

			houseRentAllowanceDTOS.add(dto);
		}

		return houseRentAllowanceDTOS;
	}
}
	