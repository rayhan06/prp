package family;

import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;
import org.eclipse.jetty.util.log.Log;

import login.LoginDTO;


import sessionmanager.SessionConstants;
import user.UserDAO;
import user.UserDTO;
import user.UserNameAndDetails;
import user.UserRepository;

import workflow.WorkflowController;

import java.util.*;
import javax.servlet.http.*;

import com.google.gson.Gson;

import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import employee_records.EmploymentStatusEnum;
import pb.*;





/**
 * Servlet implementation class FamilyServlet
 */
@WebServlet("/FamilyServlet")
@MultipartConfig
public class FamilyServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(FamilyServlet.class);

    String tableName = "family";
    String servletName = "FamilyServlet";

	FamilyMemberDAO familyMemberDAO;
    private final Gson gson = new Gson();
    UserDAO userDAO = new UserDAO();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public FamilyServlet()
	{
        super();
    	try
    	{
			familyMemberDAO = new FamilyMemberDAO("family_member");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			
			
			request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}



	


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
		
			if(actionType.equals("getPatientDetails"))
			{
				String userName = Utils.getDigitEnglishFromBangla(request.getParameter("userName"));
				System.out.println("Username found = " + userName);
				if(!Utils.isValidUserName(userName))
				{
					return;
				}
				Employee_recordsDTO erDTO = Employee_recordsRepository.getInstance().getByUserNameOrPhone(userName, "");
				if(erDTO != null)
				{
					if((erDTO.employmentStatus != EmploymentStatusEnum.ACTIVE.getValue() 
							&& erDTO.employmentStatus != EmploymentStatusEnum.LPR.getValue()) || erDTO.isDeleted)
					{
						return;
					}
				}
				UserDTO familyUserDTO = userDAO.getUserDTOByUsernameFromOnlyUsers(userName);
				if(familyUserDTO == null || !familyUserDTO.active)
				{
					return;
				}
				long familyMemberId = Long.parseLong(request.getParameter("familyMemberId"));
				FamilyMemberDTO familyMemberDTO = null;
				if(familyMemberId != FamilyDTO.OWN && familyMemberId != FamilyDTO.OTHER && familyMemberId != FamilyDTO.UNSELECTED)
				{
					System.out.println("Family member chosen");
					familyMemberDTO = familyMemberDAO.getEFEDTOById(familyMemberId);
				}
				else if(familyMemberId == FamilyDTO.OWN)
				{
					System.out.println("Self chosen: " + userName);
					UserDTO employeeDTO = userDAO.getUserDTOByUsernameFromOnlyUsers(userName);
					if(employeeDTO!= null)
					{
						System.out.println("UserDTO not null");
						familyMemberDTO = WorkflowController.getFamilyMemberDTOFromEmployeeRecordId(employeeDTO.employee_record_id);
					}
					else
					{
						System.out.println("UserDTO null");
					}
				}

				if(familyMemberDTO != null)
				{
					SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					familyMemberDTO.dateOfBirthFormatted = f.format(new Date(familyMemberDTO.dateOfBirth));
					System.out.println("drawingDocumentsDetailsDTO.fcm = " + familyMemberDTO.nameEn);
					response.setContentType( "application/json" );
					response.setStatus( HttpServletResponse.SC_OK );
					PrintWriter printWriter = response.getWriter();
					printWriter.print(gson.toJson(familyMemberDTO));
					printWriter.flush();
					printWriter.close();
				}
			}
			else if(actionType.equals("getFamily"))
			{
				String userName = Utils.getDigitEnglishFromBangla(request.getParameter("userName"));
				logger.debug("getFamily called for userName = " + userName);
				if(!Utils.isValidUserName(userName))
				{
					return;
				}
				Employee_recordsDTO erDTO = Employee_recordsRepository.getInstance().getByUserNameOrPhone(userName, "");
				if(erDTO != null)
				{
					if((erDTO.employmentStatus != EmploymentStatusEnum.ACTIVE.getValue() 
							&& erDTO.employmentStatus != EmploymentStatusEnum.LPR.getValue()) || erDTO.isDeleted)
					{
						return;
					}
				}
				if(userName == null || userName.equalsIgnoreCase(""))
				{
					userName = FamilyDTO.defaultUser;
				}
				
				UserDTO familyUserDTO = userDAO.getUserDTOByUsernameFromOnlyUsers(userName);
				if(familyUserDTO == null || !familyUserDTO.active)
				{
					return;
				}
				logger.debug("familyUserDTO.active = " + familyUserDTO.active + " for userName = " + userName);
				

				String language = request.getParameter("language");
				if(language == null)
				{
					language = "English";
				}
				System.out.println("language = " + language);
				String options = "";

				int defaultOption = FamilyDTO.UNSELECTED;
				if(request.getParameter("defaultOption") != null)
				{
					defaultOption = Integer.parseInt(request.getParameter("defaultOption"));
				}

				if(defaultOption == FamilyDTO.UNSELECTED)
				{
					if(language.equalsIgnoreCase("english"))
					{
						options += "<option value = '' " +  getSelected(FamilyDTO.UNSELECTED, defaultOption) + ">" + FamilyDTO.catEn[-FamilyDTO.UNSELECTED] + "</option>";
					}
					else
					{
						options += "<option value = '' " +  getSelected(FamilyDTO.UNSELECTED, defaultOption) + ">" + FamilyDTO.catBn[-FamilyDTO.UNSELECTED] + "</option>";
					}
				}

				if(!userName.equals(FamilyDTO.defaultUser))
				{

					if(language.equalsIgnoreCase("english"))
					{
						options += "<option value = '" + FamilyDTO.OWN + "' " +  getSelected(FamilyDTO.OWN, defaultOption) + ">" + FamilyDTO.catEn[-FamilyDTO.OWN] + "</option>";
						options += "<option value = '" + FamilyDTO.OTHER + "' " +  getSelected(FamilyDTO.OTHER, defaultOption) + ">" + FamilyDTO.catEn[-FamilyDTO.OTHER] + "</option>";
					}
					else
					{
						options += "<option value = '" + FamilyDTO.OWN + "' " +  getSelected(FamilyDTO.OWN, defaultOption) + ">" + FamilyDTO.catBn[-FamilyDTO.OWN] + "</option>";
						options += "<option value = '" + FamilyDTO.OTHER + "' " +  getSelected(FamilyDTO.OTHER, defaultOption) + ">" + FamilyDTO.catBn[-FamilyDTO.OTHER] + "</option>";
					}

					familyUserDTO = userDAO.getUserDTOByUsernameFromOnlyUsers(userName);

					List<FamilyMemberDTO> FamilyMembersDTOs = familyMemberDAO.getFamilyMemberDTOsFromEFIByErId(familyUserDTO.employee_record_id);

					if(FamilyMembersDTOs != null)
					{
						for(FamilyMemberDTO familyMemberDTO : FamilyMembersDTOs)
						{
							options += "<option value = '" + familyMemberDTO.iD + "' " +  getSelected((int)familyMemberDTO.iD, defaultOption) + ">";
							if(language.equalsIgnoreCase("english"))
							{
								options += familyMemberDTO.nameEn;
								options += " (" + CommonDAO.getName("English", "relation", familyMemberDTO.relationshipCat) + ")";
							}
							else
							{
								options += familyMemberDTO.nameBn;
								options += " (" + CommonDAO.getName("Bangla", "relation", familyMemberDTO.relationshipCat) + ")";
							}
	
							options += "</option>";
						}
					}

					response.getWriter().write(options);

				}
				else
				{
					if(language.equalsIgnoreCase("english"))
					{
						options += "<option value = '" + FamilyDTO.EMERGENCY + "'>"+ FamilyDTO.catEn[-FamilyDTO.EMERGENCY] + "</option>";
						options += "<option value = '" + FamilyDTO.VISITING_PERSON + "'>" + FamilyDTO.catEn[-FamilyDTO.VISITING_PERSON] + "</option>";
					}
					else
					{
						options += "<option value = '" + FamilyDTO.EMERGENCY + "'>"+ FamilyDTO.catBn[-FamilyDTO.EMERGENCY] + "</option>";
						options += "<option value = '" + FamilyDTO.VISITING_PERSON + "'>" + FamilyDTO.catBn[-FamilyDTO.VISITING_PERSON] + "</option>";
					}
					response.getWriter().write(options);
				}


			}

			else if(actionType.equals("getEmployeeInfo"))
			{
				String userName = "";
				if(request.getParameter("userName") != null)
				{
					userName = Utils.getDigitEnglishFromBangla(request.getParameter("userName"));
				}
				else
				{
					String phone = Utils.getDigitEnglishFromBangla(request.getParameter("phone"));
					userName = Employee_recordsRepository.getInstance().getUserNameByPhone(phone);
				}
				
				System.out.println("username found = " + userName);
				if(!Utils.isValidUserName(userName))
				{
					return;
				}
				Employee_recordsDTO erDTO = Employee_recordsRepository.getInstance().getByUserNameOrPhone(userName, "");
				if(erDTO != null)
				{
					if((erDTO.employmentStatus != EmploymentStatusEnum.ACTIVE.getValue() 
							&& erDTO.employmentStatus != EmploymentStatusEnum.LPR.getValue()) || erDTO.isDeleted)
					{
						return;
					}
				}
				
				UserDTO familyUserDTO = userDAO.getUserDTOByUsernameFromOnlyUsers(userName);
				if(familyUserDTO == null || !familyUserDTO.active)
				{
					return;
				}
				logger.debug("getEmployeeInfo familyUserDTO.active = " + familyUserDTO.active + " for userName = " + userName);
				
				System.out.println("getEmployeeInfo, userName = " + userName);
				UserDTO employeeDTO = userDAO.getUserDTOByUsernameFromOnlyUsers(userName);
				if(employeeDTO!= null)
				{
					String language = request.getParameter("language");
					if(language == null)
					{
						language = "English";
					}
					System.out.println("language = " + language);
					UserNameAndDetails userNameAndDetails = new UserNameAndDetails();
					userNameAndDetails.userName = userName;
					userNameAndDetails.userName = userName;
					userNameAndDetails.name = Employee_recordsRepository.getInstance().getByUserName(userName, language);
					userNameAndDetails.phone = Employee_recordsRepository.getInstance().getPhoneByUserName(userName);

					employeeDTO = UserRepository.getUserDTOByUserName(userName);
					if(employeeDTO != null)
					{
						userNameAndDetails.designation = WorkflowController.getOrganogramName(employeeDTO.organogramID, language);
						userNameAndDetails.deptName = WorkflowController.getUnitNameFromOrganogramId(employeeDTO.organogramID, language);
					}
					
					
				

					String responseJson = gson.toJson( userNameAndDetails );
					response.setContentType( "application/json" );
					response.getWriter().write( responseJson );
					response.getWriter().close();
				}
				else
				{
					System.out.println("Got null userDTO");
				}

			}

		
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private String getSelected(int value, int defaultOption)
	{
		if(value == defaultOption)
		{
			return "selected";
		}
		else
		{
			return "";
		}
	}



}

