package family;
import java.util.*; 
import util.*; 


public class FamilyDTO extends CommonDTO
{

	public long organogramId = 0;
	public long userId = 0;
	public String userName = "";
	
	public static final String defaultUser = "superadmin";
	
	public static final int OWN = -1;
	public static final int UNSELECTED = -2;
	public static final int OTHER = -3;
	public static final int EMERGENCY = -4;
	public static final int VISITING_PERSON = -5;
	
	public static final String[] catEn = {"", "Own", "Select", "Other", "Emergency", "Visiting Person"};
	public static final String[] catBn = {"", "নিজে", "বাছাই করুন", "অন্যান্য", "জরুরী", "পরিদর্শনরত ব্যক্তি"};
	
	
	public List<FamilyMemberDTO> familyMemberDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$FamilyDTO[" +
            " iD = " + iD +
            " organogramId = " + organogramId +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}