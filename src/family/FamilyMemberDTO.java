package family;
import java.util.*;

import pb.CatDTO;
import util.*; 


public class FamilyMemberDTO extends CommonDTO
{

	public long familyId = 0;
    public String nameEn = "";
    public String nameBn = "";
	public int relationshipCat = 0;
	public long dateOfBirth = 0;
	public String dateOfBirthFormatted = "";
	public int bloodGroupCat = 0;
	public int genderCat = CatDTO.CATDEFAULT;
	public int religionCat = 0;
    public String adress = "";
	public long drawingFileId = -1;
    public String nid = "";
    public String mobile = "";
    public String birthRegistrationNumber = "";
	
	public List<FamilyMemberDTO> familyMemberDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$FamilyMemberDTO[" +
            " iD = " + iD +
            " familyId = " + familyId +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " relationshipCat = " + relationshipCat +
            " dateOfBirth = " + dateOfBirth +
            " bloodGroupCat = " + bloodGroupCat +
            " genderCat = " + genderCat +
            " religionCat = " + religionCat +
            " adress = " + adress +
            " drawingFileId = " + drawingFileId +
            " nid = " + nid +
            " birthRegistrationNumber = " + birthRegistrationNumber +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}