package family;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import org.apache.log4j.Logger;
import common.ConnectionAndStatementUtil;
import util.*;


public class FamilyMemberDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public FamilyMemberDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";

	}
	
	public FamilyMemberDAO()
	{
		this("family_member");		
	}

	public FamilyMemberDTO getFromEFI(ResultSet rs)
	{
		try {
			FamilyMemberDTO familymemberDTO = new FamilyMemberDTO();
			familymemberDTO.iD = rs.getLong("ID");
			familymemberDTO.familyId = -1;
			familymemberDTO.nameEn = rs.getString("first_name_en");
			if(rs.getString("last_name_en") != null)
			{
				familymemberDTO.nameEn += " " + rs.getString("last_name_en");
			}
			familymemberDTO.nameBn = rs.getString("first_name_bn");
			if(rs.getString("last_name_bn") != null)
			{
				familymemberDTO.nameBn += " " + rs.getString("last_name_bn");
			}
			familymemberDTO.relationshipCat = rs.getInt("relation_type");
			familymemberDTO.dateOfBirth = rs.getLong("dob");
			familymemberDTO.bloodGroupCat = rs.getInt("blood_group_cat");
			familymemberDTO.genderCat = rs.getInt("gender_cat");
			familymemberDTO.religionCat = rs.getInt("religion_type");
			familymemberDTO.adress = rs.getString("present_address");
			familymemberDTO.drawingFileId = rs.getLong("files_dropzone");
			familymemberDTO.nid = rs.getString("national_id_no");
			familymemberDTO.birthRegistrationNumber = rs.getString("UBRN");
			familymemberDTO.mobile = rs.getString("mobile_number_1");
			familymemberDTO.isDeleted = rs.getInt("isDeleted");
			familymemberDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return familymemberDTO;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public List<FamilyMemberDTO> getFamilyMemberDTOsFromEFIByErId(long employeeId)
	{
		String sql = "SELECT * FROM employee_family_info where isDeleted=0 and employee_records_id=" + employeeId + 
				" order by lastModificationTime";
		return ConnectionAndStatementUtil.getListOfT(sql,this::getFromEFI);
	}
	
	public FamilyMemberDTO getEFEDTOById(long id)
	{
		String sql = "SELECT * ";

		sql += " FROM employee_family_info ";
		
        sql += " WHERE id =" + id;
        return ConnectionAndStatementUtil.getT(sql,this::getFromEFI);
	}
	
	public FamilyMemberDTO getEFEDTOByErIdAndRelationShip(long erId, int whoIsThePatientCat)
	{
		String sql = "SELECT * ";

		sql += " FROM employee_family_info ";
		
        sql += " WHERE employee_records_id =" + erId
        		+ " and relation_type = " + whoIsThePatientCat;
        return ConnectionAndStatementUtil.getT(sql,this::getFromEFI);
	}

}
	