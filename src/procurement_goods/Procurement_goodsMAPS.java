package procurement_goods;
import java.util.*; 
import util.*;


public class Procurement_goodsMAPS extends CommonMaps
{	
	public Procurement_goodsMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("nameEn".toLowerCase(), "nameEn".toLowerCase());
		java_DTO_map.put("nameBn".toLowerCase(), "nameBn".toLowerCase());
		java_DTO_map.put("procurementGoodsTypeId".toLowerCase(), "procurementGoodsTypeId".toLowerCase());
		java_DTO_map.put("unitPrice".toLowerCase(), "unitPrice".toLowerCase());
		java_DTO_map.put("estCost".toLowerCase(), "estCost".toLowerCase());
		java_DTO_map.put("methodAndTypeCat".toLowerCase(), "methodAndTypeCat".toLowerCase());
		java_DTO_map.put("approvingAuthority".toLowerCase(), "approvingAuthority".toLowerCase());
		java_DTO_map.put("sourceOfFundCat".toLowerCase(), "sourceOfFundCat".toLowerCase());
		java_DTO_map.put("timeCode".toLowerCase(), "timeCode".toLowerCase());
		java_DTO_map.put("tender".toLowerCase(), "tender".toLowerCase());
		java_DTO_map.put("tenderOpening".toLowerCase(), "tenderOpening".toLowerCase());
		java_DTO_map.put("tenderEvaluation".toLowerCase(), "tenderEvaluation".toLowerCase());
		java_DTO_map.put("approvalToward".toLowerCase(), "approvalToward".toLowerCase());
		java_DTO_map.put("awardNotification".toLowerCase(), "awardNotification".toLowerCase());
		java_DTO_map.put("siginingOfContract".toLowerCase(), "siginingOfContract".toLowerCase());
		java_DTO_map.put("contractSignatureTime".toLowerCase(), "contractSignatureTime".toLowerCase());
		java_DTO_map.put("contractCompletionTime".toLowerCase(), "contractCompletionTime".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());

		java_SQL_map.put("name_en".toLowerCase(), "nameEn".toLowerCase());
		java_SQL_map.put("name_bn".toLowerCase(), "nameBn".toLowerCase());
		java_SQL_map.put("procurement_goods_type_id".toLowerCase(), "procurementGoodsTypeId".toLowerCase());
		java_SQL_map.put("unit_price".toLowerCase(), "unitPrice".toLowerCase());
		java_SQL_map.put("est_cost".toLowerCase(), "estCost".toLowerCase());
		java_SQL_map.put("method_and_type_cat".toLowerCase(), "methodAndTypeCat".toLowerCase());
		java_SQL_map.put("approving_authority".toLowerCase(), "approvingAuthority".toLowerCase());
		java_SQL_map.put("source_of_fund_cat".toLowerCase(), "sourceOfFundCat".toLowerCase());
		java_SQL_map.put("time_code".toLowerCase(), "timeCode".toLowerCase());
		java_SQL_map.put("tender".toLowerCase(), "tender".toLowerCase());
		java_SQL_map.put("tender_opening".toLowerCase(), "tenderOpening".toLowerCase());
		java_SQL_map.put("tender_evaluation".toLowerCase(), "tenderEvaluation".toLowerCase());
		java_SQL_map.put("approval_toward".toLowerCase(), "approvalToward".toLowerCase());
		java_SQL_map.put("award_notification".toLowerCase(), "awardNotification".toLowerCase());
		java_SQL_map.put("sigining_of_contract".toLowerCase(), "siginingOfContract".toLowerCase());
		java_SQL_map.put("contract_signature_time".toLowerCase(), "contractSignatureTime".toLowerCase());
		java_SQL_map.put("contract_completion_time".toLowerCase(), "contractCompletionTime".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("English Name".toLowerCase(), "nameEn".toLowerCase());
		java_Text_map.put("Bangla Name".toLowerCase(), "nameBn".toLowerCase());
		java_Text_map.put("Procurement Goods Type Id".toLowerCase(), "procurementGoodsTypeId".toLowerCase());
		java_Text_map.put("Unit Price".toLowerCase(), "unitPrice".toLowerCase());
		java_Text_map.put("Est Cost".toLowerCase(), "estCost".toLowerCase());
		java_Text_map.put("Method And Type".toLowerCase(), "methodAndTypeCat".toLowerCase());
		java_Text_map.put("Approving Authority".toLowerCase(), "approvingAuthority".toLowerCase());
		java_Text_map.put("Source Of Fund".toLowerCase(), "sourceOfFundCat".toLowerCase());
		java_Text_map.put("Time Code".toLowerCase(), "timeCode".toLowerCase());
		java_Text_map.put("Tender".toLowerCase(), "tender".toLowerCase());
		java_Text_map.put("Tender Opening".toLowerCase(), "tenderOpening".toLowerCase());
		java_Text_map.put("Tender Evaluation".toLowerCase(), "tenderEvaluation".toLowerCase());
		java_Text_map.put("Approval Toward".toLowerCase(), "approvalToward".toLowerCase());
		java_Text_map.put("Award Notification".toLowerCase(), "awardNotification".toLowerCase());
		java_Text_map.put("Sigining Of Contract".toLowerCase(), "siginingOfContract".toLowerCase());
		java_Text_map.put("Contract Signature Time".toLowerCase(), "contractSignatureTime".toLowerCase());
		java_Text_map.put("Contract Completion Time".toLowerCase(), "contractCompletionTime".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
			
	}

}