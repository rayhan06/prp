package procurement_goods;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pb.CatDAO;
import pb.PBNameRepository;
import procurement_package.ProcurementGoodsTypeDTO;
import procurement_package.ProcurementGoodsTypeRepository;
import procurement_package.Procurement_packageRepository;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;
import util.StringUtils;
import util.UtilCharacter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class Procurement_goodsDAO  extends NavigationService4
{
	public static final int GETYEARLYSEARCH = 4;
	public static final int GETYEARLYSEARCHCOUNT = 5;

	Logger logger = Logger.getLogger(getClass());
	

	
	public Procurement_goodsDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Procurement_goodsMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"procurement_year",
			"report_name",
			"procurement_package_type",
			"procurement_goods_type_type",
			"quantity",
			"parent_id",
			"pi_unit_id",
			"unit_price",
			"est_cost",
			"method_and_type_cat",
			"returnable",
			"is_actual_item",
			"parent_ids_comma_separated",
			"approving_authority",
			"source_of_fund_cat",
			"time_code",
			"tender",
			"description",
			"tender_opening",
			"tender_evaluation",
			"approval_toward",
			"award_notification",
			"sigining_of_contract",
			"contract_signature_time",
			"contract_completion_time",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"search_column",
			"assigned_office_unit_ids",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Procurement_goodsDAO()
	{
		this("procurement_goods");		
	}


	private static class LazyLoader
	{
		static final Procurement_goodsDAO INSTANCE = new Procurement_goodsDAO();
	}

	public static Procurement_goodsDAO getInstance()
	{
		return Procurement_goodsDAO.LazyLoader.INSTANCE;
	}


	public void setSearchColumn(Procurement_goodsDTO procurement_goodsDTO)
	{
		procurement_goodsDTO.searchColumn = "";
		procurement_goodsDTO.searchColumn += procurement_goodsDTO.nameEn + " ";
		procurement_goodsDTO.searchColumn += procurement_goodsDTO.nameBn + " ";
		procurement_goodsDTO.searchColumn += procurement_goodsDTO.procurementYear + " ";
		procurement_goodsDTO.searchColumn += procurement_goodsDTO.reportName + " ";
		procurement_goodsDTO.searchColumn += CatDAO.getName("English", "method_and_type", procurement_goodsDTO.methodAndTypeCat) + " " + CatDAO.getName("Bangla", "method_and_type", procurement_goodsDTO.methodAndTypeCat) + " ";
		procurement_goodsDTO.searchColumn += procurement_goodsDTO.quantity + " ";
		procurement_goodsDTO.searchColumn += procurement_goodsDTO.parentId + " ";
		procurement_goodsDTO.searchColumn += procurement_goodsDTO.piUnitId + " ";
		procurement_goodsDTO.searchColumn += procurement_goodsDTO.unitPrice + " ";
		procurement_goodsDTO.searchColumn += procurement_goodsDTO.approvingAuthority + " ";
		procurement_goodsDTO.searchColumn += CatDAO.getName("English", "source_of_fund", procurement_goodsDTO.sourceOfFundCat) + " " + CatDAO.getName("Bangla", "source_of_fund", procurement_goodsDTO.sourceOfFundCat) + " ";
		procurement_goodsDTO.searchColumn += procurement_goodsDTO.tender + " ";
		procurement_goodsDTO.searchColumn += procurement_goodsDTO.description + " ";
		procurement_goodsDTO.searchColumn += procurement_goodsDTO.tenderOpening + " ";
		procurement_goodsDTO.searchColumn += procurement_goodsDTO.tenderEvaluation + " ";
		procurement_goodsDTO.searchColumn += procurement_goodsDTO.approvalToward + " ";
		procurement_goodsDTO.searchColumn += procurement_goodsDTO.awardNotification + " ";
		procurement_goodsDTO.searchColumn += procurement_goodsDTO.siginingOfContract + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Procurement_goodsDTO procurement_goodsDTO = (Procurement_goodsDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(procurement_goodsDTO);
		if(isInsert)
		{
			ps.setObject(index++,procurement_goodsDTO.iD);
		}
		ps.setObject(index++,procurement_goodsDTO.nameEn);
		ps.setObject(index++,procurement_goodsDTO.nameBn);
		ps.setObject(index++,procurement_goodsDTO.procurementYear);
		ps.setObject(index++,procurement_goodsDTO.reportName);
		ps.setObject(index++,procurement_goodsDTO.procurementPackageId);
		ps.setObject(index++,procurement_goodsDTO.procurementGoodsTypeId);
		ps.setObject(index++,procurement_goodsDTO.quantity );
		ps.setObject(index++,procurement_goodsDTO.parentId );
		ps.setObject(index++,procurement_goodsDTO.piUnitId );
		ps.setObject(index++,procurement_goodsDTO.unitPrice);
		ps.setObject(index++,procurement_goodsDTO.estCost);
		ps.setObject(index++,procurement_goodsDTO.methodAndTypeCat);
		ps.setObject(index++,procurement_goodsDTO.returnable);
		ps.setObject(index++,procurement_goodsDTO.isActualItem);
		ps.setObject(index++,procurement_goodsDTO.parentIdsCommaSeparated);
		ps.setObject(index++,procurement_goodsDTO.approvingAuthority);
		ps.setObject(index++,procurement_goodsDTO.sourceOfFundCat);
		ps.setObject(index++,procurement_goodsDTO.timeCode);
		ps.setObject(index++,procurement_goodsDTO.tender);
		ps.setObject(index++,procurement_goodsDTO.description);
		ps.setObject(index++,procurement_goodsDTO.tenderOpening);
		ps.setObject(index++,procurement_goodsDTO.tenderEvaluation);
		ps.setObject(index++,procurement_goodsDTO.approvalToward);
		ps.setObject(index++,procurement_goodsDTO.awardNotification);
		ps.setObject(index++,procurement_goodsDTO.siginingOfContract);
		ps.setObject(index++,procurement_goodsDTO.contractSignatureTime);
		ps.setObject(index++,procurement_goodsDTO.contractCompletionTime);
		ps.setObject(index++,procurement_goodsDTO.insertedByUserId);
		ps.setObject(index++,procurement_goodsDTO.insertedByOrganogramId);
		ps.setObject(index++,procurement_goodsDTO.insertionDate);
		ps.setObject(index++,procurement_goodsDTO.searchColumn);
		ps.setObject(index++,procurement_goodsDTO.assignedOfcUnitIds);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Procurement_goodsDTO procurement_goodsDTO, ResultSet rs) throws SQLException
	{
		procurement_goodsDTO.iD = rs.getLong("ID");
		procurement_goodsDTO.nameEn = rs.getString("name_en");
		procurement_goodsDTO.nameBn = rs.getString("name_bn");
		procurement_goodsDTO.procurementYear = rs.getString("procurement_year");
		procurement_goodsDTO.reportName = rs.getString("report_name");
		procurement_goodsDTO.procurementPackageId = rs.getLong("procurement_package_type");
		procurement_goodsDTO.procurementGoodsTypeId = rs.getLong("procurement_goods_type_type");
		procurement_goodsDTO.quantity = rs.getLong("quantity");
		procurement_goodsDTO.parentId = rs.getLong("parent_id");
		procurement_goodsDTO.piUnitId = rs.getLong("pi_unit_id");
		procurement_goodsDTO.unitPrice = rs.getDouble("unit_price");
		procurement_goodsDTO.estCost = rs.getDouble("est_cost");
		procurement_goodsDTO.methodAndTypeCat = rs.getInt("method_and_type_cat");
		procurement_goodsDTO.returnable = rs.getInt("returnable");
		procurement_goodsDTO.isActualItem = rs.getInt("is_actual_item");
		procurement_goodsDTO.parentIdsCommaSeparated = rs.getString("parent_ids_comma_separated");
		procurement_goodsDTO.approvingAuthority = rs.getString("approving_authority");
		procurement_goodsDTO.sourceOfFundCat = rs.getInt("source_of_fund_cat");
		procurement_goodsDTO.timeCode = rs.getString("time_code");
		procurement_goodsDTO.tender = rs.getString("tender");
		procurement_goodsDTO.description = rs.getString("description");
		procurement_goodsDTO.tenderOpening = rs.getString("tender_opening");
		procurement_goodsDTO.tenderEvaluation = rs.getString("tender_evaluation");
		procurement_goodsDTO.approvalToward = rs.getString("approval_toward");
		procurement_goodsDTO.awardNotification = rs.getString("award_notification");
		procurement_goodsDTO.siginingOfContract = rs.getString("sigining_of_contract");
		procurement_goodsDTO.contractSignatureTime = rs.getLong("contract_signature_time");
		procurement_goodsDTO.contractCompletionTime = rs.getLong("contract_completion_time");
		procurement_goodsDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		procurement_goodsDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
		procurement_goodsDTO.insertionDate = rs.getLong("insertion_date");
		procurement_goodsDTO.isDeleted = rs.getInt("isDeleted");
		procurement_goodsDTO.lastModificationTime = rs.getLong("lastModificationTime");
		procurement_goodsDTO.searchColumn = rs.getString("search_column");
		procurement_goodsDTO.assignedOfcUnitIds = rs.getString("assigned_office_unit_ids");
	}



	public Procurement_goodsDTO build(ResultSet rs)
	{
		try
		{
			Procurement_goodsDTO procurement_goodsDTO = new Procurement_goodsDTO();
			procurement_goodsDTO.iD = rs.getLong("ID");
			procurement_goodsDTO.nameEn = rs.getString("name_en");
			procurement_goodsDTO.nameBn = rs.getString("name_bn");
			procurement_goodsDTO.procurementYear = rs.getString("procurement_year");
			procurement_goodsDTO.reportName = rs.getString("report_name");
			procurement_goodsDTO.procurementPackageId = rs.getLong("procurement_package_type");
			procurement_goodsDTO.procurementGoodsTypeId = rs.getLong("procurement_goods_type_type");
			procurement_goodsDTO.quantity = rs.getLong("quantity");
			procurement_goodsDTO.parentId = rs.getLong("parent_id");
			procurement_goodsDTO.piUnitId = rs.getLong("pi_unit_id");
			procurement_goodsDTO.unitPrice = rs.getDouble("unit_price");
			procurement_goodsDTO.estCost = rs.getDouble("est_cost");
			procurement_goodsDTO.methodAndTypeCat = rs.getInt("method_and_type_cat");
			procurement_goodsDTO.returnable = rs.getInt("returnable");
			procurement_goodsDTO.isActualItem = rs.getInt("is_actual_item");
			procurement_goodsDTO.parentIdsCommaSeparated = rs.getString("parent_ids_comma_separated");
			procurement_goodsDTO.approvingAuthority = rs.getString("approving_authority");
			procurement_goodsDTO.sourceOfFundCat = rs.getInt("source_of_fund_cat");
			procurement_goodsDTO.timeCode = rs.getString("time_code");
			procurement_goodsDTO.tender = rs.getString("tender");
			procurement_goodsDTO.description = rs.getString("description");
			procurement_goodsDTO.tenderOpening = rs.getString("tender_opening");
			procurement_goodsDTO.tenderEvaluation = rs.getString("tender_evaluation");
			procurement_goodsDTO.approvalToward = rs.getString("approval_toward");
			procurement_goodsDTO.awardNotification = rs.getString("award_notification");
			procurement_goodsDTO.siginingOfContract = rs.getString("sigining_of_contract");
			procurement_goodsDTO.contractSignatureTime = rs.getLong("contract_signature_time");
			procurement_goodsDTO.contractCompletionTime = rs.getLong("contract_completion_time");
			procurement_goodsDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			procurement_goodsDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			procurement_goodsDTO.insertionDate = rs.getLong("insertion_date");
			procurement_goodsDTO.isDeleted = rs.getInt("isDeleted");
			procurement_goodsDTO.lastModificationTime = rs.getLong("lastModificationTime");
			procurement_goodsDTO.searchColumn = rs.getString("search_column");
			procurement_goodsDTO.assignedOfcUnitIds = rs.getString("assigned_office_unit_ids");
			return procurement_goodsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	public Procurement_goodsDTO buildReportName(ResultSet rs)
	{
		try
		{
			Procurement_goodsDTO procurement_goodsDTO = new Procurement_goodsDTO();
			procurement_goodsDTO.reportName = rs.getString("report_name");
			return procurement_goodsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	public int buildCountId(ResultSet rs)
	{
		try
		{
			return rs.getInt("countID");
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return 0;
		}
	}

	//need another getter for repository
	public Procurement_goodsDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		Procurement_goodsDTO procurement_goodsDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
		return procurement_goodsDTO;
	}

	public List<Procurement_goodsDTO> getDTOByParentId (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE parent_id = " + ID + " ";
		List<Procurement_goodsDTO> procurement_goodsDTOs =
				ConnectionAndStatementUtil.getDTOList(sql, Arrays.asList(ID), this::build);
		return procurement_goodsDTOs;
	}


	public List<Procurement_goodsDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	}


	public void deleteAll(String idsForDelete) throws Exception
	{
		long lastModificationTime = System.currentTimeMillis();
		StringBuilder sqlBuilder = new StringBuilder("UPDATE ")
				.append(tableName)
				.append(" SET isDeleted=1,lastModificationTime=")
				.append(lastModificationTime)
				.append(" WHERE ID in ")
				.append(idsForDelete);
		ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
			String sql = sqlBuilder.toString();
			Connection connection = model.getConnection();
			Statement stmt = model.getStatement();
			try {
				logger.debug(sql);
				stmt.execute(sql);
				recordUpdateTime(connection, lastModificationTime);
			} catch (SQLException ex) {
				logger.error(ex);
			}
		});
	}

	public void deleteItems(List<Long> idsToDelete, HttpServletRequest request, HttpServletResponse response) throws IOException {
		StringBuilder sql = new StringBuilder("UPDATE ")
				.append(tableName)
				.append(" Set isDeleted = 1,")
				.append(" lastModificationTime = ")
				.append(System.currentTimeMillis())
				.append(" WHERE ID in ( ");
		for(Long id : idsToDelete){
			sql.append(id + ",");
		}

		if(idsToDelete.size() != 0){
			int size = sql.toString().length();
			sql.replace(size-1, size, ")");
		}

		ConnectionAndStatementUtil.getWriteStatement(st -> {
			try {
				logger.debug(sql);
				st.execute(sql.toString());
			} catch (SQLException ex) {
				logger.error(ex);
			}
		});

		PBNameRepository.getInstance().reload(false);
		response.sendRedirect("Procurement_goodsServlet?actionType=search");
	}
	
	
	
	//add repository
	public List<Procurement_goodsDTO> getAllProcurement_goods (boolean isFirstReload)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}

	
	public List<Procurement_goodsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	public List<Procurement_goodsDTO> getDTOsForYearlySearch(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOsForYearlySearch(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}
	
	public List<Procurement_goodsDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		List<Object> objectList = new ArrayList<Object>();
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat, objectList);
		return ConnectionAndStatementUtil.getListOfT(sql,objectList,this::build);
	}

	public List<Procurement_goodsDTO> getDTOsForYearlySearch(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
											  String filter, boolean tableHasJobCat)
	{
		List<Procurement_goodsDTO> procurement_goodsDTOList = new ArrayList<>();

		try{
			List<Object> objectList = new ArrayList<Object>();

			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETYEARLYSEARCH, isPermanentTable, userDTO, filter, tableHasJobCat, objectList);

			printSql(sql);

			return ConnectionAndStatementUtil.getListOfT(sql,objectList,this::buildReportName);

		}catch(Exception ex){
			ex.printStackTrace();
		}
		return procurement_goodsDTOList;

	}

	public int getCount(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
						String filter, boolean tableHasJobCat) {
		int count = 0;

		try {
			List<Object> objectList = new ArrayList<Object>();

			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETCOUNT, isPermanentTable, userDTO,
					filter, tableHasJobCat, objectList);

			//printSql(sql);

			return ConnectionAndStatementUtil.getT(sql,objectList,this::buildCountId);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return count;

	}



	public int getCountYearlySearch(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
						String filter, boolean tableHasJobCat) {
		int count = 0;

		try {
			List<Object> objectList = new ArrayList<Object>();

			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETYEARLYSEARCHCOUNT, isPermanentTable, userDTO,
					filter, tableHasJobCat, objectList);

			//printSql(sql);

			return ConnectionAndStatementUtil.getT(sql,objectList,this::buildCountId);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return count;

	}









	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		boolean viewAll = false;

		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		else if(category == GETYEARLYSEARCH)
		{
			sql += " distinct " + tableName + ".report_name as report_name ";
		}
		else if(category == GETYEARLYSEARCHCOUNT)
		{
			sql += " count( " + tableName + ".report_name) as countID ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";

		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			boolean gotPrcYEar = false;

			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ?";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name_en")
						|| str.equals("name_bn")
						|| str.equals("package_name")
						|| str.equals("package_id")
						|| str.equals("type_name")
						|| str.equals("type_id")
						|| str.equals("procurement_year")
						|| str.equals("report_name")
						|| str.equals("method_and_type_cat")
						|| str.equals("returnable")
						|| str.equals("is_actual_item")
						|| str.equals("approving_authority")
						|| str.equals("source_of_fund_cat")
						|| str.equals("tender")
						|| str.equals("description")
						|| str.equals("tender_opening")
						|| str.equals("tender_evaluation")
						|| str.equals("approval_toward")
						|| str.equals("award_notification")
						|| str.equals("sigining_of_contract")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("name_bn"))
					{
						AllFieldSql += "" + tableName + ".name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					 else if(str.equals("package_id"))
					 {
						 if (p_searchCriteria.get(str) != null) {
							 String ids = String.valueOf(p_searchCriteria.get(str));
							 if (StringUtils.isValidString(ids)) {
								 AllFieldSql += "" + tableName + ".procurement_package_type in ( " + ids +" ) ";
								 i ++;
							 }
						 }
					 }

					 else if(str.equals("package_name"))
					 {
						 if (p_searchCriteria.get(str) != null) {
							 String name = String.valueOf(p_searchCriteria.get(str));
							 if (StringUtils.isValidString(name)) {
								 String ids = "(" + String.join(",", Procurement_packageRepository.getInstance().getDTOsLikeName(name)
										 .stream()
										 .map(dto -> String.valueOf(dto.iD))
										 .collect(Collectors.toList())) + ")";
								 if (StringUtils.isValidString(ids)) {
									 AllFieldSql += "" + tableName + ".procurement_package_type in " + ids;
//									 objectList.add("(" + ids + ")");
								 }
								 else AllFieldSql += " false ";
								 i ++;
							 }
						 }
					 }

					 else if(str.equals("type_id"))
					 {
						 if (p_searchCriteria.get(str) != null) {
							 String ids = String.valueOf(p_searchCriteria.get(str));
							 if (StringUtils.isValidString(ids)) {
								 AllFieldSql += "" + tableName + ".procurement_goods_type_type in ( " + ids +" ) ";
								 i ++;
							 }
						 }
					 }

					 else if(str.equals("type_name"))
					 {
						 if (p_searchCriteria.get(str) != null) {
							 String name = String.valueOf(p_searchCriteria.get(str));
							 if (StringUtils.isValidString(name)) {
								 String ids = "(" + String.join(",", ProcurementGoodsTypeRepository.getInstance().getDTOsLikeName(name)
										 .stream()
										 .map(dto -> String.valueOf(dto.iD))
										 .collect(Collectors.toList())) + ")";
								 if (StringUtils.isValidString(ids)) {
									 AllFieldSql += "" + tableName + ".procurement_goods_type_type in " + ids;
//									 objectList.add("(" + ids + ")");
								 }
								 else AllFieldSql += " false ";
								 i ++;
							 }
						 }
					 }

					else if(str.equals("procurement_year"))
					{
						gotPrcYEar = true;
						AllFieldSql += "" + tableName + ".procurement_year like '" + p_searchCriteria.get(str) + "'";
						i ++;
					}
					else if(str.equals("report_name"))
					{
						AllFieldSql += "" + tableName + ".report_name like '" + p_searchCriteria.get(str) + "'";
						i ++;
					}
					else if(str.equals("method_and_type_cat"))
					{
						AllFieldSql += "" + tableName + ".method_and_type_cat = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("returnable"))
					{
						AllFieldSql += "" + tableName + ".returnable = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("is_actual_item"))
					{
						AllFieldSql += "" + tableName + ".is_actual_item = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("approving_authority"))
					{
						AllFieldSql += "" + tableName + ".approving_authority like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("source_of_fund_cat"))
					{
						AllFieldSql += "" + tableName + ".source_of_fund_cat = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("tender"))
					{
						AllFieldSql += "" + tableName + ".tender like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("description"))
					{
						AllFieldSql += "" + tableName + ".description like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("tender_opening"))
					{
						AllFieldSql += "" + tableName + ".tender_opening like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("tender_evaluation"))
					{
						AllFieldSql += "" + tableName + ".tender_evaluation like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approval_toward"))
					{
						AllFieldSql += "" + tableName + ".approval_toward like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("award_notification"))
					{
						AllFieldSql += "" + tableName + ".award_notification like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("sigining_of_contract"))
					{
						AllFieldSql += "" + tableName + ".sigining_of_contract like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 AND " + tableName + ".is_actual_item = 1 ";
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}

//		if(p_searchCriteria.get("procurement_year_ajax") == null)
//		{
//
//			Date today = new Date();
//			SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
//			String todayString = sf.format(today);
//			int currentYear = Integer.parseInt(todayString.substring(6));
//			int previousYear = currentYear-1;
//			int nextYear = currentYear+1;
//
//			String firstYear = "";
//			String secondYear = "";
//
//			int month = Integer.parseInt(todayString.substring(3,5));
//			firstYear = month > 6 ? String.valueOf(currentYear) : String.valueOf(previousYear);
//			secondYear = month > 6 ? String.valueOf(nextYear).substring(2) : String.valueOf(currentYear).substring(2);
//
//			String reportYear = firstYear + "-" + secondYear;
//
//
//			sql += " and procurement_year = '" + reportYear + "' ";
//		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc";
//				", " + tableName + ".procurement_goods_type_type ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }

	public List<Procurement_goodsDTO> getDTOsBySql(String sql) {
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}

	public String getCircularData(long goodId, String language){
		List<String> data = new ArrayList<>();
		Procurement_goodsDTO dto = Procurement_goodsRepository.getInstance().getProcurement_goodsDTOByID(goodId);
		if(dto != null){
			data.add(UtilCharacter.getDataByLanguage(language, dto.nameBn, dto.nameEn));
			while (dto != null && dto.parentId != -1) {
				dto = Procurement_goodsRepository.getInstance().getProcurement_goodsDTOByID(dto.parentId);
				if (dto != null) {
					data.add(UtilCharacter.getDataByLanguage(language, dto.nameBn, dto.nameEn));
				}
			}
			if(dto != null){
				ProcurementGoodsTypeDTO goodsTypeDTO = ProcurementGoodsTypeRepository.getInstance().
						getProcurementGoodsTypeDTOByID(dto.procurementGoodsTypeId);
				if(goodsTypeDTO != null){
					data.add(UtilCharacter.getDataByLanguage(language, goodsTypeDTO.nameBn, goodsTypeDTO.nameEn));
				}
			}
		}

		if (data.isEmpty()) {
			return "";
		}
		Collections.reverse(data);
		return String.join(", ", data);
	}

	public Long buildLongFromResultSet(ResultSet rs){
		try {
			return rs.getLong("item_id");
		} catch (SQLException ex) {
			logger.error(ex);
			return null;
		}
	}

	public List<Long> getAllowedIdsToDelete(List<Long> requestedIdsToDelete){
		StringBuilder getIdsUsedInAnnualDemand = new StringBuilder("SELECT DISTINCT item_id ")
				.append(" FROM pi_annual_demand_child ").append(" WHERE isDeleted = 0 ORDER BY item_id ");
		List<Long> idsUsedInAnnualDemand = ConnectionAndStatementUtil.getListOfT(getIdsUsedInAnnualDemand.toString(),
				this::buildLongFromResultSet);
		idsUsedInAnnualDemand.stream().map(requestedIdsToDelete::remove);
		return requestedIdsToDelete;
	}
}
	