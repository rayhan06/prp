package procurement_goods;

import pi_unit.Pi_unitRepository;
import procurement_package.ProcurementGoodsTypeDTO;
import procurement_package.ProcurementGoodsTypeRepository;
import procurement_package.Procurement_packageDTO;
import procurement_package.Procurement_packageRepository;

import java.util.List;
import java.util.stream.Collectors;

public class ProcurementItemGroupOrTypeModel {

    public String piPackageItemMapId = "";
    public String piPackageItemMapChildId = "";
    public String parentId = "";
    public String itemId = "";
    public String procurementPackageId = "";
    public String procurementGoodsTypeId = "";
    public String itemName = "";
    public String subTypes = "";
    public String unit = "";
    public Long unitId = -1L;
    public String description = "";

    public String itemGroupName = "";
    public String itemTypeName = "";



    public ProcurementItemGroupOrTypeModel(List<Procurement_goodsDTO> procurementGoodsDTOSubTypes, Procurement_goodsDTO dto,
                                           String language){
        boolean isLanguageEnglish = "English".equalsIgnoreCase(language);
        itemId = String.valueOf(dto.iD);
        parentId = String.valueOf(dto.parentId);
        procurementPackageId = String.valueOf(dto.procurementPackageId);
        Procurement_packageDTO procurement_packageDTO = Procurement_packageRepository.getInstance().getProcurement_packageDTOByID(dto.procurementPackageId);
        if(procurement_packageDTO != null){
            itemGroupName = isLanguageEnglish ? procurement_packageDTO.nameEn:procurement_packageDTO.nameBn;
        }
        ProcurementGoodsTypeDTO procurementGoodsTypeDTO = ProcurementGoodsTypeRepository.getInstance().getProcurementGoodsTypeDTOByID(dto.procurementGoodsTypeId);
        if(procurementGoodsTypeDTO != null){
            itemTypeName = isLanguageEnglish ? procurementGoodsTypeDTO.nameEn:procurementGoodsTypeDTO.nameBn;
        }
        procurementGoodsTypeId = String.valueOf(dto.procurementGoodsTypeId);
        itemName = isLanguageEnglish ? dto.nameEn:dto.nameBn;
        unit = isLanguageEnglish? Pi_unitRepository.getInstance().getPi_unitDTOByiD(dto.piUnitId).nameEn:
                Pi_unitRepository.getInstance().getPi_unitDTOByiD(dto.piUnitId).nameBn;
        unitId =  dto.piUnitId;
        description = dto.description;
        piPackageItemMapId = String.valueOf(dto.piPackageItemMapId);
        piPackageItemMapChildId = String.valueOf(dto.piPackageItemMapChildId);
        subTypes = procurementGoodsDTOSubTypes.stream().map(e -> isLanguageEnglish?e.nameEn:e.nameBn).collect(Collectors.joining(" -> "));
    }

    @Override
    public String toString() {
        return "PiProductModel{" +
                "goodsId='" + itemId + '\'' +
                "procurementPackageId='" + procurementPackageId + '\'' +
                ", procurementGoodsTypeId='" + procurementGoodsTypeId + '\'' +
                ", itemName='" + itemName + '\'' +
                '}';
    }
}
