package procurement_goods;

import job_applicant_application.Job_applicant_applicationDTO;

public class ProcurementGoodsModel {

    public Long packageId = -1L;
    public Long procurementGoodsTypeId = -1L;
    public Long procurementGoodsParentId = -1L;




    public ProcurementGoodsModel(){ }
    public ProcurementGoodsModel(Procurement_goodsDTO dto, String language){
        boolean isLanguageEnglish = "English".equalsIgnoreCase(language);
        packageId = dto.procurementPackageId;
        procurementGoodsTypeId = dto.procurementGoodsTypeId;
        procurementGoodsParentId = dto.parentId;

    }




    @Override
    public String toString() {
        return "JobApplicantApplicationModel{" +
                ", packageId='" + packageId + '\'' +
                ", procurementGoodsTypeId='" + procurementGoodsTypeId + '\'' +
                '}';
    }
}
