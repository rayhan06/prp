package procurement_goods;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.CommonDAO;
import pb.OptionDTO;
import pb.PBNameRepository;
import pb.Utils;
import permission.MenuConstants;
import pi_app_request_details.Pi_app_request_detailsDAO;
import pi_app_request_details.Pi_app_request_detailsDTO;
import pi_package_item_map.PiPackageItemMapChildDTO;
import pi_package_item_map.PiPackageItemMapChildRepository;
import pi_unit.Pi_unitDTO;
import pi_unit.Pi_unitRepository;
import procurement_package.ProcurementGoodsTypeDTO;
import procurement_package.ProcurementGoodsTypeRepository;
import procurement_package.Procurement_packageDTO;
import procurement_package.Procurement_packageRepository;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;




/**
 * Servlet implementation class Procurement_goodsServlet
 */
@WebServlet("/Procurement_goodsServlet")
@MultipartConfig
public class Procurement_goodsServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Procurement_goodsServlet.class);

    String tableName = "procurement_goods";

	Procurement_goodsDAO procurement_goodsDAO;
	CommonRequestHandler commonRequestHandler;
	private final HashSet<String> concatedData = new HashSet<>();
	private final Gson gson = new Gson();
	private final ObjectMapper mapper = new ObjectMapper();


	/**
     * @see HttpServlet#HttpServlet()
     */
    public Procurement_goodsServlet()
	{
        super();
    	try
    	{
			procurement_goodsDAO = Procurement_goodsDAO.getInstance();
			commonRequestHandler = new CommonRequestHandler(procurement_goodsDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_GOODS_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
					return;
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_GOODS_UPDATE))
				{
					getProcurement_goods(request, response);
					return;
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_GOODS_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
//							String reportName = request.getParameter("report_name_ajax");
//							String procurementYear = request.getParameter("procurement_year_ajax");
//							reportName  =reportName == null ? "" : " report_name like '" + reportName + "' ";
//							procurementYear  =procurementYear == null ? "" : " procurement_year = '" + procurementYear + "' ";
//
//							String and = !(reportName.isEmpty() || procurementYear.isEmpty()) ? " AND " : "";
//							filter = reportName + and + procurementYear;
							searchProcurement_goods(request, response, isPermanentTable, "");
						}
						else
						{
							searchProcurement_goods(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchProcurement_goods(request, response, tempTableName, isPermanentTable);
					}
					return;
				}
			}
			else if(actionType.equals("yearlySearch"))
			{
				System.out.println("yearlySearch requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_GOODS_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							String reportName = request.getParameter("report_name_ajax");
							String procurementYear = request.getParameter("procurement_year_ajax");
							reportName  =reportName == null ? "" : reportName;
							procurementYear  =procurementYear == null ? "" : procurementYear;
							if (!reportName.isEmpty()) {
								reportName = " report_name like '%" + reportName + "%' AND ";
							}
							procurementYear = " procurement_year = '" + procurementYear + "' ";
							filter = reportName + procurementYear;
							yearlySearchProcurement_goods(request, response, isPermanentTable, filter);
						}
						else
						{
							yearlySearchProcurement_goods(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchProcurement_goods(request, response, tempTableName, isPermanentTable);
					}
					return;
				}
			}
			else if(actionType.equals("getReport"))
			{
				System.out.println("report requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_GOODS_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							String reportName = request.getParameter("report_name_ajax");
							String procurementYear = request.getParameter("procurement_year_ajax");
							reportName  =reportName == null ? "" : reportName;
							procurementYear  =procurementYear == null ? "" : procurementYear;
//							if (!reportName.isEmpty()) {
//								reportName = " report_name like '" + reportName + "' AND ";
//							}
							reportName = " report_name like '" + reportName + "' AND ";
							procurementYear = " procurement_year = '" + procurementYear + "' ";
							filter = reportName + procurementYear;
							report(request, response, isPermanentTable, filter);
						}
						else
						{
							report(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchProcurement_goods(request, response, tempTableName, isPermanentTable);
					}
					return;
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_GOODS_SEARCH))
				{
					commonRequestHandler.view(request, response);
					return;
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("getByGoodsType"))
			{
				getByGoodsType(request, response);
				return;
			}
			else if(actionType.equals("getByParent"))
			{
				getByParent(request, response);
				return;
			}
			else if(actionType.equals("getGoodsDtoResponseById"))
			{
				Long procurementGoodsId = Long.parseLong(request.getParameter("procurementGoodsId"));
				ProcurementGoodsModel model = null;
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				Procurement_goodsDTO procurement_goodsDTO = Procurement_goodsRepository.getInstance().
						getProcurement_goodsDTOByID(procurementGoodsId);

				if(procurement_goodsDTO!=null ){
					model = new ProcurementGoodsModel(procurement_goodsDTO,HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language);
				}

				response.getWriter().println(gson.toJson(model));
				return;
			}
			if(actionType.equals("itemGroupOrType"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_GOODS_ADD))
				{
					request.getRequestDispatcher("procurement_goods/procurement_goodsItemGroupOrTypeEdit.jsp").forward(request, response);
					return;
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			if(actionType.equals("getItemWiseRow"))
			{
				// permision fix
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_GOODS_ADD) || true)
				{
					List<Procurement_goodsDTO> procurement_goodsDTOS = new ArrayList<>();
					List<Procurement_goodsDTO> procurementGoodsItemDTOs = new ArrayList<>();

					HashMap<Long, List<Procurement_goodsDTO> > itemToSubTypesMap = new HashMap<>();

					List<ProcurementItemGroupOrTypeModel> modelList = null;

					long procurementPackageId = Long.parseLong(request.getParameter("procurementPackageId"));
					long procurementGoodsTypeId = Long.parseLong(request.getParameter("procurementGoodsTypeId"));
					response.setContentType("application/json");
					response.setCharacterEncoding("UTF-8");
					procurement_goodsDTOS = Procurement_goodsRepository.getInstance().
							getProcurement_goodsDTOBypackage_id(procurementPackageId);

					procurement_goodsDTOS = procurement_goodsDTOS.stream().filter(e -> e.procurementGoodsTypeId == procurementGoodsTypeId).collect(Collectors.toList());

					if(!procurement_goodsDTOS.isEmpty()){
						List<Long> parentIds = procurement_goodsDTOS.stream().filter(f -> f.parentId != -1).map(e ->e.parentId).collect(Collectors.toList());
						procurementGoodsItemDTOs = procurement_goodsDTOS.stream().filter(e -> !parentIds.contains(e.iD)).collect(Collectors.toList());

						populateItemToSubTypeMap(itemToSubTypesMap,procurementGoodsItemDTOs);

						modelList = procurementGoodsItemDTOs
								.stream()
								.map(dto -> new ProcurementItemGroupOrTypeModel(itemToSubTypesMap.get(dto.iD),
										dto, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language))
								.collect(Collectors.toList());

					}

					response.getWriter().println(gson.toJson(modelList));
					return;
				}

			}

			if(actionType.equals("getItemWiseRowEdit"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PI_APP_REQUEST_ADD))
				{
					response.setContentType("application/json");
					response.setCharacterEncoding("UTF-8");

					List<Procurement_goodsDTO> procurement_goodsDTOS = new ArrayList<>();
					List<Procurement_goodsDTO> procurementGoodsItemDTOs = new ArrayList<>();
					List<PiPackageItemMapChildDTO> piPackageItemMapChildDTOS = new ArrayList<>();

					HashMap<Long, List<Procurement_goodsDTO> > itemToSubTypesMap = new HashMap<>();

					List<ProcurementItemGroupOrTypeModel> modelList = null;

					long piPackageItemMapId = Long.parseLong(request.getParameter("piPackageItemMapId"));

					String modalActionType = request.getParameter("modalActionType");
					if(modalActionType != null &&!modalActionType.isEmpty() &&
							modalActionType.trim().equalsIgnoreCase("dataByPiPackageNewId")){
						long piPackageNewId = Long.parseLong(request.getParameter("piPackageNewId"));
						piPackageItemMapChildDTOS = PiPackageItemMapChildRepository.getInstance().
								getPiPackageItemMapChildDTOBypiPackageNewId(piPackageNewId);
					}
					else{
						piPackageItemMapChildDTOS = PiPackageItemMapChildRepository.getInstance().
								getPiPackageItemMapChildDTOBypiPackageItemMapId(piPackageItemMapId);
					}

					for(PiPackageItemMapChildDTO piPackageItemMapChildDTO : piPackageItemMapChildDTOS){
						Procurement_goodsDTO procurement_goodsDTOS1 = Procurement_goodsRepository.getInstance().
								getProcurement_goodsDTOByID(piPackageItemMapChildDTO.itemGroupId);
						if(procurement_goodsDTOS1 != null){
							procurement_goodsDTOS.add(procurement_goodsDTOS1);
						}

						Procurement_goodsDTO procurementGoodsItemDTOs1 = Procurement_goodsRepository.getInstance().
								getProcurement_goodsDTOByID(piPackageItemMapChildDTO.itemId);
						if(procurementGoodsItemDTOs1 != null){
							procurementGoodsItemDTOs1.piPackageItemMapId = piPackageItemMapChildDTO.piPackageItemMapId;
							procurementGoodsItemDTOs1.piPackageItemMapChildId = piPackageItemMapChildDTO.iD;
							procurementGoodsItemDTOs.add(procurementGoodsItemDTOs1);
						}
					}

					//if(!procurement_goodsDTOS.isEmpty() && !procurementGoodsItemDTOs.isEmpty()){
					if( !procurementGoodsItemDTOs.isEmpty()){

						populateItemToSubTypeMap(itemToSubTypesMap,procurementGoodsItemDTOs);

						modelList = procurementGoodsItemDTOs
								.stream()
								.map(dto -> new ProcurementItemGroupOrTypeModel(itemToSubTypesMap.get(dto.iD),
										dto, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language))
								.collect(Collectors.toList());

					}

					response.getWriter().println(gson.toJson(modelList));
					return;
				}

			}

			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void populateItemToSubTypeMap(HashMap<Long, List<Procurement_goodsDTO>> itemToSubTypesMap, List<Procurement_goodsDTO> procurementGoodsItemDTOs) {
		for(Procurement_goodsDTO procurement_goodsDTO : procurementGoodsItemDTOs){
			List<Procurement_goodsDTO> allSubTypes = new ArrayList<>();
			long parentId = procurement_goodsDTO.parentId;
			while (parentId != -1) {
				Procurement_goodsDTO parent = procurement_goodsDAO.getDTOByID(parentId);
				allSubTypes.add(parent);
				parentId = parent.parentId;
			}
			Collections.reverse(allSubTypes);
			itemToSubTypesMap.put(procurement_goodsDTO.iD,allSubTypes);
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_GOODS_ADD))
				{
					System.out.println("going to  addProcurement_goods ");
					addProcurement_goods(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addProcurement_goods ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

//			else if(actionType.equals("addAll"))
//			{
//
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_GOODS_ADD))
//				{
//					System.out.println("going to  addProcurement_goods all");
//					addProcurement_goodsAll(request, response, true, userDTO, true);
//				}
//				else
//				{
//					System.out.println("Not going to  addProcurement_goods all");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//
//			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_GOODS_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addProcurement_goods ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_GOODS_UPDATE))
				{
					addProcurement_goods(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				List<Pi_app_request_detailsDTO> pi_app_request_detailsDTOS;
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_GOODS_UPDATE))
				{
					List<Long> IDsRequestedToDelete = Arrays.stream(request.getParameterValues("ID"))
							.map(Long::parseLong).collect(Collectors.toList());

					List<Long> FilteredIdsToDelete = Procurement_goodsDAO.getInstance()
							.getAllowedIdsToDelete(IDsRequestedToDelete);

					procurement_goodsDAO.deleteItems(FilteredIdsToDelete, request, response);
					reloadNecessaryRepository();
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_GOODS_SEARCH))
				{
					String filter = request.getParameter("filter");
					System.out.println("filter = " + filter);
					if(filter!=null)
					{
						filter = ""; //shouldn't be directly used, rather manipulate it.
						String reportName = request.getParameter("report_name_ajax");
						String procurementYear = request.getParameter("procurement_year_ajax");
						reportName  =reportName == null ? "" : " report_name like '" + reportName + "' ";
						procurementYear  =procurementYear == null ? "" : " procurement_year = '" + procurementYear + "' ";
//						if (!reportName.isEmpty()) {
//							reportName = " report_name like '" + reportName + "' AND ";
//						}
						String and = !(reportName.isEmpty() || procurementYear.isEmpty()) ? " AND " : "";
						filter = reportName + and + procurementYear;
						searchProcurement_goods(request, response, true, filter);
					}
					else
					{
						searchProcurement_goods(request, response, true, "");
					}
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("yearlySearch"))
			{
				System.out.println("yearlySearch requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_GOODS_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							String reportName = request.getParameter("report_name_ajax");
							String procurementYear = request.getParameter("procurement_year_ajax");
							reportName  =reportName == null ? "" : reportName;
							procurementYear  =procurementYear == null ? "" : procurementYear;
							if (!reportName.isEmpty()) {
								reportName = " report_name like '%" + reportName + "%' AND ";
							}
							procurementYear = " procurement_year = '" + procurementYear + "' ";
							filter = reportName + procurementYear;
							yearlySearchProcurement_goods(request, response, isPermanentTable, filter);
						}
						else
						{
							yearlySearchProcurement_goods(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchProcurement_goods(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("getReport"))
			{
				System.out.println("report requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_GOODS_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							String reportName = request.getParameter("report_name_ajax");
							String procurementYear = request.getParameter("procurement_year_ajax");
							reportName  =reportName == null ? "" : reportName;
							procurementYear  =procurementYear == null ? "" : procurementYear;
//							if (!reportName.isEmpty()) {
//								reportName = " report_name like '" + reportName + "' AND ";
//							}
							reportName = " report_name like '" + reportName + "' AND ";
							procurementYear = " procurement_year = '" + procurementYear + "' ";
							filter = reportName + procurementYear;
							report(request, response, isPermanentTable, filter);
							reportPost(request, response, isPermanentTable, filter);
						}
						else
						{
							reportPost(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchProcurement_goods(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Procurement_goodsDTO procurement_goodsDTO = Procurement_goodsRepository.getInstance().getProcurement_goodsDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(procurement_goodsDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

//	private void addProcurement_goodsBackup(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
//	{
//		// TODO Auto-generated method stub
//		try
//		{
//			request.setAttribute("failureMessage", "");
//			System.out.println("%%%% addProcurement_goods");
//			String path = getServletContext().getRealPath("/img2/");
//			Procurement_goodsDTO procurement_goodsDTO;
//			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
//			String existingNameEn = "";
//			String existingNameBn = "";
//
//			if(addFlag == true)
//			{
//				procurement_goodsDTO = new Procurement_goodsDTO();
//			}
//			else
//			{
//				procurement_goodsDTO = (Procurement_goodsDTO)Procurement_goodsRepository.getInstance().getProcurement_goodsDTOByID(Long.parseLong(request.getParameter("iD")));
//				existingNameEn = procurement_goodsDTO.nameEn;
//				existingNameBn = procurement_goodsDTO.nameBn;
//			}
//
//			String Value = "";
//
//			Value = request.getParameter("nameEn");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("nameEn = " + Value);
//			if(Value != null)
//			{
//				procurement_goodsDTO.nameEn = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("nameBn");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("nameBn = " + Value);
//			if(Value != null)
//			{
//				procurement_goodsDTO.nameBn = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("reportName");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("reportName = " + Value);
//			if(Value != null)
//			{
//				procurement_goodsDTO.reportName = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("procurementYear");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("procurementYear = " + Value);
//			if(Value != null)
//			{
//				procurement_goodsDTO.procurementYear = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("procurementPackageId");
//
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("procurementPackageId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				procurement_goodsDTO.procurementPackageId = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("piUnitId");
//
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("piUnitId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				procurement_goodsDTO.piUnitId = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("description");
//
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("description = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				procurement_goodsDTO.description = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("procurementGoodsTypeId");
//
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("procurementGoodsTypeId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				procurement_goodsDTO.procurementGoodsTypeId = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("unitPrice");
//
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("unitPrice = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				procurement_goodsDTO.unitPrice = Double.parseDouble(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("estCost");
//
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("estCost = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				procurement_goodsDTO.estCost = Double.parseDouble(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("methodAndTypeCat");
//
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("methodAndTypeCat = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				procurement_goodsDTO.methodAndTypeCat = Integer.parseInt(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("quantity");
//
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("quantity = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				procurement_goodsDTO.quantity = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("approvingAuthority");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("approvingAuthority = " + Value);
//			if(Value != null)
//			{
//				procurement_goodsDTO.approvingAuthority = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("sourceOfFundCat");
//
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("sourceOfFundCat = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				procurement_goodsDTO.sourceOfFundCat = Integer.parseInt(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("timeCode");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("timeCode = " + Value);
//			if(Value != null)
//			{
//				procurement_goodsDTO.timeCode = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("tender");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("tender = " + Value);
//			if(Value != null)
//			{
//				procurement_goodsDTO.tender = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("tenderOpening");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("tenderOpening = " + Value);
//			if(Value != null)
//			{
//				procurement_goodsDTO.tenderOpening = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("tenderEvaluation");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("tenderEvaluation = " + Value);
//			if(Value != null)
//			{
//				procurement_goodsDTO.tenderEvaluation = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("approvalToward");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("approvalToward = " + Value);
//			if(Value != null)
//			{
//				procurement_goodsDTO.approvalToward = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("awardNotification");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("awardNotification = " + Value);
//			if(Value != null)
//			{
//				procurement_goodsDTO.awardNotification = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("siginingOfContract");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("siginingOfContract = " + Value);
//			if(Value != null)
//			{
//				procurement_goodsDTO.siginingOfContract = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("contractSignatureTime");
//
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("contractSignatureTime = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				procurement_goodsDTO.contractSignatureTime = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("contractCompletionTime");
//
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("contractCompletionTime = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				procurement_goodsDTO.contractCompletionTime = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			if(addFlag)
//			{
//				procurement_goodsDTO.insertedByUserId = userDTO.ID;
//			}
//
//
//			if(addFlag)
//			{
//				procurement_goodsDTO.insertedByOrganogramId = userDTO.organogramID;
//			}
//
//
//			if(addFlag)
//			{
//				Calendar c = Calendar.getInstance();
//				c.set(Calendar.HOUR_OF_DAY, 0);
//				c.set(Calendar.MINUTE, 0);
//				c.set(Calendar.SECOND, 0);
//				c.set(Calendar.MILLISECOND, 0);
//
//				procurement_goodsDTO.insertionDate = c.getTimeInMillis();
//			}
//
//
//			Value = request.getParameter("searchColumn");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("searchColumn = " + Value);
//			if(Value != null)
//			{
//				procurement_goodsDTO.searchColumn = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			boolean valid = validateProcurement_goodsDTOByRequest(request);
//
//			if (valid) {
//
//				// bn en validations starts
//
//				int numberOfHierarchyForValidation = request.getParameterValues("subTypeEn").length;
//				for (int i=0 ;i< numberOfHierarchyForValidation; i++) {
//					Value = request.getParameterValues("subTypeEn")[i];
//					if(Value != null)
//					{
//						Value = Jsoup.clean(Value,Whitelist.simpleText());
//					}
//					if(Value != null && !Value.trim().isEmpty() )
//					{
//						if(!Utils.notBangla(Value)){
//							PrintWriter out = response.getWriter();
//							out.println(new Gson().toJson("Not English"));
//							out.close();
//							return;
//						}
//					}
//
//					Value = request.getParameterValues("subTypeBn")[i];
//					if(Value != null)
//					{
//						Value = Jsoup.clean(Value,Whitelist.simpleText());
//					}
//
//					if(Value != null && !Value.trim().isEmpty() )
//					{
//						if(!Utils.notEnglish(Value)){
//							PrintWriter out = response.getWriter();
//							out.println(new Gson().toJson("Not Bangla"));
//							out.close();
//							return;
//						}
//					}
//
//				}
//
//
//				// bn en validation ends
//
//
//				System.out.println("Done adding  addProcurement_goods dto = " + procurement_goodsDTO);
//				long returnedID = -1;
//
//				if(isPermanentTable == false) //add new row for validation and make the old row outdated
//				{
//					procurement_goodsDAO.setIsDeleted(procurement_goodsDTO.iD, CommonDTO.OUTDATED);
//					returnedID = procurement_goodsDAO.add(procurement_goodsDTO);
//					procurement_goodsDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
//				}
//				else if(addFlag == true)
//				{
//					int numberOfHierarchy = request.getParameterValues("subTypeEn").length;
//
//					for (int i=0 ;i< numberOfHierarchy; i++) {
//
//						procurement_goodsDTO.parentId = returnedID;
//
//						Value = request.getParameterValues("subTypeEn")[i];
//
//						if(Value != null)
//						{
//							Value = Jsoup.clean(Value,Whitelist.simpleText());
//						}
//						System.out.println("subTypeEn = " + Value);
//						if(Value != null && !Value.trim().isEmpty() )
//						{
//							procurement_goodsDTO.nameEn = (Value);
//						}
//						else
//						{
////							valid = false;
//							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//						}
//
//						Value = request.getParameterValues("subTypeBn")[i];
//
//						if(Value != null)
//						{
//							Value = Jsoup.clean(Value,Whitelist.simpleText());
//						}
//						System.out.println("subTypeBn = " + Value);
//						if(Value != null && !Value.trim().isEmpty() )
//						{
//							procurement_goodsDTO.nameBn = (Value);
//						}
//						else
//						{
////							valid = false;
//							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//						}
//
//
//						if (i == numberOfHierarchy - 1)  {
//
//							Value = request.getParameterValues("returnable")[0];
//
//							if(Value != null)
//							{
//								Value = Jsoup.clean(Value,Whitelist.simpleText());
//							}
//							System.out.println("returnable = " + Value);
//							if(Value != null)
//							{
//								procurement_goodsDTO.returnable = Integer.parseInt(Value);
//							}
//							else
//							{
////							valid = false;
//								System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//							}
//
//
//							procurement_goodsDTO.isActualItem = 1;
//						}
//
//
//						returnedID = procurement_goodsDAO.manageWriteOperations(procurement_goodsDTO, SessionConstants.INSERT, -1, userDTO);
//
//					}
//
//					PrintWriter out = response.getWriter();
//					out.println(gson.toJson("Success"));
//					out.close();
//
//				}
//				else
//				{
//					boolean childExists = !procurement_goodsDAO.getDTOByParentId(procurement_goodsDTO.iD).isEmpty();
//
//					String description = procurement_goodsDTO.description;
//					long piUnitId = procurement_goodsDTO.piUnitId;
//					long parentId = procurement_goodsDTO.iD;
//					long goodsTypeId = procurement_goodsDTO.procurementGoodsTypeId;
//					long insertionDate = new Date().getTime();
//					long procurementPackageId = procurement_goodsDTO.procurementPackageId;
//
//					procurement_goodsDTO = procurement_goodsDAO.getDTOByID(procurement_goodsDTO.iD);
//
//					procurement_goodsDTO.description = description;
//					procurement_goodsDTO.piUnitId = piUnitId;
//
//					int numberOfHierarchy = request.getParameterValues("subTypeEn").length;
//
//					for (int i=0 ;i< 1; i++) {
//
//						Value = request.getParameterValues("subTypeEn")[i];
//
//						if(Value != null)
//						{
//							Value = Jsoup.clean(Value,Whitelist.simpleText());
//						}
//						System.out.println("subTypeEn = " + Value);
//						if(Value != null && !Value.trim().isEmpty() )
//						{
//							procurement_goodsDTO.nameEn = (Value);
//						}
//						else
//						{
////							valid = false;
//							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//						}
//
//						Value = request.getParameterValues("subTypeBn")[i];
//
//						if(Value != null)
//						{
//							Value = Jsoup.clean(Value,Whitelist.simpleText());
//						}
//						System.out.println("subTypeBn = " + Value);
//						if(Value != null && !Value.trim().isEmpty() )
//						{
//							procurement_goodsDTO.nameBn = (Value);
//						}
//						else
//						{
//							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//						}
//
//
//
//						if (i == numberOfHierarchy - 1)  {
//
//							Value = request.getParameterValues("returnable")[0];
//
//							if(Value != null)
//							{
//								Value = Jsoup.clean(Value,Whitelist.simpleText());
//							}
//							System.out.println("returnable = " + Value);
//							if(Value != null)
//							{
//								procurement_goodsDTO.returnable = Integer.parseInt(Value);
//							}
//							else
//							{
////							valid = false;
//								System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//							}
//
//
//							procurement_goodsDTO.isActualItem = 1;
//						}
//
//
//						if (!(childExists
//								&&
//								!(existingNameBn.equals(procurement_goodsDTO.nameBn)
//										&& existingNameEn.equals(procurement_goodsDTO.nameEn)))) {
//							returnedID = procurement_goodsDAO.manageWriteOperations(procurement_goodsDTO, SessionConstants.UPDATE, -1, userDTO);
//						}
//
//					}
//
//					for (int i=1 ;i< numberOfHierarchy; i++) {
//						procurement_goodsDTO = new Procurement_goodsDTO();
//
//						procurement_goodsDTO.description = description;
//						procurement_goodsDTO.piUnitId = piUnitId;
//						procurement_goodsDTO.parentId = parentId;
//						procurement_goodsDTO.procurementGoodsTypeId = goodsTypeId;
//						procurement_goodsDTO.insertionDate = insertionDate;
//						procurement_goodsDTO.procurementPackageId = procurementPackageId;
//
//						Value = request.getParameterValues("subTypeEn")[i];
//
//						if(Value != null)
//						{
//							Value = Jsoup.clean(Value,Whitelist.simpleText());
//						}
//						System.out.println("subTypeEn = " + Value);
//						if(Value != null && !Value.trim().isEmpty() )
//						{
//							procurement_goodsDTO.nameEn = (Value);
//						}
//						else
//						{
////							valid = false;
//							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//						}
//
//						Value = request.getParameterValues("subTypeBn")[i];
//
//						if(Value != null)
//						{
//							Value = Jsoup.clean(Value,Whitelist.simpleText());
//						}
//						System.out.println("subTypeBn = " + Value);
//						if(Value != null && !Value.trim().isEmpty() )
//						{
//							procurement_goodsDTO.nameBn = (Value);
//						}
//						else
//						{
//							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//						}
//
//
//						if (i == numberOfHierarchy - 1) {
//
//							Value = request.getParameterValues("returnable")[i];
//
//							if(Value != null)
//							{
//								Value = Jsoup.clean(Value,Whitelist.simpleText());
//							}
//							System.out.println("returnable = " + Value);
//							if(Value != null)
//							{
//								procurement_goodsDTO.returnable = Integer.parseInt(Value);
//							}
//							else
//							{
////							valid = false;
//								System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//							}
//
//							procurement_goodsDTO.isActualItem = 1;
//
//						}
//
//
//						returnedID = procurement_goodsDAO.manageWriteOperations(procurement_goodsDTO, SessionConstants.INSERT, -1, userDTO);
//
//					}
//
//					Procurement_goodsRepository.getInstance().reload(false);
//
//					if (childExists
//							&&
//							!(existingNameBn.equals(procurement_goodsDTO.nameBn)
//									&& existingNameEn.equals(procurement_goodsDTO.nameEn))) {
//						PrintWriter out = response.getWriter();
//						out.println(gson.toJson("Child Exists"));
//						out.close();
//					}
//					else {
//						PrintWriter out = response.getWriter();
//						out.println(gson.toJson("Success"));
//						out.close();
//					}
//
//				}
//
//
//
//			}
//
//			else {
//				PrintWriter out = response.getWriter();
//				out.println(gson.toJson("Invalid Input"));
//				out.close();
//			}
//
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//		}
//	}

	private void addProcurement_goods(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addProcurement_goods");
			String path = getServletContext().getRealPath("/img2/");
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
			String existingNameEn = "";
			String existingNameBn = "";




			List<Procurement_goodsAddRequestDTO> procurement_goodsAddRequestDTOList = mapper.readValue(request.getParameter("itemList"), new TypeReference<List<Procurement_goodsAddRequestDTO>>(){});

			String packageId = "-1";
			String typeId = "-1";

			for (Procurement_goodsAddRequestDTO procurement_goodsAddRequestDTO: procurement_goodsAddRequestDTOList)
			{

				if (StringUtils.isValidString(procurement_goodsAddRequestDTO.procurementPackageId) && !procurement_goodsAddRequestDTO.procurementPackageId.equals("-1")) packageId = procurement_goodsAddRequestDTO.procurementPackageId;
				if (StringUtils.isValidString(procurement_goodsAddRequestDTO.procurementGoodsTypeId) && !procurement_goodsAddRequestDTO.procurementGoodsTypeId.equals("-1")) typeId = procurement_goodsAddRequestDTO.procurementGoodsTypeId;

				Procurement_goodsDTO procurement_goodsDTO;

				if(procurement_goodsAddRequestDTO.iD == -1)
				{
					procurement_goodsDTO = new Procurement_goodsDTO();
					addFlag = true;
				}
				else
				{
					procurement_goodsDTO = Procurement_goodsRepository.getInstance().getProcurement_goodsDTOByID(procurement_goodsAddRequestDTO.iD);
					existingNameEn = procurement_goodsDTO.nameEn;
					existingNameBn = procurement_goodsDTO.nameBn;
					addFlag = false;
				}

				String Value = "";

				Value = procurement_goodsAddRequestDTO.nameEn;

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("nameEn = " + Value);
				if(Value != null)
				{
					procurement_goodsDTO.nameEn = (Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = procurement_goodsAddRequestDTO.nameBn;

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("nameBn = " + Value);
				if(Value != null)
				{
					procurement_goodsDTO.nameBn = (Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = procurement_goodsAddRequestDTO.reportName;

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("reportName = " + Value);
				if(Value != null)
				{
					procurement_goodsDTO.reportName = (Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = procurement_goodsAddRequestDTO.procurementYear;

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("procurementYear = " + Value);
				if(Value != null)
				{
					procurement_goodsDTO.procurementYear = (Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = packageId;

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("procurementPackageId = " + Value);
				if(Value != null && !Value.equalsIgnoreCase("") && !Value.equalsIgnoreCase("-1"))
				{
					procurement_goodsDTO.procurementPackageId = Long.parseLong(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = procurement_goodsAddRequestDTO.piUnitId;

				if(Value != null && !Value.equalsIgnoreCase("") && !Value.equalsIgnoreCase("-1"))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("piUnitId = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					procurement_goodsDTO.piUnitId = Long.parseLong(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = procurement_goodsAddRequestDTO.description;

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("description = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					procurement_goodsDTO.description = (Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = typeId;

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("procurementGoodsTypeId = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					procurement_goodsDTO.procurementGoodsTypeId = Long.parseLong(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = procurement_goodsAddRequestDTO.unitPrice;

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("unitPrice = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					procurement_goodsDTO.unitPrice = Double.parseDouble(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = procurement_goodsAddRequestDTO.estCost;

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("estCost = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					procurement_goodsDTO.estCost = Double.parseDouble(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = procurement_goodsAddRequestDTO.methodAndTypeCat;

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("methodAndTypeCat = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					procurement_goodsDTO.methodAndTypeCat = Integer.parseInt(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = procurement_goodsAddRequestDTO.quantity;

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("quantity = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					procurement_goodsDTO.quantity = Long.parseLong(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = procurement_goodsAddRequestDTO.approvingAuthority;

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("approvingAuthority = " + Value);
				if(Value != null)
				{
					procurement_goodsDTO.approvingAuthority = (Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = procurement_goodsAddRequestDTO.sourceOfFundCat;

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("sourceOfFundCat = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					procurement_goodsDTO.sourceOfFundCat = Integer.parseInt(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = procurement_goodsAddRequestDTO.timeCode;

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("timeCode = " + Value);
				if(Value != null)
				{
					procurement_goodsDTO.timeCode = (Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = procurement_goodsAddRequestDTO.tender;

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("tender = " + Value);
				if(Value != null)
				{
					procurement_goodsDTO.tender = (Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = procurement_goodsAddRequestDTO.tenderOpening;

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("tenderOpening = " + Value);
				if(Value != null)
				{
					procurement_goodsDTO.tenderOpening = (Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = procurement_goodsAddRequestDTO.tenderEvaluation;

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("tenderEvaluation = " + Value);
				if(Value != null)
				{
					procurement_goodsDTO.tenderEvaluation = (Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = procurement_goodsAddRequestDTO.approvalToward;

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("approvalToward = " + Value);
				if(Value != null)
				{
					procurement_goodsDTO.approvalToward = (Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = procurement_goodsAddRequestDTO.awardNotification;

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("awardNotification = " + Value);
				if(Value != null)
				{
					procurement_goodsDTO.awardNotification = (Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = procurement_goodsAddRequestDTO.siginingOfContract;

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("siginingOfContract = " + Value);
				if(Value != null)
				{
					procurement_goodsDTO.siginingOfContract = (Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = procurement_goodsAddRequestDTO.contractSignatureTime;

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("contractSignatureTime = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					procurement_goodsDTO.contractSignatureTime = Long.parseLong(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = procurement_goodsAddRequestDTO.contractCompletionTime;

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("contractCompletionTime = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					procurement_goodsDTO.contractCompletionTime = Long.parseLong(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				if(addFlag)
				{
					procurement_goodsDTO.insertedByUserId = userDTO.ID;
				}


				if(addFlag)
				{
					procurement_goodsDTO.insertedByOrganogramId = userDTO.organogramID;
				}


				if(addFlag)
				{
					Calendar c = Calendar.getInstance();
					c.set(Calendar.HOUR_OF_DAY, 0);
					c.set(Calendar.MINUTE, 0);
					c.set(Calendar.SECOND, 0);
					c.set(Calendar.MILLISECOND, 0);

					procurement_goodsDTO.insertionDate = c.getTimeInMillis();
				}


				Value = procurement_goodsAddRequestDTO.searchColumn;

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("searchColumn = " + Value);
				if(Value != null)
				{
					procurement_goodsDTO.searchColumn = (Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				boolean valid = validateSingleProcurement_goodsDTOByRequestAndIndex(procurement_goodsAddRequestDTO);

				if (valid) {

					// bn en validations starts
					if(Utils.notEnglish(procurement_goodsAddRequestDTO.nameEn)){
						PrintWriter out = response.getWriter();
						out.println(new Gson().toJson("Not English"));
						out.close();
						return;
					}
					if(Utils.notBangla(procurement_goodsAddRequestDTO.nameBn)){
						PrintWriter out = response.getWriter();
						out.println(new Gson().toJson("Not Bangla"));
						out.close();
						return;
					}

					int numberOfHierarchyForValidation = procurement_goodsAddRequestDTO.subTypeBn.size();

					for (int i=0 ;i< numberOfHierarchyForValidation; i++) {
						Value = procurement_goodsAddRequestDTO.subTypeEn.get(i);
						if(Value != null)
						{
							Value = Jsoup.clean(Value,Whitelist.simpleText());
						}
						if(Value != null && !Value.trim().isEmpty() )
						{
							if(!Utils.notBangla(Value)){
								PrintWriter out = response.getWriter();
								out.println(new Gson().toJson("Not English"));
								out.close();
								return;
							}
						}

						Value = procurement_goodsAddRequestDTO.subTypeBn.get(i);
						if(Value != null)
						{
							Value = Jsoup.clean(Value,Whitelist.simpleText());
						}

						if(Value != null && !Value.trim().isEmpty() )
						{
							if(!Utils.notEnglish(Value)){
								PrintWriter out = response.getWriter();
								out.println(new Gson().toJson("Not Bangla"));
								out.close();
								return;
							}
						}

					}


					// bn en validation ends


					System.out.println("Done adding  addProcurement_goods dto = " + procurement_goodsDTO);
					long returnedID = -1;

					if(isPermanentTable == false) //add new row for validation and make the old row outdated
					{
						procurement_goodsDAO.setIsDeleted(procurement_goodsDTO.iD, CommonDTO.OUTDATED);
						returnedID = procurement_goodsDAO.add(procurement_goodsDTO);
						procurement_goodsDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
					}
					else
					{
						int addFlagConstant = procurement_goodsAddRequestDTO.iD == -1 ? SessionConstants.INSERT : SessionConstants.UPDATE;

						Long itemId = -1L;

						int numberOfHierarchy = procurement_goodsAddRequestDTO.subTypeBn.size();

						if (addFlagConstant == SessionConstants.UPDATE) {

							if (!Pi_app_request_detailsDAO.getInstance().getByItemId(procurement_goodsDTO.iD).isEmpty()) {
								PrintWriter out = response.getWriter();
								out.println(gson.toJson("App exists"));
								out.close();
								return;
							}
							itemId = procurement_goodsAddRequestDTO.iD;
							if (StringUtils.isValidString(procurement_goodsDTO.parentIdsCommaSeparated)) Procurement_goodsDAO.getInstance().deleteAll("(" + procurement_goodsDTO.parentIdsCommaSeparated + ")");

						}

						List<String> parentIdListCommaSeparated = new ArrayList<>(1);

						for (int i=0 ;i< numberOfHierarchy; i++) {

							if (returnedID != -1) parentIdListCommaSeparated.add(String.valueOf(returnedID));
							procurement_goodsDTO.iD = -1L;
							procurement_goodsDTO.parentId = returnedID;
							procurement_goodsDTO.parentIdsCommaSeparated = String.join(",", parentIdListCommaSeparated);
							procurement_goodsDTO.isActualItem = 0;

							Value = procurement_goodsAddRequestDTO.subTypeEn.get(i);

							if(Value != null)
							{
								Value = Jsoup.clean(Value,Whitelist.simpleText());
							}
							System.out.println("subTypeEn = " + Value);
							if(Value != null && !Value.trim().isEmpty() )
							{
								procurement_goodsDTO.nameEn = (Value);
							}
							else
							{
//							valid = false;
								System.out.println("FieldName has a null Value, not updating" + " = " + Value);
							}

							Value = procurement_goodsAddRequestDTO.subTypeBn.get(i);

							if(Value != null)
							{
								Value = Jsoup.clean(Value,Whitelist.simpleText());
							}
							System.out.println("subTypeBn = " + Value);
							if(Value != null && !Value.trim().isEmpty() )
							{
								procurement_goodsDTO.nameBn = (Value);
							}
							else
							{
//							valid = false;
								System.out.println("FieldName has a null Value, not updating" + " = " + Value);
							}

							returnedID = procurement_goodsDAO.manageWriteOperations(procurement_goodsDTO, SessionConstants.INSERT, -1, userDTO);

						}


						if (returnedID != -1) parentIdListCommaSeparated.add(String.valueOf(returnedID));
						procurement_goodsDTO.iD = itemId;
						procurement_goodsDTO.parentId = returnedID;
						procurement_goodsDTO.parentIdsCommaSeparated = String.join(",", parentIdListCommaSeparated);


						Value = procurement_goodsAddRequestDTO.nameEn;

						if(Value != null)
						{
							Value = Jsoup.clean(Value,Whitelist.simpleText());
						}
						System.out.println("nameEn = " + Value);
						if(Value != null && !Value.trim().isEmpty() )
						{
							procurement_goodsDTO.nameEn = (Value);
						}
						else
						{
//							valid = false;
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}

						Value = procurement_goodsAddRequestDTO.nameBn;

						if(Value != null)
						{
							Value = Jsoup.clean(Value,Whitelist.simpleText());
						}
						System.out.println("nameBn = " + Value);
						if(Value != null && !Value.trim().isEmpty() )
						{
							procurement_goodsDTO.nameBn = (Value);
						}
						else
						{
//							valid = false;
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}

						Value = procurement_goodsAddRequestDTO.returnable;

						if(Value != null)
						{
							Value = Jsoup.clean(Value,Whitelist.simpleText());
						}
						System.out.println("returnable = " + Value);
						if(Value != null)
						{
							procurement_goodsDTO.returnable = Integer.parseInt(Value);
						}
						else
						{
//							valid = false;
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}


						procurement_goodsDTO.isActualItem = 1;


						returnedID = procurement_goodsDAO.manageWriteOperations(procurement_goodsDTO, addFlagConstant, -1, userDTO);



						reloadNecessaryRepository();



					}



				}

				else {
					PrintWriter out = response.getWriter();
					out.println(gson.toJson("Invalid Input"));
					out.close();
					return;
				}


			}

			PrintWriter out = response.getWriter();
			out.println(gson.toJson("Success"));
			out.close();
			return;

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}



//	private boolean validateProcurement_goodsDTOByRequest(HttpServletRequest request) throws Exception{
//		boolean valid = true;
//		valid &= validateSingleProcurement_goodsDTOByRequestAndIndex(request);
//
//		return valid;
//	}
//
//	private boolean validateProcurement_goodsDTOByRequest(List<Procurement_goodsAddRequestDTO> procurement_goodsAddRequestDTOList) throws Exception{
//
//		return !procurement_goodsAddRequestDTOList
//				.stream()
//				.anyMatch(procurement_goodsAddRequestDTO -> !validateSingleProcurement_goodsDTOByRequestAndIndex(procurement_goodsAddRequestDTO));
//
//	}


//	private boolean validateSingleProcurement_goodsDTOByRequestAndIndex(HttpServletRequest request) throws Exception{
//
//		Procurement_goodsDTO procurement_goodsDTO = new Procurement_goodsDTO();
//		boolean valid = true;
//
//		String Value = "";
//
//
//		Value = request.getParameter("iD");
//
//		if(Value != null)
//		{
//			Value = Jsoup.clean(Value,Whitelist.simpleText());
//		}
//		System.out.println("ID = " + Value);
//		if(Value != null)
//		{
//			procurement_goodsDTO.iD = Long.parseLong(Value);
//		}
//		else
//		{
//			valid = false;
//			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//		}
//
//		int numberOfHierarchy = request.getParameterValues("subTypeEn").length;
//
//		for (int i=0 ;i< numberOfHierarchy; i++) {
//
//			Value = request.getParameterValues("subTypeEn")[i];
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("subTypeEn = " + Value);
//			if(Value != null && !Value.trim().isEmpty() )
//			{
//				procurement_goodsDTO.nameEn = (Value);
//			}
//			else
//			{
//				valid = false;
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameterValues("subTypeBn")[i];
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("subTypeBn = " + Value);
//			if(Value != null && !Value.trim().isEmpty() )
//			{
//				procurement_goodsDTO.nameBn = (Value);
//			}
//			else
//			{
//				valid = false;
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//		}
//
//
//		procurement_goodsDTO.reportName = "admin";
//
//		Value = request.getParameter("procurementPackageId");
//
//		if(Value != null && !Value.equalsIgnoreCase(""))
//		{
//			Value = Jsoup.clean(Value,Whitelist.simpleText());
//		}
//		System.out.println("procurementPackageId = " + Value);
//		if(Value != null && !Value.equalsIgnoreCase("") && !Value.equalsIgnoreCase("-1") )
//		{
//			procurement_goodsDTO.procurementPackageId = Long.parseLong(Value);
//		}
//		else
//		{
//			valid = false;
//			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//		}
//
//		Value = request.getParameter("piUnitId");
//
//		if(Value != null && !Value.equalsIgnoreCase(""))
//		{
//			Value = Jsoup.clean(Value,Whitelist.simpleText());
//		}
//		System.out.println("piUnitId = " + Value);
//		if(Value != null && !Value.equalsIgnoreCase("") && !Value.equalsIgnoreCase("-1") )
//		{
//			procurement_goodsDTO.piUnitId = Long.parseLong(Value);
//		}
//		else
//		{
//			valid = false;
//			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//		}
//
//		Value = request.getParameter("procurementGoodsTypeId");
//
//		if(Value != null && !Value.equalsIgnoreCase(""))
//		{
//			Value = Jsoup.clean(Value,Whitelist.simpleText());
//		}
//		System.out.println("procurementGoodsTypeId = " + Value);
//		if(Value != null && !Value.equalsIgnoreCase("") && !Value.equalsIgnoreCase("-1") )
//		{
//			procurement_goodsDTO.procurementGoodsTypeId = Long.parseLong(Value);
//		}
//		else
//		{
//			valid = false;
//			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//		}
//
//
//
//		return valid;
//
//	}


	private boolean validateSingleProcurement_goodsDTOByRequestAndIndex(Procurement_goodsAddRequestDTO procurement_goodsAddRequestDTO) {

		boolean valid = true;
		String Value = "";

		boolean foundInvalidSubType =
				procurement_goodsAddRequestDTO.subTypeBn
				.stream()
				.anyMatch(subTypeBn -> !StringUtils.isValidString(subTypeBn))
		||		procurement_goodsAddRequestDTO.subTypeEn
				.stream()
				.anyMatch(subTypeBn -> !StringUtils.isValidString(subTypeBn));

		if (foundInvalidSubType) return false;


		if (!(StringUtils.isValidString(procurement_goodsAddRequestDTO.nameBn)
		&&  StringUtils.isValidString(procurement_goodsAddRequestDTO.nameEn))) return false;

		procurement_goodsAddRequestDTO.reportName = "admin";

		Value = procurement_goodsAddRequestDTO.procurementPackageId;

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("procurementPackageId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase("") && !Value.equalsIgnoreCase("-1") )
		{
		}
		else
		{
			valid = false;
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = procurement_goodsAddRequestDTO.piUnitId;

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("piUnitId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase("") && !Value.equalsIgnoreCase("-1") )
		{
		}
		else
		{
			valid = false;
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = procurement_goodsAddRequestDTO.procurementGoodsTypeId;

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("procurementGoodsTypeId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase("") && !Value.equalsIgnoreCase("-1") )
		{
		}
		else
		{
			valid = false;
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}



		return valid;

	}


	private void getProcurement_goods(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getProcurement_goods");
		Procurement_goodsDTO procurement_goodsDTO = null;
		try
		{
			procurement_goodsDTO = Procurement_goodsRepository.getInstance().getProcurement_goodsDTOByID(id);
			request.setAttribute("ID", procurement_goodsDTO.iD);
			request.setAttribute("procurement_goodsDTO",procurement_goodsDTO);
			request.setAttribute("procurement_goodsDAO",procurement_goodsDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "procurement_goods/procurement_goodsInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "procurement_goods/procurement_goodsSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "procurement_goods/procurement_goodsEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "procurement_goods/procurement_goodsEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getProcurement_goods(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getProcurement_goods(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchProcurement_goods(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchProcurement_goods 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_PROCUREMENT_GOODS,
			request,
			procurement_goodsDAO,
			SessionConstants.VIEW_PROCUREMENT_GOODS,
			SessionConstants.SEARCH_PROCUREMENT_GOODS,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("procurement_goodsDAO",procurement_goodsDAO);
		request.getSession(true).setAttribute("report_name",request.getParameter("report_name_ajax"));
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to procurement_goods/procurement_goodsApproval.jsp");
	        	rd = request.getRequestDispatcher("procurement_goods/procurement_goodsApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to procurement_goods/procurement_goodsApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("procurement_goods/procurement_goodsApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to procurement_goods/procurement_goodsSearch.jsp");
	        	rd = request.getRequestDispatcher("procurement_goods/procurement_goodsSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to procurement_goods/procurement_goodsSearchForm.jsp");
	        	rd = request.getRequestDispatcher("procurement_goods/procurement_goodsSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

	private void yearlySearchProcurement_goods(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  yearly searchProcurement_goods 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

		Procurement_goodsReportNavigationManager rnManager = new Procurement_goodsReportNavigationManager(
				SessionConstants.NAV_PROCUREMENT_GOODS,
				request,
				procurement_goodsDAO,
				SessionConstants.VIEW_PROCUREMENT_GOODS,
				SessionConstants.SEARCH_PROCUREMENT_GOODS,
				tableName,
				isPermanent,
				userDTO,
				filter,
				true);
		try
		{
			System.out.println("trying to dojob");
			rnManager.doJobYearlySearch(loginDTO);
		}
		catch(Exception e)
		{
			System.out.println("failed to dojob" + e);
		}

		request.setAttribute("procurement_goodsDAO",procurement_goodsDAO);
		RequestDispatcher rd;
		if(!isPermanent)
		{
			if(hasAjax == false)
			{
				System.out.println("Going to procurement_goods/procurement_goodsApproval.jsp");
				rd = request.getRequestDispatcher("procurement_goods/procurement_goodsApproval.jsp");
			}
			else
			{
				System.out.println("Going to procurement_goods/procurement_goodsApprovalForm.jsp");
				rd = request.getRequestDispatcher("procurement_goods/procurement_goodsApprovalForm.jsp");
			}
		}
		else
		{
			if(hasAjax == false)
			{
				System.out.println("Going to procurement_goods/procurement_goodsYearlySearch.jsp");
				rd = request.getRequestDispatcher("procurement_goods/procurement_goodsYearlySearch.jsp");
			}
			else
			{
				System.out.println("Going to procurement_goods/procurement_goodsYearlySearchForm.jsp");
				rd = request.getRequestDispatcher("procurement_goods/procurement_goodsYearlySearchForm.jsp");
			}
		}
		rd.forward(request, response);
	}

	private void report(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchProcurement_goods 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

		Procurement_goodsReportNavigationManager rnManager = new Procurement_goodsReportNavigationManager(
				SessionConstants.NAV_PROCUREMENT_GOODS,
				request,
				procurement_goodsDAO,
				SessionConstants.VIEW_PROCUREMENT_GOODS,
				SessionConstants.SEARCH_PROCUREMENT_GOODS,
				tableName,
				isPermanent,
				userDTO,
				filter,
				true);
		try
		{
			System.out.println("trying to dojob");
			rnManager.doJob(loginDTO);
		}
		catch(Exception e)
		{
			System.out.println("failed to dojob" + e);
		}

		request.setAttribute("procurement_goodsDAO",procurement_goodsDAO);
		RequestDispatcher rd;
		if(!isPermanent)
		{
			if(hasAjax == false)
			{
				System.out.println("Going to procurement_goods/procurement_goodsApproval.jsp");
				rd = request.getRequestDispatcher("procurement_goods/procurement_goodsApproval.jsp");
			}
			else
			{
				System.out.println("Going to procurement_goods/procurement_goodsApprovalForm.jsp");
				rd = request.getRequestDispatcher("procurement_goods/procurement_goodsApprovalForm.jsp");
			}
		}
		else
		{
			if(hasAjax == false)
			{
				System.out.println("Going to procurement_goods/procurement_goodsReport.jsp");
				rd = request.getRequestDispatcher("procurement_goods/procurement_goodsReport.jsp");
			}
			else
			{
				System.out.println("Going to procurement_goods/procurement_goodsReportForm.jsp");
				rd = request.getRequestDispatcher("procurement_goods/procurement_goodsReportForm.jsp");
			}
		}
		rd.forward(request, response);
	}

	private void reportPost(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchProcurement_goods 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

		Procurement_goodsReportNavigationManager rnManager = new Procurement_goodsReportNavigationManager(
				SessionConstants.NAV_PROCUREMENT_GOODS,
				request,
				procurement_goodsDAO,
				SessionConstants.VIEW_PROCUREMENT_GOODS,
				SessionConstants.SEARCH_PROCUREMENT_GOODS,
				tableName,
				isPermanent,
				userDTO,
				filter,
				true);
		try
		{
			System.out.println("trying to dojob");
			rnManager.doJob(loginDTO);
		}
		catch(Exception e)
		{
			System.out.println("failed to dojob" + e);
		}

		request.setAttribute("procurement_goodsDAO",procurement_goodsDAO);
		RequestDispatcher rd;
		if(!isPermanent)
		{
			if(hasAjax == false)
			{
				System.out.println("Going to procurement_goods/procurement_goodsApproval.jsp");
				rd = request.getRequestDispatcher("procurement_goods/procurement_goodsApproval.jsp");
			}
			else
			{
				System.out.println("Going to procurement_goods/procurement_goodsApprovalForm.jsp");
				rd = request.getRequestDispatcher("procurement_goods/procurement_goodsApprovalForm.jsp");
			}
		}
		else
		{
			if(hasAjax == false)
			{
				System.out.println("Going to procurement_goods/procurement_goodsReport.jsp");
				rd = request.getRequestDispatcher("procurement_goods/procurement_goodsReport.jsp");
			}
			else
			{
				System.out.println("Going to procurement_goods/procurement_goodsReportForm.jsp");
				rd = request.getRequestDispatcher("procurement_goods/procurement_goodsReportForm.jsp");
			}
		}
		rd.forward(request, response);
	}



	private void getOptionByProperty(HttpServletRequest request, HttpServletResponse response, long id, String property) throws ServletException, IOException
	{
		System.out.println("in getProcurement_package");
		Procurement_packageDTO procurement_packageDTO = null;
		try
		{
			long filterUnitLong = -1;
			String filterUnit = request.getParameter("filterUnit");
			if (filterUnit != null)filterUnitLong = Long.parseLong(filterUnit);
			String index = request.getParameter("index");
			LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
			UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
			String language = LM.getLanguageIDByUserDTO(userDTO) == CommonConstant.Language_ID_English ? "English" : "Bangla";
			Procurement_goodsDAO procurement_goodsDAO = Procurement_goodsDAO.getInstance();
			String whereClause = property + " = " + id;

			Procurement_goodsResponseDTO procurement_goodsResponseDTO = new Procurement_goodsResponseDTO();

			if (property.equals("procurement_goods_type_type")) {
//				whereClause += " and parent_id = -1 ";
				whereClause += " and is_actual_item = 1 ";
			}
			else {
				Procurement_goodsDTO dto = procurement_goodsDAO.getDTOByID(id);
				procurement_goodsResponseDTO = gson.fromJson(
						gson.toJson(dto), Procurement_goodsResponseDTO.class
				);

				Procurement_packageDTO packageDTO = Procurement_packageRepository.getInstance().getProcurement_packageDTOByID(dto.procurementPackageId);
				ProcurementGoodsTypeDTO goodsTypeDTO = ProcurementGoodsTypeRepository.getInstance().getProcurementGoodsTypeDTOByID(dto.procurementGoodsTypeId);
				Pi_unitDTO piUnitDTO = Pi_unitRepository.getInstance().getPi_unitDTOByiD(procurement_goodsResponseDTO.piUnitId);

				procurement_goodsResponseDTO.procurementPackageNameBn = packageDTO.nameBn;
				procurement_goodsResponseDTO.procurementPackageNameEn = packageDTO.nameEn;

				procurement_goodsResponseDTO.procurementGoodsTypeNameBn = goodsTypeDTO.nameBn;
				procurement_goodsResponseDTO.procurementGoodsTypeNameEn = goodsTypeDTO.nameEn;

				procurement_goodsResponseDTO.piUnitNameEn = piUnitDTO.nameEn;
				procurement_goodsResponseDTO.piUnitNameBn = piUnitDTO.nameBn;

				procurement_goodsResponseDTO.circularData = procurement_goodsDAO.getCircularData(dto.iD, language);
			}

			String sql = "SELECT * FROM " + tableName + " where isDeleted=0 and " + whereClause;
			List<Procurement_goodsDTO> procurement_goodsDTOS = procurement_goodsDAO.getDTOsBySql(sql);
			boolean childExists = !procurement_goodsDTOS.isEmpty();

			if (!childExists) {
				Procurement_goodsResponseWithOptionDTO responseDTO = new Procurement_goodsResponseWithOptionDTO();
				responseDTO.procurementGoodsResponseDTO = procurement_goodsResponseDTO;

				PrintWriter out = response.getWriter();
				out.println(gson.toJson(responseDTO));
				out.close();
				return;
			}
			else {

				String options = "";

				if (filterUnitLong != 1) {
					options = CommonDAO.getOptions (language, "select", "procurement_goods", "", "", "", "", "", "any", whereClause);
				}
				else {
					List<OptionDTO> optionDTOList =procurement_goodsDTOS.stream()
							.filter(dto -> {
								String assignedOfficeUnitIds = dto.assignedOfcUnitIds;
								if (StringUtils.isValidString(assignedOfficeUnitIds)) {
									return !(Arrays.stream(assignedOfficeUnitIds.split(","))
											.anyMatch(unitId ->
													unitId.equals(String.valueOf(userDTO.unitID))
											)
									);
								}
								return true;
							})
							.map(dto->new OptionDTO(dto.nameEn,dto.nameBn,String.valueOf(dto.iD)))
							.collect(Collectors.toList());
					options = Utils.buildOptions(optionDTOList, language, null);
				}


				String selectStartString = "<select class='form-control' name='subType'\n" +
						"                            id='" + "subType_select_" + index + "' tag='pb_html'>";
				String selectEndString = "</select>";

				String responseString = "<div class=\"form-group row\" id=\"div_id_" + index + "\">\n" +
						"<label class=\"col-md-4 col-form-label text-md-right\">" + LM.getText(LC.PROCUREMENT_GOODS_ADD_ID, loginDTO) + "\n" +
						"                </label>\n" +
						"                <div class=\"col-md-8\">" + selectStartString + options + selectEndString +
						" </div>\n" +
						"            </div>\n" +
						"</div>";

				Procurement_goodsResponseWithOptionDTO responseDTO = new Procurement_goodsResponseWithOptionDTO();
				responseDTO.options = responseString;
				responseDTO.procurementGoodsResponseDTO = procurement_goodsResponseDTO;

				PrintWriter out = response.getWriter();
				out.println(gson.toJson(responseDTO));
				out.close();
				return;

			}
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getByGoodsType(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		if (request.getParameter("ID") == null) return;
		getOptionByProperty(request, response, Long.parseLong(request.getParameter("ID")), "procurement_goods_type_type");
	}


	private void getByParent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		if (request.getParameter("ID") == null) return;
		getOptionByProperty(request, response, Long.parseLong(request.getParameter("ID")), "parent_id");
	}

	private void reloadNecessaryRepository(){
		PBNameRepository.getInstance().reload(false);
		Procurement_goodsRepository.getInstance().reload(false);
	}
}

