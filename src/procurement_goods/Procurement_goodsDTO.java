package procurement_goods;
import java.util.*;

import pb.OptionDTO;
import util.*;


public class Procurement_goodsDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
    public String procurementYear = "";
    public String reportName = "";
	public long procurementPackageId = -1;
	public long procurementGoodsTypeId = -1;
	public double unitPrice = 0;
	public double estCost = 0;
	public int methodAndTypeCat = -1;
    public int returnable = 0;
    public int isActualItem = 0;
    public String parentIdsCommaSeparated = "";
    public long quantity = 0;
	public long parentId = -1;
	public long piUnitId = -1;
    public String approvingAuthority = "";
	public int sourceOfFundCat = -1;
    public String timeCode = "";
    public String tender = "";
    public String description = "";
    public String tenderOpening = "";
    public String tenderEvaluation = "";
    public String approvalToward = "";
    public String awardNotification = "";
    public String siginingOfContract = "";
	public long contractSignatureTime = -1;
	public long contractCompletionTime = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;

    public String assignedOfcUnitIds = "";

    public long piPackageItemMapId = -1;
    public long piPackageItemMapChildId = -1;

    public OptionDTO getOptionDTO() {

        return new OptionDTO(
                nameEn,
                nameBn,
                String.format("%d", iD)
        );
    }


    @Override
	public String toString() {
            return "$Procurement_goodsDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " procurementYear = " + procurementYear +
            " reportName = " + reportName +
            " procurementGoodsTypeId = " + procurementGoodsTypeId +
            " unitPrice = " + unitPrice +
            " estCost = " + estCost +
            " methodAndTypeCat = " + methodAndTypeCat +
                    " returnable = " + returnable +
                    " parentIdsCommaSeparated = " + parentIdsCommaSeparated +
                    " isActualItem = " + isActualItem +
            " quantity = " + quantity +
            " approvingAuthority = " + approvingAuthority +
            " sourceOfFundCat = " + sourceOfFundCat +
            " timeCode = " + timeCode +
            " tender = " + tender +
            " tenderOpening = " + tenderOpening +
            " tenderEvaluation = " + tenderEvaluation +
            " approvalToward = " + approvalToward +
            " awardNotification = " + awardNotification +
            " siginingOfContract = " + siginingOfContract +
            " contractSignatureTime = " + contractSignatureTime +
            " contractCompletionTime = " + contractCompletionTime +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            " assignedOfcUnitIds = " + assignedOfcUnitIds +
            "]";
    }

}