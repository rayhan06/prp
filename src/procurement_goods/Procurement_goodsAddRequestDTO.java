package procurement_goods;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import pb.OptionDTO;
import util.CommonDTO;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Procurement_goodsAddRequestDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";

    @JsonProperty("subTypeEn")
    public List<String> subTypeEn;

    @JsonProperty("subTypeBn")
    public List<String> subTypeBn;

    public String procurementYear = "";
    public String reportName = "";
	public String procurementPackageId = "-1";
	public String procurementGoodsTypeId = "-1";
	public String unitPrice = "0";
	public String estCost = "0";
	public String methodAndTypeCat = "-1";
    public String returnable = "1";
    public String isActualItem = "0";
    public String quantity = "0";
	public String parentId = "-1";
	public String piUnitId = "-1";
    public String approvingAuthority = "";
	public String sourceOfFundCat = "-1";
    public String timeCode = "";
    public String tender = "";
    public String description = "";
    public String tenderOpening = "";
    public String tenderEvaluation = "";
    public String approvalToward = "";
    public String awardNotification = "";
    public String siginingOfContract = "";
	public String contractSignatureTime = "-1";
	public String contractCompletionTime = "-1";
	public String insertedByUserId = "-1";
	public String insertedByOrganogramId = "-1";
	public String insertionDate = "-1";

    public String assignedOfcUnitIds = "";

    public List<String> getSubTypeEn() {

        return subTypeEn;

    }


    public void setSubTypeEn(List<String> subTypeEns) {

        this.subTypeEn= subTypeEns;

    }

    public List<String> getSubTypeBn() {

        return subTypeBn;

    }


    public void setSubTypeBn(List<String> subTypeBns) {

        this.subTypeBn= subTypeBns;

    }

    public OptionDTO getOptionDTO() {

        return new OptionDTO(
                nameEn,
                nameBn,
                String.format("%d", iD)
        );
    }


    @Override
	public String toString() {
            return "$Procurement_goodsDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " procurementYear = " + procurementYear +
            " reportName = " + reportName +
            " procurementGoodsTypeId = " + procurementGoodsTypeId +
            " unitPrice = " + unitPrice +
            " estCost = " + estCost +
            " methodAndTypeCat = " + methodAndTypeCat +
                    " returnable = " + returnable +
            " quantity = " + quantity +
            " approvingAuthority = " + approvingAuthority +
            " sourceOfFundCat = " + sourceOfFundCat +
            " timeCode = " + timeCode +
            " tender = " + tender +
            " tenderOpening = " + tenderOpening +
            " tenderEvaluation = " + tenderEvaluation +
            " approvalToward = " + approvalToward +
            " awardNotification = " + awardNotification +
            " siginingOfContract = " + siginingOfContract +
            " contractSignatureTime = " + contractSignatureTime +
            " contractCompletionTime = " + contractCompletionTime +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            " assignedOfcUnitIds = " + assignedOfcUnitIds +
            " subTypeEn = " + subTypeEn +
            " subTypeBn = " + subTypeBn +
            "]";
    }

}