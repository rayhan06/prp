package procurement_goods;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import office_units.Office_unitsDTO;
import org.apache.log4j.Logger;

import pb.OptionDTO;
import pb.Utils;
import pi_unit.Pi_unitDTO;
import pi_unit.Pi_unitRepository;
import procurement_package.Procurement_packageDTO;
import recruitment_test_name.Recruitment_test_nameDTO;
import repository.Repository;
import repository.RepositoryManager;
import util.StringUtils;
import util.UtilCharacter;
import vm_route_travel.Vm_route_travelDAO;


public class Procurement_goodsRepository implements Repository {
	Procurement_goodsDAO procurement_goodsDAO = null;
	Gson gson;

	public void setDAO(Procurement_goodsDAO procurement_goodsDAO)
	{
		this.procurement_goodsDAO = procurement_goodsDAO;
		gson = new Gson();
	}
	
	
	static Logger logger = Logger.getLogger(Procurement_goodsRepository.class);
	Map<Long, Procurement_goodsDTO>mapOfProcurement_goodsDTOToiD;
	Map<String, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOToprocurementYear;
	Map<String, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOToreportName;
	Map<Long, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOToparentId;
	Map<Long, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOToPackageId;
//	Map<String, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOTonameEn;
//	Map<String, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOTonameBn;
//	Map<Long, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOToprocurementGoodsTypeId;
//	Map<Double, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOTounitPrice;
//	Map<Double, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOToestCost;
//	Map<Integer, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOTomethodAndTypeCat;
//	Map<String, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOToapprovingAuthority;
//	Map<Integer, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOTosourceOfFundCat;
//	Map<String, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOTotimeCode;
//	Map<String, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOTotender;
//	Map<String, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOTotenderOpening;
//	Map<String, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOTotenderEvaluation;
//	Map<String, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOToapprovalToward;
//	Map<String, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOToawardNotification;
//	Map<String, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOTosiginingOfContract;
//	Map<Long, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOTocontractSignatureTime;
//	Map<Long, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOTocontractCompletionTime;
//	Map<Long, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOToinsertedByUserId;
//	Map<Long, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOToinsertedByOrganogramId;
//	Map<Long, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOToinsertionDate;
//	Map<Long, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOTolastModificationTime;
//	Map<String, Set<Procurement_goodsDTO> >mapOfProcurement_goodsDTOTosearchColumn;


	static Procurement_goodsRepository instance = null;  
	private Procurement_goodsRepository(){
		mapOfProcurement_goodsDTOToiD = new ConcurrentHashMap<>();
		mapOfProcurement_goodsDTOToprocurementYear = new ConcurrentHashMap<>();
		mapOfProcurement_goodsDTOToreportName = new ConcurrentHashMap<>();
		mapOfProcurement_goodsDTOToparentId = new ConcurrentHashMap<>();
		mapOfProcurement_goodsDTOToPackageId = new ConcurrentHashMap<>();
// 		mapOfProcurement_goodsDTOTonameEn = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOTonameBn = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOToprocurementGoodsTypeId = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOTounitPrice = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOToestCost = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOTomethodAndTypeCat = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOToapprovingAuthority = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOTosourceOfFundCat = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOTotimeCode = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOTotender = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOTotenderOpening = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOTotenderEvaluation = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOToapprovalToward = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOToawardNotification = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOTosiginingOfContract = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOTocontractSignatureTime = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOTocontractCompletionTime = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOToinsertedByUserId = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOToinsertionDate = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOTolastModificationTime = new ConcurrentHashMap<>();
//		mapOfProcurement_goodsDTOTosearchColumn = new ConcurrentHashMap<>();

		setDAO(Procurement_goodsDAO.getInstance());

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Procurement_goodsRepository getInstance(){
		if (instance == null){
			instance = new Procurement_goodsRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(procurement_goodsDAO == null)
		{
			return;
		}
		try {
			List<Procurement_goodsDTO> procurement_goodsDTOs = procurement_goodsDAO.getAllProcurement_goods(reloadAll);
			for(Procurement_goodsDTO procurement_goodsDTO : procurement_goodsDTOs) {
				Procurement_goodsDTO oldProcurement_goodsDTO = getProcurement_goodsDTOByIDWithoutClone(procurement_goodsDTO.iD);
				if( oldProcurement_goodsDTO != null ) {
					mapOfProcurement_goodsDTOToiD.remove(oldProcurement_goodsDTO.iD);
				
					if(mapOfProcurement_goodsDTOToprocurementYear.containsKey(oldProcurement_goodsDTO.procurementYear)) {
						mapOfProcurement_goodsDTOToprocurementYear.get(oldProcurement_goodsDTO.procurementYear).remove(oldProcurement_goodsDTO);
					}
					if(mapOfProcurement_goodsDTOToprocurementYear.get(oldProcurement_goodsDTO.procurementYear).isEmpty()) {
						mapOfProcurement_goodsDTOToprocurementYear.remove(oldProcurement_goodsDTO.procurementYear);
					}
 					if(mapOfProcurement_goodsDTOToreportName.containsKey(oldProcurement_goodsDTO.reportName)) {
						mapOfProcurement_goodsDTOToreportName.get(oldProcurement_goodsDTO.reportName).remove(oldProcurement_goodsDTO);
					}
					if(mapOfProcurement_goodsDTOToreportName.get(oldProcurement_goodsDTO.reportName).isEmpty()) {
						mapOfProcurement_goodsDTOToreportName.remove(oldProcurement_goodsDTO.reportName);
					}
					if(mapOfProcurement_goodsDTOToparentId.containsKey(oldProcurement_goodsDTO.parentId)) {
						mapOfProcurement_goodsDTOToparentId.get(oldProcurement_goodsDTO.parentId).remove(oldProcurement_goodsDTO);
					}
					if(mapOfProcurement_goodsDTOToparentId.get(oldProcurement_goodsDTO.parentId).isEmpty()) {
						mapOfProcurement_goodsDTOToparentId.remove(oldProcurement_goodsDTO.parentId);
					}

					if(mapOfProcurement_goodsDTOToPackageId.containsKey(oldProcurement_goodsDTO.procurementPackageId)) {
						mapOfProcurement_goodsDTOToPackageId.get(oldProcurement_goodsDTO.procurementPackageId).remove(oldProcurement_goodsDTO);
					}
					if(mapOfProcurement_goodsDTOToPackageId.get(oldProcurement_goodsDTO.procurementPackageId).isEmpty()) {
						mapOfProcurement_goodsDTOToPackageId.remove(oldProcurement_goodsDTO.procurementPackageId);
					}
// 					if(mapOfProcurement_goodsDTOTonameEn.containsKey(oldProcurement_goodsDTO.nameEn)) {
//						mapOfProcurement_goodsDTOTonameEn.get(oldProcurement_goodsDTO.nameEn).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOTonameEn.get(oldProcurement_goodsDTO.nameEn).isEmpty()) {
//						mapOfProcurement_goodsDTOTonameEn.remove(oldProcurement_goodsDTO.nameEn);
//					}
//
//					if(mapOfProcurement_goodsDTOTonameBn.containsKey(oldProcurement_goodsDTO.nameBn)) {
//						mapOfProcurement_goodsDTOTonameBn.get(oldProcurement_goodsDTO.nameBn).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOTonameBn.get(oldProcurement_goodsDTO.nameBn).isEmpty()) {
//						mapOfProcurement_goodsDTOTonameBn.remove(oldProcurement_goodsDTO.nameBn);
//					}
//
//					if(mapOfProcurement_goodsDTOToprocurementGoodsTypeId.containsKey(oldProcurement_goodsDTO.procurementGoodsTypeId)) {
//						mapOfProcurement_goodsDTOToprocurementGoodsTypeId.get(oldProcurement_goodsDTO.procurementGoodsTypeId).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOToprocurementGoodsTypeId.get(oldProcurement_goodsDTO.procurementGoodsTypeId).isEmpty()) {
//						mapOfProcurement_goodsDTOToprocurementGoodsTypeId.remove(oldProcurement_goodsDTO.procurementGoodsTypeId);
//					}
//
//					if(mapOfProcurement_goodsDTOTounitPrice.containsKey(oldProcurement_goodsDTO.unitPrice)) {
//						mapOfProcurement_goodsDTOTounitPrice.get(oldProcurement_goodsDTO.unitPrice).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOTounitPrice.get(oldProcurement_goodsDTO.unitPrice).isEmpty()) {
//						mapOfProcurement_goodsDTOTounitPrice.remove(oldProcurement_goodsDTO.unitPrice);
//					}
//
//					if(mapOfProcurement_goodsDTOToestCost.containsKey(oldProcurement_goodsDTO.estCost)) {
//						mapOfProcurement_goodsDTOToestCost.get(oldProcurement_goodsDTO.estCost).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOToestCost.get(oldProcurement_goodsDTO.estCost).isEmpty()) {
//						mapOfProcurement_goodsDTOToestCost.remove(oldProcurement_goodsDTO.estCost);
//					}
//
//					if(mapOfProcurement_goodsDTOTomethodAndTypeCat.containsKey(oldProcurement_goodsDTO.methodAndTypeCat)) {
//						mapOfProcurement_goodsDTOTomethodAndTypeCat.get(oldProcurement_goodsDTO.methodAndTypeCat).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOTomethodAndTypeCat.get(oldProcurement_goodsDTO.methodAndTypeCat).isEmpty()) {
//						mapOfProcurement_goodsDTOTomethodAndTypeCat.remove(oldProcurement_goodsDTO.methodAndTypeCat);
//					}
//
//					if(mapOfProcurement_goodsDTOToapprovingAuthority.containsKey(oldProcurement_goodsDTO.approvingAuthority)) {
//						mapOfProcurement_goodsDTOToapprovingAuthority.get(oldProcurement_goodsDTO.approvingAuthority).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOToapprovingAuthority.get(oldProcurement_goodsDTO.approvingAuthority).isEmpty()) {
//						mapOfProcurement_goodsDTOToapprovingAuthority.remove(oldProcurement_goodsDTO.approvingAuthority);
//					}
//
//					if(mapOfProcurement_goodsDTOTosourceOfFundCat.containsKey(oldProcurement_goodsDTO.sourceOfFundCat)) {
//						mapOfProcurement_goodsDTOTosourceOfFundCat.get(oldProcurement_goodsDTO.sourceOfFundCat).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOTosourceOfFundCat.get(oldProcurement_goodsDTO.sourceOfFundCat).isEmpty()) {
//						mapOfProcurement_goodsDTOTosourceOfFundCat.remove(oldProcurement_goodsDTO.sourceOfFundCat);
//					}
//
//					if(mapOfProcurement_goodsDTOTotimeCode.containsKey(oldProcurement_goodsDTO.timeCode)) {
//						mapOfProcurement_goodsDTOTotimeCode.get(oldProcurement_goodsDTO.timeCode).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOTotimeCode.get(oldProcurement_goodsDTO.timeCode).isEmpty()) {
//						mapOfProcurement_goodsDTOTotimeCode.remove(oldProcurement_goodsDTO.timeCode);
//					}
//
//					if(mapOfProcurement_goodsDTOTotender.containsKey(oldProcurement_goodsDTO.tender)) {
//						mapOfProcurement_goodsDTOTotender.get(oldProcurement_goodsDTO.tender).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOTotender.get(oldProcurement_goodsDTO.tender).isEmpty()) {
//						mapOfProcurement_goodsDTOTotender.remove(oldProcurement_goodsDTO.tender);
//					}
//
//					if(mapOfProcurement_goodsDTOTotenderOpening.containsKey(oldProcurement_goodsDTO.tenderOpening)) {
//						mapOfProcurement_goodsDTOTotenderOpening.get(oldProcurement_goodsDTO.tenderOpening).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOTotenderOpening.get(oldProcurement_goodsDTO.tenderOpening).isEmpty()) {
//						mapOfProcurement_goodsDTOTotenderOpening.remove(oldProcurement_goodsDTO.tenderOpening);
//					}
//
//					if(mapOfProcurement_goodsDTOTotenderEvaluation.containsKey(oldProcurement_goodsDTO.tenderEvaluation)) {
//						mapOfProcurement_goodsDTOTotenderEvaluation.get(oldProcurement_goodsDTO.tenderEvaluation).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOTotenderEvaluation.get(oldProcurement_goodsDTO.tenderEvaluation).isEmpty()) {
//						mapOfProcurement_goodsDTOTotenderEvaluation.remove(oldProcurement_goodsDTO.tenderEvaluation);
//					}
//
//					if(mapOfProcurement_goodsDTOToapprovalToward.containsKey(oldProcurement_goodsDTO.approvalToward)) {
//						mapOfProcurement_goodsDTOToapprovalToward.get(oldProcurement_goodsDTO.approvalToward).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOToapprovalToward.get(oldProcurement_goodsDTO.approvalToward).isEmpty()) {
//						mapOfProcurement_goodsDTOToapprovalToward.remove(oldProcurement_goodsDTO.approvalToward);
//					}
//
//					if(mapOfProcurement_goodsDTOToawardNotification.containsKey(oldProcurement_goodsDTO.awardNotification)) {
//						mapOfProcurement_goodsDTOToawardNotification.get(oldProcurement_goodsDTO.awardNotification).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOToawardNotification.get(oldProcurement_goodsDTO.awardNotification).isEmpty()) {
//						mapOfProcurement_goodsDTOToawardNotification.remove(oldProcurement_goodsDTO.awardNotification);
//					}
//
//					if(mapOfProcurement_goodsDTOTosiginingOfContract.containsKey(oldProcurement_goodsDTO.siginingOfContract)) {
//						mapOfProcurement_goodsDTOTosiginingOfContract.get(oldProcurement_goodsDTO.siginingOfContract).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOTosiginingOfContract.get(oldProcurement_goodsDTO.siginingOfContract).isEmpty()) {
//						mapOfProcurement_goodsDTOTosiginingOfContract.remove(oldProcurement_goodsDTO.siginingOfContract);
//					}
//
//					if(mapOfProcurement_goodsDTOTocontractSignatureTime.containsKey(oldProcurement_goodsDTO.contractSignatureTime)) {
//						mapOfProcurement_goodsDTOTocontractSignatureTime.get(oldProcurement_goodsDTO.contractSignatureTime).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOTocontractSignatureTime.get(oldProcurement_goodsDTO.contractSignatureTime).isEmpty()) {
//						mapOfProcurement_goodsDTOTocontractSignatureTime.remove(oldProcurement_goodsDTO.contractSignatureTime);
//					}
//
//					if(mapOfProcurement_goodsDTOTocontractCompletionTime.containsKey(oldProcurement_goodsDTO.contractCompletionTime)) {
//						mapOfProcurement_goodsDTOTocontractCompletionTime.get(oldProcurement_goodsDTO.contractCompletionTime).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOTocontractCompletionTime.get(oldProcurement_goodsDTO.contractCompletionTime).isEmpty()) {
//						mapOfProcurement_goodsDTOTocontractCompletionTime.remove(oldProcurement_goodsDTO.contractCompletionTime);
//					}
//
//					if(mapOfProcurement_goodsDTOToinsertedByUserId.containsKey(oldProcurement_goodsDTO.insertedByUserId)) {
//						mapOfProcurement_goodsDTOToinsertedByUserId.get(oldProcurement_goodsDTO.insertedByUserId).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOToinsertedByUserId.get(oldProcurement_goodsDTO.insertedByUserId).isEmpty()) {
//						mapOfProcurement_goodsDTOToinsertedByUserId.remove(oldProcurement_goodsDTO.insertedByUserId);
//					}
//
//					if(mapOfProcurement_goodsDTOToinsertedByOrganogramId.containsKey(oldProcurement_goodsDTO.insertedByOrganogramId)) {
//						mapOfProcurement_goodsDTOToinsertedByOrganogramId.get(oldProcurement_goodsDTO.insertedByOrganogramId).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOToinsertedByOrganogramId.get(oldProcurement_goodsDTO.insertedByOrganogramId).isEmpty()) {
//						mapOfProcurement_goodsDTOToinsertedByOrganogramId.remove(oldProcurement_goodsDTO.insertedByOrganogramId);
//					}
//
//					if(mapOfProcurement_goodsDTOToinsertionDate.containsKey(oldProcurement_goodsDTO.insertionDate)) {
//						mapOfProcurement_goodsDTOToinsertionDate.get(oldProcurement_goodsDTO.insertionDate).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOToinsertionDate.get(oldProcurement_goodsDTO.insertionDate).isEmpty()) {
//						mapOfProcurement_goodsDTOToinsertionDate.remove(oldProcurement_goodsDTO.insertionDate);
//					}
//
//					if(mapOfProcurement_goodsDTOTolastModificationTime.containsKey(oldProcurement_goodsDTO.lastModificationTime)) {
//						mapOfProcurement_goodsDTOTolastModificationTime.get(oldProcurement_goodsDTO.lastModificationTime).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOTolastModificationTime.get(oldProcurement_goodsDTO.lastModificationTime).isEmpty()) {
//						mapOfProcurement_goodsDTOTolastModificationTime.remove(oldProcurement_goodsDTO.lastModificationTime);
//					}
//
//					if(mapOfProcurement_goodsDTOTosearchColumn.containsKey(oldProcurement_goodsDTO.searchColumn)) {
//						mapOfProcurement_goodsDTOTosearchColumn.get(oldProcurement_goodsDTO.searchColumn).remove(oldProcurement_goodsDTO);
//					}
//					if(mapOfProcurement_goodsDTOTosearchColumn.get(oldProcurement_goodsDTO.searchColumn).isEmpty()) {
//						mapOfProcurement_goodsDTOTosearchColumn.remove(oldProcurement_goodsDTO.searchColumn);
//					}
					
					
				}
				if(procurement_goodsDTO.isDeleted == 0) 
				{
					
					mapOfProcurement_goodsDTOToiD.put(procurement_goodsDTO.iD, procurement_goodsDTO);

 					if( ! mapOfProcurement_goodsDTOToprocurementYear.containsKey(procurement_goodsDTO.procurementYear)) {
						mapOfProcurement_goodsDTOToprocurementYear.put(procurement_goodsDTO.procurementYear, new HashSet<>());
					}
					mapOfProcurement_goodsDTOToprocurementYear.get(procurement_goodsDTO.procurementYear).add(procurement_goodsDTO);
					if( ! mapOfProcurement_goodsDTOToreportName.containsKey(procurement_goodsDTO.reportName)) {
						mapOfProcurement_goodsDTOToreportName.put(procurement_goodsDTO.reportName, new HashSet<>());
					}
					mapOfProcurement_goodsDTOToreportName.get(procurement_goodsDTO.reportName).add(procurement_goodsDTO);
					if( ! mapOfProcurement_goodsDTOToparentId.containsKey(procurement_goodsDTO.parentId)) {
						mapOfProcurement_goodsDTOToparentId.put(procurement_goodsDTO.parentId, new HashSet<>());
					}
					mapOfProcurement_goodsDTOToparentId.get(procurement_goodsDTO.parentId).add(procurement_goodsDTO);

					if( ! mapOfProcurement_goodsDTOToPackageId.containsKey(procurement_goodsDTO.procurementPackageId)) {
						mapOfProcurement_goodsDTOToPackageId.put(procurement_goodsDTO.procurementPackageId, new HashSet<>());
					}
					mapOfProcurement_goodsDTOToPackageId.get(procurement_goodsDTO.procurementPackageId).add(procurement_goodsDTO);
// 					if( ! mapOfProcurement_goodsDTOTonameEn.containsKey(procurement_goodsDTO.nameEn)) {
//						mapOfProcurement_goodsDTOTonameEn.put(procurement_goodsDTO.nameEn, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOTonameEn.get(procurement_goodsDTO.nameEn).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOTonameBn.containsKey(procurement_goodsDTO.nameBn)) {
//						mapOfProcurement_goodsDTOTonameBn.put(procurement_goodsDTO.nameBn, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOTonameBn.get(procurement_goodsDTO.nameBn).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOToprocurementGoodsTypeId.containsKey(procurement_goodsDTO.procurementGoodsTypeId)) {
//						mapOfProcurement_goodsDTOToprocurementGoodsTypeId.put(procurement_goodsDTO.procurementGoodsTypeId, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOToprocurementGoodsTypeId.get(procurement_goodsDTO.procurementGoodsTypeId).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOTounitPrice.containsKey(procurement_goodsDTO.unitPrice)) {
//						mapOfProcurement_goodsDTOTounitPrice.put(procurement_goodsDTO.unitPrice, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOTounitPrice.get(procurement_goodsDTO.unitPrice).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOToestCost.containsKey(procurement_goodsDTO.estCost)) {
//						mapOfProcurement_goodsDTOToestCost.put(procurement_goodsDTO.estCost, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOToestCost.get(procurement_goodsDTO.estCost).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOTomethodAndTypeCat.containsKey(procurement_goodsDTO.methodAndTypeCat)) {
//						mapOfProcurement_goodsDTOTomethodAndTypeCat.put(procurement_goodsDTO.methodAndTypeCat, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOTomethodAndTypeCat.get(procurement_goodsDTO.methodAndTypeCat).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOToapprovingAuthority.containsKey(procurement_goodsDTO.approvingAuthority)) {
//						mapOfProcurement_goodsDTOToapprovingAuthority.put(procurement_goodsDTO.approvingAuthority, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOToapprovingAuthority.get(procurement_goodsDTO.approvingAuthority).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOTosourceOfFundCat.containsKey(procurement_goodsDTO.sourceOfFundCat)) {
//						mapOfProcurement_goodsDTOTosourceOfFundCat.put(procurement_goodsDTO.sourceOfFundCat, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOTosourceOfFundCat.get(procurement_goodsDTO.sourceOfFundCat).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOTotimeCode.containsKey(procurement_goodsDTO.timeCode)) {
//						mapOfProcurement_goodsDTOTotimeCode.put(procurement_goodsDTO.timeCode, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOTotimeCode.get(procurement_goodsDTO.timeCode).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOTotender.containsKey(procurement_goodsDTO.tender)) {
//						mapOfProcurement_goodsDTOTotender.put(procurement_goodsDTO.tender, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOTotender.get(procurement_goodsDTO.tender).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOTotenderOpening.containsKey(procurement_goodsDTO.tenderOpening)) {
//						mapOfProcurement_goodsDTOTotenderOpening.put(procurement_goodsDTO.tenderOpening, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOTotenderOpening.get(procurement_goodsDTO.tenderOpening).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOTotenderEvaluation.containsKey(procurement_goodsDTO.tenderEvaluation)) {
//						mapOfProcurement_goodsDTOTotenderEvaluation.put(procurement_goodsDTO.tenderEvaluation, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOTotenderEvaluation.get(procurement_goodsDTO.tenderEvaluation).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOToapprovalToward.containsKey(procurement_goodsDTO.approvalToward)) {
//						mapOfProcurement_goodsDTOToapprovalToward.put(procurement_goodsDTO.approvalToward, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOToapprovalToward.get(procurement_goodsDTO.approvalToward).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOToawardNotification.containsKey(procurement_goodsDTO.awardNotification)) {
//						mapOfProcurement_goodsDTOToawardNotification.put(procurement_goodsDTO.awardNotification, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOToawardNotification.get(procurement_goodsDTO.awardNotification).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOTosiginingOfContract.containsKey(procurement_goodsDTO.siginingOfContract)) {
//						mapOfProcurement_goodsDTOTosiginingOfContract.put(procurement_goodsDTO.siginingOfContract, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOTosiginingOfContract.get(procurement_goodsDTO.siginingOfContract).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOTocontractSignatureTime.containsKey(procurement_goodsDTO.contractSignatureTime)) {
//						mapOfProcurement_goodsDTOTocontractSignatureTime.put(procurement_goodsDTO.contractSignatureTime, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOTocontractSignatureTime.get(procurement_goodsDTO.contractSignatureTime).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOTocontractCompletionTime.containsKey(procurement_goodsDTO.contractCompletionTime)) {
//						mapOfProcurement_goodsDTOTocontractCompletionTime.put(procurement_goodsDTO.contractCompletionTime, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOTocontractCompletionTime.get(procurement_goodsDTO.contractCompletionTime).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOToinsertedByUserId.containsKey(procurement_goodsDTO.insertedByUserId)) {
//						mapOfProcurement_goodsDTOToinsertedByUserId.put(procurement_goodsDTO.insertedByUserId, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOToinsertedByUserId.get(procurement_goodsDTO.insertedByUserId).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOToinsertedByOrganogramId.containsKey(procurement_goodsDTO.insertedByOrganogramId)) {
//						mapOfProcurement_goodsDTOToinsertedByOrganogramId.put(procurement_goodsDTO.insertedByOrganogramId, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOToinsertedByOrganogramId.get(procurement_goodsDTO.insertedByOrganogramId).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOToinsertionDate.containsKey(procurement_goodsDTO.insertionDate)) {
//						mapOfProcurement_goodsDTOToinsertionDate.put(procurement_goodsDTO.insertionDate, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOToinsertionDate.get(procurement_goodsDTO.insertionDate).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOTolastModificationTime.containsKey(procurement_goodsDTO.lastModificationTime)) {
//						mapOfProcurement_goodsDTOTolastModificationTime.put(procurement_goodsDTO.lastModificationTime, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOTolastModificationTime.get(procurement_goodsDTO.lastModificationTime).add(procurement_goodsDTO);
//
//					if( ! mapOfProcurement_goodsDTOTosearchColumn.containsKey(procurement_goodsDTO.searchColumn)) {
//						mapOfProcurement_goodsDTOTosearchColumn.put(procurement_goodsDTO.searchColumn, new HashSet<>());
//					}
//					mapOfProcurement_goodsDTOTosearchColumn.get(procurement_goodsDTO.searchColumn).add(procurement_goodsDTO);
//
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public Procurement_goodsDTO clone(Procurement_goodsDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Procurement_goodsDTO.class);
	}

	public List<Procurement_goodsDTO> clone(List<Procurement_goodsDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public Procurement_goodsDTO getProcurement_goodsDTOByIDWithoutClone( long ID){
		return mapOfProcurement_goodsDTOToiD.get(ID);
	}
	
	public List<Procurement_goodsDTO> getProcurement_goodsList() {
		List <Procurement_goodsDTO> procurement_goodss = new ArrayList<Procurement_goodsDTO>(this.mapOfProcurement_goodsDTOToiD.values());
		return procurement_goodss;
	}

	public Procurement_goodsDTO getProcurement_goodsDTOByID( long ID){
		return clone(mapOfProcurement_goodsDTOToiD.get(ID));
	}

	public List<Procurement_goodsDTO> getDTOsByIds(List<Long> ids){
		List<Procurement_goodsDTO> dtos = new ArrayList<>();
		ids.forEach(i -> {
			Procurement_goodsDTO dto = getProcurement_goodsDTOByID(i);
			if(dto != null){
				dtos.add(dto);
			}
		});
		return dtos;
	}

	public List<Procurement_goodsDTO> getDTOsLikeName(String name) {
		if (!StringUtils.isValidString(name)) return getProcurement_goodsList();
		return getProcurement_goodsList()
				.stream()
				.filter(dto ->
						dto.nameEn.contains(name) || dto.nameBn.contains(name))
				.collect(Collectors.toList());
	}

	public List<Procurement_goodsDTO> getProcurement_goodsDTOByprocurement_year(String name_en) {
		return new ArrayList<>( mapOfProcurement_goodsDTOToprocurementYear.getOrDefault(name_en,new HashSet<>()));
	}


 	public List<Procurement_goodsDTO> getProcurement_goodsDTOByreport_name(String name_en) {
		return new ArrayList<>( mapOfProcurement_goodsDTOToreportName.getOrDefault(name_en,new HashSet<>()));
	}

	public List<Procurement_goodsDTO> getProcurement_goodsDTOByparent_id(long parentId) {
		return new ArrayList<>( mapOfProcurement_goodsDTOToparentId.getOrDefault(parentId,new HashSet<>()));
	}

	public List<Procurement_goodsDTO> getProcurement_goodsDTOBypackage_id(long packageId) {
		return new ArrayList<>( mapOfProcurement_goodsDTOToPackageId.getOrDefault(packageId,new HashSet<>()));
	}


	public List<Procurement_goodsDTO> getProcurement_goodsDTOByreport_nameAndProcurement_year(String reportName, String procurementYear) {
		List<Procurement_goodsDTO> procurement_goodsDTOS = new ArrayList<>( mapOfProcurement_goodsDTOToprocurementYear.getOrDefault(procurementYear,new HashSet<>()));
		return procurement_goodsDTOS
				.stream()
				.filter(procurement_goodsDTO -> procurement_goodsDTO.reportName.equals(reportName))
				.collect(Collectors.toList());
	}
// 	public List<Procurement_goodsDTO> getProcurement_goodsDTOByname_en(String name_en) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOTonameEn.getOrDefault(name_en,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOByname_bn(String name_bn) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOTonameBn.getOrDefault(name_bn,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOByprocurement_goods_type_id(long procurement_goods_type_id) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOToprocurementGoodsTypeId.getOrDefault(procurement_goods_type_id,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOByunit_price(double unit_price) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOTounitPrice.getOrDefault(unit_price,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOByest_cost(double est_cost) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOToestCost.getOrDefault(est_cost,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOBymethod_and_type_cat(long method_and_type_cat) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOTomethodAndTypeCat.getOrDefault(method_and_type_cat,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOByapproving_authority(String approving_authority) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOToapprovingAuthority.getOrDefault(approving_authority,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOBysource_of_fund_cat(long source_of_fund_cat) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOTosourceOfFundCat.getOrDefault(source_of_fund_cat,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOBytime_code(String time_code) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOTotimeCode.getOrDefault(time_code,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOBytender(String tender) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOTotender.getOrDefault(tender,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOBytender_opening(String tender_opening) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOTotenderOpening.getOrDefault(tender_opening,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOBytender_evaluation(String tender_evaluation) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOTotenderEvaluation.getOrDefault(tender_evaluation,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOByapproval_toward(String approval_toward) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOToapprovalToward.getOrDefault(approval_toward,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOByaward_notification(String award_notification) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOToawardNotification.getOrDefault(award_notification,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOBysigining_of_contract(String sigining_of_contract) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOTosiginingOfContract.getOrDefault(sigining_of_contract,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOBycontract_signature_time(long contract_signature_time) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOTocontractSignatureTime.getOrDefault(contract_signature_time,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOBycontract_completion_time(long contract_completion_time) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOTocontractCompletionTime.getOrDefault(contract_completion_time,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOByinserted_by_user_id(long inserted_by_user_id) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOByinsertion_date(long insertion_date) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOBylastModificationTime(long lastModificationTime) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
//	}
//
//
//	public List<Procurement_goodsDTO> getProcurement_goodsDTOBysearch_column(String search_column) {
//		return new ArrayList<>( mapOfProcurement_goodsDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
//	}

	public String geText(String language, long id) {
		Procurement_goodsDTO dto = getProcurement_goodsDTOByID(id);
		return dto == null ? "" : (language.equalsIgnoreCase("English") ?
				(dto.nameEn == null ? "" : dto.nameEn)
				: (dto.nameBn == null ? "" : dto.nameBn));
	}

	public long getItemUnitId(String Language, long itemId){
		Procurement_goodsDTO dto = Procurement_goodsRepository.getInstance().getProcurement_goodsDTOByID(itemId);
		return dto == null ? -1 : dto.piUnitId;
	}

	public String getItemUnitIdText(String Language, long itemId){
		Procurement_goodsDTO dto = Procurement_goodsRepository.getInstance().getProcurement_goodsDTOByID(itemId);
		return dto == null ? "" : Pi_unitRepository.getInstance().getName(Language, dto.piUnitId);
	}

	public String buildOptions(String language, Long selectedId) {
		List<OptionDTO> optionDTOList = null;
		List<Procurement_goodsDTO> procurement_goodsDTOS = getProcurement_goodsList();
		if (procurement_goodsDTOS != null && procurement_goodsDTOS.size() > 0) {
			optionDTOList = procurement_goodsDTOS.stream()
					.map(Procurement_goodsDTO::getOptionDTO)
					.collect(Collectors.toList());
		}
		return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "procurement_goods";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}

	public String getName(String Language, long itemId){
		Procurement_goodsDTO dto = getProcurement_goodsDTOByID(itemId);
		return dto == null ? "" : UtilCharacter.getDataByLanguage(Language, dto.nameBn, dto.nameEn);
	}
}


