package procurement_goods;

import util.CommonDTO;


public class Procurement_goodsResponseDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
    public String procurementYear = "";
    public String reportName = "";
	public long procurementPackageId = -1;
	public String procurementPackageNameEn = "";
	public String procurementPackageNameBn = "";
	public long procurementGoodsTypeId = -1;
	public String procurementGoodsTypeNameEn = "";
	public String procurementGoodsTypeNameBn = "";
	public double unitPrice = 0;
	public double estCost = 0;
	public int methodAndTypeCat = -1;
	public long quantity = 0;
	public long parentId = -1;
	public long piUnitId = -1;
	public String piUnitNameEn = "";
	public String piUnitNameBn = "";
    public String approvingAuthority = "";
	public int sourceOfFundCat = -1;
    public String timeCode = "";
    public String tender = "";
    public String description = "";
    public String tenderOpening = "";
    public String tenderEvaluation = "";
    public String approvalToward = "";
    public String awardNotification = "";
    public String siginingOfContract = "";
	public long contractSignatureTime = -1;
	public long contractCompletionTime = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	public String circularData = "";

	
    @Override
	public String toString() {
            return "$Procurement_goodsDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " procurementYear = " + procurementYear +
            " reportName = " + reportName +
            " procurementGoodsTypeId = " + procurementGoodsTypeId +
            " unitPrice = " + unitPrice +
            " estCost = " + estCost +
            " methodAndTypeCat = " + methodAndTypeCat +
            " quantity = " + quantity +
            " approvingAuthority = " + approvingAuthority +
            " sourceOfFundCat = " + sourceOfFundCat +
            " timeCode = " + timeCode +
            " tender = " + tender +
            " tenderOpening = " + tenderOpening +
            " tenderEvaluation = " + tenderEvaluation +
            " approvalToward = " + approvalToward +
            " awardNotification = " + awardNotification +
            " siginingOfContract = " + siginingOfContract +
            " contractSignatureTime = " + contractSignatureTime +
            " contractCompletionTime = " + contractCompletionTime +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            "]";
    }

}