package username_migrate;

/*
 * @author Md. Erfan Hossain
 * @created 26/07/2022 - 3:11 PM
 * @project parliament
 */

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import util.StringUtils;

import java.sql.SQLException;
import java.util.List;
import java.util.function.BiFunction;


public class UserNameMigration {
    static Logger logger = Logger.getLogger(UserNameMigration.class);
    public static void main(String[] args) {
        updateUsernameForMP();
        updateUsernameForEmployee();
        updateUsernameInElectionWiseMP();
        updateUsernameInUsers();
    }

    private static void updateUsernameInElectionWiseMP() {
        String sql = "SELECT canceled_username FROM election_wise_mp";
        List<String> list = ConnectionAndStatementUtil.getListOfT(sql, rs -> {
            try {
                return rs.getString("canceled_username");
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        });
        String updateSql = "UPDATE election_wise_mp SET username = '%s' WHERE canceled_username = '%s'";
        updateMpUserName(list, (newUserName, oldUserName) -> String.format(updateSql, newUserName, oldUserName));
        logger.debug("ELECTION WISE MP SIZE : " + list.size());
    }

    private static void updateMpUserName(List<String> list, BiFunction<String, String, String> biFunction) {
        list.forEach(e -> {
            int seatNo = Integer.parseInt(e.substring(3, 6));
            int mpCount = Integer.parseInt(e.substring(6));
            String newUsername = e.substring(0, 3) + String.format("%04d", seatNo) + String.format("%02d", mpCount);
            String sql = biFunction.apply(newUsername, e);
            executeUpdate(sql);
        });
    }

    private static void updateUsernameForMP() {
        String sql = "SELECT canceled_employee_number FROM employee_records WHERE is_mp = 1";
        List<String> list = ConnectionAndStatementUtil.getListOfT(sql, rs -> {
            try {
                return rs.getString("canceled_employee_number");
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        });
        String updateSql = "UPDATE employee_records SET employee_number = '%s',employee_number_bng = '%s',updated = 1 " +
                "WHERE canceled_employee_number = '%s'";
        updateMpUserName(list, (newUsername, oldUsername) -> {
            String bngUsername = StringUtils.convertToBanNumber(newUsername);
            return String.format(updateSql, newUsername, bngUsername, oldUsername);
        });

        logger.debug("MP SIZE FROM EMPLOYEE RECORDS : " + list.size());
    }

    private static void executeUpdate(String sql) {
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            logger.debug(sql);
            try {
                st.executeUpdate(sql);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
    }

    private static class Emp {
        int clazz;
        int employment;
        int empOfficer;
        String username;
    }

    private static void updateUsernameForEmployee() {
        String sql = "SELECT employee_class_cat,employment_cat,emp_officer_cat,canceled_employee_number " +
                "FROM employee_records WHERE is_mp = 2 AND employee_class_cat!=0";
        List<Emp> list = ConnectionAndStatementUtil.getListOfT(sql, rs -> {
            try {
                Emp emp = new Emp();
                emp.clazz = rs.getInt("employee_class_cat");
                emp.employment = rs.getInt("employment_cat");
                emp.empOfficer = rs.getInt("emp_officer_cat");
                emp.username = rs.getString("canceled_employee_number");
                return emp;
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        });

        String updateSql = "UPDATE employee_records SET employee_number = '%s',employee_number_bng = '%s',updated = 1 " +
                " WHERE canceled_employee_number = '%s' AND employee_class_cat = %d AND employment_cat = %d AND emp_officer_cat = %d";

        list.forEach(emp -> {
            int seq = Integer.parseInt(emp.username.substring(3));
            String newUsername = emp.clazz +
                    String.valueOf(emp.employment) +
                    String.format("%02d", emp.empOfficer) +
                    String.format("%05d", seq);
            String newUsernameBn = StringUtils.convertToBanNumber(newUsername);
            String sql2 = String.format(updateSql, newUsername, newUsernameBn, emp.username, emp.clazz, emp.employment, emp.empOfficer);
            executeUpdate(sql2);
        });

        logger.debug("EMPLOYEE SIZE FROM EMPLOYEE RECORDS : " + list.size());
    }

    private static class UserName {
        String old;
        String newUserName;
    }

    private static void updateUsernameInUsers() {
        String sql = "SELECT u.canceled_username,er.employee_number FROM users u INNER JOIN employee_records er ON u.canceled_username = er.canceled_employee_number WHERE er.updated = 1";
        List<UserName> list = ConnectionAndStatementUtil.getListOfT(sql, rs -> {
            try {
                UserName un = new UserName();
                un.old = rs.getString("u.canceled_username");
                un.newUserName = rs.getString("er.employee_number");
                return un;
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        });

        String updateSQL = "UPDATE users SET username = '%s' WHERE canceled_username = '%s'";

        list.forEach(e -> {
            String sql2 = String.format(updateSQL, e.newUserName, e.old);
            executeUpdate(sql2);
        });

        logger.debug("USER NAME SIZE FROM USERS : " + list.size());
    }
}
