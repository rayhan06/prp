package username_migrate;

/*
 * @author Md. Erfan Hossain
 * @created 01/08/2022 - 12:07 AM
 * @project parliament
 */

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import sms.SmsService;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SendSms {
    static String sms = "জনাব %s,\nআপনার পিআরপি (https://prp.parliament.gov.bd) সফটওয়্যারের নতুন ইউজার নাম: %s\nপাসওয়ার্ডঃ 12345678\nধন্যবাদ,\nবিএন্ডআইটি অনুবিভাগ ।";
    static Logger logger = Logger.getLogger(SendSms.class);
    static List<Long> idList = Arrays.asList(3212611L);

    private static class EmpInfo {
        String nameEn;
        String nameBn;
        String userName;
        String mobile;
        long id;


        @Override
        public String toString() {
            return "EmpInfo{" +
                    "nameEn='" + nameEn + '\'' +
                    ", nameBn='" + nameBn + '\'' +
                    ", userName='" + userName + '\'' +
                    ", mobile='" + mobile + '\'' +
                    ", id=" + id +
                    '}';
        }
    }

    public static void main(String[] args) throws IOException {
        String sql = "select name_eng,name_bng,employee_number,personal_mobile,id from employee_records" +
                " where id in (%s)";
        String ids = idList.stream()
                .map(String::valueOf)
                .collect(Collectors.joining(","));
        String sql2 = String.format(sql, ids);
        List<EmpInfo> list = ConnectionAndStatementUtil.getListOfT(sql2, rs -> {
            EmpInfo e = new EmpInfo();
            try {
                e.nameEn = rs.getString("name_eng");
                e.nameBn = rs.getString("name_bng");
                e.userName = rs.getString("employee_number");
                e.mobile = rs.getString("personal_mobile");
                e.id = rs.getLong("id");
                return e;
            } catch (SQLException ex) {
                logger.error("--------- " + ex.getMessage() + " ------------");
                return null;
            }
        });

        list.forEach(emp -> {
            logger.debug(emp);
            String msg = String.format(sms, emp.nameBn, emp.userName);
            logger.debug(msg);
            try {
                SmsService.send(emp.mobile, msg);
            } catch (IOException e) {
                logger.error("--------- " + e.getMessage() + " ------------");
            }
        });

        /*String str = "UPDATE employee_records SET send_sms = 1 WHERE ID IN (%s)";
        String ids = idList.stream()
                .map(String::valueOf)
                .collect(Collectors.joining(","));
        String updateSQL = String.format(str,ids);
        logger.debug(updateSQL);
        ConnectionAndStatementUtil.getWriteStatement(st->{
            try {
                st.executeUpdate(updateSQL);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });*/
    }
}
