package disciplinary_log;

import com.google.gson.Gson;
import common.BaseServlet;
import common.CommonDAOService;
import common.RoleEnum;
import disciplinary_action.Disciplinary_actionDAO;
import disciplinary_action.Disciplinary_actionDTO;
import disciplinary_details.Disciplinary_detailsDAO;
import disciplinary_details.Disciplinary_detailsDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.EmpOfficeModel;
import login.LoginDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


@WebServlet("/Disciplinary_logServlet")
@MultipartConfig
public class Disciplinary_logServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Disciplinary_logServlet.class);
    private final String tableName = "disciplinary_log";
    private final Disciplinary_logDAO disciplinary_logDAO = Disciplinary_logDAO.getInstance();
    private final Gson gson = new Gson();

    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    public String getServletName() {
        return "Disciplinary_logServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return disciplinary_logDAO;
    }

    public String getTableJoinClause(HttpServletRequest request) {
        StringBuilder joinClause = new StringBuilder();
        Long organogramId = (Long) request.getAttribute("internal_organogram");
        if (organogramId != null) {
            OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(organogramId);
            if (officeUnitOrganograms.orgTree.endsWith("$")) {
                joinClause.append(" INNER JOIN disciplinary_details on disciplinary_details.disciplinary_log_id = disciplinary_log.incident_number");
            } else {
                Set<Long> organogramIds = OfficeUnitOrganogramsRepository.getInstance().getDescentsOrganogram(organogramId);
                if (organogramIds.size() > 0) {
                    Set<Long> empIds = EmployeeOfficeRepository.getInstance().getEmpIdByOrganogramId(organogramIds);
                    if (empIds.size() > 0) {
                        joinClause.append(" INNER JOIN disciplinary_details on disciplinary_details.disciplinary_log_id = disciplinary_log.incident_number");
                    }
                }
            }
        }
        if (request.getParameter("complain_by") != null && !(request.getParameter("complain_by")).trim().equals("")) {
            joinClause.append(" INNER JOIN disciplinary_log_complain_by ON disciplinary_log_complain_by.incident_number = disciplinary_log.incident_number");
        }
        if (request.getParameter("complain_for") != null && !(request.getParameter("complain_for")).trim().equals("")) {
            joinClause.append(" INNER JOIN disciplinary_details on disciplinary_details.disciplinary_log_id = disciplinary_log.incident_number");
        }
        return joinClause.toString();
    }

    public String getWhereClause(HttpServletRequest request) {
        StringBuilder whereClause = new StringBuilder();
        if (request.getParameter("complain_by") != null && !(request.getParameter("complain_by")).trim().equals("")) {
            whereClause.append(" and (disciplinary_log_complain_by.isDeleted = 0 ) ");
        }
        if (request.getParameter("complain_for") != null && !(request.getParameter("complain_for")).trim().equals("")) {
            whereClause.append(" and (disciplinary_details.isDeleted = 0 )");
        }
        return whereClause.toString();
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Disciplinary_logDTO disciplinary_logDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        long currentLongTime = System.currentTimeMillis();

        if (addFlag) {
            disciplinary_logDTO = new Disciplinary_logDTO();
            disciplinary_logDTO.insertionDate = currentLongTime;
            disciplinary_logDTO.insertedBy = String.valueOf(userDTO.ID);
            disciplinary_logDTO.complaintStatusCat = 1;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            disciplinary_logDTO.newGenerateIncidentNumber = "dl_" + sdf.format(new Date());
        } else {
            disciplinary_logDTO = (Disciplinary_logDTO) disciplinary_logDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
        }
        disciplinary_logDTO.modifiedBy = String.valueOf(userDTO.ID);
        disciplinary_logDTO.lastModificationTime = currentLongTime;
        disciplinary_logDTO.complaintSourceCat = Integer.parseInt(request.getParameter("complaintSourceCat").trim());
        disciplinary_logDTO.incidentType = Long.parseLong(Jsoup.clean(request.getParameter("incidentType"), Whitelist.simpleText()));
        String Value = request.getParameter("reportedDate");
        Value = Jsoup.clean(Value, Whitelist.simpleText());
        Date d = f.parse(Value);
        disciplinary_logDTO.reportedDate = d.getTime();

        Value = request.getParameter("incidentTypeOther");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        logger.debug("incidentTypeOther = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            disciplinary_logDTO.incidentTypeOther = Value;
        }
        Value = request.getParameter("incidentSummary");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        logger.debug("incidentSummary = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            disciplinary_logDTO.incidentSummary = Value;
        }
        Value = request.getParameter("complainedByExternal");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        logger.debug("complainedBy = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            disciplinary_logDTO.complainedByExternal = Value;
        }
        Value = request.getParameter("filesDropzone");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        logger.debug("filesDropzone = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {

            logger.debug("filesDropzone = " + Value);
            if (!Value.equalsIgnoreCase("")) {
                disciplinary_logDTO.filesDropzone = Long.parseLong(Value);
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

            if (!addFlag) {
                String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        } else {
            logger.debug("FieldName has a null Value, not updating" + " = " + Value);
        }
        logger.debug("Done adding  addDisciplinary_log dto = " + disciplinary_logDTO);
        Value = request.getParameter("addedEmployees");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        List<EmpOfficeModel> accusedEmployees = Arrays.asList(gson.fromJson(Value, EmpOfficeModel[].class));

        Value = request.getParameter("complainedByInternal");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        List<EmpOfficeModel> complainedByList = Arrays.asList(gson.fromJson(Value, EmpOfficeModel[].class));
        if (addFlag) {
            disciplinary_logDAO.add(disciplinary_logDTO);
            new Disciplinary_detailsDAO().insertAfterDisciplinaryLog(disciplinary_logDTO.incidentNumber, accusedEmployees, disciplinary_logDTO.insertedBy, disciplinary_logDTO.insertionDate);
            new Disciplinary_logComplainByDAO().insertComplainedByList(disciplinary_logDTO.incidentNumber,
                    complainedByList, disciplinary_logDTO.insertedBy, disciplinary_logDTO.insertionDate);
        } else {
            disciplinary_logDAO.update(disciplinary_logDTO);
            Disciplinary_detailsDAO disciplinary_detailsDAO = new Disciplinary_detailsDAO();
            Map<Boolean, List<EmpOfficeModel>> alreadyAccusedMap = disciplinary_detailsDAO.deleteDeletedInfoAndReturnAMap(
                    accusedEmployees, disciplinary_logDTO.incidentNumber, disciplinary_logDTO.modifiedBy,
                    disciplinary_logDTO.lastModificationTime);
            disciplinary_detailsDAO.insertAfterDisciplinaryLog(disciplinary_logDTO.incidentNumber,
                    alreadyAccusedMap.get(false), disciplinary_logDTO.modifiedBy,
                    disciplinary_logDTO.lastModificationTime);

            Disciplinary_logComplainByDAO logComplainByDAO = new Disciplinary_logComplainByDAO();

            Map<Boolean, List<EmpOfficeModel>> alreadyAddedMap = logComplainByDAO.deleteDeletedInfoAndReturnAMap(
                    complainedByList, disciplinary_logDTO.incidentNumber,
                    disciplinary_logDTO.modifiedBy, disciplinary_logDTO.lastModificationTime
            );

            logComplainByDAO.insertComplainedByList(
                    disciplinary_logDTO.incidentNumber, alreadyAddedMap.get(false),
                    disciplinary_logDTO.modifiedBy, disciplinary_logDTO.lastModificationTime
            );
        }
        return disciplinary_logDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.DISCIPLINARY_LOG_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.DISCIPLINARY_LOG_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.DISCIPLINARY_LOG_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Disciplinary_logServlet.class;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getAddEmployeeModal":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID,
                            MenuConstants.DISCIPLINARY_LOG_ADD)) {
                        request.getRequestDispatcher("disciplinary_log/disciplinary_logAddModalBody.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "search":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID,
                            MenuConstants.DISCIPLINARY_LOG_SEARCH)) {
                        if (userDTO.roleID == RoleEnum.EMPLOYEE_OF_PARLIAMENT.getRoleId()) {
                            request.setAttribute("internal_organogram", userDTO.organogramID);
                            Map<String, String> extraCriteriaMap;
                            if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) == null) {
                                extraCriteriaMap = new HashMap<>();
                            } else {
                                extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
                            }
                            long organogramId = userDTO.organogramID;
                            OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(organogramId);
                            if (officeUnitOrganograms.orgTree.endsWith("$")) {
                                extraCriteriaMap.put("complain_for_internal", String.valueOf(userDTO.employee_record_id));
                            } else {
                                Set<Long> organogramIds = OfficeUnitOrganogramsRepository.getInstance().getDescentsOrganogram(organogramId);
                                if (organogramIds.size() > 0) {
                                    Set<Long> empIds = EmployeeOfficeRepository.getInstance().getEmpIdByOrganogramId(organogramIds);
                                    if (empIds.size() > 0) {
                                        String ids = empIds.stream()
                                                .map(String::valueOf)
                                                .collect(Collectors.joining(","));
                                        extraCriteriaMap.put("complain_for_internal", ids);
                                    } else if (request.getParameter("complain_for") != null && !(request.getParameter("complain_for")).trim().equals("")) {
                                        extraCriteriaMap.put("complain_for", "-1");
                                    }
                                } else if (request.getParameter("complain_for") != null && !(request.getParameter("complain_for")).trim().equals("")) {
                                    extraCriteriaMap.put("complain_for", "-1");
                                }
                            }
                            request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                        }
                        super.doGet(request, response);
                        return;
                    }
                    break;
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "delete":
                    deleteDisciplinary_log(request, response, userDTO);
                    return;
                default:
                    super.doPost(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }


    private void deleteDisciplinary_log(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception {

        String[] IDsToDelete = request.getParameterValues("ID");
        Disciplinary_detailsDAO disciplinary_detailsDAO = new Disciplinary_detailsDAO();
        Disciplinary_actionDAO disciplinary_actionDAO = Disciplinary_actionDAO.getInstance();
        for (String s : IDsToDelete) {
            long id = Long.parseLong(s);
            Disciplinary_logDTO disciplinary_logDTO = (Disciplinary_logDTO) disciplinary_logDAO.getDTOByID(id);
            List<Disciplinary_detailsDTO> disciplinary_detailsDTOList = disciplinary_detailsDAO.getByForeignKey(disciplinary_logDTO.incidentNumber);
            List<Disciplinary_logComplainByDTO> logComplainByDTOS = new Disciplinary_logComplainByDAO().getByForeignKey(disciplinary_logDTO.incidentNumber);
            List<Long> toBeDeletedIds = disciplinary_detailsDTOList.stream()
                    .map(dto -> dto.iD)
                    .collect(Collectors.toList());
            long curtime = Calendar.getInstance().getTimeInMillis();
            disciplinary_detailsDAO.deleteByIds(toBeDeletedIds, userDTO.userName, curtime);
            Disciplinary_actionDTO disciplinary_actionDTO = disciplinary_actionDAO.getDTObyIncidentNumber(disciplinary_logDTO.incidentNumber);
            if (disciplinary_actionDTO != null) {
                disciplinary_actionDAO.delete(userDTO.ID, disciplinary_actionDTO.iD);
            }
            ///DELETE FROM COMPLAIN BY TABLE////////
            toBeDeletedIds = logComplainByDTOS.stream()
                    .map((dto -> dto.iD))
                    .collect(Collectors.toList());
            new Disciplinary_logComplainByDAO().deleteByIds(toBeDeletedIds, userDTO.userName, curtime);
            disciplinary_logDAO.delete(userDTO.ID, id, curtime);
        }
        response.sendRedirect("Disciplinary_logServlet?actionType=search");

    }
}
