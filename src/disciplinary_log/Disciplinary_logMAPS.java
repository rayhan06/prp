package disciplinary_log;
import java.util.*; 
import util.*;


public class Disciplinary_logMAPS extends CommonMaps
{	
	public Disciplinary_logMAPS(String tableName)
	{
		
		java_allfield_type_map.put("complaint_source_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("reported_date".toLowerCase(), "Long");
		java_allfield_type_map.put("incident_type".toLowerCase(), "Long");
		java_allfield_type_map.put("complained_by".toLowerCase(), "String");
		java_allfield_type_map.put("complaint_status_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("complaint_resolved_date".toLowerCase(), "Long");

		java_anyfield_search_map.put("language_text.languageTextEnglish", "String");
		java_anyfield_search_map.put("language_text.languageTextBangla", "String");

		java_anyfield_search_map.put(tableName + ".reported_date".toLowerCase(), "Long");
		java_anyfield_search_map.put("incident.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("incident.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".incident_type_other".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".complained_by".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".complaint_resolved_date".toLowerCase(), "Long");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("incidentNumber".toLowerCase(), "incidentNumber".toLowerCase());
		java_DTO_map.put("complaintSourceCat".toLowerCase(), "complaintSourceCat".toLowerCase());
		java_DTO_map.put("reportedDate".toLowerCase(), "reportedDate".toLowerCase());
		java_DTO_map.put("incidentType".toLowerCase(), "incidentType".toLowerCase());
		java_DTO_map.put("incidentTypeOther".toLowerCase(), "incidentTypeOther".toLowerCase());
		java_DTO_map.put("incidentSummary".toLowerCase(), "incidentSummary".toLowerCase());
		java_DTO_map.put("complainedBy".toLowerCase(), "complainedBy".toLowerCase());
		java_DTO_map.put("worksDone".toLowerCase(), "worksDone".toLowerCase());
		java_DTO_map.put("findings".toLowerCase(), "findings".toLowerCase());
		java_DTO_map.put("followUpAction".toLowerCase(), "followUpAction".toLowerCase());
		java_DTO_map.put("complaintStatusCat".toLowerCase(), "complaintStatusCat".toLowerCase());
		java_DTO_map.put("complaintResolvedDate".toLowerCase(), "complaintResolvedDate".toLowerCase());
		java_DTO_map.put("handlingDuration".toLowerCase(), "handlingDuration".toLowerCase());
		java_DTO_map.put("filesDropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("incident_number".toLowerCase(), "incidentNumber".toLowerCase());
		java_SQL_map.put("complaint_source_cat".toLowerCase(), "complaintSourceCat".toLowerCase());
		java_SQL_map.put("reported_date".toLowerCase(), "reportedDate".toLowerCase());
		java_SQL_map.put("incident_type".toLowerCase(), "incidentType".toLowerCase());
		java_SQL_map.put("incident_type_other".toLowerCase(), "incidentTypeOther".toLowerCase());
		java_SQL_map.put("incident_summary".toLowerCase(), "incidentSummary".toLowerCase());
		java_SQL_map.put("complained_by".toLowerCase(), "complainedBy".toLowerCase());
		java_SQL_map.put("works_done".toLowerCase(), "worksDone".toLowerCase());
		java_SQL_map.put("findings".toLowerCase(), "findings".toLowerCase());
		java_SQL_map.put("follow_up_action".toLowerCase(), "followUpAction".toLowerCase());
		java_SQL_map.put("complaint_status_cat".toLowerCase(), "complaintStatusCat".toLowerCase());
		java_SQL_map.put("complaint_resolved_date".toLowerCase(), "complaintResolvedDate".toLowerCase());
		java_SQL_map.put("handling_duration".toLowerCase(), "handlingDuration".toLowerCase());
		java_SQL_map.put("files_dropzone".toLowerCase(), "filesDropzone".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Incident Number".toLowerCase(), "incidentNumber".toLowerCase());
		java_Text_map.put("Complaint Source Cat".toLowerCase(), "complaintSourceCat".toLowerCase());
		java_Text_map.put("Reported Date".toLowerCase(), "reportedDate".toLowerCase());
		java_Text_map.put("Incident".toLowerCase(), "incidentType".toLowerCase());
		java_Text_map.put("Incident Type Other".toLowerCase(), "incidentTypeOther".toLowerCase());
		java_Text_map.put("Incident Summary".toLowerCase(), "incidentSummary".toLowerCase());
		java_Text_map.put("Complained By".toLowerCase(), "complainedBy".toLowerCase());
		java_Text_map.put("Works Done".toLowerCase(), "worksDone".toLowerCase());
		java_Text_map.put("Findings".toLowerCase(), "findings".toLowerCase());
		java_Text_map.put("Follow Up Action".toLowerCase(), "followUpAction".toLowerCase());
		java_Text_map.put("Complaint Status Cat".toLowerCase(), "complaintStatusCat".toLowerCase());
		java_Text_map.put("Complaint Resolved Date".toLowerCase(), "complaintResolvedDate".toLowerCase());
		java_Text_map.put("Handling Duration".toLowerCase(), "handlingDuration".toLowerCase());
		java_Text_map.put("Files Dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}