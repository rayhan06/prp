package disciplinary_log;

/*
 * @author Md. Erfan Hossain
 * @created 19/03/2021 - 12:57 AM
 * @project parliament
 */

public class DisciplinaryLogDTODetails extends Disciplinary_logDTO{
    public long employeeRecordId;
    public String complainByNameEn;
    public String complainByNameBn;
    public String complainByOfficeEn;
    public String complainByOfficeBn;
    public String complainByDesignationEn;
    public String complainByDesignationBn;

    public String complainForNameEn;
    public String complainForNameBn;
    public String complainForOfficeEn;
    public String complainForOfficeBn;
    public String complainForDesignationEn;
    public String complainForDesignationBn;

    @Override
    public String toString() {
        return "DisciplinaryLogDTODetails{" +
                "employeeRecordId=" + employeeRecordId +
                ", complainByNameEn='" + complainByNameEn + '\'' +
                ", complainByNameBn='" + complainByNameBn + '\'' +
                ", complainByOfficeEn='" + complainByOfficeEn + '\'' +
                ", complainByOfficeBn='" + complainByOfficeBn + '\'' +
                ", complainByDesignationEn='" + complainByDesignationEn + '\'' +
                ", complainByDesignationBn='" + complainByDesignationBn + '\'' +
                ", complainForNameEn='" + complainForNameEn + '\'' +
                ", complainForNameBn='" + complainForNameBn + '\'' +
                ", complainForOfficeEn='" + complainForOfficeEn + '\'' +
                ", complainForOfficeBn='" + complainForOfficeBn + '\'' +
                ", complainForDesignationEn='" + complainForDesignationEn + '\'' +
                ", complainForDesignationBn='" + complainForDesignationBn + '\'' +
                '}';
    }
}
