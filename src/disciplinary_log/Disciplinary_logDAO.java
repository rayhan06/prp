package disciplinary_log;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import common.NameDTO;
import incident.IncidentRepository;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import sessionmanager.SessionConstants;
import util.CommonDTO;
import util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@SuppressWarnings({"rawtypes", "unused", "unchecked"})
public class Disciplinary_logDAO implements CommonDAOService<Disciplinary_logDTO> {

    private static final Logger logger = Logger.getLogger(Disciplinary_logDAO.class);

    private static final String addSqlQuery = "INSERT INTO {tableName} (incident_number,complaint_source_cat,reported_date,incident_type,incident_type_other,"
            .concat(" incident_summary,complained_by_external,complaint_status_cat,complaint_resolved_date,handling_duration,search_column,")
            .concat(" files_dropzone,modified_by,lastModificationTime,insertion_date,inserted_by,isDeleted,ID)")
            .concat(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery = "UPDATE {tableName} SET incident_number=?, complaint_source_cat=?, reported_date=?, incident_type=?,incident_type_other=?,"
            .concat(" incident_summary=?,complained_by_external=?,complaint_status_cat=?,complaint_resolved_date=?,handling_duration=?,search_column=?,")
            .concat(" files_dropzone=?,modified_by=?,lastModificationTime =? WHERE ID = ?");

    private static final String sqlGetByIncidentNumbers = "SELECT * FROM disciplinary_log where incident_number IN ({incident_numbers}) AND isDeleted = 0 ORDER BY lastModificationTime DESC";

    //private static final String updateComplainStatus = "UPDATE disciplinary_log SET complaint_status_cat = %d, modified_by = '%s', lastModificationTime = %d WHERE incident_number = '%s'";

    private static final String getByIncidentNumber = "SELECT * FROM disciplinary_log where incident_number = ? AND isDeleted = 0";

    private static final Map<String, String> searchMap = new HashMap<>();


    private static class LazyLoader {
        static final Disciplinary_logDAO INSTANCE = new Disciplinary_logDAO();
    }

    public static Disciplinary_logDAO getInstance() {
        return Disciplinary_logDAO.LazyLoader.INSTANCE;
    }

    private Disciplinary_logDAO() {
        searchMap.put("complaint_source_cat", "and (disciplinary_log.complaint_source_cat = ?)");
        searchMap.put("incident_type", "and (disciplinary_log.incident_type = ?)");
        searchMap.put("complaint_status_cat", "and (disciplinary_log.complaint_status_cat = ?)");
        searchMap.put("reported_date_start", "and (disciplinary_log.reported_date >= ?)");
        searchMap.put("reported_date_end", "and (disciplinary_log.reported_date <= ?)");
        searchMap.put("AnyField", " and (disciplinary_log.search_column like ?)");
        searchMap.put("complain_by", " and (disciplinary_log_complain_by.employee_records_id = ?)");
        searchMap.put("complain_for", " and (disciplinary_details.employee_records_id = ?)");
        searchMap.put("complain_for_internal", " and (disciplinary_details.employee_records_id IN ?)");
    }

    public void setSearchColumn(Disciplinary_logDTO dto) {
        StringBuilder column = new StringBuilder();
        CategoryLanguageModel sourceType = CatRepository.getInstance().getCategoryLanguageModel("complaint_source", dto.complaintSourceCat);
        if (sourceType != null) {
            column.append(sourceType.englishText).append(" ");
            column.append(sourceType.banglaText).append(" ");
        }
        IncidentRepository repository = IncidentRepository.getInstance();
        column.append(repository.getText("ENGLISH", dto.incidentType)).append(" ");
        column.append(repository.getText("BANGLA", dto.incidentType)).append(" ");
        CategoryLanguageModel statusType = CatRepository.getInstance().getCategoryLanguageModel("complaint_status", dto.complaintStatusCat);
        if (statusType != null) {
            column.append(statusType.englishText).append(" ");
            column.append(statusType.banglaText).append(" ");
        }
        column.append(dto.complainedByExternal).append(" ");
        dto.searchColumn = column.toString();
    }

    public void set(PreparedStatement ps, Disciplinary_logDTO disciplinary_logDTO, boolean isInsert) throws SQLException {
        if (isInsert) {
            disciplinary_logDTO.incidentNumber = disciplinary_logDTO.newGenerateIncidentNumber + "_" + disciplinary_logDTO.iD;
        }
        int index = 0;
        setSearchColumn(disciplinary_logDTO);
        ps.setObject(++index, disciplinary_logDTO.incidentNumber);
        ps.setObject(++index, disciplinary_logDTO.complaintSourceCat);
        ps.setObject(++index, disciplinary_logDTO.reportedDate);
        ps.setObject(++index, disciplinary_logDTO.incidentType);
        ps.setObject(++index, disciplinary_logDTO.incidentTypeOther);
        ps.setObject(++index, disciplinary_logDTO.incidentSummary);
        ps.setObject(++index, disciplinary_logDTO.complainedByExternal);
        ps.setObject(++index, disciplinary_logDTO.complaintStatusCat);
        ps.setObject(++index, disciplinary_logDTO.complaintResolvedDate);
        ps.setObject(++index, disciplinary_logDTO.handlingDuration);
        ps.setObject(++index, disciplinary_logDTO.searchColumn);
        ps.setObject(++index, disciplinary_logDTO.filesDropzone);
        ps.setObject(++index, disciplinary_logDTO.modifiedBy);
        ps.setObject(++index, System.currentTimeMillis());
        if (isInsert) {
            ps.setObject(++index, disciplinary_logDTO.insertionDate);
            ps.setObject(++index, disciplinary_logDTO.insertedBy);
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, disciplinary_logDTO.iD);
    }

    @Override
    public Disciplinary_logDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Disciplinary_logDTO disciplinary_logDTO = new Disciplinary_logDTO();
            setDisciplinaryLogDTOFromResultSet(rs, disciplinary_logDTO);
            return disciplinary_logDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "disciplinary_log";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    private void setDisciplinaryLogDTOFromResultSet(ResultSet rs, Disciplinary_logDTO disciplinary_logDTO) throws SQLException {
        disciplinary_logDTO.iD = rs.getLong("disciplinary_log.ID");
        disciplinary_logDTO.incidentNumber = rs.getString("disciplinary_log.incident_number");
        disciplinary_logDTO.complaintSourceCat = rs.getInt("disciplinary_log.complaint_source_cat");
        disciplinary_logDTO.reportedDate = rs.getLong("disciplinary_log.reported_date");
        disciplinary_logDTO.incidentType = rs.getLong("disciplinary_log.incident_type");
        disciplinary_logDTO.incidentTypeOther = rs.getString("disciplinary_log.incident_type_other");
        disciplinary_logDTO.incidentSummary = rs.getString("disciplinary_log.incident_summary");
        disciplinary_logDTO.complainedByExternal = rs.getString("disciplinary_log.complained_by_external");
        disciplinary_logDTO.complaintStatusCat = rs.getInt("disciplinary_log.complaint_status_cat");
        disciplinary_logDTO.complaintResolvedDate = rs.getLong("disciplinary_log.complaint_resolved_date");
        disciplinary_logDTO.handlingDuration = rs.getInt("disciplinary_log.handling_duration");
        disciplinary_logDTO.searchColumn = rs.getString("disciplinary_log.search_column");
        disciplinary_logDTO.filesDropzone = rs.getLong("disciplinary_log.files_dropzone");
        disciplinary_logDTO.insertionDate = rs.getLong("disciplinary_log.insertion_date");
        disciplinary_logDTO.insertedBy = rs.getString("disciplinary_log.inserted_by");
        disciplinary_logDTO.modifiedBy = rs.getString("disciplinary_log.modified_by");
        disciplinary_logDTO.isDeleted = rs.getInt("disciplinary_log.isDeleted");
        disciplinary_logDTO.lastModificationTime = rs.getLong("disciplinary_log.lastModificationTime");
    }

    public DisciplinaryLogDTODetails buildObjectFromResultSetForComplainBy(ResultSet rs) {
        return setDisciplinaryLogDTODetailsFromResultSetForComplainBy(rs, new DisciplinaryLogDTODetails());
    }

    private DisciplinaryLogDTODetails setDisciplinaryLogDTODetailsFromResultSetForComplainBy(ResultSet rs, DisciplinaryLogDTODetails dtoDetails) {
        try {
            setDisciplinaryLogDTOFromResultSet(rs, dtoDetails);
            dtoDetails.employeeRecordId = rs.getLong("disciplinary_log_complain_by.employee_records_id");
            dtoDetails.complainByNameEn = rs.getString("disciplinary_log_complain_by.employee_name_en");
            dtoDetails.complainByNameBn = rs.getString("disciplinary_log_complain_by.employee_name_bn");
            dtoDetails.complainByOfficeEn = rs.getString("disciplinary_log_complain_by.office_name_en");
            dtoDetails.complainByOfficeBn = rs.getString("disciplinary_log_complain_by.office_name_bn");
            dtoDetails.complainByDesignationEn = rs.getString("disciplinary_log_complain_by.organogram_name_en");
            dtoDetails.complainByDesignationBn = rs.getString("disciplinary_log_complain_by.organogram_name_bn");
            return dtoDetails;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public DisciplinaryLogDTODetails buildObjectFromResultSetForComplainFor(ResultSet rs) {
        try {
            DisciplinaryLogDTODetails dtoDetails = new DisciplinaryLogDTODetails();
            setDisciplinaryLogDTOFromResultSet(rs, dtoDetails);
            setDisciplinaryLogDTODetailsFromResultSetForComplainFor(rs, dtoDetails);
            return dtoDetails;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    private void setDisciplinaryLogDTODetailsFromResultSetForComplainFor(ResultSet rs, DisciplinaryLogDTODetails dtoDetails) throws SQLException {
        dtoDetails.employeeRecordId = rs.getLong("disciplinary_details.employee_records_id");
        dtoDetails.complainForNameEn = rs.getString("disciplinary_details.employee_name_en");
        dtoDetails.complainForNameBn = rs.getString("disciplinary_details.employee_name_bn");
        dtoDetails.complainForOfficeEn = rs.getString("disciplinary_details.office_name_en");
        dtoDetails.complainForOfficeBn = rs.getString("disciplinary_details.office_name_bn");
        dtoDetails.complainForDesignationEn = rs.getString("disciplinary_details.organogram_name_en");
        dtoDetails.complainForDesignationBn = rs.getString("disciplinary_details.organogram_name_bn");
    }

    public DisciplinaryLogDTODetails buildObjectFromResultSetForComplainForAndComplainBy(ResultSet rs) {
        DisciplinaryLogDTODetails dtoDetails = buildObjectFromResultSetForComplainBy(rs);
        if (dtoDetails != null) {
            try {
                setDisciplinaryLogDTODetailsFromResultSetForComplainFor(rs, dtoDetails);
            } catch (SQLException ex) {
                logger.error(ex);
                return null;
            }
        }
        return dtoDetails;
    }

    public DisciplinaryLogModel buildDisciplinaryLogModel(Disciplinary_logDTO dto) {
        if (dto != null) {
            DisciplinaryLogModel model = new DisciplinaryLogModel();
            model.dto = dto;
            CategoryLanguageModel languageModel = CatRepository.getInstance().getCategoryLanguageModel("complaint_source", dto.complaintSourceCat);
            if (languageModel != null) {
                model.complaintSourceEng = languageModel.englishText;
                model.complaintSourceBan = languageModel.banglaText;
            }
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            model.reportedDateEng = sdf.format(new Date(dto.reportedDate));
            model.reportedDateBan = StringUtils.convertToBanNumber(model.reportedDateEng);

            NameDTO nameDTO = IncidentRepository.getInstance().getDTOByID(dto.incidentType);
            if (nameDTO != null) {
                model.incidentEngText = nameDTO.nameEn;
                model.incidentBangText = nameDTO.nameBn;
            }
            languageModel = CatRepository.getInstance().getCategoryLanguageModel("complaint_status", dto.complaintStatusCat);
            if (languageModel != null) {
                model.complaintStatusEng = languageModel.englishText;
                model.complaintStatusBan = languageModel.banglaText;
            }
            if (dto.complaintResolvedDate > 0) {
                model.complaintResolvedDateEng = sdf.format(new Date(dto.complaintResolvedDate));
                model.complaintResolvedDateBan = StringUtils.convertToBanNumber(model.complaintResolvedDateEng);
            }
            return model;
        }
        return null;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Disciplinary_logDTO) commonDTO, addSqlQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Disciplinary_logDTO) commonDTO, updateSqlQuery, false);
    }

    public CommonDTO getDTOByID(long ID) throws Exception {
        return getDTOFromID(ID);
    }

    public List<Disciplinary_logDTO> getAllDisciplinary_log(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }


//    public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
//                                                          UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList) {
//        StringBuilder joinClause = new StringBuilder();
//        StringBuilder whereClause = new StringBuilder();
//        if (filter != null && filter.trim().length() > 0) {
//            long organogramId = Long.parseLong(filter);
//            OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(organogramId);
//            if (officeUnitOrganograms.orgTree.endsWith("$")) {
//                joinClause.append(" INNER JOIN disciplinary_details on disciplinary_details.disciplinary_log_id = disciplinary_log.incident_number");
//                p_searchCriteria.put("complain_for_internal", String.valueOf(userDTO.employee_record_id));
//            } else {
//                Set<Long> organogramIds = OfficeUnitOrganogramsRepository.getInstance().getDescentsOrganogram(organogramId);
//                if (organogramIds.size() > 0) {
//                    Set<Long> empIds = EmployeeOfficeRepository.getInstance().getEmpIdByOrganogramId(organogramIds);
//                    if (empIds.size() > 0) {
//                        String ids = empIds.stream()
//                                .map(String::valueOf)
//                                .collect(Collectors.joining(","));
//                        p_searchCriteria.put("complain_for_internal", ids);
//                        joinClause.append(" INNER JOIN disciplinary_details on disciplinary_details.disciplinary_log_id = disciplinary_log.incident_number");
//                    } else if (p_searchCriteria.get("complain_for") != null && !((String) p_searchCriteria.get("complain_for")).trim().equals("")) {
//                        p_searchCriteria.put("complain_for", -1);
//                    }
//                } else if (p_searchCriteria.get("complain_for") != null && !((String) p_searchCriteria.get("complain_for")).trim().equals("")) {
//                    p_searchCriteria.put("complain_for", -1);
//                }
//            }
//        }
//        if (p_searchCriteria.get("complain_by") != null && !((String) p_searchCriteria.get("complain_by")).trim().equals("")) {
//            joinClause.append(" INNER JOIN disciplinary_log_complain_by ON disciplinary_log_complain_by.incident_number = disciplinary_log.incident_number");
//            whereClause.append(" and (disciplinary_log_complain_by.isDeleted = 0 ) ");
//        }
//        if (p_searchCriteria.get("complain_for") != null && !((String) p_searchCriteria.get("complain_for")).trim().equals("")) {
//            if (p_searchCriteria.get("complain_for_internal") == null) {
//                joinClause.append(" INNER JOIN disciplinary_details on disciplinary_details.disciplinary_log_id = disciplinary_log.incident_number");
//            }
//            whereClause.append(" and (disciplinary_details.isDeleted = 0 )");
//        }
//        return getSearchQuery(tableName, p_searchCriteria, limit, offset, category, searchMap, joinClause.toString(), whereClause.toString(), objectList);
//    }

    public Map<String, DisciplinaryLogModel> getDisciplinaryLogModelsByDisciplinaryLogIds(Set<String> incidentNumbers) {
        if (incidentNumbers == null || incidentNumbers.size() == 0) {
            return new HashMap<>();
        }
        String inQuery = incidentNumbers.stream()
                .map(s -> "'" + s + "'")
                .collect(Collectors.joining(","));
        String sql = sqlGetByIncidentNumbers.replace("{incident_numbers}", inQuery);
        return getDTOs(sql)
                .stream()
                .map(this::buildDisciplinaryLogModel)
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(model -> model.dto.incidentNumber, model -> model));
    }

    public Disciplinary_logDTO getDTOByIncidentNumber(String incidentNumber) {
        return ConnectionAndStatementUtil.getT(getByIncidentNumber, Collections.singletonList(incidentNumber), this::buildObjectFromResultSet);
    }

    public void updateComplainStatus(String incidentNumber, int complainStatusValue, String modifiedBy, long modifiedTime) throws Exception {
        Disciplinary_logDTO disciplinary_logDTO = getDTOByIncidentNumber(incidentNumber);
        if (disciplinary_logDTO == null) {
            return;
        }
        CategoryLanguageModel categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel("complaint_status", complainStatusValue);
        if (categoryLanguageModel.englishText.equals("CLOSED") && disciplinary_logDTO.complaintResolvedDate == SessionConstants.MIN_DATE) {
            disciplinary_logDTO.complaintResolvedDate = modifiedTime;
            disciplinary_logDTO.handlingDuration = (int) TimeUnit.DAYS.convert(disciplinary_logDTO.complaintResolvedDate - disciplinary_logDTO.reportedDate, TimeUnit.MILLISECONDS) + 1;
        }
        disciplinary_logDTO.complaintStatusCat = complainStatusValue;
        disciplinary_logDTO.modifiedBy = modifiedBy;
        disciplinary_logDTO.lastModificationTime = modifiedTime;
        update(disciplinary_logDTO);
    }
}
