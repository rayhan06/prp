package disciplinary_log;

import employee_records.EmployeeFlatInfoDTO;

public class Disciplinary_logComplainByDTO extends EmployeeFlatInfoDTO {
    public String incidentNumber;

    public String getIdsStrInJsonString(){
        return String.format("{employeeRecordId: '%s', officeUnitId: '%s', organogramId: '%s'}",
                employeeRecordsId,officeUnitId,organogramId);
    }
}
