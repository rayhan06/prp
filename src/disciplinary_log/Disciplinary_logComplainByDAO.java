package disciplinary_log;

import employee_records.EmpOfficeModel;
import employee_records.EmployeeFlatInfoDAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
@SuppressWarnings({"unused","Duplicates"})
public class Disciplinary_logComplainByDAO implements EmployeeFlatInfoDAO<Disciplinary_logComplainByDTO>{
    private final List<String>extraColumns= Collections.singletonList("incident_number");
    @Override
    public String getTableName() {
        return "disciplinary_log_complain_by";
    }

    @Override
    public List<String> getExtraColumnListForAddSQLQueryClause() {
        return extraColumns;
    }

    @Override
    public List<String> getExtraColumnListUpdateSQLQueryClause() {
        return extraColumns;
    }

    @Override
    public void setExtraPreparedStatementParams(PreparedStatement ps, Disciplinary_logComplainByDTO dto, boolean isInsert) throws Exception {
        int index=0;
        ps.setObject(++index,dto.incidentNumber);
    }

    @Override
    public Disciplinary_logComplainByDTO buildTFromResultSet(ResultSet rs) throws SQLException {
        Disciplinary_logComplainByDTO dto=new Disciplinary_logComplainByDTO();
        dto.incidentNumber=rs.getString("incident_number");
        return dto;
    }
    public void insertComplainedByList(String logId, List<EmpOfficeModel> employeeList,String insertedBy, long insertTime) {
        if (employeeList == null || employeeList.size() == 0) {
            return;
        }
        employeeList.forEach(employee -> {
            Disciplinary_logComplainByDTO dto = new Disciplinary_logComplainByDTO();
            dto.incidentNumber = logId;
            dto.employeeRecordsId = employee.employeeRecordId;
            dto.officeUnitId=employee.officeUnitId;
            dto.organogramId=employee.organogramId;
            dto.insertionDate = insertTime;
            dto.insertBy = insertedBy;
            dto.modified_by = insertedBy;
            dto.lastModificationTime = insertTime;
            logger.debug(dto);
            try {
                addEmployeeFlatInfoDTO(dto);
            } catch (Exception e) {
                logger.error(e);
                e.printStackTrace();
            }
        });
    }
    //////No Update needed now/////////////
}
