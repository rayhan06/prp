package disciplinary_log;

public class DisciplinaryLogModel {
	public Disciplinary_logDTO dto;

	public String complaintSourceEng = "";
	public String complaintSourceBan = "";
	public String reportedDateEng = "";
	public String reportedDateBan = "";
	public String incidentBangText = "";
	public String incidentEngText = "";
	public String complaintStatusEng = "";
	public String complaintStatusBan = "";
	public String complaintResolvedDateEng = "";
	public String complaintResolvedDateBan = "";


	@Override
	public String toString() {
		return "DisciplinaryLogModel{" +
				"dto=" + dto +
				", complaintSourceEng='" + complaintSourceEng + '\'' +
				", complaintSourceBan='" + complaintSourceBan + '\'' +
				", reportedDateEng='" + reportedDateEng + '\'' +
				", reportedDateBan='" + reportedDateBan + '\'' +
				", incidentBangText='" + incidentBangText + '\'' +
				", incidentEngText='" + incidentEngText + '\'' +
				", complaintStatusEng='" + complaintStatusEng + '\'' +
				", complaintStatusBan='" + complaintStatusBan + '\'' +
				", complaintResolvedDateEng='" + complaintResolvedDateEng + '\'' +
				", complaintResolvedDateBan='" + complaintResolvedDateBan + '\'' +
				'}';
	}
}
