package disciplinary_log;

import sessionmanager.SessionConstants;
import util.CommonDTO;

public class Disciplinary_logDTO extends CommonDTO {

	public String incidentNumber = "";
	public int complaintSourceCat = 0;
	public long reportedDate = 0;
	public long incidentType = 0;
	public String incidentTypeOther = "";
	public String incidentSummary = "";
	public String complainedByExternal="";
	public int complaintStatusCat = 0;
	public long complaintResolvedDate = SessionConstants.MIN_DATE;
	public int handlingDuration = 0;
	public long filesDropzone = 0;
	public long insertionDate = 0;
	public String insertedBy = "";
	public String modifiedBy = "";
	public String newGenerateIncidentNumber;

	@Override
	public String toString() {
		return "Disciplinary_logDTO{" +
				"incidentNumber='" + incidentNumber + '\'' +
				", complaintSourceCat=" + complaintSourceCat +
				", reportedDate=" + reportedDate +
				", incidentType=" + incidentType +
				", incidentTypeOther='" + incidentTypeOther + '\'' +
				", incidentSummary='" + incidentSummary + '\'' +
				", complaintStatusCat=" + complaintStatusCat +
				", complaintResolvedDate=" + complaintResolvedDate +
				", handlingDuration=" + handlingDuration +
				", filesDropzone=" + filesDropzone +
				", insertionDate=" + insertionDate +
				", insertedBy='" + insertedBy + '\'' +
				", modifiedBy='" + modifiedBy + '\'' +
				", newGenerateIncidentNumber='" + newGenerateIncidentNumber + '\'' +
				'}';
	}

}