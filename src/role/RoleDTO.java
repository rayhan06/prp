package role;

import annotation.ColumnName;
import annotation.PrimaryKey;
import annotation.TableName;
import report.Join;
import user.UserDTO;

@TableName("role")
public class RoleDTO
{
	@ColumnName("roleName")
	public String roleName = "";
	@ColumnName("description")
    public String description = "";
	@ColumnName("role_bn")
    public String roleBn = "";
    @PrimaryKey
    @ColumnName("ID")
    @Join({UserDTO.class,MenuPermissionDTO.class})
    public long ID;
    @ColumnName("isDeleted")
    public int isDeleted;
    @ColumnName("lastModificationTime")
    public long lastModificationTime;

    @Override
    public String toString() {
        return "RoleDTO{" +
                "roleName='" + roleName + '\'' +
                ", description='" + description + '\'' +
                ", ID=" + ID +
                ", isDeleted=" + isDeleted +
                ", lastModificationTime=" + lastModificationTime +
                '}';
    }
}

