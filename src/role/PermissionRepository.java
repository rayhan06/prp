package role;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class PermissionRepository {
    private static final Logger logger = Logger.getLogger(PermissionRepository.class);
    private static Map<Long, Set<Integer>> mapOfMenuSetToRoleID = new ConcurrentHashMap<>();
    private static final Map<Long, Set<Integer>> mapOfColumnPermissionSetToRoleID = new ConcurrentHashMap<>();
    private static Map<Long, RoleDTO> mapOfRoleDTOToRoleID = new ConcurrentHashMap<>();

    private static final String getRoleSql = "SELECT * FROM role where isDeleted = 0";
    private static final String getMenuPermissionSql = "SELECT * FROM menu_permission ";
    private static final String getColumnPermissionSql = "SELECT * FROM column_permission ";

    static {
        logger.debug("loading start of PermissionRepository");
        reload();
        logger.debug("loading end of PermissionRepository");
    }

    private PermissionRepository() {
    }

    public static PermissionRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    private static class LazyLoader {
        static final PermissionRepository INSTANCE = new PermissionRepository();
    }

    private static void reloadRole() {
        logger.debug("PermissionRepository reloadRole start");
        ConnectionAndStatementUtil.getReadStatement(st -> {
            try {
                logger.debug(getRoleSql);
                ResultSet rs = st.executeQuery(getRoleSql);
                Map<Long, RoleDTO> temp = new ConcurrentHashMap<>();
                while (rs.next()) {
                    if (rs.getInt("isDeleted") == 0) {
                        RoleDTO roleDTO = new RoleDTO();
                        roleDTO.ID = rs.getLong("ID");
                        roleDTO.roleName = rs.getString("roleName");
                        roleDTO.roleBn = rs.getString("role_bn");
                        if(roleDTO.roleBn == null)
                        {
                        	roleDTO.roleBn = roleDTO.roleName;
                        }
                        roleDTO.description = rs.getString("description");
                        roleDTO.isDeleted = rs.getInt("isDeleted");
                        temp.put(roleDTO.ID, roleDTO);
                    }
                }
                mapOfRoleDTOToRoleID = temp;
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
        logger.debug("<<PermissionRepository reloadRole end>>");
    }

    public static RoleDTO getRoleDTOByRoleID(long roleID) {
        return mapOfRoleDTOToRoleID.get(roleID);
    }

    public static ArrayList<RoleDTO> getAllRoles() {
        return new ArrayList<>(mapOfRoleDTOToRoleID.values());
    }

    private static void reloadMenuPermission() {
        logger.debug("reloadMenuPermission start");
        ConnectionAndStatementUtil.getReadStatement(st -> {
            try {
                logger.debug(getMenuPermissionSql);
                ResultSet rs = st.executeQuery(getMenuPermissionSql);
                Map<Long, Set<Integer>> temp = new ConcurrentHashMap<>();
                while (rs.next()) {
                    long roleID = rs.getLong("roleID");
                    int menuID = rs.getInt("menuID");
                    Set<Integer> menuIDSet = temp.getOrDefault(roleID, new HashSet<>());
                    menuIDSet.add(menuID);
                    temp.put(roleID, menuIDSet);
                }
                mapOfMenuSetToRoleID = temp;
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
        logger.debug("reloadMenuPermission end");
    }


    private static void reloadColumnPermission() {
        logger.debug("reloadColumnPermission start");
        ConnectionAndStatementUtil.getReadStatement(st -> {
            try {
                logger.debug(getColumnPermissionSql);
                ResultSet rs = st.executeQuery(getColumnPermissionSql);
                while (rs.next()) {
                    long roleID = rs.getLong("roleID");
                    int columnID = rs.getInt("columnID");
                    Set<Integer> columnIDSet = mapOfColumnPermissionSetToRoleID.getOrDefault(roleID, new HashSet<>());
                    columnIDSet.add(columnID);
                    mapOfColumnPermissionSetToRoleID.put(roleID, columnIDSet);
                }
                logger.debug(mapOfColumnPermissionSetToRoleID);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
        logger.debug("reloadColumnPermission end");
    }

    public static void reload(long[] p_ids) {
        for (long p_id : p_ids) {
            mapOfRoleDTOToRoleID.remove(p_id);
            mapOfMenuSetToRoleID.remove(p_id);
            mapOfColumnPermissionSetToRoleID.remove(p_id);
        }
        reload();
    }

    public static void reload() {
        reloadRole();
        reloadMenuPermission();
        reloadColumnPermission();
    }

    public static boolean checkPermissionByRoleIDAndMenuID(long roleID, int menuID) {
        return mapOfMenuSetToRoleID
                .getOrDefault(roleID, Collections.emptySet())
                .contains(menuID);
    }

    public static boolean checkPermissionByRoleIDAndColumnID(long roleID, int columnID) {
        return mapOfColumnPermissionSetToRoleID
                .getOrDefault(roleID, Collections.emptySet())
                .contains(columnID);
    }

    public String buildOptions(String language, String selectedId) {
        List<OptionDTO> optionDTOList = null;
        List<RoleDTO> roleDTOS = getAllRoles();
        if (roleDTOS.size() > 0) {
            optionDTOList = roleDTOS.stream()
                    .map(model -> new OptionDTO(model.roleName, model.roleName, String.valueOf(model.ID)))
                    .collect(Collectors.toList());
        }
        return Utils.buildOptionsMultipleSelection(optionDTOList, language, selectedId);
    }
}