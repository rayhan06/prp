package role;

import common.ConnectionAndStatementUtil;
import common.RequestFailureException;
import dbm.DBMR;
import dbm.DBMW;
import login.LoginDTO;
import org.apache.log4j.Logger;
import util.ConnectionUtil;
import util.DAOResult;
import util.NavigationService;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings({"rawtypes","unchecked","Duplicates"})
public class RoleDAO implements NavigationService {
    private static final Logger logger = Logger.getLogger(RoleDAO.class);


    public Collection getDTOs(Collection recordIDs, LoginDTO loginDTO) {
        String sql = "select ID, roleName, role_bn, description from role ";
        if (recordIDs.size() > 0) {
            String whereClause = (String) recordIDs.stream()
                    .map(String::valueOf)
                    .collect(Collectors.joining(","," WHERE ID IN (",") "));
            sql = sql + whereClause;
        }
        return ConnectionAndStatementUtil.getListOfT(sql,resultSet->{
            try{
                RoleDTO row = new RoleDTO();
                row.ID = resultSet.getInt("ID");
                row.roleName = resultSet.getString("roleName");
                row.roleBn = resultSet.getString("role_bn");
                row.description = resultSet.getString("description");
                return row;
            }catch (SQLException ex){
                logger.error("",ex);
                return null;
            }
        });
    }

    public static List<RoleDTO> getDTOs() {
        String sql = "select ID, roleName, role_bn, description from role where isDeleted = 0";
        return ConnectionAndStatementUtil.getListOfT(sql,resultSet->{
            try{
                RoleDTO row = new RoleDTO();
                row.ID = resultSet.getInt("ID");
                row.roleName = resultSet.getString("roleName");
                row.roleBn = resultSet.getString("role_bn");
                row.description = resultSet.getString("description");
                return row;
            }catch (SQLException ex){
                logger.error("",ex);
                return null;
            }
        });
    }

    public static boolean hasRolePermission(long roleID, int menuID) {
        String sql = "SELECT * FROM menu_permission where roleid = ? and menuid = ?";
        return ConnectionAndStatementUtil.getT(sql,Arrays.asList(roleID,menuID),rs-> true,false);
    }

    public static List<Integer> getOrganogramRoles(long organogramID) {
        String sql = "SELECT role_type FROM office_unit_organograms where id = ?";
        List<String> roleTypes = ConnectionAndStatementUtil.getListOfT(sql,Collections.singletonList(organogramID),rs->{
            try {
                return rs.getString("role_type");
            } catch (SQLException ex) {
                logger.error("",ex);
                return null;
            }
        });

        return roleTypes.stream()
                .map(e-> Stream.of(e.split(",")).map(Integer::parseInt).collect(Collectors.toList()))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public Collection getIDs(LoginDTO loginDTO) {
        String sql = "select ID from role where isDeleted = 0 ";
        return ConnectionAndStatementUtil.getListOfT(sql,rs->{
            try{
                return rs.getString("ID");
            }catch (SQLException ex){
                logger.error("",ex);
                return null;
            }
        });
    }

    public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO) throws Exception {
        Hashtable<String, String> searchCriteria = (Hashtable<String, String>) p_searchCriteria;
        boolean conditionAdded = false;
        List<Object> objectList = new ArrayList<>();
        String sql = "SELECT ID FROM role";
        String roleNameInputFromUI = searchCriteria.get("roleName");
        if (roleNameInputFromUI != null && !roleNameInputFromUI.trim().equals("")) {
            sql += " WHERE  ";
            conditionAdded = true;
            sql += "roleName LIKE ? ";
            objectList.add("%" + roleNameInputFromUI + "%");
        }

        String descriptionInputFromUI = searchCriteria.get("description");
        if (descriptionInputFromUI != null && !descriptionInputFromUI.trim().equals("")) {
            if (conditionAdded) {
                sql += " AND  ";
            } else {
                sql += " WHERE  ";
            }
            conditionAdded = true;
            sql += "description LIKE ? ";
            objectList.add("%" + descriptionInputFromUI + "%");
        }

        return ConnectionAndStatementUtil.getListOfT(sql,objectList,rs->{
            try {
                return rs.getLong(1);
            } catch (SQLException ex) {
                logger.error("",ex);
                return null;
            }
        });
    }

    public DAOResult addRole(RoleDTO roleDTO) {
        String sql;
        DAOResult daoResult = new DAOResult();
        Connection cn = null;
        Statement stmt = null;

        Statement stmt2 = null;
        ResultSet resultSet;

        int tempTrack = 0;

        try {
            cn = DBMR.getInstance().getConnection();

            sql = "select ID from role where roleName = '" + roleDTO.roleName + "'";
            stmt = cn.createStatement();
            resultSet = stmt.executeQuery(sql);
            if (resultSet.next()) {
                resultSet.close();
                daoResult.setResult("error.roleName.role", false, DAOResult.VALIDATION_ERROR);
                return daoResult;
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (cn != null) {
                    DBMR.getInstance().freeConnection(cn);
                }
            } catch (Exception ignored) {
            }


            long roleID = DBMW.getInstance().getNextSequenceId("role");
            cn = DBMW.getInstance().getConnection();
            tempTrack = 1;
            roleDTO.ID = roleID;
            sql = "insert into role(ID,roleName,description) values(" + roleID + ",'" + roleDTO.roleName + "','" + roleDTO.description + "'" + ")";
            logger.debug("sql " + sql);
            stmt2 = cn.createStatement();
            stmt2.executeUpdate(sql);


            daoResult.setResult("", true, DAOResult.DONE);

        } catch (Exception e) {
            daoResult.setResult(e.toString(), false, DAOResult.DB_EXCEPTION);
            logger.fatal("DAO ", e);
            try {
                if (cn != null) {
                    cn.rollback();
                }
            } catch (Exception ex) {
                logger.fatal("DAO ", ex);
            }

        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (stmt2 != null) {
                    stmt2.close();
                }
            } catch (Exception e) {
                logger.fatal("DAO finally :" + e);
            }

            try {
                if (cn != null) {
                    if (tempTrack == 1) {
                        cn.setAutoCommit(true);
                        DBMW.getInstance().freeConnection(cn);
                    } else {
                        DBMR.getInstance().freeConnection(cn);
                    }
                }
            } catch (Exception e) {
                logger.fatal("DAO finally :" + e);
            }
        }

        //PermissionRepository.getInstance().reload();

        return daoResult;
    }

    public RoleDTO getRole(long p_id) {
        String sql = "select ID, roleName, role_bn, description from role where ID = ? and isDeleted = 0 ";
        return ConnectionAndStatementUtil.getT(sql,Collections.singletonList(p_id),rs->{
            try{
                RoleDTO row = new RoleDTO();
                row.ID = rs.getInt("ID");
                row.roleName = rs.getString("roleName");
                row.roleBn = rs.getString("role_bn");
                row.description = rs.getString("description");
                return row;
            }catch (SQLException ex){
                logger.error("",ex);
                return null;
            }
        });
    }


    public void updateRoleDTO(RoleDTO roleDTO) {
        String sql = "UPDATE role SET roleName=?,description=?,isDeleted=?,lastModificationTime=? WHERE ID = ?";
        ConnectionAndStatementUtil.getWritePrepareStatement(ps->{
            try{
                int index = 0;
                ps.setObject(++index, roleDTO.roleName);
                ps.setObject(++index, roleDTO.description);
                ps.setObject(++index, roleDTO.isDeleted);
                ps.setObject(++index, System.currentTimeMillis());
                ps.setObject(++index, roleDTO.ID);
                ps.executeUpdate();
            }catch (SQLException ex){
                logger.error("",ex);
            }
        },sql);
    }


    public DAOResult dropRoles(long[] p_ids) {
        DAOResult daoResult = new DAOResult();
        DBMW dm = null;
        Connection cn = null;
        Statement stmt = null;


        long currentTime = System.currentTimeMillis();

        String sql = "delete from role where ID =";
        String permissionDelSql = "delete from menu_permission where roleID=";
        String userDelSql = "delete from user where roleID=";
        String columnDelSql = "delete from column_permission where roleID=";
        if (p_ids == null || p_ids.length == 0) {
            daoResult.setResult("", true, DAOResult.DONE);
            return daoResult;
        }

        try {
            dm = DBMW.getInstance();
            cn = dm.getConnection();
            cn.setAutoCommit(false);
            stmt = cn.createStatement();

            for (long p_id : p_ids) {
                String fullQuery = sql + p_id;
                stmt.executeUpdate(fullQuery);
                fullQuery = permissionDelSql + p_id;
                stmt.executeUpdate(fullQuery);
                fullQuery = userDelSql + p_id;
                int numberOfDeletedUser = stmt.executeUpdate(fullQuery);
                if (numberOfDeletedUser != 0) {
                    throw new RequestFailureException("User exists with role ID " + p_id);
                }
                fullQuery = columnDelSql + p_id;
                stmt.executeUpdate(fullQuery);
            }

            ConnectionUtil.updateVbSequencer("role", currentTime, stmt);

            cn.commit();
            daoResult.setResult("", true, DAOResult.DONE);
            PermissionRepository.reload(p_ids);
        } catch (Exception e) {
            try {
                cn.rollback();
            } catch (SQLException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            daoResult.setResult(e.toString(), false, DAOResult.DB_EXCEPTION);
            logger.fatal("DAO " + e);
            if (e instanceof RequestFailureException) {
                throw (RequestFailureException) e;
            }
        } finally {
            try {
                stmt.close();
            } catch (Throwable th) {
                logger.fatal("Failed to close Statement", th);
            }
            try {
                if (cn != null) {
                    cn.setAutoCommit(true);
                    dm.freeConnection(cn);
                }
            } catch (Exception e) {
                logger.fatal("DAO finally :" + e);
            }
        }

        return daoResult;
    }

    public List<RoleDTO> getRolesByPartialMatch(String roleName) {
        String sql = "select ID, roleName, description from role where roleName like '" + roleName + "%' and isDeleted = 0";
        return ConnectionAndStatementUtil.getListOfT(sql, rs -> {
            try {
                RoleDTO roleDTO = new RoleDTO();
                roleDTO.ID = rs.getInt("ID");
                roleDTO.roleName = rs.getString("roleName");
                roleDTO.description = rs.getString("description");
                return roleDTO;
            } catch (SQLException ex) {
                logger.error("",ex);
                return null;
            }
        });
    }
}