package vm_vehicle_fitness_tax_report;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Vm_vehicle_fitness_tax_report_Servlet")
public class Vm_vehicle_fitness_tax_report_Servlet  extends HttpServlet {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","tr","fitness_expiry_date",">=","","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},
		{"criteria","tr","fitness_expiry_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},
		//{"criteria","tr","tax_token_expiry_date",">=","AND","long","","",Long.MIN_VALUE + "","endDate", LC.HM_START_DATE + ""},
		{"criteria","tr","tax_token_expiry_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},
		{"criteria","tr","vehicle_type_cat","=","AND","int","","","any","vehicleTypeCat", LC.VM_VEHICLE_FITNESS_TAX_REPORT_WHERE_VEHICLETYPECAT + ""},
		{"criteria","tr","isDeleted","=","AND","String","","","0","isDeleted", LC.VM_VEHICLE_FITNESS_TAX_REPORT_WHERE_ISDELETED + ""}
	};

	String[][] Display =
	{
		{"display","tr","vehicle_type_cat","cat",""},
		{"display","tr","reg_no","text",""},
		{"display","tr","fitness_expiry_date","date",""},
		{"display","tr","tax_token_expiry_date","date",""} ,
		//{"display","tr","fitness_tax_token_expiry_date","fitness_tax_token_expiry_date_check",""}
	};

	String[][] Display2 =
	{
		{"display","tr","vehicle_type_cat","cat",""},
		{"display","tr","reg_no","text",""},
		{"display","tr","fitness_expiry_date","date",""},
		{"display","tr","tax_token_expiry_date","date",""} ,
		//{"display","tr","fitness_tax_token_expiry_date","fitness_tax_token_expiry_date_check",""}
	};

	String GroupBy = "";
	String OrderBY = "";

	ReportRequestHandler reportRequestHandler;

	public Vm_vehicle_fitness_tax_report_Servlet(){

	}

	private final ReportService reportService = new ReportService();

	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}

		String reportType = request.getParameter("fitnessTaxTokenTypeCat");

		if(reportType!=null && !reportType.isEmpty()){

			System.out.println("Report Type: "+reportType);

			if(reportType.equals("1")){

				List<String[]> l = new ArrayList<String[]>(Arrays.asList(Display2));

				l.remove(3);
				Display = l.toArray(new String[][]{});



				Display[0][4] = LM.getText(LC.VM_VEHICLE_FITNESS_TAX_REPORT_SELECT_VEHICLETYPECAT, loginDTO);
				Display[1][4] = LM.getText(LC.VM_VEHICLE_FITNESS_TAX_REPORT_SELECT_REGNO, loginDTO);
				Display[2][4] = LM.getText(LC.VM_VEHICLE_FITNESS_TAX_REPORT_SELECT_FITNESSEXPIRYDATE, loginDTO);
			}
			else if(reportType.equals("2")){

				List<String[]> l = new ArrayList<String[]>(Arrays.asList(Display2));

				l.remove(2);
				Display = l.toArray(new String[][]{});


				Display[0][4] = LM.getText(LC.VM_VEHICLE_FITNESS_TAX_REPORT_SELECT_VEHICLETYPECAT, loginDTO);
				Display[1][4] = LM.getText(LC.VM_VEHICLE_FITNESS_TAX_REPORT_SELECT_REGNO, loginDTO);
				Display[2][4] = LM.getText(LC.VM_VEHICLE_FITNESS_TAX_REPORT_SELECT_TAXTOKENEXPIRYDATE, loginDTO);
			}

			else{
				List<String[]> l = new ArrayList<String[]>(Arrays.asList(Display2));
				Display = l.toArray(new String[][]{});

				Display[0][4] = LM.getText(LC.VM_VEHICLE_FITNESS_TAX_REPORT_SELECT_VEHICLETYPECAT, loginDTO);
				Display[1][4] = LM.getText(LC.VM_VEHICLE_FITNESS_TAX_REPORT_SELECT_REGNO, loginDTO);
				Display[2][4] = LM.getText(LC.VM_VEHICLE_FITNESS_TAX_REPORT_SELECT_FITNESSEXPIRYDATE, loginDTO);
				Display[3][4] = LM.getText(LC.VM_VEHICLE_FITNESS_TAX_REPORT_SELECT_TAXTOKENEXPIRYDATE, loginDTO);

			}


		}
		else{
			List<String[]> l = new ArrayList<String[]>(Arrays.asList(Display2));
			Display = l.toArray(new String[][]{});

			Display[0][4] = LM.getText(LC.VM_VEHICLE_FITNESS_TAX_REPORT_SELECT_VEHICLETYPECAT, loginDTO);
			Display[1][4] = LM.getText(LC.VM_VEHICLE_FITNESS_TAX_REPORT_SELECT_REGNO, loginDTO);
			Display[2][4] = LM.getText(LC.VM_VEHICLE_FITNESS_TAX_REPORT_SELECT_FITNESSEXPIRYDATE, loginDTO);
			Display[3][4] = LM.getText(LC.VM_VEHICLE_FITNESS_TAX_REPORT_SELECT_TAXTOKENEXPIRYDATE, loginDTO);

		}











		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);

		sql = "vm_vehicle tr";


		//Display[4][4] = LM.getText(LC.VM_VEHICLE_FITNESS_TAX_TOKEN_REPORT_WHERE_FITNESSEXPIRYDATE, loginDTO);


		String reportName = LM.getText(LC.VM_VEHICLE_FITNESS_TAX_REPORT_OTHER_VM_VEHICLE_FITNESS_TAX_REPORT, loginDTO);

		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);


		reportRequestHandler.handleReportGet(request, response, userDTO, "vm_vehicle_fitness_tax_report",
				MenuConstants.VM_VEHICLE_FITNESS_TAX_REPORT_DETAILS, language, reportName, "vm_vehicle_fitness_tax_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doGet(request, response);
	}
}
