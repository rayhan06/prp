package vm_requisition_pending;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import employee_assign.EmployeeSearchIds;
import employee_assign.EmployeeSearchModalUtil;
import employee_assign.EmployeeSearchModel;
import mail.EmailService;
import mail.SendEmailDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsRepository;
import offices.OfficesDTO;
import offices.OfficesRepository;
import org.apache.log4j.Logger;

import login.LoginDTO;
import pb.Utils;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import sms.SmsService;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import java.util.stream.Collectors;
import javax.servlet.http.*;


import com.google.gson.Gson;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import vm_fuel_request.InvalidDataException;
import vm_requisition.*;
import vm_requisition_approver.Vm_requisition_approverDTO;
import vm_requisition_approver.Vm_requisition_approverRepository;
import vm_vehicle.Vm_vehicleDTO;
import vm_vehicle.Vm_vehicleRepository;


/**
 * Servlet implementation class Vm_requisition_pendingServlet
 */
@WebServlet("/Vm_requisition_pendingServlet")
@MultipartConfig
public class Vm_requisition_pendingServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Vm_requisition_pendingServlet.class);

    String tableName = "vm_requisition";

    Vm_requisitionDAO vm_requisitionDAO;
    CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vm_requisition_pendingServlet() {
        super();
        try {
            vm_requisitionDAO = new Vm_requisitionDAO(tableName);
            commonRequestHandler = new CommonRequestHandler(vm_requisitionDAO);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
        try {
            String actionType = request.getParameter("actionType");

            if (!actionType.equals("search") && !actionType.equals("prePendingSearch")) {
                long approverOrgId = userDTO.organogramID;
                List<Vm_requisition_approverDTO> approver = Vm_requisition_approverRepository.getInstance().getVm_requisition_approverDTOByapprover_org_id(approverOrgId);

                if (approver.isEmpty()) {
                    throw new InvalidDataException("Invalid employee request to approve");
                }
            }


            if (actionType.equals("getEditPage")) {
                if (request.getParameter("ID") != null) {
                    String pendingFilter = " status = " + CommonApprovalStatus.PENDING.getValue() + " AND ID = " + Long.parseLong(request.getParameter("ID")) + " ";
                    int pendingRequest = vm_requisitionDAO.getCount(null, 1, 0, true, userDTO, pendingFilter, false);
                    if (pendingRequest != 1) {
                        throw new card_info.InvalidDataException("No Pending approval is found for id : " + request.getParameter("ID"));
                    }
                }

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_UPDATE)) {
                    getVm_requisition(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getURL")) {
                String URL = request.getParameter("URL");
                System.out.println("URL = " + URL);
                response.sendRedirect(URL);
            } else if (actionType.equals("search")) {
                System.out.println("search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_SEARCH)) {
                    if (isPermanentTable) {
                        String filter = request.getParameter("filter");
                        System.out.println("filter = " + filter);
                        if (filter != null) {
                            filter = ""; //shouldn't be directly used, rather manipulate it.
                            searchVm_requisition(request, response, isPermanentTable, filter);
                        } else {
                            searchVm_requisition(request, response, isPermanentTable, "");
                        }
                    } else {
                        //searchVm_requisition(request, response, tempTableName, isPermanentTable);
                    }
                }
            } else if (actionType.equals("prePendingSearch")) {
                System.out.println("prePendingSearch requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_SEARCH)) {
                    if (isPermanentTable) {
                        String filter = request.getParameter("filter");
                        System.out.println("filter = " + filter);
                        if (filter != null) {
                            filter = ""; //shouldn't be directly used, rather manipulate it.
                            prePendingSearchVm_requisition(request, response, isPermanentTable, filter);
                        } else {
                            prePendingSearchVm_requisition(request, response, isPermanentTable, "");
                        }
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                }
            } else if (actionType.equals("view")) {
                System.out.println("view requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_SEARCH)) {
                    view(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else {
                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        System.out.println("doPost");
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);

            if (!actionType.equals("search")) {
                long approverOrgId = userDTO.organogramID;
                List<Vm_requisition_approverDTO> approver = Vm_requisition_approverRepository.getInstance().getVm_requisition_approverDTOByapprover_org_id(approverOrgId);

                if (approver.isEmpty()) {
                    throw new InvalidDataException("Invalid employee request to approve");
                }
            }

            if (actionType.equals("getDTO")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_ADD)) {
                    getDTO(request, response);
                } else {
                    System.out.println("Not going to  addVm_requisition ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("edit")) {

                if (request.getParameter("iD") != null) {
                    String pendingFilter = " status = " + CommonApprovalStatus.PENDING.getValue() + " AND ID = " + Long.parseLong(request.getParameter("iD")) + " ";
                    int pendingRequest = vm_requisitionDAO.getCount(null, 1, 0, true, userDTO, pendingFilter, false);
                    if (pendingRequest != 1) {
                        throw new card_info.InvalidDataException("No Pending approval is found for id : " + request.getParameter("iD"));
                    }
                }

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_UPDATE)) {
                    addVm_requisition(request, response, false, userDTO, isPermanentTable);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("search")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_SEARCH)) {
                    searchVm_requisition(request, response, true, "");
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getGeo")) {
                System.out.println("going to geoloc ");
                request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getDTO(HttpServletRequest request, HttpServletResponse response) {
        try {
            System.out.println("In getDTO");
            Vm_requisitionDTO vm_requisitionDTO = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(Long.parseLong(request.getParameter("ID")));
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            String encoded = this.gson.toJson(vm_requisitionDTO);
            System.out.println("json encoded data = " + encoded);
            out.print(encoded);
            out.flush();
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void addVm_requisition(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addVm_requisition");
            String path = getServletContext().getRealPath("/img2/");
            Vm_requisitionDTO vm_requisitionDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

            if (addFlag == true) {
                vm_requisitionDTO = new Vm_requisitionDTO();
            } else {
                vm_requisitionDTO = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(Long.parseLong(request.getParameter("iD")));
            }

            String Value = "";

            if (addFlag) {
                vm_requisitionDTO.insertedByUserId = userDTO.ID;
            }


            if (addFlag) {
                vm_requisitionDTO.insertedByOrganogramId = userDTO.organogramID;
            }


            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            if (addFlag) {
                vm_requisitionDTO.insertionDate = c.getTimeInMillis();
            }

            Gson gson = new Gson();
            EmployeeSearchModel modelDecisionBy =
                    gson.fromJson(
                            EmployeeSearchModalUtil.getEmployeeSearchModelJson(userDTO.employee_record_id, userDTO.unitID, userDTO.organogramID),
                            EmployeeSearchModel.class);

            vm_requisitionDTO.decisionByEmpId = modelDecisionBy.employeeRecordId;
            vm_requisitionDTO.decisionByOfficeUnitId = modelDecisionBy.officeUnitId;
            vm_requisitionDTO.decisionByOrgId = modelDecisionBy.organogramId;

            vm_requisitionDTO.decisionByNameEn = modelDecisionBy.employeeNameEn;
            vm_requisitionDTO.decisionByNameBn = modelDecisionBy.employeeNameBn;
            vm_requisitionDTO.decisionByOfficeUnitNameEn = modelDecisionBy.officeUnitNameEn;
            vm_requisitionDTO.decisionByOfficeUnitNameBn = modelDecisionBy.officeUnitNameBn;
            vm_requisitionDTO.decisionByOfficeUnitOrgNameEn = modelDecisionBy.organogramNameEn;
            vm_requisitionDTO.decisionByOfficeUnitOrgNameBn = modelDecisionBy.organogramNameBn;

            vm_requisitionDTO.decisionByPhoneNum = modelDecisionBy.phoneNumber;

            vm_requisitionDTO.secondApproverApproveOrRejectionDate = System.currentTimeMillis();

            OfficesDTO decisionByOffice = OfficesRepository.getInstance().getOfficesDTOByID(vm_requisitionDTO.decisionByOfficeId);

            if (decisionByOffice != null) {
                vm_requisitionDTO.decisionByOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(modelDecisionBy.officeUnitId).officeId;

                vm_requisitionDTO.decisionByOfficeNameEn = decisionByOffice.officeNameEng;
                vm_requisitionDTO.decisionByOfficeNameBn = decisionByOffice.officeNameBng;
            }

            Value = request.getParameter("status");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("status = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.status = Integer.parseInt(Value);
                vm_requisitionDTO.secondApproverStatus = vm_requisitionDTO.status;

            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            vm_requisitionDTO.lastModificationTime = System.currentTimeMillis();
            vm_requisitionDTO.decisionOn = c.getTimeInMillis();

            Value = request.getParameter("decisionDescription");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("decisionDescription = " + Value);
            if (Value != null) {
                vm_requisitionDTO.decisionDescription = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("givenVehicleType");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("givenVehicleType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.givenVehicleType = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("givenVehicleId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("givenVehicleId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.givenVehicleId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("driverEmpId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("driverEmpId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                EmployeeSearchModel[] models = gson.fromJson(Value, EmployeeSearchModel[].class);

                for (EmployeeSearchModel model : models) {

                    vm_requisitionDTO.driverEmpId = model.employeeRecordId;
                    vm_requisitionDTO.driverOfficeUnitId = model.officeUnitId;
                    vm_requisitionDTO.driverOrgId = model.organogramId;

                    vm_requisitionDTO.driverNameEn = model.employeeNameEn;
                    vm_requisitionDTO.driverNameBn = model.employeeNameBn;
                    vm_requisitionDTO.driverOfficeUnitNameEn = model.officeUnitNameEn;
                    vm_requisitionDTO.driverOfficeUnitNameBn = model.officeUnitNameBn;
                    vm_requisitionDTO.driverOfficeUnitOrgNameEn = model.organogramNameEn;
                    vm_requisitionDTO.driverOfficeUnitOrgNameBn = model.organogramNameBn;

                    vm_requisitionDTO.driverPhoneNum = model.phoneNumber;

                    OfficesDTO driverOffice = OfficesRepository.getInstance().getOfficesDTOByID(vm_requisitionDTO.driverOfficeId);

                    if (driverOffice != null) {
                        vm_requisitionDTO.driverOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(model.officeUnitId).officeId;

                        vm_requisitionDTO.driverOfficeNameEn = driverOffice.officeNameEng;
                        vm_requisitionDTO.driverOfficeNameBn = driverOffice.officeNameBng;
                    }

                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            System.out.println("Done adding  addVm_requisition dto = " + vm_requisitionDTO);
            long[] returnedID = {-1};

            Utils.handleTransactionForNavigationService4(() -> {
                if (isPermanentTable == false) //add new row for validation and make the old row outdated
                {
                    vm_requisitionDAO.setIsDeleted(vm_requisitionDTO.iD, CommonDTO.OUTDATED);
                    returnedID[0] = vm_requisitionDAO.add(vm_requisitionDTO);
                    vm_requisitionDAO.setIsDeleted(returnedID[0], CommonDTO.WAITING_FOR_APPROVAL);

                    if (isPermanentTable) {
                        String inPlaceSubmit = request.getParameter("inplacesubmit");

                        if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                            getVm_requisition(request, response, returnedID[0]);
                        } else {
                            response.sendRedirect("Vm_requisition_pendingServlet?actionType=search");
                        }
                    } else {
                        commonRequestHandler.validate(Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(returnedID[0]), request, response, userDTO);
                    }
                } else if (addFlag == true) {
                    returnedID[0] = vm_requisitionDAO.manageWriteOperations(vm_requisitionDTO, SessionConstants.INSERT, -1, userDTO);

                    if (isPermanentTable) {
                        String inPlaceSubmit = request.getParameter("inplacesubmit");

                        if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                            getVm_requisition(request, response, returnedID[0]);
                        } else {
                            response.sendRedirect("Vm_requisition_pendingServlet?actionType=search");
                        }
                    } else {
                        commonRequestHandler.validate(Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(returnedID[0]), request, response, userDTO);
                    }
                } else {
                    Vm_requisitionDAO vm_requisitionDAO = new Vm_requisitionDAO();
                    HashSet<Long> assignedVehicleIds = new HashSet<Long>(
                            (List<Long>) vm_requisitionDAO.getVehicleIDs(vm_requisitionDTO)
                    );

                    HashSet<Long> assignedDriverIds = new HashSet<Long>(
                            (List<Long>) vm_requisitionDAO.getDriverIDs(vm_requisitionDTO)
                    );

                    if (assignedVehicleIds.contains(vm_requisitionDTO.givenVehicleId) && vm_requisitionDTO.status == CommonApprovalStatus.SATISFIED.getValue()) {
                        PrintWriter out = response.getWriter();
                        out.println(new Gson().toJson("Vehicle occupied"));
                        out.close();
                    } else if (assignedDriverIds.contains(vm_requisitionDTO.driverEmpId) && vm_requisitionDTO.status == CommonApprovalStatus.SATISFIED.getValue()) {
                        PrintWriter out = response.getWriter();
                        out.println(new Gson().toJson("Driver occupied"));
                        out.close();
                    } else {
                        returnedID[0] = vm_requisitionDAO.manageWriteOperations(vm_requisitionDTO, SessionConstants.UPDATE, -1, userDTO);
                        OfficeUnitOrganograms lineManager = OfficeUnitOrganogramsRepository.getInstance().
                                getById(vm_requisitionDTO.requesterOrgId);


                        EmployeeSearchModel model =
                                gson.fromJson(
                                        EmployeeSearchModalUtil.getEmployeeSearchModelJson(vm_requisitionDTO.requesterEmpId,
                                                vm_requisitionDTO.requesterOfficeUnitId, vm_requisitionDTO.requesterOrgId),
                                        EmployeeSearchModel.class);

                        EmployeeSearchModel driverModel =
                                gson.fromJson(
                                        EmployeeSearchModalUtil.getEmployeeSearchModelJson(vm_requisitionDTO.driverEmpId,
                                                vm_requisitionDTO.driverOfficeUnitId, vm_requisitionDTO.driverOrgId),
                                        EmployeeSearchModel.class);

                        String requesterPhoneNum = model.phoneNumber;
                        String formatted_startDate = simpleDateFormat.format(new Date(vm_requisitionDTO.startDate));

                        boolean isApproved = false;

                        String requesterText = "আপনার যানবাহন অধিযাচনটি অনুমোদিত হয়নি";


                        String driverText = "আপনার পরবর্তী ট্রিপ "+Utils.getDigitBanglaFromEnglish(formatted_startDate)+" এ"+
                                ", সময়ঃ "+Utils.getDigitBanglaFromEnglish(vm_requisitionDTO.startTime)+" নিম্নে উল্লেখিত কর্মকর্তার সাথে।\n";
                        
                        driverText += "নামঃ "+model.employeeNameBn+"\nপদবীঃ "+model.organogramNameBn+"\nমোবাইলঃ "+requesterPhoneNum.substring(2);
                        if (vm_requisitionDTO.status == CommonApprovalStatus.SATISFIED.getValue()) {
                            isApproved = true;
                            String driverNameBn = vm_requisitionDTO.driverNameBn;
                            String vehicleNumber = "";
                            Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().
                                    getVm_vehicleDTOByID(vm_requisitionDTO.givenVehicleId);
                            if (vm_vehicleDTO != null) {
                                vehicleNumber = vm_vehicleDTO.regNo;
                                driverText += "\nগাড়ির নম্বরঃ "+ vm_vehicleDTO.regNo;
                            }

                            requesterText = "আপনার যানবাহন অধিযাচনটি অনুমোদিত হয়েছে।";
                            requesterText += "\nগাড়ির নাম্বার:"+vehicleNumber+"\nড্রাইভারের নাম: "+driverNameBn+"\nড্রাইভার মোবাইল: "+vm_requisitionDTO.driverPhoneNum.substring(2);

                            if(!driverModel.phoneNumber.isEmpty()){
                                SmsService.send(driverModel.phoneNumber, String.format(driverText ));
                            }
                            if (driverModel.email != null && !driverModel.email.isEmpty()) {

                                driverText += "<br><br>Best Regards,<br>PRP Development Team";
                                sendMail(driverModel.email , driverText, "Your Next Trip");
                            }


                        }
                        VmRequisitionApprovalNotification.getInstance().sendApproveOrRejectNotification(Arrays.asList(lineManager.id), vm_requisitionDTO, isApproved);

                        /*sms and email start*/
                        
                        SmsService.send(requesterPhoneNum, String.format(requesterText ));
                        if (model.email != null && !model.email.isEmpty()) {

                            String moreInfoText = "<br> Visit <a href=\"https://prp.parliament.gov.bd/Vm_requisitionServlet?actionType=view&ID="+vm_requisitionDTO.iD+"\">PRP</a> for more information";
                            String emailText = requesterText+moreInfoText;
                            emailText += "<br><br>Best Regards,<br>PRP Development Team";
                            sendMail(model.email , emailText, "Request For Use Of Government Vehicle");
                        }


                        /*sms and email end*/

                        PrintWriter out = response.getWriter();
                        out.println(new Gson().toJson("Success"));
                        out.close();
                    }
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendMail(String mailId, String message, String subject) {
        if (mailId != null && mailId.trim().length() > 0) {
            SendEmailDTO sendEmailDTO = new SendEmailDTO();
            sendEmailDTO.setTo(new String[]{mailId});
            sendEmailDTO.setText(message);
            sendEmailDTO.setSubject(subject);
            sendEmailDTO.setFrom("edms@pbrlp.gov.bd");
            try {
                new EmailService().sendMail(sendEmailDTO);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            logger.debug("<<<<mail id is null or empty, mailId : " + mailId);
        }
    }

    private void vehicleReceiveEditForm(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {

        System.out.println("in vehicleReceiveEditForm");
        Vm_requisitionDTO vm_requisitionDTO = null;
        try {
            vm_requisitionDTO = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(id);
            request.setAttribute("ID", vm_requisitionDTO.iD);
            request.setAttribute("vm_requisitionDTO", vm_requisitionDTO);
            request.setAttribute("vm_requisitionDAO", vm_requisitionDAO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "vm_requisition_pending/vm_requisitionInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "vm_requisition_pending/vm_requisitionSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "vm_receive/vehicleReceiveEditFormBody.jsp?actionType=edit";
                } else {
                    URL = "vm_receive/vehicleReceiveEditForm.jsp?actionType=edit";
                    //URL = "vm_requisition_pending/vm_requisitionEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void vehicleReceiveViewForm(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {

        System.out.println("in vehicleReceiveViewForm");
        Vm_requisitionDTO vm_requisitionDTO = null;
        try {
            vm_requisitionDTO = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(id);
            request.setAttribute("ID", vm_requisitionDTO.iD);
            request.setAttribute("vm_requisitionDTO", vm_requisitionDTO);
            request.setAttribute("vm_requisitionDAO", vm_requisitionDAO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "vm_requisition_pending/vm_requisitionInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "vm_requisition_pending/vm_requisitionSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "vm_receive/vm_receiveViewFormBody.jsp?actionType=edit";
                } else {
                    URL = "vm_receive/vm_receiveViewForm.jsp?actionType=edit";
                    //URL = "vm_requisition_pending/vm_requisitionEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getVm_requisition(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {
        System.out.println("in getVm_requisition");
        Vm_requisitionDTO vm_requisitionDTO = null;
        try {
            vm_requisitionDTO = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(id);
            request.setAttribute("ID", vm_requisitionDTO.iD);
            request.setAttribute("vm_requisitionDTO", vm_requisitionDTO);
            request.setAttribute("vm_requisitionDAO", vm_requisitionDAO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");
            String decision = request.getParameter("decision");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "vm_requisition_pending/vm_requisitionInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "vm_requisition_pending/vm_requisitionSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "vm_requisition_pending/vm_requisition" + decision + "EditBody.jsp?actionType=edit";
                } else {
                    URL = "vm_requisition_pending/vm_requisition" + decision + "Edit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getVm_requisition(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getVm_requisition(request, response, Long.parseLong(request.getParameter("ID")));
    }


    private void vehicleReceiveViewForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        vehicleReceiveViewForm(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void vehicleReceiveEditForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        vehicleReceiveEditForm(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void searchVm_requisition(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  searchVm_requisition 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        String status = request.getParameter("status");
        if (status == null || status.trim().isEmpty())
            filter = " status = " + CommonApprovalStatus.PENDING.getValue() + " ";

        long approverOrgId = userDTO.organogramID;
        List<Vm_requisition_approverDTO> approver = Vm_requisition_approverRepository.getInstance().getVm_requisition_approverDTOByapprover_org_id(approverOrgId);
        if (approver.isEmpty()) {
            if (filter != null && !filter.isEmpty()) filter += " AND ";
            filter += " FALSE ";
        }

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_VM_REQUISITION,
                request,
                vm_requisitionDAO,
                SessionConstants.VIEW_VM_REQUISITION,
                SessionConstants.SEARCH_VM_REQUISITION,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("vm_requisitionDAO", vm_requisitionDAO);
        RequestDispatcher rd;
        if (!isPermanent) {
            if (hasAjax == false) {
                System.out.println("Going to vm_requisition_pending/vm_requisitionApproval.jsp");
                rd = request.getRequestDispatcher("vm_requisition_pending/vm_requisitionApproval.jsp");
            } else {
                System.out.println("Going to vm_requisition_pending/vm_requisitionApprovalForm.jsp");
                rd = request.getRequestDispatcher("vm_requisition_pending/vm_requisitionApprovalForm.jsp");
            }
        } else {
            if (hasAjax == false) {
                System.out.println("Going to vm_requisition_pending/vm_requisitionSearch.jsp");
                rd = request.getRequestDispatcher("vm_requisition_pending/vm_requisitionSearch.jsp");
            } else {
                System.out.println("Going to vm_requisition_pending/vm_requisitionSearchForm.jsp");
                rd = request.getRequestDispatcher("vm_requisition_pending/vm_requisitionSearchForm.jsp");
            }
        }
        rd.forward(request, response);
    }

    private void prePendingSearchVm_requisition(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  pre pending searchVm_requisition 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        String status = request.getParameter("status");
        if (status == null || status.trim().isEmpty()) {
            filter = " ( status = " + CommonApprovalStatus.PRE_PENDING.getValue() + " ";
            filter += " OR first_approver_org_id = " + userDTO.organogramID + " ) ";
        }


        long officeUnitId = userDTO.unitID;
        OfficeUnitOrganograms unitHead = OfficeUnitOrganogramsRepository.getInstance().getOfficeUnitHeadWithActiveEmpStatusAndNotConsiderSubstitute(officeUnitId);

        if (filter != null && !filter.isEmpty()) filter += " AND ";
//		if (unitHead != null && unitHead.id == userDTO.organogramID) {
        if (true) {

            List<OfficeUnitOrganograms> inferiorUnitHeads = OfficeUnitOrganogramsRepository.getInstance().
                    getImmediateInferiorOfficeUnitHeadWithActiveEmpStatusByUnitHead(userDTO.organogramID);

            String inferiorOrgIds = inferiorUnitHeads.stream()
                    .map(officeUnitOrganograms -> String.valueOf(officeUnitOrganograms.id))
                    .collect(Collectors.joining(", "));

            if (inferiorOrgIds != null && !inferiorOrgIds.isEmpty()) {
                filter += " ( requester_org_id in  (" + inferiorOrgIds + ") ) OR ";
            }

            filter += " ( requester_office_unit_id = " + userDTO.unitID + " AND requester_org_id !=  " + userDTO.organogramID + " ) ";
        } else filter += " FALSE ";


        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_VM_REQUISITION,
                request,
                vm_requisitionDAO,
                SessionConstants.VIEW_VM_REQUISITION,
                SessionConstants.SEARCH_VM_REQUISITION,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("vm_requisitionDAO", vm_requisitionDAO);
        RequestDispatcher rd;
        if (!isPermanent) {
            if (hasAjax == false) {
                System.out.println("Going to vm_requisition_pending/vm_requisitionApproval.jsp");
                rd = request.getRequestDispatcher("vm_requisition_pending/vm_requisitionApproval.jsp");
            } else {
                System.out.println("Going to vm_requisition_pending/vm_requisitionApprovalForm.jsp");
                rd = request.getRequestDispatcher("vm_requisition_pending/vm_requisitionApprovalForm.jsp");
            }
        } else {
            if (hasAjax == false) {
                System.out.println("Going to vm_requisition_pending/vm_requisitionSearch.jsp");
                rd = request.getRequestDispatcher("vm_requisition_pre_pending/vm_requisitionPrePendingSearch.jsp");
            } else {
                System.out.println("Going to vm_requisition_pending/vm_requisitionSearchForm.jsp");
                rd = request.getRequestDispatcher("vm_requisition_pre_pending/vm_requisitionPrePendingSearchForm.jsp");
            }
        }
        rd.forward(request, response);
    }

    private void AddVm_receive(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  AddVm_receive 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_VM_REQUISITION,
                request,
                vm_requisitionDAO,
                SessionConstants.VIEW_VM_REQUISITION,
                SessionConstants.SEARCH_VM_REQUISITION,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("vm_requisitionDAO", vm_requisitionDAO);
        RequestDispatcher rd;
        if (!isPermanent) {
            if (hasAjax == false) {
                System.out.println("Going to vm_receive/vm_requisitionApproval.jsp");
                rd = request.getRequestDispatcher("vm_requisition_pending/vm_requisitionApproval.jsp");
            } else {
                System.out.println("Going to vm_requisition_pending/vm_requisitionApprovalForm.jsp");
                rd = request.getRequestDispatcher("vm_requisition_pending/vm_requisitionApprovalForm.jsp");
            }
        } else {
            if (hasAjax == false) {
                System.out.println("Going to vm_receive/vm_receiveSearchAndAdd.jsp");
                rd = request.getRequestDispatcher("vm_receive/vm_receiveSearchAndAdd.jsp");
            } else {
                System.out.println("Going to vm_receive/vm_receiveSearchAndAddForm.jsp");
                rd = request.getRequestDispatcher("vm_receive/vm_receiveSearchAndAddForm.jsp");
            }
        }
        rd.forward(request, response);
    }

    public void view(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String ID = request.getParameter("ID");
        String modal = request.getParameter("modal");
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        RequestDispatcher rd;
        System.out.println("View requested, tableName = " + tableName);
        if (isPermanentTable) {
            if (modal != null && modal.equalsIgnoreCase("1")) {
                rd = request.getRequestDispatcher("vm_requisition_pending" + "/" + tableName + "ViewModal.jsp?ID=" + ID);
            } else {
                rd = request.getRequestDispatcher("vm_requisition_pending" + "/" + tableName + "View.jsp?ID=" + ID);
            }
        } else {
            if (modal != null && modal.equalsIgnoreCase("1")) {
                rd = request.getRequestDispatcher("vm_requisition_pending" + "/" + tableName + "ApprovalModal.jsp?ID=" + ID);
            } else {
                rd = request.getRequestDispatcher("vm_requisition_pending" + "/" + tableName + "ApprovalView.jsp?ID=" + ID);
            }
        }
        rd.forward(request, response);

    }

}

