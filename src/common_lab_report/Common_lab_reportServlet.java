package common_lab_report;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import prescription_details.PrescriptionLabTestDTO;

import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;

import lab_test.*;


import com.google.gson.Gson;

import family.FamilyDTO;
import pb.*;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Common_lab_reportServlet
 */
@WebServlet("/Common_lab_reportServlet")
@MultipartConfig
public class Common_lab_reportServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Common_lab_reportServlet.class);

    String tableName = "common_lab_report";

	Common_lab_reportDAO common_lab_reportDAO;
	CommonRequestHandler commonRequestHandler;
	CommonLabReportDetailsDAO commonLabReportDetailsDAO;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Common_lab_reportServlet()
	{
        super();
    	try
    	{
			common_lab_reportDAO = new Common_lab_reportDAO(tableName);
			commonLabReportDetailsDAO = new CommonLabReportDetailsDAO("common_lab_report_details");
			commonRequestHandler = new CommonRequestHandler(common_lab_reportDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(userDTO.roleID == SessionConstants.LAB_TECHNITIAN_ROLE || userDTO.roleID == SessionConstants.PATHOLOGY_HEAD ||userDTO.roleID == SessionConstants.ADMIN_ROLE)
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(userDTO.roleID == SessionConstants.LAB_TECHNITIAN_ROLE || userDTO.roleID == SessionConstants.PATHOLOGY_HEAD || userDTO.roleID == SessionConstants.ADMIN_ROLE)
				{
					getCommon_lab_report(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getFormattedSearchPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.COMMON_LAB_REPORT_SEARCH)
						)
				{
					String userName = Utils.getDigitEnglishFromBangla(request.getParameter("userName"));
					if(!Utils.isValidUserName(userName))
					{
						return;
					}
					int whoIsThePatientCat = Integer.parseInt(request.getParameter("whoIsThePatientCat"));
					String filter = " appointment.employee_user_name = '" + userName + "' ";
					if(whoIsThePatientCat != FamilyDTO.UNSELECTED)
					{
						filter+= " and appointment.who_is_the_patient_cat =" + whoIsThePatientCat;
					}
					System.out.println("filter = " + filter);
					
					searchCommon_lab_report(request, response, isPermanentTable, filter);
					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.COMMON_LAB_REPORT_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchCommon_lab_report(request, response, isPermanentTable, filter);
						}
						else
						{
							searchCommon_lab_report(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchCommon_lab_report(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.COMMON_LAB_REPORT_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				/*else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}*/

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(userDTO.roleID == SessionConstants.LAB_TECHNITIAN_ROLE || userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.PATHOLOGY_HEAD)
				{
					System.out.println("going to  addCommon_lab_report ");
					addCommon_lab_report(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addCommon_lab_report ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.COMMON_LAB_REPORT_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addCommon_lab_report ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(userDTO.roleID == SessionConstants.LAB_TECHNITIAN_ROLE || userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.PATHOLOGY_HEAD)
				{
					addCommon_lab_report(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			
			else if(actionType.equals("delete"))
			{
				deleteCommon_lab_report(request, response, userDTO);
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.COMMON_LAB_REPORT_SEARCH))
				{
					searchCommon_lab_report(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Common_lab_reportDTO common_lab_reportDTO = common_lab_reportDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(common_lab_reportDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	
	private void addCommon_lab_report(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addCommon_lab_report");

			Common_lab_reportDTO common_lab_reportDTO;

			if(addFlag == true)
			{
				common_lab_reportDTO = new Common_lab_reportDTO();
			}
			else
			{
				common_lab_reportDTO = common_lab_reportDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("prescriptionLabTestId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("prescriptionLabTestId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				common_lab_reportDTO.prescriptionLabTestId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("labTestId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("labTestId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				common_lab_reportDTO.labTestId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("name");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("name = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				common_lab_reportDTO.name = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

	

			Value = request.getParameter("sex");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("sex = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				common_lab_reportDTO.sex = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("referredBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("referredBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				common_lab_reportDTO.referredBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("specimen");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("specimen = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				common_lab_reportDTO.specimen = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("comment");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.basic());
			}
			System.out.println("comment = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				common_lab_reportDTO.comment = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("reference");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.basic());
			}
			System.out.println("reference = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				common_lab_reportDTO.reference = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				common_lab_reportDTO.insertedByUserId = userDTO.ID;			
				common_lab_reportDTO.insertedByOrganogramId = userDTO.organogramID;
				common_lab_reportDTO.insertedByErId = userDTO.employee_record_id;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				common_lab_reportDTO.insertionDate = c.getTimeInMillis();
			}


			System.out.println("Done adding  addCommon_lab_report dto = " + common_lab_reportDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				common_lab_reportDAO.setIsDeleted(common_lab_reportDTO.iD, CommonDTO.OUTDATED);
				returnedID = common_lab_reportDAO.add(common_lab_reportDTO);
				common_lab_reportDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = common_lab_reportDAO.manageWriteOperations(common_lab_reportDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = common_lab_reportDAO.manageWriteOperations(common_lab_reportDTO, SessionConstants.UPDATE, -1, userDTO);
			}

			Lab_testDAO lab_testDAO = new Lab_testDAO();
			Lab_testDTO lab_testDTO = lab_testDAO.getDTOByID(common_lab_reportDTO.labTestId);

			System.out.println("Lab test name = " + lab_testDTO.nameEn);
			



			List<CommonLabReportDetailsDTO> commonLabReportDetailsDTOList = createCommonLabReportDetailsDTOListByRequest(request);
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(commonLabReportDetailsDTOList != null)
				{
					for(CommonLabReportDetailsDTO commonLabReportDetailsDTO: commonLabReportDetailsDTOList)
					{
						commonLabReportDetailsDTO.commonLabReportId = common_lab_reportDTO.iD;
						commonLabReportDetailsDAO.add(commonLabReportDetailsDTO);
					}
				}

			}
			else
			{
				List<Long> childIdsFromRequest = commonLabReportDetailsDAO.getChildIdsFromRequest(request, "commonLabReportDetails");
				//delete the removed children
				commonLabReportDetailsDAO.deleteChildrenNotInList("common_lab_report", "common_lab_report_details", common_lab_reportDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = commonLabReportDetailsDAO.getChilIds("common_lab_report", "common_lab_report_details", common_lab_reportDTO.iD);


				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							CommonLabReportDetailsDTO commonLabReportDetailsDTO =  createCommonLabReportDetailsDTOByRequestAndIndex(request, false, i);
							commonLabReportDetailsDTO.commonLabReportId = common_lab_reportDTO.iD;
							commonLabReportDetailsDAO.update(commonLabReportDetailsDTO);
						}
						else
						{
							CommonLabReportDetailsDTO commonLabReportDetailsDTO =  createCommonLabReportDetailsDTOByRequestAndIndex(request, true, i);
							commonLabReportDetailsDTO.commonLabReportId = common_lab_reportDTO.iD;
							commonLabReportDetailsDAO.add(commonLabReportDetailsDTO);
						}
					}
				}
				else
				{
					commonLabReportDetailsDAO.deleteChildrenByParent(common_lab_reportDTO.iD, "common_lab_report_id");
				}

			}
			
			int approvalStatus = Integer.parseInt(request.getParameter("approvalStatus"));
			System.out.println("Is approval, approvalStatus= " + approvalStatus);
			if((approvalStatus == PrescriptionLabTestDTO.APPROVED || approvalStatus == PrescriptionLabTestDTO.REJECTED) && userDTO.roleID == SessionConstants.PATHOLOGY_HEAD )
			{
				
				common_lab_reportDAO.approve(common_lab_reportDTO.iD, approvalStatus, userDTO);
			}

			response.sendRedirect("Common_lab_reportServlet?actionType=view&ID=" + common_lab_reportDTO.iD);
			

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}





	private List<CommonLabReportDetailsDTO> createCommonLabReportDetailsDTOListByRequest(HttpServletRequest request) throws Exception{
		List<CommonLabReportDetailsDTO> commonLabReportDetailsDTOList = new ArrayList<CommonLabReportDetailsDTO>();
		if(request.getParameterValues("commonLabReportDetails.iD") != null)
		{
			int commonLabReportDetailsItemNo = request.getParameterValues("commonLabReportDetails.iD").length;


			for(int index=0;index<commonLabReportDetailsItemNo;index++){
				CommonLabReportDetailsDTO commonLabReportDetailsDTO = createCommonLabReportDetailsDTOByRequestAndIndex(request,true,index);
				commonLabReportDetailsDTOList.add(commonLabReportDetailsDTO);
			}

			return commonLabReportDetailsDTOList;
		}
		return null;
	}


	private CommonLabReportDetailsDTO createCommonLabReportDetailsDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{

		CommonLabReportDetailsDTO commonLabReportDetailsDTO;
		if(addFlag == true )
		{
			commonLabReportDetailsDTO = new CommonLabReportDetailsDTO();
		}
		else
		{
			commonLabReportDetailsDTO = commonLabReportDetailsDAO.getDTOByID(Long.parseLong(request.getParameterValues("commonLabReportDetails.iD")[index]));
		}



		String Value = "";
		Value = request.getParameterValues("commonLabReportDetails.commonLabReportId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			commonLabReportDetailsDTO.commonLabReportId = Long.parseLong(Value);
		}

		
		Value = request.getParameterValues("commonLabReportDetails.labTestListId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			commonLabReportDetailsDTO.labTestListId = Long.parseLong(Value);
		}

		
		Value = request.getParameterValues("commonLabReportDetails.testName")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			commonLabReportDetailsDTO.testName = (Value);
		}

		
		Value = request.getParameterValues("commonLabReportDetails.standardValue")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			commonLabReportDetailsDTO.standardValue = (Value);
		}
		
		Value = request.getParameterValues("commonLabReportDetails.unit")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			commonLabReportDetailsDTO.unit = (Value);
		}

		
		Value = request.getParameterValues("commonLabReportDetails.testResult")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			commonLabReportDetailsDTO.testResult = (Value);
		}

		

		Value = request.getParameterValues("commonLabReportDetails.isTitle")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			commonLabReportDetailsDTO.isTitle = Boolean.parseBoolean(Value);
		}

		
		return commonLabReportDetailsDTO;

	}




	private void deleteCommon_lab_report(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException
	{
		try
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);


				Common_lab_reportDTO common_lab_reportDTO = common_lab_reportDAO.getDTOByID(id);
				common_lab_reportDAO.manageWriteOperations(common_lab_reportDTO, SessionConstants.DELETE, id, userDTO);
				commonLabReportDetailsDAO.deleteChildrenByParent(common_lab_reportDTO.iD, "common_lab_report_id");
				response.sendRedirect("Common_lab_reportServlet?actionType=search");

			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

	}

	private void getCommon_lab_report(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getCommon_lab_report");
		Common_lab_reportDTO common_lab_reportDTO = null;
		try
		{
			common_lab_reportDTO = common_lab_reportDAO.getDTOByID(id);
			request.setAttribute("ID", common_lab_reportDTO.iD);
			request.setAttribute("common_lab_reportDTO",common_lab_reportDTO);
			request.setAttribute("common_lab_reportDAO",common_lab_reportDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "common_lab_report/common_lab_reportInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "common_lab_report/common_lab_reportSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "common_lab_report/common_lab_reportEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "common_lab_report/common_lab_reportEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getCommon_lab_report(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getCommon_lab_report(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchCommon_lab_report(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchCommon_lab_report 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_COMMON_LAB_REPORT,
			request,
			common_lab_reportDAO,
			SessionConstants.VIEW_COMMON_LAB_REPORT,
			SessionConstants.SEARCH_COMMON_LAB_REPORT,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("common_lab_reportDAO",common_lab_reportDAO);
        RequestDispatcher rd;
        
        if(hasAjax == false)
        {
        	System.out.println("Going to common_lab_report/common_lab_reportSearch.jsp");
        	rd = request.getRequestDispatcher("common_lab_report/common_lab_reportSearch.jsp");
        }
        else
        {
        	System.out.println("Going to common_lab_report/common_lab_reportSearchForm.jsp");
        	rd = request.getRequestDispatcher("common_lab_report/common_lab_reportSearchForm.jsp");
        }
        
		rd.forward(request, response);
	}

}

