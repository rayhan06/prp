package common_lab_report;
import java.util.*;

import lab_test.LabTestListDTO;
import sessionmanager.SessionConstants;
import util.*; 


public class CommonLabReportDetailsDTO extends CommonDTO
{

	public long commonLabReportId = 0;
	public long labTestListId = 0;
    public String testName = "";
    public String standardValue = "";
    public String testResult = "";
    public String unit = "";
    public boolean isTitle;
    
    public void set(LabTestListDTO labTestListDTO)
    {
		isTitle = labTestListDTO.isTitle;
		testName = labTestListDTO.nameEn;
		unit = labTestListDTO.unit;
		standardValue = labTestListDTO.normalRange;
		labTestListId = labTestListDTO.iD;
		if(unit == null || unit.equalsIgnoreCase(""))
		{
			if(labTestListDTO.labTestId != SessionConstants.LAB_TEST_SERO)
			{
				testResult = standardValue;
			}
		}
    }
	
    @Override
	public String toString() {
            return "$CommonLabReportDetailsDTO[" +
            " iD = " + iD +
            " commonLabReportId = " + commonLabReportId +
            " labTestListId = " + labTestListId +
            " testName = " + testName +
            " standardValue = " + standardValue +
            " testResult = " + testResult +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}