package common_lab_report;
import java.util.*; 
import util.*; 


public class Common_lab_reportDTO extends CommonDTO
{

	public long prescriptionLabTestId = 0;
	public long labTestId = 0;
    public String name = "";
	public int age = 0;
    public String sex = "";
    public String referredBy = "";
    public String specimen = "";
    public String comment = "";
	public long insertedByUserId = 0;
	public long insertedByOrganogramId = 0;
	public long insertionDate = 0;
    public String labTestDetails= "";
    public String reference= "";
    
    public long insertedByErId = -1;
	
	public List<CommonLabReportDetailsDTO> commonLabReportDetailsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Common_lab_reportDTO[" +
            " iD = " + iD +
            " prescriptionLabTestId = " + prescriptionLabTestId +
            " labTestId = " + labTestId +
            " name = " + name +
            " age = " + age +
            " sex = " + sex +
            " referredBy = " + referredBy +
            " specimen = " + specimen +
            " comment = " + comment +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}