package common_lab_report;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import appointment.KeyCountDTO;
import lab_test.Lab_testDAO;
import lab_test.Lab_testDTO;
import repository.RepositoryManager;
import sessionmanager.SessionConstants;
import util.*;

import pb_notifications.Pb_notificationsDAO;
import prescription_details.PrescriptionLabTestDAO;
import prescription_details.PrescriptionLabTestDTO;
import prescription_details.Prescription_detailsDAO;
import prescription_details.Prescription_detailsDTO;
import user.UserDTO;
import user.UserRepository;

public class Common_lab_reportDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();
	Lab_testDAO lab_testDAO = new Lab_testDAO();
	Prescription_detailsDAO prescription_detailsDAO = new Prescription_detailsDAO();
	
	public Common_lab_reportDAO(String tableName)
	{
		super(tableName);
		joinSQL = " join prescription_lab_test on common_lab_report.prescription_lab_test_id = prescription_lab_test.id ";
		joinSQL += " join prescription_details on prescription_details.id = prescription_lab_test.prescription_details_id ";
		joinSQL += " join appointment on prescription_details.appointment_id = appointment.id ";
		commonMaps = new Common_lab_reportMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"prescription_lab_test_id",
			"lab_test_id",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"inserted_by_er_id",
			"insertion_date",
			"name",
			"age",
			"referred_by",
			"specimen",
			"sex",
			"comment",
			"reference",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Common_lab_reportDAO()
	{
		this("common_lab_report");		
	}
	
	public void setSearchColumn(Common_lab_reportDTO common_lab_reportDTO)
	{
		common_lab_reportDTO.searchColumn = "";
		common_lab_reportDTO.searchColumn += common_lab_reportDTO.name + " ";
		Lab_testDTO lab_testDTO = lab_testDAO.getDTOByID(common_lab_reportDTO.labTestId);
		common_lab_reportDTO.searchColumn += lab_testDTO.nameEn + " " + lab_testDTO.nameBn + "";
		
	}
	
	public KeyCountDTO getCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("lab_test_id");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public List<KeyCountDTO> getTestCount ()
    {
		String sql = "SELECT lab_test_id, count(id) FROM common_lab_report where isdeleted = 0 group by lab_test_id order by count(id) desc;";				
		return ConnectionAndStatementUtil.getListOfT(sql,this::getCount);	
    }
	
	public List<KeyCountDTO> getPathologyCount (long startDate, long endDate)
    {
		String sql = "SELECT lab_test_id, count(id) FROM common_lab_report "
				+ "where isdeleted = 0 and insertion_date >= " + startDate + " and insertion_date <= " + endDate + " "
				+ "and lab_test_id != " + SessionConstants.LAB_TEST_XRAY + " and lab_test_id != " + SessionConstants.LAB_TEST_ULTRASONOGRAM + " "
				+ "group by lab_test_id "
				+ "order by count(id) desc;";				
		return ConnectionAndStatementUtil.getListOfT(sql,this::getCount);	
    }
	
	public int getXRayCount (long startDate, long endDate)
    {
		String sql = "select SUM(CASE WHEN approval_status = 1 THEN 1 ELSE 0 END) as count from"
				+ " prescription_lab_test join prescription_details on prescription_details.id = prescription_lab_test.prescription_details_id"
				+ " where lab_test_type = " + SessionConstants.LAB_TEST_XRAY
				+ "  and prescription_details.visit_date >= " + startDate
				+ "  and prescription_details.visit_date <= " + endDate;	
		return ConnectionAndStatementUtil.getT(sql,this::getRawCount, 0);	
    }
	
	public int getRawCount(ResultSet rs)
	{
		int count = 0;
		try
		{
			count = rs.getInt("count");

			return count;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return 0;
		}
	}
	
	public void sendNoti(Common_lab_reportDTO common_lab_reportDTO, Prescription_detailsDTO prescription_detailsDTO)
	{
		Lab_testDTO lab_testDTO = lab_testDAO.getDTOByID(common_lab_reportDTO.labTestId);
		String englishText = "Your " + lab_testDTO.nameEn + " Report is ready.";
		String banglaText =  "আপনার " + lab_testDTO.nameEn + " রিপোর্ট রেডি।";


        String URL =  "Common_lab_reportServlet?actionType=view&ID=" + common_lab_reportDTO.iD;
		UserDTO patientDTO = UserRepository.getUserDTOByUserName(prescription_detailsDTO.employeeUserName);

		if(patientDTO != null)
		{
			pb_notificationsDAO.addPb_notificationsAndSendMailSMS(patientDTO.organogramID, System.currentTimeMillis(), URL, 
					englishText, banglaText, "Lab Report", prescription_detailsDTO.phone);
		}
	}
	
	public void approve(PrescriptionLabTestDTO prescriptionLabTestDTO, int approvalStatus, UserDTO approverDTO)
	{
		prescriptionLabTestDTO.approvalStatus = approvalStatus;
    	prescriptionLabTestDTO.approvalDate = System.currentTimeMillis();
    	prescriptionLabTestDTO.approverUserName = approverDTO.userName;
    	prescriptionLabTestDTO.approverOrganogramId = approverDTO.organogramID;
    	prescriptionLabTestDTO.actualDeliveryDate = System.currentTimeMillis();
	}
	
	public void approve(long id, int approvalStatus, UserDTO approverDTO) throws Exception
	{
		PrescriptionLabTestDAO prescriptionLabTestDAO = new PrescriptionLabTestDAO();
		Common_lab_reportDTO common_lab_reportDTO = getDTOByID(id);
		if(common_lab_reportDTO != null)
		{
		    PrescriptionLabTestDTO prescriptionLabTestDTO = prescriptionLabTestDAO.getDTOByID(common_lab_reportDTO.prescriptionLabTestId);

		    if(approverDTO != null)
		    {
		    	approve(prescriptionLabTestDTO, approvalStatus, approverDTO);		    	
		    	prescriptionLabTestDAO.update(prescriptionLabTestDTO);
		    	Prescription_detailsDTO prescription_detailsDTO = prescription_detailsDAO.getDTOByID(prescriptionLabTestDTO.prescriptionDetailsId);
		    	if(prescriptionLabTestDTO.approvalStatus == PrescriptionLabTestDTO.APPROVED)
		    	{
		    		sendNoti(common_lab_reportDTO, prescription_detailsDTO);
		    	}
		    }
		}
	}
	
	
	public long add(CommonDTO commonDTO) throws Exception {
		long id = super.add(commonDTO);
		Common_lab_reportDTO common_lab_reportDTO = (Common_lab_reportDTO)commonDTO;
		
		
		PrescriptionLabTestDAO prescriptionLabTestDAO = new PrescriptionLabTestDAO();
	    
	    PrescriptionLabTestDTO prescriptionLabTestDTO = prescriptionLabTestDAO.getDTOByID(common_lab_reportDTO.prescriptionLabTestId);
	    prescriptionLabTestDTO.isDone = true;
	    prescriptionLabTestDTO.testStatus = 1;
	    
	    UserDTO testerReportWriterDTO = UserRepository.getUserDTOByUserID(common_lab_reportDTO.insertedByUserId);
	    if(testerReportWriterDTO != null)
	    {
	    	prescriptionLabTestDTO.testerId = testerReportWriterDTO.userName;
	    	if(prescriptionLabTestDTO.labTestType != SessionConstants.LAB_TEST_XRAY && prescriptionLabTestDTO.labTestType != SessionConstants.LAB_TEST_ULTRASONOGRAM)
	    	{
	    		prescriptionLabTestDTO.xRayUserName = testerReportWriterDTO.userName;
	    		prescriptionLabTestDTO.testingDate = System.currentTimeMillis();
	    	}
	    }

	    if(prescriptionLabTestDTO.labTestType == SessionConstants.LAB_TEST_XRAY || prescriptionLabTestDTO.labTestType == SessionConstants.LAB_TEST_ULTRASONOGRAM)
	    {	    	
	    	approve(prescriptionLabTestDTO, PrescriptionLabTestDTO.APPROVED, testerReportWriterDTO);		
	    	prescriptionLabTestDTO.actualDeliveryDate = System.currentTimeMillis();
	    }
	    

	    prescriptionLabTestDAO.update(prescriptionLabTestDTO);
	    
	    Prescription_detailsDTO prescription_detailsDTO = prescription_detailsDAO.getDTOByID(prescriptionLabTestDTO.prescriptionDetailsId);
	    if(prescription_detailsDTO.canBeEdited)
	    {
	    	 prescription_detailsDTO.canBeEdited = false;
	 		 prescription_detailsDAO.update(prescription_detailsDTO);
	    }
	   

		try {
			if(prescriptionLabTestDTO.labTestType == SessionConstants.LAB_TEST_XRAY || prescriptionLabTestDTO.labTestType == SessionConstants.LAB_TEST_ULTRASONOGRAM)
		    {
				sendNoti(common_lab_reportDTO, prescription_detailsDTO);
		    }
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Common_lab_reportDTO common_lab_reportDTO = (Common_lab_reportDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(common_lab_reportDTO);
		if(isInsert)
		{
			ps.setObject(index++,common_lab_reportDTO.iD);
		}
		ps.setObject(index++,common_lab_reportDTO.prescriptionLabTestId);
		ps.setObject(index++,common_lab_reportDTO.labTestId);
		ps.setObject(index++,common_lab_reportDTO.insertedByUserId);
		ps.setObject(index++,common_lab_reportDTO.insertedByOrganogramId);
		ps.setObject(index++,common_lab_reportDTO.insertedByErId);
		ps.setObject(index++,common_lab_reportDTO.insertionDate);
		ps.setObject(index++,common_lab_reportDTO.name);
		ps.setObject(index++,common_lab_reportDTO.age);
		ps.setObject(index++,common_lab_reportDTO.referredBy);
		ps.setObject(index++,common_lab_reportDTO.specimen);
		ps.setObject(index++,common_lab_reportDTO.sex);
		ps.setObject(index++,common_lab_reportDTO.comment);
		ps.setObject(index++,common_lab_reportDTO.reference);
		ps.setObject(index++,common_lab_reportDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Common_lab_reportDTO build(ResultSet rs)
	{
		try
		{
			Common_lab_reportDTO common_lab_reportDTO = new Common_lab_reportDTO();
			common_lab_reportDTO.iD = rs.getLong("ID");
			common_lab_reportDTO.prescriptionLabTestId = rs.getLong("prescription_lab_test_id");
			common_lab_reportDTO.labTestId = rs.getLong("lab_test_id");
			common_lab_reportDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			common_lab_reportDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			common_lab_reportDTO.insertedByErId = rs.getLong("inserted_by_er_id");
			common_lab_reportDTO.insertionDate = rs.getLong("insertion_date");
			common_lab_reportDTO.name = rs.getString("name");
			common_lab_reportDTO.age = rs.getInt("age");
			common_lab_reportDTO.referredBy = rs.getString("referred_by");
			common_lab_reportDTO.specimen = rs.getString("specimen");
			common_lab_reportDTO.sex = rs.getString("sex");
			common_lab_reportDTO.comment = rs.getString("comment");
			common_lab_reportDTO.reference = rs.getString("reference");
			common_lab_reportDTO.searchColumn = rs.getString("search_column");
			common_lab_reportDTO.isDeleted = rs.getInt("isDeleted");
			common_lab_reportDTO.lastModificationTime = rs.getLong("lastModificationTime");
			if(common_lab_reportDTO.comment == null)
			{
				common_lab_reportDTO.comment = "";
			}
			if(common_lab_reportDTO.reference == null)
			{
				common_lab_reportDTO.reference = "";
			}
			return common_lab_reportDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	public long getTime(ResultSet rs)
	{
		try {
			return rs.getLong("lastModificationTime");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}	
	}

	public long getTimeByLabTestId (long prescription_lab_test_id) throws Exception
	{
		String sql = "SELECT lastModificationTime ";

		sql += " FROM " + tableName;
		
        sql += " WHERE isDeleted = 0 and prescription_lab_test_id=" + prescription_lab_test_id + " order by id desc";
        return ConnectionAndStatementUtil.getT(sql,this::getTime, 0L);
	}
	
	public long getId(ResultSet rs)
	{
		try {
			return rs.getLong("id");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1L;
		}	
	}

	
	public long getIdByLabTestId (long prescription_lab_test_id) throws Exception
	{
		String sql = "SELECT id ";

		sql += " FROM " + tableName;
		
        sql += " WHERE isDeleted = 0 and prescription_lab_test_id=" + prescription_lab_test_id + " order by id desc";
        return ConnectionAndStatementUtil.getT(sql,this::getId, -1L);			
	}
	
	

	public Common_lab_reportDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Common_lab_reportDTO common_lab_reportDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		try {
			CommonLabReportDetailsDAO commonLabReportDetailsDAO = new CommonLabReportDetailsDAO("common_lab_report_details");			
			List<CommonLabReportDetailsDTO> commonLabReportDetailsDTOList = commonLabReportDetailsDAO.getCommonLabReportDetailsDTOListByCommonLabReportID(common_lab_reportDTO.iD);
			common_lab_reportDTO.commonLabReportDetailsDTOList = commonLabReportDetailsDTOList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
		return common_lab_reportDTO;
	}

	
	public List<Common_lab_reportDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<Common_lab_reportDTO> getAllCommon_lab_report (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Common_lab_reportDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Common_lab_reportDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name")
						|| str.equals("referred_by")
						|| str.equals("specimen")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name"))
					{
						AllFieldSql += "" + tableName + ".name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("referred_by"))
					{
						AllFieldSql += "" + tableName + ".referred_by like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("specimen"))
					{
						AllFieldSql += "" + tableName + ".specimen like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	