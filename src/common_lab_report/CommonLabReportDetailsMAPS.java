package common_lab_report;
import java.util.*; 
import util.*;


public class CommonLabReportDetailsMAPS extends CommonMaps
{	
	public CommonLabReportDetailsMAPS(String tableName)
	{
		
		java_allfield_type_map.put("common_lab_report_id".toLowerCase(), "Long");
		java_allfield_type_map.put("lab_test_list_id".toLowerCase(), "Long");
		java_allfield_type_map.put("test_name".toLowerCase(), "String");
		java_allfield_type_map.put("standard_value".toLowerCase(), "String");
		java_allfield_type_map.put("test_result".toLowerCase(), "String");


		java_anyfield_search_map.put(tableName + ".common_lab_report_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".lab_test_list_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".test_name".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".standard_value".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".test_result".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("commonLabReportId".toLowerCase(), "commonLabReportId".toLowerCase());
		java_DTO_map.put("labTestListId".toLowerCase(), "labTestListId".toLowerCase());
		java_DTO_map.put("testName".toLowerCase(), "testName".toLowerCase());
		java_DTO_map.put("standardValue".toLowerCase(), "standardValue".toLowerCase());
		java_DTO_map.put("testResult".toLowerCase(), "testResult".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("common_lab_report_id".toLowerCase(), "commonLabReportId".toLowerCase());
		java_SQL_map.put("lab_test_list_id".toLowerCase(), "labTestListId".toLowerCase());
		java_SQL_map.put("test_name".toLowerCase(), "testName".toLowerCase());
		java_SQL_map.put("standard_value".toLowerCase(), "standardValue".toLowerCase());
		java_SQL_map.put("test_result".toLowerCase(), "testResult".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Common Lab Report Id".toLowerCase(), "commonLabReportId".toLowerCase());
		java_Text_map.put("Lab Test List Id".toLowerCase(), "labTestListId".toLowerCase());
		java_Text_map.put("Test Name".toLowerCase(), "testName".toLowerCase());
		java_Text_map.put("Standard Value".toLowerCase(), "standardValue".toLowerCase());
		java_Text_map.put("Test Result".toLowerCase(), "testResult".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}