package common_lab_report;


import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.*;

import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;



import repository.RepositoryManager;

import util.*;

import user.UserDTO;

public class CommonLabReportDetailsDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public CommonLabReportDetailsDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new CommonLabReportDetailsMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"common_lab_report_id",
			"lab_test_list_id",
			"test_result",
			"test_name",
			"standard_value",
			"unit",
			"is_title",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public CommonLabReportDetailsDAO()
	{
		this("common_lab_report_details");		
	}
	
	public void setSearchColumn(CommonLabReportDetailsDTO commonlabreportdetailsDTO)
	{
		commonlabreportdetailsDTO.searchColumn = "";
		commonlabreportdetailsDTO.searchColumn += commonlabreportdetailsDTO.testResult + " ";
		commonlabreportdetailsDTO.searchColumn += commonlabreportdetailsDTO.testName + " ";
		commonlabreportdetailsDTO.searchColumn += commonlabreportdetailsDTO.standardValue + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		CommonLabReportDetailsDTO commonlabreportdetailsDTO = (CommonLabReportDetailsDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(commonlabreportdetailsDTO);
		if(isInsert)
		{
			ps.setObject(index++,commonlabreportdetailsDTO.iD);
		}
		ps.setObject(index++,commonlabreportdetailsDTO.commonLabReportId);
		ps.setObject(index++,commonlabreportdetailsDTO.labTestListId);
		ps.setObject(index++,commonlabreportdetailsDTO.testResult);
		ps.setObject(index++,commonlabreportdetailsDTO.testName);
		ps.setObject(index++,commonlabreportdetailsDTO.standardValue);
		ps.setObject(index++,commonlabreportdetailsDTO.unit);
		ps.setObject(index++,commonlabreportdetailsDTO.isTitle);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public CommonLabReportDetailsDTO build(ResultSet rs)
	{
		try
		{
			CommonLabReportDetailsDTO commonlabreportdetailsDTO = new CommonLabReportDetailsDTO();
			commonlabreportdetailsDTO.iD = rs.getLong("ID");
			commonlabreportdetailsDTO.commonLabReportId = rs.getLong("common_lab_report_id");
			commonlabreportdetailsDTO.labTestListId = rs.getLong("lab_test_list_id");
			commonlabreportdetailsDTO.testResult = rs.getString("test_result");
			commonlabreportdetailsDTO.testName = rs.getString("test_name");
			commonlabreportdetailsDTO.standardValue = rs.getString("standard_value");
			commonlabreportdetailsDTO.unit = rs.getString("unit");
			if(commonlabreportdetailsDTO.unit == null)
			{
				commonlabreportdetailsDTO.unit = "";
			}
			commonlabreportdetailsDTO.isTitle = rs.getBoolean("is_title");
			commonlabreportdetailsDTO.isDeleted = rs.getInt("isDeleted");
			commonlabreportdetailsDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return commonlabreportdetailsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	
	public List<CommonLabReportDetailsDTO> getCommonLabReportDetailsDTOListByCommonLabReportID(long commonLabReportID) throws Exception
	{
		String sql = "SELECT * FROM common_lab_report_details where isDeleted=0 and common_lab_report_id="+commonLabReportID+" order by common_lab_report_details.lastModificationTime";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}

	public CommonLabReportDetailsDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		CommonLabReportDetailsDTO commonlabreportdetailsDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return commonlabreportdetailsDTO;
	}

	
	public List<CommonLabReportDetailsDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<CommonLabReportDetailsDTO> getAllCommonLabReportDetails (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<CommonLabReportDetailsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<CommonLabReportDetailsDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
				
}
	