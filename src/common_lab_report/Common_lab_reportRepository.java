package common_lab_report;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Common_lab_reportRepository implements Repository {
	Common_lab_reportDAO common_lab_reportDAO = null;
	
	public void setDAO(Common_lab_reportDAO common_lab_reportDAO)
	{
		this.common_lab_reportDAO = common_lab_reportDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Common_lab_reportRepository.class);
	Map<Long, Common_lab_reportDTO>mapOfCommon_lab_reportDTOToiD;
	Map<Long, Set<Common_lab_reportDTO> >mapOfCommon_lab_reportDTOToprescriptionLabTestId;
	Map<Long, Set<Common_lab_reportDTO> >mapOfCommon_lab_reportDTOTolabTestId;
	Map<String, Set<Common_lab_reportDTO> >mapOfCommon_lab_reportDTOToname;
	Map<Integer, Set<Common_lab_reportDTO> >mapOfCommon_lab_reportDTOToage;
	Map<String, Set<Common_lab_reportDTO> >mapOfCommon_lab_reportDTOTosex;
	Map<String, Set<Common_lab_reportDTO> >mapOfCommon_lab_reportDTOToreferredBy;
	Map<String, Set<Common_lab_reportDTO> >mapOfCommon_lab_reportDTOTospecimen;
	Map<String, Set<Common_lab_reportDTO> >mapOfCommon_lab_reportDTOTocomment;
	Map<Long, Set<Common_lab_reportDTO> >mapOfCommon_lab_reportDTOToinsertedByUserId;
	Map<Long, Set<Common_lab_reportDTO> >mapOfCommon_lab_reportDTOToinsertedByOrganogramId;
	Map<Long, Set<Common_lab_reportDTO> >mapOfCommon_lab_reportDTOToinsertionDate;
	Map<String, Set<Common_lab_reportDTO> >mapOfCommon_lab_reportDTOTosearchColumn;
	Map<Long, Set<Common_lab_reportDTO> >mapOfCommon_lab_reportDTOTolastModificationTime;


	static Common_lab_reportRepository instance = null;  
	private Common_lab_reportRepository(){
		mapOfCommon_lab_reportDTOToiD = new ConcurrentHashMap<>();
		mapOfCommon_lab_reportDTOToprescriptionLabTestId = new ConcurrentHashMap<>();
		mapOfCommon_lab_reportDTOTolabTestId = new ConcurrentHashMap<>();
		mapOfCommon_lab_reportDTOToname = new ConcurrentHashMap<>();
		mapOfCommon_lab_reportDTOToage = new ConcurrentHashMap<>();
		mapOfCommon_lab_reportDTOTosex = new ConcurrentHashMap<>();
		mapOfCommon_lab_reportDTOToreferredBy = new ConcurrentHashMap<>();
		mapOfCommon_lab_reportDTOTospecimen = new ConcurrentHashMap<>();
		mapOfCommon_lab_reportDTOTocomment = new ConcurrentHashMap<>();
		mapOfCommon_lab_reportDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfCommon_lab_reportDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfCommon_lab_reportDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfCommon_lab_reportDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfCommon_lab_reportDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Common_lab_reportRepository getInstance(){
		if (instance == null){
			instance = new Common_lab_reportRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(common_lab_reportDAO == null)
		{
			return;
		}
		try {
			List<Common_lab_reportDTO> common_lab_reportDTOs = common_lab_reportDAO.getAllCommon_lab_report(reloadAll);
			for(Common_lab_reportDTO common_lab_reportDTO : common_lab_reportDTOs) {
				Common_lab_reportDTO oldCommon_lab_reportDTO = getCommon_lab_reportDTOByID(common_lab_reportDTO.iD);
				if( oldCommon_lab_reportDTO != null ) {
					mapOfCommon_lab_reportDTOToiD.remove(oldCommon_lab_reportDTO.iD);
				
					if(mapOfCommon_lab_reportDTOToprescriptionLabTestId.containsKey(oldCommon_lab_reportDTO.prescriptionLabTestId)) {
						mapOfCommon_lab_reportDTOToprescriptionLabTestId.get(oldCommon_lab_reportDTO.prescriptionLabTestId).remove(oldCommon_lab_reportDTO);
					}
					if(mapOfCommon_lab_reportDTOToprescriptionLabTestId.get(oldCommon_lab_reportDTO.prescriptionLabTestId).isEmpty()) {
						mapOfCommon_lab_reportDTOToprescriptionLabTestId.remove(oldCommon_lab_reportDTO.prescriptionLabTestId);
					}
					
					if(mapOfCommon_lab_reportDTOTolabTestId.containsKey(oldCommon_lab_reportDTO.labTestId)) {
						mapOfCommon_lab_reportDTOTolabTestId.get(oldCommon_lab_reportDTO.labTestId).remove(oldCommon_lab_reportDTO);
					}
					if(mapOfCommon_lab_reportDTOTolabTestId.get(oldCommon_lab_reportDTO.labTestId).isEmpty()) {
						mapOfCommon_lab_reportDTOTolabTestId.remove(oldCommon_lab_reportDTO.labTestId);
					}
					
					if(mapOfCommon_lab_reportDTOToname.containsKey(oldCommon_lab_reportDTO.name)) {
						mapOfCommon_lab_reportDTOToname.get(oldCommon_lab_reportDTO.name).remove(oldCommon_lab_reportDTO);
					}
					if(mapOfCommon_lab_reportDTOToname.get(oldCommon_lab_reportDTO.name).isEmpty()) {
						mapOfCommon_lab_reportDTOToname.remove(oldCommon_lab_reportDTO.name);
					}
					
					if(mapOfCommon_lab_reportDTOToage.containsKey(oldCommon_lab_reportDTO.age)) {
						mapOfCommon_lab_reportDTOToage.get(oldCommon_lab_reportDTO.age).remove(oldCommon_lab_reportDTO);
					}
					if(mapOfCommon_lab_reportDTOToage.get(oldCommon_lab_reportDTO.age).isEmpty()) {
						mapOfCommon_lab_reportDTOToage.remove(oldCommon_lab_reportDTO.age);
					}
					
					if(mapOfCommon_lab_reportDTOTosex.containsKey(oldCommon_lab_reportDTO.sex)) {
						mapOfCommon_lab_reportDTOTosex.get(oldCommon_lab_reportDTO.sex).remove(oldCommon_lab_reportDTO);
					}
					if(mapOfCommon_lab_reportDTOTosex.get(oldCommon_lab_reportDTO.sex).isEmpty()) {
						mapOfCommon_lab_reportDTOTosex.remove(oldCommon_lab_reportDTO.sex);
					}
					
					if(mapOfCommon_lab_reportDTOToreferredBy.containsKey(oldCommon_lab_reportDTO.referredBy)) {
						mapOfCommon_lab_reportDTOToreferredBy.get(oldCommon_lab_reportDTO.referredBy).remove(oldCommon_lab_reportDTO);
					}
					if(mapOfCommon_lab_reportDTOToreferredBy.get(oldCommon_lab_reportDTO.referredBy).isEmpty()) {
						mapOfCommon_lab_reportDTOToreferredBy.remove(oldCommon_lab_reportDTO.referredBy);
					}
					
					if(mapOfCommon_lab_reportDTOTospecimen.containsKey(oldCommon_lab_reportDTO.specimen)) {
						mapOfCommon_lab_reportDTOTospecimen.get(oldCommon_lab_reportDTO.specimen).remove(oldCommon_lab_reportDTO);
					}
					if(mapOfCommon_lab_reportDTOTospecimen.get(oldCommon_lab_reportDTO.specimen).isEmpty()) {
						mapOfCommon_lab_reportDTOTospecimen.remove(oldCommon_lab_reportDTO.specimen);
					}
					
					if(mapOfCommon_lab_reportDTOTocomment.containsKey(oldCommon_lab_reportDTO.comment)) {
						mapOfCommon_lab_reportDTOTocomment.get(oldCommon_lab_reportDTO.comment).remove(oldCommon_lab_reportDTO);
					}
					if(mapOfCommon_lab_reportDTOTocomment.get(oldCommon_lab_reportDTO.comment).isEmpty()) {
						mapOfCommon_lab_reportDTOTocomment.remove(oldCommon_lab_reportDTO.comment);
					}
					
					if(mapOfCommon_lab_reportDTOToinsertedByUserId.containsKey(oldCommon_lab_reportDTO.insertedByUserId)) {
						mapOfCommon_lab_reportDTOToinsertedByUserId.get(oldCommon_lab_reportDTO.insertedByUserId).remove(oldCommon_lab_reportDTO);
					}
					if(mapOfCommon_lab_reportDTOToinsertedByUserId.get(oldCommon_lab_reportDTO.insertedByUserId).isEmpty()) {
						mapOfCommon_lab_reportDTOToinsertedByUserId.remove(oldCommon_lab_reportDTO.insertedByUserId);
					}
					
					if(mapOfCommon_lab_reportDTOToinsertedByOrganogramId.containsKey(oldCommon_lab_reportDTO.insertedByOrganogramId)) {
						mapOfCommon_lab_reportDTOToinsertedByOrganogramId.get(oldCommon_lab_reportDTO.insertedByOrganogramId).remove(oldCommon_lab_reportDTO);
					}
					if(mapOfCommon_lab_reportDTOToinsertedByOrganogramId.get(oldCommon_lab_reportDTO.insertedByOrganogramId).isEmpty()) {
						mapOfCommon_lab_reportDTOToinsertedByOrganogramId.remove(oldCommon_lab_reportDTO.insertedByOrganogramId);
					}
					
					if(mapOfCommon_lab_reportDTOToinsertionDate.containsKey(oldCommon_lab_reportDTO.insertionDate)) {
						mapOfCommon_lab_reportDTOToinsertionDate.get(oldCommon_lab_reportDTO.insertionDate).remove(oldCommon_lab_reportDTO);
					}
					if(mapOfCommon_lab_reportDTOToinsertionDate.get(oldCommon_lab_reportDTO.insertionDate).isEmpty()) {
						mapOfCommon_lab_reportDTOToinsertionDate.remove(oldCommon_lab_reportDTO.insertionDate);
					}
					
					if(mapOfCommon_lab_reportDTOTosearchColumn.containsKey(oldCommon_lab_reportDTO.searchColumn)) {
						mapOfCommon_lab_reportDTOTosearchColumn.get(oldCommon_lab_reportDTO.searchColumn).remove(oldCommon_lab_reportDTO);
					}
					if(mapOfCommon_lab_reportDTOTosearchColumn.get(oldCommon_lab_reportDTO.searchColumn).isEmpty()) {
						mapOfCommon_lab_reportDTOTosearchColumn.remove(oldCommon_lab_reportDTO.searchColumn);
					}
					
					if(mapOfCommon_lab_reportDTOTolastModificationTime.containsKey(oldCommon_lab_reportDTO.lastModificationTime)) {
						mapOfCommon_lab_reportDTOTolastModificationTime.get(oldCommon_lab_reportDTO.lastModificationTime).remove(oldCommon_lab_reportDTO);
					}
					if(mapOfCommon_lab_reportDTOTolastModificationTime.get(oldCommon_lab_reportDTO.lastModificationTime).isEmpty()) {
						mapOfCommon_lab_reportDTOTolastModificationTime.remove(oldCommon_lab_reportDTO.lastModificationTime);
					}
					
					
				}
				if(common_lab_reportDTO.isDeleted == 0) 
				{
					
					mapOfCommon_lab_reportDTOToiD.put(common_lab_reportDTO.iD, common_lab_reportDTO);
				
					if( ! mapOfCommon_lab_reportDTOToprescriptionLabTestId.containsKey(common_lab_reportDTO.prescriptionLabTestId)) {
						mapOfCommon_lab_reportDTOToprescriptionLabTestId.put(common_lab_reportDTO.prescriptionLabTestId, new HashSet<>());
					}
					mapOfCommon_lab_reportDTOToprescriptionLabTestId.get(common_lab_reportDTO.prescriptionLabTestId).add(common_lab_reportDTO);
					
					if( ! mapOfCommon_lab_reportDTOTolabTestId.containsKey(common_lab_reportDTO.labTestId)) {
						mapOfCommon_lab_reportDTOTolabTestId.put(common_lab_reportDTO.labTestId, new HashSet<>());
					}
					mapOfCommon_lab_reportDTOTolabTestId.get(common_lab_reportDTO.labTestId).add(common_lab_reportDTO);
					
					if( ! mapOfCommon_lab_reportDTOToname.containsKey(common_lab_reportDTO.name)) {
						mapOfCommon_lab_reportDTOToname.put(common_lab_reportDTO.name, new HashSet<>());
					}
					mapOfCommon_lab_reportDTOToname.get(common_lab_reportDTO.name).add(common_lab_reportDTO);
					
					if( ! mapOfCommon_lab_reportDTOToage.containsKey(common_lab_reportDTO.age)) {
						mapOfCommon_lab_reportDTOToage.put(common_lab_reportDTO.age, new HashSet<>());
					}
					mapOfCommon_lab_reportDTOToage.get(common_lab_reportDTO.age).add(common_lab_reportDTO);
					
					if( ! mapOfCommon_lab_reportDTOTosex.containsKey(common_lab_reportDTO.sex)) {
						mapOfCommon_lab_reportDTOTosex.put(common_lab_reportDTO.sex, new HashSet<>());
					}
					mapOfCommon_lab_reportDTOTosex.get(common_lab_reportDTO.sex).add(common_lab_reportDTO);
					
					if( ! mapOfCommon_lab_reportDTOToreferredBy.containsKey(common_lab_reportDTO.referredBy)) {
						mapOfCommon_lab_reportDTOToreferredBy.put(common_lab_reportDTO.referredBy, new HashSet<>());
					}
					mapOfCommon_lab_reportDTOToreferredBy.get(common_lab_reportDTO.referredBy).add(common_lab_reportDTO);
					
					if( ! mapOfCommon_lab_reportDTOTospecimen.containsKey(common_lab_reportDTO.specimen)) {
						mapOfCommon_lab_reportDTOTospecimen.put(common_lab_reportDTO.specimen, new HashSet<>());
					}
					mapOfCommon_lab_reportDTOTospecimen.get(common_lab_reportDTO.specimen).add(common_lab_reportDTO);
					
					if( ! mapOfCommon_lab_reportDTOTocomment.containsKey(common_lab_reportDTO.comment)) {
						mapOfCommon_lab_reportDTOTocomment.put(common_lab_reportDTO.comment, new HashSet<>());
					}
					mapOfCommon_lab_reportDTOTocomment.get(common_lab_reportDTO.comment).add(common_lab_reportDTO);
					
					if( ! mapOfCommon_lab_reportDTOToinsertedByUserId.containsKey(common_lab_reportDTO.insertedByUserId)) {
						mapOfCommon_lab_reportDTOToinsertedByUserId.put(common_lab_reportDTO.insertedByUserId, new HashSet<>());
					}
					mapOfCommon_lab_reportDTOToinsertedByUserId.get(common_lab_reportDTO.insertedByUserId).add(common_lab_reportDTO);
					
					if( ! mapOfCommon_lab_reportDTOToinsertedByOrganogramId.containsKey(common_lab_reportDTO.insertedByOrganogramId)) {
						mapOfCommon_lab_reportDTOToinsertedByOrganogramId.put(common_lab_reportDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfCommon_lab_reportDTOToinsertedByOrganogramId.get(common_lab_reportDTO.insertedByOrganogramId).add(common_lab_reportDTO);
					
					if( ! mapOfCommon_lab_reportDTOToinsertionDate.containsKey(common_lab_reportDTO.insertionDate)) {
						mapOfCommon_lab_reportDTOToinsertionDate.put(common_lab_reportDTO.insertionDate, new HashSet<>());
					}
					mapOfCommon_lab_reportDTOToinsertionDate.get(common_lab_reportDTO.insertionDate).add(common_lab_reportDTO);
					
					if( ! mapOfCommon_lab_reportDTOTosearchColumn.containsKey(common_lab_reportDTO.searchColumn)) {
						mapOfCommon_lab_reportDTOTosearchColumn.put(common_lab_reportDTO.searchColumn, new HashSet<>());
					}
					mapOfCommon_lab_reportDTOTosearchColumn.get(common_lab_reportDTO.searchColumn).add(common_lab_reportDTO);
					
					if( ! mapOfCommon_lab_reportDTOTolastModificationTime.containsKey(common_lab_reportDTO.lastModificationTime)) {
						mapOfCommon_lab_reportDTOTolastModificationTime.put(common_lab_reportDTO.lastModificationTime, new HashSet<>());
					}
					mapOfCommon_lab_reportDTOTolastModificationTime.get(common_lab_reportDTO.lastModificationTime).add(common_lab_reportDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Common_lab_reportDTO> getCommon_lab_reportList() {
		List <Common_lab_reportDTO> common_lab_reports = new ArrayList<Common_lab_reportDTO>(this.mapOfCommon_lab_reportDTOToiD.values());
		return common_lab_reports;
	}
	
	
	public Common_lab_reportDTO getCommon_lab_reportDTOByID( long ID){
		return mapOfCommon_lab_reportDTOToiD.get(ID);
	}
	
	
	public List<Common_lab_reportDTO> getCommon_lab_reportDTOByprescription_lab_test_id(long prescription_lab_test_id) {
		return new ArrayList<>( mapOfCommon_lab_reportDTOToprescriptionLabTestId.getOrDefault(prescription_lab_test_id,new HashSet<>()));
	}
	
	
	public List<Common_lab_reportDTO> getCommon_lab_reportDTOBylab_test_id(long lab_test_id) {
		return new ArrayList<>( mapOfCommon_lab_reportDTOTolabTestId.getOrDefault(lab_test_id,new HashSet<>()));
	}
	
	
	public List<Common_lab_reportDTO> getCommon_lab_reportDTOByname(String name) {
		return new ArrayList<>( mapOfCommon_lab_reportDTOToname.getOrDefault(name,new HashSet<>()));
	}
	
	
	public List<Common_lab_reportDTO> getCommon_lab_reportDTOByage(int age) {
		return new ArrayList<>( mapOfCommon_lab_reportDTOToage.getOrDefault(age,new HashSet<>()));
	}
	
	
	public List<Common_lab_reportDTO> getCommon_lab_reportDTOBysex(String sex) {
		return new ArrayList<>( mapOfCommon_lab_reportDTOTosex.getOrDefault(sex,new HashSet<>()));
	}
	
	
	public List<Common_lab_reportDTO> getCommon_lab_reportDTOByreferred_by(String referred_by) {
		return new ArrayList<>( mapOfCommon_lab_reportDTOToreferredBy.getOrDefault(referred_by,new HashSet<>()));
	}
	
	
	public List<Common_lab_reportDTO> getCommon_lab_reportDTOByspecimen(String specimen) {
		return new ArrayList<>( mapOfCommon_lab_reportDTOTospecimen.getOrDefault(specimen,new HashSet<>()));
	}
	
	
	public List<Common_lab_reportDTO> getCommon_lab_reportDTOBycomment(String comment) {
		return new ArrayList<>( mapOfCommon_lab_reportDTOTocomment.getOrDefault(comment,new HashSet<>()));
	}
	
	
	public List<Common_lab_reportDTO> getCommon_lab_reportDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfCommon_lab_reportDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Common_lab_reportDTO> getCommon_lab_reportDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfCommon_lab_reportDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Common_lab_reportDTO> getCommon_lab_reportDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfCommon_lab_reportDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Common_lab_reportDTO> getCommon_lab_reportDTOBysearch_column(String search_column) {
		return new ArrayList<>( mapOfCommon_lab_reportDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
	}
	
	
	public List<Common_lab_reportDTO> getCommon_lab_reportDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfCommon_lab_reportDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "common_lab_report";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


