package common_lab_report;
import java.util.*; 
import util.*;


public class Common_lab_reportMAPS extends CommonMaps
{	
	public Common_lab_reportMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("prescriptionLabTestId".toLowerCase(), "prescriptionLabTestId".toLowerCase());
		java_DTO_map.put("labTestId".toLowerCase(), "labTestId".toLowerCase());
		java_DTO_map.put("name".toLowerCase(), "name".toLowerCase());
		java_DTO_map.put("age".toLowerCase(), "age".toLowerCase());
		java_DTO_map.put("sex".toLowerCase(), "sex".toLowerCase());
		java_DTO_map.put("referredBy".toLowerCase(), "referredBy".toLowerCase());
		java_DTO_map.put("specimen".toLowerCase(), "specimen".toLowerCase());
		java_DTO_map.put("comment".toLowerCase(), "comment".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("prescription_lab_test_id".toLowerCase(), "prescriptionLabTestId".toLowerCase());
		java_SQL_map.put("lab_test_id".toLowerCase(), "labTestId".toLowerCase());
		java_SQL_map.put("name".toLowerCase(), "name".toLowerCase());
		java_SQL_map.put("age".toLowerCase(), "age".toLowerCase());
		java_SQL_map.put("sex".toLowerCase(), "sex".toLowerCase());
		java_SQL_map.put("referred_by".toLowerCase(), "referredBy".toLowerCase());
		java_SQL_map.put("specimen".toLowerCase(), "specimen".toLowerCase());
		java_SQL_map.put("comment".toLowerCase(), "comment".toLowerCase());
		java_SQL_map.put("inserted_by_user_id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_SQL_map.put("inserted_by_organogram_id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_SQL_map.put("insertion_date".toLowerCase(), "insertionDate".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Prescription Lab Test Id".toLowerCase(), "prescriptionLabTestId".toLowerCase());
		java_Text_map.put("Lab Test Id".toLowerCase(), "labTestId".toLowerCase());
		java_Text_map.put("Name".toLowerCase(), "name".toLowerCase());
		java_Text_map.put("Age".toLowerCase(), "age".toLowerCase());
		java_Text_map.put("Sex".toLowerCase(), "sex".toLowerCase());
		java_Text_map.put("Referred By".toLowerCase(), "referredBy".toLowerCase());
		java_Text_map.put("Specimen".toLowerCase(), "specimen".toLowerCase());
		java_Text_map.put("Comment".toLowerCase(), "comment".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}