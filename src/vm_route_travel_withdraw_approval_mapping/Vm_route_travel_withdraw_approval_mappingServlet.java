package vm_route_travel_withdraw_approval_mapping;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import card_info.CardApprovalResponse;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import task_type.TaskTypeEnum;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import vm_fuel_request.*;
import vm_fuel_request_approval_mapping.*;
import vm_requisition.CommonApprovalStatus;
import vm_route_travel.Vm_route_travelDAO;
import vm_route_travel.Vm_route_travelDTO;
import vm_route_travel_withdraw.Vm_route_travel_withdrawDAO;
import vm_route_travel_withdraw.Vm_route_travel_withdrawDTO;
import vm_route_travel_withdraw.Vm_route_travel_withdrawRepository;
import vm_route_travel_withdraw.Vm_route_travel_withdrawServlet;
import vm_route_travel_withdraw_approval.Vm_route_travel_withdraw_approvalDTO;
import vm_route_travel_withdraw_approval.Vm_route_travel_withdraw_approvalRepository;


/**
 * Servlet implementation class Vm_route_travel_withdraw_approval_mappingServlet
 */
@WebServlet("/Vm_route_travel_withdraw_approval_mappingServlet")
@MultipartConfig
public class Vm_route_travel_withdraw_approval_mappingServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(Vm_route_travel_withdraw_approval_mappingServlet.class);

	private static final String tableName = "vm_route_travel_withdraw_approval_mapping";

	private final Vm_route_travel_withdraw_approval_mappingDAO card_approval_mappingDAO;

	public Vm_route_travel_withdraw_approval_mappingServlet() {
		super();
		card_approval_mappingDAO = new Vm_route_travel_withdraw_approval_mappingDAO(tableName);
	}



	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		if (userDTO == null) {
			request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			return;
		}
		try {
			String actionType = request.getParameter("actionType");
			logger.debug("actionType : "+actionType);
			switch (actionType) {
				case "getApprovalPage":
					long cardInfoId = Long.parseLong(request.getParameter("cardInfoId"));
					//long requester_emp_id = Long.parseLong(request.getParameter("requester_emp_id"));
					getVm_WithdrawalApprovalModel(cardInfoId, request, userDTO);
					request.getRequestDispatcher("vm_route_travel_withdraw_approval_mapping/vm_route_travel_withdraw_approval_mappingEdit.jsp").forward(request, response);
					return;
				case "search":
					if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CARD_APPROVAL_MAPPING_SEARCH)) {

//                        if (userDTO.roleID == RoleEnum.EMPLOYEE_OF_PARLIAMENT.getRoleId()) {
						searchVm_fuel_request_approval_mapping(request, response, String.valueOf(userDTO.employee_record_id), loginDTO, userDTO);
//                        } else {
//                            searchVm_fuel_request_approval_mapping(request, response, "", loginDTO, userDTO);
//                        }
						return;
					}
					break;
				case "approvedVm_withdrawal":
					System.out.println("approvedVm_withdrawal here reached 1");
					if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CARD_APPROVAL_MAPPING_ADD)) {
						System.out.println("approvedVm_withdrawal here reached 2");
						Approve(request, userDTO, true,null,null);
						response.sendRedirect("Vm_route_travel_withdraw_approval_mappingServlet?actionType=search");
						return;
					}
					break;
				case "rejectVm_withdrawal":
					if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CARD_APPROVAL_MAPPING_ADD)) {
						Approve(request, userDTO, false,null,null);
						response.sendRedirect("Vm_route_travel_withdraw_approval_mappingServlet?actionType=search");
						return;
					}
					break;
			}
		} catch (Exception ex) {
			logger.debug(ex);
			ex.printStackTrace();
		}
		request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		if (userDTO == null) {
			request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			return;
		}

		try {
			String actionType = request.getParameter("actionType");
			switch (actionType) {
				case "approvedVm_withdrawal":
					if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CARD_APPROVAL_MAPPING_ADD)) {
						SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
//                        Date from = f.parse(Jsoup.clean(request.getParameter("validationStartDate"), Whitelist.simpleText()));
//                        Date to = f.parse(Jsoup.clean(request.getParameter("validationEndDate"), Whitelist.simpleText()));
						Approve(request, userDTO, true,null,null);
						response.sendRedirect("Vm_route_travel_withdraw_approval_mappingServlet?actionType=search");
						return;
					}
					break;
				case "rejectVm_withdrawal":
					if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CARD_APPROVAL_MAPPING_ADD)) {
						Approve(request, userDTO, false,null,null);
						response.sendRedirect("Vm_route_travel_withdraw_approval_mappingServlet?actionType=search");
						return;
					}
					break;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
		}
		request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
	}

	private void getVm_WithdrawalApprovalModel(long cardInfoId, HttpServletRequest request, UserDTO userDTO) {

		Vm_route_travel_withdraw_approvalModel approvalModel;
		String language = request.getParameter("language");
		//Vm_route_travel_withdrawDTO card_infoDTO = new Vm_route_travel_withdrawDAO().getDTOByID(cardInfoId);
		Vm_route_travel_withdrawDTO card_infoDTO = Vm_route_travel_withdrawRepository.getInstance().getVm_route_travel_withdrawDTOByid(cardInfoId);
		Vm_route_travelDTO vm_route_travelDTO = new Vm_route_travelDAO().getDTOByRequesterEmpID(card_infoDTO.requesterEmpId);



		if (card_infoDTO != null) {
			Vm_route_travel_withdraw_approvalDTO cardApprovalDTO = Vm_route_travel_withdraw_approvalRepository.getInstance().getByEmployeeRecordId(userDTO.employee_record_id);
			Vm_route_travel_withdraw_approval_mappingDTO cardApprovalMappingDTO = new Vm_route_travel_withdraw_approval_mappingDAO().getByVm_fuel_requestIdAndApproverEmployeeRecordId(cardInfoId,userDTO.employee_record_id);
			approvalModel = new Vm_route_travel_withdraw_approvalModel(card_infoDTO, cardApprovalDTO, language,cardApprovalMappingDTO);
			request.setAttribute("Vm_route_travel_withdraw_approvalModel", approvalModel);
			request.setAttribute("Vm_route_travel_withdrawDTO", card_infoDTO);
			request.setAttribute("Vm_route_travelDTO", vm_route_travelDTO);
		}
	}

	private void Approve(HttpServletRequest request, UserDTO userDTO, boolean isAccepted,Date validFrom,Date validTo) throws Exception {

		long requesterEmployeeRecordId = userDTO.employee_record_id;
		long cardInfoId = Long.parseLong(request.getParameter("cardInfoId"));

		String routeTravelTableName = "vm_route_travel";
		Vm_route_travelDAO vm_route_travelDAO = new Vm_route_travelDAO(routeTravelTableName);
		Vm_route_travelDTO vm_route_travelDTO = vm_route_travelDAO.getDTOByOrganogramID(userDTO.organogramID);


		//Vm_route_travel_withdrawDTO card_infoDTO = new Vm_route_travel_withdrawDAO().getDTOByID(cardInfoId);
		Vm_route_travel_withdrawDTO card_infoDTO = Vm_route_travel_withdrawRepository.getInstance().getVm_route_travel_withdrawDTOByid(cardInfoId);
		card_infoDTO.iD = card_infoDTO.id;

		Vm_route_travel_withdrawDAO cardInfoDAO = new Vm_route_travel_withdrawDAO();
		cardInfoDAO.update(card_infoDTO);

		TaskTypeEnum taskTypeEnum = TaskTypeEnum.ROUTE_WITHDRAW;
//        if (card_infoDTO. == Vm_fuel_requestCategoryEnum.PERMANENT.getValue()) {
//            if (employeeRecordsDTO.employeeClass == 3 || employeeRecordsDTO.employeeClass == 4) {
//                taskTypeEnum = TaskTypeEnum.PERMANENT_CARD_FOR_3RD_OR_4TH_CLASS_EMPLOYEE;
//            } else {
//                taskTypeEnum = TaskTypeEnum.PERMANENT_CARD_FOR_1ST_OR_2ND_CLASS_EMPLOYEE;
//            }
//        } else {
//            taskTypeEnum = TaskTypeEnum.TEMPORARY_CARD_FOR_EMPLOYEE;
//        }
		CreateWithdrawApprovalModel model = new CreateWithdrawApprovalModel.CreateCardApprovalModelBuilder()
				.setTaskTypeId(taskTypeEnum.getValue())
				.setWithdrawInfoId(card_infoDTO.iD)
				.setRequesterEmployeeRecordId(requesterEmployeeRecordId)
				.build();
		if (isAccepted) {
			CardApprovalResponse response = new Vm_route_travel_withdraw_approval_mappingDAO().movedToNextLevelOfApproval(model);
			if(!response.hasNextApproval){
				//card_infoDTO.status = Vm_fuel_requestStatusEnum.APPROVED.getValue();
				card_infoDTO.status = CommonApprovalStatus.SATISFIED.getValue();
				cardInfoDAO.update(card_infoDTO);

				if(vm_route_travelDTO!=null){
					//vm_route_travelDTO.status=5;
					vm_route_travelDTO.status= CommonApprovalStatus.WITHDRAWN.getValue();
					vm_route_travelDAO.manageWriteOperations(vm_route_travelDTO, SessionConstants.UPDATE, -1, userDTO);
				}
			}
		} else {
			new Vm_route_travel_withdraw_approval_mappingDAO().rejectPendingApproval(model);
			//card_infoDTO.status = Vm_fuel_requestStatusEnum.REJECTED.getValue();
			card_infoDTO.status = CommonApprovalStatus.DISSATISFIED.getValue();
			cardInfoDAO.update(card_infoDTO);
		}
	}


	private void searchVm_fuel_request_approval_mapping(HttpServletRequest request, HttpServletResponse response, String filter,
														LoginDTO loginDTO, UserDTO userDTO) throws Exception {
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");

		logger.debug("ajax = " + ajax + " hasajax = " + hasAjax);

		

		RecordNavigationManager4 rnManager = new RecordNavigationManager4(
				SessionConstants.NAV_CARD_APPROVAL_MAPPING,
				request,
				card_approval_mappingDAO,
				SessionConstants.VIEW_CARD_APPROVAL_MAPPING,
				SessionConstants.SEARCH_CARD_APPROVAL_MAPPING,
				tableName,
				true,
				userDTO,
				filter,
				true);

		rnManager.doJob(loginDTO);


		request.setAttribute("Vm_route_travel_withdraw_approval_mappingDAO", card_approval_mappingDAO);

		if (!hasAjax) {
			request.getRequestDispatcher("vm_route_travel_withdraw_approval_mapping/vm_route_travel_withdraw_approval_mappingSearch.jsp").forward(request, response);
		} else {
			request.getRequestDispatcher("vm_route_travel_withdraw_approval_mapping/vm_route_travel_withdraw_approval_mappingSearchForm.jsp").forward(request, response);
		}
	}

}

