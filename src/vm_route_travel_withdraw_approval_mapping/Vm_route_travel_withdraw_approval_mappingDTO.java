package vm_route_travel_withdraw_approval_mapping;
import java.util.*; 
import util.*; 


public class Vm_route_travel_withdraw_approval_mappingDTO extends CommonDTO
{

	public long routeTravelWithdrawId = -1;
	public long routeTravelWithdrawApprovalId = -1;
	public int sequence = -1;
	public int status = -1;
	public long employeeRecordId = -1;
	public long withdrawEmployeeInfoId = -1;
	public long withdrawEmployeeOfficeInfoId = -1;
	public long approverEmployeeRecordsId = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;

	public long insertionTime = 0;
	public long modifiedBy = 0;
	public int withdrawApprovalStatusCat = 0;
	public long employeeRecordsId = 0;
	public long taskTypeId = 0;
	public long insertedBy = 0;
	
	
    @Override
	public String toString() {
            return "$Vm_route_travel_withdraw_approval_mappingDTO[" +
            " iD = " + iD +
            " routeTravelWithdrawId = " + routeTravelWithdrawId +
            " routeTravelWithdrawApprovalId = " + routeTravelWithdrawApprovalId +
            " sequence = " + sequence +
            " status = " + status +
            " employeeRecordId = " + employeeRecordId +
            " withdrawEmployeeInfoId = " + withdrawEmployeeInfoId +
            " withdrawEmployeeOfficeInfoId = " + withdrawEmployeeOfficeInfoId +
            " approverEmployeeRecordsId = " + approverEmployeeRecordsId +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
			" insertionTime = " + insertionTime +
			" modifiedBy = " + modifiedBy +
			" withdrawApprovalStatusCat = " + withdrawApprovalStatusCat +
			" employeeRecordsId = " + employeeRecordsId +
			" taskTypeId = " + taskTypeId +
			" insertedBy = " + insertedBy +

            "]";
    }

}