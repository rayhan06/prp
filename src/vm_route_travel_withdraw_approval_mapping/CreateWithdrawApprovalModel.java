package vm_route_travel_withdraw_approval_mapping;

/*
 * @author Md. Erfan Hossain
 * @created 07/05/2021 - 12:31 AM
 * @project parliament
 */

import card_info.CardCategoryEnum;

public class CreateWithdrawApprovalModel {
    private final CardCategoryEnum cardCategoryEnum;
    private final long taskTypeId;
    private final long cardInfoId;
    private final long requesterEmployeeRecordId;
    private final long cardEmployeeInfoId;
    private final long cardEmployeeOfficeInfoId;
    private final long cardEmployeeImagesId;
    private final long employeeRecordId;

    private CreateWithdrawApprovalModel(CreateCardApprovalModelBuilder builder){
        cardCategoryEnum = builder.cardCategoryEnum;
        taskTypeId = builder.taskTypeId;
        cardInfoId = builder.cardInfoId;
        requesterEmployeeRecordId = builder.requesterEmployeeRecordId;
        cardEmployeeInfoId = builder.cardEmployeeInfoId;
        cardEmployeeOfficeInfoId = builder.cardEmployeeOfficeInfoId;
        employeeRecordId = builder.employeeRecordId;
        cardEmployeeImagesId = builder.cardEmployeeImagesId;
    }

    public long getEmployeeRecordId() {
        return employeeRecordId;
    }

    public CardCategoryEnum getCardCategoryEnum() {
        return cardCategoryEnum;
    }

    public long getTaskTypeId() {
        return taskTypeId;
    }

    public long getCardInfoId() {
        return cardInfoId;
    }

    public long getRequesterEmployeeRecordId() {
        return requesterEmployeeRecordId;
    }

    public long getCardEmployeeInfoId() {
        return cardEmployeeInfoId;
    }

    public long getCardEmployeeOfficeInfoId() {
        return cardEmployeeOfficeInfoId;
    }

    public long getCardEmployeeImagesId() {
        return cardEmployeeImagesId;
    }

    public static class CreateCardApprovalModelBuilder{
        private CardCategoryEnum cardCategoryEnum;
        private long taskTypeId;
        private long cardInfoId;
        private long requesterEmployeeRecordId;
        private long cardEmployeeInfoId;
        private long cardEmployeeOfficeInfoId;
        private long employeeRecordId;
        private long cardEmployeeImagesId;

        public CreateCardApprovalModelBuilder setEmployeeRecordId(long employeeRecordId) {
            this.employeeRecordId = employeeRecordId;
            return this;
        }

        public CreateCardApprovalModelBuilder setCardCategoryEnum(CardCategoryEnum cardCategoryEnum) {
            this.cardCategoryEnum = cardCategoryEnum;
            return this;
        }

        public CreateCardApprovalModelBuilder setTaskTypeId(long taskTypeId) {
            this.taskTypeId = taskTypeId;
            return this;
        }

        public CreateCardApprovalModelBuilder setWithdrawInfoId(long cardInfoId) {
            this.cardInfoId = cardInfoId;
            return this;
        }

        public CreateCardApprovalModelBuilder setRequesterEmployeeRecordId(long requesterEmployeeRecordId) {
            this.requesterEmployeeRecordId = requesterEmployeeRecordId;
            return this;
        }

        public CreateCardApprovalModelBuilder setCardEmployeeInfoId(long cardEmployeeInfoId) {
            this.cardEmployeeInfoId = cardEmployeeInfoId;
            return this;
        }

        public CreateCardApprovalModelBuilder setCardEmployeeImagesId(long cardEmployeeImagesId) {
            this.cardEmployeeImagesId = cardEmployeeImagesId;
            return this;
        }

        public CreateCardApprovalModelBuilder setCardEmployeeOfficeInfoId(long cardEmployeeOfficeInfoId) {
            this.cardEmployeeOfficeInfoId = cardEmployeeOfficeInfoId;
            return this;
        }

        public CreateWithdrawApprovalModel build(){
            return new CreateWithdrawApprovalModel(this);
        }
    }
}
