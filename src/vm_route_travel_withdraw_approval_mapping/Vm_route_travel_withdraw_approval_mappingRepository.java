package vm_route_travel_withdraw_approval_mapping;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Vm_route_travel_withdraw_approval_mappingRepository implements Repository {
	Vm_route_travel_withdraw_approval_mappingDAO vm_route_travel_withdraw_approval_mappingDAO = null;
	
	public void setDAO(Vm_route_travel_withdraw_approval_mappingDAO vm_route_travel_withdraw_approval_mappingDAO)
	{
		this.vm_route_travel_withdraw_approval_mappingDAO = vm_route_travel_withdraw_approval_mappingDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Vm_route_travel_withdraw_approval_mappingRepository.class);
	Map<Long, Vm_route_travel_withdraw_approval_mappingDTO>mapOfVm_route_travel_withdraw_approval_mappingDTOToiD;
	Map<Long, Set<Vm_route_travel_withdraw_approval_mappingDTO> >mapOfVm_route_travel_withdraw_approval_mappingDTOTorouteTravelWithdrawId;
	Map<Long, Set<Vm_route_travel_withdraw_approval_mappingDTO> >mapOfVm_route_travel_withdraw_approval_mappingDTOTorouteTravelWithdrawApprovalId;
	Map<Integer, Set<Vm_route_travel_withdraw_approval_mappingDTO> >mapOfVm_route_travel_withdraw_approval_mappingDTOTosequence;
	Map<Integer, Set<Vm_route_travel_withdraw_approval_mappingDTO> >mapOfVm_route_travel_withdraw_approval_mappingDTOTostatus;
	Map<Long, Set<Vm_route_travel_withdraw_approval_mappingDTO> >mapOfVm_route_travel_withdraw_approval_mappingDTOToemployeeRecordId;
	Map<Long, Set<Vm_route_travel_withdraw_approval_mappingDTO> >mapOfVm_route_travel_withdraw_approval_mappingDTOTowithdrawEmployeeInfoId;
	Map<Long, Set<Vm_route_travel_withdraw_approval_mappingDTO> >mapOfVm_route_travel_withdraw_approval_mappingDTOTowithdrawEmployeeOfficeInfoId;
	Map<Long, Set<Vm_route_travel_withdraw_approval_mappingDTO> >mapOfVm_route_travel_withdraw_approval_mappingDTOToapproverEmployeeRecordsId;
	Map<Long, Set<Vm_route_travel_withdraw_approval_mappingDTO> >mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertedByUserId;
	Map<Long, Set<Vm_route_travel_withdraw_approval_mappingDTO> >mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertedByOrganogramId;
	Map<Long, Set<Vm_route_travel_withdraw_approval_mappingDTO> >mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertionDate;
	Map<Long, Set<Vm_route_travel_withdraw_approval_mappingDTO> >mapOfVm_route_travel_withdraw_approval_mappingDTOTolastModificationTime;


	static Vm_route_travel_withdraw_approval_mappingRepository instance = null;  
	private Vm_route_travel_withdraw_approval_mappingRepository(){
		mapOfVm_route_travel_withdraw_approval_mappingDTOToiD = new ConcurrentHashMap<>();
		mapOfVm_route_travel_withdraw_approval_mappingDTOTorouteTravelWithdrawId = new ConcurrentHashMap<>();
		mapOfVm_route_travel_withdraw_approval_mappingDTOTorouteTravelWithdrawApprovalId = new ConcurrentHashMap<>();
		mapOfVm_route_travel_withdraw_approval_mappingDTOTosequence = new ConcurrentHashMap<>();
		mapOfVm_route_travel_withdraw_approval_mappingDTOTostatus = new ConcurrentHashMap<>();
		mapOfVm_route_travel_withdraw_approval_mappingDTOToemployeeRecordId = new ConcurrentHashMap<>();
		mapOfVm_route_travel_withdraw_approval_mappingDTOTowithdrawEmployeeInfoId = new ConcurrentHashMap<>();
		mapOfVm_route_travel_withdraw_approval_mappingDTOTowithdrawEmployeeOfficeInfoId = new ConcurrentHashMap<>();
		mapOfVm_route_travel_withdraw_approval_mappingDTOToapproverEmployeeRecordsId = new ConcurrentHashMap<>();
		mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfVm_route_travel_withdraw_approval_mappingDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vm_route_travel_withdraw_approval_mappingRepository getInstance(){
		if (instance == null){
			instance = new Vm_route_travel_withdraw_approval_mappingRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(vm_route_travel_withdraw_approval_mappingDAO == null)
		{
			return;
		}
		try {
			List<Vm_route_travel_withdraw_approval_mappingDTO> vm_route_travel_withdraw_approval_mappingDTOs = vm_route_travel_withdraw_approval_mappingDAO.getAllVm_route_travel_withdraw_approval_mapping(reloadAll);
			for(Vm_route_travel_withdraw_approval_mappingDTO vm_route_travel_withdraw_approval_mappingDTO : vm_route_travel_withdraw_approval_mappingDTOs) {
				Vm_route_travel_withdraw_approval_mappingDTO oldVm_route_travel_withdraw_approval_mappingDTO = getVm_route_travel_withdraw_approval_mappingDTOByID(vm_route_travel_withdraw_approval_mappingDTO.iD);
				if( oldVm_route_travel_withdraw_approval_mappingDTO != null ) {
					mapOfVm_route_travel_withdraw_approval_mappingDTOToiD.remove(oldVm_route_travel_withdraw_approval_mappingDTO.iD);
				
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOTorouteTravelWithdrawId.containsKey(oldVm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawId)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTorouteTravelWithdrawId.get(oldVm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawId).remove(oldVm_route_travel_withdraw_approval_mappingDTO);
					}
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOTorouteTravelWithdrawId.get(oldVm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawId).isEmpty()) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTorouteTravelWithdrawId.remove(oldVm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawId);
					}
					
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOTorouteTravelWithdrawApprovalId.containsKey(oldVm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawApprovalId)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTorouteTravelWithdrawApprovalId.get(oldVm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawApprovalId).remove(oldVm_route_travel_withdraw_approval_mappingDTO);
					}
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOTorouteTravelWithdrawApprovalId.get(oldVm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawApprovalId).isEmpty()) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTorouteTravelWithdrawApprovalId.remove(oldVm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawApprovalId);
					}
					
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOTosequence.containsKey(oldVm_route_travel_withdraw_approval_mappingDTO.sequence)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTosequence.get(oldVm_route_travel_withdraw_approval_mappingDTO.sequence).remove(oldVm_route_travel_withdraw_approval_mappingDTO);
					}
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOTosequence.get(oldVm_route_travel_withdraw_approval_mappingDTO.sequence).isEmpty()) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTosequence.remove(oldVm_route_travel_withdraw_approval_mappingDTO.sequence);
					}
					
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOTostatus.containsKey(oldVm_route_travel_withdraw_approval_mappingDTO.status)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTostatus.get(oldVm_route_travel_withdraw_approval_mappingDTO.status).remove(oldVm_route_travel_withdraw_approval_mappingDTO);
					}
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOTostatus.get(oldVm_route_travel_withdraw_approval_mappingDTO.status).isEmpty()) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTostatus.remove(oldVm_route_travel_withdraw_approval_mappingDTO.status);
					}
					
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOToemployeeRecordId.containsKey(oldVm_route_travel_withdraw_approval_mappingDTO.employeeRecordId)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOToemployeeRecordId.get(oldVm_route_travel_withdraw_approval_mappingDTO.employeeRecordId).remove(oldVm_route_travel_withdraw_approval_mappingDTO);
					}
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOToemployeeRecordId.get(oldVm_route_travel_withdraw_approval_mappingDTO.employeeRecordId).isEmpty()) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOToemployeeRecordId.remove(oldVm_route_travel_withdraw_approval_mappingDTO.employeeRecordId);
					}
					
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOTowithdrawEmployeeInfoId.containsKey(oldVm_route_travel_withdraw_approval_mappingDTO.withdrawEmployeeInfoId)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTowithdrawEmployeeInfoId.get(oldVm_route_travel_withdraw_approval_mappingDTO.withdrawEmployeeInfoId).remove(oldVm_route_travel_withdraw_approval_mappingDTO);
					}
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOTowithdrawEmployeeInfoId.get(oldVm_route_travel_withdraw_approval_mappingDTO.withdrawEmployeeInfoId).isEmpty()) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTowithdrawEmployeeInfoId.remove(oldVm_route_travel_withdraw_approval_mappingDTO.withdrawEmployeeInfoId);
					}
					
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOTowithdrawEmployeeOfficeInfoId.containsKey(oldVm_route_travel_withdraw_approval_mappingDTO.withdrawEmployeeOfficeInfoId)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTowithdrawEmployeeOfficeInfoId.get(oldVm_route_travel_withdraw_approval_mappingDTO.withdrawEmployeeOfficeInfoId).remove(oldVm_route_travel_withdraw_approval_mappingDTO);
					}
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOTowithdrawEmployeeOfficeInfoId.get(oldVm_route_travel_withdraw_approval_mappingDTO.withdrawEmployeeOfficeInfoId).isEmpty()) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTowithdrawEmployeeOfficeInfoId.remove(oldVm_route_travel_withdraw_approval_mappingDTO.withdrawEmployeeOfficeInfoId);
					}
					
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOToapproverEmployeeRecordsId.containsKey(oldVm_route_travel_withdraw_approval_mappingDTO.approverEmployeeRecordsId)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOToapproverEmployeeRecordsId.get(oldVm_route_travel_withdraw_approval_mappingDTO.approverEmployeeRecordsId).remove(oldVm_route_travel_withdraw_approval_mappingDTO);
					}
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOToapproverEmployeeRecordsId.get(oldVm_route_travel_withdraw_approval_mappingDTO.approverEmployeeRecordsId).isEmpty()) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOToapproverEmployeeRecordsId.remove(oldVm_route_travel_withdraw_approval_mappingDTO.approverEmployeeRecordsId);
					}
					
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertedByUserId.containsKey(oldVm_route_travel_withdraw_approval_mappingDTO.insertedByUserId)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertedByUserId.get(oldVm_route_travel_withdraw_approval_mappingDTO.insertedByUserId).remove(oldVm_route_travel_withdraw_approval_mappingDTO);
					}
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertedByUserId.get(oldVm_route_travel_withdraw_approval_mappingDTO.insertedByUserId).isEmpty()) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertedByUserId.remove(oldVm_route_travel_withdraw_approval_mappingDTO.insertedByUserId);
					}
					
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertedByOrganogramId.containsKey(oldVm_route_travel_withdraw_approval_mappingDTO.insertedByOrganogramId)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertedByOrganogramId.get(oldVm_route_travel_withdraw_approval_mappingDTO.insertedByOrganogramId).remove(oldVm_route_travel_withdraw_approval_mappingDTO);
					}
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertedByOrganogramId.get(oldVm_route_travel_withdraw_approval_mappingDTO.insertedByOrganogramId).isEmpty()) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertedByOrganogramId.remove(oldVm_route_travel_withdraw_approval_mappingDTO.insertedByOrganogramId);
					}
					
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertionDate.containsKey(oldVm_route_travel_withdraw_approval_mappingDTO.insertionDate)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertionDate.get(oldVm_route_travel_withdraw_approval_mappingDTO.insertionDate).remove(oldVm_route_travel_withdraw_approval_mappingDTO);
					}
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertionDate.get(oldVm_route_travel_withdraw_approval_mappingDTO.insertionDate).isEmpty()) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertionDate.remove(oldVm_route_travel_withdraw_approval_mappingDTO.insertionDate);
					}
					
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOTolastModificationTime.containsKey(oldVm_route_travel_withdraw_approval_mappingDTO.lastModificationTime)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTolastModificationTime.get(oldVm_route_travel_withdraw_approval_mappingDTO.lastModificationTime).remove(oldVm_route_travel_withdraw_approval_mappingDTO);
					}
					if(mapOfVm_route_travel_withdraw_approval_mappingDTOTolastModificationTime.get(oldVm_route_travel_withdraw_approval_mappingDTO.lastModificationTime).isEmpty()) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTolastModificationTime.remove(oldVm_route_travel_withdraw_approval_mappingDTO.lastModificationTime);
					}
					
					
				}
				if(vm_route_travel_withdraw_approval_mappingDTO.isDeleted == 0) 
				{
					
					mapOfVm_route_travel_withdraw_approval_mappingDTOToiD.put(vm_route_travel_withdraw_approval_mappingDTO.iD, vm_route_travel_withdraw_approval_mappingDTO);
				
					if( ! mapOfVm_route_travel_withdraw_approval_mappingDTOTorouteTravelWithdrawId.containsKey(vm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawId)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTorouteTravelWithdrawId.put(vm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawId, new HashSet<>());
					}
					mapOfVm_route_travel_withdraw_approval_mappingDTOTorouteTravelWithdrawId.get(vm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawId).add(vm_route_travel_withdraw_approval_mappingDTO);
					
					if( ! mapOfVm_route_travel_withdraw_approval_mappingDTOTorouteTravelWithdrawApprovalId.containsKey(vm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawApprovalId)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTorouteTravelWithdrawApprovalId.put(vm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawApprovalId, new HashSet<>());
					}
					mapOfVm_route_travel_withdraw_approval_mappingDTOTorouteTravelWithdrawApprovalId.get(vm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawApprovalId).add(vm_route_travel_withdraw_approval_mappingDTO);
					
					if( ! mapOfVm_route_travel_withdraw_approval_mappingDTOTosequence.containsKey(vm_route_travel_withdraw_approval_mappingDTO.sequence)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTosequence.put(vm_route_travel_withdraw_approval_mappingDTO.sequence, new HashSet<>());
					}
					mapOfVm_route_travel_withdraw_approval_mappingDTOTosequence.get(vm_route_travel_withdraw_approval_mappingDTO.sequence).add(vm_route_travel_withdraw_approval_mappingDTO);
					
					if( ! mapOfVm_route_travel_withdraw_approval_mappingDTOTostatus.containsKey(vm_route_travel_withdraw_approval_mappingDTO.status)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTostatus.put(vm_route_travel_withdraw_approval_mappingDTO.status, new HashSet<>());
					}
					mapOfVm_route_travel_withdraw_approval_mappingDTOTostatus.get(vm_route_travel_withdraw_approval_mappingDTO.status).add(vm_route_travel_withdraw_approval_mappingDTO);
					
					if( ! mapOfVm_route_travel_withdraw_approval_mappingDTOToemployeeRecordId.containsKey(vm_route_travel_withdraw_approval_mappingDTO.employeeRecordId)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOToemployeeRecordId.put(vm_route_travel_withdraw_approval_mappingDTO.employeeRecordId, new HashSet<>());
					}
					mapOfVm_route_travel_withdraw_approval_mappingDTOToemployeeRecordId.get(vm_route_travel_withdraw_approval_mappingDTO.employeeRecordId).add(vm_route_travel_withdraw_approval_mappingDTO);
					
					if( ! mapOfVm_route_travel_withdraw_approval_mappingDTOTowithdrawEmployeeInfoId.containsKey(vm_route_travel_withdraw_approval_mappingDTO.withdrawEmployeeInfoId)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTowithdrawEmployeeInfoId.put(vm_route_travel_withdraw_approval_mappingDTO.withdrawEmployeeInfoId, new HashSet<>());
					}
					mapOfVm_route_travel_withdraw_approval_mappingDTOTowithdrawEmployeeInfoId.get(vm_route_travel_withdraw_approval_mappingDTO.withdrawEmployeeInfoId).add(vm_route_travel_withdraw_approval_mappingDTO);
					
					if( ! mapOfVm_route_travel_withdraw_approval_mappingDTOTowithdrawEmployeeOfficeInfoId.containsKey(vm_route_travel_withdraw_approval_mappingDTO.withdrawEmployeeOfficeInfoId)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTowithdrawEmployeeOfficeInfoId.put(vm_route_travel_withdraw_approval_mappingDTO.withdrawEmployeeOfficeInfoId, new HashSet<>());
					}
					mapOfVm_route_travel_withdraw_approval_mappingDTOTowithdrawEmployeeOfficeInfoId.get(vm_route_travel_withdraw_approval_mappingDTO.withdrawEmployeeOfficeInfoId).add(vm_route_travel_withdraw_approval_mappingDTO);
					
					if( ! mapOfVm_route_travel_withdraw_approval_mappingDTOToapproverEmployeeRecordsId.containsKey(vm_route_travel_withdraw_approval_mappingDTO.approverEmployeeRecordsId)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOToapproverEmployeeRecordsId.put(vm_route_travel_withdraw_approval_mappingDTO.approverEmployeeRecordsId, new HashSet<>());
					}
					mapOfVm_route_travel_withdraw_approval_mappingDTOToapproverEmployeeRecordsId.get(vm_route_travel_withdraw_approval_mappingDTO.approverEmployeeRecordsId).add(vm_route_travel_withdraw_approval_mappingDTO);
					
					if( ! mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertedByUserId.containsKey(vm_route_travel_withdraw_approval_mappingDTO.insertedByUserId)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertedByUserId.put(vm_route_travel_withdraw_approval_mappingDTO.insertedByUserId, new HashSet<>());
					}
					mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertedByUserId.get(vm_route_travel_withdraw_approval_mappingDTO.insertedByUserId).add(vm_route_travel_withdraw_approval_mappingDTO);
					
					if( ! mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertedByOrganogramId.containsKey(vm_route_travel_withdraw_approval_mappingDTO.insertedByOrganogramId)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertedByOrganogramId.put(vm_route_travel_withdraw_approval_mappingDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertedByOrganogramId.get(vm_route_travel_withdraw_approval_mappingDTO.insertedByOrganogramId).add(vm_route_travel_withdraw_approval_mappingDTO);
					
					if( ! mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertionDate.containsKey(vm_route_travel_withdraw_approval_mappingDTO.insertionDate)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertionDate.put(vm_route_travel_withdraw_approval_mappingDTO.insertionDate, new HashSet<>());
					}
					mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertionDate.get(vm_route_travel_withdraw_approval_mappingDTO.insertionDate).add(vm_route_travel_withdraw_approval_mappingDTO);
					
					if( ! mapOfVm_route_travel_withdraw_approval_mappingDTOTolastModificationTime.containsKey(vm_route_travel_withdraw_approval_mappingDTO.lastModificationTime)) {
						mapOfVm_route_travel_withdraw_approval_mappingDTOTolastModificationTime.put(vm_route_travel_withdraw_approval_mappingDTO.lastModificationTime, new HashSet<>());
					}
					mapOfVm_route_travel_withdraw_approval_mappingDTOTolastModificationTime.get(vm_route_travel_withdraw_approval_mappingDTO.lastModificationTime).add(vm_route_travel_withdraw_approval_mappingDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Vm_route_travel_withdraw_approval_mappingDTO> getVm_route_travel_withdraw_approval_mappingList() {
		List <Vm_route_travel_withdraw_approval_mappingDTO> vm_route_travel_withdraw_approval_mappings = new ArrayList<Vm_route_travel_withdraw_approval_mappingDTO>(this.mapOfVm_route_travel_withdraw_approval_mappingDTOToiD.values());
		return vm_route_travel_withdraw_approval_mappings;
	}
	
	
	public Vm_route_travel_withdraw_approval_mappingDTO getVm_route_travel_withdraw_approval_mappingDTOByID( long ID){
		return mapOfVm_route_travel_withdraw_approval_mappingDTOToiD.get(ID);
	}
	
	
	public List<Vm_route_travel_withdraw_approval_mappingDTO> getVm_route_travel_withdraw_approval_mappingDTOByroute_travel_withdraw_id(long route_travel_withdraw_id) {
		return new ArrayList<>( mapOfVm_route_travel_withdraw_approval_mappingDTOTorouteTravelWithdrawId.getOrDefault(route_travel_withdraw_id,new HashSet<>()));
	}
	
	
	public List<Vm_route_travel_withdraw_approval_mappingDTO> getVm_route_travel_withdraw_approval_mappingDTOByroute_travel_withdraw_approval_id(long route_travel_withdraw_approval_id) {
		return new ArrayList<>( mapOfVm_route_travel_withdraw_approval_mappingDTOTorouteTravelWithdrawApprovalId.getOrDefault(route_travel_withdraw_approval_id,new HashSet<>()));
	}
	
	
	public List<Vm_route_travel_withdraw_approval_mappingDTO> getVm_route_travel_withdraw_approval_mappingDTOBysequence(int sequence) {
		return new ArrayList<>( mapOfVm_route_travel_withdraw_approval_mappingDTOTosequence.getOrDefault(sequence,new HashSet<>()));
	}
	
	
	public List<Vm_route_travel_withdraw_approval_mappingDTO> getVm_route_travel_withdraw_approval_mappingDTOBystatus(int status) {
		return new ArrayList<>( mapOfVm_route_travel_withdraw_approval_mappingDTOTostatus.getOrDefault(status,new HashSet<>()));
	}
	
	
	public List<Vm_route_travel_withdraw_approval_mappingDTO> getVm_route_travel_withdraw_approval_mappingDTOByemployee_record_id(long employee_record_id) {
		return new ArrayList<>( mapOfVm_route_travel_withdraw_approval_mappingDTOToemployeeRecordId.getOrDefault(employee_record_id,new HashSet<>()));
	}
	
	
	public List<Vm_route_travel_withdraw_approval_mappingDTO> getVm_route_travel_withdraw_approval_mappingDTOBywithdraw_employee_info_id(long withdraw_employee_info_id) {
		return new ArrayList<>( mapOfVm_route_travel_withdraw_approval_mappingDTOTowithdrawEmployeeInfoId.getOrDefault(withdraw_employee_info_id,new HashSet<>()));
	}
	
	
	public List<Vm_route_travel_withdraw_approval_mappingDTO> getVm_route_travel_withdraw_approval_mappingDTOBywithdraw_employee_office_info_id(long withdraw_employee_office_info_id) {
		return new ArrayList<>( mapOfVm_route_travel_withdraw_approval_mappingDTOTowithdrawEmployeeOfficeInfoId.getOrDefault(withdraw_employee_office_info_id,new HashSet<>()));
	}
	
	
	public List<Vm_route_travel_withdraw_approval_mappingDTO> getVm_route_travel_withdraw_approval_mappingDTOByapprover_employee_records_id(long approver_employee_records_id) {
		return new ArrayList<>( mapOfVm_route_travel_withdraw_approval_mappingDTOToapproverEmployeeRecordsId.getOrDefault(approver_employee_records_id,new HashSet<>()));
	}
	
	
	public List<Vm_route_travel_withdraw_approval_mappingDTO> getVm_route_travel_withdraw_approval_mappingDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Vm_route_travel_withdraw_approval_mappingDTO> getVm_route_travel_withdraw_approval_mappingDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Vm_route_travel_withdraw_approval_mappingDTO> getVm_route_travel_withdraw_approval_mappingDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfVm_route_travel_withdraw_approval_mappingDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Vm_route_travel_withdraw_approval_mappingDTO> getVm_route_travel_withdraw_approval_mappingDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfVm_route_travel_withdraw_approval_mappingDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_route_travel_withdraw_approval_mapping";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


