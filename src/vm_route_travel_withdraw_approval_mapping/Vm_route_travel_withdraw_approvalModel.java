package vm_route_travel_withdraw_approval_mapping;


import util.StringUtils;

import vm_route_travel_withdraw.Vm_route_travel_withdrawDTO;
import vm_route_travel_withdraw_approval.Vm_route_travel_withdraw_approvalDTO;

public class Vm_route_travel_withdraw_approvalModel {
    public long cardInfoId = -1;
    public String approvedBy = "";
    public String approvedOn = "";
    public Vm_route_travel_withdraw_approval_mappingDTO cardApprovalMappingDTO;




    public Vm_route_travel_withdraw_approvalModel(Vm_route_travel_withdrawDTO card_infoDTO, Vm_route_travel_withdraw_approvalDTO cardApprovalDTO, String Language, Vm_route_travel_withdraw_approval_mappingDTO cardApprovalMappingDTO) {
        boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
        cardInfoId = card_infoDTO.iD;
        this.cardApprovalMappingDTO = cardApprovalMappingDTO;
        if (cardApprovalDTO != null) {
            approvedBy = isLanguageEnglish ? cardApprovalDTO.nameBn : cardApprovalDTO.nameEn;
            approvedOn = StringUtils.getFormattedDate(Language, cardApprovalDTO.insertionDate);
        }
    }

    public Vm_route_travel_withdraw_approvalModel() {

    }

    @Override
    public String toString() {
        return "Vm_fuel_requestApprovalModel{" +
                "cardInfoId=" + cardInfoId +
                ", approvedBy='" + approvedBy + '\'' +
                ", approvedOn='" + approvedOn + '\'' +
                ", cardApprovalMappingDTO=" + cardApprovalMappingDTO +
                '}';
    }
}
