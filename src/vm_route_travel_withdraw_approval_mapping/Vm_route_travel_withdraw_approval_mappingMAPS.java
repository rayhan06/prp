package vm_route_travel_withdraw_approval_mapping;
import java.util.*; 
import util.*;


public class Vm_route_travel_withdraw_approval_mappingMAPS extends CommonMaps
{	
	public Vm_route_travel_withdraw_approval_mappingMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("routeTravelWithdrawId".toLowerCase(), "routeTravelWithdrawId".toLowerCase());
		java_DTO_map.put("routeTravelWithdrawApprovalId".toLowerCase(), "routeTravelWithdrawApprovalId".toLowerCase());
		java_DTO_map.put("sequence".toLowerCase(), "sequence".toLowerCase());
		java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
		java_DTO_map.put("employeeRecordId".toLowerCase(), "employeeRecordId".toLowerCase());
		java_DTO_map.put("withdrawEmployeeInfoId".toLowerCase(), "withdrawEmployeeInfoId".toLowerCase());
		java_DTO_map.put("withdrawEmployeeOfficeInfoId".toLowerCase(), "withdrawEmployeeOfficeInfoId".toLowerCase());
		java_DTO_map.put("approverEmployeeRecordsId".toLowerCase(), "approverEmployeeRecordsId".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());


		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Route Travel Withdraw Id".toLowerCase(), "routeTravelWithdrawId".toLowerCase());
		java_Text_map.put("Route Travel Withdraw Approval Id".toLowerCase(), "routeTravelWithdrawApprovalId".toLowerCase());
		java_Text_map.put("Sequence".toLowerCase(), "sequence".toLowerCase());
		java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
		java_Text_map.put("Employee Record Id".toLowerCase(), "employeeRecordId".toLowerCase());
		java_Text_map.put("Withdraw Employee Info Id".toLowerCase(), "withdrawEmployeeInfoId".toLowerCase());
		java_Text_map.put("Withdraw Employee Office Info Id".toLowerCase(), "withdrawEmployeeOfficeInfoId".toLowerCase());
		java_Text_map.put("Approver Employee Records Id".toLowerCase(), "approverEmployeeRecordsId".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}