package vm_route_travel_withdraw_approval_mapping;

import card_info.*;
import common.ConnectionAndStatementUtil;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.EmployeeCommonDTOService;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import task_type_approval_path.TaskTypeApprovalPathDTO;
import task_type_approval_path.TaskTypeApprovalPathRepository;
import user.UserDTO;
import util.CommonDTO;
import util.LockManager;
import util.NavigationService4;
import vm_route_travel_withdraw.Vm_route_travel_withdrawMAPS;
import vm_route_travel_withdraw_approval.Vm_route_travel_withdraw_approvalDTO;
import vm_route_travel_withdraw_approval.Vm_route_travel_withdraw_approvalRepository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class Vm_route_travel_withdraw_approval_mappingDAO  extends NavigationService4 implements EmployeeCommonDTOService<Vm_route_travel_withdraw_approval_mappingDTO>
{
	private static final Logger logger = Logger.getLogger(Vm_route_travel_withdraw_approval_mappingDTO.class);

	private static final String updateSqlQuery = "UPDATE {tableName} SET withdrawApprovalStatusCat = ?,sequence = ?, modified_by = ?,lastModificationTime = ?," +
			"isDeleted = ? WHERE id = ?";
	private static final String getByCardInfoId = "SELECT * FROM vm_route_travel_withdraw_approval_mapping WHERE route_travel_withdraw_id = %d AND isDeleted = 0 ORDER BY sequence DESC";
	private static final String getByCardInfoIdAndApproverEmployeeRecordsId = "SELECT * FROM vm_route_travel_withdraw_approval_mapping WHERE route_travel_withdraw_id = %d AND approver_employee_records_id = %d";
	private static final String getByCardInfoIdAndStatus = "SELECT * FROM vm_route_travel_withdraw_approval_mapping WHERE route_travel_withdraw_id = %d AND withdrawApprovalStatusCat = %d AND isDeleted = 0";
	private static final String updateStatus = "UPDATE vm_route_travel_withdraw_approval_mapping SET withdrawApprovalStatusCat = %d,isDeleted = %d,modified_by = %d,lastModificationTime = %d WHERE id IN (%s)";
	private static final String updateStatusByCardInfoId = "UPDATE vm_route_travel_withdraw_approval_mapping SET withdrawApprovalStatusCat = %d,isDeleted = %d," +
			"modified_by = %d,lastModificationTime = %d WHERE withdrawApprovalStatusCat = %d AND route_travel_withdraw_id = %d";

	private static final String getByVm_fuel_requestInfoIdAndApproverEmployeeRecordsId = "SELECT * FROM vm_route_travel_withdraw_approval_mapping WHERE route_travel_withdraw_id = %d AND approver_employee_records_id = %d";

	private static final Map<String, String> searchMap = new HashMap<>();

	static {
		searchMap.put("withdrawApprovalStatusCat", "and (withdrawApprovalStatusCat = ?)");
		searchMap.put("employee_records_id_internal", "and (approver_employee_records_id = ?)");
	}

	public Vm_route_travel_withdraw_approval_mappingDAO(String tableName) {
		super(tableName);
		useSafeSearch = true;
		commonMaps = new Vm_route_travel_withdrawMAPS(tableName);
	}

	public Vm_route_travel_withdraw_approval_mappingDAO() {
		this("vm_route_travel_withdraw_approval_mapping");
	}

	public void setSearchColumn(Vm_route_travel_withdraw_approval_mappingDTO card_approval_mappingDTO) {
//		CategoryLanguageModel model = CatRepository.getInstance().getCategoryLanguageModel("card_approval_status", card_approval_mappingDTO.cardApprovalStatusCat);
//		card_approval_mappingDTO.searchColumn = model.englishText + " " + model.banglaText;
	}

	private static final String addSqlQuery = "INSERT INTO {tableName} (withdrawApprovalStatusCat,sequence,modified_by,lastModificationTime," +
			"route_travel_withdraw_id,route_travel_withdraw_approval_id,withdraw_employee_info_id,withdraw_employee_office_info_id,inserted_by,insertion_time,taskTypeId," +
			"employee_record_id,employee_records_id,approver_employee_records_id,isDeleted,id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	@Override
	public void set(PreparedStatement ps, Vm_route_travel_withdraw_approval_mappingDTO dto, boolean isInsert) throws SQLException {
		int index = 0;
		ps.setInt(++index, dto.withdrawApprovalStatusCat);
		ps.setInt(++index, dto.sequence);
		ps.setLong(++index, dto.modifiedBy);
		ps.setLong(++index, dto.lastModificationTime);
		if (isInsert) {
			ps.setLong(++index, dto.routeTravelWithdrawId);
			ps.setLong(++index, dto.routeTravelWithdrawApprovalId);
			ps.setLong(++index, dto.withdrawEmployeeInfoId);
			ps.setLong(++index, dto.withdrawEmployeeOfficeInfoId);
			ps.setLong(++index, dto.insertedBy);
			ps.setLong(++index, dto.insertionTime);
			ps.setLong(++index, dto.taskTypeId);
			ps.setLong(++index, dto.employeeRecordsId);
			ps.setLong(++index, dto.employeeRecordsId);
			ps.setLong(++index, dto.approverEmployeeRecordsId);
		}
		ps.setInt(++index, dto.isDeleted);
		ps.setLong(++index, dto.iD);
	}


	@Override
	public Vm_route_travel_withdraw_approval_mappingDTO buildObjectFromResultSet(ResultSet rs) {
		try {
			Vm_route_travel_withdraw_approval_mappingDTO dto = new Vm_route_travel_withdraw_approval_mappingDTO();
			dto.iD = rs.getLong("id");
			dto.routeTravelWithdrawId = rs.getLong("route_travel_withdraw_id");
			dto.routeTravelWithdrawApprovalId = rs.getLong("route_travel_withdraw_approval_id");
			dto.withdrawEmployeeInfoId = rs.getLong("withdraw_employee_info_id");
			dto.withdrawEmployeeOfficeInfoId = rs.getLong("withdraw_employee_office_info_id");
			dto.withdrawApprovalStatusCat = rs.getInt("withdrawApprovalStatusCat");
			dto.sequence = rs.getInt("sequence");
			dto.taskTypeId = rs.getLong("taskTypeId");
			dto.insertedBy = rs.getLong("inserted_by");
			dto.insertionTime = rs.getLong("insertion_time");
			dto.modifiedBy = rs.getLong("modified_by");
			dto.lastModificationTime = rs.getLong("lastModificationTime");
			dto.isDeleted = rs.getInt("isDeleted");
			dto.employeeRecordId = rs.getInt("employee_record_id");
			dto.employeeRecordsId = rs.getInt("employee_records_id");
			dto.approverEmployeeRecordsId = rs.getInt("approver_employee_records_id");
			dto.status =  rs.getInt("status");
			return dto;
		} catch (SQLException ex) {
			logger.error(ex);
			return null;
		}
	}

	@Override
	public String getTableName() {
		return tableName;
	}

	@Override
	public long add(CommonDTO dto) throws Exception {
		return executeAddOrUpdateQuery((Vm_route_travel_withdraw_approval_mappingDTO) dto, addSqlQuery, true);
	}

	@Override
	public long update(CommonDTO dto) throws Exception {
		return executeAddOrUpdateQuery((Vm_route_travel_withdraw_approval_mappingDTO) dto, updateSqlQuery, false);
	}

	public Vm_route_travel_withdraw_approval_mappingDTO getDTOByID(long ID) {
		return getDTOFromID(ID);
	}

	public List<Vm_route_travel_withdraw_approval_mappingDTO> getAllCard_approval_mapping(boolean isFirstReload) {
		return getAllDTOs(isFirstReload);
	}

	public List<Vm_route_travel_withdraw_approval_mappingDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}


	public List<Vm_route_travel_withdraw_approval_mappingDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable,
												  UserDTO userDTO, String filter, boolean tableHasJobCat) {
		List<Object> objectList = new ArrayList<>();
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat,objectList);
		return getDTOs(sql,objectList);
	}

	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
														  UserDTO userDTO, String filter, boolean tableHasJobCat,List<Object>objectList) {
		if (filter != null && filter.trim().length() > 0) {
			p_searchCriteria.put("employee_records_id_internal", String.valueOf(userDTO.employee_record_id));
		}
		return getSearchQuery(tableName, p_searchCriteria, limit, offset, category, searchMap,objectList);
	}

	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
										   boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat,List<Object>objectList) {
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat,objectList);
	}

	public CardApprovalResponse createWithdrawApproval(CreateWithdrawApprovalModel model) throws Exception {
		String sql = String.format(getByCardInfoId, model.getCardInfoId());
		Vm_route_travel_withdraw_approval_mappingDTO dto = ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
		System.out.println("dto in createWithdrawApproval: "+dto+" getByCardInfoId sql: "+getByCardInfoId);
		if (dto != null) {
			throw new DuplicateCardInfoException("Approval is already created for " + model.getCardInfoId());
		}
		CardApprovalResponse cardApprovalResponse = new CardApprovalResponse();
		EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(model.getRequesterEmployeeRecordId());
		OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);

		if(true){
			Vm_route_travel_withdraw_approval_mappingDTO cardApprovalMappingDTO;


			int nextLevel = 1;
			List<TaskTypeApprovalPathDTO> nextApprovalPath = TaskTypeApprovalPathRepository.getInstance().getByTaskTypeIdAndLevel(model.getTaskTypeId(), nextLevel);


			if (nextApprovalPath == null || nextApprovalPath.size() == 0) {
				cardApprovalResponse.hasNextApproval = false;
				cardApprovalResponse.organogramIds = null;
			} else {

				List<Long> organogramIds = add(nextApprovalPath, model, nextLevel);
				cardApprovalResponse.hasNextApproval = true;
				cardApprovalResponse.organogramIds = organogramIds;
			}
		}

//		if ( officeUnitOrganograms.orgTree.endsWith("|")) {
//		if (model.getCardCategoryEnum() == CardCategoryEnum.TEMPORARY && officeUnitOrganograms.orgTree.endsWith("|")) {
//			Vm_route_travel_withdraw_approval_mappingDTO cardApprovalMappingDTO;
//			cardApprovalMappingDTO = buildDTO(model.getRequesterEmployeeRecordId(),model, 1, 1, ApprovalStatus.SATISFIED);
//			if (cardApprovalMappingDTO == null) {
//				throw new CardApproverNotFoundException("Withdrawal Approver is not found for " + employeeOfficeDTO.employeeRecordId);
//			}
//			add(cardApprovalMappingDTO);
//			List<TaskTypeApprovalPathDTO> nextApprovalPath = TaskTypeApprovalPathRepository.getInstance().getByTaskTypeIdAndLevel(model.getTaskTypeId(), 2);
//			if (nextApprovalPath == null || nextApprovalPath.size() == 0) {
//				cardApprovalResponse.hasNextApproval = false;
//				cardApprovalResponse.organogramIds = null;
//			} else {
//				List<Long> organogramIds = add(nextApprovalPath, model, 2);
//				cardApprovalResponse.hasNextApproval = true;
//				cardApprovalResponse.organogramIds = organogramIds;
//			}
//		}
//		else {
//			TaskTypeApprovalPathDTO taskTypeApprovalPathDTO = new TaskTypeApprovalPathDTO();
//			taskTypeApprovalPathDTO.officeUnitOrganogramId = officeUnitOrganograms.superior_designation_id;
//
//			System.out.println("taskTypeApprovalPathDTO with map: "+taskTypeApprovalPathDTO);
//			List<Long> organogramIds = add(Collections.singletonList(taskTypeApprovalPathDTO), model, 1);
//			cardApprovalResponse.hasNextApproval = true;
//			cardApprovalResponse.organogramIds = organogramIds;
//		}
		return cardApprovalResponse;
	}

	private Vm_route_travel_withdraw_approval_mappingDTO buildDTO(long approverEmployeeRecordId, CreateWithdrawApprovalModel model, int level,
											  int isDeleted, ApprovalStatus approvalStatus) {

		//System.out.println("approverEmployeeRecordId with mapping: "+approverEmployeeRecordId);
		Vm_route_travel_withdraw_approval_mappingDTO cardApprovalMappingDTO = new Vm_route_travel_withdraw_approval_mappingDTO();
		Vm_route_travel_withdraw_approvalDTO cardApprovalDTO = Vm_route_travel_withdraw_approvalRepository.getInstance().getByEmployeeRecordId(approverEmployeeRecordId, model.getRequesterEmployeeRecordId());
		if (cardApprovalDTO == null) {
			return null;
		}
		cardApprovalMappingDTO.routeTravelWithdrawId = model.getCardInfoId();
		cardApprovalMappingDTO.routeTravelWithdrawApprovalId = cardApprovalDTO.iD;
		cardApprovalMappingDTO.withdrawApprovalStatusCat = approvalStatus.getValue();
		cardApprovalMappingDTO.sequence = level;
		cardApprovalMappingDTO.taskTypeId = model.getTaskTypeId();
		cardApprovalMappingDTO.insertedBy = cardApprovalMappingDTO.modifiedBy = model.getRequesterEmployeeRecordId();
		cardApprovalMappingDTO.insertionTime = cardApprovalMappingDTO.lastModificationTime = System.currentTimeMillis();
		cardApprovalMappingDTO.isDeleted = isDeleted;
		cardApprovalMappingDTO.employeeRecordsId = model.getEmployeeRecordId();
		cardApprovalMappingDTO.withdrawEmployeeInfoId = model.getCardEmployeeInfoId();
		cardApprovalMappingDTO.withdrawEmployeeOfficeInfoId = model.getCardEmployeeOfficeInfoId();
		cardApprovalMappingDTO.approverEmployeeRecordsId = cardApprovalDTO.employeeRecordId;
		return cardApprovalMappingDTO;
	}

	private List<Long> add(List<TaskTypeApprovalPathDTO> nextApprovalPath, CreateWithdrawApprovalModel model, int level) {
		long currentTime = System.currentTimeMillis();
		System.out.println("nextApprovalPath in mapping: "+nextApprovalPath);
		List<Long> addedToApproval = new ArrayList<>();
		nextApprovalPath.forEach(approver -> {
			EmployeeOfficeDTO approverOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(approver.officeUnitOrganogramId);
			if (approverOfficeDTO != null) {
				//for inserting data into approval table
				Vm_route_travel_withdraw_approval_mappingDTO dto = buildDTO(approverOfficeDTO.employeeRecordId, model, level, 0, ApprovalStatus.PENDING);
				if (dto != null) {
					try {
						add(dto);
						addedToApproval.add(approverOfficeDTO.officeUnitOrganogramId);
					} catch (Exception e) {
						logger.error(e);
					}
				}
			}
		});
		return addedToApproval;
	}

	public CardApprovalResponse movedToNextLevelOfApproval(CreateWithdrawApprovalModel model) throws InvalidDataException, AlreadyApprovedException {
		CardApprovalResponse response = new CardApprovalResponse();
		synchronized (LockManager.getLock(model.getCardInfoId()+"CWAM")) {
			ValidateResponse validateResponse = validate(model);
			String ids = validateResponse.dtoList.stream()
					.map(e -> String.valueOf(e.iD))
					.collect(Collectors.joining(","));
			String sql2 = String.format(updateStatus, ApprovalStatus.SATISFIED.getValue(), 0, model.getRequesterEmployeeRecordId(), System.currentTimeMillis(), ids);

			boolean res = (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
				try {
					st.executeUpdate(sql2);
					return true;
				} catch (SQLException ex) {
					logger.error(ex);
					return false;
				}
			});
			if (!res) {
				throw new InvalidDataException("Exception occurred during updating approval status");
			}
			int nextLevel = validateResponse.currentLevel + 1;
			List<TaskTypeApprovalPathDTO> nextApprovalPath = TaskTypeApprovalPathRepository.getInstance().getByTaskTypeIdAndLevel(validateResponse.cardApprovalMappingDTO.taskTypeId, nextLevel);

			if (nextApprovalPath == null || nextApprovalPath.size() == 0) {
				response.hasNextApproval = false;
				response.organogramIds = null;
			} else {
				List<Long> organogramIds = add(nextApprovalPath, model, nextLevel);
				response.hasNextApproval = true;
				response.organogramIds = organogramIds;
			}
		}
		return response;
	}

	public boolean rejectPendingApproval(CreateWithdrawApprovalModel model) throws InvalidDataException, AlreadyApprovedException {
		synchronized (LockManager.getLock(model.getCardInfoId()+"CWAM")) {
			validate(model);
			String sql = String.format(updateStatusByCardInfoId, ApprovalStatus.DISSATISFIED.getValue(), 0, model.getRequesterEmployeeRecordId(), System.currentTimeMillis(),
					ApprovalStatus.PENDING.getValue(), model.getCardInfoId());
			return (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
				try {
					st.executeUpdate(sql);
					return true;
				} catch (SQLException ex) {
					logger.error(ex);
					return false;
				}
			});
		}
	}

	private Vm_route_travel_withdraw_approval_mappingDAO.ValidateResponse validate(CreateWithdrawApprovalModel model) throws InvalidDataException, AlreadyApprovedException {
		String sql = String.format(getByCardInfoIdAndStatus, model.getCardInfoId(), ApprovalStatus.PENDING.getValue());
		List<Vm_route_travel_withdraw_approval_mappingDTO> dtoList = ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
		if (dtoList == null || dtoList.size() == 0) {
			throw new InvalidDataException("No Pending approval is found for cardInfoId : " + model.getCardInfoId());
		}
		Vm_route_travel_withdraw_approval_mappingDTO validRequester = dtoList.stream()
				.filter(dto -> dto.approverEmployeeRecordsId == model.getRequesterEmployeeRecordId())
				.findAny()
				.orElse(null);

		if (validRequester == null) {
			sql = String.format(getByCardInfoIdAndApproverEmployeeRecordsId, model.getCardInfoId(), model.getRequesterEmployeeRecordId());
			validRequester = ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
			if (validRequester != null) {
				throw new AlreadyApprovedException();
			}
			throw new InvalidDataException("Invalid employee request to approve");
		}
		Set<Integer> sequenceValueSet = dtoList.stream()
				.map(e -> e.sequence)
				.collect(Collectors.toSet());
		if (sequenceValueSet.size() > 1) {
			throw new InvalidDataException("Multiple sequence value is found for Pending approval of cardInfoId : " + model.getCardInfoId());
		}
		return new Vm_route_travel_withdraw_approval_mappingDAO.ValidateResponse(dtoList, (Integer) sequenceValueSet.toArray()[0],validRequester);
	}

	private static class ValidateResponse {
		List<Vm_route_travel_withdraw_approval_mappingDTO> dtoList;
		int currentLevel;
		Vm_route_travel_withdraw_approval_mappingDTO cardApprovalMappingDTO;

		public ValidateResponse(List<Vm_route_travel_withdraw_approval_mappingDTO> dtoList, int currentLevel,Vm_route_travel_withdraw_approval_mappingDTO cardApprovalMappingDTO) {
			this.dtoList = dtoList;
			this.currentLevel = currentLevel;
			this.cardApprovalMappingDTO = cardApprovalMappingDTO;
		}
	}

	public Vm_route_travel_withdraw_approval_mappingDTO getByCardInfoIdAndApproverEmployeeRecordId(long cardInfoId,long approverEmployeeId){
		String sql = String.format(getByCardInfoIdAndApproverEmployeeRecordsId, cardInfoId, approverEmployeeId);
		return ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
	}

	public Vm_route_travel_withdraw_approval_mappingDTO getByVm_fuel_requestIdAndApproverEmployeeRecordId(long cardInfoId,long approverEmployeeId){
		String sql = String.format(getByVm_fuel_requestInfoIdAndApproverEmployeeRecordsId, cardInfoId, approverEmployeeId);
		return ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
	}

//	public List<Card_approval_mappingDTO> getAllApprovalDTOByCardInfoId(long cardInfoId){
//		String sql = String.format(getByCardInfoId,cardInfoId);
//		List<Vm_route_travel_withdraw_approval_mappingDTO> list = ConnectionAndStatementUtil.getListOfT(sql,this::buildObjectFromResultSet);
//		return list.stream()
//				.filter(dto->dto.withdrawApprovalStatusCat == ApprovalStatus.PENDING.getValue() || dto.approverEmployeeRecordsId == dto.modifiedBy)
//				.collect(Collectors.toList());
//	}
	
//	Logger logger = Logger.getLogger(getClass());
//
//	private static final String getByCardInfoId = "SELECT * FROM vm_route_travel_withdraw_approval_mapping WHERE route_travel_withdraw_id = %d AND isDeleted = 0 ORDER BY sequence DESC";
//
//	public Vm_route_travel_withdraw_approval_mappingDAO(String tableName)
//	{
//		super(tableName);
//		joinSQL = "";
//		commonMaps = new Vm_route_travel_withdraw_approval_mappingMAPS(tableName);
//		columnNames = new String[]
//		{
//			"ID",
//			"route_travel_withdraw_id",
//			"route_travel_withdraw_approval_id",
//			"sequence",
//			"status",
//			"employee_record_id",
//			"withdraw_employee_info_id",
//			"withdraw_employee_office_info_id",
//			"approver_employee_records_id",
//			"inserted_by_user_id",
//			"inserted_by_organogram_id",
//			"insertion_date",
//			"isDeleted",
//			"lastModificationTime"
//		};
//	}
//
//	public Vm_route_travel_withdraw_approval_mappingDAO()
//	{
//		this("vm_route_travel_withdraw_approval_mapping");
//	}
//
//	public void setSearchColumn(Vm_route_travel_withdraw_approval_mappingDTO vm_route_travel_withdraw_approval_mappingDTO)
//	{
//		vm_route_travel_withdraw_approval_mappingDTO.searchColumn = "";
//	}
//
//	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
//	{
//		Vm_route_travel_withdraw_approval_mappingDTO vm_route_travel_withdraw_approval_mappingDTO = (Vm_route_travel_withdraw_approval_mappingDTO)commonDTO;
//		int index = 1;
//		long lastModificationTime = System.currentTimeMillis();
//		setSearchColumn(vm_route_travel_withdraw_approval_mappingDTO);
//		if(isInsert)
//		{
//			ps.setObject(index++,vm_route_travel_withdraw_approval_mappingDTO.iD);
//		}
//		ps.setObject(index++,vm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawId);
//		ps.setObject(index++,vm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawApprovalId);
//		ps.setObject(index++,vm_route_travel_withdraw_approval_mappingDTO.sequence);
//		ps.setObject(index++,vm_route_travel_withdraw_approval_mappingDTO.status);
//		ps.setObject(index++,vm_route_travel_withdraw_approval_mappingDTO.employeeRecordId);
//		ps.setObject(index++,vm_route_travel_withdraw_approval_mappingDTO.withdrawEmployeeInfoId);
//		ps.setObject(index++,vm_route_travel_withdraw_approval_mappingDTO.withdrawEmployeeOfficeInfoId);
//		ps.setObject(index++,vm_route_travel_withdraw_approval_mappingDTO.approverEmployeeRecordsId);
//		ps.setObject(index++,vm_route_travel_withdraw_approval_mappingDTO.insertedByUserId);
//		ps.setObject(index++,vm_route_travel_withdraw_approval_mappingDTO.insertedByOrganogramId);
//		ps.setObject(index++,vm_route_travel_withdraw_approval_mappingDTO.insertionDate);
//		if(isInsert)
//		{
//			ps.setObject(index++, 0);
//			ps.setObject(index++, lastModificationTime);
//		}
//	}
//
//	public Vm_route_travel_withdraw_approval_mappingDTO build(ResultSet rs)
//	{
//		try
//		{
//			Vm_route_travel_withdraw_approval_mappingDTO vm_route_travel_withdraw_approval_mappingDTO = new Vm_route_travel_withdraw_approval_mappingDTO();
//			vm_route_travel_withdraw_approval_mappingDTO.iD = rs.getLong("ID");
//			vm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawId = rs.getLong("route_travel_withdraw_id");
//			vm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawApprovalId = rs.getLong("route_travel_withdraw_approval_id");
//			vm_route_travel_withdraw_approval_mappingDTO.sequence = rs.getInt("sequence");
//			vm_route_travel_withdraw_approval_mappingDTO.status = rs.getInt("status");
//			vm_route_travel_withdraw_approval_mappingDTO.employeeRecordId = rs.getLong("employee_record_id");
//			vm_route_travel_withdraw_approval_mappingDTO.withdrawEmployeeInfoId = rs.getLong("withdraw_employee_info_id");
//			vm_route_travel_withdraw_approval_mappingDTO.withdrawEmployeeOfficeInfoId = rs.getLong("withdraw_employee_office_info_id");
//			vm_route_travel_withdraw_approval_mappingDTO.approverEmployeeRecordsId = rs.getLong("approver_employee_records_id");
//			vm_route_travel_withdraw_approval_mappingDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
//			vm_route_travel_withdraw_approval_mappingDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
//			vm_route_travel_withdraw_approval_mappingDTO.insertionDate = rs.getLong("insertion_date");
//			vm_route_travel_withdraw_approval_mappingDTO.isDeleted = rs.getInt("isDeleted");
//			vm_route_travel_withdraw_approval_mappingDTO.lastModificationTime = rs.getLong("lastModificationTime");
//			return vm_route_travel_withdraw_approval_mappingDTO;
//		}
//		catch (SQLException ex)
//		{
//			logger.error(ex);
//			return null;
//		}
//	}
//
//
//
//	public Vm_route_travel_withdraw_approval_mappingDTO getDTOByID (long id)
//	{
//		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
//		Vm_route_travel_withdraw_approval_mappingDTO vm_route_travel_withdraw_approval_mappingDTO = ConnectionAndStatementUtil.getT(sql,this::build);
//		return vm_route_travel_withdraw_approval_mappingDTO;
//	}
//
//
//	public List<Vm_route_travel_withdraw_approval_mappingDTO> getDTOs(Collection recordIDs)
//	{
//		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
//		for(int i = 0;i<recordIDs.size();i++){
//			if(i!=0){
//				sql+=",";
//			}
//			sql+=((ArrayList)recordIDs).get(i);
//		}
//		sql+=")  order by lastModificationTime desc";
//		printSql(sql);
//		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
//	}
//
	public List<Vm_route_travel_withdraw_approval_mappingDTO> getAllVm_route_travel_withdraw_approval_mapping (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;
		}
		sql += " order by " + tableName + ".lastModificationTime desc";

		System.out.println("createApprovalDTOForEmployeeRecordId 7 "+sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::buildObjectFromResultSet);
    }
//
//
//	public List<Vm_route_travel_withdraw_approval_mappingDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
//	{
//		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
//	}
//
//
//	public List<Vm_route_travel_withdraw_approval_mappingDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
//			String filter, boolean tableHasJobCat)
//	{
//		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
//		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
//	}
//	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
//			UserDTO userDTO, String filter, boolean tableHasJobCat)
//    {
//		boolean viewAll = false;
//
//		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
//		{
//			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
//			viewAll = true;
//		}
//		else
//		{
//			System.out.println("ViewAll is null ");
//		}
//
//		String sql = "SELECT ";
//		if(category == GETIDS)
//		{
//			sql += " " + tableName + ".ID as ID ";
//		}
//		else if(category == GETCOUNT)
//		{
//			sql += " count( " + tableName + ".ID) as countID ";
//		}
//		else if(category == GETDTOS)
//		{
//			sql += " distinct " + tableName + ".* ";
//		}
//		sql += "FROM " + tableName + " ";
//		sql += joinSQL;
//
//
//
//		String AnyfieldSql = "";
//		String AllFieldSql = "";
//
//		if(p_searchCriteria != null)
//		{
//
//
//			Enumeration names = p_searchCriteria.keys();
//			String str, value;
//
//			AnyfieldSql = "(";
//
//			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
//			{
//				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
//			}
//			AnyfieldSql += ")";
//			System.out.println("AnyfieldSql = " + AnyfieldSql);
//
//			AllFieldSql = "(";
//			int i = 0;
//			while(names.hasMoreElements())
//			{
//				str = (String) names.nextElement();
//				value = (String)p_searchCriteria.get(str);
//		        System.out.println(str + ": " + value);
//				if(value != null && !value.equalsIgnoreCase("") && (
//						 str.equals("insertion_date_start")
//						|| str.equals("insertion_date_end")
//				)
//
//				)
//				{
//					if(p_searchCriteria.get(str).equals("any"))
//					{
//						continue;
//					}
//
//					if( i > 0)
//					{
//						AllFieldSql+= " AND  ";
//					}
//
//					 if(str.equals("insertion_date_start"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("insertion_date_end"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
//						i ++;
//					}
//
//
//				}
//			}
//
//			AllFieldSql += ")";
//			System.out.println("AllFieldSql = " + AllFieldSql);
//
//
//		}
//
//
//		sql += " WHERE ";
//
//		sql += " (" + tableName + ".isDeleted = 0 ";
//		sql += ")";
//
//
//		if(!filter.equalsIgnoreCase(""))
//		{
//			sql += " and " + filter + " ";
//		}
//
//		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
//		{
//			sql += " AND " + AnyfieldSql;
//
//		}
//		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
//		{
//			sql += " AND " + AllFieldSql;
//		}
//
//
//
//		sql += " order by " + tableName + ".lastModificationTime desc ";
//
//		printSql(sql);
//
//		if(limit >= 0)
//		{
//			sql += " limit " + limit;
//		}
//		if(offset >= 0)
//		{
//			sql += " offset " + offset;
//		}
//
//		System.out.println("-------------- sql = " + sql);
//
//		return sql;
//    }
//
//	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
//			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
//    {
//		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
//    }
//
////	@Override
////	public Vm_route_travel_withdraw_approval_mappingDTO buildObjectFromResultSet(ResultSet rs) {
////		try {
////			Vm_route_travel_withdraw_approval_mappingDTO dto = new Vm_route_travel_withdraw_approval_mappingDTO();
////			dto.iD = rs.getLong("ID");
////			dto.routeTravelWithdrawId = rs.getLong("card_info_id");
////			dto.routeTravelWithdrawApprovalId = rs.getLong("card_approval_id");
////			dto.cardEmployeeInfoId = rs.getLong("card_employee_info_id");
////			dto.cardEmployeeOfficeInfoId = rs.getLong("card_employee_office_info_id");
////			dto.cardApprovalStatusCat = rs.getInt("card_approval_status_cat");
////			dto.sequence = rs.getInt("sequence");
////			dto.taskTypeId = rs.getLong("task_type_id");
////			dto.insertedBy = rs.getLong("inserted_by");
////			dto.insertionTime = rs.getLong("insertion_time");
////			dto.modifiedBy = rs.getLong("modified_by");
////			dto.lastModificationTime = rs.getLong("lastModificationTime");
////			dto.isDeleted = rs.getInt("isDeleted");
////			dto.employeeRecordId = rs.getInt("employee_records_id");
////			dto.approverEmployeeRecordsId = rs.getInt("approver_employee_records_id");
////			return dto;
////		} catch (SQLException ex) {
////			logger.error(ex);
////			return null;
////		}
////	}
//
//	private Vm_route_travel_withdraw_approval_mappingDTO buildDTO(long approverEmployeeRecordId, CreateWithdrawApprovalModel model, int level,
//											  int isDeleted, ApprovalStatus approvalStatus) {
//		Vm_route_travel_withdraw_approval_mappingDTO cardApprovalMappingDTO = new Vm_route_travel_withdraw_approval_mappingDTO();
//		CardApprovalDTO cardApprovalDTO = CardApprovalRepository.getInstance().getByEmployeeRecordId(approverEmployeeRecordId, model.getRequesterEmployeeRecordId());
//		if (cardApprovalDTO == null) {
//			return null;
//		}
//		cardApprovalMappingDTO.cardInfoId = model.getCardInfoId();
//		cardApprovalMappingDTO.cardApprovalId = cardApprovalDTO.iD;
//		cardApprovalMappingDTO.cardApprovalStatusCat = approvalStatus.getValue();
//		cardApprovalMappingDTO.sequence = level;
//		cardApprovalMappingDTO.taskTypeId = model.getTaskTypeId();
//		cardApprovalMappingDTO.insertedBy = cardApprovalMappingDTO.modifiedBy = model.getRequesterEmployeeRecordId();
//		cardApprovalMappingDTO.insertionTime = cardApprovalMappingDTO.lastModificationTime = System.currentTimeMillis();
//		cardApprovalMappingDTO.isDeleted = isDeleted;
//		cardApprovalMappingDTO.employeeRecordsId = model.getEmployeeRecordId();
//		cardApprovalMappingDTO.cardEmployeeInfoId = model.getCardEmployeeInfoId();
//		cardApprovalMappingDTO.cardEmployeeOfficeInfoId = model.getCardEmployeeOfficeInfoId();
//		cardApprovalMappingDTO.approverEmployeeRecordsId = cardApprovalDTO.employeeRecordId;
//		cardApprovalMappingDTO.cardEmployeeImagesId = model.getCardEmployeeImagesId();
//		return cardApprovalMappingDTO;
//	}
//
//	private List<Long> add(List<TaskTypeApprovalPathDTO> nextApprovalPath, CreateCardApprovalModel model, int level) {
//		long currentTime = System.currentTimeMillis();
//		List<Long> addedToApproval = new ArrayList<>();
//		nextApprovalPath.forEach(approver -> {
//			EmployeeOfficeDTO approverOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(approver.officeUnitOrganogramId);
//			if (approverOfficeDTO != null) {
//				Card_approval_mappingDTO dto = buildDTO(approverOfficeDTO.employeeRecordId, model, level, 0, ApprovalStatus.PENDING);
//				if (dto != null) {
//					try {
//						add(dto);
//						addedToApproval.add(approverOfficeDTO.officeUnitOrganogramId);
//					} catch (Exception e) {
//						logger.error(e);
//					}
//				}
//			}
//		});
//		return addedToApproval;
//	}
//
//	@Override
//	public void set(PreparedStatement ps, Vm_route_travel_withdraw_approval_mappingDTO vm_route_travel_withdraw_approval_mappingDTO, boolean isInsert) throws SQLException {
//
//	}
//
//	@Override
//	public Vm_route_travel_withdraw_approval_mappingDTO buildObjectFromResultSet(ResultSet rs) {
//		try {
//			Vm_route_travel_withdraw_approval_mappingDTO dto = new Vm_route_travel_withdraw_approval_mappingDTO();
//			dto.iD = rs.getLong("ID");
//			dto.routeTravelWithdrawId = rs.getLong("card_info_id");
//			dto.routeTravelWithdrawApprovalId = rs.getLong("card_approval_id");
//			dto.withdrawEmployeeInfoId = rs.getLong("card_employee_info_id");
//			dto.withdrawEmployeeOfficeInfoId = rs.getLong("card_employee_office_info_id");
//			dto.withdrawApprovalStatusCat = rs.getInt("card_approval_status_cat");
//			dto.sequence = rs.getInt("sequence");
//			dto.taskTypeId = rs.getLong("task_type_id");
//			dto.insertedBy = rs.getLong("inserted_by");
//			dto.insertionTime = rs.getLong("insertion_time");
//			dto.modifiedBy = rs.getLong("modified_by");
//			dto.lastModificationTime = rs.getLong("lastModificationTime");
//			dto.isDeleted = rs.getInt("isDeleted");
//			dto.employeeRecordsId = rs.getInt("employee_records_id");
//			dto.approverEmployeeRecordsId = rs.getInt("approver_employee_records_id");
//			return dto;
//		} catch (SQLException ex) {
//			logger.error(ex);
//			return null;
//		}
//	}
//
//	@Override
//	public String getTableName() {
//		return tableName;
//	}
//
//	public CardApprovalResponse createWithdrawApproval(CreateWithdrawApprovalModel model) throws Exception {
//		String sql = String.format(getByCardInfoId, model.getCardInfoId());
//		Vm_route_travel_withdraw_approval_mappingDTO dto = ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
//		if (dto != null) {
//			throw new DuplicateCardInfoException("Approval is already created for " + model.getCardInfoId());
//		}
//		CardApprovalResponse cardApprovalResponse = new CardApprovalResponse();
//		EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(model.getRequesterEmployeeRecordId());
//		OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
//		if ( officeUnitOrganograms.orgTree.endsWith("|")) {
//		//if (model.getCardCategoryEnum() == CardCategoryEnum.TEMPORARY && officeUnitOrganograms.orgTree.endsWith("|")) {
//			Vm_route_travel_withdraw_approval_mappingDTO vm_route_travel_withdraw_approval_mappingDTO;
//			vm_route_travel_withdraw_approval_mappingDTO = buildDTO(model.getRequesterEmployeeRecordId(),model, 1, 1, ApprovalStatus.SATISFIED);
//			if (vm_route_travel_withdraw_approval_mappingDTO == null) {
//				throw new CardApproverNotFoundException("Withdrawal Approver is not found for " + employeeOfficeDTO.employeeRecordId);
//			}
//			add(vm_route_travel_withdraw_approval_mappingDTO);
//			List<TaskTypeApprovalPathDTO> nextApprovalPath = TaskTypeApprovalPathRepository.getInstance().getByTaskTypeIdAndLevel(model.getTaskTypeId(), 2);
//			if (nextApprovalPath == null || nextApprovalPath.size() == 0) {
//				cardApprovalResponse.hasNextApproval = false;
//				cardApprovalResponse.organogramIds = null;
//			} else {
//				List<Long> organogramIds = add(nextApprovalPath, model, 2);
//				cardApprovalResponse.hasNextApproval = true;
//				cardApprovalResponse.organogramIds = organogramIds;
//			}
//		} else {
//			TaskTypeApprovalPathDTO taskTypeApprovalPathDTO = new TaskTypeApprovalPathDTO();
//			taskTypeApprovalPathDTO.officeUnitOrganogramId = officeUnitOrganograms.superior_designation_id;
//			List<Long> organogramIds = add(Collections.singletonList(taskTypeApprovalPathDTO), model, 1);
//			cardApprovalResponse.hasNextApproval = true;
//			cardApprovalResponse.organogramIds = organogramIds;
//		}
//		return cardApprovalResponse;
//	}
//
////	public CardApprovalResponse createVmWithdrawApproval(CreateCardApprovalModel model) throws Exception {
////		String sql = String.format(getByCardInfoId, model.getCardInfoId());
////		Vm_route_travel_withdraw_approval_mappingDTO dto = ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
////		if (dto != null) {
////			throw new DuplicateCardInfoException("Approval is already created for " + model.getCardInfoId());
////		}
////		CardApprovalResponse cardApprovalResponse = new CardApprovalResponse();
////		EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(model.getRequesterEmployeeRecordId());
////		OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
////		if (model.getCardCategoryEnum() == CardCategoryEnum.TEMPORARY && officeUnitOrganograms.orgTree.endsWith("|")) {
////			Card_approval_mappingDTO cardApprovalMappingDTO;
////			cardApprovalMappingDTO = buildDTO(model.getRequesterEmployeeRecordId(),model, 1, 1, ApprovalStatus.SATISFIED);
////			if (cardApprovalMappingDTO == null) {
////				throw new CardApproverNotFoundException("Card Approver is not found for " + employeeOfficeDTO.employeeRecordId);
////			}
////			add(cardApprovalMappingDTO);
////			List<TaskTypeApprovalPathDTO> nextApprovalPath = TaskTypeApprovalPathRepository.getInstance().getByTaskTypeIdAndLevel(model.getTaskTypeId(), 2);
////			if (nextApprovalPath == null || nextApprovalPath.size() == 0) {
////				cardApprovalResponse.hasNextApproval = false;
////				cardApprovalResponse.organogramIds = null;
////			} else {
////				List<Long> organogramIds = add(nextApprovalPath, model, 2);
////				cardApprovalResponse.hasNextApproval = true;
////				cardApprovalResponse.organogramIds = organogramIds;
////			}
////		} else {
////			TaskTypeApprovalPathDTO taskTypeApprovalPathDTO = new TaskTypeApprovalPathDTO();
////			taskTypeApprovalPathDTO.officeUnitOrganogramId = officeUnitOrganograms.superior_designation_id;
////			List<Long> organogramIds = add(Collections.singletonList(taskTypeApprovalPathDTO), model, 1);
////			cardApprovalResponse.hasNextApproval = true;
////			cardApprovalResponse.organogramIds = organogramIds;
////		}
////		return cardApprovalResponse;
////	}

}
	