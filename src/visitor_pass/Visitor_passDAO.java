package visitor_pass;

import java.io.IOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.*;


import gate_pass.Gate_passDAO;

import pb_notifications.Pb_notificationsDAO;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import repository.RepositoryManager;

import sms.SmsService;
import util.*;
import user.UserDTO;

public class Visitor_passDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	public static final int approvalStatusPending = 0;
	public static final int approvalStatusApproved = 1;
	public static final int approvalStatusCancelled = 2;
	public static final int approvalStatusReceived = 3;
	public static final int VISITOR_TYPE_LOCAL = 1;
	public static final int VISITOR_TYPE_FOREIGNER = 2;

	public static final long SAS_APPROVE_LEVEL_ID = 701;
	public static final long RECEIVER_DEPARTMENT_LEVEL_ID = 702;

	Gate_passDAO gate_passDAO = new Gate_passDAO();

	public Visitor_passDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Visitor_passMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"visiting_date",
			"visiting_time",
			"name",
			"address",
			"office_name",
			"designation",
			"mobile_number",
			"email",
			"enjoy_assembly",
			"approval_file",
			"type_of_visitor",
			"institutional_letter",
			"nationality_type",
			"profession_type",
			"passport_no",
			"visa_expiry",
			"payment_status",
			"search_column",
			"visit_notice_cat",
			"first_layer_approval_status",
			"first_layer_approved_by",
			"first_layer_approval_time",
			"second_layer_approval_status",
			"second_layer_approved_by",
			"second_layer_approval_time",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Visitor_passDAO()
	{
		this("visitor_pass");		
	}
	
	public void setSearchColumn(Visitor_passDTO visitor_passDTO)
	{
		visitor_passDTO.searchColumn = "";
		visitor_passDTO.searchColumn += visitor_passDTO.name + " ";
//		visitor_passDTO.searchColumn += visitor_passDTO.address + " ";
		visitor_passDTO.searchColumn += visitor_passDTO.mobileNumber + " ";
		visitor_passDTO.searchColumn += visitor_passDTO.email + " ";
		visitor_passDTO.searchColumn += visitor_passDTO.passportNo + " ";
	}
	
	public String getStatus(Visitor_passDTO visitor_passDTO, boolean isLangEng)
	{
		if(visitor_passDTO.firstLayerApprovalStatus == Visitor_passDAO.approvalStatusPending)
		{
			if(isLangEng)
			{
				return "Verification Pending";
			}
			else
			{
				return "যাচাইয়ের অপেক্ষায়";
			}
		}
		else if(visitor_passDTO.firstLayerApprovalStatus == Visitor_passDAO.approvalStatusCancelled)
		{
			if(isLangEng)
			{
				return "Verification Failed";
			}
			else
			{
				return "যাচাই করা যায় নি";
			}
		}
		else if(visitor_passDTO.firstLayerApprovalStatus == Visitor_passDAO.approvalStatusReceived 
        		&& visitor_passDTO.secondLayerApprovalStatus != Visitor_passDAO.approvalStatusApproved )
		{
			if(isLangEng)
			{
				return "Serjeant-at-arms Approval Pending";
			}
			else
			{
				return "সার্জেন্ট-অ্যাট-আর্মসের অনুমোদনের অপেক্ষায়";
			}
		}
		else if(visitor_passDTO.firstLayerApprovalStatus == Visitor_passDAO.approvalStatusReceived 
        		&& visitor_passDTO.secondLayerApprovalStatus == Visitor_passDAO.approvalStatusApproved )
		{
			if(isLangEng)
			{
				return "Approved";
			}
			else
			{
				return "অনুমোদিত";
			}
		}
		else if(visitor_passDTO.secondLayerApprovalStatus == Visitor_passDAO.approvalStatusCancelled)
		{
			if(isLangEng)
			{
				return "Rejected";
			}
			else
			{
				return "অনুমোদন বাতিল";
			}
		}
		return "";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Visitor_passDTO visitor_passDTO = (Visitor_passDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(visitor_passDTO);
		if(isInsert)
		{
			ps.setObject(index++,visitor_passDTO.iD);
		}
		ps.setObject(index++,visitor_passDTO.visitingDate);
		ps.setObject(index++,visitor_passDTO.visitingTime);
		ps.setObject(index++,visitor_passDTO.name);
		ps.setObject(index++,visitor_passDTO.address);
		ps.setObject(index++,visitor_passDTO.officeName);
		ps.setObject(index++,visitor_passDTO.designation);
		ps.setObject(index++,visitor_passDTO.mobileNumber);
		ps.setObject(index++,visitor_passDTO.email);
		ps.setObject(index++,visitor_passDTO.enjoyAssembly);
		ps.setObject(index++,visitor_passDTO.approvalFile);
		ps.setObject(index++,visitor_passDTO.typeOfVisitor);
		ps.setObject(index++,visitor_passDTO.fileID);
		ps.setObject(index++,visitor_passDTO.nationalityType);
		ps.setObject(index++,visitor_passDTO.professionType);
		ps.setObject(index++,visitor_passDTO.passportNo);
		ps.setObject(index++,visitor_passDTO.visaExpiry);
		ps.setObject(index++,visitor_passDTO.paymentStatus);
		ps.setObject(index++,visitor_passDTO.searchColumn);
		ps.setObject(index++,visitor_passDTO.visitNoticeCat);
		ps.setObject(index++,visitor_passDTO.firstLayerApprovalStatus);
		ps.setObject(index++,visitor_passDTO.firstLayerApprovedBy);
		ps.setObject(index++,visitor_passDTO.firstLayerApprovalTime);
		ps.setObject(index++,visitor_passDTO.secondLayerApprovalStatus);
		ps.setObject(index++,visitor_passDTO.secondLayerApprovedBy);
		ps.setObject(index++,visitor_passDTO.secondLayerApprovalTime);
		ps.setObject(index++,visitor_passDTO.insertedByUserId);
		ps.setObject(index++,visitor_passDTO.insertedByOrganogramId);
		ps.setObject(index++,visitor_passDTO.insertionDate);
		ps.setObject(index++,visitor_passDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Visitor_passDTO build(ResultSet rs)
	{
		try
		{
			Visitor_passDTO visitor_passDTO = new Visitor_passDTO();
			visitor_passDTO.iD = rs.getLong("ID");
			visitor_passDTO.visitingDate = rs.getLong("visiting_date");
			visitor_passDTO.visitingTime = rs.getString("visiting_time");
			visitor_passDTO.name = rs.getString("name");
			visitor_passDTO.address = rs.getString("address");
			visitor_passDTO.officeName = rs.getString("office_name");
			visitor_passDTO.designation = rs.getString("designation");
			visitor_passDTO.mobileNumber = rs.getString("mobile_number");
			visitor_passDTO.email = rs.getString("email");
			visitor_passDTO.enjoyAssembly = rs.getInt("enjoy_assembly");
			visitor_passDTO.approvalFile = rs.getLong("approval_file");
			visitor_passDTO.typeOfVisitor = rs.getInt("type_of_visitor");
			visitor_passDTO.fileID = rs.getLong("institutional_letter");
			visitor_passDTO.nationalityType = rs.getInt("nationality_type");
			visitor_passDTO.professionType = rs.getInt("profession_type");
			visitor_passDTO.passportNo = rs.getString("passport_no");
			visitor_passDTO.visaExpiry = rs.getLong("visa_expiry");
			visitor_passDTO.paymentStatus = rs.getInt("payment_status");
			visitor_passDTO.visitNoticeCat = rs.getInt("visit_notice_cat");
			visitor_passDTO.firstLayerApprovalStatus = rs.getInt("first_layer_approval_status");
			visitor_passDTO.firstLayerApprovedBy = rs.getString("first_layer_approved_by");
			visitor_passDTO.firstLayerApprovalTime = rs.getLong("first_layer_approval_time");
			visitor_passDTO.secondLayerApprovalStatus = rs.getInt("second_layer_approval_status");
			visitor_passDTO.secondLayerApprovedBy = rs.getString("second_layer_approved_by");
			visitor_passDTO.secondLayerApprovalTime = rs.getLong("second_layer_approval_time");
			visitor_passDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			visitor_passDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			visitor_passDTO.insertionDate = rs.getLong("insertion_date");
			visitor_passDTO.lastModifierUser = rs.getString("last_modifier_user");
			visitor_passDTO.isDeleted = rs.getInt("isDeleted");
			visitor_passDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return visitor_passDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

	public Visitor_passDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Visitor_passDTO visitor_passDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return visitor_passDTO;
	}

	
	public List<Visitor_passDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<Visitor_passDTO> getAllVisitor_pass (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Visitor_passDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}
	
	 public boolean canSeeAllVisitorPasses(UserDTO u_DTO) 
	 {

         return u_DTO.roleID == SessionConstants.ADMIN_ROLE || u_DTO.roleID == SessionConstants.GATE_MANAGEMENT_ADMIN_ROLE
                 || u_DTO.roleID == SessionConstants.VISITOR_APROVER;
     }
	 
	 

	
	public List<Visitor_passDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("visiting_date_start")
						|| str.equals("visiting_date_end")
						|| str.equals("name")
						|| str.equals("address")
						|| str.equals("mobile_number")
						|| str.equals("email")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
						|| str.equals("visiting_date")
						|| str.equals("passport_no")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("visiting_date_start"))
					{
						AllFieldSql += "" + tableName + ".visiting_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("visiting_date_end"))
					{
						AllFieldSql += "" + tableName + ".visiting_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("name"))
					{
						AllFieldSql += "" + tableName + ".name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("address"))
					{
						AllFieldSql += "" + tableName + ".address like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("mobile_number"))
					{
						AllFieldSql += "" + tableName + ".mobile_number like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("email"))
					{
						AllFieldSql += "" + tableName + ".email like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					 else if(str.equals("visiting_date"))
					 {
						 AllFieldSql += "" + tableName + ".visiting_date <= " + p_searchCriteria.get(str);
						 i ++;
					 }
					 else if(str.equals("passport_no"))
					 {
						 AllFieldSql += "" + tableName + ".passport_no <= " + p_searchCriteria.get(str);
						 i ++;
					 }
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
//		if(!filter.equalsIgnoreCase(""))
//		{
//			sql += " and " + filter + " ";
//		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
		if(!canSeeAllVisitorPasses(userDTO))
		{
			sql += " and " + tableName + ".inserted_by_user_id = " + userDTO.ID;
		}
		

		if (userDTO.roleID == SessionConstants.SERJEANT_AT_ARMS_ROLE) {
			sql += " and " + tableName + ".first_layer_approval_status = " + approvalStatusReceived;
		}


		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		long today = c.getTimeInMillis();
		//sql += " and " + tableName + ".visiting_date >= " + today;



		sql += " order by " + tableName + ".lastModificationTime desc ";


		/*if ((filter.equalsIgnoreCase("arms_office") && !gate_passDAO.isAuthenticApprover(userDTO, SAS_APPROVE_LEVEL_ID)) || (filter.equalsIgnoreCase("") && !gate_passDAO.isAuthenticApprover(userDTO, RECEIVER_DEPARTMENT_LEVEL_ID)) ) 
		{
			sql += " limit 0";
		}
		else */if(limit >= 0)
		{
			sql += " limit " + limit;
		}


		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }


	public void sendApprovalSMS(Visitor_passDTO dto) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String messageBody = "Dear "+dto.name+",\nYour applied Parliament Visitor Pass ("+simpleDateFormat.format(new Date(dto.visitingDate))
				+ ") has been approved!";

		try {
			SmsService.send(dto.mobileNumber, messageBody);
		} catch (IOException ioException) {
			logger.debug("Can't send sms on Gate Pass Approval");
		}

	}

	public void sendApprovalMail(Visitor_passDTO dto) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

//        Notifier notifier = new Notifier();
		String[] to = new String[1];
		Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();
		String subject = "Bangladesh Parliament VISITOR PASS approval notification";

		Visitor_passDAO dao = new Visitor_passDAO();

		String messageBody = "Dear "+dto.name+",\nYour applied visitor Pass ("+sdf.format(new Date(dto.visitingDate))
				+ ") has been approved!\n\nSincerely\nAnwar Hossain Zahid\nVisitor Receive Department\nBangladesh Parliament";

		to[0] = dto.email;

		try{
			logger.debug("in send mail ..." + messageBody);
			if(to == null) return;
//            notifier.sendEmail(to, EmailConstant.RECEIVER_SYSTEM_USER,"",subject, messageBody);
			pb_notificationsDAO.sendMail(to, subject, messageBody, -1, null);
			logger.debug("mail pushed successfully..." + messageBody);
		}catch(Exception ex){
			logger.fatal("", ex);
		}
	}
}
	