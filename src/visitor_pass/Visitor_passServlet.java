package visitor_pass;

import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import files.FilesDAO;
import files.FilesDTO;
import gate_pass.Gate_passDAO;


import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import login.LoginDTO;

import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;

import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;

import com.google.gson.Gson;

import pb.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import visitor_pass_affiliated_person.Visitor_pass_affiliated_personDAO;
import visitor_pass_affiliated_person.Visitor_pass_affiliated_personDTO;


/**
 * Servlet implementation class Visitor_passServlet
 */
@WebServlet("/Visitor_passServlet")
@MultipartConfig
public class Visitor_passServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Visitor_passServlet.class);
    private final FilesDAO filesDAO = new FilesDAO();

    String tableName = "visitor_pass";

    Visitor_passDAO visitor_passDAO;
    CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Visitor_passServlet() {
        super();
        try {
            visitor_passDAO = new Visitor_passDAO(tableName);
            commonRequestHandler = new CommonRequestHandler(visitor_passDAO);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getAddPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_ADD)) {
                    commonRequestHandler.getAddPage(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            }
            else if (actionType.equals("getEditPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_UPDATE)) {
                    getVisitor_pass(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            }
            else if (actionType.equals("downloadDropzoneFile")) {
                long id = Long.parseLong(request.getParameter("id"));

                FilesDTO filesDTO = (FilesDTO) filesDAO.getDTOByID(id);

                Utils.ProcessFile(request, response, filesDTO.fileTitle, filesDTO.inputStream);

                return;
            }
            else if (actionType.equals("getURL")) {
                String URL = request.getParameter("URL");
                System.out.println("URL = " + URL);
                response.sendRedirect(URL);
            }
            else if (actionType.equals("search") || actionType.equals("armsOfficeSearch")) {
                System.out.println("search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_SEARCH)) {
                    if (isPermanentTable) {
                        String filter = request.getParameter("filter");
                        System.out.println("filter = " + filter);
                        if (filter != null) {
                            filter = ""; //shouldn't be directly used, rather manipulate it.
                            searchVisitor_pass(request, response, isPermanentTable, filter);
                        } else {
                            searchVisitor_pass(request, response, isPermanentTable, "");
                        }
                    } else {
                        //searchVisitor_pass(request, response, tempTableName, isPermanentTable);
                    }
                }
            }
            else if (actionType.equals("view")) {
                System.out.println("view requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_SEARCH)) {
                    commonRequestHandler.view(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            }
            else if(actionType.equals("armsOfficeView")) {
                request.getRequestDispatcher("visitor_pass/visitor_pass_arms_officeView.jsp").forward(request, response);
            }
           
            else if(actionType.equals("getPaymentPage")) {
                request.getRequestDispatcher("visitor_pass/visitor_pass_paymentPage.jsp").forward(request, response);
            }
            else if(actionType.equals("makePayment")) {
                HttpSession session = request.getSession(false);
                Visitor_passDTO dto = (Visitor_passDTO) session.getAttribute("visitor_passDTO");
                session.removeAttribute("visitor_passDTO");

                dto.paymentStatus=1;
                visitor_passDAO.add(dto);

                List<Visitor_pass_affiliated_personDTO> visitor_pass_affiliated_personDTOList = (List<Visitor_pass_affiliated_personDTO>) session.getAttribute("visitor_pass_affiliated_personDTOList");
                session.removeAttribute("visitor_pass_affiliated_personDTOList");

                Visitor_pass_affiliated_personDAO visitor_pass_affiliated_personDAO = new Visitor_pass_affiliated_personDAO();

                for(Visitor_pass_affiliated_personDTO afdto : visitor_pass_affiliated_personDTOList) {
                    afdto.visitorPassId = dto.iD;
                    visitor_pass_affiliated_personDAO.add(afdto);
                }

                session.setAttribute("fireSwal", "yes");
                response.sendRedirect("Visitor_passServlet?actionType=getAddPage");
            }
            else if (actionType.equals("visitorPassFirstLayerApprovalFromView")) {
                String approvalStatus = request.getParameter("approvalstatus");

                long visitorPassId = Long.parseLong(request.getParameter("visitorPassId"));

                Visitor_passDTO dto = visitor_passDAO.getDTOByID(visitorPassId);
                if (approvalStatus.equalsIgnoreCase("approved")) {
                    if (dto.firstLayerApprovalStatus==Visitor_passDAO.approvalStatusPending || dto.firstLayerApprovalStatus==Visitor_passDAO.approvalStatusCancelled) {
                        dto.firstLayerApprovalStatus = Visitor_passDAO.approvalStatusReceived;
                        approvalStatus="received";
                    } else {
                        dto.firstLayerApprovalStatus = Visitor_passDAO.approvalStatusApproved;
                    }
                    dto.firstLayerApprovalTime = System.currentTimeMillis();
                    assert userDTO != null;
                    dto.firstLayerApprovedBy = userDTO.userName;

                    try {
                        dto.visitingDate = Long.parseLong(request.getParameter("visitingDate"));
                        dto.visitingTime = request.getParameter("visitingTime");
                        dto.approvalFile = Long.parseLong(request.getParameter("approvalFile"));

                        visitor_passDAO.sendApprovalSMS(dto);
                        visitor_passDAO.sendApprovalMail(dto);
                    } catch (NumberFormatException nfe) {
                        logger.debug(nfe);
                    }

                }
                else if (approvalStatus.equalsIgnoreCase("dismiss")) {
                    dto.firstLayerApprovalStatus = Gate_passDAO.approvalStatusCancelled;
                    dto.firstLayerApprovalTime = System.currentTimeMillis();
                    assert userDTO != null;
                    dto.firstLayerApprovedBy = userDTO.userName;
                }
                visitor_passDAO.update(dto);

                HttpSession session = request.getSession(false);
                session.setAttribute("notificationAfterApproval", approvalStatus);

                response.sendRedirect(getServletContext().getContextPath()+"/Visitor_passServlet?actionType=search");
            }
            else if (actionType.equals("visitorPassSecondLayerApprovalFromView")) {
                String approvalStatus = request.getParameter("approvalstatus");
                long visitorPassId = Long.parseLong(request.getParameter("visitorPassId"));

                Visitor_passDTO dto = visitor_passDAO.getDTOByID(visitorPassId);
                if (approvalStatus.equalsIgnoreCase("approved")) {
                    dto.secondLayerApprovalStatus = Visitor_passDAO.approvalStatusApproved;
                } else {
                    dto.secondLayerApprovalStatus = Visitor_passDAO.approvalStatusCancelled;
                }
                dto.secondLayerApprovalTime = System.currentTimeMillis();
                assert userDTO != null;
                dto.secondLayerApprovedBy = userDTO.userName;

                HttpSession session = request.getSession(false);
                session.setAttribute("notificationAfterApproval", approvalStatus);

                visitor_passDAO.update(dto);
                response.sendRedirect(getServletContext().getContextPath()+"/Visitor_passServlet?actionType=armsOfficeSearch");

            }
            else {
                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        System.out.println("doPost");
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("add")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_ADD)) {
                    System.out.println("going to  addVisitor_pass ");
                    addVisitor_pass(request, response, true, userDTO, true);
                } else {
                    System.out.println("Not going to  addVisitor_pass ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            }
            else if (actionType.equals("UploadFilesFromDropZone")) {

                long ColumnID = Long.parseLong(request.getParameter("ColumnID"));
                String pageType = request.getParameter("pageType");

                logger.debug("In " + pageType);
                Utils.UploadFilesFromDropZone(request, response, userDTO.ID, ColumnID, "Visitor_passServlet");

            }
            else if(actionType.equals("upload"))
			{
            	if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_ADD)) {
                    System.out.println("going to  upload xl ");
                    uploadXl(request, response, userDTO);
                } else {
                    System.out.println("Not going to  addVisitor_pass ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
			}
            else if (actionType.equals("getDTO")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_ADD)) {
                    getDTO(request, response);
                } else {
                    System.out.println("Not going to  addVisitor_pass ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            }
            else if (actionType.equals("edit")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_UPDATE)) {
                    addVisitor_pass(request, response, false, userDTO, isPermanentTable);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            }
            else if (actionType.equals("visitorPassFirstLayerApproval")) {
				String approvalStatus = request.getParameter("approvalstatus");
				long visitorPassId = Long.parseLong(request.getParameter("visitorPassId"));

				Visitor_passDTO dto = visitor_passDAO.getDTOByID(visitorPassId);
				if (approvalStatus.equalsIgnoreCase("approved")) {
				    if (dto.firstLayerApprovalStatus==Visitor_passDAO.approvalStatusPending || dto.firstLayerApprovalStatus==Visitor_passDAO.approvalStatusCancelled) {
                        dto.firstLayerApprovalStatus = Visitor_passDAO.approvalStatusReceived;
                    } else {
                        dto.firstLayerApprovalStatus = Visitor_passDAO.approvalStatusApproved;
                    }
					dto.firstLayerApprovalTime = System.currentTimeMillis();
					assert userDTO != null;
					dto.firstLayerApprovedBy = userDTO.userName;

					try {
                        dto.visitingDate = Long.parseLong(request.getParameter("visitingDate"));
                        dto.visitingTime = request.getParameter("visitingTime");
                        dto.approvalFile = Long.parseLong(request.getParameter("approvalFile"));

                        visitor_passDAO.sendApprovalSMS(dto);
                        visitor_passDAO.sendApprovalMail(dto);
                    } catch (NumberFormatException nfe) {
					    logger.debug(nfe);
                    }


				}
				else if (approvalStatus.equalsIgnoreCase("dismiss")) {
					dto.firstLayerApprovalStatus = Gate_passDAO.approvalStatusCancelled;
					dto.firstLayerApprovalTime = System.currentTimeMillis();
					assert userDTO != null;
					dto.firstLayerApprovedBy = userDTO.userName;
				}
				visitor_passDAO.update(dto);
			}
            else if (actionType.equals("visitorPassSecondLayerApproval")) {
                String approvalStatus = request.getParameter("approvalstatus");
                long gatePassId = Long.parseLong(request.getParameter("gatePassId"));

                Visitor_passDTO dto = visitor_passDAO.getDTOByID(gatePassId);
                if (approvalStatus.equalsIgnoreCase("approved")) {
                    dto.secondLayerApprovalStatus = Visitor_passDAO.approvalStatusApproved;
                } else {
                    dto.secondLayerApprovalStatus = Visitor_passDAO.approvalStatusCancelled;
                }
                dto.secondLayerApprovalTime = System.currentTimeMillis();
                assert userDTO != null;
                dto.secondLayerApprovedBy = userDTO.userName;

                visitor_passDAO.update(dto);
            }
            else if (actionType.equals("delete")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_UPDATE)) {
                    commonRequestHandler.delete(request, response, userDTO);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            }
            else if (actionType.equals("search")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_SEARCH)) {
                    searchVisitor_pass(request, response, true, "");
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            }
            else if (actionType.equals("getGeo")) {
                System.out.println("going to geoloc ");
                request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void uploadXl(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException {
    	System.out.println("%%%% ajax upload called");
		Part part;
		try
		{
			part = request.getPart("testing_excelDatabase");
			String Value = commonRequestHandler.getFileName(part);
			int visitorType = Integer.parseInt(request.getParameter("visitorType"));
			System.out.println("file name = " + Value);
			if( (Value != null && !Value.equalsIgnoreCase("")))
			{
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					String FileName = "visitorList";
					String path = getServletContext().getRealPath("/img2/");
					System.out.println("filePath: "+path);
					Utils.uploadFile(part, FileName, path);
					List<Visitor_pass_affiliated_personDTO> visitor_pass_affiliated_personDTOList = ReadXLsToArraylist(request, FileName, visitorType);
					
					PrintWriter out = response.getWriter();
					response.setContentType("application/json");
					response.setCharacterEncoding("UTF-8");
					String encoded = this.gson.toJson(visitor_pass_affiliated_personDTOList);
					System.out.println("json encoded data = " + encoded);
					out.print(encoded);
					out.flush();
				
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
    
	public ArrayList<Visitor_pass_affiliated_personDTO> ReadXLsToArraylist(HttpServletRequest request, String fileName, int visitorType) throws IOException
	{
		String path = getServletContext().getRealPath("/img2/");
		File excelFile = new File(path + File.separator
				+ fileName);
		FileInputStream fis = new FileInputStream(excelFile);


		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workbook.getSheetAt(0);
		Iterator<Row> rowIt = sheet.iterator();
		ArrayList<String> Rows = new ArrayList<String>();
		ArrayList<Visitor_pass_affiliated_personDTO> visitor_pass_affiliated_personDTOs = new ArrayList<Visitor_pass_affiliated_personDTO>();


		Visitor_pass_affiliated_personDTO visitor_pass_affiliated_personDTO;

		String failureMessage = "";
		
		boolean is1stRow = true;
		while(rowIt.hasNext())
		{

			Row row = rowIt.next();

			Iterator<Cell> cellIterator = row.cellIterator();

			visitor_pass_affiliated_personDTO = new Visitor_pass_affiliated_personDTO();

			int j = 0;

			try
			{
				if(is1stRow){
					is1stRow = false;
					continue;
				}
				while (cellIterator.hasNext())
				{
					Cell cell = cellIterator.next();

					
					String value = Jsoup.clean(cell.toString(), Whitelist.simpleText());
					
					if(visitorType == Visitor_passDTO.LOCAL)
					{
						if(j == 0)
						{
							visitor_pass_affiliated_personDTO.mobileNumber = value;
						}
						else if(j == 1)
						{
							if(value.toLowerCase().startsWith("n"))
							{
								visitor_pass_affiliated_personDTO.credentialCat = 1;
							}
							else if(value.toLowerCase().startsWith("p"))
							{
								visitor_pass_affiliated_personDTO.credentialCat = 2;
							}
							else if(value.toLowerCase().startsWith("b"))
							{
								visitor_pass_affiliated_personDTO.credentialCat = 3;
							}
						}
						else if(j == 2)
						{
							visitor_pass_affiliated_personDTO.credentialNo = value;
						}
						else if(j == 3)
						{
							visitor_pass_affiliated_personDTO.name = value;
						}
					}
					else
					{
						if(j == 0)
						{
							visitor_pass_affiliated_personDTO.name = value;
						}
						else if(j == 1)
						{
							visitor_pass_affiliated_personDTO.credentialNo = value;
						}
						else if(j == 2)
						{
							visitor_pass_affiliated_personDTO.nationality = value;
						}
					}

					j ++;

				}

				System.out.println("INSERTING to the list: " + visitor_pass_affiliated_personDTO);
				visitor_pass_affiliated_personDTOs.add(visitor_pass_affiliated_personDTO);
				
			}
			catch (Exception e)
			{
				e.printStackTrace();

			}



			System.out.println();

		}
		if(failureMessage.equalsIgnoreCase(""))
		{
			failureMessage = " Successfully parsed all rows";
		}
		else
		{
			failureMessage = " Failed on rows: " + failureMessage;
		}
		request.setAttribute("failureMessage", failureMessage);

		workbook.close();
		fis.close();
		return visitor_pass_affiliated_personDTOs;

	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response) {
        try {
            System.out.println("In getDTO");
            Visitor_passDTO visitor_passDTO = visitor_passDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            String encoded = this.gson.toJson(visitor_passDTO);
            System.out.println("json encoded data = " + encoded);
            out.print(encoded);
            out.flush();
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void addVisitor_pass(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addVisitor_pass");
            String path = getServletContext().getRealPath("/img2/");
            Visitor_passDTO visitor_passDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

            if (addFlag == true) {
                visitor_passDTO = new Visitor_passDTO();
            } else {
                visitor_passDTO = visitor_passDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
            }
            String FileNamePrefix;
            if (addFlag == true) {
                FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
            } else {
                FileNamePrefix = request.getParameter("iD");
            }

            String Value = "";

            Value = request.getParameter("visitingDate");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("visitingDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    Date d = f.parse(Value);
                    visitor_passDTO.visitingDate = d.getTime();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("visitingTime");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("visitingTime = " + Value);
            if (Value != null) {
                visitor_passDTO.visitingTime = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            
            Value = request.getParameter("visitNoticeCat");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("visitNoticeCat = " + Value);
            if (Value != null) {
                visitor_passDTO.visitNoticeCat = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("name");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("name = " + Value);
            if (Value != null) {
                visitor_passDTO.name = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("address");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("address = " + Value);
            if (Value != null) {
                visitor_passDTO.address = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            
            Value = request.getParameter("officeName");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("officeName = " + Value);
            if (Value != null) {
                visitor_passDTO.officeName = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            
            Value = request.getParameter("designation");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("designation = " + Value);
            if (Value != null) {
                visitor_passDTO.designation = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("mobileNumber");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("mobileNumber = " + Value);
            if (Value != null) {
                visitor_passDTO.mobileNumber = "88"+Value;
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("email");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("email = " + Value);
            if (Value != null) {
                visitor_passDTO.email = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("enjoyAssembly");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("enjoyAssembly = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                visitor_passDTO.enjoyAssembly = Integer.parseInt(Value);
            }

            Value = request.getParameter("localOrForeign");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("typeOfVisitor = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                visitor_passDTO.typeOfVisitor = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("filesDropzone");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }

            logger.debug("filesDropzone = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                logger.debug("filesDropzone = " + Value);
                if (!Value.equalsIgnoreCase("")) {
                    visitor_passDTO.fileID = Long.parseLong(Value);
                } else {
                    logger.debug("FieldName has a null Value, not updating" + " = " + Value);
                }

                if (!addFlag) {
                    String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                    String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                    for (int i = 0; i < deleteArray.length; i++) {
                        if (i > 0) {
                            filesDAO.delete(Long.parseLong(deleteArray[i]));
                        }
                    }
                }
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }



            Value = request.getParameter("nationality");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("nationalityType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                visitor_passDTO.nationalityType = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("professionType");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("professionType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                visitor_passDTO.professionType = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("passportNo");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("passportNo = " + Value);
            if (Value != null) {
                visitor_passDTO.passportNo = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("visaExpiry");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("visaExpiry = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
				try {
					Date d = f.parse(Value);
					visitor_passDTO.visitingDate = d.getTime();
				} catch (Exception e) {
					e.printStackTrace();
				}
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("paymentStatus");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paymentStatus = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                visitor_passDTO.paymentStatus = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("firstLayerApprovalStatus");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("firstLayerApprovalStatus = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                visitor_passDTO.firstLayerApprovalStatus = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("firstLayerApprovedBy");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("firstLayerApprovedBy = " + Value);
            if (Value != null) {
                visitor_passDTO.firstLayerApprovedBy = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("firstLayerApprovalTime");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("firstLayerApprovalTime = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                visitor_passDTO.firstLayerApprovalTime = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("secondLayerApprovalStatus");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("secondLayerApprovalStatus = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                visitor_passDTO.secondLayerApprovalStatus = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("secondLayerApprovedBy");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("secondLayerApprovedBy = " + Value);
            if (Value != null) {
                visitor_passDTO.secondLayerApprovedBy = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("secondLayerApprovalTime");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("secondLayerApprovalTime = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                visitor_passDTO.secondLayerApprovalTime = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            if (addFlag) {
                visitor_passDTO.insertedByUserId = userDTO.ID;
            }


            if (addFlag) {
                visitor_passDTO.insertedByOrganogramId = userDTO.organogramID;
            }


            if (addFlag) {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);

                visitor_passDTO.insertionDate = c.getTimeInMillis();
            }


            visitor_passDTO.lastModifierUser = userDTO.userName;


            System.out.println("Done adding  addVisitor_pass dto = " + visitor_passDTO);

            HttpSession session = request.getSession(false);

            int personCount = Integer.parseInt(request.getParameter("additionalPersonCount"));

            List<Visitor_pass_affiliated_personDTO> visitor_pass_affiliated_personDTOList = new ArrayList<>();
            for(int i = 0; i < personCount; i++){
                if(request.getParameter("name_"+i).equals(""))
                    continue;

                Visitor_pass_affiliated_personDTO dto = new Visitor_pass_affiliated_personDTO();
                dto.visitorPassId = visitor_passDTO.iD;

                if(request.getParameter("credentialType_"+i) != null)
                    dto.credentialCat = Integer.parseInt(request.getParameter("credentialType_"+i));

                dto.credentialNo = request.getParameter("credentialNo_"+i);

                if(request.getParameter("mobileNumber_"+i) != null)
                    dto.mobileNumber = request.getParameter("mobileNumber_"+i);

                if(request.getParameter("nationality_"+i) != null)
                    dto.nationality = (request.getParameter("nationality_"+i));

                dto.name = request.getParameter("name_"+i);

                System.out.println(dto);
                visitor_pass_affiliated_personDTOList.add(dto);
                //visitor_pass_affiliated_personDAO.add(dto);
            }

            if (visitor_passDTO.typeOfVisitor==Visitor_passDAO.VISITOR_TYPE_LOCAL) {
                long visitorPassId = visitor_passDAO.manageWriteOperations(visitor_passDTO, SessionConstants.INSERT, -1, userDTO);

                Visitor_pass_affiliated_personDAO visitor_pass_affiliated_personDAO = new Visitor_pass_affiliated_personDAO();

                for(Visitor_pass_affiliated_personDTO dto : visitor_pass_affiliated_personDTOList) {
                    dto.visitorPassId = visitorPassId;
                    visitor_pass_affiliated_personDAO.add(dto);
                }

                session.setAttribute("fireSwal", "yes");
                response.sendRedirect("Visitor_passServlet?actionType=getAddPage");
            } else {
                session.setAttribute("visitor_passDTO", visitor_passDTO);
                session.setAttribute("visitor_pass_affiliated_personDTOList", visitor_pass_affiliated_personDTOList);
                response.sendRedirect("Visitor_passServlet?actionType=getPaymentPage");
            }




//            long returnedID = -1;
//
//            if (isPermanentTable == false) //add new row for validation and make the old row outdated
//            {
//                visitor_passDAO.setIsDeleted(visitor_passDTO.iD, CommonDTO.OUTDATED);
//                returnedID = visitor_passDAO.add(visitor_passDTO);
//                visitor_passDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
//            } else if (addFlag == true) {
//                returnedID = visitor_passDAO.manageWriteOperations(visitor_passDTO, SessionConstants.INSERT, -1, userDTO);
//            } else {
//                returnedID = visitor_passDAO.manageWriteOperations(visitor_passDTO, SessionConstants.UPDATE, -1, userDTO);
//            }
//
//
//            if (isPermanentTable) {
//                String inPlaceSubmit = (String) request.getParameter("inplacesubmit");
//
//                if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
//                    getVisitor_pass(request, response, returnedID);
//                } else {
//                    response.sendRedirect("Visitor_passServlet?actionType=search");
//                }
//            } else {
//                commonRequestHandler.validate(visitor_passDAO.getDTOByID(returnedID), request, response, userDTO);
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getVisitor_pass(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {
        System.out.println("in getVisitor_pass");
        Visitor_passDTO visitor_passDTO = null;
        try {
            visitor_passDTO = visitor_passDAO.getDTOByID(id);
            request.setAttribute("ID", visitor_passDTO.iD);
            request.setAttribute("visitor_passDTO", visitor_passDTO);
            request.setAttribute("visitor_passDAO", visitor_passDAO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "visitor_pass/visitor_passInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "visitor_pass/visitor_passSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "visitor_pass/visitor_passEditBody.jsp?actionType=edit";
                } else {
                    URL = "visitor_pass/visitor_passEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getVisitor_pass(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getVisitor_pass(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void searchVisitor_pass(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  searchVisitor_pass 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_VISITOR_PASS,
                request,
                visitor_passDAO,
                SessionConstants.VIEW_VISITOR_PASS,
                SessionConstants.SEARCH_VISITOR_PASS,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("visitor_passDAO", visitor_passDAO);
        RequestDispatcher rd;
        if (!isPermanent) {
            if (hasAjax == false) {
                System.out.println("Going to visitor_pass/visitor_passApproval.jsp");
                rd = request.getRequestDispatcher("visitor_pass/visitor_passApproval.jsp");
            } else {
                System.out.println("Going to visitor_pass/visitor_passApprovalForm.jsp");
                rd = request.getRequestDispatcher("visitor_pass/visitor_passApprovalForm.jsp");
            }
        } else {
            if (hasAjax == false) {
                System.out.println("Going to visitor_pass/visitor_passSearch.jsp");
                rd = request.getRequestDispatcher("visitor_pass/visitor_passSearch.jsp");
            } else {
                System.out.println("Going to visitor_pass/visitor_passSearchForm.jsp");
                rd = request.getRequestDispatcher("visitor_pass/visitor_passSearchForm.jsp");
            }
        }
        rd.forward(request, response);
    }

    private void armsOfficeSearchGate_pass(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in arms office search Visitor_pass 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_VISITOR_PASS,
                request,
                visitor_passDAO,
                SessionConstants.VIEW_VISITOR_PASS,
                SessionConstants.SEARCH_VISITOR_PASS,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("visitor_passDAO", visitor_passDAO);
        RequestDispatcher rd;


        if (hasAjax == false) {
            System.out.println("Going to visitor_pass/visitor_pass_arms_officeSearch.jsp");
            rd = request.getRequestDispatcher("visitor_pass/visitor_pass_arms_officeSearch.jsp");
        } else {
            System.out.println("Going to visitor_pass/visitor_pass_arms_officeSearchForm.jsp");
            rd = request.getRequestDispatcher("visitor_pass/visitor_pass_arms_officeSearchForm.jsp");
        }

        rd.forward(request, response);
    }

}

