package visitor_pass;

import util.*; 


public class Visitor_passDTO extends CommonDTO
{

	public long visitingDate = System.currentTimeMillis();
    public String visitingTime = "";
    public String name = "";
    public String address = "";
    public String officeName = "";
    public String designation = "";
    public String mobileNumber = "";
    public String email = "";
	public int enjoyAssembly = 0;
	public long approvalFile = 0;
	public int typeOfVisitor = -1;
	public byte[] institutionalLetter = null;	
	public int nationalityType = -1;
	public int professionType = -1;
    public String passportNo = "";
	public long visaExpiry = -1;
	public int paymentStatus = 0;
	public int firstLayerApprovalStatus = 0;
    public String firstLayerApprovedBy = "";
	public long firstLayerApprovalTime = -1;
	public int secondLayerApprovalStatus = 0;
    public String secondLayerApprovedBy = "";
	public long secondLayerApprovalTime = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
    
    public int visitNoticeCat = -1;
    
    public static final long SAMPLE_XL_ID = 67700L;
    public static final long FOREIGN_XL_ID = 67701L;
    
    public static final int LOCAL = 1;
    public static final int FOREIGNER = 2;
    
    public static final int BANGLADESHI = 1;
    public static final int OTHERS = 200;
    
    public static final int SHORT_TIME = 0;
    public static final int LONG_TIME = 1;
    
	
	
    @Override
	public String toString() {
            return "$Visitor_passDTO[" +
            " iD = " + iD +
            " visitingDate = " + visitingDate +
            " visitingTime = " + visitingTime +
            " name = " + name +
            " address = " + address +
            " mobileNumber = " + mobileNumber +
            " email = " + email +
            " enjoyAssembly = " + enjoyAssembly +
            " approvalFile = " + approvalFile +
            " typeOfVisitor = " + typeOfVisitor +
            " institutionalLetter = " + institutionalLetter +
            " nationalityType = " + nationalityType +
            " professionType = " + professionType +
            " passportNo = " + passportNo +
            " visaExpiry = " + visaExpiry +
            " paymentStatus = " + paymentStatus +
            " firstLayerApprovalStatus = " + firstLayerApprovalStatus +
            " firstLayerApprovedBy = " + firstLayerApprovedBy +
            " firstLayerApprovalTime = " + firstLayerApprovalTime +
            " secondLayerApprovalStatus = " + secondLayerApprovalStatus +
            " secondLayerApprovedBy = " + secondLayerApprovedBy +
            " secondLayerApprovalTime = " + secondLayerApprovalTime +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}