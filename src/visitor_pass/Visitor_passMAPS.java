package visitor_pass;
import java.util.*; 
import util.*;


public class Visitor_passMAPS extends CommonMaps
{	
	public Visitor_passMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("visitingDate".toLowerCase(), "visitingDate".toLowerCase());
		java_DTO_map.put("visitingTime".toLowerCase(), "visitingTime".toLowerCase());
		java_DTO_map.put("name".toLowerCase(), "name".toLowerCase());
		java_DTO_map.put("address".toLowerCase(), "address".toLowerCase());
		java_DTO_map.put("mobileNumber".toLowerCase(), "mobileNumber".toLowerCase());
		java_DTO_map.put("email".toLowerCase(), "email".toLowerCase());
		java_DTO_map.put("enjoyAssembly".toLowerCase(), "enjoyAssembly".toLowerCase());
		java_DTO_map.put("approvalFile".toLowerCase(), "approvalFile".toLowerCase());
		java_DTO_map.put("typeOfVisitor".toLowerCase(), "typeOfVisitor".toLowerCase());
		java_DTO_map.put("institutionalLetter".toLowerCase(), "institutionalLetter".toLowerCase());
		java_DTO_map.put("nationalityType".toLowerCase(), "nationalityType".toLowerCase());
		java_DTO_map.put("professionType".toLowerCase(), "professionType".toLowerCase());
		java_DTO_map.put("passportNo".toLowerCase(), "passportNo".toLowerCase());
		java_DTO_map.put("visaExpiry".toLowerCase(), "visaExpiry".toLowerCase());
		java_DTO_map.put("paymentStatus".toLowerCase(), "paymentStatus".toLowerCase());
		java_DTO_map.put("firstLayerApprovalStatus".toLowerCase(), "firstLayerApprovalStatus".toLowerCase());
		java_DTO_map.put("firstLayerApprovedBy".toLowerCase(), "firstLayerApprovedBy".toLowerCase());
		java_DTO_map.put("firstLayerApprovalTime".toLowerCase(), "firstLayerApprovalTime".toLowerCase());
		java_DTO_map.put("secondLayerApprovalStatus".toLowerCase(), "secondLayerApprovalStatus".toLowerCase());
		java_DTO_map.put("secondLayerApprovedBy".toLowerCase(), "secondLayerApprovedBy".toLowerCase());
		java_DTO_map.put("secondLayerApprovalTime".toLowerCase(), "secondLayerApprovalTime".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("lastModifierUser".toLowerCase(), "lastModifierUser".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("visiting_date".toLowerCase(), "visitingDate".toLowerCase());
		java_SQL_map.put("visiting_time".toLowerCase(), "visitingTime".toLowerCase());
		java_SQL_map.put("name".toLowerCase(), "name".toLowerCase());
		java_SQL_map.put("address".toLowerCase(), "address".toLowerCase());
		java_SQL_map.put("mobile_number".toLowerCase(), "mobileNumber".toLowerCase());
		java_SQL_map.put("email".toLowerCase(), "email".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Visiting Date".toLowerCase(), "visitingDate".toLowerCase());
		java_Text_map.put("Visiting Time".toLowerCase(), "visitingTime".toLowerCase());
		java_Text_map.put("Name".toLowerCase(), "name".toLowerCase());
		java_Text_map.put("Address".toLowerCase(), "address".toLowerCase());
		java_Text_map.put("Mobile Number".toLowerCase(), "mobileNumber".toLowerCase());
		java_Text_map.put("Email".toLowerCase(), "email".toLowerCase());
		java_Text_map.put("Enjoy Assembly".toLowerCase(), "enjoyAssembly".toLowerCase());
		java_Text_map.put("Approval File".toLowerCase(), "approvalFile".toLowerCase());
		java_Text_map.put("Type Of Visitor".toLowerCase(), "typeOfVisitor".toLowerCase());
		java_Text_map.put("Institutional Letter".toLowerCase(), "institutionalLetter".toLowerCase());
		java_Text_map.put("Nationality".toLowerCase(), "nationalityType".toLowerCase());
		java_Text_map.put("Profession".toLowerCase(), "professionType".toLowerCase());
		java_Text_map.put("Passport No".toLowerCase(), "passportNo".toLowerCase());
		java_Text_map.put("Visa Expiry".toLowerCase(), "visaExpiry".toLowerCase());
		java_Text_map.put("Payment Status".toLowerCase(), "paymentStatus".toLowerCase());
		java_Text_map.put("First Layer Approval Status".toLowerCase(), "firstLayerApprovalStatus".toLowerCase());
		java_Text_map.put("First Layer Approved By".toLowerCase(), "firstLayerApprovedBy".toLowerCase());
		java_Text_map.put("First Layer Approval Time".toLowerCase(), "firstLayerApprovalTime".toLowerCase());
		java_Text_map.put("Second Layer Approval Status".toLowerCase(), "secondLayerApprovalStatus".toLowerCase());
		java_Text_map.put("Second Layer Approved By".toLowerCase(), "secondLayerApprovedBy".toLowerCase());
		java_Text_map.put("Second Layer Approval Time".toLowerCase(), "secondLayerApprovalTime".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Last Modifier User".toLowerCase(), "lastModifierUser".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}