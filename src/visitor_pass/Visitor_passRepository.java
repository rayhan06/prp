package visitor_pass;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Visitor_passRepository implements Repository {
	Visitor_passDAO visitor_passDAO = new Visitor_passDAO();
	
	public void setDAO(Visitor_passDAO visitor_passDAO)
	{
		this.visitor_passDAO = visitor_passDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Visitor_passRepository.class);
	Map<Long, Visitor_passDTO>mapOfVisitor_passDTOToiD;
	Map<Long, Set<Visitor_passDTO> >mapOfVisitor_passDTOTovisitingDate;
	Map<String, Set<Visitor_passDTO> >mapOfVisitor_passDTOTovisitingTime;
	Map<String, Set<Visitor_passDTO> >mapOfVisitor_passDTOToname;
	Map<String, Set<Visitor_passDTO> >mapOfVisitor_passDTOToaddress;
	Map<String, Set<Visitor_passDTO> >mapOfVisitor_passDTOTomobileNumber;
	Map<String, Set<Visitor_passDTO> >mapOfVisitor_passDTOToemail;
	Map<Integer, Set<Visitor_passDTO> >mapOfVisitor_passDTOToenjoyAssembly;
	Map<String, Set<Visitor_passDTO> >mapOfVisitor_passDTOToapprovalFile;
	Map<Integer, Set<Visitor_passDTO> >mapOfVisitor_passDTOTotypeOfVisitor;
	Map<String, Set<Visitor_passDTO> >mapOfVisitor_passDTOToinstitutionalLetter;
	Map<Integer, Set<Visitor_passDTO> >mapOfVisitor_passDTOTonationalityType;
	Map<Integer, Set<Visitor_passDTO> >mapOfVisitor_passDTOToprofessionType;
	Map<String, Set<Visitor_passDTO> >mapOfVisitor_passDTOTopassportNo;
	Map<Long, Set<Visitor_passDTO> >mapOfVisitor_passDTOTovisaExpiry;
	Map<Integer, Set<Visitor_passDTO> >mapOfVisitor_passDTOTopaymentStatus;
	Map<Integer, Set<Visitor_passDTO> >mapOfVisitor_passDTOTofirstLayerApprovalStatus;
	Map<String, Set<Visitor_passDTO> >mapOfVisitor_passDTOTofirstLayerApprovedBy;
	Map<Long, Set<Visitor_passDTO> >mapOfVisitor_passDTOTofirstLayerApprovalTime;
	Map<Integer, Set<Visitor_passDTO> >mapOfVisitor_passDTOTosecondLayerApprovalStatus;
	Map<String, Set<Visitor_passDTO> >mapOfVisitor_passDTOTosecondLayerApprovedBy;
	Map<Long, Set<Visitor_passDTO> >mapOfVisitor_passDTOTosecondLayerApprovalTime;
	Map<Long, Set<Visitor_passDTO> >mapOfVisitor_passDTOToinsertedByUserId;
	Map<Long, Set<Visitor_passDTO> >mapOfVisitor_passDTOToinsertedByOrganogramId;
	Map<Long, Set<Visitor_passDTO> >mapOfVisitor_passDTOToinsertionDate;
	Map<String, Set<Visitor_passDTO> >mapOfVisitor_passDTOTolastModifierUser;
	Map<Long, Set<Visitor_passDTO> >mapOfVisitor_passDTOTolastModificationTime;


	static Visitor_passRepository instance = null;  
	private Visitor_passRepository(){
		mapOfVisitor_passDTOToiD = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOTovisitingDate = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOTovisitingTime = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOToname = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOToaddress = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOTomobileNumber = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOToemail = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOToenjoyAssembly = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOToapprovalFile = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOTotypeOfVisitor = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOToinstitutionalLetter = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOTonationalityType = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOToprofessionType = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOTopassportNo = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOTovisaExpiry = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOTopaymentStatus = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOTofirstLayerApprovalStatus = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOTofirstLayerApprovedBy = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOTofirstLayerApprovalTime = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOTosecondLayerApprovalStatus = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOTosecondLayerApprovedBy = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOTosecondLayerApprovalTime = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOTolastModifierUser = new ConcurrentHashMap<>();
		mapOfVisitor_passDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Visitor_passRepository getInstance(){
		if (instance == null){
			instance = new Visitor_passRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(visitor_passDAO == null)
		{
			return;
		}
		try {
			List<Visitor_passDTO> visitor_passDTOs = visitor_passDAO.getAllVisitor_pass(reloadAll);
			for(Visitor_passDTO visitor_passDTO : visitor_passDTOs) {
				Visitor_passDTO oldVisitor_passDTO = getVisitor_passDTOByID(visitor_passDTO.iD);
				if( oldVisitor_passDTO != null ) {
					mapOfVisitor_passDTOToiD.remove(oldVisitor_passDTO.iD);
				
					if(mapOfVisitor_passDTOTovisitingDate.containsKey(oldVisitor_passDTO.visitingDate)) {
						mapOfVisitor_passDTOTovisitingDate.get(oldVisitor_passDTO.visitingDate).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOTovisitingDate.get(oldVisitor_passDTO.visitingDate).isEmpty()) {
						mapOfVisitor_passDTOTovisitingDate.remove(oldVisitor_passDTO.visitingDate);
					}
					
					if(mapOfVisitor_passDTOTovisitingTime.containsKey(oldVisitor_passDTO.visitingTime)) {
						mapOfVisitor_passDTOTovisitingTime.get(oldVisitor_passDTO.visitingTime).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOTovisitingTime.get(oldVisitor_passDTO.visitingTime).isEmpty()) {
						mapOfVisitor_passDTOTovisitingTime.remove(oldVisitor_passDTO.visitingTime);
					}
					
					if(mapOfVisitor_passDTOToname.containsKey(oldVisitor_passDTO.name)) {
						mapOfVisitor_passDTOToname.get(oldVisitor_passDTO.name).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOToname.get(oldVisitor_passDTO.name).isEmpty()) {
						mapOfVisitor_passDTOToname.remove(oldVisitor_passDTO.name);
					}
					
					if(mapOfVisitor_passDTOToaddress.containsKey(oldVisitor_passDTO.address)) {
						mapOfVisitor_passDTOToaddress.get(oldVisitor_passDTO.address).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOToaddress.get(oldVisitor_passDTO.address).isEmpty()) {
						mapOfVisitor_passDTOToaddress.remove(oldVisitor_passDTO.address);
					}
					
					if(mapOfVisitor_passDTOTomobileNumber.containsKey(oldVisitor_passDTO.mobileNumber)) {
						mapOfVisitor_passDTOTomobileNumber.get(oldVisitor_passDTO.mobileNumber).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOTomobileNumber.get(oldVisitor_passDTO.mobileNumber).isEmpty()) {
						mapOfVisitor_passDTOTomobileNumber.remove(oldVisitor_passDTO.mobileNumber);
					}
					
					if(mapOfVisitor_passDTOToemail.containsKey(oldVisitor_passDTO.email)) {
						mapOfVisitor_passDTOToemail.get(oldVisitor_passDTO.email).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOToemail.get(oldVisitor_passDTO.email).isEmpty()) {
						mapOfVisitor_passDTOToemail.remove(oldVisitor_passDTO.email);
					}
					
					if(mapOfVisitor_passDTOToenjoyAssembly.containsKey(oldVisitor_passDTO.enjoyAssembly)) {
						mapOfVisitor_passDTOToenjoyAssembly.get(oldVisitor_passDTO.enjoyAssembly).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOToenjoyAssembly.get(oldVisitor_passDTO.enjoyAssembly).isEmpty()) {
						mapOfVisitor_passDTOToenjoyAssembly.remove(oldVisitor_passDTO.enjoyAssembly);
					}
					
					if(mapOfVisitor_passDTOToapprovalFile.containsKey(oldVisitor_passDTO.approvalFile)) {
						mapOfVisitor_passDTOToapprovalFile.get(oldVisitor_passDTO.approvalFile).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOToapprovalFile.get(oldVisitor_passDTO.approvalFile).isEmpty()) {
						mapOfVisitor_passDTOToapprovalFile.remove(oldVisitor_passDTO.approvalFile);
					}
					
					if(mapOfVisitor_passDTOTotypeOfVisitor.containsKey(oldVisitor_passDTO.typeOfVisitor)) {
						mapOfVisitor_passDTOTotypeOfVisitor.get(oldVisitor_passDTO.typeOfVisitor).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOTotypeOfVisitor.get(oldVisitor_passDTO.typeOfVisitor).isEmpty()) {
						mapOfVisitor_passDTOTotypeOfVisitor.remove(oldVisitor_passDTO.typeOfVisitor);
					}
					
					if(mapOfVisitor_passDTOToinstitutionalLetter.containsKey(oldVisitor_passDTO.institutionalLetter)) {
						mapOfVisitor_passDTOToinstitutionalLetter.get(oldVisitor_passDTO.institutionalLetter).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOToinstitutionalLetter.get(oldVisitor_passDTO.institutionalLetter).isEmpty()) {
						mapOfVisitor_passDTOToinstitutionalLetter.remove(oldVisitor_passDTO.institutionalLetter);
					}
					
					if(mapOfVisitor_passDTOTonationalityType.containsKey(oldVisitor_passDTO.nationalityType)) {
						mapOfVisitor_passDTOTonationalityType.get(oldVisitor_passDTO.nationalityType).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOTonationalityType.get(oldVisitor_passDTO.nationalityType).isEmpty()) {
						mapOfVisitor_passDTOTonationalityType.remove(oldVisitor_passDTO.nationalityType);
					}
					
					if(mapOfVisitor_passDTOToprofessionType.containsKey(oldVisitor_passDTO.professionType)) {
						mapOfVisitor_passDTOToprofessionType.get(oldVisitor_passDTO.professionType).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOToprofessionType.get(oldVisitor_passDTO.professionType).isEmpty()) {
						mapOfVisitor_passDTOToprofessionType.remove(oldVisitor_passDTO.professionType);
					}
					
					if(mapOfVisitor_passDTOTopassportNo.containsKey(oldVisitor_passDTO.passportNo)) {
						mapOfVisitor_passDTOTopassportNo.get(oldVisitor_passDTO.passportNo).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOTopassportNo.get(oldVisitor_passDTO.passportNo).isEmpty()) {
						mapOfVisitor_passDTOTopassportNo.remove(oldVisitor_passDTO.passportNo);
					}
					
					if(mapOfVisitor_passDTOTovisaExpiry.containsKey(oldVisitor_passDTO.visaExpiry)) {
						mapOfVisitor_passDTOTovisaExpiry.get(oldVisitor_passDTO.visaExpiry).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOTovisaExpiry.get(oldVisitor_passDTO.visaExpiry).isEmpty()) {
						mapOfVisitor_passDTOTovisaExpiry.remove(oldVisitor_passDTO.visaExpiry);
					}
					
					if(mapOfVisitor_passDTOTopaymentStatus.containsKey(oldVisitor_passDTO.paymentStatus)) {
						mapOfVisitor_passDTOTopaymentStatus.get(oldVisitor_passDTO.paymentStatus).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOTopaymentStatus.get(oldVisitor_passDTO.paymentStatus).isEmpty()) {
						mapOfVisitor_passDTOTopaymentStatus.remove(oldVisitor_passDTO.paymentStatus);
					}
					
					if(mapOfVisitor_passDTOTofirstLayerApprovalStatus.containsKey(oldVisitor_passDTO.firstLayerApprovalStatus)) {
						mapOfVisitor_passDTOTofirstLayerApprovalStatus.get(oldVisitor_passDTO.firstLayerApprovalStatus).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOTofirstLayerApprovalStatus.get(oldVisitor_passDTO.firstLayerApprovalStatus).isEmpty()) {
						mapOfVisitor_passDTOTofirstLayerApprovalStatus.remove(oldVisitor_passDTO.firstLayerApprovalStatus);
					}
					
					if(mapOfVisitor_passDTOTofirstLayerApprovedBy.containsKey(oldVisitor_passDTO.firstLayerApprovedBy)) {
						mapOfVisitor_passDTOTofirstLayerApprovedBy.get(oldVisitor_passDTO.firstLayerApprovedBy).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOTofirstLayerApprovedBy.get(oldVisitor_passDTO.firstLayerApprovedBy).isEmpty()) {
						mapOfVisitor_passDTOTofirstLayerApprovedBy.remove(oldVisitor_passDTO.firstLayerApprovedBy);
					}
					
					if(mapOfVisitor_passDTOTofirstLayerApprovalTime.containsKey(oldVisitor_passDTO.firstLayerApprovalTime)) {
						mapOfVisitor_passDTOTofirstLayerApprovalTime.get(oldVisitor_passDTO.firstLayerApprovalTime).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOTofirstLayerApprovalTime.get(oldVisitor_passDTO.firstLayerApprovalTime).isEmpty()) {
						mapOfVisitor_passDTOTofirstLayerApprovalTime.remove(oldVisitor_passDTO.firstLayerApprovalTime);
					}
					
					if(mapOfVisitor_passDTOTosecondLayerApprovalStatus.containsKey(oldVisitor_passDTO.secondLayerApprovalStatus)) {
						mapOfVisitor_passDTOTosecondLayerApprovalStatus.get(oldVisitor_passDTO.secondLayerApprovalStatus).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOTosecondLayerApprovalStatus.get(oldVisitor_passDTO.secondLayerApprovalStatus).isEmpty()) {
						mapOfVisitor_passDTOTosecondLayerApprovalStatus.remove(oldVisitor_passDTO.secondLayerApprovalStatus);
					}
					
					if(mapOfVisitor_passDTOTosecondLayerApprovedBy.containsKey(oldVisitor_passDTO.secondLayerApprovedBy)) {
						mapOfVisitor_passDTOTosecondLayerApprovedBy.get(oldVisitor_passDTO.secondLayerApprovedBy).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOTosecondLayerApprovedBy.get(oldVisitor_passDTO.secondLayerApprovedBy).isEmpty()) {
						mapOfVisitor_passDTOTosecondLayerApprovedBy.remove(oldVisitor_passDTO.secondLayerApprovedBy);
					}
					
					if(mapOfVisitor_passDTOTosecondLayerApprovalTime.containsKey(oldVisitor_passDTO.secondLayerApprovalTime)) {
						mapOfVisitor_passDTOTosecondLayerApprovalTime.get(oldVisitor_passDTO.secondLayerApprovalTime).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOTosecondLayerApprovalTime.get(oldVisitor_passDTO.secondLayerApprovalTime).isEmpty()) {
						mapOfVisitor_passDTOTosecondLayerApprovalTime.remove(oldVisitor_passDTO.secondLayerApprovalTime);
					}
					
					if(mapOfVisitor_passDTOToinsertedByUserId.containsKey(oldVisitor_passDTO.insertedByUserId)) {
						mapOfVisitor_passDTOToinsertedByUserId.get(oldVisitor_passDTO.insertedByUserId).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOToinsertedByUserId.get(oldVisitor_passDTO.insertedByUserId).isEmpty()) {
						mapOfVisitor_passDTOToinsertedByUserId.remove(oldVisitor_passDTO.insertedByUserId);
					}
					
					if(mapOfVisitor_passDTOToinsertedByOrganogramId.containsKey(oldVisitor_passDTO.insertedByOrganogramId)) {
						mapOfVisitor_passDTOToinsertedByOrganogramId.get(oldVisitor_passDTO.insertedByOrganogramId).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOToinsertedByOrganogramId.get(oldVisitor_passDTO.insertedByOrganogramId).isEmpty()) {
						mapOfVisitor_passDTOToinsertedByOrganogramId.remove(oldVisitor_passDTO.insertedByOrganogramId);
					}
					
					if(mapOfVisitor_passDTOToinsertionDate.containsKey(oldVisitor_passDTO.insertionDate)) {
						mapOfVisitor_passDTOToinsertionDate.get(oldVisitor_passDTO.insertionDate).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOToinsertionDate.get(oldVisitor_passDTO.insertionDate).isEmpty()) {
						mapOfVisitor_passDTOToinsertionDate.remove(oldVisitor_passDTO.insertionDate);
					}
					
					if(mapOfVisitor_passDTOTolastModifierUser.containsKey(oldVisitor_passDTO.lastModifierUser)) {
						mapOfVisitor_passDTOTolastModifierUser.get(oldVisitor_passDTO.lastModifierUser).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOTolastModifierUser.get(oldVisitor_passDTO.lastModifierUser).isEmpty()) {
						mapOfVisitor_passDTOTolastModifierUser.remove(oldVisitor_passDTO.lastModifierUser);
					}
					
					if(mapOfVisitor_passDTOTolastModificationTime.containsKey(oldVisitor_passDTO.lastModificationTime)) {
						mapOfVisitor_passDTOTolastModificationTime.get(oldVisitor_passDTO.lastModificationTime).remove(oldVisitor_passDTO);
					}
					if(mapOfVisitor_passDTOTolastModificationTime.get(oldVisitor_passDTO.lastModificationTime).isEmpty()) {
						mapOfVisitor_passDTOTolastModificationTime.remove(oldVisitor_passDTO.lastModificationTime);
					}
					
					
				}
				if(visitor_passDTO.isDeleted == 0) 
				{
					
					mapOfVisitor_passDTOToiD.put(visitor_passDTO.iD, visitor_passDTO);
				
					if( ! mapOfVisitor_passDTOTovisitingDate.containsKey(visitor_passDTO.visitingDate)) {
						mapOfVisitor_passDTOTovisitingDate.put(visitor_passDTO.visitingDate, new HashSet<>());
					}
					mapOfVisitor_passDTOTovisitingDate.get(visitor_passDTO.visitingDate).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOTovisitingTime.containsKey(visitor_passDTO.visitingTime)) {
						mapOfVisitor_passDTOTovisitingTime.put(visitor_passDTO.visitingTime, new HashSet<>());
					}
					mapOfVisitor_passDTOTovisitingTime.get(visitor_passDTO.visitingTime).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOToname.containsKey(visitor_passDTO.name)) {
						mapOfVisitor_passDTOToname.put(visitor_passDTO.name, new HashSet<>());
					}
					mapOfVisitor_passDTOToname.get(visitor_passDTO.name).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOToaddress.containsKey(visitor_passDTO.address)) {
						mapOfVisitor_passDTOToaddress.put(visitor_passDTO.address, new HashSet<>());
					}
					mapOfVisitor_passDTOToaddress.get(visitor_passDTO.address).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOTomobileNumber.containsKey(visitor_passDTO.mobileNumber)) {
						mapOfVisitor_passDTOTomobileNumber.put(visitor_passDTO.mobileNumber, new HashSet<>());
					}
					mapOfVisitor_passDTOTomobileNumber.get(visitor_passDTO.mobileNumber).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOToemail.containsKey(visitor_passDTO.email)) {
						mapOfVisitor_passDTOToemail.put(visitor_passDTO.email, new HashSet<>());
					}
					mapOfVisitor_passDTOToemail.get(visitor_passDTO.email).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOToenjoyAssembly.containsKey(visitor_passDTO.enjoyAssembly)) {
						mapOfVisitor_passDTOToenjoyAssembly.put(visitor_passDTO.enjoyAssembly, new HashSet<>());
					}
					mapOfVisitor_passDTOToenjoyAssembly.get(visitor_passDTO.enjoyAssembly).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOToapprovalFile.containsKey(visitor_passDTO.approvalFile)) {
						mapOfVisitor_passDTOToapprovalFile.put(String.valueOf(visitor_passDTO.approvalFile), new HashSet<>());
					}
					mapOfVisitor_passDTOToapprovalFile.get(visitor_passDTO.approvalFile).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOTotypeOfVisitor.containsKey(visitor_passDTO.typeOfVisitor)) {
						mapOfVisitor_passDTOTotypeOfVisitor.put(visitor_passDTO.typeOfVisitor, new HashSet<>());
					}
					mapOfVisitor_passDTOTotypeOfVisitor.get(visitor_passDTO.typeOfVisitor).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOToinstitutionalLetter.containsKey(visitor_passDTO.institutionalLetter)) {
						mapOfVisitor_passDTOToinstitutionalLetter.put(String.valueOf(visitor_passDTO.institutionalLetter), new HashSet<>());
					}
					mapOfVisitor_passDTOToinstitutionalLetter.get(visitor_passDTO.institutionalLetter).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOTonationalityType.containsKey(visitor_passDTO.nationalityType)) {
						mapOfVisitor_passDTOTonationalityType.put(visitor_passDTO.nationalityType, new HashSet<>());
					}
					mapOfVisitor_passDTOTonationalityType.get(visitor_passDTO.nationalityType).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOToprofessionType.containsKey(visitor_passDTO.professionType)) {
						mapOfVisitor_passDTOToprofessionType.put(visitor_passDTO.professionType, new HashSet<>());
					}
					mapOfVisitor_passDTOToprofessionType.get(visitor_passDTO.professionType).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOTopassportNo.containsKey(visitor_passDTO.passportNo)) {
						mapOfVisitor_passDTOTopassportNo.put(visitor_passDTO.passportNo, new HashSet<>());
					}
					mapOfVisitor_passDTOTopassportNo.get(visitor_passDTO.passportNo).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOTovisaExpiry.containsKey(visitor_passDTO.visaExpiry)) {
						mapOfVisitor_passDTOTovisaExpiry.put(visitor_passDTO.visaExpiry, new HashSet<>());
					}
					mapOfVisitor_passDTOTovisaExpiry.get(visitor_passDTO.visaExpiry).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOTopaymentStatus.containsKey(visitor_passDTO.paymentStatus)) {
						mapOfVisitor_passDTOTopaymentStatus.put(visitor_passDTO.paymentStatus, new HashSet<>());
					}
					mapOfVisitor_passDTOTopaymentStatus.get(visitor_passDTO.paymentStatus).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOTofirstLayerApprovalStatus.containsKey(visitor_passDTO.firstLayerApprovalStatus)) {
						mapOfVisitor_passDTOTofirstLayerApprovalStatus.put(visitor_passDTO.firstLayerApprovalStatus, new HashSet<>());
					}
					mapOfVisitor_passDTOTofirstLayerApprovalStatus.get(visitor_passDTO.firstLayerApprovalStatus).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOTofirstLayerApprovedBy.containsKey(visitor_passDTO.firstLayerApprovedBy)) {
						mapOfVisitor_passDTOTofirstLayerApprovedBy.put(visitor_passDTO.firstLayerApprovedBy, new HashSet<>());
					}
					mapOfVisitor_passDTOTofirstLayerApprovedBy.get(visitor_passDTO.firstLayerApprovedBy).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOTofirstLayerApprovalTime.containsKey(visitor_passDTO.firstLayerApprovalTime)) {
						mapOfVisitor_passDTOTofirstLayerApprovalTime.put(visitor_passDTO.firstLayerApprovalTime, new HashSet<>());
					}
					mapOfVisitor_passDTOTofirstLayerApprovalTime.get(visitor_passDTO.firstLayerApprovalTime).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOTosecondLayerApprovalStatus.containsKey(visitor_passDTO.secondLayerApprovalStatus)) {
						mapOfVisitor_passDTOTosecondLayerApprovalStatus.put(visitor_passDTO.secondLayerApprovalStatus, new HashSet<>());
					}
					mapOfVisitor_passDTOTosecondLayerApprovalStatus.get(visitor_passDTO.secondLayerApprovalStatus).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOTosecondLayerApprovedBy.containsKey(visitor_passDTO.secondLayerApprovedBy)) {
						mapOfVisitor_passDTOTosecondLayerApprovedBy.put(visitor_passDTO.secondLayerApprovedBy, new HashSet<>());
					}
					mapOfVisitor_passDTOTosecondLayerApprovedBy.get(visitor_passDTO.secondLayerApprovedBy).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOTosecondLayerApprovalTime.containsKey(visitor_passDTO.secondLayerApprovalTime)) {
						mapOfVisitor_passDTOTosecondLayerApprovalTime.put(visitor_passDTO.secondLayerApprovalTime, new HashSet<>());
					}
					mapOfVisitor_passDTOTosecondLayerApprovalTime.get(visitor_passDTO.secondLayerApprovalTime).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOToinsertedByUserId.containsKey(visitor_passDTO.insertedByUserId)) {
						mapOfVisitor_passDTOToinsertedByUserId.put(visitor_passDTO.insertedByUserId, new HashSet<>());
					}
					mapOfVisitor_passDTOToinsertedByUserId.get(visitor_passDTO.insertedByUserId).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOToinsertedByOrganogramId.containsKey(visitor_passDTO.insertedByOrganogramId)) {
						mapOfVisitor_passDTOToinsertedByOrganogramId.put(visitor_passDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfVisitor_passDTOToinsertedByOrganogramId.get(visitor_passDTO.insertedByOrganogramId).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOToinsertionDate.containsKey(visitor_passDTO.insertionDate)) {
						mapOfVisitor_passDTOToinsertionDate.put(visitor_passDTO.insertionDate, new HashSet<>());
					}
					mapOfVisitor_passDTOToinsertionDate.get(visitor_passDTO.insertionDate).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOTolastModifierUser.containsKey(visitor_passDTO.lastModifierUser)) {
						mapOfVisitor_passDTOTolastModifierUser.put(visitor_passDTO.lastModifierUser, new HashSet<>());
					}
					mapOfVisitor_passDTOTolastModifierUser.get(visitor_passDTO.lastModifierUser).add(visitor_passDTO);
					
					if( ! mapOfVisitor_passDTOTolastModificationTime.containsKey(visitor_passDTO.lastModificationTime)) {
						mapOfVisitor_passDTOTolastModificationTime.put(visitor_passDTO.lastModificationTime, new HashSet<>());
					}
					mapOfVisitor_passDTOTolastModificationTime.get(visitor_passDTO.lastModificationTime).add(visitor_passDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Visitor_passDTO> getVisitor_passList() {
		List <Visitor_passDTO> visitor_passs = new ArrayList<Visitor_passDTO>(this.mapOfVisitor_passDTOToiD.values());
		return visitor_passs;
	}
	
	
	public Visitor_passDTO getVisitor_passDTOByID( long ID){
		return mapOfVisitor_passDTOToiD.get(ID);
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOByvisiting_date(long visiting_date) {
		return new ArrayList<>( mapOfVisitor_passDTOTovisitingDate.getOrDefault(visiting_date,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOByvisiting_time(String visiting_time) {
		return new ArrayList<>( mapOfVisitor_passDTOTovisitingTime.getOrDefault(visiting_time,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOByname(String name) {
		return new ArrayList<>( mapOfVisitor_passDTOToname.getOrDefault(name,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOByaddress(String address) {
		return new ArrayList<>( mapOfVisitor_passDTOToaddress.getOrDefault(address,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOBymobile_number(String mobile_number) {
		return new ArrayList<>( mapOfVisitor_passDTOTomobileNumber.getOrDefault(mobile_number,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOByemail(String email) {
		return new ArrayList<>( mapOfVisitor_passDTOToemail.getOrDefault(email,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOByenjoy_assembly(int enjoy_assembly) {
		return new ArrayList<>( mapOfVisitor_passDTOToenjoyAssembly.getOrDefault(enjoy_assembly,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOByapproval_file(byte[] approval_file) {
		return new ArrayList<>( mapOfVisitor_passDTOToapprovalFile.getOrDefault(approval_file,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOBytype_of_visitor(int type_of_visitor) {
		return new ArrayList<>( mapOfVisitor_passDTOTotypeOfVisitor.getOrDefault(type_of_visitor,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOByinstitutional_letter(byte[] institutional_letter) {
		return new ArrayList<>( mapOfVisitor_passDTOToinstitutionalLetter.getOrDefault(institutional_letter,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOBynationality_type(int nationality_type) {
		return new ArrayList<>( mapOfVisitor_passDTOTonationalityType.getOrDefault(nationality_type,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOByprofession_type(int profession_type) {
		return new ArrayList<>( mapOfVisitor_passDTOToprofessionType.getOrDefault(profession_type,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOBypassport_no(String passport_no) {
		return new ArrayList<>( mapOfVisitor_passDTOTopassportNo.getOrDefault(passport_no,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOByvisa_expiry(long visa_expiry) {
		return new ArrayList<>( mapOfVisitor_passDTOTovisaExpiry.getOrDefault(visa_expiry,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOBypayment_status(int payment_status) {
		return new ArrayList<>( mapOfVisitor_passDTOTopaymentStatus.getOrDefault(payment_status,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOByfirst_layer_approval_status(int first_layer_approval_status) {
		return new ArrayList<>( mapOfVisitor_passDTOTofirstLayerApprovalStatus.getOrDefault(first_layer_approval_status,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOByfirst_layer_approved_by(String first_layer_approved_by) {
		return new ArrayList<>( mapOfVisitor_passDTOTofirstLayerApprovedBy.getOrDefault(first_layer_approved_by,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOByfirst_layer_approval_time(long first_layer_approval_time) {
		return new ArrayList<>( mapOfVisitor_passDTOTofirstLayerApprovalTime.getOrDefault(first_layer_approval_time,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOBysecond_layer_approval_status(int second_layer_approval_status) {
		return new ArrayList<>( mapOfVisitor_passDTOTosecondLayerApprovalStatus.getOrDefault(second_layer_approval_status,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOBysecond_layer_approved_by(String second_layer_approved_by) {
		return new ArrayList<>( mapOfVisitor_passDTOTosecondLayerApprovedBy.getOrDefault(second_layer_approved_by,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOBysecond_layer_approval_time(long second_layer_approval_time) {
		return new ArrayList<>( mapOfVisitor_passDTOTosecondLayerApprovalTime.getOrDefault(second_layer_approval_time,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfVisitor_passDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfVisitor_passDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfVisitor_passDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOBylast_modifier_user(String last_modifier_user) {
		return new ArrayList<>( mapOfVisitor_passDTOTolastModifierUser.getOrDefault(last_modifier_user,new HashSet<>()));
	}
	
	
	public List<Visitor_passDTO> getVisitor_passDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfVisitor_passDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "visitor_pass";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


