package shift_slot;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Shift_slotRepository implements Repository {
	Shift_slotDAO shift_slotDAO = null;
	
	static Logger logger = Logger.getLogger(Shift_slotRepository.class);
	Map<Long, Shift_slotDTO>mapOfShift_slotDTOToiD;
	Map<Integer, Shift_slotDTO>mapOfShift_slotDTOToShiftCat;
	Gson gson;

  
	private Shift_slotRepository(){
		shift_slotDAO = Shift_slotDAO.getInstance();
		mapOfShift_slotDTOToiD = new ConcurrentHashMap<>();
		mapOfShift_slotDTOToShiftCat =  new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Shift_slotRepository INSTANCE = new Shift_slotRepository();
    }

    public static Shift_slotRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Shift_slotDTO> shift_slotDTOs = shift_slotDAO.getAllDTOs(reloadAll);
			for(Shift_slotDTO shift_slotDTO : shift_slotDTOs) {
				Shift_slotDTO oldShift_slotDTO = getShift_slotDTOByiD(shift_slotDTO.iD);
				if( oldShift_slotDTO != null ) {
					mapOfShift_slotDTOToiD.remove(oldShift_slotDTO.iD);
					mapOfShift_slotDTOToShiftCat.remove(oldShift_slotDTO.shiftCat);
				
				}
				if(shift_slotDTO.isDeleted == 0) 
				{
					
					mapOfShift_slotDTOToiD.put(shift_slotDTO.iD, shift_slotDTO);
					mapOfShift_slotDTOToShiftCat.put(shift_slotDTO.shiftCat, shift_slotDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Shift_slotDTO clone(Shift_slotDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Shift_slotDTO.class);
	}
	
	
	public List<Shift_slotDTO> getShift_slotList() {
		List <Shift_slotDTO> shift_slots = new ArrayList<Shift_slotDTO>(this.mapOfShift_slotDTOToiD.values());
		return shift_slots;
	}
	
	public List<Shift_slotDTO> copyShift_slotList() {
		List <Shift_slotDTO> shift_slots = getShift_slotList();
		return shift_slots
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Shift_slotDTO getShift_slotDTOByiD( long iD){
		return mapOfShift_slotDTOToiD.get(iD);
	}
	
	public Shift_slotDTO getShift_slotDTOByShiftCat( int shiftCat){
		return mapOfShift_slotDTOToShiftCat.get(shiftCat);
	}
	
	public Shift_slotDTO copyShift_slotDTOByiD( long iD){
		return clone(mapOfShift_slotDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return shift_slotDAO.getTableName();
	}
}


