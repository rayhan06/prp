package shift_slot;


import java.text.SimpleDateFormat;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import org.apache.log4j.Logger;
import permission.MenuConstants;
import user.UserDTO;
import util.*;
import javax.servlet.http.*;
import common.BaseServlet;
import doctor_time_slot.Doctor_time_slotDAO;
import doctor_time_slot.Doctor_time_slotRepository;

import com.google.gson.Gson;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Shift_slotServlet
 */
@WebServlet("/Shift_slotServlet")
@MultipartConfig
public class Shift_slotServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Shift_slotServlet.class);
    Doctor_time_slotDAO doctor_time_slotDAO = new Doctor_time_slotDAO();

    @Override
    public String getTableName() {
        return Shift_slotDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Shift_slotServlet";
    }

    @Override
    public Shift_slotDAO getCommonDAOService() {
        return Shift_slotDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.SHIFT_SLOT_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.SHIFT_SLOT_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.SHIFT_SLOT_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Shift_slotServlet.class;
    }
    private final Gson gson = new Gson();
 	

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addShift_slot");
		String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		Shift_slotDTO shift_slotDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
		if(addFlag)
		{
			shift_slotDTO = new Shift_slotDTO();
		}
		else
		{
			shift_slotDTO = Shift_slotDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		Value = request.getParameter("shiftCat");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("shiftCat = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			shift_slotDTO.shiftCat = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("startTime");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("startTime = " + Value);
		if(Value != null)
		{
			shift_slotDTO.startTime = TimeFormat.getIn24HourFormat(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("endTime");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("endTime = " + Value);
		if(Value != null)
		{
			shift_slotDTO.endTime = TimeFormat.getIn24HourFormat(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		System.out.println("Done adding  addShift_slot dto = " + shift_slotDTO);

		if(addFlag == true)
		{
			Shift_slotDAO.getInstance().add(shift_slotDTO);
		}
		else
		{				
			Shift_slotDAO.getInstance().update(shift_slotDTO);
			doctor_time_slotDAO.updateShift(shift_slotDTO);
			Doctor_time_slotRepository.getInstance().reload(false);
		}
		
		Shift_slotRepository.getInstance().reload(false);
		
		

		return shift_slotDTO;

	}
}

