package shift_slot;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Shift_slotDAO  implements CommonDAOService<Shift_slotDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Shift_slotDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"shift_cat",
			"start_time",
			"end_time",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("shift_cat"," and (shift_cat = ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Shift_slotDAO INSTANCE = new Shift_slotDAO();
	}

	public static Shift_slotDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Shift_slotDTO shift_slotDTO)
	{
		shift_slotDTO.searchColumn = "";
		shift_slotDTO.searchColumn += CatDAO.getName("English", "shift", shift_slotDTO.shiftCat) + " " + CatDAO.getName("Bangla", "shift", shift_slotDTO.shiftCat) + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Shift_slotDTO shift_slotDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(shift_slotDTO);
		if(isInsert)
		{
			ps.setObject(++index,shift_slotDTO.iD);
		}
		ps.setObject(++index,shift_slotDTO.shiftCat);
		ps.setObject(++index,shift_slotDTO.startTime);
		ps.setObject(++index,shift_slotDTO.endTime);
		ps.setObject(++index,shift_slotDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(++index,shift_slotDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,shift_slotDTO.iD);
		}
	}
	
	@Override
	public Shift_slotDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Shift_slotDTO shift_slotDTO = new Shift_slotDTO();
			int i = 0;
			shift_slotDTO.iD = rs.getLong(columnNames[i++]);
			shift_slotDTO.shiftCat = rs.getInt(columnNames[i++]);
			shift_slotDTO.startTime = rs.getString(columnNames[i++]);
			shift_slotDTO.endTime = rs.getString(columnNames[i++]);
			shift_slotDTO.searchColumn = rs.getString(columnNames[i++]);
			shift_slotDTO.isDeleted = rs.getInt(columnNames[i++]);
			shift_slotDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return shift_slotDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Shift_slotDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}
	
	public Shift_slotDTO getDTOByShiftCat (int shiftCat)
	{
		String sql = "select * from shift_slot where isDeleted = 0 and shift_cat = " + shiftCat;
		return ConnectionAndStatementUtil.getT(sql,this::buildObjectFromResultSet);	
	}

	@Override
	public String getTableName() {
		return "shift_slot";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Shift_slotDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Shift_slotDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	