package shift_slot;
import java.util.*; 
import util.*; 


public class Shift_slotDTO extends CommonDTO
{

	public int shiftCat = -1;
    public String startTime = "";
    public String endTime = "";
	
	
    @Override
	public String toString() {
            return "$Shift_slotDTO[" +
            " iD = " + iD +
            " shiftCat = " + shiftCat +
            " startTime = " + startTime +
            " endTime = " + endTime +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}