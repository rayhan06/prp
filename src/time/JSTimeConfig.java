package time;

public enum JSTimeConfig {
    IS_AMPM,
    LANGUAGE,
    TIME_ID,
    MAX_TIME,
    MIN_TIME
}
