package time;

import org.apache.log4j.Logger;
import util.CommonRequestHandler;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/TimeServlet")
@MultipartConfig
public class TimeServlet extends HttpServlet
{
    private static final Logger logger = Logger.getLogger(CommonRequestHandler.class);
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        try{
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("time/time.jsp");
            requestDispatcher.forward(request, response);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }
}
