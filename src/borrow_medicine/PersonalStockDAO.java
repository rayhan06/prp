package borrow_medicine;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import appointment.KeyCountDTO;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import drug_information.Drug_informationDTO;
import medical_inventory_lot.MedicalInventoryInDTO;
import medical_inventory_out.Medical_inventory_outDTO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;
import workflow.WorkflowController;

public class PersonalStockDAO implements CommonDAOService<PersonalStockDTO>{
	
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private PersonalStockDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"doctor_id",
			"doctor_user_name",
			"drug_information_type",
			"medicine_generic_name_id",
			"quantity",
			"medical_item_type",
			"medical_dept_cat",
			"detailed_count",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

	}
	
	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final PersonalStockDAO INSTANCE = new PersonalStockDAO();
	}

	public static PersonalStockDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(PersonalStockDTO personalStockDTO)
	{
	}
	
	public void addOrUpdate()
	{
		
	}
	
	
	@Override
	public void set(PreparedStatement ps, PersonalStockDTO personalStockDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(personalStockDTO);
		if(isInsert)
		{
			ps.setObject(++index,personalStockDTO.iD);
		}
		ps.setObject(++index,personalStockDTO.organogramId);
		ps.setObject(++index,personalStockDTO.userName);
		ps.setObject(++index,personalStockDTO.drugInformationType);
		ps.setObject(++index,personalStockDTO.medicineGenericNameId);
		ps.setObject(++index,personalStockDTO.quantity);
		ps.setObject(++index,personalStockDTO.medicalItemType);
		ps.setObject(++index,personalStockDTO.medicalDeptCat);
		ps.setObject(++index,personalStockDTO.detailedCount);
		ps.setObject(++index,personalStockDTO.searchColumn);	
		
		if(isInsert)
		{
			ps.setObject(++index,personalStockDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,personalStockDTO.iD);
		}
	}
	
	@Override
	public PersonalStockDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			PersonalStockDTO personalStockDTO = new PersonalStockDTO();
			int i = 0;
			personalStockDTO.iD = rs.getLong(columnNames[i++]);
			personalStockDTO.organogramId = rs.getLong(columnNames[i++]);
			personalStockDTO.userName = rs.getString(columnNames[i++]);
			personalStockDTO.drugInformationType = rs.getLong(columnNames[i++]);
			personalStockDTO.medicineGenericNameId = rs.getLong(columnNames[i++]);
			personalStockDTO.quantity = rs.getInt(columnNames[i++]);
			personalStockDTO.medicalItemType = rs.getInt(columnNames[i++]);
			personalStockDTO.medicalDeptCat = rs.getInt(columnNames[i++]);
			personalStockDTO.detailedCount = rs.getInt(columnNames[i++]);
			personalStockDTO.searchColumn = rs.getString(columnNames[i++]);
			personalStockDTO.isDeleted = rs.getInt(columnNames[i++]);
			personalStockDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return personalStockDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	@Override
	public String getTableName() {
		return "personal_stock";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((PersonalStockDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((PersonalStockDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
	
	public Collection getDTOsByDrId(String userName, int medicalItemType) {
        String sql = "SELECT * FROM " + getTableName() + " where isDeleted=0 and doctor_user_name = '" + userName + "' and quantity > 0 and medical_item_type = " + medicalItemType;
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }
	
	public PersonalStockDTO getDTOByUserNameAndDrugId(String userName, long drugId, int medicalItemType) {
        String sql = "SELECT * FROM " + getTableName() + " where isDeleted=0 and doctor_user_name = '" + userName 
        		+ "' and drug_information_type = " + drugId
        		+ " and medical_item_type = " + medicalItemType;
        return ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
    }
	
	public PersonalStockDTO getDTOByDeptAndDrugId(int medicalDeptCat, long drugId, int medicalItemType) {
        String sql = "SELECT * FROM " + getTableName() + " where isDeleted=0 and medical_dept_cat = " + medicalDeptCat 
        		+ " and drug_information_type = " + drugId
        		+ " and medical_item_type = " + medicalItemType;
        return ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
    }
	
	public List<PersonalStockDTO> getUserwiseSum() {
        String sql = "SELECT doctor_id, doctor_user_name, sum(quantity), sum(quantity * unit_price) FROM " + getTableName() + " where isDeleted=0 "
        		+ " and medical_item_type = " + SessionConstants.MEDICAL_ITEM_DRUG
        		+ " and doctor_user_name != '' group by doctor_user_name order by sum(quantity * unit_price) desc";
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildSum);
    }
	

	public PersonalStockDTO buildSum(ResultSet rs)
	{
		try
		{
			PersonalStockDTO personalStockDTO = new PersonalStockDTO();


			personalStockDTO.organogramId = rs.getLong("doctor_id");
			personalStockDTO.userName = rs.getString("doctor_user_name");
			personalStockDTO.organizationId = WorkflowController.getOrganizationIdFromOrganogramId(personalStockDTO.organogramId);
			personalStockDTO.quantity = rs.getInt("sum(quantity)");
			personalStockDTO.cost = rs.getDouble("sum(quantity * unit_price)");

			return personalStockDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	
	
	

}
