package borrow_medicine;

import java.io.IOException;

import java.text.SimpleDateFormat;



import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;


import medical_equipment_name.Medical_equipment_nameDAO;
import medical_equipment_name.Medical_equipment_nameDTO;
import medical_inventory_lot.MedicalInventoryInDAO;
import medical_inventory_lot.MedicalInventoryInDTO;
import medical_inventory_out.Medical_inventory_outDAO;
import medical_inventory_out.Medical_inventory_outDTO;
import permission.MenuConstants;


import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;
import workflow.WorkflowController;

import javax.servlet.http.*;
import java.util.*;


import language.LC;
import language.LM;
import common.ApiResponse;
import common.BaseServlet;


import drug_information.Drug_informationDAO;
import drug_information.Drug_informationDTO;

import pb.*;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Borrow_medicineServlet
 */
@WebServlet("/Borrow_medicineServlet")
@MultipartConfig
public class Borrow_medicineServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Borrow_medicineServlet.class);
    Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
    Medical_equipment_nameDAO medical_equipment_nameDAO = new Medical_equipment_nameDAO();
    MedicalInventoryInDAO medicalInventoryInDAO = new MedicalInventoryInDAO();
    Medical_inventory_outDAO medical_inventory_outDAO = new Medical_inventory_outDAO();

    @Override
    public String getTableName() {
        return Borrow_medicineDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Borrow_medicineServlet";
    }

    @Override
    public Borrow_medicineDAO getCommonDAOService() {
        return Borrow_medicineDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.BORROW_MEDICINE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.BORROW_MEDICINE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.BORROW_MEDICINE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Borrow_medicineServlet.class;
    }
	BorrowMedicineDetailsDAO borrowMedicineDetailsDAO = BorrowMedicineDetailsDAO.getInstance();

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        init(request);
        String actionType = request.getParameter("actionType");
		if ("viewBorrowed".equals(actionType)) {
			try {

				String borrrowerUserName = request.getParameter("userName");
				request.setAttribute("userName", borrrowerUserName);
				request.getRequestDispatcher(commonPartOfDispatchURL()+"ViewBorrowed.jsp").forward(request,response);
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else if ("getApprovalPage".equals(actionType) && 
				(commonLoginData.userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE 
				|| commonLoginData.userDTO.roleID == SessionConstants.ADMIN_ROLE )) {
			try {
				request.getRequestDispatcher(commonPartOfDispatchURL()+"Approve.jsp").forward(request,response);
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else if ("deliverFromShop".equals(actionType) && 
				(commonLoginData.userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE 
				|| commonLoginData.userDTO.roleID == SessionConstants.ADMIN_ROLE
				|| commonLoginData.userDTO.roleID == SessionConstants.NURSE_ROLE
				/*|| commonLoginData.userDTO.roleID == SessionConstants.PHARMACY_PERSON*/)) {
			try {
				setDTOForJsp(request);
                finalize(request);
				request.setAttribute("isDeliveryFromShop", true);
				request.getRequestDispatcher(commonPartOfDispatchURL() + "View.jsp?ID=" + getId(request)).forward(request, response);
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
	
		else
		{
			super.doGet(request,response);
		}
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        init(request);
        String actionType = request.getParameter("actionType");
        logger.debug("medicineServletServlet " + actionType);
		if ("approve".equals(actionType) && 
				(commonLoginData.userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE 
				|| commonLoginData.userDTO.roleID == SessionConstants.ADMIN_ROLE )) {
			try {
				approveOrReject(request, response, commonLoginData.userDTO);
			} catch (Exception ex) {
				ex.printStackTrace();
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else if ("confirmDelivery".equals(actionType) && 
				(/*commonLoginData.userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE 
				|| */commonLoginData.userDTO.roleID == SessionConstants.ADMIN_ROLE 
				|| commonLoginData.userDTO.roleID == SessionConstants.NURSE_ROLE )) {
			try {
				deliverFromShop(request, response, commonLoginData.userDTO);
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else
		{
			super.doPost(request,response);
		}
    }
    
	private void deliverFromShop(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception
	{
		long id = Long.parseLong(request.getParameter("id"));
		
		Borrow_medicineDTO borrow_medicineDTO = Borrow_medicineDAO.getInstance().getDTOFromID(id);
		if(borrow_medicineDTO.approvalStatus == 1 && !borrow_medicineDTO.isDelivered)
		{
			int i = 0;
			List<BorrowMedicineDetailsDTO> borrowMedicineDetailsDTOs = (List<BorrowMedicineDetailsDTO>)borrowMedicineDetailsDAO.getDTOsByParent("borrow_medicine_id", borrow_medicineDTO.iD);

			for(BorrowMedicineDetailsDTO dri: borrowMedicineDetailsDTOs)
			{
				
				Medical_equipment_nameDTO medical_equipment_nameDTO = medical_equipment_nameDAO.getDTOByID(dri.drugInformationType);
				System.out.println("quantity = " + dri.quantity);
				if(dri.quantity > medical_equipment_nameDTO.currentStock)
				{
					dri.quantity = medical_equipment_nameDTO.currentStock;
					try {
						borrowMedicineDetailsDAO.update(dri);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if(dri.quantity > 0)
				{
					UserDTO toUserDTO = UserRepository.getUserDTOByUserName(borrow_medicineDTO.borrowedForUserName);
					Medical_inventory_outDTO medical_inventory_outDTO = new Medical_inventory_outDTO(medical_equipment_nameDTO.iD, dri.quantity,
							toUserDTO, -1, MedicalInventoryInDTO.TR_BORROW_EQUIPMENT, userDTO);				
					try {
						medical_inventory_outDAO.add(medical_inventory_outDTO);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					PersonalStockDTO personalStockDTO = PersonalStockDAO.getInstance().getDTOByUserNameAndDrugId(borrow_medicineDTO.borrowedForUserName,
							dri.drugInformationType, dri.medicalItemType);
					if(personalStockDTO == null)
					{
						personalStockDTO = new PersonalStockDTO(dri, medical_equipment_nameDTO, borrow_medicineDTO.borrowedForUserName, borrow_medicineDTO.organogramId);
						PersonalStockDAO.getInstance().add(personalStockDTO);
					}
					else
					{
						personalStockDTO.quantity += dri.quantity;
						PersonalStockDAO.getInstance().update(personalStockDTO);
					}

				}
				
				i ++;
			}
			
			borrow_medicineDTO.sellerUserName = userDTO.userName;
			borrow_medicineDTO.sellerEmployeeId = userDTO.employee_record_id;
			borrow_medicineDTO.isDelivered = true;
			borrow_medicineDTO.sellingDate = System.currentTimeMillis();
			
			
			String Value = request.getParameter("name");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("name = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				borrow_medicineDTO.borrowerName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("phone");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("phone = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				borrow_medicineDTO.borrowerPhone = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("borrowedByUserName");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("borrowedByUserName = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				borrow_medicineDTO.borrowedByUserName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			UserDTO borrowedByDTO = UserRepository.getUserDTOByUserName(borrow_medicineDTO.borrowedByUserName);
			if(borrowedByDTO != null)
			{
				borrow_medicineDTO.borrowedByEmployeeId = borrowedByDTO.employee_record_id;
			}
			
			try {
				Borrow_medicineDAO.getInstance().update(borrow_medicineDTO);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			response.sendRedirect(getAddRedirectURL(request, borrow_medicineDTO));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
	private void approveOrReject(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
		long id = Long.parseLong(request.getParameter("id"));
		int  approvalAction = Integer.parseInt(request.getParameter("approvalAction"));
		if(approvalAction != 1 && approvalAction != 2)
		{
			approvalAction = 2;
		}
		Borrow_medicineDTO borrow_medicineDTO = Borrow_medicineDAO.getInstance().getDTOFromID(id);
		if(borrow_medicineDTO != null)
		{
			borrow_medicineDTO.approvalStatus = approvalAction;
		}
		borrow_medicineDTO.approvalMessage = request.getParameter("approvalMessage");
		borrow_medicineDTO.approvedByUserName = userDTO.userName;
		borrow_medicineDTO.approvedByEmployeeId = userDTO.employeeID;
		borrow_medicineDTO.approvalDate = System.currentTimeMillis();
		if(borrow_medicineDTO.approvalStatus == 1)
		{
			int i = 0;
			List<BorrowMedicineDetailsDTO> borrowMedicineDetailsDTOs = (List<BorrowMedicineDetailsDTO>)borrowMedicineDetailsDAO.getDTOsByParent("borrow_medicine_id", borrow_medicineDTO.iD);

			for(BorrowMedicineDetailsDTO dri: borrowMedicineDetailsDTOs)
			{
				int quantity = Integer.parseInt(request.getParameterValues("borrowMedicineDetails.quantity")[i]);
				Medical_equipment_nameDTO medical_equipment_nameDTO = medical_equipment_nameDAO.getDTOByID(dri.drugInformationType);
				System.out.println("quantity = " + quantity);
				if(quantity > 0 && quantity <= medical_equipment_nameDTO.currentStock)
				{
					dri.quantity = quantity;
					try {
						borrowMedicineDetailsDAO.update(dri);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				i ++;
			}
		}

		
		
		try {
			Borrow_medicineDAO.getInstance().update(borrow_medicineDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		try {
			ApiResponse.sendSuccessResponse(response, getAjaxAddRedirectURL(request, borrow_medicineDTO));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
 	

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		// TODO Auto-generated method stub
	
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addBorrow_medicine");
		String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
		Borrow_medicineDTO borrow_medicineDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
		if(addFlag == true)
		{
			borrow_medicineDTO = new Borrow_medicineDTO();
		}
		else
		{
			borrow_medicineDTO = Borrow_medicineDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

	
		
		
		Value = request.getParameter("borrowedForUserName");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("borrowedForUserName = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			borrow_medicineDTO.borrowedForUserName = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		UserDTO borrowedForDTO = UserRepository.getUserDTOByUserName(borrow_medicineDTO.borrowedForUserName);
		if(borrowedForDTO == null)
		{
			throw new Exception(ErrorMessage.getGenericInvalidMessage(language));
		}
		borrow_medicineDTO.borrowedForEmployeeId = borrowedForDTO.employee_record_id;
		borrow_medicineDTO.doctorUserName = borrow_medicineDTO.borrowedForUserName ;
		borrow_medicineDTO.organogramId = WorkflowController.getOrganogramIDFromUserName(borrow_medicineDTO.borrowedForUserName);


		Value = request.getParameter("name");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("name = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			borrow_medicineDTO.borrowedForName = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		Value = request.getParameter("phone");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("phone = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			borrow_medicineDTO.borrowedForPhone = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("remarks");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("remarks = " + Value);
		if(Value != null)
		{
			borrow_medicineDTO.remarks = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		Value = request.getParameter("medicalItemType");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("medicalItemType = " + Value);
		if(Value != null)
		{
			borrow_medicineDTO.medicalItemType = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		if(addFlag)
		{
			borrow_medicineDTO.insertedByUserId = userDTO.ID;
		}


		if(addFlag)
		{
			borrow_medicineDTO.insertedByOrganogramId = userDTO.organogramID;
		}


		if(addFlag)
		{				
			borrow_medicineDTO.insertionDate = TimeConverter.getToday();
		}			


		
		
		System.out.println("Done adding  addBorrow_medicine dto = " + borrow_medicineDTO);
		
		List<BorrowMedicineDetailsDTO> borrowMedicineDetailsDTOList;
		try
		{
			borrowMedicineDetailsDTOList = createBorrowMedicineDetailsDTOListByRequest(request, language, borrow_medicineDTO, userDTO);
		}
		catch(Exception e)
		{
			throw e;
		}
		
		if(addFlag == true)
		{
			 Borrow_medicineDAO.getInstance().add(borrow_medicineDTO);
		}
		else
		{				
			 Borrow_medicineDAO.getInstance().update(borrow_medicineDTO);										
		}

				
		if(addFlag == true) //add or validate
		{
			if(borrowMedicineDetailsDTOList != null)
			{				
				for(BorrowMedicineDetailsDTO borrowMedicineDetailsDTO: borrowMedicineDetailsDTOList)
				{
					borrowMedicineDetailsDTO.borrowMedicineId = borrow_medicineDTO.iD; 
					borrowMedicineDetailsDAO.add(borrowMedicineDetailsDTO);
					/*Drug_informationDTO drug_informationDTO = drug_informationDAO.getDTOByID(borrowMedicineDetailsDTO.drugInformationType);
					Medical_inventory_outDTO medical_inventory_outDTO = new Medical_inventory_outDTO(drug_informationDTO,
							borrowMedicineDetailsDTO.quantity, userDTO, MedicalInventoryInDTO.TR_BORROW, borrowedForDTO, borrowMedicineDetailsDTO.medicalItemType, "", "");				
					medical_inventory_outDAO.add(medical_inventory_outDTO);
					PersonalStockDTO personalStockDTO = PersonalStockDAO.getInstance().getDTOByUserNameAndDrugId(borrowMedicineDetailsDTO.doctorUserName,
							borrowMedicineDetailsDTO.drugInformationType, borrowMedicineDetailsDTO.medicalItemType);
					if(personalStockDTO == null)
					{
						personalStockDTO = new PersonalStockDTO(borrowMedicineDetailsDTO, drug_informationDTO);
						PersonalStockDAO.getInstance().add(personalStockDTO);
					}
					else
					{
						personalStockDTO.quantity += borrowMedicineDetailsDTO.quantity;
						PersonalStockDAO.getInstance().update(personalStockDTO);
					}*/
				}
			}
		
		}			
						
		return borrow_medicineDTO;

	}
	private List<BorrowMedicineDetailsDTO> createBorrowMedicineDetailsDTOListByRequest(HttpServletRequest request, String language, Borrow_medicineDTO borrow_medicineDTO, UserDTO userDTO) throws Exception{ 
		List<BorrowMedicineDetailsDTO> borrowMedicineDetailsDTOList = new ArrayList<BorrowMedicineDetailsDTO>();
		if(request.getParameterValues("borrowMedicineDetails.iD") != null) 
		{
			int borrowMedicineDetailsItemNo = request.getParameterValues("borrowMedicineDetails.iD").length;
			
			
			for(int index=0;index<borrowMedicineDetailsItemNo;index++){
				BorrowMedicineDetailsDTO borrowMedicineDetailsDTO = createBorrowMedicineDetailsDTOByRequestAndIndex(request,true,index, language, borrow_medicineDTO, userDTO);
				for(BorrowMedicineDetailsDTO existingDTO: borrowMedicineDetailsDTOList)
				{
					if(existingDTO.drugInformationType == borrowMedicineDetailsDTO.drugInformationType)
					{
						throw new Exception(LM.getText(LC.BORROW_MEDICINE_ADD_BORROW_MEDICINE_DETAILS_DRUGINFORMATIONTYPE, language) + " " + ErrorMessage.getInvalidMessage(language));
					}
				}
				borrowMedicineDetailsDTOList.add(borrowMedicineDetailsDTO);
			}
			
			return borrowMedicineDetailsDTOList;
		}
		return null;
	}
	
	private BorrowMedicineDetailsDTO createBorrowMedicineDetailsDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,
			int index, String language, Borrow_medicineDTO borrow_medicineDTO, UserDTO userDTO) throws Exception{
	
		BorrowMedicineDetailsDTO borrowMedicineDetailsDTO;
		if(addFlag == true )
		{
			borrowMedicineDetailsDTO = new BorrowMedicineDetailsDTO();
		}
		else
		{
			borrowMedicineDetailsDTO = borrowMedicineDetailsDAO.getDTOByID(Long.parseLong(request.getParameterValues("borrowMedicineDetails.iD")[index]));
		}

		String Value = "";
		Value = request.getParameterValues("borrowMedicineDetails.borrowMedicineId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		else
		{
			throw new Exception(LM.getText(LC.BORROW_MEDICINE_ADD_BORROW_MEDICINE_DETAILS_BORROWMEDICINEID, language) + " " + ErrorMessage.getInvalidMessage(language));
		}

		borrowMedicineDetailsDTO.borrowMedicineId = Long.parseLong(Value);	
		borrowMedicineDetailsDTO.doctorId = borrow_medicineDTO.organogramId;
		borrowMedicineDetailsDTO.doctorUserName = borrow_medicineDTO.doctorUserName;
		borrowMedicineDetailsDTO.medicalItemType = borrow_medicineDTO.medicalItemType;

		Value = request.getParameterValues("borrowMedicineDetails.drugInformationType")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		else
		{
			throw new Exception(LM.getText(LC.BORROW_MEDICINE_ADD_BORROW_MEDICINE_DETAILS_DRUGINFORMATIONTYPE, language) + " " + ErrorMessage.getInvalidMessage(language));
		}
		borrowMedicineDetailsDTO.drugInformationType = Long.parseLong(Value);
		
		Value = request.getParameterValues("borrowMedicineDetails.quantity")[index];
		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		else
		{
			throw new Exception(LM.getText(LC.BORROW_MEDICINE_ADD_BORROW_MEDICINE_DETAILS_QUANTITY, language) + " " + ErrorMessage.getInvalidMessage(language));
		}
		if(borrowMedicineDetailsDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_DRUG)
		{
			Drug_informationDTO drug_informationDTO = drug_informationDAO.getDTOByID(borrowMedicineDetailsDTO.drugInformationType);
			if(drug_informationDTO == null)
			{
				throw new Exception(LM.getText(LC.BORROW_MEDICINE_ADD_BORROW_MEDICINE_DETAILS_DRUGINFORMATIONTYPE, language) + " " + ErrorMessage.getInvalidMessage(language));

			}
			borrowMedicineDetailsDTO.medicineGenericNameId = drug_informationDTO.medicineGenericNameType;
			borrowMedicineDetailsDTO.quantity = Integer.parseInt(Value);

			if(borrowMedicineDetailsDTO.quantity > drug_informationDTO.availableStock)
			{
				throw new Exception(ErrorMessage.getGenericInvalidMessage(language));			
			}
		}
		else if(borrowMedicineDetailsDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_EQUIPMENT)
		{
			Medical_equipment_nameDTO medical_equipment_nameDTO = medical_equipment_nameDAO.getDTOByID(borrowMedicineDetailsDTO.drugInformationType);
			if(medical_equipment_nameDTO == null)
			{
				if(medical_equipment_nameDTO == null)
				{
					throw new Exception(ErrorMessage.getGenericInvalidMessage(language));

				}
			}
			borrowMedicineDetailsDTO.quantity = Integer.parseInt(Value);

			/*if(borrowMedicineDetailsDTO.quantity > medical_equipment_nameDTO.currentStock)
			{
				throw new Exception(ErrorMessage.getGenericInvalidMessage(language));			
			}*/
		}

		borrowMedicineDetailsDTO.remainingQuantity = borrowMedicineDetailsDTO.quantity;

		borrowMedicineDetailsDTO.medicineGenericNameId = Long.parseLong(Value);
		borrowMedicineDetailsDTO.transactionDate = TimeConverter.getToday();

		return borrowMedicineDetailsDTO;
	
	}	
}

