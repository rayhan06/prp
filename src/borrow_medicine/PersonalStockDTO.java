package borrow_medicine;

import deliver_or_return_medicine.DeliverOrReturnDetailsDTO;
import department_requisition_lot.DepartmentRequisitionItemDTO;
import department_requisition_lot.Department_requisition_lotDTO;
import drug_information.Drug_informationDTO;
import manually_deliver_medicine.Manually_deliver_medicineDTO;
import manually_deliver_medicine.MedicineDetailsDTO;
import medical_equipment_name.Medical_equipment_nameDTO;
import prescription_details.PatientPrescriptionMedicine2DTO;
import prescription_details.Prescription_detailsDTO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;

public class PersonalStockDTO extends CommonDTO{
	
	public long organogramId = -1;
    public String userName = "";
	public long drugInformationType = -1;
	public long medicineGenericNameId = -1;
	public int quantity = 0;
	public double unitPrice = 0;
	public int medicalItemType = SessionConstants.MEDICAL_ITEM_DRUG;
	public int medicalDeptCat = -1;
	public int detailedCount = 0;
	
	public double cost = 0;
	public long organizationId = -1;
	
	
	public PersonalStockDTO()
	{
		
	}
	
	public PersonalStockDTO(Department_requisition_lotDTO department_requisition_lotDTO, DepartmentRequisitionItemDTO deepartmentRequisitionItemDTO, Drug_informationDTO drug_informationDTO)
	{
		this.organogramId = -1;
		this.userName = "";
		this.drugInformationType = drug_informationDTO.iD;
		this.medicineGenericNameId = drug_informationDTO.medicineGenericNameType;
		this.quantity = deepartmentRequisitionItemDTO.quantity;
		this.unitPrice = drug_informationDTO.unitPrice;
		medicalItemType = deepartmentRequisitionItemDTO.medicalItemCat;
		this.medicalDeptCat = department_requisition_lotDTO.medicalDeptCat;
		if(this.drugInformationType == Drug_informationDTO.ACCU_CHECK_ID)
		{
			this.detailedCount = Drug_informationDTO.ACCU_SIZE * this.quantity;
		}
	}
	
	public PersonalStockDTO(BorrowMedicineDetailsDTO borrowMedicineDetailsDTO, Drug_informationDTO drug_informationDTO)
	{
		this.organogramId = borrowMedicineDetailsDTO.doctorId;
		this.userName = borrowMedicineDetailsDTO.doctorUserName;
		this.drugInformationType = borrowMedicineDetailsDTO.drugInformationType;
		this.medicineGenericNameId = borrowMedicineDetailsDTO.medicineGenericNameId;
		this.quantity = borrowMedicineDetailsDTO.quantity;
		this.unitPrice = drug_informationDTO.unitPrice;
		medicalItemType = borrowMedicineDetailsDTO.medicalItemType;
	}
	
	public PersonalStockDTO(BorrowMedicineDetailsDTO borrowMedicineDetailsDTO, Medical_equipment_nameDTO medicalEquipment_nameDTO, 
			String userName, long organogramId)
	{
		this.organogramId = organogramId;
		this.userName = userName;
		this.drugInformationType = borrowMedicineDetailsDTO.drugInformationType;

		this.quantity = borrowMedicineDetailsDTO.quantity;
		this.unitPrice = medicalEquipment_nameDTO.unitPrice;
		medicalItemType = borrowMedicineDetailsDTO.medicalItemType;
	}
	
	public PersonalStockDTO(UserDTO userDTO, Drug_informationDTO drug_informationDTO, int quantity, int medicalItemCat)
	{
		this.organogramId = userDTO.organogramID;
		this.userName = userDTO.userName;
		this.drugInformationType = drug_informationDTO.iD;
		this.medicineGenericNameId = drug_informationDTO.medicineGenericNameType;
		this.quantity = quantity;
		this.unitPrice = drug_informationDTO.unitPrice;
		medicalItemType = medicalItemCat;
	}
	
	public PersonalStockDTO(Prescription_detailsDTO prescription_detailsDTO,
			PatientPrescriptionMedicine2DTO patientPrescriptionMedicine2DTO,
			Drug_informationDTO drug_informationDTO)
	{
		this.organogramId = prescription_detailsDTO.employeeId;
		this.userName = prescription_detailsDTO.employeeUserName;
		this.drugInformationType = patientPrescriptionMedicine2DTO.drugInformationType;
		this.medicineGenericNameId = drug_informationDTO.medicineGenericNameType;
		this.quantity = patientPrescriptionMedicine2DTO.doseSoldToday;
		this.unitPrice = drug_informationDTO.unitPrice;
		
		medicalItemType = SessionConstants.MEDICAL_ITEM_DRUG;
	}
	
	public PersonalStockDTO(Manually_deliver_medicineDTO manually_deliver_medicineDTO,
			MedicineDetailsDTO medicineDetailsDTO,
			Drug_informationDTO drug_informationDTO)
	{
		this.organogramId = manually_deliver_medicineDTO.employeeOrganogramId;
		this.userName = manually_deliver_medicineDTO.employeeUserName;
		this.drugInformationType = medicineDetailsDTO.drugInformationType;
		this.medicineGenericNameId = drug_informationDTO.medicineGenericNameType;
		this.quantity = medicineDetailsDTO.quantity;
		this.unitPrice = drug_informationDTO.unitPrice;

		medicalItemType = SessionConstants.MEDICAL_ITEM_DRUG;
	}
	
	public PersonalStockDTO(DeliverOrReturnDetailsDTO deliverOrReturnDetailsDTO,
			Drug_informationDTO drug_informationDTO)
	{
		this.organogramId = deliverOrReturnDetailsDTO.patientId;
		this.userName = deliverOrReturnDetailsDTO.patientUserName;
		this.drugInformationType = deliverOrReturnDetailsDTO.drugInformationType;
		this.medicineGenericNameId = drug_informationDTO.medicineGenericNameType;
		this.quantity = deliverOrReturnDetailsDTO.quantity;
		medicalItemType = SessionConstants.MEDICAL_ITEM_DRUG;
		this.unitPrice = drug_informationDTO.unitPrice;
	}

}
