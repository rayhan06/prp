package borrow_medicine;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Borrow_medicineRepository implements Repository {
	Borrow_medicineDAO borrow_medicineDAO = null;
	
	static Logger logger = Logger.getLogger(Borrow_medicineRepository.class);
	Map<Long, Borrow_medicineDTO>mapOfBorrow_medicineDTOToiD;
	Gson gson;

  
	private Borrow_medicineRepository(){
		borrow_medicineDAO = Borrow_medicineDAO.getInstance();
		mapOfBorrow_medicineDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Borrow_medicineRepository INSTANCE = new Borrow_medicineRepository();
    }

    public static Borrow_medicineRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Borrow_medicineDTO> borrow_medicineDTOs = borrow_medicineDAO.getAllDTOs(reloadAll);
			for(Borrow_medicineDTO borrow_medicineDTO : borrow_medicineDTOs) {
				Borrow_medicineDTO oldBorrow_medicineDTO = getBorrow_medicineDTOByiD(borrow_medicineDTO.iD);
				if( oldBorrow_medicineDTO != null ) {
					mapOfBorrow_medicineDTOToiD.remove(oldBorrow_medicineDTO.iD);
				
					
				}
				if(borrow_medicineDTO.isDeleted == 0) 
				{
					
					mapOfBorrow_medicineDTOToiD.put(borrow_medicineDTO.iD, borrow_medicineDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Borrow_medicineDTO clone(Borrow_medicineDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Borrow_medicineDTO.class);
	}
	
	
	public List<Borrow_medicineDTO> getBorrow_medicineList() {
		List <Borrow_medicineDTO> borrow_medicines = new ArrayList<Borrow_medicineDTO>(this.mapOfBorrow_medicineDTOToiD.values());
		return borrow_medicines;
	}
	
	public List<Borrow_medicineDTO> copyBorrow_medicineList() {
		List <Borrow_medicineDTO> borrow_medicines = getBorrow_medicineList();
		return borrow_medicines
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Borrow_medicineDTO getBorrow_medicineDTOByiD( long iD){
		return mapOfBorrow_medicineDTOToiD.get(iD);
	}
	
	public Borrow_medicineDTO copyBorrow_medicineDTOByiD( long iD){
		return clone(mapOfBorrow_medicineDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return borrow_medicineDAO.getTableName();
	}
}


