package borrow_medicine;
import java.util.*;

import department_requisition_lot.Department_requisition_lotDTO;
import sessionmanager.SessionConstants;
import util.*; 


public class Borrow_medicineDTO extends CommonDTO
{

	public long organogramId = -1;
    public String doctorUserName = "";
    public String stockLotNumber = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	public int medicalItemType = SessionConstants.MEDICAL_ITEM_DRUG;
	
	public String borrowedForUserName = "";
	public String borrowedByUserName = "";
	public String approvedByUserName = "";
	public String sellerUserName = "";
	
	public String approvalMessage = "";
	public String borrowerName = "";
	public String borrowerPhone = "";
	public String borrowedForName = "";
	public String borrowedForPhone = "";
	
	public long borrowedForEmployeeId = -1;
	public long borrowedByEmployeeId = -1;
	public long approvedByEmployeeId = -1;
	public long sellerEmployeeId = -1;
	
	public long approvalDate = -1;
	public long sellingDate = -1;
	
	public int approvalStatus = 0;
	
	public boolean isDelivered = false;
	
	public List<BorrowMedicineDetailsDTO> borrowMedicineDetailsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Borrow_medicineDTO[" +
            " iD = " + iD +
            " doctorId = " + organogramId +
            " doctorUserName = " + doctorUserName +
            " stockLotNumber = " + stockLotNumber +
            " remarks = " + remarks +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}