package borrow_medicine;
import java.util.*;

import sessionmanager.SessionConstants;
import util.*; 


public class BorrowMedicineDetailsDTO extends CommonDTO
{

	public long borrowMedicineId = -1;
	public long doctorId = -1;
    public String doctorUserName = "";
	public long drugInformationType = -1;
	public long medicineGenericNameId = -1;
	public int quantity = -1;
	public int remainingQuantity = 0;
	public double unitPrice = -1;
	public long transactionDate = System.currentTimeMillis();
	public int medicalItemType = SessionConstants.MEDICAL_ITEM_DRUG;
	
	public List<BorrowMedicineDetailsDTO> borrowMedicineDetailsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$BorrowMedicineDetailsDTO[" +
            " iD = " + iD +
            " borrowMedicineId = " + borrowMedicineId +
            " doctorId = " + doctorId +
            " doctorUserName = " + doctorUserName +
            " drugInformationType = " + drugInformationType +
            " medicineGenericNameId = " + medicineGenericNameId +
            " quantity = " + quantity +
            " unitPrice = " + unitPrice +
            " transactionDate = " + transactionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}