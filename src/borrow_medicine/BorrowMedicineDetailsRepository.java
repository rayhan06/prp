package borrow_medicine;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class BorrowMedicineDetailsRepository implements Repository {
	BorrowMedicineDetailsDAO borrowmedicinedetailsDAO = null;
	
	static Logger logger = Logger.getLogger(BorrowMedicineDetailsRepository.class);
	Map<Long, BorrowMedicineDetailsDTO>mapOfBorrowMedicineDetailsDTOToiD;
	Map<Long, Set<BorrowMedicineDetailsDTO> >mapOfBorrowMedicineDetailsDTOToborrowMedicineId;
	Gson gson;

  
	private BorrowMedicineDetailsRepository(){
		borrowmedicinedetailsDAO = BorrowMedicineDetailsDAO.getInstance();
		mapOfBorrowMedicineDetailsDTOToiD = new ConcurrentHashMap<>();
		mapOfBorrowMedicineDetailsDTOToborrowMedicineId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static BorrowMedicineDetailsRepository INSTANCE = new BorrowMedicineDetailsRepository();
    }

    public static BorrowMedicineDetailsRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<BorrowMedicineDetailsDTO> borrowmedicinedetailsDTOs = borrowmedicinedetailsDAO.getAllDTOs(reloadAll);
			for(BorrowMedicineDetailsDTO borrowmedicinedetailsDTO : borrowmedicinedetailsDTOs) {
				BorrowMedicineDetailsDTO oldBorrowMedicineDetailsDTO = getBorrowMedicineDetailsDTOByiD(borrowmedicinedetailsDTO.iD);
				if( oldBorrowMedicineDetailsDTO != null ) {
					mapOfBorrowMedicineDetailsDTOToiD.remove(oldBorrowMedicineDetailsDTO.iD);
				
					if(mapOfBorrowMedicineDetailsDTOToborrowMedicineId.containsKey(oldBorrowMedicineDetailsDTO.borrowMedicineId)) {
						mapOfBorrowMedicineDetailsDTOToborrowMedicineId.get(oldBorrowMedicineDetailsDTO.borrowMedicineId).remove(oldBorrowMedicineDetailsDTO);
					}
					if(mapOfBorrowMedicineDetailsDTOToborrowMedicineId.get(oldBorrowMedicineDetailsDTO.borrowMedicineId).isEmpty()) {
						mapOfBorrowMedicineDetailsDTOToborrowMedicineId.remove(oldBorrowMedicineDetailsDTO.borrowMedicineId);
					}
					
					
				}
				if(borrowmedicinedetailsDTO.isDeleted == 0) 
				{
					
					mapOfBorrowMedicineDetailsDTOToiD.put(borrowmedicinedetailsDTO.iD, borrowmedicinedetailsDTO);
				
					if( ! mapOfBorrowMedicineDetailsDTOToborrowMedicineId.containsKey(borrowmedicinedetailsDTO.borrowMedicineId)) {
						mapOfBorrowMedicineDetailsDTOToborrowMedicineId.put(borrowmedicinedetailsDTO.borrowMedicineId, new HashSet<>());
					}
					mapOfBorrowMedicineDetailsDTOToborrowMedicineId.get(borrowmedicinedetailsDTO.borrowMedicineId).add(borrowmedicinedetailsDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public BorrowMedicineDetailsDTO clone(BorrowMedicineDetailsDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, BorrowMedicineDetailsDTO.class);
	}
	
	
	public List<BorrowMedicineDetailsDTO> getBorrowMedicineDetailsList() {
		List <BorrowMedicineDetailsDTO> borrowmedicinedetailss = new ArrayList<BorrowMedicineDetailsDTO>(this.mapOfBorrowMedicineDetailsDTOToiD.values());
		return borrowmedicinedetailss;
	}
	
	public List<BorrowMedicineDetailsDTO> copyBorrowMedicineDetailsList() {
		List <BorrowMedicineDetailsDTO> borrowmedicinedetailss = getBorrowMedicineDetailsList();
		return borrowmedicinedetailss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public BorrowMedicineDetailsDTO getBorrowMedicineDetailsDTOByiD( long iD){
		return mapOfBorrowMedicineDetailsDTOToiD.get(iD);
	}
	
	public BorrowMedicineDetailsDTO copyBorrowMedicineDetailsDTOByiD( long iD){
		return clone(mapOfBorrowMedicineDetailsDTOToiD.get(iD));
	}
	
	
	public List<BorrowMedicineDetailsDTO> getBorrowMedicineDetailsDTOByborrowMedicineId(long borrowMedicineId) {
		return new ArrayList<>( mapOfBorrowMedicineDetailsDTOToborrowMedicineId.getOrDefault(borrowMedicineId,new HashSet<>()));
	}
	
	public List<BorrowMedicineDetailsDTO> copyBorrowMedicineDetailsDTOByborrowMedicineId(long borrowMedicineId)
	{
		List <BorrowMedicineDetailsDTO> borrowmedicinedetailss = getBorrowMedicineDetailsDTOByborrowMedicineId(borrowMedicineId);
		return borrowmedicinedetailss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return borrowmedicinedetailsDAO.getTableName();
	}
}


