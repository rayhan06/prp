package borrow_medicine;
import java.util.*; 
import util.*;


public class BorrowMedicineDetailsMAPS extends CommonMaps
{	
	public BorrowMedicineDetailsMAPS(String tableName)
	{
		


		java_SQL_map.put("borrow_medicine_id".toLowerCase(), "borrowMedicineId".toLowerCase());
		java_SQL_map.put("doctor_id".toLowerCase(), "doctorId".toLowerCase());
		java_SQL_map.put("doctor_user_name".toLowerCase(), "doctorUserName".toLowerCase());
		java_SQL_map.put("drug_information_type".toLowerCase(), "drugInformationType".toLowerCase());
		java_SQL_map.put("medicine_generic_name_id".toLowerCase(), "medicineGenericNameId".toLowerCase());
		java_SQL_map.put("quantity".toLowerCase(), "quantity".toLowerCase());
		java_SQL_map.put("unit_price".toLowerCase(), "unitPrice".toLowerCase());
		java_SQL_map.put("transaction_date".toLowerCase(), "transactionDate".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Borrow Medicine Id".toLowerCase(), "borrowMedicineId".toLowerCase());
		java_Text_map.put("Doctor Id".toLowerCase(), "doctorId".toLowerCase());
		java_Text_map.put("Doctor User Name".toLowerCase(), "doctorUserName".toLowerCase());
		java_Text_map.put("Drug Information".toLowerCase(), "drugInformationType".toLowerCase());
		java_Text_map.put("Medicine Generic Name Id".toLowerCase(), "medicineGenericNameId".toLowerCase());
		java_Text_map.put("Quantity".toLowerCase(), "quantity".toLowerCase());
		java_Text_map.put("Unit Price".toLowerCase(), "unitPrice".toLowerCase());
		java_Text_map.put("Transaction Date".toLowerCase(), "transactionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}