package borrow_medicine;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import workflow.WorkflowController;
import pb.*;

public class Borrow_medicineDAO  implements CommonDAOService<Borrow_medicineDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Borrow_medicineDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"doctor_id",
			"doctor_user_name",
			"stock_lot_number",
			"remarks",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"search_column",
			"medical_item_type",
			
			"borrowed_by_user_name",
			"borrowed_for_user_name",
			"seller_user_name",
			"approver_user_name",
			
			"borrowed_by_employee_id",
			"borrowed_for_employee_id",
			"seller_employee_id",
			"approver_employee_id",
			
			"selling_date",
			"approval_date",
			
			"approval_status",
			"approval_message",
			
			"borrower_name",
			"borrower_phone",
			"borrowed_for_name",
			"borrowed_for_phone",
			
			"is_delivered",
			
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("borrowed_for_user_name"," and (borrowed_for_user_name = ?)");
		searchMap.put("borrower_name"," and (borrower_name like ?)");
		searchMap.put("phone_number"," and (borrower_phone = ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Borrow_medicineDAO INSTANCE = new Borrow_medicineDAO();
	}

	public static Borrow_medicineDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Borrow_medicineDTO borrow_medicineDTO)
	{
		borrow_medicineDTO.searchColumn = "";
		borrow_medicineDTO.searchColumn += borrow_medicineDTO.borrowedByUserName + " ";
		borrow_medicineDTO.searchColumn += borrow_medicineDTO.borrowedForUserName + " ";
		borrow_medicineDTO.searchColumn += Utils.getDigits(borrow_medicineDTO.borrowerPhone, "english") + " ";
		borrow_medicineDTO.searchColumn += Utils.getDigits(borrow_medicineDTO.borrowerPhone, "bangla") + " ";
		borrow_medicineDTO.searchColumn += WorkflowController.getNameFromUserName(borrow_medicineDTO.borrowedForUserName, "english") + " ";
		borrow_medicineDTO.searchColumn += WorkflowController.getNameFromUserName(borrow_medicineDTO.borrowedByUserName, "bangla") + " ";
		
		borrow_medicineDTO.searchColumn += borrow_medicineDTO.stockLotNumber + " ";
		borrow_medicineDTO.searchColumn += borrow_medicineDTO.remarks + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Borrow_medicineDTO borrow_medicineDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(borrow_medicineDTO);
		if(isInsert)
		{
			ps.setObject(++index,borrow_medicineDTO.iD);
		}
		ps.setObject(++index,borrow_medicineDTO.organogramId);
		ps.setObject(++index,borrow_medicineDTO.doctorUserName);
		ps.setObject(++index,borrow_medicineDTO.stockLotNumber);
		ps.setObject(++index,borrow_medicineDTO.remarks);
		ps.setObject(++index,borrow_medicineDTO.insertedByUserId);
		ps.setObject(++index,borrow_medicineDTO.insertedByOrganogramId);
		ps.setObject(++index,borrow_medicineDTO.insertionDate);
		ps.setObject(++index,borrow_medicineDTO.searchColumn);
		ps.setObject(++index,borrow_medicineDTO.medicalItemType);
		
		ps.setObject(++index,borrow_medicineDTO.borrowedByUserName);
		ps.setObject(++index,borrow_medicineDTO.borrowedForUserName);
		ps.setObject(++index,borrow_medicineDTO.sellerUserName);
		ps.setObject(++index,borrow_medicineDTO.approvedByUserName);
		
		ps.setObject(++index,borrow_medicineDTO.borrowedByEmployeeId);
		ps.setObject(++index,borrow_medicineDTO.borrowedForEmployeeId);
		ps.setObject(++index,borrow_medicineDTO.sellerEmployeeId);
		ps.setObject(++index,borrow_medicineDTO.approvedByEmployeeId);
		
		ps.setObject(++index,borrow_medicineDTO.sellingDate);
		ps.setObject(++index,borrow_medicineDTO.approvalDate);
		
		ps.setObject(++index,borrow_medicineDTO.approvalStatus);
		ps.setObject(++index,borrow_medicineDTO.approvalMessage);
		
		ps.setObject(++index,borrow_medicineDTO.borrowerName);
		ps.setObject(++index,borrow_medicineDTO.borrowerPhone);
		ps.setObject(++index,borrow_medicineDTO.borrowedForName);
		ps.setObject(++index,borrow_medicineDTO.borrowedForPhone);
		
		ps.setObject(++index,borrow_medicineDTO.isDelivered);
		
		if(isInsert)
		{
			ps.setObject(++index,borrow_medicineDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,borrow_medicineDTO.iD);
		}
	}
	
	@Override
	public Borrow_medicineDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Borrow_medicineDTO borrow_medicineDTO = new Borrow_medicineDTO();
			int i = 0;
			borrow_medicineDTO.iD = rs.getLong(columnNames[i++]);
			borrow_medicineDTO.organogramId = rs.getLong(columnNames[i++]);
			borrow_medicineDTO.doctorUserName = rs.getString(columnNames[i++]);
			borrow_medicineDTO.stockLotNumber = rs.getString(columnNames[i++]);
			borrow_medicineDTO.remarks = rs.getString(columnNames[i++]);
			borrow_medicineDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			borrow_medicineDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			borrow_medicineDTO.insertionDate = rs.getLong(columnNames[i++]);
			borrow_medicineDTO.searchColumn = rs.getString(columnNames[i++]);
			borrow_medicineDTO.medicalItemType = rs.getInt(columnNames[i++]);
			
			borrow_medicineDTO.borrowedByUserName = rs.getString(columnNames[i++]);
			borrow_medicineDTO.borrowedForUserName = rs.getString(columnNames[i++]);
			borrow_medicineDTO.sellerUserName = rs.getString(columnNames[i++]);
			borrow_medicineDTO.approvedByUserName = rs.getString(columnNames[i++]);
			
			borrow_medicineDTO.borrowedByEmployeeId = rs.getLong(columnNames[i++]);
			borrow_medicineDTO.borrowedForEmployeeId = rs.getLong(columnNames[i++]);
			borrow_medicineDTO.sellerEmployeeId = rs.getLong(columnNames[i++]);
			borrow_medicineDTO.approvedByEmployeeId = rs.getLong(columnNames[i++]);
			
			borrow_medicineDTO.sellingDate = rs.getLong(columnNames[i++]);
			borrow_medicineDTO.approvalDate = rs.getLong(columnNames[i++]);

			borrow_medicineDTO.approvalStatus = rs.getInt(columnNames[i++]);
			borrow_medicineDTO.approvalMessage = rs.getString(columnNames[i++]);
			
			borrow_medicineDTO.borrowerName = rs.getString(columnNames[i++]);
			borrow_medicineDTO.borrowerPhone = rs.getString(columnNames[i++]);
			
			borrow_medicineDTO.borrowedForName = rs.getString(columnNames[i++]);
			borrow_medicineDTO.borrowedForPhone = rs.getString(columnNames[i++]);
			
			borrow_medicineDTO.isDelivered = rs.getBoolean(columnNames[i++]);
			
			
			borrow_medicineDTO.isDeleted = rs.getInt(columnNames[i++]);
			borrow_medicineDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return borrow_medicineDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Borrow_medicineDTO getDTOByID (long id)
	{
		Borrow_medicineDTO borrow_medicineDTO = null;
		try 
		{
			borrow_medicineDTO = getDTOFromID(id);
			if(borrow_medicineDTO != null)
			{
				BorrowMedicineDetailsDAO borrowMedicineDetailsDAO = BorrowMedicineDetailsDAO.getInstance();				
				List<BorrowMedicineDetailsDTO> borrowMedicineDetailsDTOList = (List<BorrowMedicineDetailsDTO>)borrowMedicineDetailsDAO.getDTOsByParent("borrow_medicine_id", borrow_medicineDTO.iD);
				borrow_medicineDTO.borrowMedicineDetailsDTOList = borrowMedicineDetailsDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return borrow_medicineDTO;
	}

	@Override
	public String getTableName() {
		return "borrow_medicine";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Borrow_medicineDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Borrow_medicineDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	