package borrow_medicine;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class BorrowMedicineDetailsDAO  implements CommonDAOService<BorrowMedicineDetailsDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private BorrowMedicineDetailsDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"borrow_medicine_id",
			"doctor_id",
			"doctor_user_name",
			"drug_information_type",
			"medicine_generic_name_id",
			"quantity",
			"remaining_quantity",
			"unit_price",
			"transaction_date",
			"medical_item_type",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("doctor_user_name"," and (doctor_user_name like ?)");
		searchMap.put("drug_information_type"," and (drug_information_type = ?)");
		searchMap.put("transaction_date_start"," and (transaction_date >= ?)");
		searchMap.put("transaction_date_end"," and (transaction_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final BorrowMedicineDetailsDAO INSTANCE = new BorrowMedicineDetailsDAO();
	}

	public static BorrowMedicineDetailsDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(BorrowMedicineDetailsDTO borrowmedicinedetailsDTO)
	{
		borrowmedicinedetailsDTO.searchColumn = "";
		borrowmedicinedetailsDTO.searchColumn += borrowmedicinedetailsDTO.doctorUserName + " ";
		borrowmedicinedetailsDTO.searchColumn += CommonDAO.getName("English", "drug_information", borrowmedicinedetailsDTO.drugInformationType) + " " + CommonDAO.getName("Bangla", "drug_information", borrowmedicinedetailsDTO.drugInformationType) + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, BorrowMedicineDetailsDTO borrowmedicinedetailsDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(borrowmedicinedetailsDTO);
		if(isInsert)
		{
			ps.setObject(++index,borrowmedicinedetailsDTO.iD);
		}
		ps.setObject(++index,borrowmedicinedetailsDTO.borrowMedicineId);
		ps.setObject(++index,borrowmedicinedetailsDTO.doctorId);
		ps.setObject(++index,borrowmedicinedetailsDTO.doctorUserName);
		ps.setObject(++index,borrowmedicinedetailsDTO.drugInformationType);
		ps.setObject(++index,borrowmedicinedetailsDTO.medicineGenericNameId);
		ps.setObject(++index,borrowmedicinedetailsDTO.quantity);
		ps.setObject(++index,borrowmedicinedetailsDTO.remainingQuantity);
		ps.setObject(++index,borrowmedicinedetailsDTO.unitPrice);
		ps.setObject(++index,borrowmedicinedetailsDTO.transactionDate);
		ps.setObject(++index,borrowmedicinedetailsDTO.medicalItemType);
		if(isInsert)
		{
			ps.setObject(++index,borrowmedicinedetailsDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,borrowmedicinedetailsDTO.iD);
		}
	}
	
	@Override
	public BorrowMedicineDetailsDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			BorrowMedicineDetailsDTO borrowmedicinedetailsDTO = new BorrowMedicineDetailsDTO();
			int i = 0;
			borrowmedicinedetailsDTO.iD = rs.getLong(columnNames[i++]);
			borrowmedicinedetailsDTO.borrowMedicineId = rs.getLong(columnNames[i++]);
			borrowmedicinedetailsDTO.doctorId = rs.getLong(columnNames[i++]);
			borrowmedicinedetailsDTO.doctorUserName = rs.getString(columnNames[i++]);
			borrowmedicinedetailsDTO.drugInformationType = rs.getLong(columnNames[i++]);
			borrowmedicinedetailsDTO.medicineGenericNameId = rs.getLong(columnNames[i++]);
			borrowmedicinedetailsDTO.quantity = rs.getInt(columnNames[i++]);
			borrowmedicinedetailsDTO.remainingQuantity = rs.getInt(columnNames[i++]);
			borrowmedicinedetailsDTO.unitPrice = rs.getDouble(columnNames[i++]);
			borrowmedicinedetailsDTO.transactionDate = rs.getLong(columnNames[i++]);
			borrowmedicinedetailsDTO.medicalItemType = rs.getInt(columnNames[i++]);
			borrowmedicinedetailsDTO.isDeleted = rs.getInt(columnNames[i++]);
			borrowmedicinedetailsDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return borrowmedicinedetailsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	public Collection getDTOsByDrId(String userName) {
        String sql = "SELECT * FROM " + getTableName() + " where isDeleted=0 and doctor_user_name = '" + userName + "' and remaining_quantity > 0 ";
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
    }
		
	public BorrowMedicineDetailsDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "borrow_medicine_details";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((BorrowMedicineDetailsDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((BorrowMedicineDetailsDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	