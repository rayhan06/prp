package allowance_employee_info;

import employee_bank_information.Employee_bank_informationDAO;
import employee_bank_information.Employee_bank_informationDTO;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import employee_records.EmploymentEnum;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import sessionmanager.SessionConstants;
import util.CommonDTO;

import java.util.Comparator;
import java.util.Objects;

public class AllowanceEmployeeInfoDTO extends CommonDTO {
    public long employeeRecordId = 0;
    public String nameEn = "";
    public String nameBn = "";
    public int employmentType;

    public long officeUnitId = 0;
    public String officeNameEn = "";
    public String officeNameBn = "";

    public long organogramId = 0;
    public String organogramNameEn = "";
    public String organogramNameBn = "";

    public String mobileNumber = "";
    public String email = "";
    public String nid = "";
    public String savingAccountNumber = "";
    public String bankNameId = "";
    public int mainSalary = 10000;
    public long religion = 0;
    public long insertBy = 0;
    public long insertionTime = SessionConstants.MIN_DATE;
    public long modifiedBy = 0;

    // not stored in DB used in OT, food bill for now
    public long joiningDate = SessionConstants.MIN_DATE;
    public long lastOfficeDate = SessionConstants.MIN_DATE;
    public int officerTypeCat;
    public String designationPrefix;
    public boolean isMp;

    public boolean isEmployeeActiveInThisTime(long timestamp) {
        if (joiningDate == SessionConstants.MIN_DATE) {
            return false;
        }
        if (lastOfficeDate == SessionConstants.MIN_DATE) {
            return joiningDate <= timestamp;
        }
        final long ONE_DAY_IN_MILLIS = (24L * 60) * 60 * 1000;
        long lastOfficeTime = lastOfficeDate + ONE_DAY_IN_MILLIS - 1;
        // joiningDate is 12AM of the date. Employee is active upto 11:59PM that date.
        return (joiningDate <= timestamp) && (timestamp <= lastOfficeTime);
    }

    public static Comparator<AllowanceEmployeeInfoDTO> compareOfficeUnitThenOrganogramOrderValue
            = Comparator.comparingLong((AllowanceEmployeeInfoDTO dto) -> dto.officeUnitId)
                        .thenComparing(dto -> OfficeUnitOrganogramsRepository.getInstance().getOrderValueById(dto.organogramId));

    public AllowanceEmployeeInfoDTO() {
    }

    private void setEmployeeInfo(Employee_recordsDTO employeeRecordsDTO) {
        employeeRecordId = employeeRecordsDTO.iD;
        if (employeeRecordsDTO.nameEng != null && employeeRecordsDTO.nameEng.trim().length() > 0) {
            nameEn = employeeRecordsDTO.nameEng.trim();
        }
        if (employeeRecordsDTO.nameBng != null && employeeRecordsDTO.nameBng.trim().length() > 0) {
            nameBn = employeeRecordsDTO.nameBng.trim();
        }

        if (employeeRecordsDTO.personalMobile != null && employeeRecordsDTO.personalMobile.trim().length() > 0) {
            mobileNumber = employeeRecordsDTO.personalMobile;
        }
        if (employeeRecordsDTO.personalEml != null && employeeRecordsDTO.personalEml.trim().length() > 0) {
            email = employeeRecordsDTO.personalEml;
        }
        if (employeeRecordsDTO.nid != null && employeeRecordsDTO.nid.trim().length() > 0) {
            nid = employeeRecordsDTO.nid;
        }
        Employee_bank_informationDTO employee_bank_informationDTO = Employee_bank_informationDAO.getInstance().getEmployeeSavingsAccountInfoByEmployeeId(employeeRecordId);
        if (employee_bank_informationDTO != null) {
            bankNameId = String.valueOf(employee_bank_informationDTO.bankNameType);
            savingAccountNumber = employee_bank_informationDTO.bankAccountNumber;
        }
        religion = employeeRecordsDTO.religion;
        employmentType = employeeRecordsDTO.employmentType;
        officerTypeCat = employeeRecordsDTO.officerTypeCat;
        isMp = employeeRecordsDTO.isMP == 1;
    }

    private void setOfficeInfo(EmployeeOfficeDTO employeeOfficeDTO) {
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
        if (officeUnitsDTO != null) {
            officeUnitId = officeUnitsDTO.iD;
            officeNameEn = officeUnitsDTO.unitNameEng;
            officeNameBn = officeUnitsDTO.unitNameBng;
        }
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        if (officeUnitOrganograms != null) {
            organogramId = officeUnitOrganograms.id;
            organogramNameEn = officeUnitOrganograms.designation_eng;
            organogramNameBn = officeUnitOrganograms.designation_bng;
        }
        if (employeeOfficeDTO.joiningDate != null) {
            joiningDate = employeeOfficeDTO.joiningDate;
        }
        if (employeeOfficeDTO.lastOfficeDate != null) {
            lastOfficeDate = employeeOfficeDTO.lastOfficeDate;
        }
    }

    public AllowanceEmployeeInfoDTO(Employee_recordsDTO employeeRecordsDTO) {
        setEmployeeInfo(employeeRecordsDTO);
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employeeRecordId);
        if (employeeOfficeDTO != null) {
            setOfficeInfo(employeeOfficeDTO);
        }
    }

    public AllowanceEmployeeInfoDTO(Employee_recordsDTO employeeRecordsDTO, EmployeeOfficeDTO employeeOfficeDTO) {
        setEmployeeInfo(employeeRecordsDTO);
        setOfficeInfo(employeeOfficeDTO);
    }

    public static AllowanceEmployeeInfoDTO buildDTO(long employeeRecordId) {
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeRecordId);
        if (employeeRecordsDTO == null) return null;
        return new AllowanceEmployeeInfoDTO(employeeRecordsDTO);
    }

    public static AllowanceEmployeeInfoDTO buildDTO(EmployeeOfficeDTO employeeOfficeDTO) {
        if (employeeOfficeDTO == null) {
            return null;
        }
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId);
        if (employeeRecordsDTO == null) {
            return null;
        }
        return new AllowanceEmployeeInfoDTO(employeeRecordsDTO, employeeOfficeDTO);
    }

    public String getDesignationPrefixWithEmploymentCat() {
        if(EmploymentEnum.MASTER_ROLL.getValue() == employmentType) {
            return "সাং";
        }
        return "";
    }

    @Override
    public String toString() {
        return "AllowanceEmployeeInfoDTO{" +
               "employeeRecordId=" + employeeRecordId +
               ", nameEn='" + nameEn + '\'' +
               ", nameBn='" + nameBn + '\'' +
               ", officeUnitId=" + officeUnitId +
               ", officeNameEn='" + officeNameEn + '\'' +
               ", officeNameBn='" + officeNameBn + '\'' +
               ", organogramId=" + organogramId +
               ", organogramNameEn='" + organogramNameEn + '\'' +
               ", organogramNameBn='" + organogramNameBn + '\'' +
               ", mobileNumber='" + mobileNumber + '\'' +
               ", email='" + email + '\'' +
               ", nid='" + nid + '\'' +
               ", savingAccountNumber='" + savingAccountNumber + '\'' +
               ", mainSalary='" + mainSalary + '\'' +
               ", insertBy=" + insertBy +
               ", insertionTime=" + insertionTime +
               ", modifiedBy=" + modifiedBy +
               '}';
    }

    public boolean hasSameData(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AllowanceEmployeeInfoDTO that = (AllowanceEmployeeInfoDTO) o;
        return employeeRecordId == that.employeeRecordId
               && officeUnitId == that.officeUnitId
               && organogramId == that.organogramId
               && mainSalary == that.mainSalary
               && religion == that.religion
               && employmentType == that.employmentType
               && officerTypeCat == that.officerTypeCat
               && Objects.equals(nameEn, that.nameEn)
               && Objects.equals(nameBn, that.nameBn)
               && Objects.equals(officeNameEn, that.officeNameEn)
               && Objects.equals(officeNameBn, that.officeNameBn)
               && Objects.equals(organogramNameEn, that.organogramNameEn)
               && Objects.equals(organogramNameBn, that.organogramNameBn)
               && Objects.equals(mobileNumber, that.mobileNumber)
               && Objects.equals(email, that.email)
               && Objects.equals(nid, that.nid)
               && Objects.equals(savingAccountNumber, that.savingAccountNumber)
               && Objects.equals(designationPrefix, that.designationPrefix);
    }
}
