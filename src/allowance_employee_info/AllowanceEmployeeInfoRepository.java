package allowance_employee_info;

import election_wise_mp.Election_wise_mpRepository;
import employee_bank_information.Employee_bank_informationDAO;
import employee_bank_information.Employee_bank_informationDTO;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_offices.EmployeeOfficesDAO;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import employee_records.MpStatusEnum;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import ot_employee_type.OT_employee_typeDTO;
import util.LockManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class AllowanceEmployeeInfoRepository {

    private final Logger logger = Logger.getLogger(AllowanceEmployeeInfoRepository.class);
    private final AllowanceEmployeeInfoDAO allowanceEmployeeInfoDAO = AllowanceEmployeeInfoDAO.getInstance();
    private final Map<Long, AllowanceEmployeeInfoDTO> mapById = new ConcurrentHashMap<>();
    private final Map<Long, AllowanceEmployeeInfoDTO> mapByEmployeeRecordId = new ConcurrentHashMap<>();

    private AllowanceEmployeeInfoRepository() {
        reload();
    }

    private static class LazyLoader {
        static final AllowanceEmployeeInfoRepository INSTANCE = new AllowanceEmployeeInfoRepository();
    }

    public static AllowanceEmployeeInfoRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    private void reload() {
        logger.debug("AllowanceEmployeeInfoRepository reload start");
        List<AllowanceEmployeeInfoDTO> list = allowanceEmployeeInfoDAO.getAllDTOs(true);
        if (list != null && list.size() > 0) {
            list.forEach(dto -> {
                mapById.put(dto.iD, dto);
                mapByEmployeeRecordId.put(dto.employeeRecordId, dto);
            });
        }
        logger.debug("AllowanceEmployeeInfoRepository reload end");
    }

    public AllowanceEmployeeInfoDTO getById(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "AEIRGBI")) {
                if (mapById.get(id) == null) {
                    AllowanceEmployeeInfoDTO dto = allowanceEmployeeInfoDAO.getDTOFromIdDeletedOrNot(id);
                    if (dto != null) {
                        mapById.put(dto.iD, dto);
                        if (dto.isDeleted == 0) {
                            mapByEmployeeRecordId.put(dto.employeeRecordId, dto);
                        }
                    }
                }
            }
        }
        return mapById.get(id);
    }

    public AllowanceEmployeeInfoDTO getByEmployeeRecordId(long employeeRecordId) {
        return getByEmployeeRecordId(employeeRecordId, 0);
    }

    public AllowanceEmployeeInfoDTO getByEmployeeRecordId(long employeeRecordId, long requesterId) {
        synchronized (LockManager.getLock(employeeRecordId + "AEIRGBERI")) {
            if (mapByEmployeeRecordId.get(employeeRecordId) == null) {
                List<AllowanceEmployeeInfoDTO> list = allowanceEmployeeInfoDAO.getByEmployeeId(employeeRecordId);
                if (list != null && list.size() > 0) {
                    AllowanceEmployeeInfoDTO dto = list.get(0);
                    mapByEmployeeRecordId.put(dto.employeeRecordId, dto);
                    mapById.put(dto.iD, dto);
                }
            }
            if (mapByEmployeeRecordId.get(employeeRecordId) == null) {
                createAllowanceEmployeeInfoDTO(employeeRecordId, requesterId);
            } else {
                AllowanceEmployeeInfoDTO dto = mapByEmployeeRecordId.get(employeeRecordId);
                if (!isMatch(dto)) {
                    boolean deleteResult = allowanceEmployeeInfoDAO.delete(requesterId, dto.iD);
                    if (deleteResult) {
                        createAllowanceEmployeeInfoDTO(employeeRecordId, requesterId);
                        mapById.remove(dto.iD);
                    }
                }
            }
        }

        return mapByEmployeeRecordId.get(employeeRecordId);
    }

    private void createAllowanceEmployeeInfoDTO(long employeeRecordId, long requesterId) {
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeRecordId);
        if (employeeRecordsDTO == null) {
            return;
        }
        AllowanceEmployeeInfoDTO dto = new AllowanceEmployeeInfoDTO();
        dto.employeeRecordId = employeeRecordId;
        dto.nameEn = employeeRecordsDTO.nameEng;
        dto.nameBn = employeeRecordsDTO.nameBng;
        dto.employmentType = employeeRecordsDTO.employmentType;

        if (employeeRecordsDTO.personalMobile != null) {
            dto.mobileNumber = employeeRecordsDTO.personalMobile;
        }
        if (employeeRecordsDTO.personalEml != null) {
            dto.email = employeeRecordsDTO.personalEml;
        }
        Employee_bank_informationDTO employee_bank_informationDTO = Employee_bank_informationDAO.getInstance().getEmployeeSavingsAccountInfoByEmployeeId(employeeRecordId);
        if (employee_bank_informationDTO != null) {
            dto.savingAccountNumber = employee_bank_informationDTO.bankAccountNumber;
        }

        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employeeRecordId);
        if (employeeOfficeDTO != null) {

            Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
            if (officeUnitsDTO != null) {
                dto.officeUnitId = officeUnitsDTO.iD;
                dto.officeNameEn = officeUnitsDTO.unitNameEng;
                dto.officeNameBn = officeUnitsDTO.unitNameBng;
            }

            OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
            if (officeUnitOrganograms != null) {
                dto.organogramId = officeUnitOrganograms.id;
                dto.organogramNameEn = officeUnitOrganograms.designation_eng;
                dto.organogramNameBn = officeUnitOrganograms.designation_bng;
            }

        }


        dto.insertBy = dto.modifiedBy = requesterId;
        dto.insertionTime = dto.lastModificationTime = System.currentTimeMillis();
        try {
            allowanceEmployeeInfoDAO.add(dto);
            mapById.put(dto.iD, dto);
            mapByEmployeeRecordId.put(dto.employeeRecordId, dto);
        } catch (Exception e) {
            logger.error(e);
        }
    }

    private boolean isMatch(AllowanceEmployeeInfoDTO dto) {
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(dto.employeeRecordId);
        if (employeeRecordsDTO == null) {
            return false;
        }
        if (dto.mobileNumber == null) {
            dto.mobileNumber = "";
        }
        if (dto.email == null) {
            dto.email = "";
        }
        if (employeeRecordsDTO.personalMobile == null) {
            employeeRecordsDTO.personalMobile = "";
        }
        if (employeeRecordsDTO.personalEml == null) {
            employeeRecordsDTO.personalEml = "";
        }
        Employee_bank_informationDTO employee_bank_informationDTO =
                Employee_bank_informationDAO.getInstance()
                                            .getEmployeeSavingsAccountInfoByEmployeeId(dto.employeeRecordId);
        String savingAccountNumber = "";
        if (employee_bank_informationDTO != null) {
            savingAccountNumber = employee_bank_informationDTO.bankAccountNumber;
        }
        return dto.nameEn.equals(employeeRecordsDTO.nameEng)
               && dto.nameBn.equals(employeeRecordsDTO.nameBng)
               && dto.mobileNumber.equals(employeeRecordsDTO.personalMobile)
               && dto.email.equals(employeeRecordsDTO.personalEml)
               && dto.nid.equals(employeeRecordsDTO.nid)
               && dto.savingAccountNumber.equals(savingAccountNumber);
    }

    public List<AllowanceEmployeeInfoDTO> getByIds(List<Long> ids) {
        List<AllowanceEmployeeInfoDTO> list = new ArrayList<>();
        List<Long> notFoundInCacheList = ids.stream()
                                            .peek(id -> addToListIfFoundInCache(id, list))
                                            .filter(id -> mapById.get(id) == null)
                                            .collect(Collectors.toList());
        if (notFoundInCacheList.size() > 0) {
            List<AllowanceEmployeeInfoDTO> list1 = allowanceEmployeeInfoDAO.getDTOs(notFoundInCacheList);
            if (list1 != null && list1.size() > 0) {
                list.addAll(list1);
            }
        }
        return list;
    }

    private void addToListIfFoundInCache(long id, List<AllowanceEmployeeInfoDTO> list) {
        AllowanceEmployeeInfoDTO dto = mapById.get(id);
        if (dto != null) {
            list.add(dto);
        }
    }

    public List<AllowanceEmployeeInfoDTO> buildDTOsFromCache(Set<Long> officeUnitIds) {
        return EmployeeOfficeRepository
                .getInstance()
                .getEmployeeOfficeDTOs(officeUnitIds)
                .stream()
                .filter(Objects::nonNull)
                .map(dto -> dto.employeeRecordId)
                .distinct()
                .map(AllowanceEmployeeInfoDTO::buildDTO)
                .filter(Objects::nonNull)
                .sorted(AllowanceEmployeeInfoDTO.compareOfficeUnitThenOrganogramOrderValue)
                .collect(Collectors.toList());
    }

    private boolean isOtFoodBillApplicableForDesignation(AllowanceEmployeeInfoDTO employeeInfoDTO) {
        String organogramName = employeeInfoDTO.organogramNameEn.toLowerCase().trim();
        return !organogramName.matches(".*\\bdriver\\b.*"); // skip drivers from ot, food bill for now
    }

    public List<AllowanceEmployeeInfoDTO> buildDTOsForOvertimeBill(Set<Long> officeUnitIds,
                                                                   OT_employee_typeDTO otEmployeeTypeDTO,
                                                                   Set<Long> excludedEmployeeRecordIds,
                                                                   long startTimeInclusive,
                                                                   long endTimeExclusive) {
        if (otEmployeeTypeDTO.isOfficeUnitIdFilterPresent()) {
            officeUnitIds =
                    officeUnitIds.stream()
                                 .filter(otEmployeeTypeDTO::isOfficeUnitIdAllowed)
                                 .collect(Collectors.toSet());
        }
        return EmployeeOfficesDAO
                .getInstance()
                .findRoutineResponsiblyDTOsByOfficeIdsIgnoringStatus(officeUnitIds)
                .stream()
                .filter(Objects::nonNull)
                .filter(employeeOfficeDTO -> !excludedEmployeeRecordIds.contains(employeeOfficeDTO.employeeRecordId))
                .filter(employeeOfficeDTO -> employeeOfficeDTO.isActiveInTimeRange(startTimeInclusive, endTimeExclusive))
                .map(AllowanceEmployeeInfoDTO::buildDTO)
                .filter(Objects::nonNull)
                .filter(this::isOtFoodBillApplicableForDesignation)
                .filter(allowanceEmployeeInfo -> otEmployeeTypeDTO.isOfficerTypeCatAllowed(allowanceEmployeeInfo.officerTypeCat))
                .filter(allowanceEmployeeInfo -> otEmployeeTypeDTO.isEmploymentCatAllowed(allowanceEmployeeInfo.employmentType))
                .sorted(AllowanceEmployeeInfoDTO.compareOfficeUnitThenOrganogramOrderValue)
                .collect(Collectors.toList());
    }

    private boolean isFoodBillApplicable(AllowanceEmployeeInfoDTO employeeInfoDTO) {
        return !employeeInfoDTO.isMp;
    }

    public List<AllowanceEmployeeInfoDTO> buildDTOsForFoodBill(Set<Long> officeUnitIds,
                                                               Set<Long> excludedEmployeeRecordIds,
                                                               long startTimeInclusive,
                                                               long endTimeExclusive) {
        return EmployeeOfficesDAO
                .getInstance()
                .findRoutineResponsiblyDTOsByOfficeIdsIgnoringStatus(officeUnitIds)
                .stream()
                .filter(Objects::nonNull)
                .filter(employeeOfficeDTO -> !excludedEmployeeRecordIds.contains(employeeOfficeDTO.employeeRecordId))
                .filter(employeeOfficeDTO -> employeeOfficeDTO.isActiveInTimeRange(startTimeInclusive, endTimeExclusive))
                .map(AllowanceEmployeeInfoDTO::buildDTO)
                .filter(Objects::nonNull)
                .filter(this::isFoodBillApplicable)
                .filter(this::isOtFoodBillApplicableForDesignation)
                .sorted(AllowanceEmployeeInfoDTO.compareOfficeUnitThenOrganogramOrderValue)
                .collect(Collectors.toList());
    }

    public List<AllowanceEmployeeInfoDTO> buildDTOsFromCacheForEmploymentType(Set<Long> officeUnitIds, int employmentType) {
        return EmployeeOfficeRepository
                .getInstance()
                .getEmployeeOfficeDTOs(officeUnitIds)
                .stream()
                .filter(Objects::nonNull)
                .map(dto -> dto.employeeRecordId)
                .map(e -> Employee_recordsRepository.getInstance().getById(e))
                .filter(Objects::nonNull)
                .filter(dto -> dto.employmentType == employmentType)
                .map(dto -> dto.iD)
                .map(AllowanceEmployeeInfoDTO::buildDTO)
                .collect(Collectors.toList());
    }

    public List<AllowanceEmployeeInfoDTO> buildDTOsFromCacheForElectionId(long electionDetailsId) {
        return Election_wise_mpRepository
                .getInstance()
                .getElection_wise_mpDTOByElectionDetailsId(electionDetailsId)
                .stream()
                .filter(Objects::nonNull)
                .filter(dto -> dto.mpStatus == MpStatusEnum.ACTIVE.getValue())
                .map(dto -> dto.employeeRecordsId)
                .map(AllowanceEmployeeInfoDTO::buildDTO)
                .collect(Collectors.toList());
    }
}