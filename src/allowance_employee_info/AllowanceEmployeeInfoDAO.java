package allowance_employee_info;

import common.EmployeeCommonDAOService;
import employee_records.Employee_recordsDTO;
import org.apache.log4j.Logger;
import user.UserDTO;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused", "Duplicates"})
public class AllowanceEmployeeInfoDAO implements EmployeeCommonDAOService<AllowanceEmployeeInfoDTO> {
    private static final Logger logger = Logger.getLogger(AllowanceEmployeeInfoDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (employee_records_id,employee_name_en,employee_name_bn,employment_type,"
            .concat("office_unit_id,office_name_en,office_name_bn,organogram_id,organogram_name_en,organogram_name_bn,")
            .concat("mobile_number,email,nid,saving_account_number,main_salary,")
            .concat("modified_by,lastModificationTime,insert_by,insertion_time,isDeleted,ID) ")
            .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery = "UPDATE {tableName} SET employee_records_id=?,employee_name_en=?,employee_name_bn=?,employment_type=?,"
            .concat("office_unit_id=?,office_name_en=?,office_name_bn=?,organogram_id=?,organogram_name_en=?,organogram_name_bn=?,")
            .concat("mobile_number=?,email=?,nid=?,saving_account_number=?,main_salary=?,")
            .concat("modified_by=?,lastModificationTime=? WHERE ID=?");

    private static AllowanceEmployeeInfoDAO INSTANCE = null;

    private AllowanceEmployeeInfoDAO() {
    }

    public static AllowanceEmployeeInfoDAO getInstance() {
        if (INSTANCE == null) {
            synchronized (AllowanceEmployeeInfoDAO.class) {
                if (INSTANCE == null) {
                    INSTANCE = new AllowanceEmployeeInfoDAO();
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, AllowanceEmployeeInfoDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.employeeRecordId);
        ps.setString(++index, dto.nameEn);
        ps.setString(++index, dto.nameBn);
        ps.setInt(++index, dto.employmentType);
        ps.setLong(++index, dto.officeUnitId);
        ps.setString(++index, dto.officeNameEn);
        ps.setString(++index, dto.officeNameBn);
        ps.setLong(++index, dto.organogramId);
        ps.setString(++index, dto.organogramNameEn);
        ps.setString(++index, dto.organogramNameBn);
        ps.setString(++index, dto.mobileNumber);
        ps.setString(++index, dto.email);
        ps.setString(++index, dto.nid);
        ps.setString(++index, dto.savingAccountNumber);
        ps.setInt(++index, dto.mainSalary);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* dto.isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public AllowanceEmployeeInfoDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            AllowanceEmployeeInfoDTO dto = new AllowanceEmployeeInfoDTO();

            dto.iD = rs.getLong("id");
            dto.employeeRecordId = rs.getLong("employee_records_id");
            dto.nameEn = rs.getString("employee_name_en");
            dto.nameBn = rs.getString("employee_name_bn");
            dto.employmentType = rs.getInt("employment_type");
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.officeNameEn = rs.getString("office_name_en");
            dto.officeNameBn = rs.getString("office_name_bn");

            dto.organogramId = rs.getLong("organogram_id");
            dto.organogramNameEn = rs.getString("organogram_name_en");
            dto.organogramNameBn = rs.getString("organogram_name_bn");
            dto.mobileNumber = rs.getString("mobile_number");
            dto.email = rs.getString("email");
            dto.nid = rs.getString("nid");
            dto.savingAccountNumber = rs.getString("saving_account_number");
            dto.mainSalary = rs.getInt("main_salary");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.insertBy = rs.getLong("insert_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");

            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "allowance_employee_info";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return null;
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((AllowanceEmployeeInfoDTO) commonDTO, updateQuery, false);
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((AllowanceEmployeeInfoDTO) commonDTO, addQuery, true);
    }

    public long getAllowanceEmployeeInfoId(Employee_recordsDTO employeeRecordsDTO, UserDTO userDTO) throws Exception {
        AllowanceEmployeeInfoDTO allowanceEmployeeInfoDTOFromEmpDto = new AllowanceEmployeeInfoDTO(employeeRecordsDTO);

        List<AllowanceEmployeeInfoDTO> dtoList = getByEmployeeId(employeeRecordsDTO.iD);
        if (dtoList != null && dtoList.size() > 0) {
            AllowanceEmployeeInfoDTO currentAllowanceEmployeeInfoDTO = dtoList.get(0);
            if (currentAllowanceEmployeeInfoDTO.hasSameData(allowanceEmployeeInfoDTOFromEmpDto)) {
                return currentAllowanceEmployeeInfoDTO.iD;
            }
            delete(userDTO.ID, currentAllowanceEmployeeInfoDTO.iD);
        }

        allowanceEmployeeInfoDTOFromEmpDto.insertBy
                = allowanceEmployeeInfoDTOFromEmpDto.modifiedBy
                = userDTO.ID;

        allowanceEmployeeInfoDTOFromEmpDto.insertionTime
                = allowanceEmployeeInfoDTOFromEmpDto.lastModificationTime
                = System.currentTimeMillis();

        return add(allowanceEmployeeInfoDTOFromEmpDto);
    }
}
