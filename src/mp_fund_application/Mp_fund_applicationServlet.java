package mp_fund_application;

import bangladehi_number_format_util.BangladeshiNumberFormatter;
import budget.BudgetUtils;
import com.google.gson.Gson;
import common.BaseServlet;
import election_constituency.Election_constituencyRepository;
import election_details.Election_detailsRepository;
import election_wise_mp.Election_wise_mpRepository;
import employee_records.Employee_recordsDTO;
import login.LoginDTO;
import mp_fund_amount.Mp_fund_amountDAO;
import mp_fund_amount.Mp_fund_amountDTO;
import mp_fund_recipient.Mp_fund_recipientDAO;
import mp_fund_recipient.Mp_fund_recipientDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


@WebServlet("/Mp_fund_applicationServlet")
@MultipartConfig
public class Mp_fund_applicationServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Mp_fund_applicationServlet.class);
    public static final long MP_BUDGET_OPERATION_ID = 9L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            if ("ajax_getFundAmountData".equals(actionType)) {
                int economicStartYear = Integer.parseInt(request.getParameter("economicStartYear"));
                long electionDetailsId = Long.parseLong(request.getParameter("electionDetailsId"));
                long electionConstituencyId = Long.parseLong(request.getParameter("electionConstituencyId"));

                Map<String, Object> res = getFundAmountData(economicStartYear, electionDetailsId, electionConstituencyId, userDTO);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().println(new Gson().toJson(res));
                return;
            }
            super.doGet(request, response);
            return;
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private Map<String, Object> getFundAmountData(int economicStartYear, long electionDetailsId,
                                                  long electionConstituencyId, UserDTO userDTO) throws Exception {
        Mp_fund_amountDTO fundAmountDTO = Mp_fund_amountDAO.getInstance().prepareMpFundDTO(
                electionDetailsId, electionConstituencyId, economicStartYear
        );
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        String electionConstituencyDistrict = Election_constituencyRepository.getInstance().getText(electionConstituencyId, language);
        Map<String, Object> res = new HashMap<>();
        res.put("electionConstituencyDistrict", electionConstituencyDistrict);
        res.put("amountLeft", fundAmountDTO.amountLeft);
        String formatted = BangladeshiNumberFormatter.getFormattedNumber(
                StringUtils.convertBanglaIfLanguageIsBangla(language, String.valueOf(fundAmountDTO.amountLeft))
        );
        res.put("amountLeftFormatted", formatted);

        res.put("totalAmount", fundAmountDTO.totalAmount);
        formatted = BangladeshiNumberFormatter.getFormattedNumber(
                StringUtils.convertBanglaIfLanguageIsBangla(language, String.valueOf(fundAmountDTO.totalAmount))
        );
        res.put("totalAmountFormatted", formatted);
        return res;
    }

    private Mp_fund_applicationDTO addFundApplicationDTO(HttpServletRequest request, UserDTO userDTO,
                                                         long applicationTotalAmount) throws Exception {
        boolean isEnglish = userDTO.languageID == SessionConstants.ENGLISH;
        long currentTime = System.currentTimeMillis();

        Mp_fund_applicationDTO mpFundApplicationDTO = new Mp_fund_applicationDTO();
        mpFundApplicationDTO.insertedBy = userDTO.ID;
        mpFundApplicationDTO.insertionTime = currentTime;
        mpFundApplicationDTO.modifiedBy = userDTO.ID;
        mpFundApplicationDTO.lastModificationTime = currentTime;

        mpFundApplicationDTO.electionDetailsId = Election_detailsRepository.getInstance().getRunningElectionDetailsDTO().iD;
        mpFundApplicationDTO.electionConstituencyId = Long.parseLong(Jsoup.clean(
                request.getParameter("electionConstituencyId"), Whitelist.simpleText())
        );
        mpFundApplicationDTO.economicYearStart = BudgetUtils.getEconomicYearBeginYear(System.currentTimeMillis());

        Mp_fund_amountDTO mpFundAmountDTO = Mp_fund_amountDAO.getInstance().getOrCreateDtoOfMp(
                mpFundApplicationDTO.electionDetailsId,
                mpFundApplicationDTO.electionConstituencyId,
                mpFundApplicationDTO.economicYearStart,
                userDTO.ID, System.currentTimeMillis()
        );
        if (mpFundAmountDTO == null) {
            throw new Exception(
                    isEnglish ? "No allocation was found for this constituency!"
                              : "এই নির্বাচনী এলাকার জন্য কোন বরাদ্দ পাওয়া যায়নি!"
            );
        }
        mpFundApplicationDTO.mpFundAmountId = mpFundAmountDTO.iD;
        if (applicationTotalAmount > mpFundAmountDTO.amountLeft) {
            throw new Exception(
                    isEnglish ? "Not enough fund to allocate for this bill!"
                              : "এই আবেদনের মোট বরাদ্দ দেবার মত ফান্ড নেই!"
            );
        }
        mpFundApplicationDTO.applicationTotalAmount = applicationTotalAmount;

        Employee_recordsDTO employeeRecordsDTO = Election_wise_mpRepository.getInstance().getMpRecordsDTOByElectionAndConstituency(
                mpFundApplicationDTO.electionDetailsId, mpFundApplicationDTO.electionConstituencyId
        );
        if (employeeRecordsDTO == null) {
            throw new Exception(
                    isEnglish ? "Honorable Minister's information is not found!"
                              : "মাননীয় মন্ত্রী মহোদয়ের তথ্য পাওয়া যায়নি!"
            );
        }
        mpFundApplicationDTO.mpNameEn = employeeRecordsDTO.nameEng;
        mpFundApplicationDTO.mpNameBn = employeeRecordsDTO.nameBng;

        Mp_fund_applicationDAO.getInstance().add(mpFundApplicationDTO);
        return mpFundApplicationDTO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Mp_fund_recipientDTO[] fundRecipientDTOs = new Gson().fromJson(
                Jsoup.clean(request.getParameter("mpFundRecipients"), Whitelist.simpleText()),
                Mp_fund_recipientDTO[].class
        );
        boolean isEnglish = userDTO.languageID == SessionConstants.ENGLISH;
        if (fundRecipientDTOs.length == 0) {
            throw new Exception(
                    isEnglish ? "Add at least one in allocations list."
                              : "অন্তত একজনকে বরাদ্দ তালিকায় যুক্ত করুন।"
            );
        }

        long applicationTotalAmount = Arrays.stream(fundRecipientDTOs)
                                            .mapToLong(fundRecipientDTO -> fundRecipientDTO.allocatedAmount)
                                            .sum();
        Mp_fund_applicationDTO fundApplicationDTO = addFundApplicationDTO(request, userDTO, applicationTotalAmount);

        for (Mp_fund_recipientDTO fundRecipientDTO : fundRecipientDTOs) {
            fundRecipientDTO.mpFundApplicationId = fundApplicationDTO.iD;
            Mp_fund_recipientDAO.getInstance().add(fundRecipientDTO);
        }
        try {
            Mp_fund_amountDAO.getInstance().allocateFund(
                    fundApplicationDTO.mpFundAmountId, applicationTotalAmount,
                    userDTO.ID, System.currentTimeMillis()
            );
        } catch (Exception ex) {
            Mp_fund_applicationDAO.getInstance().delete(userDTO.ID, fundApplicationDTO.iD);
            logger.error(ex);
            throw ex;
        }
        return fundApplicationDTO;
    }

    @Override
    public String getTableName() {
        return Mp_fund_applicationDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Mp_fund_applicationServlet";
    }

    @Override
    public Mp_fund_applicationDAO getCommonDAOService() {
        return Mp_fund_applicationDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.MP_FUND_APPLICATION_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.MP_FUND_APPLICATION_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.MP_FUND_APPLICATION_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Mp_fund_applicationServlet.class;
    }
}