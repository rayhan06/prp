package mp_fund_application;

import util.CommonDTO;


public class Mp_fund_applicationDTO extends CommonDTO {
    public long mpFundAmountId = -1;
    public int economicYearStart = -1;
    public long electionDetailsId = -1;
    public long electionConstituencyId = -1;
    public String mpNameEn = "";
    public String mpNameBn = "";
    public long applicationTotalAmount = -1;
    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;

    @Override
    public String toString() {
        return "$Mp_fund_applicationDTO[" +
                " iD = " + iD +
                " mpFundAmountId = " + mpFundAmountId +
                " economicYearStart = " + economicYearStart +
                " electionDetailsId = " + electionDetailsId +
                " electionConstituencyId = " + electionConstituencyId +
                " mpNameEn = " + mpNameEn +
                " mpNameBn = " + mpNameBn +
                " applicationTotalAmount = " + applicationTotalAmount +
                " searchColumn = " + searchColumn +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " isDeleted = " + isDeleted +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }
}