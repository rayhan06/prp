package mp_fund_application;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Mp_fund_applicationDAO implements CommonDAOService<Mp_fund_applicationDTO> {
    private static final Logger logger = Logger.getLogger(Mp_fund_applicationDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (mp_fund_amount_id,economic_year_start,election_details_id,election_constituency_id,"
                    .concat("mp_name_en,mp_name_bn,application_total_amount,search_column,modified_by,lastModificationTime,")
                    .concat("inserted_by,insertion_time,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery =
            "UPDATE {tableName} SET mp_fund_amount_id=?,economic_year_start=?,election_details_id=?,election_constituency_id=?,"
                    .concat("mp_name_en=?,mp_name_bn=?,application_total_amount=?,search_column=?,modified_by=?,lastModificationTime=? ")
                    .concat("WHERE ID=?");

    private final Map<String, String> searchMap = new HashMap<>();

    private Mp_fund_applicationDAO() {
        searchMap.put("economic_year_start", " and (economic_year_start = ?)");
        searchMap.put("election_details_id", " and (election_details_id = ?)");
        searchMap.put("election_constituency_id", " and (election_constituency_id = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private static class LazyLoader {
        static final Mp_fund_applicationDAO INSTANCE = new Mp_fund_applicationDAO();
    }

    public static Mp_fund_applicationDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Mp_fund_applicationDTO dto) {
        dto.searchColumn = dto.mpNameEn + " " + dto.mpNameBn;
    }

    @Override
    public void set(PreparedStatement ps, Mp_fund_applicationDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(dto);
        ps.setLong(++index, dto.mpFundAmountId);
        ps.setInt(++index, dto.economicYearStart);
        ps.setLong(++index, dto.electionDetailsId);
        ps.setLong(++index, dto.electionConstituencyId);
        ps.setString(++index, dto.mpNameEn);
        ps.setString(++index, dto.mpNameBn);
        ps.setLong(++index, dto.applicationTotalAmount);
        ps.setString(++index, dto.searchColumn);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Mp_fund_applicationDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Mp_fund_applicationDTO dto = new Mp_fund_applicationDTO();
            dto.iD = rs.getLong("ID");
            dto.mpFundAmountId = rs.getLong("mp_fund_amount_id");
            dto.economicYearStart = rs.getInt("economic_year_start");
            dto.electionDetailsId = rs.getLong("election_details_id");
            dto.electionConstituencyId = rs.getLong("election_constituency_id");
            dto.mpNameEn = rs.getString("mp_name_en");
            dto.mpNameBn = rs.getString("mp_name_bn");
            dto.applicationTotalAmount = rs.getLong("application_total_amount");
            dto.searchColumn = rs.getString("search_column");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Mp_fund_applicationDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "mp_fund_application";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Mp_fund_applicationDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Mp_fund_applicationDTO) commonDTO, updateQuery, false);
    }
}
	