package user_organogram;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class User_organogramRepository implements Repository {
	User_organogramDAO user_organogramDAO = null;
	
	public void setDAO(User_organogramDAO user_organogramDAO)
	{
		this.user_organogramDAO = user_organogramDAO;
	}
	
	
	static Logger logger = Logger.getLogger(User_organogramRepository.class);
	Map<Long, User_organogramDTO>mapOfUser_organogramDTOToiD;
	Map<Long, Set<User_organogramDTO> >mapOfUser_organogramDTOTouserId;
	Map<Long, Set<User_organogramDTO> >mapOfUser_organogramDTOToorganogramId;
	Map<Integer, Set<User_organogramDTO> >mapOfUser_organogramDTOTostatus;
	Map<Long, Set<User_organogramDTO> >mapOfUser_organogramDTOTolastModificationTime;


	static User_organogramRepository instance = null;  
	private User_organogramRepository(){
		mapOfUser_organogramDTOToiD = new ConcurrentHashMap<>();
		mapOfUser_organogramDTOTouserId = new ConcurrentHashMap<>();
		mapOfUser_organogramDTOToorganogramId = new ConcurrentHashMap<>();
		mapOfUser_organogramDTOTostatus = new ConcurrentHashMap<>();
		mapOfUser_organogramDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static User_organogramRepository getInstance(){
		if (instance == null){
			instance = new User_organogramRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(user_organogramDAO == null)
		{
			return;
		}
		try {
			List<User_organogramDTO> user_organogramDTOs = user_organogramDAO.getAllUser_organogram(reloadAll);
			for(User_organogramDTO user_organogramDTO : user_organogramDTOs) {
				User_organogramDTO oldUser_organogramDTO = getUser_organogramDTOByID(user_organogramDTO.iD);
				if( oldUser_organogramDTO != null ) {
					mapOfUser_organogramDTOToiD.remove(oldUser_organogramDTO.iD);
				
					if(mapOfUser_organogramDTOTouserId.containsKey(oldUser_organogramDTO.userId)) {
						mapOfUser_organogramDTOTouserId.get(oldUser_organogramDTO.userId).remove(oldUser_organogramDTO);
					}
					if(mapOfUser_organogramDTOTouserId.get(oldUser_organogramDTO.userId).isEmpty()) {
						mapOfUser_organogramDTOTouserId.remove(oldUser_organogramDTO.userId);
					}
					
					if(mapOfUser_organogramDTOToorganogramId.containsKey(oldUser_organogramDTO.organogramId)) {
						mapOfUser_organogramDTOToorganogramId.get(oldUser_organogramDTO.organogramId).remove(oldUser_organogramDTO);
					}
					if(mapOfUser_organogramDTOToorganogramId.get(oldUser_organogramDTO.organogramId).isEmpty()) {
						mapOfUser_organogramDTOToorganogramId.remove(oldUser_organogramDTO.organogramId);
					}
					
					if(mapOfUser_organogramDTOTostatus.containsKey(oldUser_organogramDTO.status)) {
						mapOfUser_organogramDTOTostatus.get(oldUser_organogramDTO.status).remove(oldUser_organogramDTO);
					}
					if(mapOfUser_organogramDTOTostatus.get(oldUser_organogramDTO.status).isEmpty()) {
						mapOfUser_organogramDTOTostatus.remove(oldUser_organogramDTO.status);
					}
					
					if(mapOfUser_organogramDTOTolastModificationTime.containsKey(oldUser_organogramDTO.lastModificationTime)) {
						mapOfUser_organogramDTOTolastModificationTime.get(oldUser_organogramDTO.lastModificationTime).remove(oldUser_organogramDTO);
					}
					if(mapOfUser_organogramDTOTolastModificationTime.get(oldUser_organogramDTO.lastModificationTime).isEmpty()) {
						mapOfUser_organogramDTOTolastModificationTime.remove(oldUser_organogramDTO.lastModificationTime);
					}
					
					
				}
				if(user_organogramDTO.isDeleted == 0) 
				{
					
					mapOfUser_organogramDTOToiD.put(user_organogramDTO.iD, user_organogramDTO);
				
					if( ! mapOfUser_organogramDTOTouserId.containsKey(user_organogramDTO.userId)) {
						mapOfUser_organogramDTOTouserId.put(user_organogramDTO.userId, new HashSet<>());
					}
					mapOfUser_organogramDTOTouserId.get(user_organogramDTO.userId).add(user_organogramDTO);
					
					if( ! mapOfUser_organogramDTOToorganogramId.containsKey(user_organogramDTO.organogramId)) {
						mapOfUser_organogramDTOToorganogramId.put(user_organogramDTO.organogramId, new HashSet<>());
					}
					mapOfUser_organogramDTOToorganogramId.get(user_organogramDTO.organogramId).add(user_organogramDTO);
					
					if( ! mapOfUser_organogramDTOTostatus.containsKey(user_organogramDTO.status)) {
						mapOfUser_organogramDTOTostatus.put(user_organogramDTO.status, new HashSet<>());
					}
					mapOfUser_organogramDTOTostatus.get(user_organogramDTO.status).add(user_organogramDTO);
					
					if( ! mapOfUser_organogramDTOTolastModificationTime.containsKey(user_organogramDTO.lastModificationTime)) {
						mapOfUser_organogramDTOTolastModificationTime.put(user_organogramDTO.lastModificationTime, new HashSet<>());
					}
					mapOfUser_organogramDTOTolastModificationTime.get(user_organogramDTO.lastModificationTime).add(user_organogramDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<User_organogramDTO> getUser_organogramList() {
		List <User_organogramDTO> user_organograms = new ArrayList<User_organogramDTO>(this.mapOfUser_organogramDTOToiD.values());
		return user_organograms;
	}
	
	
	public User_organogramDTO getUser_organogramDTOByID( long ID){
		return mapOfUser_organogramDTOToiD.get(ID);
	}
	
	
	public List<User_organogramDTO> getUser_organogramDTOByuser_id(long user_id) {
		return new ArrayList<>( mapOfUser_organogramDTOTouserId.getOrDefault(user_id,new HashSet<>()));
	}
	
	
	public List<User_organogramDTO> getUser_organogramDTOByorganogram_id(long organogram_id) {
		return new ArrayList<>( mapOfUser_organogramDTOToorganogramId.getOrDefault(organogram_id,new HashSet<>()));
	}
	
	
	public List<User_organogramDTO> getUser_organogramDTOBystatus(int status) {
		return new ArrayList<>( mapOfUser_organogramDTOTostatus.getOrDefault(status,new HashSet<>()));
	}
	
	
	public List<User_organogramDTO> getUser_organogramDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfUser_organogramDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "user_organogram";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


