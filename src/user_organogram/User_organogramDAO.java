package user_organogram;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import approval_module_map.*;

public class User_organogramDAO  extends NavigationService3
{
	
	Logger logger = Logger.getLogger(getClass());

	
	public User_organogramDAO(String tableName, String tempTableName, Approval_module_mapDTO approval_module_mapDTO)
	{
		super(tableName, tempTableName, approval_module_mapDTO);		
	}
	
	public User_organogramDAO()
	{
		super("user_organogram", null, null);		
	}
	
	
	
	public long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception
	{
		
		User_organogramDTO user_organogramDTO = (User_organogramDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			user_organogramDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "role_id";
			sql += ", ";
			sql += "user_type";
			sql += ", ";
			sql += "organogram_id";
			sql += ", ";
			sql += "designation";
			sql += ", ";
			sql += "languageID";
			sql += ", ";
			sql += "status";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
			if(tempTableDTO!=null)
			{
				sql += ", permanent_table_id, operation_type, approval_path_type, approval_order";
			}
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			if(tempTableDTO!=null)
			{
				sql += ", " + tempTableDTO.permanent_table_id + ", " + tempTableDTO.operation_type + ", " + tempTableDTO.approval_path_type + ", " + tempTableDTO.approval_order;
			}
			sql += ")";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,user_organogramDTO.iD);
			ps.setObject(index++,user_organogramDTO.roleID);
			ps.setObject(index++,user_organogramDTO.userType);
			ps.setObject(index++,user_organogramDTO.organogramId);
			ps.setObject(index++,user_organogramDTO.designation);
			ps.setObject(index++,user_organogramDTO.languageID);
			ps.setObject(index++,user_organogramDTO.status);			
			ps.setObject(index++,user_organogramDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime, tableName);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return user_organogramDTO.iD;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID, String tableName) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		User_organogramDTO user_organogramDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				user_organogramDTO = new User_organogramDTO();

				user_organogramDTO.iD = rs.getLong("ID");
				//user_organogramDTO.userId = rs.getLong("user_id");
				user_organogramDTO.organogramId = rs.getLong("organogram_id");
				user_organogramDTO.roleID = rs.getLong("role_id");
				user_organogramDTO.userType = rs.getLong("user_type");
				user_organogramDTO.designation = rs.getString("designation");
				user_organogramDTO.languageID = rs.getLong("languageID");
				
				user_organogramDTO.status = rs.getInt("status");
				user_organogramDTO.isDeleted = rs.getInt("isDeleted");
				user_organogramDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return user_organogramDTO;
	}
	
	public long update(CommonDTO commonDTO, String tableName) throws Exception
	{		
		User_organogramDTO user_organogramDTO = (User_organogramDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "role_id=?";
			sql += ", ";
			sql += "userType=?";
			sql += ", ";
			sql += "organogram_id=?";
			sql += ", ";
			sql += "designation=?";
			sql += ", ";
			sql += "languageID=?";
			sql += ", ";
			sql += "status=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + user_organogramDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			
			ps.setObject(index++,user_organogramDTO.roleID);
			ps.setObject(index++,user_organogramDTO.userType);
			ps.setObject(index++,user_organogramDTO.organogramId);
			ps.setObject(index++,user_organogramDTO.designation);
			ps.setObject(index++,user_organogramDTO.languageID);
			ps.setObject(index++,user_organogramDTO.status);			
			
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, lastModificationTime, tableName);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return user_organogramDTO.iD;
	}
	
	public List<User_organogramDTO> getDTOs(Collection recordIDs)
	{
		return getDTOs(recordIDs, tableName);
	}
	
	public List<User_organogramDTO> getDTOs(Collection recordIDs, String tableName){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		User_organogramDTO user_organogramDTO = null;
		List<User_organogramDTO> user_organogramDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return user_organogramDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				user_organogramDTO = new User_organogramDTO();
				user_organogramDTO.iD = rs.getLong("ID");
				user_organogramDTO.roleID = rs.getLong("role_id");
				user_organogramDTO.userType = rs.getLong("user_type");
				user_organogramDTO.organogramId = rs.getLong("organogram_id");
				user_organogramDTO.designation = rs.getString("designation");
				user_organogramDTO.languageID = rs.getLong("languageID");
				user_organogramDTO.status = rs.getInt("status");
				user_organogramDTO.isDeleted = rs.getInt("isDeleted");
				user_organogramDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + user_organogramDTO);
				
				user_organogramDTOList.add(user_organogramDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return user_organogramDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<User_organogramDTO> getAllUser_organogram (boolean isFirstReload)
    {
		List<User_organogramDTO> user_organogramDTOList = new ArrayList<>();

		String sql = "SELECT * FROM user_organogram";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by user_organogram.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				User_organogramDTO user_organogramDTO = new User_organogramDTO();
				user_organogramDTO = new User_organogramDTO();
				user_organogramDTO.iD = rs.getLong("ID");
				user_organogramDTO.roleID = rs.getLong("role_id");
				user_organogramDTO.userType = rs.getLong("user_type");
				user_organogramDTO.organogramId = rs.getLong("organogram_id");
				user_organogramDTO.designation = rs.getString("designation");
				user_organogramDTO.languageID = rs.getLong("languageID");
				user_organogramDTO.status = rs.getInt("status");
				user_organogramDTO.isDeleted = rs.getInt("isDeleted");
				user_organogramDTO.lastModificationTime = rs.getLong("lastModificationTime");				
				user_organogramDTOList.add(user_organogramDTO);
				
				
//				User_organogramDTO user_organogramDTO = new User_organogramDTO();
//				user_organogramDTO.iD = rs.getLong("ID");
//				user_organogramDTO.userId = rs.getLong("user_id");
//				user_organogramDTO.organogramId = rs.getLong("organogram_id");
//				user_organogramDTO.status = rs.getInt("status");
//				user_organogramDTO.isDeleted = rs.getInt("isDeleted");
//				user_organogramDTO.lastModificationTime = rs.getLong("lastModificationTime");
//				
//				user_organogramDTOList.add(user_organogramDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return user_organogramDTOList;
    }
	
	public List<User_organogramDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset)
	{
		return getDTOs(p_searchCriteria, limit, offset, tableName, true, 0);
	}
	
	public List<User_organogramDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<User_organogramDTO> user_organogramDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, tableName, isPermanentTable, userApprovalPathType);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				User_organogramDTO user_organogramDTO = new User_organogramDTO();
				user_organogramDTO = new User_organogramDTO();
				user_organogramDTO.iD = rs.getLong("ID");
				user_organogramDTO.roleID = rs.getLong("role_id");
				user_organogramDTO.userType = rs.getLong("user_type");
				user_organogramDTO.organogramId = rs.getLong("organogram_id");
				user_organogramDTO.designation = rs.getString("designation");
				user_organogramDTO.languageID = rs.getLong("languageID");
				user_organogramDTO.status = rs.getInt("status");
				user_organogramDTO.isDeleted = rs.getInt("isDeleted");
				user_organogramDTO.lastModificationTime = rs.getLong("lastModificationTime");	
				
				user_organogramDTOList.add(user_organogramDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return user_organogramDTOList;
	
	}

		
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType)
    {
		User_organogramMAPS maps = new User_organogramMAPS(tableName);
		String joinSQL = "";
		return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, tableName, isPermanentTable, userApprovalPathType, maps, joinSQL);
    }			
}
	