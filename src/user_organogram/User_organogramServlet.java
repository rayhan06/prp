package user_organogram;

import java.io.IOException;
import java.io.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;
import treeView.TreeDTO;
import treeView.TreeFactory;
import user.UserDTO;
import user.UserRepository;

import util.RecordNavigationManager3;
import util.ServletConstant;

import java.util.*;
import javax.servlet.http.*;

import user_organogram.Constants;
import approval_module_map.*;


import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

/**
 * Servlet implementation class User_organogramServlet
 */
@WebServlet("/User_organogramServlet")
@MultipartConfig
public class User_organogramServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(User_organogramServlet.class);
	Approval_module_mapDAO approval_module_mapDAO = new Approval_module_mapDAO();
    Approval_module_mapDTO approval_module_mapDTO;
    String tableName = "user_organogram";
    String tempTableName = "user_organogram_temp";
	User_organogramDAO user_organogramDAO;
    private final Gson gson = new Gson();
    
    public String actionType = "";
	public String servletType = "User_organogramServlet";
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public User_organogramServlet() 
	{
        super();
    	try
    	{
			approval_module_mapDTO = approval_module_mapDAO.getApproval_module_mapDTOByTableName("user_organogram");
			user_organogramDAO = new User_organogramDAO(tableName, tempTableName, approval_module_mapDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		
		UserDTO userDTO = null;
		
		if(loginDTO.isOisf == 1)
		{
			 userDTO = UserRepository.getUserDTOByOrganogramID(loginDTO.userID);
		}
		else
		{
			 userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		}
		
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			 actionType = request.getParameter("actionType");
			
			TreeDTO treeDTO = new TreeDTO();
			
			if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ORGANOGRAM_SEARCH))
				{
					
					if(isPermanentTable)
					{
						searchUser_organogram(request, response, tableName, isPermanentTable);
					}
					else
					{
						searchUser_organogram(request, response, tempTableName, isPermanentTable);
					}
				}
				return;
			}
			
			else if(actionType.equals("getOffice"))
			{
				
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.MINISTRIES_TO_OFFICES_INCLUDING_ORIGINS);
				treeDTO.pageTitle = "Office Management";
				treeDTO.treeName = "OfficeDropDownChain";
				treeDTO.treeType = "select";
				treeDTO.treeBody = "officeOriginTreeBody.jsp";
				treeDTO.checkBoxType = 0;
				treeDTO.plusButtonType = 0;
			}
			else if (actionType.equals("assignPathToOrganogram")) {
				treeDTO.parentChildMap = new String[]{"office_ministries", "office_layers", "offices", "office_units", "office_unit_organograms"};
				treeDTO.showNameMap = new String[]{"Office Ministries", "Office Layers", "Offices", "Office Units", "Office Unit Organograms"};
				treeDTO.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_id", "office_unit_id"};
				treeDTO.englishNameColumn = new String[]{"name_eng", "layer_name_eng", "office_name_eng", "unit_name_eng", "designation_eng"};
				treeDTO.banglaNameColumn = new String[]{"name_bng", "layer_name_bng", "office_name_bng", "unit_name_bng", "designation_bng"};
				treeDTO.pageTitle = "Assign Approval Path";
				treeDTO.treeName = "approvalPathTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "OrganogramPathTreeBody.jsp";
				treeDTO.sameTableParentColumnName = new String[]{"", "", "", "parent_unit_id", ""}; //only for recursive tables
				treeDTO.checkBoxType = 1;
				treeDTO.plusButtonType = 0;
				
				request.setAttribute(ServletConstant.ROLE_LIST, PermissionRepository.getAllRoles());
			}
			else if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ORGANOGRAM_ADD))
				{
					getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				
				treeDTO.parentChildMap = new String[]{"office_ministries", "office_layers", "offices", "office_units", "office_unit_organograms"};
				treeDTO.showNameMap = new String[]{"Office Ministries", "Office Layers", "Offices", "Office Units", "Office Unit Organograms"};
				treeDTO.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_id", "office_unit_id"};
				treeDTO.englishNameColumn = new String[]{"name_eng", "layer_name_eng", "office_name_eng", "unit_name_eng", "designation_eng"};
				treeDTO.banglaNameColumn = new String[]{"name_bng", "layer_name_bng", "office_name_bng", "unit_name_bng", "designation_bng"};
				treeDTO.pageTitle = "Assign Approval Path";
				treeDTO.treeName = "approvalPathTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "OrganogramPathTreeBody.jsp";
				treeDTO.sameTableParentColumnName = new String[]{"", "", "", "parent_unit_id", ""}; //only for recursive tables
				treeDTO.checkBoxType = 1;
				treeDTO.plusButtonType = 0;
				
				
				
				String tempId = request.getParameter("ID");
				
				if(tempId != null)
				{
				
				long tempIdLong = Long.parseLong(tempId);
				
				request.setAttribute(ServletConstant.ROLE_LIST, PermissionRepository.getAllRoles());
				
				
				User_organogramDTO user_organogramDTO = (User_organogramDTO)user_organogramDAO.getDTOByID(tempIdLong, tableName);
				request.setAttribute("ID", user_organogramDTO.iD);
				request.setAttribute("user_organogramDTO",user_organogramDTO);
				request.setAttribute("user_organogramDAO",user_organogramDAO);
				
				}
				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ORGANOGRAM_UPDATE))
//				{
//					if(isPermanentTable)
//					{
//						getUser_organogram(request, response, tableName);
//					}
//					else
//					{
//						getUser_organogram(request, response, tempTableName);
//					}
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}
			
			else if(actionType.equals("getApprovalPage"))
			{
				System.out.println("getApprovalPage requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ORGANOGRAM_SEARCH))
				{
					searchUser_organogram(request, response, tempTableName, false);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			
			logger.debug("role :**************************** " + userDTO.roleID);
			
			//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TREE_VIEW))
				//if(true)
				{
					String myLayer = request.getParameter("myLayer");
					if(myLayer != null && !myLayer.equalsIgnoreCase(""))
					{
						int layer = Integer.parseInt(request.getParameter("myLayer"));
						if(treeDTO.treeType.equalsIgnoreCase("tree") && 
								layer < treeDTO.parentChildMap.length && 
								treeDTO.sameTableParentColumnName != null &&							
								(!treeDTO.sameTableParentColumnName[layer].equals("") || !treeDTO.sameTableParentColumnName[layer].equals("")))
						{
							getRecursiveNodes(request, response, layer, treeDTO);
						}
						else
						{
							getNormalNodes(request, response, layer, treeDTO);
						}
					}
					else
					{					
						getTopNodes(request, response, treeDTO);	
					}
				}
						
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getTopNodes(HttpServletRequest request, HttpServletResponse response, TreeDTO treeDTO) throws ServletException, IOException 
	{
		System.out.println("METHOD: getTopNodes");
		try
		{
			List<Integer> IDs = GenericTree.getTopIDs(treeDTO.parentChildMap[0]);
			setAttributes(-1, 0, IDs, request, treeDTO);
			String nodeJsp = "tree.jsp";		
			setParametersAndForward(0, nodeJsp, request, response, treeDTO);			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	private void getRecursiveNodes(HttpServletRequest request, HttpServletResponse response, int layer, TreeDTO treeDTO)
    {
		System.out.println("METHOD: getRecursiveNodes");
        int id = Integer.parseInt(request.getParameter("id"));
        int parentTableID;
       
        try
        {
            parentTableID = Integer.parseInt(request.getParameter("parentID"));

            int iPrevLayer = -1;
            String prevLayer = request.getParameter("prevLayer");
            if (prevLayer != null && !prevLayer.equalsIgnoreCase(""))
            {
                iPrevLayer = Integer.parseInt(prevLayer);
            }
            System.out.println("layer = " + layer + " id = " + id + " treeDTO.parentChildMap.length = " + treeDTO.parentChildMap.length + " iPrevLayer = " + iPrevLayer);
            if (layer != 0 &&
                    (treeDTO.sameTableParentColumnName != null &&
                            (!treeDTO.sameTableParentColumnName[layer].equals("") || !treeDTO.sameTableParentColumnName[layer].equals("")))
                    && layer != iPrevLayer)  //If a recursive node suddenly starts at midpoint
            {
                parentTableID = id;
                id = 0;
                System.out.println("AAAAAAAAAAAAAAA after reshuffling layer = " + layer + " id = " + id + " parentTableID = " + parentTableID);
            }

            if (layer < treeDTO.parentChildMap.length)
            {
                List<Integer> UnitIDs = GenericTree.getRecurviveIDs(treeDTO.parentChildMap[layer], treeDTO.sameTableParentColumnName[layer], id, treeDTO.parentIDColumn[layer], parentTableID);

                List<Integer> IDs = new ArrayList<Integer>();
                List<Integer> recursiveNodeTypes = new ArrayList<Integer>();

                setAttributes(id, layer, IDs, request, treeDTO);

                if (id != 0 && layer + 1 < treeDTO.parentChildMap.length)
                {
                    List<Integer> ChildIDs = GenericTree.getChildIDs(treeDTO.parentChildMap[layer + 1], treeDTO.parentIDColumn[layer + 1], id);
                    if(ChildIDs == null)
                    {
                        ChildIDs = new ArrayList<>();

                    }
                    ChildIDs.add(-1);

                    if (ChildIDs != null)
                    {
                        System.out.println("ooooooooooooooooooo ChildIDs = " + ChildIDs.size());
                        for (int i = 0; i < ChildIDs.size(); i++) {
                            IDs.add(ChildIDs.get(i));
                            recursiveNodeTypes.add(2);
                        }
                    }
                }

                for (int i = 0; i < UnitIDs.size(); i++)
                {
                    IDs.add(UnitIDs.get(i));
                    recursiveNodeTypes.add(1);
                }
                request.setAttribute("isRecursive", 1);
                request.setAttribute("recursiveNodeTypes", recursiveNodeTypes);
                request.setAttribute("childName", treeDTO.parentChildMap[layer + 1]);
                request.setAttribute("childEnglishNameColumn", treeDTO.englishNameColumn[layer + 1]);
                request.setAttribute("childBanglaNameColumn", treeDTO.banglaNameColumn[layer + 1]);

				String nodeJsp = "treeNode.jsp";
                System.out.println("SSSSSSSSSSSS Setting parentTableID = " + parentTableID);
                setParametersAndForward(parentTableID, nodeJsp, request, response, treeDTO);

            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
	
	private void getNormalNodes(HttpServletRequest request, HttpServletResponse response, int layer, TreeDTO treeDTO) throws ServletException, IOException 
	{
		
		System.out.println("METHOD: getNormalNodes");
		try
		{
			int id = Integer.parseInt(request.getParameter("id"));
			System.out.println("NNNNNNNNNNNNNN getNormalNodes layer = " + layer + " id = " + id);
			
			if(treeDTO.parentChildMap == null)
			{
				treeDTO.parentChildMap = new String[]{"office_ministries", "office_layers", "offices", "office_units", "office_unit_organograms"};
			}
			
			if(treeDTO.parentIDColumn == null)
			{
				treeDTO.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_id", "office_unit_id"};
				
			}
			
				
				
			if(layer < treeDTO.parentChildMap.length)
			{				
				List<Integer> IDs = GenericTree.getChildIDs(treeDTO.parentChildMap[layer], treeDTO.parentIDColumn[layer], id);
				setAttributes(id, layer, IDs, request, treeDTO);
				String nodeJsp = getNodeJSP(treeDTO);
				treeDTO.treeType = "tree";
				System.out.println("Going to geoNode, treeDTO.treeType = " + treeDTO.treeType);				
				setParametersAndForward(id, nodeJsp, request, response, treeDTO);				
			
			}
			else
			{
				System.out.println("Maximum Layer Reached");
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	private void setAttributes(int parentID, int layer, List<Integer> IDs, HttpServletRequest request, TreeDTO treeDTO)
	{

		request.setAttribute("nodeIDs", IDs);
		request.setAttribute("myLayer", layer);
		String request_layer_id_pairs = request.getParameter("layer_id_pairs");
		if(request_layer_id_pairs == null)
		{
			request_layer_id_pairs = "";
		}
		System.out.println("layer_id_pairs = " + request_layer_id_pairs);
		treeDTO.layer_id_pairs = request_layer_id_pairs;
		request.setAttribute("treeDTO", treeDTO);
		
	}
	
	private void setParametersAndForward(int parentID, String nodeJSP, HttpServletRequest request, HttpServletResponse response, TreeDTO treeDTO)
	{

		try 
		{
			String parentElementID = request.getParameter("parentElementID");
			String checkBoxChecked = request.getParameter("checkBoxChecked");
			request.getRequestDispatcher("user_organogram/" + nodeJSP 
					+ "?actionType=" + actionType 
					+ "&treeType=" + treeDTO.treeType 
					+ "&treeBody=" + treeDTO.treeBody 
					+ "&servletType=" + servletType
					+ "&pageTitle=" + treeDTO.pageTitle
					+ "&parentID=" + parentID
					+ "&checkBoxChecked=" + checkBoxChecked
					+ "&parentElementID=" + parentElementID
					+ treeDTO.additionalParams
					).forward(request, response);
		}
		catch (ServletException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	private String getNodeJSP(TreeDTO treeDTO)
	{
		if(treeDTO.treeType.equalsIgnoreCase("tree"))
		{
			return "treeNode.jsp";
		}
		else if(treeDTO.treeType.equalsIgnoreCase("select"))
		{
			return "dropDownNode.jsp";
			
		}
		else if(treeDTO.treeType.equalsIgnoreCase("flat"))
		{
			return "expandedTreeNode.jsp";
			
		}
		return "";
	}
	
	

	private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		request.setAttribute("ID", -1L);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("user_organogram/user_organogramEdit.jsp");
		requestDispatcher.forward(request, response);
	}
	private String getFileName(final Part part) 
	{
	    final String partHeader = part.getHeader("content-disposition");
	    System.out.println("Part Header = {0}" +  partHeader);
	    for (String content : part.getHeader("content-disposition").split(";")) {
	        if (content.trim().startsWith("filename")) {
	            return content.substring(
	                    content.indexOf('=') + 1).trim().replace("\"", "");
	        }
	    }
	    return null;
	}

	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ORGANOGRAM_ADD))
				{
					System.out.println("going to  addUser_organogram ");
					addUser_organogram(request, response, true, userDTO, tableName, true);
				}
				else
				{
					System.out.println("Not going to  addUser_organogram ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
//			if(actionType.equals("approve"))
//			{
//				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ORGANOGRAM_ADD))
//				{					
//					approveUser_organogram(request, response, true, userDTO);
//				}
//				else
//				{
//					System.out.println("Not going to  addUser_organogram ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//				
//			}
			if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ORGANOGRAM_ADD))
				{
					if(isPermanentTable)
					{
						getDTO(request, response, tableName);
					}
					else
					{
						getDTO(request, response, tempTableName);
					}
				}
				else
				{
					System.out.println("Not going to  addUser_organogram ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ORGANOGRAM_UPDATE))
				{
					if(isPermanentTable)
					{
						addUser_organogram(request, response, false, userDTO, tableName, isPermanentTable);
					}
					else
					{
						addUser_organogram(request, response, false, userDTO, tempTableName, isPermanentTable);
					}
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{								
				deleteUser_organogram(request, response, userDTO, isPermanentTable);				
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ORGANOGRAM_SEARCH))
				{
					searchUser_organogram(request, response, tableName, true);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response, String tableName) 
	{
		try 
		{
			System.out.println("In getDTO");
			User_organogramDTO user_organogramDTO = (User_organogramDTO)user_organogramDAO.getDTOByID(Long.parseLong(request.getParameter("ID")), tableName);
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(user_organogramDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	private void approveUser_organogram(HttpServletRequest request, HttpServletResponse response, boolean b, UserDTO userDTO) 
	{
		try
		{
			long id = Long.parseLong(request.getParameter("idToApprove"));
			User_organogramDTO user_organogramDTO = (User_organogramDTO)user_organogramDAO.getDTOByID(id, tempTableName);
			user_organogramDAO.manageWriteOperations(user_organogramDTO, SessionConstants.APPROVE, id, userDTO);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
	}
	private void addUser_organogram(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, String tableName, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			
			//String tempID = request.getParameter("id");
			
			String tempDesignation = request.getParameter("designation");
			
		//	logger.debug("%%%%*************************************  addUser_organogram " + tempID + " : "+ tempD);
			
			//System.out.println("%%%%*************************************  addUser_organogram " + tempID + " : "+ tempD);
			//
			
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addUser_organogram");
			String path = getServletContext().getRealPath("/img2/");
			User_organogramDTO user_organogramDTO;
			String FileNamePrefix;
			if(addFlag == true)
			{
				user_organogramDTO = new User_organogramDTO();
				FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
			}
			else
			{
				user_organogramDTO = (User_organogramDTO)user_organogramDAO.getDTOByID(Long.parseLong(request.getParameter("identity")), tableName);
				FileNamePrefix = request.getParameter("identity");
			}
			
			String Value = "";
			Value = request.getParameter("userType");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			//System.out.println("userId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				user_organogramDTO.userType = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("id");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			//System.out.println("organogramId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				user_organogramDTO.organogramId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			user_organogramDTO.status = 1;
//			Value = request.getParameter("status");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				user_organogramDTO.status = 1;
//			}			
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
			
			Value = request.getParameter("roleName");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			//System.out.println("status = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				user_organogramDTO.roleID = 1;
			}
			
			
			
			Value = request.getParameter("languageID");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			//System.out.println("status = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				user_organogramDTO.languageID = 1;
			}
			
			if(tempDesignation != null)
			{
			
			user_organogramDTO.designation = tempDesignation;
			}
			
			
			
			if(addFlag == true)
			{
				user_organogramDAO.manageWriteOperations(user_organogramDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				if(isPermanentTable)
				{
					user_organogramDAO.manageWriteOperations(user_organogramDTO, SessionConstants.UPDATE, -1, userDTO);
				}
				else
				{
					user_organogramDAO.manageWriteOperations(user_organogramDTO, SessionConstants.VALIDATE, -1, userDTO);
				}
				
			}
			
			
			
			
			
			
			
			
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			
			if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				getUser_organogram(request, response, tableName);
			}
			else
			{
				response.sendRedirect("User_organogramServlet?actionType=search");
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void deleteUser_organogram(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, boolean deleteOrReject) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				if(deleteOrReject)
				{
					User_organogramDTO user_organogramDTO = (User_organogramDTO)user_organogramDAO.getDTOByID(id);
					user_organogramDAO.manageWriteOperations(user_organogramDTO, SessionConstants.DELETE, id, userDTO);
					response.sendRedirect("User_organogramServlet?actionType=search");
				}
				else
				{
					User_organogramDTO user_organogramDTO = (User_organogramDTO)user_organogramDAO.getDTOByID(id, tempTableName);
					user_organogramDAO.manageWriteOperations(user_organogramDTO, SessionConstants.REJECT, id, userDTO);
					response.sendRedirect("User_organogramServlet?actionType=getApprovalPage");
				}
			}			
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		
	}

	private void getUser_organogram(HttpServletRequest request, HttpServletResponse response, String tableName) throws ServletException, IOException
	{
		System.out.println("in getUser_organogram");
		User_organogramDTO user_organogramDTO = null;
		try 
		{
			user_organogramDTO = (User_organogramDTO)user_organogramDAO.getDTOByID(Long.parseLong(request.getParameter("ID")), tableName);
			request.setAttribute("ID", user_organogramDTO.iD);
			request.setAttribute("user_organogramDTO",user_organogramDTO);
			request.setAttribute("user_organogramDAO",user_organogramDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "user_organogram/user_organogramInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
//			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
//			{
//				URL = "user_organogram/user_organogramSearchRow.jsp";
//				request.setAttribute("inplacesubmit","");					
//			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "user_organogram/OrganogramPathTreeBody.jsp?actionType=edit";
				}
				else
				{
					URL = "user_organogram/OrganogramPathTreeBody.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	private void searchUser_organogram(HttpServletRequest request, HttpServletResponse response, String tableName, boolean isPermanent) throws ServletException, IOException
	{
		System.out.println("in  searchUser_organogram 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager3 rnManager = new RecordNavigationManager3(
			SessionConstants.NAV_USER_ORGANOGRAM,
			request,
			user_organogramDAO,
			SessionConstants.VIEW_USER_ORGANOGRAM,
			SessionConstants.SEARCH_USER_ORGANOGRAM,
			tableName,
			isPermanent,
			userDTO.approvalPathID);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("user_organogramDAO",user_organogramDAO);
        RequestDispatcher rd;
        if(hasAjax == false)
        {
        	System.out.println("Going to user_organogram/user_organogramSearch.jsp");
        	rd = request.getRequestDispatcher("user_organogram/user_organogramSearch.jsp");
        }
        else
        {
        	System.out.println("Going to user_organogram/user_organogramSearchForm.jsp");
        	rd = request.getRequestDispatcher("user_organogram/user_organogramSearchForm.jsp");
        }
		rd.forward(request, response);
	}
	
}

