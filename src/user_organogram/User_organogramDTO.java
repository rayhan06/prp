package user_organogram;
import java.util.*; 
import util.*; 


public class User_organogramDTO extends CommonDTO
{

	public long userId = 0;
	public long organogramId = 0;
	public long userType = 0;
	public long roleID = 0;
	public String designation = "";
	public long languageID = 0;
	public int status = 0;
	
	
    @Override
	public String toString() {
            return "$User_organogramDTO[" +
            " iD = " + iD +
            " userId = " + userId +
            " organogramId = " + organogramId +
            " status = " + status +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}