package user_organogram;
import java.util.*; 
import util.*;


public class User_organogramMAPS extends CommonMaps
{	
	public User_organogramMAPS(String tableName)
	{
		
		java_allfield_type_map.put("user_id".toLowerCase(), "Long");
		java_allfield_type_map.put("organogram_id".toLowerCase(), "Long");
		java_allfield_type_map.put("status".toLowerCase(), "Integer");

		java_anyfield_search_map.put(tableName + ".user_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".organogram_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".status".toLowerCase(), "Integer");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("userId".toLowerCase(), "userId".toLowerCase());
		java_DTO_map.put("organogramId".toLowerCase(), "organogramId".toLowerCase());
		java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("user_id".toLowerCase(), "userId".toLowerCase());
		java_SQL_map.put("organogram_id".toLowerCase(), "organogramId".toLowerCase());
		java_SQL_map.put("status".toLowerCase(), "status".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("User Id".toLowerCase(), "userId".toLowerCase());
		java_Text_map.put("Organogram Id".toLowerCase(), "organogramId".toLowerCase());
		java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}