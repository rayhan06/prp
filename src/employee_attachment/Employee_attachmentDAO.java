package employee_attachment;

import common.ConnectionAndStatementUtil;
import common.EmployeeCommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unused")
public class Employee_attachmentDAO implements EmployeeCommonDAOService<Employee_attachmentDTO> {

    private final Logger logger = Logger.getLogger(Employee_attachmentDAO.class);
    private final String addQuery;
    private final String updateQuery;
    private final String[] columnNames;
    private static final String getCountByEmployeeIdAndOfficeAndStatus = "SELECT COUNT(*) FROM employee_attachment WHERE employee_records_id = %d " +
            " AND office_unit_id = %d AND status = 1 AND isDeleted =0";
    private static final String getByEmployeeIdQuery = "SELECT * FROM employee_attachment WHERE employee_records_id = %d AND status = 1 " +
            "AND isDeleted =0";

    private Employee_attachmentDAO() {
        columnNames = new String[]
                {
                        "employee_records_id",
                        "office_unit_id",
                        "status",
                        "start_date",
                        "end_date",
                        "insertion_date",
                        "inserted_by",
                        "modified_by",
                        "search_column",
                        "lastModificationTime",
                        "isDeleted",
                        "ID"
                };
        updateQuery = getUpdateQuery2(columnNames);
        addQuery = getAddQuery2(columnNames);

        searchMap.put("employee_record_id", " and (employee_records_id = ?)");
        searchMap.put("office_unit_id", " and (office_unit_id = ?)");
        searchMap.put("start_date_start", " and (start_date >= ?)");
        searchMap.put("start_date_end", " and (start_date <= ?)");
        searchMap.put("end_date_start", " and (end_date >= ?)");
        searchMap.put("end_date_end", " and (end_date <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Employee_attachmentDAO INSTANCE = new Employee_attachmentDAO();
    }

    public static Employee_attachmentDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Employee_attachmentDTO employee_attachmentDTO) {
        employee_attachmentDTO.searchColumn = "";
        employee_attachmentDTO.searchColumn += employee_attachmentDTO.employeeRecordsId + " ";
        employee_attachmentDTO.searchColumn += employee_attachmentDTO.officeUnitId + " ";
    }

    @Override
    public void set(PreparedStatement ps, Employee_attachmentDTO employee_attachmentDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(employee_attachmentDTO);
        ps.setObject(++index, employee_attachmentDTO.employeeRecordsId);
        ps.setObject(++index, employee_attachmentDTO.officeUnitId);
        ps.setObject(++index, employee_attachmentDTO.status);
        ps.setObject(++index, employee_attachmentDTO.startDate);
        ps.setObject(++index, employee_attachmentDTO.endDate);
        ps.setObject(++index, employee_attachmentDTO.insertionDate);
        ps.setObject(++index, employee_attachmentDTO.insertedBy);
        ps.setObject(++index, employee_attachmentDTO.modifiedBy);
        ps.setObject(++index, employee_attachmentDTO.searchColumn);
        ps.setObject(++index, employee_attachmentDTO.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, employee_attachmentDTO.isDeleted);
        }
        ps.setObject(++index, employee_attachmentDTO.iD);
    }

    @Override
    public Employee_attachmentDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Employee_attachmentDTO employee_attachmentDTO = new Employee_attachmentDTO();
            int i = 0;
            employee_attachmentDTO.employeeRecordsId = rs.getLong(columnNames[i++]);
            employee_attachmentDTO.officeUnitId = rs.getLong(columnNames[i++]);
            employee_attachmentDTO.status = rs.getInt(columnNames[i++]);
            employee_attachmentDTO.startDate = rs.getLong(columnNames[i++]);
            employee_attachmentDTO.endDate = rs.getLong(columnNames[i++]);
            employee_attachmentDTO.insertionDate = rs.getLong(columnNames[i++]);
            employee_attachmentDTO.insertedBy = rs.getLong(columnNames[i++]);
            employee_attachmentDTO.modifiedBy = rs.getLong(columnNames[i++]);
            employee_attachmentDTO.searchColumn = rs.getString(columnNames[i++]);
            employee_attachmentDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            employee_attachmentDTO.isDeleted = rs.getInt(columnNames[i++]);
            employee_attachmentDTO.iD = rs.getLong(columnNames[i]);
            return employee_attachmentDTO;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "employee_attachment";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_attachmentDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_attachmentDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

    public long getCountByEmployeeIdAndOfficeAndStatus(long employeeRecordId, long officeId) {
        String sql = String.format(getCountByEmployeeIdAndOfficeAndStatus, employeeRecordId, officeId);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getLong(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return 0L;
            }
        }, 0L);
    }


    public List<Employee_attachmentDTO> getByEmployeeIdAndStatus(long employeeRecordId) {
        String sql = String.format(getByEmployeeIdQuery, employeeRecordId);
        return getDTOs(sql);
    }
}