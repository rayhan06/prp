package employee_attachment;

import sessionmanager.SessionConstants;
import util.CommonEmployeeDTO;

public class Employee_attachmentDTO extends CommonEmployeeDTO {
    public long officeUnitId = 0;
    public int status = 1;
    public long startDate = SessionConstants.MIN_DATE;
    public long endDate = SessionConstants.MIN_DATE;
    public long insertionDate = -1;
    public long insertedBy = -1;
    public long modifiedBy = -1;

    @Override
    public String toString() {
        return "Employee_attachmentDTO{" +
                "officeUnitId=" + officeUnitId +
                ", status=" + status +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", insertionDate=" + insertionDate +
                ", insertedBy=" + insertedBy +
                ", modifiedBy=" + modifiedBy +
                ", employeeRecordsId=" + employeeRecordsId +
                '}';
    }
}