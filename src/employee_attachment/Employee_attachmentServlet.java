package employee_attachment;

import common.BaseServlet;
import common.RoleEnum;
import employee_records.Employee_recordsRepository;
import language.LC;
import language.LM;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import pb.ErrorMessage;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;

@WebServlet("/Employee_attachmentServlet")
@MultipartConfig
public class Employee_attachmentServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    private final Employee_attachmentDAO employeeAttachmentDAO = Employee_attachmentDAO.getInstance();

    @Override
    public String getTableName() {
        return employeeAttachmentDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Employee_attachmentServlet";
    }

    @Override
    public Employee_attachmentDAO getCommonDAOService() {
        return employeeAttachmentDAO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.EMPLOYEE_ATTACHMENT_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.EMPLOYEE_ATTACHMENT_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.EMPLOYEE_ATTACHMENT_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Employee_attachmentServlet.class;
    }

    @Override
    public String getDeleteRedirectURL(HttpServletRequest request) {
        int tab=3;
        String data="parliament_service_history";
        if (request.getParameter("tab") != null) {
            long empId = Long.parseLong(request.getParameter("empId"));
            String userId = request.getParameter("userId");
            UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
            if(userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId()){
                return "Employee_recordsServlet?actionType=viewMultiForm&data=" + data+"&tab="+tab+"&ID="+empId+"&userId="+userId;
            }else{
                return "Employee_recordsServlet?actionType=viewMyProfile&data=" + data+"&tab="+tab+"&ID="+empId+"&userId="+userId;
            }
        } else {
            return  getServletName()+"?actionType=search";
        }
    }

    @Override
    public boolean getEditPagePermission(HttpServletRequest request) {
        long id = getId(request);
        Employee_attachmentDTO employeeAttachmentDTO = Employee_attachmentDAO.getInstance().getDTOFromID(id);
        return employeeAttachmentDTO!=null && employeeAttachmentDTO.status == 1;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Employee_attachmentDTO employee_attachmentDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        long currentTime = System.currentTimeMillis();
        if (addFlag) {
            employee_attachmentDTO = new Employee_attachmentDTO();
            employee_attachmentDTO.insertionDate = currentTime;
            employee_attachmentDTO.insertedBy = userDTO.employee_record_id;
        } else {
            employee_attachmentDTO = Employee_attachmentDAO.getInstance().getDTOFromID(getId(request));
            if(employee_attachmentDTO == null){
                throw new Exception(isLangEng?"Employee attachment info is not found":"কর্মকর্তার সংযুক্তির তথ্য পাওয়া যায়নি");
            }
            if(employee_attachmentDTO.status!=1){
                throw new Exception(isLangEng?"Employee attachment info is not permissible to edit":"কর্মকর্তার সংযুক্তির তথ্য পরিবর্তনের ক্রা যাবে না");
            }
        }

        employee_attachmentDTO.lastModificationTime = currentTime;
        employee_attachmentDTO.modifiedBy = userDTO.employee_record_id;

        try {
            employee_attachmentDTO.employeeRecordsId = Long.parseLong(request.getParameter("employeeRecordId"));
            if (Employee_recordsRepository.getInstance().getById(employee_attachmentDTO.employeeRecordsId) == null) {
                throw new Exception("");
            }
        } catch (Exception ex) {
            throw new Exception(isLangEng ? "Employee info is not found" : "কর্মকর্তার অথ্য পাওয়া যায়নি");
        }
        Office_unitsDTO officeUnitsDTO;

        try {
            employee_attachmentDTO.officeUnitId = Long.parseLong(request.getParameter("officeUnitId"));
            officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employee_attachmentDTO.officeUnitId);
            if (officeUnitsDTO == null) {
                throw new Exception("");
            }
        } catch (Exception ex) {
            throw new Exception(isLangEng ? "Office info is not found" : "দপ্তরের অথ্য পাওয়া যায়নি");
        }

        if(addFlag){
            long count = employeeAttachmentDAO.getCountByEmployeeIdAndOfficeAndStatus(employee_attachmentDTO.employeeRecordsId,employee_attachmentDTO.officeUnitId);
            if(count > 0){
                throw new Exception(isLangEng?"Already assigned as attachment responsibility in "+officeUnitsDTO.unitNameEng
                        :"ইতিমধ্যে সংযুক্তি দায়িত্ব হিসেবে "+officeUnitsDTO.unitNameBng+" এ নিযুক্ত আছেন");
            }
        }

        try {
            employee_attachmentDTO.startDate = f.parse(request.getParameter("startDate")).getTime();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_STARTDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
        }

        boolean deleted = false;
        if (!addFlag) {
            String value = request.getParameter("deleted");
            if (value != null && !value.equalsIgnoreCase("")) {
                deleted = Boolean.parseBoolean(value);
                if (deleted) {
                    try {
                        employee_attachmentDTO.endDate = f.parse(request.getParameter("endDate")).getTime();
                    } catch (Exception e) {
                        e.printStackTrace();
                        throw new Exception(LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_ENDDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
                    }
                    employee_attachmentDTO.status = 2;
                }
            }
        }

        if (deleted) {
            if(employee_attachmentDTO.endDate<employee_attachmentDTO.startDate){
                throw new Exception(isLangEng?"End date should be greater or equal than start date":"শেষের তারিখ শুরুর তারিখের বড় অথবা সমান হবে");
            }
        }else{
            employee_attachmentDTO.endDate = SessionConstants.MIN_DATE;
            employee_attachmentDTO.status = 1;
        }

        if (addFlag) {
            Employee_attachmentDAO.getInstance().add(employee_attachmentDTO);
        } else {
            Employee_attachmentDAO.getInstance().update(employee_attachmentDTO);
        }
        return employee_attachmentDTO;
    }
}