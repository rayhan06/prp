package employee_attachment;

/*
 * @author Md. Erfan Hossain
 * @created 10/08/2021 - 11:55 PM
 * @project parliament
 */

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum EmployeeAttachmentStatusEnum {
    RUNNING(1,"RUNNING","চলমান","forestgreen"),
    COMPLETED(2,"COMPLETED","সমপন্ন","crimson");

    private final int value;
    private final String engText,bngText,color;

    private static Map<Integer,EmployeeAttachmentStatusEnum> mapByValue = null;

    EmployeeAttachmentStatusEnum(int value, String engText, String bngText, String color) {
        this.value = value;
        this.engText = engText;
        this.bngText = bngText;
        this.color = color;
    }

    public int getValue() {
        return value;
    }

    public String getEngText() {
        return engText;
    }

    public String getBngText() {
        return bngText;
    }

    public String getColor() {
        return color;
    }

    private synchronized static void reload(){
        if(mapByValue == null){
            mapByValue = Stream.of(EmployeeAttachmentStatusEnum.values())
                    .collect(Collectors.toMap(e->e.value,e->e));
        }
    }

    public static EmployeeAttachmentStatusEnum getByValue(int value){
        if(mapByValue == null){
            reload();
        }
        return mapByValue.get(value);
    }
}