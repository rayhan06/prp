package task_type_assign;

import common.BaseServlet;
import common.CommonDAOService;
import common.EmployeeServletService;
import common.RoleEnum;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.HttpRequestUtils;
import util.RecordNavigationManager4;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@WebServlet("/Task_type_assignServlet")
@MultipartConfig
public class Task_type_assignServlet extends BaseServlet implements EmployeeServletService {
    private static final long serialVersionUID = 1L;

    public static final Logger logger = Logger.getLogger(Task_type_assignServlet.class);

    private final Task_type_assignDAO task_type_assignDAO = Task_type_assignDAO.getInstance();

    @Override
    public String getTableName() {
        return task_type_assignDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Task_type_assignServlet";
    }

    @Override
    public Task_type_assignDAO getCommonDAOService() {
        return task_type_assignDAO;
    }

    @Override
    public String getAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return "Task_typeServlet?actionType=search";
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return "Task_typeServlet?actionType=search";
    }

    @Override
    public String getEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return "Task_typeServlet?actionType=search";
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return "Task_typeServlet?actionType=search";
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Task_type_assignDTO task_type_assignDTO;
        if (addFlag) {
            task_type_assignDTO = new Task_type_assignDTO();
        } else {
            task_type_assignDTO = Task_type_assignRepository.getInstance().getById(Long.parseLong(request.getParameter("iD")));
        }
        task_type_assignDTO.taskTypeID = Long.parseLong(request.getParameter("taskTypeID"));
        task_type_assignDTO.roleID = Stream.of(request.getParameter("roleID").trim().split(","))
                .map(String::trim)
                .map(Long::parseLong)
                .distinct()
                .map(String::valueOf)
                .collect(Collectors.joining(","));
        task_type_assignDTO.insertedBy = task_type_assignDTO.modifiedBy = userDTO.ID;
        task_type_assignDTO.insertionTime = task_type_assignDTO.lastModificationTime = System.currentTimeMillis();

        if (addFlag) {
            task_type_assignDAO.add(task_type_assignDTO);
        } else {
            task_type_assignDAO.update(task_type_assignDTO);
        }
        return task_type_assignDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.TASK_TYPE_ASSIGN_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.TASK_TYPE_ASSIGN_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.TASK_TYPE_ASSIGN_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Task_type_assignServlet.class;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        search(request);
        super.doGet(request,response);
    }
}