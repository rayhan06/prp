package task_type_assign;

import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import role.PermissionRepository;
import task_type.Task_typeDTO;
import task_type.Task_typeRepository;
import util.LockManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class Task_type_assignRepository implements Repository {
    private final Task_type_assignDAO task_type_assignDAO = Task_type_assignDAO.getInstance();
    private final Map<Long, Task_type_assignDTO> mapById;
    private final Map<Long, Task_type_assignDTO> mapByTaskTypeId;
    private final Map<Long, Set<Long>> mapByRoleId;

    private Task_type_assignRepository() {
        mapById = new ConcurrentHashMap<>();
        mapByTaskTypeId = new ConcurrentHashMap<>();
        mapByRoleId = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyTask_type_assignRepositoryLoader {
        static Task_type_assignRepository INSTANCE = new Task_type_assignRepository();
    }

    public static Task_type_assignRepository getInstance() {
        return Task_type_assignRepository.LazyTask_type_assignRepositoryLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        List<Task_type_assignDTO> task_type_assignDTOs = task_type_assignDAO.getAllDTOs(reloadAll);
        if (task_type_assignDTOs != null && task_type_assignDTOs.size() > 0) {
            task_type_assignDTOs.stream()
                    .peek(dto -> removeIfPresent(mapById.get(dto.iD)))
                    .filter(dto -> dto.isDeleted == 0)
                    .forEach(dto -> {
                        mapById.put(dto.iD, dto);
                        mapByTaskTypeId.put(dto.taskTypeID, dto);
                        Arrays.stream(dto.roleID.split(","))
                                .map(String::trim)
                                .filter(e->e.length()>0)
                                .map(Long::parseLong)
                                .forEach(roleId -> {
                                    Set<Long> setByRoleId = mapByRoleId.getOrDefault(roleId, new HashSet<>());
                                    setByRoleId.add(dto.taskTypeID);
                                    mapByRoleId.put(roleId, setByRoleId);
                                });
                    });
        }

    }

    private void removeIfPresent(Task_type_assignDTO oldDTO) {
        if (oldDTO == null) {
            return;
        }
        if (mapById.get(oldDTO.iD) != null) {
            mapById.remove(oldDTO.iD);
            mapByTaskTypeId.remove(oldDTO.taskTypeID);
            Arrays.stream(oldDTO.roleID.split(","))
                    .map(String::trim)
                    .map(Long::parseLong)
                    .forEach(roleId -> {
                        Set<Long> setByRoleId = mapByRoleId.getOrDefault(roleId, new HashSet<>());
                        setByRoleId.remove(oldDTO.iD);
                        mapByRoleId.put(roleId, setByRoleId);
                    });
        }

    }

    public void deleteDtoById(long id) {
        Task_type_assignDTO oldTask_type_assignDTO = mapById.get(id);
        if (oldTask_type_assignDTO == null) return;
        mapById.remove(oldTask_type_assignDTO.iD);
    }


    public Task_type_assignDTO getById(long id) {
        return mapById.get(id);
    }

    public Task_type_assignDTO getByTaskTypeId(long id){
        if(mapByTaskTypeId.get(id) == null){
            synchronized (LockManager.getLock("TTI"+id)){
                if(mapByTaskTypeId.get(id) == null){
                    Task_type_assignDTO dto = task_type_assignDAO.getDtosByTaskTypeId(id);
                    if(dto!=null){
                        mapByTaskTypeId.put(dto.taskTypeID,dto);
                    }
                }
            }
        }
        return mapByTaskTypeId.get(id);
    }

    public List<Long> getByRoleId(long roleId) {
        return new ArrayList<>(mapByRoleId.getOrDefault(roleId, new HashSet<>()));
    }

    public String getRoleNamesByTaskTypeId(long taskTypeId){
        Task_type_assignDTO dto = getByTaskTypeId(taskTypeId);
        if(dto == null || dto.roleID.isEmpty()) return "";

        return Arrays.stream(dto.roleID.split(","))
                .map(String::trim)
                .map(Long::parseLong)
                .map(PermissionRepository::getRoleDTOByRoleID)
                .map(roleDTO -> roleDTO.roleName)
                .collect(Collectors.joining("<br>"));
    }

    public String buildOptionByRoleId(long roleId, String language, long selectedId) {
        List<Long> taskTypeIds = getByRoleId(roleId);
        List<Task_typeDTO> task_typeDTOList = new ArrayList<>();
        for (Long taskTypeId : taskTypeIds) {
            task_typeDTOList.add(Task_typeRepository.getInstance().getDTOById(taskTypeId));
        }
        List<OptionDTO> optionDTOList = task_typeDTOList.stream()
                .map(dto -> new OptionDTO(dto.nameEn, dto.nameBn, String.valueOf(dto.iD)))
                .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, String.valueOf(selectedId));
    }

    @Override
    public String getTableName() {
        return task_type_assignDAO.getTableName();
    }
}