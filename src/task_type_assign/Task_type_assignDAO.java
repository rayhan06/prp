package task_type_assign;

import common.CommonDAOService;
import common.CommonDTOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.NavigationService4;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"unused","Duplicates"})
public class Task_type_assignDAO implements CommonDAOService<Task_type_assignDTO> {

    private static final Logger logger = Logger.getLogger(Task_type_assignDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (task_type_ID,role_ID, " +
            "modified_by, lastModificationTime, inserted_by, insertion_time, isDeleted, ID) " +
            "VALUES(?,?,?,?,?,?,?,?)";

    private static final String updateQuery = "UPDATE {tableName} SET task_type_ID = ?,role_ID = ?, " +
            "modified_by = ?, lastModificationTime = ?  WHERE ID = ?";

    private static final String getByTaskTypeId = "SELECT * FROM  task_type_assign WHERE task_type_ID=%d AND isDeleted=0";

    private static final String deleteByTaskTypeId = "UPDATE task_type_assign SET isDeleted = 1, lastModificationTime = %d, modified_by = %d WHERE task_type_ID IN (%s)";

    private final Map<String, String> searchMap = new HashMap<>();

    private static Task_type_assignDAO INSTANCE = null;

    public static Task_type_assignDAO getInstance(){
        if(INSTANCE == null){
            synchronized (Task_type_assignDAO.class){
                if(INSTANCE == null){
                    INSTANCE = new Task_type_assignDAO();
                }
            }
        }
        return INSTANCE;
    }

    private Task_type_assignDAO() {
        searchMap.put("task_type_ID", " and (task_type_ID = ?)");
        searchMap.put("role_ID", " and (role_ID = ?)");
    }

    public void setSearchColumn(Task_type_assignDTO task_type_assignDTO) {
        StringBuilder sb = new StringBuilder();
        if (task_type_assignDTO.taskTypeID != 1) {
            sb.append(task_type_assignDTO.taskTypeID).append(" ");
        }
        if (task_type_assignDTO.roleID != null || !task_type_assignDTO.roleID.equals("")) {
            sb.append(task_type_assignDTO.roleID).append(" ");
        }
        task_type_assignDTO.searchColumn = sb.toString();
    }

    public void set(PreparedStatement ps, Task_type_assignDTO task_type_assignDTO, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, task_type_assignDTO.taskTypeID);
        ps.setString(++index, task_type_assignDTO.roleID);
        ps.setLong(++index, task_type_assignDTO.modifiedBy);
        ps.setLong(++index, task_type_assignDTO.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, task_type_assignDTO.insertedBy);
            ps.setLong(++index, task_type_assignDTO.insertionTime);
            ps.setInt(++index, 0);
        }
        ps.setLong(++index, task_type_assignDTO.iD);
    }

    public Task_type_assignDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Task_type_assignDTO task_type_assignDTO = new Task_type_assignDTO();
            task_type_assignDTO.iD = rs.getLong("ID");
            task_type_assignDTO.taskTypeID = rs.getLong("task_type_ID");
            task_type_assignDTO.roleID = rs.getString("role_ID");
            task_type_assignDTO.insertedBy = rs.getLong("inserted_by");
            task_type_assignDTO.insertionTime = rs.getLong("insertion_time");
            task_type_assignDTO.isDeleted = rs.getInt("isDeleted");
            task_type_assignDTO.modifiedBy = rs.getLong("modified_by");
            task_type_assignDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return task_type_assignDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "task_type_assign";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Task_type_assignDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Task_type_assignDTO) commonDTO, updateQuery, false);
    }

    public Task_type_assignDTO getDtosByTaskTypeId(long taskTypeId) {
        return ConnectionAndStatementUtil.getT(String.format(getByTaskTypeId, taskTypeId),this::buildObjectFromResultSet);
    }

    public void deleteByTaskTypeId(List<Long> taskTypeIds,long modifiedBy){
        String ids = taskTypeIds.stream()
                .map(String::valueOf)
                .collect(Collectors.joining(","));
        long currentTime = System.currentTimeMillis();
        String sql = String.format(deleteByTaskTypeId,currentTime,modifiedBy,ids);
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model->{
            try {
                logger.debug(sql);
                model.getStatement().executeUpdate(sql);
                recordUpdateTime(model.getConnection(),getTableName(),currentTime);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
    }
}