package task_type_assign;

import util.CommonDTO;
import util.CommonEmployeeDTO;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;


public class Task_type_assignDTO extends CommonEmployeeDTO
{

	public long taskTypeID = -1;
	public String roleID = "";
	public long insertedBy = -1;
	public long insertionTime = -1;
	public long modifiedBy = -1;

	public boolean hasPermissionForApproval(long requesterRoleId){
		Set<Long> roleIds = Arrays.stream(roleID.split(","))
				.map(String::trim)
				.map(Long::parseLong)
				.collect(Collectors.toSet());
		return roleIds.contains(requesterRoleId);
	}
	
    @Override
	public String toString() {
            return "$Task_type_assignDTO[" +
            " iD = " + iD +
            " taskTypeID = " + taskTypeID +
            " roleID = " + roleID +
            " insertedBy = " + insertedBy +
            " insertionTime = " + insertionTime +
            " isDeleted = " + isDeleted +
            " modifiedBy = " + modifiedBy +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}