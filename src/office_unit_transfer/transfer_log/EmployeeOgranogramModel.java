package office_unit_transfer.transfer_log;

public class EmployeeOgranogramModel {
    private long office_unit_id;
    private long organogram_id;
    private String designation_bng;
    private String designation_eng;
    private long employee_record_id;
    private String name_bng;
    private String name_eng;

    public long getOffice_unit_id() {
        return office_unit_id;
    }

    public void setOffice_unit_id(long office_unit_id) {
        this.office_unit_id = office_unit_id;
    }

    public long getOrganogram_id() {
        return organogram_id;
    }

    public void setOrganogram_id(long organogram_id) {
        this.organogram_id = organogram_id;
    }

    public String getDesignation_bng() {
        return designation_bng;
    }

    public void setDesignation_bng(String designation_bng) {
        this.designation_bng = designation_bng;
    }

    public String getDesignation_eng() {
        return designation_eng;
    }

    public void setDesignation_eng(String designation_eng) {
        this.designation_eng = designation_eng;
    }

    public long getEmployee_record_id() {
        return employee_record_id;
    }

    public void setEmployee_record_id(long employee_record_id) {
        this.employee_record_id = employee_record_id;
    }

    public String getName_bng() {
        return name_bng;
    }

    public void setName_bng(String name_bng) {
        this.name_bng = name_bng;
    }

    public String getName_eng() {
        return name_eng;
    }

    public void setName_eng(String name_eng) {
        this.name_eng = name_eng;
    }
}
