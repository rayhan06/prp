package office_unit_transfer.transfer_log;

public class UnitModel {
    private long office_id;
    private String office_name_bng;
    private String office_name_eng;
    private long unit_id;
    private String unit_name_bng;
    private String unit_name_eng;

    public long getOffice_id() {
        return office_id;
    }

    public void setOffice_id(long office_id) {
        this.office_id = office_id;
    }

    public String getOffice_name_bng() {
        return office_name_bng;
    }

    public void setOffice_name_bng(String office_name_bng) {
        this.office_name_bng = office_name_bng;
    }

    public String getOffice_name_eng() {
        return office_name_eng;
    }

    public void setOffice_name_eng(String office_name_eng) {
        this.office_name_eng = office_name_eng;
    }

    public long getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(long unit_id) {
        this.unit_id = unit_id;
    }

    public String getUnit_name_bng() {
        return unit_name_bng;
    }

    public void setUnit_name_bng(String unit_name_bng) {
        this.unit_name_bng = unit_name_bng;
    }

    public String getUnit_name_eng() {
        return unit_name_eng;
    }

    public void setUnit_name_eng(String unit_name_eng) {
        this.unit_name_eng = unit_name_eng;
    }
}
