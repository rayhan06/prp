package office_unit_transfer.transfer_log;

import approval_module_map.Approval_module_mapDTO;
import office_transfer_logs.Office_transfer_logsDAO;
import office_transfer_logs.Office_transfer_logsDTO;
import startup.RunLater;
import test_lib.RQueryBuilder;

import java.util.ArrayList;

public class OfficeUnitTransferService implements RunLater {
    private String[] fromUnits;
    private String toUnit;
    private ArrayList<EmployeeOgranogramModel> employeeOgranogramForLog;
    private final boolean isSameOffice;

    private String[] employee_record;
    private String[] from_unit;
    private String[] from_designation;
    private String to_office;
    private String[] to_unit;
    private String[] to_designation;

    public OfficeUnitTransferService(String[] from, String to, ArrayList<EmployeeOgranogramModel> employeeOgranogramForLog) {
        this.fromUnits = from;
        this.toUnit = to;
        this.employeeOgranogramForLog = employeeOgranogramForLog;
        isSameOffice = true;
    }

    public OfficeUnitTransferService(String[] employee_record, String[] from_unit, String[] from_designation, String to_office, String[] to_unit, String[] to_designation) {
        this.employee_record = employee_record;
        this.from_unit = from_unit;
        this.from_designation = from_designation;
        this.to_office = to_office;
        this.to_unit = to_unit;
        this.to_designation = to_designation;
        isSameOffice = false;
    }

    @Override
    public void call() {
        this.createLog();
    }

    private void createLog() {
        if (isSameOffice) sameOfficeUnitTransferLog();
        else differentOfficeUnitTransferLog();
    }

    // region Same Office

    private void sameOfficeUnitTransferLog() {
        // From Units: fromUnits
        // To Unit: toUnit

        ArrayList<UnitModel> toUnitModelList = getUnitDetails(new String[]{toUnit});
        ArrayList<UnitModel> fromUnitModelList = getUnitDetails(fromUnits);

        UnitModel toUnit = toUnitModelList.get(0);
        Office_transfer_logsDTO office_transfer_logsDTO = new Office_transfer_logsDTO();
        Office_transfer_logsDAO office_transfer_logsDAO = new Office_transfer_logsDAO("office_transfer_logs", "office_transfer_logs", new Approval_module_mapDTO());

        for (EmployeeOgranogramModel employeeOrganogram : employeeOgranogramForLog) {
            for (UnitModel fromUnit : fromUnitModelList) {
                if (employeeOrganogram.getOffice_unit_id() == fromUnit.getUnit_id()) {

                    office_transfer_logsDTO.employeeRecordId = employeeOrganogram.getEmployee_record_id();
                    office_transfer_logsDTO.employeeNameBn = employeeOrganogram.getName_bng();
                    office_transfer_logsDTO.employeeNameEn = employeeOrganogram.getName_eng();

                    office_transfer_logsDTO.toOfficeId = toUnit.getOffice_id();
                    office_transfer_logsDTO.toOfficeNameEn = toUnit.getOffice_name_eng();
                    office_transfer_logsDTO.toOfficeNameBn = toUnit.getOffice_name_bng();
                    office_transfer_logsDTO.toUnitId = toUnit.getUnit_id();
                    office_transfer_logsDTO.toUnitNameEn = toUnit.getUnit_name_eng();
                    office_transfer_logsDTO.toUnitNameBn = toUnit.getUnit_name_bng();
                    office_transfer_logsDTO.toOrganogramId = employeeOrganogram.getOrganogram_id();
                    office_transfer_logsDTO.toOrganogramNameBn = employeeOrganogram.getDesignation_bng();
                    office_transfer_logsDTO.toOrganogramNameEn = employeeOrganogram.getDesignation_eng();

                    office_transfer_logsDTO.fromOfficeId = fromUnit.getOffice_id();
                    office_transfer_logsDTO.fromOfficeNameEn = fromUnit.getOffice_name_eng();
                    office_transfer_logsDTO.fromOfficeNameBn = fromUnit.getOffice_name_bng();
                    office_transfer_logsDTO.fromUnitId = fromUnit.getUnit_id();
                    office_transfer_logsDTO.fromUnitNameEn = fromUnit.getUnit_name_eng();
                    office_transfer_logsDTO.fromUnitNameBn = fromUnit.getUnit_name_bng();
                    office_transfer_logsDTO.fromOrganogramId = employeeOrganogram.getOrganogram_id();
                    office_transfer_logsDTO.fromOrganogramNameBn = employeeOrganogram.getDesignation_bng();
                    office_transfer_logsDTO.fromOrganogramNameEn = employeeOrganogram.getDesignation_eng();

                    try {
                        office_transfer_logsDAO.add(office_transfer_logsDTO, "office_transfer_logs", null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }
    }

    private ArrayList<UnitModel> getUnitDetails(String[] unitId) {
        String ids = String.join(",", unitId);

        String sql = String.format("select\n" +
                "offices.id as office_id,\n" +
                "offices.office_name_bng,\n" +
                "offices.office_name_eng,\n" +
                "office_units.id as unit_id,\n" +
                "office_units.unit_name_bng,\n" +
                "office_units.unit_name_eng\n" +
                "\n" +
                "from offices, office_units\n" +
                "\n" +
                "where offices.id = office_units.office_id\n" +
                "and office_units.id in (%s)", ids);

        RQueryBuilder<UnitModel> rQueryBuilder = new RQueryBuilder<>();
        return rQueryBuilder.setSql(sql).of(UnitModel.class).buildRaw();
    }

    // endregion Same Office

    // region Different

    private void differentOfficeUnitTransferLog() {
        //        this.employee_record[]
        //        this.from_unit[]
        //        this.from_designation[]

        //        this.to_office
        //        this.to_unit[]
        //        this.to_designation[]
        ArrayList<OfficeUnitTransferLogModel.EmployeeDesignation> previous = getPreviousUnit(employee_record, from_designation);
        ArrayList<OfficeUnitTransferLogModel.EmployeeDesignation> current = getCurrentUnit(to_designation);

        Office_transfer_logsDTO office_transfer_logsDTO = new Office_transfer_logsDTO();
        Office_transfer_logsDAO office_transfer_logsDAO = new Office_transfer_logsDAO("office_transfer_logs", "office_transfer_logs", new Approval_module_mapDTO());

        for (int i = 0; i < previous.size(); i++) {
            OfficeUnitTransferLogModel.EmployeeDesignation prev = previous.get(i);
            OfficeUnitTransferLogModel.EmployeeDesignation curr = current.get(i);

            office_transfer_logsDTO.employeeRecordId = prev.getEmployee_record_id();
            office_transfer_logsDTO.employeeNameBn = prev.getName_bng();
            office_transfer_logsDTO.employeeNameEn = prev.getName_eng();

            office_transfer_logsDTO.toOfficeId = curr.getOffice_id();
            office_transfer_logsDTO.toOfficeNameEn = curr.getOffice_name_eng();
            office_transfer_logsDTO.toOfficeNameBn = curr.getOffice_name_bng();
            office_transfer_logsDTO.toUnitId = curr.getUnit_id();
            office_transfer_logsDTO.toUnitNameEn = curr.getUnit_name_eng();
            office_transfer_logsDTO.toUnitNameBn = curr.getUnit_name_bng();
            office_transfer_logsDTO.toOrganogramId = curr.getOrganogram_id();
            office_transfer_logsDTO.toOrganogramNameBn = curr.getDesignation_bng();
            office_transfer_logsDTO.toOrganogramNameEn = curr.getDesignation_eng();

            office_transfer_logsDTO.fromOfficeId = prev.getOffice_id();
            office_transfer_logsDTO.fromOfficeNameEn = prev.getOffice_name_eng();
            office_transfer_logsDTO.fromOfficeNameBn = prev.getOffice_name_bng();
            office_transfer_logsDTO.fromUnitId = prev.getUnit_id();
            office_transfer_logsDTO.fromUnitNameEn = prev.getUnit_name_eng();
            office_transfer_logsDTO.fromUnitNameBn = prev.getUnit_name_bng();
            office_transfer_logsDTO.fromOrganogramId = prev.getOrganogram_id();
            office_transfer_logsDTO.fromOrganogramNameBn = prev.getDesignation_bng();
            office_transfer_logsDTO.fromOrganogramNameEn = prev.getDesignation_eng();

            try {
                office_transfer_logsDAO.add(office_transfer_logsDTO, "office_transfer_logs", null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // Get previous Office, Unit, Designation by designation ids
    // Some designation may not have employee
    private ArrayList<OfficeUnitTransferLogModel.EmployeeDesignation> getPreviousUnit(String[] employee_record_id, String[] organogram_id) {
        String organogramId = String.join(",", organogram_id);

        String sql0 = String.format("select\n" +
                "\toffices.id as office_id,\n" +
                "\toffices.office_name_eng,\n" +
                "\toffices.office_name_bng,\n" +
                "\toffice_units.id as unit_id,\n" +
                "\toffice_units.unit_name_bng,\n" +
                "\toffice_units.unit_name_eng,\n" +
                "\toffice_unit_organograms.id as organogram_id,\n" +
                "\toffice_unit_organograms.designation_eng,\n" +
                "\toffice_unit_organograms.designation_bng\n" +
                "\n" +
                "from offices, office_units, office_unit_organograms\n" +
                "\n" +
                "where offices.id = office_units.office_id\n" +
                "and office_units.id = office_unit_organograms.office_unit_id\n" +
                "and office_unit_organograms.id in(%s);", organogramId);

        RQueryBuilder<OfficeUnitTransferLogModel.EmployeeDesignation> r = new RQueryBuilder<>();
        ArrayList<OfficeUnitTransferLogModel.EmployeeDesignation> designation = r.setSql(sql0).of(OfficeUnitTransferLogModel.EmployeeDesignation.class).buildRaw();

        for (int i = 0; i < designation.size(); i++) {
            OfficeUnitTransferLogModel.EmployeeDesignation s = designation.get(i);

            String employeeId = employee_record_id[i];
            String sql = String.format("select\n" +
                    "id as employee_record_id,\n" +
                    "name_bng,\n" +
                    "name_bng\n" +
                    "\n" +
                    "from employee_records where id = %s and employee_records.isDeleted = 0;", employeeId);

            RQueryBuilder<OfficeUnitTransferLogModel.EmployeeDesignation> r0 = new RQueryBuilder<>();
            ArrayList<OfficeUnitTransferLogModel.EmployeeDesignation> employee = r0.setSql(sql).of(OfficeUnitTransferLogModel.EmployeeDesignation.class).buildRaw();

            if (employee != null) {
                s.setEmployee_record_id(employee.get(0).getEmployee_record_id());
                s.setName_bng(employee.get(0).getName_bng());
                s.setName_eng(employee.get(0).getName_eng());
            }
        }
        return designation;
    }

    // Get current Office, Unit, Designation by designation ids
    private ArrayList<OfficeUnitTransferLogModel.EmployeeDesignation> getCurrentUnit(String[] organogram_id) {
        String organogramId = String.join(",", organogram_id);

        String sql0 = String.format("select\n" +
                "\toffices.id as office_id,\n" +
                "\toffices.office_name_eng,\n" +
                "\toffices.office_name_bng,\n" +
                "\toffice_units.id as unit_id,\n" +
                "\toffice_units.unit_name_bng,\n" +
                "\toffice_units.unit_name_eng,\n" +
                "\toffice_unit_organograms.id as organogram_id,\n" +
                "\toffice_unit_organograms.designation_eng,\n" +
                "\toffice_unit_organograms.designation_bng\n" +
                "\n" +
                "from offices, office_units, office_unit_organograms\n" +
                "\n" +
                "where offices.id = office_units.office_id\n" +
                "and office_units.id = office_unit_organograms.office_unit_id\n" +
                "and office_unit_organograms.id in(%s);", organogramId);

        RQueryBuilder<OfficeUnitTransferLogModel.EmployeeDesignation> r = new RQueryBuilder<>();
        ArrayList<OfficeUnitTransferLogModel.EmployeeDesignation> designation = r.setSql(sql0).of(OfficeUnitTransferLogModel.EmployeeDesignation.class).buildRaw();

        return designation;
    }

    // endregion Different
}
