package office_unit_transfer;

import com.google.gson.Gson;
import dbm.DBMW;
import office_unit_transfer.transfer_log.EmployeeOgranogramModel;
import office_unit_transfer.transfer_log.OfficeUnitTransferService;
import org.apache.commons.lang3.text.StrBuilder;
import startup.RunLater;
import startup.SlowWorker;
import test_lib.RQueryBuilder;
import test_lib.RQueryExecutor;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class OfficeUnitTransferDAO {

    private final ArrayList<EmployeeOgranogramModel> employeeOgranogramForLog = new ArrayList<>();

    public OfficeUnitTransferDAO() {
    }

    public boolean transfer(String[] from, String to) {
        StrBuilder result = new StrBuilder();
        result.append("[");
        for (String s : from) {
            result.append(transferUnits(s, to)).append(",");
        }
        System.out.println(result);

        char[] res = result.toString().toCharArray();
        res[res.length - 1] = ']';
        //return (new Gson().toJson(String.valueOf(res)));
        SlowWorker.getInstance().invoke(new OfficeUnitTransferService(from, to, employeeOgranogramForLog));
        return true;
    }

    private String transferUnits(String fromUnitId, String toUnitId) {
        OfficeUnitTransferDTO.OfficeUnitModel fromUnitModel = getUnitModel(fromUnitId);
        OfficeUnitTransferDTO.OfficeUnitModel toUnitModel = getUnitModel(toUnitId);
        String updateOldUnit = this.setOfficeUnitStatusZero(fromUnitId);

        ArrayList<OfficeUnitTransferDTO.office_unit_organograms> oldUnitOrganogramList = getOrganograms(fromUnitId);
        String[] originId = getOriginList(oldUnitOrganogramList);
        String[] organogramId = getOrganogramIdList(oldUnitOrganogramList);
        saveOrganogramAndEmployee(organogramId);
        ArrayList<OfficeUnitTransferDTO.office_origin_unit_organograms> oldOriginUnitOrganogramList = getOriginOrganograms(originId);

        String updateOldOrganogram = setOfficeUnitOrganogramStatusZero(oldUnitOrganogramList);

        return transferOrganogram(fromUnitModel, toUnitModel, oldUnitOrganogramList, oldOriginUnitOrganogramList, updateOldUnit, updateOldOrganogram);
        //return null;
    }

    private String transferOrganogram(OfficeUnitTransferDTO.OfficeUnitModel fromUnitModel,
                                      OfficeUnitTransferDTO.OfficeUnitModel toUnitModel,
                                      ArrayList<OfficeUnitTransferDTO.office_unit_organograms> oldUnitOrganogramList,
                                      ArrayList<OfficeUnitTransferDTO.office_origin_unit_organograms> oldOriginUnitOrganogramList,
                                      String updateOldUnit,
                                      String updateOldOrganogram) {

        ArrayList<OfficeUnitTransferDTO.office_unit_organograms> organograms = new ArrayList<>();
        ArrayList<OfficeUnitTransferDTO.office_origin_unit_organograms> origins = new ArrayList<>();
        this.mapOrganogramAndOrigin(oldUnitOrganogramList, oldOriginUnitOrganogramList, organograms, origins);

        // region Create Origin Model

        for (int i = 0; i < origins.size(); i++) {
            OfficeUnitTransferDTO.office_origin_unit_organograms a = origins.get(i);
            if (a == null) continue;
            a.setOffice_origin_unit_id(Integer.parseInt(Long.toString(toUnitModel.getOffice_origin_unit_id())));
            a.setSuperior_unit_id(Integer.parseInt(Long.toString(toUnitModel.getParent_unit_id())));
        }
        this.topSortOrigin(origins);

        // endregion

        // region Create Organogram Model

        for (int i = 0; i < organograms.size(); i++) {
            OfficeUnitTransferDTO.office_origin_unit_organograms a = origins.get(i);
            OfficeUnitTransferDTO.office_unit_organograms b = organograms.get(i);
            if (a == null) {
                b.setRef_origin_unit_org_id(0);
                b.setRef_sup_origin_unit_desig_id(0);
            } else {
                b.setRef_origin_unit_org_id(a.getId());
                b.setRef_sup_origin_unit_desig_id(a.getSuperior_designation_id());
            }

            b.setOffice_id(toUnitModel.getOffice_id());
            b.setOffice_unit_id(Integer.parseInt(Long.toString(toUnitModel.getId())));
            b.setSuperior_unit_id(Integer.parseInt(Long.toString(toUnitModel.getParent_unit_id())));
            b.setRef_sup_origin_unit_id(getSuperiorOriginUnitId(toUnitModel.getOffice_origin_unit_id()));
        }
        this.topSortOrganogram(organograms);
        origins = removeDuplicateOrigin(origins);

        // endregion

        String sql = createOriginSql(origins) + createOrganogramSql(organograms) + updateOldUnit + updateOldOrganogram;
        RQueryExecutor rQueryExecutor = new RQueryExecutor();
        try {
            return rQueryExecutor.execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return ("{" + "\"success\"" + ":" + "false," + "\"msg\"" + ":" + e.getMessage() + "}");
        }
    }

    private void mapOrganogramAndOrigin(ArrayList<OfficeUnitTransferDTO.office_unit_organograms> oldUnitOrganogramList,
                                        ArrayList<OfficeUnitTransferDTO.office_origin_unit_organograms> oldOriginUnitOrganogramList,
                                        ArrayList<OfficeUnitTransferDTO.office_unit_organograms> organograms,
                                        ArrayList<OfficeUnitTransferDTO.office_origin_unit_organograms> origins) {

        for (OfficeUnitTransferDTO.office_unit_organograms a : oldUnitOrganogramList) {
            boolean hasOrigin = false;
            for (OfficeUnitTransferDTO.office_origin_unit_organograms b : oldOriginUnitOrganogramList) {
                if (a.getRef_origin_unit_org_id() == b.getId()) {
                    organograms.add(a);
                    origins.add(b);
                    hasOrigin = true;
                    break;
                }
            }
            if (!hasOrigin) {
                organograms.add(a);
                origins.add(null);
            }
        }
    }

    private ArrayList<OfficeUnitTransferDTO.office_origin_unit_organograms> removeDuplicateOrigin(ArrayList<OfficeUnitTransferDTO.office_origin_unit_organograms> origins) {
        ArrayList<OfficeUnitTransferDTO.office_origin_unit_organograms> newOrigin = new ArrayList<>();
        for (OfficeUnitTransferDTO.office_origin_unit_organograms a : origins) {
            boolean has = false;
            for (OfficeUnitTransferDTO.office_origin_unit_organograms b : newOrigin) {
                if (a.getId() == b.getId()) {
                    has = true;
                    break;
                }
            }
            if (!has) newOrigin.add(a);
        }
        return newOrigin;
    }

    private void topSortOrigin(ArrayList<OfficeUnitTransferDTO.office_origin_unit_organograms> origins) {
        boolean[] visit = new boolean[origins.size() + 10];
        Queue<OfficeUnitTransferDTO.office_origin_unit_organograms> queue = new LinkedList<>();
        for (int i = 0; i < origins.size(); i++) {
            OfficeUnitTransferDTO.office_origin_unit_organograms a = origins.get(i);
            if (a == null) continue;
            if (a.getSuperior_designation_id() == 0) {
                queue.add(a);
                visit[i] = true;
            }
        }

        while (!queue.isEmpty()) {
            OfficeUnitTransferDTO.office_origin_unit_organograms q = queue.remove();
            int newSuperiorDesignationId = Integer.parseInt(Long.toString(getOriginSequenceId()));

            for (int i = 0; i < origins.size(); i++) {
                OfficeUnitTransferDTO.office_origin_unit_organograms a = origins.get(i);
                if (a == null || visit[i]) continue;
                if (a.getSuperior_designation_id() == q.getId()) {
                    a.setSuperior_designation_id(newSuperiorDesignationId);
                    queue.add(a);
                    visit[i] = true;
                }
            }
            System.out.println("---------------------------------------- " + newSuperiorDesignationId + " ----------------------------------------- ");
            q.setId(newSuperiorDesignationId);
        }

        for (int i = 0; i < origins.size(); i++) {
            if (!visit[i]) {
                visit[i] = true;
                OfficeUnitTransferDTO.office_origin_unit_organograms a = origins.get(i);
                int newId = Integer.parseInt(Long.toString(getOriginSequenceId()));
                System.out.println("---------------------------------------- " + newId + " ----------------------------------------- ");
                a.setSuperior_designation_id(0);
                a.setId(newId);
            }
        }
    }

    private void topSortOrganogram(ArrayList<OfficeUnitTransferDTO.office_unit_organograms> organograms) {
        boolean[] visit = new boolean[organograms.size() + 10];
        Queue<OfficeUnitTransferDTO.office_unit_organograms> queue = new LinkedList<>();
        for (int i = 0; i < organograms.size(); i++) {
            OfficeUnitTransferDTO.office_unit_organograms a = organograms.get(i);
            if (a == null) continue;
            if (a.getSuperior_designation_id() == 0 || a.getSuperior_designation_id() == a.getId()) {
                queue.add(a);
                visit[i] = true;
            }
        }

        while (!queue.isEmpty()) {
            OfficeUnitTransferDTO.office_unit_organograms q = queue.remove();
            int newSuperiorDesignationId = Integer.parseInt(Long.toString(getOrganogramSequenceId()));

            for (int i = 0; i < organograms.size(); i++) {
                OfficeUnitTransferDTO.office_unit_organograms a = organograms.get(i);
                if (a == null || visit[i]) continue;
                if (a.getSuperior_designation_id() == q.getId()) {
                    a.setSuperior_designation_id(newSuperiorDesignationId);
                    queue.add(a);
                    visit[i] = true;
                }
            }
            q.setId(newSuperiorDesignationId);
        }

        for (int i = 0; i < organograms.size(); i++) {
            if (!visit[i]) {
                visit[i] = true;
                OfficeUnitTransferDTO.office_unit_organograms a = organograms.get(i);
                int newId = Integer.parseInt(Long.toString(getOrganogramSequenceId()));
                a.setSuperior_designation_id(0);
                a.setId(newId);
            }
        }
    }

    private long getOriginSequenceId() {
        try {
            return DBMW.getInstance().getNextSequenceId("office_origin_unit_organograms");
        } catch (Exception e) {
            e.printStackTrace();
            return 0L;
        }
    }

    private long getOrganogramSequenceId() {
        try {
            return DBMW.getInstance().getNextSequenceId("office_unit_organograms");
        } catch (Exception e) {
            e.printStackTrace();
            return 0L;
        }
    }

    private String createOriginSql(ArrayList<OfficeUnitTransferDTO.office_origin_unit_organograms> origins) {
        StringBuilder sql = new StringBuilder();
        RInsertSqlString<OfficeUnitTransferDTO.office_origin_unit_organograms> sqlString = new RInsertSqlString<>();
        for (OfficeUnitTransferDTO.office_origin_unit_organograms a : origins) {
            sql.append(sqlString.model(a).of(OfficeUnitTransferDTO.office_origin_unit_organograms.class).buildInsert());
        }
        return sql.toString();
    }

    private String createOrganogramSql(ArrayList<OfficeUnitTransferDTO.office_unit_organograms> organograms) {
        StringBuilder sql = new StringBuilder();
        RInsertSqlString<OfficeUnitTransferDTO.office_unit_organograms> sqlString = new RInsertSqlString<>();
        for (OfficeUnitTransferDTO.office_unit_organograms a : organograms) {
            sql.append(sqlString.model(a).of(OfficeUnitTransferDTO.office_unit_organograms.class).buildInsert());
        }
        return sql.toString();
    }

    // region Prior task

    private ArrayList<OfficeUnitTransferDTO.office_origin_unit_organograms> getOriginOrganograms(String[] originId) {
        String ids = String.join(",", originId);
        String sql = String.format("select * from office_origin_unit_organograms where id in (%s) and isDeleted = 0 and status = 1;", ids);
        RQueryBuilder<OfficeUnitTransferDTO.office_origin_unit_organograms> rQueryBuilder = new RQueryBuilder<>();
        ArrayList<OfficeUnitTransferDTO.office_origin_unit_organograms> organogramList = rQueryBuilder.setSql(sql).of(OfficeUnitTransferDTO.office_origin_unit_organograms.class).buildRaw();
        return organogramList;
    }

    private ArrayList<OfficeUnitTransferDTO.office_unit_organograms> getOrganograms(String unitId) {
        String sql = String.format("select * from office_unit_organograms where office_unit_id = %s and isDeleted = 0 and status = 1;", unitId);
        RQueryBuilder<OfficeUnitTransferDTO.office_unit_organograms> rQueryBuilder = new RQueryBuilder<>();
        ArrayList<OfficeUnitTransferDTO.office_unit_organograms> organogramList = rQueryBuilder.setSql(sql).of(OfficeUnitTransferDTO.office_unit_organograms.class).buildRaw();
        return organogramList;
    }

    private OfficeUnitTransferDTO.OfficeUnitModel getUnitModel(String unitId) {
        String sql = String.format("select * from office_units where id = %s and isDeleted = 0 and status = 1;", unitId);
        RQueryBuilder<OfficeUnitTransferDTO.OfficeUnitModel> rQueryBuilder = new RQueryBuilder<>();
        OfficeUnitTransferDTO.OfficeUnitModel unitModel = rQueryBuilder.setSql(sql).of(OfficeUnitTransferDTO.OfficeUnitModel.class).buildRaw().get(0);
        return unitModel;
    }

    private String setOfficeUnitStatusZero(String unitId) {
        String sql = String.format("update office_units set status = 0, active_status = 0 where id = %s and isDeleted = 0 and status = 1;", unitId);
        return sql;
    }

    private String setOfficeUnitOrganogramStatusZero(ArrayList<OfficeUnitTransferDTO.office_unit_organograms> oldUnitOrganogramList) {
        if (oldUnitOrganogramList != null) {
            String sql = ("");
            for (int i = 0; i < oldUnitOrganogramList.size(); i++) {
                sql += String.format("update office_unit_organograms set status = 0 where id = %s and isDeleted = 0 and status = 1;", oldUnitOrganogramList.get(i).getId());
            }
            return sql;
        }
        return null;
    }

    private int getSuperiorOriginUnitId(long originUnitId) {
        String sql = String.format("select parent_unit_id from office_origin_units where id = %d;", originUnitId);
        RQueryBuilder<OfficeUnitTransferDTO.ParentUnit> rQueryBuilder = new RQueryBuilder<>();
        OfficeUnitTransferDTO.ParentUnit parentUnit = rQueryBuilder.setSql(sql).of(OfficeUnitTransferDTO.ParentUnit.class).buildRaw().get(0);
        return parentUnit.getParent_unit_id();
    }

    private String[] getOriginList(ArrayList<OfficeUnitTransferDTO.office_unit_organograms> s) {
        String[] originId = new String[s.size()];
        for (int i = 0; i < s.size(); i++) {
            originId[i] = String.valueOf(s.get(i).getRef_origin_unit_org_id());
        }
        return originId;
    }

    private String[] getOrganogramIdList(ArrayList<OfficeUnitTransferDTO.office_unit_organograms> s) {
        String[] originId = new String[s.size()];
        for (int i = 0; i < s.size(); i++) {
            originId[i] = String.valueOf(s.get(i).getId());
        }
        return originId;
    }

    // endregion Prior task

    // region ForLog
    private void saveOrganogramAndEmployee(String[] organogramId) {
        String ids = String.join(",", organogramId);
        String sql = String.format("select\n" +
                "office_unit_organograms.office_unit_id,\n" +
                "office_unit_organograms.id as organogram_id,\n" +
                "office_unit_organograms.designation_bng,\n" +
                "office_unit_organograms.designation_eng,\n" +
                "employee_records.id as employee_record_id,\n" +
                "employee_records.name_bng,\n" +
                "employee_records.name_eng\n" +
                "\n" +
                "from office_unit_organograms, employee_offices, employee_records\n" +
                "\n" +
                "where office_unit_organograms.id = employee_offices.office_unit_organogram_id\n" +
                "and employee_offices.employee_record_id = employee_records.id\n" +
                "and office_unit_organograms.id in(%s)\n" +
                "and office_unit_organograms.isDeleted = 0 and office_unit_organograms.`status` = 1\n" +
                "and employee_offices.isDeleted = 0 and employee_offices.`status` = 1\n" +
                "and employee_records.isDeleted = 0", ids);

        RQueryBuilder<EmployeeOgranogramModel> rQueryBuilder = new RQueryBuilder<>();
        ArrayList<EmployeeOgranogramModel> organogramList = rQueryBuilder.setSql(sql).of(EmployeeOgranogramModel.class).buildRaw();

        employeeOgranogramForLog.addAll(organogramList == null ? new ArrayList<>() : organogramList);
    }
    // endregion
}
