package office_unit_transfer;

public class OfficeUnitTransferDTO {

    public static class OfficeUnitModel implements Cloneable {
        private long id;
        private long office_ministry_id;
        private long office_layer_id;
        private long office_id;
        private long office_origin_unit_id;
        private String unit_name_bng;
        private String unit_name_eng;
        private String office_unit_category;
        private long parent_unit_id;
        private int parent_origin_unit_id;
        private String unit_nothi_code;
        private int unit_level;
        private int sarok_no_start;
        private String email;
        private String phone;
        private String fax;
        private boolean active_status;
        private long created_by;
        private long modified_by;
        private long created;
        private long modified;
        private boolean status;
        private boolean isDeleted;
        private long lastModificationTime;

        public OfficeUnitModel() {
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public long getOffice_ministry_id() {
            return office_ministry_id;
        }

        public void setOffice_ministry_id(long office_ministry_id) {
            this.office_ministry_id = office_ministry_id;
        }

        public long getOffice_layer_id() {
            return office_layer_id;
        }

        public void setOffice_layer_id(long office_layer_id) {
            this.office_layer_id = office_layer_id;
        }

        public long getOffice_id() {
            return office_id;
        }

        public void setOffice_id(long office_id) {
            this.office_id = office_id;
        }

        public long getOffice_origin_unit_id() {
            return office_origin_unit_id;
        }

        public void setOffice_origin_unit_id(long office_origin_unit_id) {
            this.office_origin_unit_id = office_origin_unit_id;
        }

        public String getUnit_name_bng() {
            return unit_name_bng;
        }

        public void setUnit_name_bng(String unit_name_bng) {
            this.unit_name_bng = unit_name_bng;
        }

        public String getUnit_name_eng() {
            return unit_name_eng;
        }

        public void setUnit_name_eng(String unit_name_eng) {
            this.unit_name_eng = unit_name_eng;
        }

        public String getOffice_unit_category() {
            return office_unit_category;
        }

        public void setOffice_unit_category(String office_unit_category) {
            this.office_unit_category = office_unit_category;
        }

        public long getParent_unit_id() {
            return parent_unit_id;
        }

        public void setParent_unit_id(long parent_unit_id) {
            this.parent_unit_id = parent_unit_id;
        }

        public int getParent_origin_unit_id() {
            return parent_origin_unit_id;
        }

        public void setParent_origin_unit_id(int parent_origin_unit_id) {
            this.parent_origin_unit_id = parent_origin_unit_id;
        }

        public String getUnit_nothi_code() {
            return unit_nothi_code;
        }

        public void setUnit_nothi_code(String unit_nothi_code) {
            this.unit_nothi_code = unit_nothi_code;
        }

        public int getUnit_level() {
            return unit_level;
        }

        public void setUnit_level(int unit_level) {
            this.unit_level = unit_level;
        }

        public int getSarok_no_start() {
            return sarok_no_start;
        }

        public void setSarok_no_start(int sarok_no_start) {
            this.sarok_no_start = sarok_no_start;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getFax() {
            return fax;
        }

        public void setFax(String fax) {
            this.fax = fax;
        }

        public boolean getActive_status() {
            return active_status;
        }

        public void setActive_status(boolean active_status) {
            this.active_status = active_status;
        }

        public long getCreated_by() {
            return created_by;
        }

        public void setCreated_by(long created_by) {
            this.created_by = created_by;
        }

        public long getModified_by() {
            return modified_by;
        }

        public void setModified_by(long modified_by) {
            this.modified_by = modified_by;
        }

        public long getCreated() {
            return created;
        }

        public void setCreated(long created) {
            this.created = created;
        }

        public long getModified() {
            return modified;
        }

        public void setModified(long modified) {
            this.modified = modified;
        }

        public boolean getStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public boolean getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(boolean deleted) {
            isDeleted = deleted;
        }

        public long getLastModificationTime() {
            return lastModificationTime;
        }

        public void setLastModificationTime(long lastModificationTime) {
            this.lastModificationTime = lastModificationTime;
        }

        public Object clone() {
            try {
                return super.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static class office_unit_organograms implements Cloneable {
        private long id;
        private long office_id;
        private int office_unit_id;
        private int superior_unit_id;
        private int superior_designation_id;
        private int ref_origin_unit_org_id;
        private int ref_sup_origin_unit_desig_id;
        private int ref_sup_origin_unit_id;
        private String designation_eng;
        private String designation_bng;
        private String short_name_eng;
        private String short_name_bng;
        private int designation_level;
        private int designation_sequence;
        private String designation_description;
        private boolean status;
        private boolean is_admin;
        private long created_by;
        private long modified_by;
        private long created;
        private long modified;
        private String role_type;
        private boolean isDeleted;
        private long lastModificationTime;
        private int approval_path_type;

        public office_unit_organograms() {
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public long getOffice_id() {
            return office_id;
        }

        public void setOffice_id(long office_id) {
            this.office_id = office_id;
        }

        public int getOffice_unit_id() {
            return office_unit_id;
        }

        public void setOffice_unit_id(int office_unit_id) {
            this.office_unit_id = office_unit_id;
        }

        public int getSuperior_unit_id() {
            return superior_unit_id;
        }

        public void setSuperior_unit_id(int superior_unit_id) {
            this.superior_unit_id = superior_unit_id;
        }

        public int getSuperior_designation_id() {
            return superior_designation_id;
        }

        public void setSuperior_designation_id(int superior_designation_id) {
            this.superior_designation_id = superior_designation_id;
        }

        public int getRef_origin_unit_org_id() {
            return ref_origin_unit_org_id;
        }

        public void setRef_origin_unit_org_id(int ref_origin_unit_org_id) {
            this.ref_origin_unit_org_id = ref_origin_unit_org_id;
        }

        public int getRef_sup_origin_unit_desig_id() {
            return ref_sup_origin_unit_desig_id;
        }

        public void setRef_sup_origin_unit_desig_id(int ref_sup_origin_unit_desig_id) {
            this.ref_sup_origin_unit_desig_id = ref_sup_origin_unit_desig_id;
        }

        public int getRef_sup_origin_unit_id() {
            return ref_sup_origin_unit_id;
        }

        public void setRef_sup_origin_unit_id(int ref_sup_origin_unit_id) {
            this.ref_sup_origin_unit_id = ref_sup_origin_unit_id;
        }

        public String getDesignation_eng() {
            return designation_eng;
        }

        public void setDesignation_eng(String designation_eng) {
            this.designation_eng = designation_eng;
        }

        public String getDesignation_bng() {
            return designation_bng;
        }

        public void setDesignation_bng(String designation_bng) {
            this.designation_bng = designation_bng;
        }

        public String getShort_name_eng() {
            return short_name_eng;
        }

        public void setShort_name_eng(String short_name_eng) {
            this.short_name_eng = short_name_eng;
        }

        public String getShort_name_bng() {
            return short_name_bng;
        }

        public void setShort_name_bng(String short_name_bng) {
            this.short_name_bng = short_name_bng;
        }

        public int getDesignation_level() {
            return designation_level;
        }

        public void setDesignation_level(int designation_level) {
            this.designation_level = designation_level;
        }

        public int getDesignation_sequence() {
            return designation_sequence;
        }

        public void setDesignation_sequence(int designation_sequence) {
            this.designation_sequence = designation_sequence;
        }

        public String getDesignation_description() {
            return designation_description;
        }

        public void setDesignation_description(String designation_description) {
            this.designation_description = designation_description;
        }

        public boolean getStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public boolean getIs_admin() {
            return is_admin;
        }

        public void setIs_admin(boolean is_admin) {
            this.is_admin = is_admin;
        }

        public long getCreated_by() {
            return created_by;
        }

        public void setCreated_by(long created_by) {
            this.created_by = created_by;
        }

        public long getModified_by() {
            return modified_by;
        }

        public void setModified_by(long modified_by) {
            this.modified_by = modified_by;
        }

        public long getCreated() {
            return created;
        }

        public void setCreated(long created) {
            this.created = created;
        }

        public long getModified() {
            return modified;
        }

        public void setModified(long modified) {
            this.modified = modified;
        }

        public String getRole_type() {
            return role_type;
        }

        public void setRole_type(String role_type) {
            this.role_type = role_type;
        }

        public boolean getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(boolean deleted) {
            isDeleted = deleted;
        }

        public long getLastModificationTime() {
            return lastModificationTime;
        }

        public void setLastModificationTime(long lastModificationTime) {
            this.lastModificationTime = lastModificationTime;
        }

        public int getApproval_path_type() {
            return approval_path_type;
        }

        public void setApproval_path_type(int approval_path_type) {
            this.approval_path_type = approval_path_type;
        }

        public Object clone() {
            try {
                return super.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static class office_origin_unit_organograms implements Cloneable {
        private int id;
        private int office_origin_unit_id;
        private int superior_unit_id;
        private int superior_designation_id;
        private String designation_eng;
        private String designation_bng;
        private String short_name_eng;
        private String short_name_bng;
        private int designation_level;
        private int designation_sequence;
        private boolean status;
        private int created_by;
        private long modified_by;
        private long created;
        private long modified;
        private boolean isDeleted;
        private long lastModificationTime;

        public office_origin_unit_organograms() {
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getOffice_origin_unit_id() {
            return office_origin_unit_id;
        }

        public void setOffice_origin_unit_id(int office_origin_unit_id) {
            this.office_origin_unit_id = office_origin_unit_id;
        }

        public int getSuperior_unit_id() {
            return superior_unit_id;
        }

        public void setSuperior_unit_id(int superior_unit_id) {
            this.superior_unit_id = superior_unit_id;
        }

        public int getSuperior_designation_id() {
            return superior_designation_id;
        }

        public void setSuperior_designation_id(int superior_designation_id) {
            this.superior_designation_id = superior_designation_id;
        }

        public String getDesignation_eng() {
            return designation_eng;
        }

        public void setDesignation_eng(String designation_eng) {
            this.designation_eng = designation_eng;
        }

        public String getDesignation_bng() {
            return designation_bng;
        }

        public void setDesignation_bng(String designation_bng) {
            this.designation_bng = designation_bng;
        }

        public String getShort_name_eng() {
            return short_name_eng;
        }

        public void setShort_name_eng(String short_name_eng) {
            this.short_name_eng = short_name_eng;
        }

        public String getShort_name_bng() {
            return short_name_bng;
        }

        public void setShort_name_bng(String short_name_bng) {
            this.short_name_bng = short_name_bng;
        }

        public int getDesignation_level() {
            return designation_level;
        }

        public void setDesignation_level(int designation_level) {
            this.designation_level = designation_level;
        }

        public int getDesignation_sequence() {
            return designation_sequence;
        }

        public void setDesignation_sequence(int designation_sequence) {
            this.designation_sequence = designation_sequence;
        }

        public boolean getStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public int getCreated_by() {
            return created_by;
        }

        public void setCreated_by(int created_by) {
            this.created_by = created_by;
        }

        public long getModified_by() {
            return modified_by;
        }

        public void setModified_by(long modified_by) {
            this.modified_by = modified_by;
        }

        public long getCreated() {
            return created;
        }

        public void setCreated(long created) {
            this.created = created;
        }

        public long getModified() {
            return modified;
        }

        public void setModified(long modified) {
            this.modified = modified;
        }

        public boolean getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(boolean deleted) {
            isDeleted = deleted;
        }

        public long getLastModificationTime() {
            return lastModificationTime;
        }

        public void setLastModificationTime(long lastModificationTime) {
            this.lastModificationTime = lastModificationTime;
        }

        public Object clone() {
            try {
                return super.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static class ParentUnit {
        private int parent_unit_id;

        public int getParent_unit_id() {
            return parent_unit_id;
        }

        public void setParent_unit_id(int parent_unit_id) {
            this.parent_unit_id = parent_unit_id;
        }
    }

    public static class employee_offices {
        private long id;
        private long employee_record_id;
        private String identification_number;
        private int office_id;
        private long office_unit_id;
        private long office_unit_organogram_id;
        private String designation;
        private int designation_level;
        private int designation_sequence;
        private boolean is_default_role;
        private boolean office_head;
        private boolean summary_nothi_post_type;
        private String incharge_label;
        private int main_role_id;
        private long joining_date;
        private long last_office_date;
        private boolean status;
        private long status_change_date;
        private boolean show_unit;
        private long created;
        private long modified;
        private long created_by;
        private long modified_by;
        private boolean role_type;
        private long lastModificationTime;
        private boolean isDeleted;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public long getEmployee_record_id() {
            return employee_record_id;
        }

        public void setEmployee_record_id(long employee_record_id) {
            this.employee_record_id = employee_record_id;
        }

        public String getIdentification_number() {
            return identification_number;
        }

        public void setIdentification_number(String identification_number) {
            this.identification_number = identification_number;
        }

        public int getOffice_id() {
            return office_id;
        }

        public void setOffice_id(int office_id) {
            this.office_id = office_id;
        }

        public long getOffice_unit_id() {
            return office_unit_id;
        }

        public void setOffice_unit_id(long office_unit_id) {
            this.office_unit_id = office_unit_id;
        }

        public long getOffice_unit_organogram_id() {
            return office_unit_organogram_id;
        }

        public void setOffice_unit_organogram_id(long office_unit_organogram_id) {
            this.office_unit_organogram_id = office_unit_organogram_id;
        }

        public String getDesignation() {
            return designation;
        }

        public void setDesignation(String designation) {
            this.designation = designation;
        }

        public int getDesignation_level() {
            return designation_level;
        }

        public void setDesignation_level(int designation_level) {
            this.designation_level = designation_level;
        }

        public int getDesignation_sequence() {
            return designation_sequence;
        }

        public void setDesignation_sequence(int designation_sequence) {
            this.designation_sequence = designation_sequence;
        }

        public boolean getIs_default_role() {
            return is_default_role;
        }

        public void setIs_default_role(boolean is_default_role) {
            this.is_default_role = is_default_role;
        }

        public boolean getOffice_head() {
            return office_head;
        }

        public void setOffice_head(boolean office_head) {
            this.office_head = office_head;
        }

        public boolean getSummary_nothi_post_type() {
            return summary_nothi_post_type;
        }

        public void setSummary_nothi_post_type(boolean summary_nothi_post_type) {
            this.summary_nothi_post_type = summary_nothi_post_type;
        }

        public String getIncharge_label() {
            return incharge_label;
        }

        public void setIncharge_label(String incharge_label) {
            this.incharge_label = incharge_label;
        }

        public int getMain_role_id() {
            return main_role_id;
        }

        public void setMain_role_id(int main_role_id) {
            this.main_role_id = main_role_id;
        }

        public long getJoining_date() {
            return joining_date;
        }

        public void setJoining_date(long joining_date) {
            this.joining_date = joining_date;
        }

        public long getLast_office_date() {
            return last_office_date;
        }

        public void setLast_office_date(long last_office_date) {
            this.last_office_date = last_office_date;
        }

        public boolean getStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public long getStatus_change_date() {
            return status_change_date;
        }

        public void setStatus_change_date(long status_change_date) {
            this.status_change_date = status_change_date;
        }

        public boolean getShow_unit() {
            return show_unit;
        }

        public void setShow_unit(boolean show_unit) {
            this.show_unit = show_unit;
        }

        public long getCreated() {
            return created;
        }

        public void setCreated(long created) {
            this.created = created;
        }

        public long getModified() {
            return modified;
        }

        public void setModified(long modified) {
            this.modified = modified;
        }

        public long getCreated_by() {
            return created_by;
        }

        public void setCreated_by(long created_by) {
            this.created_by = created_by;
        }

        public long getModified_by() {
            return modified_by;
        }

        public void setModified_by(long modified_by) {
            this.modified_by = modified_by;
        }

        public boolean getRole_type() {
            return role_type;
        }

        public void setRole_type(boolean role_type) {
            this.role_type = role_type;
        }

        public long getLastModificationTime() {
            return lastModificationTime;
        }

        public void setLastModificationTime(long lastModificationTime) {
            this.lastModificationTime = lastModificationTime;
        }

        public boolean getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(boolean deleted) {
            isDeleted = deleted;
        }
    }
}
