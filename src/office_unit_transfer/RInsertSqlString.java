package office_unit_transfer;

import test_lib.ClassInfoGenerator;
import test_lib.Tupple;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class RInsertSqlString<T> {
    private Class<T> className;
    private T model;

    public RInsertSqlString<T> of(Class<T> className) {
        this.className = className;
        return this;
    }

    public RInsertSqlString<T> model(T model) {
        this.model = model;
        return this;
    }

    public RInsertSqlString() {
    }

    public String buildInsert() {

        ClassInfoGenerator<T> tClassInfoGenerator = new ClassInfoGenerator<>(this.className);
        ArrayList<Tupple> data = tClassInfoGenerator.getSetterMethods();

        StringBuilder sql = new StringBuilder("insert into " + this.className.getSimpleName() + " (");

        for (int i = 0; i < data.size(); i++) {
            Tupple obj = data.get(i);
            if (i == 0) {
                sql.append(obj.get(1).toString());
            } else {
                sql.append(", ").append(obj.get(1).toString());
            }
        }

        sql.append(") values (");
        for (int i = 0; i < data.size(); i++) {

            Tupple obj = data.get(i);

            Method setterMethod = (Method) obj.get(0);
            String variable = obj.get(1).toString();
            Type type = (Type) obj.get(2);
            Method getterMethod = (Method) obj.get(3);

            Object ob = invokeData(getterMethod, variable, type, this.model);
            String val = String.valueOf(ob);
            if (type == String.class) val = ("'") + ob + ("'");

            if (i == 0) sql.append(val);
            else sql.append(", ").append(val);
        }
        sql.append(");");

        return sql.toString();
    }

    private Object invokeData(Method getMethod, String variableName, Type dataType, T instance) {
        if (dataType == int.class) {
            try {
                return getMethod.invoke(instance);
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                return 0;
            }
        } else if (dataType == long.class) {
            try {
                return getMethod.invoke(instance);
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                return 0L;
            }
        } else if (dataType == boolean.class) {
            try {
                return getMethod.invoke(instance);
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                return false;
            }
        } else if (dataType == String.class) {
            try {
                return getMethod.invoke(instance);
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                return null;
            }
        }
        return true;
    }
}
