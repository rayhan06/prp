package office_unit_transfer.same_office;

import com.google.gson.Gson;
import dbm.DBMW;
import test_lib.RInsertQueryBuilder;
import test_lib.RQueryBuilder;

import java.util.ArrayList;
import java.util.StringJoiner;

public class OfficeUnitTransferSameOfficeDAO {

    public OfficeUnitTransferSameOfficeDAO() {
    }

    public String getOfficeUnit(String officeId) {
        String sql = String.format("select * from office_units where office_id = %s and status = 1 and isDeleted = 0", officeId);
        RQueryBuilder<OfficeUnitTransferSameOfficeModel.OfficeUnitModel> rQueryBuilder = new RQueryBuilder<>();
        String data = rQueryBuilder.setSql(sql).of(OfficeUnitTransferSameOfficeModel.OfficeUnitModel.class).buildJson();

        return data;
    }

    public String getOfficeUnitOrganogram(String[] ids) {
        StringBuilder unit_id = new StringBuilder();
        for (int i = 0; i < ids.length; i++) {
            if (i == 0) unit_id.append(ids[i]);
            else unit_id.append(", ").append(ids[i]);
        }

        String sql = String.format("select\n" +
                "\toffice_unit_organograms.id,\n" +
                "\toffice_unit_organograms.designation_eng,\n" +
                "\toffice_unit_organograms.designation_bng,\n" +
                "\toffice_units.unit_name_bng,\n" +
                "\toffice_units.unit_name_eng\n" +
                "\n" +
                "from office_units, office_unit_organograms\n" +
                "\n" +
                "where office_units.id = office_unit_organograms.office_unit_id\n" +
                "and office_units.status = 1 and office_units.isDeleted = 0\n" +
                "and office_unit_organograms.status = 1 and office_unit_organograms.isDeleted = 0\n" +
                "and office_units.id in (%s)", unit_id);

        RQueryBuilder<OfficeUnitTransferSameOfficeModel.OrganogramDetails> rQueryBuilder = new RQueryBuilder<>();
        ArrayList<OfficeUnitTransferSameOfficeModel.OrganogramDetails> data = rQueryBuilder.setSql(sql).of(OfficeUnitTransferSameOfficeModel.OrganogramDetails.class).buildRaw();

        StringBuilder designation_id = new StringBuilder();
        for (int i = 0; i < data.size(); i++) {
            if (i == 0) designation_id.append(data.get(i).getId());
            else designation_id.append(", ").append(data.get(i).getId());
        }

        String sql0 = String.format("select\n" +
                "employee_records.id as employee_record_id,\n" +
                "employee_records.name_eng,\n" +
                "employee_records.name_bng,\n" +
                "employee_offices.office_id,\n" +
                "employee_offices.office_unit_id,\n" +
                "employee_offices.office_unit_organogram_id,\n" +
                "employee_offices.designation\n" +
                "\n" +
                "from employee_records, employee_offices\n" +
                "where employee_records.id = employee_offices.employee_record_id\n" +
                "and employee_records.isDeleted = 0\n" +            // and employee_records.`status` = 1
                "and employee_offices.`status` = 1 and employee_offices.isDeleted = 0\n" +
                "and employee_offices.office_unit_organogram_id in (%s);", designation_id);

        RQueryBuilder<OfficeUnitTransferSameOfficeModel.OrganogramDetails> rQueryBuilder0 = new RQueryBuilder<>();
        ArrayList<OfficeUnitTransferSameOfficeModel.OrganogramDetails> data0 = rQueryBuilder0.setSql(sql0).of(OfficeUnitTransferSameOfficeModel.OrganogramDetails.class).buildRaw();

        if (data != null && data0 != null) {
            for (int i = 0; i < data.size(); i++) {
                for (int j = 0; j < data0.size(); j++) {
                    if (data.get(i).getId() == data0.get(j).getOffice_unit_organogram_id()) {
                        data.get(i).setEmployee_record_id(data0.get(j).getEmployee_record_id());
                        data.get(i).setName_bng(data0.get(j).getName_bng());
                        data.get(i).setName_eng(data0.get(j).getName_eng());
                        data.get(i).setOffice_id(data0.get(j).getOffice_id());
                        data.get(i).setOffice_unit_id(data0.get(j).getOffice_unit_id());
                        data.get(i).setDesignation(data0.get(j).getDesignation());
                        data.get(i).setOffice_unit_organogram_id(data0.get(j).getOffice_unit_organogram_id());
                        break;
                    }
                }
            }
        }
        return new Gson().toJson(data);
    }

    public boolean doTransfer(String[] from, String to) {

        ArrayList<OfficeUnitTransferSameOfficeModel.OfficeUnitModelDetails> fromUnits = getUnit(from);
        OfficeUnitTransferSameOfficeModel.OfficeUnitModelDetails toUnit = getUnit(new String[]{to}).get(0);

        for (OfficeUnitTransferSameOfficeModel.OfficeUnitModelDetails fromuUnit : fromUnits) {
            doTransferOrganogram(fromuUnit, toUnit);
            setOfficeUnitStatus(String.valueOf(fromuUnit.getId()), 0);
        }

        return true;
    }

    private boolean doTransferOrganogram(OfficeUnitTransferSameOfficeModel.OfficeUnitModelDetails fromUnit, OfficeUnitTransferSameOfficeModel.OfficeUnitModelDetails toUnit) {

        ArrayList<OfficeUnitTransferSameOfficeModel.office_unit_organograms> fromOrganogram = getOrganogram(String.valueOf(fromUnit.getId()));
        for (OfficeUnitTransferSameOfficeModel.office_unit_organograms x : fromOrganogram) {
            x.setOffice_unit_id(toUnit.getId());
            x.setSuperior_unit_id(x.getSuperior_unit_id());
            x.setSuperior_designation_id(x.getSuperior_designation_id());
            x.setRef_origin_unit_org_id(0);
            x.setRef_sup_origin_unit_desig_id(x.getRef_sup_origin_unit_desig_id());
            x.setRef_sup_origin_unit_id(x.getRef_sup_origin_unit_id());
            x.setStatus(true);
            x.setIs_admin(false);
            long oldOgranogramId = x.getId();
            //int employeeRecordId = getEmployeeId(String.valueOf(x.getId()));

            /*
                1. First insert new organogram in office_unit_organograms
                2. Transfer employee from old to new office unit or update organogram info
                3. Disable previous office unit ogranogram status
                4. Remove from admin, head, front desk
             */
            insertOfficeUnitOrganogram(x);
            transferEmployee(String.valueOf(oldOgranogramId), String.valueOf(fromUnit.getId()), String.valueOf(x.getId()));
            setOfficeUnitOrganogramStatus(String.valueOf(oldOgranogramId), 0);

            removeAsAdmin(String.valueOf(oldOgranogramId));
            removeAsOfficeHead(String.valueOf(oldOgranogramId));
            removeAsFrontDesk(String.valueOf(x.getOffice_unit_id()), String.valueOf(oldOgranogramId));
        }

        return true;
    }

    // region Transfer

    private ArrayList<OfficeUnitTransferSameOfficeModel.OfficeUnitModelDetails> getUnit(String[] id) {
        StringJoiner sj = new StringJoiner(",");
        for (String s : id) sj.add(s);

        String sql = String.format("select * from office_units where id in (%s) and isDeleted = 0 and status = 1;", sj);
        RQueryBuilder<OfficeUnitTransferSameOfficeModel.OfficeUnitModelDetails> rQueryBuilder = new RQueryBuilder<>();
        ArrayList<OfficeUnitTransferSameOfficeModel.OfficeUnitModelDetails> data = rQueryBuilder.setSql(sql).of(OfficeUnitTransferSameOfficeModel.OfficeUnitModelDetails.class).buildRaw();
        return data;
    }

    private ArrayList<OfficeUnitTransferSameOfficeModel.office_unit_organograms> getOrganogram(String officeUnitId) {

        String sql = String.format("select * from office_unit_organograms where office_unit_id = %s and isDeleted = 0 and status = 1;", officeUnitId);
        RQueryBuilder<OfficeUnitTransferSameOfficeModel.office_unit_organograms> rQueryBuilder = new RQueryBuilder<>();
        ArrayList<OfficeUnitTransferSameOfficeModel.office_unit_organograms> data = rQueryBuilder.setSql(sql).of(OfficeUnitTransferSameOfficeModel.office_unit_organograms.class).buildRaw();
        return data;
    }

    private boolean setOfficeUnitStatus(String unitId, int status) {
        String sql = String.format("update office_units set status = %d, isDeleted = 1 where id = %s;", status, unitId);
        RQueryBuilder<Boolean> rQueryBuilder = new RQueryBuilder<>();
        boolean data = rQueryBuilder.setSql(sql).of(Boolean.class).buildUpdate();
        return data;
    }

    private boolean setOfficeUnitOrganogramStatus(String organogramId, int status) {
        String sql = String.format("update office_unit_organograms set status = %d, isDeleted = 1 where id = %s;", status, organogramId);
        RQueryBuilder<Boolean> rQueryBuilder = new RQueryBuilder<>();
        boolean data = rQueryBuilder.setSql(sql).of(Boolean.class).buildUpdate();
        return data;
    }

    private boolean insertOfficeUnitOrganogram(OfficeUnitTransferSameOfficeModel.office_unit_organograms a) {
        try {
            a.setId(DBMW.getInstance().getNextSequenceId("office_unit_organograms"));
            RInsertQueryBuilder<OfficeUnitTransferSameOfficeModel.office_unit_organograms> builder = new RInsertQueryBuilder<>();
            boolean r = builder.model(a).of(OfficeUnitTransferSameOfficeModel.office_unit_organograms.class).buildInsert();
            return r;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    private boolean transferEmployee(String oldOrganogramId, String unitId, String newOrganogramId) {
        String sql = String.format("update employee_offices set office_unit_organogram_id = %s where office_unit_id = %s and office_unit_organogram_id = %s and employee_offices.isDeleted = 0 and employee_offices.`status` = 1;", newOrganogramId, unitId, oldOrganogramId);

        RQueryBuilder<Boolean> rQueryBuilder = new RQueryBuilder<>();
        boolean data = rQueryBuilder.setSql(sql).of(Boolean.class).buildUpdate();
        return data;
    }

    private boolean removeAsOfficeHead(String office_unit_organogram_id) {
        String sql = String.format("update employee_offices set office_head = 0 where office_unit_organogram_id = %s;", office_unit_organogram_id);
        RQueryBuilder<Boolean> rQueryBuilder = new RQueryBuilder<>();
        boolean data = rQueryBuilder.setSql(sql).of(Boolean.class).buildUpdate();
        return data;
    }

    private boolean removeAsAdmin(String office_unit_organogram_id) {
        String sql = String.format("update office_unit_organograms set is_admin = 0 where id = %s;", office_unit_organogram_id);
        RQueryBuilder<Boolean> rQueryBuilder = new RQueryBuilder<>();
        boolean data = rQueryBuilder.setSql(sql).of(Boolean.class).buildUpdate();
        return data;
    }

    private boolean removeAsFrontDesk(String officeUnitId, String organogramId) {
        String sql = String.format("update office_front_desk set isDeleted = 1 where office_unit_id = %s and office_unit_organogram_id = %s;", officeUnitId, organogramId);
        RQueryBuilder<Boolean> rQueryBuilder = new RQueryBuilder<>();
        boolean data = rQueryBuilder.setSql(sql).of(Boolean.class).buildUpdate();
        return data;
    }

    private int getEmployeeId(String office_unit_organogram_id) {
        String sql = String.format("select employee_record_id from employee_offices where office_unit_organogram_id = %s and isDeleted = 0 and status = 1;", office_unit_organogram_id);
        RQueryBuilder<OfficeUnitTransferSameOfficeModel.EmployeeRecord> rQueryBuilder = new RQueryBuilder<>();
        ArrayList<OfficeUnitTransferSameOfficeModel.EmployeeRecord> data = rQueryBuilder.setSql(sql).of(OfficeUnitTransferSameOfficeModel.EmployeeRecord.class).buildRaw();
        return data.get(0).getEmployee_record_id();
    }

    // endregion Transfer
}
