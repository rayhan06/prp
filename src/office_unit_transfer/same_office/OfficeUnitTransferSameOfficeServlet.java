package office_unit_transfer.same_office;

import login.LoginDTO;
import office_unit_transfer.OfficeUnitTransferDAO;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/OfficeUnitTransferSameOfficeServlet")
@MultipartConfig
public class OfficeUnitTransferSameOfficeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(OfficeUnitTransferSameOfficeServlet.class);

    /**
     * @see HttpServlet#HttpServlet()
     */
    public OfficeUnitTransferSameOfficeServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getUnits")) {

                String officeId = request.getParameter("office_id");
                response.setContentType("application/json");

                PrintWriter out = response.getWriter();
                out.print(new OfficeUnitTransferSameOfficeDAO().getOfficeUnit(officeId));
                out.flush();
            } else if (actionType.equals("getDesignation")) {
                String[] ids = request.getParameterValues("unit_id");

                response.setContentType("application/json");
                PrintWriter out = response.getWriter();
                out.print(new OfficeUnitTransferSameOfficeDAO().getOfficeUnitOrganogram(ids));
                out.flush();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("transfer")) {

                String[] from = request.getParameterValues("from");
                String to = request.getParameter("to");
                response.setContentType("application/json");

                PrintWriter out = response.getWriter();
                //out.print(new OfficeUnitTransferSameOfficeDAO().doTransfer(from, to));
                out.print(new OfficeUnitTransferDAO().transfer(from, to));
                out.flush();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }
}
