/*
package office_unit_transfer.different_office;

import login.LoginDTO;
import office_unit_transfer.same_office.OfficeUnitTransferSameOfficeServlet;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/OfficeUnitTransferDifferentOfficeServlet")
@MultipartConfig
public class OfficeUnitTransferDifferentOfficeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(OfficeUnitTransferSameOfficeServlet.class);

    public OfficeUnitTransferDifferentOfficeServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getUnits")) {

                String officeId = request.getParameter("office_id");
                response.setContentType("application/json");

                PrintWriter out = response.getWriter();
                out.print(new OfficeUnitTransferDifferentOfficeDAO().getUnitListToTransfer(officeId));
                out.flush();
            } else if (actionType.equals("getDesignation")) {
                String[] ids = request.getParameterValues("unit_id");

                response.setContentType("application/json");
                PrintWriter out = response.getWriter();
                out.print(new OfficeUnitTransferDifferentOfficeDAO().getOfficeUnitOrganogram(ids));
                out.flush();
            } else if (actionType.equals("getOrganogram")) {
                String unit_id = request.getParameter("unit_id");

                response.setContentType("application/json");
                PrintWriter out = response.getWriter();
                out.print(new OfficeUnitTransferDifferentOfficeDAO().getOrganogramListToTransfer(unit_id));
                out.flush();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("transfer")) {

                String[] employee_record = request.getParameterValues("emp");
                String[] from_unit = request.getParameterValues("from_unit");
                String[] from_designation = request.getParameterValues("frm_desg");
                String to_office = request.getParameter("to_office");
                String[] to_unit = request.getParameterValues("to_unit");
                String[] to_designation = request.getParameterValues("to_desg");
                String[] to_designation_name = request.getParameterValues("to_desg_name");
                response.setContentType("application/json");

                for (int i = 0; i < to_designation_name.length; i++) {
                    byte ptext[] = to_designation_name[i].getBytes("ISO-8859-1");
                    to_designation_name[i] = new String(ptext, "UTF-8");
                    System.out.println(to_designation_name[i]);
                }

                PrintWriter out = response.getWriter();
                out.print(new OfficeUnitTransferDifferentOfficeDAO().startTransfer(employee_record, from_unit, from_designation, to_office, to_unit, to_designation, to_designation_name));
                out.flush();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }
}
*/
