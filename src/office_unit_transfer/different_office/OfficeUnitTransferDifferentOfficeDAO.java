package office_unit_transfer.different_office;

import dbm.DBMW;
import office_unit_transfer.OfficeUnitTransferDTO;
import office_unit_transfer.RInsertSqlString;
import office_unit_transfer.same_office.OfficeUnitTransferSameOfficeModel;
import office_unit_transfer.transfer_log.OfficeUnitTransferService;
import startup.SlowWorker;
import test_lib.RQueryBuilder;
import test_lib.RQueryExecutor;

import java.sql.SQLException;

public class OfficeUnitTransferDifferentOfficeDAO {

    public String getOfficeUnitOrganogram(String[] ids) {
        String unit_id = "";
        for (int i = 0; i < ids.length; i++) {
            if (i == 0) unit_id += ids[i];
            else unit_id += ", " + ids[i];
        }

        String sql = String.format("select \n" +
                "\temployee_offices.id, \n" +
                "\temployee_offices.employee_record_id, \n" +
                "\temployee_records.name_bng, \n" +
                "\temployee_records.name_eng, \n" +
                "\temployee_offices.office_id, \n" +
                "\temployee_offices.office_unit_id, \n" +
                "\temployee_offices.office_unit_organogram_id,\n" +
                "\temployee_offices.designation,\n" +
                "    office_units.unit_name_bng,\n" +
                "    office_units.unit_name_eng\n" +
                "\n" +
                "from employee_records, employee_offices, office_units\n" +
                "\n" +
                "where employee_records.id = employee_offices.employee_record_id \n" +
                "and office_units.id = employee_offices.office_unit_id\n" +
                "and employee_offices.office_unit_id in (%s) \n" +
                "and employee_records.isDeleted = 0;", unit_id);
        RQueryBuilder<OfficeUnitTransferSameOfficeModel.OrganogramDetails> rQueryBuilder = new RQueryBuilder<>();

        return rQueryBuilder.setSql(sql).of(OfficeUnitTransferSameOfficeModel.OrganogramDetails.class).buildJson();
    }

    public String getUnitListToTransfer(String officeId) {
        String sql = String.format("select offices.id as office_id, office_units.id as office_unit_id, office_units.unit_name_bng, office_units.unit_name_eng from offices, office_units where offices.id = office_units.office_id and offices.id = %s and offices.isDeleted = 0 and office_units.isDeleted = 0 and offices.status = 1 and office_units.status = 1; ", officeId);

        RQueryBuilder<OfficeUnitTransferDifferentOfficeModel.EmployeeOfficeModelFrom> rQueryBuilder = new RQueryBuilder<>();
        String data = rQueryBuilder.setSql(sql).of(OfficeUnitTransferDifferentOfficeModel.EmployeeOfficeModelFrom.class).buildJson();

        return data;
    }

    public String getOrganogramListToTransfer(String organogramId) {
        String sql = String.format("select \n" +
                "office_unit_organograms.id as organogram_id,\n" +
                "office_unit_organograms.office_id as office_id,\n" +
                "office_unit_organograms.office_unit_id as office_unit_id,\n" +
                "office_unit_organograms.designation_eng,\n" +
                "office_unit_organograms.designation_bng\n" +
                "\n" +
                "from office_units \n" +
                "join office_unit_organograms on office_units.id = office_unit_organograms.office_unit_id\n" +
                "left join employee_offices on office_unit_organograms.id = employee_offices.office_unit_organogram_id\n" +
                "where employee_offices.id is null \n" +
                "and office_unit_organograms.isDeleted = 0\n" +
                "and office_unit_organograms.`status` = 1\n" +
                "and office_units.id = %s", organogramId);

        RQueryBuilder<OfficeUnitTransferDifferentOfficeModel.OrganogramModelFrom> rQueryBuilder = new RQueryBuilder<>();

        return rQueryBuilder.setSql(sql).of(OfficeUnitTransferDifferentOfficeModel.OrganogramModelFrom.class).buildJson();
    }

    public String startTransfer(String[] employee_record, String[] from_unit, String[] from_designation, String to_office, String[] to_unit, String[] to_designation, String[] to_designation_name) {

        // 0. set status 0 of from_unit
        // 1. set employee_offices status = 0 where office_unit_organogram_id (from_designation)
        // 2. Get designation name from to_designation
        // 3. Insert new row on employee_offices (employee_record, to_office, to_unit, to_designation, designation name)
        StringBuilder finalSql = new StringBuilder();
        for (String unit : from_unit) {
            finalSql.append(this.setUnitStatusZero(unit));
        }
        for (String designation : from_designation) {
            finalSql.append(this.setEmployeeOfficeStatusZero(designation));
        }
        for (String designation : from_designation) {
            finalSql.append(this.setDesignationStatusZero(designation));
        }
        for (int i = 0; i < employee_record.length; i++) {
            OfficeUnitTransferDTO.employee_offices employee_offices = new OfficeUnitTransferDTO.employee_offices();
            employee_offices.setId(getEmployeeOfficesSequenceId());
            employee_offices.setEmployee_record_id(Long.valueOf(employee_record[i]));
            employee_offices.setOffice_id(Integer.valueOf(to_office));
            employee_offices.setOffice_unit_id(Long.valueOf(to_unit[i]));
            employee_offices.setOffice_unit_organogram_id(Long.valueOf(to_designation[i]));
            employee_offices.setDesignation(to_designation_name[i]);
            employee_offices.setOffice_head(false);
            employee_offices.setJoining_date(System.currentTimeMillis());
            employee_offices.setLast_office_date(0L);
            employee_offices.setStatus(true);
            employee_offices.setIsDeleted(false);

            RInsertSqlString<OfficeUnitTransferDTO.employee_offices> sqlString = new RInsertSqlString<>();
            finalSql.append(sqlString.model(employee_offices).of(OfficeUnitTransferDTO.employee_offices.class).buildInsert());
        }
        System.out.println(finalSql);

        RQueryExecutor rQueryExecutor = new RQueryExecutor();
        try {
            String r = rQueryExecutor.execute(finalSql.toString());
            SlowWorker.getInstance().invoke(new OfficeUnitTransferService(employee_record, from_unit, from_designation, to_office, to_unit, to_designation));
            return r;
        } catch (SQLException e) {
            e.printStackTrace();
            String jsonString = ("{" + "\"success\"" + ":" + "false," + "\"msg\"" + ":" + e.getMessage() + "}");
            return jsonString;
        }
    }

    private String setUnitStatusZero(String unitId) {
        return String.format("update office_units set status = 0 where id = %s;", unitId);
    }

    private String setDesignationStatusZero(String organogramId) {
        return String.format("update office_unit_organograms set status = 0 where id = %s;", organogramId);
    }

    private String setEmployeeOfficeStatusZero(String organogramId) {
        return String.format("update employee_offices set office_head = 0, status = 0 where office_unit_organogram_id = %s;", organogramId);
    }

    private long getEmployeeOfficesSequenceId() {
        try {
            return DBMW.getInstance().getNextSequenceId("employee_offices");
        } catch (Exception e) {
            e.printStackTrace();
            return 0L;
        }
    }
}
