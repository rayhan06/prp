package office_unit_transfer.different_office;

public class OfficeUnitTransferDifferentOfficeModel {

    public static class EmployeeOfficeModelFrom {
        private int office_id;
        private int office_unit_id;
        private String unit_name_bng;
        private String unit_name_eng;

        public int getOffice_id() {
            return office_id;
        }

        public void setOffice_id(int office_id) {
            this.office_id = office_id;
        }

        public int getOffice_unit_id() {
            return office_unit_id;
        }

        public void setOffice_unit_id(int office_unit_id) {
            this.office_unit_id = office_unit_id;
        }

        public String getUnit_name_bng() {
            return unit_name_bng;
        }

        public void setUnit_name_bng(String unit_name_bng) {
            this.unit_name_bng = unit_name_bng;
        }

        public String getUnit_name_eng() {
            return unit_name_eng;
        }

        public void setUnit_name_eng(String unit_name_eng) {
            this.unit_name_eng = unit_name_eng;
        }
    }

    public static class OrganogramModelFrom {
        public int organogram_id;
        public int office_id;
        public int office_unit_id;
        public String designation_eng;
        public String designation_bng;

        public int getOrganogram_id() {
            return organogram_id;
        }

        public void setOrganogram_id(int organogram_id) {
            this.organogram_id = organogram_id;
        }

        public int getOffice_id() {
            return office_id;
        }

        public void setOffice_id(int office_id) {
            this.office_id = office_id;
        }

        public int getOffice_unit_id() {
            return office_unit_id;
        }

        public void setOffice_unit_id(int office_unit_id) {
            this.office_unit_id = office_unit_id;
        }

        public String getDesignation_eng() {
            return designation_eng;
        }

        public void setDesignation_eng(String designation_eng) {
            this.designation_eng = designation_eng;
        }

        public String getDesignation_bng() {
            return designation_bng;
        }

        public void setDesignation_bng(String designation_bng) {
            this.designation_bng = designation_bng;
        }
    }
}
