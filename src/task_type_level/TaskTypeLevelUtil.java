package task_type_level;

import office_units.Office_unitsRepository;
import task_type.Task_typeDTO;
import task_type.Task_typeRepository;

import java.util.Arrays;
import java.util.stream.Collectors;

public class TaskTypeLevelUtil {
    public static String getTaskTypeName(String language, long taskTypeId){
        Task_typeDTO taskTypeDTO = Task_typeRepository.getInstance().getDTOById(taskTypeId);
        return taskTypeDTO.getFormattedName(language);
    }

    public static String showOfficesName(String language, String officeUnitIds) {
        return Arrays.stream(officeUnitIds.split(","))
                .map(Long::parseLong)
                .map(id -> Office_unitsRepository.getInstance().geText(language, id))
                .collect(Collectors.joining("<br>"));
    }
}
