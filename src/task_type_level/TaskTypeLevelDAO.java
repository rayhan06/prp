package task_type_level;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import util.CommonDTO;
import util.HttpRequestUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SuppressWarnings({"unused","Duplicates"})
public class TaskTypeLevelDAO implements CommonDAOService<TaskTypeLevelDTO> {

    private static final Logger logger = Logger.getLogger(TaskTypeLevelDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (level,office_unit_ids,modified_by,lastModificationTime," +
            "task_type_id,insertion_date,inserted_by,isDeleted,id) VALUES (?,?,?,?,?,?,?,?,?)";

    private static final String updateQuery = "UPDATE {tableName} SET level = ?,office_unit_ids=?,modified_by=?,lastModificationTime=? WHERE id = ?";

    private static final String getByTaskInfoId = "SELECT * FROM task_type_level WHERE task_type_id = %d AND isDeleted = 0";

    private static final String deleteByTaskTypeId = "UPDATE task_type_level SET isDeleted = 1, lastModificationTime = %d, modified_by = %d WHERE task_type_id IN (%s)";

    private static TaskTypeLevelDAO INSTANCE = null;

    public static TaskTypeLevelDAO getInstance(){
        if(INSTANCE == null){
            synchronized (TaskTypeLevelDAO.class){
                if(INSTANCE == null){
                    INSTANCE = new TaskTypeLevelDAO();
                }
            }
        }
        return INSTANCE;
    }
    private TaskTypeLevelDAO() {
    }

    @Override
    public void set(PreparedStatement ps, TaskTypeLevelDTO taskTypeLevelDTO, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setObject(++index, taskTypeLevelDTO.level);
        ps.setObject(++index, taskTypeLevelDTO.officeUnitIds);
        ps.setObject(++index, taskTypeLevelDTO.modifiedBy);
        ps.setObject(++index, taskTypeLevelDTO.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, taskTypeLevelDTO.taskTypeId);
            ps.setObject(++index, taskTypeLevelDTO.insertionDate);
            ps.setObject(++index, taskTypeLevelDTO.insertedBy);
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, taskTypeLevelDTO.iD);
    }

    @Override
    public TaskTypeLevelDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            TaskTypeLevelDTO dto = new TaskTypeLevelDTO();
            dto.iD = rs.getLong("id");
            dto.taskTypeId = rs.getLong("task_type_id");
            dto.level = rs.getInt("level");
            dto.officeUnitIds = rs.getString("office_unit_ids");
            dto.insertionDate = rs.getLong("insertion_date");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((TaskTypeLevelDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((TaskTypeLevelDTO) commonDTO, updateQuery, false);
    }

    public TaskTypeLevelDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }

    @Override
    public String getTableName() {
        return "task_type_level";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return null;
    }

    public List<TaskTypeLevelDTO> getByTaskInfoId(long taskInfoId){
        String sql = String.format(getByTaskInfoId,taskInfoId);
        return ConnectionAndStatementUtil.getListOfT(sql,this::buildObjectFromResultSet);
    }

    public void deleteByTaskTypeId(List<Long> taskTypeIds,long modifiedBy){
        String ids = taskTypeIds.stream()
                .map(String::valueOf)
                .collect(Collectors.joining(","));
        long currentTime = System.currentTimeMillis();
        String sql = String.format(deleteByTaskTypeId,currentTime, modifiedBy,ids);
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model->{
            try {
                logger.debug(sql);
                model.getStatement().executeUpdate(sql);
                recordUpdateTime(model.getConnection(),getTableName(),currentTime);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
    }
}
	