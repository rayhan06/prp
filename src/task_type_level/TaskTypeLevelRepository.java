package task_type_level;

import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class TaskTypeLevelRepository implements Repository {
    private static final Logger logger = Logger.getLogger(TaskTypeLevelRepository.class);

    private final TaskTypeLevelDAO task_typeLevelDAO = TaskTypeLevelDAO.getInstance();

    private final Map<Long, TaskTypeLevelDTO> mapById = new ConcurrentHashMap<>();

    private final List<TaskTypeLevelDTO> allList = new ArrayList<>();

    private TaskTypeLevelRepository() {
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class TaskTypeLevelRepositoryLazyLoader {
        static final TaskTypeLevelRepository INSTANCE = new TaskTypeLevelRepository();
    }

    public synchronized static TaskTypeLevelRepository getInstance() {
        return TaskTypeLevelRepositoryLazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        logger.debug("reload start for reloadAll : " + reloadAll);
        List<TaskTypeLevelDTO> dtoList = task_typeLevelDAO.getAllDTOs(reloadAll);
        dtoList.stream()
                .peek(this::removeIfFound)
                .filter(dto->dto.isDeleted == 0)
                .forEach(this::addDTO);
        logger.debug("reload end for reloadAll : " + reloadAll);
    }

    private void addDTO(TaskTypeLevelDTO dto) {
        mapById.put(dto.iD, dto);
        allList.add(dto);
    }

    private void removeIfFound(TaskTypeLevelDTO dto) {
        TaskTypeLevelDTO oldDTO = mapById.get(dto.iD);
        if(oldDTO!=null){
            mapById.remove(dto.iD);
            allList.remove(oldDTO);
        }
    }

    public TaskTypeLevelDTO getById(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock("TTID"+id)) {
                if (mapById.get(id) == null) {
                    TaskTypeLevelDTO dto = task_typeLevelDAO.getDTOFromID(id);
                    if(dto!=null && dto.isDeleted == 0){
                        mapById.put(dto.iD,dto);
                    }
                }
            }
        }
        return mapById.get(id);
    }

    public List<TaskTypeLevelDTO> getByTaskTypeId(long taskTypeId) {
        List<TaskTypeLevelDTO> list = allList.parallelStream()
                .filter(dto->dto.taskTypeId == taskTypeId)
                .collect(Collectors.toList());
        if(list.size() == 0){
            synchronized (LockManager.getLock("TTI"+taskTypeId)) {
                List<TaskTypeLevelDTO> dtoList = task_typeLevelDAO.getByTaskInfoId(taskTypeId);
                if (dtoList.size() > 0) {
                    dtoList.forEach(this::addDTO);
                }
                return dtoList;
            }
        }
        return list;
    }

    public TaskTypeLevelDTO getByTaskTypeIdAndLevel(long taskTypeId, int level) {
        return getByTaskTypeId(taskTypeId)
                .stream()
                .filter(dto->dto.level == level)
                .findAny()
                .orElse(null);
    }

    public TaskTypeLevelDTO getNextLevel(long taskTypeId, int level) {
        return getByTaskTypeId(taskTypeId)
                .stream()
                .filter(dto->dto.level> level)
                .collect(Collectors.collectingAndThen(Collectors.collectingAndThen(Collectors.toList(),ls->{
                    ls.sort(Comparator.comparing(e->e.level));
                    return ls;
                }),ls->ls.size() == 0 ? null : ls.get(0)));
    }

    @Override
    public String getTableName() {
        return task_typeLevelDAO.getTableName();
    }

    public String buildOfficeOptionFromTaskTypeId(String language, long taskTypeId, int level) {
        TaskTypeLevelDTO taskTypeLevelDTO = getByTaskTypeIdAndLevel(taskTypeId, level);
        if (taskTypeLevelDTO == null){
            return Office_unitsRepository.getInstance().buildOptions(language, 0L, false, null);
        }
        List<Office_unitsDTO> officeUnitsDTOList = Arrays.stream(taskTypeLevelDTO.officeUnitIds.split(","))
                .map(Long::parseLong)
                .map(Office_unitsRepository.getInstance()::getOffice_unitsDTOByID)
                .collect(Collectors.toList());
        return Office_unitsRepository.getInstance().buildOptions(language, 0L, false, officeUnitsDTOList);
    }

    public TaskTypeLevelDTO getIdForMaxLevel(long taskTypeId){
        List<TaskTypeLevelDTO> taskTypeLevelDTOList = getByTaskTypeId(taskTypeId);
        return taskTypeLevelDTOList.stream()
                .max(Comparator.comparing(o->o.level))
                .orElse(null);
    }
}