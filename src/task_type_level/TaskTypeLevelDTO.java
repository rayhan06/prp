package task_type_level;

import util.*;

public class TaskTypeLevelDTO extends CommonDTO {
    public long taskTypeId = 0;
    public int level = 0;
    public String officeUnitIds = "";
    public long insertionDate = 0;
    public long insertedBy = 0;
    public long modifiedBy = 0;

    public TaskTypeLevelDTO(){}

    public TaskTypeLevelDTO(long taskTypeId,int level){
        this.taskTypeId = taskTypeId;
        this.level = level;
    }

    public boolean hasSameData(TaskTypeLevelDTO other){
        return taskTypeId == other.taskTypeId
                && level  == other.level
                && officeUnitIds.equals(other.officeUnitIds);
    }

    @Override
    public String toString() {
        return "$Task_type_levelDTO[" +
                " iD = " + iD +
                " taskTypeId = " + taskTypeId +
                " level = " + level +
                " officeUnitIds = " + officeUnitIds +
                " insertionDate = " + insertionDate +
                " insertedBy = " + insertedBy +
                " modifiedBy = " + modifiedBy +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}