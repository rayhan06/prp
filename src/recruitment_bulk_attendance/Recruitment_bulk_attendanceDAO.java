package recruitment_bulk_attendance;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import job_applicant_application.Job_applicant_applicationDTO;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import java.util.stream.Collectors;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

import static sessionmanager.SessionConstants.AttendanceStatusMap;

public class Recruitment_bulk_attendanceDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	//Recruitment_bulk_attendanceDAO recruitment_bulk_attendanceDAO = new Recruitment_bulk_attendanceDAO();
	String parentTableName = "job_applicant_application";

	
	public Recruitment_bulk_attendanceDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Recruitment_bulk_attendanceMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"job_id",
			"level_id",
			"roll",
			"status",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Recruitment_bulk_attendanceDAO()
	{
		this("recruitment_bulk_attendance");		
	}
	
	public void setSearchColumn(Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO)
	{
		recruitment_bulk_attendanceDTO.searchColumn = "";
		recruitment_bulk_attendanceDTO.searchColumn += recruitment_bulk_attendanceDTO.jobId + " ";
		recruitment_bulk_attendanceDTO.searchColumn += recruitment_bulk_attendanceDTO.levelId + " ";
		recruitment_bulk_attendanceDTO.searchColumn += recruitment_bulk_attendanceDTO.roll + " ";
		recruitment_bulk_attendanceDTO.searchColumn += recruitment_bulk_attendanceDTO.status + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO = (Recruitment_bulk_attendanceDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(recruitment_bulk_attendanceDTO);
		if(isInsert)
		{
			ps.setObject(index++,recruitment_bulk_attendanceDTO.iD);
		}
		ps.setObject(index++,recruitment_bulk_attendanceDTO.jobId);
		ps.setObject(index++,recruitment_bulk_attendanceDTO.levelId);
		ps.setObject(index++,recruitment_bulk_attendanceDTO.roll);
		ps.setObject(index++,recruitment_bulk_attendanceDTO.status);
		ps.setObject(index++,recruitment_bulk_attendanceDTO.insertionDate);
		ps.setObject(index++,recruitment_bulk_attendanceDTO.insertedBy);
		ps.setObject(index++,recruitment_bulk_attendanceDTO.modifiedBy);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO, ResultSet rs) throws SQLException
	{
		recruitment_bulk_attendanceDTO.id = rs.getLong("id");
		recruitment_bulk_attendanceDTO.jobId = rs.getLong("job_id");
		recruitment_bulk_attendanceDTO.levelId = rs.getLong("level_id");
		recruitment_bulk_attendanceDTO.roll = rs.getLong("roll");
		recruitment_bulk_attendanceDTO.status = rs.getLong("status");
		recruitment_bulk_attendanceDTO.insertionDate = rs.getLong("insertion_date");
		recruitment_bulk_attendanceDTO.insertedBy = rs.getString("inserted_by");
		recruitment_bulk_attendanceDTO.modifiedBy = rs.getString("modified_by");
		recruitment_bulk_attendanceDTO.isDeleted = rs.getInt("isDeleted");
		recruitment_bulk_attendanceDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}
	
	
	
	
		
	

	//need another getter for repository
	public Recruitment_bulk_attendanceDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE id=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				recruitment_bulk_attendanceDTO = new Recruitment_bulk_attendanceDTO();

				get(recruitment_bulk_attendanceDTO, rs);

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return recruitment_bulk_attendanceDTO;
	}

	//need ID repository
//	public Recruitment_bulk_attendanceDTO getIDDTOsByJobLevelID (int job_id,int level_id)
//	{
//		Connection connection = null;
//		ResultSet rs = null;
//		Statement stmt = null;
//		Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO = null;
//		try{
//
//			String sql = "SELECT ID ";
//
//			sql += " FROM " + parentTableName;
//
//			sql += " WHERE job_id=" + job_id;
//
//			sql += " AND level=" + level_id;
//
//			printSql(sql);
//
//			connection = DBMR.getInstance().getConnection();
//			stmt = connection.createStatement();
//
//
//			rs = stmt.executeQuery(sql);
//
//			if(rs.next()){
//				recruitment_bulk_attendanceDTO = new Recruitment_bulk_attendanceDTO();
//
//				get(recruitment_bulk_attendanceDTO, rs);
//
//			}
//
//
//		}catch(Exception ex){
//			ex.printStackTrace();
//		}finally{
//			try{
//				if (stmt != null) {
//					stmt.close();
//				}
//			} catch (Exception e){}
//
//			try{
//				if (connection != null)
//				{
//					DBMR.getInstance().freeConnection(connection);
//				}
//			}catch(Exception ex2){}
//		}
//		return recruitment_bulk_attendanceDTO;
//	}


	
	
	
	
	public List<Recruitment_bulk_attendanceDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO = null;
		List<Recruitment_bulk_attendanceDTO> recruitment_bulk_attendanceDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return recruitment_bulk_attendanceDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE id IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				recruitment_bulk_attendanceDTO = new Recruitment_bulk_attendanceDTO();
				get(recruitment_bulk_attendanceDTO, rs);
				System.out.println("got this DTO: " + recruitment_bulk_attendanceDTO);
				
				recruitment_bulk_attendanceDTOList.add(recruitment_bulk_attendanceDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return recruitment_bulk_attendanceDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Recruitment_bulk_attendanceDTO> getAllRecruitment_bulk_attendance (boolean isFirstReload)
    {
		List<Recruitment_bulk_attendanceDTO> recruitment_bulk_attendanceDTOList = new ArrayList<>();

		String sql = "SELECT * FROM recruitment_bulk_attendance";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by recruitment_bulk_attendance.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO = new Recruitment_bulk_attendanceDTO();
				get(recruitment_bulk_attendanceDTO, rs);
				
				recruitment_bulk_attendanceDTOList.add(recruitment_bulk_attendanceDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return recruitment_bulk_attendanceDTOList;
    }

	
	public List<Recruitment_bulk_attendanceDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Recruitment_bulk_attendanceDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Recruitment_bulk_attendanceDTO> recruitment_bulk_attendanceDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO = new Recruitment_bulk_attendanceDTO();
				get(recruitment_bulk_attendanceDTO, rs);
				
				recruitment_bulk_attendanceDTOList.add(recruitment_bulk_attendanceDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return recruitment_bulk_attendanceDTOList;
	
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("job_id")
						|| str.equals("level_id")
						|| str.equals("roll")
						|| str.equals("status")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("job_id"))
					{
						AllFieldSql += "" + tableName + ".job_id like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("level_id"))
					{
						AllFieldSql += "" + tableName + ".level_id like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("roll"))
					{
						AllFieldSql += "" + tableName + ".roll like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("status"))
					{
						AllFieldSql += "" + tableName + ".status like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }

	public int JobLevelAttendanceExistence (int job_id,int level_id)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		int existence = 1;
		try{

			String sql = "SELECT * ";

			sql += " FROM " + tableName;

			sql += " WHERE job_id=" + job_id + " and level_id=" + level_id;

			sql += " AND isDeleted=0 ";

			printSql(sql);
			//System.out.println("sql att 2 : "+sql);

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				existence=0;
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}

		return existence;
	}

	public List<Recruitment_bulk_attendanceDTO> getRollDTOsByJobLevelID(int job_id, int level_id){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO = null;
		List<Recruitment_bulk_attendanceDTO> recruitment_bulk_attendanceDTOList = new ArrayList<>();


		try{

			String sql = "SELECT * ";

			sql += " FROM " + tableName;

			sql += " WHERE job_id=" + job_id;

			sql += " AND level_id=" + level_id;

			sql += " AND isDeleted =  0";

			sql += " AND status=" + MapGetPresentKey();

			System.out.println("rec mark sql :"+sql);

			printSql(sql);

			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				recruitment_bulk_attendanceDTO = new Recruitment_bulk_attendanceDTO();
				get(recruitment_bulk_attendanceDTO, rs);
				System.out.println("got this DTO: " + recruitment_bulk_attendanceDTO);

				recruitment_bulk_attendanceDTOList.add(recruitment_bulk_attendanceDTO);

			}

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return recruitment_bulk_attendanceDTOList;

	}
	public int MapGetPresentKey(){
		return   AttendanceStatusMap
				.entrySet()
				.stream()
				.filter(m->m.getValue().equalsIgnoreCase("Present"))
				.map(Map.Entry::getKey)
				.findAny().get();

	}
				
}
	