package recruitment_bulk_attendance;
import java.util.*; 
import util.*;


public class Recruitment_bulk_attendanceMAPS extends CommonMaps
{	
	public Recruitment_bulk_attendanceMAPS(String tableName)
	{
		

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("jobId".toLowerCase(), "jobId".toLowerCase());
		java_DTO_map.put("levelId".toLowerCase(), "levelId".toLowerCase());
		java_DTO_map.put("roll".toLowerCase(), "roll".toLowerCase());
		java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("job_id".toLowerCase(), "jobId".toLowerCase());
		java_SQL_map.put("level_id".toLowerCase(), "levelId".toLowerCase());
		java_SQL_map.put("roll".toLowerCase(), "roll".toLowerCase());
		java_SQL_map.put("status".toLowerCase(), "status".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Job Id".toLowerCase(), "jobId".toLowerCase());
		java_Text_map.put("Level Id".toLowerCase(), "levelId".toLowerCase());
		java_Text_map.put("Roll".toLowerCase(), "roll".toLowerCase());
		java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}