package recruitment_bulk_attendance;
import java.util.*; 
import util.*; 


public class Recruitment_bulk_attendanceDTO extends CommonDTO
{

	public long id = 0;
	public long jobId = 0;
	public long levelId = 0;
	public long roll = 0;
	public long status = 0;
	public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Recruitment_bulk_attendanceDTO[" +
            " id = " + id +
            " jobId = " + jobId +
            " levelId = " + levelId +
            " roll = " + roll +
            " status = " + status +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}