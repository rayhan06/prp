package recruitment_bulk_attendance;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Recruitment_bulk_attendanceRepository implements Repository {
	Recruitment_bulk_attendanceDAO recruitment_bulk_attendanceDAO = null;
	
	public void setDAO(Recruitment_bulk_attendanceDAO recruitment_bulk_attendanceDAO)
	{
		this.recruitment_bulk_attendanceDAO = recruitment_bulk_attendanceDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Recruitment_bulk_attendanceRepository.class);
	Map<Long, Recruitment_bulk_attendanceDTO>mapOfRecruitment_bulk_attendanceDTOToid;
	Map<Long, Set<Recruitment_bulk_attendanceDTO> >mapOfRecruitment_bulk_attendanceDTOTojobId;
	Map<Long, Set<Recruitment_bulk_attendanceDTO> >mapOfRecruitment_bulk_attendanceDTOTolevelId;
	Map<Long, Set<Recruitment_bulk_attendanceDTO> >mapOfRecruitment_bulk_attendanceDTOToroll;
	Map<Long, Set<Recruitment_bulk_attendanceDTO> >mapOfRecruitment_bulk_attendanceDTOTostatus;
	Map<Long, Set<Recruitment_bulk_attendanceDTO> >mapOfRecruitment_bulk_attendanceDTOToinsertionDate;
	Map<String, Set<Recruitment_bulk_attendanceDTO> >mapOfRecruitment_bulk_attendanceDTOToinsertedBy;
	Map<String, Set<Recruitment_bulk_attendanceDTO> >mapOfRecruitment_bulk_attendanceDTOTomodifiedBy;
	Map<Long, Set<Recruitment_bulk_attendanceDTO> >mapOfRecruitment_bulk_attendanceDTOTolastModificationTime;


	static Recruitment_bulk_attendanceRepository instance = null;  
	private Recruitment_bulk_attendanceRepository(){
		mapOfRecruitment_bulk_attendanceDTOToid = new ConcurrentHashMap<>();
		mapOfRecruitment_bulk_attendanceDTOTojobId = new ConcurrentHashMap<>();
		mapOfRecruitment_bulk_attendanceDTOTolevelId = new ConcurrentHashMap<>();
		mapOfRecruitment_bulk_attendanceDTOToroll = new ConcurrentHashMap<>();
		mapOfRecruitment_bulk_attendanceDTOTostatus = new ConcurrentHashMap<>();
		mapOfRecruitment_bulk_attendanceDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfRecruitment_bulk_attendanceDTOToinsertedBy = new ConcurrentHashMap<>();
		mapOfRecruitment_bulk_attendanceDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfRecruitment_bulk_attendanceDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Recruitment_bulk_attendanceRepository getInstance(){
		if (instance == null){
			instance = new Recruitment_bulk_attendanceRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(recruitment_bulk_attendanceDAO == null)
		{
			return;
		}
		try {
			List<Recruitment_bulk_attendanceDTO> recruitment_bulk_attendanceDTOs = recruitment_bulk_attendanceDAO.getAllRecruitment_bulk_attendance(reloadAll);
			for(Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO : recruitment_bulk_attendanceDTOs) {
				Recruitment_bulk_attendanceDTO oldRecruitment_bulk_attendanceDTO = getRecruitment_bulk_attendanceDTOByid(recruitment_bulk_attendanceDTO.id);
				if( oldRecruitment_bulk_attendanceDTO != null ) {
					mapOfRecruitment_bulk_attendanceDTOToid.remove(oldRecruitment_bulk_attendanceDTO.id);
				
					if(mapOfRecruitment_bulk_attendanceDTOTojobId.containsKey(oldRecruitment_bulk_attendanceDTO.jobId)) {
						mapOfRecruitment_bulk_attendanceDTOTojobId.get(oldRecruitment_bulk_attendanceDTO.jobId).remove(oldRecruitment_bulk_attendanceDTO);
					}
					if(mapOfRecruitment_bulk_attendanceDTOTojobId.get(oldRecruitment_bulk_attendanceDTO.jobId).isEmpty()) {
						mapOfRecruitment_bulk_attendanceDTOTojobId.remove(oldRecruitment_bulk_attendanceDTO.jobId);
					}
					
					if(mapOfRecruitment_bulk_attendanceDTOTolevelId.containsKey(oldRecruitment_bulk_attendanceDTO.levelId)) {
						mapOfRecruitment_bulk_attendanceDTOTolevelId.get(oldRecruitment_bulk_attendanceDTO.levelId).remove(oldRecruitment_bulk_attendanceDTO);
					}
					if(mapOfRecruitment_bulk_attendanceDTOTolevelId.get(oldRecruitment_bulk_attendanceDTO.levelId).isEmpty()) {
						mapOfRecruitment_bulk_attendanceDTOTolevelId.remove(oldRecruitment_bulk_attendanceDTO.levelId);
					}
					
					if(mapOfRecruitment_bulk_attendanceDTOToroll.containsKey(oldRecruitment_bulk_attendanceDTO.roll)) {
						mapOfRecruitment_bulk_attendanceDTOToroll.get(oldRecruitment_bulk_attendanceDTO.roll).remove(oldRecruitment_bulk_attendanceDTO);
					}
					if(mapOfRecruitment_bulk_attendanceDTOToroll.get(oldRecruitment_bulk_attendanceDTO.roll).isEmpty()) {
						mapOfRecruitment_bulk_attendanceDTOToroll.remove(oldRecruitment_bulk_attendanceDTO.roll);
					}
					
					if(mapOfRecruitment_bulk_attendanceDTOTostatus.containsKey(oldRecruitment_bulk_attendanceDTO.status)) {
						mapOfRecruitment_bulk_attendanceDTOTostatus.get(oldRecruitment_bulk_attendanceDTO.status).remove(oldRecruitment_bulk_attendanceDTO);
					}
					if(mapOfRecruitment_bulk_attendanceDTOTostatus.get(oldRecruitment_bulk_attendanceDTO.status).isEmpty()) {
						mapOfRecruitment_bulk_attendanceDTOTostatus.remove(oldRecruitment_bulk_attendanceDTO.status);
					}
					
					if(mapOfRecruitment_bulk_attendanceDTOToinsertionDate.containsKey(oldRecruitment_bulk_attendanceDTO.insertionDate)) {
						mapOfRecruitment_bulk_attendanceDTOToinsertionDate.get(oldRecruitment_bulk_attendanceDTO.insertionDate).remove(oldRecruitment_bulk_attendanceDTO);
					}
					if(mapOfRecruitment_bulk_attendanceDTOToinsertionDate.get(oldRecruitment_bulk_attendanceDTO.insertionDate).isEmpty()) {
						mapOfRecruitment_bulk_attendanceDTOToinsertionDate.remove(oldRecruitment_bulk_attendanceDTO.insertionDate);
					}
					
					if(mapOfRecruitment_bulk_attendanceDTOToinsertedBy.containsKey(oldRecruitment_bulk_attendanceDTO.insertedBy)) {
						mapOfRecruitment_bulk_attendanceDTOToinsertedBy.get(oldRecruitment_bulk_attendanceDTO.insertedBy).remove(oldRecruitment_bulk_attendanceDTO);
					}
					if(mapOfRecruitment_bulk_attendanceDTOToinsertedBy.get(oldRecruitment_bulk_attendanceDTO.insertedBy).isEmpty()) {
						mapOfRecruitment_bulk_attendanceDTOToinsertedBy.remove(oldRecruitment_bulk_attendanceDTO.insertedBy);
					}
					
					if(mapOfRecruitment_bulk_attendanceDTOTomodifiedBy.containsKey(oldRecruitment_bulk_attendanceDTO.modifiedBy)) {
						mapOfRecruitment_bulk_attendanceDTOTomodifiedBy.get(oldRecruitment_bulk_attendanceDTO.modifiedBy).remove(oldRecruitment_bulk_attendanceDTO);
					}
					if(mapOfRecruitment_bulk_attendanceDTOTomodifiedBy.get(oldRecruitment_bulk_attendanceDTO.modifiedBy).isEmpty()) {
						mapOfRecruitment_bulk_attendanceDTOTomodifiedBy.remove(oldRecruitment_bulk_attendanceDTO.modifiedBy);
					}
					
					if(mapOfRecruitment_bulk_attendanceDTOTolastModificationTime.containsKey(oldRecruitment_bulk_attendanceDTO.lastModificationTime)) {
						mapOfRecruitment_bulk_attendanceDTOTolastModificationTime.get(oldRecruitment_bulk_attendanceDTO.lastModificationTime).remove(oldRecruitment_bulk_attendanceDTO);
					}
					if(mapOfRecruitment_bulk_attendanceDTOTolastModificationTime.get(oldRecruitment_bulk_attendanceDTO.lastModificationTime).isEmpty()) {
						mapOfRecruitment_bulk_attendanceDTOTolastModificationTime.remove(oldRecruitment_bulk_attendanceDTO.lastModificationTime);
					}
					
					
				}
				if(recruitment_bulk_attendanceDTO.isDeleted == 0) 
				{
					
					mapOfRecruitment_bulk_attendanceDTOToid.put(recruitment_bulk_attendanceDTO.id, recruitment_bulk_attendanceDTO);
				
					if( ! mapOfRecruitment_bulk_attendanceDTOTojobId.containsKey(recruitment_bulk_attendanceDTO.jobId)) {
						mapOfRecruitment_bulk_attendanceDTOTojobId.put(recruitment_bulk_attendanceDTO.jobId, new HashSet<>());
					}
					mapOfRecruitment_bulk_attendanceDTOTojobId.get(recruitment_bulk_attendanceDTO.jobId).add(recruitment_bulk_attendanceDTO);
					
					if( ! mapOfRecruitment_bulk_attendanceDTOTolevelId.containsKey(recruitment_bulk_attendanceDTO.levelId)) {
						mapOfRecruitment_bulk_attendanceDTOTolevelId.put(recruitment_bulk_attendanceDTO.levelId, new HashSet<>());
					}
					mapOfRecruitment_bulk_attendanceDTOTolevelId.get(recruitment_bulk_attendanceDTO.levelId).add(recruitment_bulk_attendanceDTO);
					
					if( ! mapOfRecruitment_bulk_attendanceDTOToroll.containsKey(recruitment_bulk_attendanceDTO.roll)) {
						mapOfRecruitment_bulk_attendanceDTOToroll.put(recruitment_bulk_attendanceDTO.roll, new HashSet<>());
					}
					mapOfRecruitment_bulk_attendanceDTOToroll.get(recruitment_bulk_attendanceDTO.roll).add(recruitment_bulk_attendanceDTO);
					
					if( ! mapOfRecruitment_bulk_attendanceDTOTostatus.containsKey(recruitment_bulk_attendanceDTO.status)) {
						mapOfRecruitment_bulk_attendanceDTOTostatus.put(recruitment_bulk_attendanceDTO.status, new HashSet<>());
					}
					mapOfRecruitment_bulk_attendanceDTOTostatus.get(recruitment_bulk_attendanceDTO.status).add(recruitment_bulk_attendanceDTO);
					
					if( ! mapOfRecruitment_bulk_attendanceDTOToinsertionDate.containsKey(recruitment_bulk_attendanceDTO.insertionDate)) {
						mapOfRecruitment_bulk_attendanceDTOToinsertionDate.put(recruitment_bulk_attendanceDTO.insertionDate, new HashSet<>());
					}
					mapOfRecruitment_bulk_attendanceDTOToinsertionDate.get(recruitment_bulk_attendanceDTO.insertionDate).add(recruitment_bulk_attendanceDTO);
					
					if( ! mapOfRecruitment_bulk_attendanceDTOToinsertedBy.containsKey(recruitment_bulk_attendanceDTO.insertedBy)) {
						mapOfRecruitment_bulk_attendanceDTOToinsertedBy.put(recruitment_bulk_attendanceDTO.insertedBy, new HashSet<>());
					}
					mapOfRecruitment_bulk_attendanceDTOToinsertedBy.get(recruitment_bulk_attendanceDTO.insertedBy).add(recruitment_bulk_attendanceDTO);
					
					if( ! mapOfRecruitment_bulk_attendanceDTOTomodifiedBy.containsKey(recruitment_bulk_attendanceDTO.modifiedBy)) {
						mapOfRecruitment_bulk_attendanceDTOTomodifiedBy.put(recruitment_bulk_attendanceDTO.modifiedBy, new HashSet<>());
					}
					mapOfRecruitment_bulk_attendanceDTOTomodifiedBy.get(recruitment_bulk_attendanceDTO.modifiedBy).add(recruitment_bulk_attendanceDTO);
					
					if( ! mapOfRecruitment_bulk_attendanceDTOTolastModificationTime.containsKey(recruitment_bulk_attendanceDTO.lastModificationTime)) {
						mapOfRecruitment_bulk_attendanceDTOTolastModificationTime.put(recruitment_bulk_attendanceDTO.lastModificationTime, new HashSet<>());
					}
					mapOfRecruitment_bulk_attendanceDTOTolastModificationTime.get(recruitment_bulk_attendanceDTO.lastModificationTime).add(recruitment_bulk_attendanceDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Recruitment_bulk_attendanceDTO> getRecruitment_bulk_attendanceList() {
		List <Recruitment_bulk_attendanceDTO> recruitment_bulk_attendances = new ArrayList<Recruitment_bulk_attendanceDTO>(this.mapOfRecruitment_bulk_attendanceDTOToid.values());
		return recruitment_bulk_attendances;
	}
	
	
	public Recruitment_bulk_attendanceDTO getRecruitment_bulk_attendanceDTOByid( long id){
		return mapOfRecruitment_bulk_attendanceDTOToid.get(id);
	}
	
	
	public List<Recruitment_bulk_attendanceDTO> getRecruitment_bulk_attendanceDTOByjob_id(long job_id) {
		return new ArrayList<>( mapOfRecruitment_bulk_attendanceDTOTojobId.getOrDefault(job_id,new HashSet<>()));
	}
	
	
	public List<Recruitment_bulk_attendanceDTO> getRecruitment_bulk_attendanceDTOBylevel_id(long level_id) {
		return new ArrayList<>( mapOfRecruitment_bulk_attendanceDTOTolevelId.getOrDefault(level_id,new HashSet<>()));
	}
	
	
	public List<Recruitment_bulk_attendanceDTO> getRecruitment_bulk_attendanceDTOByroll(long roll) {
		return new ArrayList<>( mapOfRecruitment_bulk_attendanceDTOToroll.getOrDefault(roll,new HashSet<>()));
	}
	
	
	public List<Recruitment_bulk_attendanceDTO> getRecruitment_bulk_attendanceDTOBystatus(long status) {
		return new ArrayList<>( mapOfRecruitment_bulk_attendanceDTOTostatus.getOrDefault(status,new HashSet<>()));
	}
	
	
	public List<Recruitment_bulk_attendanceDTO> getRecruitment_bulk_attendanceDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfRecruitment_bulk_attendanceDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Recruitment_bulk_attendanceDTO> getRecruitment_bulk_attendanceDTOByinserted_by(String inserted_by) {
		return new ArrayList<>( mapOfRecruitment_bulk_attendanceDTOToinsertedBy.getOrDefault(inserted_by,new HashSet<>()));
	}
	
	
	public List<Recruitment_bulk_attendanceDTO> getRecruitment_bulk_attendanceDTOBymodified_by(String modified_by) {
		return new ArrayList<>( mapOfRecruitment_bulk_attendanceDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Recruitment_bulk_attendanceDTO> getRecruitment_bulk_attendanceDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfRecruitment_bulk_attendanceDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "recruitment_bulk_attendance";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


