package recruitment_bulk_attendance;

import java.io.IOException;
import java.io.*;
import java.net.URL;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import job_applicant_application.Job_applicant_applicationDAO;
import job_applicant_application.Job_applicant_applicationDTO;
import language.LC;
import language.LM;
import org.apache.log4j.Logger;

import login.LoginDTO;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.*;
import org.json.JSONObject;
import permission.MenuConstants;
import recruitment_job_specific_exam_type.RecruitmentJobSpecificExamTypeDTO;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;



import java.util.ArrayList;
import java.util.Iterator;


import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import util.UtilCharacter;

import static sessionmanager.SessionConstants.AttendanceStatusMap;


/**
 * Servlet implementation class Recruitment_bulk_attendanceServlet
 */
@WebServlet("/Recruitment_bulk_attendanceServlet")
@MultipartConfig
public class Recruitment_bulk_attendanceServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Recruitment_bulk_attendanceServlet.class);

    String tableName = "recruitment_bulk_attendance";
	String parentTableName = "job_applicant_application";

	Recruitment_bulk_attendanceDAO recruitment_bulk_attendanceDAO;
	Job_applicant_applicationDAO job_applicant_applicationDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Recruitment_bulk_attendanceServlet()
	{
        super();
    	try
    	{
			recruitment_bulk_attendanceDAO = new Recruitment_bulk_attendanceDAO(tableName);
			job_applicant_applicationDAO = new Job_applicant_applicationDAO(parentTableName);
			commonRequestHandler = new CommonRequestHandler(recruitment_bulk_attendanceDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");

            if (actionType.equals("getCandidateAttendanceExcelList")) {

                String actionMessage = request.getParameter("message");

                if(actionMessage!=null && actionMessage.equalsIgnoreCase("success")){
                    //System.out.println("IN ALL ROW success: "+actionMessage);
//                    response.setContentType("text/html");
//                    PrintWriter success = response.getWriter();
//                    success.println("<div class=\"alert alert-success\" role=\"alert\"> All row insetred </div>");

                }

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_CANDIDATE_LIST)) {
                    getCandidateList(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            }

			else if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_ATTENDANCE_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_ATTENDANCE_UPDATE))
				{
					getRecruitment_bulk_attendance(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getUploadPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_ATTENDANCE_UPLOAD))
				{
					commonRequestHandler.geUploadPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_ATTENDANCE_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchRecruitment_bulk_attendance(request, response, isPermanentTable, filter);
						}
						else
						{
							searchRecruitment_bulk_attendance(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchRecruitment_bulk_attendance(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_ATTENDANCE_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	public ArrayList<Recruitment_bulk_attendanceDTO> ReadXLsToArraylist(HttpServletRequest request, String fileName) throws IOException
	{
		String path = getServletContext().getRealPath("/img2/");
	    File excelFile = new File(path + File.separator
                + fileName);
	    FileInputStream fis = new FileInputStream(excelFile);


	    XSSFWorkbook workbook = new XSSFWorkbook(fis);
	    XSSFSheet sheet = workbook.getSheetAt(0);
	    Iterator<Row> rowIt = sheet.iterator();
	    ArrayList<String> Rows = new ArrayList<String>();
	    ArrayList<Recruitment_bulk_attendanceDTO> recruitment_bulk_attendanceDTOs = new ArrayList<Recruitment_bulk_attendanceDTO>();


	    Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO;

		String failureMessage = "";
	    int i = 0;

		Recruitment_bulk_attendanceMAPS recruitment_bulk_attendanceMAPS = new Recruitment_bulk_attendanceMAPS("recruitment_bulk_attendance");
	    while(rowIt.hasNext())
	    {
			Row row = rowIt.next();

			Iterator<Cell> cellIterator = row.cellIterator();

			recruitment_bulk_attendanceDTO = new Recruitment_bulk_attendanceDTO();

			int j = 0;

			try
			{
				while (cellIterator.hasNext())
				{
					Cell cell = cellIterator.next();


					if(i == 0)
					{
						Rows.add(cell.toString());
						System.out.println("Rows found: " + cell + " ");
					}
					else
					{

						String rowName = Rows.get(j).toLowerCase();
						System.out.println("rowname: " + rowName + " rowname from map = " + recruitment_bulk_attendanceMAPS.java_Text_map.get(rowName));

						if(rowName == null || rowName.equalsIgnoreCase(""))
						{
							System.out.println("null row name");
							break;
						}
						if(cell == null || cell.toString().equalsIgnoreCase(""))
						{
							System.out.println("null cell");
							j++;
							continue;
						}
						else
						{
							System.out.println("Inserting Value = " + cell + " to row " + rowName );

						}
						if(recruitment_bulk_attendanceMAPS.java_Text_map.get(rowName) != null && recruitment_bulk_attendanceMAPS.java_Text_map.get(rowName).equalsIgnoreCase("id"))
						{
							recruitment_bulk_attendanceDTO.id = (long)Double.parseDouble(cell.toString());
						}
						else if(recruitment_bulk_attendanceMAPS.java_Text_map.get(rowName) != null && recruitment_bulk_attendanceMAPS.java_Text_map.get(rowName).equalsIgnoreCase("jobId"))
						{
							recruitment_bulk_attendanceDTO.jobId = (long)Double.parseDouble(cell.toString());
						}
						else if(recruitment_bulk_attendanceMAPS.java_Text_map.get(rowName) != null && recruitment_bulk_attendanceMAPS.java_Text_map.get(rowName).equalsIgnoreCase("levelId"))
						{
							recruitment_bulk_attendanceDTO.levelId = (long)Double.parseDouble(cell.toString());
						}
						else if(recruitment_bulk_attendanceMAPS.java_Text_map.get(rowName) != null && recruitment_bulk_attendanceMAPS.java_Text_map.get(rowName).equalsIgnoreCase("roll"))
						{
							recruitment_bulk_attendanceDTO.roll = (long)Double.parseDouble(cell.toString());
							//recruitment_bulk_attendanceDTO.roll = Math.round(recruitment_bulk_attendanceDTO.roll);
						}
						else if(recruitment_bulk_attendanceMAPS.java_Text_map.get(rowName) != null && recruitment_bulk_attendanceMAPS.java_Text_map.get(rowName).equalsIgnoreCase("status"))
						{

							for (Map.Entry<Integer,String> entry : AttendanceStatusMap.entrySet())    {

								if(entry.getValue().equalsIgnoreCase(cell.toString())){
									recruitment_bulk_attendanceDTO.status = entry.getKey();
									break;
								}
								else{
								}

							}
							//recruitment_bulk_attendanceDTO.status = (long)Double.parseDouble(cell.toString());
						}
						else if(recruitment_bulk_attendanceMAPS.java_Text_map.get(rowName) != null && recruitment_bulk_attendanceMAPS.java_Text_map.get(rowName).equalsIgnoreCase("insertionDate"))
						{
							recruitment_bulk_attendanceDTO.insertionDate = (long)Double.parseDouble(cell.toString());
						}
						else if(recruitment_bulk_attendanceMAPS.java_Text_map.get(rowName) != null && recruitment_bulk_attendanceMAPS.java_Text_map.get(rowName).equalsIgnoreCase("insertedBy"))
						{
							recruitment_bulk_attendanceDTO.insertedBy = (cell.toString());
						}
						else if(recruitment_bulk_attendanceMAPS.java_Text_map.get(rowName) != null && recruitment_bulk_attendanceMAPS.java_Text_map.get(rowName).equalsIgnoreCase("modifiedBy"))
						{
							recruitment_bulk_attendanceDTO.modifiedBy = (cell.toString());
						}
					}
					j ++;

				  }


				  if(i != 0)
				  {
					  System.out.println("INSERTING to the list: " + recruitment_bulk_attendanceDTO);
					  recruitment_bulk_attendanceDTOs.add(recruitment_bulk_attendanceDTO);
				  }
			}
			catch (Exception e)
			{
				e.printStackTrace();
				failureMessage += (i + 1) + " ";
			}
			i ++;

			System.out.println();

	    }
		if(failureMessage.equalsIgnoreCase(""))
		{
			failureMessage = " Successfully parsed all rows";
		}
		else
		{
			failureMessage = " Failed on rows: " + failureMessage;
		}
		request.setAttribute("failureMessage", failureMessage);

	    workbook.close();
	    fis.close();
	    return recruitment_bulk_attendanceDTOs;

	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if (actionType.equals("DownloadTemplate")) {

				String isDownload = request.getParameter("download_excel");
				String isUpload = request.getParameter("upload_excel");
				String job_id = request.getParameter("job");
				String level_id = request.getParameter("level");
				String job_name = request.getParameter("job_name");
				String level_name = request.getParameter("level_name");

				Random r = new Random();

				if (isDownload != null /*&& isDownload.equalsIgnoreCase("Download Excel")*/) {

//					String subject = "Ed Sheeran";
//					URL url = new URL("https://en.wikipedia.org/w/api.php?action=query&prop=extracts&format=json&exsentences=1&exintro=&explaintext=&exsectionformat=plain&titles=" + subject.replace(" ", "%20"));
//					String text = "";
//					try (BufferedReader br = new BufferedReader(new InputStreamReader(url.openConnection().getInputStream()))) {
//						String line = null;
//						while (null != (line = br.readLine())) {
//							line = line.trim();
//							if (true) {
//								text += line;
//							}
//						}
//					}
//
//					System.out.println("text = " + text);
//					JSONObject json = new JSONObject(text);
//					JSONObject query = json.getJSONObject("query");
//					JSONObject pages = query.getJSONObject("pages");
//					for(String key: pages.keySet()) {
//						System.out.println("key = " + key);
//						JSONObject page = pages.getJSONObject(key);
//						String extract = page.getString("extract");
//						System.out.println("extract = " + extract);
//					}

					List<Job_applicant_applicationDTO> recruitment_bulk_attendanceDTO_job_level = job_applicant_applicationDAO.getIDDTOsByJobLevelID(Integer.parseInt(job_id),Integer.parseInt(level_id));

					String file_name = "attendance_"+job_name+"_"+level_name+"_bulk_attendance.xlsx";
					response.setContentType("application/vnd.ms-excel");
					response.setHeader("Content-Disposition", "attachment; filename="+file_name);

					List<String>  attendanceStatus  = new ArrayList<String>(AttendanceStatusMap.values());

					XSSFWorkbook workbook    = UtilCharacter.newWorkBook();
					Sheet sheet = UtilCharacter.newSheet(workbook, LM.getText(LC.CANDIDATE_LIST_CANDIDATE_LIST,loginDTO));

					int row_index=0;
					int cell_col_index=0;
					boolean addMerged=false;
					Row newRow = sheet.createRow(row_index);
					IndexedColors indexedColor = IndexedColors.BLACK1;
					boolean isBold=true;
					int languageID = LM.getLanguageIDByUserDTO(userDTO);
					String Language = languageID == 1?"english":"bangla";

					UtilCharacter.alreadyCreatedRow(workbook,sheet,"Identity",row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,isBold);
					UtilCharacter.alreadyCreatedRow(workbook,sheet,"Roll",row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,isBold);
					UtilCharacter.alreadyCreatedRow(workbook,sheet,"Status",row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,isBold);



					int k = 0;
					int row_i=0;
					int col_j=0;
					isBold=false;
					for (Job_applicant_applicationDTO records:recruitment_bulk_attendanceDTO_job_level){

						row_index++;
						cell_col_index=0;
						newRow = sheet.createRow(row_index);
						UtilCharacter.alreadyCreatedRow(workbook,sheet,UtilCharacter.getDataByLanguage(Language,records.applicant_name_bn+", "+records.father_name,records.applicant_name_en+", "+records.father_name),row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,isBold);
						UtilCharacter.alreadyCreatedRow(workbook,sheet,String.valueOf(records.rollNumber),row_index,row_index,cell_col_index,cell_col_index++,addMerged,newRow,indexedColor,isBold);
						Cell cell = UtilCharacter.alreadyCreatedRow(workbook,sheet,"",row_index,row_index,cell_col_index,cell_col_index,addMerged,newRow,indexedColor,isBold);
//						row_i++;
//						col_j++;
//						Row row = sheet.createRow(row_i);
//						int applicant_id = (int) records.iD;
//
//						k = 0;
//
//						Cell cell = row.createCell(k);
//						cell.setCellValue(Integer.parseInt(String.valueOf(records.rollNumber)));
//
//						k++;
//						cell = row.createCell(k);
						cell.setCellValue(attendanceStatus.get(0));
						XSSFDataValidationHelper dvHelper = new
								XSSFDataValidationHelper((XSSFSheet) sheet);
						XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint)
								dvHelper.createExplicitListConstraint(attendanceStatus.toArray(new String[0]));
						CellRangeAddressList addressList = new CellRangeAddressList(row_index,row_index,cell_col_index,cell_col_index);
						XSSFDataValidation validation = (XSSFDataValidation)dvHelper.createValidation(
								dvConstraint, addressList);
						validation.setShowErrorBox(true);
						sheet.addValidationData(validation);

					}

					workbook.write(response.getOutputStream()); // Write workbook to response.
					workbook.close();

				}

			}
			else if(actionType.equals("excelReviewWithoutUpload")){

				String job_id = request.getParameter("job_id");
				String level_id = request.getParameter("level_id");
				System.out.println("new job_id: "+job_id+" level: "+level_id);
				List<Job_applicant_applicationDTO> recruitment_bulk_attendanceDTO_job_level = job_applicant_applicationDAO.getIDDTOsByJobLevelID(Integer.parseInt(job_id),Integer.parseInt(level_id));
				List<String>  attendanceStatus  = new ArrayList<String>(AttendanceStatusMap.values());
				ArrayList<Recruitment_bulk_attendanceDTO> recruitment_bulk_attendanceDTOs = new ArrayList<Recruitment_bulk_attendanceDTO>();
				Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO;


				for(Job_applicant_applicationDTO dto:recruitment_bulk_attendanceDTO_job_level)  {
					recruitment_bulk_attendanceDTO = new Recruitment_bulk_attendanceDTO();
					recruitment_bulk_attendanceDTO.roll = Long.parseLong(dto.rollNumber);
					recruitment_bulk_attendanceDTOs.add(recruitment_bulk_attendanceDTO);
				}

				//ArrayList<Recruitment_bulk_attendanceDTO> recruitment_bulk_attendanceDTOs = ReadXLsToArraylist(request, FileName);
				HttpSession session = request.getSession(true);
				session.setAttribute("recruitment_bulk_attendanceDTOs", recruitment_bulk_attendanceDTOs);
				RequestDispatcher rd = request.getRequestDispatcher("recruitment_bulk_attendance/recruitment_bulk_attendanceReview.jsp?actionType=edit");
				rd.forward(request, response);

			}
			else if(actionType.equals("checkJobLevelExistence"))
			{
				String job = request.getParameter("job_id");
				String level = request.getParameter("level_id");
				int job_id = Integer.parseInt(job);
				int lebel_id = Integer.parseInt(level);

				int student_existence=job_applicant_applicationDAO.JobLevelIdExistence(job_id,lebel_id);
				int attendace_existence = recruitment_bulk_attendanceDAO.JobLevelAttendanceExistence(job_id,lebel_id);
				//System.out.println("attendace_existence: "+attendace_existence);
				PrintWriter out = response.getWriter();
				out.println(student_existence+","+attendace_existence);
				out.close();
			}
			else if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_ATTENDANCE_ADD))
				{
					System.out.println("going to  addRecruitment_bulk_attendance ");
					addRecruitment_bulk_attendance(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addRecruitment_bulk_attendance ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_ATTENDANCE_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addRecruitment_bulk_attendance ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_ATTENDANCE_UPDATE))
				{
					addRecruitment_bulk_attendance(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("upload"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_ATTENDANCE_UPLOAD))
				{
					uploadRecruitment_bulk_attendance(request, response, false);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("uploadConfirmed"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_ATTENDANCE_UPLOAD))
				{
					System.out.println("uploadConfirmed");
					addRecruitment_bulk_attendances(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_ATTENDANCE_SEARCH))
				{
					searchRecruitment_bulk_attendance(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO = recruitment_bulk_attendanceDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(recruitment_bulk_attendanceDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	private void uploadRecruitment_bulk_attendance(HttpServletRequest request, HttpServletResponse response, Boolean addFlag) throws IOException
	{
		System.out.println("%%%% ajax upload called");
		Part filePart_recruitment_bulk_attendanceDatabase;
		try
		{
			filePart_recruitment_bulk_attendanceDatabase = request.getPart("testing_excelDatabase");
			String Value = commonRequestHandler.getFileName(filePart_recruitment_bulk_attendanceDatabase);
			System.out.println("recruitment_bulk_attendanceDatabase = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					String FileName = "recruitment_bulk_attendancedatabase";
					String path = getServletContext().getRealPath("/img2/");
					Utils.uploadFile(filePart_recruitment_bulk_attendanceDatabase, FileName, path);
					ArrayList<Recruitment_bulk_attendanceDTO> recruitment_bulk_attendanceDTOs = ReadXLsToArraylist(request, FileName);
					HttpSession session = request.getSession(true);
					session.setAttribute("recruitment_bulk_attendanceDTOs", recruitment_bulk_attendanceDTOs);

					RequestDispatcher rd = request.getRequestDispatcher("recruitment_bulk_attendance/recruitment_bulk_attendanceReview.jsp?actionType=edit");
					rd.forward(request, response);
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	private void addRecruitment_bulk_attendances(HttpServletRequest request, HttpServletResponse response) throws IOException
		{
		 String[] paramValues = request.getParameterValues("id");
         for (int i = 0; i < paramValues.length; i++)
         {
			String paramValue = paramValues[i];
//			int isPresent = recruitment_bulk_attendanceDAO.MapGetPresentKey();
//			long job_id = 0;
//			int level=0;
//			long roll;
			Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO = new Recruitment_bulk_attendanceDTO();

			Job_applicant_applicationDTO  job_applicant_applicationDTO  = job_applicant_applicationDAO.getDTOByJobIdAndLevelAndRollNumber(Long.parseLong(request.getParameterValues("jobId")[i]),Long.parseLong(request.getParameterValues("levelId")[i]),request.getParameterValues("roll")[i]);

			try
			{

				if(job_applicant_applicationDTO!=null) {
					String Value = "";

					if (request.getParameterValues("id") != null) {
						Value = request.getParameterValues("id")[i];
						if (Value != null) {
							Value = Jsoup.clean(Value, Whitelist.simpleText());
						}
						System.out.println("id = " + Value);
						if (Value != null && !Value.equalsIgnoreCase("")) {
							recruitment_bulk_attendanceDTO.id = Long.parseLong(Value);
						} else {
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
					}
					if (request.getParameterValues("jobId") != null) {
						Value = request.getParameterValues("jobId")[i];
						if (Value != null) {
							Value = Jsoup.clean(Value, Whitelist.simpleText());
						}
						System.out.println("jobId = " + Value);
						if (Value != null && !Value.equalsIgnoreCase("")) {
							recruitment_bulk_attendanceDTO.jobId = Long.parseLong(Value);
							//job_applicant_applicationDTO.jobId = recruitment_bulk_attendanceDTO.jobId;
						} else {
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
					}
					if (request.getParameterValues("levelId") != null) {
						Value = request.getParameterValues("levelId")[i];
						if (Value != null) {
							Value = Jsoup.clean(Value, Whitelist.simpleText());
						}
						System.out.println("levelId = " + Value);
						if (Value != null && !Value.equalsIgnoreCase("")) {
							recruitment_bulk_attendanceDTO.levelId = Long.parseLong(Value);
							//job_applicant_applicationDTO.level = Integer.parseInt(Value);
						} else {
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
					}
					if (request.getParameterValues("roll") != null) {
						Value = request.getParameterValues("roll")[i];
						if (Value != null) {
							Value = Jsoup.clean(Value, Whitelist.simpleText());
						}
						System.out.println("roll = " + Value);
						if (Value != null && !Value.equalsIgnoreCase("")) {
							recruitment_bulk_attendanceDTO.roll = Long.parseLong(Value);
							//job_applicant_applicationDTO.rollNumber = Value;
							//job_applicant_applicationDTO.iD =  recruitment_bulk_attendanceDTO.roll-1000;
						} else {
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
					}
					if (request.getParameterValues("status") != null) {
						Value = request.getParameterValues("status")[i];
						if (Value != null) {
							Value = Jsoup.clean(Value, Whitelist.simpleText());
						}
						System.out.println("status = " + Value);
						if (Value != null && !Value.equalsIgnoreCase("")) {
							recruitment_bulk_attendanceDTO.status = Long.parseLong(Value);
							job_applicant_applicationDTO.isPresent = Integer.parseInt(Value);
//						System.out.println("IS PRESENT BOOLEAN :"+job_applicant_applicationDTO.isPresent);
						} else {
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
					}
					if (request.getParameterValues("insertionDate") != null) {
						Value = request.getParameterValues("insertionDate")[i];
						if (Value != null) {
							Value = Jsoup.clean(Value, Whitelist.simpleText());
						}
						System.out.println("insertionDate = " + Value);
						if (Value != null && !Value.equalsIgnoreCase("")) {
							recruitment_bulk_attendanceDTO.insertionDate = Long.parseLong(Value);
						} else {
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
					}
					if (request.getParameterValues("insertedBy") != null) {
						Value = request.getParameterValues("insertedBy")[i];
						if (Value != null) {
							Value = Jsoup.clean(Value, Whitelist.simpleText());
						}
						System.out.println("insertedBy = " + Value);
						if (Value != null && !Value.equalsIgnoreCase("")) {
							recruitment_bulk_attendanceDTO.insertedBy = (Value);
						} else {
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
					}
					if (request.getParameterValues("modifiedBy") != null) {
						Value = request.getParameterValues("modifiedBy")[i];
						if (Value != null) {
							Value = Jsoup.clean(Value, Whitelist.simpleText());
						}
						System.out.println("modifiedBy = " + Value);
						if (Value != null && !Value.equalsIgnoreCase("")) {
							recruitment_bulk_attendanceDTO.modifiedBy = (Value);
						} else {
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
					}


					System.out.println("Done adding  addRecruitment_bulk_attendance dto = " + recruitment_bulk_attendanceDTO);


					recruitment_bulk_attendanceDAO.add(recruitment_bulk_attendanceDTO);
					job_applicant_applicationDAO.update(job_applicant_applicationDTO);


				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

         }
		 response.sendRedirect("Recruitment_bulk_attendanceServlet?actionType=search");
	}

	private void addRecruitment_bulk_attendance(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addRecruitment_bulk_attendance");
			String path = getServletContext().getRealPath("/img2/");
			Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				recruitment_bulk_attendanceDTO = new Recruitment_bulk_attendanceDTO();
			}
			else
			{
				recruitment_bulk_attendanceDTO = recruitment_bulk_attendanceDAO.getDTOByID(Long.parseLong(request.getParameter("id")));
				recruitment_bulk_attendanceDTO.iD = recruitment_bulk_attendanceDTO.id;
			}
			String FileNamePrefix;
			if(addFlag == true)
			{
				FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
			}
			else
			{
				FileNamePrefix = request.getParameter("iD");
			}

			String Value = "";

			Value = request.getParameter("jobId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("jobId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				recruitment_bulk_attendanceDTO.jobId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("levelId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("levelId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				recruitment_bulk_attendanceDTO.levelId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("roll");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("roll = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				recruitment_bulk_attendanceDTO.roll = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("status");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("status = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				recruitment_bulk_attendanceDTO.status = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				recruitment_bulk_attendanceDTO.insertionDate = c.getTimeInMillis();
			}


			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				recruitment_bulk_attendanceDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				recruitment_bulk_attendanceDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addRecruitment_bulk_attendance dto = " + recruitment_bulk_attendanceDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				recruitment_bulk_attendanceDAO.setIsDeleted(recruitment_bulk_attendanceDTO.iD, CommonDTO.OUTDATED);
				returnedID = recruitment_bulk_attendanceDAO.add(recruitment_bulk_attendanceDTO);
				recruitment_bulk_attendanceDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = recruitment_bulk_attendanceDAO.manageWriteOperations(recruitment_bulk_attendanceDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = recruitment_bulk_attendanceDAO.manageWriteOperations(recruitment_bulk_attendanceDTO, SessionConstants.UPDATE, -1, userDTO);
			}









			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getRecruitment_bulk_attendance(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Recruitment_bulk_attendanceServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(recruitment_bulk_attendanceDAO.getDTOByID(returnedID), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}









	private void getRecruitment_bulk_attendance(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getRecruitment_bulk_attendance");
		Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO = null;
		try
		{
			recruitment_bulk_attendanceDTO = recruitment_bulk_attendanceDAO.getDTOByID(id);
			request.setAttribute("ID", recruitment_bulk_attendanceDTO.iD);
			request.setAttribute("recruitment_bulk_attendanceDTO",recruitment_bulk_attendanceDTO);
			request.setAttribute("recruitment_bulk_attendanceDAO",recruitment_bulk_attendanceDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "recruitment_bulk_attendance/recruitment_bulk_attendanceInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "recruitment_bulk_attendance/recruitment_bulk_attendanceSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "recruitment_bulk_attendance/recruitment_bulk_attendanceEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "recruitment_bulk_attendance/recruitment_bulk_attendanceEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getRecruitment_bulk_attendance(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getRecruitment_bulk_attendance(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchRecruitment_bulk_attendance(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchRecruitment_bulk_attendance 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_RECRUITMENT_BULK_ATTENDANCE,
			request,
			recruitment_bulk_attendanceDAO,
			SessionConstants.VIEW_RECRUITMENT_BULK_ATTENDANCE,
			SessionConstants.SEARCH_RECRUITMENT_BULK_ATTENDANCE,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("recruitment_bulk_attendanceDAO",recruitment_bulk_attendanceDAO);
        RequestDispatcher rd ;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to recruitment_bulk_attendance/recruitment_bulk_attendanceApproval.jsp");
	        	rd = request.getRequestDispatcher("recruitment_bulk_attendance/recruitment_bulk_attendanceApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to recruitment_bulk_attendance/recruitment_bulk_attendanceApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("recruitment_bulk_attendance/recruitment_bulk_attendanceApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to recruitment_bulk_attendance/recruitment_bulk_attendanceSearch.jsp");
	        	rd = request.getRequestDispatcher("recruitment_bulk_attendance/recruitment_bulk_attendanceSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to recruitment_bulk_attendance/recruitment_bulk_attendanceSearchForm.jsp");
	        	rd = request.getRequestDispatcher("recruitment_bulk_attendance/recruitment_bulk_attendanceSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
    void getCandidateList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd;
        rd = request.getRequestDispatcher("recruitment_bulk_attendance/recruitment_job_specific_candidate_attendance_excel_list.jsp");
        rd.forward(request, response);
    }

}

