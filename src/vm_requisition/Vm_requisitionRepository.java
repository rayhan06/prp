package vm_requisition;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;
import vm_fuel_request.Vm_fuel_requestDAO;


public class Vm_requisitionRepository implements Repository {
	Vm_requisitionDAO vm_requisitionDAO = null;
	Gson gson;

	public void setDAO(Vm_requisitionDAO vm_requisitionDAO)
	{
		this.vm_requisitionDAO = vm_requisitionDAO;
		gson = new Gson();

	}
	
	
	static Logger logger = Logger.getLogger(Vm_requisitionRepository.class);
	Map<Long, Vm_requisitionDTO>mapOfVm_requisitionDTOToiD;
//	Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOToinsertedByUserId;
//	Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOToinsertedByOrganogramId;
//	Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOToinsertionDate;
//	Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTolastModificationTime;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTosearchColumn;
	Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTorequesterOrgId;
//	Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodecisionByOrgId;
//	Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodriverOrgId;
//	Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTostartDate;
//	Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOToendDate;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTostartAddress;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOToendAddress;
//	Map<Integer, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTovehicleRequisitionPurposeCat;
//	Map<Integer, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTovehicleTypeCat;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTotripDescription;
//	Map<Integer, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTostatus;
//	Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodecisionOn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodecisionDescription;
//	Map<Integer, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTogivenVehicleType;
//	Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTogivenVehicleId;
//	Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOToreceiveDate;
//	Map<Double, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTototalTripDistance;
//	Map<Double, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTototalTripTime;
//	Map<Integer, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTopetrolGiven;
//	Map<Double, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTopetrolAmount;
//	Map<Integer, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTopaymentGiven;
//	Map<Integer, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTopaymentType;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTostartTime;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOToendTime;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOToreceiveTime;
//	Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTorequesterOfficeId;
//	Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodecisionByOfficeId;
//	Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodriverOfficeId;
//	Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTorequesterOfficeUnitId;
//	Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodecisionByOfficeUnitId;
//	Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodriverOfficeUnitId;
    Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTorequesterEmpId;
//	Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodecisionByEmpId;
//	Map<Long, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodriverEmpId;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTorequesterPhoneNum;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodecisionByPhoneNum;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodriverPhoneNum;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTorequesterNameEn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodecisionByNameEn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodriverNameEn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTorequesterNameBn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodecisionByNameBn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodriverNameBn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTorequesterOfficeNameEn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodecisionByOfficeNameEn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodriverOfficeNameEn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTorequesterOfficeNameBn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodecisionByOfficeNameBn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodriverOfficeNameBn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTorequesterOfficeUnitNameEn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodecisionByOfficeUnitNameEn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodriverOfficeUnitNameEn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTorequesterOfficeUnitNameBn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodecisionByOfficeUnitNameBn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodriverOfficeUnitNameBn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTorequesterOfficeUnitOrgNameEn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodecisionByOfficeUnitOrgNameEn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodriverOfficeUnitOrgNameEn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTorequesterOfficeUnitOrgNameBn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodecisionByOfficeUnitOrgNameBn;
//	Map<String, Set<Vm_requisitionDTO> >mapOfVm_requisitionDTOTodriverOfficeUnitOrgNameBn;


	static Vm_requisitionRepository instance = null;  
	private Vm_requisitionRepository(){
		mapOfVm_requisitionDTOToiD = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOToinsertedByUserId = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOToinsertionDate = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTolastModificationTime = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfVm_requisitionDTOTorequesterOrgId = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodecisionByOrgId = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodriverOrgId = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTostartDate = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOToendDate = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTostartAddress = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOToendAddress = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTovehicleRequisitionPurposeCat = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTovehicleTypeCat = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTotripDescription = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTostatus = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodecisionOn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodecisionDescription = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTogivenVehicleType = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTogivenVehicleId = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOToreceiveDate = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTototalTripDistance = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTototalTripTime = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTopetrolGiven = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTopetrolAmount = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTopaymentGiven = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTopaymentType = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTostartTime = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOToendTime = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOToreceiveTime = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTorequesterOfficeId = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodecisionByOfficeId = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodriverOfficeId = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTorequesterOfficeUnitId = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodecisionByOfficeUnitId = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodriverOfficeUnitId = new ConcurrentHashMap<>();
		mapOfVm_requisitionDTOTorequesterEmpId = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodecisionByEmpId = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodriverEmpId = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTorequesterPhoneNum = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodecisionByPhoneNum = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodriverPhoneNum = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTorequesterNameEn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodecisionByNameEn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodriverNameEn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTorequesterNameBn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodecisionByNameBn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodriverNameBn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTorequesterOfficeNameEn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodecisionByOfficeNameEn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodriverOfficeNameEn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTorequesterOfficeNameBn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodecisionByOfficeNameBn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodriverOfficeNameBn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTorequesterOfficeUnitNameEn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodecisionByOfficeUnitNameEn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodriverOfficeUnitNameEn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTorequesterOfficeUnitNameBn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodecisionByOfficeUnitNameBn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodriverOfficeUnitNameBn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTorequesterOfficeUnitOrgNameEn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodecisionByOfficeUnitOrgNameEn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodriverOfficeUnitOrgNameEn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTorequesterOfficeUnitOrgNameBn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodecisionByOfficeUnitOrgNameBn = new ConcurrentHashMap<>();
//		mapOfVm_requisitionDTOTodriverOfficeUnitOrgNameBn = new ConcurrentHashMap<>();

		setDAO(new Vm_requisitionDAO());

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vm_requisitionRepository getInstance(){
		if (instance == null){
			instance = new Vm_requisitionRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(vm_requisitionDAO == null)
		{
			return;
		}
		try {
			List<Vm_requisitionDTO> vm_requisitionDTOs = vm_requisitionDAO.getAllVm_requisition(reloadAll);
			for(Vm_requisitionDTO vm_requisitionDTO : vm_requisitionDTOs) {
				Vm_requisitionDTO oldVm_requisitionDTO = getVm_requisitionDTOByIDWithoutClone(vm_requisitionDTO.iD);
				if( oldVm_requisitionDTO != null ) {
					mapOfVm_requisitionDTOToiD.remove(oldVm_requisitionDTO.iD);
				
//					if(mapOfVm_requisitionDTOToinsertedByUserId.containsKey(oldVm_requisitionDTO.insertedByUserId)) {
//						mapOfVm_requisitionDTOToinsertedByUserId.get(oldVm_requisitionDTO.insertedByUserId).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOToinsertedByUserId.get(oldVm_requisitionDTO.insertedByUserId).isEmpty()) {
//						mapOfVm_requisitionDTOToinsertedByUserId.remove(oldVm_requisitionDTO.insertedByUserId);
//					}
//
//					if(mapOfVm_requisitionDTOToinsertedByOrganogramId.containsKey(oldVm_requisitionDTO.insertedByOrganogramId)) {
//						mapOfVm_requisitionDTOToinsertedByOrganogramId.get(oldVm_requisitionDTO.insertedByOrganogramId).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOToinsertedByOrganogramId.get(oldVm_requisitionDTO.insertedByOrganogramId).isEmpty()) {
//						mapOfVm_requisitionDTOToinsertedByOrganogramId.remove(oldVm_requisitionDTO.insertedByOrganogramId);
//					}
//
//					if(mapOfVm_requisitionDTOToinsertionDate.containsKey(oldVm_requisitionDTO.insertionDate)) {
//						mapOfVm_requisitionDTOToinsertionDate.get(oldVm_requisitionDTO.insertionDate).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOToinsertionDate.get(oldVm_requisitionDTO.insertionDate).isEmpty()) {
//						mapOfVm_requisitionDTOToinsertionDate.remove(oldVm_requisitionDTO.insertionDate);
//					}
//
//					if(mapOfVm_requisitionDTOTolastModificationTime.containsKey(oldVm_requisitionDTO.lastModificationTime)) {
//						mapOfVm_requisitionDTOTolastModificationTime.get(oldVm_requisitionDTO.lastModificationTime).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTolastModificationTime.get(oldVm_requisitionDTO.lastModificationTime).isEmpty()) {
//						mapOfVm_requisitionDTOTolastModificationTime.remove(oldVm_requisitionDTO.lastModificationTime);
//					}
//
//					if(mapOfVm_requisitionDTOTosearchColumn.containsKey(oldVm_requisitionDTO.searchColumn)) {
//						mapOfVm_requisitionDTOTosearchColumn.get(oldVm_requisitionDTO.searchColumn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTosearchColumn.get(oldVm_requisitionDTO.searchColumn).isEmpty()) {
//						mapOfVm_requisitionDTOTosearchColumn.remove(oldVm_requisitionDTO.searchColumn);
//					}
//
					if(mapOfVm_requisitionDTOTorequesterOrgId.containsKey(oldVm_requisitionDTO.requesterOrgId)) {
						mapOfVm_requisitionDTOTorequesterOrgId.get(oldVm_requisitionDTO.requesterOrgId).remove(oldVm_requisitionDTO);
					}
					if(mapOfVm_requisitionDTOTorequesterOrgId.get(oldVm_requisitionDTO.requesterOrgId).isEmpty()) {
						mapOfVm_requisitionDTOTorequesterOrgId.remove(oldVm_requisitionDTO.requesterOrgId);
					}
//
//					if(mapOfVm_requisitionDTOTodecisionByOrgId.containsKey(oldVm_requisitionDTO.decisionByOrgId)) {
//						mapOfVm_requisitionDTOTodecisionByOrgId.get(oldVm_requisitionDTO.decisionByOrgId).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodecisionByOrgId.get(oldVm_requisitionDTO.decisionByOrgId).isEmpty()) {
//						mapOfVm_requisitionDTOTodecisionByOrgId.remove(oldVm_requisitionDTO.decisionByOrgId);
//					}
//
//					if(mapOfVm_requisitionDTOTodriverOrgId.containsKey(oldVm_requisitionDTO.driverOrgId)) {
//						mapOfVm_requisitionDTOTodriverOrgId.get(oldVm_requisitionDTO.driverOrgId).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodriverOrgId.get(oldVm_requisitionDTO.driverOrgId).isEmpty()) {
//						mapOfVm_requisitionDTOTodriverOrgId.remove(oldVm_requisitionDTO.driverOrgId);
//					}
//
//					if(mapOfVm_requisitionDTOTostartDate.containsKey(oldVm_requisitionDTO.startDate)) {
//						mapOfVm_requisitionDTOTostartDate.get(oldVm_requisitionDTO.startDate).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTostartDate.get(oldVm_requisitionDTO.startDate).isEmpty()) {
//						mapOfVm_requisitionDTOTostartDate.remove(oldVm_requisitionDTO.startDate);
//					}
//
//					if(mapOfVm_requisitionDTOToendDate.containsKey(oldVm_requisitionDTO.endDate)) {
//						mapOfVm_requisitionDTOToendDate.get(oldVm_requisitionDTO.endDate).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOToendDate.get(oldVm_requisitionDTO.endDate).isEmpty()) {
//						mapOfVm_requisitionDTOToendDate.remove(oldVm_requisitionDTO.endDate);
//					}
//
//					if(mapOfVm_requisitionDTOTostartAddress.containsKey(oldVm_requisitionDTO.startAddress)) {
//						mapOfVm_requisitionDTOTostartAddress.get(oldVm_requisitionDTO.startAddress).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTostartAddress.get(oldVm_requisitionDTO.startAddress).isEmpty()) {
//						mapOfVm_requisitionDTOTostartAddress.remove(oldVm_requisitionDTO.startAddress);
//					}
//
//					if(mapOfVm_requisitionDTOToendAddress.containsKey(oldVm_requisitionDTO.endAddress)) {
//						mapOfVm_requisitionDTOToendAddress.get(oldVm_requisitionDTO.endAddress).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOToendAddress.get(oldVm_requisitionDTO.endAddress).isEmpty()) {
//						mapOfVm_requisitionDTOToendAddress.remove(oldVm_requisitionDTO.endAddress);
//					}
//
//					if(mapOfVm_requisitionDTOTovehicleRequisitionPurposeCat.containsKey(oldVm_requisitionDTO.vehicleRequisitionPurposeCat)) {
//						mapOfVm_requisitionDTOTovehicleRequisitionPurposeCat.get(oldVm_requisitionDTO.vehicleRequisitionPurposeCat).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTovehicleRequisitionPurposeCat.get(oldVm_requisitionDTO.vehicleRequisitionPurposeCat).isEmpty()) {
//						mapOfVm_requisitionDTOTovehicleRequisitionPurposeCat.remove(oldVm_requisitionDTO.vehicleRequisitionPurposeCat);
//					}
//
//					if(mapOfVm_requisitionDTOTovehicleTypeCat.containsKey(oldVm_requisitionDTO.vehicleTypeCat)) {
//						mapOfVm_requisitionDTOTovehicleTypeCat.get(oldVm_requisitionDTO.vehicleTypeCat).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTovehicleTypeCat.get(oldVm_requisitionDTO.vehicleTypeCat).isEmpty()) {
//						mapOfVm_requisitionDTOTovehicleTypeCat.remove(oldVm_requisitionDTO.vehicleTypeCat);
//					}
//
//					if(mapOfVm_requisitionDTOTotripDescription.containsKey(oldVm_requisitionDTO.tripDescription)) {
//						mapOfVm_requisitionDTOTotripDescription.get(oldVm_requisitionDTO.tripDescription).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTotripDescription.get(oldVm_requisitionDTO.tripDescription).isEmpty()) {
//						mapOfVm_requisitionDTOTotripDescription.remove(oldVm_requisitionDTO.tripDescription);
//					}
//
//					if(mapOfVm_requisitionDTOTostatus.containsKey(oldVm_requisitionDTO.status)) {
//						mapOfVm_requisitionDTOTostatus.get(oldVm_requisitionDTO.status).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTostatus.get(oldVm_requisitionDTO.status).isEmpty()) {
//						mapOfVm_requisitionDTOTostatus.remove(oldVm_requisitionDTO.status);
//					}
//
//					if(mapOfVm_requisitionDTOTodecisionOn.containsKey(oldVm_requisitionDTO.decisionOn)) {
//						mapOfVm_requisitionDTOTodecisionOn.get(oldVm_requisitionDTO.decisionOn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodecisionOn.get(oldVm_requisitionDTO.decisionOn).isEmpty()) {
//						mapOfVm_requisitionDTOTodecisionOn.remove(oldVm_requisitionDTO.decisionOn);
//					}
//
//					if(mapOfVm_requisitionDTOTodecisionDescription.containsKey(oldVm_requisitionDTO.decisionDescription)) {
//						mapOfVm_requisitionDTOTodecisionDescription.get(oldVm_requisitionDTO.decisionDescription).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodecisionDescription.get(oldVm_requisitionDTO.decisionDescription).isEmpty()) {
//						mapOfVm_requisitionDTOTodecisionDescription.remove(oldVm_requisitionDTO.decisionDescription);
//					}
//
//					if(mapOfVm_requisitionDTOTogivenVehicleType.containsKey(oldVm_requisitionDTO.givenVehicleType)) {
//						mapOfVm_requisitionDTOTogivenVehicleType.get(oldVm_requisitionDTO.givenVehicleType).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTogivenVehicleType.get(oldVm_requisitionDTO.givenVehicleType).isEmpty()) {
//						mapOfVm_requisitionDTOTogivenVehicleType.remove(oldVm_requisitionDTO.givenVehicleType);
//					}
//
//					if(mapOfVm_requisitionDTOTogivenVehicleId.containsKey(oldVm_requisitionDTO.givenVehicleId)) {
//						mapOfVm_requisitionDTOTogivenVehicleId.get(oldVm_requisitionDTO.givenVehicleId).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTogivenVehicleId.get(oldVm_requisitionDTO.givenVehicleId).isEmpty()) {
//						mapOfVm_requisitionDTOTogivenVehicleId.remove(oldVm_requisitionDTO.givenVehicleId);
//					}
//
//					if(mapOfVm_requisitionDTOToreceiveDate.containsKey(oldVm_requisitionDTO.receiveDate)) {
//						mapOfVm_requisitionDTOToreceiveDate.get(oldVm_requisitionDTO.receiveDate).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOToreceiveDate.get(oldVm_requisitionDTO.receiveDate).isEmpty()) {
//						mapOfVm_requisitionDTOToreceiveDate.remove(oldVm_requisitionDTO.receiveDate);
//					}
//
//					if(mapOfVm_requisitionDTOTototalTripDistance.containsKey(oldVm_requisitionDTO.totalTripDistance)) {
//						mapOfVm_requisitionDTOTototalTripDistance.get(oldVm_requisitionDTO.totalTripDistance).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTototalTripDistance.get(oldVm_requisitionDTO.totalTripDistance).isEmpty()) {
//						mapOfVm_requisitionDTOTototalTripDistance.remove(oldVm_requisitionDTO.totalTripDistance);
//					}
//
//					if(mapOfVm_requisitionDTOTototalTripTime.containsKey(oldVm_requisitionDTO.totalTripTime)) {
//						mapOfVm_requisitionDTOTototalTripTime.get(oldVm_requisitionDTO.totalTripTime).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTototalTripTime.get(oldVm_requisitionDTO.totalTripTime).isEmpty()) {
//						mapOfVm_requisitionDTOTototalTripTime.remove(oldVm_requisitionDTO.totalTripTime);
//					}
//
//					if(mapOfVm_requisitionDTOTopetrolGiven.containsKey(oldVm_requisitionDTO.petrolGiven)) {
//						mapOfVm_requisitionDTOTopetrolGiven.get(oldVm_requisitionDTO.petrolGiven).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTopetrolGiven.get(oldVm_requisitionDTO.petrolGiven).isEmpty()) {
//						mapOfVm_requisitionDTOTopetrolGiven.remove(oldVm_requisitionDTO.petrolGiven);
//					}
//
//					if(mapOfVm_requisitionDTOTopetrolAmount.containsKey(oldVm_requisitionDTO.petrolAmount)) {
//						mapOfVm_requisitionDTOTopetrolAmount.get(oldVm_requisitionDTO.petrolAmount).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTopetrolAmount.get(oldVm_requisitionDTO.petrolAmount).isEmpty()) {
//						mapOfVm_requisitionDTOTopetrolAmount.remove(oldVm_requisitionDTO.petrolAmount);
//					}
//
//					if(mapOfVm_requisitionDTOTopaymentGiven.containsKey(oldVm_requisitionDTO.paymentGiven)) {
//						mapOfVm_requisitionDTOTopaymentGiven.get(oldVm_requisitionDTO.paymentGiven).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTopaymentGiven.get(oldVm_requisitionDTO.paymentGiven).isEmpty()) {
//						mapOfVm_requisitionDTOTopaymentGiven.remove(oldVm_requisitionDTO.paymentGiven);
//					}
//
//					if(mapOfVm_requisitionDTOTopaymentType.containsKey(oldVm_requisitionDTO.paymentType)) {
//						mapOfVm_requisitionDTOTopaymentType.get(oldVm_requisitionDTO.paymentType).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTopaymentType.get(oldVm_requisitionDTO.paymentType).isEmpty()) {
//						mapOfVm_requisitionDTOTopaymentType.remove(oldVm_requisitionDTO.paymentType);
//					}
//
//					if(mapOfVm_requisitionDTOTostartTime.containsKey(oldVm_requisitionDTO.startTime)) {
//						mapOfVm_requisitionDTOTostartTime.get(oldVm_requisitionDTO.startTime).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTostartTime.get(oldVm_requisitionDTO.startTime).isEmpty()) {
//						mapOfVm_requisitionDTOTostartTime.remove(oldVm_requisitionDTO.startTime);
//					}
//
//					if(mapOfVm_requisitionDTOToendTime.containsKey(oldVm_requisitionDTO.endTime)) {
//						mapOfVm_requisitionDTOToendTime.get(oldVm_requisitionDTO.endTime).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOToendTime.get(oldVm_requisitionDTO.endTime).isEmpty()) {
//						mapOfVm_requisitionDTOToendTime.remove(oldVm_requisitionDTO.endTime);
//					}
//
//					if(mapOfVm_requisitionDTOToreceiveTime.containsKey(oldVm_requisitionDTO.receiveTime)) {
//						mapOfVm_requisitionDTOToreceiveTime.get(oldVm_requisitionDTO.receiveTime).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOToreceiveTime.get(oldVm_requisitionDTO.receiveTime).isEmpty()) {
//						mapOfVm_requisitionDTOToreceiveTime.remove(oldVm_requisitionDTO.receiveTime);
//					}
//
//					if(mapOfVm_requisitionDTOTorequesterOfficeId.containsKey(oldVm_requisitionDTO.requesterOfficeId)) {
//						mapOfVm_requisitionDTOTorequesterOfficeId.get(oldVm_requisitionDTO.requesterOfficeId).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTorequesterOfficeId.get(oldVm_requisitionDTO.requesterOfficeId).isEmpty()) {
//						mapOfVm_requisitionDTOTorequesterOfficeId.remove(oldVm_requisitionDTO.requesterOfficeId);
//					}
//
//					if(mapOfVm_requisitionDTOTodecisionByOfficeId.containsKey(oldVm_requisitionDTO.decisionByOfficeId)) {
//						mapOfVm_requisitionDTOTodecisionByOfficeId.get(oldVm_requisitionDTO.decisionByOfficeId).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodecisionByOfficeId.get(oldVm_requisitionDTO.decisionByOfficeId).isEmpty()) {
//						mapOfVm_requisitionDTOTodecisionByOfficeId.remove(oldVm_requisitionDTO.decisionByOfficeId);
//					}
//
//					if(mapOfVm_requisitionDTOTodriverOfficeId.containsKey(oldVm_requisitionDTO.driverOfficeId)) {
//						mapOfVm_requisitionDTOTodriverOfficeId.get(oldVm_requisitionDTO.driverOfficeId).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodriverOfficeId.get(oldVm_requisitionDTO.driverOfficeId).isEmpty()) {
//						mapOfVm_requisitionDTOTodriverOfficeId.remove(oldVm_requisitionDTO.driverOfficeId);
//					}
//
//					if(mapOfVm_requisitionDTOTorequesterOfficeUnitId.containsKey(oldVm_requisitionDTO.requesterOfficeUnitId)) {
//						mapOfVm_requisitionDTOTorequesterOfficeUnitId.get(oldVm_requisitionDTO.requesterOfficeUnitId).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTorequesterOfficeUnitId.get(oldVm_requisitionDTO.requesterOfficeUnitId).isEmpty()) {
//						mapOfVm_requisitionDTOTorequesterOfficeUnitId.remove(oldVm_requisitionDTO.requesterOfficeUnitId);
//					}
//
//					if(mapOfVm_requisitionDTOTodecisionByOfficeUnitId.containsKey(oldVm_requisitionDTO.decisionByOfficeUnitId)) {
//						mapOfVm_requisitionDTOTodecisionByOfficeUnitId.get(oldVm_requisitionDTO.decisionByOfficeUnitId).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodecisionByOfficeUnitId.get(oldVm_requisitionDTO.decisionByOfficeUnitId).isEmpty()) {
//						mapOfVm_requisitionDTOTodecisionByOfficeUnitId.remove(oldVm_requisitionDTO.decisionByOfficeUnitId);
//					}
//
//					if(mapOfVm_requisitionDTOTodriverOfficeUnitId.containsKey(oldVm_requisitionDTO.driverOfficeUnitId)) {
//						mapOfVm_requisitionDTOTodriverOfficeUnitId.get(oldVm_requisitionDTO.driverOfficeUnitId).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodriverOfficeUnitId.get(oldVm_requisitionDTO.driverOfficeUnitId).isEmpty()) {
//						mapOfVm_requisitionDTOTodriverOfficeUnitId.remove(oldVm_requisitionDTO.driverOfficeUnitId);
//					}
//
					if(mapOfVm_requisitionDTOTorequesterEmpId.containsKey(oldVm_requisitionDTO.requesterEmpId)) {
						mapOfVm_requisitionDTOTorequesterEmpId.get(oldVm_requisitionDTO.requesterEmpId).remove(oldVm_requisitionDTO);
					}
					if(mapOfVm_requisitionDTOTorequesterEmpId.get(oldVm_requisitionDTO.requesterEmpId).isEmpty()) {
						mapOfVm_requisitionDTOTorequesterEmpId.remove(oldVm_requisitionDTO.requesterEmpId);
					}
//
//					if(mapOfVm_requisitionDTOTodecisionByEmpId.containsKey(oldVm_requisitionDTO.decisionByEmpId)) {
//						mapOfVm_requisitionDTOTodecisionByEmpId.get(oldVm_requisitionDTO.decisionByEmpId).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodecisionByEmpId.get(oldVm_requisitionDTO.decisionByEmpId).isEmpty()) {
//						mapOfVm_requisitionDTOTodecisionByEmpId.remove(oldVm_requisitionDTO.decisionByEmpId);
//					}
//
//					if(mapOfVm_requisitionDTOTodriverEmpId.containsKey(oldVm_requisitionDTO.driverEmpId)) {
//						mapOfVm_requisitionDTOTodriverEmpId.get(oldVm_requisitionDTO.driverEmpId).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodriverEmpId.get(oldVm_requisitionDTO.driverEmpId).isEmpty()) {
//						mapOfVm_requisitionDTOTodriverEmpId.remove(oldVm_requisitionDTO.driverEmpId);
//					}
//
//					if(mapOfVm_requisitionDTOTorequesterPhoneNum.containsKey(oldVm_requisitionDTO.requesterPhoneNum)) {
//						mapOfVm_requisitionDTOTorequesterPhoneNum.get(oldVm_requisitionDTO.requesterPhoneNum).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTorequesterPhoneNum.get(oldVm_requisitionDTO.requesterPhoneNum).isEmpty()) {
//						mapOfVm_requisitionDTOTorequesterPhoneNum.remove(oldVm_requisitionDTO.requesterPhoneNum);
//					}
//
//					if(mapOfVm_requisitionDTOTodecisionByPhoneNum.containsKey(oldVm_requisitionDTO.decisionByPhoneNum)) {
//						mapOfVm_requisitionDTOTodecisionByPhoneNum.get(oldVm_requisitionDTO.decisionByPhoneNum).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodecisionByPhoneNum.get(oldVm_requisitionDTO.decisionByPhoneNum).isEmpty()) {
//						mapOfVm_requisitionDTOTodecisionByPhoneNum.remove(oldVm_requisitionDTO.decisionByPhoneNum);
//					}
//
//					if(mapOfVm_requisitionDTOTodriverPhoneNum.containsKey(oldVm_requisitionDTO.driverPhoneNum)) {
//						mapOfVm_requisitionDTOTodriverPhoneNum.get(oldVm_requisitionDTO.driverPhoneNum).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodriverPhoneNum.get(oldVm_requisitionDTO.driverPhoneNum).isEmpty()) {
//						mapOfVm_requisitionDTOTodriverPhoneNum.remove(oldVm_requisitionDTO.driverPhoneNum);
//					}
//
//					if(mapOfVm_requisitionDTOTorequesterNameEn.containsKey(oldVm_requisitionDTO.requesterNameEn)) {
//						mapOfVm_requisitionDTOTorequesterNameEn.get(oldVm_requisitionDTO.requesterNameEn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTorequesterNameEn.get(oldVm_requisitionDTO.requesterNameEn).isEmpty()) {
//						mapOfVm_requisitionDTOTorequesterNameEn.remove(oldVm_requisitionDTO.requesterNameEn);
//					}
//
//					if(mapOfVm_requisitionDTOTodecisionByNameEn.containsKey(oldVm_requisitionDTO.decisionByNameEn)) {
//						mapOfVm_requisitionDTOTodecisionByNameEn.get(oldVm_requisitionDTO.decisionByNameEn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodecisionByNameEn.get(oldVm_requisitionDTO.decisionByNameEn).isEmpty()) {
//						mapOfVm_requisitionDTOTodecisionByNameEn.remove(oldVm_requisitionDTO.decisionByNameEn);
//					}
//
//					if(mapOfVm_requisitionDTOTodriverNameEn.containsKey(oldVm_requisitionDTO.driverNameEn)) {
//						mapOfVm_requisitionDTOTodriverNameEn.get(oldVm_requisitionDTO.driverNameEn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodriverNameEn.get(oldVm_requisitionDTO.driverNameEn).isEmpty()) {
//						mapOfVm_requisitionDTOTodriverNameEn.remove(oldVm_requisitionDTO.driverNameEn);
//					}
//
//					if(mapOfVm_requisitionDTOTorequesterNameBn.containsKey(oldVm_requisitionDTO.requesterNameBn)) {
//						mapOfVm_requisitionDTOTorequesterNameBn.get(oldVm_requisitionDTO.requesterNameBn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTorequesterNameBn.get(oldVm_requisitionDTO.requesterNameBn).isEmpty()) {
//						mapOfVm_requisitionDTOTorequesterNameBn.remove(oldVm_requisitionDTO.requesterNameBn);
//					}
//
//					if(mapOfVm_requisitionDTOTodecisionByNameBn.containsKey(oldVm_requisitionDTO.decisionByNameBn)) {
//						mapOfVm_requisitionDTOTodecisionByNameBn.get(oldVm_requisitionDTO.decisionByNameBn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodecisionByNameBn.get(oldVm_requisitionDTO.decisionByNameBn).isEmpty()) {
//						mapOfVm_requisitionDTOTodecisionByNameBn.remove(oldVm_requisitionDTO.decisionByNameBn);
//					}
//
//					if(mapOfVm_requisitionDTOTodriverNameBn.containsKey(oldVm_requisitionDTO.driverNameBn)) {
//						mapOfVm_requisitionDTOTodriverNameBn.get(oldVm_requisitionDTO.driverNameBn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodriverNameBn.get(oldVm_requisitionDTO.driverNameBn).isEmpty()) {
//						mapOfVm_requisitionDTOTodriverNameBn.remove(oldVm_requisitionDTO.driverNameBn);
//					}
//
//					if(mapOfVm_requisitionDTOTorequesterOfficeNameEn.containsKey(oldVm_requisitionDTO.requesterOfficeNameEn)) {
//						mapOfVm_requisitionDTOTorequesterOfficeNameEn.get(oldVm_requisitionDTO.requesterOfficeNameEn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTorequesterOfficeNameEn.get(oldVm_requisitionDTO.requesterOfficeNameEn).isEmpty()) {
//						mapOfVm_requisitionDTOTorequesterOfficeNameEn.remove(oldVm_requisitionDTO.requesterOfficeNameEn);
//					}
//
//					if(mapOfVm_requisitionDTOTodecisionByOfficeNameEn.containsKey(oldVm_requisitionDTO.decisionByOfficeNameEn)) {
//						mapOfVm_requisitionDTOTodecisionByOfficeNameEn.get(oldVm_requisitionDTO.decisionByOfficeNameEn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodecisionByOfficeNameEn.get(oldVm_requisitionDTO.decisionByOfficeNameEn).isEmpty()) {
//						mapOfVm_requisitionDTOTodecisionByOfficeNameEn.remove(oldVm_requisitionDTO.decisionByOfficeNameEn);
//					}
//
//					if(mapOfVm_requisitionDTOTodriverOfficeNameEn.containsKey(oldVm_requisitionDTO.driverOfficeNameEn)) {
//						mapOfVm_requisitionDTOTodriverOfficeNameEn.get(oldVm_requisitionDTO.driverOfficeNameEn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodriverOfficeNameEn.get(oldVm_requisitionDTO.driverOfficeNameEn).isEmpty()) {
//						mapOfVm_requisitionDTOTodriverOfficeNameEn.remove(oldVm_requisitionDTO.driverOfficeNameEn);
//					}
//
//					if(mapOfVm_requisitionDTOTorequesterOfficeNameBn.containsKey(oldVm_requisitionDTO.requesterOfficeNameBn)) {
//						mapOfVm_requisitionDTOTorequesterOfficeNameBn.get(oldVm_requisitionDTO.requesterOfficeNameBn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTorequesterOfficeNameBn.get(oldVm_requisitionDTO.requesterOfficeNameBn).isEmpty()) {
//						mapOfVm_requisitionDTOTorequesterOfficeNameBn.remove(oldVm_requisitionDTO.requesterOfficeNameBn);
//					}
//
//					if(mapOfVm_requisitionDTOTodecisionByOfficeNameBn.containsKey(oldVm_requisitionDTO.decisionByOfficeNameBn)) {
//						mapOfVm_requisitionDTOTodecisionByOfficeNameBn.get(oldVm_requisitionDTO.decisionByOfficeNameBn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodecisionByOfficeNameBn.get(oldVm_requisitionDTO.decisionByOfficeNameBn).isEmpty()) {
//						mapOfVm_requisitionDTOTodecisionByOfficeNameBn.remove(oldVm_requisitionDTO.decisionByOfficeNameBn);
//					}
//
//					if(mapOfVm_requisitionDTOTodriverOfficeNameBn.containsKey(oldVm_requisitionDTO.driverOfficeNameBn)) {
//						mapOfVm_requisitionDTOTodriverOfficeNameBn.get(oldVm_requisitionDTO.driverOfficeNameBn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodriverOfficeNameBn.get(oldVm_requisitionDTO.driverOfficeNameBn).isEmpty()) {
//						mapOfVm_requisitionDTOTodriverOfficeNameBn.remove(oldVm_requisitionDTO.driverOfficeNameBn);
//					}
//
//					if(mapOfVm_requisitionDTOTorequesterOfficeUnitNameEn.containsKey(oldVm_requisitionDTO.requesterOfficeUnitNameEn)) {
//						mapOfVm_requisitionDTOTorequesterOfficeUnitNameEn.get(oldVm_requisitionDTO.requesterOfficeUnitNameEn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTorequesterOfficeUnitNameEn.get(oldVm_requisitionDTO.requesterOfficeUnitNameEn).isEmpty()) {
//						mapOfVm_requisitionDTOTorequesterOfficeUnitNameEn.remove(oldVm_requisitionDTO.requesterOfficeUnitNameEn);
//					}
//
//					if(mapOfVm_requisitionDTOTodecisionByOfficeUnitNameEn.containsKey(oldVm_requisitionDTO.decisionByOfficeUnitNameEn)) {
//						mapOfVm_requisitionDTOTodecisionByOfficeUnitNameEn.get(oldVm_requisitionDTO.decisionByOfficeUnitNameEn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodecisionByOfficeUnitNameEn.get(oldVm_requisitionDTO.decisionByOfficeUnitNameEn).isEmpty()) {
//						mapOfVm_requisitionDTOTodecisionByOfficeUnitNameEn.remove(oldVm_requisitionDTO.decisionByOfficeUnitNameEn);
//					}
//
//					if(mapOfVm_requisitionDTOTodriverOfficeUnitNameEn.containsKey(oldVm_requisitionDTO.driverOfficeUnitNameEn)) {
//						mapOfVm_requisitionDTOTodriverOfficeUnitNameEn.get(oldVm_requisitionDTO.driverOfficeUnitNameEn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodriverOfficeUnitNameEn.get(oldVm_requisitionDTO.driverOfficeUnitNameEn).isEmpty()) {
//						mapOfVm_requisitionDTOTodriverOfficeUnitNameEn.remove(oldVm_requisitionDTO.driverOfficeUnitNameEn);
//					}
//
//					if(mapOfVm_requisitionDTOTorequesterOfficeUnitNameBn.containsKey(oldVm_requisitionDTO.requesterOfficeUnitNameBn)) {
//						mapOfVm_requisitionDTOTorequesterOfficeUnitNameBn.get(oldVm_requisitionDTO.requesterOfficeUnitNameBn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTorequesterOfficeUnitNameBn.get(oldVm_requisitionDTO.requesterOfficeUnitNameBn).isEmpty()) {
//						mapOfVm_requisitionDTOTorequesterOfficeUnitNameBn.remove(oldVm_requisitionDTO.requesterOfficeUnitNameBn);
//					}
//
//					if(mapOfVm_requisitionDTOTodecisionByOfficeUnitNameBn.containsKey(oldVm_requisitionDTO.decisionByOfficeUnitNameBn)) {
//						mapOfVm_requisitionDTOTodecisionByOfficeUnitNameBn.get(oldVm_requisitionDTO.decisionByOfficeUnitNameBn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodecisionByOfficeUnitNameBn.get(oldVm_requisitionDTO.decisionByOfficeUnitNameBn).isEmpty()) {
//						mapOfVm_requisitionDTOTodecisionByOfficeUnitNameBn.remove(oldVm_requisitionDTO.decisionByOfficeUnitNameBn);
//					}
//
//					if(mapOfVm_requisitionDTOTodriverOfficeUnitNameBn.containsKey(oldVm_requisitionDTO.driverOfficeUnitNameBn)) {
//						mapOfVm_requisitionDTOTodriverOfficeUnitNameBn.get(oldVm_requisitionDTO.driverOfficeUnitNameBn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodriverOfficeUnitNameBn.get(oldVm_requisitionDTO.driverOfficeUnitNameBn).isEmpty()) {
//						mapOfVm_requisitionDTOTodriverOfficeUnitNameBn.remove(oldVm_requisitionDTO.driverOfficeUnitNameBn);
//					}
//
//					if(mapOfVm_requisitionDTOTorequesterOfficeUnitOrgNameEn.containsKey(oldVm_requisitionDTO.requesterOfficeUnitOrgNameEn)) {
//						mapOfVm_requisitionDTOTorequesterOfficeUnitOrgNameEn.get(oldVm_requisitionDTO.requesterOfficeUnitOrgNameEn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTorequesterOfficeUnitOrgNameEn.get(oldVm_requisitionDTO.requesterOfficeUnitOrgNameEn).isEmpty()) {
//						mapOfVm_requisitionDTOTorequesterOfficeUnitOrgNameEn.remove(oldVm_requisitionDTO.requesterOfficeUnitOrgNameEn);
//					}
//
//					if(mapOfVm_requisitionDTOTodecisionByOfficeUnitOrgNameEn.containsKey(oldVm_requisitionDTO.decisionByOfficeUnitOrgNameEn)) {
//						mapOfVm_requisitionDTOTodecisionByOfficeUnitOrgNameEn.get(oldVm_requisitionDTO.decisionByOfficeUnitOrgNameEn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodecisionByOfficeUnitOrgNameEn.get(oldVm_requisitionDTO.decisionByOfficeUnitOrgNameEn).isEmpty()) {
//						mapOfVm_requisitionDTOTodecisionByOfficeUnitOrgNameEn.remove(oldVm_requisitionDTO.decisionByOfficeUnitOrgNameEn);
//					}
//
//					if(mapOfVm_requisitionDTOTodriverOfficeUnitOrgNameEn.containsKey(oldVm_requisitionDTO.driverOfficeUnitOrgNameEn)) {
//						mapOfVm_requisitionDTOTodriverOfficeUnitOrgNameEn.get(oldVm_requisitionDTO.driverOfficeUnitOrgNameEn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodriverOfficeUnitOrgNameEn.get(oldVm_requisitionDTO.driverOfficeUnitOrgNameEn).isEmpty()) {
//						mapOfVm_requisitionDTOTodriverOfficeUnitOrgNameEn.remove(oldVm_requisitionDTO.driverOfficeUnitOrgNameEn);
//					}
//
//					if(mapOfVm_requisitionDTOTorequesterOfficeUnitOrgNameBn.containsKey(oldVm_requisitionDTO.requesterOfficeUnitOrgNameBn)) {
//						mapOfVm_requisitionDTOTorequesterOfficeUnitOrgNameBn.get(oldVm_requisitionDTO.requesterOfficeUnitOrgNameBn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTorequesterOfficeUnitOrgNameBn.get(oldVm_requisitionDTO.requesterOfficeUnitOrgNameBn).isEmpty()) {
//						mapOfVm_requisitionDTOTorequesterOfficeUnitOrgNameBn.remove(oldVm_requisitionDTO.requesterOfficeUnitOrgNameBn);
//					}
//
//					if(mapOfVm_requisitionDTOTodecisionByOfficeUnitOrgNameBn.containsKey(oldVm_requisitionDTO.decisionByOfficeUnitOrgNameBn)) {
//						mapOfVm_requisitionDTOTodecisionByOfficeUnitOrgNameBn.get(oldVm_requisitionDTO.decisionByOfficeUnitOrgNameBn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodecisionByOfficeUnitOrgNameBn.get(oldVm_requisitionDTO.decisionByOfficeUnitOrgNameBn).isEmpty()) {
//						mapOfVm_requisitionDTOTodecisionByOfficeUnitOrgNameBn.remove(oldVm_requisitionDTO.decisionByOfficeUnitOrgNameBn);
//					}
//
//					if(mapOfVm_requisitionDTOTodriverOfficeUnitOrgNameBn.containsKey(oldVm_requisitionDTO.driverOfficeUnitOrgNameBn)) {
//						mapOfVm_requisitionDTOTodriverOfficeUnitOrgNameBn.get(oldVm_requisitionDTO.driverOfficeUnitOrgNameBn).remove(oldVm_requisitionDTO);
//					}
//					if(mapOfVm_requisitionDTOTodriverOfficeUnitOrgNameBn.get(oldVm_requisitionDTO.driverOfficeUnitOrgNameBn).isEmpty()) {
//						mapOfVm_requisitionDTOTodriverOfficeUnitOrgNameBn.remove(oldVm_requisitionDTO.driverOfficeUnitOrgNameBn);
//					}
					
					
				}
				if(vm_requisitionDTO.isDeleted == 0) 
				{
					
					mapOfVm_requisitionDTOToiD.put(vm_requisitionDTO.iD, vm_requisitionDTO);
				
//					if( ! mapOfVm_requisitionDTOToinsertedByUserId.containsKey(vm_requisitionDTO.insertedByUserId)) {
//						mapOfVm_requisitionDTOToinsertedByUserId.put(vm_requisitionDTO.insertedByUserId, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOToinsertedByUserId.get(vm_requisitionDTO.insertedByUserId).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOToinsertedByOrganogramId.containsKey(vm_requisitionDTO.insertedByOrganogramId)) {
//						mapOfVm_requisitionDTOToinsertedByOrganogramId.put(vm_requisitionDTO.insertedByOrganogramId, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOToinsertedByOrganogramId.get(vm_requisitionDTO.insertedByOrganogramId).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOToinsertionDate.containsKey(vm_requisitionDTO.insertionDate)) {
//						mapOfVm_requisitionDTOToinsertionDate.put(vm_requisitionDTO.insertionDate, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOToinsertionDate.get(vm_requisitionDTO.insertionDate).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTolastModificationTime.containsKey(vm_requisitionDTO.lastModificationTime)) {
//						mapOfVm_requisitionDTOTolastModificationTime.put(vm_requisitionDTO.lastModificationTime, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTolastModificationTime.get(vm_requisitionDTO.lastModificationTime).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTosearchColumn.containsKey(vm_requisitionDTO.searchColumn)) {
//						mapOfVm_requisitionDTOTosearchColumn.put(vm_requisitionDTO.searchColumn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTosearchColumn.get(vm_requisitionDTO.searchColumn).add(vm_requisitionDTO);
//
					if( ! mapOfVm_requisitionDTOTorequesterOrgId.containsKey(vm_requisitionDTO.requesterOrgId)) {
						mapOfVm_requisitionDTOTorequesterOrgId.put(vm_requisitionDTO.requesterOrgId, new HashSet<>());
					}
					mapOfVm_requisitionDTOTorequesterOrgId.get(vm_requisitionDTO.requesterOrgId).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodecisionByOrgId.containsKey(vm_requisitionDTO.decisionByOrgId)) {
//						mapOfVm_requisitionDTOTodecisionByOrgId.put(vm_requisitionDTO.decisionByOrgId, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodecisionByOrgId.get(vm_requisitionDTO.decisionByOrgId).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodriverOrgId.containsKey(vm_requisitionDTO.driverOrgId)) {
//						mapOfVm_requisitionDTOTodriverOrgId.put(vm_requisitionDTO.driverOrgId, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodriverOrgId.get(vm_requisitionDTO.driverOrgId).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTostartDate.containsKey(vm_requisitionDTO.startDate)) {
//						mapOfVm_requisitionDTOTostartDate.put(vm_requisitionDTO.startDate, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTostartDate.get(vm_requisitionDTO.startDate).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOToendDate.containsKey(vm_requisitionDTO.endDate)) {
//						mapOfVm_requisitionDTOToendDate.put(vm_requisitionDTO.endDate, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOToendDate.get(vm_requisitionDTO.endDate).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTostartAddress.containsKey(vm_requisitionDTO.startAddress)) {
//						mapOfVm_requisitionDTOTostartAddress.put(vm_requisitionDTO.startAddress, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTostartAddress.get(vm_requisitionDTO.startAddress).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOToendAddress.containsKey(vm_requisitionDTO.endAddress)) {
//						mapOfVm_requisitionDTOToendAddress.put(vm_requisitionDTO.endAddress, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOToendAddress.get(vm_requisitionDTO.endAddress).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTovehicleRequisitionPurposeCat.containsKey(vm_requisitionDTO.vehicleRequisitionPurposeCat)) {
//						mapOfVm_requisitionDTOTovehicleRequisitionPurposeCat.put(vm_requisitionDTO.vehicleRequisitionPurposeCat, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTovehicleRequisitionPurposeCat.get(vm_requisitionDTO.vehicleRequisitionPurposeCat).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTovehicleTypeCat.containsKey(vm_requisitionDTO.vehicleTypeCat)) {
//						mapOfVm_requisitionDTOTovehicleTypeCat.put(vm_requisitionDTO.vehicleTypeCat, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTovehicleTypeCat.get(vm_requisitionDTO.vehicleTypeCat).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTotripDescription.containsKey(vm_requisitionDTO.tripDescription)) {
//						mapOfVm_requisitionDTOTotripDescription.put(vm_requisitionDTO.tripDescription, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTotripDescription.get(vm_requisitionDTO.tripDescription).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTostatus.containsKey(vm_requisitionDTO.status)) {
//						mapOfVm_requisitionDTOTostatus.put(vm_requisitionDTO.status, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTostatus.get(vm_requisitionDTO.status).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodecisionOn.containsKey(vm_requisitionDTO.decisionOn)) {
//						mapOfVm_requisitionDTOTodecisionOn.put(vm_requisitionDTO.decisionOn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodecisionOn.get(vm_requisitionDTO.decisionOn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodecisionDescription.containsKey(vm_requisitionDTO.decisionDescription)) {
//						mapOfVm_requisitionDTOTodecisionDescription.put(vm_requisitionDTO.decisionDescription, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodecisionDescription.get(vm_requisitionDTO.decisionDescription).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTogivenVehicleType.containsKey(vm_requisitionDTO.givenVehicleType)) {
//						mapOfVm_requisitionDTOTogivenVehicleType.put(vm_requisitionDTO.givenVehicleType, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTogivenVehicleType.get(vm_requisitionDTO.givenVehicleType).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTogivenVehicleId.containsKey(vm_requisitionDTO.givenVehicleId)) {
//						mapOfVm_requisitionDTOTogivenVehicleId.put(vm_requisitionDTO.givenVehicleId, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTogivenVehicleId.get(vm_requisitionDTO.givenVehicleId).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOToreceiveDate.containsKey(vm_requisitionDTO.receiveDate)) {
//						mapOfVm_requisitionDTOToreceiveDate.put(vm_requisitionDTO.receiveDate, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOToreceiveDate.get(vm_requisitionDTO.receiveDate).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTototalTripDistance.containsKey(vm_requisitionDTO.totalTripDistance)) {
//						mapOfVm_requisitionDTOTototalTripDistance.put(vm_requisitionDTO.totalTripDistance, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTototalTripDistance.get(vm_requisitionDTO.totalTripDistance).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTototalTripTime.containsKey(vm_requisitionDTO.totalTripTime)) {
//						mapOfVm_requisitionDTOTototalTripTime.put(vm_requisitionDTO.totalTripTime, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTototalTripTime.get(vm_requisitionDTO.totalTripTime).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTopetrolGiven.containsKey(vm_requisitionDTO.petrolGiven)) {
//						mapOfVm_requisitionDTOTopetrolGiven.put(vm_requisitionDTO.petrolGiven, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTopetrolGiven.get(vm_requisitionDTO.petrolGiven).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTopetrolAmount.containsKey(vm_requisitionDTO.petrolAmount)) {
//						mapOfVm_requisitionDTOTopetrolAmount.put(vm_requisitionDTO.petrolAmount, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTopetrolAmount.get(vm_requisitionDTO.petrolAmount).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTopaymentGiven.containsKey(vm_requisitionDTO.paymentGiven)) {
//						mapOfVm_requisitionDTOTopaymentGiven.put(vm_requisitionDTO.paymentGiven, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTopaymentGiven.get(vm_requisitionDTO.paymentGiven).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTopaymentType.containsKey(vm_requisitionDTO.paymentType)) {
//						mapOfVm_requisitionDTOTopaymentType.put(vm_requisitionDTO.paymentType, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTopaymentType.get(vm_requisitionDTO.paymentType).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTostartTime.containsKey(vm_requisitionDTO.startTime)) {
//						mapOfVm_requisitionDTOTostartTime.put(vm_requisitionDTO.startTime, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTostartTime.get(vm_requisitionDTO.startTime).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOToendTime.containsKey(vm_requisitionDTO.endTime)) {
//						mapOfVm_requisitionDTOToendTime.put(vm_requisitionDTO.endTime, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOToendTime.get(vm_requisitionDTO.endTime).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOToreceiveTime.containsKey(vm_requisitionDTO.receiveTime)) {
//						mapOfVm_requisitionDTOToreceiveTime.put(vm_requisitionDTO.receiveTime, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOToreceiveTime.get(vm_requisitionDTO.receiveTime).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTorequesterOfficeId.containsKey(vm_requisitionDTO.requesterOfficeId)) {
//						mapOfVm_requisitionDTOTorequesterOfficeId.put(vm_requisitionDTO.requesterOfficeId, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTorequesterOfficeId.get(vm_requisitionDTO.requesterOfficeId).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodecisionByOfficeId.containsKey(vm_requisitionDTO.decisionByOfficeId)) {
//						mapOfVm_requisitionDTOTodecisionByOfficeId.put(vm_requisitionDTO.decisionByOfficeId, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodecisionByOfficeId.get(vm_requisitionDTO.decisionByOfficeId).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodriverOfficeId.containsKey(vm_requisitionDTO.driverOfficeId)) {
//						mapOfVm_requisitionDTOTodriverOfficeId.put(vm_requisitionDTO.driverOfficeId, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodriverOfficeId.get(vm_requisitionDTO.driverOfficeId).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTorequesterOfficeUnitId.containsKey(vm_requisitionDTO.requesterOfficeUnitId)) {
//						mapOfVm_requisitionDTOTorequesterOfficeUnitId.put(vm_requisitionDTO.requesterOfficeUnitId, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTorequesterOfficeUnitId.get(vm_requisitionDTO.requesterOfficeUnitId).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodecisionByOfficeUnitId.containsKey(vm_requisitionDTO.decisionByOfficeUnitId)) {
//						mapOfVm_requisitionDTOTodecisionByOfficeUnitId.put(vm_requisitionDTO.decisionByOfficeUnitId, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodecisionByOfficeUnitId.get(vm_requisitionDTO.decisionByOfficeUnitId).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodriverOfficeUnitId.containsKey(vm_requisitionDTO.driverOfficeUnitId)) {
//						mapOfVm_requisitionDTOTodriverOfficeUnitId.put(vm_requisitionDTO.driverOfficeUnitId, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodriverOfficeUnitId.get(vm_requisitionDTO.driverOfficeUnitId).add(vm_requisitionDTO);
//
					if( ! mapOfVm_requisitionDTOTorequesterEmpId.containsKey(vm_requisitionDTO.requesterEmpId)) {
						mapOfVm_requisitionDTOTorequesterEmpId.put(vm_requisitionDTO.requesterEmpId, new HashSet<>());
					}
					mapOfVm_requisitionDTOTorequesterEmpId.get(vm_requisitionDTO.requesterEmpId).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodecisionByEmpId.containsKey(vm_requisitionDTO.decisionByEmpId)) {
//						mapOfVm_requisitionDTOTodecisionByEmpId.put(vm_requisitionDTO.decisionByEmpId, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodecisionByEmpId.get(vm_requisitionDTO.decisionByEmpId).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodriverEmpId.containsKey(vm_requisitionDTO.driverEmpId)) {
//						mapOfVm_requisitionDTOTodriverEmpId.put(vm_requisitionDTO.driverEmpId, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodriverEmpId.get(vm_requisitionDTO.driverEmpId).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTorequesterPhoneNum.containsKey(vm_requisitionDTO.requesterPhoneNum)) {
//						mapOfVm_requisitionDTOTorequesterPhoneNum.put(vm_requisitionDTO.requesterPhoneNum, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTorequesterPhoneNum.get(vm_requisitionDTO.requesterPhoneNum).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodecisionByPhoneNum.containsKey(vm_requisitionDTO.decisionByPhoneNum)) {
//						mapOfVm_requisitionDTOTodecisionByPhoneNum.put(vm_requisitionDTO.decisionByPhoneNum, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodecisionByPhoneNum.get(vm_requisitionDTO.decisionByPhoneNum).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodriverPhoneNum.containsKey(vm_requisitionDTO.driverPhoneNum)) {
//						mapOfVm_requisitionDTOTodriverPhoneNum.put(vm_requisitionDTO.driverPhoneNum, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodriverPhoneNum.get(vm_requisitionDTO.driverPhoneNum).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTorequesterNameEn.containsKey(vm_requisitionDTO.requesterNameEn)) {
//						mapOfVm_requisitionDTOTorequesterNameEn.put(vm_requisitionDTO.requesterNameEn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTorequesterNameEn.get(vm_requisitionDTO.requesterNameEn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodecisionByNameEn.containsKey(vm_requisitionDTO.decisionByNameEn)) {
//						mapOfVm_requisitionDTOTodecisionByNameEn.put(vm_requisitionDTO.decisionByNameEn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodecisionByNameEn.get(vm_requisitionDTO.decisionByNameEn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodriverNameEn.containsKey(vm_requisitionDTO.driverNameEn)) {
//						mapOfVm_requisitionDTOTodriverNameEn.put(vm_requisitionDTO.driverNameEn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodriverNameEn.get(vm_requisitionDTO.driverNameEn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTorequesterNameBn.containsKey(vm_requisitionDTO.requesterNameBn)) {
//						mapOfVm_requisitionDTOTorequesterNameBn.put(vm_requisitionDTO.requesterNameBn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTorequesterNameBn.get(vm_requisitionDTO.requesterNameBn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodecisionByNameBn.containsKey(vm_requisitionDTO.decisionByNameBn)) {
//						mapOfVm_requisitionDTOTodecisionByNameBn.put(vm_requisitionDTO.decisionByNameBn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodecisionByNameBn.get(vm_requisitionDTO.decisionByNameBn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodriverNameBn.containsKey(vm_requisitionDTO.driverNameBn)) {
//						mapOfVm_requisitionDTOTodriverNameBn.put(vm_requisitionDTO.driverNameBn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodriverNameBn.get(vm_requisitionDTO.driverNameBn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTorequesterOfficeNameEn.containsKey(vm_requisitionDTO.requesterOfficeNameEn)) {
//						mapOfVm_requisitionDTOTorequesterOfficeNameEn.put(vm_requisitionDTO.requesterOfficeNameEn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTorequesterOfficeNameEn.get(vm_requisitionDTO.requesterOfficeNameEn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodecisionByOfficeNameEn.containsKey(vm_requisitionDTO.decisionByOfficeNameEn)) {
//						mapOfVm_requisitionDTOTodecisionByOfficeNameEn.put(vm_requisitionDTO.decisionByOfficeNameEn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodecisionByOfficeNameEn.get(vm_requisitionDTO.decisionByOfficeNameEn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodriverOfficeNameEn.containsKey(vm_requisitionDTO.driverOfficeNameEn)) {
//						mapOfVm_requisitionDTOTodriverOfficeNameEn.put(vm_requisitionDTO.driverOfficeNameEn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodriverOfficeNameEn.get(vm_requisitionDTO.driverOfficeNameEn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTorequesterOfficeNameBn.containsKey(vm_requisitionDTO.requesterOfficeNameBn)) {
//						mapOfVm_requisitionDTOTorequesterOfficeNameBn.put(vm_requisitionDTO.requesterOfficeNameBn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTorequesterOfficeNameBn.get(vm_requisitionDTO.requesterOfficeNameBn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodecisionByOfficeNameBn.containsKey(vm_requisitionDTO.decisionByOfficeNameBn)) {
//						mapOfVm_requisitionDTOTodecisionByOfficeNameBn.put(vm_requisitionDTO.decisionByOfficeNameBn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodecisionByOfficeNameBn.get(vm_requisitionDTO.decisionByOfficeNameBn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodriverOfficeNameBn.containsKey(vm_requisitionDTO.driverOfficeNameBn)) {
//						mapOfVm_requisitionDTOTodriverOfficeNameBn.put(vm_requisitionDTO.driverOfficeNameBn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodriverOfficeNameBn.get(vm_requisitionDTO.driverOfficeNameBn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTorequesterOfficeUnitNameEn.containsKey(vm_requisitionDTO.requesterOfficeUnitNameEn)) {
//						mapOfVm_requisitionDTOTorequesterOfficeUnitNameEn.put(vm_requisitionDTO.requesterOfficeUnitNameEn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTorequesterOfficeUnitNameEn.get(vm_requisitionDTO.requesterOfficeUnitNameEn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodecisionByOfficeUnitNameEn.containsKey(vm_requisitionDTO.decisionByOfficeUnitNameEn)) {
//						mapOfVm_requisitionDTOTodecisionByOfficeUnitNameEn.put(vm_requisitionDTO.decisionByOfficeUnitNameEn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodecisionByOfficeUnitNameEn.get(vm_requisitionDTO.decisionByOfficeUnitNameEn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodriverOfficeUnitNameEn.containsKey(vm_requisitionDTO.driverOfficeUnitNameEn)) {
//						mapOfVm_requisitionDTOTodriverOfficeUnitNameEn.put(vm_requisitionDTO.driverOfficeUnitNameEn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodriverOfficeUnitNameEn.get(vm_requisitionDTO.driverOfficeUnitNameEn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTorequesterOfficeUnitNameBn.containsKey(vm_requisitionDTO.requesterOfficeUnitNameBn)) {
//						mapOfVm_requisitionDTOTorequesterOfficeUnitNameBn.put(vm_requisitionDTO.requesterOfficeUnitNameBn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTorequesterOfficeUnitNameBn.get(vm_requisitionDTO.requesterOfficeUnitNameBn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodecisionByOfficeUnitNameBn.containsKey(vm_requisitionDTO.decisionByOfficeUnitNameBn)) {
//						mapOfVm_requisitionDTOTodecisionByOfficeUnitNameBn.put(vm_requisitionDTO.decisionByOfficeUnitNameBn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodecisionByOfficeUnitNameBn.get(vm_requisitionDTO.decisionByOfficeUnitNameBn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodriverOfficeUnitNameBn.containsKey(vm_requisitionDTO.driverOfficeUnitNameBn)) {
//						mapOfVm_requisitionDTOTodriverOfficeUnitNameBn.put(vm_requisitionDTO.driverOfficeUnitNameBn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodriverOfficeUnitNameBn.get(vm_requisitionDTO.driverOfficeUnitNameBn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTorequesterOfficeUnitOrgNameEn.containsKey(vm_requisitionDTO.requesterOfficeUnitOrgNameEn)) {
//						mapOfVm_requisitionDTOTorequesterOfficeUnitOrgNameEn.put(vm_requisitionDTO.requesterOfficeUnitOrgNameEn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTorequesterOfficeUnitOrgNameEn.get(vm_requisitionDTO.requesterOfficeUnitOrgNameEn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodecisionByOfficeUnitOrgNameEn.containsKey(vm_requisitionDTO.decisionByOfficeUnitOrgNameEn)) {
//						mapOfVm_requisitionDTOTodecisionByOfficeUnitOrgNameEn.put(vm_requisitionDTO.decisionByOfficeUnitOrgNameEn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodecisionByOfficeUnitOrgNameEn.get(vm_requisitionDTO.decisionByOfficeUnitOrgNameEn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodriverOfficeUnitOrgNameEn.containsKey(vm_requisitionDTO.driverOfficeUnitOrgNameEn)) {
//						mapOfVm_requisitionDTOTodriverOfficeUnitOrgNameEn.put(vm_requisitionDTO.driverOfficeUnitOrgNameEn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodriverOfficeUnitOrgNameEn.get(vm_requisitionDTO.driverOfficeUnitOrgNameEn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTorequesterOfficeUnitOrgNameBn.containsKey(vm_requisitionDTO.requesterOfficeUnitOrgNameBn)) {
//						mapOfVm_requisitionDTOTorequesterOfficeUnitOrgNameBn.put(vm_requisitionDTO.requesterOfficeUnitOrgNameBn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTorequesterOfficeUnitOrgNameBn.get(vm_requisitionDTO.requesterOfficeUnitOrgNameBn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodecisionByOfficeUnitOrgNameBn.containsKey(vm_requisitionDTO.decisionByOfficeUnitOrgNameBn)) {
//						mapOfVm_requisitionDTOTodecisionByOfficeUnitOrgNameBn.put(vm_requisitionDTO.decisionByOfficeUnitOrgNameBn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodecisionByOfficeUnitOrgNameBn.get(vm_requisitionDTO.decisionByOfficeUnitOrgNameBn).add(vm_requisitionDTO);
//
//					if( ! mapOfVm_requisitionDTOTodriverOfficeUnitOrgNameBn.containsKey(vm_requisitionDTO.driverOfficeUnitOrgNameBn)) {
//						mapOfVm_requisitionDTOTodriverOfficeUnitOrgNameBn.put(vm_requisitionDTO.driverOfficeUnitOrgNameBn, new HashSet<>());
//					}
//					mapOfVm_requisitionDTOTodriverOfficeUnitOrgNameBn.get(vm_requisitionDTO.driverOfficeUnitOrgNameBn).add(vm_requisitionDTO);
//
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public Vm_requisitionDTO clone(Vm_requisitionDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Vm_requisitionDTO.class);
	}

	public List<Vm_requisitionDTO> clone(List<Vm_requisitionDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public Vm_requisitionDTO getVm_requisitionDTOByIDWithoutClone( long ID){
		return mapOfVm_requisitionDTOToiD.get(ID);
	}
	
	public List<Vm_requisitionDTO> getVm_requisitionList() {
		List <Vm_requisitionDTO> vm_requisitions = new ArrayList<Vm_requisitionDTO>(this.mapOfVm_requisitionDTOToiD.values());
		return vm_requisitions;
	}
	
	
	public Vm_requisitionDTO getVm_requisitionDTOByID( long ID){
		return clone(mapOfVm_requisitionDTOToiD.get(ID));
	}
	
	
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByinserted_by_user_id(long inserted_by_user_id) {
//		return new ArrayList<>( mapOfVm_requisitionDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
//		return new ArrayList<>( mapOfVm_requisitionDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByinsertion_date(long insertion_date) {
//		return new ArrayList<>( mapOfVm_requisitionDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBylastModificationTime(long lastModificationTime) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBysearch_column(String search_column) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
//	}
//
//
	public List<Vm_requisitionDTO> getVm_requisitionDTOByrequester_org_id(long requester_org_id) {
		return clone(new ArrayList<>( mapOfVm_requisitionDTOTorequesterOrgId.getOrDefault(requester_org_id,new HashSet<>())));
	}

	public List<Vm_requisitionDTO> getUnpaidPersonalVm_requisitionDTOByRequester_org_id(long requester_org_id) {
		List<Vm_requisitionDTO> vm_requisitionDTOS = new ArrayList<>( mapOfVm_requisitionDTOTorequesterOrgId.getOrDefault(requester_org_id,new HashSet<>()));
		return vm_requisitionDTOS
				.stream()
				.filter(vm_requisitionDTO -> vm_requisitionDTO.vehicleRequisitionPurposeCat == 2
						&& vm_requisitionDTO.paymentGiven != 1
						&& vm_requisitionDTO.status == CommonApprovalStatus.RECEIVED.getValue())
				.map(vm_requisitionDTO -> clone(vm_requisitionDTO))
				.collect(Collectors.toList());
	}

	public List<Vm_requisitionDTO> getUnpaidPersonalVm_requisitionDTOByRequesterEmpId(long requesterEmpId) {
		List<Vm_requisitionDTO> vm_requisitionDTOS = new ArrayList<>( mapOfVm_requisitionDTOTorequesterEmpId.getOrDefault(requesterEmpId,new HashSet<>()));
		return vm_requisitionDTOS
				.stream()
				.filter(vm_requisitionDTO -> vm_requisitionDTO.vehicleRequisitionPurposeCat == 2
						&& vm_requisitionDTO.paymentGiven != 1 // 1 = paid, 2 = unpaid
						&& vm_requisitionDTO.status == CommonApprovalStatus.RECEIVED.getValue())
				.map(vm_requisitionDTO -> clone(vm_requisitionDTO))
				.collect(Collectors.toList());
	}
	public List<Vm_requisitionDTO> getVm_requisitionDTOByRequesterEmpIdAndVehicleId(long requesterEmpId, long vehicleId) {
		List<Vm_requisitionDTO> vm_requisitionDTOS = new ArrayList<>( mapOfVm_requisitionDTOTorequesterEmpId.getOrDefault(requesterEmpId,new HashSet<>()));
		return vm_requisitionDTOS
				.stream()
				.filter(vm_requisitionDTO -> vm_requisitionDTO.givenVehicleId == vehicleId)
				.filter(vm_requisitionDTO -> vm_requisitionDTO.status == CommonApprovalStatus.RECEIVED.getValue())
				.map(vm_requisitionDTO -> clone(vm_requisitionDTO))
				.sorted(Comparator.comparingLong(dto -> dto.iD))
				.collect(Collectors.toList());
	}
	public List<Vm_requisitionDTO> getVm_requisitionDTOByVehicleId(long vehicleId) {
		List<Vm_requisitionDTO> vm_requisitionDTOS = getVm_requisitionList();
		return vm_requisitionDTOS
				.stream()
				.filter(vm_requisitionDTO -> vm_requisitionDTO.givenVehicleId == vehicleId)
				.filter(vm_requisitionDTO -> vm_requisitionDTO.status == CommonApprovalStatus.RECEIVED.getValue())
				.map(vm_requisitionDTO -> clone(vm_requisitionDTO))
				.sorted(Comparator.comparingLong(dto -> dto.iD))
				.collect(Collectors.toList());
	}

//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydecision_by_org_id(long decision_by_org_id) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodecisionByOrgId.getOrDefault(decision_by_org_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydriver_org_id(long driver_org_id) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodriverOrgId.getOrDefault(driver_org_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBystart_date(long start_date) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTostartDate.getOrDefault(start_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByend_date(long end_date) {
//		return new ArrayList<>( mapOfVm_requisitionDTOToendDate.getOrDefault(end_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBystart_address(String start_address) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTostartAddress.getOrDefault(start_address,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByend_address(String end_address) {
//		return new ArrayList<>( mapOfVm_requisitionDTOToendAddress.getOrDefault(end_address,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByvehicle_requisition_purpose_cat(int vehicle_requisition_purpose_cat) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTovehicleRequisitionPurposeCat.getOrDefault(vehicle_requisition_purpose_cat,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByvehicle_type_cat(int vehicle_type_cat) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTovehicleTypeCat.getOrDefault(vehicle_type_cat,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBytrip_description(String trip_description) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTotripDescription.getOrDefault(trip_description,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBystatus(int status) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTostatus.getOrDefault(status,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydecision_on(long decision_on) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodecisionOn.getOrDefault(decision_on,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydecision_description(String decision_description) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodecisionDescription.getOrDefault(decision_description,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBygiven_vehicle_type(int given_vehicle_type) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTogivenVehicleType.getOrDefault(given_vehicle_type,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBygiven_vehicle_id(long given_vehicle_id) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTogivenVehicleId.getOrDefault(given_vehicle_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByreceive_date(long receive_date) {
//		return new ArrayList<>( mapOfVm_requisitionDTOToreceiveDate.getOrDefault(receive_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBytotal_trip_distance(double total_trip_distance) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTototalTripDistance.getOrDefault(total_trip_distance,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBytotal_trip_time(double total_trip_time) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTototalTripTime.getOrDefault(total_trip_time,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBypetrol_given(int petrol_given) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTopetrolGiven.getOrDefault(petrol_given,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBypetrol_amount(double petrol_amount) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTopetrolAmount.getOrDefault(petrol_amount,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBypayment_given(int payment_given) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTopaymentGiven.getOrDefault(payment_given,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBypayment_type(int payment_type) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTopaymentType.getOrDefault(payment_type,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBystart_time(String start_time) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTostartTime.getOrDefault(start_time,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByend_time(String end_time) {
//		return new ArrayList<>( mapOfVm_requisitionDTOToendTime.getOrDefault(end_time,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByreceive_time(String receive_time) {
//		return new ArrayList<>( mapOfVm_requisitionDTOToreceiveTime.getOrDefault(receive_time,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByrequester_office_id(long requester_office_id) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTorequesterOfficeId.getOrDefault(requester_office_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydecision_by_office_id(long decision_by_office_id) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodecisionByOfficeId.getOrDefault(decision_by_office_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydriver_office_id(long driver_office_id) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodriverOfficeId.getOrDefault(driver_office_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByrequester_office_unit_id(long requester_office_unit_id) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTorequesterOfficeUnitId.getOrDefault(requester_office_unit_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydecision_by_office_unit_id(long decision_by_office_unit_id) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodecisionByOfficeUnitId.getOrDefault(decision_by_office_unit_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydriver_office_unit_id(long driver_office_unit_id) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodriverOfficeUnitId.getOrDefault(driver_office_unit_id,new HashSet<>()));
//	}
//
//
	public List<Vm_requisitionDTO> getVm_requisitionDTOByrequester_emp_id(long requester_emp_id) {
		return new ArrayList<>( mapOfVm_requisitionDTOTorequesterEmpId.getOrDefault(requester_emp_id,new HashSet<>()));
	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydecision_by_emp_id(long decision_by_emp_id) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodecisionByEmpId.getOrDefault(decision_by_emp_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydriver_emp_id(long driver_emp_id) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodriverEmpId.getOrDefault(driver_emp_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByrequester_phone_num(String requester_phone_num) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTorequesterPhoneNum.getOrDefault(requester_phone_num,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydecision_by_phone_num(String decision_by_phone_num) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodecisionByPhoneNum.getOrDefault(decision_by_phone_num,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydriver_phone_num(String driver_phone_num) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodriverPhoneNum.getOrDefault(driver_phone_num,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByrequester_name_en(String requester_name_en) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTorequesterNameEn.getOrDefault(requester_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydecision_by_name_en(String decision_by_name_en) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodecisionByNameEn.getOrDefault(decision_by_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydriver_name_en(String driver_name_en) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodriverNameEn.getOrDefault(driver_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByrequester_name_bn(String requester_name_bn) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTorequesterNameBn.getOrDefault(requester_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydecision_by_name_bn(String decision_by_name_bn) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodecisionByNameBn.getOrDefault(decision_by_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydriver_name_bn(String driver_name_bn) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodriverNameBn.getOrDefault(driver_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByrequester_office_name_en(String requester_office_name_en) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTorequesterOfficeNameEn.getOrDefault(requester_office_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydecision_by_office_name_en(String decision_by_office_name_en) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodecisionByOfficeNameEn.getOrDefault(decision_by_office_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydriver_office_name_en(String driver_office_name_en) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodriverOfficeNameEn.getOrDefault(driver_office_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByrequester_office_name_bn(String requester_office_name_bn) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTorequesterOfficeNameBn.getOrDefault(requester_office_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydecision_by_office_name_bn(String decision_by_office_name_bn) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodecisionByOfficeNameBn.getOrDefault(decision_by_office_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydriver_office_name_bn(String driver_office_name_bn) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodriverOfficeNameBn.getOrDefault(driver_office_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByrequester_office_unit_name_en(String requester_office_unit_name_en) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTorequesterOfficeUnitNameEn.getOrDefault(requester_office_unit_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydecision_by_office_unit_name_en(String decision_by_office_unit_name_en) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodecisionByOfficeUnitNameEn.getOrDefault(decision_by_office_unit_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydriver_office_unit_name_en(String driver_office_unit_name_en) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodriverOfficeUnitNameEn.getOrDefault(driver_office_unit_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByrequester_office_unit_name_bn(String requester_office_unit_name_bn) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTorequesterOfficeUnitNameBn.getOrDefault(requester_office_unit_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydecision_by_office_unit_name_bn(String decision_by_office_unit_name_bn) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodecisionByOfficeUnitNameBn.getOrDefault(decision_by_office_unit_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydriver_office_unit_name_bn(String driver_office_unit_name_bn) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodriverOfficeUnitNameBn.getOrDefault(driver_office_unit_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByrequester_office_unit_org_name_en(String requester_office_unit_org_name_en) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTorequesterOfficeUnitOrgNameEn.getOrDefault(requester_office_unit_org_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydecision_by_office_unit_org_name_en(String decision_by_office_unit_org_name_en) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodecisionByOfficeUnitOrgNameEn.getOrDefault(decision_by_office_unit_org_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydriver_office_unit_org_name_en(String driver_office_unit_org_name_en) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodriverOfficeUnitOrgNameEn.getOrDefault(driver_office_unit_org_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOByrequester_office_unit_org_name_bn(String requester_office_unit_org_name_bn) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTorequesterOfficeUnitOrgNameBn.getOrDefault(requester_office_unit_org_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydecision_by_office_unit_org_name_bn(String decision_by_office_unit_org_name_bn) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodecisionByOfficeUnitOrgNameBn.getOrDefault(decision_by_office_unit_org_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisitionDTO> getVm_requisitionDTOBydriver_office_unit_org_name_bn(String driver_office_unit_org_name_bn) {
//		return new ArrayList<>( mapOfVm_requisitionDTOTodriverOfficeUnitOrgNameBn.getOrDefault(driver_office_unit_org_name_bn,new HashSet<>()));
//	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_requisition";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


