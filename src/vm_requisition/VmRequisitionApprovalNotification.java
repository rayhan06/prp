package vm_requisition;

/*
 * @author Md. Erfan Hossain
 * @created 11/05/2021 - 3:26 PM
 * @project parliament
 */

import business_card_info.Business_card_infoDTO;
import card_info.CardEmployeeInfoDTO;
import card_info.CardEmployeeInfoRepository;
import card_info.CardEmployeeOfficeInfoDTO;
import card_info.CardEmployeeOfficeInfoRepository;
import pb.Utils;
import pb_notifications.Pb_notificationsDAO;
import util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class VmRequisitionApprovalNotification {
    private final Pb_notificationsDAO pb_notificationsDAO;

    private VmRequisitionApprovalNotification(){
        pb_notificationsDAO = Pb_notificationsDAO.getInstance();
    }

    private static class BusinessCardApprovalNotificationLazyLoader{
        static final VmRequisitionApprovalNotification INSTANCE = new VmRequisitionApprovalNotification();
    }

    public static VmRequisitionApprovalNotification getInstance(){
        return BusinessCardApprovalNotificationLazyLoader.INSTANCE;
    }

    public void sendPrePendingNotification(List<Long> organogramIds, Vm_requisitionDTO vm_requisitionDTO){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String formatted_startDate = simpleDateFormat.format(new Date((vm_requisitionDTO.startDate)));

        String startDateEn = formatted_startDate;
        String startDateBn = Utils.getDigits(formatted_startDate, "Bangla");
        String startTimeEn = vm_requisitionDTO.startTime;
        String startTimeBn = Utils.getDigits(vm_requisitionDTO.startTime, "Bangla");

        String empEngText = vm_requisitionDTO.requesterNameEn + "("+vm_requisitionDTO.requesterOfficeUnitOrgNameEn+","+vm_requisitionDTO.requesterOfficeUnitNameEn+")";
        String empBngText = vm_requisitionDTO.requesterNameBn + "("+vm_requisitionDTO.requesterOfficeUnitOrgNameBn+","+vm_requisitionDTO.requesterOfficeUnitNameBn+")";
        String notificationMessage = empEngText+" ("+ startDateEn + " " + startTimeEn +") "+"requisition request is waiting to be considered for approval process.$"
                +empBngText+" ("+ startDateBn + " " + startTimeBn +") "+"অধিযাচন অনুরোধটি অনুমোদন প্রক্রিয়ার জন্য বিবেচনা করুন";
        String url = "Vm_requisitionServlet?actionType=getPrePendingPage&ID="+vm_requisitionDTO.iD;
        sendNotification(organogramIds,notificationMessage,url);
    }

    public void sendPendingNotification(List<Long> organogramIds, Vm_requisitionDTO vm_requisitionDTO){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String formatted_startDate = simpleDateFormat.format(new Date((vm_requisitionDTO.startDate)));

        String startDateEn = formatted_startDate;
        String startDateBn = Utils.getDigits(formatted_startDate, "Bangla");
        String startTimeEn = vm_requisitionDTO.startTime;
        String startTimeBn = Utils.getDigits(vm_requisitionDTO.startTime, "Bangla");

        String empEngText = vm_requisitionDTO.requesterNameEn + "("+vm_requisitionDTO.requesterOfficeUnitOrgNameEn+","+vm_requisitionDTO.requesterOfficeUnitNameEn+")";
        String empBngText = vm_requisitionDTO.requesterNameBn + "("+vm_requisitionDTO.requesterOfficeUnitOrgNameBn+","+vm_requisitionDTO.requesterOfficeUnitNameBn+")";
        String notificationMessage = empEngText+" ("+ startDateEn + " " + startTimeEn +") "+"requisition request is waiting to be considered for approval process.$"
                +empBngText+" ("+ startDateBn + " " + startTimeBn +") "+"অধিযাচন অনুরোধটি অনুমোদন প্রক্রিয়ার জন্য বিবেচনা করুন";
        String url = "Vm_requisition_pendingServlet?actionType=view&ID="+vm_requisitionDTO.iD;
        sendNotification(organogramIds,notificationMessage,url);
    }

    public void sendApproveOrRejectNotification(List<Long> organogramIds, Vm_requisitionDTO vm_requisitionDTO, boolean isApproved){
        String cardEngText = isApproved ? "You requested for vehicle requisition has been approved."
                : "You requested for vehicle requisition has been rejected.";
        String cardBngText = isApproved ? "আপনার অনুরোধ করা যানবাহন অধিযাচন অনুমোদিত হয়েছে।"
                : "আপনার অনুরোধ করা যানবাহন অধিযাচন প্রত্যাখ্যান করা হয়েছে।";
        String notificationMessage = cardEngText +"$"+cardBngText;
        String url = "Vm_requisitionServlet?actionType=view&ID="+vm_requisitionDTO.iD;
        sendNotification(organogramIds,notificationMessage,url);
    }

    private void sendNotification(List<Long> organogramIds,String notificationMessage,String url){
        if(organogramIds == null || organogramIds.size() == 0){
            return;
        }
        Thread thread = new Thread(()->{
            long currentTime = System.currentTimeMillis();
            organogramIds.forEach(id-> pb_notificationsDAO.addPb_notifications(id, currentTime, url, notificationMessage, false));
        });
        thread.setDaemon(true);
        thread.start();
    }

    public void sendApproveNotificationToLastApprover(List<Long> organogramIds, Business_card_infoDTO cardInfoDTO){
        String cardEngText = "Approved Business";
        String cardBngText = "অনুমোদিত বিজনেস";
        CardEmployeeInfoDTO cardEmployeeInfoDTO = CardEmployeeInfoRepository.getInstance().getById(cardInfoDTO.cardEmployeeInfoId);
        CardEmployeeOfficeInfoDTO cardEmployeeOfficeInfoDTO = CardEmployeeOfficeInfoRepository.getInstance().getById(cardInfoDTO.cardEmployeeOfficeInfoId);
        String empEngText = cardEmployeeInfoDTO.nameEn + "("+cardEmployeeOfficeInfoDTO.organogramEng+","+cardEmployeeOfficeInfoDTO.officeUnitEng+")s";
        String empBngText = cardEmployeeInfoDTO.nameBn + "("+cardEmployeeOfficeInfoDTO.organogramBng+","+cardEmployeeOfficeInfoDTO.officeUnitBng+") এর";
        String notificationMessage = empEngText+" "+cardEngText+" "+"card-"+cardInfoDTO.iD+" "+"is waiting for next activity.$"
                +empBngText+" "+cardBngText+" "+"কার্ড-"+ StringUtils.convertToBanNumber(String.valueOf(cardInfoDTO.iD))+" "+ "পরবর্তী কার্যক্রমের অপেক্ষায় আছে।";
        String url = "Business_card_approval_mappingServlet?actionType=getApprovalPage&cardInfoId="+cardInfoDTO.iD;
        sendNotification(organogramIds,notificationMessage,url);
    }
}