package vm_requisition;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

import common.ConnectionAndStatementUtil;

import java.sql.SQLException;
import java.util.stream.Collectors;


import org.apache.log4j.Logger;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Vm_requisitionDAO  extends NavigationService4
{
	public static final int GETDRIVERIDS = 4;
	public static final int GETVEHICLEIDS = 5;

	Logger logger = Logger.getLogger(getClass());
	private final String getLastUsageOfSameVehicle = ("SELECT * FROM vm_requisition WHERE " +
			" requester_emp_id=%d AND " +
			" given_vehicle_id=%d AND " +
			" ID!=%d AND " +
			" status=%d " +
			" ORDER BY ID DESC LIMIT 1");

	public Vm_requisitionDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Vm_requisitionMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"search_column",
			"requester_org_id",
			"decision_by_org_id",
			"driver_org_id",
			"start_date",
			"end_date",
			"start_address",
			"end_address",
			"vehicle_requisition_purpose_cat",
			"vehicle_type_cat",
			"trip_description",
			"status",
			"decision_on",
			"decision_description",
			"given_vehicle_type",
			"given_vehicle_id",
			"receive_date",
			"total_trip_distance",
			"meter_reading_initial",
			"meter_reading_end",
			"total_trip_time",
			"petrol_given",
			"petrol_previous_stock",
			"petrol_amount",
			"petrol_consumption",
			"petrol_stock",
			"payment_given",
			"payment_type",
			"start_time",
			"end_time",
			"receive_time",
			"requester_office_id",
			"decision_by_office_id",
			"driver_office_id",
			"requester_office_unit_id",
			"decision_by_office_unit_id",
			"driver_office_unit_id",
			"requester_emp_id",
			"decision_by_emp_id",
			"driver_emp_id",
			"requester_phone_num",
			"decision_by_phone_num",
			"driver_phone_num",
			"requester_name_en",
			"decision_by_name_en",
			"driver_name_en",
			"requester_name_bn",
			"decision_by_name_bn",
			"driver_name_bn",
			"requester_office_name_en",
			"decision_by_office_name_en",
			"driver_office_name_en",
			"requester_office_name_bn",
			"decision_by_office_name_bn",
			"driver_office_name_bn",
			"requester_office_unit_name_en",
			"decision_by_office_unit_name_en",
			"driver_office_unit_name_en",
			"requester_office_unit_name_bn",
			"decision_by_office_unit_name_bn",
			"driver_office_unit_name_bn",
			"requester_office_unit_org_name_en",
			"decision_by_office_unit_org_name_en",
			"driver_office_unit_org_name_en",
			"requester_office_unit_org_name_bn",
			"decision_by_office_unit_org_name_bn",
			"driver_office_unit_org_name_bn",
			"paidAmount",
			"vehicle_requisition_location_cat",
			"files_dropzone",
			"first_approver_org_id",
			"first_approver_office_id",
			"first_approver_office_unit_id",
			"first_approver_emp_id",
			"first_approver_phone_num",
			"first_approver_name_en",
			"first_approver_name_bn",
			"first_approver_office_name_en",
			"first_approver_office_name_bn",
			"first_approver_office_unit_name_en",
			"first_approver_office_unit_name_bn",
			"first_approver_office_unit_org_name_en",
			"first_approver_office_unit_org_name_bn",
			"cancellation_reason",
			"cancellation_date",
			"first_approver_approve_or_rejection_date",
			"second_approver_approve_or_rejection_date",
			"first_approver_status",
			"second_approver_status",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Vm_requisitionDAO()
	{
		this("vm_requisition");		
	}
	
	public void setSearchColumn(Vm_requisitionDTO vm_requisitionDTO)
	{
		vm_requisitionDTO.searchColumn = "";
		vm_requisitionDTO.searchColumn += CatRepository.getName("English", "vehicle_requisition_purpose", vm_requisitionDTO.vehicleRequisitionPurposeCat) + " " + CatRepository.getName("Bangla", "vehicle_requisition_purpose", vm_requisitionDTO.vehicleRequisitionPurposeCat) + " ";
		vm_requisitionDTO.searchColumn += CatRepository.getName("English", "vehicle_type", vm_requisitionDTO.vehicleTypeCat) + " " + CatRepository.getName("Bangla", "vehicle_type", vm_requisitionDTO.vehicleTypeCat) + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.tripDescription + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.decisionOn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.decisionDescription + " ";
//		vm_requisitionDTO.searchColumn += CommonDAO.getName("English", "given_vehicle", vm_requisitionDTO.givenVehicleType) + " " + CommonDAO.getName("Bangla", "given_vehicle", vm_requisitionDTO.givenVehicleType) + " ";
//		vm_requisitionDTO.searchColumn += CommonDAO.getName("English", "payment", vm_requisitionDTO.paymentType) + " " + CommonDAO.getName("Bangla", "payment", vm_requisitionDTO.paymentType) + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.requesterPhoneNum + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.decisionByPhoneNum + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.driverPhoneNum + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.requesterNameEn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.decisionByNameEn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.driverNameEn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.requesterNameBn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.decisionByNameBn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.driverNameBn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.requesterOfficeNameEn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.decisionByOfficeNameEn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.driverOfficeNameEn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.requesterOfficeNameBn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.decisionByOfficeNameBn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.driverOfficeNameBn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.requesterOfficeUnitNameEn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.decisionByOfficeUnitNameEn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.driverOfficeUnitNameEn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.requesterOfficeUnitNameBn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.decisionByOfficeUnitNameBn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.driverOfficeUnitNameBn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.requesterOfficeUnitOrgNameEn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.decisionByOfficeUnitOrgNameEn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.driverOfficeUnitOrgNameEn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.requesterOfficeUnitOrgNameBn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.decisionByOfficeUnitOrgNameBn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.driverOfficeUnitOrgNameBn + " ";
		vm_requisitionDTO.searchColumn += vm_requisitionDTO.paidAmount + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Vm_requisitionDTO vm_requisitionDTO = (Vm_requisitionDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(vm_requisitionDTO);
		if(isInsert)
		{
			ps.setObject(index++,vm_requisitionDTO.iD);
		}
		ps.setObject(index++,vm_requisitionDTO.insertedByUserId);
		ps.setObject(index++,vm_requisitionDTO.insertedByOrganogramId);
		ps.setObject(index++,vm_requisitionDTO.insertionDate);
		ps.setObject(index++,vm_requisitionDTO.searchColumn);
		ps.setObject(index++,vm_requisitionDTO.requesterOrgId);
		ps.setObject(index++,vm_requisitionDTO.decisionByOrgId);
		ps.setObject(index++,vm_requisitionDTO.driverOrgId);
		ps.setObject(index++,vm_requisitionDTO.startDate);
		ps.setObject(index++,vm_requisitionDTO.endDate);
		ps.setObject(index++,vm_requisitionDTO.startAddress);
		ps.setObject(index++,vm_requisitionDTO.endAddress);
		ps.setObject(index++,vm_requisitionDTO.vehicleRequisitionPurposeCat);
		ps.setObject(index++,vm_requisitionDTO.vehicleTypeCat);
		ps.setObject(index++,vm_requisitionDTO.tripDescription);
		ps.setObject(index++,vm_requisitionDTO.status);
		ps.setObject(index++,vm_requisitionDTO.decisionOn);
		ps.setObject(index++,vm_requisitionDTO.decisionDescription);
		ps.setObject(index++,vm_requisitionDTO.givenVehicleType);
		ps.setObject(index++,vm_requisitionDTO.givenVehicleId);
		ps.setObject(index++,vm_requisitionDTO.receiveDate);
		ps.setObject(index++,vm_requisitionDTO.totalTripDistance);
		ps.setObject(index++,vm_requisitionDTO.meterReadingInitial);
		ps.setObject(index++,vm_requisitionDTO.meterReadingEnd);
		ps.setObject(index++,vm_requisitionDTO.totalTripTime);
		ps.setObject(index++,vm_requisitionDTO.petrolGiven);
		ps.setObject(index++,vm_requisitionDTO.petrolPreviousStock);
		ps.setObject(index++,vm_requisitionDTO.petrolAmount);
		ps.setObject(index++,vm_requisitionDTO.petrolConsumption);
		ps.setObject(index++,vm_requisitionDTO.petrolStock);
		ps.setObject(index++,vm_requisitionDTO.paymentGiven);
		ps.setObject(index++,vm_requisitionDTO.paymentType);
		ps.setObject(index++,vm_requisitionDTO.startTime);
		ps.setObject(index++,vm_requisitionDTO.endTime);
		ps.setObject(index++,vm_requisitionDTO.receiveTime);
		ps.setObject(index++,vm_requisitionDTO.requesterOfficeId);
		ps.setObject(index++,vm_requisitionDTO.decisionByOfficeId);
		ps.setObject(index++,vm_requisitionDTO.driverOfficeId);
		ps.setObject(index++,vm_requisitionDTO.requesterOfficeUnitId);
		ps.setObject(index++,vm_requisitionDTO.decisionByOfficeUnitId);
		ps.setObject(index++,vm_requisitionDTO.driverOfficeUnitId);
		ps.setObject(index++,vm_requisitionDTO.requesterEmpId);
		ps.setObject(index++,vm_requisitionDTO.decisionByEmpId);
		ps.setObject(index++,vm_requisitionDTO.driverEmpId);
		ps.setObject(index++,vm_requisitionDTO.requesterPhoneNum);
		ps.setObject(index++,vm_requisitionDTO.decisionByPhoneNum);
		ps.setObject(index++,vm_requisitionDTO.driverPhoneNum);
		ps.setObject(index++,vm_requisitionDTO.requesterNameEn);
		ps.setObject(index++,vm_requisitionDTO.decisionByNameEn);
		ps.setObject(index++,vm_requisitionDTO.driverNameEn);
		ps.setObject(index++,vm_requisitionDTO.requesterNameBn);
		ps.setObject(index++,vm_requisitionDTO.decisionByNameBn);
		ps.setObject(index++,vm_requisitionDTO.driverNameBn);
		ps.setObject(index++,vm_requisitionDTO.requesterOfficeNameEn);
		ps.setObject(index++,vm_requisitionDTO.decisionByOfficeNameEn);
		ps.setObject(index++,vm_requisitionDTO.driverOfficeNameEn);
		ps.setObject(index++,vm_requisitionDTO.requesterOfficeNameBn);
		ps.setObject(index++,vm_requisitionDTO.decisionByOfficeNameBn);
		ps.setObject(index++,vm_requisitionDTO.driverOfficeNameBn);
		ps.setObject(index++,vm_requisitionDTO.requesterOfficeUnitNameEn);
		ps.setObject(index++,vm_requisitionDTO.decisionByOfficeUnitNameEn);
		ps.setObject(index++,vm_requisitionDTO.driverOfficeUnitNameEn);
		ps.setObject(index++,vm_requisitionDTO.requesterOfficeUnitNameBn);
		ps.setObject(index++,vm_requisitionDTO.decisionByOfficeUnitNameBn);
		ps.setObject(index++,vm_requisitionDTO.driverOfficeUnitNameBn);
		ps.setObject(index++,vm_requisitionDTO.requesterOfficeUnitOrgNameEn);
		ps.setObject(index++,vm_requisitionDTO.decisionByOfficeUnitOrgNameEn);
		ps.setObject(index++,vm_requisitionDTO.driverOfficeUnitOrgNameEn);
		ps.setObject(index++,vm_requisitionDTO.requesterOfficeUnitOrgNameBn);
		ps.setObject(index++,vm_requisitionDTO.decisionByOfficeUnitOrgNameBn);
		ps.setObject(index++,vm_requisitionDTO.driverOfficeUnitOrgNameBn);
		ps.setObject(index++,vm_requisitionDTO.paidAmount);
		ps.setObject(index++,vm_requisitionDTO.vehicleRequisitionLocationCat);
		ps.setObject(index++,vm_requisitionDTO.filesDropzone);
		ps.setObject(index++,vm_requisitionDTO.firstApproverOrgId);
		ps.setObject(index++,vm_requisitionDTO.firstApproverOfficeId);
		ps.setObject(index++,vm_requisitionDTO.firstApproverOfficeUnitId);
		ps.setObject(index++,vm_requisitionDTO.firstApproverEmpId);
		ps.setObject(index++,vm_requisitionDTO.firstApproverPhoneNum);
		ps.setObject(index++,vm_requisitionDTO.firstApproverNameEn);
		ps.setObject(index++,vm_requisitionDTO.firstApproverNameBn);
		ps.setObject(index++,vm_requisitionDTO.firstApproverOfficeNameEn);
		ps.setObject(index++,vm_requisitionDTO.firstApproverOfficeNameBn);
		ps.setObject(index++,vm_requisitionDTO.firstApproverOfficeUnitNameEn);
		ps.setObject(index++,vm_requisitionDTO.firstApproverOfficeUnitNameBn);
		ps.setObject(index++,vm_requisitionDTO.firstApproverOfficeUnitOrgNameEn);
		ps.setObject(index++,vm_requisitionDTO.firstApproverOfficeUnitOrgNameBn);
		ps.setObject(index++,vm_requisitionDTO.cancellationReason);
		ps.setObject(index++,vm_requisitionDTO.cancellationDate);
		ps.setObject(index++,vm_requisitionDTO.firstApproverApproveOrRejectionDate);
		ps.setObject(index++,vm_requisitionDTO.secondApproverApproveOrRejectionDate);
		ps.setObject(index++,vm_requisitionDTO.firstApproverStatus);
		ps.setObject(index++,vm_requisitionDTO.secondApproverStatus);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Vm_requisitionDTO vm_requisitionDTO, ResultSet rs) throws SQLException
	{
		vm_requisitionDTO.iD = rs.getLong("ID");
		vm_requisitionDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		vm_requisitionDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
		vm_requisitionDTO.insertionDate = rs.getLong("insertion_date");
		vm_requisitionDTO.isDeleted = rs.getInt("isDeleted");
		vm_requisitionDTO.lastModificationTime = rs.getLong("lastModificationTime");
		vm_requisitionDTO.searchColumn = rs.getString("search_column");
		vm_requisitionDTO.requesterOrgId = rs.getLong("requester_org_id");
		vm_requisitionDTO.decisionByOrgId = rs.getLong("decision_by_org_id");
		vm_requisitionDTO.driverOrgId = rs.getLong("driver_org_id");
		vm_requisitionDTO.startDate = rs.getLong("start_date");
		vm_requisitionDTO.endDate = rs.getLong("end_date");
		vm_requisitionDTO.startAddress = rs.getString("start_address");
		vm_requisitionDTO.endAddress = rs.getString("end_address");
		vm_requisitionDTO.vehicleRequisitionPurposeCat = rs.getInt("vehicle_requisition_purpose_cat");
		vm_requisitionDTO.vehicleTypeCat = rs.getInt("vehicle_type_cat");
		vm_requisitionDTO.tripDescription = rs.getString("trip_description");
		vm_requisitionDTO.status = rs.getInt("status");
		vm_requisitionDTO.decisionOn = rs.getLong("decision_on");
		vm_requisitionDTO.decisionDescription = rs.getString("decision_description");
		vm_requisitionDTO.givenVehicleType = rs.getInt("given_vehicle_type");
		vm_requisitionDTO.givenVehicleId = rs.getLong("given_vehicle_id");
		vm_requisitionDTO.receiveDate = rs.getLong("receive_date");
		vm_requisitionDTO.totalTripDistance = rs.getDouble("total_trip_distance");
		vm_requisitionDTO.meterReadingInitial = rs.getDouble("meter_reading_initial");
		vm_requisitionDTO.meterReadingEnd = rs.getDouble("meter_reading_end");
		vm_requisitionDTO.totalTripTime = rs.getDouble("total_trip_time");
		vm_requisitionDTO.petrolGiven = rs.getInt("petrol_given");
		vm_requisitionDTO.petrolPreviousStock = rs.getDouble("petrol_previous_stock");
		vm_requisitionDTO.petrolAmount = rs.getDouble("petrol_amount");
		vm_requisitionDTO.petrolConsumption = rs.getDouble("petrol_consumption");
		vm_requisitionDTO.petrolStock = rs.getDouble("petrol_stock");
		vm_requisitionDTO.paymentGiven = rs.getInt("payment_given");
		vm_requisitionDTO.paymentType = rs.getInt("payment_type");
		vm_requisitionDTO.startTime = rs.getString("start_time");
		vm_requisitionDTO.endTime = rs.getString("end_time");
		vm_requisitionDTO.receiveTime = rs.getString("receive_time");
		vm_requisitionDTO.requesterOfficeId = rs.getLong("requester_office_id");
		vm_requisitionDTO.decisionByOfficeId = rs.getLong("decision_by_office_id");
		vm_requisitionDTO.driverOfficeId = rs.getLong("driver_office_id");
		vm_requisitionDTO.requesterOfficeUnitId = rs.getLong("requester_office_unit_id");
		vm_requisitionDTO.decisionByOfficeUnitId = rs.getLong("decision_by_office_unit_id");
		vm_requisitionDTO.driverOfficeUnitId = rs.getLong("driver_office_unit_id");
		vm_requisitionDTO.requesterEmpId = rs.getLong("requester_emp_id");
		vm_requisitionDTO.decisionByEmpId = rs.getLong("decision_by_emp_id");
		vm_requisitionDTO.driverEmpId = rs.getLong("driver_emp_id");
		vm_requisitionDTO.requesterPhoneNum = rs.getString("requester_phone_num");
		vm_requisitionDTO.decisionByPhoneNum = rs.getString("decision_by_phone_num");
		vm_requisitionDTO.driverPhoneNum = rs.getString("driver_phone_num");
		vm_requisitionDTO.requesterNameEn = rs.getString("requester_name_en");
		vm_requisitionDTO.decisionByNameEn = rs.getString("decision_by_name_en");
		vm_requisitionDTO.driverNameEn = rs.getString("driver_name_en");
		vm_requisitionDTO.requesterNameBn = rs.getString("requester_name_bn");
		vm_requisitionDTO.decisionByNameBn = rs.getString("decision_by_name_bn");
		vm_requisitionDTO.driverNameBn = rs.getString("driver_name_bn");
		vm_requisitionDTO.requesterOfficeNameEn = rs.getString("requester_office_name_en");
		vm_requisitionDTO.decisionByOfficeNameEn = rs.getString("decision_by_office_name_en");
		vm_requisitionDTO.driverOfficeNameEn = rs.getString("driver_office_name_en");
		vm_requisitionDTO.requesterOfficeNameBn = rs.getString("requester_office_name_bn");
		vm_requisitionDTO.decisionByOfficeNameBn = rs.getString("decision_by_office_name_bn");
		vm_requisitionDTO.driverOfficeNameBn = rs.getString("driver_office_name_bn");
		vm_requisitionDTO.requesterOfficeUnitNameEn = rs.getString("requester_office_unit_name_en");
		vm_requisitionDTO.decisionByOfficeUnitNameEn = rs.getString("decision_by_office_unit_name_en");
		vm_requisitionDTO.driverOfficeUnitNameEn = rs.getString("driver_office_unit_name_en");
		vm_requisitionDTO.requesterOfficeUnitNameBn = rs.getString("requester_office_unit_name_bn");
		vm_requisitionDTO.decisionByOfficeUnitNameBn = rs.getString("decision_by_office_unit_name_bn");
		vm_requisitionDTO.driverOfficeUnitNameBn = rs.getString("driver_office_unit_name_bn");
		vm_requisitionDTO.requesterOfficeUnitOrgNameEn = rs.getString("requester_office_unit_org_name_en");
		vm_requisitionDTO.decisionByOfficeUnitOrgNameEn = rs.getString("decision_by_office_unit_org_name_en");
		vm_requisitionDTO.driverOfficeUnitOrgNameEn = rs.getString("driver_office_unit_org_name_en");
		vm_requisitionDTO.requesterOfficeUnitOrgNameBn = rs.getString("requester_office_unit_org_name_bn");
		vm_requisitionDTO.decisionByOfficeUnitOrgNameBn = rs.getString("decision_by_office_unit_org_name_bn");
		vm_requisitionDTO.driverOfficeUnitOrgNameBn = rs.getString("driver_office_unit_org_name_bn");
		vm_requisitionDTO.paidAmount = Double.parseDouble(rs.getString("paidAmount"));
		vm_requisitionDTO.vehicleRequisitionLocationCat = rs.getInt("vehicle_requisition_location_cat");
		vm_requisitionDTO.filesDropzone = rs.getLong("files_dropzone");

		vm_requisitionDTO.firstApproverOrgId = rs.getLong("first_approver_org_id");
		vm_requisitionDTO.firstApproverOfficeId = rs.getLong("first_approver_office_id");
		vm_requisitionDTO.firstApproverOfficeUnitId = rs.getLong("first_approver_office_unit_id");
		vm_requisitionDTO.firstApproverEmpId = rs.getLong("first_approver_emp_id");
		vm_requisitionDTO.firstApproverPhoneNum = rs.getString("first_approver_phone_num");
		vm_requisitionDTO.firstApproverNameEn = rs.getString("first_approver_name_en");
		vm_requisitionDTO.firstApproverNameBn = rs.getString("first_approver_name_bn");
		vm_requisitionDTO.firstApproverOfficeNameEn = rs.getString("first_approver_office_name_en");
		vm_requisitionDTO.firstApproverOfficeNameBn = rs.getString("first_approver_office_name_bn");
		vm_requisitionDTO.firstApproverOfficeUnitNameEn = rs.getString("first_approver_office_unit_name_en");
		vm_requisitionDTO.firstApproverOfficeUnitNameBn = rs.getString("first_approver_office_unit_name_bn");
		vm_requisitionDTO.firstApproverOfficeUnitOrgNameEn = rs.getString("first_approver_office_unit_org_name_en");
		vm_requisitionDTO.firstApproverOfficeUnitOrgNameBn = rs.getString("first_approver_office_unit_org_name_bn");
		vm_requisitionDTO.cancellationReason = rs.getString("cancellation_reason");
		vm_requisitionDTO.cancellationDate = rs.getLong("cancellation_date");
		vm_requisitionDTO.firstApproverApproveOrRejectionDate = rs.getLong("first_approver_approve_or_rejection_date");
		vm_requisitionDTO.secondApproverApproveOrRejectionDate = rs.getLong("second_approver_approve_or_rejection_date");

		vm_requisitionDTO.firstApproverStatus = rs.getInt("first_approver_status");
		vm_requisitionDTO.secondApproverStatus = rs.getInt("second_approver_status");
	}



	public Vm_requisitionDTO build(ResultSet rs)
	{
		try
		{
			Vm_requisitionDTO vm_requisitionDTO = new Vm_requisitionDTO();
			vm_requisitionDTO.iD = rs.getLong("ID");
			vm_requisitionDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			vm_requisitionDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			vm_requisitionDTO.insertionDate = rs.getLong("insertion_date");
			vm_requisitionDTO.isDeleted = rs.getInt("isDeleted");
			vm_requisitionDTO.lastModificationTime = rs.getLong("lastModificationTime");
			vm_requisitionDTO.searchColumn = rs.getString("search_column");
			vm_requisitionDTO.requesterOrgId = rs.getLong("requester_org_id");
			vm_requisitionDTO.decisionByOrgId = rs.getLong("decision_by_org_id");
			vm_requisitionDTO.driverOrgId = rs.getLong("driver_org_id");
			vm_requisitionDTO.startDate = rs.getLong("start_date");
			vm_requisitionDTO.endDate = rs.getLong("end_date");
			vm_requisitionDTO.startAddress = rs.getString("start_address");
			vm_requisitionDTO.endAddress = rs.getString("end_address");
			vm_requisitionDTO.vehicleRequisitionPurposeCat = rs.getInt("vehicle_requisition_purpose_cat");
			vm_requisitionDTO.vehicleTypeCat = rs.getInt("vehicle_type_cat");
			vm_requisitionDTO.tripDescription = rs.getString("trip_description");
			vm_requisitionDTO.status = rs.getInt("status");
			vm_requisitionDTO.decisionOn = rs.getLong("decision_on");
			vm_requisitionDTO.decisionDescription = rs.getString("decision_description");
			vm_requisitionDTO.givenVehicleType = rs.getInt("given_vehicle_type");
			vm_requisitionDTO.givenVehicleId = rs.getLong("given_vehicle_id");
			vm_requisitionDTO.receiveDate = rs.getLong("receive_date");
			vm_requisitionDTO.totalTripDistance = rs.getDouble("total_trip_distance");
			vm_requisitionDTO.meterReadingInitial = rs.getDouble("meter_reading_initial");
			vm_requisitionDTO.meterReadingEnd = rs.getDouble("meter_reading_end");
			vm_requisitionDTO.totalTripTime = rs.getDouble("total_trip_time");
			vm_requisitionDTO.petrolGiven = rs.getInt("petrol_given");
			vm_requisitionDTO.petrolPreviousStock = rs.getDouble("petrol_previous_stock");
			vm_requisitionDTO.petrolAmount = rs.getDouble("petrol_amount");
			vm_requisitionDTO.petrolConsumption = rs.getDouble("petrol_consumption");
			vm_requisitionDTO.petrolStock = rs.getDouble("petrol_stock");
			vm_requisitionDTO.paymentGiven = rs.getInt("payment_given");
			vm_requisitionDTO.paymentType = rs.getInt("payment_type");
			vm_requisitionDTO.startTime = rs.getString("start_time");
			vm_requisitionDTO.endTime = rs.getString("end_time");
			vm_requisitionDTO.receiveTime = rs.getString("receive_time");
			vm_requisitionDTO.requesterOfficeId = rs.getLong("requester_office_id");
			vm_requisitionDTO.decisionByOfficeId = rs.getLong("decision_by_office_id");
			vm_requisitionDTO.driverOfficeId = rs.getLong("driver_office_id");
			vm_requisitionDTO.requesterOfficeUnitId = rs.getLong("requester_office_unit_id");
			vm_requisitionDTO.decisionByOfficeUnitId = rs.getLong("decision_by_office_unit_id");
			vm_requisitionDTO.driverOfficeUnitId = rs.getLong("driver_office_unit_id");
			vm_requisitionDTO.requesterEmpId = rs.getLong("requester_emp_id");
			vm_requisitionDTO.decisionByEmpId = rs.getLong("decision_by_emp_id");
			vm_requisitionDTO.driverEmpId = rs.getLong("driver_emp_id");
			vm_requisitionDTO.requesterPhoneNum = rs.getString("requester_phone_num");
			vm_requisitionDTO.decisionByPhoneNum = rs.getString("decision_by_phone_num");
			vm_requisitionDTO.driverPhoneNum = rs.getString("driver_phone_num");
			vm_requisitionDTO.requesterNameEn = rs.getString("requester_name_en");
			vm_requisitionDTO.decisionByNameEn = rs.getString("decision_by_name_en");
			vm_requisitionDTO.driverNameEn = rs.getString("driver_name_en");
			vm_requisitionDTO.requesterNameBn = rs.getString("requester_name_bn");
			vm_requisitionDTO.decisionByNameBn = rs.getString("decision_by_name_bn");
			vm_requisitionDTO.driverNameBn = rs.getString("driver_name_bn");
			vm_requisitionDTO.requesterOfficeNameEn = rs.getString("requester_office_name_en");
			vm_requisitionDTO.decisionByOfficeNameEn = rs.getString("decision_by_office_name_en");
			vm_requisitionDTO.driverOfficeNameEn = rs.getString("driver_office_name_en");
			vm_requisitionDTO.requesterOfficeNameBn = rs.getString("requester_office_name_bn");
			vm_requisitionDTO.decisionByOfficeNameBn = rs.getString("decision_by_office_name_bn");
			vm_requisitionDTO.driverOfficeNameBn = rs.getString("driver_office_name_bn");
			vm_requisitionDTO.requesterOfficeUnitNameEn = rs.getString("requester_office_unit_name_en");
			vm_requisitionDTO.decisionByOfficeUnitNameEn = rs.getString("decision_by_office_unit_name_en");
			vm_requisitionDTO.driverOfficeUnitNameEn = rs.getString("driver_office_unit_name_en");
			vm_requisitionDTO.requesterOfficeUnitNameBn = rs.getString("requester_office_unit_name_bn");
			vm_requisitionDTO.decisionByOfficeUnitNameBn = rs.getString("decision_by_office_unit_name_bn");
			vm_requisitionDTO.driverOfficeUnitNameBn = rs.getString("driver_office_unit_name_bn");
			vm_requisitionDTO.requesterOfficeUnitOrgNameEn = rs.getString("requester_office_unit_org_name_en");
			vm_requisitionDTO.decisionByOfficeUnitOrgNameEn = rs.getString("decision_by_office_unit_org_name_en");
			vm_requisitionDTO.driverOfficeUnitOrgNameEn = rs.getString("driver_office_unit_org_name_en");
			vm_requisitionDTO.requesterOfficeUnitOrgNameBn = rs.getString("requester_office_unit_org_name_bn");
			vm_requisitionDTO.decisionByOfficeUnitOrgNameBn = rs.getString("decision_by_office_unit_org_name_bn");
			vm_requisitionDTO.driverOfficeUnitOrgNameBn = rs.getString("driver_office_unit_org_name_bn");
			vm_requisitionDTO.paidAmount = Double.parseDouble(rs.getString("paidAmount"));
			vm_requisitionDTO.vehicleRequisitionLocationCat = rs.getInt("vehicle_requisition_location_cat");
			vm_requisitionDTO.filesDropzone = rs.getLong("files_dropzone");
			vm_requisitionDTO.firstApproverOrgId = rs.getLong("first_approver_org_id");
			vm_requisitionDTO.firstApproverOfficeId = rs.getLong("first_approver_office_id");
			vm_requisitionDTO.firstApproverOfficeUnitId = rs.getLong("first_approver_office_unit_id");
			vm_requisitionDTO.firstApproverEmpId = rs.getLong("first_approver_emp_id");
			vm_requisitionDTO.firstApproverPhoneNum = rs.getString("first_approver_phone_num");
			vm_requisitionDTO.firstApproverNameEn = rs.getString("first_approver_name_en");
			vm_requisitionDTO.firstApproverNameBn = rs.getString("first_approver_name_bn");
			vm_requisitionDTO.firstApproverOfficeNameEn = rs.getString("first_approver_office_name_en");
			vm_requisitionDTO.firstApproverOfficeNameBn = rs.getString("first_approver_office_name_bn");
			vm_requisitionDTO.firstApproverOfficeUnitNameEn = rs.getString("first_approver_office_unit_name_en");
			vm_requisitionDTO.firstApproverOfficeUnitNameBn = rs.getString("first_approver_office_unit_name_bn");
			vm_requisitionDTO.firstApproverOfficeUnitOrgNameEn = rs.getString("first_approver_office_unit_org_name_en");
			vm_requisitionDTO.firstApproverOfficeUnitOrgNameBn = rs.getString("first_approver_office_unit_org_name_bn");
			vm_requisitionDTO.cancellationReason = rs.getString("cancellation_reason");
			vm_requisitionDTO.cancellationDate = rs.getLong("cancellation_date");
			vm_requisitionDTO.firstApproverApproveOrRejectionDate = rs.getLong("first_approver_approve_or_rejection_date");
			vm_requisitionDTO.secondApproverApproveOrRejectionDate = rs.getLong("second_approver_approve_or_rejection_date");

			vm_requisitionDTO.firstApproverStatus = rs.getInt("first_approver_status");
			vm_requisitionDTO.secondApproverStatus = rs.getInt("second_approver_status");
			return vm_requisitionDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	public Long buildId(ResultSet rs)
	{
		try
		{
			return rs.getLong("ID");
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	//need another getter for repository
	public Vm_requisitionDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		Vm_requisitionDTO vm_requisitionDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
		return vm_requisitionDTO;
	}

	public Vm_requisitionDTO getLastUsedVehicleDTO(Long requesterEmpId, Long vehicleId, Long requisitionId, int status){
		String sql = String.format(getLastUsageOfSameVehicle, requesterEmpId, vehicleId,requisitionId, status);
		return ConnectionAndStatementUtil.getT(sql, this::build);
	}




	public List<Vm_requisitionDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	}
	
	

	
	
	
	//add repository
	public List<Vm_requisitionDTO> getAllVm_requisition (boolean isFirstReload)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}

	
	public List<Vm_requisitionDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}


	public List<Vm_requisitionDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		List<Object> objectList = new ArrayList<Object>();

		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat, objectList);
		return ConnectionAndStatementUtil.getListOfT(sql,objectList,this::build);
	}
	public Collection getDriverIDs(Vm_requisitionDTO vm_requisitionDTO)
			{
		System.out.println("table: " + "getDriverIDs");
		List<Long> idList = new ArrayList<>();

		try {

			String filter = " (start_date < " + vm_requisitionDTO.endDate;
			filter += " OR (start_date = " + vm_requisitionDTO.endDate;
			filter += " AND (STR_TO_DATE( start_time , '%l:%i %p' )) <= " + "(STR_TO_DATE( '" + vm_requisitionDTO.endTime + "' , '%l:%i %p' ))))";
			filter += " AND (end_date > " + vm_requisitionDTO.startDate;
			filter += " OR (end_date = " + vm_requisitionDTO.startDate;
			filter += " AND (STR_TO_DATE( end_time , '%l:%i %p' )) >= " + "(STR_TO_DATE( '" + vm_requisitionDTO.startTime + "' , '%l:%i %p' ))))" + " ";

			String sql = getSqlWithSearchCriteria(null, -1, -1, GETDRIVERIDS, true, userDTO, filter, false, new ArrayList<>());

			return ConnectionAndStatementUtil.getListOfT(sql,this::buildId);


		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return idList;
	}
	public Collection getVehicleIDs(Vm_requisitionDTO vm_requisitionDTO)
			{
		System.out.println("table: " + "getVehicleIDs");
		List<Long> idList = new ArrayList<>();

		try {

			String filter = " (start_date < " + vm_requisitionDTO.endDate;
			filter += " OR (start_date = " + vm_requisitionDTO.endDate;
			filter += " AND (STR_TO_DATE( start_time , '%l:%i %p' )) <= " + "(STR_TO_DATE( '" + vm_requisitionDTO.endTime + "' , '%l:%i %p' ))))";
			filter += " AND (end_date > " + vm_requisitionDTO.startDate;
			filter += " OR (end_date = " + vm_requisitionDTO.startDate;
			filter += " AND (STR_TO_DATE( end_time , '%l:%i %p' )) >= " + "(STR_TO_DATE( '" + vm_requisitionDTO.startTime + "' , '%l:%i %p' ))))" + " ";

			String sql = getSqlWithSearchCriteriaForNormalSearchWithoutLastModificationOrder(null, -1, -1, GETVEHICLEIDS, userDTO, filter, false, new ArrayList<>());

			return ConnectionAndStatementUtil.getListOfT(sql,this::buildId);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return idList;
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}else if(category == GETDRIVERIDS)
		{
			sql += " distinct " + tableName + ".driver_emp_id as ID ";
		}else if(category == GETVEHICLEIDS)
		{
			sql += " distinct " + tableName + ".given_vehicle_id as ID ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ?";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println("vm req search: "+str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
						|| str.equals("start_date_start")
						|| str.equals("start_date_end")
						|| str.equals("end_date_start")
						|| str.equals("end_date_end")
						|| str.equals("vehicle_requisition_purpose_cat")
						|| str.equals("vehicle_type_cat")
						|| str.equals("status")
						|| str.equals("trip_description")
						|| str.equals("decision_on")
						|| str.equals("decision_description")
						|| str.equals("given_vehicle_type")
						|| str.equals("receive_date_start")
						|| str.equals("receive_date_end")
						|| str.equals("payment_type")
						|| str.equals("requester_phone_num")
						|| str.equals("decision_by_phone_num")
						|| str.equals("driver_phone_num")
						|| str.equals("requester_name_en")
						|| str.equals("decision_by_name_en")
						|| str.equals("driver_name_en")
						|| str.equals("requester_name_bn")
						|| str.equals("decision_by_name_bn")
						|| str.equals("driver_name_bn")
						|| str.equals("requester_office_name_en")
						|| str.equals("decision_by_office_name_en")
						|| str.equals("driver_office_name_en")
						|| str.equals("requester_office_name_bn")
						|| str.equals("decision_by_office_name_bn")
						|| str.equals("driver_office_name_bn")
						|| str.equals("requester_office_unit_name_en")
						|| str.equals("decision_by_office_unit_name_en")
						|| str.equals("driver_office_unit_name_en")
						|| str.equals("requester_office_unit_name_bn")
						|| str.equals("decision_by_office_unit_name_bn")
						|| str.equals("driver_office_unit_name_bn")
						|| str.equals("requester_office_unit_org_name_en")
						|| str.equals("decision_by_office_unit_org_name_en")
						|| str.equals("driver_office_unit_org_name_en")
						|| str.equals("requester_office_unit_org_name_bn")
						|| str.equals("decision_by_office_unit_org_name_bn")
						|| str.equals("driver_office_unit_org_name_bn")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("start_date_start"))
					{
						AllFieldSql += "" + tableName + ".start_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("start_date_end"))
					{
						AllFieldSql += "" + tableName + ".start_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("end_date_start"))
					{
						AllFieldSql += "" + tableName + ".end_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("end_date_end"))
					{
						AllFieldSql += "" + tableName + ".end_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("vehicle_requisition_purpose_cat"))
					{
						AllFieldSql += "" + tableName + ".vehicle_requisition_purpose_cat = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("vehicle_type_cat"))
					{
						AllFieldSql += "" + tableName + ".vehicle_type_cat = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("status"))
					{
						AllFieldSql += "" + tableName + ".status = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("trip_description"))
					{
						AllFieldSql += "" + tableName + ".trip_description like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_on"))
					{
						AllFieldSql += "" + tableName + ".decision_on like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_description"))
					{
						AllFieldSql += "" + tableName + ".decision_description like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("given_vehicle_type"))
					{
						AllFieldSql += "" + tableName + ".given_vehicle_type = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("receive_date_start"))
					{
						AllFieldSql += "" + tableName + ".receive_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("receive_date_end"))
					{
						AllFieldSql += "" + tableName + ".receive_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("payment_type"))
					{
						AllFieldSql += "" + tableName + ".payment_type = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("requester_phone_num"))
					{
						AllFieldSql += "" + tableName + ".requester_phone_num like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_phone_num"))
					{
						AllFieldSql += "" + tableName + ".decision_by_phone_num like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_phone_num"))
					{
						AllFieldSql += "" + tableName + ".driver_phone_num like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_name_en"))
					{
						AllFieldSql += "" + tableName + ".decision_by_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_name_en"))
					{
						AllFieldSql += "" + tableName + ".driver_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_name_bn"))
					{
						AllFieldSql += "" + tableName + ".decision_by_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_name_bn"))
					{
						AllFieldSql += "" + tableName + ".driver_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_office_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_office_name_en"))
					{
						AllFieldSql += "" + tableName + ".decision_by_office_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_office_name_en"))
					{
						AllFieldSql += "" + tableName + ".driver_office_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_office_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_office_name_bn"))
					{
						AllFieldSql += "" + tableName + ".decision_by_office_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_office_name_bn"))
					{
						AllFieldSql += "" + tableName + ".driver_office_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_office_unit_name_en"))
					{
						AllFieldSql += "" + tableName + ".decision_by_office_unit_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_office_unit_name_en"))
					{
						AllFieldSql += "" + tableName + ".driver_office_unit_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_office_unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".decision_by_office_unit_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_office_unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".driver_office_unit_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_org_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_org_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_office_unit_org_name_en"))
					{
						AllFieldSql += "" + tableName + ".decision_by_office_unit_org_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_office_unit_org_name_en"))
					{
						AllFieldSql += "" + tableName + ".driver_office_unit_org_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_org_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_org_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_office_unit_org_name_bn"))
					{
						AllFieldSql += "" + tableName + ".decision_by_office_unit_org_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_office_unit_org_name_bn"))
					{
						AllFieldSql += "" + tableName + ".driver_office_unit_org_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }

	public String getSqlWithSearchCriteriaForNormalSearchWithoutLastModificationOrder(Hashtable p_searchCriteria, int limit, int offset, int category,
																					  UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
	{
		boolean viewAll = false;

		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}

		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}else if(category == GETDRIVERIDS)
		{
			sql += " distinct " + tableName + ".driver_emp_id as ID ";
		}else if(category == GETVEHICLEIDS)
		{
			sql += " distinct " + tableName + ".given_vehicle_id as ID ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;



		String AnyfieldSql = "";
		String AllFieldSql = "";

		if(p_searchCriteria != null)
		{


			Enumeration names = p_searchCriteria.keys();
			String str, value;

			AnyfieldSql = "(";

			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ?";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);

			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
				System.out.println("vm req search: "+str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						str.equals("insertion_date_start")
								|| str.equals("insertion_date_end")
								|| str.equals("start_date_start")
								|| str.equals("start_date_end")
								|| str.equals("end_date_start")
								|| str.equals("end_date_end")
								|| str.equals("vehicle_requisition_purpose_cat")
								|| str.equals("vehicle_type_cat")
								|| str.equals("status")
								|| str.equals("trip_description")
								|| str.equals("decision_on")
								|| str.equals("decision_description")
								|| str.equals("given_vehicle_type")
								|| str.equals("receive_date_start")
								|| str.equals("receive_date_end")
								|| str.equals("payment_type")
								|| str.equals("requester_phone_num")
								|| str.equals("decision_by_phone_num")
								|| str.equals("driver_phone_num")
								|| str.equals("requester_name_en")
								|| str.equals("decision_by_name_en")
								|| str.equals("driver_name_en")
								|| str.equals("requester_name_bn")
								|| str.equals("decision_by_name_bn")
								|| str.equals("driver_name_bn")
								|| str.equals("requester_office_name_en")
								|| str.equals("decision_by_office_name_en")
								|| str.equals("driver_office_name_en")
								|| str.equals("requester_office_name_bn")
								|| str.equals("decision_by_office_name_bn")
								|| str.equals("driver_office_name_bn")
								|| str.equals("requester_office_unit_name_en")
								|| str.equals("decision_by_office_unit_name_en")
								|| str.equals("driver_office_unit_name_en")
								|| str.equals("requester_office_unit_name_bn")
								|| str.equals("decision_by_office_unit_name_bn")
								|| str.equals("driver_office_unit_name_bn")
								|| str.equals("requester_office_unit_org_name_en")
								|| str.equals("decision_by_office_unit_org_name_en")
								|| str.equals("driver_office_unit_org_name_en")
								|| str.equals("requester_office_unit_org_name_bn")
								|| str.equals("decision_by_office_unit_org_name_bn")
								|| str.equals("driver_office_unit_org_name_bn")
				)

				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}

					if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("start_date_start"))
					{
						AllFieldSql += "" + tableName + ".start_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("start_date_end"))
					{
						AllFieldSql += "" + tableName + ".start_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("end_date_start"))
					{
						AllFieldSql += "" + tableName + ".end_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("end_date_end"))
					{
						AllFieldSql += "" + tableName + ".end_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("vehicle_requisition_purpose_cat"))
					{
						AllFieldSql += "" + tableName + ".vehicle_requisition_purpose_cat = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("vehicle_type_cat"))
					{
						AllFieldSql += "" + tableName + ".vehicle_type_cat = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("status"))
					{
						AllFieldSql += "" + tableName + ".status = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("trip_description"))
					{
						AllFieldSql += "" + tableName + ".trip_description like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_on"))
					{
						AllFieldSql += "" + tableName + ".decision_on like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_description"))
					{
						AllFieldSql += "" + tableName + ".decision_description like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("given_vehicle_type"))
					{
						AllFieldSql += "" + tableName + ".given_vehicle_type = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("receive_date_start"))
					{
						AllFieldSql += "" + tableName + ".receive_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("receive_date_end"))
					{
						AllFieldSql += "" + tableName + ".receive_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("payment_type"))
					{
						AllFieldSql += "" + tableName + ".payment_type = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("requester_phone_num"))
					{
						AllFieldSql += "" + tableName + ".requester_phone_num like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_phone_num"))
					{
						AllFieldSql += "" + tableName + ".decision_by_phone_num like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_phone_num"))
					{
						AllFieldSql += "" + tableName + ".driver_phone_num like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_name_en"))
					{
						AllFieldSql += "" + tableName + ".decision_by_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_name_en"))
					{
						AllFieldSql += "" + tableName + ".driver_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_name_bn"))
					{
						AllFieldSql += "" + tableName + ".decision_by_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_name_bn"))
					{
						AllFieldSql += "" + tableName + ".driver_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_office_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_office_name_en"))
					{
						AllFieldSql += "" + tableName + ".decision_by_office_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_office_name_en"))
					{
						AllFieldSql += "" + tableName + ".driver_office_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_office_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_office_name_bn"))
					{
						AllFieldSql += "" + tableName + ".decision_by_office_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_office_name_bn"))
					{
						AllFieldSql += "" + tableName + ".driver_office_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_office_unit_name_en"))
					{
						AllFieldSql += "" + tableName + ".decision_by_office_unit_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_office_unit_name_en"))
					{
						AllFieldSql += "" + tableName + ".driver_office_unit_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_office_unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".decision_by_office_unit_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_office_unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".driver_office_unit_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_org_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_org_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_office_unit_org_name_en"))
					{
						AllFieldSql += "" + tableName + ".decision_by_office_unit_org_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_office_unit_org_name_en"))
					{
						AllFieldSql += "" + tableName + ".driver_office_unit_org_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_org_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_org_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_office_unit_org_name_bn"))
					{
						AllFieldSql += "" + tableName + ".decision_by_office_unit_org_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_office_unit_org_name_bn"))
					{
						AllFieldSql += "" + tableName + ".driver_office_unit_org_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}


				}
			}

			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);


		}


		sql += " WHERE ";

		sql += " (" + tableName + ".isDeleted = 0 ";
		sql += ")";


		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}

		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;

		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{
			sql += " AND " + AllFieldSql;
		}



//		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);

		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}

		System.out.println("-------------- sql = " + sql);

		return sql;
	}


	public long diffBetweenTime(long dateSmaller, String timeSmaller, long dateBigger, String timeBigger) {

		if (timeSmaller.contains(" ")) {
			String[] timeSmallerSlices = timeSmaller.split(" ");
			String[] smallerHourMinute = timeSmallerSlices[0].split(":");

			if (timeSmallerSlices[1].equals("AM") && smallerHourMinute[0].equals("12"))smallerHourMinute[0]="00";
			long smallerMinutes = Long.parseLong(smallerHourMinute[0])*60 + Long.parseLong(smallerHourMinute[1]);
			if (timeSmallerSlices[1].equals("PM") && !smallerHourMinute[0].equals("12"))smallerMinutes += (12*60);
			long smallerTimeMillies = smallerMinutes*60*1000;
			dateSmaller += smallerTimeMillies;

		}

		if (timeBigger.contains(" ")) {
			String[] timeBiggerSlices = timeBigger.split(" ");
			String[] biggerHourMinute = timeBiggerSlices[0].split(":");

			if (timeBiggerSlices[1].equals("AM") && biggerHourMinute[0].equals("12"))biggerHourMinute[0]="00";
			long biggerMinutes = Long.parseLong(biggerHourMinute[0])*60 + Long.parseLong(biggerHourMinute[1]);
			if (timeBiggerSlices[1].equals("PM") && !biggerHourMinute[0].equals("12"))biggerMinutes += (12*60);
			long biggerTimeMillies = biggerMinutes*60*1000;
			dateBigger += biggerTimeMillies;

		}

		return dateBigger - dateSmaller;

	}

	public List<Vm_requisitionDTO> getAllVm_requisitionByVehicleType ()
	{

		List<Vm_requisitionDTO> requisitionDTOS = Vm_requisitionRepository.getInstance().getVm_requisitionList()
				.stream()
				.filter(i -> i.status == CommonApprovalStatus.SATISFIED.getValue() ||
						i.status == CommonApprovalStatus.PAID.getValue())
				.collect(Collectors.toList());

		List<Vm_requisitionDTO> dtos = new ArrayList<>();

		Set<Integer> typeIds = requisitionDTOS.stream().map(i -> i.vehicleTypeCat).collect(Collectors.toSet());
		typeIds.forEach(i -> {
			Vm_requisitionDTO dto = new Vm_requisitionDTO();
			dto.givenVehicleType = i;
			dto.requisitionCount = (int) requisitionDTOS.stream().filter(j -> j.vehicleTypeCat == i).count();
			dtos.add(dto);
		});

		return dtos;
	}

	public long getLongDateAndTime(long dateSmaller, String timeSmaller) {

		if (timeSmaller.contains(" ")) {
			String[] timeSmallerSlices = timeSmaller.split(" ");
			String[] smallerHourMinute = timeSmallerSlices[0].split(":");

			if (timeSmallerSlices[1].equals("AM") && smallerHourMinute[0].equals("12"))smallerHourMinute[0]="00";
			long smallerMinutes = Long.parseLong(smallerHourMinute[0])*60 + Long.parseLong(smallerHourMinute[1]);
			if (timeSmallerSlices[1].equals("PM") && !smallerHourMinute[0].equals("12"))smallerMinutes += (12*60);
			long smallerTimeMillies = smallerMinutes*60*1000;
			dateSmaller += smallerTimeMillies;

		}

		return dateSmaller;

	}
				
}
	