package vm_requisition;

import pb.OptionDTO;
import pb.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum CommonApprovalStatus {
    PENDING(1),
    SATISFIED(3),
    DISSATISFIED(4),
    PAID(12),
    CANCELLED(13),
    RECEIVED(14),
    WITHDRAWN(15),
    UNPAID(16),
    PRE_PENDING(17),
    USED(19),
    AVAILABLE(18),
    NOT_AVAILABLE(20),
    SECOND_APPROVER_PENDING(21),
    THIRD_APPROVER_PENDING(22),
    REQUISITION_NOT_APPROVED(31),
    REQUISITION_REJECTED_BY_FIRST_APPROVER(32),
    REQUISITION_APPROVED_BY_FIRST_APPROVER(33),
    REQUISITION_REJECTED_BY_SECOND_APPROVER(34),
    REQUISITION_APPROVED_BY_SECOND_APPROVER(35),
    REQUISITION_SUPPLIED_BY_STORE_KEEPER(36),
    AUCTIONED(40),
    ACTIVE(41),
    INACTIVE(42),
    CANCELLED_AUCTION(43);

    private final int value;

    CommonApprovalStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static String getColor(int value) {
        if (value == SATISFIED.getValue()) {
            return "green";
        } else if (value == DISSATISFIED.getValue()) {
            return "crimson";
        } else if (value == PAID.getValue()) {
            return "#5867dd";
        } else if (value == PENDING.getValue()) {
            return "blue";
        } else if (value == PRE_PENDING.getValue()) {
            return "#FF8C00"; //dark orange
        } else if (value == UNPAID.getValue()) {
            return "red";
        }
        return "#22ccc1";
    }

    public static String getText(int value, String language) {
        if (value == AVAILABLE.getValue()) {
            return language.equalsIgnoreCase("english") ? "Available" : "ব্যবহারযোগ্য";
        } else if (value == USED.getValue()) {
            return language.equalsIgnoreCase("english") ? "Used" : "ব্যবহৃত";
        } else if (value == NOT_AVAILABLE.getValue()) {
            return language.equalsIgnoreCase("english") ? "Not Available" : "ব্যবহারযোগ্য নয়";
        } else if (value == PENDING.getValue()) {
            return language.equalsIgnoreCase("english") ? "Pending" : "অনুমোদনের অপেক্ষায়";
        } else if (value == SECOND_APPROVER_PENDING.getValue()) {
            return language.equalsIgnoreCase("english") ? " Second Approval Pending" : "দ্বিতীয় অনুমোদনের অপেক্ষায়";
        } else if (value == THIRD_APPROVER_PENDING.getValue()) {
            return language.equalsIgnoreCase("english") ? " Third Approval Pending" : "তৃতীয় অনুমোদনের অপেক্ষায়";
        } else if (value == SATISFIED.getValue()) {
            return language.equalsIgnoreCase("english") ? "Approved" : "অনুমোদিত";
        } else if (value == AUCTIONED.getValue()) {
            return language.equalsIgnoreCase("english") ? "Auctioned" : "নিলামকৃত";
        }

        return "";

    }


    public static String getRequisitionText(int value, String language) {
        if (value == REQUISITION_NOT_APPROVED.getValue()) {
            return language.equalsIgnoreCase("english") ? "Applied for approval" : "অনুমোদনের জন্য পাঠানো হয়েছে";
        } else if (value == REQUISITION_REJECTED_BY_FIRST_APPROVER.getValue()) {
            return language.equalsIgnoreCase("english") ? "Requisition rejected" : "অনুরোধ বাতিল হয়েছে";
        } else if (value == REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue()) {
            return language.equalsIgnoreCase("english") ? "Applied for approval from admin office" : "এডমিন অফিসে অনুমোদনের জন্য পাঠানো হয়েছে";
        } else if (value == REQUISITION_REJECTED_BY_SECOND_APPROVER.getValue()) {
            return language.equalsIgnoreCase("english") ? "Requisition rejected" : "অনুরোধ বাতিল হয়েছে";
        } else if (value == REQUISITION_APPROVED_BY_SECOND_APPROVER.getValue()) {
            return language.equalsIgnoreCase("english") ? "Store keeper will supply requisition" : "স্টোর কিপার কর্তৃক সরবরাহ করা হবে";
        } else if (value == REQUISITION_SUPPLIED_BY_STORE_KEEPER.getValue()) {
            return language.equalsIgnoreCase("english") ? "Supplied" : "সরবরাহ করা হয়েছে";
        }

        return "";

    }

    public static String buildOptionRequisition(String language) {
        List<OptionDTO> optionDTOList;
        List<Option> nameDTOList = new ArrayList<>();
        nameDTOList.add(new Option(getRequisitionText(REQUISITION_NOT_APPROVED.getValue(), "bangla"), getRequisitionText(REQUISITION_NOT_APPROVED.getValue(), "english"), REQUISITION_NOT_APPROVED.getValue()));
        nameDTOList.add(new Option(getRequisitionText(REQUISITION_REJECTED_BY_FIRST_APPROVER.getValue(), "bangla"), getRequisitionText(REQUISITION_REJECTED_BY_FIRST_APPROVER.getValue(), "english"), REQUISITION_REJECTED_BY_FIRST_APPROVER.getValue()));
        nameDTOList.add(new Option(getRequisitionText(REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue(), "bangla"), getRequisitionText(REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue(), "english"), REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue()));
        nameDTOList.add(new Option(getRequisitionText(REQUISITION_REJECTED_BY_SECOND_APPROVER.getValue(), "bangla"), getRequisitionText(REQUISITION_REJECTED_BY_SECOND_APPROVER.getValue(), "english"), REQUISITION_REJECTED_BY_SECOND_APPROVER.getValue()));
        nameDTOList.add(new Option(getRequisitionText(REQUISITION_APPROVED_BY_SECOND_APPROVER.getValue(), "bangla"), getRequisitionText(REQUISITION_APPROVED_BY_SECOND_APPROVER.getValue(), "english"), REQUISITION_APPROVED_BY_SECOND_APPROVER.getValue()));
        nameDTOList.add(new Option(getRequisitionText(REQUISITION_SUPPLIED_BY_STORE_KEEPER.getValue(), "bangla"), getRequisitionText(REQUISITION_SUPPLIED_BY_STORE_KEEPER.getValue(), "english"), REQUISITION_SUPPLIED_BY_STORE_KEEPER.getValue()));
        optionDTOList = nameDTOList.stream()
                .map(dto -> new OptionDTO(dto.englishName, dto.banglaName, String.valueOf(dto.value)))
                .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, null);
    }

    public static String buildOptionForAsset(String language) {
        List<OptionDTO> optionDTOList;
        CommonApprovalStatus[] allCommonApprovalStatus = CommonApprovalStatus.class.getEnumConstants();
        List<CommonApprovalStatus> statusesForAsset = Arrays.asList(AVAILABLE, NOT_AVAILABLE, USED);

        optionDTOList = Arrays.stream(allCommonApprovalStatus)
                .filter(statusesForAsset::contains)
                .map(e -> new OptionDTO(getText(e.getValue(), "english"),
                        getText(e.getValue(), "bangla"), String.valueOf(e.value)))
                .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, null);
    }

    private static class Option {
        String banglaName;
        String englishName;
        int value;

        public Option(String banglaName, String englishName, int value) {
            this.banglaName = banglaName;
            this.englishName = englishName;
            this.value = value;
        }
    }
}
