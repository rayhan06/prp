package vm_requisition;
import java.util.*; 
import util.*; 


public class Vm_requisitionDTO extends CommonDTO
{

	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	public long requesterOrgId = -1;
	public long decisionByOrgId = -1;
	public long driverOrgId = -1;
	public long startDate = System.currentTimeMillis();
	public long endDate = System.currentTimeMillis();
    public String startAddress = "";
    public String endAddress = "";
	public int vehicleRequisitionPurposeCat = -1;
	public int vehicleTypeCat = -1;
    public String tripDescription = "";
	public int status = -1;
	public long decisionOn = -1;
    public String decisionDescription = "";
	public int givenVehicleType = -1;
	public long givenVehicleId = -1;
	public long receiveDate = System.currentTimeMillis();
	public double totalTripDistance = -1;
	public double meterReadingInitial = -1;
	public double meterReadingEnd = -1;
	public double totalTripTime = -1;
	public int petrolGiven = -1;
    public double petrolPreviousStock = -1;
	public double petrolAmount = -1;
	public double petrolConsumption = -1;
	public double petrolStock = -1;
	public int paymentGiven = -1;
	public int paymentType = -1;
    public String startTime = "";
    public String endTime = "";
    public String receiveTime = "";
	public long requesterOfficeId = -1;
	public long decisionByOfficeId = -1;
	public long driverOfficeId = -1;
	public long requesterOfficeUnitId = -1;
	public long decisionByOfficeUnitId = -1;
	public long driverOfficeUnitId = -1;
	public long requesterEmpId = -1;
	public long decisionByEmpId = -1;
	public long driverEmpId = -1;
    public String requesterPhoneNum = "";
    public String decisionByPhoneNum = "";
    public String driverPhoneNum = "";
    public String requesterNameEn = "";
    public String decisionByNameEn = "";
    public String driverNameEn = "";
    public String requesterNameBn = "";
    public String decisionByNameBn = "";
    public String driverNameBn = "";
    public String requesterOfficeNameEn = "";
    public String decisionByOfficeNameEn = "";
    public String driverOfficeNameEn = "";
    public String requesterOfficeNameBn = "";
    public String decisionByOfficeNameBn = "";
    public String driverOfficeNameBn = "";
    public String requesterOfficeUnitNameEn = "";
    public String decisionByOfficeUnitNameEn = "";
    public String driverOfficeUnitNameEn = "";
    public String requesterOfficeUnitNameBn = "";
    public String decisionByOfficeUnitNameBn = "";
    public String driverOfficeUnitNameBn = "";
    public String requesterOfficeUnitOrgNameEn = "";
    public String decisionByOfficeUnitOrgNameEn = "";
    public String driverOfficeUnitOrgNameEn = "";
    public String requesterOfficeUnitOrgNameBn = "";
    public String decisionByOfficeUnitOrgNameBn = "";
    public String driverOfficeUnitOrgNameBn = "";

    public long firstApproverOrgId = -1;
    public long firstApproverOfficeId = -1;
    public long firstApproverOfficeUnitId = -1;
    public long firstApproverEmpId = -1;
    public String firstApproverPhoneNum = "";
    public String firstApproverNameEn = "";
    public String firstApproverNameBn = "";
    public String firstApproverOfficeNameEn = "";
    public String firstApproverOfficeNameBn = "";
    public String firstApproverOfficeUnitNameEn = "";
    public String firstApproverOfficeUnitNameBn = "";
    public String firstApproverOfficeUnitOrgNameEn = "";
    public String firstApproverOfficeUnitOrgNameBn = "";

    public double paidAmount = -1;
    public int requisitionCount = 0;

    public int vehicleRequisitionLocationCat = -1;

    public long filesDropzone = -1;

    public String cancellationReason="";

    public long  cancellationDate = 0;

    public long  firstApproverApproveOrRejectionDate = 0;
    public long  secondApproverApproveOrRejectionDate = 0;

    public int firstApproverStatus = -1;
    public int secondApproverStatus = -1;


    @Override
	public String toString() {
            return "$Vm_requisitionDTO[" +
            " iD = " + iD +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            " requesterOrgId = " + requesterOrgId +
            " decisionByOrgId = " + decisionByOrgId +
            " driverOrgId = " + driverOrgId +
            " startDate = " + startDate +
            " endDate = " + endDate +
            " startAddress = " + startAddress +
            " endAddress = " + endAddress +
            " vehicleRequisitionPurposeCat = " + vehicleRequisitionPurposeCat +
            " vehicleTypeCat = " + vehicleTypeCat +
            " tripDescription = " + tripDescription +
            " status = " + status +
            " decisionOn = " + decisionOn +
            " decisionDescription = " + decisionDescription +
            " givenVehicleType = " + givenVehicleType +
            " givenVehicleId = " + givenVehicleId +
            " receiveDate = " + receiveDate +
            " totalTripDistance = " + totalTripDistance +
            " totalTripTime = " + totalTripTime +
            " petrolPreviousStock = " + petrolPreviousStock +
            " petrolAmount = " + petrolAmount +
            " petrolConsumption = " + petrolConsumption +
            " petrolStock = " + petrolStock +
            " petrolGiven = " + petrolGiven +
            " paymentGiven = " + paymentGiven +
            " paymentType = " + paymentType +
            " startTime = " + startTime +
            " endTime = " + endTime +
            " receiveTime = " + receiveTime +
            " requesterOfficeId = " + requesterOfficeId +
            " decisionByOfficeId = " + decisionByOfficeId +
            " driverOfficeId = " + driverOfficeId +
            " requesterOfficeUnitId = " + requesterOfficeUnitId +
            " decisionByOfficeUnitId = " + decisionByOfficeUnitId +
            " driverOfficeUnitId = " + driverOfficeUnitId +
            " requesterEmpId = " + requesterEmpId +
            " decisionByEmpId = " + decisionByEmpId +
            " driverEmpId = " + driverEmpId +
            " requesterPhoneNum = " + requesterPhoneNum +
            " decisionByPhoneNum = " + decisionByPhoneNum +
            " driverPhoneNum = " + driverPhoneNum +
            " requesterNameEn = " + requesterNameEn +
            " decisionByNameEn = " + decisionByNameEn +
            " driverNameEn = " + driverNameEn +
            " requesterNameBn = " + requesterNameBn +
            " decisionByNameBn = " + decisionByNameBn +
            " driverNameBn = " + driverNameBn +
            " requesterOfficeNameEn = " + requesterOfficeNameEn +
            " decisionByOfficeNameEn = " + decisionByOfficeNameEn +
            " driverOfficeNameEn = " + driverOfficeNameEn +
            " requesterOfficeNameBn = " + requesterOfficeNameBn +
            " decisionByOfficeNameBn = " + decisionByOfficeNameBn +
            " driverOfficeNameBn = " + driverOfficeNameBn +
            " requesterOfficeUnitNameEn = " + requesterOfficeUnitNameEn +
            " decisionByOfficeUnitNameEn = " + decisionByOfficeUnitNameEn +
            " driverOfficeUnitNameEn = " + driverOfficeUnitNameEn +
            " requesterOfficeUnitNameBn = " + requesterOfficeUnitNameBn +
            " decisionByOfficeUnitNameBn = " + decisionByOfficeUnitNameBn +
            " driverOfficeUnitNameBn = " + driverOfficeUnitNameBn +
            " requesterOfficeUnitOrgNameEn = " + requesterOfficeUnitOrgNameEn +
            " decisionByOfficeUnitOrgNameEn = " + decisionByOfficeUnitOrgNameEn +
            " driverOfficeUnitOrgNameEn = " + driverOfficeUnitOrgNameEn +
            " requesterOfficeUnitOrgNameBn = " + requesterOfficeUnitOrgNameBn +
            " decisionByOfficeUnitOrgNameBn = " + decisionByOfficeUnitOrgNameBn +
            " driverOfficeUnitOrgNameBn = " + driverOfficeUnitOrgNameBn +
            " paidAmount = " + paidAmount +
            "]";
    }

}