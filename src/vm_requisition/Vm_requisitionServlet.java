package vm_requisition;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import common.ApiResponse;
import employee_assign.EmployeeSearchModalUtil;
import employee_assign.EmployeeSearchModel;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import files.FilesDAO;
import mail.EmailService;
import mail.SendEmailDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsRepository;
import offices.OfficesDTO;
import offices.OfficesRepository;
import org.apache.log4j.Logger;

import login.LoginDTO;
import org.eclipse.jetty.util.ajax.JSON;
import permission.MenuConstants;
import role.PermissionRepository;


import role.RoleDTO;
import sessionmanager.SessionConstants;

import sms.SmsService;
import test_lib.EmployeeRecordDTO;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import java.util.stream.Collectors;
import javax.servlet.http.*;


import com.google.gson.Gson;

import pb.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import vm_employee_assigned_vehicle.Vm_employee_assigned_vehicleDTO;
import vm_employee_assigned_vehicle.Vm_employee_assigned_vehicleRepository;
import vm_requisition_approver.Vm_requisition_approverDTO;
import vm_requisition_approver.Vm_requisition_approverRepository;

import static sessionmanager.SessionConstants.*;


/**
 * Servlet implementation class Vm_requisitionServlet
 */
@WebServlet("/Vm_requisitionServlet")
@MultipartConfig
public class Vm_requisitionServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Vm_requisitionServlet.class);

    String tableName = "vm_requisition";

    Vm_requisitionDAO vm_requisitionDAO;
    CommonRequestHandler commonRequestHandler;
    FilesDAO filesDAO = new FilesDAO();
    private final Gson gson = new Gson();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vm_requisitionServlet() {
        super();
        try {
            vm_requisitionDAO = new Vm_requisitionDAO(tableName);
            commonRequestHandler = new CommonRequestHandler(vm_requisitionDAO);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
        try {
            String actionType = request.getParameter("actionType");

            if (!actionType.equals("search") && !actionType.equals("add") && !actionType.equals("getPrePendingPage")) {
                if (request.getParameter("ID") != null) {
                    RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
                    boolean isAdmin = role.ID == SessionConstants.VEHICLE_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;

                    String filterOwn = " (requester_org_id = " + userDTO.organogramID + " OR inserted_by_organogram_id = " + userDTO.organogramID;
                    filterOwn += isAdmin ? " OR true)" : ")";
                    filterOwn += " AND ";
                    filterOwn += " ID = " + Long.parseLong(request.getParameter("ID")) + " ";
                    int pendingRequest = vm_requisitionDAO.getCount(null, 1, 0, true, userDTO, filterOwn, false);
                    if (pendingRequest != 1) {
                        throw new card_info.InvalidDataException("This employee has no request for id : " + request.getParameter("ID"));
                    }
                }
            } else if (actionType.equals("getPrePendingPage")) {
                if (request.getParameter("ID") != null) {
                    String filterOwn = "  status = " + CommonApprovalStatus.PRE_PENDING.getValue();
                    //filterOwn += " OR first_approver_org_id = " + userDTO.organogramID + " ) ";
                    filterOwn += " AND ";
                    filterOwn += " ID = " + Long.parseLong(request.getParameter("ID")) + " ";
                    Vm_requisitionDTO pendingRequest = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(Long.parseLong(request.getParameter("ID")));
                    if (pendingRequest == null) {
                        logger.debug("This employee has no request for id : " + request.getParameter("ID"));
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    } else {
                        if (pendingRequest.insertedByOrganogramId != userDTO.organogramID) {
                            OfficeUnitOrganograms superior = OfficeUnitOrganogramsRepository.getInstance().
                                    getImmediateSuperiorToOrganogramIdWithActiveEmpStatusAndConsiderSubstitute(pendingRequest.requesterOrgId);
                            if (superior == null) {

                                logger.debug("This employee has no line manager");
                                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                            }
                            if (superior.id != userDTO.organogramID) {
//							if (superior.superior_designation_id != userDTO.organogramID) {

                                logger.debug("This employee has no pre pending request for id : " + request.getParameter("ID"));
                                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                            }
                        }
                    }
                }
            }

            if (actionType.equals("getAddPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_ADD)) {
                    commonRequestHandler.getAddPage(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getEditPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_ADD)) {
                    getVm_requisition(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getPrePendingPage")) {
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_UPDATE))
//				{
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
                getPrePendingVm_requisition(request, response);

            } else if (actionType.equals("cancel")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_ADD)) {
                    cancel(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("decidePrepending")) {
                decidePrePending(request, response, false, userDTO, true);

            } else if (actionType.equals("getURL")) {
                String URL = request.getParameter("URL");
                System.out.println("URL = " + URL);
                response.sendRedirect(URL);
            } else if (actionType.equals("search")) {
                System.out.println("search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_SEARCH)) {
                    if (isPermanentTable) {
                        String filter = request.getParameter("filter");
                        System.out.println("filter = " + filter);
                        if (filter != null) {
                            filter = ""; //shouldn't be directly used, rather manipulate it.
                            searchVm_requisition(request, response, isPermanentTable, filter);
                        } else {
                            searchVm_requisition(request, response, isPermanentTable, "");
                        }
                    } else {
                        //searchVm_requisition(request, response, tempTableName, isPermanentTable);
                    }
                }
            } else if (actionType.equals("view")) {
                System.out.println("view requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_SEARCH)) {
                    commonRequestHandler.view(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            }

            /*Vehicle receive start*/
            else if (actionType.equals("vehicle_receive_getAddPage")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_RECEIVE_ADD)) {
                    if (isPermanentTable) {
                        String filter = request.getParameter("filter");
                        System.out.println("filter = " + filter);
                        if (filter != null) {
                            filter = "";
                            AddVm_receive(request, response, isPermanentTable, filter);
                        } else {
                            AddVm_receive(request, response, isPermanentTable, "");
                        }
                    } else {
                        //searchVm_requisition(request, response, tempTableName, isPermanentTable);
                    }
                }
            } else if (actionType.equals("vehicle_receive_search")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_RECEIVE_SEARCH)) {
                    if (isPermanentTable) {
                        String filter = request.getParameter("filter");
                        System.out.println("filter = " + filter);
                        if (filter != null) {
                            filter = "";
                            SearchVm_receive(request, response, isPermanentTable, filter);
                        } else {
                            SearchVm_receive(request, response, isPermanentTable, "");
                        }
                    } else {
                        //searchVm_requisition(request, response, tempTableName, isPermanentTable);
                    }
                }
            } else if (actionType.equals("vehicle_receive_payment")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_RECEIVE_PAYMENT)) {
                    if (isPermanentTable) {
                        String filter = request.getParameter("filter");
                        System.out.println("filter = " + filter);
                        if (filter != null) {
                            filter = "";
                            PaymentVm_receive(request, response, isPermanentTable, filter);
                        } else {
                            PaymentVm_receive(request, response, isPermanentTable, "");
                        }
                    } else {
                        //searchVm_requisition(request, response, tempTableName, isPermanentTable);
                    }
                }
            } else if (actionType.equals("vehicleReceiveViewFormPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_RECEIVE_ADD)) {
                    vehicleReceiveViewForm(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("vehicleReceiveSearchViewFormPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_RECEIVE_SEARCH)) {
                    vehicleReceiveSearchViewForm(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("vehicleReceiveSearchViewFormPageForSameCarSameUser")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_RECEIVE_SEARCH)) {
                    vehicleReceiveSearchViewFormSameCarSameEmp(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("vehicleReceiveSearchViewFormPageForSameCar")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_RECEIVE_SEARCH)) {
                    vehicleReceiveSearchViewFormSameCar(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("vehicleReceiveEditFormPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_RECEIVE_ADD)) {
                    vehicleReceiveEditForm(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("receiveFormMoneyReceipt")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_RECEIVE_ADD)) {

                    receiveFormMoneyReceipt(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("vehicle_receive_getSearchPage")) {
                System.out.println("vehicle_receive_getSearchPage requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_SEARCH)) {
                    if (isPermanentTable) {
                        String filter = request.getParameter("filter");
                        System.out.println("filter = " + filter);
                        if (filter != null) {
                            filter = ""; //shouldn't be directly used, rather manipulate it.
                            vehicle_receive_getSearchPage(request, response, isPermanentTable, filter);
                        } else {
                            vehicle_receive_getSearchPage(request, response, isPermanentTable, "");
                        }
                    } else {
                        //searchVm_requisition(request, response, tempTableName, isPermanentTable);
                    }
                }
            } else if (actionType.equals("vehicle_already_received_getSearchPage")) {
                System.out.println("vehicle_already_received_getSearchPage requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_SEARCH)) {
                    if (isPermanentTable) {
                        String filter = request.getParameter("filter");
                        System.out.println("filter = " + filter);
                        if (filter != null) {
                            filter = ""; //shouldn't be directly used, rather manipulate it.
                            vehicle_already_received_getSearchPage(request, response, isPermanentTable, filter);
                        } else {
                            vehicle_already_received_getSearchPage(request, response, isPermanentTable, "");
                        }
                    } else {
                        //searchVm_requisition(request, response, tempTableName, isPermanentTable);
                    }
                }
            }



            /*Vehicle receive end*/

            else {
                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        System.out.println("doPost");
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);

            if (!actionType.equals("search") && !actionType.equals("add")) {
                if (request.getParameter("iD") != null) {
                    RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
                    boolean isAdmin = role.ID == SessionConstants.VEHICLE_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;

                    String filterOwn = " (requester_org_id = " + userDTO.organogramID + " OR inserted_by_organogram_id = " + userDTO.organogramID;
                    filterOwn += isAdmin ? " OR true)" : ")";
                    filterOwn += " AND ";
                    filterOwn += " ID = " + Long.parseLong(request.getParameter("iD")) + " ";
                    int pendingRequest = vm_requisitionDAO.getCount(null, 1, 0, true, userDTO, filterOwn, false);
                    if (pendingRequest != 1) {
                        throw new card_info.InvalidDataException("This employee has no request for id : " + request.getParameter("iD"));
                    }
                }
            } else if (actionType.equals("edit")) {
                if (request.getParameter("iD") != null) {
                    String filterOwn = " status = " + CommonApprovalStatus.PRE_PENDING.getValue();
                    filterOwn += " AND ";
                    filterOwn += " ID = " + Long.parseLong(request.getParameter("iD")) + " ";
                    if (request.getParameter("iD") != null) {
                        Vm_requisitionDTO pendingRequest = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(Long.parseLong(request.getParameter("iD")));
                        if (pendingRequest == null || pendingRequest.status != CommonApprovalStatus.PRE_PENDING.getValue()) {
                            throw new card_info.InvalidDataException("This employee has no request for id : " + request.getParameter("iD"));
                        } else {
                            if (pendingRequest.insertedByOrganogramId != userDTO.organogramID) {
                                OfficeUnitOrganograms superior = OfficeUnitOrganogramsRepository.getInstance()
                                        .getImmediateSuperiorToOrganogramIdWithActiveEmpStatusAndConsiderSubstitute(pendingRequest.requesterOrgId);
                                if (superior == null) {
                                    throw new card_info.InvalidDataException("This employee has no line manager");
                                }
                                if (superior.id != userDTO.organogramID) {
                                    throw new card_info.InvalidDataException("This employee has no pre pending request for id : " + request.getParameter("iD"));
                                }
                            }
                        }
                    }
                }
            }

            if (actionType.equals("add")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_ADD)) {
                    System.out.println("going to  addVm_requisition ");
                    addVm_requisition(request, response, true, userDTO, true);
                } else {
                    System.out.println("Not going to  addVm_requisition ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("getDTO")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_ADD)) {
                    getDTO(request, response);
                } else {
                    System.out.println("Not going to  addVm_requisition ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("edit")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_UPDATE)) {
                    addVm_requisition(request, response, false, userDTO, isPermanentTable);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("delete")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_UPDATE)) {
                    commonRequestHandler.delete(request, response, userDTO);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("search")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_SEARCH)) {
                    searchVm_requisition(request, response, true, "");
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getGeo")) {
                System.out.println("going to geoloc ");
                request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
            }

            /*vehicle receive start*/

            else if (actionType.equals("receiveVehicle")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_ADD)) {
                    System.out.println("going to  receiveVehicle add ");
                    receiveVehicle(request, response, false, userDTO, true);

                } else {
                    System.out.println("Not going to  receiveVehicle ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("paymentStatus")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_ADD)) {
                    System.out.println("going to  paymentStatus");
                    paymentStatus(request, response, false, userDTO, true);

                } else {
                    System.out.println("Not going to  paymentStatus ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("paymentStatusFromSearch")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_ADD)) {
                    System.out.println("going to  paymentStatusFromSearch");
                    paymentStatusFromSearch(request, response, false, userDTO, true);

                } else {
                    System.out.println("Not going to  paymentStatusFromSearch ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            }

            /*vehicle receive end*/

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getDTO(HttpServletRequest request, HttpServletResponse response) {
        try {
            System.out.println("In getDTO");
            Vm_requisitionDTO vm_requisitionDTO = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(Long.parseLong(request.getParameter("ID")));
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            String encoded = this.gson.toJson(vm_requisitionDTO);
            System.out.println("json encoded data = " + encoded);
            out.print(encoded);
            out.flush();
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void addVm_requisition(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addVm_requisition");
            String path = getServletContext().getRealPath("/img2/");
            Vm_requisitionDTO vm_requisitionDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

            boolean valid = true;

            if (addFlag == true) {
                vm_requisitionDTO = new Vm_requisitionDTO();
            } else {
                vm_requisitionDTO = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(Long.parseLong(request.getParameter("iD")));
            }

            String Value = "";

            if (addFlag) {
                vm_requisitionDTO.insertedByUserId = userDTO.ID;
            }


            if (addFlag) {
                vm_requisitionDTO.insertedByOrganogramId = userDTO.organogramID;
            }


            if (addFlag) {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);

                vm_requisitionDTO.insertionDate = c.getTimeInMillis();
            }


            Value = request.getParameter("searchColumn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("searchColumn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.searchColumn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterOrgId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterOrgId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.requesterOrgId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("decisionByOrgId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("decisionByOrgId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.decisionByOrgId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("driverOrgId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("driverOrgId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.driverOrgId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("startDate");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("startDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    Date d = f.parse(Value);
                    vm_requisitionDTO.startDate = d.getTime();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("endDate");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("endDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    Date d = f.parse(Value);
                    vm_requisitionDTO.endDate = d.getTime();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("vehicleRequisitionLocationCat");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("vehicleRequisitionLocationCat = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("") && !Value.equals("-1")) {
                vm_requisitionDTO.vehicleRequisitionLocationCat = Integer.parseInt(Value);
            } else {
                valid = false;
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            String[] Values = request.getParameterValues("startAddresses");

//			if(Values != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
            System.out.println("startAddress = " + Values);
            if (Values != null && Values.length > 0) {
                StringBuilder address = new StringBuilder();
                for (int i = 0; i < Values.length; i++) {
                    if (Values[i].trim().isEmpty()) continue;
                    valid &= !(Values[i].contains("$"));
                    address.append("$" + Values[i]);
                }
                if (!address.toString().trim().isEmpty()) vm_requisitionDTO.startAddress = address.substring(1);
                else valid = false;
            } else {
                valid = false;
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Values = request.getParameterValues("endAddresses");

//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
            System.out.println("endAddress = " + Value);
            if (Values != null && Values.length > 0) {
                StringBuilder address = new StringBuilder();
                for (int i = 0; i < Values.length; i++) {
                    if (Values[i].trim().isEmpty()) continue;
                    valid &= !(Values[i].contains("$"));
                    address.append("$" + Values[i]);
                }
                if (!address.toString().trim().isEmpty()) vm_requisitionDTO.endAddress = address.substring(1);
                else valid = false;
            } else {
                valid = false;
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("vehicleRequisitionPurposeCat");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("vehicleRequisitionPurposeCat = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("") && !Value.equals("-1")) {
                vm_requisitionDTO.vehicleRequisitionPurposeCat = Integer.parseInt(Value);
            } else {
                valid = false;
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("vehicleTypeCat");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("vehicleTypeCat = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("") && !Value.equalsIgnoreCase("-1")) {
                vm_requisitionDTO.vehicleTypeCat = Integer.parseInt(Value);
            } else {
                valid = false;
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("tripDescription");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("tripDescription = " + Value);
            if (Value != null) {
                vm_requisitionDTO.tripDescription = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("status");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("status = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
//				vm_requisitionDTO.status = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            if (vm_requisitionDTO.status == -1) vm_requisitionDTO.status = CommonApprovalStatus.PRE_PENDING.getValue();

            Value = request.getParameter("decisionOn");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("decisionOn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.decisionOn = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("decisionDescription");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("decisionDescription = " + Value);
            if (Value != null) {
                vm_requisitionDTO.decisionDescription = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("givenVehicleType");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("givenVehicleType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.givenVehicleType = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("givenVehicleId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("givenVehicleId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.givenVehicleId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("receiveDate");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("receiveDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    Date d = f.parse(Value);
                    vm_requisitionDTO.receiveDate = d.getTime();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("totalTripDistance");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("totalTripDistance = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.totalTripDistance = Double.parseDouble(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("totalTripTime");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("totalTripTime = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.totalTripTime = Double.parseDouble(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("petrolGiven");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("petrolGiven = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.petrolGiven = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("petrolAmount");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("petrolAmount = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.petrolAmount = Double.parseDouble(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("paymentGiven");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paymentGiven = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.paymentGiven = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("paymentType");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paymentType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.paymentType = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("startTime");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("startTime = " + Value);
            if (Value != null) {
                vm_requisitionDTO.startTime = (Value);
            } else {
                valid = false;
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("endTime");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("endTime = " + Value);
            if (Value != null) {
                vm_requisitionDTO.endTime = (Value);
            } else {
                valid = false;
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("receiveTime");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("receiveTime = " + Value);
            if (Value != null) {
                vm_requisitionDTO.receiveTime = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterOfficeId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterOfficeId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.requesterOfficeId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("decisionByOfficeId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("decisionByOfficeId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.decisionByOfficeId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("driverOfficeId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("driverOfficeId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.driverOfficeId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterOfficeUnitId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterOfficeUnitId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.requesterOfficeUnitId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("decisionByOfficeUnitId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("decisionByOfficeUnitId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.decisionByOfficeUnitId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("driverOfficeUnitId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("driverOfficeUnitId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.driverOfficeUnitId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("decisionByEmpId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("decisionByEmpId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.decisionByEmpId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("driverEmpId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("driverEmpId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.driverEmpId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterPhoneNum");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterPhoneNum = " + Value);
            if (Value != null) {
                vm_requisitionDTO.requesterPhoneNum = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("decisionByPhoneNum");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("decisionByPhoneNum = " + Value);
            if (Value != null) {
                vm_requisitionDTO.decisionByPhoneNum = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("driverPhoneNum");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("driverPhoneNum = " + Value);
            if (Value != null) {
                vm_requisitionDTO.driverPhoneNum = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterNameEn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.requesterNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("decisionByNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("decisionByNameEn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.decisionByNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("driverNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("driverNameEn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.driverNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterNameBn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.requesterNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("decisionByNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("decisionByNameBn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.decisionByNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("driverNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("driverNameBn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.driverNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterOfficeNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterOfficeNameEn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.requesterOfficeNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("decisionByOfficeNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("decisionByOfficeNameEn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.decisionByOfficeNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("driverOfficeNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("driverOfficeNameEn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.driverOfficeNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterOfficeNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterOfficeNameBn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.requesterOfficeNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("decisionByOfficeNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("decisionByOfficeNameBn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.decisionByOfficeNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("driverOfficeNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("driverOfficeNameBn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.driverOfficeNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterOfficeUnitNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterOfficeUnitNameEn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.requesterOfficeUnitNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("decisionByOfficeUnitNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("decisionByOfficeUnitNameEn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.decisionByOfficeUnitNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("driverOfficeUnitNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("driverOfficeUnitNameEn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.driverOfficeUnitNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterOfficeUnitNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterOfficeUnitNameBn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.requesterOfficeUnitNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("decisionByOfficeUnitNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("decisionByOfficeUnitNameBn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.decisionByOfficeUnitNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("driverOfficeUnitNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("driverOfficeUnitNameBn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.driverOfficeUnitNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterOfficeUnitOrgNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterOfficeUnitOrgNameEn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.requesterOfficeUnitOrgNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("decisionByOfficeUnitOrgNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("decisionByOfficeUnitOrgNameEn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.decisionByOfficeUnitOrgNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("driverOfficeUnitOrgNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("driverOfficeUnitOrgNameEn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.driverOfficeUnitOrgNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterOfficeUnitOrgNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterOfficeUnitOrgNameBn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.requesterOfficeUnitOrgNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("decisionByOfficeUnitOrgNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("decisionByOfficeUnitOrgNameBn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.decisionByOfficeUnitOrgNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("driverOfficeUnitOrgNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("driverOfficeUnitOrgNameBn = " + Value);
            if (Value != null) {
                vm_requisitionDTO.driverOfficeUnitOrgNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("filesDropzone");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("filesDropzone = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                System.out.println("filesDropzone = " + Value);

                vm_requisitionDTO.filesDropzone = Long.parseLong(Value);


                if (addFlag == false) {
                    String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                    String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                    for (int i = 0; i < deleteArray.length; i++) {
                        System.out.println("going to delete " + deleteArray[i]);
                        if (i > 0) {
                            filesDAO.delete(Long.parseLong(deleteArray[i]));
                        }
                    }
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterEmpId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterEmpId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                EmployeeSearchModel[] models = gson.fromJson(Value, EmployeeSearchModel[].class);

                for (EmployeeSearchModel model : models) {

                    vm_requisitionDTO.requesterEmpId = model.employeeRecordId;
                    vm_requisitionDTO.requesterOfficeUnitId = model.officeUnitId;
                    vm_requisitionDTO.requesterOrgId = model.organogramId;

                    vm_requisitionDTO.requesterNameEn = model.employeeNameEn;
                    vm_requisitionDTO.requesterNameBn = model.employeeNameBn;
                    vm_requisitionDTO.requesterOfficeUnitNameEn = model.officeUnitNameEn;
                    vm_requisitionDTO.requesterOfficeUnitNameBn = model.officeUnitNameBn;
                    vm_requisitionDTO.requesterOfficeUnitOrgNameEn = model.organogramNameEn;
                    vm_requisitionDTO.requesterOfficeUnitOrgNameBn = model.organogramNameBn;

                    vm_requisitionDTO.requesterPhoneNum = model.phoneNumber;

                    OfficesDTO requesterOffice = OfficesRepository.getInstance().getOfficesDTOByID(vm_requisitionDTO.requesterOfficeId);

                    if (requesterOffice != null) {
                        vm_requisitionDTO.requesterOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(model.officeUnitId).officeId;

                        vm_requisitionDTO.requesterOfficeNameEn = requesterOffice.officeNameEng;
                        vm_requisitionDTO.requesterOfficeNameBn = requesterOffice.officeNameBng;
                    }

                }
            } else {
                valid = false;
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            if (valid) {
                if (vm_requisitionDTO.startDate > vm_requisitionDTO.endDate) valid = false;
                else if (vm_requisitionDTO.startDate == vm_requisitionDTO.endDate) {
                    String[] startTimes = vm_requisitionDTO.startTime.split(" ");
                    String[] endTimes = vm_requisitionDTO.endTime.split(" ");

                    String formattedStartTime = startTimes[1] + startTimes[0];
                    String formattedEndTime = endTimes[1] + endTimes[0];

                    valid = formattedStartTime.compareTo(formattedEndTime) < 0;
                }
            }

            System.out.println("Done adding  addVm_requisition dto = " + vm_requisitionDTO);
            long[] returnedID = {-1};

            if (valid) {

                List<Vm_employee_assigned_vehicleDTO> vm_employee_assigned_vehicleDTOS = Vm_employee_assigned_vehicleRepository.getInstance()
                        .getVmEmployeeAssignedVehicleByEmployeeRecordsId(vm_requisitionDTO.requesterEmpId);

                Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(vm_requisitionDTO.requesterEmpId);

                System.out.println("employeeRecordsDTO.officerTypeCat : " + employeeRecordsDTO.employeeClass);

                if ((vm_employee_assigned_vehicleDTOS != null && !vm_employee_assigned_vehicleDTOS.isEmpty()) ||
                        (employeeRecordsDTO != null && employeeRecordsDTO.employeeClass != 1)) {
                    PrintWriter out = response.getWriter();
                    out.println(new Gson().toJson("Employee already has vehicle"));
                    out.close();
                } else {
                    String filterExisting = " requester_org_id = " + vm_requisitionDTO.requesterOrgId;
                    filterExisting += " AND iD != " + vm_requisitionDTO.iD;
                    filterExisting += " AND (start_date < " + vm_requisitionDTO.endDate;
                    filterExisting += " OR (start_date = " + vm_requisitionDTO.endDate;
                    filterExisting += " AND (STR_TO_DATE( start_time , '%l:%i %p' )) <= " + "(STR_TO_DATE( '" + vm_requisitionDTO.endTime + "' , '%l:%i %p' ))))";
                    filterExisting += " AND (end_date > " + vm_requisitionDTO.startDate;
                    filterExisting += " OR (end_date = " + vm_requisitionDTO.startDate;
                    filterExisting += " AND (STR_TO_DATE( end_time , '%l:%i %p' )) >= " + "(STR_TO_DATE( '" + vm_requisitionDTO.startTime + "' , '%l:%i %p' ))))" + " ";

                    List<Vm_requisitionDTO> existing = vm_requisitionDAO.getDTOs(null, 1, 0, true, userDTO, filterExisting, false);
                    if (!existing.isEmpty()) {

                        PrintWriter out = response.getWriter();
                        out.println(new Gson().toJson("Requisition exists"));
                        out.close();
                    } else {

                        OfficeUnitOrganograms lineManager = OfficeUnitOrganogramsRepository.getInstance().
                                getImmediateSuperiorToOrganogramIdWithActiveEmpStatusAndConsiderSubstitute(vm_requisitionDTO.requesterOrgId);

                        if (lineManager == null) {
                            PrintWriter out = response.getWriter();
                            out.println(new Gson().toJson("No line manager"));
                            out.close();
                        } else {
                            Utils.handleTransactionForNavigationService4(() -> {
                                if (isPermanentTable == false) //add new row for validation and make the old row outdated
                                {
                                    vm_requisitionDAO.setIsDeleted(vm_requisitionDTO.iD, CommonDTO.OUTDATED);
                                    returnedID[0] = vm_requisitionDAO.add(vm_requisitionDTO);
                                    vm_requisitionDAO.setIsDeleted(returnedID[0], CommonDTO.WAITING_FOR_APPROVAL);
                                } else if (addFlag == true) {
                                    returnedID[0] = vm_requisitionDAO.manageWriteOperations(vm_requisitionDTO, SessionConstants.INSERT, -1, userDTO);
                                } else {
                                    returnedID[0] = vm_requisitionDAO.manageWriteOperations(vm_requisitionDTO, SessionConstants.UPDATE, -1, userDTO);
                                }

                                VmRequisitionApprovalNotification.getInstance().sendPrePendingNotification(Arrays.asList(lineManager.id), vm_requisitionDTO);
                            });

                            PrintWriter out = response.getWriter();
                            out.println(new Gson().toJson("Success"));
                            out.close();
                        }

                    }
                }


            } else {
                PrintWriter out = response.getWriter();
                out.println(new Gson().toJson("Invalid Input"));
                out.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void decidePrePending(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addVm_requisition");
            String path = getServletContext().getRealPath("/img2/");
            Vm_requisitionDTO vm_requisitionDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

            vm_requisitionDTO = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(Long.parseLong(request.getParameter("iD")));

            String Value = "";

            if (addFlag) {
                vm_requisitionDTO.insertedByUserId = userDTO.ID;
            }


            if (addFlag) {
                vm_requisitionDTO.insertedByOrganogramId = userDTO.organogramID;
            }


            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            if (addFlag) {
                vm_requisitionDTO.insertionDate = c.getTimeInMillis();
            }

            Gson gson = new Gson();
            EmployeeSearchModel modelDecisionBy =
                    gson.fromJson(
                            EmployeeSearchModalUtil.getEmployeeSearchModelJson(userDTO.employee_record_id, userDTO.unitID, userDTO.organogramID),
                            EmployeeSearchModel.class);

            vm_requisitionDTO.decisionByEmpId = modelDecisionBy.employeeRecordId;
            vm_requisitionDTO.decisionByOfficeUnitId = modelDecisionBy.officeUnitId;
            vm_requisitionDTO.decisionByOrgId = modelDecisionBy.organogramId;

            vm_requisitionDTO.decisionByNameEn = modelDecisionBy.employeeNameEn;
            vm_requisitionDTO.decisionByNameBn = modelDecisionBy.employeeNameBn;
            vm_requisitionDTO.decisionByOfficeUnitNameEn = modelDecisionBy.officeUnitNameEn;
            vm_requisitionDTO.decisionByOfficeUnitNameBn = modelDecisionBy.officeUnitNameBn;
            vm_requisitionDTO.decisionByOfficeUnitOrgNameEn = modelDecisionBy.organogramNameEn;
            vm_requisitionDTO.decisionByOfficeUnitOrgNameBn = modelDecisionBy.organogramNameBn;

            vm_requisitionDTO.decisionByPhoneNum = modelDecisionBy.phoneNumber;

            OfficesDTO decisionByOffice = OfficesRepository.getInstance().getOfficesDTOByID(vm_requisitionDTO.decisionByOfficeId);

            if (decisionByOffice != null) {
                vm_requisitionDTO.decisionByOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(modelDecisionBy.officeUnitId).officeId;

                vm_requisitionDTO.decisionByOfficeNameEn = decisionByOffice.officeNameEng;
                vm_requisitionDTO.decisionByOfficeNameBn = decisionByOffice.officeNameBng;
            }

            /*1st approver start*/
            vm_requisitionDTO.firstApproverEmpId = modelDecisionBy.employeeRecordId;
            vm_requisitionDTO.firstApproverOfficeUnitId = modelDecisionBy.officeUnitId;
            vm_requisitionDTO.firstApproverOrgId = modelDecisionBy.organogramId;

            vm_requisitionDTO.firstApproverNameEn = modelDecisionBy.employeeNameEn;
            vm_requisitionDTO.firstApproverNameBn = modelDecisionBy.employeeNameBn;
            vm_requisitionDTO.firstApproverOfficeUnitNameEn = modelDecisionBy.officeUnitNameEn;
            vm_requisitionDTO.firstApproverOfficeUnitNameBn = modelDecisionBy.officeUnitNameBn;
            vm_requisitionDTO.firstApproverOfficeUnitOrgNameEn = modelDecisionBy.organogramNameEn;
            vm_requisitionDTO.firstApproverOfficeUnitOrgNameBn = modelDecisionBy.organogramNameBn;

            vm_requisitionDTO.firstApproverPhoneNum = modelDecisionBy.phoneNumber;

            vm_requisitionDTO.firstApproverApproveOrRejectionDate = System.currentTimeMillis();

            if (decisionByOffice != null) {
                vm_requisitionDTO.firstApproverOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(modelDecisionBy.officeUnitId).officeId;

                vm_requisitionDTO.firstApproverOfficeNameEn = decisionByOffice.officeNameEng;
                vm_requisitionDTO.firstApproverOfficeNameBn = decisionByOffice.officeNameBng;
            }
            /*1st approver end*/

            Value = request.getParameter("decision");

            vm_requisitionDTO.status = Value.equals("Reject") ? CommonApprovalStatus.DISSATISFIED.getValue() : CommonApprovalStatus.PENDING.getValue();

            vm_requisitionDTO.firstApproverStatus =  vm_requisitionDTO.status==CommonApprovalStatus.PENDING.getValue()?CommonApprovalStatus.SATISFIED.getValue() :
            CommonApprovalStatus.DISSATISFIED.getValue();

            vm_requisitionDTO.decisionOn = c.getTimeInMillis();

            Value = request.getParameter("decisionDescription");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("decisionDescription = " + Value);
            if (Value != null) {
                vm_requisitionDTO.decisionDescription = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("givenVehicleType");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("givenVehicleType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.givenVehicleType = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("givenVehicleId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("givenVehicleId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_requisitionDTO.givenVehicleId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("driverEmpId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("driverEmpId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                EmployeeSearchModel[] models = gson.fromJson(Value, EmployeeSearchModel[].class);

                for (EmployeeSearchModel model : models) {

                    vm_requisitionDTO.driverEmpId = model.employeeRecordId;
                    vm_requisitionDTO.driverOfficeUnitId = model.officeUnitId;
                    vm_requisitionDTO.driverOrgId = model.organogramId;

                    vm_requisitionDTO.driverNameEn = model.employeeNameEn;
                    vm_requisitionDTO.driverNameBn = model.employeeNameBn;
                    vm_requisitionDTO.driverOfficeUnitNameEn = model.officeUnitNameEn;
                    vm_requisitionDTO.driverOfficeUnitNameBn = model.officeUnitNameBn;
                    vm_requisitionDTO.driverOfficeUnitOrgNameEn = model.organogramNameEn;
                    vm_requisitionDTO.driverOfficeUnitOrgNameBn = model.organogramNameBn;

                    vm_requisitionDTO.driverPhoneNum = model.phoneNumber;

                    OfficesDTO driverOffice = OfficesRepository.getInstance().getOfficesDTOByID(vm_requisitionDTO.driverOfficeId);

                    if (driverOffice != null) {
                        vm_requisitionDTO.driverOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(model.officeUnitId).officeId;

                        vm_requisitionDTO.driverOfficeNameEn = driverOffice.officeNameEng;
                        vm_requisitionDTO.driverOfficeNameBn = driverOffice.officeNameBng;
                    }

                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            System.out.println("Done adding  addVm_requisition dto = " + vm_requisitionDTO);
            long returnedID = -1;

            if (isPermanentTable == false) //add new row for validation and make the old row outdated
            {
                vm_requisitionDAO.setIsDeleted(vm_requisitionDTO.iD, CommonDTO.OUTDATED);
                returnedID = vm_requisitionDAO.add(vm_requisitionDTO);
                vm_requisitionDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);

                if (isPermanentTable) {
                    String inPlaceSubmit = request.getParameter("inplacesubmit");

                    if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                        getVm_requisition(request, response, returnedID);
                    } else {
                        response.sendRedirect("Vm_requisition_pendingServlet?actionType=prePendingSearch");
                    }
                } else {
                    commonRequestHandler.validate(Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(returnedID), request, response, userDTO);
                }
            } else if (addFlag == true) {
                returnedID = vm_requisitionDAO.manageWriteOperations(vm_requisitionDTO, SessionConstants.INSERT, -1, userDTO);

                if (isPermanentTable) {
                    String inPlaceSubmit = request.getParameter("inplacesubmit");

                    if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                        getVm_requisition(request, response, returnedID);
                    } else {
                        response.sendRedirect("Vm_requisition_pendingServlet?actionType=prePendingSearch");
                    }
                } else {
                    commonRequestHandler.validate(Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(returnedID), request, response, userDTO);
                }
            } else {
                returnedID = vm_requisitionDAO.manageWriteOperations(vm_requisitionDTO, SessionConstants.UPDATE, -1, userDTO);
            }
            if (returnedID != -1 && vm_requisitionDTO.status == CommonApprovalStatus.PENDING.getValue()) {

                List<Vm_requisition_approverDTO> approver = Vm_requisition_approverRepository.getInstance().getVm_requisition_approverList();

                if (approver != null && approver.size() > 0) {
                    OfficeUnitOrganograms secondApprover = OfficeUnitOrganogramsRepository.getInstance().
                            getById(approver.get(0).approverOrgId);

                    VmRequisitionApprovalNotification.getInstance().sendPendingNotification(Arrays.asList(secondApprover.id), vm_requisitionDTO);
                }


            }

            response.sendRedirect("Vm_requisition_pendingServlet?actionType=prePendingSearch");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void paymentStatusFromSearch(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% paymentStatusFromSearch");
            String path = getServletContext().getRealPath("/img2/");
            Vm_requisitionDTO vm_requisitionDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

            if (addFlag == true) {
                vm_requisitionDTO = new Vm_requisitionDTO();
            } else {
                vm_requisitionDTO = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(Long.parseLong(request.getParameter("iD")));

            }

            String Value = "";

            Value = request.getParameter("paidStatus");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paidStatus = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    vm_requisitionDTO.paymentGiven = Integer.parseInt(Value);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }


            Value = request.getParameter("payment_type");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("payment_type = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    vm_requisitionDTO.paymentType = Integer.parseInt(Value);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }


            long returnedID = -1;

            if (isPermanentTable == false) //add new row for validation and make the old row outdated
            {
                vm_requisitionDAO.setIsDeleted(vm_requisitionDTO.iD, CommonDTO.OUTDATED);
                returnedID = vm_requisitionDAO.add(vm_requisitionDTO);
                vm_requisitionDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
            } else if (addFlag == true) {
                returnedID = vm_requisitionDAO.manageWriteOperations(vm_requisitionDTO, SessionConstants.INSERT, -1, userDTO);
            } else {
                returnedID = vm_requisitionDAO.manageWriteOperations(vm_requisitionDTO, SessionConstants.UPDATE, -1, userDTO);
            }

//			if(isPermanentTable){
//				PrintWriter out = response.getWriter();
//				out.println("Vm_requisitionServlet?actionType=vehicle_receive_payment");
//				out.close();
//			}


            if (isPermanentTable) {
                //response.sendRedirect("Vm_requisitionServlet?actionType=vehicle_receive_payment");
                ApiResponse apiResponse = null;
                apiResponse = ApiResponse.makeSuccessResponse("Vm_requisitionServlet?actionType=vehicle_receive_payment");

                PrintWriter pw = response.getWriter();
                pw.write(apiResponse.getJSONString());
                pw.flush();
                pw.close();
            } else {
                commonRequestHandler.validate(Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(returnedID), request, response, userDTO);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void paymentStatus(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addVm_requisition");
            String path = getServletContext().getRealPath("/img2/");
            Vm_requisitionDTO vm_requisitionDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

            if (addFlag == true) {
                vm_requisitionDTO = new Vm_requisitionDTO();
            } else {
                vm_requisitionDTO = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(Long.parseLong(request.getParameter("iD")));

            }

            String Value = "";

            Value = request.getParameter("paidStatus");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paidStatus = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    vm_requisitionDTO.paymentGiven = Integer.parseInt(Value);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }


            Value = request.getParameter("paidAmount");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paidAmount = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    vm_requisitionDTO.paidAmount = Double.parseDouble(Value);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("payment_type");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("payment_type = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    vm_requisitionDTO.paymentType = Integer.parseInt(Value);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }


            long returnedID = -1;

            if (isPermanentTable == false) //add new row for validation and make the old row outdated
            {
                vm_requisitionDAO.setIsDeleted(vm_requisitionDTO.iD, CommonDTO.OUTDATED);
                returnedID = vm_requisitionDAO.add(vm_requisitionDTO);
                vm_requisitionDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
            } else if (addFlag == true) {
                returnedID = vm_requisitionDAO.manageWriteOperations(vm_requisitionDTO, SessionConstants.INSERT, -1, userDTO);
            } else {
                returnedID = vm_requisitionDAO.manageWriteOperations(vm_requisitionDTO, SessionConstants.UPDATE, -1, userDTO);
            }

            if (isPermanentTable) {
                //response.sendRedirect("Vm_requisitionServlet?actionType=vehicle_receive_search");
                ApiResponse apiResponse = null;
                apiResponse = ApiResponse.makeSuccessResponse("Vm_requisitionServlet?actionType=vehicle_receive_search");

                PrintWriter pw = response.getWriter();
                pw.write(apiResponse.getJSONString());
                pw.flush();
                pw.close();
            } else {
                commonRequestHandler.validate(Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(returnedID), request, response, userDTO);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void receiveVehicle(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addVm_requisition");
            String path = getServletContext().getRealPath("/img2/");
            Vm_requisitionDTO vm_requisitionDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

            if (addFlag == true) {
                vm_requisitionDTO = new Vm_requisitionDTO();
            } else {
                vm_requisitionDTO = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(Long.parseLong(request.getParameter("iD")));

            }

            String Value = "";


            Value = request.getParameter("vehicle_receive_date");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("receiveDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    //Date d = f.parse(Value);
                    //vm_requisitionDTO.receiveDate = d.getTime();
                    vm_requisitionDTO.receiveDate = Long.parseLong(Value);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("inTime");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("inTime = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    vm_requisitionDTO.receiveTime = Value;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }


            Value = request.getParameter("vehicle_travelled_distance");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("vehicle_travelled_distance = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    vm_requisitionDTO.totalTripDistance = Double.parseDouble(Value);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }


            Value = request.getParameter("journey_start_meter_reading");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("journey_start_meter_reading = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    vm_requisitionDTO.meterReadingInitial = Double.parseDouble(Value);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }


            Value = request.getParameter("journey_end_meter_reading");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("journey_end_meter_reading = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    vm_requisitionDTO.meterReadingEnd = Double.parseDouble(Value);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("select_petrol_supply");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("select_petrol_supply = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                if (Value.equalsIgnoreCase("on")) {
                    Value = "1";
                } else {
                    Value = "2";
                }


                vm_requisitionDTO.petrolGiven = Integer.parseInt(Value);
            }


//			Value = request.getParameter("select_petrol_supply");
//
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("select_petrol_supply = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				try
//				{
//					vm_requisitionDTO.petrolGiven = Integer.parseInt(Value);
//				}
//				catch (Exception e)
//				{
//					e.printStackTrace();
//				}
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}


            Value = request.getParameter("vehicle_petrol_required");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("vehicle_petrol_required = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    vm_requisitionDTO.petrolAmount = Double.parseDouble(Value);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("vehicle_petrol_consumption");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    vm_requisitionDTO.petrolConsumption = Double.parseDouble(Value);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

//            PETROL STOCK AND PREVIOUS PETROL STOCK CALCULATION
            if(!addFlag) {
                if (vm_requisitionDTO.petrolPreviousStock < 0) vm_requisitionDTO.petrolPreviousStock = 0;
                if (vm_requisitionDTO.petrolAmount < 0) vm_requisitionDTO.petrolAmount = 0;
                if (vm_requisitionDTO.petrolConsumption < 0) vm_requisitionDTO.petrolConsumption = 0;
                if (vm_requisitionDTO.petrolStock < 0) vm_requisitionDTO.petrolStock = 0;
                Vm_requisitionDTO dto = vm_requisitionDAO
                        .getLastUsedVehicleDTO(vm_requisitionDTO.requesterEmpId, vm_requisitionDTO.givenVehicleId,
                                vm_requisitionDTO.iD,CommonApprovalStatus.RECEIVED.getValue());
                if(dto != null) vm_requisitionDTO.petrolPreviousStock = dto.petrolStock;
                else vm_requisitionDTO.petrolPreviousStock = 0;
                vm_requisitionDTO.petrolStock = vm_requisitionDTO.petrolPreviousStock + vm_requisitionDTO.petrolAmount
                        - vm_requisitionDTO.petrolConsumption;
            }


            Value = request.getParameter("vehicle_hour_spent");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("vehicle_hour_spent = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    vm_requisitionDTO.totalTripTime = Double.parseDouble(Value);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            //vm_requisitionDTO.status = 4;//status=0 => pending,1=>approved,2=>reject,3=>cancel,4=>receive

            vm_requisitionDTO.status = CommonApprovalStatus.RECEIVED.getValue();

            vm_requisitionDTO.paidAmount = (vm_requisitionDTO.totalTripDistance * VM_REQUISITION_PER_KM_FARE) +
                    (vm_requisitionDTO.totalTripTime * VM_REQUISITION_HOURLY_FARE) +
                    (vm_requisitionDTO.petrolAmount * VM_REQUISITION_PETROL_PER_LITER);

            long returnedID = -1;

            if (isPermanentTable == false) //add new row for validation and make the old row outdated
            {
                vm_requisitionDAO.setIsDeleted(vm_requisitionDTO.iD, CommonDTO.OUTDATED);
                returnedID = vm_requisitionDAO.add(vm_requisitionDTO);
                vm_requisitionDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
            } else if (addFlag == true) {
                returnedID = vm_requisitionDAO.manageWriteOperations(vm_requisitionDTO, SessionConstants.INSERT, -1, userDTO);
            } else {
                returnedID = vm_requisitionDAO.manageWriteOperations(vm_requisitionDTO, SessionConstants.UPDATE, -1, userDTO);
            }

            if (isPermanentTable) {
                ApiResponse apiResponse = null;
                if (vm_requisitionDTO.vehicleRequisitionPurposeCat == 2) {
                    apiResponse = ApiResponse.makeSuccessResponse("Vm_requisitionServlet?actionType=receiveFormMoneyReceipt&ID=" + vm_requisitionDTO.iD);
                    //response.sendRedirect("Vm_requisitionServlet?actionType=receiveFormMoneyReceipt&ID="+vm_requisitionDTO.iD);
                } else {
                    apiResponse = ApiResponse.makeSuccessResponse("Vm_requisitionServlet?actionType=vehicle_receive_search");
                    //response.sendRedirect("Vm_requisitionServlet?actionType=vehicle_receive_search");
                }

                PrintWriter pw = response.getWriter();
                pw.write(apiResponse.getJSONString());
                pw.flush();
                pw.close();
                //return;

            } else {
                commonRequestHandler.validate(Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(returnedID), request, response, userDTO);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void receiveFormMoneyReceipt(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {

        System.out.println("in receiveFormMoneyReceipt");
        Vm_requisitionDTO vm_requisitionDTO = null;
        try {
            vm_requisitionDTO = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(id);
            request.setAttribute("ID", vm_requisitionDTO.iD);
            request.setAttribute("vm_requisitionDTO", vm_requisitionDTO);
            request.setAttribute("vm_requisitionDAO", vm_requisitionDAO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            URL = "vm_receive/vehicleReceiveFormMoneyReceipt.jsp?actionType=edit";

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void vehicleReceiveEditForm(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {

        System.out.println("in vehicleReceiveEditForm");
        Vm_requisitionDTO vm_requisitionDTO = null;
        try {
            vm_requisitionDTO = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(id);
            request.setAttribute("ID", vm_requisitionDTO.iD);
            request.setAttribute("vm_requisitionDTO", vm_requisitionDTO);
            request.setAttribute("vm_requisitionDAO", vm_requisitionDAO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "vm_requisition/vm_requisitionInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "vm_requisition/vm_requisitionSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "vm_receive/vehicleReceiveEditFormBody.jsp?actionType=edit";
                } else {
                    URL = "vm_receive/vehicleReceiveEditForm.jsp?actionType=edit";
                    //URL = "vm_requisition/vm_requisitionEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void vehicleReceiveSearchViewForm(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {

        System.out.println("in vm_receiveSearchViewForm");
        Vm_requisitionDTO vm_requisitionDTO = null;
        try {
            vm_requisitionDTO = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(id);
            request.setAttribute("ID", vm_requisitionDTO.iD);
            request.setAttribute("vm_requisitionDTO", vm_requisitionDTO);
            request.setAttribute("vm_requisitionDAO", vm_requisitionDAO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            URL = "vm_receive/vm_receiveSearchViewForm.jsp?actionType=edit";

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void vehicleReceiveSearchViewFormSameCarSameEmp(HttpServletRequest request, HttpServletResponse response,
                 long empId, long vehicleId) throws ServletException, IOException {

        System.out.println("in vm_receiveSearchViewForm");
        List<Vm_requisitionDTO> vm_requisitionDTOs = null;
        try {
            vm_requisitionDTOs = Vm_requisitionRepository.getInstance()
                    .getVm_requisitionDTOByRequesterEmpIdAndVehicleId(empId, vehicleId);
            request.setAttribute("vm_requisitionDTOs", vm_requisitionDTOs);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            URL = "vm_receive/vm_receiveSearchViewForm.jsp?actionType=edit";

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void vehicleReceiveSearchViewFormSameCar(HttpServletRequest request, HttpServletResponse response,
                                                     long vehicleId) throws ServletException, IOException {

        System.out.println("in vm_receiveSearchViewForm");
        List<Vm_requisitionDTO> vm_requisitionDTOs = null;
        try {
            vm_requisitionDTOs = Vm_requisitionRepository.getInstance()
                    .getVm_requisitionDTOByVehicleId(vehicleId);

            request.setAttribute("vm_requisitionDTOs", vm_requisitionDTOs);
            request.setAttribute("vehicleId", vehicleId);
            request.getRequestDispatcher("vm_vehicle_wise_log_book_report/vm_vehicle_wise_log_bookViewBody.jsp")
                    .forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void vehicleReceiveViewForm(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {

        System.out.println("in vehicleReceiveViewForm");
        Vm_requisitionDTO vm_requisitionDTO = null;
        try {
            vm_requisitionDTO = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(id);
            request.setAttribute("ID", vm_requisitionDTO.iD);
            request.setAttribute("vm_requisitionDTO", vm_requisitionDTO);
            request.setAttribute("vm_requisitionDAO", vm_requisitionDAO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "vm_requisition/vm_requisitionInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "vm_requisition/vm_requisitionSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "vm_receive/vm_receiveViewFormBody.jsp?actionType=edit";
                } else {
                    URL = "vm_receive/vm_receiveViewForm.jsp?actionType=edit";
                    //URL = "vm_requisition/vm_requisitionEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getVm_requisition(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {
        System.out.println("in getVm_requisition");
        Vm_requisitionDTO vm_requisitionDTO = null;
        try {
            vm_requisitionDTO = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(id);
            request.setAttribute("ID", vm_requisitionDTO.iD);
            request.setAttribute("vm_requisitionDTO", vm_requisitionDTO);
            request.setAttribute("vm_requisitionDAO", vm_requisitionDAO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "vm_requisition/vm_requisitionInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "vm_requisition/vm_requisitionSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "vm_requisition/vm_requisitionEditBody.jsp?actionType=edit";
                } else {
                    URL = "vm_requisition/vm_requisitionEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getPrePendingVm_requisition(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {
        System.out.println("in getVm_requisition");
        Vm_requisitionDTO vm_requisitionDTO = null;
        try {
            vm_requisitionDTO = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(id);
            request.setAttribute("ID", vm_requisitionDTO.iD);
            request.setAttribute("vm_requisitionDTO", vm_requisitionDTO);
            request.setAttribute("vm_requisitionDAO", vm_requisitionDAO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "vm_requisition/vm_requisitionInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "vm_requisition/vm_requisitionSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "vm_requisition/vm_requisitionPrePendingEditBody.jsp?actionType=edit";
                } else {
                    URL = "vm_requisition/vm_requisitionPrePendingEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getVm_requisition(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getVm_requisition(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void getPrePendingVm_requisition(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getPrePendingVm_requisition(request, response, Long.parseLong(request.getParameter("ID")));
    }


    private void cancel(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% cancel vm requisition");
            String path = getServletContext().getRealPath("/img2/");
            Vm_requisitionDTO vm_requisitionDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

            System.out.println("in  searchVm_requisition 1");
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
            UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

            vm_requisitionDTO = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(Long.parseLong(request.getParameter("vmRequisitionId")));
            String cancellationReason = request.getParameter("cancellationReason");
            if (cancellationReason == null || cancellationReason.isEmpty()) {
                cancellationReason = "";
            }
            vm_requisitionDTO.status = CommonApprovalStatus.CANCELLED.getValue();
            vm_requisitionDTO.cancellationReason = cancellationReason;
            vm_requisitionDTO.cancellationDate = System.currentTimeMillis();
            vm_requisitionDTO.lastModificationTime = System.currentTimeMillis();

            System.out.println("Done adding  addVm_requisition dto = " + vm_requisitionDTO);
            long returnedID = -1;

            returnedID = vm_requisitionDAO.manageWriteOperations(vm_requisitionDTO, SessionConstants.UPDATE, -1, userDTO);

            //cancellation sms and email start

            String formatted_startDate = simpleDateFormat.format(new Date(vm_requisitionDTO.startDate));

            EmployeeSearchModel requesterModel =
                    gson.fromJson(
                            EmployeeSearchModalUtil.getEmployeeSearchModelJson(vm_requisitionDTO.requesterEmpId,
                                    vm_requisitionDTO.requesterOfficeUnitId, vm_requisitionDTO.requesterOrgId),
                            EmployeeSearchModel.class);

            EmployeeSearchModel driverModel =
                    gson.fromJson(
                            EmployeeSearchModalUtil.getEmployeeSearchModelJson(vm_requisitionDTO.driverEmpId,
                                    vm_requisitionDTO.driverOfficeUnitId, vm_requisitionDTO.driverOrgId),
                            EmployeeSearchModel.class);

            String requesterCancellationText = "আপনার "+Utils.getDigitBanglaFromEnglish(formatted_startDate)+", "+
                    Utils.getDigitBanglaFromEnglish(vm_requisitionDTO.startTime)+" এর যানবাহন অধিযাচন এর অনুরোধটি বাতিল করা হয়েছে। ধন্যবাদ।";
            String driverCancellationText = "আপনার "+Utils.getDigitBanglaFromEnglish(formatted_startDate)+", "+
                    Utils.getDigitBanglaFromEnglish(vm_requisitionDTO.startTime)+" এর "+requesterModel.employeeNameBn+", "+
                    requesterModel.organogramNameBn+", "+requesterModel.officeUnitNameBn+" এর দায়িত্বটি বাতিল করা হয়েছে। ধন্যবাদ। ";

            //sms start
            if(!requesterModel.phoneNumber.isEmpty()){
                SmsService.send(requesterModel.phoneNumber, String.format(requesterCancellationText ));
            }
            if(!driverModel.phoneNumber.isEmpty()){
                SmsService.send(driverModel.phoneNumber, String.format(driverCancellationText ));
            }
            //sms end

            //email start
            if (requesterModel.email != null && !requesterModel.email.isEmpty()) {
                sendMail(requesterModel.email , requesterCancellationText, "Cancellation For Use Of Government Vehicle");
            }
            if (driverModel.email != null && !driverModel.email.isEmpty()) {

                sendMail(driverModel.email , driverCancellationText, "Cancellation For Use Of Government Vehicle");
            }
            //email end

            //cancellation sms and email end


            if (true) {
                String inPlaceSubmit = request.getParameter("inplacesubmit");

                if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                    //getVm_requisition(request, response, returnedID);
                    ApiResponse.sendErrorResponse(response, "Can not cancel");
                } else {
                    //response.sendRedirect("Vm_requisitionServlet?actionType=search");
                    ApiResponse.sendSuccessResponse(response, "Vm_requisitionServlet?actionType=search");
                }
            } else {
                commonRequestHandler.validate(Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(returnedID), request, response, userDTO);
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e);
            ApiResponse.sendErrorResponse(response, e.getMessage());
        }
    }
    private void sendMail(String mailId, String message, String subject) {
        if (mailId != null && mailId.trim().length() > 0) {
            SendEmailDTO sendEmailDTO = new SendEmailDTO();
            sendEmailDTO.setTo(new String[]{mailId});
            sendEmailDTO.setText(message);
            sendEmailDTO.setSubject(subject);
            sendEmailDTO.setFrom("edms@pbrlp.gov.bd");
            try {
                new EmailService().sendMail(sendEmailDTO);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            logger.debug("<<<<mail id is null or empty, mailId : " + mailId);
        }
    }

    private void vehicleReceiveSearchViewForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        vehicleReceiveSearchViewForm(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void vehicleReceiveSearchViewFormSameCarSameEmp(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        vehicleReceiveSearchViewFormSameCarSameEmp(request, response, Long.parseLong(request.getParameter("EmpId")),
                Long.parseLong(request.getParameter("vehicleId")));
    }
    private void vehicleReceiveSearchViewFormSameCar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        vehicleReceiveSearchViewFormSameCar(request, response, Long.parseLong(request.getParameter("vehicleId")));
    }

    private void vehicleReceiveViewForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        vehicleReceiveViewForm(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void vehicleReceiveEditForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        vehicleReceiveEditForm(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void receiveFormMoneyReceipt(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        receiveFormMoneyReceipt(request, response, Long.parseLong(request.getParameter("ID")));
    }


    private void searchVm_requisition(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  searchVm_requisition 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
        boolean isAdmin = role.ID == SessionConstants.VEHICLE_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;

        String filterOwn = " (requester_org_id = " + userDTO.organogramID + " OR inserted_by_organogram_id = " + userDTO.organogramID;
        filterOwn += isAdmin ? " OR true) " : ") ";
        if (filter != null && !filter.isEmpty()) filter += " AND ";
        filter += filterOwn;

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_VM_REQUISITION,
                request,
                vm_requisitionDAO,
                SessionConstants.VIEW_VM_REQUISITION,
                SessionConstants.SEARCH_VM_REQUISITION,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("vm_requisitionDAO", vm_requisitionDAO);
        RequestDispatcher rd;
        if (!isPermanent) {
            if (hasAjax == false) {
                System.out.println("Going to vm_requisition/vm_requisitionApproval.jsp");
                rd = request.getRequestDispatcher("vm_requisition/vm_requisitionApproval.jsp");
            } else {
                System.out.println("Going to vm_requisition/vm_requisitionApprovalForm.jsp");
                rd = request.getRequestDispatcher("vm_requisition/vm_requisitionApprovalForm.jsp");
            }
        } else {
            if (hasAjax == false) {
                System.out.println("Going to vm_requisition/vm_requisitionSearch.jsp");
                rd = request.getRequestDispatcher("vm_requisition/vm_requisitionSearch.jsp");
            } else {
                System.out.println("Going to vm_requisition/vm_requisitionSearchForm.jsp");
                rd = request.getRequestDispatcher("vm_requisition/vm_requisitionSearchForm.jsp");
            }
        }
        rd.forward(request, response);
    }


    private void PaymentVm_receive(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  PaymentVm_receive 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);


        //filter = " status = 4 AND payment_given=2 ";

        filter = " status = " + CommonApprovalStatus.RECEIVED.getValue() +
                " AND payment_given= " + CommonApprovalStatus.UNPAID.getValue()+
                " AND requester_emp_id = " + userDTO.employee_record_id + " ";


        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_VM_REQUISITION,
                request,
                vm_requisitionDAO,
                SessionConstants.VIEW_VM_REQUISITION,
                SessionConstants.SEARCH_VM_REQUISITION,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("vm_requisitionDAO", vm_requisitionDAO);
        RequestDispatcher rd;
        if (!isPermanent) {
            if (hasAjax == false) {
                System.out.println("Going to vm_receive/vm_requisitionApproval.jsp");
                rd = request.getRequestDispatcher("vm_requisition/vm_requisitionApproval.jsp");
            } else {
                System.out.println("Going to vm_requisition/vm_requisitionApprovalForm.jsp");
                rd = request.getRequestDispatcher("vm_requisition/vm_requisitionApprovalForm.jsp");
            }
        } else {
            if (hasAjax == false) {
                System.out.println("Going to vm_receive/vm_receiveSearchAndAdd.jsp");
                rd = request.getRequestDispatcher("vm_receive/vm_receivePayment.jsp");
            } else {
                System.out.println("Going to vm_receive/vm_receiveSearchAndAddForm.jsp");
                rd = request.getRequestDispatcher("vm_receive/vm_receivePaymentSearchForm.jsp");
            }
        }
        rd.forward(request, response);
    }

    private void SearchVm_receive(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  SearchVm_receive 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        //filter = " status = 1 AND (receive_time!='' AND receive_time!='00:00:00' AND  payment_given!=-1) ";
        //filter = " status = 1 AND (receive_time!='' AND receive_time!='00:00:00' ) ";
        //filter = " status = 4 ";

        filter = " status = " + CommonApprovalStatus.RECEIVED.getValue() +
                " AND requester_emp_id = " + userDTO.employee_record_id;


        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_VM_REQUISITION,
                request,
                vm_requisitionDAO,
                SessionConstants.VIEW_VM_REQUISITION,
                SessionConstants.SEARCH_VM_REQUISITION,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("vm_requisitionDAO", vm_requisitionDAO);
        RequestDispatcher rd;
        if (!isPermanent) {
            if (hasAjax == false) {
                System.out.println("Going to vm_receive/vm_requisitionApproval.jsp");
                rd = request.getRequestDispatcher("vm_requisition/vm_requisitionApproval.jsp");
            } else {
                System.out.println("Going to vm_requisition/vm_requisitionApprovalForm.jsp");
                rd = request.getRequestDispatcher("vm_requisition/vm_requisitionApprovalForm.jsp");
            }
        } else {
            if (hasAjax == false) {
                System.out.println("Going to vm_receive/vm_receiveSearchAndAdd.jsp");
                rd = request.getRequestDispatcher("vm_receive/vm_receiveSearch.jsp");
            } else {
                System.out.println("Going to vm_receive/vm_receiveSearchAndAddForm.jsp");
                rd = request.getRequestDispatcher("vm_receive/vm_receiveSearchBody.jsp");
            }
        }
        rd.forward(request, response);
    }

    private void AddVm_receive(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  AddVm_receive 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        //filter = " status = 1 AND (receive_time='' OR receive_time='00:00:00')  ";

        filter = " status = " + CommonApprovalStatus.SATISFIED.getValue() +
                " AND (receive_time='' OR receive_time='00:00:00')  " +
                " AND requester_emp_id = " + userDTO.employee_record_id;

        //filter = " status = 1  AND  payment_given=-1 ";


        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_VM_REQUISITION,
                request,
                vm_requisitionDAO,
                SessionConstants.VIEW_VM_REQUISITION,
                SessionConstants.SEARCH_VM_REQUISITION,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("vm_requisitionDAO", vm_requisitionDAO);
        RequestDispatcher rd;
        if (!isPermanent) {
            if (hasAjax == false) {
                System.out.println("Going to vm_receive/vm_requisitionApproval.jsp");
                rd = request.getRequestDispatcher("vm_requisition/vm_requisitionApproval.jsp");
            } else {
                System.out.println("Going to vm_requisition/vm_requisitionApprovalForm.jsp");
                rd = request.getRequestDispatcher("vm_requisition/vm_requisitionApprovalForm.jsp");
            }
        } else {
            if (hasAjax == false) {
                System.out.println("Going to vm_receive/vm_receiveSearchAndAdd.jsp");
                rd = request.getRequestDispatcher("vm_receive/vm_receiveSearchAndAdd.jsp");
            } else {
                System.out.println("Going to vm_receive/vm_receiveSearchAndAddForm.jsp");
                rd = request.getRequestDispatcher("vm_receive/vm_receiveSearchAndAddForm.jsp");
            }
        }
        rd.forward(request, response);
    }

    private void vehicle_receive_getSearchPage(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  vehicle_receive_getSearchPage 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        //String filterOwn = " status = 1 AND (receive_time='' OR receive_time='00:00:00') ";
        String filterOwn = " status = " + CommonApprovalStatus.SATISFIED.getValue() + " AND (receive_time='' OR receive_time='00:00:00')  ";
        if (filter != null && !filter.isEmpty()) filter += " AND ";
        filter += filterOwn;

        //filter = " status = 1 AND (receive_time='' OR receive_time='00:00:00')  ";

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_VM_REQUISITION,
                request,
                vm_requisitionDAO,
                SessionConstants.VIEW_VM_REQUISITION,
                SessionConstants.SEARCH_VM_REQUISITION,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("vm_requisitionDAO", vm_requisitionDAO);
        RequestDispatcher rd;
        if (!isPermanent) {
            if (hasAjax == false) {
                System.out.println("Going to vm_requisition/vm_requisitionApproval.jsp");
                rd = request.getRequestDispatcher("vm_requisition/vm_requisitionApproval.jsp");
            } else {
                System.out.println("Going to vm_requisition/vm_requisitionApprovalForm.jsp");
                rd = request.getRequestDispatcher("vm_requisition/vm_requisitionApprovalForm.jsp");
            }
        } else {
            if (hasAjax == false) {
                System.out.println("Going to vm_receive/vm_receiveSearchAndAdd.jsp");
                rd = request.getRequestDispatcher("vm_receive/vm_receiveSearchAndAdd.jsp");
            } else {
                System.out.println("Going to vm_receive/vm_receiveSearchAndAdd.jsp");
                rd = request.getRequestDispatcher("vm_receive/vm_receiveSearchAndAddForm.jsp");
            }
        }
        rd.forward(request, response);
    }

    private void vehicle_already_received_getSearchPage(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  vehicle_already_received_getSearchPage 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        //filter = " status = 1 AND (receive_time!='' AND receive_time!='00:00:00' AND  payment_given!=-1) ";
        //filter = " status = 1 AND (receive_time!='' AND receive_time!='00:00:00' ) ";
        //String filterOwn = " status = 4 ";
        String filterOwn = " status = " + CommonApprovalStatus.RECEIVED.getValue() + " ";
        if (filter != null && !filter.isEmpty()) filter += " AND ";
        filter += filterOwn;
        //filter = " status = 4 ";


        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_VM_REQUISITION,
                request,
                vm_requisitionDAO,
                SessionConstants.VIEW_VM_REQUISITION,
                SessionConstants.SEARCH_VM_REQUISITION,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("vm_requisitionDAO", vm_requisitionDAO);
        RequestDispatcher rd;


        if (!isPermanent) {
            if (hasAjax == false) {
                System.out.println("Going to vm_requisition/vm_requisitionApproval.jsp");
                rd = request.getRequestDispatcher("vm_requisition/vm_requisitionApproval.jsp");
            } else {
                System.out.println("Going to vm_requisition/vm_requisitionApprovalForm.jsp");
                rd = request.getRequestDispatcher("vm_requisition/vm_requisitionApprovalForm.jsp");
            }
        } else {
//			if(hasAjax == false)
//			{
//				System.out.println("Going to vm_requisition/vm_requisitionSearch.jsp");
//				rd = request.getRequestDispatcher("vm_requisition/vm_requisitionSearch.jsp");
//			}
//			else
//			{
//				System.out.println("Going to vm_requisition/vm_requisitionSearchForm.jsp");
//				rd = request.getRequestDispatcher("vm_requisition/vm_requisitionSearchForm.jsp");
//			}

            if (hasAjax == false) {
                System.out.println("Going to vm_receive/vm_receiveSearch.jsp");
                rd = request.getRequestDispatcher("vm_receive/vm_receiveSearch.jsp");
            } else {
                System.out.println("Going to vm_receive/vm_receiveSearch.jsp");
                rd = request.getRequestDispatcher("vm_receive/vm_receiveSearchForm.jsp");
            }
        }
        rd.forward(request, response);
    }

}

