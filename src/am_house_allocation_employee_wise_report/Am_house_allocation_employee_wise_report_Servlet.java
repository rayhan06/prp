package am_house_allocation_employee_wise_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Am_house_allocation_employee_wise_report_Servlet")
public class Am_house_allocation_employee_wise_report_Servlet  extends HttpServlet {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","tr","allocation_date",">=","","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},
		{"criteria","tr","allocation_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},
		{"criteria","tr","withdrawn_date",">=","AND","long","","",Long.MIN_VALUE + "","withdrawalStartDate", LC.HM_END_DATE + ""},
		{"criteria","tr","withdrawn_date","<=","AND","long","","",Long.MAX_VALUE + "","withdrawalEndDate", LC.HM_END_DATE + ""},
		{"criteria","tr","requester_emp_id","=","AND","String","","","any","requesterEmpId", LC.AM_HOUSE_ALLOCATION_EMPLOYEE_WISE_REPORT_WHERE_REQUESTEREMPID + ""},
		{"criteria","tr","isDeleted","=","AND","String","","","0","isDeleted", LC.AM_HOUSE_ALLOCATION_EMPLOYEE_WISE_REPORT_WHERE_ISDELETED + ""}
	};

	String[][] Display =
	{
			{"display", "tr", "requester_emp_id", "erIdToUserName", ""},
		{"display","tr","requester_emp_id","employee_records_id",""},
		{"display","tr","am_house_old_new_cat","cat",""},
		{"display","tr","am_house_location_cat","cat",""},
		{"display","tr","am_house_class_cat","cat",""},
		{"display","tr","house_id","amHouseIdConverter",""},
		{"display","tr","status","am_office_status_check",""},
		{"display","tr","allocation_date","date",""},
		{"display","tr","withdrawn_date","dateConverterForMinusOne",""}
	};

	String GroupBy = "";
	String OrderBY = "";

	ReportRequestHandler reportRequestHandler;

	public Am_house_allocation_employee_wise_report_Servlet(){

	}

	private final ReportService reportService = new ReportService();

	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}

		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);

		sql = "am_house_allocation tr";

		int i = 0;
		Display[i++][4] = LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_SELECT_USERNAME, loginDTO);
		Display[i++][4] = LM.getText(LC.AM_HOUSE_ALLOCATION_EMPLOYEE_WISE_REPORT_SELECT_REQUESTEREMPID, loginDTO);
		Display[i++][4] = LM.getText(LC.AM_HOUSE_ALLOCATION_EMPLOYEE_WISE_REPORT_SELECT_AMHOUSEOLDNEWCAT, loginDTO);
		Display[i++][4] = LM.getText(LC.AM_HOUSE_ALLOCATION_EMPLOYEE_WISE_REPORT_SELECT_AMHOUSELOCATIONCAT, loginDTO);
		Display[i++][4] = LM.getText(LC.AM_HOUSE_ALLOCATION_EMPLOYEE_WISE_REPORT_SELECT_AMHOUSECLASSCAT, loginDTO);
		Display[i++][4] = LM.getText(LC.AM_HOUSE_ALLOCATION_EMPLOYEE_WISE_REPORT_SELECT_HOUSEID, loginDTO);
		Display[i++][4] = LM.getText(LC.AM_HOUSE_ALLOCATION_EMPLOYEE_WISE_REPORT_SELECT_STATUS, loginDTO);
		Display[i++][4] = LM.getText(LC.AM_HOUSE_ALLOCATION_EMPLOYEE_WISE_REPORT_SELECT_ALLOCATIONDATE, loginDTO);
		Display[i++][4] = LM.getText(LC.AM_HOUSE_ALLOCATION_EMPLOYEE_WISE_REPORT_SELECT_WITHDRAWNDATE, loginDTO);


		String reportName = LM.getText(LC.AM_HOUSE_ALLOCATION_EMPLOYEE_WISE_REPORT_OTHER_AM_HOUSE_ALLOCATION_EMPLOYEE_WISE_REPORT, loginDTO);

		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);


		reportRequestHandler.handleReportGet(request, response, userDTO, "am_house_allocation_employee_wise_report",
				MenuConstants.AM_HOUSE_ALLOCATION_EMPLOYEE_WISE_REPORT_DETAILS, language, reportName, "am_house_allocation_employee_wise_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doGet(request, response);
	}
}
