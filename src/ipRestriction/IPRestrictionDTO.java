package ipRestriction;

public class IPRestrictionDTO {
	public long ID;
	public String IP;
	public long lastHitTime;
	public long nextHitTimeAfter;
	public int type;
	public int hitCount = 1;
	public String username;
	public long lastModificationTime;
	public int isDeleted;

	@Override
	public String toString() {
		return "IPRestrictionDTO [ID=" + ID + ", IP=" + IP + ", lastHitTime=" + lastHitTime + ", nextHitTimeAfter="
				+ nextHitTimeAfter + ", type=" + type + ", username=" + username + ", lastModificationTime="
				+ lastModificationTime + ", isDeleted=" + isDeleted + "]";
	}
}