package ipRestriction;

import common.ConnectionAndStatementUtil;
import common.VbSequencerService;
import dbm.DBMW;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@SuppressWarnings("unused")
public class IPRestrictionDAO implements VbSequencerService {
    private static final Logger logger = Logger.getLogger(IPRestrictionDAO.class);

    private static final String addQuery = "INSERT INTO at_ip_restriction (IP,username,lastHitTime,hitCount,nextHitTimeAfter,type," +
            "lastModificationTime,isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?)";

    private static final String updateQuery = "UPDATE at_ip_restriction SET IP=?,username=?,lastHitTime=?,hitCount=?,nextHitTimeAfter=?," +
            "type=?, lastModificationTime = ? WHERE ID = ?";

    private static final String deleteQuery = "UPDATE at_ip_restriction SET isDeleted = 1,lastModificationTime = %d WHERE ID = %d";

    private static final String getByIpAndUserName = "select * from at_ip_restriction where IP = ? and username = ? and isDeleted = 0 ";

    private static final String getAll = "SELECT * FROM at_ip_restriction WHERE isDeleted = 0";

    private IPRestrictionDAO(){}

    private static final class IPRestrictionDAOHolder {
        static final IPRestrictionDAO INSTANCE = new IPRestrictionDAO();
    }

    public static IPRestrictionDAO getInstance(){
        return IPRestrictionDAOHolder.INSTANCE;
    }

    private void set(PreparedStatement ps,IPRestrictionDTO dto,boolean isInsert) throws Exception {
        int index = 0;
        ps.setObject(++index,dto.IP);
        ps.setObject(++index,dto.username);
        ps.setObject(++index,dto.lastHitTime);
        ps.setObject(++index,dto.hitCount);
        ps.setObject(++index,dto.nextHitTimeAfter);
        ps.setObject(++index,dto.type);
        ps.setObject(++index,dto.lastModificationTime);
        if(isInsert){
            dto.ID = DBMW.getInstance().getNextSequenceId("at_ip_restriction");
            ps.setObject(++index,0);
        }
        ps.setObject(++index,dto.ID);
    }

    private IPRestrictionDTO buildDTOFromResultSet(ResultSet rs){
        IPRestrictionDTO dto = new IPRestrictionDTO();
        try {
            dto.IP = rs.getString("IP");
            dto.username = rs.getString("username");
            dto.lastHitTime = rs.getLong("lastHitTime");
            dto.hitCount = rs.getInt("hitCount");
            dto.nextHitTimeAfter = rs.getLong("nextHitTimeAfter");
            dto.type = rs.getInt("type");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.ID = rs.getInt("ID");
            return dto;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void add(IPRestrictionDTO ipRestrictionDTO) throws Exception {
        ConnectionAndStatementUtil.getLogWritePrepareStatement(ps->{
            try {
                set(ps,ipRestrictionDTO,true);
                logger.debug(ps);
                ps.execute();
                IPRestrictionRepository.getInstance().add(ipRestrictionDTO);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        },addQuery);
    }

    public void update(IPRestrictionDTO ipRestrictionDTO) throws Exception {
        ConnectionAndStatementUtil.getLogWritePrepareStatement(ps->{
            try {
                set(ps,ipRestrictionDTO,false);
                logger.debug(ps);
                ps.executeUpdate();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        },updateQuery);
    }

    public void delete(String ip, String username) throws Exception {
        IPRestrictionDTO ipRestrictionDTO = IPRestrictionRepository.getInstance().getByIPAndUserName(ip,username);
        if(ipRestrictionDTO!=null){
            long currentTime = System.currentTimeMillis();
            ConnectionAndStatementUtil.getLogWriteStatement(st->{
                String sql = String.format(deleteQuery,currentTime,ipRestrictionDTO.ID);
                try {
                    logger.debug(sql);
                    st.executeUpdate(sql);
                    IPRestrictionRepository.getInstance().delete(ipRestrictionDTO);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            });
        }
    }

    public List<IPRestrictionDTO> getAll(){
        return ConnectionAndStatementUtil.getListOfT(getAll,this::buildDTOFromResultSet);
    }
}