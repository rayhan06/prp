package ipRestriction;

/*
 * @author Md. Erfan Hossain
 * @created 07/07/2021 - 12:39 AM
 * @project parliament
 */

import java.util.List;

public class IPRestrictionRepository {
    private List<IPRestrictionDTO> list;

    private IPRestrictionRepository(){
        reload();
    }

    private static class LazyLoader{
        static final IPRestrictionRepository INSTANCE = new IPRestrictionRepository();
    }

    public static IPRestrictionRepository getInstance(){
        return LazyLoader.INSTANCE;
    }

    public void reload(){
        list = IPRestrictionDAO.getInstance().getAll();
    }

    public IPRestrictionDTO getByIPAndUserName(String ip,String username){
        return list.parallelStream()
                .filter(e->e.IP.equals(ip) && e.username.equals(username))
                .findAny()
                .orElse(null);
    }

    public void add(IPRestrictionDTO dto){
        if(dto!=null){
            list.add(dto);
        }
    }

    public void delete(IPRestrictionDTO dto){
        list.remove(dto);
    }
}