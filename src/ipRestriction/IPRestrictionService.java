package ipRestriction;

import config.GlobalConfigConstants;
import config.GlobalConfigurationRepository;
import org.apache.log4j.Logger;
import websecuritylog.WebSecurityLogDTO;

public class IPRestrictionService {

    private static final Logger logger = Logger.getLogger(IPRestrictionService.class);
    public void updateOrInsert(WebSecurityLogDTO webSecurityLogDTO) throws Exception {
        IPRestrictionDTO ipRestrictionDTO = new IPRestrictionDTO();
        ipRestrictionDTO.IP = webSecurityLogDTO.getUrl();
        ipRestrictionDTO.username = webSecurityLogDTO.getUsername();
        ipRestrictionDTO.lastHitTime = webSecurityLogDTO.getAttemptTime();
        updateOrInsert(ipRestrictionDTO);
    }

    public void updateOrInsert(IPRestrictionDTO ipRestrictionDTOArg) throws Exception {
        IPRestrictionDTO ipRestrictionDTO = IPRestrictionRepository.getInstance().getByIPAndUserName(ipRestrictionDTOArg.IP, ipRestrictionDTOArg.username);
        if (ipRestrictionDTO != null) {
            int hitCountToBlock = Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.HIT_COUNT_TO_BLOCK_LOGIN).value);
            int hitCountToDelay = Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.HIT_COUNT_TO_START_DELAY_LOGIN).value);
            if (ipRestrictionDTO.hitCount + 1 >= hitCountToBlock + hitCountToDelay) {
                logger.debug("################### setNextHitTimeAfter " + hitCountToBlock + ":" + hitCountToDelay + ":" + ipRestrictionDTO.hitCount);
                ipRestrictionDTO.nextHitTimeAfter = Long.MAX_VALUE;
                ipRestrictionDTO.hitCount = ipRestrictionDTO.hitCount + 1;
            } else if (ipRestrictionDTO.hitCount + 1 >= hitCountToDelay) {
                int delayDurationInMillis = Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.LOGIN_DELAY_TIME_IN_SECONDS).value) * 1000;
                ipRestrictionDTO.nextHitTimeAfter = System.currentTimeMillis() + delayDurationInMillis;
                ipRestrictionDTO.hitCount = ipRestrictionDTO.hitCount + 1;
            } else {
                ipRestrictionDTO.hitCount = ipRestrictionDTO.hitCount + 1;
            }
            IPRestrictionDAO.getInstance().update(ipRestrictionDTO);
        } else {
            ipRestrictionDTO = new IPRestrictionDTO();
            ipRestrictionDTO.username = ipRestrictionDTOArg.username;
            ipRestrictionDTO.lastHitTime = System.currentTimeMillis();
            ipRestrictionDTO.IP = ipRestrictionDTOArg.IP;
            ipRestrictionDTO.lastModificationTime = System.currentTimeMillis();
            IPRestrictionDAO.getInstance().add(ipRestrictionDTO);
        }
    }

    public IPRestrictionDTO getIPRestrictionDTOByIPAndUser(String ip, String username) throws Exception {
        return IPRestrictionRepository.getInstance().getByIPAndUserName(ip, username);
    }

    public void deleteIPRestrictionDTOByIPAndUser(String loginSourceIP, String username) throws Exception {
        IPRestrictionDAO.getInstance().delete(loginSourceIP, username);
    }
}