package payroll_month_bill;

import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import bill_register.Bill_registerDAO;
import bill_register.Bill_registerDTO;
import bill_register.Bill_registerServlet;
import budget.BudgetCategoryEnum;
import budget.BudgetUtils;
import budget_mapping.Budget_mappingDTO;
import budget_mapping.Budget_mappingRepository;
import budget_office.Budget_officeRepository;
import budget_register.Budget_registerDAO;
import budget_register.Budget_registerDTO;
import budget_selection_info.BudgetSelectionInfoRepository;
import com.google.gson.Gson;
import common.BaseServlet;
import employee_records.EmploymentEnum;
import finance.CashTypeEnum;
import finance.FinanceUtil;
import login.LoginDTO;
import office_unit_organograms.SameDesignationGroup;
import org.apache.log4j.Logger;
import payroll_allowance_configuration.Payroll_allowance_configurationModel;
import payroll_allowance_configuration.Payroll_allowance_configurationRepository;
import payroll_allowance_lookup.Payroll_allowance_lookupDTO;
import payroll_allowance_lookup.Payroll_allowance_lookupRepository;
import payroll_deduction_lookup.Payroll_deduction_lookupDTO;
import payroll_deduction_lookup.Payroll_deduction_lookupRepository;
import payroll_month_bill_allowance.Payroll_month_bill_allowanceDAO;
import payroll_month_bill_allowance.Payroll_month_bill_allowanceDTO;
import payroll_month_bill_deduction.Payroll_month_bill_deductionDAO;
import payroll_month_bill_deduction.Payroll_month_bill_deductionDTO;
import pbReport.DateUtils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import tax_deduction_configuration.Tax_deduction_configurationDTO;
import tax_deduction_configuration.Tax_deduction_configurationModel;
import tax_deduction_configuration.Tax_deduction_configurationRepository;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Servlet implementation class Payroll_month_billServlet
 */
@WebServlet("/Payroll_month_billServlet")
@MultipartConfig
public class Payroll_month_billServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static final long PRIVILEGE_SALARY_SUB_CODE_ID = 304L;
    public static final String MONTH_BILL_DESCRIPTION = "%s-এ কর্মরত %s জন প্রিভিলেজ কর্মকর্তা/কর্মচারীর %s মাসের মজুরী";
    public static final CashTypeEnum CASH_TYPE = CashTypeEnum.SALARY;
    public static Logger logger = Logger.getLogger(Payroll_month_billServlet.class);

    @Override
    public String getTableName() {
        return Payroll_month_billDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Payroll_month_billServlet";
    }

    @Override
    public Payroll_month_billDAO getCommonDAOService() {
        return Payroll_month_billDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PAYROLL_MONTH_BILL_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PAYROLL_MONTH_BILL_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PAYROLL_MONTH_BILL_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Payroll_month_billServlet.class;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "ajax_getMonthPayrollData": {
                    long monthYear = new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("monthYear")).getTime();
                    long budgetOfficeId = Long.parseLong(request.getParameter("budgetOfficeId"));
                    int employmentCat = Integer.parseInt(request.getParameter("employmentCat"));
                    String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
                    Map<String, Object> res = getMonthPayrollData(budgetOfficeId, monthYear, employmentCat, language);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().println(new Gson().toJson(res));
                    return;
                }
                case "prepareBill": {
                    setBillDTOs(request);
                    request.getRequestDispatcher("payroll_month_bill/payroll_month_billPreview.jsp").forward(request, response);
                    return;
                }
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void setBillDTOs(HttpServletRequest request) throws FileNotFoundException {
        long monthYear = Long.parseLong(request.getParameter("monthYear"));
        long budgetOfficeId = Long.parseLong(request.getParameter("budgetOfficeId"));
        int employmentCat = EmploymentEnum.PRIVILEGED.getValue();
        List<Payroll_month_billDTO> billDTOs = Payroll_month_billDAO.getInstance().getByBudgetOfficeAndMonthAndEmployment(budgetOfficeId, monthYear, employmentCat);
        if (billDTOs == null || billDTOs.isEmpty())
            throw new FileNotFoundException("no DTOs found");

        request.setAttribute("monthBillDTOs", billDTOs);
        request.setAttribute("monthYear", monthYear);
        request.setAttribute("budgetOfficeId", budgetOfficeId);
    }

    private Map<String, Object> getMonthPayrollData(long budgetOfficeId, long monthYear, int employmentCat, String language) {
        List<Payroll_month_billDTO> dtoList = Payroll_month_billDAO.getInstance().getByBudgetOfficeAndMonthAndEmployment(budgetOfficeId, monthYear, employmentCat);
        List<Payroll_month_billModel> month_billModels;

        boolean isAlreadyAdded = dtoList != null && !dtoList.isEmpty();
        List<Payroll_allowance_configurationModel> allowanceModels = new ArrayList<>();
        List<Tax_deduction_configurationModel> deductionModels = new ArrayList<>();
        Map<String, Object> res = new HashMap<>();
        if (isAlreadyAdded) {
            month_billModels =
                    dtoList.stream()
                            .map(dto -> new Payroll_month_billModel(dto, language))
                            .collect(Collectors.toList());
            if (month_billModels.size() > 0) {
                Payroll_month_billModel model = month_billModels.get(0);
                allowanceModels = model.allowanceDTOList.stream()
                        .map(dto -> new Payroll_allowance_configurationModel(dto, language))
                        .sorted(Comparator.comparing(dto -> dto.configurationId))
                        .collect(Collectors.toList());
                deductionModels = model.deductionDTOList.stream()
                        .map(dto -> new Tax_deduction_configurationModel(dto, language))
                        .sorted(Comparator.comparing(dto -> dto.configurationId))
                        .collect(Collectors.toList());
            }

        } else {
            Set<Long> budgetOfficeIds = Budget_officeRepository.getInstance()
                    .getOfficeUnitIdSet(budgetOfficeId);
            List<AllowanceEmployeeInfoDTO> employeeInfoDTOs =
                    AllowanceEmployeeInfoRepository.getInstance().buildDTOsFromCacheForEmploymentType(budgetOfficeIds, employmentCat);

            month_billModels =
                    employeeInfoDTOs.stream()
                            .map(employeeInfoDTO -> new Payroll_month_billModel(employeeInfoDTO, monthYear, employmentCat, language))
                            .collect(Collectors.toList());

            allowanceModels = Payroll_allowance_configurationRepository.getInstance()
                    .getActiveModels(EmploymentEnum.PRIVILEGED.getValue(), language);
            deductionModels = Tax_deduction_configurationRepository.
                    getInstance().getActiveModels(EmploymentEnum.PRIVILEGED.getValue(), language);
        }

        res.put("isAlreadyAdded", isAlreadyAdded);
        res.put("monthBillModels", month_billModels);
        res.put("allowanceModels", allowanceModels);
        res.put("deductionModels", deductionModels);
        return res;
    }


    public Payroll_month_billDTO addMonthPayroll(Payroll_month_billServlet.UserInput userInput, int employmentCat, Map<Long, List<String>> deductionAmountMap, int index, long curTime, List<String> houseAddressList) throws Exception {
        Payroll_month_billDTO month_billDTO = new Payroll_month_billDTO();
        month_billDTO.voucherNumber = userInput.voucherNumber;
        month_billDTO.insertedBy = month_billDTO.modifiedBy = userInput.modifierId;
        month_billDTO.insertionTime = month_billDTO.lastModificationTime = curTime;
        month_billDTO.budgetOfficeId = userInput.budgetOfficeId;
        month_billDTO.monthYear = userInput.monthYear;
        month_billDTO.employmentCat = employmentCat;
        month_billDTO.budgetRegisterId = userInput.budgetRegisterId;
        month_billDTO.billRegisterId = userInput.billRegisterId;
        AllowanceEmployeeInfoDTO employeeInfoDTO =
                AllowanceEmployeeInfoRepository.getInstance()
                        .getByEmployeeRecordId(userInput.employeeRecordsId);
        if (employeeInfoDTO == null) return null;

        month_billDTO.allowanceEmployeeInfoId = employeeInfoDTO.iD;
        long organogramId = employeeInfoDTO.organogramId;
        month_billDTO.organogramKey = new SameDesignationGroup(organogramId).organogramKey.trim();
        List<Payroll_month_bill_allowanceDTO> allowanceDTOList = getAllowanceDTOList(month_billDTO.organogramKey, employmentCat);
        List<Payroll_month_bill_deductionDTO> deductionDTOList = getDeductionDTOList(month_billDTO.organogramKey, employmentCat);
        month_billDTO.netAmount = allowanceDTOList.stream().mapToInt(e -> e.amount).sum() -
                deductionDTOList.stream().map(dto -> deductionAmountMap.get(dto.taxDeductionConfigurationId).get(index)).mapToLong(Long::parseLong).sum();
        long returnId = Payroll_month_billDAO.getInstance().add(month_billDTO);
        addMonthBillAllowance(allowanceDTOList, month_billDTO.lastModificationTime, month_billDTO.modifiedBy, returnId);
        addMonthBillDeduction(deductionDTOList, deductionAmountMap, index, month_billDTO.lastModificationTime, month_billDTO.modifiedBy, returnId, houseAddressList);
        return month_billDTO;
    }

    private void addMonthBillAllowance(List<Payroll_month_bill_allowanceDTO> allowanceDTOList, long time, long modifiedId, long payrollMonthId) throws Exception {
        for (Payroll_month_bill_allowanceDTO allowanceDTO : allowanceDTOList) {
            allowanceDTO.insertedBy = modifiedId;
            allowanceDTO.modifiedBy = modifiedId;
            allowanceDTO.insertionTime = time;
            allowanceDTO.lastModificationTime = time;
            allowanceDTO.payrollMonthBillId = payrollMonthId;
            Payroll_month_bill_allowanceDAO.getInstance().add(allowanceDTO);
        }
    }

    private void addMonthBillDeduction(List<Payroll_month_bill_deductionDTO> deductionDTOList, Map<Long, List<String>> deductionAmountMap, int index, long time, long modifiedId, long payrollMonthId, List<String> houseAddressList) throws Exception {
        String tmp;
        for (Payroll_month_bill_deductionDTO deductionDTO : deductionDTOList) {
            deductionDTO.insertedBy = modifiedId;
            deductionDTO.modifiedBy = modifiedId;
            deductionDTO.insertionTime = time;
            deductionDTO.lastModificationTime = time;
            deductionDTO.amount = 0;
            tmp = deductionAmountMap.get(deductionDTO.taxDeductionConfigurationId).get(index);
            if (tmp != null && !tmp.equalsIgnoreCase("")) {
                deductionDTO.amount = Integer.parseInt(tmp);
            }
            if (index < houseAddressList.size() && deductionDTO.taxDeductionConfigurationId == Tax_deduction_configurationRepository.getInstance().getHouseRentDeductionId()) {
                deductionDTO.dataField = houseAddressList.get(index);
            }
            deductionDTO.payrollMonthBillId = payrollMonthId;
            Payroll_month_bill_deductionDAO.getInstance().add(deductionDTO);
        }
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        String url = getServletName() + "?actionType=";

        String source = request.getParameter("source");
        if ("prepareBill".equalsIgnoreCase(source)) {
            url += "prepareBill";
        } else if ("prepareSummary".equalsIgnoreCase(source)) {
            url += "prepareSummary";
        }

        Payroll_month_billDTO month_billDTO = (Payroll_month_billDTO) commonDTO;
        url += ("&monthYear=" + month_billDTO.monthYear + "&budgetOfficeId=" + month_billDTO.budgetOfficeId);
        return url;
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAjaxAddRedirectURL(request, commonDTO);
    }

    public static String getBillDescription(UserInput userInput) {
        String budgetOfficeNameBn = Budget_officeRepository.getInstance().getText(userInput.budgetOfficeId, "Bangla");
        String monthName = DateUtils.getMonthYear(userInput.monthYear, "Bangla", "/");
        return String.format(
                MONTH_BILL_DESCRIPTION,
                budgetOfficeNameBn,
                StringUtils.convertToBanNumber(String.valueOf(userInput.employeeCount)),
                monthName
        );
    }

    private Budget_registerDTO addBudgetRegisterDTO(UserInput userInput) throws Exception {
        Budget_registerDTO budgetRegisterDTO = new Budget_registerDTO();

        budgetRegisterDTO.recipientName = FinanceUtil.getFinance1HeadDesignation("bangla");
        budgetRegisterDTO.issueNumber = "";
        budgetRegisterDTO.issueDate = System.currentTimeMillis();
        budgetRegisterDTO.description = getBillDescription(userInput);
        budgetRegisterDTO.billAmount = -1;

        Long budgetSelectionInfoId =
                BudgetSelectionInfoRepository.getInstance()
                        .getId(BudgetUtils.getEconomicYear(userInput.monthYear));
        if (budgetSelectionInfoId != null)
            budgetRegisterDTO.budgetSelectionInfoId = budgetSelectionInfoId;

        Budget_mappingDTO budgetMappingDTO =
                Budget_mappingRepository.getInstance()
                        .getDTO(userInput.budgetOfficeId, BudgetCategoryEnum.OPERATIONAL.getValue());
        if (budgetMappingDTO != null) {
            budgetRegisterDTO.budgetMappingId = budgetMappingDTO.iD;
            budgetRegisterDTO.budgetOfficeId = budgetMappingDTO.budgetOfficeId;
        }
        budgetRegisterDTO.economicSubCodeId = PRIVILEGE_SALARY_SUB_CODE_ID;

        budgetRegisterDTO.insertionTime
                = budgetRegisterDTO.lastModificationTime
                = userInput.modificationTime;
        budgetRegisterDTO.insertedBy
                = budgetRegisterDTO.modifiedBy
                = userInput.modifierId;

        Budget_registerDAO.getInstance().add(budgetRegisterDTO);
        return budgetRegisterDTO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Payroll_month_billServlet.UserInput userInput = new Payroll_month_billServlet.UserInput();
        userInput.budgetOfficeId = Long.parseLong(request.getParameter("budgetOfficeId"));
        userInput.monthYear = new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("monthYear")).getTime();
        int employmentCat = EmploymentEnum.PRIVILEGED.getValue();
        List<Tax_deduction_configurationDTO> deduction_configurationDTOs = Tax_deduction_configurationRepository.
                getInstance().getActiveDTOs(employmentCat);

        if (addFlag) {
            List<Payroll_month_billDTO> dtoList = Payroll_month_billDAO.getInstance().getByBudgetOfficeAndMonthAndEmployment(
                    userInput.budgetOfficeId, userInput.monthYear, employmentCat
            );
            boolean isAlreadyAdded = dtoList != null && !dtoList.isEmpty();
            if (!isAlreadyAdded) {
                long curTime = System.currentTimeMillis();
                userInput.modificationTime = System.currentTimeMillis();
                String[] employeeRecordsIds = request.getParameterValues("employeeRecordsId");
                if (employeeRecordsIds.length <= 1 && Long.parseLong(employeeRecordsIds[0]) < 0) {
                    throw new Exception(isLangEng ? "No Employee Found" : "কোন কর্মকর্তা পাওয়া যায়নি");
                }
                Map<Long, List<String>> deductionAmountMap = new HashMap<>();
                List<String> houseAddressList = new ArrayList<>();
                for (Tax_deduction_configurationDTO dto : deduction_configurationDTOs) {
                    if (request.getParameterValues("deduction_" + dto.iD) != null) {
                        List<String> tmp = Arrays.asList(request.getParameterValues("deduction_" + dto.iD));
                        deductionAmountMap.put(dto.iD, tmp);
                    }
                    if (dto.iD == Tax_deduction_configurationRepository.getInstance().getHouseRentDeductionId()) {
                        houseAddressList = Arrays.asList(request.getParameterValues("deduction_" + dto.iD + "_address"));
                    }
                }
                userInput.modifierId = userDTO.ID;
                Budget_registerDTO addedBudgetRegisterDTO = addBudgetRegisterDTO(userInput);
                long budgetRegisterId = addedBudgetRegisterDTO.iD;
                Bill_registerDTO addedBillRegisterDTO = Bill_registerServlet.addBillRegister(addedBudgetRegisterDTO, CASH_TYPE, true);
                long billRegisterId = addedBillRegisterDTO.iD;
                int index = 0;
                long totalAmount = 0;
                for (String employeeRecordsId : employeeRecordsIds) {
                    userInput.employeeRecordsId = Long.parseLong(employeeRecordsId);
                    userInput.budgetRegisterId = budgetRegisterId;
                    userInput.billRegisterId = billRegisterId;
                    userInput.voucherNumber = String.valueOf(budgetRegisterId);
                    if (userInput.employeeRecordsId < 0) continue;
                    Payroll_month_billDTO returnDTO = addMonthPayroll(userInput, employmentCat, deductionAmountMap, index, curTime, houseAddressList);
                    if (returnDTO != null) {
                        totalAmount += returnDTO.netAmount;
                    }
                    index++;
                }
                userInput.employeeCount = index;
                addedBudgetRegisterDTO.billAmount = addedBillRegisterDTO.billAmount = totalAmount;
                addedBudgetRegisterDTO.description = addedBillRegisterDTO.description = getBillDescription(userInput);
                Budget_registerDAO.getInstance().update(addedBudgetRegisterDTO);
                Bill_registerDAO.getInstance().update(addedBillRegisterDTO);
            }
        }
        Payroll_month_billDTO month_billDTO = new Payroll_month_billDTO();
        month_billDTO.monthYear = userInput.monthYear;
        month_billDTO.budgetOfficeId = userInput.budgetOfficeId;
        return month_billDTO;
    }

    static class UserInput {
        public String voucherNumber;
        public long modifierId;
        public long budgetOfficeId;
        public long budgetRegisterId;
        public long billRegisterId;
        public long monthYear;
        public long id;
        public long employeeRecordsId;
        public int employeeCount;
        long modificationTime;
    }

    public static List<Payroll_month_bill_allowanceDTO> getAllowanceDTOList(String organogramKey, int employmentCat) {
        List<Payroll_month_bill_allowanceDTO> allowanceDTOList = new ArrayList<>();
        List<Payroll_allowance_lookupDTO> allowance_lookupDTOS = Payroll_allowance_lookupRepository.getInstance().getDTOByOrganogramKey(employmentCat, organogramKey);
        allowance_lookupDTOS.forEach(dto -> {
            Payroll_month_bill_allowanceDTO tmpDTO = new Payroll_month_bill_allowanceDTO();
            tmpDTO.payrollAllowanceConfigurationId = dto.payrollAllowanceConfigId;
            tmpDTO.amount = (int) dto.amount;
            allowanceDTOList.add(tmpDTO);
        });
        return allowanceDTOList;
    }

    public static List<Payroll_month_bill_deductionDTO> getDeductionDTOList(String organogramKey, int employmentCat) {
        List<Payroll_month_bill_deductionDTO> deductionDTOList = new ArrayList<>();
        List<Payroll_deduction_lookupDTO> deduction_lookupDTOS = Payroll_deduction_lookupRepository.getInstance().getDTOByOrganogramKey(employmentCat, organogramKey);
        deduction_lookupDTOS.forEach(dto -> {
            Payroll_month_bill_deductionDTO tmpDTO = new Payroll_month_bill_deductionDTO();
            tmpDTO.taxDeductionConfigurationId = dto.taxDeductionConfigId;
            tmpDTO.amount = (int) dto.amount;
            deductionDTOList.add(tmpDTO);
        });
        return deductionDTOList;
    }
}

