package payroll_month_bill;

import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import payroll_month_bill_allowance.Payroll_month_bill_allowanceDAO;
import payroll_month_bill_allowance.Payroll_month_bill_allowanceDTO;
import payroll_month_bill_deduction.Payroll_month_bill_deductionDAO;
import payroll_month_bill_deduction.Payroll_month_bill_deductionDTO;
import util.StringUtils;

import java.util.List;

import static util.UtilCharacter.getDataByLanguage;

@SuppressWarnings({"Duplicates"})
public class Payroll_month_billModel {
    public String employeeRecordsId = "-1";
    public String payrollMonthBillId = "-1";
    public String name = "";
    public String officeName = "";
    public String organogramName = "";
    public String nid = "";
    public String mobileNumber = "";
    public String savingAccountNumber = "";
    public List<Payroll_month_bill_allowanceDTO> allowanceDTOList;
    public List<Payroll_month_bill_deductionDTO> deductionDTOList;
    public String totalAmount = "";
    public String netAmount = "";
    public String organogramKey = "";
    public String employmentCat = "";

    @SuppressWarnings({"unused"})
    public Payroll_month_billModel(AllowanceEmployeeInfoDTO employeeInfoDTO, long monthYear, int employmentCat, String language) {
        setEmployeeInfo(this, employeeInfoDTO, language);
        this.employmentCat = String.valueOf(employmentCat);
        allowanceDTOList = Payroll_month_billServlet.getAllowanceDTOList(organogramKey, employmentCat);
        deductionDTOList = Payroll_month_billServlet.getDeductionDTOList(organogramKey, employmentCat);
        int totalAmountInt = allowanceDTOList.stream().mapToInt(dto -> dto.amount).sum();
        totalAmount = String.valueOf(totalAmountInt);
        int deduct = deductionDTOList.stream().mapToInt(dto -> dto.amount).sum();
        netAmount = String.valueOf(totalAmountInt - deduct);
    }

    public Payroll_month_billModel(Payroll_month_billDTO billDTO, String language) {
        AllowanceEmployeeInfoDTO allowanceEmployeeInfoDTO = AllowanceEmployeeInfoRepository.getInstance().getById(billDTO.allowanceEmployeeInfoId);
        setEmployeeInfo(this, allowanceEmployeeInfoDTO, language);
        setAllowanceInfo(this, billDTO);
    }

    private static void setEmployeeInfo(Payroll_month_billModel model, AllowanceEmployeeInfoDTO employeeInfoDTO, String language) {
        model.employeeRecordsId = String.valueOf(employeeInfoDTO.employeeRecordId);
        model.name = getDataByLanguage(language, employeeInfoDTO.nameBn, employeeInfoDTO.nameEn);
        model.officeName = getDataByLanguage(language, employeeInfoDTO.officeNameBn, employeeInfoDTO.officeNameEn);
        model.organogramName = getDataByLanguage(language, employeeInfoDTO.organogramNameBn, employeeInfoDTO.organogramNameEn);
        model.nid = StringUtils.convertBanglaIfLanguageIsBangla(language, employeeInfoDTO.nid);
        model.mobileNumber = StringUtils.convertBanglaIfLanguageIsBangla(language, employeeInfoDTO.mobileNumber);
        model.savingAccountNumber = StringUtils.convertBanglaIfLanguageIsBangla(language, employeeInfoDTO.savingAccountNumber);
        model.organogramKey = employeeInfoDTO.organogramNameEn.toLowerCase().trim();
    }

    private static void setAllowanceInfo(Payroll_month_billModel model, Payroll_month_billDTO billDTO) {
        model.payrollMonthBillId = String.valueOf(billDTO.iD);
        model.allowanceDTOList = Payroll_month_bill_allowanceDAO.getInstance().getByByMonthBillId(billDTO.iD);
        model.deductionDTOList = Payroll_month_bill_deductionDAO.getInstance().getByByMonthBillId(billDTO.iD);
        int totalAmountInt = model.allowanceDTOList.stream().mapToInt(dto -> dto.amount).sum();
        model.totalAmount = totalAmountInt < 0 ? "" : String.valueOf(totalAmountInt);
        int deduct = model.deductionDTOList.stream().mapToInt(dto -> dto.amount).sum();
        int netAmountInt = totalAmountInt - deduct;
        model.netAmount = netAmountInt < 0 ? "" : String.valueOf(netAmountInt);
    }
}
