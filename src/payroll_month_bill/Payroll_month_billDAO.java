package payroll_month_bill;

import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import common.CommonDAOService;
import dbm.DBMW;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static util.StringUtils.convertToBanNumber;

@SuppressWarnings({"Duplicates"})
public class Payroll_month_billDAO implements CommonDAOService<Payroll_month_billDTO> {

    private static final Logger logger = Logger.getLogger(Payroll_month_billDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (budget_office_id,budget_register_id,bill_register_id,month_year,allowance_employee_info_id,employment_cat,organogram_key,voucher_number,net_amount,"
            .concat("modified_by,lastModificationTime,search_column,")
            .concat("inserted_by,insertion_time,isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery = "UPDATE {tableName} SET budget_office_id=?,budget_register_id=?,bill_register_id=?,month_year=?,allowance_employee_info_id=?,employment_cat=?,"
            .concat("organogram_key=?,voucher_number=?,net_amount=?,modified_by=?,")
            .concat("lastModificationTime=?,search_column=? WHERE ID=?");

    private static final String getByBudgetOfficeAndMonthAndEmployment = "SELECT * FROM payroll_month_bill WHERE budget_office_id=%d AND month_year=%d AND employment_cat=%d AND isDeleted=0";

    private final Map<String, String> searchMap = new HashMap<>();

    private Payroll_month_billDAO() {
        searchMap.put("organogram_key", " and (organogram_key = ?)");
        searchMap.put("budgetOfficeId", " AND (budget_office_id = ?) ");
        searchMap.put("monthYear", " AND (month_year = ?) ");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private static class LazyLoader {
        static final Payroll_month_billDAO INSTANCE = new Payroll_month_billDAO();
    }

    public static Payroll_month_billDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Payroll_month_billDTO payroll_month_billDTO) {
        AllowanceEmployeeInfoDTO allowanceEmployeeInfoDTO =
                AllowanceEmployeeInfoRepository.getInstance().getById(payroll_month_billDTO.allowanceEmployeeInfoId);
        payroll_month_billDTO.searchColumn =
                allowanceEmployeeInfoDTO.nameEn + " " + allowanceEmployeeInfoDTO.nameBn + " "
                        + allowanceEmployeeInfoDTO.officeNameEn + " "
                        + allowanceEmployeeInfoDTO.officeNameBn + " "
                        + allowanceEmployeeInfoDTO.organogramNameEn + " "
                        + allowanceEmployeeInfoDTO.organogramNameBn + " "
                        + allowanceEmployeeInfoDTO.mobileNumber + " "
                        + convertToBanNumber(allowanceEmployeeInfoDTO.mobileNumber) + " "
                        + allowanceEmployeeInfoDTO.savingAccountNumber + " "
                        + convertToBanNumber(allowanceEmployeeInfoDTO.savingAccountNumber);
    }

    @Override
    public void set(PreparedStatement ps, Payroll_month_billDTO dto, boolean isInsert) throws SQLException {
        setSearchColumn(dto);
        int index = 0;
        ps.setLong(++index, dto.budgetOfficeId);
        ps.setLong(++index, dto.budgetRegisterId);
        ps.setLong(++index, dto.billRegisterId);
        ps.setLong(++index, dto.monthYear);
        ps.setLong(++index, dto.allowanceEmployeeInfoId);
        ps.setLong(++index, dto.employmentCat);
        ps.setString(++index, dto.organogramKey);
        ps.setString(++index, dto.voucherNumber);
        ps.setLong(++index, dto.netAmount);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        ps.setObject(++index, dto.searchColumn);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Payroll_month_billDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Payroll_month_billDTO dto = new Payroll_month_billDTO();
            dto.iD = rs.getLong("ID");
            dto.budgetOfficeId = rs.getLong("budget_office_id");
            dto.budgetRegisterId = rs.getLong("budget_register_id");
            dto.billRegisterId = rs.getLong("bill_register_id");
            dto.monthYear = rs.getLong("month_year");
            dto.allowanceEmployeeInfoId = rs.getLong("allowance_employee_info_id");
            dto.employmentCat = rs.getInt("employment_cat");
            dto.organogramKey = rs.getString("organogram_key");
            dto.voucherNumber = rs.getString("voucher_number");
            dto.netAmount = rs.getLong("net_amount");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.searchColumn = rs.getString("search_column");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Payroll_month_billDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "payroll_month_bill";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Payroll_month_billDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Payroll_month_billDTO) commonDTO, updateQuery, false);
    }

    public String getNextVoucherNumber() throws Exception {
        long nextId = DBMW.getInstance().getNextSequenceId(getTableName());
        return "PMB-" + nextId;
    }

    public List<Payroll_month_billDTO> getByBudgetOfficeAndMonthAndEmployment(long budgetOfficeId, long monthYear, int employmentCat) {
        return getDTOs(String.format(
                getByBudgetOfficeAndMonthAndEmployment, budgetOfficeId, monthYear, employmentCat
        ));
    }

    private static final String getByOfficeAndTimeRange =
            "SELECT * FROM payroll_month_bill WHERE budget_office_id = %d AND (month_year >= %d AND month_year < %d)";

    public long getUptoLastBillTotal(long budgetOfficeId, long startMonthYearInclusive, long endMonthYearExclusive) {
        List<Payroll_month_billDTO> previousBillDTOs = getDTOs(String.format(
                getByOfficeAndTimeRange, budgetOfficeId, startMonthYearInclusive, endMonthYearExclusive
        ));
        if (previousBillDTOs == null) return 0;
        return previousBillDTOs.stream()
                .mapToLong(dto -> dto.netAmount)
                .sum();
    }
}