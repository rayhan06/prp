package payroll_month_bill;

import util.CommonDTO;


public class Payroll_month_billDTO extends CommonDTO {
    public long allowanceEmployeeInfoId = -1;
    public long monthYear = -1;
    public long budgetOfficeId = -1;
    public long budgetRegisterId = -1;
    public long billRegisterId = -1;
    public int employmentCat = -1;
    public String organogramKey = "";
    public String voucherNumber = "";
    public long totalAmount = 0;
    public long netAmount = 0;
    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;


    @Override
    public String toString() {
        return "$Payroll_month_billDTO[" +
                " iD = " + iD +
                " allowanceEmployeeInfoId = " + allowanceEmployeeInfoId +
                " monthYear = " + monthYear +
                " budgetOfficeId = " + budgetOfficeId +
                " employmentCat = " + employmentCat +
                " organogramKey = " + organogramKey +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " isDeleted = " + isDeleted +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }
}