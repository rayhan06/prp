package employee_leave_details;
import util.*;

public class Employee_leave_detailsApprovalMAPS extends CommonMaps
{	
	public Employee_leave_detailsApprovalMAPS(String tableName)
	{
		
		java_allfield_type_map.put("employee_records_id".toLowerCase(), "Long");
		java_allfield_type_map.put("leave_type_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("leave_start_date".toLowerCase(), "Long");
		java_allfield_type_map.put("leave_end_date".toLowerCase(), "Long");
		java_allfield_type_map.put("leave_reliever_id".toLowerCase(), "Long");
		java_allfield_type_map.put("contact_number_during_leave".toLowerCase(), "String");

		java_allfield_type_map.put("job_cat", "Integer");
		java_allfield_type_map.put("approval_status_cat", "Integer");
		java_allfield_type_map.put("initiator", "Long");
		java_allfield_type_map.put("assigned_to", "Long");
		java_allfield_type_map.put("starting_date", "Long");
		java_allfield_type_map.put("ending_date", "Long");
		
		java_table_map.put("approval_status_cat", "approval_summary");
		java_table_map.put("initiator", "approval_summary");
		java_table_map.put("assigned_to", "approval_summary");
	}

}