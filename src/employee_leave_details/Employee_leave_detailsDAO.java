package employee_leave_details;

import common.ConnectionAndStatementUtil;
import common.EmployeeCommonDAOService;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_pay_scale.Employee_pay_scaleDTO;
import employee_pay_scale.Employee_pay_scaleRepository;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import sessionmanager.SessionConstants;
import util.CommonDTO;
import util.CommonMaps;
import util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@SuppressWarnings({"rawtypes", "unused", "Duplicates"})
public class Employee_leave_detailsDAO implements EmployeeCommonDAOService<Employee_leave_detailsDTO> {

    private static final Logger logger = Logger.getLogger(Employee_leave_detailsDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (employee_records_id,leave_type_cat,leave_start_date,leave_end_date,joining_date_after_leave,comments,leave_reliever_id,"
            + " contact_number_during_leave,contact_address,job_cat,is_approved,approval_date,files_dropzone,insertion_date,inserted_by,"
            + " modified_by,search_column,employee_name_eng, employee_name_bng, office_unit_id, office_eng, office_bng, organogram_id, organogram_eng, organogram_bng,"
            + "lastModificationTime,last_leave_id,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static final String updateSqlQuery =
            "UPDATE {tableName} SET employee_records_id=?,leave_type_cat=?,leave_start_date=?,"
            + "leave_end_date=?,joining_date_after_leave=?,comments=?,leave_reliever_id=?,contact_number_during_leave=?,contact_address=?,"
            + "job_cat=?,is_approved=?,approval_date=?,files_dropzone=?,insertion_date=?,inserted_by=?,"
            + "modified_by=?,search_column=?,employee_name_eng=?, employee_name_bng=?, office_unit_id=?, office_eng=?, office_bng=?, organogram_id=?, organogram_eng=?, organogram_bng=?,"
            + "lastModificationTime = ?,last_leave_id=? WHERE ID = ?";
    private static final String sqlGetLastLeaveDTO =
            "SELECT * FROM employee_leave_details where employee_records_id = ? AND isDeleted = 0 order by lastModificationTime desc limit 1";

    private static final String getRelieverLeaveCount =
            "SELECT Count(*) as countId FROM %s WHERE employee_records_id=? " +
            "AND leave_start_date>=? " +
            "AND leave_end_date<=? AND isDeleted=0";

    private static final String getRelieverLeave =
            "SELECT * FROM employee_leave_details WHERE employee_records_id=%d " +
            "AND leave_end_date>=%d AND isDeleted=0";
    private static final String getByEmployeeRecordIdAndLeaveStartDate =
            "SELECT * FROM employee_leave_details WHERE employee_records_id = ? " +
            "AND leave_start_date>=? AND isDeleted = 0";

    public CommonMaps approvalMaps = new Employee_leave_detailsApprovalMAPS("employee_leave_details");

    private static final Map<String, String> searchMap = new HashMap<>();

    private Employee_leave_detailsDAO() {
        searchMap.put("leave_type_cat", " and (leave_type_cat = ?)");
        searchMap.put("leave_start_date", " and (leave_start_date >= ?)");
        searchMap.put("leave_end_date", " and (leave_end_date <= ?)");
        searchMap.put("employee_records_id", " and (employee_records_id = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private static class LazyLoader {
        static final Employee_leave_detailsDAO INSTANCE = new Employee_leave_detailsDAO();
    }

    public static Employee_leave_detailsDAO getInstance() {
        return Employee_leave_detailsDAO.LazyLoader.INSTANCE;
    }

    public void set(PreparedStatement ps, Employee_leave_detailsDTO employee_leave_detailsDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(employee_leave_detailsDTO);
        ps.setObject(++index, employee_leave_detailsDTO.employeeRecordsId);
        ps.setObject(++index, employee_leave_detailsDTO.leaveTypeCat);
        ps.setObject(++index, employee_leave_detailsDTO.leaveStartDate);
        ps.setObject(++index, employee_leave_detailsDTO.leaveEndDate);
        ps.setObject(++index, employee_leave_detailsDTO.joiningDateAfterLeave);
        ps.setObject(++index, employee_leave_detailsDTO.comments);
        ps.setObject(++index, employee_leave_detailsDTO.leaveRelieverId);
        ps.setObject(++index, employee_leave_detailsDTO.contactNumberDuringLeave);
        ps.setObject(++index, employee_leave_detailsDTO.contactAddress);
        ps.setObject(++index, employee_leave_detailsDTO.jobCat);
        ps.setObject(++index, employee_leave_detailsDTO.isApproved);
        ps.setObject(++index, employee_leave_detailsDTO.approvalDate);
        ps.setObject(++index, employee_leave_detailsDTO.filesDropzone);
        ps.setObject(++index, employee_leave_detailsDTO.insertionDate);
        ps.setObject(++index, employee_leave_detailsDTO.insertedBy);
        ps.setObject(++index, employee_leave_detailsDTO.modifiedBy);
        ps.setObject(++index, employee_leave_detailsDTO.searchColumn);
        ps.setObject(++index, employee_leave_detailsDTO.employeeNameEng);
        ps.setObject(++index, employee_leave_detailsDTO.employeeNameBng);
        ps.setObject(++index, employee_leave_detailsDTO.officeUnitId);
        ps.setObject(++index, employee_leave_detailsDTO.officeEng);
        ps.setObject(++index, employee_leave_detailsDTO.officeBng);
        ps.setObject(++index, employee_leave_detailsDTO.organogramId);
        ps.setObject(++index, employee_leave_detailsDTO.organogramEng);
        ps.setObject(++index, employee_leave_detailsDTO.organogramBng);
        ps.setObject(++index, System.currentTimeMillis());
        ps.setObject(++index, employee_leave_detailsDTO.lastLeaveId);
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, employee_leave_detailsDTO.iD);
    }

    public void setSearchColumn(Employee_leave_detailsDTO employee_leave_detailsDTO) {
        List<String> list = new ArrayList<>();
        try {
            Employee_recordsDTO employeeRecordsDTO = new Employee_recordsDAO().getEmployee_recordsDTOByID(employee_leave_detailsDTO.employeeRecordsId);
            if (employeeRecordsDTO != null) {
                if (employeeRecordsDTO.nameEng != null && employeeRecordsDTO.nameEng.trim().length() > 0) {
                    list.add(employeeRecordsDTO.nameEng.trim());
                }
                if (employeeRecordsDTO.nameBng != null && employeeRecordsDTO.nameBng.trim().length() > 0) {
                    list.add(employeeRecordsDTO.nameBng.trim());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        CategoryLanguageModel languageModel = CatRepository.getInstance().getCategoryLanguageModel("leave_type", employee_leave_detailsDTO.leaveTypeCat);
        if (languageModel != null) {
            list.add(languageModel.englishText);
            list.add(languageModel.banglaText);
        }
        list.add(Employee_recordsDAO.getEmployeeName(employee_leave_detailsDTO.leaveRelieverId, "English"));
        list.add(Employee_recordsDAO.getEmployeeName(employee_leave_detailsDTO.leaveRelieverId, "Bangla"));

        employee_leave_detailsDTO.searchColumn = String.join(" ", list);
    }

    @Override
    public Employee_leave_detailsDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Employee_leave_detailsDTO employee_leave_detailsDTO = new Employee_leave_detailsDTO();
            employee_leave_detailsDTO.iD = rs.getLong("ID");
            employee_leave_detailsDTO.employeeRecordsId = rs.getLong("employee_records_id");
            employee_leave_detailsDTO.leaveTypeCat = rs.getInt("leave_type_cat");
            employee_leave_detailsDTO.leaveStartDate = rs.getLong("leave_start_date");
            employee_leave_detailsDTO.leaveEndDate = rs.getLong("leave_end_date");
            employee_leave_detailsDTO.joiningDateAfterLeave = rs.getLong("joining_date_after_leave");
            employee_leave_detailsDTO.comments = rs.getString("comments");
            employee_leave_detailsDTO.leaveRelieverId = rs.getLong("leave_reliever_id");
            employee_leave_detailsDTO.contactNumberDuringLeave = rs.getString("contact_number_during_leave");
            employee_leave_detailsDTO.contactAddress = rs.getString("contact_address");
            employee_leave_detailsDTO.jobCat = rs.getInt("job_cat");
            employee_leave_detailsDTO.isApproved = rs.getBoolean("is_approved");
            employee_leave_detailsDTO.approvalDate = rs.getLong("approval_date");
            employee_leave_detailsDTO.filesDropzone = rs.getLong("files_dropzone");
            employee_leave_detailsDTO.insertionDate = rs.getLong("insertion_date");
            employee_leave_detailsDTO.insertedBy = rs.getString("inserted_by");
            employee_leave_detailsDTO.modifiedBy = rs.getString("modified_by");
            employee_leave_detailsDTO.searchColumn = rs.getString("search_column");
            employee_leave_detailsDTO.employeeNameEng = rs.getString("employee_name_eng");
            employee_leave_detailsDTO.employeeNameBng = rs.getString("employee_name_bng");
            employee_leave_detailsDTO.officeUnitId = rs.getInt("office_unit_id");
            employee_leave_detailsDTO.officeEng = rs.getString("office_eng");
            employee_leave_detailsDTO.officeBng = rs.getString("office_bng");
            employee_leave_detailsDTO.organogramId = rs.getInt("organogram_id");
            employee_leave_detailsDTO.organogramEng = rs.getString("organogram_eng");
            employee_leave_detailsDTO.organogramBng = rs.getString("organogram_bng");
            employee_leave_detailsDTO.isDeleted = rs.getInt("isDeleted");
            employee_leave_detailsDTO.lastModificationTime = rs.getLong("lastModificationTime");
            employee_leave_detailsDTO.lastLeaveId = rs.getLong("last_leave_id");
            return employee_leave_detailsDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "employee_leave_details";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_leave_detailsDTO) commonDTO, addSqlQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_leave_detailsDTO) commonDTO, updateSqlQuery, false);
    }

    public CommonDTO getDTOByID(long ID) throws Exception {
        return getDTOFromID(ID);
    }

    public List<Employee_leave_detailsDTO> getAllEmployee_leave_details(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }

    public Employee_leave_detailsDTO getLastLeaveDTO(long employeeRecordId) {
        List<Employee_leave_detailsDTO> list = getDTOs(sqlGetLastLeaveDTO, ps -> {
            try {
                ps.setLong(1, employeeRecordId);
            } catch (SQLException e) {
                logger.error(e);
            }
        });
        return list.size() == 0 ? null : list.get(0);
    }

    public EmpLeaveDetails getEmpLeaveDetails(long id) {
        Employee_leave_detailsDTO dto = getDTOFromID(id);
        EmpLeaveDetails leaveDetails = buildEmpLeaveDetails(dto);
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(dto.employeeRecordsId);
        if (employeeRecordsDTO != null) {
            leaveDetails.empNameEng = employeeRecordsDTO.nameEng;
            leaveDetails.empNameBng = employeeRecordsDTO.nameBng;
            leaveDetails.empClass = employeeRecordsDTO.employeeClass;
        }
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getFromCacheByEmployeeRecordIdIsDefault(dto.employeeRecordsId);
        if (employeeOfficeDTO != null) {
            if (employeeOfficeDTO.officeUnitOrganogramId != null) {
                OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
                if (officeUnitOrganograms != null) {
                    leaveDetails.designationEng = officeUnitOrganograms.designation_eng;
                    leaveDetails.designationBng = officeUnitOrganograms.designation_bng;
                    Employee_pay_scaleDTO employeePayScaleDTO = Employee_pay_scaleRepository.getInstance().getByJobGradeCat(officeUnitOrganograms.job_grade_type_cat);
                    if (employeePayScaleDTO != null) {
                        CategoryLanguageModel categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel("national_pay_scale_type", employeePayScaleDTO.employeeClassCat);
                        if (categoryLanguageModel != null) {
                            leaveDetails.basicPayInEng = categoryLanguageModel.englishText;
                            leaveDetails.basicPayInBng = categoryLanguageModel.banglaText;
                        }
                    }
                }
            }
        }
        if (dto.lastLeaveId != -1) {
            Employee_leave_detailsDTO lastLeaveDTO = getDTOFromID(dto.lastLeaveId);
            leaveDetails.lastLeaveDetails = buildEmpLeaveDetails(lastLeaveDTO);
        } else {
            leaveDetails.lastLeaveDetails = new EmpLeaveDetails();
        }
        return leaveDetails;
    }

    private EmpLeaveDetails buildEmpLeaveDetails(Employee_leave_detailsDTO dto) {
        EmpLeaveDetails empLeaveDetails = new EmpLeaveDetails();
        if (dto != null) {
            empLeaveDetails.dto = dto;
            empLeaveDetails.employeeRecordsId = dto.employeeRecordsId;
            CategoryLanguageModel languageModel = CatRepository.getInstance().getCategoryLanguageModel("leave_type", dto.leaveTypeCat);
            if (languageModel != null) {
                empLeaveDetails.leaveTypeEng = languageModel.englishText;
                empLeaveDetails.leaveTypeBan = languageModel.banglaText;
            }
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
            empLeaveDetails.leaveStartDateEng = dto.leaveStartDate > SessionConstants.MIN_DATE ? simpleDateFormat.format(new Date(dto.leaveStartDate)) : "";
            empLeaveDetails.leaveStartDateBan = !empLeaveDetails.leaveStartDateEng.equals("") ? StringUtils.convertToBanNumber(empLeaveDetails.leaveStartDateEng) : "";
            empLeaveDetails.leaveEndDateEng = dto.leaveEndDate > SessionConstants.MIN_DATE ? simpleDateFormat.format(new Date(dto.leaveEndDate)) : "";
            empLeaveDetails.leaveEndDateBan = !empLeaveDetails.leaveEndDateEng.equals("") ? StringUtils.convertToBanNumber(empLeaveDetails.leaveEndDateEng) : "";
            empLeaveDetails.joiningDateAfterLeaveEng = dto.joiningDateAfterLeave > SessionConstants.MIN_DATE ? simpleDateFormat.format(new Date(dto.joiningDateAfterLeave)) : "";
            empLeaveDetails.joiningDateAfterLeaveBan = !empLeaveDetails.joiningDateAfterLeaveEng.equals("") ? StringUtils.convertToBanNumber(empLeaveDetails.joiningDateAfterLeaveEng) : "";
            empLeaveDetails.leaveCountDaysEng = calculateDateDifferenceInDays(dto.leaveStartDate, dto.leaveEndDate);
            empLeaveDetails.leaveCountDaysBan = StringUtils.convertToBanNumber(empLeaveDetails.leaveCountDaysEng);
            empLeaveDetails.comments = dto.comments;
            empLeaveDetails.leaveRelieverName = Employee_recordsRepository.getInstance().getEmployeeName(dto.leaveRelieverId, "ENGLISH");
            empLeaveDetails.leaveRelieverNameBn = Employee_recordsRepository.getInstance().getEmployeeName(dto.leaveRelieverId, "BANGLA");
            empLeaveDetails.contactNumberDuringLeave = dto.contactNumberDuringLeave;
            empLeaveDetails.contactAddress = dto.contactAddress;
            empLeaveDetails.isApproved = dto.isApproved;
            if (dto.approvalDate > 0) {
                empLeaveDetails.approvalDateEng = simpleDateFormat.format(new Date(dto.approvalDate));
                empLeaveDetails.approvalDateBan = StringUtils.convertToBanNumber(empLeaveDetails.approvalDateEng);
            }
            empLeaveDetails.filesDropzone = dto.filesDropzone;
            empLeaveDetails.jobCat = dto.jobCat;
        }
        return empLeaveDetails;
    }

    private String calculateDateDifferenceInDays(long startDateValue, long endDateValue) {
        Date startDate = new Date(startDateValue);
        Date endDate = new Date(endDateValue);
        long diffInMillies = Math.abs(endDate.getTime() - startDate.getTime());
        return String.valueOf(TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS) + 1);
    }

    private String parseContactAddress(String contactAddress) {
        String contactAddressDetails = "";
        if (contactAddress != null && !contactAddress.equals("")) {
            String[] addressToken = contactAddress.split(":");
            if (addressToken.length > 0) {
                if (addressToken.length > 2) {
                    contactAddressDetails = addressToken[2];
                }
                if (addressToken.length > 1) {
                    List<String> tokens = Arrays.asList(addressToken[1].split(","));
                    Collections.reverse(tokens);
                    contactAddressDetails = tokens.stream()
                                                  .collect(Collectors.joining(",", contactAddressDetails, ""));
                }
            }
        }
        return contactAddressDetails;
    }

    public List<EmpLeaveDetails> getEmpLeaveDetailsByEmployeeId(long employeeId) {
        return getByEmployeeId(employeeId)
                .stream()
                .map(this::buildEmpLeaveDetails)
                .collect(Collectors.toList());
    }

    public List<Employee_leave_detailsDTO> getLeaveDetaiForReleiver(long employeeRecordsId) {
        long currentTime = System.currentTimeMillis();
        return getDTOs(String.format(getRelieverLeave, employeeRecordsId, currentTime));
    }

    public List<Employee_leave_detailsDTO> getLeaveDetailsByEmployeeInRange(long startDate, long endDate, long employeeRecordsId) {
        String sql = "SELECT * FROM " + getTableName() + " WHERE employee_records_id = " + employeeRecordsId + " AND ((leave_start_date >= " + startDate + " AND leave_start_date <= " + endDate + " ) OR (leave_end_date >= " + startDate + " AND leave_end_date <= " + endDate + "))";
        return getDTOs(sql);
    }


    public boolean isRelieverAvailable(long organogramId, long startDate, long endDate) {
        String getCountSql = String.format(getRelieverLeaveCount, getTableName());
        int count = ConnectionAndStatementUtil.getT(getCountSql, Arrays.asList(organogramId, startDate, endDate), rs -> {
            try {
                return Integer.valueOf(rs.getString("countId"));
            } catch (SQLException ex) {
                ex.printStackTrace();
                return -1;
            }
        });
        return count == 0;
    }

    public Map<String, Integer> getCountByLeaveType(long employeeRecordId, long timeFrom, String language) {
        return ConnectionAndStatementUtil
                .getListOfT(getByEmployeeRecordIdAndLeaveStartDate, Arrays.asList(employeeRecordId, timeFrom), this::buildObjectFromResultSet)
                .stream()
                .filter(dto -> dto.employeeRecordsId == employeeRecordId && dto.leaveStartDate >= timeFrom)
                .collect(Collectors.groupingBy(dto -> dto.leaveTypeCat, Collectors.collectingAndThen(Collectors.counting(), Long::intValue)))
                .entrySet()
                .stream()
                .filter(e -> CatRepository.getInstance().getCategoryLanguageModel("leave_type", e.getKey()) != null)
                .collect(Collectors.toMap(e -> CatRepository.getInstance().getText(language, "leave_type", e.getKey()), Map.Entry::getValue));
    }

    private static final String findApprovedLeaveByEmployeeInRangeQuery
            = "SELECT * FROM employee_leave_details WHERE " +
              "employee_records_id = ? " +
              "AND NOT ((leave_end_date < ?) OR (? <= leave_start_date)) " +
              "AND is_approved = 1 " +
              "AND isDeleted = 0";

    public List<Employee_leave_detailsDTO> findApprovedLeavesByEmployeeInRange(long employeeRecordId,
                                                                               long startInclusive,
                                                                               long endExclusive) {
        return ConnectionAndStatementUtil.getListOfT(
                findApprovedLeaveByEmployeeInRangeQuery,
                Arrays.asList(employeeRecordId, startInclusive, endExclusive),
                this::buildObjectFromResultSet
        );
    }
}