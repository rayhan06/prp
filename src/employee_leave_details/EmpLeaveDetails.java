package employee_leave_details;

public class EmpLeaveDetails {
	public Employee_leave_detailsDTO dto;
	public String empNameEng="";
	public String empNameBng="";
	public int empClass=0;
	public String basicPayInEng="";
	public String basicPayInBng="";
	public String designationBng="";
	public String designationEng="";
	public long employeeRecordsId=0;
	public String leaveTypeEng ="";
	public String leaveTypeBan="";
	public String leaveStartDateEng="";
	public String leaveStartDateBan="";
	public String leaveEndDateEng="";
	public String leaveEndDateBan="";
	public String joiningDateAfterLeaveEng="";
	public String joiningDateAfterLeaveBan="";
	public String leaveCountDaysEng = "";
	public String leaveCountDaysBan = "";
	public String comments = "";
	public String leaveRelieverName = "";
	public String leaveRelieverNameBn = "";
	public String contactNumberDuringLeave = "";
	public String contactAddress = "";
	public boolean isApproved = false;
	public String approvalDateEng = "";
	public String approvalDateBan = "";
	public long filesDropzone = 0;
	public int jobCat = -1;
	
	public EmpLeaveDetails lastLeaveDetails;

	@Override
	public String toString() {
		return "EmpLeaveDetails{" +
				"dto=" + dto +
				", empNameEng='" + empNameEng + '\'' +
				", empNameBng='" + empNameBng + '\'' +
				", empClass=" + empClass +
				", employeeRecordsId=" + employeeRecordsId +
				", leaveTypeEng='" + leaveTypeEng + '\'' +
				", leaveTypeBan='" + leaveTypeBan + '\'' +
				", leaveStartDateEng='" + leaveStartDateEng + '\'' +
				", leaveStartDateBan='" + leaveStartDateBan + '\'' +
				", leaveEndDateEng='" + leaveEndDateEng + '\'' +
				", leaveEndDateBan='" + leaveEndDateBan + '\'' +
				", joiningDateAfterLeaveEng='" + joiningDateAfterLeaveEng + '\'' +
				", joiningDateAfterLeaveBan='" + joiningDateAfterLeaveBan + '\'' +
				", leaveCountDaysEng='" + leaveCountDaysEng + '\'' +
				", leaveCountDaysBan='" + leaveCountDaysBan + '\'' +
				", comments='" + comments + '\'' +
				", leaveRelieverName='" + leaveRelieverName + '\'' +
				", contactNumberDuringLeave='" + contactNumberDuringLeave + '\'' +
				", contactAddress='" + contactAddress + '\'' +
				", isApproved=" + isApproved +
				", approvalDateEng='" + approvalDateEng + '\'' +
				", approvalDateBan='" + approvalDateBan + '\'' +
				", filesDropzone=" + filesDropzone +
				", jobCat=" + jobCat +
				", lastLeaveDetails=" + lastLeaveDetails +
				'}';
	}
}
