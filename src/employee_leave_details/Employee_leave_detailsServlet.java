package employee_leave_details;

import com.google.gson.Gson;
import common.BaseServlet;
import common.CommonDAOService;
import common.EmployeeServletService;
import common.RoleEnum;
import employee_education_info.Employee_education_infoServlet;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.EmployeeFlatInfoDTO;
import employee_records.Employee_recordsRepository;
import language.LC;
import language.LM;
import leave_reliever_mapping.Leave_reliever_mappingDAO;
import leave_reliever_mapping.Leave_reliever_mappingDTO;
import leave_reliever_mapping.Leave_reliever_mappingRepository;
import login.LoginDTO;

import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import pb.CatRepository;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonDTO;

import util.CommonRequestHandler;
import util.HttpRequestUtils;
import util.RecordNavigator;
import util.StringUtils;
import workflow.WorkflowController;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

@WebServlet("/Employee_leave_detailsServlet")
@MultipartConfig
public class Employee_leave_detailsServlet extends BaseServlet implements EmployeeServletService {
    private static final long serialVersionUID = 1L;
    public static final Logger logger = Logger.getLogger(Employee_leave_detailsServlet.class);

    private final static String tableName = "employee_leave_details";

    private final Employee_leave_detailsDAO employee_leave_detailsDAO = Employee_leave_detailsDAO.getInstance();

    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    public String getServletName() {
        return "Employee_leave_detailsServlet";
    }

    @Override
    public Employee_leave_detailsDAO getCommonDAOService() {
        return employee_leave_detailsDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Employee_leave_detailsDTO employee_leave_detailsDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        if (addFlag) {
            employee_leave_detailsDTO = new Employee_leave_detailsDTO();
            employee_leave_detailsDTO.employeeRecordsId = Long.parseLong(request.getParameter("employeeRecordsId"));
            employee_leave_detailsDTO.insertedBy = userDTO.userName;
            employee_leave_detailsDTO.insertionDate = new Date().getTime();
        } else {
            employee_leave_detailsDTO = (Employee_leave_detailsDTO) employee_leave_detailsDAO
                    .getDTOByID(Long.parseLong(request.getParameter("iD")));
        }
        employee_leave_detailsDTO.modifiedBy = userDTO.userName;
        employee_leave_detailsDTO.lastModificationTime = new Date().getTime();
        employee_leave_detailsDTO.leaveTypeCat = Integer.parseInt(Jsoup.clean(request.getParameter("leaveTypeCat"), Whitelist.simpleText()));
        Date d = f.parse(request.getParameter("leaveStartDate"));
        employee_leave_detailsDTO.leaveStartDate = d.getTime();
        d = f.parse(request.getParameter("leaveEndDate"));
        employee_leave_detailsDTO.leaveEndDate = d.getTime();
        d = f.parse(request.getParameter("joiningDateAfterLeave"));
        employee_leave_detailsDTO.joiningDateAfterLeave = d.getTime();
        employee_leave_detailsDTO.comments = Jsoup.clean(request.getParameter("comments"), Whitelist.simpleText()).trim();
        employee_leave_detailsDTO.lastLeaveId = Long.parseLong(Jsoup.clean(request.getParameter("lastLeaveId"), Whitelist.simpleText()));
        employee_leave_detailsDTO.leaveRelieverId = Long.parseLong(Jsoup.clean(request.getParameter("leaveRelieverId"), Whitelist.simpleText()));
        EmployeeFlatInfoDTO employeeFlatInfoDTO = null;
        List<EmployeeOfficeDTO> employeeOfficeDTOList = EmployeeOfficeRepository.getInstance().getByEmployeeRecordId(employee_leave_detailsDTO.employeeRecordsId);
        EmployeeOfficeDTO employeeOfficeDTO = employeeOfficeDTOList.stream()
                .filter(dto -> dto.isDefault == 1)
                .findAny()
                .orElse(null);
        //EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficesDAO.getInstance().getByEmployeeRecordIdIsDefault(employee_leave_detailsDTO.employeeRecordsId);
        //office_unit_organograms officeUnitOrganograms = new Office_unit_organogramsDAO().getOffice_unit_organogramsDTOByID(employeeOfficeDTO.officeUnitOrganogramId);
        if (employeeOfficeDTO != null) {
            OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
            if (officeUnitOrganograms != null) {
                List<Leave_reliever_mappingDTO> leave_reliever_mappingDTOS = Leave_reliever_mappingRepository.getInstance().getLeave_reliever_mappingDTOByorganogram_id(officeUnitOrganograms.id);
                if (leave_reliever_mappingDTOS != null && leave_reliever_mappingDTOS.size() > 0) {
                    Leave_reliever_mappingDTO leave_reliever_mappingDTO = leave_reliever_mappingDTOS.get(0);
                    EmployeeFlatInfoDTO leaveReliever1 = Leave_reliever_mappingDAO.buildFlatDTO(leave_reliever_mappingDTO.leaveReliever1);
                    EmployeeFlatInfoDTO leaveReliever2 = Leave_reliever_mappingDAO.buildFlatDTO(leave_reliever_mappingDTO.leaveReliever2);

                    if (leaveReliever1.employeeRecordsId == employee_leave_detailsDTO.leaveRelieverId) {
                        employeeFlatInfoDTO = leaveReliever1;
                    } else if (leaveReliever2.employeeRecordsId == employee_leave_detailsDTO.leaveRelieverId) {
                        employeeFlatInfoDTO = leaveReliever2;
                    } else {
                        employeeFlatInfoDTO = null;
                    }
                }
            }
        }

        //List<Leave_reliever_mappingDTO> leave_reliever_mappingDTOS = new Leave_reliever_mappingDAO().getDtosByOrganogramId(officeUnitOrganograms.id);


        if (employeeFlatInfoDTO != null) {
            employee_leave_detailsDTO.employeeNameBng = employeeFlatInfoDTO.employeeNameBn;
            employee_leave_detailsDTO.employeeNameEng = employeeFlatInfoDTO.employeeNameEn;
            employee_leave_detailsDTO.officeUnitId = employeeFlatInfoDTO.officeUnitId;
            employee_leave_detailsDTO.officeEng = employeeFlatInfoDTO.officeNameEn;
            employee_leave_detailsDTO.officeBng = employeeFlatInfoDTO.officeNameBn;
            employee_leave_detailsDTO.organogramId = employeeFlatInfoDTO.organogramId;
            employee_leave_detailsDTO.organogramBng = employeeFlatInfoDTO.organogramNameBn;
            employee_leave_detailsDTO.organogramEng = employeeFlatInfoDTO.organogramNameEn;
        }

        String value = request.getParameter("contactNumberDuringLeave");
        value = value == null ? null : value.trim();
        if (value != null && value.length() > 0) {
            if (Utils.isMobileNumberValid("88" + value)) {
                employee_leave_detailsDTO.contactNumberDuringLeave = "88" + value;
            } else {
                throw new Exception(isLangEng ? "Contact number during leave must starts with 01 and then contains 9 digits"
                        : 	"ছুটি চলাকালীন যোগাযোগের নাম্বার 01 দিয়ে শুরু হবে এবং পরবর্তীতে ৯টা সংখ্যা হবে");
            }
        } else {
            employee_leave_detailsDTO.contactNumberDuringLeave = "";
        }
        //employee_leave_detailsDTO.contactNumberDuringLeave = Jsoup.clean(request.getParameter("contactNumberDuringLeave"),Whitelist.simpleText()).trim();
        employee_leave_detailsDTO.contactAddress = Jsoup.clean(request.getParameter("contactAddress"), Whitelist.simpleText());
        employee_leave_detailsDTO.jobCat = Integer.parseInt(request.getParameter("jobCat"));
        employee_leave_detailsDTO.isApproved = request.getParameter("isApproved") != null && !request.getParameter("isApproved").equalsIgnoreCase("");
        try {
            d = f.parse(request.getParameter("approvalDate"));
            employee_leave_detailsDTO.approvalDate = d.getTime();
        } catch (Exception e) {
            logger.error(e);
        }
        employee_leave_detailsDTO.filesDropzone = Long.parseLong(request.getParameter("filesDropzone"));


        if (!addFlag) {
            String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
            String[] deleteArray = filesDropzoneFilesToDelete.split(",");
            for (int i = 0; i < deleteArray.length; i++) {
                if (i > 0) {
                    filesDAO.delete(Long.parseLong(deleteArray[i]));
                }
            }
        }

        logger.debug("Done adding  addEmployee_leave_details dto = " + employee_leave_detailsDTO);

        if (addFlag) {
            employee_leave_detailsDAO.add(employee_leave_detailsDTO);
        } else {
            employee_leave_detailsDAO.update(employee_leave_detailsDTO);
        }
        return employee_leave_detailsDTO;
    }


    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Employee_education_infoServlet.class;
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request, 6, "leave_details");
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request, 6, "leave_details");
    }

    @Override
    public String getDeleteRedirectURL(HttpServletRequest request) {
        return getAddOrEditOrDeleteRedirectURL(request, 6, "leave_details");
    }

    @Override
    public boolean getAddPagePermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    @Override
    public boolean getEditPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getViewPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getEditPermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getAddPermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    private boolean isValid(HttpServletRequest request) {
        return true;
    }
    
    @Override
    public boolean checkAddPermission(HttpServletRequest request) {
    	 UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
         if (userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId() || userDTO.roleID == RoleEnum.DATA_ENTRY.getRoleId()) {
             return true;
         }
         long empId = userDTO.employee_record_id;
         if(request.getParameter("empId") != null)
         {
         	empId= Long.parseLong(request.getParameter("empId"));
         }
         long employeeOrgId = WorkflowController.getOrganogramIDFromErID(empId);
         String employeeUsername = WorkflowController.getUserNameFromErId(empId, "english");
         		
         boolean permission = empId == userDTO.employee_record_id;
         if(!permission)
         {
        	 long myOfficeId = WorkflowController.getOfficeIdFromOrganogramId(userDTO.organogramID);
        	 long employeeOfficeId = WorkflowController.getOfficeIdFromOrganogramId(employeeOrgId);
        	 permission = (myOfficeId == employeeOfficeId);       	
         }
         
         if(!permission)
         {
        	 permission = (userDTO.roleID == SessionConstants.HR_1_ROLE && employeeUsername.startsWith("1"));       	 
         }
         if(!permission)
         {
        	 permission = (userDTO.roleID == SessionConstants.HR_2_ROLE && employeeUsername.startsWith("2"));       	 
         }
         if(!permission)
         {
        	 permission = (userDTO.roleID == SessionConstants.HR_3_ROLE 
        			 && (employeeUsername.startsWith("3") || employeeUsername.startsWith("4")));       	 
         }
         if (!permission) {
             employeeServletServiceLogger.error("empId doesn't match, empId : " + empId + " userDTO.employee_record_id : " + userDTO.employee_record_id);
         }
         return permission;
    }
    
    @Override
    public boolean checkEditOrViewPagePermission(HttpServletRequest request) {
    	return checkAddPermission(request);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        String language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getReliever":
                    long organogramId = Long.valueOf(Jsoup.clean(request.getParameter("organogram"), Whitelist.simpleText()));
                    String startDate = Jsoup.clean(request.getParameter("startDate"), Whitelist.simpleText());
                    String endDate = Jsoup.clean(request.getParameter("endDate"), Whitelist.simpleText());
                    EmployeeFlatInfoDTO relieverDTO = getAvailableReliever(organogramId, startDate, endDate);
                    String sendJsonFlatInfo = "";
                    if (relieverDTO != null) {
                        sendJsonFlatInfo = new Gson().toJson(relieverDTO);
                    }
                    response.setContentType("application/json");
                    response.getWriter().println(sendJsonFlatInfo);
                    return;
                case "getLeaveStatistic":
                    getLeaveStatistic(request, response, userDTO, language);
                    return;
                case "getRelieverEntryPage":
                	request.getRequestDispatcher("employee_leave_details/leave_entry.jsp").forward(request, response);
                    return;
                case "xl":
                	{
                        if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.EMPLOYEE_LEAVE_DETAILS_SEARCH)) 
                        {
                            getXl(request, response, userDTO, loginDTO);
                            return;
                        }
                    }
                default:
                    search(request);
                    super.doGet(request, response);
            }
        } catch (Exception ex) {
            logger.error(ex);
        }

    }

    private void getXl(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, LoginDTO loginDTO) {
		// TODO Auto-generated method stub
    	 Map<String, String> params = HttpRequestUtils.buildRequestParams(request);
         if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) != null) {
             Map<String, String> extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
             params.putAll(extraCriteriaMap);
         }
         RecordNavigator recordNavigator = getCommonDAOService().getRecordNavigatorLimitless(params, getTableJoinClause(request), getWhereClause(request), getSortClause(request));
         List<Employee_leave_detailsDTO> employee_leave_detailsDTOs = (List<Employee_leave_detailsDTO>) recordNavigator.list;
         String Language = LM.getLanguage(userDTO);

         getXl(employee_leave_detailsDTOs, response, Language, loginDTO, userDTO);
		
	}

	private void getXl(List<Employee_leave_detailsDTO> employee_leave_detailsDTOs, HttpServletResponse response,
			String Language, LoginDTO loginDTO, UserDTO userDTO) {
		XSSFWorkbook wb = new XSSFWorkbook();
        boolean isLangEng = Language.equalsIgnoreCase("english");

        List<String> headers = new ArrayList<>();
        
        headers.add(LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_EMPLOYEERECORDSID, userDTO));
        headers.add(LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LEAVETYPECAT, loginDTO));

        headers.add(LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LEAVESTARTDATE, loginDTO));
        headers.add(LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LEAVEENDDATE, loginDTO));

        headers.add(LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_JOININGDATEAFTERLEAVE, loginDTO));
        headers.add(LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LEAVERELIEVERID, loginDTO));
        headers.add(LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_CONTACTNUMBERDURINGLEAVE, loginDTO));


        


        String tabName = "Leave Details";
        CommonRequestHandler commonRequestHandler = new CommonRequestHandler();
        Sheet sheet;
        sheet = wb.createSheet(tabName);
        Row headerRow = sheet.createRow(0);
        try {
            int cellIndex = 0;
            for (String header : headers) {
                sheet.setColumnWidth(cellIndex, 25 * 256);
                Cell cell = headerRow.createCell(cellIndex);
                cell.setCellValue(header);
                cell.setCellStyle(commonRequestHandler.getHeaderStyle(wb));

                cellIndex++;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }

        int i = 1;
        CellStyle wrapStyle = wb.createCellStyle();
        wrapStyle.setWrapText(true);
        wrapStyle.setVerticalAlignment(VerticalAlignment.TOP);
        for (Employee_leave_detailsDTO employee_leave_detailsDTO : employee_leave_detailsDTOs) {
            Row row = sheet.createRow(i);

            int cellIndex = 0;

            Cell cell = row.createCell(cellIndex);
            cell.setCellStyle(wrapStyle);
            cell.setCellValue(Employee_recordsRepository.getInstance().getEmployeeName(employee_leave_detailsDTO.employeeRecordsId, Language));
            cellIndex++;

            cell = row.createCell(cellIndex);
            cell.setCellStyle(wrapStyle);
            cell.setCellValue(CatRepository.getInstance().getText(Language, "leave_type", employee_leave_detailsDTO.leaveTypeCat));
            cellIndex++;

            cell = row.createCell(cellIndex);
            cell.setCellStyle(wrapStyle);
            cell.setCellValue(StringUtils.getFormattedDate(Language, employee_leave_detailsDTO.leaveStartDate));
            cellIndex++;


            cell = row.createCell(cellIndex);
            cell.setCellStyle(wrapStyle);
            cell.setCellValue(StringUtils.getFormattedDate(Language, employee_leave_detailsDTO.leaveEndDate));
            cellIndex++;

            cell = row.createCell(cellIndex);
            cell.setCellStyle(wrapStyle);
            cell.setCellValue(StringUtils.getFormattedDate(Language, employee_leave_detailsDTO.joiningDateAfterLeave));
            cellIndex++;

            cell = row.createCell(cellIndex);
            cell.setCellStyle(wrapStyle);
            cell.setCellValue(Employee_recordsRepository.getInstance().getEmployeeName(employee_leave_detailsDTO.leaveRelieverId,Language));
            cellIndex++;

            cell = row.createCell(cellIndex);
            cell.setCellStyle(wrapStyle);
            cell.setCellValue(StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(employee_leave_detailsDTO.contactNumberDuringLeave)));
            cellIndex++;

    
            i++;
        }

        try {
            String fileName = "Leave.xlsx";
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            System.out.println("Writing xl");
            wb.write(response.getOutputStream());
            wb.close();
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
		
	}

	private void getLeaveStatistic(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO,
                                   String language) throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH, -6);
        long sixMonthEarlierTime = calendar.getTime().getTime();
        long employeeRecordId = userDTO.employee_record_id;

        Map<String, Integer> countByLeaveType = employee_leave_detailsDAO.getCountByLeaveType(employeeRecordId, sixMonthEarlierTime, language);

        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.print(gson.toJson(countByLeaveType));
        out.flush();
    }

    private EmployeeFlatInfoDTO getAvailableReliever(long organogramId, String startDate, String endDate) throws Exception {
        List<Leave_reliever_mappingDTO> leave_reliever_mappingDTOList = Leave_reliever_mappingRepository.getInstance()
                .getLeave_reliever_mappingDTOByorganogram_id(organogramId);
        if (leave_reliever_mappingDTOList == null || leave_reliever_mappingDTOList.size() == 0)
            return null;
        Leave_reliever_mappingDTO reliever_mappingDTO = leave_reliever_mappingDTOList.get(0);
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        long start = f.parse(startDate).getTime();
        long end = f.parse(endDate).getTime();
        if (employee_leave_detailsDAO.isRelieverAvailable(reliever_mappingDTO.leaveReliever1, start, end)) {
            return Leave_reliever_mappingDAO.buildFlatDTO(reliever_mappingDTO.leaveReliever1);
        } else if (employee_leave_detailsDAO.isRelieverAvailable(reliever_mappingDTO.leaveReliever2, start, end)) {
            return Leave_reliever_mappingDAO.buildFlatDTO(reliever_mappingDTO.leaveReliever2);
        }
        return null;
    }
}
