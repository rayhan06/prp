package employee_leave_details;

import util.CommonMaps;

public class Employee_leave_detailsMAPS extends CommonMaps {
	public Employee_leave_detailsMAPS(String tableName) {

		java_allfield_type_map.put("employee_records_id".toLowerCase(), "Long");
		java_allfield_type_map.put("leave_type_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("leave_start_date".toLowerCase(), "Long");
		java_allfield_type_map.put("leave_end_date".toLowerCase(), "Long");
		java_allfield_type_map.put("leave_reliever_id".toLowerCase(), "Long");
		java_allfield_type_map.put("contact_number_during_leave".toLowerCase(), "String");

		java_anyfield_search_map.put("language_text.languageTextEnglish", "String");
		java_anyfield_search_map.put("language_text.languageTextBangla", "String");

		java_anyfield_search_map.put(tableName + ".employee_records_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".leave_start_date".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".leave_end_date".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".leave_reliever_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".contact_number_during_leave".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("employeeRecordsId".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_DTO_map.put("leaveTypeCat".toLowerCase(), "leaveTypeCat".toLowerCase());
		java_DTO_map.put("leaveStartDate".toLowerCase(), "leaveStartDate".toLowerCase());
		java_DTO_map.put("leaveEndDate".toLowerCase(), "leaveEndDate".toLowerCase());
		java_DTO_map.put("joiningDateAfterLave".toLowerCase(), "joiningDateAfterLave".toLowerCase());
		java_DTO_map.put("comments".toLowerCase(), "comments".toLowerCase());
		java_DTO_map.put("leaveRelieverId".toLowerCase(), "leaveRelieverId".toLowerCase());
		java_DTO_map.put("contactNumberDuringLeave".toLowerCase(), "contactNumberDuringLeave".toLowerCase());
		java_DTO_map.put("contactAddress".toLowerCase(), "contactAddress".toLowerCase());
		java_DTO_map.put("jobCat".toLowerCase(), "jobCat".toLowerCase());
		java_DTO_map.put("isApproved".toLowerCase(), "isApproved".toLowerCase());
		java_DTO_map.put("approvalDate".toLowerCase(), "approvalDate".toLowerCase());
		java_DTO_map.put("filesDropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("employee_records_id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_SQL_map.put("leave_type_cat".toLowerCase(), "leaveTypeCat".toLowerCase());
		java_SQL_map.put("leave_start_date".toLowerCase(), "leaveStartDate".toLowerCase());
		java_SQL_map.put("leave_end_date".toLowerCase(), "leaveEndDate".toLowerCase());
		java_SQL_map.put("joining_date_after_leave".toLowerCase(), "joiningDateAfterLave".toLowerCase());
		java_SQL_map.put("comments".toLowerCase(), "comments".toLowerCase());
		java_SQL_map.put("leave_reliever_id".toLowerCase(), "leaveRelieverId".toLowerCase());
		java_SQL_map.put("contact_number_during_leave".toLowerCase(), "contactNumberDuringLeave".toLowerCase());
		java_SQL_map.put("contact_address".toLowerCase(), "contactAddress".toLowerCase());
		java_SQL_map.put("job_cat".toLowerCase(), "jobCat".toLowerCase());
		java_SQL_map.put("is_approved".toLowerCase(), "isApproved".toLowerCase());
		java_SQL_map.put("approval_date".toLowerCase(), "approvalDate".toLowerCase());
		java_SQL_map.put("files_dropzone".toLowerCase(), "filesDropzone".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Employee Records Id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_Text_map.put("Leave Type Cat".toLowerCase(), "leaveTypeCat".toLowerCase());
		java_Text_map.put("Leave Start Date".toLowerCase(), "leaveStartDate".toLowerCase());
		java_Text_map.put("Leave End Date".toLowerCase(), "leaveEndDate".toLowerCase());
		java_Text_map.put("Joining Date After Leave".toLowerCase(), "joiningDateAfterLave".toLowerCase());
		java_Text_map.put("Comments".toLowerCase(), "comments".toLowerCase());
		java_Text_map.put("Leave Reliever Id".toLowerCase(), "leaveRelieverId".toLowerCase());
		java_Text_map.put("Contact Number During Leave".toLowerCase(), "contactNumberDuringLeave".toLowerCase());
		java_Text_map.put("Contact Address".toLowerCase(), "contactAddress".toLowerCase());
		java_Text_map.put("Job Cat".toLowerCase(), "jobCat".toLowerCase());
		java_Text_map.put("Is Approved".toLowerCase(), "isApproved".toLowerCase());
		java_Text_map.put("Approval Date".toLowerCase(), "approvalDate".toLowerCase());
		java_Text_map.put("Files Dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

	}

}