package employee_leave_details;

import sessionmanager.SessionConstants;
import util.CommonEmployeeDTO;

public class Employee_leave_detailsDTO extends CommonEmployeeDTO {

    public int leaveTypeCat = 0;
    public long leaveStartDate = SessionConstants.MIN_DATE;
    public long leaveEndDate = SessionConstants.MIN_DATE;
    public long joiningDateAfterLeave = SessionConstants.MIN_DATE;
    public String comments = "";
    public long leaveRelieverId = 0;
    public String contactNumberDuringLeave = "";
    public String contactAddress = "";
    public boolean isApproved = false;
    public long approvalDate = 0;
    public long filesDropzone = 0;
    public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
    public long lastLeaveId = -1;
    public String employeeNameEng = "";
    public String employeeNameBng = "";
    public long officeUnitId = 0;
    public String officeEng = "";
    public String officeBng = "";
    public long organogramId = 0;
    public String organogramEng = "";
    public String organogramBng = "";


    @Override
    public String toString() {
        return "Employee_leave_detailsDTO{" +
                "employeeRecordsId=" + employeeRecordsId +
                ", leaveTypeCat=" + leaveTypeCat +
                ", leaveStartDate=" + leaveStartDate +
                ", leaveEndDate=" + leaveEndDate +
                ", joiningDateAfterLeave=" + joiningDateAfterLeave +
                ", comments='" + comments + '\'' +
                ", leaveRelieverId=" + leaveRelieverId +
                ", contactNumberDuringLeave='" + contactNumberDuringLeave + '\'' +
                ", contactAddress='" + contactAddress + '\'' +
                ", isApproved=" + isApproved +
                ", approvalDate=" + approvalDate +
                ", filesDropzone=" + filesDropzone +
                ", insertionDate=" + insertionDate +
                ", insertedBy='" + insertedBy + '\'' +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", lastLeaveId=" + lastLeaveId +
                ", searchColumn='" + searchColumn + '\'' +
                ", employeeNameEng='" + employeeNameEng + '\'' +
                ", employeeNameBng='" + employeeNameBng + '\'' +
                ", officeUnitId=" + officeUnitId +
                ", officeEng='" + officeEng + '\'' +
                ", officeBng='" + officeBng + '\'' +
                ", organogramId=" + organogramId +
                ", organogramEng='" + organogramEng + '\'' +
                ", organogramBng='" + organogramBng + '\'' +
                '}';
    }

    public String getIdsStrInJsonString() {
        return String.format("{employeeRecordId: '%d', leaveStartDate: '%d', leaveEndDate: '%d'}",
                employeeRecordsId, leaveStartDate, leaveEndDate);
    }
}