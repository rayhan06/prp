package parliament_session;

import java.util.*;

import pb.OptionDTO;
import sessionmanager.SessionConstants;
import util.*;


public class Parliament_sessionDTO extends CommonDTO {

    public long electionDetailsId = 0;
    public int sessionNumber = 0;
    public long startDate = SessionConstants.MIN_DATE;
    public long endDate = SessionConstants.MIN_DATE;
    public long insertionDate = SessionConstants.MIN_DATE;
    public String insertedBy = "";
    public String modifiedBy = "";

    public List<ParliamentSessionDateDTO> parliamentSessionDateDTOList = new ArrayList<>();

    public String getText(boolean isLangEn) {
        if (isLangEn) {
            return String.format(
                    "Session - %d | %s - %s",
                    sessionNumber,
                    StringUtils.getFormattedDate(true, startDate),
                    StringUtils.getFormattedDate(true, endDate)
            );
        }
        return String.format(
                "সেশন - %s | %s - %s",
                StringUtils.convertToBanNumber(String.format("%d", sessionNumber)),
                StringUtils.getFormattedDate(false, startDate),
                StringUtils.getFormattedDate(false, endDate)
        );
    }

    public String getSessionText(boolean isLangEn) {
        if (isLangEn) {
            return String.format(
                    "Session - %d",
                    sessionNumber
            );
        }
        return String.format(
                "সেশন - %s",
                StringUtils.convertToBanNumber(String.format("%d", sessionNumber))
        );
    }

    public OptionDTO getOptionDtoWithDate() {
        return new OptionDTO(getText(true), getText(false), String.format("%d", iD));
    }

    @Override
    public String toString() {
        return "$Parliament_sessionDTO[" +
               " iD = " + iD +
               " electionDetailsId = " + electionDetailsId +
               " sessionNumber = " + sessionNumber +
               " startDate = " + startDate +
               " endDate = " + endDate +
               " insertionDate = " + insertionDate +
               " insertedBy = " + insertedBy +
               " modifiedBy = " + modifiedBy +
               " isDeleted = " + isDeleted +
               " lastModificationTime = " + lastModificationTime +
               "]";
    }

}