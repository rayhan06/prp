package parliament_session;

import com.google.gson.Gson;
import common.ApiResponse;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import parliament_gallery.Parliament_galleryDAO;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@WebServlet("/Parliament_sessionServlet")
@MultipartConfig
public class Parliament_sessionServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Parliament_sessionServlet.class);

    String tableName = "parliament_session";

    Parliament_sessionDAO parliament_sessionDAO;
    CommonRequestHandler commonRequestHandler;
    ParliamentSessionDateDAO parliamentSessionDateDAO;
    private final Gson gson = new Gson();

    public Parliament_sessionServlet() {
        super();

        parliament_sessionDAO = new Parliament_sessionDAO(tableName);
        parliamentSessionDateDAO = new ParliamentSessionDateDAO("parliament_session_date");
        commonRequestHandler = new CommonRequestHandler(parliament_sessionDAO);

    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getAddPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_SESSION_ADD)) {
                        commonRequestHandler.getAddPage(request, response);
                        return;
                    }
                    break;
                case "getEditPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_SESSION_UPDATE)) {
                        String URL = getParliament_session(request, response);
                        request.getRequestDispatcher(URL).forward(request, response);
                        return;
                    }

                    break;
                case "getURL":
                    String URL = request.getParameter("URL");
                    response.sendRedirect(URL);
                    return;
                case "search":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_SESSION_SEARCH)) {

                        String filter = request.getParameter("filter");
                        if (filter != null) {
                            filter = "";
                            searchParliament_session(request, response, filter);
                        } else {
                            searchParliament_session(request, response, "");
                        }
                        return;
                    }
                    break;
                case "view":
                    System.out.println("view requested");
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_SESSION_SEARCH)) {
                        commonRequestHandler.view(request, response);
                        return;
                    }

                    break;
                case "buildParliamentSession":
                    long electionId = Long.parseLong(request.getParameter("election_id"));
                    String language = request.getParameter("language");
                    String options = Parliament_sessionRepository.getInstance().buildOptions(language, electionId);
                    PrintWriter out = response.getWriter();
                    out.println(options);
                    out.close();
                    return;
            }
        } catch (Exception ex) {
            logger.debug(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);

    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "ajax_add":

                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_SESSION_ADD)) {
                        try {
                            String URL = addParliament_session(request, response, true, userDTO);
                            ApiResponse.sendSuccessResponse(response, URL);
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    }

                    break;
                case "getDTO":

                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_SESSION_ADD)) {
                        getDTO(request, response);
                        return;
                    }


                    break;
                case "ajax_edit":

                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_SESSION_UPDATE)) {
                        try {
                            String URL = addParliament_session(request, response, false, userDTO);
                            ApiResponse.sendSuccessResponse(response, URL);
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    }
                case "delete":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_SESSION_UPDATE)) {
                        commonRequestHandler.delete(request, response, userDTO);
                        return;
                    }
                    break;
                case "search":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_SESSION_SEARCH)) {
                        searchParliament_session(request, response, "");
                        return;
                    }

                    break;
                case "getGeo":
                    request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
                    return;
            }

        } catch (Exception ex) {

            logger.debug(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);

    }

    private void getDTO(HttpServletRequest request, HttpServletResponse response) throws IOException {

        System.out.println("In getDTO");
        Parliament_sessionDTO parliament_sessionDTO = parliament_sessionDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String encoded = this.gson.toJson(parliament_sessionDTO);
        out.print(encoded);
        out.flush();


    }

    private String addParliament_session(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = userDTO.languageID == CommonConstant.Language_ID_English;
        request.setAttribute("failureMessage", "");
        Parliament_sessionDTO parliament_sessionDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        long currentTime = System.currentTimeMillis();
        if (addFlag) {
            parliament_sessionDTO = new Parliament_sessionDTO();
            parliament_sessionDTO.insertionDate = currentTime;
            parliament_sessionDTO.insertedBy = String.valueOf(userDTO.employee_record_id);
        } else {
            parliament_sessionDTO = parliament_sessionDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
        }
        parliament_sessionDTO.modifiedBy = String.valueOf(userDTO.employee_record_id);
        parliament_sessionDTO.lastModificationTime = currentTime;
        parliament_sessionDTO.electionDetailsId = Long.parseLong(request.getParameter("electionDetailsId"));
        parliament_sessionDTO.sessionNumber = Integer.parseInt(request.getParameter("sessionNumber"));


        Date d = f.parse(Jsoup.clean(request.getParameter("startDate"), Whitelist.simpleText()));
        parliament_sessionDTO.startDate = d.getTime();


        d = f.parse(Jsoup.clean(request.getParameter("endDate"), Whitelist.simpleText()));
        parliament_sessionDTO.endDate = d.getTime();

        if (parliament_sessionDTO.startDate > parliament_sessionDTO.endDate)
            throw new Exception(
                    isLangEng ? "From date can not be greater than to date"
                            : "তারিখ হতে, তারিখ পর্যন্ত থেকে বড় হতে পারে না"
            );
        System.out.println("Done adding  addParliament_session dto = " + parliament_sessionDTO);
        long returnedID;

        if (addFlag) {
            returnedID = parliament_sessionDAO.manageWriteOperations(parliament_sessionDTO, SessionConstants.INSERT, -1, userDTO);
            populateGatePassGallery(parliament_sessionDTO, userDTO);
//            Parliament_galleryDAO parliament_galleryDAO = new Parliament_galleryDAO();
//            parliament_galleryDAO.populateGatePassGalleryByParliamentSessionDTO(parliament_sessionDTO, userDTO);
        } else {
            returnedID = parliament_sessionDAO.manageWriteOperations(parliament_sessionDTO, SessionConstants.UPDATE, -1, userDTO);
        }


        List<ParliamentSessionDateDTO> parliamentSessionDateDTOList = createParliamentSessionDateDTOListByRequest(request, userDTO);
        if (addFlag) //add or validate
        {
            if (parliamentSessionDateDTOList != null) {
                for (ParliamentSessionDateDTO parliamentSessionDateDTO : parliamentSessionDateDTOList) {
                    parliamentSessionDateDTO.parliamentSessionId = parliament_sessionDTO.iD;
                    parliamentSessionDateDAO.add(parliamentSessionDateDTO);
                }
            }

        } else {
            List<Long> childIdsFromRequest = parliamentSessionDateDAO.getChildIdsFromRequest(request, "parliamentSessionDate");
            //delete the removed children
            parliamentSessionDateDAO.deleteChildrenNotInList("parliament_session", "parliament_session_date", parliament_sessionDTO.iD, childIdsFromRequest);
            List<Long> childIDsInDatabase = parliamentSessionDateDAO.getChilIds("parliament_session", "parliament_session_date", parliament_sessionDTO.iD);


            if (childIdsFromRequest != null) {
                for (int i = 0; i < childIdsFromRequest.size(); i++) {
                    Long childIDFromRequest = childIdsFromRequest.get(i);
                    if (childIDsInDatabase.contains(childIDFromRequest)) {
                        ParliamentSessionDateDTO parliamentSessionDateDTO = createParliamentSessionDateDTOByRequestAndIndex(request, false, i, userDTO);
                        parliamentSessionDateDTO.parliamentSessionId = parliament_sessionDTO.iD;
                        parliamentSessionDateDAO.update(parliamentSessionDateDTO);
                    } else {
                        ParliamentSessionDateDTO parliamentSessionDateDTO = createParliamentSessionDateDTOByRequestAndIndex(request, true, i, userDTO);
                        parliamentSessionDateDTO.parliamentSessionId = parliament_sessionDTO.iD;
                        parliamentSessionDateDAO.add(parliamentSessionDateDTO);
                    }
                }
            } else {
                parliamentSessionDateDAO.deleteParliamentSessionDateByParliamentSessionID(parliament_sessionDTO.iD);
            }

        }


        String inPlaceSubmit = request.getParameter("inplacesubmit");

        if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
            return getParliament_session(request, response, returnedID);
        } else {
            String data = request.getParameter("data");
            if("otTypeCalendar".equals(data)) {
                return "OT_type_CalendarServlet?actionType=getAddPage";
            }
            return "Parliament_sessionServlet?actionType=search";
        }


    }

    private void populateGatePassGallery(Parliament_sessionDTO parliament_sessionDTO, UserDTO userDTO) {
        ExecutorService executorService = Executors.newSingleThreadExecutor((r) -> {
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            return thread;
        });
        executorService.execute(
                () -> {
                    Parliament_galleryDAO parliament_galleryDAO = new Parliament_galleryDAO();
                    parliament_galleryDAO.populateGatePassGalleryByParliamentSessionDTO(parliament_sessionDTO, userDTO);
                }
        );
        executorService.shutdown();
    }

    private List<ParliamentSessionDateDTO> createParliamentSessionDateDTOListByRequest(HttpServletRequest request, UserDTO userDTO) throws ParseException {
        List<ParliamentSessionDateDTO> parliamentSessionDateDTOList = new ArrayList<>();
        if (request.getParameterValues("parliamentSessionDate.iD") != null) {
            int parliamentSessionDateItemNo = request.getParameterValues("parliamentSessionDate.iD").length;


            for (int index = 0; index < parliamentSessionDateItemNo; index++) {
                ParliamentSessionDateDTO parliamentSessionDateDTO = createParliamentSessionDateDTOByRequestAndIndex(request, true, index, userDTO);
                parliamentSessionDateDTOList.add(parliamentSessionDateDTO);
            }

            return parliamentSessionDateDTOList;
        }
        return null;
    }


    private ParliamentSessionDateDTO createParliamentSessionDateDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, UserDTO userDTO) throws ParseException {

        ParliamentSessionDateDTO parliamentSessionDateDTO;
        if (addFlag) {
            parliamentSessionDateDTO = new ParliamentSessionDateDTO();
        } else {
            parliamentSessionDateDTO = parliamentSessionDateDAO.getDTOByID(Long.parseLong(request.getParameterValues("parliamentSessionDate.iD")[index]));
        }
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        parliamentSessionDateDTO.parliamentSessionId = Long.parseLong(request.getParameterValues("parliamentSessionDate.parliamentSessionId")[index]);
        Date d = f.parse(request.getParameterValues("parliamentSessionDate.sessionDate")[index]);
        parliamentSessionDateDTO.sessionDate = d.getTime();

        long currentTime = System.currentTimeMillis();
        parliamentSessionDateDTO.insertionDate = currentTime;
        parliamentSessionDateDTO.insertedBy = String.valueOf(userDTO.employee_record_id);
        parliamentSessionDateDTO.modifiedBy = String.valueOf(userDTO.employee_record_id);
        return parliamentSessionDateDTO;

    }


    private String getParliament_session(HttpServletRequest request, HttpServletResponse response, long id) {

        Parliament_sessionDTO parliament_sessionDTO;

        parliament_sessionDTO = parliament_sessionDAO.getDTOByID(id);
        request.setAttribute("ID", parliament_sessionDTO.iD);
        request.setAttribute("parliament_sessionDTO", parliament_sessionDTO);
        request.setAttribute("parliament_sessionDAO", parliament_sessionDAO);

        String URL;

        String inPlaceEdit = request.getParameter("inplaceedit");
        String inPlaceSubmit = request.getParameter("inplacesubmit");
        String getBodyOnly = request.getParameter("getBodyOnly");

        if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
            URL = "parliament_session/parliament_sessionInPlaceEdit.jsp";
            request.setAttribute("inplaceedit", "");
        } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
            URL = "parliament_session/parliament_sessionSearchRow.jsp";
            request.setAttribute("inplacesubmit", "");
        } else {
            if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                URL = "parliament_session/parliament_sessionEditBody.jsp?actionType=edit";
            } else {
                URL = "parliament_session/parliament_sessionEdit.jsp?actionType=edit";
            }
        }

        return URL;


    }


    private String getParliament_session(HttpServletRequest request, HttpServletResponse response) {
        return getParliament_session(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void searchParliament_session(HttpServletRequest request, HttpServletResponse response, String filter) throws Exception {
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_PARLIAMENT_SESSION,
                request,
                parliament_sessionDAO,
                SessionConstants.VIEW_PARLIAMENT_SESSION,
                SessionConstants.SEARCH_PARLIAMENT_SESSION,
                tableName,
                true,
                userDTO,
                filter,
                true);

        rnManager.doJob(loginDTO);


        request.setAttribute("parliament_sessionDAO", parliament_sessionDAO);

        if (!hasAjax) {
            System.out.println("Going to parliament_session/parliament_sessionSearch.jsp");
            request.getRequestDispatcher("parliament_session/parliament_sessionSearch.jsp").forward(request, response);
        } else {
            System.out.println("Going to parliament_session/parliament_sessionSearchForm.jsp");
            request.getRequestDispatcher("parliament_session/parliament_sessionSearchForm.jsp").forward(request, response);
        }

    }

}

