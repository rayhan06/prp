package parliament_session;
import java.util.*;

import sessionmanager.SessionConstants;
import util.*;


public class ParliamentSessionDateDTO extends CommonDTO
{

	public long parliamentSessionId = 0;
	public long sessionDate = SessionConstants.MIN_DATE;
	public long insertionDate = SessionConstants.MIN_DATE;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	public List<ParliamentSessionDateDTO> parliamentSessionDateDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$ParliamentSessionDateDTO[" +
            " iD = " + iD +
            " parliamentSessionId = " + parliamentSessionId +
            " sessionDate = " + sessionDate +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}