package parliament_session;

import util.CommonMaps;


public class ParliamentSessionDateMAPS extends CommonMaps {
    public ParliamentSessionDateMAPS(String tableName) {


        java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
        java_DTO_map.put("parliamentSessionId".toLowerCase(), "parliamentSessionId".toLowerCase());
        java_DTO_map.put("sessionDate".toLowerCase(), "sessionDate".toLowerCase());
        java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
        java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
        java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
        java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

        java_SQL_map.put("parliament_session_id".toLowerCase(), "parliamentSessionId".toLowerCase());
        java_SQL_map.put("session_date".toLowerCase(), "sessionDate".toLowerCase());

        java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
        java_Text_map.put("Parliament Session Id".toLowerCase(), "parliamentSessionId".toLowerCase());
        java_Text_map.put("Session Date".toLowerCase(), "sessionDate".toLowerCase());
        java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
        java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
        java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
        java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

    }

}