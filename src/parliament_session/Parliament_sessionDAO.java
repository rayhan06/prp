package parliament_session;

import common.ConnectionAndStatementUtil;
import dbm.DBMR;
import org.apache.log4j.Logger;
import ot_type_calendar.OT_type_CalendarDTO;
import pb.OptionDTO;
import pb.Utils;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"unused", "rawtypes", "Duplicates"})
public class Parliament_sessionDAO extends NavigationService4 {

    Logger logger = Logger.getLogger(getClass());


    public Parliament_sessionDAO(String tableName) {
        super(tableName);
        joinSQL = "";
        commonMaps = new Parliament_sessionMAPS(tableName);
        columnNames = new String[]
                {
                        "ID",
                        "election_details_id",
                        "session_number",
                        "start_date",
                        "end_date",
                        "insertion_date",
                        "inserted_by",
                        "modified_by",
                        "isDeleted",
                        "lastModificationTime"
                };
    }

    public Parliament_sessionDAO() {
        this("parliament_session");
    }

    public void setSearchColumn(Parliament_sessionDTO parliament_sessionDTO) {
        parliament_sessionDTO.searchColumn = "";

    }

    public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException {
        Parliament_sessionDTO parliament_sessionDTO = (Parliament_sessionDTO) commonDTO;
        int index = 1;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(parliament_sessionDTO);
        if (isInsert) {
            ps.setObject(index++, parliament_sessionDTO.iD);
        }
        ps.setObject(index++, parliament_sessionDTO.electionDetailsId);
        ps.setObject(index++, parliament_sessionDTO.sessionNumber);
        ps.setObject(index++, parliament_sessionDTO.startDate);
        ps.setObject(index++, parliament_sessionDTO.endDate);
        ps.setObject(index++, parliament_sessionDTO.insertionDate);
        ps.setObject(index++, parliament_sessionDTO.insertedBy);
        ps.setObject(index++, parliament_sessionDTO.modifiedBy);
        if (isInsert) {
            ps.setObject(index++, 0);
            ps.setObject(index++, lastModificationTime);
        }
    }

    public void get(Parliament_sessionDTO parliament_sessionDTO, ResultSet rs) throws SQLException {
        parliament_sessionDTO.iD = rs.getLong("ID");
        parliament_sessionDTO.electionDetailsId = rs.getLong("election_details_id");
        parliament_sessionDTO.sessionNumber = rs.getInt("session_number");
        parliament_sessionDTO.startDate = rs.getLong("start_date");
        parliament_sessionDTO.endDate = rs.getLong("end_date");
        parliament_sessionDTO.insertionDate = rs.getLong("insertion_date");
        parliament_sessionDTO.insertedBy = rs.getString("inserted_by");
        parliament_sessionDTO.modifiedBy = rs.getString("modified_by");
        parliament_sessionDTO.isDeleted = rs.getInt("isDeleted");
        parliament_sessionDTO.lastModificationTime = rs.getLong("lastModificationTime");
    }

    public Parliament_sessionDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Parliament_sessionDTO dto = new Parliament_sessionDTO();
            get(dto, rs);
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    private static final String findByElectionDetailsIdSql =
            "SELECT * FROM parliament_session WHERE isDeleted = 0 AND election_details_id = %d";

    public List<Parliament_sessionDTO> findByElectionDetailsId(long electionDetailsId) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(findByElectionDetailsIdSql, electionDetailsId),
                this::buildObjectFromResultSet
        );
    }

    public String buildOptionsWithDate(String language, Long electionDetailsId, String selectedId) {
        List<OptionDTO> optionDTOList =
                findByElectionDetailsId(electionDetailsId)
                        .stream()
                        .sorted(Comparator.comparingInt(dto -> dto.sessionNumber))
                        .map(Parliament_sessionDTO::getOptionDtoWithDate)
                        .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, selectedId);
    }


    //need another getter for repository
    public Parliament_sessionDTO getDTOByID(long ID) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Parliament_sessionDTO parliament_sessionDTO = null;
        try {

            String sql = "SELECT * ";

            sql += " FROM " + tableName;

            sql += " WHERE ID=" + ID;

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                parliament_sessionDTO = new Parliament_sessionDTO();

                get(parliament_sessionDTO, rs);

            }


            ParliamentSessionDateDAO parliamentSessionDateDAO = new ParliamentSessionDateDAO("parliament_session_date");
            List<ParliamentSessionDateDTO> parliamentSessionDateDTOList = parliamentSessionDateDAO.getParliamentSessionDateDTOListByParliamentSessionID(parliament_sessionDTO.iD);
            parliament_sessionDTO.parliamentSessionDateDTOList = parliamentSessionDateDTOList;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return parliament_sessionDTO;
    }


    public List<Parliament_sessionDTO> getDTOs(Collection recordIDs) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Parliament_sessionDTO parliament_sessionDTO = null;
        List<Parliament_sessionDTO> parliament_sessionDTOList = new ArrayList<>();
        if (recordIDs.isEmpty()) {
            return parliament_sessionDTOList;
        }
        try {

            String sql = "SELECT * ";

            sql += " FROM " + tableName;

            sql += " WHERE ID IN ( ";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql += ",";
                }
                sql += ((ArrayList) recordIDs).get(i);
            }
            sql += ")  order by lastModificationTime desc";

            printSql(sql);

            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                parliament_sessionDTO = new Parliament_sessionDTO();
                get(parliament_sessionDTO, rs);
                System.out.println("got this DTO: " + parliament_sessionDTO);

                parliament_sessionDTOList.add(parliament_sessionDTO);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return parliament_sessionDTOList;

    }


    //add repository
    public List<Parliament_sessionDTO> getAllParliament_session(boolean isFirstReload) {
        List<Parliament_sessionDTO> parliament_sessionDTOList = new ArrayList<>();

        String sql = "SELECT * FROM parliament_session";
        sql += " WHERE ";


        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        sql += " order by parliament_session.lastModificationTime desc";
        printSql(sql);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;


        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);


            while (rs.next()) {
                Parliament_sessionDTO parliament_sessionDTO = new Parliament_sessionDTO();
                get(parliament_sessionDTO, rs);

                parliament_sessionDTOList.add(parliament_sessionDTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return parliament_sessionDTOList;
    }


    public List<Parliament_sessionDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }


    public List<Parliament_sessionDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
                                               String filter, boolean tableHasJobCat) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        List<Parliament_sessionDTO> parliament_sessionDTOList = new ArrayList<>();

        try {

            String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Parliament_sessionDTO parliament_sessionDTO = new Parliament_sessionDTO();
                get(parliament_sessionDTO, rs);

                parliament_sessionDTOList.add(parliament_sessionDTO);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return parliament_sessionDTOList;

    }

    public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                          UserDTO userDTO, String filter, boolean tableHasJobCat) {
        boolean viewAll = false;

        if (p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null) {
            System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
            viewAll = true;
        } else {
            System.out.println("ViewAll is null ");
        }

        String sql = "SELECT ";
        if (category == GETIDS) {
            sql += " " + tableName + ".ID as ID ";
        } else if (category == GETCOUNT) {
            sql += " count( " + tableName + ".ID) as countID ";
        } else if (category == GETDTOS) {
            sql += " distinct " + tableName + ".* ";
        }
        sql += "FROM " + tableName + " ";
        sql += joinSQL;


        String AnyfieldSql = "";
        String AllFieldSql = "";

        if (p_searchCriteria != null) {


            Enumeration names = p_searchCriteria.keys();
            String str, value;

            AnyfieldSql = "(";

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                AnyfieldSql += tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
            }
            AnyfieldSql += ")";
            System.out.println("AnyfieldSql = " + AnyfieldSql);

            AllFieldSql = "(";
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                System.out.println(str + ": " + value);
                if (value != null && !value.equalsIgnoreCase("") && (
                        str.equals("start_date_start")
                                || str.equals("start_date_end")
                                || str.equals("end_date_start")
                                || str.equals("end_date_end")
                                || str.equals("insertion_date_start")
                                || str.equals("insertion_date_end")
                )

                ) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql += " AND  ";
                    }

                    if (str.equals("start_date_start")) {
                        AllFieldSql += "" + tableName + ".start_date >= " + p_searchCriteria.get(str);
                        i++;
                    } else if (str.equals("start_date_end")) {
                        AllFieldSql += "" + tableName + ".start_date <= " + p_searchCriteria.get(str);
                        i++;
                    } else if (str.equals("end_date_start")) {
                        AllFieldSql += "" + tableName + ".end_date >= " + p_searchCriteria.get(str);
                        i++;
                    } else if (str.equals("end_date_end")) {
                        AllFieldSql += "" + tableName + ".end_date <= " + p_searchCriteria.get(str);
                        i++;
                    } else if (str.equals("insertion_date_start")) {
                        AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
                        i++;
                    } else if (str.equals("insertion_date_end")) {
                        AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
                        i++;
                    }


                }
            }

            AllFieldSql += ")";
            System.out.println("AllFieldSql = " + AllFieldSql);


        }


        sql += " WHERE ";

        sql += " (" + tableName + ".isDeleted = 0 ";
        sql += ")";


        if (!filter.equalsIgnoreCase("")) {
            sql += " and " + filter + " ";
        }

        if (!AnyfieldSql.equals("()") && !AnyfieldSql.equals("")) {
            sql += " AND " + AnyfieldSql;

        }
        if (!AllFieldSql.equals("()") && !AllFieldSql.equals("")) {
            sql += " AND " + AllFieldSql;
        }


        sql += " order by " + tableName + ".lastModificationTime desc ";

        printSql(sql);

        if (limit >= 0) {
            sql += " limit " + limit;
        }
        if (offset >= 0) {
            sql += " offset " + offset;
        }

        System.out.println("-------------- sql = " + sql);

        return sql;
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
                                           boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat) {
        return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
    }

}
	