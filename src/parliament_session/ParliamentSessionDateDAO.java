package parliament_session;

import dbm.DBMR;
import dbm.DBMW;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;

public class ParliamentSessionDateDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public ParliamentSessionDateDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new ParliamentSessionDateMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"parliament_session_id",
			"session_date",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public ParliamentSessionDateDAO()
	{
		this("parliament_session_date");		
	}
	
	public void setSearchColumn(ParliamentSessionDateDTO parliamentsessiondateDTO)
	{
		parliamentsessiondateDTO.searchColumn = "";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		ParliamentSessionDateDTO parliamentsessiondateDTO = (ParliamentSessionDateDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(parliamentsessiondateDTO);
		if(isInsert)
		{
			ps.setObject(index++,parliamentsessiondateDTO.iD);
		}
		ps.setObject(index++,parliamentsessiondateDTO.parliamentSessionId);
		ps.setObject(index++,parliamentsessiondateDTO.sessionDate);
		ps.setObject(index++,parliamentsessiondateDTO.insertionDate);
		ps.setObject(index++,parliamentsessiondateDTO.insertedBy);
		ps.setObject(index++,parliamentsessiondateDTO.modifiedBy);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(ParliamentSessionDateDTO parliamentsessiondateDTO, ResultSet rs) throws SQLException
	{
		parliamentsessiondateDTO.iD = rs.getLong("ID");
		parliamentsessiondateDTO.parliamentSessionId = rs.getLong("parliament_session_id");
		parliamentsessiondateDTO.sessionDate = rs.getLong("session_date");
		parliamentsessiondateDTO.insertionDate = rs.getLong("insertion_date");
		parliamentsessiondateDTO.insertedBy = rs.getString("inserted_by");
		parliamentsessiondateDTO.modifiedBy = rs.getString("modified_by");
		parliamentsessiondateDTO.isDeleted = rs.getInt("isDeleted");
		parliamentsessiondateDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}
	
	
	
	
		
	
	public void deleteParliamentSessionDateByParliamentSessionID(long parliamentSessionID) throws Exception{
		
		
		Connection connection = null;
		Statement stmt = null;
		try{
			
			String sql = "UPDATE parliament_session_date SET isDeleted=0 WHERE parliament_session_id="+parliamentSessionID;			
			logger.debug("sql " + sql);
			connection = DBMW.getInstance().getConnection();
			stmt = connection.createStatement();
			stmt.execute(sql);
		}catch(Exception ex){
			logger.debug("fatal",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMW.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
	}		
   
	public List<ParliamentSessionDateDTO> getParliamentSessionDateDTOListByParliamentSessionID(long parliamentSessionID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		ParliamentSessionDateDTO parliamentsessiondateDTO = null;
		List<ParliamentSessionDateDTO> parliamentsessiondateDTOList = new ArrayList<>();
		
		try{
			
			String sql = "SELECT * FROM parliament_session_date where isDeleted=0 and parliament_session_id="+parliamentSessionID+" order by parliament_session_date.lastModificationTime";
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);

			while(rs.next()){
				parliamentsessiondateDTO = new ParliamentSessionDateDTO();
				get(parliamentsessiondateDTO, rs);
				parliamentsessiondateDTOList.add(parliamentsessiondateDTO);

			}			
			
		}catch(Exception ex){
			logger.debug("fatal",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return parliamentsessiondateDTOList;
	}

	//need another getter for repository
	public ParliamentSessionDateDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		ParliamentSessionDateDTO parliamentsessiondateDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				parliamentsessiondateDTO = new ParliamentSessionDateDTO();

				get(parliamentsessiondateDTO, rs);

			}			
			
			
			
			ParliamentSessionDateDAO parliamentSessionDateDAO = new ParliamentSessionDateDAO("parliament_session_date");			
			List<ParliamentSessionDateDTO> parliamentSessionDateDTOList = parliamentSessionDateDAO.getParliamentSessionDateDTOListByParliamentSessionID(parliamentsessiondateDTO.iD);
			parliamentsessiondateDTO.parliamentSessionDateDTOList = parliamentSessionDateDTOList;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return parliamentsessiondateDTO;
	}
	
	
	
	
	public List<ParliamentSessionDateDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		ParliamentSessionDateDTO parliamentsessiondateDTO = null;
		List<ParliamentSessionDateDTO> parliamentsessiondateDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return parliamentsessiondateDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				parliamentsessiondateDTO = new ParliamentSessionDateDTO();
				get(parliamentsessiondateDTO, rs);
				System.out.println("got this DTO: " + parliamentsessiondateDTO);
				
				parliamentsessiondateDTOList.add(parliamentsessiondateDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return parliamentsessiondateDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<ParliamentSessionDateDTO> getAllParliamentSessionDate (boolean isFirstReload)
    {
		List<ParliamentSessionDateDTO> parliamentsessiondateDTOList = new ArrayList<>();

		String sql = "SELECT * FROM parliament_session_date";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by parliamentsessiondate.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				ParliamentSessionDateDTO parliamentsessiondateDTO = new ParliamentSessionDateDTO();
				get(parliamentsessiondateDTO, rs);
				
				parliamentsessiondateDTOList.add(parliamentsessiondateDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return parliamentsessiondateDTOList;
    }

	
	public List<ParliamentSessionDateDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<ParliamentSessionDateDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<ParliamentSessionDateDTO> parliamentsessiondateDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				ParliamentSessionDateDTO parliamentsessiondateDTO = new ParliamentSessionDateDTO();
				get(parliamentsessiondateDTO, rs);
				
				parliamentsessiondateDTOList.add(parliamentsessiondateDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return parliamentsessiondateDTOList;
	
	}
				
}
	