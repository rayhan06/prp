package parliament_session;

import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.StringUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class Parliament_sessionRepository implements Repository {
    Parliament_sessionDAO parliament_sessionDAO;

    public void setDAO(Parliament_sessionDAO parliament_sessionDAO) {
        this.parliament_sessionDAO = parliament_sessionDAO;
    }


    static Logger logger = Logger.getLogger(Parliament_sessionRepository.class);
    Map<Long, Parliament_sessionDTO> mapOfParliament_sessionDTOToiD;
    Map<Long, Set<Parliament_sessionDTO>> mapOfParliament_sessionDTOToelectionDetailsId;
    Map<Integer, Set<Parliament_sessionDTO>> mapOfParliament_sessionDTOTosessionNumber;


    static Parliament_sessionRepository instance = null;

    private Parliament_sessionRepository() {
        mapOfParliament_sessionDTOToiD = new ConcurrentHashMap<>();
        mapOfParliament_sessionDTOToelectionDetailsId = new ConcurrentHashMap<>();
        mapOfParliament_sessionDTOTosessionNumber = new ConcurrentHashMap<>();
        parliament_sessionDAO = new Parliament_sessionDAO();
        RepositoryManager.getInstance().addRepository(this);
    }

    public synchronized static Parliament_sessionRepository getInstance() {
        if (instance == null) {
            instance = new Parliament_sessionRepository();
        }
        return instance;
    }

    public void reload(boolean reloadAll) {
        if (parliament_sessionDAO == null) {
            return;
        }
        try {
            List<Parliament_sessionDTO> parliament_sessionDTOs = parliament_sessionDAO.getAllParliament_session(reloadAll);
            for (Parliament_sessionDTO parliament_sessionDTO : parliament_sessionDTOs) {
                Parliament_sessionDTO oldParliament_sessionDTO = getParliament_sessionDTOByID(parliament_sessionDTO.iD);
                if (oldParliament_sessionDTO != null) {
                    mapOfParliament_sessionDTOToiD.remove(oldParliament_sessionDTO.iD);

                    if (mapOfParliament_sessionDTOToelectionDetailsId.containsKey(oldParliament_sessionDTO.electionDetailsId)) {
                        mapOfParliament_sessionDTOToelectionDetailsId.get(oldParliament_sessionDTO.electionDetailsId).remove(oldParliament_sessionDTO);
                    }
                    if (mapOfParliament_sessionDTOToelectionDetailsId.get(oldParliament_sessionDTO.electionDetailsId).isEmpty()) {
                        mapOfParliament_sessionDTOToelectionDetailsId.remove(oldParliament_sessionDTO.electionDetailsId);
                    }

                    if (mapOfParliament_sessionDTOTosessionNumber.containsKey(oldParliament_sessionDTO.sessionNumber)) {
                        mapOfParliament_sessionDTOTosessionNumber.get(oldParliament_sessionDTO.sessionNumber).remove(oldParliament_sessionDTO);
                    }
                    if (mapOfParliament_sessionDTOTosessionNumber.get(oldParliament_sessionDTO.sessionNumber).isEmpty()) {
                        mapOfParliament_sessionDTOTosessionNumber.remove(oldParliament_sessionDTO.sessionNumber);
                    }

                }
                if (parliament_sessionDTO.isDeleted == 0) {

                    mapOfParliament_sessionDTOToiD.put(parliament_sessionDTO.iD, parliament_sessionDTO);

                    if (!mapOfParliament_sessionDTOToelectionDetailsId.containsKey(parliament_sessionDTO.electionDetailsId)) {
                        mapOfParliament_sessionDTOToelectionDetailsId.put(parliament_sessionDTO.electionDetailsId, new HashSet<>());
                    }
                    mapOfParliament_sessionDTOToelectionDetailsId.get(parliament_sessionDTO.electionDetailsId).add(parliament_sessionDTO);

                    if (!mapOfParliament_sessionDTOTosessionNumber.containsKey(parliament_sessionDTO.sessionNumber)) {
                        mapOfParliament_sessionDTOTosessionNumber.put(parliament_sessionDTO.sessionNumber, new HashSet<>());
                    }
                    mapOfParliament_sessionDTOTosessionNumber.get(parliament_sessionDTO.sessionNumber).add(parliament_sessionDTO);
                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public List<Parliament_sessionDTO> getParliament_sessionList() {
        return new ArrayList<>(this.mapOfParliament_sessionDTOToiD.values());
    }


    public Parliament_sessionDTO getParliament_sessionDTOByID(long ID) {
        return mapOfParliament_sessionDTOToiD.get(ID);
    }


    public List<Parliament_sessionDTO> getParliament_sessionDTOByelection_details_id(long election_details_id) {
        return new ArrayList<>(mapOfParliament_sessionDTOToelectionDetailsId.getOrDefault(election_details_id, new HashSet<>()));
    }

    public Map<Long, Parliament_sessionDTO> getByIds(List<Long> ids) {
        return ids.stream()
                  .filter(id -> mapOfParliament_sessionDTOToiD.get(id) != null)
                  .map(mapOfParliament_sessionDTOToiD::get)
                  .collect(Collectors.toMap(e -> e.iD, e -> e));
    }


    public String buildOptions(String language, Long electionId) {
        List<OptionDTO> optionDTOList = null;
        List<Parliament_sessionDTO> parliament_sessionDTOSByElection = getParliament_sessionDTOByelection_details_id(electionId);
        if (parliament_sessionDTOSByElection != null && parliament_sessionDTOSByElection.size() > 0) {
            optionDTOList = parliament_sessionDTOSByElection.stream()
                                                            .map(dto -> new OptionDTO(String.valueOf(dto.sessionNumber), StringUtils.convertToBanNumber(String.valueOf(dto.sessionNumber)), String.valueOf(dto.iD)))
                                                            .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, null);
    }

    public String buildOptionsWithDate(String language, Long electionId, Long selectedId) {
        List<OptionDTO> optionDTOList = null;
        List<Parliament_sessionDTO> parliament_sessionDTOSByElection = getParliament_sessionDTOByelection_details_id(electionId);
        if (parliament_sessionDTOSByElection != null && parliament_sessionDTOSByElection.size() > 0) {
            optionDTOList = parliament_sessionDTOSByElection.stream()
                                                            .map(Parliament_sessionDTO::getOptionDtoWithDate)
                                                            .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String getText(long id, boolean isLangEn) {
        Parliament_sessionDTO parliamentSessionDTO = getParliament_sessionDTOByID(id);
        return parliamentSessionDTO == null ? "" : parliamentSessionDTO.getText(isLangEn);
    }

    public String getSessionNo(long id, boolean isLangEn) {
        Parliament_sessionDTO parliamentSessionDTO = getParliament_sessionDTOByID(id);
        if (parliamentSessionDTO == null) {
            return "";
        }
        String sessionNumber = String.format("%d", parliamentSessionDTO.sessionNumber);
        return isLangEn ? sessionNumber : StringUtils.convertToBanNumber(sessionNumber);
    }

    @Override
    public String getTableName() {
        return "parliament_session";
    }
}


