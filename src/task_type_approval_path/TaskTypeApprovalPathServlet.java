package task_type_approval_path;

import common.BaseServlet;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import task_type_assign.Task_type_assignDTO;
import task_type_assign.Task_type_assignRepository;
import task_type_level.TaskTypeLevelDTO;
import task_type_level.TaskTypeLevelRepository;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("Duplicates")
@WebServlet("/Task_type_approval_pathServlet")
@MultipartConfig
public class TaskTypeApprovalPathServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(TaskTypeApprovalPathServlet.class);

    private final TaskTypeApprovalPathDAO task_typeApprovalPathDAO = TaskTypeApprovalPathDAO.getInstance();

    @Override
    public String getTableName() {
        return task_typeApprovalPathDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Task_type_approval_pathServlet";
    }

    @Override
    public TaskTypeApprovalPathDAO getCommonDAOService() {
        return task_typeApprovalPathDAO;
    }

    @Override
    public boolean getAddPagePermission(HttpServletRequest request) {
        return getAddEditPagePermission(request);
    }

    @Override
    public boolean getEditPagePermission(HttpServletRequest request) {
        return getAddEditPagePermission(request);
    }

    @Override
    public boolean getViewPagePermission(HttpServletRequest request) {
        return getAddEditPagePermission(request);
    }

    private boolean getAddEditPagePermission(HttpServletRequest request) {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        long taskTypeLevelId = Long.parseLong(request.getParameter("taskTypeLevelId"));
        TaskTypeLevelDTO taskTypeLevelDTO = TaskTypeLevelRepository.getInstance().getById(taskTypeLevelId);
        if (taskTypeLevelDTO == null) {
            return false;
        }
        Task_type_assignDTO taskTypeAssignDTO = Task_type_assignRepository.getInstance().getByTaskTypeId(taskTypeLevelDTO.taskTypeId);
        if (taskTypeAssignDTO == null) {
            return false;
        }
        return Stream.of(taskTypeAssignDTO.roleID.split(","))
                .map(String::trim)
                .map(Long::parseLong)
                .filter(role -> role == userDTO.roleID)
                .findAny()
                .orElse(null) != null;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        if (addFlag) {
            return addTask_type_approval_path(request);
        } else {
            return editTask_type_approval_path(request);
        }
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.TASK_TYPE_APPROVAL_PATH_ADD, MenuConstants.APPROVAL_PATH_ASSIGN};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.TASK_TYPE_APPROVAL_PATH_UPDATE, MenuConstants.APPROVAL_PATH_ASSIGN};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.TASK_TYPE_APPROVAL_PATH_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return TaskTypeApprovalPathServlet.class;
    }

    @Override
    public String getAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditRedirectURL(request);
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditRedirectURL(request);
    }

    @Override
    public String getEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditRedirectURL(request);
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditRedirectURL(request);
    }

    private String getAddOrEditRedirectURL(HttpServletRequest request) {
        String data = request.getParameter("data");
        if ("approvalPathAssign".equalsIgnoreCase(data)) {
            return "Approval_path_assignServlet?actionType=search";
        } else {
            return "Task_typeServlet?actionType=search";
        }
    }

    @Override
    public boolean getAddPermission(HttpServletRequest request) {
        return getAddEditPermission(request);
    }

    @Override
    public boolean getEditPermission(HttpServletRequest request) {
        return getAddEditPermission(request);
    }

    private boolean getAddEditPermission(HttpServletRequest request) {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        TaskTypeLevelDTO taskTypeLevelDTO = TaskTypeLevelRepository.getInstance().getById(Long.parseLong(request.getParameter("taskTypeLevelId")));
        Task_type_assignDTO taskTypeAssignDTO = Task_type_assignRepository.getInstance().getByTaskTypeId(taskTypeLevelDTO.taskTypeId);
        if(taskTypeAssignDTO == null){
            return false;
        }
        return Stream.of(taskTypeAssignDTO.roleID.split(","))
                .map(String::trim)
                .map(Long::parseLong)
                .anyMatch(roleId -> roleId == userDTO.roleID);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if ("getEditPage".equals(request.getParameter("actionType"))) {
            if (Utils.checkPermission(getEditPageMenuConstants()) && getAddEditPagePermission(request)) {
                long taskTypeLevelId = Long.parseLong(request.getParameter("taskTypeLevelId"));
                List<TaskTypeApprovalPathDTO> taskApprovalList = TaskTypeApprovalPathRepository.getInstance().getByTaskTypeLevelId(taskTypeLevelId);
                //if (taskApprovalList.size() > 0) {
                    request.setAttribute("taskApprovalList", taskApprovalList);
                    request.getRequestDispatcher(commonPartOfDispatchURL() + "Edit.jsp?actionType=edit").forward(request, response);
                    return;
                //}
            }
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);

        } else {
            super.doGet(request, response);
        }
    }

    private TaskTypeApprovalPathDTO addTask_type_approval_path(HttpServletRequest request) throws Exception {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        TaskTypeApprovalPathDTO task_typeApprovalPathDTO;
        long currentTime = System.currentTimeMillis();
        task_typeApprovalPathDTO = new TaskTypeApprovalPathDTO();
        task_typeApprovalPathDTO.insertionDate = currentTime;
        task_typeApprovalPathDTO.insertedBy = String.valueOf(userDTO.ID);
        task_typeApprovalPathDTO.modifiedBy = String.valueOf(userDTO.ID);
        task_typeApprovalPathDTO.lastModificationTime = currentTime;
        String organograms = request.getParameter("officeUnitOrganogramId");
        if (organograms == null || organograms.trim().length() == 0) {
            throw new Exception(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng ? "Please add employee" : "অনুগ্রহ করে কর্মকর্তা যোগ করুন");
        }
        List<Long> organogramIds = Stream.of(organograms.trim().split(","))
                .map(String::trim)
                .map(Long::parseLong)
                .collect(Collectors.toList());
        task_typeApprovalPathDTO.taskTypeLevelId = Long.parseLong(request.getParameter("taskTypeLevelId"));
        task_typeApprovalPathDTO.level = TaskTypeLevelRepository.getInstance().getById(task_typeApprovalPathDTO.taskTypeLevelId).level;
        addMultipleRows(task_typeApprovalPathDTO, organogramIds);
        return task_typeApprovalPathDTO;
    }

    private TaskTypeApprovalPathDTO editTask_type_approval_path(HttpServletRequest request) throws Exception {
        TaskTypeApprovalPathDTO task_typeApprovalPathDTO;
        long currentTime = System.currentTimeMillis();
        task_typeApprovalPathDTO = new TaskTypeApprovalPathDTO();
        task_typeApprovalPathDTO.modifiedBy = String.valueOf(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.ID);
        task_typeApprovalPathDTO.lastModificationTime = currentTime;
        String organograms = request.getParameter("officeUnitOrganogramId");
        if (organograms == null || organograms.trim().length() == 0) {
            throw new Exception(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng ? "Please add employee" : "অনুগ্রহ করে কর্মকর্তা যোগ করুন");
        }
        List<Long> organogramIds = Stream.of(organograms.trim().split(","))
                .map(String::trim)
                .map(Long::parseLong)
                .collect(Collectors.toList());
        task_typeApprovalPathDTO.taskTypeLevelId = Long.parseLong(request.getParameter("taskTypeLevelId"));
        task_typeApprovalPathDTO.level = TaskTypeLevelRepository.getInstance().getById(task_typeApprovalPathDTO.taskTypeLevelId).level;
        Map<Boolean, List<Long>> alreadyAddedMap = task_typeApprovalPathDAO.deleteDeletedInfoAndReturnAMap(organogramIds, task_typeApprovalPathDTO.taskTypeLevelId,
                task_typeApprovalPathDTO.modifiedBy, task_typeApprovalPathDTO.lastModificationTime);
        addMultipleRows(task_typeApprovalPathDTO, alreadyAddedMap.get(false));
        return task_typeApprovalPathDTO;
    }

    private void addMultipleRows(TaskTypeApprovalPathDTO taskTypeApprovalPathDTO, List<Long> organogramIds) {
        organogramIds.forEach(organogramId -> {
            TaskTypeApprovalPathDTO typeApprovalPathDTO = new TaskTypeApprovalPathDTO();
            typeApprovalPathDTO.taskTypeLevelId = taskTypeApprovalPathDTO.taskTypeLevelId;
            typeApprovalPathDTO.officeUnitOrganogramId = organogramId;
            typeApprovalPathDTO.level = taskTypeApprovalPathDTO.level;
            typeApprovalPathDTO.insertionDate = taskTypeApprovalPathDTO.insertionDate;
            typeApprovalPathDTO.insertedBy = taskTypeApprovalPathDTO.insertedBy;
            typeApprovalPathDTO.lastModificationTime = System.currentTimeMillis();
            typeApprovalPathDTO.modifiedBy = taskTypeApprovalPathDTO.modifiedBy;
            try {
                task_typeApprovalPathDAO.add(typeApprovalPathDTO);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
    }
}