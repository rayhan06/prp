package task_type_approval_path;

import common.ConnectionAndStatementUtil;
import common.EmployeeCommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;
import util.HttpRequestUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"unused","Duplicates"})
public class TaskTypeApprovalPathDAO implements EmployeeCommonDAOService<TaskTypeApprovalPathDTO> {
    private static final Logger logger = Logger.getLogger(TaskTypeApprovalPathDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (task_type_level_id,office_unit_organogram_id,level,"
            .concat("modified_by,lastModificationTime,insertion_date,inserted_by,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery = "UPDATE {tableName} SET task_type_level_id=?,office_unit_organogram_id=?,"
            .concat("level=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final String getByTaskTypeId = "SELECT * FROM  task_type_approval_path WHERE task_type_level_id=%d AND isDeleted=0 order by level";

    String deleteByIds = "UPDATE %s SET isDeleted = 1,modified_by= '%s',lastModificationTime = %d WHERE id IN (%s)";

    private static final String getTaskLevelId = "select distinct task_type_level_id from task_type_approval_path where task_type_level_id in (%s);";

    private static final String deleteByTaskTypeLevelId = "UPDATE task_type_approval_path SET isDeleted = 1, lastModificationTime = %d, modified_by = %d WHERE task_type_level_id IN (%s)";

    private final Map<String, String> searchMap = new HashMap<>();

    private static TaskTypeApprovalPathDAO INSTANCE = null;

    public static TaskTypeApprovalPathDAO getInstance(){
        if(INSTANCE == null){
            synchronized (TaskTypeApprovalPathDAO.class){
                if(INSTANCE == null){
                    INSTANCE = new TaskTypeApprovalPathDAO();
                }
            }
        }
        return INSTANCE;
    }

    private TaskTypeApprovalPathDAO() {
        searchMap.put("task_type_level_id", "and (task_type_level_id = ?)");
        searchMap.put("office_unit_organogram_id", "and (office_unit_organogram_id = ?)");
        searchMap.put("level", "and (level = ?)");
    }



    @Override
    public void set(PreparedStatement ps, TaskTypeApprovalPathDTO task_typeApprovalPathDTO, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setObject(++index, task_typeApprovalPathDTO.taskTypeLevelId);
        ps.setObject(++index, task_typeApprovalPathDTO.officeUnitOrganogramId);
        ps.setObject(++index, task_typeApprovalPathDTO.level);
        ps.setObject(++index, task_typeApprovalPathDTO.modifiedBy);
        ps.setObject(++index, task_typeApprovalPathDTO.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, task_typeApprovalPathDTO.insertionDate);
            ps.setObject(++index, task_typeApprovalPathDTO.insertedBy);
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, task_typeApprovalPathDTO.iD);
    }

    @Override
    public TaskTypeApprovalPathDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            TaskTypeApprovalPathDTO task_typeApprovalPathDTO = new TaskTypeApprovalPathDTO();
            task_typeApprovalPathDTO.iD = rs.getLong("ID");
            task_typeApprovalPathDTO.taskTypeLevelId = rs.getLong("task_type_level_id");
            task_typeApprovalPathDTO.officeUnitOrganogramId = rs.getLong("office_unit_organogram_id");
            task_typeApprovalPathDTO.level = rs.getInt("level");
            task_typeApprovalPathDTO.insertionDate = rs.getLong("insertion_date");
            task_typeApprovalPathDTO.lastModificationTime = rs.getLong("lastModificationTime");
            task_typeApprovalPathDTO.insertedBy = rs.getString("inserted_by");
            task_typeApprovalPathDTO.modifiedBy = rs.getString("modified_by");
            task_typeApprovalPathDTO.isDeleted = rs.getInt("isDeleted");
            return task_typeApprovalPathDTO;

        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "task_type_approval_path";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((TaskTypeApprovalPathDTO) commonDTO, updateQuery, false);
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((TaskTypeApprovalPathDTO) commonDTO, addQuery, true);
    }

    public List<TaskTypeApprovalPathDTO> getDTOsByTaskTypeLevelId(long taskTypeLevelId) {
        return getDTOs(String.format(getByTaskTypeId, taskTypeLevelId));
    }

    void deleteByIds(List<Long> idList, String requestBy) {
        if(idList == null || idList.size() == 0){
            return;
        }
        String ids = idList.stream().map(String::valueOf).collect(Collectors.joining(","));
        long currentTime = System.currentTimeMillis();
        String sql = String.format(deleteByIds, getTableName(), requestBy, currentTime, ids);
        logger.debug("sql : " + sql);
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            try {
                model.getStatement().executeUpdate(sql);
                recordUpdateTime(model.getConnection(),getTableName(),currentTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
    }
    Map<Boolean, List<Long>> deleteDeletedInfoAndReturnAMap(List<Long> organogramList,long taskTypeLevelId, String requestBy, long requestTime) {
        List<TaskTypeApprovalPathDTO> oldDTOList = getDTOsByTaskTypeLevelId(taskTypeLevelId);
        if (organogramList == null || organogramList.size() == 0) {
            if (oldDTOList != null && oldDTOList.size() > 0) {
                List<Long> ids = oldDTOList.stream()
                        .map(e -> e.iD)
                        .collect(Collectors.toList());
                deleteByIds(ids, requestBy);
            }
            return new HashMap<>();
        } else if (oldDTOList == null || oldDTOList.size() == 0) {
            Map<Boolean, List<Long>> booleanListMap = new HashMap<>();
            booleanListMap.put(false, organogramList);
            return booleanListMap;
        } else {
            Map<Long, Long> map = oldDTOList.stream()
                    .collect(Collectors.toMap(dto -> dto.officeUnitOrganogramId, dto -> dto.iD));
            Set<Long> oldSet = map.keySet();

            Set<Long> latestSet = new HashSet<>(organogramList);

            List<Long> deletedIds = oldDTOList.stream()
                    .filter(dto -> !latestSet.contains(dto.officeUnitOrganogramId))
                    .map(dto -> map.get(dto.officeUnitOrganogramId))
                    .collect(Collectors.toList());
            deleteByIds(deletedIds, requestBy);

            return latestSet.stream()
                    .collect(Collectors.partitioningBy(oldSet::contains,
                            Collectors.mapping(e -> e, Collectors.toList())));
        }
    }

    public List<Long> getTaskLevelId(List<Long> ids){
        return ConnectionAndStatementUtil.getDTOListByNumbers(getTaskLevelId,ids,rs->{
            try {
                return rs.getLong(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        });
    }

    public void deleteByTaskTypeLevelId(List<Long> taskTypeLevelIds,long modifiedBy){
        String ids = taskTypeLevelIds.stream()
                .map(String::valueOf)
                .collect(Collectors.joining(","));
        long currentTime = System.currentTimeMillis();
        String sql = String.format(deleteByTaskTypeLevelId,currentTime, modifiedBy,ids);
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model->{
            try {
                logger.debug(sql);
                model.getStatement().executeUpdate(sql);
                recordUpdateTime(model.getConnection(),getTableName(),currentTime);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
    }
}