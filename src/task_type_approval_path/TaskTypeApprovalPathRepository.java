package task_type_approval_path;

import leave_reliever_mapping.Leave_reliever_mappingRepository;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import task_type_level.TaskTypeLevelDTO;
import task_type_level.TaskTypeLevelRepository;
import util.LockManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@SuppressWarnings({"unused"})
public class TaskTypeApprovalPathRepository implements Repository {
    private final TaskTypeApprovalPathDAO taskTypeApprovalPathDAO = TaskTypeApprovalPathDAO.getInstance();
    private static final Logger logger = Logger.getLogger(Leave_reliever_mappingRepository.class);
    private final Map<Long, TaskTypeApprovalPathDTO> mapById;
    private final Map<Long, List<TaskTypeApprovalPathDTO>> mapByTaskTypeLevelId;

    private TaskTypeApprovalPathRepository() {
        mapById = new ConcurrentHashMap<>();
        mapByTaskTypeLevelId = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        static TaskTypeApprovalPathRepository INSTANCE = new TaskTypeApprovalPathRepository();
    }

    public synchronized static TaskTypeApprovalPathRepository getInstance() {
        return TaskTypeApprovalPathRepository.LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        logger.debug("Task Approval Path loading start for reloadAll: " + reloadAll);
        List<TaskTypeApprovalPathDTO> task_typeApprovalPathDTOS = taskTypeApprovalPathDAO.getAllDTOs(reloadAll);
        if (task_typeApprovalPathDTOS != null && task_typeApprovalPathDTOS.size() > 0) {
            task_typeApprovalPathDTOS.stream()
                    .peek(dto -> removeIfPresent(mapById.get(dto.iD)))
                    .filter(dto -> dto.isDeleted == 0)
                    .forEach(dto -> {
                        mapById.put(dto.iD, dto);
                        List<TaskTypeApprovalPathDTO> list = mapByTaskTypeLevelId.getOrDefault(dto.taskTypeLevelId, new ArrayList<>());
                        list.add(dto);
                        mapByTaskTypeLevelId.put(dto.taskTypeLevelId, list);
                    });
            mapByTaskTypeLevelId.values().forEach(ls -> ls.sort(Comparator.comparing(o -> o.level)));
        }
        logger.debug("Task_Approval_Path loading end for reloadAll: " + reloadAll);
    }

    private void removeIfPresent(TaskTypeApprovalPathDTO oldDTO) {
        if (oldDTO == null) {
            return;
        }
        mapById.remove(oldDTO.iD);
        mapByTaskTypeLevelId.values()
                .forEach(childSet -> childSet.remove(oldDTO));

    }

    public TaskTypeApprovalPathDTO getById(long id) {
        if (mapById.get(id) == null) {

            synchronized (LockManager.getLock(id + "TTAPR")) {
                TaskTypeApprovalPathDTO dto = taskTypeApprovalPathDAO.getDTOFromID(id);
                if (dto != null) {
                    mapById.put(id, dto);
                    List<TaskTypeApprovalPathDTO> list = mapByTaskTypeLevelId.getOrDefault(dto.taskTypeLevelId, new ArrayList<>());
                    list.add(dto);
                    list.sort(Comparator.comparingInt(o -> o.level));
                }
            }
        }
        return mapById.get(id);
    }

    public List<TaskTypeApprovalPathDTO> getByTaskTypeLevelId(long taskLevelTypeId) {
        if (mapByTaskTypeLevelId.get(taskLevelTypeId) == null) {
            synchronized (LockManager.getLock(taskLevelTypeId + "CIR")) {
                if (mapByTaskTypeLevelId.get(taskLevelTypeId) == null) {
                    List<TaskTypeApprovalPathDTO> list = taskTypeApprovalPathDAO.getDTOsByTaskTypeLevelId(taskLevelTypeId);
                    if (list != null && list.size() > 0) {
                        mapByTaskTypeLevelId.put(taskLevelTypeId, list);
                        list.forEach(dto -> mapById.put(dto.iD, dto));
                    }
                }
            }
        }
        return new ArrayList<>(mapByTaskTypeLevelId.getOrDefault(taskLevelTypeId, new ArrayList<>()));
    }

    public List<TaskTypeApprovalPathDTO> getByTaskTypeIdAndLevel(long taskTypeId, int level) {
        TaskTypeLevelDTO taskTypeLevelDTO = TaskTypeLevelRepository.getInstance().getByTaskTypeIdAndLevel(taskTypeId, level);
        if (taskTypeLevelDTO == null) {
            return new ArrayList<>();
        }
        return getByTaskTypeLevelId(taskTypeLevelDTO.iD);
    }

    public boolean hasNextApprover(long taskTypeId, int level) {
        return TaskTypeLevelRepository.getInstance().getByTaskTypeIdAndLevel(taskTypeId, level) != null;
    }

    @Override
    public String getTableName() {
        return taskTypeApprovalPathDAO.getTableName();
    }

    public Map<Long,List<TaskTypeApprovalPathDTO>> getMapByTaskTypeLevelId(List<Long> taskTypeLevelIds){
        taskTypeLevelIds = taskTypeLevelIds.stream()
                .distinct()
                .collect(Collectors.toList());
        Map<Long,List<TaskTypeApprovalPathDTO>> map = new HashMap<>();
        for(Long id : taskTypeLevelIds){
            map.put(id,getByTaskTypeLevelId(id));
        }
        return map;
    }

    public Set<Long> isApprovalPathIsNotSetYet(List<Long> taskTypeLevelIds){
        List<Long> notFoundIds = taskTypeLevelIds.stream()
                .distinct()
                .filter(id->mapByTaskTypeLevelId.get(id) == null)
                .collect(Collectors.toList());
        if(notFoundIds.size() == 0){
            return new HashSet<>();
        }
        List<Long> founds = taskTypeApprovalPathDAO.getTaskLevelId(notFoundIds);
        return notFoundIds.stream().filter(id->!founds.contains(id)).collect(Collectors.toSet());
    }
}