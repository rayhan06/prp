package task_type_approval_path;

import util.CommonEmployeeDTO;


public class TaskTypeApprovalPathDTO extends CommonEmployeeDTO {

    public long taskTypeLevelId = 0;
    public long officeUnitOrganogramId = 0;
    public int level = 0;
    public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";


    @Override
    public String toString() {
        return "$Task_type_approval_pathDTO[" +
                " iD = " + iD +
                " taskTypeId = " + taskTypeLevelId +
                " officeUnitOrganogramId = " + officeUnitOrganogramId +
                " level = " + level +
                " insertionDate = " + insertionDate +
                " lastModificationTime = " + lastModificationTime +
                " insertedBy = " + insertedBy +
                " modifiedBy = " + modifiedBy +
                " isDeleted = " + isDeleted +
                "]";
    }

}