package food_bill_finance;

import food_bill.Food_billServlet;
import pb.CatRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;
import util.StringUtils;

@SuppressWarnings({"Duplicates"})
public class Food_bill_financeDTO extends CommonDTO {
    public long budgetMappingId = -1;
    public long economicSubCodeId = -1;
    public long foodBillSubmissionConfigId = -1;
    public int parliamentNumber = -1;
    public int sessionNumber = -1;
    public int foodBillTypeCat = 1;
    public long budgetRegisterId = -1;
    public long billRegisterId = -1;
    public long totalEmployeeCount = 0;
    public long totalBillAmount = 0;

    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = -1;

    public String getText(boolean isLangEn) {
        if (!isLangEn) {
            return String.format(
                    "%s-তম সংসদ | %s-তম অধিবেশন (%s)",
                    StringUtils.convertToBanNumber(String.format("%d", parliamentNumber)),
                    StringUtils.convertToBanNumber(String.format("%d", sessionNumber)),
                    CatRepository.getInstance().getText(false, SessionConstants.FOOD_BILL_TYPE_CAT_DOMAIN_NAME, foodBillTypeCat)
            );
        }
        return String.format(
                "%d-th Parliament | %d-th Session (%s)",
                parliamentNumber,
                sessionNumber,
                CatRepository.getInstance().getText(true, SessionConstants.FOOD_BILL_TYPE_CAT_DOMAIN_NAME, foodBillTypeCat)
        );
    }

    public boolean isDeletableByUser(UserDTO userDTO) {
        return Food_billServlet.isAllowedToSeeAllOfficeBill(userDTO);
    }
}
