package food_bill_finance;

import budget_office.Budget_officeRepository;
import budget_register.Budget_registerDAO;
import budget_register.Budget_registerDTO;
import common.BaseServlet;
import common.CommonDAOService;
import common.CustomException;
import food_allowance.Food_allowanceDAO;
import food_allowance.Food_allowanceModel;
import food_bill.Food_billDAO;
import food_bill.Food_billDTO;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import pb.Utils;
import pbReport.GenerateExcelReportService;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@SuppressWarnings({"Duplicates"})
@WebServlet("/Food_bill_financeServlet")
@MultipartConfig
public class Food_bill_financeServlet extends BaseServlet  implements GenerateExcelReportService {
    private final Logger logger = Logger.getLogger(Food_bill_financeServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        String actionType = request.getParameter("actionType");
        try {
            switch (actionType) {
                case "getForwardingPage":
                    if (Utils.checkPermission(userDTO, MenuConstants.FOOD_BILL_FINANCE)) {
                        long foodBillFinanceId = Utils.parseMandatoryLong(
                                request.getParameter("ID"),
                                "invalid foodBillFinanceId"
                        );
                        Food_bill_financeDTO billFinanceDTO = Food_bill_financeDAO.getInstance().getDTOFromID(foodBillFinanceId);
                        if (billFinanceDTO == null) {
                            throw new Exception("Food_bill_financeDTO missing for id=" + foodBillFinanceId);
                        }
                        List<Food_billDTO> billDTOs = Food_billDAO.getInstance().findByFoodBillFinanceId(foodBillFinanceId);
                        if (billDTOs == null || billDTOs.isEmpty()) {
                            throw new FileNotFoundException("no DTOs found");
                        }
                        request.setAttribute("billDTOs", billDTOs);
                        request.getRequestDispatcher("food_bill_finance/food_bill_financeForwarding.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "getBankStatementPage":
                    if (Utils.checkPermission(userDTO, MenuConstants.FOOD_BILL_FINANCE)) {
                        Long foodBillFinanceId = Utils.parseMandatoryLong(
                                request.getParameter("ID"),
                                "invalid foodBillFinanceId"
                        );
                        Food_bill_financeDTO billFinanceDTO = Food_bill_financeDAO.getInstance().getDTOFromID(foodBillFinanceId);
                        if (billFinanceDTO == null) {
                            throw new Exception("Food_bill_financeDTO missing for id=" + foodBillFinanceId);
                        }
                        request.setAttribute("allowanceModels", getFoodAllowanceModelForBankStatement(foodBillFinanceId));
                        request.setAttribute("budgetRegisterId", billFinanceDTO.budgetRegisterId);
                        request.setAttribute("foodBillFinanceId", foodBillFinanceId);
                        request.getRequestDispatcher("food_bill_finance/food_bill_financeBankStatement.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "getBankStatementXl": {
                    Long foodBillFinanceId = Utils.parseMandatoryLong(
                            request.getParameter("ID"),
                            "invalid foodBillFinanceId"
                    );
                    Food_bill_financeDTO billFinanceDTO = Food_bill_financeDAO.getInstance().getDTOFromID(foodBillFinanceId);
                    if (billFinanceDTO == null) {
                        throw new Exception("Food_bill_financeDTO missing for id=" + foodBillFinanceId);
                    }
                    Budget_registerDTO budgetRegisterDTO = Budget_registerDAO.getInstance().getDTOFromID(billFinanceDTO.budgetRegisterId);
                    List<Food_allowanceModel> allowanceModel = getFoodAllowanceModelForBankStatement(foodBillFinanceId);

                    List<List<Object>> excelRows =
                            IntStream.range(0, allowanceModel.size())
                                     .mapToObj(index -> allowanceModel.get(index).getExcelRow(index))
                                     .collect(Collectors.toList());
                    excelRows.add(0, Food_allowanceModel.excelTitleRow);

                    XSSFWorkbook workbook = createXl(excelRows, null, budgetRegisterDTO.description, "bangla", userDTO);
                    response.setContentType("application/vnd.ms-excel");
                    String fileName = "Food Bill Bank Statement - "
                                      + Budget_officeRepository.getInstance().getText(budgetRegisterDTO.budgetOfficeId, "english")
                                      + " - "
                                      + billFinanceDTO.getText(true).replaceAll(Pattern.quote("|"), "-")
                                      + ".xlsx";
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                    workbook.write(response.getOutputStream());
                    workbook.close();
                    return;
                }
                case "view":
                case "search":
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        String actionType = request.getParameter("actionType");
        try {
            if ("ajax_deleteBill".equals(actionType)) {
                Map<String, Object> res = deleteBill(request, userDTO);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().print(gson.toJson(res));
            }
            return;
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    public void deleteBillDtoOrThrow(String idStr, UserDTO userDTO, boolean isLangEn) throws Exception {
        long id = Utils.parseMandatoryLong(idStr, "invalid id=" + idStr);
        Food_bill_financeDTO billFinanceDTO = Food_bill_financeDAO.getInstance().getDTOFromID(id);
        if (billFinanceDTO == null) {
            throw new CustomException(isLangEn ? "Already deleted!" : "ইতোমধ্যে ডিলিট করা হয়েছে!");
        }
        if (!billFinanceDTO.isDeletableByUser(userDTO)) {
            throw new CustomException(isLangEn ? "You are not allowed to delete" : "আপনার ডিলিট করার অনুমতি নেই");
        }
        boolean isDeleted = Food_bill_financeDAO.getInstance().deleteBillById(id, userDTO.ID, System.currentTimeMillis());
        if (!isDeleted) {
            throw new RuntimeException("failed to delete Food_bill_financeDTO.id=" + id);
        }
    }

    private Map<String, Object> deleteBill(HttpServletRequest request, UserDTO userDTO) {
        boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Map<String, Object> res = new HashMap<>();
        res.put("success", true);
        res.put("message", isLangEn ? "Deleted successfully" : "সফলভাবে ডিলিট করা হয়েছে");

        try {
            deleteBillDtoOrThrow(request.getParameter("ID"), userDTO, isLangEn);
        } catch (CustomException customException) {
            logger.error(customException);
            res.put("success", false);
            res.put("message", customException.getMessage());
        } catch (Exception e) {
            logger.error(e);
            res.put("success", false);
            res.put("message", isLangEn ? "Failed to delete" : "ডিলিট করতে ব্যর্থ হয়েছে");
        }
        return res;
    }

    private List<Food_allowanceModel> getFoodAllowanceModelForBankStatement(long foodBillFinanceId) throws FileNotFoundException {
        List<Food_billDTO> billDTOs = Food_billDAO.getInstance().findByFoodBillFinanceId(foodBillFinanceId);
        if (billDTOs == null || billDTOs.isEmpty()) {
            throw new FileNotFoundException("no DTOs found");
        }
        return billDTOs.stream()
                       .sequential()
                       .sorted(Comparator.comparingInt(billDto -> billDto.financeSerialNumber))
                       .flatMap(foodBillDTO ->
                               Food_allowanceDAO
                                       .getInstance()
                                       .findByFoodBillId(foodBillDTO.iD)
                                       .stream()
                                       .sequential()
                                       .sorted(Comparator.comparingInt(allowanceDto -> allowanceDto.serialNo))
                       ).map(dto -> new Food_allowanceModel(dto, "BANGLA"))
                       .collect(Collectors.toList());
    }

    @Override
    public String getTableName() {
        return getCommonDAOService().getTableName();
    }

    @Override
    public String getServletName() {
        return "Food_bill_financeServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return Food_bill_financeDAO.getInstance();
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.FOOD_BILL_FINANCE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Food_bill_financeServlet.class;
    }
}
