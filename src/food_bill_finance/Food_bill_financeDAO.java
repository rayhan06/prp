package food_bill_finance;

import common.CommonDAOService;
import food_bill.Food_billDAO;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Food_bill_financeDAO implements CommonDAOService<Food_bill_financeDTO> {
    private static final Logger logger = Logger.getLogger(Food_bill_financeDAO.class);

    private static final Map<String, String> searchMap = new HashMap<>();

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (budget_mapping_id,economic_sub_code_id,food_bill_submission_config_id,"
                    .concat("parliament_number,session_number,food_bill_type_cat,budget_register_id,bill_register_id,")
                    .concat("total_employee_count,total_bill_amount,modified_by,lastModificationTime,")
                    .concat("inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET budget_mapping_id=?,economic_sub_code_id=?,food_bill_submission_config_id=?,"
                    .concat("parliament_number=?,session_number=?,food_bill_type_cat=?,budget_register_id=?,bill_register_id=?,")
                    .concat("total_employee_count=?,total_bill_amount=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private Food_bill_financeDAO() {
        searchMap.put("budgetMappingId", " AND (budget_mapping_id = ?) ");
        searchMap.put("economicSubCodeId", " AND (economic_sub_code_id = ?) ");
        searchMap.put("foodBillSubmissionConfigId", " AND (food_bill_submission_config_id = ?) ");
        searchMap.put("foodBillTypeCat", " AND (food_bill_type_cat = ?) ");
    }

    private static class LazyLoader {
        static final Food_bill_financeDAO INSTANCE = new Food_bill_financeDAO();
    }

    public static Food_bill_financeDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Food_bill_financeDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.budgetMappingId);
        ps.setLong(++index, dto.economicSubCodeId);
        ps.setLong(++index, dto.foodBillSubmissionConfigId);
        ps.setInt(++index, dto.parliamentNumber);
        ps.setInt(++index, dto.sessionNumber);
        ps.setInt(++index, dto.foodBillTypeCat);
        ps.setLong(++index, dto.budgetRegisterId);
        ps.setLong(++index, dto.billRegisterId);
        ps.setLong(++index, dto.totalEmployeeCount);
        ps.setLong(++index, dto.totalBillAmount);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Food_bill_financeDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Food_bill_financeDTO dto = new Food_bill_financeDTO();
            dto.iD = rs.getLong("ID");
            dto.budgetMappingId = rs.getLong("budget_mapping_id");
            dto.economicSubCodeId = rs.getLong("economic_sub_code_id");
            dto.foodBillSubmissionConfigId = rs.getLong("food_bill_submission_config_id");
            dto.parliamentNumber = rs.getInt("parliament_number");
            dto.sessionNumber = rs.getInt("session_number");
            dto.foodBillTypeCat = rs.getInt("food_bill_type_cat");
            dto.budgetRegisterId = rs.getLong("budget_register_id");
            dto.billRegisterId = rs.getLong("bill_register_id");
            dto.totalEmployeeCount = rs.getLong("total_employee_count");
            dto.totalBillAmount = rs.getLong("total_bill_amount");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "food_bill_finance";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Food_bill_financeDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Food_bill_financeDTO) commonDTO, updateSqlQuery, false);
    }

    public boolean deleteBillById(long id, long modifiedBy, long modificationTime) {
        Food_billDAO.getInstance().financeBillDeleted(id, modifiedBy, modificationTime);
        return delete(modifiedBy, id, modificationTime);
    }
}
