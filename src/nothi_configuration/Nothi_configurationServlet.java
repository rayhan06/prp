package nothi_configuration;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;


import permission.MenuConstants;

import user.UserDTO;
import util.*;
import javax.servlet.http.*;

import common.ApiResponse;
import common.BaseServlet;
import nothi.NothiDAO;
import nothi.NothiDTO;
import nothi_history.*;


import com.google.gson.Gson;
import pb.*;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Nothi_configurationServlet
 */
@WebServlet("/Nothi_configurationServlet")
@MultipartConfig
public class Nothi_configurationServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Nothi_configurationServlet.class);

    @Override
    public String getTableName() {
        return Nothi_configurationDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Nothi_configurationServlet";
    }

    @Override
    public Nothi_configurationDAO getCommonDAOService() {
        return Nothi_configurationDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.NOTHI_CONFIGURATION_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.NOTHI_CONFIGURATION_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.NOTHI_CONFIGURATION_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Nothi_configurationServlet.class;
    }
    private final Gson gson = new Gson();
 	
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        init(request);
        String actionType = request.getParameter("actionType");
		if ("getDetails".equals(actionType)) {
			try {
				long id = Long.parseLong(request.getParameter("id"));
				Nothi_configurationDTO nothi_configurationDTO = Nothi_configurationDAO.getInstance().getDTOByID(id);
				if(nothi_configurationDTO != null)
				{
					response.getWriter().write(nothi_configurationDTO.description);
				}
				else
				{
					response.getWriter().write("");
				}
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}	
		else
		{
			super.doGet(request,response);
		}
    }

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addNothi_configuration");
		String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		boolean isLangEng = language.equalsIgnoreCase("english");
		Nothi_configurationDTO nothi_configurationDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
		if(addFlag)
		{
			nothi_configurationDTO = new Nothi_configurationDTO();
		}
		else
		{
			nothi_configurationDTO = Nothi_configurationDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		Value = request.getParameter("nothiNumber");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("nothiNumber = " + Value);
		if(Value != null)
		{
			nothi_configurationDTO.nothiNumber = (Value);
			if(!nothi_configurationDTO.nothiNumber.matches("^[\\.0-9\\u09E6-\\u09FF]*$"))
			{
				throw new Exception(isLangEng?"Only numbers and dots are allowed in Nothi Number":
					"নথি নম্বরে কেবলমাত্র সংখ্যা আর ডট গ্রহণযোগ্য"
					);
			}
			nothi_configurationDTO.nothiNumber= Utils.getDigits(nothi_configurationDTO.nothiNumber, "english");
			Nothi_configurationDTO oldNothi_configurationDTO = Nothi_configurationDAO.getInstance().getByNothiNumber(nothi_configurationDTO.nothiNumber, nothi_configurationDTO.iD);
			if(oldNothi_configurationDTO != null)
			{
				throw new Exception(isLangEng?"Duplicate Nothi Configuration Found with This Number":
					"এ নম্বর দিয়ে আরেকটি নথি কনফিগারেশন পাওয়া গেছে"
					);
			}
			
			
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("description");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("description = " + Value);
		if(Value != null)
		{
			nothi_configurationDTO.description = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		if(addFlag)
		{
			nothi_configurationDTO.insertedByUserId = userDTO.ID;		
			nothi_configurationDTO.insertedByOrganogramId = userDTO.organogramID;				
			nothi_configurationDTO.insertionDate = TimeConverter.getToday();
		}			


		nothi_configurationDTO.lastModifierUser = userDTO.userName;

		System.out.println("Done adding  addNothi_configuration dto = " + nothi_configurationDTO);

		if(addFlag == true)
		{
			Nothi_configurationDAO.getInstance().add(nothi_configurationDTO);
		}
		else
		{
			NothiDTO nothiDTO = NothiDAO.getInstance().getByNothiConfigId(nothi_configurationDTO.iD);
			
			if(nothiDTO != null)
			{
				nothiDTO.set(nothi_configurationDTO);
				NothiDAO.getInstance().update(nothiDTO);
				List<Nothi_historyDTO> nothi_historyDTOs = Nothi_historyDAO.getInstance().getAllByNothiConfigId(nothi_configurationDTO.iD);
				for(Nothi_historyDTO nothi_historyDTO: nothi_historyDTOs)
				{
					nothi_historyDTO.setNumber(nothiDTO);
					Nothi_historyDAO.getInstance().update(nothi_historyDTO);
				}
			}
			
			Nothi_configurationDAO.getInstance().update(nothi_configurationDTO);										
		}
		
		

		return nothi_configurationDTO;

	}
}

