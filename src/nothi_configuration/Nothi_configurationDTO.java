package nothi_configuration;

import util.*; 


public class Nothi_configurationDTO extends CommonDTO
{

    public String nothiNumber = "";
    public String description = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
    public String searchColumn = "";
	
	
    @Override
	public String toString() {
            return "$Nothi_configurationDTO[" +
            " iD = " + iD +
            " nothiNumber = " + nothiNumber +
            " description = " + description +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}