package nothi_configuration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import language.LC;
import language.LM;

import org.apache.log4j.Logger;

import util.*;
import pb.*;

public class Nothi_configurationDAO  implements CommonDAOService<Nothi_configurationDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Nothi_configurationDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"nothi_number",
			"description",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("nothi_number"," and (nothi_number like ?)");
		searchMap.put("description"," and (description like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Nothi_configurationDAO INSTANCE = new Nothi_configurationDAO();
	}

	public static Nothi_configurationDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Nothi_configurationDTO nothi_configurationDTO)
	{
		nothi_configurationDTO.searchColumn = "";
		nothi_configurationDTO.searchColumn += nothi_configurationDTO.nothiNumber + " ";
		nothi_configurationDTO.searchColumn += Utils.getDigits(nothi_configurationDTO.nothiNumber, "bangla") + " ";
		nothi_configurationDTO.searchColumn += nothi_configurationDTO.description + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Nothi_configurationDTO nothi_configurationDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(nothi_configurationDTO);
		if(isInsert)
		{
			ps.setObject(++index,nothi_configurationDTO.iD);
		}
		ps.setObject(++index,nothi_configurationDTO.nothiNumber);
		ps.setObject(++index,nothi_configurationDTO.description);
		ps.setObject(++index,nothi_configurationDTO.insertedByUserId);
		ps.setObject(++index,nothi_configurationDTO.insertedByOrganogramId);
		ps.setObject(++index,nothi_configurationDTO.insertionDate);
		ps.setObject(++index,nothi_configurationDTO.lastModifierUser);
		ps.setObject(++index,nothi_configurationDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(++index,nothi_configurationDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,nothi_configurationDTO.iD);
		}
	}
	
	@Override
	public Nothi_configurationDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Nothi_configurationDTO nothi_configurationDTO = new Nothi_configurationDTO();
			int i = 0;
			nothi_configurationDTO.iD = rs.getLong(columnNames[i++]);
			nothi_configurationDTO.nothiNumber = rs.getString(columnNames[i++]);
			nothi_configurationDTO.description = rs.getString(columnNames[i++]);
			nothi_configurationDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			nothi_configurationDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			nothi_configurationDTO.insertionDate = rs.getLong(columnNames[i++]);
			nothi_configurationDTO.lastModifierUser = rs.getString(columnNames[i++]);
			nothi_configurationDTO.searchColumn = rs.getString(columnNames[i++]);
			nothi_configurationDTO.isDeleted = rs.getInt(columnNames[i++]);
			nothi_configurationDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return nothi_configurationDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Nothi_configurationDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "nothi_configuration";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Nothi_configurationDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Nothi_configurationDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
	
	public String getOptions(boolean isLangEng, List<Nothi_configurationDTO> nothi_configurationDTOs)
	{
		String options = "<option value = '-1'>" 
				+ LM.getText(LC.HM_SELECT, isLangEng)
				+ "</option>";
		
		if(nothi_configurationDTOs != null)
		{
			for(Nothi_configurationDTO nothi_configurationDTO: nothi_configurationDTOs)
			{
				options += "<option value ='" + nothi_configurationDTO.iD + "'>"
						+ (Utils.getDigits(nothi_configurationDTO.nothiNumber, isLangEng))
						+ "</option>";

			}
		}
		
		return options;
	}
	
	public String getUnusedOptions(boolean isLangEng, long exceptId)
	{
		return getOptions(isLangEng, getAllUnused(exceptId));
	}
	
	public String getUnusedOptions(boolean isLangEng)
	{
		return getOptions(isLangEng, getAllUnused(-1));
	}
	
	public String getOptions(boolean isLangEng)
	{
		return getOptions(isLangEng, getAllDTOs());
	}
	
	public List<Nothi_configurationDTO> getAllUnused (long exceptId)
    {
		String sql = "SELECT * FROM " + getTableName() + " WHERE isDeleted = 0  " 
				+ " and id not in "
				+ "(select nothi_configuration_type from nothi where isDeleted = 0 and nothi_configuration_type != " + exceptId + ")";

		return ConnectionAndStatementUtil.getListOfT(sql,this::buildObjectFromResultSet);	
    }
	
	public Nothi_configurationDTO getByNothiNumber (String nothiNumber)
    {
		String sql = "SELECT * FROM " + getTableName() + " WHERE isDeleted = 0  " 
				+ " and nothi_number = '" +nothiNumber + "'";

		return ConnectionAndStatementUtil.getT(sql,this::buildObjectFromResultSet);	
    }
	
	public Nothi_configurationDTO getByNothiNumber (String nothiNumber, long id)
    {
		String sql = "SELECT * FROM " + getTableName() + " WHERE isDeleted = 0  " 
				+ " and nothi_number = '" +nothiNumber + "' and id != " + id;

		return ConnectionAndStatementUtil.getT(sql,this::buildObjectFromResultSet);	
    }
				
}
	