package mail;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;

public class EmailAttachment {

    private String name;
    private String contentType;
    private byte[] data;
    private InputStream inputStream;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) throws IOException {

        if( inputStream != null ) {
            this.data = IOUtils.toByteArray(inputStream);
            this.inputStream = inputStream;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
