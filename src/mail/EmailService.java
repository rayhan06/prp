package mail;

import com.sun.mail.imap.IMAPFolder;
import common.StringUtils;
import email_log.EmailInboxDTO;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import pb.Utils;
import sms.SmsService;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.URLDataSource;
import javax.mail.*;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class EmailService {

    private static String mailHost = null;
    private static String mailPort = null;
    private static String inboxMailPort = null;
    private static String mailUsername = null;
    private static String mailPassword = null;
    private static final Logger logger = Logger.getLogger(EmailService.class);

    static{

        Properties mailProps = new Properties();
        InputStream is = SmsService.class.getResourceAsStream( "/app.properties" );
        try {

            mailProps.load(is);
            mailHost = mailProps.getProperty( "mail.host" );
            mailPort = mailProps.getProperty( "mail.port" );
            inboxMailPort = mailProps.getProperty( "mail.port.imap" );
            mailUsername = mailProps.getProperty( "mail.username" );
            mailPassword = mailProps.getProperty( "mail.password" );
        }
        catch (IOException e) {

            logger.error("",e);
        }
    }

    /**
     * Returns a Properties object which is configured for a POP3/IMAP server
     *
     * @param protocol either "imap" or "pop3"
     * @param host
     * @param port
     * @return a Properties object
     */
    private Properties getServerProperties(String protocol, String host,
                                           String port) {
        Properties properties = new Properties();

        // server setting
        properties.put(String.format("mail.%s.host", protocol), host);
        properties.put(String.format("mail.%s.port", protocol), port);

        // SSL setting
        properties.setProperty(
                String.format("mail.%s.socketFactory.class", protocol),
                "javax.net.ssl.SSLSocketFactory");
        properties.setProperty(
                String.format("mail.%s.socketFactory.fallback", protocol),
                "false");
        properties.setProperty(
                String.format("mail.%s.socketFactory.port", protocol),
                String.valueOf(port));

        return properties;
    }

    /**
     * Downloads new messages and fetches details for each message.
     *
     * @param userName
     * @param password
     */
    public List<EmailInboxDTO> downloadEmails(String userName, String password) {
    	if(!Utils.isValidUserName(userName))
		{
			return null;
		}
        String protocol = "imap";
        String host = mailHost;
        String port = inboxMailPort;
        Properties properties = getServerProperties(protocol, host, port);
        Session session = Session.getDefaultInstance(properties);
        List<EmailInboxDTO> emailInboxDTOS = new ArrayList<>();
        try {
            // connects to the message store
            Store store = session.getStore(protocol);
            store.connect(host, Integer.parseInt(port), userName, password);

            // opens the inbox folder
            IMAPFolder folderInbox = (IMAPFolder) store.getFolder("INBOX");
            folderInbox.open(Folder.READ_WRITE);

            // fetches new messages from server
            Message[] messages = folderInbox.getMessages();

            for (Message message : messages) {
                EmailInboxDTO emailInboxDTO = new EmailInboxDTO();
                Long messageId = ((UIDFolder) folderInbox).getUID(message);
                logger.debug(messageId);
                Address[] fromAddress = message.getFrom();
                emailInboxDTO.setFrom(fromAddress[0].toString());
                emailInboxDTO.setTo(parseAddresses(message.getRecipients(RecipientType.TO)));
                emailInboxDTO.setCc(parseAddresses(message.getRecipients(RecipientType.CC)));
                emailInboxDTO.setBcc(parseAddresses(message.getRecipients(RecipientType.BCC)));
                emailInboxDTO.setSubject(message.getSubject());
                emailInboxDTO.setSentDate(message.getSentDate() == null ? 0 : message.getSentDate().getTime());
                emailInboxDTO.setUid(messageId);
                emailInboxDTO.setSeen(message.getFlags().contains(Flags.Flag.SEEN));
//                emailInboxDTO.setText(getBodyText((MimeMultipart) msg.getContent()));

                emailInboxDTO.setContentType(message.getContentType());
//                emailInboxDTO.messageContent = getTextFromMessage(msg);
                emailInboxDTOS.add(emailInboxDTO);

            }

            // disconnect
            folderInbox.close(false);
            store.close();
        } catch (NoSuchProviderException ex) {
            logger.debug("No provider for protocol: " + protocol);
            logger.error("",ex);
        } catch (MessagingException ex) {
            logger.debug("Could not connect to the message store");
            logger.error("",ex);
        }
        return emailInboxDTOS;
    }

    public String getTextFromMessage(Message message) throws MessagingException, IOException {
        String result = "";
        if (message.isMimeType("text/plain")) {
            result = message.getContent().toString();
        } else if (message.isMimeType("multipart/*")) {
            MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
            result = getTextFromMimeMultipart(mimeMultipart);
        }
        return result;
    }

    public String getBodyText(MimeMultipart mimeMultipart) throws IOException, MessagingException {
        BodyPart bodyPart = mimeMultipart.getBodyPart(0);
        String result = "";
        if (bodyPart.isMimeType("text/plain")) {
            result = result + "\n" + bodyPart.getContent();
        } else if (bodyPart.isMimeType("text/html")) {
            String html = (String) bodyPart.getContent();
            result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
        } else if (bodyPart.getContent() instanceof MimeMultipart){
            result = result + getBodyText((MimeMultipart)bodyPart.getContent());
        }
        return result;
    }

    public List<EmailAttachment> getAttachments(MimeMultipart mimeMultipart) throws MessagingException, IOException{
        List<EmailAttachment> attachments = new ArrayList<>();
        int count = mimeMultipart.getCount();
        for (int i = 1; i < count; i++) {
            EmailAttachment attachment = new EmailAttachment();
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);

            if(!Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition()) &&
                    StringUtils.isBlank(bodyPart.getFileName())) {
                continue; // dealing with attachments only
            }

            attachment.setContentType(bodyPart.getContentType());
            attachment.setName(bodyPart.getFileName());
            InputStream is = null;
            byte[] data = null;

            OutputStream os = new ByteArrayOutputStream();
            IOUtils.copy( bodyPart.getInputStream(), os );
            data = ((ByteArrayOutputStream)os).toByteArray();

            attachment.setData( data );

            attachments.add(attachment);
        }
        return attachments;
    }

    private String getTextFromMimeMultipart(MimeMultipart mimeMultipart)  throws MessagingException, IOException{
        String result = "";
        int count = mimeMultipart.getCount();
        for (int i = 0; i < count; i++) {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            String contentType = bodyPart.getContentType();
            if (bodyPart.isMimeType("text/plain")) {
                result = result + "\n" + bodyPart.getContent();
//                return result; // without break same text appears twice in my tests
            } else if (bodyPart.isMimeType("text/html")) {
                String html = (String) bodyPart.getContent();
                result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
            } else if (bodyPart.getContent() instanceof MimeMultipart){
                result = result + getTextFromMimeMultipart((MimeMultipart)bodyPart.getContent());
            } else if (bodyPart.isMimeType("image/*")){
                result = result ;//+ getTextFromMimeMultipart((MimeMultipart)bodyPart.getContent());
            } else if (bodyPart.isMimeType("application/*")){
                result = result;// + getTextFromMimeMultipart((MimeMultipart)bodyPart.getContent());
            }
        }
        return result;
    }

    /**
     * Returns a list of addresses in String format separated by comma
     *
     * @param address an array of Address objects
     * @return a string represents a list of addresses
     */
    public String parseAddresses(Address[] address) {
        String listAddress = "";

        if (address != null) {
            listAddress = Stream.of(address)
                    .map(Address::toString)
                    .collect(Collectors.joining(","));
        }
        if (listAddress.length() > 1) {
            listAddress = listAddress.substring(0, listAddress.length() - 2);
        }

        return listAddress;
    }

    public void sendMail(SendEmailDTO sendEmailDTO ){

    	if(sendEmailDTO.getTo() == null)
    	{
    		return;
    	}
        Properties props = new Properties();

        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", mailHost);
        props.put("mail.smtp.port", mailPort );
        props.put("mail.smtp.socketFactory.port", mailPort ); //SSL Port
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

        // Get the Session object.
        Session session = Session.getInstance( props ,
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(mailUsername, mailPassword);
                }
            }
        );

        try {
            Message message = new MimeMessage(session);
            Multipart multipart = new MimeMultipart();

            message.setFrom( new InternetAddress( mailUsername ) );

            Address[] toAddresses = InternetAddress.parse( sendEmailDTO.getTo() == null? "": String.join( ",", sendEmailDTO.getTo() ) );
            Address[] ccAddresses = InternetAddress.parse( sendEmailDTO.getCc() == null? "": String.join( ",", sendEmailDTO.getCc() ) );
            Address[] bccAddresses = InternetAddress.parse( sendEmailDTO.getBcc() == null? "":String.join( ",", sendEmailDTO.getBcc() ) );

            message.setRecipients(RecipientType.TO, toAddresses );
            message.setRecipients(RecipientType.CC, ccAddresses );
            message.setRecipients(RecipientType.BCC, bccAddresses );

            message.setSubject( sendEmailDTO.getSubject() );

            BodyPart messageBodyPart = new MimeBodyPart();

            messageBodyPart.setContent( sendEmailDTO.getText(), "text/html; charset=utf-8" );

            multipart.addBodyPart(messageBodyPart);

            if( sendEmailDTO.getAttachments() != null && sendEmailDTO.getAttachments().size() > 0 ) {
                attachFiles(sendEmailDTO, multipart);
            }

            message.setContent(multipart);

            Transport.send(message);

            logger.debug("Sent mail successfully....");

        } catch (MessagingException | MalformedURLException e) {
            logger.error("",e);
            throw new RuntimeException(e);
        }
    }

    private void attachFiles(SendEmailDTO sendEmailDTO, Multipart multipart) throws MessagingException, MalformedURLException {

        int count = 1;
        for( EmailAttachment emailAttachment : sendEmailDTO.getAttachments() ){

            if( emailAttachment.getData() != null && emailAttachment.getData().length > 0 ) {
                BodyPart messageBodyPart = new MimeBodyPart();
                DataSource source = new ByteArrayDataSource(emailAttachment.getData(), emailAttachment.getContentType());
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName( count + "_" + emailAttachment.getName());
                count++;
                multipart.addBodyPart(messageBodyPart);
            }

            else if( emailAttachment.getUrl() != null && emailAttachment.getUrl().length() > 0 ){

                URL url = new URL(emailAttachment.getUrl() );

                BodyPart messageBodyPart = new MimeBodyPart();

                DataSource source = new URLDataSource( url );

                messageBodyPart.setDataHandler( new DataHandler( source ) );
                messageBodyPart.setFileName( emailAttachment.getName() );
                messageBodyPart.setDisposition( Part.ATTACHMENT );
                messageBodyPart.setFileName( emailAttachment.getName() );
                multipart.addBodyPart( messageBodyPart );
            }
        }
    }

    public Message getMessageByUID(Long uid, String userName, String password){
    	if(!Utils.isValidUserName(userName))
		{
			return null;
		}
        String protocol = "imap";
        String host = mailHost;
        String port = inboxMailPort;
        Properties properties = getServerProperties(protocol, host, port);
        Session session = Session.getDefaultInstance(properties);
        try {
            // connects to the message store
            Store store = session.getStore(protocol);
            store.connect(host, Integer.parseInt(port), userName, password);

            // opens the inbox folder
            IMAPFolder folderInbox = (IMAPFolder) store.getFolder("INBOX");
            folderInbox.open(Folder.READ_WRITE);

            // fetches new messages from server
            Message message = ((UIDFolder) folderInbox).getMessageByUID(uid);
            folderInbox.setFlags(new Message[] {message}, new Flags(Flags.Flag.SEEN), true);
            return message;


        } catch (NoSuchProviderException ex) {
            logger.debug("No provider for protocol: " + protocol);
            logger.error("",ex);
        } catch (MessagingException ex) {
            logger.debug("Could not connect to the message store");
            logger.error("",ex);
        }

        return null;
    }

    /**
     * Test downloading e-mail messages
     */
    public static void main(String[] args) throws Exception {
        // for POP3
        //String protocol = "pop3";
        //String host = "pop.gmail.com";
        //String port = "995";

        // for IMAP
        String protocol = "imap";
        String host = "mail.pbrlp.gov.bd";
        String port = "993";


        String userName = "edms@pbrlp.gov.bd";
        String password = "ZYy9nGozC87#";

        EmailService receiver = new EmailService();

        List<EmailInboxDTO> emailInboxDTOS = receiver.downloadEmails(userName, password);
        Message message = receiver.getMessageByUID(emailInboxDTOS.get(0).getUid(), userName, password);
        List<File> attachments = new ArrayList<File>();
        String body = receiver.getTextFromMessage(message);

        receiver.downloadEmails(userName, password);

        SendEmailDTO sendEmailDTO = new SendEmailDTO();
        sendEmailDTO.setFrom( "edms@pbrlp.gov.bd" );
        sendEmailDTO.to = new String[]{"alam.cse09@gmail.com"}; //new String[]{"saifun@revesoft.com"};
        //sendEmailDTO.cc = new String[]{"alam.cse09@gmail.com", "mahadi.hassan@revesoft.com"};
        //sendEmailDTO.bcc = new String[]{"alam.cse09@gmail.com", "maalam@revesoft.com"};
        sendEmailDTO.subject = "Testing Auto PDF attachment";
        sendEmailDTO.text = "EDMS MAIL TEST, check if you have an attachment with this email";

        /*FilesDTO filesDTO = (FilesDTO) new FilesDAO().getDTOByID( 14400L );

        EmailAttachment emailAttachment = new EmailAttachment();
        emailAttachment.setName( filesDTO.fileTitle );
        emailAttachment.setContentType(filesDTO.fileTypes);
        emailAttachment.setInputStream( filesDTO.inputStream );

        EmailAttachment emailAttachment1 = new EmailAttachment();
        emailAttachment1.setName( "TestImage.png" );
        emailAttachment1.setUrl( "https://images.unsplash.com/photo-1494253109108-2e30c049369b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80" );*/

        EmailAttachment emailAttachment2 = new EmailAttachment();
        emailAttachment2.setName( "DraftLetter_1800.pdf" );
        emailAttachment2.setContentType( "application/pdf" );
        //emailAttachment2.setData( new Noc_infoDAO().getByteArrayFromPdf( 100L ) );

        List<EmailAttachment> emailAttachments = new ArrayList<>();
        /*emailAttachments.add( emailAttachment );
        emailAttachments.add( emailAttachment1 );*/
        emailAttachments.add( emailAttachment2 );

        sendEmailDTO.setAttachments( emailAttachments );

        receiver.sendMail( sendEmailDTO );
        //receiver.downloadEmails(protocol, host, port, userName, password);
    }
}
