package organogram_role_mapping;
import java.util.*; 
import util.*; 


public class Organogram_role_mappingDTO extends CommonDTO
{

	public long organogramId = 0;
	public long roleId = 0;
	
	
    @Override
	public String toString() {
            return "$Organogram_role_mappingDTO[" +
            " iD = " + iD +
            " organogramId = " + organogramId +
            " roleId = " + roleId +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}