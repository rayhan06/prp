package organogram_role_mapping;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import approval_module_map.*;

public class Organogram_role_mappingDAO  extends NavigationService3
{
	
	Logger logger = Logger.getLogger(getClass());

	
	public Organogram_role_mappingDAO(String tableName, String tempTableName, Approval_module_mapDTO approval_module_mapDTO)
	{
		super(tableName, tempTableName, approval_module_mapDTO);		
	}
	
	
	
	public long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception
	{
		
		Organogram_role_mappingDTO organogram_role_mappingDTO = (Organogram_role_mappingDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			organogram_role_mappingDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "organogram_id";
			sql += ", ";
			sql += "role_id";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
			if(tempTableDTO!=null)
			{
				sql += ", permanent_table_id, operation_type, approval_path_type, approval_order";
			}
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			if(tempTableDTO!=null)
			{
				sql += ", " + tempTableDTO.permanent_table_id + ", " + tempTableDTO.operation_type + ", " + tempTableDTO.approval_path_type + ", " + tempTableDTO.approval_order;
			}
			sql += ")";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,organogram_role_mappingDTO.iD);
			ps.setObject(index++,organogram_role_mappingDTO.organogramId);
			ps.setObject(index++,organogram_role_mappingDTO.roleId);
			ps.setObject(index++,organogram_role_mappingDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection,  lastModificationTime, tableName);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return organogram_role_mappingDTO.iD;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID, String tableName) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Organogram_role_mappingDTO organogram_role_mappingDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				organogram_role_mappingDTO = new Organogram_role_mappingDTO();

				organogram_role_mappingDTO.iD = rs.getLong("ID");
				organogram_role_mappingDTO.organogramId = rs.getLong("organogram_id");
				organogram_role_mappingDTO.roleId = rs.getLong("role_id");
				organogram_role_mappingDTO.isDeleted = rs.getInt("isDeleted");
				organogram_role_mappingDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return organogram_role_mappingDTO;
	}
	
	public long update(CommonDTO commonDTO, String tableName) throws Exception
	{		
		Organogram_role_mappingDTO organogram_role_mappingDTO = (Organogram_role_mappingDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "organogram_id=?";
			sql += ", ";
			sql += "role_id=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + organogram_role_mappingDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,organogram_role_mappingDTO.organogramId);
			ps.setObject(index++,organogram_role_mappingDTO.roleId);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, lastModificationTime, tableName);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return organogram_role_mappingDTO.iD;
	}
	
	public List<Organogram_role_mappingDTO> getDTOs(Collection recordIDs)
	{
		return getDTOs(recordIDs, tableName);
	}
	
	public List<Organogram_role_mappingDTO> getDTOs(Collection recordIDs, String tableName){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Organogram_role_mappingDTO organogram_role_mappingDTO = null;
		List<Organogram_role_mappingDTO> organogram_role_mappingDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return organogram_role_mappingDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				organogram_role_mappingDTO = new Organogram_role_mappingDTO();
				organogram_role_mappingDTO.iD = rs.getLong("ID");
				organogram_role_mappingDTO.organogramId = rs.getLong("organogram_id");
				organogram_role_mappingDTO.roleId = rs.getLong("role_id");
				organogram_role_mappingDTO.isDeleted = rs.getInt("isDeleted");
				organogram_role_mappingDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + organogram_role_mappingDTO);
				
				organogram_role_mappingDTOList.add(organogram_role_mappingDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return organogram_role_mappingDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Organogram_role_mappingDTO> getAllOrganogram_role_mapping (boolean isFirstReload)
    {
		List<Organogram_role_mappingDTO> organogram_role_mappingDTOList = new ArrayList<>();

		String sql = "SELECT * FROM organogram_role_mapping";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by organogram_role_mapping.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Organogram_role_mappingDTO organogram_role_mappingDTO = new Organogram_role_mappingDTO();
				organogram_role_mappingDTO.iD = rs.getLong("ID");
				organogram_role_mappingDTO.organogramId = rs.getLong("organogram_id");
				organogram_role_mappingDTO.roleId = rs.getLong("role_id");
				organogram_role_mappingDTO.isDeleted = rs.getInt("isDeleted");
				organogram_role_mappingDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				organogram_role_mappingDTOList.add(organogram_role_mappingDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return organogram_role_mappingDTOList;
    }
	
	public List<Organogram_role_mappingDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset)
	{
		return getDTOs(p_searchCriteria, limit, offset, tableName, true, 0);
	}
	
	public List<Organogram_role_mappingDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Organogram_role_mappingDTO> organogram_role_mappingDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, tableName, isPermanentTable, userApprovalPathType);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Organogram_role_mappingDTO organogram_role_mappingDTO = new Organogram_role_mappingDTO();
				organogram_role_mappingDTO.iD = rs.getLong("ID");
				organogram_role_mappingDTO.organogramId = rs.getLong("organogram_id");
				organogram_role_mappingDTO.roleId = rs.getLong("role_id");
				organogram_role_mappingDTO.isDeleted = rs.getInt("isDeleted");
				organogram_role_mappingDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				organogram_role_mappingDTOList.add(organogram_role_mappingDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return organogram_role_mappingDTOList;
	
	}

		
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType)
    {
		Organogram_role_mappingMAPS maps = new Organogram_role_mappingMAPS(tableName);
		String joinSQL = "";
		return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, tableName, isPermanentTable, userApprovalPathType, maps, joinSQL);
    }			
}
	