package organogram_role_mapping;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Organogram_role_mappingRepository implements Repository {
	Organogram_role_mappingDAO organogram_role_mappingDAO = null;
	
	public void setDAO(Organogram_role_mappingDAO organogram_role_mappingDAO)
	{
		this.organogram_role_mappingDAO = organogram_role_mappingDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Organogram_role_mappingRepository.class);
	Map<Long, Organogram_role_mappingDTO>mapOfOrganogram_role_mappingDTOToiD;
	Map<Long, Set<Organogram_role_mappingDTO> >mapOfOrganogram_role_mappingDTOToorganogramId;
	Map<Long, Set<Organogram_role_mappingDTO> >mapOfOrganogram_role_mappingDTOToroleId;
	Map<Long, Set<Organogram_role_mappingDTO> >mapOfOrganogram_role_mappingDTOTolastModificationTime;


	static Organogram_role_mappingRepository instance = null;  
	private Organogram_role_mappingRepository(){
		mapOfOrganogram_role_mappingDTOToiD = new ConcurrentHashMap<>();
		mapOfOrganogram_role_mappingDTOToorganogramId = new ConcurrentHashMap<>();
		mapOfOrganogram_role_mappingDTOToroleId = new ConcurrentHashMap<>();
		mapOfOrganogram_role_mappingDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Organogram_role_mappingRepository getInstance(){
		if (instance == null){
			instance = new Organogram_role_mappingRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(organogram_role_mappingDAO == null)
		{
			return;
		}
		try {
			List<Organogram_role_mappingDTO> organogram_role_mappingDTOs = organogram_role_mappingDAO.getAllOrganogram_role_mapping(reloadAll);
			for(Organogram_role_mappingDTO organogram_role_mappingDTO : organogram_role_mappingDTOs) {
				Organogram_role_mappingDTO oldOrganogram_role_mappingDTO = getOrganogram_role_mappingDTOByID(organogram_role_mappingDTO.iD);
				if( oldOrganogram_role_mappingDTO != null ) {
					mapOfOrganogram_role_mappingDTOToiD.remove(oldOrganogram_role_mappingDTO.iD);
				
					if(mapOfOrganogram_role_mappingDTOToorganogramId.containsKey(oldOrganogram_role_mappingDTO.organogramId)) {
						mapOfOrganogram_role_mappingDTOToorganogramId.get(oldOrganogram_role_mappingDTO.organogramId).remove(oldOrganogram_role_mappingDTO);
					}
					if(mapOfOrganogram_role_mappingDTOToorganogramId.get(oldOrganogram_role_mappingDTO.organogramId).isEmpty()) {
						mapOfOrganogram_role_mappingDTOToorganogramId.remove(oldOrganogram_role_mappingDTO.organogramId);
					}
					
					if(mapOfOrganogram_role_mappingDTOToroleId.containsKey(oldOrganogram_role_mappingDTO.roleId)) {
						mapOfOrganogram_role_mappingDTOToroleId.get(oldOrganogram_role_mappingDTO.roleId).remove(oldOrganogram_role_mappingDTO);
					}
					if(mapOfOrganogram_role_mappingDTOToroleId.get(oldOrganogram_role_mappingDTO.roleId).isEmpty()) {
						mapOfOrganogram_role_mappingDTOToroleId.remove(oldOrganogram_role_mappingDTO.roleId);
					}
					
					if(mapOfOrganogram_role_mappingDTOTolastModificationTime.containsKey(oldOrganogram_role_mappingDTO.lastModificationTime)) {
						mapOfOrganogram_role_mappingDTOTolastModificationTime.get(oldOrganogram_role_mappingDTO.lastModificationTime).remove(oldOrganogram_role_mappingDTO);
					}
					if(mapOfOrganogram_role_mappingDTOTolastModificationTime.get(oldOrganogram_role_mappingDTO.lastModificationTime).isEmpty()) {
						mapOfOrganogram_role_mappingDTOTolastModificationTime.remove(oldOrganogram_role_mappingDTO.lastModificationTime);
					}
					
					
				}
				if(organogram_role_mappingDTO.isDeleted == 0) 
				{
					
					mapOfOrganogram_role_mappingDTOToiD.put(organogram_role_mappingDTO.iD, organogram_role_mappingDTO);
				
					if( ! mapOfOrganogram_role_mappingDTOToorganogramId.containsKey(organogram_role_mappingDTO.organogramId)) {
						mapOfOrganogram_role_mappingDTOToorganogramId.put(organogram_role_mappingDTO.organogramId, new HashSet<>());
					}
					mapOfOrganogram_role_mappingDTOToorganogramId.get(organogram_role_mappingDTO.organogramId).add(organogram_role_mappingDTO);
					
					if( ! mapOfOrganogram_role_mappingDTOToroleId.containsKey(organogram_role_mappingDTO.roleId)) {
						mapOfOrganogram_role_mappingDTOToroleId.put(organogram_role_mappingDTO.roleId, new HashSet<>());
					}
					mapOfOrganogram_role_mappingDTOToroleId.get(organogram_role_mappingDTO.roleId).add(organogram_role_mappingDTO);
					
					if( ! mapOfOrganogram_role_mappingDTOTolastModificationTime.containsKey(organogram_role_mappingDTO.lastModificationTime)) {
						mapOfOrganogram_role_mappingDTOTolastModificationTime.put(organogram_role_mappingDTO.lastModificationTime, new HashSet<>());
					}
					mapOfOrganogram_role_mappingDTOTolastModificationTime.get(organogram_role_mappingDTO.lastModificationTime).add(organogram_role_mappingDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Organogram_role_mappingDTO> getOrganogram_role_mappingList() {
		List <Organogram_role_mappingDTO> organogram_role_mappings = new ArrayList<Organogram_role_mappingDTO>(this.mapOfOrganogram_role_mappingDTOToiD.values());
		return organogram_role_mappings;
	}
	
	
	public Organogram_role_mappingDTO getOrganogram_role_mappingDTOByID( long ID){
		return mapOfOrganogram_role_mappingDTOToiD.get(ID);
	}
	
	
	public List<Organogram_role_mappingDTO> getOrganogram_role_mappingDTOByorganogram_id(long organogram_id) {
		return new ArrayList<>( mapOfOrganogram_role_mappingDTOToorganogramId.getOrDefault(organogram_id,new HashSet<>()));
	}
	
	
	public List<Organogram_role_mappingDTO> getOrganogram_role_mappingDTOByrole_id(long role_id) {
		return new ArrayList<>( mapOfOrganogram_role_mappingDTOToroleId.getOrDefault(role_id,new HashSet<>()));
	}
	
	
	public List<Organogram_role_mappingDTO> getOrganogram_role_mappingDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfOrganogram_role_mappingDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "organogram_role_mapping";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


