package budget_selection_info;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import user.UserDTO;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class BudgetSelectionInfoDAO implements CommonDAOService<BudgetSelectionInfoDTO> {
    private static final Logger logger = Logger.getLogger(BudgetSelectionInfoDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (economic_year, is_submitted, "
            .concat("modified_by, lastModificationTime, inserted_by, insertion_time, isDeleted, ID) ")
            .concat("VALUES (?,?,?,?,?,?,?,?)");

    private static final String updateQuery = "UPDATE {tableName} SET economic_year = ?, is_submitted = ?, "
            .concat("modified_by = ?, lastModificationTime = ? WHERE ID = ?");

    private static final String getByEconomicYear = "SELECT * FROM budget_selection_info WHERE economic_year = ? and isDeleted=0";

    private static BudgetSelectionInfoDAO INSTANCE = null;

    public static BudgetSelectionInfoDAO getInstance() {
        if (INSTANCE == null) {
            synchronized (BudgetSelectionInfoDAO.class) {
                if (INSTANCE == null) {
                    INSTANCE = new BudgetSelectionInfoDAO();
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, BudgetSelectionInfoDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setString(++index, dto.economicYear);
        ps.setBoolean(++index, dto.isSubmitted);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public BudgetSelectionInfoDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            BudgetSelectionInfoDTO dto = new BudgetSelectionInfoDTO();

            dto.iD = rs.getLong("ID");
            dto.economicYear = rs.getString("economic_year");
            dto.isSubmitted = rs.getBoolean("is_submitted");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");

            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "budget_selection_info";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return null;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((BudgetSelectionInfoDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((BudgetSelectionInfoDTO) commonDTO, updateQuery, false);
    }

    public List<BudgetSelectionInfoDTO> getAllBudgetSelectionInfo(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }

    public BudgetSelectionInfoDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }

    public BudgetSelectionInfoDTO getDTOByEconomicYear(String economicYear) {
        return ConnectionAndStatementUtil.getT(getByEconomicYear, Collections.singletonList(economicYear), this::buildObjectFromResultSet);
    }

    public BudgetSelectionInfoDTO getOrCreateAndGetDTO(String economicYear, UserDTO userDTO) throws Exception {
        BudgetSelectionInfoDTO dto = getDTOByEconomicYear(economicYear);
        if (dto == null) {
            dto = new BudgetSelectionInfoDTO();
            dto.economicYear = economicYear;
            dto.isSubmitted = false;
            dto.insertedBy = dto.modifiedBy = userDTO.ID;
            dto.insertionTime = dto.lastModificationTime = System.currentTimeMillis();
            add(dto);
        }
        return dto;
    }
}