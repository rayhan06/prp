CREATE TABLE budget_selection_info
(
    ID                   BIGINT(20) PRIMARY KEY,
    economic_year        VARCHAR(30) UNIQUE ,
    is_submitted         TINYINT    DEFAULT 0,
    isDeleted            INT(11)    DEFAULT 0,
    inserted_by          BIGINT(20) DEFAULT 0,
    insertion_time       BIGINT(20) DEFAULT -62135791200000,
    modified_by          BIGINT(20) DEFAULT 0,
    lastModificationTime BIGINT(20) DEFAULT -62135791200000
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;