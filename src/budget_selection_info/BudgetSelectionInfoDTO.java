package budget_selection_info;

import pb.OptionDTO;
import sessionmanager.SessionConstants;
import util.CommonDTO;
import util.StringUtils;

public class BudgetSelectionInfoDTO extends CommonDTO {
    public String economicYear = "";
    public boolean isSubmitted = false;
    public long insertedBy = 0;
    public long insertionTime = SessionConstants.MIN_DATE;
    public long modifiedBy = 0;

    public OptionDTO getOptionDTO() {
        return new OptionDTO(
                economicYear,
                StringUtils.convertToBanNumber(economicYear),
                String.valueOf(iD)
        );
    }
}
