package budget_selection_info;

import budget.BudgetUtils;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;
import util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class BudgetSelectionInfoRepository implements Repository {
    private static final Logger logger = Logger.getLogger(BudgetSelectionInfoRepository.class);

    private final BudgetSelectionInfoDAO dao;
    private final Map<Long, BudgetSelectionInfoDTO> mapById;
    private final Map<String, BudgetSelectionInfoDTO> mapByEconomicYear;

    private BudgetSelectionInfoRepository() {
        dao = BudgetSelectionInfoDAO.getInstance();
        mapById = new ConcurrentHashMap<>();
        mapByEconomicYear = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        static final BudgetSelectionInfoRepository INSTANCE = new BudgetSelectionInfoRepository();
    }

    public static BudgetSelectionInfoRepository getInstance() {
        return BudgetSelectionInfoRepository.LazyLoader.INSTANCE;
    }

    @Override
    public void reload(boolean reloadAll) {
        logger.debug("BudgetSelectionInfoRepository reload start for, reloadAll : " + reloadAll);
        List<BudgetSelectionInfoDTO> dtoList = dao.getAllBudgetSelectionInfo(reloadAll);
        if (dtoList != null && dtoList.size() > 0) {
            dtoList.stream()
                   .peek(this::removeIfPresent)
                   .filter(dto -> dto.isDeleted == 0)
                   .forEach(this::storeInCache);
        }
        logger.debug("BudgetSelectionInfoRepository reload end for, reloadAll : " + reloadAll);
    }

    private void removeIfPresent(BudgetSelectionInfoDTO newDTO) {
        if (newDTO == null) return;
        if (mapById.get(newDTO.iD) != null) {
            mapById.remove(newDTO.iD);
        }
        if (mapByEconomicYear.get(newDTO.economicYear) != null) {
            mapByEconomicYear.remove(newDTO.economicYear);
        }
    }

    private void storeInCache(BudgetSelectionInfoDTO dto) {
        if (dto != null) {
            mapById.put(dto.iD, dto);
            mapByEconomicYear.put(dto.economicYear, dto);
        }
    }

    public List<BudgetSelectionInfoDTO> getAllDTOs() {
        return new ArrayList<>(mapById.values());
    }

    public BudgetSelectionInfoDTO getDTOByID(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "BSIDBI")) {
                if (mapById.get(id) == null) {
                    BudgetSelectionInfoDTO dto = dao.getDTOByID(id);
                    storeInCache(dto);
                }
            }
        }
        return mapById.get(id);
    }

    public BudgetSelectionInfoDTO getDTOByEconomicYear(String economicYear) {
        if (mapByEconomicYear.get(economicYear) == null) {
            synchronized (LockManager.getLock(economicYear + "EYEAR")) {
                if (mapByEconomicYear.get(economicYear) == null) {
                    BudgetSelectionInfoDTO dto = dao.getDTOByEconomicYear(economicYear);
                    storeInCache(dto);
                }
            }
        }
        return mapByEconomicYear.get(economicYear);
    }

    public Long getRunningYearSelectionId() {
        return getId(BudgetUtils.getRunningEconomicYear());
    }

    public Long getId(String economicYear) {
        BudgetSelectionInfoDTO selectionInfoDTO = getDTOByEconomicYear(economicYear);
        return selectionInfoDTO == null ? null : selectionInfoDTO.iD;
    }

    @Override
    public String getTableName() {
        return "budget_selection_info";
    }

    private List<OptionDTO> getOptionDTOs(Predicate<BudgetSelectionInfoDTO> filter) {
        return mapById.values()
                      .stream()
                      .filter(filter)
                      .sorted((a, b) -> a.economicYear.compareToIgnoreCase(b.economicYear))
                      .map(BudgetSelectionInfoDTO::getOptionDTO)
                      .collect(Collectors.toList());
    }

    public String buildEconomicYears(String language, Long selectedId) {
        return Utils.buildOptions(
                getOptionDTOs(e -> true),
                language,
                selectedId == null ? null : String.valueOf(selectedId)
        );
    }

    public String buildOptionForSubmittedEconomicYears(String language, Long selectedId) {
        return Utils.buildOptions(
                getOptionDTOs(e -> e.isSubmitted),
                language,
                selectedId == null ? null : String.valueOf(selectedId)
        );
    }

    public String getEconomicYearById(String language, Long selectionInfoId) {
        BudgetSelectionInfoDTO budgetSelectionInfoDTO = getDTOByID(selectionInfoId);
        if (budgetSelectionInfoDTO == null) return "";
        return StringUtils.convertBanglaIfLanguageIsBangla(language, budgetSelectionInfoDTO.economicYear);
    }
}
