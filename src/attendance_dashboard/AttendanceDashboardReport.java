package attendance_dashboard;

import attendance_alert_details.Attendance_alert_detailsDAO;
import employee_attendance.EmployeeAttendanceByDate;
import employee_attendance.EmployeeLastAttendanceSummary;
import employee_attendance.Employee_attendanceDAO;
import employee_attendance.Employee_attendanceDTO;
import employee_leave_details.Employee_leave_detailsDAO;
import employee_leave_details.Employee_leave_detailsDTO;
import holiday.HolidayRepository;
import test_lib.util.Pair;
import util.TimeFormat;

import java.util.*;

public class AttendanceDashboardReport {



    public Map<Long, Map<Long,EmployeeAttendanceByDate>> getAllEmployeeAttendanceMapInRange(long fromDate, long toDate) {

        Map<Long, Map<Long,EmployeeAttendanceByDate>> employeeAttendanceMap = new HashMap<>();

        while(fromDate<=toDate) {

            List<Employee_attendanceDTO> allAttendance = new Employee_attendanceDAO().getAttendanceDetailsByDate(fromDate);

            constructEmployeesAttendanceMap(employeeAttendanceMap, allAttendance, fromDate);

            fromDate += (24*60*60*1000); // +1 day
        }

        return employeeAttendanceMap;
    }

    private void constructEmployeesAttendanceMap(Map<Long, Map<Long,EmployeeAttendanceByDate>> employeeAttendanceMap,List<Employee_attendanceDTO> attendanceData, long date) {

        Map<Long, Pair<String,String>> employeeWithTimeMap = new HashMap<>();

        Set<Long> approvedEmployee = new HashSet<>();
        long employeeRecordsId;

        for(Employee_attendanceDTO dto: attendanceData) {

            employeeRecordsId = dto.employeeRecordsType;

            if(dto.isApproved==1) {
                approvedEmployee.add(employeeRecordsId);
            }

            if(employeeWithTimeMap.containsKey(employeeRecordsId)) {

                Pair bothTime = employeeWithTimeMap.get(employeeRecordsId);
                employeeWithTimeMap.put(employeeRecordsId,
                        new Pair(new Employee_attendanceDAO().getMinTime(dto.inTime,bothTime.getKey().toString()),
                                new Employee_attendanceDAO().getMaxTime(dto.inTime,bothTime.getValue().toString())));
            } else {

                employeeWithTimeMap.put(employeeRecordsId,new Pair(new Employee_attendanceDAO().getMinTime(dto.inTime,dto.outTime),new Employee_attendanceDAO().getMaxTime(dto.inTime,dto.outTime)));

            }
        }

        for(Map.Entry<Long, Pair<String,String>> entry: employeeWithTimeMap.entrySet()) {

            EmployeeAttendanceByDate employeeAttendanceByDate = new EmployeeAttendanceByDate();
            employeeAttendanceByDate.isPresent = true;

            if(!approvedEmployee.contains(entry.getKey())) {

                employeeAttendanceByDate.isLate = new Attendance_alert_detailsDAO().isEmployeeLate(entry.getKey(),date, TimeFormat.getInAmPmFormat(entry.getValue().getKey()), false);
                employeeAttendanceByDate.isEarlyLeave = new Attendance_alert_detailsDAO().isEmployeeLeaveEarly(entry.getKey(),date,TimeFormat.getInAmPmFormat(entry.getValue().getValue()), false);
            }
            if(!employeeAttendanceMap.containsKey(entry.getKey())) {

                employeeAttendanceMap.put(entry.getKey(), new HashMap<>());
            }
            employeeAttendanceMap.get(entry.getKey()).put(date, employeeAttendanceByDate);
        }
    }

    public EmployeeLastAttendanceSummary getEmployeeAttendanceSummary( long fromDate, long toDate, Map<Long, Map<Long,EmployeeAttendanceByDate>> employeeAttendanceMap,long employeeRecordsId) {

        EmployeeLastAttendanceSummary summary = new EmployeeLastAttendanceSummary();

        Map<Long,EmployeeAttendanceByDate> employeeDateWiseAttendanceMap = employeeAttendanceMap.get(employeeRecordsId);
        Map<Long,String> holidayMap = HolidayRepository.getInstance().getHolidayToDescriptionMap();
        List<Employee_leave_detailsDTO> leaveDetailsDTOS =  Employee_leave_detailsDAO.getInstance().getByEmployeeId(employeeRecordsId);

        long currentDateInMs = fromDate;

        int totalWorkingDays = 0;
        int totalPresent = 0;
        int totalLate = 0;
        int totalEarlyOut = 0;

        while(currentDateInMs <= toDate ){

            if(!holidayMap.containsKey(currentDateInMs)) {

                totalWorkingDays++;

                if(employeeDateWiseAttendanceMap != null && employeeDateWiseAttendanceMap.containsKey(currentDateInMs)) {

                    totalPresent++;
                    EmployeeAttendanceByDate attendanceStatus = employeeDateWiseAttendanceMap.get(currentDateInMs);

                    if(attendanceStatus.isLate) totalLate++;
                    if(attendanceStatus.isEarlyLeave) totalEarlyOut++;

                } else {
                    boolean leaveStatus = new Employee_attendanceDAO().isEmployeeOnLeave(leaveDetailsDTOS, currentDateInMs);

                    if(leaveStatus) totalPresent++;
                }
            }

            currentDateInMs += (24*60*60*1000); // +1 day
        }

        summary.totalWorkingDays = totalWorkingDays;
        summary.totalPresent = totalPresent;
        summary.totalLate = totalLate;
        summary.totalEarlyOut = totalEarlyOut;

        return summary;
    }
}
