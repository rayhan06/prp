package attendance_dashboard;

import com.google.gson.Gson;
import employee_assign.EmployeeAssignDTO;
import employee_attendance.*;
import employee_offices.EmployeeOfficeRepository;
import job_applicant_application.Job_applicant_applicationDAO;
import login.LoginDTO;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import recruitment_dashboard.PositionWiseStatusDTO;
import sessionmanager.SessionConstants;
import support_ticket.Support_ticketDAO;
import test_lib.util.Pair;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;


@WebServlet("/AttendanceDashboardServlet")
@MultipartConfig
public class AttendanceDashboardServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(AttendanceDashboardServlet.class);
	private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AttendanceDashboardServlet()
	{
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		try
		{

			String actionType = request.getParameter("actionType");
			if(actionType.equals("main"))
			{

				request.getRequestDispatcher("attendance_dashboard/attendance_dashboard.jsp").forward(request, response);

			} else if(actionType.equals("employeeMonthlyAttendance")) {
				String language = request.getParameter("language");
				LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
				UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
				MonthlyAttendanceDTO dto = getEmployeeMonthlyAttendanceComparison(language,userDTO);
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				String encoded = this.gson.toJson(dto);
				PrintWriter out = response.getWriter();
				out.print(encoded);
				out.flush();
				out.close();
			} else if(actionType.equals("departmentWiseReport")) {
				String reportType = request.getParameter("reportType");
				String month = request.getParameter("month");
				String year = request.getParameter("year");
				String officeUnitId = request.getParameter("officeUnitId");
				DepartmentWiseReportDTO dto;
				if(reportType.equals(AttendanceDashboardConstant.DEPARTMENT_ALL_REPORT)) {
					dto = getDepartmentAllReport(officeUnitId,month,year);
				} else {
					dto = getDepartmentWiseReport(officeUnitId, reportType,month,year);
				}
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				String encoded = this.gson.toJson(dto);
				PrintWriter out = response.getWriter();
				out.print(encoded);
				out.flush();
				out.close();
			} else if(actionType.equals("employeeAttendanceHistory")) {
				String language = request.getParameter("language");
				LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
				UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
				AttendanceComparison dto = getEmployeeAttendanceHistory(language,userDTO);
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				String encoded = this.gson.toJson(dto);
				PrintWriter out = response.getWriter();
				out.print(encoded);
				out.flush();
				out.close();
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

	}

	private AttendanceComparison getEmployeeAttendanceHistory(String language,UserDTO userDTO) {

        long today = new Employee_attendanceDAO().getCurrentTimeInMs(false);
        long sub = 24*60*60*29;
        sub = sub * 1000;
        long firstDay = today - sub;

        AttendanceReportDetails attendanceReportDetails = new Employee_attendanceDAO().getEmployeeAttendanceHistoryInRange(userDTO.employee_record_id, firstDay, today);

		AttendanceComparison attendanceComparison = new AttendanceComparison();

		attendanceComparison.workingDays = attendanceReportDetails.workingDays;
		attendanceComparison.presentDays = attendanceReportDetails.presentDays;
		attendanceComparison.absentDays = attendanceReportDetails.absentDays;
		attendanceComparison.lateInDays = attendanceReportDetails.totalLateInRange;
		attendanceComparison.earlyOutDays = attendanceReportDetails.totalEarlyOutRange;

		return attendanceComparison;
	}

    private MonthlyAttendanceDTO getEmployeeMonthlyAttendanceComparison(String language,UserDTO userDTO) {
		Calendar c = Calendar.getInstance();
		List<String> monthNameEn = Arrays.asList("","January","February","March","April","May","June","July","August","September","October","November","December");
		List<String> monthNameBn = Arrays.asList("","জানুয়ারি","ফেব্রুয়ারি","মার্চ","এপ্রিল","মে","জুন","জুলাই","আগস্ট","সেপ্টেম্বার","অক্টোবার","নভেমবার","ডিসেম্বার");
		int currentMonth = Calendar.getInstance().get(Calendar.MONTH)+1;
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		int lastXMonth = 4;
		MonthlyAttendanceDTO monthlyAttendanceDTO = new MonthlyAttendanceDTO();
		while (lastXMonth>0) {
			long firstDay = new Employee_attendanceDAO().constructDateTime(String.valueOf(currentMonth), String.valueOf(currentYear),true);
			long lastDay = new Employee_attendanceDAO().constructDateTime(String.valueOf(currentMonth), String.valueOf(currentYear),false);
			AttendanceReportDetails report = new Employee_attendanceDAO().employeePresentAbsentReport(userDTO.employee_record_id,firstDay,lastDay);
			AttendanceComparison attendanceComparison = new AttendanceComparison();
			if(language.equalsIgnoreCase("English")) {
				attendanceComparison.month = monthNameEn.get(currentMonth);
				attendanceComparison.presentDays = report.presentDays;
				attendanceComparison.workingDays = report.workingDays;
			} else {
				attendanceComparison.month = monthNameBn.get(currentMonth);
				attendanceComparison.presentDays = report.presentDays;
				attendanceComparison.workingDays = report.workingDays;
			}

			monthlyAttendanceDTO.data.add(attendanceComparison);
			currentMonth--;
			if(currentMonth==0) {
				currentYear--;
				currentMonth=12;
			}
			lastXMonth--;
		}
		return monthlyAttendanceDTO;
	}

	private DepartmentWiseReportDTO getDepartmentAllReport(String officeUnitIdStr,String month, String year) {

		Pair<Long,Long> dateRange = parseDateRangeFromSearchParam(month,year);
		long fromDate = dateRange.getKey();
		long toDate = dateRange.getValue();

		Map<Long, Map<Long, EmployeeAttendanceByDate>> employeeAttendanceMap;

		if(fromDate==-1 || toDate==-1) {
			EmployeeAttendanceRepository.getInstance().updateAttendanceCache();
			employeeAttendanceMap = EmployeeAttendanceRepository.getInstance().getEmployeeAttendanceLast30DaysMap();
		} else {
			employeeAttendanceMap = new AttendanceDashboardReport().getAllEmployeeAttendanceMapInRange( fromDate, toDate );
		}

		long officeUnitId = 1;
		if(officeUnitIdStr!=null && !officeUnitIdStr.isEmpty() && officeUnitIdStr.matches("[0-9]+" )) {
			officeUnitId = Long.parseLong(officeUnitIdStr);
		}

		DepartmentWithAttendanceReport lateReport = getDepartmentAttendanceReport( fromDate, toDate, officeUnitId, AttendanceDashboardConstant.LATE_IN_REPORT, employeeAttendanceMap );
		DepartmentWithAttendanceReport earlyOutReport = getDepartmentAttendanceReport( fromDate, toDate, officeUnitId, AttendanceDashboardConstant.EARLY_OUT_REPORT, employeeAttendanceMap );
		DepartmentWithAttendanceReport absentReport = getDepartmentAttendanceReport( fromDate, toDate, officeUnitId, AttendanceDashboardConstant.ABSENT_REPORT, employeeAttendanceMap );

		DepartmentWithAttendanceReport report = new DepartmentWithAttendanceReport();
		report.latePercent = lateReport.latePercent;
		report.earlyOutPercent = earlyOutReport.earlyOutPercent;
		report.absentPercent = absentReport.absentPercent;

		DepartmentWiseReportDTO departmentWiseReportDTO = new DepartmentWiseReportDTO();
		departmentWiseReportDTO.data.add(report);

		return departmentWiseReportDTO;
	}

	private DepartmentWiseReportDTO getDepartmentWiseReport(String officeUnitId,String reportType, String month, String year) {

		//List<Office_unitsDTO> office_unitsDTOs = getAllOfficeUnits();
		List<Office_unitsDTO> office_unitsDTOs = new ArrayList<>();

		long officeUnit = 1;

		if(officeUnitId!=null && !officeUnitId.isEmpty() && officeUnitId.matches("[0-9]+")) {
			officeUnit = Long.parseLong(officeUnitId);
		}

		office_unitsDTOs = Office_unitsRepository.getInstance().getChildList(officeUnit);
        if(office_unitsDTOs==null) {
			office_unitsDTOs = Arrays.asList(Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnit));
		}
		Pair<Long,Long> dateRange = parseDateRangeFromSearchParam(month,year);

		long fromDate = dateRange.getKey();
		long toDate = dateRange.getValue();

		Map<Long, Map<Long, EmployeeAttendanceByDate>> employeeAttendanceMap;

		if(fromDate==-1 || toDate==-1) {
			EmployeeAttendanceRepository.getInstance().updateAttendanceCache();
			employeeAttendanceMap = EmployeeAttendanceRepository.getInstance().getEmployeeAttendanceLast30DaysMap();
		} else {
			employeeAttendanceMap = new AttendanceDashboardReport().getAllEmployeeAttendanceMapInRange( fromDate, toDate );
		}

		DepartmentWiseReportDTO departmentWiseReportDTO = new DepartmentWiseReportDTO();

		if(office_unitsDTOs==null) return departmentWiseReportDTO;

		for(Office_unitsDTO dto: office_unitsDTOs) {
			DepartmentWithAttendanceReport report = getDepartmentAttendanceReport( fromDate, toDate, dto.iD,reportType,employeeAttendanceMap );
			report.departmentNameBn = dto.unitNameBng;
			report.departmentNameEn = dto.unitNameEng;
			departmentWiseReportDTO.data.add(report);
		}
		return departmentWiseReportDTO;
	}

	public String buildOfficeUnits(String language) {
		String smLanguage = language.toLowerCase();
    	if(!smLanguage.equalsIgnoreCase("bangla") && !smLanguage.equalsIgnoreCase("english"))
    	{
    		language = "english";
    	}
		List<OptionDTO> optionDTOList = null;
		List<Office_unitsDTO> office_unitsDTOs = getAllOfficeUnits();
		optionDTOList = office_unitsDTOs.stream()
				.map(dto -> new OptionDTO(dto.unitNameEng,dto.unitNameBng,String.valueOf(dto.iD)))
				.collect(Collectors.toList());
		String selectedId = office_unitsDTOs.size()>0 ? String.valueOf(office_unitsDTOs.get(0).iD) : null;
		return Utils.buildOptions(optionDTOList, language, selectedId);
	}

	private List<Office_unitsDTO> getAllOfficeUnits() {
		String filter = " parent_unit = " + SessionConstants.SECRETARY_UNIT;
		List<Long> officeUnitIds = new Support_ticketDAO().getUnits(filter);
		List<Office_unitsDTO> office_unitsDTOs = new ArrayList<Office_unitsDTO>();
		for(long officeUnitId: officeUnitIds)
		{
			Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitId);
			if(office_unitsDTO != null)
			{
				office_unitsDTOs.add(office_unitsDTO);
			}
		}
		return office_unitsDTOs;
	}

	private DepartmentWithAttendanceReport getDepartmentAttendanceReport( long fromDate, long toDate,long officeUnitId,String reportType, Map<Long, Map<Long, EmployeeAttendanceByDate>> employeeAttendanceMap ) {

		List<EmployeeAssignDTO> employeeAssignDTOS = EmployeeOfficeRepository.getInstance().getEmployeeAssignDTOs(officeUnitId);
		DepartmentWithAttendanceReport report = new DepartmentWithAttendanceReport();

		//Map<Long, Map<Long, EmployeeAttendanceByDate>> employeeAttendanceMap = new AttendanceDashboardReport().getAllEmployeeAttendanceMapInRange( fromDate, toDate );
		EmployeeLastAttendanceSummary attendanceSummary;

		if(reportType.equals(AttendanceDashboardConstant.LATE_IN_REPORT)) {
			double totalPresent = 0;
			double totalLate = 0;
			for(EmployeeAssignDTO dto: employeeAssignDTOS) {

				if(fromDate==-1 || toDate==-1) {
					attendanceSummary = EmployeeAttendanceRepository.getInstance().getEmployeeAttendanceSummaryLast30Days( dto.employee_id );
				} else {
					attendanceSummary = new AttendanceDashboardReport().getEmployeeAttendanceSummary( fromDate, toDate, employeeAttendanceMap,dto.employee_id );
				}

				totalPresent += attendanceSummary.totalPresent;
				totalLate += attendanceSummary.totalLate;
			}
			if(totalPresent==0.0) {
				report.latePercent = 0.0;
			} else {
				report.latePercent = (totalLate * 100.0) / totalPresent;
			}

		} else if(reportType.equals(AttendanceDashboardConstant.EARLY_OUT_REPORT)) {
			double totalPresent = 0;
			double totalEarlyOut = 0;
			for(EmployeeAssignDTO dto: employeeAssignDTOS) {

				if(fromDate==-1 || toDate==-1) {
					attendanceSummary = EmployeeAttendanceRepository.getInstance().getEmployeeAttendanceSummaryLast30Days( dto.employee_id );
				} else {
					attendanceSummary = new AttendanceDashboardReport().getEmployeeAttendanceSummary( fromDate, toDate, employeeAttendanceMap,dto.employee_id );
				}

				totalPresent += attendanceSummary.totalPresent;
				totalEarlyOut += attendanceSummary.totalEarlyOut;
			}
			if(totalPresent==0.0) {
				report.earlyOutPercent = 0.0;
			} else {
				report.earlyOutPercent = (totalEarlyOut * 100.0) / totalPresent;
			}
		} else if(reportType.equals(AttendanceDashboardConstant.ABSENT_REPORT)) {
			double totalWorkingDays = 0;
			double totalAbsent = 0;
			for(EmployeeAssignDTO dto: employeeAssignDTOS) {

				if(fromDate==-1 || toDate==-1) {
					attendanceSummary = EmployeeAttendanceRepository.getInstance().getEmployeeAttendanceSummaryLast30Days( dto.employee_id );
				} else {
					attendanceSummary = new AttendanceDashboardReport().getEmployeeAttendanceSummary( fromDate, toDate, employeeAttendanceMap,dto.employee_id );
				}

				totalWorkingDays += attendanceSummary.totalWorkingDays;
				totalAbsent += attendanceSummary.totalWorkingDays - attendanceSummary.totalPresent;
			}
			if(totalWorkingDays==0.0) {
				report.absentPercent = 0.0;
			} else {
				report.absentPercent = (totalAbsent * 100.0) / totalWorkingDays;
			}
		} else if(reportType.equals(AttendanceDashboardConstant.ALL_DEPARTMENT_SUMMERY)) {

			double totalPresent = 0;
			double totalLate = 0;
			double totalEarlyOut = 0;
			double totalWorkingDays = 0;
			double totalAbsent = 0;

			for(EmployeeAssignDTO dto: employeeAssignDTOS) {

				if(fromDate==-1 || toDate==-1) {
					attendanceSummary = EmployeeAttendanceRepository.getInstance().getEmployeeAttendanceSummaryLast30Days( dto.employee_id );
				} else {
					attendanceSummary = new AttendanceDashboardReport().getEmployeeAttendanceSummary( fromDate, toDate, employeeAttendanceMap,dto.employee_id );
				}

				totalPresent += attendanceSummary.totalPresent;
				totalLate += attendanceSummary.totalLate;
				totalEarlyOut += attendanceSummary.totalEarlyOut;
				totalWorkingDays += attendanceSummary.totalWorkingDays;
				totalAbsent += attendanceSummary.totalWorkingDays - attendanceSummary.totalPresent;
			}

			if(totalPresent==0.0) {
				report.latePercent = 0.0;
			} else {
				report.latePercent = (totalLate * 100.0) / totalPresent;
			}

			if(totalPresent==0.0) {
				report.earlyOutPercent = 0.0;
			} else {
				report.earlyOutPercent = (totalEarlyOut * 100.0) / totalPresent;
			}

			if(totalWorkingDays==0.0) {
				report.absentPercent = 0.0;
			} else {
				report.absentPercent = (totalAbsent * 100.0) / totalWorkingDays;
			}
		}

		return report;
	}

	private Pair<Long,Long> parseDateRangeFromSearchParam(String month, String year) {

		Long startDate = null;
		Long endDate = null;
		try {
			if(month != null && !month.isEmpty() && year != null && !year.isEmpty() && !year.equalsIgnoreCase("null") && !month.equalsIgnoreCase("null")) {
				startDate = new Employee_attendanceDAO().constructDateTime(month, year, true);
			} else {
				 startDate = -1L;//new Employee_attendanceDAO().getCurrentTimeInMs(true);
			}

			if(month != null && !month.isEmpty() && year != null && !year.isEmpty() && !year.equalsIgnoreCase("null") && !month.equalsIgnoreCase("null")) {
				endDate = new Employee_attendanceDAO().constructDateTime(month, year, false);
			} else {
				 endDate = -1L;//new Employee_attendanceDAO().getCurrentTimeInMs(false);
			}
		} catch (Exception e) {

		}
		return new Pair<>(startDate,endDate);
	}


}

