package attendance_dashboard;

public class AttendanceDashboardConstant {

    public static final String LATE_IN_REPORT = "late-in-report";
    public static final String EARLY_OUT_REPORT = "early-out-report";
    public static final String ABSENT_REPORT = "absent-report";
    public static final String DEPARTMENT_ALL_REPORT = "department-all-report";
    public static final String ALL_DEPARTMENT_SUMMERY = "all-department-summery";

}
