package attendance_dashboard;

public class DepartmentWithAttendanceReport {

    public String departmentNameEn;
    public String departmentNameBn;
    public double latePercent;
    public double earlyOutPercent;
    public double absentPercent;

}
