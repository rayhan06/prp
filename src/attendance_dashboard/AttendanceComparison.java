package attendance_dashboard;

public class AttendanceComparison {

    public String month;
    public int workingDays;
    public int presentDays;
    public int absentDays;
    public int lateInDays;
    public int earlyOutDays;

}
