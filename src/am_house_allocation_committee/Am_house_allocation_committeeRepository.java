package am_house_allocation_committee;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class Am_house_allocation_committeeRepository implements Repository {
	Am_house_allocation_committeeDAO am_house_allocation_committeeDAO;
	Gson gson = new Gson();
	
	static Logger logger = Logger.getLogger(Am_house_allocation_committeeRepository.class);
	Map<Long, Am_house_allocation_committeeDTO>mapOfAm_house_allocation_committeeDTOToiD;

  
	private Am_house_allocation_committeeRepository(){
		am_house_allocation_committeeDAO = Am_house_allocation_committeeDAO.getInstance();
		mapOfAm_house_allocation_committeeDTOToiD = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Am_house_allocation_committeeRepository INSTANCE = new Am_house_allocation_committeeRepository();
    }

    public static Am_house_allocation_committeeRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Am_house_allocation_committeeDTO> am_house_allocation_committeeDTOs = am_house_allocation_committeeDAO.getAllDTOs(reloadAll);
			for(Am_house_allocation_committeeDTO am_house_allocation_committeeDTO : am_house_allocation_committeeDTOs) {
				Am_house_allocation_committeeDTO oldAm_house_allocation_committeeDTO =
						getAm_house_allocation_committeeDTOByIDWithOutClone(am_house_allocation_committeeDTO.iD);
				if( oldAm_house_allocation_committeeDTO != null ) {
					mapOfAm_house_allocation_committeeDTOToiD.remove(oldAm_house_allocation_committeeDTO.iD);
				
					
				}
				if(am_house_allocation_committeeDTO.isDeleted == 0) 
				{
					
					mapOfAm_house_allocation_committeeDTOToiD.put(am_house_allocation_committeeDTO.iD, am_house_allocation_committeeDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Am_house_allocation_committeeDTO> getAm_house_allocation_committeeList() {
		return clone(new ArrayList<>(this.mapOfAm_house_allocation_committeeDTOToiD.values()));
	}
	
	
	public Am_house_allocation_committeeDTO getAm_house_allocation_committeeDTOByID( long ID){
		return clone(mapOfAm_house_allocation_committeeDTOToiD.get(ID));
	}

	public Am_house_allocation_committeeDTO getAm_house_allocation_committeeDTOByIDWithOutClone( long ID){
		return mapOfAm_house_allocation_committeeDTOToiD.get(ID);
	}

	
	@Override
	public String getTableName() {
		return "am_house_allocation_committee";
	}

	public Am_house_allocation_committeeDTO clone(Am_house_allocation_committeeDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Am_house_allocation_committeeDTO.class);
	}

	public List<Am_house_allocation_committeeDTO> clone(List<Am_house_allocation_committeeDTO> dtoList) {
		return dtoList
				.stream()
				.map(this::clone)
				.collect(Collectors.toList());
	}

	public Am_house_allocation_committeeDTO getActiveCommittee(){
		List<Am_house_allocation_committeeDTO> dtos = getAm_house_allocation_committeeList().stream().
				filter(i -> i.isActive).collect(Collectors.toList());
		return dtos.isEmpty() ? null : dtos.get(0);
	}
}


