package am_house_allocation_committee;

import am_house_allocation_committee_member.Am_house_allocation_committee_memberDAO;
import am_house_allocation_committee_member.Am_house_allocation_committee_memberDTO;
import com.google.gson.Gson;
import common.BaseServlet;
import employee_assign.EmployeeSearchModel;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.UtilCharacter;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;




/**
 * Servlet implementation class Am_house_allocation_committeeServlet
 */
@WebServlet("/Am_house_allocation_committeeServlet")
@MultipartConfig
public class Am_house_allocation_committeeServlet extends BaseServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Am_house_allocation_committeeServlet.class);

    @Override
    public String getTableName() {
        return Am_house_allocation_committeeDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Am_house_allocation_committeeServlet";
    }

    @Override
    public Am_house_allocation_committeeDAO getCommonDAOService() {
        return Am_house_allocation_committeeDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.AM_HOUSE_ALLOCATION_COMMITTEE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.AM_HOUSE_ALLOCATION_COMMITTEE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.AM_HOUSE_ALLOCATION_COMMITTEE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Am_house_allocation_committeeServlet.class;
    }
    private final Gson gson = new Gson();

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request,CommonDTO commonDTO){
        return getServletName()+"?actionType=getAddPage";
    }


	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception
	{
		Am_house_allocation_committeeDTO committeeDTO = new Am_house_allocation_committeeDTO();
		committeeDTO.insertionDate = committeeDTO.lastModificationTime = System.currentTimeMillis();
		committeeDTO.insertedByUserId = userDTO.ID;
		committeeDTO.insertedByOrganogramId = userDTO.organogramID;
		committeeDTO.isActive = true;

        String value = Jsoup.clean(request.getParameter("chairmanId"), Whitelist.simpleText());
        if(!value.isEmpty()){
            List<EmployeeSearchModel> models = Arrays.asList(gson.fromJson(value, EmployeeSearchModel[].class));
            if(!models.isEmpty()){
                committeeDTO.chairmanEmpId = models.get(0).employeeRecordId;
                committeeDTO.chairmanOfficeUnitId = models.get(0).officeUnitId;
                committeeDTO.chairmanOrgId = models.get(0).organogramId;
                committeeDTO.chairmanNameEn = models.get(0).employeeNameEn;
                committeeDTO.chairmanNameBn = models.get(0).employeeNameBn;
                committeeDTO.chairmanOfficeUnitOrgNameEn = models.get(0).organogramNameEn;
                committeeDTO.chairmanOfficeUnitOrgNameBn = models.get(0).organogramNameBn;
                committeeDTO.chairmanOfficeUnitNameEn = models.get(0).officeUnitNameEn;
                committeeDTO.chairmanOfficeUnitNameBn = models.get(0).officeUnitNameBn;
                committeeDTO.chairmanPhoneNum = models.get(0).phoneNumber;
            } else {
                UtilCharacter.throwException("সভাপতি প্রয়োজনীয়", "Chairman Invalid");
            }
        } else{
            UtilCharacter.throwException("সভাপতি প্রয়োজনীয়", "Chairman Invalid");
        }

        value = Jsoup.clean(request.getParameter("secId"), Whitelist.simpleText());
        if(!value.isEmpty()){
            List<EmployeeSearchModel> models = Arrays.asList(gson.fromJson(value, EmployeeSearchModel[].class));
            if(!models.isEmpty()){
                committeeDTO.secretaryEmpId = models.get(0).employeeRecordId;
                committeeDTO.secretaryOfficeUnitId = models.get(0).officeUnitId;
                committeeDTO.secretaryOrgId = models.get(0).organogramId;
                committeeDTO.secretaryNameEn = models.get(0).employeeNameEn;
                committeeDTO.secretaryNameBn = models.get(0).employeeNameBn;
                committeeDTO.secretaryOfficeUnitOrgNameEn = models.get(0).organogramNameEn;
                committeeDTO.secretaryOfficeUnitOrgNameBn = models.get(0).organogramNameBn;
                committeeDTO.secretaryOfficeUnitNameEn = models.get(0).officeUnitNameEn;
                committeeDTO.secretaryOfficeUnitNameBn = models.get(0).officeUnitNameBn;
                committeeDTO.secretaryPhoneNum = models.get(0).phoneNumber;
            } else {
                UtilCharacter.throwException("সদস্য সচিব প্রয়োজনীয়", "Member Secretary Invalid");
            }
        } else{
            UtilCharacter.throwException("সদস্য সচিব প্রয়োজনীয়", "Member Secretary Invalid");
        }

        List<Am_house_allocation_committee_memberDTO> memberDTOS = new ArrayList<>();

        value = Jsoup.clean(request.getParameter("memberId"), Whitelist.simpleText());
        if(!value.isEmpty()){
            List<EmployeeSearchModel> models = Arrays.asList(gson.fromJson(value, EmployeeSearchModel[].class));
            if(models.size() >= 3){
                for(EmployeeSearchModel model: models){
                    Am_house_allocation_committee_memberDTO memberDTO =
                            new Am_house_allocation_committee_memberDTO();
                    memberDTO.memberEmpId = model.employeeRecordId;
                    memberDTO.memberOfficeUnitId = model.officeUnitId;
                    memberDTO.memberOrgId = model.organogramId;
                    memberDTO.memberNameEn = model.employeeNameEn;
                    memberDTO.memberNameBn = model.employeeNameBn;
                    memberDTO.memberOfficeUnitOrgNameEn = model.organogramNameEn;
                    memberDTO.memberOfficeUnitOrgNameBn = model.organogramNameBn;
                    memberDTO.memberOfficeUnitNameEn = model.officeUnitNameEn;
                    memberDTO.memberOfficeUnitNameBn = model.officeUnitNameBn;
                    memberDTO.memberPhoneNum = model.phoneNumber;
                    memberDTOS.add(memberDTO);
                }

            } else {
                UtilCharacter.throwException("নূন্যতম ৩ জন সদস্য প্রয়োজনীয় ", "Member must be equal or greater than 3");
            }
        } else{
            UtilCharacter.throwException("সদস্যগণ প্রয়োজনীয়", "Member Invalid");
        }


        getCommonDAOService().add(committeeDTO);

        Am_house_allocation_committee_memberDAO memberDAO = Am_house_allocation_committee_memberDAO.getInstance();

        memberDTOS.forEach(i -> {
            i.committeeId = committeeDTO.iD;
            i.insertionDate = i.lastModificationTime = System.currentTimeMillis();
            i.insertedByUserId = userDTO.ID;
            i.insertedByOrganogramId = userDTO.organogramID;
            try {
                memberDAO.add(i);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        Am_house_allocation_committeeDAO committeeDAO = Am_house_allocation_committeeDAO.getInstance();
        List<Am_house_allocation_committeeDTO> prevDTOs = committeeDAO.getAllActiveCommittee();
        prevDTOs.stream().filter(i -> i.iD != committeeDTO.iD).forEach(dto -> {
            dto.isActive = false;
            dto.lastModificationTime = System.currentTimeMillis();
            dto.modifiedBy = userDTO.ID;
            try {
                committeeDAO.update(dto);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

		return committeeDTO;
	}
}

