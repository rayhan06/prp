package am_house_allocation_committee;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import recruitment_job_description.Recruitment_job_descriptionDTO;
import util.*;
import pb.*;

public class Am_house_allocation_committeeDAO  implements CommonDAOService<Am_house_allocation_committeeDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	public Am_house_allocation_committeeDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"is_active",
			"chairman_org_id",
			"chairman_office_id",
			"chairman_office_unit_id",
			"chairman_emp_id",
			"chairman_phone_num",
			"chairman_name_en",
			"chairman_name_bn",
			"chairman_office_name_en",
			"chairman_office_name_bn",
			"chairman_office_unit_name_en",
			"chairman_office_unit_name_bn",
			"chairman_office_unit_org_name_en",
			"chairman_office_unit_org_name_bn",
			"secretary_org_id",
			"secretary_office_id",
			"secretary_office_unit_id",
			"secretary_emp_id",
			"secretary_phone_num",
			"secretary_name_en",
			"secretary_name_bn",
			"secretary_office_name_en",
			"secretary_office_name_bn",
			"secretary_office_unit_name_en",
			"secretary_office_unit_name_bn",
			"secretary_office_unit_org_name_en",
			"secretary_office_unit_org_name_bn",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"modified_by",
			"insertion_date",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("chairman_phone_num"," and (chairman_phone_num like ?)");
		searchMap.put("chairman_name_en"," and (chairman_name_en like ?)");
		searchMap.put("chairman_name_bn"," and (chairman_name_bn like ?)");
		searchMap.put("chairman_office_name_en"," and (chairman_office_name_en like ?)");
		searchMap.put("chairman_office_name_bn"," and (chairman_office_name_bn like ?)");
		searchMap.put("chairman_office_unit_name_en"," and (chairman_office_unit_name_en like ?)");
		searchMap.put("chairman_office_unit_name_bn"," and (chairman_office_unit_name_bn like ?)");
		searchMap.put("chairman_office_unit_org_name_en"," and (chairman_office_unit_org_name_en like ?)");
		searchMap.put("chairman_office_unit_org_name_bn"," and (chairman_office_unit_org_name_bn like ?)");
		searchMap.put("secretary_phone_num"," and (secretary_phone_num like ?)");
		searchMap.put("secretary_name_en"," and (secretary_name_en like ?)");
		searchMap.put("secretary_name_bn"," and (secretary_name_bn like ?)");
		searchMap.put("secretary_office_name_en"," and (secretary_office_name_en like ?)");
		searchMap.put("secretary_office_name_bn"," and (secretary_office_name_bn like ?)");
		searchMap.put("secretary_office_unit_name_en"," and (secretary_office_unit_name_en like ?)");
		searchMap.put("secretary_office_unit_name_bn"," and (secretary_office_unit_name_bn like ?)");
		searchMap.put("secretary_office_unit_org_name_en"," and (secretary_office_unit_org_name_en like ?)");
		searchMap.put("secretary_office_unit_org_name_bn"," and (secretary_office_unit_org_name_bn like ?)");
		searchMap.put("modified_by"," and (modified_by like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Am_house_allocation_committeeDAO INSTANCE = new Am_house_allocation_committeeDAO();
	}

	public static Am_house_allocation_committeeDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Am_house_allocation_committeeDTO am_house_allocation_committeeDTO)
	{
		am_house_allocation_committeeDTO.searchColumn = "";
		am_house_allocation_committeeDTO.searchColumn += am_house_allocation_committeeDTO.chairmanPhoneNum + " ";
		am_house_allocation_committeeDTO.searchColumn += am_house_allocation_committeeDTO.chairmanNameEn + " ";
		am_house_allocation_committeeDTO.searchColumn += am_house_allocation_committeeDTO.chairmanNameBn + " ";
		am_house_allocation_committeeDTO.searchColumn += am_house_allocation_committeeDTO.chairmanOfficeNameEn + " ";
		am_house_allocation_committeeDTO.searchColumn += am_house_allocation_committeeDTO.chairmanOfficeNameBn + " ";
		am_house_allocation_committeeDTO.searchColumn += am_house_allocation_committeeDTO.chairmanOfficeUnitNameEn + " ";
		am_house_allocation_committeeDTO.searchColumn += am_house_allocation_committeeDTO.chairmanOfficeUnitNameBn + " ";
		am_house_allocation_committeeDTO.searchColumn += am_house_allocation_committeeDTO.chairmanOfficeUnitOrgNameEn + " ";
		am_house_allocation_committeeDTO.searchColumn += am_house_allocation_committeeDTO.chairmanOfficeUnitOrgNameBn + " ";
		am_house_allocation_committeeDTO.searchColumn += am_house_allocation_committeeDTO.secretaryPhoneNum + " ";
		am_house_allocation_committeeDTO.searchColumn += am_house_allocation_committeeDTO.secretaryNameEn + " ";
		am_house_allocation_committeeDTO.searchColumn += am_house_allocation_committeeDTO.secretaryNameBn + " ";
		am_house_allocation_committeeDTO.searchColumn += am_house_allocation_committeeDTO.secretaryOfficeNameEn + " ";
		am_house_allocation_committeeDTO.searchColumn += am_house_allocation_committeeDTO.secretaryOfficeNameBn + " ";
		am_house_allocation_committeeDTO.searchColumn += am_house_allocation_committeeDTO.secretaryOfficeUnitNameEn + " ";
		am_house_allocation_committeeDTO.searchColumn += am_house_allocation_committeeDTO.secretaryOfficeUnitNameBn + " ";
		am_house_allocation_committeeDTO.searchColumn += am_house_allocation_committeeDTO.secretaryOfficeUnitOrgNameEn + " ";
		am_house_allocation_committeeDTO.searchColumn += am_house_allocation_committeeDTO.secretaryOfficeUnitOrgNameBn + " ";
		am_house_allocation_committeeDTO.searchColumn += am_house_allocation_committeeDTO.modifiedBy + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Am_house_allocation_committeeDTO am_house_allocation_committeeDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(am_house_allocation_committeeDTO);
		if(isInsert)
		{
			ps.setObject(++index,am_house_allocation_committeeDTO.iD);
		}
		ps.setObject(++index,am_house_allocation_committeeDTO.isActive);
		ps.setObject(++index,am_house_allocation_committeeDTO.chairmanOrgId);
		ps.setObject(++index,am_house_allocation_committeeDTO.chairmanOfficeId);
		ps.setObject(++index,am_house_allocation_committeeDTO.chairmanOfficeUnitId);
		ps.setObject(++index,am_house_allocation_committeeDTO.chairmanEmpId);
		ps.setObject(++index,am_house_allocation_committeeDTO.chairmanPhoneNum);
		ps.setObject(++index,am_house_allocation_committeeDTO.chairmanNameEn);
		ps.setObject(++index,am_house_allocation_committeeDTO.chairmanNameBn);
		ps.setObject(++index,am_house_allocation_committeeDTO.chairmanOfficeNameEn);
		ps.setObject(++index,am_house_allocation_committeeDTO.chairmanOfficeNameBn);
		ps.setObject(++index,am_house_allocation_committeeDTO.chairmanOfficeUnitNameEn);
		ps.setObject(++index,am_house_allocation_committeeDTO.chairmanOfficeUnitNameBn);
		ps.setObject(++index,am_house_allocation_committeeDTO.chairmanOfficeUnitOrgNameEn);
		ps.setObject(++index,am_house_allocation_committeeDTO.chairmanOfficeUnitOrgNameBn);
		ps.setObject(++index,am_house_allocation_committeeDTO.secretaryOrgId);
		ps.setObject(++index,am_house_allocation_committeeDTO.secretaryOfficeId);
		ps.setObject(++index,am_house_allocation_committeeDTO.secretaryOfficeUnitId);
		ps.setObject(++index,am_house_allocation_committeeDTO.secretaryEmpId);
		ps.setObject(++index,am_house_allocation_committeeDTO.secretaryPhoneNum);
		ps.setObject(++index,am_house_allocation_committeeDTO.secretaryNameEn);
		ps.setObject(++index,am_house_allocation_committeeDTO.secretaryNameBn);
		ps.setObject(++index,am_house_allocation_committeeDTO.secretaryOfficeNameEn);
		ps.setObject(++index,am_house_allocation_committeeDTO.secretaryOfficeNameBn);
		ps.setObject(++index,am_house_allocation_committeeDTO.secretaryOfficeUnitNameEn);
		ps.setObject(++index,am_house_allocation_committeeDTO.secretaryOfficeUnitNameBn);
		ps.setObject(++index,am_house_allocation_committeeDTO.secretaryOfficeUnitOrgNameEn);
		ps.setObject(++index,am_house_allocation_committeeDTO.secretaryOfficeUnitOrgNameBn);
		ps.setObject(++index,am_house_allocation_committeeDTO.insertedByUserId);
		ps.setObject(++index,am_house_allocation_committeeDTO.insertedByOrganogramId);
		ps.setObject(++index,am_house_allocation_committeeDTO.modifiedBy);
		ps.setObject(++index,am_house_allocation_committeeDTO.insertionDate);
		ps.setObject(++index,am_house_allocation_committeeDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(++index,am_house_allocation_committeeDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,am_house_allocation_committeeDTO.iD);
		}
	}
	
	@Override
	public Am_house_allocation_committeeDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Am_house_allocation_committeeDTO am_house_allocation_committeeDTO = new Am_house_allocation_committeeDTO();
			int i = 0;
			am_house_allocation_committeeDTO.iD = rs.getLong(columnNames[i++]);
			am_house_allocation_committeeDTO.isActive = rs.getBoolean(columnNames[i++]);
			am_house_allocation_committeeDTO.chairmanOrgId = rs.getLong(columnNames[i++]);
			am_house_allocation_committeeDTO.chairmanOfficeId = rs.getLong(columnNames[i++]);
			am_house_allocation_committeeDTO.chairmanOfficeUnitId = rs.getLong(columnNames[i++]);
			am_house_allocation_committeeDTO.chairmanEmpId = rs.getLong(columnNames[i++]);
			am_house_allocation_committeeDTO.chairmanPhoneNum = rs.getString(columnNames[i++]);
			am_house_allocation_committeeDTO.chairmanNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_committeeDTO.chairmanNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_committeeDTO.chairmanOfficeNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_committeeDTO.chairmanOfficeNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_committeeDTO.chairmanOfficeUnitNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_committeeDTO.chairmanOfficeUnitNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_committeeDTO.chairmanOfficeUnitOrgNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_committeeDTO.chairmanOfficeUnitOrgNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_committeeDTO.secretaryOrgId = rs.getLong(columnNames[i++]);
			am_house_allocation_committeeDTO.secretaryOfficeId = rs.getLong(columnNames[i++]);
			am_house_allocation_committeeDTO.secretaryOfficeUnitId = rs.getLong(columnNames[i++]);
			am_house_allocation_committeeDTO.secretaryEmpId = rs.getLong(columnNames[i++]);
			am_house_allocation_committeeDTO.secretaryPhoneNum = rs.getString(columnNames[i++]);
			am_house_allocation_committeeDTO.secretaryNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_committeeDTO.secretaryNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_committeeDTO.secretaryOfficeNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_committeeDTO.secretaryOfficeNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_committeeDTO.secretaryOfficeUnitNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_committeeDTO.secretaryOfficeUnitNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_committeeDTO.secretaryOfficeUnitOrgNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_committeeDTO.secretaryOfficeUnitOrgNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_committeeDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			am_house_allocation_committeeDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			am_house_allocation_committeeDTO.modifiedBy = rs.getLong(columnNames[i++]);
			am_house_allocation_committeeDTO.insertionDate = rs.getLong(columnNames[i++]);
			am_house_allocation_committeeDTO.searchColumn = rs.getString(columnNames[i++]);
			am_house_allocation_committeeDTO.isDeleted = rs.getInt(columnNames[i++]);
			am_house_allocation_committeeDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return am_house_allocation_committeeDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Am_house_allocation_committeeDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "am_house_allocation_committee";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_house_allocation_committeeDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_house_allocation_committeeDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }

	public List<Am_house_allocation_committeeDTO> getAllActiveCommittee()
	{
		String sql = "SELECT * FROM am_house_allocation_committee";
		sql += " WHERE ";
		sql +=" is_active = 1  and isDeleted =  0";

		return getDTOs(sql, this::buildObjectFromResultSet);
	}
				
}
	