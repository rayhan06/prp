package am_house_allocation_committee;
import java.util.*; 
import util.*; 


public class Am_house_allocation_committeeDTO extends CommonDTO
{

	public boolean isActive = false;
	public long chairmanOrgId = -1;
	public long chairmanOfficeId = -1;
	public long chairmanOfficeUnitId = -1;
	public long chairmanEmpId = -1;
    public String chairmanPhoneNum = "";
    public String chairmanNameEn = "";
    public String chairmanNameBn = "";
    public String chairmanOfficeNameEn = "";
    public String chairmanOfficeNameBn = "";
    public String chairmanOfficeUnitNameEn = "";
    public String chairmanOfficeUnitNameBn = "";
    public String chairmanOfficeUnitOrgNameEn = "";
    public String chairmanOfficeUnitOrgNameBn = "";
	public long secretaryOrgId = -1;
	public long secretaryOfficeId = -1;
	public long secretaryOfficeUnitId = -1;
	public long secretaryEmpId = -1;
    public String secretaryPhoneNum = "";
    public String secretaryNameEn = "";
    public String secretaryNameBn = "";
    public String secretaryOfficeNameEn = "";
    public String secretaryOfficeNameBn = "";
    public String secretaryOfficeUnitNameEn = "";
    public String secretaryOfficeUnitNameBn = "";
    public String secretaryOfficeUnitOrgNameEn = "";
    public String secretaryOfficeUnitOrgNameBn = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long modifiedBy = -1;
	public long insertionDate = -1;
	
	
    @Override
	public String toString() {
            return "$Am_house_allocation_committeeDTO[" +
            " iD = " + iD +
            " isActive = " + isActive +
            " chairmanOrgId = " + chairmanOrgId +
            " chairmanOfficeId = " + chairmanOfficeId +
            " chairmanOfficeUnitId = " + chairmanOfficeUnitId +
            " chairmanEmpId = " + chairmanEmpId +
            " chairmanPhoneNum = " + chairmanPhoneNum +
            " chairmanNameEn = " + chairmanNameEn +
            " chairmanNameBn = " + chairmanNameBn +
            " chairmanOfficeNameEn = " + chairmanOfficeNameEn +
            " chairmanOfficeNameBn = " + chairmanOfficeNameBn +
            " chairmanOfficeUnitNameEn = " + chairmanOfficeUnitNameEn +
            " chairmanOfficeUnitNameBn = " + chairmanOfficeUnitNameBn +
            " chairmanOfficeUnitOrgNameEn = " + chairmanOfficeUnitOrgNameEn +
            " chairmanOfficeUnitOrgNameBn = " + chairmanOfficeUnitOrgNameBn +
            " secretaryOrgId = " + secretaryOrgId +
            " secretaryOfficeId = " + secretaryOfficeId +
            " secretaryOfficeUnitId = " + secretaryOfficeUnitId +
            " secretaryEmpId = " + secretaryEmpId +
            " secretaryPhoneNum = " + secretaryPhoneNum +
            " secretaryNameEn = " + secretaryNameEn +
            " secretaryNameBn = " + secretaryNameBn +
            " secretaryOfficeNameEn = " + secretaryOfficeNameEn +
            " secretaryOfficeNameBn = " + secretaryOfficeNameBn +
            " secretaryOfficeUnitNameEn = " + secretaryOfficeUnitNameEn +
            " secretaryOfficeUnitNameBn = " + secretaryOfficeUnitNameBn +
            " secretaryOfficeUnitOrgNameEn = " + secretaryOfficeUnitOrgNameEn +
            " secretaryOfficeUnitOrgNameBn = " + secretaryOfficeUnitOrgNameBn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " modifiedBy = " + modifiedBy +
            " insertionDate = " + insertionDate +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}