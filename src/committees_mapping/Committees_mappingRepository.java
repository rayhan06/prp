package committees_mapping;

import election_constituency.Election_constituencyDAO;
import election_constituency.Election_constituencyDTO;
import election_details.Election_detailsDTO;
import election_details.Election_detailsRepository;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.StringUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;


@SuppressWarnings({"unused"})
public class Committees_mappingRepository implements Repository {
    private final Committees_mappingDAO committees_mappingDAO;

    private static final Logger logger = Logger.getLogger(Committees_mappingRepository.class);

    private final Map<Long, Committees_mappingDTO> mapById;
    private final Map<Long, List<Committees_mappingDTO>> mapByEmployeeRecordId;

    private Committees_mappingRepository() {
        mapById = new ConcurrentHashMap<>();
        mapByEmployeeRecordId = new ConcurrentHashMap<>();

        committees_mappingDAO = Committees_mappingDAO.getInstance();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        static Committees_mappingRepository INSTANCE = new Committees_mappingRepository();
    }

    public synchronized static Committees_mappingRepository getInstance() {
        return Committees_mappingRepository.LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        logger.debug("Committees_mappingRepository reload start reloadAll : "+reloadAll);
        List<Committees_mappingDTO> list = committees_mappingDAO.getAllDTOs(reloadAll);
        list.stream()
                .peek(this::removeIfPresent)
                .filter(dto -> dto.isDeleted == 0)
                .forEach(this::addToMaps);
        logger.debug("Committees_mappingRepository reload end reloadAll : "+reloadAll);
    }

    private void addToMaps(Committees_mappingDTO dto){
        mapById.put(dto.iD, dto);
        List<Committees_mappingDTO> list = mapByEmployeeRecordId.getOrDefault(dto.employeeRecordsId,new ArrayList<>());
        list.add(dto);
        mapByEmployeeRecordId.put(dto.employeeRecordsId,list);
    }

    private void removeIfPresent(Committees_mappingDTO dto) {
        Committees_mappingDTO oldDTO = mapById.get(dto.iD);
        if(oldDTO!=null){
            mapById.remove(dto.iD);
            mapByEmployeeRecordId.get(oldDTO.employeeRecordsId).remove(oldDTO);
        }
    }

    public List<Committees_mappingDTO> getCommittees_mappingDTOList() {
        return new ArrayList<>(this.mapById.values());
    }

    public Committees_mappingDTO getCommittees_mappingDTOByID(long ID) {
        if(mapById.get(ID) == null){
            Committees_mappingDTO dto = committees_mappingDAO.getDTOFromID(ID);
            if(dto!=null){
                addToMaps(dto);
            }
            return dto;
        }
        return mapById.get(ID);
    }

    public List<Committees_mappingDTO> getCommittees_mappingDTOsByEmployeeRecordId(long employeeRecordId) {
        List<Committees_mappingDTO> committees_mappingDTOS = mapByEmployeeRecordId.get(employeeRecordId);

        if (committees_mappingDTOS==null)
            return new ArrayList<>();
        else
            return committees_mappingDTOS;
    }


    @Override
    public String getTableName() {
        return "committees_mapping";
    }
}


