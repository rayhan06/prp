package committees_mapping;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.*;
import java.sql.SQLException;
import java.util.stream.Collectors;
import committees.CommitteesRepository;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import election_constituency.Election_constituencyDTO;
import election_constituency.Election_constituencyRepository;
import election_wise_mp.Election_wise_mpDTO;
import election_wise_mp.Election_wise_mpRepository;
import employee_records.CommitteesInfoModel;
import employee_records.Employee_recordsRepository;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.OptionDTO;
import pb.Utils;
import user.UserDTO;
import util.*;

public class Committees_mappingDAO  implements CommonDAOService<Committees_mappingDTO>
{
	
	private static final Logger logger = Logger.getLogger(Committees_mappingDAO.class);
	
	private static final String addQuery = "insert into {tableName} (committees_id, employee_records_id, election_details_id, election_wise_mp_id, " +
			"committees_role_cat, start_date, end_date, search_column, " +
			"lastModificationTime, modified_by, insertion_date, inserted_by, isDeleted, ID) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static final String updateQuery = "update {tableName} set committees_id=?, employee_records_id=?, election_details_id=?, election_wise_mp_id=?, " +
			"committees_role_cat=?, start_date=?, end_date=?, search_column=?, lastModificationTime=?, modified_by=?, isDeleted=? where ID=?";

	private static final String getByEmpRecIdAndElectionDetailsId = "select * from committees_mapping where employee_records_id = ? and election_details_id = ? and isDeleted = 0";

	private static final String getCommitteeMemberByCommitteeId = "select * from committees_mapping where committees_id = ? and election_details_id = ? and isDeleted = 0";

	private static final String getDTOByEmpComAndElecDetailsId = "select * from committees_mapping where committees_id = ? and election_details_id = ? and employee_records_id = ? and isDeleted = 0";

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Committees_mappingDAO INSTANCE = new Committees_mappingDAO();
	}

	public static Committees_mappingDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}

	private Committees_mappingDAO() {
		searchMap.put("committees_role_cat"," and (committees_role_cat = ?)");
		searchMap.put("committees_id"," and (committees_id = ?)");
		searchMap.put("MP_name"," and (search_column like ?)");
	}

	
	public void setSearchColumn(Committees_mappingDTO committees_mappingDTO)
	{
		committees_mappingDTO.searchColumn = "";
		committees_mappingDTO.searchColumn += Employee_recordsRepository.getInstance().getEmployeeName(committees_mappingDTO.employeeRecordsId, "English");
		committees_mappingDTO.searchColumn += " ";
		committees_mappingDTO.searchColumn += Employee_recordsRepository.getInstance().getEmployeeName(committees_mappingDTO.employeeRecordsId, "Bangla");
		committees_mappingDTO.searchColumn += " ";
		committees_mappingDTO.searchColumn += CommitteesRepository.getInstance().getText("English", committees_mappingDTO.committeesId);
		committees_mappingDTO.searchColumn += " ";
		committees_mappingDTO.searchColumn += CommitteesRepository.getInstance().getText("Bangla", committees_mappingDTO.committeesId);
	}
	
	@Override
	public void set(PreparedStatement ps, Committees_mappingDTO committees_mappingDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();

		setSearchColumn(committees_mappingDTO);

		ps.setObject(++index,committees_mappingDTO.committeesId);
		ps.setObject(++index,committees_mappingDTO.employeeRecordsId);
		ps.setObject(++index,committees_mappingDTO.electionDetailsId);
		ps.setObject(++index,committees_mappingDTO.electionWiseMpId);
		ps.setObject(++index,committees_mappingDTO.committeesRoleCat);
		ps.setObject(++index,committees_mappingDTO.startDate);
		ps.setObject(++index,committees_mappingDTO.endDate);
		ps.setObject(++index,committees_mappingDTO.searchColumn);
		ps.setObject(++index, lastModificationTime);
		ps.setObject(++index,committees_mappingDTO.modifiedBy);

		if(isInsert)
		{
			ps.setObject(++index,committees_mappingDTO.insertionDate);
			ps.setObject(++index,committees_mappingDTO.insertedBy);
		}

		ps.setObject(++index,committees_mappingDTO.isDeleted);
		ps.setObject(++index,committees_mappingDTO.iD);

	}
	
	@Override
	public Committees_mappingDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Committees_mappingDTO committees_mappingDTO = new Committees_mappingDTO();

			committees_mappingDTO.iD = rs.getLong("ID");
			committees_mappingDTO.committeesId = rs.getLong("committees_id");
			committees_mappingDTO.employeeRecordsId = rs.getLong("employee_records_id");
			committees_mappingDTO.electionDetailsId = rs.getLong("election_details_id");
			committees_mappingDTO.electionWiseMpId = rs.getLong("election_wise_mp_id");
			committees_mappingDTO.committeesRoleCat = rs.getInt("committees_role_cat");
			committees_mappingDTO.startDate = rs.getLong("start_date");
			committees_mappingDTO.endDate = rs.getLong("end_date");
			committees_mappingDTO.insertionDate = rs.getLong("insertion_date");
			committees_mappingDTO.insertedBy = rs.getLong("inserted_by");
			committees_mappingDTO.modifiedBy = rs.getLong("modified_by");
			committees_mappingDTO.searchColumn = rs.getString("search_column");
			committees_mappingDTO.isDeleted = rs.getInt("isDeleted");
			committees_mappingDTO.lastModificationTime = rs.getLong("lastModificationTime");

			return committees_mappingDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Committees_mappingDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "committees_mapping";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Committees_mappingDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Committees_mappingDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }



	public static String buildOptions(String language,Long selectedId,long electionDetailsType){

		List<Election_constituencyDTO> electionConstituencyDTOList = Election_constituencyRepository.getInstance().getElectionConstituencyListByElectionDetailsId(electionDetailsType);

		return buildOptions(language,selectedId,electionConstituencyDTOList);
	}

	public static String buildOptions(String language,Long selectedId,List<Election_constituencyDTO> list){
		List<OptionDTO> optionDTOList = null;
		if(list!=null && list.size()>0){
			list.sort(Comparator.comparingInt(o -> o.constituencyNumber));

			optionDTOList = new ArrayList<>();
			OptionDTO optionDTO;
			for (Election_constituencyDTO dto: list) {
				optionDTO = buildOptionDTO(dto);
				if (optionDTO != null) {
					optionDTOList.add(optionDTO);
				}
			}
		}
		return Utils.buildOptions(optionDTOList,language,selectedId == null ? null : String.valueOf(selectedId));
	}

	private static OptionDTO buildOptionDTO(Election_constituencyDTO dto) {

		List<Election_wise_mpDTO> election_wise_mpDTOs = Election_wise_mpRepository.getInstance().getElection_wise_mpDTOByElectionDetailsId(dto.electionDetailsType);

		List<Election_wise_mpDTO> selectedMpDTOs = election_wise_mpDTOs.stream().filter(mpDTO -> mpDTO.electionConstituencyId==dto.iD).filter(mpStatus -> mpStatus.mpStatus==1).collect(Collectors.toList());

		if (selectedMpDTOs.size() != 0) {
			Election_wise_mpDTO activeMP = selectedMpDTOs.get(0);
			return new OptionDTO(String.valueOf(dto.constituencyNumber).concat(" | ").concat(dto.constituencyNameEn).concat(" | ").concat(Employee_recordsRepository.getInstance().getEmployeeName(activeMP.employeeRecordsId, "English")),
					StringUtils.convertToBanNumber(String.valueOf(dto.constituencyNumber)).concat(" | ").concat(dto.constituencyNameBn).concat(" | ").concat(Employee_recordsRepository.getInstance().getEmployeeName(activeMP.employeeRecordsId, "Bangla")),
					String.valueOf(activeMP.employeeRecordsId));
		}

		return null;
	}


	public static List<CommitteesInfoModel> getCommitteesInfoModels(long employeeRecordsId, long electionDetailsId) {

		return ConnectionAndStatementUtil.getListOfT(getByEmpRecIdAndElectionDetailsId, ps -> {
			try {
				ps.setLong(1, employeeRecordsId);
				ps.setLong(2,electionDetailsId);
			} catch (SQLException ex) {
				logger.error(ex);
				ex.printStackTrace();
			}
		}, Committees_mappingDAO::buildCommitteesInfoModelFromResultSet);
	}

	private static CommitteesInfoModel buildCommitteesInfoModelFromResultSet(ResultSet rs) {
    	CommitteesInfoModel committeesInfoModel = new CommitteesInfoModel();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

		try {
			committeesInfoModel.committeesNameId = rs.getLong("committees_id");
			committeesInfoModel.designation = CatRepository.getName("English", "committees_role", rs.getInt("committees_role_cat"));
			committeesInfoModel.startDate = simpleDateFormat.format(new Date(rs.getLong("start_date")));
			committeesInfoModel.endDate = simpleDateFormat.format(new Date(rs.getLong("end_date")));

			return committeesInfoModel;
		} catch (SQLException sqlException) {
			sqlException.printStackTrace();
			return null;
		}
	}

	public Map<Long, String> getAllCommitteeMemberMap(long committeesId, long parliamentNumber, String language) {
    	Map<Long, String> committeeMemberMap = new HashMap<>();
    	Election_constituencyDTO election_constituencyDTO;
		String constituencyInfo = "";

    	List<Committees_mappingDTO> committees_mappingDTOS =  getDTOsByCommitteeIdAndParliamentNo(committeesId, parliamentNumber);


    	for (Committees_mappingDTO dto: committees_mappingDTOS) {
    		election_constituencyDTO = Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(Election_wise_mpRepository.getInstance().getDTOByID(dto.electionWiseMpId).electionConstituencyId);
    		constituencyInfo = language.equalsIgnoreCase("English") ? String.valueOf(election_constituencyDTO.constituencyNumber).concat(" | ").concat(election_constituencyDTO.constituencyNameEn).
					concat(" | ").concat(Employee_recordsRepository.getInstance().getEmployeeName(dto.employeeRecordsId, "English")).concat(" | ").concat(String.valueOf(dto.committeesRoleCat)) :
					StringUtils.convertToBanNumber(String.valueOf(election_constituencyDTO.constituencyNumber)).concat(" | ").concat(election_constituencyDTO.
							constituencyNameBn).concat(" | ").concat(Employee_recordsRepository.getInstance().getEmployeeName(dto.employeeRecordsId, "Bangla")).concat(" | ").concat(String.valueOf(dto.committeesRoleCat));
    		committeeMemberMap.put(dto.employeeRecordsId, constituencyInfo);
		}

    	return committeeMemberMap;
	}


	private static Committees_mappingDTO buildCommitteeFromResultSet(ResultSet rs)
	{
		try
		{
			Committees_mappingDTO committees_mappingDTO = new Committees_mappingDTO();

			committees_mappingDTO.iD = rs.getLong("ID");
			committees_mappingDTO.committeesId = rs.getLong("committees_id");
			committees_mappingDTO.employeeRecordsId = rs.getLong("employee_records_id");
			committees_mappingDTO.electionDetailsId = rs.getLong("election_details_id");
			committees_mappingDTO.electionWiseMpId = rs.getLong("election_wise_mp_id");
			committees_mappingDTO.committeesRoleCat = rs.getInt("committees_role_cat");
			committees_mappingDTO.startDate = rs.getLong("start_date");
			committees_mappingDTO.endDate = rs.getLong("end_date");
			committees_mappingDTO.insertionDate = rs.getLong("insertion_date");
			committees_mappingDTO.insertedBy = rs.getLong("inserted_by");
			committees_mappingDTO.modifiedBy = rs.getLong("modified_by");
			committees_mappingDTO.searchColumn = rs.getString("search_column");
			committees_mappingDTO.isDeleted = rs.getInt("isDeleted");
			committees_mappingDTO.lastModificationTime = rs.getLong("lastModificationTime");

			return committees_mappingDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}


	public Committees_mappingDTO getByEmpIdComIdAndElecDetailsId(long committeesId, long electionDetailsId, long employeeRecordsId) {
		return ConnectionAndStatementUtil.getT(getDTOByEmpComAndElecDetailsId, ps -> {
			try {
				ps.setLong(1, committeesId);
				ps.setLong(2, electionDetailsId);
				ps.setLong(3, employeeRecordsId);
			} catch (SQLException ex) {
				logger.error(ex);
				ex.printStackTrace();
			}
		}, Committees_mappingDAO::buildCommitteeFromResultSet);
	}


	public void deleteRemovedEntries(List<Committees_mappingDTO> dtos, long committeesId, long electionDetailsId, UserDTO userDTO) {
    	int checkedFlag;
		List<Committees_mappingDTO> committees_mappingDTOS =  getDTOsByCommitteeIdAndParliamentNo(committeesId, electionDetailsId);

		for (Committees_mappingDTO savedDTO: committees_mappingDTOS) {
			checkedFlag = 0;
			for (Committees_mappingDTO userGivenDto: dtos) {
				if (savedDTO.employeeRecordsId==userGivenDto.employeeRecordsId) {
					checkedFlag = 1;
					break;
				}
			}

			if (checkedFlag==0) {
				Committees_mappingDAO.getInstance().delete(userDTO.employee_record_id, savedDTO.iD);
			}
		}
	}


	public static List<Committees_mappingDTO> getDTOsByCommitteeIdAndParliamentNo(long committeesId, long parliamentNo) {
    	return ConnectionAndStatementUtil.getListOfT(getCommitteeMemberByCommitteeId, ps -> {
			try {
				ps.setLong(1, committeesId);
				ps.setLong(2, parliamentNo);
			} catch (SQLException ex) {
				logger.error(ex);
				ex.printStackTrace();
			}
		}, Committees_mappingDAO::buildCommitteeFromResultSet);
	}
}
	