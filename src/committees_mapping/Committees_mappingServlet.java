package committees_mapping;

import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

import com.google.gson.Gson;
import election_constituency.Election_constituencyDTO;
import election_constituency.Election_constituencyRepository;
import election_wise_mp.Election_wise_mpDTO;
import election_wise_mp.Election_wise_mpRepository;
import employee_bank_information.Employee_bank_informationDAO;
import employee_records.Employee_recordsRepository;
import house_rent_allowance.House_rent_allowanceDTO;
import login.LoginDTO;
import mp_travel_allowance.Mp_travel_allowanceDAO;
import mp_travel_allowance.Mp_travel_allowanceDTO;
import org.apache.log4j.Logger;
import parliament_session.Parliament_sessionRepository;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;
import javax.servlet.http.*;
import java.util.*;
import java.util.stream.Collectors;

import language.LC;
import language.LM;
import common.BaseServlet;
import pb.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Committees_mappingServlet
 */
@WebServlet("/Committees_mappingServlet")
@MultipartConfig
public class Committees_mappingServlet extends BaseServlet
{
	private static final long serialVersionUID = 1L;
    public static final Logger logger = Logger.getLogger(Committees_mappingServlet.class);

    @Override
    public String getTableName() {
        return Committees_mappingDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Committees_mappingServlet";
    }

    @Override
    public Committees_mappingDAO getCommonDAOService() {
        return Committees_mappingDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.COMMITTEES_MAPPING_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.COMMITTEES_MAPPING_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.COMMITTEES_MAPPING_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Committees_mappingServlet.class;
    }

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception
	{
		// TODO Auto-generated method stub
		try
		{
			String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
			Committees_mappingDTO committees_mappingDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
			request.setAttribute("failureMessage", "");
			String Value = "";
			List<Committees_mappingDTO> committees_mappingDTOS = null;
			long committeesId, electionDetailsId, startDate, endDate, ele;

			Value = request.getParameter("committeesId");
			if(Value != null && !Value.equalsIgnoreCase("")) {
				Value = Jsoup.clean(Value,Whitelist.simpleText());
				committeesId = Long.parseLong(Value);
			} else {
				committeesId = -1;
			}

			Value = request.getParameter("electionDetailsId");
			if(Value != null && !Value.equalsIgnoreCase("")) {
				Value = Jsoup.clean(Value,Whitelist.simpleText());
				electionDetailsId = Long.parseLong(Value);
			} else {
				electionDetailsId = -1;
			}

			Value = request.getParameter("encodedString");
			if(Value != null && !Value.equalsIgnoreCase("")) {
				Value = Jsoup.clean(Value,Whitelist.simpleText());
				committees_mappingDTOS = decodeEncodedString(Value, committeesId, electionDetailsId);
			} else {
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("startDate");
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());

				try
				{
					Date d = f.parse(Value);
					startDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw new Exception(LM.getText(LC.COMMITTEES_MAPPING_ADD_STARTDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
				}
			}
			else
			{
				startDate = SessionConstants.MIN_DATE;
			}

			Value = request.getParameter("endDate");
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
				try
				{
					Date d = f.parse(Value);
					endDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw new Exception(LM.getText(LC.COMMITTEES_MAPPING_ADD_ENDDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
				}
			}
			else
			{
				endDate = SessionConstants.MIN_DATE;
			}

			if(addFlag) {

				for (Committees_mappingDTO dto: committees_mappingDTOS) {
					dto.committeesId = committeesId;
					dto.electionDetailsId = electionDetailsId;
					dto.startDate = startDate;
					dto.endDate = endDate;
					dto.electionWiseMpId = Election_wise_mpRepository.getInstance().getByElectionAndRecordsId(dto.electionDetailsId, dto.employeeRecordsId).iD;
					dto.insertionDate = TimeConverter.getToday();
					dto.insertedBy = userDTO.employee_record_id;

					dto.modifiedBy = userDTO.employee_record_id;

					Committees_mappingDAO.getInstance().add(dto);
				}
			} else {
				for (Committees_mappingDTO dto: committees_mappingDTOS) {
					if (dto.iD==-1) {
						dto.committeesId = committeesId;
						dto.electionDetailsId = electionDetailsId;
						dto.startDate = startDate;
						dto.endDate = endDate;
						dto.electionWiseMpId = Election_wise_mpRepository.getInstance().getByElectionAndRecordsId(dto.electionDetailsId, dto.employeeRecordsId).iD;
						dto.insertionDate = TimeConverter.getToday();
						dto.insertedBy = userDTO.employee_record_id;

						dto.modifiedBy = userDTO.employee_record_id;

						Committees_mappingDAO.getInstance().add(dto);
					} else {
						dto.startDate = startDate;
						dto.endDate = endDate;
						dto.modifiedBy = userDTO.employee_record_id;
						Committees_mappingDAO.getInstance().update(dto);
					}
				}

				Committees_mappingDAO.getInstance().deleteRemovedEntries(committees_mappingDTOS, committeesId, electionDetailsId, userDTO);
			}

			return committees_mappingDTOS.get(0);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		if (userDTO == null) {
			request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			return;
		}
		try {
			String actionType = request.getParameter("actionType");
			long electionDetailsId;
			long electionConstituencyId;
			long employeeRecordsId;
			String options;
			String value;
			Map<String, Object> res = new HashMap<>();
			Election_wise_mpDTO election_wise_mpDTO;
			String employeeName;

			String language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
			switch (actionType) {
				case "getElectionConstituencyListWithMpName":
					electionDetailsId = Long.parseLong(request.getParameter("electionDetailsId"));
					value = request.getParameter("electionDetailsId");
					if (value==null || value.equalsIgnoreCase("")) {
						options =  Committees_mappingDAO.buildOptions(language, null, electionDetailsId);
					} else {
						employeeRecordsId = Long.parseLong(value);
						options =  Committees_mappingDAO.buildOptions(language, employeeRecordsId, electionDetailsId);
					}

					response.setContentType("text/html; charset=UTF-8");
					response.getWriter().println(options);
					return;
				case "getEditPageRecord":
					employeeRecordsId = Long.parseLong(request.getParameter("employeeRecordsId"));
					electionDetailsId = Long.parseLong(request.getParameter("electionDetailsId"));
					long committeesId = Long.parseLong(request.getParameter("committeesId"));

					Map<Long, String> allCommitteeMembers = Committees_mappingDAO.getInstance().getAllCommitteeMemberMap(committeesId, electionDetailsId, language);

					options = Committees_mappingDAO.buildOptions(language, employeeRecordsId, electionDetailsId);

					res.put("allCommitteeMembers", allCommitteeMembers);
					res.put("options", options);

					Gson gson = new Gson();
					String json = gson.toJson(res);

					response.setContentType("application/json; charset=UTF-8");
					response.getWriter().println(json);
					return;
				case "viewCommittee":
					if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.COMMITTEES_MAPPING_SEARCH)) {
						request.getRequestDispatcher("committees_mapping/committees_mappingCommitteeView.jsp").forward(request, response);
					} else {
						request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
					}
					return;
				default:
					super.doGet(request, response);
					return;
			}
		} catch (Exception e) {
			logger.error(e);
		}
		request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
	}

	List<Committees_mappingDTO> decodeEncodedString(String encodedString, long committeesId, long electionDetailsId) {
    	List<Committees_mappingDTO> committees_mappingDTOS = new ArrayList<>();

		String[] splitData;
		String[] rawData = encodedString.split(";");
		long emp_rec_id;
		int committeeRoleCat;

		for (String str: rawData) {
			splitData = str.split(",");
			emp_rec_id = Long.parseLong(splitData[0]);
			committeeRoleCat = Integer.parseInt(splitData[1]);
			Committees_mappingDTO dto = Committees_mappingDAO.getInstance().getByEmpIdComIdAndElecDetailsId(committeesId, electionDetailsId, emp_rec_id);

			if (dto==null) {
				dto = new Committees_mappingDTO();
				dto.employeeRecordsId = Long.parseLong(splitData[0]);
				dto.committeesRoleCat = committeeRoleCat;
			} else {
				dto.committeesRoleCat = committeeRoleCat;
			}

			committees_mappingDTOS.add(dto);
		}
		return committees_mappingDTOS;
	}
}

