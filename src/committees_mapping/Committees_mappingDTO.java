package committees_mapping;

import util.CommonDTO;


public class Committees_mappingDTO extends CommonDTO
{

	public long committeesId = -1;
	public long employeeRecordsId = -1;
	public long electionDetailsId = -1;
	public long electionWiseMpId = -1;
	public int committeesRoleCat = -1;
	public long startDate = System.currentTimeMillis();
	public long endDate = System.currentTimeMillis();
	public long insertionDate = -1;
	public long insertedBy = -1;
	public long modifiedBy = -1;
	
	
    @Override
	public String toString() {
            return "$Committees_mappingDTO[" +
            " iD = " + iD +
            " committeesId = " + committeesId +
            " employeeRecordsId = " + employeeRecordsId +
            " electionDetailsId = " + electionDetailsId +
            " electionWiseMpId = " + electionWiseMpId +
            " committeesRoleCat = " + committeesRoleCat +
            " startDate = " + startDate +
            " endDate = " + endDate +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}