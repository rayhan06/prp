package mp_payroll_deduction_configuration;

import economic_sub_code.Economic_sub_codeDTO;
import economic_sub_code.Economic_sub_codeRepository;
import mp_payroll_bill_deduction.Mp_payroll_bill_deductionDTO;
import pb.Utils;

public class Mp_payroll_deduction_configurationModel {
    public String configurationId;
    public String name;
    public String code;
    public String amount;

    public Mp_payroll_deduction_configurationModel(Mp_payroll_deduction_configurationDTO dto, String language) {
        configurationId = String.valueOf(dto.iD);
        Economic_sub_codeDTO codeDTO = Economic_sub_codeRepository.getInstance().getDTOByID(dto.economicSubCodeId);
        name = language.equalsIgnoreCase("ENGLISH") ? codeDTO.descriptionEn : codeDTO.descriptionBn;
        code = Utils.getDigits(codeDTO.code, language);
        amount = Utils.getDigits(dto.amount, language);
    }

    public Mp_payroll_deduction_configurationModel(Mp_payroll_bill_deductionDTO dto, String language) {
        configurationId = String.valueOf(dto.mpPayrollDeductionConfigurationId);
        Mp_payroll_deduction_configurationDTO payroll_deduction_configurationDTO = Mp_payroll_deduction_configurationRepository.getInstance().getDTODeletedOrNot(dto.mpPayrollDeductionConfigurationId);
        Economic_sub_codeDTO codeDTO = Economic_sub_codeRepository.getInstance().getDTOByID(payroll_deduction_configurationDTO.economicSubCodeId);
        name = language.equalsIgnoreCase("ENGLISH") ? codeDTO.descriptionEn : codeDTO.descriptionBn;
        code = Utils.getDigits(codeDTO.code, language);
        amount = Utils.getDigits(payroll_deduction_configurationDTO.amount, language);
    }
}
