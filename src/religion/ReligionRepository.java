package religion;

import common.NameRepository;

public class ReligionRepository extends NameRepository {

    private ReligionRepository() {
        super(ReligionDao.getInstance(), ReligionRepository.class);
    }

    private static class LazyLoader {
        static ReligionRepository INSTANCE = new ReligionRepository();
    }

    public static ReligionRepository getInstance() {
        return ReligionRepository.LazyLoader.INSTANCE;
    }
}
