package religion;

import common.NameDao;

public class ReligionDao extends NameDao {

    private ReligionDao() {
        super("religion");
    }

    private static class LazyLoader{
        static final ReligionDao INSTANCE = new ReligionDao();
    }

    public static ReligionDao getInstance(){
        return LazyLoader.INSTANCE;
    }
}
