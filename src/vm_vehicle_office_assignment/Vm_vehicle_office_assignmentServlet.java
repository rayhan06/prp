package vm_vehicle_office_assignment;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import common.ApiResponse;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import vm_vehicle.Vm_vehicleDTO;


/**
 * Servlet implementation class Vm_vehicle_office_assignmentServlet
 */
@WebServlet("/Vm_vehicle_office_assignmentServlet")
@MultipartConfig
public class Vm_vehicle_office_assignmentServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Vm_vehicle_office_assignmentServlet.class);

    String tableName = "vm_vehicle_office_assignment";

	Vm_vehicle_office_assignmentDAO vm_vehicle_office_assignmentDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vm_vehicle_office_assignmentServlet()
	{
        super();
    	try
    	{
			vm_vehicle_office_assignmentDAO = new Vm_vehicle_office_assignmentDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(vm_vehicle_office_assignmentDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_OFFICE_ASSIGNMENT_UPDATE))
				{
					getVm_vehicle_office_assignment(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_OFFICE_ASSIGNMENT_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchVm_vehicle_office_assignment(request, response, isPermanentTable, filter);
						}
						else
						{
							searchVm_vehicle_office_assignment(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchVm_vehicle_office_assignment(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_OFFICE_ASSIGNMENT_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("getValidationByVehicleId")){
				System.out.println("In getValidationByVehicleId");
				String type = request.getParameter("type");
				Long vehicleId =  Long.parseLong(request.getParameter("vehicleId"));
				boolean valid = false;
				int count = vm_vehicle_office_assignmentDAO.getCountOfVehicleId(vehicleId);

				if(type.equalsIgnoreCase("add")){
					if(count == 0){
						valid = true;
					}
				}else {
					Vm_vehicle_office_assignmentDTO dto = Vm_vehicle_office_assignmentRepository.getInstance().
							getVm_vehicle_office_assignmentDTOByID(Long.parseLong(request.getParameter("iD")));
//							(Vm_vehicle_office_assignmentDTO) vm_vehicle_office_assignmentDAO.getDTOByID
//									(Long.parseLong(request.getParameter("iD")));
					if(dto.vehicleId == vehicleId && count == 1){
						valid = true;
					} else if(dto.vehicleId != vehicleId && count == 0){
						valid = true;
					}

				}

				PrintWriter out = response.getWriter();
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");

				String encoded = this.gson.toJson(valid);
				System.out.println("json encoded data = " + encoded);
				out.print(encoded);
				out.flush();

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD))
				{
					System.out.println("going to  addVm_vehicle_office_assignment ");
					addVm_vehicle_office_assignment(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addVm_vehicle_office_assignment ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addVm_vehicle_office_assignment ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_OFFICE_ASSIGNMENT_UPDATE))
				{
					addVm_vehicle_office_assignment(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_OFFICE_ASSIGNMENT_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_OFFICE_ASSIGNMENT_SEARCH))
				{
					searchVm_vehicle_office_assignment(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Vm_vehicle_office_assignmentDTO vm_vehicle_office_assignmentDTO = Vm_vehicle_office_assignmentRepository.
					getInstance().getVm_vehicle_office_assignmentDTOByID(Long.parseLong(request.getParameter("ID")));
//					(Vm_vehicle_office_assignmentDTO)vm_vehicle_office_assignmentDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(vm_vehicle_office_assignmentDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addVm_vehicle_office_assignment(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub

		ApiResponse apiResponse;

		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addVm_vehicle_office_assignment");
			String path = getServletContext().getRealPath("/img2/");
			Vm_vehicle_office_assignmentDTO vm_vehicle_office_assignmentDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				vm_vehicle_office_assignmentDTO = new Vm_vehicle_office_assignmentDTO();
			}
			else
			{
				vm_vehicle_office_assignmentDTO = Vm_vehicle_office_assignmentRepository.getInstance().
						getVm_vehicle_office_assignmentDTOByID(Long.parseLong(request.getParameter("iD")));
//						(Vm_vehicle_office_assignmentDTO)vm_vehicle_office_assignmentDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("vehicleTypeCat");
			System.out.println("vehicleTypeCat = " + Value);
			Value = Jsoup.clean(Value,Whitelist.simpleText());


			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_vehicle_office_assignmentDTO.vehicleTypeCat = Integer.parseInt(Value);
				if(vm_vehicle_office_assignmentDTO.vehicleTypeCat == -1){
					throw new Exception(" Select vehicle type");
				}
			}
			else
			{
				throw new Exception(" Invalid vehicle type");
			}

			Value = request.getParameter("vehicleId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("vehicleId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_vehicle_office_assignmentDTO.vehicleId = Long.parseLong(Value);
			}
			else
			{
				throw new Exception(" Invalid vehicle");
			}

			Value = request.getParameter("officeId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("officeId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_vehicle_office_assignmentDTO.officeId = Long.parseLong(Value);
			}
			else
			{
				throw new Exception(" Invalid office");
			}


			Value = request.getParameter("startDate");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("startDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				Date d = f.parse(Value);
				vm_vehicle_office_assignmentDTO.start_time = d.getTime();

			}
			else
			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				throw new Exception(" Invalid start date");
			}


			Value = request.getParameter("endDate");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("endDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				Date d = f.parse(Value);
				vm_vehicle_office_assignmentDTO.end_time = d.getTime();

			}
//			END DATE NOT MANDATORY
//			else
//			{
//				throw new Exception(" Invalid end date");
//			}


			// start date and end date validation

//			if(vm_vehicle_office_assignmentDTO.end_time < vm_vehicle_office_assignmentDTO.start_time){
//				throw new Exception(" end date must be greater than start date");
//			}




			Value = request.getParameter("remarks");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("remarks = " + Value);
			if(Value != null)
			{
				vm_vehicle_office_assignmentDTO.remarks = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				vm_vehicle_office_assignmentDTO.insertedByUserId = userDTO.ID;
				vm_vehicle_office_assignmentDTO.insertedByOrganogramId = userDTO.organogramID;
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				vm_vehicle_office_assignmentDTO.insertionDate = c.getTimeInMillis();
			}

			System.out.println("Done adding  addVm_vehicle_office_assignment dto = " + vm_vehicle_office_assignmentDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				vm_vehicle_office_assignmentDAO.setIsDeleted(vm_vehicle_office_assignmentDTO.iD, CommonDTO.OUTDATED);
				returnedID = vm_vehicle_office_assignmentDAO.add(vm_vehicle_office_assignmentDTO);
				vm_vehicle_office_assignmentDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = vm_vehicle_office_assignmentDAO.manageWriteOperations(vm_vehicle_office_assignmentDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = vm_vehicle_office_assignmentDAO.manageWriteOperations(vm_vehicle_office_assignmentDTO, SessionConstants.UPDATE, -1, userDTO);
			}


			long finalReturnedID = returnedID;
			List<Vm_vehicle_office_assignmentDTO> dtos = vm_vehicle_office_assignmentDAO.
					getAllVm_vehicle_office_assignmentByVehicleId(vm_vehicle_office_assignmentDTO.vehicleId).
					stream().filter(i -> i.iD != finalReturnedID).collect(Collectors.toList());

			for(Vm_vehicle_office_assignmentDTO dto: dtos){
				vm_vehicle_office_assignmentDAO.delete(dto.iD);
			}

			apiResponse = ApiResponse.makeSuccessResponse("Vm_vehicle_office_assignmentServlet?actionType=search");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			apiResponse = ApiResponse.makeErrorResponse(e.getMessage());
		}

		PrintWriter pw = response.getWriter();
		pw.write(apiResponse.getJSONString());
		pw.flush();
		pw.close();
	}









	private void getVm_vehicle_office_assignment(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getVm_vehicle_office_assignment");
		Vm_vehicle_office_assignmentDTO vm_vehicle_office_assignmentDTO = null;
		try
		{
			vm_vehicle_office_assignmentDTO = Vm_vehicle_office_assignmentRepository.getInstance().
					getVm_vehicle_office_assignmentDTOByID(id);
//					(Vm_vehicle_office_assignmentDTO)vm_vehicle_office_assignmentDAO.getDTOByID(id);
			request.setAttribute("ID", vm_vehicle_office_assignmentDTO.iD);
			request.setAttribute("vm_vehicle_office_assignmentDTO",vm_vehicle_office_assignmentDTO);
			request.setAttribute("vm_vehicle_office_assignmentDAO",vm_vehicle_office_assignmentDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "vm_vehicle_office_assignment/vm_vehicle_office_assignmentInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "vm_vehicle_office_assignment/vm_vehicle_office_assignmentSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "vm_vehicle_office_assignment/vm_vehicle_office_assignmentEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "vm_vehicle_office_assignment/vm_vehicle_office_assignmentEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getVm_vehicle_office_assignment(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getVm_vehicle_office_assignment(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchVm_vehicle_office_assignment(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchVm_vehicle_office_assignment 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_VM_VEHICLE_OFFICE_ASSIGNMENT,
			request,
			vm_vehicle_office_assignmentDAO,
			SessionConstants.VIEW_VM_VEHICLE_OFFICE_ASSIGNMENT,
			SessionConstants.SEARCH_VM_VEHICLE_OFFICE_ASSIGNMENT,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("vm_vehicle_office_assignmentDAO",vm_vehicle_office_assignmentDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_vehicle_office_assignment/vm_vehicle_office_assignmentApproval.jsp");
	        	rd = request.getRequestDispatcher("vm_vehicle_office_assignment/vm_vehicle_office_assignmentApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_vehicle_office_assignment/vm_vehicle_office_assignmentApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("vm_vehicle_office_assignment/vm_vehicle_office_assignmentApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_vehicle_office_assignment/vm_vehicle_office_assignmentSearch.jsp");
	        	rd = request.getRequestDispatcher("vm_vehicle_office_assignment/vm_vehicle_office_assignmentSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_vehicle_office_assignment/vm_vehicle_office_assignmentSearchForm.jsp");
	        	rd = request.getRequestDispatcher("vm_vehicle_office_assignment/vm_vehicle_office_assignmentSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

