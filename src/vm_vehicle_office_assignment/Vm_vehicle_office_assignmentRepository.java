package vm_vehicle_office_assignment;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class Vm_vehicle_office_assignmentRepository implements Repository {
	Vm_vehicle_office_assignmentDAO vm_vehicle_office_assignmentDAO = new Vm_vehicle_office_assignmentDAO();
	Gson gson = new Gson();
	
	public void setDAO(Vm_vehicle_office_assignmentDAO vm_vehicle_office_assignmentDAO)
	{
		this.vm_vehicle_office_assignmentDAO = vm_vehicle_office_assignmentDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Vm_vehicle_office_assignmentRepository.class);
	Map<Long, Vm_vehicle_office_assignmentDTO>mapOfVm_vehicle_office_assignmentDTOToiD;
	Map<Long, Set<Vm_vehicle_office_assignmentDTO>>mapOfVm_vehicle_office_assignmentDTOTovehicleId;
	Map<Long, Set<Vm_vehicle_office_assignmentDTO> >mapOfVm_vehicle_office_assignmentDTOToofficeId;


	static Vm_vehicle_office_assignmentRepository instance = null;  
	private Vm_vehicle_office_assignmentRepository(){
		mapOfVm_vehicle_office_assignmentDTOToiD = new ConcurrentHashMap<>();
		mapOfVm_vehicle_office_assignmentDTOTovehicleId = new ConcurrentHashMap<>();
		mapOfVm_vehicle_office_assignmentDTOToofficeId = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vm_vehicle_office_assignmentRepository getInstance(){
		if (instance == null){
			instance = new Vm_vehicle_office_assignmentRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(vm_vehicle_office_assignmentDAO == null)
		{
			return;
		}
		try {
			List<Vm_vehicle_office_assignmentDTO> vm_vehicle_office_assignmentDTOs = (List<Vm_vehicle_office_assignmentDTO>)
					vm_vehicle_office_assignmentDAO.getAll(reloadAll);
			for(Vm_vehicle_office_assignmentDTO vm_vehicle_office_assignmentDTO : vm_vehicle_office_assignmentDTOs) {
				Vm_vehicle_office_assignmentDTO oldVm_vehicle_office_assignmentDTO =
						getVm_vehicle_office_assignmentDTOByIDWithoutClone(vm_vehicle_office_assignmentDTO.iD);
				if( oldVm_vehicle_office_assignmentDTO != null ) {
					mapOfVm_vehicle_office_assignmentDTOToiD.remove(oldVm_vehicle_office_assignmentDTO.iD);

					if(mapOfVm_vehicle_office_assignmentDTOTovehicleId.containsKey(oldVm_vehicle_office_assignmentDTO.vehicleId)) {
						mapOfVm_vehicle_office_assignmentDTOTovehicleId.get(oldVm_vehicle_office_assignmentDTO.vehicleId).remove(oldVm_vehicle_office_assignmentDTO);
					}
					if(mapOfVm_vehicle_office_assignmentDTOTovehicleId.get(oldVm_vehicle_office_assignmentDTO.vehicleId).isEmpty()) {
						mapOfVm_vehicle_office_assignmentDTOTovehicleId.remove(oldVm_vehicle_office_assignmentDTO.vehicleId);
					}

					if(mapOfVm_vehicle_office_assignmentDTOToofficeId.containsKey(oldVm_vehicle_office_assignmentDTO.officeId)) {
						mapOfVm_vehicle_office_assignmentDTOToofficeId.get(oldVm_vehicle_office_assignmentDTO.officeId).remove(oldVm_vehicle_office_assignmentDTO);
					}
					if(mapOfVm_vehicle_office_assignmentDTOToofficeId.get(oldVm_vehicle_office_assignmentDTO.officeId).isEmpty()) {
						mapOfVm_vehicle_office_assignmentDTOToofficeId.remove(oldVm_vehicle_office_assignmentDTO.officeId);
					}

				}
				if(vm_vehicle_office_assignmentDTO.isDeleted == 0) 
				{
					mapOfVm_vehicle_office_assignmentDTOToiD.put(vm_vehicle_office_assignmentDTO.iD, vm_vehicle_office_assignmentDTO);

					if( ! mapOfVm_vehicle_office_assignmentDTOTovehicleId.containsKey(vm_vehicle_office_assignmentDTO.vehicleId)) {
						mapOfVm_vehicle_office_assignmentDTOTovehicleId.put(vm_vehicle_office_assignmentDTO.vehicleId, new HashSet<>());
					}
					mapOfVm_vehicle_office_assignmentDTOTovehicleId.get(vm_vehicle_office_assignmentDTO.vehicleId).add(vm_vehicle_office_assignmentDTO);

					if( ! mapOfVm_vehicle_office_assignmentDTOToofficeId.containsKey(vm_vehicle_office_assignmentDTO.officeId)) {
						mapOfVm_vehicle_office_assignmentDTOToofficeId.put(vm_vehicle_office_assignmentDTO.officeId, new HashSet<>());
					}
					mapOfVm_vehicle_office_assignmentDTOToofficeId.get(vm_vehicle_office_assignmentDTO.officeId).add(vm_vehicle_office_assignmentDTO);

				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Vm_vehicle_office_assignmentDTO> getVm_vehicle_office_assignmentList() {
		List <Vm_vehicle_office_assignmentDTO> vm_vehicle_office_assignments = new ArrayList<Vm_vehicle_office_assignmentDTO>(this.mapOfVm_vehicle_office_assignmentDTOToiD.values());
		return clone(vm_vehicle_office_assignments);
	}
	
	
	public Vm_vehicle_office_assignmentDTO getVm_vehicle_office_assignmentDTOByIDWithoutClone( long ID){
		return mapOfVm_vehicle_office_assignmentDTOToiD.get(ID);
	}

	public Vm_vehicle_office_assignmentDTO getVm_vehicle_office_assignmentDTOByID( long ID){
		return clone(mapOfVm_vehicle_office_assignmentDTOToiD.get(ID));
	}

	public List<Vm_vehicle_office_assignmentDTO> getVm_vehicle_office_assignmentDTOByvehicle_id(long vehicle_id) {
		return clone(new ArrayList<>( mapOfVm_vehicle_office_assignmentDTOTovehicleId.getOrDefault(vehicle_id,new HashSet<>())));
	}


	public List<Vm_vehicle_office_assignmentDTO> getVm_vehicle_office_assignmentDTOByoffice_id(long office_id) {
		return clone(new ArrayList<>( mapOfVm_vehicle_office_assignmentDTOToofficeId.getOrDefault(office_id,new HashSet<>())));
	}

	public Vm_vehicle_office_assignmentDTO clone(Vm_vehicle_office_assignmentDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Vm_vehicle_office_assignmentDTO.class);
	}

	public List<Vm_vehicle_office_assignmentDTO> clone(List<Vm_vehicle_office_assignmentDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_vehicle_office_assignment";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


