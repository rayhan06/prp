package vm_vehicle_office_assignment;

import util.CommonDTO;


public class Vm_vehicle_office_assignmentDTO extends CommonDTO
{

	public int vehicleTypeCat = -1;
	public long vehicleId = -1;
	public long officeId = -1;
//    public String remarks = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public int count = 0;
	public long start_time = 0;
	public long end_time = 0;
	
	
    @Override
	public String toString() {
            return "$Vm_vehicle_office_assignmentDTO[" +
            " iD = " + iD +
            " vehicleTypeCat = " + vehicleTypeCat +
            " vehicleId = " + vehicleId +
            " officeId = " + officeId +
            " remarks = " + remarks +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            "]";
    }

}