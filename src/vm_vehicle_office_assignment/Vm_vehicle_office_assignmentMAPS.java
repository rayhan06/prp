package vm_vehicle_office_assignment;
import java.util.*; 
import util.*;


public class Vm_vehicle_office_assignmentMAPS extends CommonMaps
{	
	public Vm_vehicle_office_assignmentMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("vehicleTypeCat".toLowerCase(), "vehicleTypeCat".toLowerCase());
		java_DTO_map.put("vehicleId".toLowerCase(), "vehicleId".toLowerCase());
		java_DTO_map.put("officeId".toLowerCase(), "officeId".toLowerCase());
		java_DTO_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());

		java_SQL_map.put("vehicle_type_cat".toLowerCase(), "vehicleTypeCat".toLowerCase());
		java_SQL_map.put("vehicle_id".toLowerCase(), "vehicleId".toLowerCase());
		java_SQL_map.put("office_id".toLowerCase(), "officeId".toLowerCase());
		java_SQL_map.put("remarks".toLowerCase(), "remarks".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Vehicle Type".toLowerCase(), "vehicleTypeCat".toLowerCase());
		java_Text_map.put("Vehicle Id".toLowerCase(), "vehicleId".toLowerCase());
		java_Text_map.put("Office Id".toLowerCase(), "officeId".toLowerCase());
		java_Text_map.put("Remarks".toLowerCase(), "remarks".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
			
	}

}