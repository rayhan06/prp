package vm_vehicle_office_assignment;

import common.ConnectionAndStatementUtil;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import pb.CatDAO;
import pb.CatRepository;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;
import vm_vehicle.Vm_vehicleDTO;
import vm_vehicle.Vm_vehicleRepository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class Vm_vehicle_office_assignmentDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Vm_vehicle_office_assignmentDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Vm_vehicle_office_assignmentMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"vehicle_type_cat",
			"vehicle_id",
			"office_id",
			"remarks",
			"start_time",
			"end_time",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Vm_vehicle_office_assignmentDAO()
	{
		this("vm_vehicle_office_assignment");		
	}
	
	public void setSearchColumn(Vm_vehicle_office_assignmentDTO vm_vehicle_office_assignmentDTO)
	{
		vm_vehicle_office_assignmentDTO.searchColumn = "";
		vm_vehicle_office_assignmentDTO.searchColumn += CatRepository.getName("English", "vehicle_type", vm_vehicle_office_assignmentDTO.vehicleTypeCat) + " " + CatRepository.getName("Bangla", "vehicle_type", vm_vehicle_office_assignmentDTO.vehicleTypeCat) + " ";
		vm_vehicle_office_assignmentDTO.searchColumn += vm_vehicle_office_assignmentDTO.remarks + " ";
		Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_vehicle_office_assignmentDTO.vehicleId);
		if(vm_vehicleDTO != null){
			vm_vehicle_office_assignmentDTO.searchColumn +=  vm_vehicleDTO.regNo + " ";
		}

		Office_unitsDTO unit = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(vm_vehicle_office_assignmentDTO.officeId);
		if(unit != null){
			vm_vehicle_office_assignmentDTO.searchColumn += unit.unitNameBng + " " + unit.unitNameEng + " ";
		}

	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Vm_vehicle_office_assignmentDTO vm_vehicle_office_assignmentDTO = (Vm_vehicle_office_assignmentDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(vm_vehicle_office_assignmentDTO);
		if(isInsert)
		{
			ps.setObject(index++,vm_vehicle_office_assignmentDTO.iD);
		}
		ps.setObject(index++,vm_vehicle_office_assignmentDTO.vehicleTypeCat);
		ps.setObject(index++,vm_vehicle_office_assignmentDTO.vehicleId);
		ps.setObject(index++,vm_vehicle_office_assignmentDTO.officeId);
		ps.setObject(index++,vm_vehicle_office_assignmentDTO.remarks);
		ps.setObject(index++,vm_vehicle_office_assignmentDTO.start_time);
		ps.setObject(index++,vm_vehicle_office_assignmentDTO.end_time);
		ps.setObject(index++,vm_vehicle_office_assignmentDTO.insertedByUserId);
		ps.setObject(index++,vm_vehicle_office_assignmentDTO.insertedByOrganogramId);
		ps.setObject(index++,vm_vehicle_office_assignmentDTO.insertionDate);
		ps.setObject(index++,vm_vehicle_office_assignmentDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}

	public Vm_vehicle_office_assignmentDTO build(ResultSet rs)
	{
		try
		{
			Vm_vehicle_office_assignmentDTO vm_vehicle_office_assignmentDTO = new Vm_vehicle_office_assignmentDTO();
			vm_vehicle_office_assignmentDTO.iD = rs.getLong("ID");
			vm_vehicle_office_assignmentDTO.vehicleTypeCat = rs.getInt("vehicle_type_cat");
			vm_vehicle_office_assignmentDTO.vehicleId = rs.getLong("vehicle_id");
			vm_vehicle_office_assignmentDTO.officeId = rs.getLong("office_id");
			vm_vehicle_office_assignmentDTO.start_time = rs.getLong("start_time");
			vm_vehicle_office_assignmentDTO.end_time = rs.getLong("end_time");
			vm_vehicle_office_assignmentDTO.remarks = rs.getString("remarks");
			vm_vehicle_office_assignmentDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			vm_vehicle_office_assignmentDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			vm_vehicle_office_assignmentDTO.insertionDate = rs.getLong("insertion_date");
			vm_vehicle_office_assignmentDTO.isDeleted = rs.getInt("isDeleted");
			vm_vehicle_office_assignmentDTO.lastModificationTime = rs.getLong("lastModificationTime");
			vm_vehicle_office_assignmentDTO.searchColumn = rs.getString("search_column");
			return vm_vehicle_office_assignmentDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}



	public Vm_vehicle_office_assignmentDTO buildForCount(ResultSet rs)
	{
		try
		{
			Vm_vehicle_office_assignmentDTO vm_vehicle_office_assignmentDTO = new Vm_vehicle_office_assignmentDTO();
			vm_vehicle_office_assignmentDTO.count = rs.getInt("count");
			return vm_vehicle_office_assignmentDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	
	
	
		
	

	//need another getter for repository







	public List<Vm_vehicle_office_assignmentDTO> getDTOsByOfficeIds(List<Long> officeIds){
		if(officeIds.isEmpty()){
			return new ArrayList<>();
		}

		return Vm_vehicle_office_assignmentRepository.getInstance().getVm_vehicle_office_assignmentList()
				.stream()
				.filter(i -> officeIds.contains(i.officeId))
				.collect(Collectors.toList());


//		String sql = "SELECT * ";
//
//		sql += " FROM " + tableName;
//
//		sql += " WHERE isDeleted = 0 and office_id IN ( ";
//
//		for(int i = 0;i<officeIds.size();i++){
//			if(i!=0){
//				sql+=",";
//			}
//			sql+= " ? ";
//		}
//		sql+=")  order by lastModificationTime desc";
//
//		printSql(sql);
//		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(officeIds.toArray()),this::build);



	}
	
	

	
	
	
	//add repository


	

	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ? ";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("vehicle_type_cat")
						|| str.equals("office_option")
						|| str.equals("remarks")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("vehicle_type_cat"))
					{
						AllFieldSql += "" + tableName + ".vehicle_type_cat = ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					 if(str.equals("office_option"))
					{
						AllFieldSql += "" + tableName + ".office_id = ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
//					else if(str.equals("remarks"))
//					{
//						AllFieldSql += "" + tableName + ".remarks like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("insertion_date_start"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("insertion_date_end"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
//						i ++;
//					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	



	public int getCountOfVehicleId (long vehicleId)
	{
		return Vm_vehicle_office_assignmentRepository.getInstance().
				getVm_vehicle_office_assignmentDTOByvehicle_id(vehicleId).size();


//		String sql = "SELECT COUNT(*) AS count FROM vm_vehicle_office_assignment ";
//		sql+=" WHERE isDeleted = 0 AND vehicle_id = ? " ;
//		printSql(sql);
//
//		int count = 0;
//
//		Vm_vehicle_office_assignmentDTO dto = ConnectionAndStatementUtil.
//				getT(sql, Arrays.asList(vehicleId),this::buildForCount);
//		if(dto != null){
//			count = dto.count;
//		}
//		return count;

	}

	public List<Vm_vehicle_office_assignmentDTO> getAllVm_vehicle_office_assignmentByVehicleId (long vehicleId )
	{
		return Vm_vehicle_office_assignmentRepository.getInstance().
				getVm_vehicle_office_assignmentDTOByvehicle_id(vehicleId);

//		String sql = "SELECT * FROM vm_vehicle_office_assignment";
//		sql+=" WHERE isDeleted =  0 AND vehicle_id = ? ";
//
//		sql += " order by vm_vehicle_office_assignment.lastModificationTime desc";
//		printSql(sql);
//
//		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(vehicleId),this::build);


	}

	public List<Vm_vehicle_office_assignmentDTO> getAllVm_vehicle_office_assignmentCount ()
	{
		List<Vm_vehicle_office_assignmentDTO> dtos = new ArrayList<>();
		Set<Long> officeIds = Vm_vehicle_office_assignmentRepository.getInstance().
				mapOfVm_vehicle_office_assignmentDTOToofficeId.keySet();

		officeIds.forEach(i -> {
			Vm_vehicle_office_assignmentDTO vm_vehicle_office_assignmentDTO = new Vm_vehicle_office_assignmentDTO();
			vm_vehicle_office_assignmentDTO.officeId = i;
			vm_vehicle_office_assignmentDTO.count = Vm_vehicle_office_assignmentRepository.getInstance().
					getVm_vehicle_office_assignmentDTOByoffice_id(i).size();
			dtos.add(vm_vehicle_office_assignmentDTO);
		});

		return dtos;

//		List<Vm_vehicle_office_assignmentDTO> vmVehicleOfficeAssignmentDTOS = Vm_vehicle_office_assignmentRepository.
//				getInstance().getVm_vehicle_office_assignmentList();
//
//
////		Set<Long> officeIds = vmVehicleOfficeAssignmentDTOS.stream().map(i -> i.officeId).collect(Collectors.toSet());



//		String sql = "SELECT office_id, COUNT(*) AS count FROM vm_vehicle_office_assignment ";
//		sql += " WHERE isDeleted = 0 GROUP BY office_id";
//		printSql(sql);
//		return ConnectionAndStatementUtil.getListOfT(sql,this::buildForVehicle_office_assignmentCount);
	}

	public Vm_vehicle_office_assignmentDTO buildForVehicle_office_assignmentCount(ResultSet rs)
	{
		try
		{
			Vm_vehicle_office_assignmentDTO vm_vehicle_office_assignmentDTO = new Vm_vehicle_office_assignmentDTO();
			vm_vehicle_office_assignmentDTO.officeId = rs.getLong("office_id");
			vm_vehicle_office_assignmentDTO.count = rs.getInt("count");
			return vm_vehicle_office_assignmentDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
				
}
	