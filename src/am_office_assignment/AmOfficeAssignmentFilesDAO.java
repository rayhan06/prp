package am_office_assignment;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

import am_office_approval_mapping.Am_office_approval_mappingDTO;
import common.CommonDAOService;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.*;
import pb.*;
import user.UserDTO;

public class AmOfficeAssignmentFilesDAO  implements CommonDAOService<AmOfficeAssignmentFilesDTO>
{

	private static final Logger logger = Logger.getLogger(AmOfficeAssignmentFilesDAO.class);

	private static final String addQuery = "INSERT INTO {tableName} (am_office_assignment_id,title," +
			"files_dropzone,insertion_date,inserted_by,modified_by,search_column,lastModificationTime," +
			"isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?)";

	private static final String updateQuery = "UPDATE {tableName} SET am_office_assignment_id = ?,title=?,files_dropzone=?," +
			"insertion_date=?,inserted_by=?,modified_by=?,search_column=?," +
			"lastModificationTime=? WHERE ID = ?";

	private static final String getByAmOfficeAssignmentId =
			"SELECT * FROM am_office_assignment_files WHERE am_office_assignment_id = %d AND isDeleted = 0 ORDER BY lastModificationTime DESC";

	private final Map<String,String> searchMap = new HashMap<>();

	private static class LazyLoader{
		static final AmOfficeAssignmentFilesDAO INSTANCE = new AmOfficeAssignmentFilesDAO();
	}

	public static AmOfficeAssignmentFilesDAO getInstance(){
		return AmOfficeAssignmentFilesDAO.LazyLoader.INSTANCE;
	}

	private AmOfficeAssignmentFilesDAO() {
//		searchMap.put("vehicle_type_cat"," and (vehicle_type_cat = ?)");
//		searchMap.put("vehicle_id"," and (vehicle_id = ?)");
//		searchMap.put("incident_cat"," and (incident_cat = ?)");
//		searchMap.put("incident_date_start"," and (incident_date>= ?)");
//		searchMap.put("incident_date_end"," and (incident_date <= ?)");
//		searchMap.put("AnyField"," and (search_column like ?)");
//		searchMap.put("inserted_by_organogram_id_internal"," and (inserted_by_organogram_id = ?)");
	}
	
	public void setSearchColumn(AmOfficeAssignmentFilesDTO amofficeassignmentfilesDTO)
	{
		amofficeassignmentfilesDTO.searchColumn = "";
		amofficeassignmentfilesDTO.searchColumn += amofficeassignmentfilesDTO.title + " ";
		amofficeassignmentfilesDTO.searchColumn += amofficeassignmentfilesDTO.insertedBy + " ";
		amofficeassignmentfilesDTO.searchColumn += amofficeassignmentfilesDTO.modifiedBy + " ";
	}
	

	
	


	@Override
	public void set(PreparedStatement ps, AmOfficeAssignmentFilesDTO amofficeassignmentfilesDTO, boolean isInsert) throws SQLException {

		setSearchColumn(amofficeassignmentfilesDTO);
		int index = 0;
		ps.setObject(++index,amofficeassignmentfilesDTO.amOfficeAssignmentId);
		ps.setObject(++index,amofficeassignmentfilesDTO.title);
		ps.setObject(++index,amofficeassignmentfilesDTO.filesDropzone);
		ps.setObject(++index,amofficeassignmentfilesDTO.insertionDate);
		ps.setObject(++index,amofficeassignmentfilesDTO.insertedBy);
		ps.setObject(++index,amofficeassignmentfilesDTO.modifiedBy);
		ps.setObject(++index,amofficeassignmentfilesDTO.searchColumn);
		ps.setObject(++index, amofficeassignmentfilesDTO.lastModificationTime);
		if(isInsert)
		{
			ps.setObject(++index, 0);
		}
		ps.setObject(++index, amofficeassignmentfilesDTO.iD);

	}

	@Override
	public AmOfficeAssignmentFilesDTO buildObjectFromResultSet(ResultSet rs) {
		try {
			AmOfficeAssignmentFilesDTO amofficeassignmentfilesDTO = new AmOfficeAssignmentFilesDTO();

			amofficeassignmentfilesDTO.iD = rs.getLong("ID");
			amofficeassignmentfilesDTO.amOfficeAssignmentId = rs.getLong("am_office_assignment_id");
			amofficeassignmentfilesDTO.title = rs.getString("title");
			amofficeassignmentfilesDTO.filesDropzone = rs.getLong("files_dropzone");
			amofficeassignmentfilesDTO.insertionDate = rs.getLong("insertion_date");
			amofficeassignmentfilesDTO.insertedBy = rs.getString("inserted_by");
			amofficeassignmentfilesDTO.modifiedBy = rs.getString("modified_by");
			amofficeassignmentfilesDTO.isDeleted = rs.getInt("isDeleted");
			amofficeassignmentfilesDTO.lastModificationTime = rs.getLong("lastModificationTime");
			amofficeassignmentfilesDTO.searchColumn = rs.getString("search_column");

			return amofficeassignmentfilesDTO;
		} catch (SQLException ex) {
			logger.error(ex);
			return null;
		}
	}

	@Override
	public String getTableName() {
		return "am_office_assignment_files";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	@Override
	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((AmOfficeAssignmentFilesDTO) commonDTO,addQuery,true);
	}

	@Override
	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((AmOfficeAssignmentFilesDTO) commonDTO,updateQuery,false);
	}

	public List<AmOfficeAssignmentFilesDTO> getByAmOfficeAssignmentId(Long amOfficeAssignmentId){
		String sql = String.format(getByAmOfficeAssignmentId, amOfficeAssignmentId);
		List<AmOfficeAssignmentFilesDTO> amOfficeAssignmentFilesDTOS = ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
		return amOfficeAssignmentFilesDTOS;
	}
}
	