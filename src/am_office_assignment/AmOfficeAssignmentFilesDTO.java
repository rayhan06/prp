package am_office_assignment;
import java.util.*; 
import util.*; 


public class AmOfficeAssignmentFilesDTO extends CommonDTO
{
    public long amOfficeAssignmentId = -1;
    public String title = "";
	public long filesDropzone = -1;
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
    //public String searchColumn = "";
	
	public List<AmOfficeAssignmentFilesDTO> amOfficeAssignmentFilesDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$AmOfficeAssignmentFilesDTO[" +
            " iD = " + iD +
            " amOfficeAssignmentId = " + amOfficeAssignmentId +
            " title = " + title +
            " filesDropzone = " + filesDropzone +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            "]";
    }

}