package am_office_assignment;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

import am_office_approval_mapping.Am_office_approval_mappingDTO;
import am_office_assignment_request.Am_office_assignment_requestDAO;
import am_office_assignment_request.Am_office_assignment_requestDTO;
import common.CommonDAOService;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.*;
import pb.*;
import user.UserDTO;

public class Am_office_assignmentDAO  implements CommonDAOService<Am_office_assignmentDTO>
{

	private static final Logger logger = Logger.getLogger(Am_office_assignmentDAO.class);

	private static final String addQuery = "INSERT INTO {tableName} (office_unit_id,am_office_assignment_request_id," +
			"building_type_cat,assignment_date,level,block,room_no,side,unit," +
			"remarks,insertion_date,inserted_by,modified_by,search_column,lastModificationTime,office_type," +

			"requester_org_id,requester_office_id," +
			"requester_office_unit_id,requester_emp_id," +
			"requester_phone_num,requester_name_en,requester_name_bn,requester_office_name_en,requester_office_name_bn," +
			"requester_office_unit_name_en," +
			"requester_office_unit_name_bn,requester_office_unit_org_name_en,requester_office_unit_org_name_bn,assignment_details_en,assignment_details_bn," +
			"withdrawal_date,status,concatedOfcTypeAndOfcUnit,"+
			"isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static final String updateQuery = "UPDATE {tableName} SET office_unit_id = ?,am_office_assignment_request_id=?,building_type_cat=?," +
			"assignment_date=?,level=?,block=?,room_no=?,side=?,unit=?," +
			"remarks=?,insertion_date=?,inserted_by=?,modified_by=?,search_column=?," +
			"lastModificationTime=?,office_type=?,requester_org_id=?,requester_office_id=?,requester_office_unit_id=?,requester_emp_id=?," +
			"requester_phone_num=?,requester_name_en=?,requester_name_bn=?,requester_office_name_en=?,requester_office_name_bn=?,requester_office_unit_name_en=?," +
			"requester_office_unit_name_bn=?,requester_office_unit_org_name_en=?,requester_office_unit_org_name_bn=?,assignment_details_en=?," +
			"assignment_details_bn=?,withdrawal_date=?,status=?,concatedOfcTypeAndOfcUnit=? WHERE ID = ?";

	private static final String getByAmOfficeAssignmentRequestId =
			"SELECT * FROM am_office_assignment WHERE am_office_assignment_request_id = %d AND isDeleted = 0";

	private final Map<String,String> searchMap = new HashMap<>();

	private static class LazyLoader{
		static final Am_office_assignmentDAO INSTANCE = new Am_office_assignmentDAO();
	}

	public static Am_office_assignmentDAO getInstance(){
		return Am_office_assignmentDAO.LazyLoader.INSTANCE;
	}

	private Am_office_assignmentDAO() {
		searchMap.put("employeeRecordsId", " AND (requester_emp_id = ?)");
		searchMap.put("amOfficeAssignmentStatusCat", " AND (status = ?)");
		searchMap.put("building_type_cat", " AND (building_type_cat = ?)");
		searchMap.put("assignment_date", " AND (assignment_date = ?)");
		searchMap.put("AnyField", " and (search_column like ?)");
	}
	
	public void setSearchColumn(Am_office_assignmentDTO am_office_assignmentDTO)
	{
		am_office_assignmentDTO.searchColumn = "";
		am_office_assignmentDTO.searchColumn += CatDAO.getName("English", "building_type", am_office_assignmentDTO.buildingTypeCat) + " " + CatDAO.getName("Bangla", "building_type", am_office_assignmentDTO.buildingTypeCat) + " ";
		am_office_assignmentDTO.searchColumn += am_office_assignmentDTO.requesterNameEn + " ";
		am_office_assignmentDTO.searchColumn += am_office_assignmentDTO.requesterNameBn + " ";
		am_office_assignmentDTO.searchColumn += am_office_assignmentDTO.requesterOfficeUnitOrgNameEn + " ";
		am_office_assignmentDTO.searchColumn += am_office_assignmentDTO.requesterOfficeUnitOrgNameBn + " ";
		am_office_assignmentDTO.searchColumn += am_office_assignmentDTO.requesterOfficeUnitNameEn + " ";
		am_office_assignmentDTO.searchColumn += am_office_assignmentDTO.requesterOfficeUnitNameBn + " ";
		am_office_assignmentDTO.searchColumn += StringUtils.getFormattedDate("English",am_office_assignmentDTO.assignmentDate) + " ";
		am_office_assignmentDTO.searchColumn += StringUtils.getFormattedDate("Bangla",am_office_assignmentDTO.assignmentDate) + " ";
		am_office_assignmentDTO.searchColumn += CatRepository.getInstance().getText(
				"English", "am_office_assignment_request_status", am_office_assignmentDTO.status
		) + " ";
		am_office_assignmentDTO.searchColumn += CatRepository.getInstance().getText(
				"Bangla", "am_office_assignment_request_status", am_office_assignmentDTO.status
		) + " ";
	}

	@Override
	public void set(PreparedStatement ps, Am_office_assignmentDTO am_office_assignmentDTO, boolean isInsert) throws SQLException {
		setSearchColumn(am_office_assignmentDTO);
		int index = 0;

		ps.setObject(++index,am_office_assignmentDTO.officeUnitId);
		ps.setObject(++index,am_office_assignmentDTO.amOfficeAssignmentRequestId);
		ps.setObject(++index,am_office_assignmentDTO.buildingTypeCat);
		ps.setObject(++index,am_office_assignmentDTO.assignmentDate);
		ps.setObject(++index,am_office_assignmentDTO.level);
		ps.setObject(++index,am_office_assignmentDTO.block);
		ps.setObject(++index,am_office_assignmentDTO.roomNo);
		ps.setObject(++index,am_office_assignmentDTO.side);
		ps.setObject(++index,am_office_assignmentDTO.unit);
		ps.setObject(++index,am_office_assignmentDTO.remarks);
		ps.setObject(++index,am_office_assignmentDTO.insertionDate);
		ps.setObject(++index,am_office_assignmentDTO.insertedBy);
		ps.setObject(++index,am_office_assignmentDTO.modifiedBy);
		ps.setObject(++index,am_office_assignmentDTO.searchColumn);
		ps.setObject(++index, am_office_assignmentDTO.lastModificationTime);
		ps.setObject(++index, am_office_assignmentDTO.officeType);

		ps.setObject(++index,am_office_assignmentDTO.requesterOrgId);
		ps.setObject(++index,am_office_assignmentDTO.requesterOfficeId);
		ps.setObject(++index,am_office_assignmentDTO.requesterOfficeUnitId);
		ps.setObject(++index,am_office_assignmentDTO.requesterEmpId);
		ps.setObject(++index,am_office_assignmentDTO.requesterPhoneNum);
		ps.setObject(++index,am_office_assignmentDTO.requesterNameEn);
		ps.setObject(++index,am_office_assignmentDTO.requesterNameBn);
		ps.setObject(++index,am_office_assignmentDTO.requesterOfficeNameEn);
		ps.setObject(++index,am_office_assignmentDTO.requesterOfficeNameBn);
		ps.setObject(++index,am_office_assignmentDTO.requesterOfficeUnitNameEn);
		ps.setObject(++index,am_office_assignmentDTO.requesterOfficeUnitNameBn);
		ps.setObject(++index,am_office_assignmentDTO.requesterOfficeUnitOrgNameEn);
		ps.setObject(++index,am_office_assignmentDTO.requesterOfficeUnitOrgNameBn);

		ps.setObject(++index,am_office_assignmentDTO.assignmentDetailsEn);
		ps.setObject(++index,am_office_assignmentDTO.assignmentDetailsBn);

		ps.setObject(++index,am_office_assignmentDTO.withdrawalDate);
		ps.setObject(++index,am_office_assignmentDTO.status);

		ps.setObject(++index,am_office_assignmentDTO.concatedOfcTypeAndOfcUnit);


		if (isInsert) {
			ps.setObject(++index, 0);
		}
		ps.setObject(++index, am_office_assignmentDTO.iD);


	}

	@Override
	public Am_office_assignmentDTO buildObjectFromResultSet(ResultSet rs) {
		try {
			Am_office_assignmentDTO am_office_assignmentDTO = new Am_office_assignmentDTO();
			am_office_assignmentDTO.iD = rs.getLong("ID");
			am_office_assignmentDTO.officeUnitId = rs.getLong("office_unit_id");
			am_office_assignmentDTO.amOfficeAssignmentRequestId = rs.getLong("am_office_assignment_request_id");
			am_office_assignmentDTO.buildingTypeCat = rs.getInt("building_type_cat");
			am_office_assignmentDTO.assignmentDate = rs.getLong("assignment_date");
			am_office_assignmentDTO.level = rs.getLong("level");
			am_office_assignmentDTO.block = rs.getLong("block");
			am_office_assignmentDTO.roomNo = rs.getLong("room_no");
			am_office_assignmentDTO.side = rs.getLong("side");
			am_office_assignmentDTO.unit = rs.getLong("unit");
			am_office_assignmentDTO.remarks = rs.getString("remarks");
			am_office_assignmentDTO.insertionDate = rs.getLong("insertion_date");
			am_office_assignmentDTO.insertedBy = rs.getString("inserted_by");
			am_office_assignmentDTO.modifiedBy = rs.getString("modified_by");
			am_office_assignmentDTO.isDeleted = rs.getInt("isDeleted");
			am_office_assignmentDTO.lastModificationTime = rs.getLong("lastModificationTime");
			am_office_assignmentDTO.searchColumn = rs.getString("search_column");
			am_office_assignmentDTO.officeType = rs.getLong("office_type");
			am_office_assignmentDTO.requesterOrgId = rs.getLong("requester_org_id");
			am_office_assignmentDTO.requesterOfficeId = rs.getLong("requester_office_id");
			am_office_assignmentDTO.requesterOfficeUnitId = rs.getLong("requester_office_unit_id");
			am_office_assignmentDTO.requesterEmpId = rs.getLong("requester_emp_id");
			am_office_assignmentDTO.requesterPhoneNum = rs.getString("requester_phone_num");
			am_office_assignmentDTO.requesterNameEn = rs.getString("requester_name_en");
			am_office_assignmentDTO.requesterNameBn = rs.getString("requester_name_bn");
			am_office_assignmentDTO.requesterOfficeNameEn = rs.getString("requester_office_name_en");
			am_office_assignmentDTO.requesterOfficeNameBn = rs.getString("requester_office_name_bn");
			am_office_assignmentDTO.requesterOfficeUnitNameEn = rs.getString("requester_office_unit_name_en");
			am_office_assignmentDTO.requesterOfficeUnitNameBn = rs.getString("requester_office_unit_name_bn");
			am_office_assignmentDTO.requesterOfficeUnitOrgNameEn = rs.getString("requester_office_unit_org_name_en");
			am_office_assignmentDTO.requesterOfficeUnitOrgNameBn = rs.getString("requester_office_unit_org_name_bn");

			am_office_assignmentDTO.assignmentDetailsEn = rs.getString("assignment_details_en");
			am_office_assignmentDTO.assignmentDetailsBn = rs.getString("assignment_details_bn");

			am_office_assignmentDTO.withdrawalDate = rs.getLong("withdrawal_date");
			am_office_assignmentDTO.status = rs.getInt("status");

			am_office_assignmentDTO.concatedOfcTypeAndOfcUnit = rs.getString("concatedOfcTypeAndOfcUnit");

			return am_office_assignmentDTO;
		} catch (SQLException ex) {
			logger.error(ex);
			return null;
		}
	}


	@Override
	public String getTableName() {
		return "am_office_assignment";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	@Override
	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_office_assignmentDTO) commonDTO,addQuery,true);
	}

	@Override
	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_office_assignmentDTO) commonDTO,updateQuery,false);
	}

	public Am_office_assignmentDTO getByAmOfficeAssignmentRequestId(Long amOfficeAssignmentRequestId) {
		String sql = String.format(getByAmOfficeAssignmentRequestId, amOfficeAssignmentRequestId);
		Am_office_assignmentDTO am_office_assignmentDTO = ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
		return am_office_assignmentDTO;
	}

	public int getCountByParliamentBuildingRoomId(long roomId){
		String countQuery = "SELECT count(*) as countID FROM "
				+ getTableName() + " where isDeleted = 0 and building_type_cat = 1 and room_no = ? ";
		return ConnectionAndStatementUtil.getT(countQuery, Arrays.asList(roomId), rs -> {
			try {
				return rs.getInt("countID");
			} catch (SQLException ex) {
				ex.printStackTrace();
				return 0;
			}
		}, 0);
	}
}
	