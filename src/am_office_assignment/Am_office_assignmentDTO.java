package am_office_assignment;
import java.util.*; 
import util.*; 


public class Am_office_assignmentDTO extends CommonDTO
{

	public long officeUnitId = -1;
	public long amOfficeAssignmentRequestId = -1;
	public int buildingTypeCat = -1;
	public long assignmentDate = System.currentTimeMillis();
	public long level = -1;
	public long block = -1;
	public long roomNo = -1;
	public long side = -1;
	public long unit = -1;
    //public String remarks = "";
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
    //public String searchColumn = "";

	public long officeType = -1;

	public long requesterOrgId = -1;
	public long requesterOfficeId = -1;
	public long requesterOfficeUnitId = -1;
	public long requesterEmpId = -1;
	public String requesterPhoneNum = "";
	public String requesterNameEn = "";
	public String requesterNameBn = "";
	public String requesterOfficeNameEn = "";
	public String requesterOfficeNameBn = "";
	public String requesterOfficeUnitNameEn = "";
	public String requesterOfficeUnitNameBn = "";
	public String requesterOfficeUnitOrgNameEn = "";
	public String requesterOfficeUnitOrgNameBn = "";

	public String assignmentDetailsEn = "";
	public String assignmentDetailsBn = "";

	public long withdrawalDate = -1;
	public int status = -1;

	public String concatedOfcTypeAndOfcUnit = "";
	
	public List<AmOfficeAssignmentFilesDTO> amOfficeAssignmentFilesDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Am_office_assignmentDTO[" +
            " iD = " + iD +
            " officeUnitId = " + officeUnitId +
            " amOfficeAssignmentRequestId = " + amOfficeAssignmentRequestId +
            " buildingTypeCat = " + buildingTypeCat +
            " assignmentDate = " + assignmentDate +
            " level = " + level +
            " block = " + block +
            " roomNo = " + roomNo +
            " side = " + side +
            " unit = " + unit +
            " remarks = " + remarks +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
			" officeType = " + officeType +
			" requesterOrgId = " + requesterOrgId +
			" requesterOfficeId = " + requesterOfficeId +
			" requesterOfficeUnitId = " + requesterOfficeUnitId +
			" requesterEmpId = " + requesterEmpId +
			" requesterPhoneNum = " + requesterPhoneNum +
			" requesterNameEn = " + requesterNameEn +
			" requesterNameBn = " + requesterNameBn +
			" requesterOfficeNameEn = " + requesterOfficeNameEn +
			" requesterOfficeNameBn = " + requesterOfficeNameBn +
			" requesterOfficeUnitNameEn = " + requesterOfficeUnitNameEn +
			" requesterOfficeUnitNameBn = " + requesterOfficeUnitNameBn +
			" requesterOfficeUnitOrgNameEn = " + requesterOfficeUnitOrgNameEn +
			" requesterOfficeUnitOrgNameBn = " + requesterOfficeUnitOrgNameBn +
			" assignmentDetailsEn = " + assignmentDetailsEn +
			" assignmentDetailsBn = " + assignmentDetailsBn +
			" withdrawalDate = " + withdrawalDate +
			" status = " + status +
			" concatedOfcTypeAndOfcUnit = " + concatedOfcTypeAndOfcUnit +
            "]";
    }

}