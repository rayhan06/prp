package am_office_assignment;
import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import am_minister_hostel_block.AmMinisterHostelSideRepository;
import am_minister_hostel_block.Am_minister_hostel_blockRepository;
import am_minister_hostel_level.Am_minister_hostel_levelDAO;
import am_minister_hostel_level.Am_minister_hostel_levelDTO;
import am_minister_hostel_level.Am_minister_hostel_levelRepository;
import am_minister_hostel_unit.Am_minister_hostel_unitRepository;
import am_office_approval_mapping.Am_office_approval_mappingDAO;
import am_office_approval_mapping.Am_office_approval_mappingDTO;
import am_office_assignment_request.Am_office_assignment_requestDAO;
import am_office_assignment_request.Am_office_assignment_requestDTO;
import am_parliament_building_block.Am_parliament_building_blockRepository;
import am_parliament_building_level.Am_parliament_building_levelDTO;
import am_parliament_building_level.Am_parliament_building_levelRepository;
import am_parliament_building_room.Am_parliament_building_roomDAO;
import am_parliament_building_room.Am_parliament_building_roomDTO;
import common.BaseServlet;
import common.CommonDAOService;
import dbm.DBMW;
import office_units.Office_unitsRepository;
import offices.OfficesDTO;
import offices.OfficesRepository;
import org.apache.log4j.Logger;
import login.LoginDTO;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;
import java.util.*;
import javax.servlet.http.*;
import language.LC;
import language.LM;
import common.ApiResponse;
import com.google.gson.Gson;
import pb.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import vm_requisition.CommonApprovalStatus;

@SuppressWarnings({"Duplicates"})
@WebServlet("/Am_office_assignmentServlet")
@MultipartConfig
public class Am_office_assignmentServlet extends BaseServlet {

	private static final long serialVersionUID = 1L;
	public static Logger logger = Logger.getLogger(Am_office_assignmentServlet.class);


	private final Gson gson = new Gson();

	@Override
	public String getTableName() {
		return Am_office_assignmentDAO.getInstance().getTableName();
	}

	@Override
	public String getServletName() {
		return "Am_office_assignmentServlet";
	}

	@Override
	public CommonDAOService getCommonDAOService() {
		return Am_office_assignmentDAO.getInstance();
	}

	@Override
	public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {

		Am_office_assignmentDTO am_office_assignmentDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

		String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		boolean isLanEng = Language.equalsIgnoreCase("English");

		if(addFlag == true)
		{
			am_office_assignmentDTO = new Am_office_assignmentDTO();

			am_office_assignmentDTO.insertionDate = am_office_assignmentDTO.lastModificationTime = System.currentTimeMillis();
			am_office_assignmentDTO.insertedBy = am_office_assignmentDTO.modifiedBy = String.valueOf(userDTO.ID);
		}
		else
		{
			am_office_assignmentDTO = Am_office_assignmentDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));

			if (am_office_assignmentDTO == null) {
				throw new Exception(isLanEng?"Office assignment information is not found":"অফিস বরাদ্দের তথ্য খুঁজে পাওয়া যায় নি");
			}
			am_office_assignmentDTO.lastModificationTime = System.currentTimeMillis();
			am_office_assignmentDTO.modifiedBy = String.valueOf(userDTO.ID);
		}

		String Value = "";
		String pbLevel="",pbBlock="",pbRoom="";
		String mhBlock="",mhSide="",mhUnit="",mhLevel="";

		String officeType =  request.getParameter("officeType");

		switch (officeType){
			case "1":
				Value = request.getParameter("officeUnitId");
				if (Value == null ||Value.equals("") || Value.equals("-1")) {
					throw new Exception(isLanEng?"Please provide parliament office":"অনুগ্রহপূর্বক সংসদের অফিস প্রদান করুন");
				}
				break;
			case "2":
				Value = request.getParameter("select2OtherOffice");
				if (Value == null ||Value.equals("") || Value.equals("-1")) {
					throw new Exception(isLanEng?"Please provide office":"অনুগ্রহপূর্বক অফিস প্রদান করুন");
				}
				break;
			default:
				throw new Exception(isLanEng?"Please provide office type":"অনুগ্রহপূর্বক অফিসের ধরন প্রদান করুন");
		}


		am_office_assignmentDTO.officeType = Long.parseLong(officeType);
		am_office_assignmentDTO.officeUnitId = Long.parseLong(Value);

		if (request.getParameter("amOfficeAssignmentRequestId") == null || request.getParameter("amOfficeAssignmentRequestId").equals("-1")) {
			throw new Exception(isLanEng?"Office assignment request information is not found":"অফিস বরাদ্দের তথ্য খুঁজে পাওয়া যায় নি");
		}
		Value = request.getParameter("amOfficeAssignmentRequestId");
		am_office_assignmentDTO.amOfficeAssignmentRequestId = Long.parseLong(Value);
		Am_office_assignment_requestDTO am_office_assignment_requestDTO =  Am_office_assignment_requestDAO.getInstance().getDTOFromID(am_office_assignmentDTO.amOfficeAssignmentRequestId );
		am_office_assignment_requestDTO.status = CommonApprovalStatus.SATISFIED.getValue();

		am_office_assignmentDTO.requesterEmpId = am_office_assignment_requestDTO.requesterEmpId;
		am_office_assignmentDTO.requesterOfficeUnitId = am_office_assignment_requestDTO.requesterOfficeUnitId;
		am_office_assignmentDTO.requesterOrgId = am_office_assignment_requestDTO.requesterOrgId;

		am_office_assignmentDTO.requesterNameEn = am_office_assignment_requestDTO.requesterNameEn;
		am_office_assignmentDTO.requesterNameBn = am_office_assignment_requestDTO.requesterNameBn;
		am_office_assignmentDTO.requesterOfficeUnitNameEn = am_office_assignment_requestDTO.requesterOfficeUnitNameEn;
		am_office_assignmentDTO.requesterOfficeUnitNameBn = am_office_assignment_requestDTO.requesterOfficeUnitNameBn;
		am_office_assignmentDTO.requesterOfficeUnitOrgNameEn = am_office_assignment_requestDTO.requesterNameBn;
		am_office_assignmentDTO.requesterOfficeUnitOrgNameBn = am_office_assignment_requestDTO.requesterOfficeUnitOrgNameBn;

		am_office_assignmentDTO.requesterPhoneNum = am_office_assignment_requestDTO.requesterPhoneNum;

		OfficesDTO requesterOffice = OfficesRepository.getInstance().getOfficesDTOByID(am_office_assignmentDTO.requesterOfficeId);

		if (requesterOffice != null) {
			am_office_assignmentDTO.requesterOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(am_office_assignment_requestDTO.requesterOfficeUnitId).officeId;

			am_office_assignmentDTO.requesterOfficeNameEn = requesterOffice.officeNameEng;
			am_office_assignmentDTO.requesterOfficeNameBn = requesterOffice.officeNameBng;
		}

		if (request.getParameter("buildingTypeCat") == null || request.getParameter("buildingTypeCat").equals("") || request.getParameter("buildingTypeCat").equals("-1")) {
			throw new Exception(isLanEng?"Please provide building type":"অনুগ্রহপূর্বক ভবনের ধরন প্রদান করুন");
		}
		Value = request.getParameter("buildingTypeCat");
		am_office_assignmentDTO.buildingTypeCat = Integer.parseInt(Value);


		if (request.getParameter("assignmentDate") == null) {
			throw new Exception(isLanEng?"Please provide allocation date":"অনুগ্রহপূর্বক বরাদ্দের তারিখ করুন");
		}
		Value = request.getParameter("assignmentDate");
		am_office_assignmentDTO.assignmentDate = f.parse(Value).getTime();


		switch (am_office_assignmentDTO.buildingTypeCat){
			case 1:
				pbLevel = request.getParameter("parliamentBuildingLevel");
				pbBlock = request.getParameter("parliamentBuildingBlock");
				pbRoom = request.getParameter("roomNo");
				if (pbLevel == null || pbLevel.equalsIgnoreCase("")) {
					throw new Exception(isLanEng?"Please provide level":"অনুগ্রহপূর্বক লেভেল প্রদান করুন");
				}
				if (pbBlock == null || pbBlock.equalsIgnoreCase("")) {
					throw new Exception(isLanEng?"Please provide block":"অনুগ্রহপূর্বক ব্লক প্রদান করুন");
				}
				if (pbRoom == null || pbRoom.equalsIgnoreCase("")) {
					throw new Exception(isLanEng?"Please provide room":"অনুগ্রহপূর্বক কক্ষ প্রদান করুন");
				}
				am_office_assignmentDTO.level = Long.parseLong(pbLevel);
				am_office_assignmentDTO.block = Long.parseLong(pbBlock);

				Am_parliament_building_roomDTO am_parliament_building_roomDTO =  Am_parliament_building_roomDAO.getInstance().getDTOFromID(Long.parseLong(pbRoom));
				//if(am_parliament_building_roomDTO.status == CommonApprovalStatus.AVAILABLE.getValue()){
				if(true){
					am_office_assignmentDTO.roomNo = Long.parseLong(pbRoom);
					am_parliament_building_roomDTO.status = CommonApprovalStatus.USED.getValue();
					am_parliament_building_roomDTO.lastModificationTime = System.currentTimeMillis();
					Am_parliament_building_roomDAO.getInstance().update(am_parliament_building_roomDTO);

					Am_parliament_building_levelDTO pbLevelDTO = Am_parliament_building_levelRepository.getInstance().getAm_parliament_building_levelDTOByID(am_office_assignmentDTO.level);
					//Am_parliament_building_blockDTO pbBlockDTO = Am_parliament_building_blockRepository.getInstance().getAm_parliament_building_blockDTOByID(am_office_assignmentDTO.block);

					am_office_assignmentDTO.assignmentDetailsEn = "Level: "+pbLevelDTO.levelNo
							+", Block: " +Am_parliament_building_blockRepository.getInstance().getText(am_office_assignmentDTO.block,"English")
					        +", Room: "+am_parliament_building_roomDTO.roomNo;
					
					am_office_assignmentDTO.assignmentDetailsBn = "লেভেল: "+pbLevelDTO.levelNo
							+", ব্লক: " +Am_parliament_building_blockRepository.getInstance().getText(am_office_assignmentDTO.block,"Bangla")
							+", রুম: "+am_parliament_building_roomDTO.roomNo;

				}
				else{
					throw new Exception(isLanEng?"Selected room is not available":"বাছাইকৃত কক্ষ ইতিমধ্যে অন্যকে দেওয়া হয়েছে");
				}


				break;
			case 2:
				mhBlock = request.getParameter("mpHostelBlock");
				mhSide=request.getParameter("side");
				mhUnit=request.getParameter("unit");
				mhLevel = request.getParameter("mpHostelLevel");
				if (mhBlock == null || mhBlock.equalsIgnoreCase("")) {
					throw new Exception(isLanEng?"Please provide block":"অনুগ্রহপূর্বক ব্লক প্রদান করুন");
				}
				if (mhSide == null || mhSide.equalsIgnoreCase("")) {
					throw new Exception(isLanEng?"Please provide side":"অনুগ্রহপূর্বক সাইড প্রদান করুন");
				}
				if (mhUnit == null || mhUnit.equalsIgnoreCase("")) {
					throw new Exception(isLanEng?"Please provide unit":"অনুগ্রহপূর্বক ইউনিট প্রদান করুন");
				}
				if (mhLevel == null || mhLevel.equalsIgnoreCase("")) {
					throw new Exception(isLanEng?"Please provide level":"অনুগ্রহপূর্বক লেভেল প্রদান করুন");
				}
				am_office_assignmentDTO.block = Long.parseLong(mhBlock);
				am_office_assignmentDTO.side = Long.parseLong(mhSide);
				am_office_assignmentDTO.unit = Long.parseLong(mhUnit);

				Am_minister_hostel_levelDTO am_minister_hostel_levelDTO =  Am_minister_hostel_levelDAO.getInstance().getDTOFromID(Long.parseLong(mhLevel));
				//if(am_minister_hostel_levelDTO.status == CommonApprovalStatus.AVAILABLE.getValue()){
				if(true){
					am_office_assignmentDTO.level = Long.parseLong(mhLevel);
					am_minister_hostel_levelDTO.status = CommonApprovalStatus.USED.getValue();
					am_minister_hostel_levelDTO.lastModificationTime = System.currentTimeMillis();
					Am_minister_hostel_levelDAO.getInstance().update(am_minister_hostel_levelDTO);



					am_office_assignmentDTO.assignmentDetailsEn = "Block: "+Am_minister_hostel_blockRepository.getInstance().getAm_minister_hostel_blockDTOByID(am_office_assignmentDTO.block).blockNo
							+", Side: " + CatRepository.getInstance().getText("English","am_minister_hostel_side", AmMinisterHostelSideRepository.getInstance().getAmMinisterHostelSideDTOByID(am_office_assignmentDTO.side).ministerHostelSideCat)
							+", Unit: "+Am_minister_hostel_unitRepository.getInstance().getAm_minister_hostel_unitDTOByID(am_office_assignmentDTO.unit).unitNumber
							+", Level: "+CatRepository.getInstance().getText("English","am_minister_hostel_level", Am_minister_hostel_levelRepository.getInstance().getAm_minister_hostel_levelDTOByID(am_office_assignmentDTO.level).amMinisterHostelLevelCat);

					am_office_assignmentDTO.assignmentDetailsBn = "ব্লক: "+Am_minister_hostel_blockRepository.getInstance().getAm_minister_hostel_blockDTOByID(am_office_assignmentDTO.block).blockNo
							+", পাশ: " + CatRepository.getInstance().getText("Bangla","am_minister_hostel_side", AmMinisterHostelSideRepository.getInstance().getAmMinisterHostelSideDTOByID(am_office_assignmentDTO.side).ministerHostelSideCat)
							+", ইউনিট: "+Am_minister_hostel_unitRepository.getInstance().getAm_minister_hostel_unitDTOByID(am_office_assignmentDTO.unit).unitNumber
							+", লেভেল: "+CatRepository.getInstance().getText("Bangla","am_minister_hostel_level", Am_minister_hostel_levelRepository.getInstance().getAm_minister_hostel_levelDTOByID(am_office_assignmentDTO.level).amMinisterHostelLevelCat);
				}
				else{
					throw new Exception(isLanEng?"Selected level is unavailable":"বাছাইকৃত লেভেল ইতিমধ্যে অন্যকে দেওয়া হয়েছে");
				}
				break;
			default:

		}


		Value = request.getParameter("remarks");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("remarks = " + Value);
		if(Value != null && !Value.isEmpty())
		{
			am_office_assignmentDTO.remarks = (Value);
		}
		else
		{
			logger.debug("FieldName has a null Value, not updating" + " = " + Value);
		}

		am_office_assignmentDTO.status = CommonApprovalStatus.SATISFIED.getValue();
		am_office_assignmentDTO.concatedOfcTypeAndOfcUnit = am_office_assignmentDTO.officeType+","+ am_office_assignmentDTO.officeUnitId;


		if (addFlag) {
			Am_office_assignmentDAO.getInstance().add(am_office_assignmentDTO);
			Am_office_assignment_requestDAO.getInstance().update(am_office_assignment_requestDTO);
		} else {
			Am_office_assignmentDAO.getInstance().update(am_office_assignmentDTO);
			Am_office_assignment_requestDAO.getInstance().update(am_office_assignment_requestDTO);
		}

		List<AmOfficeAssignmentFilesDTO> amOfficeAssignmentFilesDTOList = createAmOfficeAssignmentFilesDTOListByRequest(request, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language,userDTO);




		if(addFlag == true)
		{
			if(amOfficeAssignmentFilesDTOList != null)
			{
				for(AmOfficeAssignmentFilesDTO amOfficeAssignmentFilesDTO: amOfficeAssignmentFilesDTOList)
				{
					amOfficeAssignmentFilesDTO.amOfficeAssignmentId = am_office_assignmentDTO.iD;
					AmOfficeAssignmentFilesDAO.getInstance().add(amOfficeAssignmentFilesDTO);
				}
			}

		}
		return  am_office_assignmentDTO;

	}

	private List<AmOfficeAssignmentFilesDTO> createAmOfficeAssignmentFilesDTOListByRequest(HttpServletRequest request, String language,UserDTO userDTO) throws Exception{
		List<AmOfficeAssignmentFilesDTO> amOfficeAssignmentFilesDTOList = new ArrayList<AmOfficeAssignmentFilesDTO>();
		if(request.getParameterValues("amOfficeAssignmentFiles.iD") != null)
		{
			int amOfficeAssignmentFilesItemNo = request.getParameterValues("amOfficeAssignmentFiles.iD").length;


			for(int index=0;index<amOfficeAssignmentFilesItemNo;index++){
				AmOfficeAssignmentFilesDTO amOfficeAssignmentFilesDTO = createAmOfficeAssignmentFilesDTOByRequestAndIndex(request,true,index, language,userDTO);
				amOfficeAssignmentFilesDTOList.add(amOfficeAssignmentFilesDTO);
			}

			return amOfficeAssignmentFilesDTOList;
		}
		return null;
	}


	private AmOfficeAssignmentFilesDTO createAmOfficeAssignmentFilesDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, String language,UserDTO userDTO) throws Exception{


		AmOfficeAssignmentFilesDTO amOfficeAssignmentFilesDTO=null;
		if(addFlag == true )
		{
			amOfficeAssignmentFilesDTO = new AmOfficeAssignmentFilesDTO();
		}
		else
		{
			amOfficeAssignmentFilesDTO = AmOfficeAssignmentFilesDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameterValues("amOfficeAssignmentFiles.iD")[index]));
		}


		String Value = "";
		Value = request.getParameterValues("amOfficeAssignmentFiles.title")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		else
		{
			throw new Exception(LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_AM_OFFICE_ASSIGNMENT_FILES_TITLE, language) + " " + ErrorMessage.getInvalidMessage(language));
		}

		amOfficeAssignmentFilesDTO.title = (Value);
		Value = request.getParameterValues("amOfficeAssignmentFiles.filesDropzone")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		else
		{
			throw new Exception(LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_AM_OFFICE_ASSIGNMENT_FILES_FILESDROPZONE, language) + " " + ErrorMessage.getInvalidMessage(language));
		}

		amOfficeAssignmentFilesDTO.filesDropzone = Long.parseLong(Value);

		amOfficeAssignmentFilesDTO.insertionDate = amOfficeAssignmentFilesDTO.lastModificationTime = System.currentTimeMillis();
		amOfficeAssignmentFilesDTO.insertedBy = amOfficeAssignmentFilesDTO.modifiedBy = String.valueOf(userDTO.ID);

		return amOfficeAssignmentFilesDTO;

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		if (userDTO == null) {
			request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			return;
		}

		try {
			String actionType = request.getParameter("actionType");
			switch (actionType) {
				case "getFileNextSequenceId":
					System.out.println("In getFileNextSequenceId");

					try {
						String responseText = String.valueOf(DBMW.getInstance().getNextSequenceId("fileid"));
                        ApiResponse.sendSuccessResponse(response, responseText);
					} catch (Exception ex) {
						logger.error(ex);
						ApiResponse.sendErrorResponse(response, ex.getMessage());
					}
					return;

				case "getAddPage":
					long amOfficeAssignmentRequestId = Long.parseLong(request.getParameter("amOfficeAssignmentRequestId"));
					setApprovalPageData(amOfficeAssignmentRequestId, request, userDTO);
					request.getRequestDispatcher("am_office_assignment/am_office_assignmentEdit.jsp").forward(request, response);
					return;
				default:
					super.doGet(request, response);
					return;
			}
		} catch (Exception ex) {
			logger.error(ex);
		}
		request.getRequestDispatcher("common/error_page.jsp").forward(request, response);


	}

	private void setApprovalPageData(long amOfficeAssignmentRequestId, HttpServletRequest request, UserDTO userDTO) throws Exception {
		Am_office_assignment_requestDTO am_office_assignment_requestDTO = Am_office_assignment_requestDAO.getInstance().getDTOByID(amOfficeAssignmentRequestId);
		if (am_office_assignment_requestDTO == null)
			throw new Exception("No application for office assignment is not found with id = " + amOfficeAssignmentRequestId);
		request.setAttribute("am_office_assignment_requestDTO", am_office_assignment_requestDTO);

		Am_office_approval_mappingDTO approverAmOfficeAssignmentRequestDTO =
				Am_office_approval_mappingDAO.getInstance().getByAmOfficeAssignmentRequestAndApproverRecordsId(amOfficeAssignmentRequestId, userDTO.employee_record_id);

		if (approverAmOfficeAssignmentRequestDTO == null)
			throw new Exception("User Not allowed to approve  for office with  id = " + amOfficeAssignmentRequestId);
		request.setAttribute("approverAmOfficeAssignmentRequestDTO", approverAmOfficeAssignmentRequestDTO);
	}


	@Override
	public int[] getAddPageMenuConstants() {
		return new int[] {MenuConstants.AM_OFFICE_ASSIGNMENT_ADD};
	}

	@Override
	public int[] getEditPageMenuConstants() {
		return new int[] {MenuConstants.AM_OFFICE_ASSIGNMENT_UPDATE};
	}

	@Override
	public int[] getSearchMenuConstants() {
		return new int[] {MenuConstants.AM_OFFICE_ASSIGNMENT_SEARCH};
	}

	@Override
	public Class<? extends HttpServlet> getClazz() {
		return Am_office_assignmentServlet.class;
	}




}

