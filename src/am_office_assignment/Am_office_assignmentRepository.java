package am_office_assignment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import am_other_office_unit.Am_other_office_unitDTO;
import am_other_office_unit.Am_other_office_unitRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;

import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.UtilCharacter;
import vm_vehicle.Vm_vehicleDTO;


public class Am_office_assignmentRepository implements Repository {
	Am_office_assignmentDAO am_office_assignmentDAO = null;
	
	public void setDAO(Am_office_assignmentDAO am_office_assignmentDAO)
	{
		this.am_office_assignmentDAO = am_office_assignmentDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Am_office_assignmentRepository.class);
	Map<Long, Am_office_assignmentDTO>mapOfAm_office_assignmentDTOToiD;

  
	private Am_office_assignmentRepository(){
		am_office_assignmentDAO = Am_office_assignmentDAO.getInstance();
		am_office_assignmentDAO = null;
		mapOfAm_office_assignmentDTOToiD = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Am_office_assignmentRepository INSTANCE = new Am_office_assignmentRepository();
    }

    public static Am_office_assignmentRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Am_office_assignmentDTO> am_office_assignmentDTOs = am_office_assignmentDAO.getAllDTOs(reloadAll);

			for(Am_office_assignmentDTO am_office_assignmentDTO : am_office_assignmentDTOs) {
				Am_office_assignmentDTO oldAm_office_assignmentDTO = getAm_office_assignmentDTOByID(am_office_assignmentDTO.iD);
				if( oldAm_office_assignmentDTO != null ) {
					mapOfAm_office_assignmentDTOToiD.remove(oldAm_office_assignmentDTO.iD);
				
					
				}
				if(am_office_assignmentDTO.isDeleted == 0) 
				{
					
					mapOfAm_office_assignmentDTOToiD.put(am_office_assignmentDTO.iD, am_office_assignmentDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Am_office_assignmentDTO> getAm_office_assignmentList() {
		List <Am_office_assignmentDTO> am_office_assignments = new ArrayList<Am_office_assignmentDTO>(this.mapOfAm_office_assignmentDTOToiD.values());
		return am_office_assignments;
	}
	
	
	public Am_office_assignmentDTO getAm_office_assignmentDTOByID( long ID){
		return mapOfAm_office_assignmentDTOToiD.get(ID);
	}

	public String getText(long officeType,long officeUnitId,String language){

		if(officeType==1){
			Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitId);
			return UtilCharacter.getDataByLanguage(language,office_unitsDTO.unitNameBng,office_unitsDTO.unitNameEng);
		}
		else if(officeType==2){
			Am_other_office_unitDTO am_other_office_unitDTO  = Am_other_office_unitRepository.getInstance().getAm_other_office_unitDTOByID(officeUnitId);
			return UtilCharacter.getDataByLanguage(language,am_other_office_unitDTO.officeNameBn,am_other_office_unitDTO.officeNameEn);
		}

		else{
			return "";
		}

	}

	
	@Override
	public String getTableName() {
		return "am_office_assignment";
	}
}


