package xray_technologist_report;


import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;

import login.LoginDTO;
import pb.CommonDAO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Xray_technologist_report_Servlet")
public class Xray_technologist_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","lab_test_type","=","","int","","",SessionConstants.LAB_TEST_XRAY + "","none",  ""},		
		{"criteria","","is_done","=","AND","int","","","1","none",  ""},		
		{"criteria","","testing_date",">=","AND","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","testing_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},
		{"criteria","users","employee_record_id","=","AND","String","","","any","xray_technologist_user_name",LC.HM_NAME + "", "userNameToEmployeeRecordId"}
	};
	
	String[][] Display =
	{
		{"display","","users.employee_record_id","erIdToName",""},		
		{"display","","count(prescription_lab_test.id)","number",""},		
		{"display","","sum(plate_count_1)","int",""},		
		{"display","","sum(plate_count_2)","int",""},		
		{"display","","sum(plate_count_3)","int",""}		
	};
	
	String GroupBy = "xray_technologist_user_name";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Xray_technologist_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "prescription_lab_test join users on (users.username = xray_technologist_user_name and users.isDeleted = 0) ";

		Display[0][4] = language.equalsIgnoreCase("english")?"X-Ray Technologist":"এক্স রে টেকনোলজিস্ট";
        Display[1][4] = language.equalsIgnoreCase("english")?"X-Ray count":"এক্স রের সংখ্যা";
		Display[2][4] = CommonDAO.getName(language, "medical_equipment_name", SessionConstants.PLATE_1);
		Display[3][4] = CommonDAO.getName(language, "medical_equipment_name", SessionConstants.PLATE_2);
		Display[4][4] = CommonDAO.getName(language, "medical_equipment_name", SessionConstants.PLATE_3);

		
		String reportName = LM.getText(LC.XRAY_TECHNOLOGIST_REPORT_OTHER_XRAY_TECHNOLOGIST_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(1, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(2, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(3, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(4, ReportRequestHandler.RIGHT_ALIGN_INT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "xray_technologist_report",
				MenuConstants.XRAY_TECHNOLOGIST_REPORT_DETAILS, language, reportName, "xray_technologist_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
