package asset_type;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import brand.BrandDTO;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Asset_typeRepository implements Repository {
	Asset_typeDAO asset_typeDAO = null;
	
	public void setDAO(Asset_typeDAO asset_typeDAO)
	{
		this.asset_typeDAO = asset_typeDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Asset_typeRepository.class);
	Map<Long, Asset_typeDTO>mapOfAsset_typeDTOToiD;
	Map<String, Set<Asset_typeDTO> >mapOfAsset_typeDTOTonameEn;
	Map<String, Set<Asset_typeDTO> >mapOfAsset_typeDTOTonameBn;
	Map<String, Set<Asset_typeDTO> >mapOfAsset_typeDTOTosearchColumn;
	Map<Long, Set<Asset_typeDTO> >mapOfAsset_typeDTOToinsertedByUserId;
	Map<Long, Set<Asset_typeDTO> >mapOfAsset_typeDTOToinsertedByOrganogramId;
	Map<Long, Set<Asset_typeDTO> >mapOfAsset_typeDTOToinsertionDate;
	Map<String, Set<Asset_typeDTO> >mapOfAsset_typeDTOTolastModifierUser;
	Map<Long, Set<Asset_typeDTO> >mapOfAsset_typeDTOTolastModificationTime;


	static Asset_typeRepository instance = null;  
	private Asset_typeRepository(){
		mapOfAsset_typeDTOToiD = new ConcurrentHashMap<>();
		mapOfAsset_typeDTOTonameEn = new ConcurrentHashMap<>();
		mapOfAsset_typeDTOTonameBn = new ConcurrentHashMap<>();
		mapOfAsset_typeDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfAsset_typeDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfAsset_typeDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfAsset_typeDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfAsset_typeDTOTolastModifierUser = new ConcurrentHashMap<>();
		mapOfAsset_typeDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Asset_typeRepository getInstance(){
		if (instance == null){
			instance = new Asset_typeRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(asset_typeDAO == null)
		{
			return;
		}
		try {
			List<Asset_typeDTO> asset_typeDTOs = asset_typeDAO.getAllAsset_type(reloadAll);
			for(Asset_typeDTO asset_typeDTO : asset_typeDTOs) {
				Asset_typeDTO oldAsset_typeDTO = getAsset_typeDTOByID(asset_typeDTO.iD);
				if( oldAsset_typeDTO != null ) {
					mapOfAsset_typeDTOToiD.remove(oldAsset_typeDTO.iD);
				
					if(mapOfAsset_typeDTOTonameEn.containsKey(oldAsset_typeDTO.nameEn)) {
						mapOfAsset_typeDTOTonameEn.get(oldAsset_typeDTO.nameEn).remove(oldAsset_typeDTO);
					}
					if(mapOfAsset_typeDTOTonameEn.get(oldAsset_typeDTO.nameEn).isEmpty()) {
						mapOfAsset_typeDTOTonameEn.remove(oldAsset_typeDTO.nameEn);
					}
					
					if(mapOfAsset_typeDTOTonameBn.containsKey(oldAsset_typeDTO.nameBn)) {
						mapOfAsset_typeDTOTonameBn.get(oldAsset_typeDTO.nameBn).remove(oldAsset_typeDTO);
					}
					if(mapOfAsset_typeDTOTonameBn.get(oldAsset_typeDTO.nameBn).isEmpty()) {
						mapOfAsset_typeDTOTonameBn.remove(oldAsset_typeDTO.nameBn);
					}
					
					if(mapOfAsset_typeDTOTosearchColumn.containsKey(oldAsset_typeDTO.searchColumn)) {
						mapOfAsset_typeDTOTosearchColumn.get(oldAsset_typeDTO.searchColumn).remove(oldAsset_typeDTO);
					}
					if(mapOfAsset_typeDTOTosearchColumn.get(oldAsset_typeDTO.searchColumn).isEmpty()) {
						mapOfAsset_typeDTOTosearchColumn.remove(oldAsset_typeDTO.searchColumn);
					}
					
					if(mapOfAsset_typeDTOToinsertedByUserId.containsKey(oldAsset_typeDTO.insertedByUserId)) {
						mapOfAsset_typeDTOToinsertedByUserId.get(oldAsset_typeDTO.insertedByUserId).remove(oldAsset_typeDTO);
					}
					if(mapOfAsset_typeDTOToinsertedByUserId.get(oldAsset_typeDTO.insertedByUserId).isEmpty()) {
						mapOfAsset_typeDTOToinsertedByUserId.remove(oldAsset_typeDTO.insertedByUserId);
					}
					
					if(mapOfAsset_typeDTOToinsertedByOrganogramId.containsKey(oldAsset_typeDTO.insertedByOrganogramId)) {
						mapOfAsset_typeDTOToinsertedByOrganogramId.get(oldAsset_typeDTO.insertedByOrganogramId).remove(oldAsset_typeDTO);
					}
					if(mapOfAsset_typeDTOToinsertedByOrganogramId.get(oldAsset_typeDTO.insertedByOrganogramId).isEmpty()) {
						mapOfAsset_typeDTOToinsertedByOrganogramId.remove(oldAsset_typeDTO.insertedByOrganogramId);
					}
					
					if(mapOfAsset_typeDTOToinsertionDate.containsKey(oldAsset_typeDTO.insertionDate)) {
						mapOfAsset_typeDTOToinsertionDate.get(oldAsset_typeDTO.insertionDate).remove(oldAsset_typeDTO);
					}
					if(mapOfAsset_typeDTOToinsertionDate.get(oldAsset_typeDTO.insertionDate).isEmpty()) {
						mapOfAsset_typeDTOToinsertionDate.remove(oldAsset_typeDTO.insertionDate);
					}
					
					if(mapOfAsset_typeDTOTolastModifierUser.containsKey(oldAsset_typeDTO.lastModifierUser)) {
						mapOfAsset_typeDTOTolastModifierUser.get(oldAsset_typeDTO.lastModifierUser).remove(oldAsset_typeDTO);
					}
					if(mapOfAsset_typeDTOTolastModifierUser.get(oldAsset_typeDTO.lastModifierUser).isEmpty()) {
						mapOfAsset_typeDTOTolastModifierUser.remove(oldAsset_typeDTO.lastModifierUser);
					}
					
					if(mapOfAsset_typeDTOTolastModificationTime.containsKey(oldAsset_typeDTO.lastModificationTime)) {
						mapOfAsset_typeDTOTolastModificationTime.get(oldAsset_typeDTO.lastModificationTime).remove(oldAsset_typeDTO);
					}
					if(mapOfAsset_typeDTOTolastModificationTime.get(oldAsset_typeDTO.lastModificationTime).isEmpty()) {
						mapOfAsset_typeDTOTolastModificationTime.remove(oldAsset_typeDTO.lastModificationTime);
					}
					
					
				}
				if(asset_typeDTO.isDeleted == 0) 
				{
					
					mapOfAsset_typeDTOToiD.put(asset_typeDTO.iD, asset_typeDTO);
				
					if( ! mapOfAsset_typeDTOTonameEn.containsKey(asset_typeDTO.nameEn)) {
						mapOfAsset_typeDTOTonameEn.put(asset_typeDTO.nameEn, new HashSet<>());
					}
					mapOfAsset_typeDTOTonameEn.get(asset_typeDTO.nameEn).add(asset_typeDTO);
					
					if( ! mapOfAsset_typeDTOTonameBn.containsKey(asset_typeDTO.nameBn)) {
						mapOfAsset_typeDTOTonameBn.put(asset_typeDTO.nameBn, new HashSet<>());
					}
					mapOfAsset_typeDTOTonameBn.get(asset_typeDTO.nameBn).add(asset_typeDTO);
					
					if( ! mapOfAsset_typeDTOTosearchColumn.containsKey(asset_typeDTO.searchColumn)) {
						mapOfAsset_typeDTOTosearchColumn.put(asset_typeDTO.searchColumn, new HashSet<>());
					}
					mapOfAsset_typeDTOTosearchColumn.get(asset_typeDTO.searchColumn).add(asset_typeDTO);
					
					if( ! mapOfAsset_typeDTOToinsertedByUserId.containsKey(asset_typeDTO.insertedByUserId)) {
						mapOfAsset_typeDTOToinsertedByUserId.put(asset_typeDTO.insertedByUserId, new HashSet<>());
					}
					mapOfAsset_typeDTOToinsertedByUserId.get(asset_typeDTO.insertedByUserId).add(asset_typeDTO);
					
					if( ! mapOfAsset_typeDTOToinsertedByOrganogramId.containsKey(asset_typeDTO.insertedByOrganogramId)) {
						mapOfAsset_typeDTOToinsertedByOrganogramId.put(asset_typeDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfAsset_typeDTOToinsertedByOrganogramId.get(asset_typeDTO.insertedByOrganogramId).add(asset_typeDTO);
					
					if( ! mapOfAsset_typeDTOToinsertionDate.containsKey(asset_typeDTO.insertionDate)) {
						mapOfAsset_typeDTOToinsertionDate.put(asset_typeDTO.insertionDate, new HashSet<>());
					}
					mapOfAsset_typeDTOToinsertionDate.get(asset_typeDTO.insertionDate).add(asset_typeDTO);
					
					if( ! mapOfAsset_typeDTOTolastModifierUser.containsKey(asset_typeDTO.lastModifierUser)) {
						mapOfAsset_typeDTOTolastModifierUser.put(asset_typeDTO.lastModifierUser, new HashSet<>());
					}
					mapOfAsset_typeDTOTolastModifierUser.get(asset_typeDTO.lastModifierUser).add(asset_typeDTO);
					
					if( ! mapOfAsset_typeDTOTolastModificationTime.containsKey(asset_typeDTO.lastModificationTime)) {
						mapOfAsset_typeDTOTolastModificationTime.put(asset_typeDTO.lastModificationTime, new HashSet<>());
					}
					mapOfAsset_typeDTOTolastModificationTime.get(asset_typeDTO.lastModificationTime).add(asset_typeDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Asset_typeDTO> getAsset_typeList() {
		List <Asset_typeDTO> asset_types = new ArrayList<Asset_typeDTO>(this.mapOfAsset_typeDTOToiD.values());
		return asset_types;
	}
	
	
	public Asset_typeDTO getAsset_typeDTOByID( long ID){
		return mapOfAsset_typeDTOToiD.get(ID);
	}
	
	
	public List<Asset_typeDTO> getAsset_typeDTOByname_en(String name_en) {
		return new ArrayList<>( mapOfAsset_typeDTOTonameEn.getOrDefault(name_en,new HashSet<>()));
	}
	
	
	public List<Asset_typeDTO> getAsset_typeDTOByname_bn(String name_bn) {
		return new ArrayList<>( mapOfAsset_typeDTOTonameBn.getOrDefault(name_bn,new HashSet<>()));
	}
	
	
	public List<Asset_typeDTO> getAsset_typeDTOBysearch_column(String search_column) {
		return new ArrayList<>( mapOfAsset_typeDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
	}
	
	
	public List<Asset_typeDTO> getAsset_typeDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfAsset_typeDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Asset_typeDTO> getAsset_typeDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfAsset_typeDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Asset_typeDTO> getAsset_typeDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfAsset_typeDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Asset_typeDTO> getAsset_typeDTOBylast_modifier_user(String last_modifier_user) {
		return new ArrayList<>( mapOfAsset_typeDTOTolastModifierUser.getOrDefault(last_modifier_user,new HashSet<>()));
	}
	
	
	public List<Asset_typeDTO> getAsset_typeDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfAsset_typeDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "asset_type";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}

	public String getAssetTypeText(long ID, boolean isLanguageEnglish) {
		Asset_typeDTO dto = mapOfAsset_typeDTOToiD.get(ID);

		return dto==null ? "" : isLanguageEnglish ? dto.nameEn : dto.nameBn;
	}
}


