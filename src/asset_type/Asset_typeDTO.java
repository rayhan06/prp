package asset_type;
import java.util.*; 
import util.*; 


public class Asset_typeDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
    
    public static final long HARDWARE = 0;
	public static final long ACCESSORY = 100;
	public static final long SOFTWARE = 101;
	public static final long DATA_CENTRE = 102;
	public static final long OTHERS = 1300;
	
	
    @Override
	public String toString() {
            return "$Asset_typeDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}