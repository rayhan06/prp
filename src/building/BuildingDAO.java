package building;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class BuildingDAO  implements CommonDAOService<BuildingDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private BuildingDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"minimum_level",
			"maximum_level",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("name_en"," and (name_en like ?)");
		searchMap.put("name_bn"," and (name_bn like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final BuildingDAO INSTANCE = new BuildingDAO();
	}

	public static BuildingDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(BuildingDTO buildingDTO)
	{
		buildingDTO.searchColumn = "";
		buildingDTO.searchColumn += buildingDTO.nameEn + " ";
		buildingDTO.searchColumn += buildingDTO.nameBn + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, BuildingDTO buildingDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(buildingDTO);
		if(isInsert)
		{
			ps.setObject(++index,buildingDTO.iD);
		}
		ps.setObject(++index,buildingDTO.nameEn);
		ps.setObject(++index,buildingDTO.nameBn);
		ps.setObject(++index,buildingDTO.minimumLevel);
		ps.setObject(++index,buildingDTO.maximumLevel);
		ps.setObject(++index,buildingDTO.insertedByUserId);
		ps.setObject(++index,buildingDTO.insertedByOrganogramId);
		ps.setObject(++index,buildingDTO.insertionDate);
		ps.setObject(++index,buildingDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(++index,buildingDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,buildingDTO.iD);
		}
	}
	
	@Override
	public BuildingDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			BuildingDTO buildingDTO = new BuildingDTO();
			int i = 0;
			buildingDTO.iD = rs.getLong(columnNames[i++]);
			buildingDTO.nameEn = rs.getString(columnNames[i++]);
			buildingDTO.nameBn = rs.getString(columnNames[i++]);
			buildingDTO.minimumLevel = rs.getInt(columnNames[i++]);
			buildingDTO.maximumLevel = rs.getInt(columnNames[i++]);
			buildingDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			buildingDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			buildingDTO.insertionDate = rs.getLong(columnNames[i++]);
			buildingDTO.lastModifierUser = rs.getString(columnNames[i++]);
			buildingDTO.isDeleted = rs.getInt(columnNames[i++]);
			buildingDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return buildingDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public BuildingDTO getDTOByID (long id)
	{
		BuildingDTO buildingDTO = null;
		try 
		{
			buildingDTO = getDTOFromID(id);
			if(buildingDTO != null)
			{
				BuildingBlockDAO buildingBlockDAO = BuildingBlockDAO.getInstance();				
				List<BuildingBlockDTO> buildingBlockDTOList = (List<BuildingBlockDTO>)buildingBlockDAO.getDTOsByParent("building_id", buildingDTO.iD);
				buildingDTO.buildingBlockDTOList = buildingBlockDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return buildingDTO;
	}

	@Override
	public String getTableName() {
		return "building";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((BuildingDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((BuildingDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
				
}
	