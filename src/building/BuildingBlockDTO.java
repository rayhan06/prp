package building;
import java.util.*; 
import util.*; 


public class BuildingBlockDTO extends CommonDTO
{

	public long buildingId = -1;
    public String nameEn = "";
    public String nameBn = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	public int ordering = 1;
	
	public List<BuildingBlockDTO> buildingBlockDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$BuildingBlockDTO[" +
            " iD = " + iD +
            " buildingId = " + buildingId +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}