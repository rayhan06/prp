package building;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;
import repository.RepositoryManager;

public class BuildingBlockDAO  implements CommonDAOService<BuildingBlockDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private BuildingBlockDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"building_id",
			"name_en",
			"name_bn",
			"ordering",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("name_en"," and (name_en like ?)");
		searchMap.put("name_bn"," and (name_bn like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final BuildingBlockDAO INSTANCE = new BuildingBlockDAO();
	}

	public static BuildingBlockDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(BuildingBlockDTO buildingblockDTO)
	{
		buildingblockDTO.searchColumn = "";
		buildingblockDTO.searchColumn += buildingblockDTO.nameEn + " ";
		buildingblockDTO.searchColumn += buildingblockDTO.nameBn + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, BuildingBlockDTO buildingblockDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(buildingblockDTO);
		if(isInsert)
		{
			ps.setObject(++index,buildingblockDTO.iD);
		}
		ps.setObject(++index,buildingblockDTO.buildingId);
		ps.setObject(++index,buildingblockDTO.nameEn);
		ps.setObject(++index,buildingblockDTO.nameBn);
		ps.setObject(++index,buildingblockDTO.ordering);
		ps.setObject(++index,buildingblockDTO.insertedByUserId);
		ps.setObject(++index,buildingblockDTO.insertedByOrganogramId);
		ps.setObject(++index,buildingblockDTO.insertionDate);
		ps.setObject(++index,buildingblockDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(++index,buildingblockDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,buildingblockDTO.iD);
		}
	}
	
	@Override
	public BuildingBlockDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			BuildingBlockDTO buildingblockDTO = new BuildingBlockDTO();
			int i = 0;
			buildingblockDTO.iD = rs.getLong(columnNames[i++]);
			buildingblockDTO.buildingId = rs.getLong(columnNames[i++]);
			buildingblockDTO.nameEn = rs.getString(columnNames[i++]);
			buildingblockDTO.nameBn = rs.getString(columnNames[i++]);
			buildingblockDTO.ordering = rs.getInt(columnNames[i++]);
			buildingblockDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			buildingblockDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			buildingblockDTO.insertionDate = rs.getLong(columnNames[i++]);
			buildingblockDTO.lastModifierUser = rs.getString(columnNames[i++]);
			buildingblockDTO.isDeleted = rs.getInt(columnNames[i++]);
			buildingblockDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return buildingblockDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public BuildingBlockDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "building_block";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((BuildingBlockDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((BuildingBlockDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
	
	@Override
	public List<BuildingBlockDTO> getAllDTOs(boolean isFirstReload) {
        String sql;
        if (isFirstReload) {
            sql = String.format("SELECT * FROM %s order by ordering asc", getTableName());
        } else {
            sql = String.format("SELECT * FROM %s WHERE lastModificationTime >= %d order by ordering asc", getTableName(), RepositoryManager.lastModifyTime);
        }
        return getDTOs(sql);
    }
				
}
	