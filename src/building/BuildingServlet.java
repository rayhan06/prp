package building;


import java.text.SimpleDateFormat;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import permission.MenuConstants;

import user.UserDTO;
import util.*;
import javax.servlet.http.*;
import java.util.*;

import common.BaseServlet;
import com.google.gson.Gson;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class BuildingServlet
 */
@WebServlet("/BuildingServlet")
@MultipartConfig
public class BuildingServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(BuildingServlet.class);

    @Override
    public String getTableName() {
        return BuildingDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "BuildingServlet";
    }

    @Override
    public BuildingDAO getCommonDAOService() {
        return BuildingDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.BUILDING_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.BUILDING_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.BUILDING_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return BuildingServlet.class;
    }
	BuildingBlockDAO buildingBlockDAO = BuildingBlockDAO.getInstance();
    private final Gson gson = new Gson();
 	

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addBuilding");
		String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		BuildingDTO buildingDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
		if(addFlag)
		{
			buildingDTO = new BuildingDTO();
		}
		else
		{
			buildingDTO = BuildingDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		Value = request.getParameter("nameEn");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("nameEn = " + Value);
		if(Value != null)
		{
			buildingDTO.nameEn = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("nameBn");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("nameBn = " + Value);
		if(Value != null)
		{
			buildingDTO.nameBn = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("minimumLevel");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("minimumLevel = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			buildingDTO.minimumLevel = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("maximumLevel");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("maximumLevel = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			buildingDTO.maximumLevel = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		if(addFlag)
		{
			buildingDTO.insertedByUserId = userDTO.ID;
		}


		if(addFlag)
		{
			buildingDTO.insertedByOrganogramId = userDTO.organogramID;
		}


		if(addFlag)
		{				
			buildingDTO.insertionDate = TimeConverter.getToday();
		}			


		buildingDTO.lastModifierUser = userDTO.userName;

		System.out.println("Done adding  addBuilding dto = " + buildingDTO);

		if(addFlag == true)
		{
			BuildingDAO.getInstance().add(buildingDTO);
		}
		else
		{				
			BuildingDAO.getInstance().update(buildingDTO);										
		}
		
		List<BuildingBlockDTO> buildingBlockDTOList = createBuildingBlockDTOListByRequest(request, language, buildingDTO, userDTO);
		

		if(addFlag == true) //add or validate
		{
			if(buildingBlockDTOList != null)
			{				
				for(BuildingBlockDTO buildingBlockDTO: buildingBlockDTOList)
				{
					buildingBlockDAO.add(buildingBlockDTO);
				}
			}
		
		}			
		else
		{
			List<Long> childIdsFromRequest = buildingBlockDAO.getChildIdsFromRequest(request, "buildingBlock");
			//delete the removed children
			buildingBlockDAO.deleteChildrenNotInList("building", "building_block", buildingDTO.iD, childIdsFromRequest);
			List<Long> childIDsInDatabase = buildingBlockDAO.getChilIds("building", "building_block", buildingDTO.iD);
			
			
			if(childIdsFromRequest != null)
			{
				for(int i = 0; i < childIdsFromRequest.size(); i ++)
				{
					Long childIDFromRequest = childIdsFromRequest.get(i);
					if(childIDsInDatabase.contains(childIDFromRequest))
					{
						BuildingBlockDTO buildingBlockDTO =  createBuildingBlockDTOByRequestAndIndex(request, false, i, language, buildingDTO, userDTO);
						buildingBlockDAO.update(buildingBlockDTO);
					}
					else
					{
						BuildingBlockDTO buildingBlockDTO =  createBuildingBlockDTOByRequestAndIndex(request, true, i, language, buildingDTO, userDTO);
						buildingBlockDAO.add(buildingBlockDTO);
					}
				}
			}
			else
			{
				buildingBlockDAO.deleteChildrenByParent(buildingDTO.iD, "building_id");
			}
			
		}					
		return buildingDTO;

	}
	private List<BuildingBlockDTO> createBuildingBlockDTOListByRequest(HttpServletRequest request, String language, BuildingDTO buildingDTO, UserDTO userDTO) throws Exception{ 
		List<BuildingBlockDTO> buildingBlockDTOList = new ArrayList<BuildingBlockDTO>();
		if(request.getParameterValues("buildingBlock.iD") != null) 
		{
			int buildingBlockItemNo = request.getParameterValues("buildingBlock.iD").length;
			
			
			for(int index=0;index<buildingBlockItemNo;index++){
				BuildingBlockDTO buildingBlockDTO = createBuildingBlockDTOByRequestAndIndex(request,true,index, language, buildingDTO, userDTO);
				buildingBlockDTOList.add(buildingBlockDTO);
			}
			
			return buildingBlockDTOList;
		}
		return null;
	}
	
	private BuildingBlockDTO createBuildingBlockDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, String language, BuildingDTO buildingDTO, UserDTO userDTO) throws Exception{
	
		BuildingBlockDTO buildingBlockDTO;
		if(addFlag == true )
		{
			buildingBlockDTO = new BuildingBlockDTO();
		}
		else
		{
			buildingBlockDTO = buildingBlockDAO.getDTOByID(Long.parseLong(request.getParameterValues("buildingBlock.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		buildingBlockDTO.buildingId = buildingDTO.iD; 

		String Value = "";

		Value = request.getParameterValues("buildingBlock.nameEn")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("nameEn = " + Value);
		if(Value != null)
		{
			buildingBlockDTO.nameEn = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameterValues("buildingBlock.nameBn")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("nameBn = " + Value);
		if(Value != null)
		{
			buildingBlockDTO.nameBn = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		Value = request.getParameterValues("buildingBlock.ordering")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("ordering = " + Value);
		if(Value != null)
		{
			buildingBlockDTO.ordering = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		if(addFlag)
		{
			buildingBlockDTO.insertedByUserId = userDTO.ID;
		}


		if(addFlag)
		{
			buildingBlockDTO.insertedByOrganogramId = userDTO.organogramID;
		}


		if(addFlag)
		{				
			buildingBlockDTO.insertionDate = TimeConverter.getToday();
		}			


		buildingBlockDTO.lastModifierUser = userDTO.userName;

		return buildingBlockDTO;
	
	}	
}

