package building;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class BuildingBlockRepository implements Repository {
	BuildingBlockDAO buildingblockDAO = null;
	
	static Logger logger = Logger.getLogger(BuildingBlockRepository.class);
	Map<Long, BuildingBlockDTO>mapOfBuildingBlockDTOToiD;
	Map<Long, Set<BuildingBlockDTO> >mapOfBuildingBlockDTOTobuildingId;
	Gson gson;

  
	private BuildingBlockRepository(){
		buildingblockDAO = BuildingBlockDAO.getInstance();
		mapOfBuildingBlockDTOToiD = new ConcurrentHashMap<>();
		mapOfBuildingBlockDTOTobuildingId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static BuildingBlockRepository INSTANCE = new BuildingBlockRepository();
    }

    public static BuildingBlockRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<BuildingBlockDTO> buildingblockDTOs = buildingblockDAO.getAllDTOs(reloadAll);
			for(BuildingBlockDTO buildingblockDTO : buildingblockDTOs) {
				BuildingBlockDTO oldBuildingBlockDTO = getBuildingBlockDTOByiD(buildingblockDTO.iD);
				if( oldBuildingBlockDTO != null ) {
					mapOfBuildingBlockDTOToiD.remove(oldBuildingBlockDTO.iD);
				
					if(mapOfBuildingBlockDTOTobuildingId.containsKey(oldBuildingBlockDTO.buildingId)) {
						mapOfBuildingBlockDTOTobuildingId.get(oldBuildingBlockDTO.buildingId).remove(oldBuildingBlockDTO);
					}
					if(mapOfBuildingBlockDTOTobuildingId.get(oldBuildingBlockDTO.buildingId).isEmpty()) {
						mapOfBuildingBlockDTOTobuildingId.remove(oldBuildingBlockDTO.buildingId);
					}
					
					
				}
				if(buildingblockDTO.isDeleted == 0) 
				{
					
					mapOfBuildingBlockDTOToiD.put(buildingblockDTO.iD, buildingblockDTO);
				
					if( ! mapOfBuildingBlockDTOTobuildingId.containsKey(buildingblockDTO.buildingId)) {
						mapOfBuildingBlockDTOTobuildingId.put(buildingblockDTO.buildingId, new HashSet<>());
					}
					mapOfBuildingBlockDTOTobuildingId.get(buildingblockDTO.buildingId).add(buildingblockDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public BuildingBlockDTO clone(BuildingBlockDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, BuildingBlockDTO.class);
	}
	
	
	public List<BuildingBlockDTO> getBuildingBlockList() {
		List <BuildingBlockDTO> buildingblocks = new ArrayList<BuildingBlockDTO>(this.mapOfBuildingBlockDTOToiD.values());
		return buildingblocks;
	}
	
	public List<BuildingBlockDTO> copyBuildingBlockList() {
		List <BuildingBlockDTO> buildingblocks = getBuildingBlockList();
		return buildingblocks
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public BuildingBlockDTO getBuildingBlockDTOByiD( long iD){
		return mapOfBuildingBlockDTOToiD.get(iD);
	}
	
	public BuildingBlockDTO copyBuildingBlockDTOByiD( long iD){
		return clone(mapOfBuildingBlockDTOToiD.get(iD));
	}
	
	
	public List<BuildingBlockDTO> getBuildingBlockDTOBybuildingId(long buildingId) {
		List<BuildingBlockDTO> blocks =  new ArrayList<>( mapOfBuildingBlockDTOTobuildingId.getOrDefault(buildingId,new HashSet<>()));
		blocks.sort(Comparator.comparing(e -> e.ordering));
		return blocks;
	}
	
	public List<BuildingBlockDTO> copyBuildingBlockDTOBybuildingId(long buildingId)
	{
		List <BuildingBlockDTO> buildingblocks = getBuildingBlockDTOBybuildingId(buildingId);
		return buildingblocks
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return buildingblockDAO.getTableName();
	}
}


