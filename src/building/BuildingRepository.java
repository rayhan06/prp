package building;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class BuildingRepository implements Repository {
	BuildingDAO buildingDAO = null;
	
	static Logger logger = Logger.getLogger(BuildingRepository.class);
	Map<Long, BuildingDTO>mapOfBuildingDTOToiD;
	Gson gson;

  
	private BuildingRepository(){
		buildingDAO = BuildingDAO.getInstance();
		mapOfBuildingDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static BuildingRepository INSTANCE = new BuildingRepository();
    }

    public static BuildingRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<BuildingDTO> buildingDTOs = buildingDAO.getAllDTOs(reloadAll);
			for(BuildingDTO buildingDTO : buildingDTOs) {
				BuildingDTO oldBuildingDTO = getBuildingDTOByiD(buildingDTO.iD);
				if( oldBuildingDTO != null ) {
					mapOfBuildingDTOToiD.remove(oldBuildingDTO.iD);
				
					
				}
				if(buildingDTO.isDeleted == 0) 
				{
					
					mapOfBuildingDTOToiD.put(buildingDTO.iD, buildingDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public BuildingDTO clone(BuildingDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, BuildingDTO.class);
	}
	
	
	public List<BuildingDTO> getBuildingList() {
		List <BuildingDTO> buildings = new ArrayList<BuildingDTO>(this.mapOfBuildingDTOToiD.values());
		return buildings;
	}
	
	public List<BuildingDTO> copyBuildingList() {
		List <BuildingDTO> buildings = getBuildingList();
		return buildings
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public BuildingDTO getBuildingDTOByiD( long iD){
		return mapOfBuildingDTOToiD.get(iD);
	}
	
	public BuildingDTO copyBuildingDTOByiD( long iD){
		return clone(mapOfBuildingDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return buildingDAO.getTableName();
	}
}


