package building;
import java.util.*; 
import util.*; 


public class BuildingDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
	public int minimumLevel = 1;
	public int maximumLevel = 1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	
	public List<BuildingBlockDTO> buildingBlockDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$BuildingDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " minimumLevel = " + minimumLevel +
            " maximumLevel = " + maximumLevel +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}