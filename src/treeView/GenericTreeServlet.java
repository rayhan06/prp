package treeView;

import java.io.IOException;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import employee_records.Employee_recordsDAO;
import language.LC;
import language.LM;
import org.apache.log4j.Logger;

import centre.Constants;
import inbox.InboxServlet;
import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import pb.*;
import util.CommonConstant;
import util.RecordNavigationManager2;

/**
 * Servlet implementation class TreeServlet
 */
@WebServlet("/GenericTreeServlet")
public class GenericTreeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public static Logger logger = Logger.getLogger(GenericTreeServlet.class);

	public String actionType = "";
	public String servletType = "GenericTreeServlet";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GenericTreeServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		try
		{
			actionType = request.getParameter("actionType");
			TreeDTO treeDTO = new TreeDTO();
			//System.out.println("############################# In doget2 actionType = " + actionType);
			if(actionType.equals("getOffice"))
			{

				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.MINISTRIES_TO_OFFICES_INCLUDING_ORIGINS);
				treeDTO.pageTitle = "Office Management";
				treeDTO.treeName = "OfficeDropDownChain";
				treeDTO.treeType = "select";
				treeDTO.treeBody = "officeOriginTreeBody.jsp";
				treeDTO.checkBoxType = 0;
				treeDTO.plusButtonType = 0;
			}

			else if(actionType.equals("getOriginUnit"))
			{
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.ORIGIN_UNITS_TO_ORIGIN_ORGANOGRAMS);
				treeDTO.pageTitle = "Office Units";
				treeDTO.treeName = "OfficeOriginUnitsTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "officeOriginTreeBody.jsp";
				treeDTO.checkBoxType = 2;
				treeDTO.plusButtonType = 0;
				treeDTO.drawExtraLayer = true;
			}

			else if(actionType.equals("getUnit"))
			{
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.UNITS_TO_ORGANOGRAMS);
				treeDTO.pageTitle = "Office Units";
				treeDTO.treeName = "OfficeUnitsTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "officeOriginTreeBody.jsp";
				treeDTO.checkBoxType = 2;
				treeDTO.plusButtonType = 0;
				treeDTO.drawExtraLayer = true;
			}
			else if(actionType.equals("getApprovalPath"))
			{
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.MINISTRIES_TO_OFFICES_INCLUDING_ORIGINS);
				treeDTO.pageTitle = "Approval Path";
				treeDTO.treeName = "OfficeDropDownChain";
				treeDTO.treeType = "select";
				treeDTO.treeBody = "approvalPathTreeBody.jsp";
				treeDTO.checkBoxType = 0;
				treeDTO.additionalParams = "&pathType=1";
				treeDTO.plusButtonType = 0;
			}
			else if(actionType.equals("getApprovalPathTemplate"))
			{
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.MINISTRIES_TO_ORIGINS);
				treeDTO.pageTitle = "Approval Path Template";
				treeDTO.treeName = "OfficeDropDownChain";
				treeDTO.treeType = "select";
				treeDTO.treeBody = "approvalPathTreeBody.jsp";
				treeDTO.checkBoxType = 0;
				treeDTO.plusButtonType = 0;
				treeDTO.additionalParams = "&pathType=2";
			}
			else if(actionType.equals("assignPathToOrganogram"))
			{
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.MINISTRIES_TO_ORGANOGRAMS_EXCLUDING_ORIGINS);
				treeDTO.pageTitle = "Assign Approval Path";
				treeDTO.treeName = "approvalPathTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "OrganogramPathTreeBody.jsp";
				treeDTO.checkBoxType = 1;
				treeDTO.plusButtonType = 0;
			}
			else if(actionType.equals("assignPathToOffices"))
			{
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.MINISTRIES_TO_OFFICES_EXCLUDING_ORIGINS);
				treeDTO.pageTitle = "Assign Approval Path";
				treeDTO.treeName = "approvalPathTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "OrganogramPathTreeBody.jsp";
				treeDTO.checkBoxType = 1;
				treeDTO.plusButtonType = 0;
			}
			else if(actionType.equals("assignPathToMinistries"))
			{
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.MINISTRIES);
				treeDTO.pageTitle = "Assign Approval Path";
				treeDTO.treeName = "approvalPathTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "OrganogramPathTreeBody.jsp";
				treeDTO.checkBoxType = 1;
				treeDTO.plusButtonType = 0;
			}
			else if(actionType.equals("getUnitWithCheckBox"))
			{
				//System.out.println("in getUnitWithCheckBox" );
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.UNITS_TO_ORGANOGRAMS);
				treeDTO.pageTitle = "Office Units";
				treeDTO.treeName = "OfficeUnitsTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "approvalPathTreeBody.jsp";
				treeDTO.checkBoxType = 1;
				treeDTO.additionalParams = "&pathType=1";
				treeDTO.plusButtonType = 0;
			}
			else if(actionType.equals("getUnitOriginWithCheckBox")) //used in approval path
			{
				//System.out.println("in getUnitOriginWithCheckBox" );
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.UNITS_TO_ORGANOGRAMS);
				treeDTO.pageTitle = "Office Units";
				treeDTO.treeName = "OfficeUnitsTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "approvalPathTreeBody.jsp";
				treeDTO.checkBoxType = 5;
				treeDTO.additionalParams = "&pathType=2";
				treeDTO.plusButtonType = 0;
				treeDTO.drawExtraLayer = true;
			}
			else if(actionType.equals("getUnitOriginWithCheckBox2")) //used in approval path
			{
				//System.out.println("in getUnitOriginWithCheckBox 2" );
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.UNITS_TO_ORGANOGRAMS);
				treeDTO.pageTitle = "Office Units";
				treeDTO.treeName = "OfficeUnitsTree2";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "treeNode.jsp";
				treeDTO.checkBoxType = 4;
				treeDTO.additionalParams = "&pathType=2";
				treeDTO.plusButtonType = 0;
				treeDTO.drawExtraLayer = true;
			}
			else if(actionType.equals("getRole"))
			{
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.MINISTRIES_TO_OFFICES_INCLUDING_ORIGINS);
				treeDTO.pageTitle = "Role Management";
				treeDTO.treeName = "OfficeDropDownChain";
				treeDTO.treeType = "select";
				treeDTO.treeBody = "roleTreeBody.jsp";
				treeDTO.checkBoxType = 0;
				treeDTO.plusButtonType = 0;
			}
			else if(actionType.equals("getFlatUnits"))
			{
				//System.out.println("in getFlatUnits" );
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.UNITS_TO_ORGANOGRAMS);
				treeDTO.pageTitle = "Office Units";
				treeDTO.treeName = "OfficeUnitsTree";
				treeDTO.treeType = "flat";
				treeDTO.treeBody = "roleTreeBody.jsp";
				treeDTO.checkBoxType = 1;
				treeDTO.plusButtonType = 0;

			}
			else if(actionType.equals("getOfficeRole"))
			{
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.MINISTRIES_TO_ORGANOGRAMS_EXCLUDING_ORIGINS);
				treeDTO.pageTitle = "Role Management";
				treeDTO.treeName = "OfficeDropDownChain";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "officeRoleTreeBody.jsp";
				treeDTO.checkBoxType = 2;
				treeDTO.plusButtonType = 0;
			}
			else if(actionType.equals("assignApprovalPathTemplate"))
			{
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.UNITS_TO_ORGANOGRAMS);
				treeDTO.pageTitle = "Office Units";
				treeDTO.treeName = "OfficeUnitsTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "approvalAssignmentTreeBody.jsp";
				treeDTO.checkBoxType = 4;
				treeDTO.additionalParams = "&pathType=2";
				treeDTO.plusButtonType = 0;
				treeDTO.drawExtraLayer = true;
			}
			else if (actionType.equals("assignPathToOrganogram")) {
				treeDTO.parentChildMap = new String[]{"office_ministries", "office_layers", "offices", "office_units", "office_unit_organograms"};
				treeDTO.showNameMap = new String[]{"Office Ministries", "Office Layers", "Offices", "Office Units", "Office Unit Organograms"};
				treeDTO.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_id", "office_unit_id"};
				treeDTO.englishNameColumn = new String[]{"name_eng", "layer_name_eng", "office_name_eng", "unit_name_eng", "designation_eng"};
				treeDTO.banglaNameColumn = new String[]{"name_bng", "layer_name_bng", "office_name_bng", "unit_name_bng", "designation_bng"};
				treeDTO.pageTitle = "Assign Approval Path";
				treeDTO.treeName = "approvalPathTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "OrganogramPathTreeBody.jsp";
				treeDTO.sameTableParentColumnName = new String[]{"", "", "", "parent_unit_id", ""}; //only for recursive tables
				treeDTO.checkBoxType = 1;
				treeDTO.plusButtonType = 0;
			}
			else if (actionType.equals("searchEmployeeRecord")) {
				treeDTO.parentChildMap = new String[]{"office_ministries", "office_layers", "office_origins", "offices"};
				treeDTO.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_origin_id"};
				treeDTO.englishNameColumn = new String[]{"name_eng", "layer_name_eng", "office_name_eng", "office_name_eng"};
				treeDTO.banglaNameColumn = new String[]{"name_bng", "layer_name_bng", "office_name_bng", "office_name_bng"};
				treeDTO.pageTitle = "Office Origin Units";
				treeDTO.treeName = "OfficeOriginUnitsTree";
				treeDTO.treeType = "select";
				treeDTO.treeBody = "employee_record/employeeRecordSearch.jsp";
				treeDTO.checkBoxType = 0;
				//treeDTO.sameTableParentColumnName = new String[] {"parent_unit_id", ""}; //only for recursive tables

				treeDTO.showNameMap = new String[]{LM.getText(LC.OFFICE_HEAD_MINISTRY, loginDTO),
						LM.getText(LC.OFFICE_HEAD_LAYER, loginDTO),
						LM.getText(LC.OFFICE_HEAD_ORIGIN, loginDTO),
						LM.getText(LC.OFFICE_HEAD_OFFICE, loginDTO)};

				RecordNavigationManager2 rnManager = new RecordNavigationManager2(
						SessionConstants.NAV_EMPLOYEE_RECORDS,
						request,
						new Employee_recordsDAO(),
						SessionConstants.VIEW_EMPLOYEE_RECORDS,
						SessionConstants.SEARCH_EMPLOYEE_RECORDS,
						"employee_records");
				try {
					//System.out.println("trying to dojob");
					rnManager.doJob(loginDTO);
				} catch (Exception e) {
					//System.out.println("failed to dojob" + e);
				}

			}else if (actionType.equals("addEmployeeOfficeManagement")) {
				treeDTO.parentChildMap = new String[]{"office_ministries", "office_layers", "office_origins", "offices"};
				treeDTO.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_origin_id"};
				treeDTO.englishNameColumn = new String[]{"name_eng", "layer_name_eng", "office_name_eng", "office_name_eng"};
				treeDTO.banglaNameColumn = new String[]{"name_bng", "layer_name_bng", "office_name_bng", "office_name_bng"};
				treeDTO.pageTitle = "Office Origin Units";
				treeDTO.treeName = "OfficeOriginUnitsTree";
				treeDTO.treeType = "select";
				treeDTO.treeBody = "employeeManagement/officeAndEmployeeManagement.jsp";
				treeDTO.checkBoxType = 0;
				//sameTableParentColumnName = new String[] {"parent_unit_id", ""}; //only for recursive tables

				treeDTO.showNameMap = new String[]{LM.getText(LC.OFFICE_ADMIN_MINISTRY, loginDTO),
						LM.getText(LC.OFFICE_ADMIN_LAYER, loginDTO),
						LM.getText(LC.OFFICE_ADMIN_ORIGIN, loginDTO),
						LM.getText(LC.OFFICE_ADMIN_OFFICE, loginDTO)};
			}else if (actionType.equals("addOriginUnitOrganograms")) {
				treeDTO.parentChildMap = new String[]{"office_ministries", "office_layers", "office_origins", "office_origin_units"};
				treeDTO.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_origin_id"};
				treeDTO.englishNameColumn = new String[]{"name_eng", "layer_name_eng", "office_name_eng", "unit_name_eng"};
				treeDTO.banglaNameColumn = new String[]{"name_bng", "layer_name_bng", "office_name_bng", "unit_name_bng"};
				treeDTO.pageTitle = "Office Origin Units";
				treeDTO.treeName = "OfficeOriginUnitsTree";
				treeDTO.treeType = "select";
				treeDTO.treeBody = "office/OriginUnitOrganogramsEdms.jsp";
				treeDTO.checkBoxType = 0;
				//sameTableParentColumnName = new String[] {"parent_unit_id", ""}; //only for recursive tables

				treeDTO.showNameMap = new String[]{LM.getText(LC.OFFICE_ADMIN_MINISTRY, loginDTO),
						LM.getText(LC.OFFICE_ADMIN_LAYER, loginDTO),
						LM.getText(LC.OFFICE_ADMIN_ORIGIN, loginDTO),
						LM.getText(LC.OFFICE_ADMIN_OFFICE, loginDTO)};
			} else if (actionType.equals("getOfficeStuffAndOfficeDoptorSection")) {
				treeDTO.parentChildMap = new String[]{"office_origin_units", "office_origin_unit_organograms"};
				treeDTO.showNameMap = new String[]{"Doptor/Section", "Office Stuffs"};
				treeDTO.parentIDColumn = new String[]{"office_origin_id", "office_origin_unit_id"};
				treeDTO.englishNameColumn = new String[]{"unit_name_eng", "designation_eng"};
				treeDTO.banglaNameColumn = new String[]{"unit_name_bng", "designation_bng"};
				treeDTO.pageTitle = "Office Origin Unit";
				treeDTO.treeName = "OfficeOriginUnitTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "OriginUnitOrganograms.jsp";
				treeDTO.checkBoxType = 0;
				treeDTO.sameTableParentColumnName = new String[]{"parent_unit_id", ""}; //only for recursive tables
				treeDTO.plusButtonType = 1;
				treeDTO.drawExtraLayer = true;
				treeDTO.hasExtraLayerPlusButton = new boolean[]{false, true};
			} else if (actionType.equals("getOfficeUnitOrganogramManagement")) {
				treeDTO.parentChildMap = new String[]{"office_ministries", "office_layers", "office_origins", "offices"};
				treeDTO.showNameMap = new String[]{"Office Ministries", "Office Layers", "Office Origins", "Offices"};
				treeDTO.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_origin_id"};
				treeDTO.englishNameColumn = new String[]{"name_eng", "layer_name_eng", "office_name_eng", "office_name_eng"};
				treeDTO.banglaNameColumn = new String[]{"name_bng", "layer_name_bng", "office_name_bng", "office_name_bng"};
				treeDTO.pageTitle = "Office Unit Ogranogram Management";
				treeDTO.treeName = "OfficeUnitOgranogramManagement";
				treeDTO.treeType = "select";
				treeDTO.treeBody = "office/OfficeUnitOrganogramManagement.jsp";
				treeDTO.checkBoxType = 2;
				treeDTO.plusButtonType = 0;

				treeDTO.showNameMap = new String[]{LM.getText(LC.OFFICE_ADMIN_MINISTRY, loginDTO),
						LM.getText(LC.OFFICE_ADMIN_LAYER, loginDTO),
						LM.getText(LC.OFFICE_ADMIN_ORIGIN, loginDTO),
						LM.getText(LC.OFFICE_ADMIN_OFFICE, loginDTO)};
				request.setAttribute("customMenu", LM.getLanguageIDByUserDTO(userDTO) == 1 ? "Office unit organogram" : "অফিস শাখা কাঠামো");
			} else if (actionType.equals("getUnitForOfficeUnitOrganogramManagement")) {
				treeDTO.parentChildMap = new String[]{"office_units", "office_unit_organograms"};
				treeDTO.showNameMap = new String[]{"Office Units", "Office Unit Organograms"};
				treeDTO.parentIDColumn = new String[]{"id", "office_unit_id"};
				treeDTO.englishNameColumn = new String[]{"unit_name_eng", "designation_eng"};
				treeDTO.banglaNameColumn = new String[]{"unit_name_bng", "designation_bng"};
				treeDTO.pageTitle = "Office Units";
				treeDTO.treeName = "OfficeUnitOrganogramsTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "office/OfficeUnitOrganogramManagement.jsp";
				treeDTO.checkBoxType = 2;
				treeDTO.sameTableParentColumnName = null; //only for recursive tables
				treeDTO.plusButtonType = 0;
			} else if (actionType.equals("getOriginUnitForOfficeUnitOrganogramManagement")) {
				treeDTO.parentChildMap = new String[]{"office_origin_units", "office_origin_unit_organograms"};
				treeDTO.showNameMap = new String[]{"Office Origin Units", "Office Origin Unit Organograms"};
				treeDTO.parentIDColumn = new String[]{"id", "office_origin_unit_id"};
				treeDTO.englishNameColumn = new String[]{"unit_name_eng", "designation_eng"};
				treeDTO.banglaNameColumn = new String[]{"unit_name_bng", "designation_bng"};
				treeDTO.pageTitle = "Office Units";
				treeDTO.treeName = "OfficeOriginUnitOrganogramsTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "office/OfficeUnitOrganogramManagement.jsp";
				treeDTO.checkBoxType = 2;
				treeDTO.sameTableParentColumnName = null; //only for recursive tables
				treeDTO.plusButtonType = 0;
			}




			//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_AND_EMPLOYEE_MANAGEMENT))
			{
				String myLayer = request.getParameter("myLayer");
				if(myLayer != null && !myLayer.equalsIgnoreCase(""))
				{
					int layer = Integer.parseInt(request.getParameter("myLayer"));

					getNormalNodes(request, response, layer, treeDTO);

				}
				else
				{
					getTopNodes(request, response, treeDTO);
				}
			}
		
		}
		catch(Exception ex)
		{
			ex.printStackTrace();

			logger.debug("a1 : "+actionType + " : " + ex);
		}
	}

	private void setAttributes(int parentID, int layer, List<Integer> IDs, HttpServletRequest request, TreeDTO treeDTO)
	{

		request.setAttribute("nodeIDs", IDs);
		request.setAttribute("myLayer", layer);
		String request_layer_id_pairs = request.getParameter("layer_id_pairs");
		if(request_layer_id_pairs == null)
		{
			request_layer_id_pairs = "";
		}
		//System.out.println("layer_id_pairs = " + request_layer_id_pairs);
		treeDTO.layer_id_pairs = request_layer_id_pairs;
		request.setAttribute("treeDTO", treeDTO);

	}

	private void setParametersAndForward(int parentID, String nodeJSP, HttpServletRequest request, HttpServletResponse response, TreeDTO treeDTO)
	{

		try
		{
			String parentElementID = request.getParameter("parentElementID");
			String checkBoxChecked = request.getParameter("checkBoxChecked");
			//System.out.println("nodeJSP = " + nodeJSP);
			request.getRequestDispatcher("treeView/" + nodeJSP
					+ "?actionType=" + actionType
					+ "&treeType=" + treeDTO.treeType
					+ "&treeBody=" + treeDTO.treeBody
					+ "&servletType=" + servletType
					+ "&pageTitle=" + treeDTO.pageTitle
					+ "&parentID=" + parentID
					+ "&checkBoxChecked=" + checkBoxChecked
					+ "&parentElementID=" + parentElementID
					+ treeDTO.additionalParams
			).forward(request, response);
		}
		catch (ServletException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}


	private String getNodeJSP(TreeDTO treeDTO)
	{
		if(treeDTO.treeType.equalsIgnoreCase("tree"))
		{
			return "treeNode.jsp";
		}
		else if(treeDTO.treeType.equalsIgnoreCase("select"))
		{
			return "dropDownNode.jsp";

		}
		else if(treeDTO.treeType.equalsIgnoreCase("flat"))
		{
			return "expandedTreeNode.jsp";

		}
		return "";
	}

	private void getTopNodes(HttpServletRequest request, HttpServletResponse response, TreeDTO treeDTO) throws ServletException, IOException
	{
		//System.out.println("METHOD: getTopNodes");
		try
		{
			List<Integer> IDs = GenericTree.getTopIDs(treeDTO.parentChildMap[0]);
			setAttributes(-1, 0, IDs, request, treeDTO);
			String nodeJsp = "tree.jsp";
			if(treeDTO.treeBody.equalsIgnoreCase("treeNode.jsp"))
			{
				nodeJsp = treeDTO.treeBody;
			}
			setParametersAndForward(0, nodeJsp, request, response, treeDTO);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}



	private void getNormalNodes(HttpServletRequest request, HttpServletResponse response, int layer, TreeDTO treeDTO) throws ServletException, IOException
	{

		//System.out.println("METHOD: getNormalNodes");
		try
		{
			int id = Integer.parseInt(request.getParameter("id"));
			//System.out.println("NNNNNNNNNNNNNN getNormalNodes layer = " + layer + " id = " + id);

			if(treeDTO.parentChildMap == null)
			{
				treeDTO.parentChildMap = new String[]{"office_ministries", "office_layers", "offices", "office_units", "office_unit_organograms"};
			}

			if(treeDTO.parentIDColumn == null)
			{
				treeDTO.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_id", "office_unit_id"};

			}



			if(layer < treeDTO.parentChildMap.length)
			{
				List<Integer> IDs = GenericTree.getChildIDs(treeDTO.parentChildMap[layer], treeDTO.parentIDColumn[layer], id);
				setAttributes(id, layer, IDs, request, treeDTO);
				String nodeJsp = getNodeJSP(treeDTO);
				treeDTO.treeType = "tree";
				//System.out.println("Going to geoNode, treeDTO.treeType = " + treeDTO.treeType);
				setParametersAndForward(id, nodeJsp, request, response, treeDTO);

			}
			else
			{
				//System.out.println("Maximum Layer Reached");
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
